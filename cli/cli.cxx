/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <stdlib.h>
#include <QtDebug>
#include <QTranslator>
#include <QLibraryInfo>
#include <QLocalSocket>

#include "core/component.hxx"
#include "core/abort.hxx"
#include "clicore/terminaloutput.hxx"
#include "clicore/graphics.hxx"
#include "core/threadpool.hxx"
#include "core/memory.hxx"
#include "cli.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::CLI;

Q_DECLARE_METATYPE(QLocalSocket::LocalSocketState);

CPD3CLI::CPD3CLI(QObject *parent, Archive::Access &archive) : QObject(parent), archive(archive)
{
    action = nullptr;
    pipeline = nullptr;

    enablePipelineAcceleration = false;
    enablePipelineSerialization = false;
    disableArchiveClipping = false;

    char *env;
    if ((env = getenv("CPD3CLI_ACCELERATION")) != NULL) {
        if (env[0] && !atoi(env))
            enablePipelineAcceleration = false;
    }
    if ((env = getenv("CPD3CLI_COMBINEPROCESSES")) != NULL) {
        if (env[0] && !atoi(env))
            enablePipelineSerialization = false;
    }
    if ((env = getenv("CPD3CLI_NOCLIP")) != NULL) {
        if (env[0] && atoi(env))
            disableArchiveClipping = true;
    }
}

CPD3CLI::~CPD3CLI() = default;

void CPD3CLI::shutdown()
{
    progress.abort();

    if (action)
        action->signalTerminate();
    if (pipeline)
        pipeline->signalTerminate();

    if (action)
        action->wait();
    if (pipeline)
        pipeline->waitInEventLoop();

    archive.signalTerminate();
    archive.waitForLocks();
}

void CPD3CLI::startPipeline()
{
    if (!pipeline)
        return;

    pipeline->finished.connect(this, std::bind(&CPD3CLI::checkFinished, this), true);

    if (!pipeline->start()) {
        pipeline.reset();
    }
    checkFinished();
}

void CPD3CLI::startAction()
{
    if (!action)
        return;

    QObject::connect(action.get(), &CPD3Action::finished, this, &CPD3CLI::checkFinished,
                     Qt::QueuedConnection);
    action->start();
    checkFinished();
}


void CPD3CLI::checkFinished()
{
    if (action && !action->isFinished())
        return;
    if (pipeline && !pipeline->isFinished())
        return;

    action.reset();
    pipeline.reset();

    archive.waitForLocks();

    progress.finished();

    QCoreApplication::exit(0);
}

static std::unique_ptr<Archive::Access> archive;

static void archiveShutdown()
{
    if (!archive)
        return;
    archive->signalTerminate();
    archive.reset();
}

#if defined(ALLOCATOR_TYPE) && ALLOCATOR_TYPE == 1

#include <tbb/scalable_allocator.h>

static void allocatorRelease()
{
    ::free(nullptr);
#ifdef HAVE_TBB_COMMAND
    scalable_allocation_command(TBBMALLOC_CLEAN_ALL_BUFFERS, nullptr);
#endif
}

static void installAllocator()
{ CPD3::Memory::install_release_function(allocatorRelease); }

#elif defined(ALLOCATOR_TYPE) && ALLOCATOR_TYPE == 2
#include <gperftools/malloc_extension.h>
static void allocatorRelease()
{ MallocExtension::instance()->ReleaseFreeMemory(); }
static void installAllocator()
{ CPD3::Memory::install_release_function(allocatorRelease); }
#else
static void installAllocator()
{ }
#endif

int main(int argc, char **argv)
{
    installAllocator();

    std::unique_ptr<QCoreApplication> app(CLIGraphics::createApplication(argc, argv));

    qRegisterMetaType<SequenceValue>("CPD3::Data::SequenceValue");
    qRegisterMetaType<SequenceValue::Transfer>("CPD3::Data::SequenceValue::Transfer");

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app->installTranslator(&qtTranslator);

    QString translateLocation;
    QByteArray cpd3(qgetenv("CPD3"));
    if (cpd3.length() > 0) {
        translateLocation = QString(cpd3) + "/locale";
    }
    QTranslator cpd3Translator;
    cpd3Translator.load("cpd3_" + QLocale::system().name(), translateLocation);
    app->installTranslator(&cpd3Translator);
    QTranslator cliTranslator;
    cliTranslator.load("cli_" + QLocale::system().name(), translateLocation);
    app->installTranslator(&cliTranslator);

    Abort::installAbortHandler(true);

    archive.reset(new Archive::Access);
    ::atexit(archiveShutdown);

    std::unique_ptr<CPD3CLI> cli(new CPD3CLI(app.get(), *archive));
    {
        auto arguments = app->arguments();
        if (arguments.isEmpty()) {
            cli.reset();
            qFatal("No program name available");
            return 2;
        }
        if (int rc = cli->parseArguments(std::move(arguments))) {
            cli.reset();
            ThreadPool::system()->wait();
            app.reset();
            if (rc == -1)
                return 1;
            return 0;
        }
    }

    std::unique_ptr<AbortPoller> abortPoller(new AbortPoller);
    QObject::connect(app.get(), &QCoreApplication::aboutToQuit, cli.get(), &CPD3CLI::shutdown);
    QObject::connect(abortPoller.get(), &AbortPoller::aborted, app.get(), &QCoreApplication::quit);
    abortPoller->start();
    int rc = app->exec();
    abortPoller.reset();
    cli.reset();
    archive.reset();
    ThreadPool::system()->wait();
    app.reset();

    return rc;
}
