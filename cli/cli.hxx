/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CPD3CLI_H
#define CPD3CLI_H

#include "core/first.hxx"

#include <stdio.h>
#include <QtGlobal>
#include <QObject>
#include <QIODevice>
#include <QLocalServer>
#include <QLocalSocket>
#include <QString>
#include <QStringList>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "core/timeparse.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/stream.hxx"
#include "datacore/streampipeline.hxx"
#include "clicore/terminaloutput.hxx"

class CPD3CLI : public QObject {
Q_OBJECT

    CPD3::Data::Archive::Access &archive;

    std::unique_ptr<CPD3::CPD3Action> action;

    std::unique_ptr<CPD3::Data::StreamPipeline> pipeline;
    bool enablePipelineAcceleration;
    bool enablePipelineSerialization;
    bool disableArchiveClipping;

    enum HelpType {
        ARCHIVE_READ,
        ARCHIVE_FILTER,
        ARCHIVE_FILTER_SILENT, ARCHIVE_FILTER_OUTPUT,
        OTHER_READ,
        OTHER_EXTERNAL,
        ACTION_BASIC,
        ACTION_TIME,
        ACTION_TIME_OPTIONAL,
        EXTERNAL_TIME,
        EXTERNAL_TIME_OPTIONAL
    };

    friend class Parser;

    CPD3::Data::Archive::Selection::List parseArchiveRead(const QStringList &arguments,
                                                          CPD3::Time::Bounds *clip = nullptr);

    std::vector<CPD3::Data::SequenceName::Component> parseStationSpecification
            (QStringList &arguments, int allowStations = 0, int requireStations = 0);

    int parseOptions(const QString &componentName,
                     const QString &componentDisplay,
                     const QString &componentDescription,
                     HelpType helpType,
                     QStringList &arguments,
                     CPD3::ComponentOptions &options,
                     const QList<CPD3::ComponentExample> &examples = QList<
                              CPD3::ComponentExample>(),
                     int allowStations = 0,
                     int requireStations = 0,
                     bool allowUndefinedTimes = true,
                     CPD3::Time::LogicalTimeUnit defaultTimeUnits = CPD3::Time::Day);

    CPD3::CLI::TerminalProgress progress;

    void startPipeline();

    void startAction();

public:
    CPD3CLI(QObject *parent, CPD3::Data::Archive::Access &archive);

    virtual ~CPD3CLI();

    int parseArguments(QStringList arguments);

public slots:

    void shutdown();

    void checkFinished();
};

#endif
