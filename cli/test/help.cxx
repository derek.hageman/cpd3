/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QObject>
#include <QtDebug>
#include <QProcess>
#include <QString>

#include "core/qtcompat.hxx"

class TestCLIHelp : public QObject {
Q_OBJECT
private slots:

    void initTestCase()
    {
        CPD3::Logging::suppressForTesting();

        QVERIFY(qputenv("CPD3ARCHIVE", QByteArray("sqlite:_")));
    }

    void global()
    {
        QProcess p;
        p.start("da", QStringList() << "--help");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        QVERIFY(output.contains("Usage:"));
        QVERIFY(output.contains("Available components:"));
        QVERIFY(output.contains("da.get"));
    }

    void getGlobal()
    {
        QProcess p;
        p.start("da.get", QStringList() << "--help");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        QVERIFY(output.contains("Usage: da.get"));
        QVERIFY(output.contains("Arguments:"));
    }

    void argumentInvalid()
    {
        QProcess p;
        p.start("da.get", QStringList() << "--help=ZZZZZZ");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QVERIFY(p.exitCode() != 0);
        QString output(QString::fromUtf8(p.readAllStandardError()));
        QVERIFY(!output.isEmpty());
    }

    void argumentStation()
    {
        QProcess p;
        p.start("da.get", QStringList() << "--help=station");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        QVERIFY(!output.isEmpty());
    }

    void argumentVariables()
    {
        QProcess p;
        p.start("da.get", QStringList() << "--help=variables");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        QVERIFY(!output.isEmpty());
    }

    void argumentTimes()
    {
        QProcess p;
        p.start("da.get", QStringList() << "--help=times");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        QVERIFY(!output.isEmpty());
    }

    void argumentArchive()
    {
        QProcess p;
        p.start("da.get", QStringList() << "--help=archive");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        QVERIFY(!output.isEmpty());
    }

    void argumentSingleString()
    {
        QProcess p;
        p.start("da.export", QStringList() << "--help=join-delimiter");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        QVERIFY(!output.isEmpty());
    }

    void argumentBoolean()
    {
        QProcess p;
        p.start("da.export", QStringList() << "--help=time-iso");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        QVERIFY(!output.isEmpty());
    }

    void argumentEnum()
    {
        QProcess p;
        p.start("da.export", QStringList() << "--help=mode");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        QVERIFY(!output.isEmpty());
    }

    void argumentInstrumentSuffixSet()
    {
        QProcess p;
        p.start("da.corr_stp", QStringList() << "--help=instruments");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        QVERIFY(!output.isEmpty());
    }

    void argumentFilterInput()
    {
        QProcess p;
        p.start("da.corr_stp", QStringList() << "--help=p");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        QVERIFY(!output.isEmpty());
    }

    void argumentFilterOperate()
    {
        QProcess p;
        p.start("da.corr_stp", QStringList() << "--help=correct-volume");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        QVERIFY(!output.isEmpty());
    }

    void argumentTimeIntervalSelection()
    {
        QProcess p;
        p.start("da.time_shift", QStringList() << "--help=shift");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        QVERIFY(!output.isEmpty());
    }
};

QTEST_APPLESS_MAIN(TestCLIHelp)

#include "help.moc"
