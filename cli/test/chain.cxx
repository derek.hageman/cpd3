/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <stdlib.h>
#include <QTest>
#include <QObject>
#include <QtDebug>
#include <QProcess>
#include <QString>
#include <QList>
#include <QTemporaryFile>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/externalsink.hxx"
#include "datacore/archive/access.hxx"
#include "core/number.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Data;


class TestCLIChain : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

private slots:

    void initTestCase()
    {
        CPD3::Logging::suppressForTesting();

        QVERIFY(databaseFile.open());

        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());


        SequenceValue::Transfer input;
        for (int i = 0; i < 1000; i++) {
            input.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(13.66),
                               1230768000.0 + i,
                               1230768001.0 + i);
            input.emplace_back(SequenceName("brw", "raw", "P_S11"), Variant::Root(911.93),
                               1230768000.0 + i,
                               1230768001.0 + i);
            input.emplace_back(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(12.0),
                               1230768000.0 + i,
                               1230768001.0 + i);
        }

        Archive::Access(databaseFile).writeSynchronous(input);

        unsetenv("CPD3CLI_ACCELERATION");
        unsetenv("CPD3CLI_COMBINEPROCESSES");
    }

    void filterFile()
    {
        QTemporaryFile temp;
        temp.open();

        QProcess p;
        p.start("sh", QStringList() <<
                "-c" <<
                "da.corr_stp brw T_S11,P_S11,BsG_S11 2009:1 1d raw > " + temp.fileName());
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        p.start("da.corr_stp", QStringList() << temp.fileName());
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        ExternalConverter *reader = new StandardDataInput();
        StreamSink::Buffer buffer;
        reader->start();
        reader->setEgress(&buffer);
        reader->incomingData(p.readAllStandardOutput());
        reader->endData();
        QVERIFY(reader->wait());
        delete reader;

        QCOMPARE((int) buffer.values().size(), 3000);
        double den = ((911.93 / 1013.25) * (273.15 / (13.66 + 273.15)));
        double expected = (12.0 / den) / den;
        for (int i = 0; i < 1000; i++) {
            int base = i * 3;
            for (int j = 0; j < 3; j++) {
                SequenceValue check(buffer.values().at(base + j));
                QCOMPARE(check.getStart(), 1230768000.0 + i);
                QCOMPARE(check.getEnd(), 1230768001.0 + i);
                QCOMPARE(check.getStation(), Data::SequenceName::Component("brw"));
                QCOMPARE(check.getArchive(), Data::SequenceName::Component("raw"));

                if (check.getVariable() == "T_S11") {
                    QCOMPARE(check.getValue().toDouble(), 13.66);
                } else if (check.getVariable() == "P_S11") {
                    QCOMPARE(check.getValue().toDouble(), 911.93);
                } else if (check.getVariable() == "BsG_S11") {
                    QCOMPARE(check.getValue().toDouble(), expected);
                } else {
                    QFAIL("Unknown variable");
                }
            }
        }
    }

    void filterChain()
    {
        QProcess p;
        p.start("sh",
                QStringList() << "-c" << "da.corr_stp brw T_S11,P_S11,BsG_S11 2009:1 1d raw | "
                        "da.corr_stp -");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        ExternalConverter *reader = new StandardDataInput();
        StreamSink::Buffer buffer;
        reader->start();
        reader->setEgress(&buffer);
        reader->incomingData(p.readAllStandardOutput());
        reader->endData();
        QVERIFY(reader->wait());
        delete reader;

        QCOMPARE((int) buffer.values().size(), 3000);
        double den = ((911.93 / 1013.25) * (273.15 / (13.66 + 273.15)));
        double expected = (12.0 / den) / den;
        for (int i = 0; i < 1000; i++) {
            int base = i * 3;
            for (int j = 0; j < 3; j++) {
                SequenceValue check(buffer.values().at(base + j));
                QCOMPARE(check.getStart(), 1230768000.0 + i);
                QCOMPARE(check.getEnd(), 1230768001.0 + i);
                QCOMPARE(check.getStation(), Data::SequenceName::Component("brw"));
                QCOMPARE(check.getArchive(), Data::SequenceName::Component("raw"));

                if (check.getVariable() == "T_S11") {
                    QCOMPARE(check.getValue().toDouble(), 13.66);
                } else if (check.getVariable() == "P_S11") {
                    QCOMPARE(check.getValue().toDouble(), 911.93);
                } else if (check.getVariable() == "BsG_S11") {
                    QCOMPARE(check.getValue().toDouble(), expected);
                } else {
                    QFAIL("Unknown variable");
                }
            }
        }
    }

    void filterTrippleChain()
    {
        QProcess p;
        p.start("sh",
                QStringList() << "-c" << "da.corr_stp brw T_S11,P_S11,BsG_S11 2009:1 1d raw | "
                        "da.corr_stp - | da.corr_stp");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        ExternalConverter *reader = new StandardDataInput();
        StreamSink::Buffer buffer;
        reader->start();
        reader->setEgress(&buffer);
        reader->incomingData(p.readAllStandardOutput());
        reader->endData();
        QVERIFY(reader->wait());
        delete reader;

        QCOMPARE((int) buffer.values().size(), 3000);
        double den = ((911.93 / 1013.25) * (273.15 / (13.66 + 273.15)));
        double expected = ((12.0 / den) / den) / den;
        for (int i = 0; i < 1000; i++) {
            int base = i * 3;
            for (int j = 0; j < 3; j++) {
                SequenceValue check(buffer.values().at(base + j));
                QCOMPARE(check.getStart(), 1230768000.0 + i);
                QCOMPARE(check.getEnd(), 1230768001.0 + i);
                QCOMPARE(check.getStation(), Data::SequenceName::Component("brw"));
                QCOMPARE(check.getArchive(), Data::SequenceName::Component("raw"));

                if (check.getVariable() == "T_S11") {
                    QCOMPARE(check.getValue().toDouble(), 13.66);
                } else if (check.getVariable() == "P_S11") {
                    QCOMPARE(check.getValue().toDouble(), 911.93);
                } else if (check.getVariable() == "BsG_S11") {
                    QCOMPARE(check.getValue().toDouble(), expected);
                } else {
                    QFAIL("Unknown variable");
                }
            }
        }
    }

    void filterChainNoCombine()
    {
        setenv("CPD3CLI_COMBINEPROCESSES", "0", 1);

        QProcess p;
        p.start("sh",
                QStringList() << "-c" << "da.corr_stp brw T_S11,P_S11,BsG_S11 2009:1 1d raw | "
                        "da.corr_stp -");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        ExternalConverter *reader = new StandardDataInput();
        StreamSink::Buffer buffer;
        reader->start();
        reader->setEgress(&buffer);
        reader->incomingData(p.readAllStandardOutput());
        reader->endData();
        QVERIFY(reader->wait());
        delete reader;

        QCOMPARE((int) buffer.values().size(), 3000);
        double den = ((911.93 / 1013.25) * (273.15 / (13.66 + 273.15)));
        double expected = (12.0 / den) / den;
        for (int i = 0; i < 1000; i++) {
            int base = i * 3;
            for (int j = 0; j < 3; j++) {
                SequenceValue check(buffer.values().at(base + j));
                QCOMPARE(check.getStart(), 1230768000.0 + i);
                QCOMPARE(check.getEnd(), 1230768001.0 + i);
                QCOMPARE(check.getStation(), Data::SequenceName::Component("brw"));
                QCOMPARE(check.getArchive(), Data::SequenceName::Component("raw"));

                if (check.getVariable() == "T_S11") {
                    QCOMPARE(check.getValue().toDouble(), 13.66);
                } else if (check.getVariable() == "P_S11") {
                    QCOMPARE(check.getValue().toDouble(), 911.93);
                } else if (check.getVariable() == "BsG_S11") {
                    QCOMPARE(check.getValue().toDouble(), expected);
                } else {
                    QFAIL("Unknown variable");
                }
            }
        }
    }

    void filterChainNoRaw()
    {
        setenv("CPD3CLI_COMBINEPROCESSES", "0", 1);
        setenv("CPD3CLI_ACCELERATION", "0", 1);

        QProcess p;
        p.start("sh",
                QStringList() << "-c" << "da.corr_stp brw T_S11,P_S11,BsG_S11 2009:1 1d raw | "
                        "da.corr_stp -");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        ExternalConverter *reader = new StandardDataInput();
        StreamSink::Buffer buffer;
        reader->start();
        reader->setEgress(&buffer);
        reader->incomingData(p.readAllStandardOutput());
        reader->endData();
        QVERIFY(reader->wait());
        delete reader;

        QCOMPARE((int) buffer.values().size(), 3000);
        double den = ((911.93 / 1013.25) * (273.15 / (13.66 + 273.15)));
        double expected = (12.0 / den) / den;
        for (int i = 0; i < 1000; i++) {
            int base = i * 3;
            for (int j = 0; j < 3; j++) {
                SequenceValue check(buffer.values().at(base + j));
                QCOMPARE(check.getStart(), 1230768000.0 + i);
                QCOMPARE(check.getEnd(), 1230768001.0 + i);
                QCOMPARE(check.getStation(), Data::SequenceName::Component("brw"));
                QCOMPARE(check.getArchive(), Data::SequenceName::Component("raw"));

                if (check.getVariable() == "T_S11") {
                    QCOMPARE(check.getValue().toDouble(), 13.66);
                } else if (check.getVariable() == "P_S11") {
                    QCOMPARE(check.getValue().toDouble(), 911.93);
                } else if (check.getVariable() == "BsG_S11") {
                    QCOMPARE(check.getValue().toDouble(), expected);
                } else {
                    QFAIL("Unknown variable");
                }
            }
        }
    }

    void generalFilterFile()
    {
        QTemporaryFile temp;
        temp.open();

        QProcess p;
        p.start("sh", QStringList() <<
                "-c" <<
                "da.time_shift --shift=10s brw T_S11,P_S11,BsG_S11 2009:1 1d raw > " +
                        temp.fileName());
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        p.start("da.time_shift", QStringList() << "--shift=10s" << temp.fileName());
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        ExternalConverter *reader = new StandardDataInput();
        StreamSink::Buffer buffer;
        reader->start();
        reader->setEgress(&buffer);
        reader->incomingData(p.readAllStandardOutput());
        reader->endData();
        QVERIFY(reader->wait());
        delete reader;

        QCOMPARE((int) buffer.values().size(), 3000);
        for (int i = 0; i < 1000; i++) {
            int base = i * 3;
            for (int j = 0; j < 3; j++) {
                SequenceValue check(buffer.values().at(base + j));
                QCOMPARE(check.getStart(), 1230768000.0 + i + 20.0);
                QCOMPARE(check.getEnd(), 1230768001.0 + i + 20.0);
                QCOMPARE(check.getStation(), Data::SequenceName::Component("brw"));
                QCOMPARE(check.getArchive(), Data::SequenceName::Component("raw"));

                if (check.getVariable() == "T_S11") {
                    QCOMPARE(check.getValue().toDouble(), 13.66);
                } else if (check.getVariable() == "P_S11") {
                    QCOMPARE(check.getValue().toDouble(), 911.93);
                } else if (check.getVariable() == "BsG_S11") {
                    QCOMPARE(check.getValue().toDouble(), 12.0);
                } else {
                    QFAIL("Unknown variable");
                }
            }
        }
    }

    void generalFilterChain()
    {
        QProcess p;
        p.start("sh", QStringList() <<
                "-c" <<
                "da.time_shift --shift=10s brw T_S11,P_S11,BsG_S11 2009:1 1d raw | "
                        "da.time_shift --shift=-15s -");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        ExternalConverter *reader = new StandardDataInput();
        StreamSink::Buffer buffer;
        reader->start();
        reader->setEgress(&buffer);
        reader->incomingData(p.readAllStandardOutput());
        reader->endData();
        QVERIFY(reader->wait());
        delete reader;

        QCOMPARE((int) buffer.values().size(), 3000);
        for (int i = 0; i < 1000; i++) {
            int base = i * 3;
            for (int j = 0; j < 3; j++) {
                SequenceValue check(buffer.values().at(base + j));
                QCOMPARE(check.getStart(), 1230768000.0 + i - 5.0);
                QCOMPARE(check.getEnd(), 1230768001.0 + i - 5.0);
                QCOMPARE(check.getStation(), Data::SequenceName::Component("brw"));
                QCOMPARE(check.getArchive(), Data::SequenceName::Component("raw"));

                if (check.getVariable() == "T_S11") {
                    QCOMPARE(check.getValue().toDouble(), 13.66);
                } else if (check.getVariable() == "P_S11") {
                    QCOMPARE(check.getValue().toDouble(), 911.93);
                } else if (check.getVariable() == "BsG_S11") {
                    QCOMPARE(check.getValue().toDouble(), 12.0);
                } else {
                    QFAIL("Unknown variable");
                }
            }
        }
    }

    void generalFilterTrippleChain()
    {
        QProcess p;
        p.start("sh", QStringList() <<
                "-c" <<
                "da.time_shift --shift=10s brw T_S11,P_S11,BsG_S11 2009:1 1d raw | "
                        "da.time_shift --shift=10s - | da.time_shift --shift=-25s");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        ExternalConverter *reader = new StandardDataInput();
        StreamSink::Buffer buffer;
        reader->start();
        reader->setEgress(&buffer);
        reader->incomingData(p.readAllStandardOutput());
        reader->endData();
        QVERIFY(reader->wait());
        delete reader;

        QCOMPARE((int) buffer.values().size(), 3000);
        for (int i = 0; i < 1000; i++) {
            int base = i * 3;
            for (int j = 0; j < 3; j++) {
                SequenceValue check(buffer.values().at(base + j));
                QCOMPARE(check.getStart(), 1230768000.0 + i - 5.0);
                QCOMPARE(check.getEnd(), 1230768001.0 + i - 5.0);
                QCOMPARE(check.getStation(), Data::SequenceName::Component("brw"));
                QCOMPARE(check.getArchive(), Data::SequenceName::Component("raw"));

                if (check.getVariable() == "T_S11") {
                    QCOMPARE(check.getValue().toDouble(), 13.66);
                } else if (check.getVariable() == "P_S11") {
                    QCOMPARE(check.getValue().toDouble(), 911.93);
                } else if (check.getVariable() == "BsG_S11") {
                    QCOMPARE(check.getValue().toDouble(), 12.0);
                } else {
                    QFAIL("Unknown variable");
                }
            }
        }
    }

    void generalFilterChainNoCombine()
    {
        setenv("CPD3CLI_COMBINEPROCESSES", "0", 1);

        QProcess p;
        p.start("sh", QStringList() <<
                "-c" <<
                "da.time_shift --shift=5s brw T_S11,P_S11,BsG_S11 2009:1 1d raw | "
                        "da.time_shift --shift=15s -");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        ExternalConverter *reader = new StandardDataInput();
        StreamSink::Buffer buffer;
        reader->start();
        reader->setEgress(&buffer);
        reader->incomingData(p.readAllStandardOutput());
        reader->endData();
        QVERIFY(reader->wait());
        delete reader;

        QCOMPARE((int) buffer.values().size(), 3000);
        for (int i = 0; i < 1000; i++) {
            int base = i * 3;
            for (int j = 0; j < 3; j++) {
                SequenceValue check(buffer.values().at(base + j));
                QCOMPARE(check.getStart(), 1230768000.0 + i + 20.0);
                QCOMPARE(check.getEnd(), 1230768001.0 + i + 20.0);
                QCOMPARE(check.getStation(), Data::SequenceName::Component("brw"));
                QCOMPARE(check.getArchive(), Data::SequenceName::Component("raw"));

                if (check.getVariable() == "T_S11") {
                    QCOMPARE(check.getValue().toDouble(), 13.66);
                } else if (check.getVariable() == "P_S11") {
                    QCOMPARE(check.getValue().toDouble(), 911.93);
                } else if (check.getVariable() == "BsG_S11") {
                    QCOMPARE(check.getValue().toDouble(), 12.0);
                } else {
                    QFAIL("Unknown variable");
                }
            }
        }
    }

    void generalFilterChainNoRaw()
    {
        setenv("CPD3CLI_COMBINEPROCESSES", "0", 1);
        setenv("CPD3CLI_ACCELERATION", "0", 1);

        QProcess p;
        p.start("sh", QStringList() <<
                "-c" <<
                "da.time_shift --shift=15s brw T_S11,P_S11,BsG_S11 2009:1 1d raw | "
                        "da.time_shift --shift=5s -");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        ExternalConverter *reader = new StandardDataInput();
        StreamSink::Buffer buffer;
        reader->start();
        reader->setEgress(&buffer);
        reader->incomingData(p.readAllStandardOutput());
        reader->endData();
        QVERIFY(reader->wait());
        delete reader;

        QCOMPARE((int) buffer.values().size(), 3000);
        for (int i = 0; i < 1000; i++) {
            int base = i * 3;
            for (int j = 0; j < 3; j++) {
                SequenceValue check(buffer.values().at(base + j));
                QCOMPARE(check.getStart(), 1230768000.0 + i + 20.0);
                QCOMPARE(check.getEnd(), 1230768001.0 + i + 20.0);
                QCOMPARE(check.getStation(), Data::SequenceName::Component("brw"));
                QCOMPARE(check.getArchive(), Data::SequenceName::Component("raw"));

                if (check.getVariable() == "T_S11") {
                    QCOMPARE(check.getValue().toDouble(), 13.66);
                } else if (check.getVariable() == "P_S11") {
                    QCOMPARE(check.getValue().toDouble(), 911.93);
                } else if (check.getVariable() == "BsG_S11") {
                    QCOMPARE(check.getValue().toDouble(), 12.0);
                } else {
                    QFAIL("Unknown variable");
                }
            }
        }
    }

    void generalFilterBasicFilterChain()
    {
        QProcess p;
        p.start("sh", QStringList() <<
                "-c" <<
                "da.time_shift --shift=10s brw T_S11,P_S11,BsG_S11 2009:1 1d raw | "
                        "da.corr_stp - | da.time_shift --shift=-25s");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        ExternalConverter *reader = new StandardDataInput();
        StreamSink::Buffer buffer;
        reader->start();
        reader->setEgress(&buffer);
        reader->incomingData(p.readAllStandardOutput());
        reader->endData();
        QVERIFY(reader->wait());
        delete reader;

        QCOMPARE((int) buffer.values().size(), 3000);
        double den = ((911.93 / 1013.25) * (273.15 / (13.66 + 273.15)));
        double expected = (12.0 / den);
        for (int i = 0; i < 1000; i++) {
            int base = i * 3;
            for (int j = 0; j < 3; j++) {
                SequenceValue check(buffer.values().at(base + j));
                QCOMPARE(check.getStart(), 1230768000.0 + i - 15.0);
                QCOMPARE(check.getEnd(), 1230768001.0 + i - 15.0);
                QCOMPARE(check.getStation(), Data::SequenceName::Component("brw"));
                QCOMPARE(check.getArchive(), Data::SequenceName::Component("raw"));

                if (check.getVariable() == "T_S11") {
                    QCOMPARE(check.getValue().toDouble(), 13.66);
                } else if (check.getVariable() == "P_S11") {
                    QCOMPARE(check.getValue().toDouble(), 911.93);
                } else if (check.getVariable() == "BsG_S11") {
                    QCOMPARE(check.getValue().toDouble(), expected);
                } else {
                    QFAIL("Unknown variable");
                }
            }
        }
    }

    void basicFilterGeneralFilterChain()
    {
        QProcess p;
        p.start("sh",
                QStringList() << "-c" << "da.corr_stp brw T_S11,P_S11,BsG_S11 2009:1 1d raw | "
                        "da.time_shift --shift=10s - | da.corr_stp");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        ExternalConverter *reader = new StandardDataInput();
        StreamSink::Buffer buffer;
        reader->start();
        reader->setEgress(&buffer);
        reader->incomingData(p.readAllStandardOutput());
        reader->endData();
        QVERIFY(reader->wait());
        delete reader;

        QCOMPARE((int) buffer.values().size(), 3000);
        double den = ((911.93 / 1013.25) * (273.15 / (13.66 + 273.15)));
        double expected = (12.0 / den) / den;
        for (int i = 0; i < 1000; i++) {
            int base = i * 3;
            for (int j = 0; j < 3; j++) {
                SequenceValue check(buffer.values().at(base + j));
                QCOMPARE(check.getStart(), 1230768000.0 + i + 10.0);
                QCOMPARE(check.getEnd(), 1230768001.0 + i + 10.0);
                QCOMPARE(check.getStation(), Data::SequenceName::Component("brw"));
                QCOMPARE(check.getArchive(), Data::SequenceName::Component("raw"));

                if (check.getVariable() == "T_S11") {
                    QCOMPARE(check.getValue().toDouble(), 13.66);
                } else if (check.getVariable() == "P_S11") {
                    QCOMPARE(check.getValue().toDouble(), 911.93);
                } else if (check.getVariable() == "BsG_S11") {
                    QCOMPARE(check.getValue().toDouble(), expected);
                } else {
                    QFAIL("Unknown variable");
                }
            }
        }
    }

    void egressArchive()
    {
        QProcess p;
        p.start("da.export", QStringList() << "brw" << "BsG_S11" << "2009:1" << "1d" << "raw");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        QByteArray output(p.readAllStandardOutput());
        QVERIFY(output.startsWith("DateTimeUTC,BsG_S11"));
        while (output.endsWith('\n') || output.endsWith('\r')) { output.chop(1); }
        QVERIFY(output.endsWith("2009-01-01 00:16:39,00012.000"));
    }

    void egressFile()
    {
        QTemporaryFile temp;
        temp.open();

        QProcess p;
        p.start("sh", QStringList() <<
                "-c" <<
                "da.corr_stp brw T_S11,P_S11,BsG_S11 2009:1 1d raw > " + temp.fileName());
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        p.start("da.export", QStringList() << "--flags=breakdown" << temp.fileName());
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        QByteArray output(p.readAllStandardOutput());
        QVERIFY(output.startsWith("DateTimeUTC,BsG_S11,T_S11,P_S11"));
        while (output.endsWith('\n') || output.endsWith('\r')) { output.chop(1); }
        QVERIFY(output.endsWith("2009-01-01 00:16:39,00014.000,00013.660,00911.930"));
    }

    void egressChain()
    {
        QProcess p;
        p.start("sh", QStringList() << "-c" << "da.get brw BsG_S11 2009:1 1d raw | "
                "da.export");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        QByteArray output(p.readAllStandardOutput());
        QVERIFY(output.startsWith("DateTimeUTC,BsG_S11"));
        while (output.endsWith('\n') || output.endsWith('\r')) { output.chop(1); }
        QVERIFY(output.endsWith("2009-01-01 00:16:39,00012.000"));
    }

    void egressShortChain()
    {
        QProcess p;
        p.start("sh", QStringList() << "-c" << "da.get brw BsG_S11 2009:1 60s raw | "
                "da.export");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        QByteArray output(p.readAllStandardOutput());
        QVERIFY(output.startsWith("DateTimeUTC,BsG_S11"));
        while (output.endsWith('\n') || output.endsWith('\r')) { output.chop(1); }
        QVERIFY(output.endsWith("2009-01-01 00:00:59,00012.000"));
    }

    void cleanup()
    {
        unsetenv("CPD3CLI_ACCELERATION");
        unsetenv("CPD3CLI_COMBINEPROCESSES");
    }

};

QTEST_APPLESS_MAIN(TestCLIChain)

#include "chain.moc"
