/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <stdlib.h>
#include <QTest>
#include <QObject>
#include <QtDebug>
#include <QProcess>
#include <QString>
#include <QList>
#include <QTemporaryFile>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/stream.hxx"
#include "core/number.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class TestCLIArguments : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

private slots:

    void initTestCase()
    {
        CPD3::Logging::suppressForTesting();

        QVERIFY(databaseFile.open());

        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());

        SequenceValue::Transfer input;
        for (int i = 0; i < 1000; i++) {
            input.emplace_back(SequenceName("brw", "raw", "T_S11"), Variant::Root(13.66),
                               1230768000.0 + i,
                               1230768001.0 + i);
            input.emplace_back(SequenceName("brw", "raw", "P_S11"), Variant::Root(911.93),
                               1230768000.0 + i,
                               1230768001.0 + i);
            input.emplace_back(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(12.0),
                               1230768000.0 + i,
                               1230768001.0 + i);
            input.emplace_back(SequenceName("brw", "raw", "BsB_S11", {"pm1"}), Variant::Root(13.0),
                               1230768000.0 + i, 1230768001.0 + i);
        }

        Archive::Access(databaseFile).writeSynchronous(input);
    }

    void getSimple()
    {
        QProcess p;
        p.start("da.get",
                QStringList() << "brw" << "T_S11,P_S11,BsG_S11,::BsB_S11:!pm10:pm1" << "2009:1"
                              << "1d" << "raw");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        ExternalConverter *reader = new StandardDataInput();
        StreamSink::Buffer buffer;
        reader->start();
        reader->setEgress(&buffer);
        reader->incomingData(p.readAllStandardOutput());
        reader->endData();
        QVERIFY(reader->wait());
        delete reader;

        QCOMPARE((int) buffer.values().size(), 4000);
        for (int i = 0; i < 1000; i++) {
            int base = i * 4;
            for (int j = 0; j < 4; j++) {
                SequenceValue check(buffer.values().at(base + j));
                QCOMPARE(check.getStart(), 1230768000.0 + i);
                QCOMPARE(check.getEnd(), 1230768001.0 + i);
                QCOMPARE(check.getStation(), Data::SequenceName::Component("brw"));
                QCOMPARE(check.getArchive(), Data::SequenceName::Component("raw"));

                if (check.getVariable() == "T_S11") {
                    QCOMPARE(check.getValue().toDouble(), 13.66);
                } else if (check.getVariable() == "P_S11") {
                    QCOMPARE(check.getValue().toDouble(), 911.93);
                } else if (check.getVariable() == "BsG_S11") {
                    QCOMPARE(check.getValue().toDouble(), 12.0);
                } else if (check.getVariable() == "BsB_S11") {
                    QCOMPARE(check.getValue().toDouble(), 13.0);
                } else {
                    QFAIL("Unknown variable");
                }
            }
        }
    }

    void filterSimple()
    {
        QProcess p;
        p.start("da.corr_stp",
                QStringList() << "brw" << "T_S11,P_S11,BsG_S11,BsB_S11" << "2009:1" << "1d"
                              << "raw");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        ExternalConverter *reader = new StandardDataInput();
        StreamSink::Buffer buffer;
        reader->start();
        reader->setEgress(&buffer);
        reader->incomingData(p.readAllStandardOutput());
        reader->endData();
        QVERIFY(reader->wait());
        delete reader;

        QCOMPARE((int) buffer.values().size(), 4000);
        double expected = 12.0 / ((911.93 / 1013.25) * (273.15 / (13.66 + 273.15)));
        for (int i = 0; i < 1000; i++) {
            int base = i * 4;
            for (int j = 0; j < 4; j++) {
                SequenceValue check(buffer.values().at(base + j));
                QCOMPARE(check.getStart(), 1230768000.0 + i);
                QCOMPARE(check.getEnd(), 1230768001.0 + i);
                QCOMPARE(check.getStation(), Data::SequenceName::Component("brw"));
                QCOMPARE(check.getArchive(), Data::SequenceName::Component("raw"));

                if (check.getVariable() == "T_S11") {
                    QCOMPARE(check.getValue().toDouble(), 13.66);
                } else if (check.getVariable() == "P_S11") {
                    QCOMPARE(check.getValue().toDouble(), 911.93);
                } else if (check.getVariable() == "BsG_S11") {
                    QCOMPARE(check.getValue().toDouble(), expected);
                    QVERIFY(check.getFlavors().empty());
                } else if (check.getVariable() == "BsB_S11") {
                    QVERIFY(!FP::defined(check.getValue().toDouble()));
                    QCOMPARE(check.getFlavors(), Data::SequenceName::Flavors{"pm1"});
                } else {
                    QFAIL("Unknown variable");
                }
            }
        }
    }

    void instrumentSuffixList()
    {
        QProcess p;
        p.start("da.corr_stp",
                QStringList() << "--instruments=S11,S12" << "brw" << "T_S11,P_S11,BsG_S11"
                              << "2009:1" << "1d" << "raw");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        ExternalConverter *reader = new StandardDataInput();
        StreamSink::Buffer buffer;
        reader->start();
        reader->setEgress(&buffer);
        reader->incomingData(p.readAllStandardOutput());
        reader->endData();
        QVERIFY(reader->wait());
        delete reader;

        QCOMPARE((int) buffer.values().size(), 3000);
        double expected = 12.0 / ((911.93 / 1013.25) * (273.15 / (13.66 + 273.15)));
        for (int i = 0; i < 1000; i++) {
            int base = i * 3;
            for (int j = 0; j < 3; j++) {
                SequenceValue check(buffer.values().at(base + j));
                QCOMPARE(check.getStart(), 1230768000.0 + i);
                QCOMPARE(check.getEnd(), 1230768001.0 + i);
                QCOMPARE(check.getStation(), Data::SequenceName::Component("brw"));
                QCOMPARE(check.getArchive(), Data::SequenceName::Component("raw"));

                if (check.getVariable() == "T_S11") {
                    QCOMPARE(check.getValue().toDouble(), 13.66);
                } else if (check.getVariable() == "P_S11") {
                    QCOMPARE(check.getValue().toDouble(), 911.93);
                } else if (check.getVariable() == "BsG_S11") {
                    QCOMPARE(check.getValue().toDouble(), expected);
                } else {
                    QFAIL("Unknown variable");
                }
            }
        }
    }


    void variousFilterOptions()
    {
        QProcess p;
        p.start("da.corr_stp", QStringList()
                << "--correct-concentration=BsG_S11;::BsB_S11:=pm1,1230768000.0,1230769001.0"
                << "--p=910.93" << "--t=::T_S11:="
                << "--stp-t=273.15,1230768000.0,1230768500.0/T_S11,,,1230768500.0,none" << "brw"
                << "T_S11,P_S11,BsG_S11,brw:raw:BsB_S11:!pm10:pm1" << "2009:1" << "1d" << "raw");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        ExternalConverter *reader = new StandardDataInput();
        StreamSink::Buffer buffer;
        reader->start();
        reader->setEgress(&buffer);
        reader->incomingData(p.readAllStandardOutput());
        reader->endData();
        QVERIFY(reader->wait());
        delete reader;

        QCOMPARE((int) buffer.values().size(), 4000);
        for (int i = 0; i < 1000; i++) {
            int base = i * 4;
            for (int j = 0; j < 4; j++) {
                SequenceValue check(buffer.values().at(base + j));
                QCOMPARE(check.getStart(), 1230768000.0 + i);
                QCOMPARE(check.getEnd(), 1230768001.0 + i);
                QCOMPARE(check.getStation(), Data::SequenceName::Component("brw"));
                QCOMPARE(check.getArchive(), Data::SequenceName::Component("raw"));

                if (check.getVariable() == "T_S11") {
                    QCOMPARE(check.getValue().toDouble(), 13.66);
                } else if (check.getVariable() == "P_S11") {
                    QCOMPARE(check.getValue().toDouble(), 911.93);
                } else if (check.getVariable() == "BsG_S11") {
                    double expected;
                    if (check.getStart() < 1230768500.0) {
                        expected = 12.0 / ((910.93 / 1013.25) * (273.15 / (13.66 + 273.15)));
                    } else {
                        expected = 12.0 / ((910.93 / 1013.25));
                    }
                    QCOMPARE(check.getValue().toDouble(), expected);
                } else if (check.getVariable() == "BsB_S11") {
                    double expected;
                    if (check.getStart() < 1230768500.0) {
                        expected = 13.0 / ((910.93 / 1013.25) * (273.15 / (13.66 + 273.15)));
                    } else {
                        expected = 13.0 / ((910.93 / 1013.25));
                    }
                    QCOMPARE(check.getValue().toDouble(), expected);
                } else {
                    QFAIL("Unknown variable");
                }
            }
        }
    }

    void variousExportOptions()
    {
        QProcess p;
        p.start("da.export", QStringList() << "--mode=cpd1" << "--time-iso" << "--time-excel=off"
                                           << "--time-fractional-year=true" << "--join-delimiter=;"
                                           << "brw" << "T_S11,P_S11,BsG_S11" << "2009:1" << "1d"
                                           << "raw");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        QByteArray output(p.readAllStandardOutput());
        QVERIFY(output.startsWith("STN;DateTimeUTC;FractionalYear;Year;DOY;BsG_S11;T_S11;P_S11"));
        while (output.endsWith('\n') || output.endsWith('\r')) { output.chop(1); }
        QVERIFY(output.endsWith(
                "BRW;2009-01-01T00:16:39Z;2009.00003168;2009;001.01156;00012.000;00013.660;00911.930"));
    }

    void timeIntervalSelectionBasic()
    {
        QProcess p;
        p.start("da.time_shift",
                QStringList() << "--shift=10s" << "brw" << "T_S11,P_S11,BsG_S11" << "2009:1" << "1d"
                              << "raw");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        ExternalConverter *reader = new StandardDataInput();
        StreamSink::Buffer buffer;
        reader->start();
        reader->setEgress(&buffer);
        reader->incomingData(p.readAllStandardOutput());
        reader->endData();
        QVERIFY(reader->wait());
        delete reader;

        QCOMPARE((int) buffer.values().size(), 3000);
        for (int i = 0; i < 1000; i++) {
            int base = i * 3;
            for (int j = 0; j < 3; j++) {
                SequenceValue check(buffer.values().at(base + j));
                QCOMPARE(check.getStart(), 1230768010.0 + i);
                QCOMPARE(check.getEnd(), 1230768011.0 + i);
                QCOMPARE(check.getStation(), Data::SequenceName::Component("brw"));
                QCOMPARE(check.getArchive(), Data::SequenceName::Component("raw"));

                if (check.getVariable() == "T_S11") {
                    QCOMPARE(check.getValue().toDouble(), 13.66);
                } else if (check.getVariable() == "P_S11") {
                    QCOMPARE(check.getValue().toDouble(), 911.93);
                } else if (check.getVariable() == "BsG_S11") {
                    QCOMPARE(check.getValue().toDouble(), 12.0);
                } else {
                    QFAIL("Unknown variable");
                }
            }
        }
    }

    void timeIntervalSelectionAdvanced()
    {
        QProcess p;
        p.start("da.time_shift",
                QStringList() << "--shift=10s,1230768000.0,1230768500.0/1m,1230768500.0,none"
                              << "brw" << "T_S11,P_S11,BsG_S11" << "2009:1" << "1d" << "raw");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        ExternalConverter *reader = new StandardDataInput();
        StreamSink::Buffer buffer;
        reader->start();
        reader->setEgress(&buffer);
        reader->incomingData(p.readAllStandardOutput());
        reader->endData();
        QVERIFY(reader->wait());
        delete reader;

        QCOMPARE((int) buffer.values().size(), 3000);
        for (int i = 0; i < 1000; i++) {
            int base = i * 3;
            for (int j = 0; j < 3; j++) {
                SequenceValue check(buffer.values().at(base + j));
                if (i < 500) {
                    QCOMPARE(check.getStart(), 1230768010.0 + i);
                    QCOMPARE(check.getEnd(), 1230768011.0 + i);
                } else {
                    QCOMPARE(check.getStart(), 1230768060.0 + i);
                    QCOMPARE(check.getEnd(), 1230768061.0 + i);
                }
                QCOMPARE(check.getStation(), Data::SequenceName::Component("brw"));
                QCOMPARE(check.getArchive(), Data::SequenceName::Component("raw"));

                if (check.getVariable() == "T_S11") {
                    QCOMPARE(check.getValue().toDouble(), 13.66);
                } else if (check.getVariable() == "P_S11") {
                    QCOMPARE(check.getValue().toDouble(), 911.93);
                } else if (check.getVariable() == "BsG_S11") {
                    QCOMPARE(check.getValue().toDouble(), 12.0);
                } else {
                    QFAIL("Unknown variable");
                }
            }
        }
    }

    void action()
    {
        QProcess p;
        p.start("da.tasks", QStringList());
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        p.start("da.tasks", QStringList() << "brw");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        p.start("da.tasks", QStringList() << "brw" << "brw");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        p.start("da.tasks", QStringList() << "brw,brw");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
    }

};

QTEST_APPLESS_MAIN(TestCLIArguments)

#include "arguments.moc"
