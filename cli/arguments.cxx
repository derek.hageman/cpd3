/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#ifdef Q_OS_UNIX

#include <unistd.h>

#endif

#include <math.h>
#include <QTextStream>
#include <QLocalSocket>
#include <QFile>
#include <QDir>
#include <QSettings>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "core/timeparse.hxx"
#include "core/range.hxx"
#include "datacore/streampipeline.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/externalsink.hxx"
#include "datacore/stream.hxx"
#include "clicore/terminaloutput.hxx"
#include "clicore/archiveparse.hxx"
#include "clicore/optionparse.hxx"
#include "clicore/argumentparser.hxx"
#include "cli.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::CLI;

CPD3::Data::Archive::Selection::List CPD3CLI::parseArchiveRead(const QStringList &arguments,
                                                               Time::Bounds *clip)
{
    try {
        return ArchiveParse::parse(arguments, clip, &archive);
    } catch (AcrhiveParsingException ape) {
        TerminalOutput::simpleWordWrap(ape.getDescription(), true);
        QCoreApplication::exit(1);
    }
    return CPD3::Data::Archive::Selection::List();
}

std::vector<SequenceName::Component> CPD3CLI::parseStationSpecification(QStringList &arguments,
                                                                        int allowStations,
                                                                        int requireStations)
{
    if (allowStations <= 0 && requireStations <= 0)
        return {};

    Archive::Access::ReadLock lock(archive);

    SequenceName::ComponentSet stations;
    bool useAllStations = false;
    while (!arguments.isEmpty()) {
        auto split = arguments.front().split(QRegExp("[:;,]+"), QString::SkipEmptyParts);
        bool anyMissed = split.isEmpty();
        SequenceName::ComponentSet maybeStations;
        for (const auto &part : split) {
            if (part.toLower() == tr("allstations", "all stations string")) {
                useAllStations = true;
                continue;
            }
            auto add = archive.availableStations({part.toStdString()});
            if (add.empty()) {
                anyMissed = true;
                break;
            }
            maybeStations.insert(add.begin(), add.end());
        }
        if (anyMissed)
            break;
        stations.insert(maybeStations.begin(), maybeStations.end());
        arguments.removeFirst();
    }
    if (useAllStations) {
        stations = archive.availableStations();
    }

    if (stations.empty() && !useAllStations && requireStations == 1 && allowStations <= 1) {
        stations = SequenceName::impliedStations(&archive);
    }

    Archive::Selection::Match result;
    std::copy(stations.begin(), stations.end(), Util::back_emplacer(result));
    return result;
}

class Parser : public ArgumentParser {
    QString componentName;
    QString componentDisplay;
    QString componentDescription;
    CPD3CLI::HelpType helpType;
    int allowStations;
    int requireStations;
    bool allowUndefinedTimes;
    Time::LogicalTimeUnit defaultTimeUnits;
public:
    Parser(const QString &componentNameIn,
           const QString &componentDisplayIn,
           const QString &componentDescriptionIn,
           CPD3CLI::HelpType helpTypeIn,
           int allowStationsIn,
           int requireStationsIn,
           bool allowUndefinedTimesIn,
           Time::LogicalTimeUnit defaultTimeUnitsIn) : componentName(componentNameIn),
                                                       componentDisplay(componentDisplayIn),
                                                       componentDescription(componentDescriptionIn),
                                                       helpType(helpTypeIn),
                                                       allowStations(allowStationsIn),
                                                       requireStations(requireStationsIn),
                                                       allowUndefinedTimes(allowUndefinedTimesIn),
                                                       defaultTimeUnits(defaultTimeUnitsIn)
    { }

    virtual ~Parser()
    { }

protected:
    virtual QList<BareWordHelp> getBareWordHelp()
    {
        QList<BareWordHelp> result;
        switch (helpType) {
        case CPD3CLI::ARCHIVE_FILTER:
        case CPD3CLI::ARCHIVE_FILTER_OUTPUT:
        case CPD3CLI::ARCHIVE_FILTER_SILENT:
        case CPD3CLI::ARCHIVE_READ:
            result.append(BareWordHelp(tr("station", "station argument name"),
                                       tr("The (default) station to read data from"),
                                       tr("Inferred from the current directory"),
                                       tr("The \"station\" bare word argument is used to specify the station(s) used to "
                                          "look up variables that do not include an explicit station as part of an "
                                          "archive read specification.  This is the three digit station identification "
                                          "code.  For example \"brw\".  If a variable does not specify a station, then "
                                          "this is the station it will be looked up from.  Multiple stations may be "
                                          "specified by separating them with \":\", \";\" or \",\".  Regular expressions "
                                          "are accepted.")));
            result.append(BareWordHelp(tr("variables", "variables argument name"),
                                       tr("The list of variable specifies to read from the archive"),
                                       QString(),
                                       tr("The \"variables\" bare word argument sets the list of variables to read "
                                          "from the archive.  This can be either a direct list of variables (such as "
                                          "\"T_S11\" or a alias for multiple variables, such as \"S11a\".  In the single "
                                          "variable form regular expressions may be used, as long as they match the "
                                          "entire variable name.  This list is delimited by commas.  If a specification "
                                          "includes a colon then the part before the colon is treated as an override to "
                                          "the default archive.  If it includes two colons then the first field is "
                                          "treated as the station, the second as the archive, and the third as the "
                                          "actual variable specification.  If it includes four or more colons then "
                                          "the specification is treated as with three except that the trailing "
                                          "components specify the flavor (e.x. PM1 or PM10) restriction.  This "
                                          "restriction consists of an optional prefix and the flavor name.  If there is "
                                          "no prefix then the flavor is required.  If the prefix is \"!\" then the flavor "
                                          "is excluded, that is the results will never include it.  If the prefix is"
                                          "\"=\" then the flavor is added to the list of flavors that any match must "
                                          "have exactly."
                                          "\nFor example:"
                                          "\"T_S11\" specifies the variable in the \"default\" station (set "
                                          "either explicitly on the command line or inferred from the current directory) "
                                          "and the \"default\" archive (either the \"raw\" archive or the one set on "
                                          "the command line.\n"
                                          "\"raw:T_S1[12]\" specifies the variables T_S11 and T_S12 from the raw archive\n"
                                          "on the \"default\" station.\n"
                                          "\"brw:raw:S11a\" specifies the \"S11a\" alias record for the station \"brw\"\n"
                                          "and the \"raw\" archive.\n"
                                          "\":avgh:T_S11:pm1:!stats\" specifies T_S11 from the \"default\" station "
                                          "hourly averages restricted to only PM1 data, but not any calculated statistics.\n\n"
                                          "The string \"everything\" can also be used to retrieve all available "
                                          "variables.")));
            result.append(BareWordHelp(tr("times", "times argument name"),
                                       tr("The time range of data to read"), QString(),
                                       TimeParse::describeListBoundsUsage(false,
                                                                          allowUndefinedTimes,
                                                                          defaultTimeUnits)));
            result.append(BareWordHelp(tr("archive", "archive argument name"),
                                       tr("The (default) archive to read data from"),
                                       tr("The \"raw\" data archive"),
                                       tr("The \"archive\" bare word argument is used to specify the archive used to "
                                          "Look up variables that do not include an archive station as part of an "
                                          "archive read specification.  This is the internal archive name, such as "
                                          "\"raw\" or \"clean\".  Multiple archives may be specified by separating them "
                                          "with \":\", \";\" or \",\".  Regular expressions are accepted.")));


            if (helpType == CPD3CLI::ARCHIVE_FILTER ||
                    helpType == CPD3CLI::ARCHIVE_FILTER_OUTPUT ||
                    helpType == CPD3CLI::ARCHIVE_FILTER_SILENT) {
                result.append(BareWordHelp(tr("file", "file argument name"),
                                           tr("The file to read data from or - for standard input"),
                                           QString(),
                                           tr("The \"file\" bare word argument is used to specify the the file to read data "
                                              "from.  If it is present and exists then data is read from the given file name "
                                              " instead of from standard input.  Alternatively \"-\" may be used to "
                                              "explicitly specify standard input.")));
            }
            break;

        case CPD3CLI::OTHER_READ:
            result.append(BareWordHelp(tr("file", "file argument name"),
                                       tr("The file to read data from or - for standard input"),
                                       QString(),
                                       tr("The \"file\" bare word argument is used to specify the the file to read data "
                                          "from.  If it is present and exists then data is read from the given file name "
                                          " instead of from standard input.  Alternatively \"-\" may be used to "
                                          "explicitly specify standard input.")));
            break;

        case CPD3CLI::OTHER_EXTERNAL:
            break;

        case CPD3CLI::ACTION_BASIC:
        case CPD3CLI::ACTION_TIME:
        case CPD3CLI::ACTION_TIME_OPTIONAL:
        case CPD3CLI::EXTERNAL_TIME:
        case CPD3CLI::EXTERNAL_TIME_OPTIONAL:
            if (allowStations > 0 || requireStations > 0) {
                result.append(BareWordHelp(tr("station", "station argument name"),
                                           tr("The station to use"),
                                           ((requireStations == 1 && allowStations <= 1) ? tr(
                                                   "Inferred from the current directory")
                                                                                         : QString()),
                                           ((allowStations > 1) ? tr(
                                                   "The \"station\" bare word argument is used to specify the stations the "
                                                   "component operates on.  This is the three digit station identification code.  "
                                                   "For example \"brw\".  Multiple stations may be specified by separating them "
                                                   "with \":\", \";\" or \",\" or by specifying multiple arguments.  Regular "
                                                   "expressions are accepted.") : tr(
                                                   "The \"station\" bare word argument is used to specify the station the "
                                                   "component operates on.  This is the three digit station identification code.  "
                                                   "For example \"brw\".  Regular expressions are accepted, but must resolve to "
                                                   "only a single station."))));
            }
            if (helpType == CPD3CLI::ACTION_TIME ||
                    helpType == CPD3CLI::ACTION_TIME_OPTIONAL ||
                    helpType == CPD3CLI::EXTERNAL_TIME ||
                    helpType == CPD3CLI::EXTERNAL_TIME_OPTIONAL) {
                /* Don't use the zero length description here since
                 * that's "default" behavior rather than infinite
                 * time ranges */
                result.append(BareWordHelp(tr("times", "times argument name"),
                                           tr("The time range to operate on"), QString(),
                                           TimeParse::describeListBoundsUsage(false,
                                                                              allowUndefinedTimes,
                                                                              defaultTimeUnits)));
            }
            break;
        }
        return result;
    }

    virtual QString getBareWordCommandLine()
    {
        switch (helpType) {
        case CPD3CLI::ARCHIVE_READ:
            return tr("[station] variables times [archive]", "archive read bare words");
        case CPD3CLI::ARCHIVE_FILTER:
        case CPD3CLI::ARCHIVE_FILTER_OUTPUT:
        case CPD3CLI::ARCHIVE_FILTER_SILENT:
            return tr("[[station] variables times [archive]]|[file]", "archive filter bare words");
        case CPD3CLI::OTHER_READ:
            return tr("[file]", "other read bare words");
        case CPD3CLI::OTHER_EXTERNAL:
            return QString();
        case CPD3CLI::ACTION_BASIC:
        case CPD3CLI::ACTION_TIME:
        case CPD3CLI::ACTION_TIME_OPTIONAL:
        case CPD3CLI::EXTERNAL_TIME:
        case CPD3CLI::EXTERNAL_TIME_OPTIONAL:
            QString stationsString;
            if (requireStations > 0) {
                if (allowStations > 1 || requireStations > 1) {
                    stationsString = tr("station...", "station required multiple");
                } else if (requireStations == 1) {
                    stationsString = tr("[station]", "station inference single");
                } else {
                    stationsString = tr("station", "station required single");
                }
            } else if (allowStations > 1) {
                stationsString = tr("[station...]", "station optional single");
            } else if (allowStations == 1) {
                stationsString = tr("[station]", "station optional single");
            }

            if (helpType == CPD3CLI::ACTION_TIME || helpType == CPD3CLI::EXTERNAL_TIME) {
                return tr("%1 times", "required time bare words").arg(stationsString);
            } else if (helpType == CPD3CLI::ACTION_TIME_OPTIONAL ||
                    helpType == CPD3CLI::EXTERNAL_TIME_OPTIONAL) {
                return tr("%1 [times]", "optional time bare words").arg(stationsString);
            } else {
                return stationsString;
            }

        }
        Q_ASSERT(false);
        return QString();
    }

    virtual QString getProgramName()
    {
        return tr("da.%1", "program name").arg(componentName);
    }

    virtual QString getDescription()
    {
        switch (helpType) {
        case CPD3CLI::ARCHIVE_READ:
            return tr("%1 - %2\n\n"
                      "The specification of what data to retrieve (variables, start, etc) is "
                      "required.").arg(componentDisplay, componentDescription);
        case CPD3CLI::ARCHIVE_FILTER:
        case CPD3CLI::ARCHIVE_FILTER_OUTPUT:
            return tr("%1 - %2\n\n"
                      "If used without any data specification (variables, start, etc) then "
                      "input will be read from standard input.  If an existing, readable file is "
                      "given than input will be read from that file.  Otherwise the data given on "
                      "the command line is operated on.  In either case output is produced on "
                      "standard output.").arg(componentDisplay, componentDescription);
        case CPD3CLI::ARCHIVE_FILTER_SILENT:
            return tr("%1 - %2\n\n"
                      "If used without any data specification (variables, start, etc) then "
                      "input will be read from standard input.  If an existing, readable file is "
                      "given than input will be read from that file.  Otherwise the data given on "
                      "the command line is operated on.").arg(componentDisplay,
                                                              componentDescription);
        case CPD3CLI::OTHER_READ:
            return tr("%1 - %2\n\n"
                      "If a single readable file is given then the input is read from it.  Otherwise "
                      "input is read from standard input.").arg(componentDisplay,
                                                                componentDescription);
        default:
            return tr("%1 - %2").arg(componentDisplay, componentDescription);
        }
        Q_ASSERT(false);
        return QString();
    }

    virtual QString getPostProcessingDescription(int nBareWords, int nArguments)
    {
        switch (helpType) {
        case CPD3CLI::ARCHIVE_READ:
        case CPD3CLI::ARCHIVE_FILTER:
        case CPD3CLI::ARCHIVE_FILTER_OUTPUT:
        case CPD3CLI::ARCHIVE_FILTER_SILENT:
        case CPD3CLI::OTHER_EXTERNAL:
            return ArgumentParser::getPostProcessingDescription(nBareWords, nArguments);
        case CPD3CLI::OTHER_READ:
            return tr("Switches can be given at any point in the command line, however a "
                      "simple \"--\" terminates switch processing and any remaining argument is "
                      "treated as the input file.");
        case CPD3CLI::ACTION_BASIC:
            if (requireStations > 0) {
                if (allowStations > requireStations) {
                    return tr(
                            "Switches can be given at any point in the command line, however a simple "
                            "\"--\" terminates switch processing and all further arguments are treated as "
                            "stations.  There must be at least %n station(s) specified, however more are "
                            "optionally allowed.", "", requireStations);
                } else if (requireStations == 1) {
                    return tr(
                            "Switches can be given at any point in the command line, however a simple "
                            "\"--\" terminates switch processing and all further arguments are treated as "
                            "stations.   Exactly one station is required but an attempt to infer it from "
                            "the working directory will be made if no valid ones are present on the "
                            "command line.");
                }
            } else if (allowStations == 1) {
                return tr(
                        "Switches can be given at any point in the command line, however a simple "
                        "\"--\" terminates switch processing and all further arguments are treated as "
                        "stations.   No more than a single station is allowed, if any others are "
                        "specified an error will be produced.");
            } else if (allowStations > 0) {
                return tr(
                        "Switches can be given at any point in the command line, however a simple "
                        "\"--\" terminates switch processing and all further arguments are treated as "
                        "stations.");
            } else {
                return ArgumentParser::getPostProcessingDescription(nBareWords, nArguments);
            }
        case CPD3CLI::ACTION_TIME:
        case CPD3CLI::EXTERNAL_TIME:
            if (requireStations > 0) {
                if (allowStations > requireStations) {
                    return tr(
                            "A time range is required for operation.  Switches can be given at any point "
                            "in the command line, however a simple \"--\" terminates switch processing and "
                            "all further arguments are treated as bare words. Leading bare words that "
                            "match valid stations are treated as such.  There must be at least %n "
                            "station(s) specified, however more are optionally allowed.  All remaining "
                            "bare words are treated as part of the time specification.", "",
                            requireStations);
                } else if (requireStations == 1) {
                    return tr(
                            "A time range is required for operation.  Switches can be given at any point "
                            "in the command line, however a simple \"--\" terminates switch processing and "
                            "all further arguments are treated as bare words. A Leading bare word that "
                            "matches a valid stations is treated as such.  Exactly one station is "
                            "required but an attempt to infer it from the working directory will be made "
                            "if no valid ones are present on the command line.  All remaining "
                            "bare words are treated as part of the time specification.");
                }
            } else if (allowStations == 1) {
                return tr(
                        "A time range is required for operation.  Switches can be given at any point "
                        "in the command line, however a simple \"--\" terminates switch processing and "
                        "all further arguments are treated as bare words. A leading bare word that "
                        "matches valid stations is treated as such.  All remaining bare words are "
                        "treated as part of the time specification.");
            } else if (allowStations > 0) {
                return tr(
                        "A time range is required for operation.  Switches can be given at any point "
                        "in the command line, however a simple \"--\" terminates switch processing and "
                        "all further arguments are treated as bare words. An leading bare word that "
                        "matches a valid station is treated as such.  All remaining bare words are "
                        "treated as part of the time specification.");
            }
        case CPD3CLI::ACTION_TIME_OPTIONAL:
        case CPD3CLI::EXTERNAL_TIME_OPTIONAL:
            if (requireStations > 0) {
                if (allowStations > requireStations) {
                    return tr(
                            "Switches can be given at any point in the command line, however a simple "
                            "\"--\" terminates switch processing and all further arguments are treated as "
                            "bare words. Leading bare words that match valid stations are treated as "
                            "such.  There must be at least %n station(s) specified, however more are "
                            "optionally allowed.  All remaining bare words are treated as part of the "
                            "time specification.  If there are no remaining bare words for the time "
                            "specification then the default behavior is used.", "",
                            requireStations);
                } else if (requireStations == 1) {
                    return tr(
                            "Switches can be given at any point in the command line, however a simple "
                            "\"--\" terminates switch processing and all further arguments are treated as "
                            "bare words. A Leading bare word that matches a valid stations is treated as "
                            "such.  Exactly one station is required but an attempt to infer it from the "
                            "working directory will be made if no valid ones are present on the command "
                            "line.  All remaining bare words are treated as part of the time "
                            "specification.  If there are no remaining bare words for the time "
                            "specification then the default behavior is used.");
                }
            } else if (allowStations == 1) {
                return tr(
                        "Switches can be given at any point in the command line, however a simple "
                        "\"--\" terminates switch processing and all further arguments are treated "
                        "as bare words. A leading bare word that matches valid stations is treated "
                        "as such.  All remaining bare words are treated as part of the time "
                        "specification.  If there are no remaining bare words for the time "
                        "specification then the default behavior is used.");
            } else if (allowStations > 0) {
                return tr(
                        "Switches can be given at any point in the command line, however a simple "
                        "\"--\" terminates switch processing and all further arguments are treated "
                        "as bare words. An leading bare word that matches a valid station is treated "
                        "as such.  All remaining bare words are treated as part of the time "
                        "specification.  If there are no remaining bare words for the time "
                        "specification then the default behavior is used.");
            }
        }
        Q_ASSERT(false);
        return ArgumentParser::getPostProcessingDescription(nBareWords, nArguments);
    }

    virtual QString getExamples(const QList<ComponentExample> &examples)
    {
        switch (helpType) {
        case CPD3CLI::ARCHIVE_READ:
            return tr("\nExamples:\n"
                      "General usage:\n\n"
                      "    da.get brw S11a 2010:1 2010:2\n\n"
                      "This retrieves the record \"S11a\" (which is an alias for several relevant "
                      "nephelometer parameters) for the station \"brw\" from 2010-01-01 to "
                      "2010-01-02 from the \"raw\" archive.\n\n"
                      "Specifying a list of variables:\n\n"
                      "    da.get brw BsB_S11,BsG_S11,BsR_S11 2010 5 2010 7 clean\n\n"
                      "This retrieves the given variables from the \"clean\" archive and also "
                      "demonstrates more relaxed time specification.\n\n"
                      "Advanced variable selection:\n\n"
                      "    da.get ::BsB_S11:pm1 spo:clean:BsB_S11 2010:1\n\n"
                      "This retrieves only variable \"BsB_S11\" from the raw archive of the implied "
                      "station (from the working directory or other information) but only the PM1 "
                      "cut size data.  It also retrieves the variable from the spo clean archive "
                      "with both cut sizes.  The data are retrieved from 2010-01-01 to 2010-01-02.");
        case CPD3CLI::ARCHIVE_FILTER:
        case CPD3CLI::ARCHIVE_FILTER_OUTPUT:
            return generateExamples(examples, OptionParse::Example_DataFilter);
        case CPD3CLI::ARCHIVE_FILTER_SILENT:
            return generateExamples(examples, OptionParse::Example_DataFilterSilent);
        case CPD3CLI::OTHER_EXTERNAL:
            return generateExamples(examples, OptionParse::Example_DataInputExternal);
        case CPD3CLI::OTHER_READ:
            return generateExamples(examples, OptionParse::Example_DataInputFile);
        case CPD3CLI::ACTION_BASIC:
            return generateExamples(examples, OptionParse::Example_Action);
        case CPD3CLI::ACTION_TIME:
            return generateExamples(examples, OptionParse::Example_ActionTimes);
        case CPD3CLI::ACTION_TIME_OPTIONAL:
            return generateExamples(examples, OptionParse::Example_ActionTimesOptional);
        case CPD3CLI::EXTERNAL_TIME:
            return generateExamples(examples, OptionParse::Example_ExternalTimes);
        case CPD3CLI::EXTERNAL_TIME_OPTIONAL:
            return generateExamples(examples, OptionParse::Example_ExternalTimesOptional);
        }
        Q_ASSERT(false);
        return ArgumentParser::getExamples(examples);
    }

    virtual QHash<QString, QString> getDocumentationTypeID()
    {
        QHash<QString, QString> result;
        switch (helpType) {
        case CPD3CLI::ARCHIVE_READ:
            result.insert("type", "archive");
            break;
        case CPD3CLI::ARCHIVE_FILTER:
            result.insert("type", "filter");
            break;
        case CPD3CLI::ARCHIVE_FILTER_OUTPUT:
            result.insert("type", "filter");
            result.insert("output", "external");
            break;
        case CPD3CLI::ARCHIVE_FILTER_SILENT:
            result.insert("type", "filter");
            result.insert("output", "none");
            break;
        case CPD3CLI::OTHER_READ:
            result.insert("type", "read");
            break;
        case CPD3CLI::OTHER_EXTERNAL:
            result.insert("type", "external");
            break;
        case CPD3CLI::ACTION_BASIC:
            result.insert("type", "action");
            break;
        case CPD3CLI::ACTION_TIME:
            result.insert("type", "action");
            result.insert("time", "required");
            break;
        case CPD3CLI::ACTION_TIME_OPTIONAL:
            result.insert("type", "action");
            result.insert("time", "optional");
            break;
        case CPD3CLI::EXTERNAL_TIME:
            result.insert("type", "input");
            result.insert("time", "required");
            break;
        case CPD3CLI::EXTERNAL_TIME_OPTIONAL:
            result.insert("type", "input");
            result.insert("time", "optional");
            break;

        }

        if (requireStations > 0)
            result.insert("requiredStations", QString::number(requireStations));
        if (allowStations > 0) {
            if (allowStations >= INT_MAX)
                result.insert("allowedStations", QString());
            else
                result.insert("allowedStations", QString::number(allowStations));
        }

        if (!componentName.isEmpty())
            result.insert("component", componentName);
        if (!componentDisplay.isEmpty())
            result.insert("display", componentDisplay);
        if (!componentDescription.isEmpty())
            result.insert("description", componentDescription);

        return result;
    }
};


int CPD3CLI::parseOptions(const QString &componentName,
                          const QString &componentDisplay,
                          const QString &componentDescription,
                          HelpType helpType,
                          QStringList &arguments,
                          ComponentOptions &options,
                          const QList<ComponentExample> &examples,
                          int allowStations,
                          int requireStations,
                          bool allowUndefinedTimes,
                          Time::LogicalTimeUnit defaultTimeUnits)
{
    Parser parser(componentName, componentDisplay, componentDescription, helpType, allowStations,
                  requireStations, allowUndefinedTimes, defaultTimeUnits);

    try {
        parser.parse(arguments, options, examples);
    } catch (ArgumentParsingException ape) {
        QString text(ape.getText());
        if (ape.isError()) {
            if (!text.isEmpty())
                TerminalOutput::simpleWordWrap(text, true);
            QCoreApplication::exit(1);
            return -1;
        } else {
            if (!text.isEmpty())
                TerminalOutput::simpleWordWrap(text);
            QCoreApplication::exit(0);
        }
        return 1;
    }
    return 0;
}

static QStringList externalComponents()
{
    QStringList result;
    static const char *displayTypes[] =
            {"scatter", "timeseries", "cycle", "pdf", "cdf", "allan", "density", "layout"};
    for (const char **s = &displayTypes[0],
            **endS = s + sizeof(displayTypes) / sizeof(displayTypes[0]); s != endS; ++s) {
        result.append(QString("show.%1").arg(*s));
        result.append(QString("plot.%1").arg(*s));
    }

    result.append("get");
    result.append("config.text");
    result.append("ioserver");
    result.append("output.cpd2");
    result.append("editor");
    result.append("multiplex");
    result.append("sync.read");
    result.append("sync.write");
    result.append("sync.peer");

    return result;
}

static void listAllComponents(bool toStderr = false)
{
    QStringList sorted = externalComponents();
    for (const auto &component : ComponentLoader::list("cli")) {
        if (!component.second.interface("action") &&
                !component.second.interface("actiontime") &&
                !component.second.interface("ExternalSink") &&
                !component.second.interface("externalconverter") &&
                !component.second.interface("externalsource") &&
                !component.second.interface("processingstage"))
            continue;

        QString add = component.first;
        add.replace('_', '.');
        sorted.push_back(add);
    }
    std::sort(sorted.begin(), sorted.end());

    int maximumWidth = 1;
    for (QList<QString>::const_iterator cmp = sorted.constBegin(), end = sorted.constEnd();
            cmp != end;
            ++cmp) {
        int len = cmp->length() + 4;
        if (len > maximumWidth)
            maximumWidth = len;
    }

    int terminalWidth = TerminalOutput::terminalWidth() - 5;
    if (terminalWidth <= 0)
        terminalWidth = 79;

    int nColumns = (int) floor(terminalWidth / (double) maximumWidth);
    if (nColumns <= 1) {
        for (QList<QString>::const_iterator cmp = sorted.constBegin(), end = sorted.constEnd();
                cmp != end;
                ++cmp) {
            TerminalOutput::output(QString("    da.%1\n").arg(*cmp), toStderr);
        }
    } else {
        if (nColumns > sorted.size())
            nColumns = sorted.size();

        int nRows = (int) ceil((double) sorted.size() / (double) nColumns);
        int columnWidth = (int) ceil(terminalWidth / (double) nColumns);
        if (columnWidth > maximumWidth)
            columnWidth = maximumWidth;

        for (int row = 0; row < nRows; row++) {
            TerminalOutput::output(QString("    da.%1").arg(sorted.at(row)), toStderr);
            int idxPrior = row;
            for (int col = 1; col < nColumns; col++) {
                int idx = row + col * nRows;
                if (idx >= sorted.size())
                    break;

                int nPad = columnWidth - 3 - sorted.at(idxPrior).length();
                if (nPad > 0) {
                    TerminalOutput::output(QString(' ').repeated(nPad), toStderr);
                }
                TerminalOutput::output(QString("da.%1").arg(sorted.at(idx)), toStderr);
                idxPrior = idx;
            }
            TerminalOutput::output(QString('\n'), toStderr);
        }
    }
}

static QString getInvokedName(const QString &argument)
{
    QFileInfo file(argument);
    QString result(file.fileName());
    if (result.endsWith(".exe", Qt::CaseInsensitive))
        result.chop(4);
    if (!result.isEmpty())
        return result;
    return argument;
}

static bool shouldPromptForInput()
{
#ifdef Q_OS_UNIX
    int fd = ::fileno(stdin);
    if (fd >= 0)
        return ::isatty(fd);
    return false;
#else
    return true;
#endif
}

int CPD3CLI::parseArguments(QStringList arguments)
{
    std::pair<ComponentLoader::Information, QObject *> component;

    for (auto &arg : arguments) {
        ExternalSink::normalizeUnicode(arg);
    }

    QString name(getInvokedName(arguments.first()));
    if (name.compare("da", Qt::CaseInsensitive) == 0) {
        if (arguments.length() > 1) {
            if (arguments.at(1) == tr("--list", "list switch")) {
                TerminalOutput::output(tr("Available components:\n"));
                listAllComponents();
                QCoreApplication::exit(0);
                return 1;
            } else if (arguments.at(1).startsWith(tr("--component=", "component switch"))) {
                arguments.removeFirst();
                name = arguments.takeFirst().mid(tr("--component=", "component switch").length());
            } else if (arguments.at(1) == tr("--help", "help switch") ||
                    arguments.at(1) == tr("help", "help switch")) {
                TerminalOutput::output(
                        tr("Usage: da.<component> [arguments]:\n   Or: da --component=component [arguments]\n   Or: da <component> [arguments]\nUse da.<component> --help for component specific help.\n"));
                TerminalOutput::output(tr("Available components:\n"));
                listAllComponents();
                QCoreApplication::exit(0);
                return 1;
            } else {
                arguments.removeFirst();
                name = arguments.takeFirst();
                component = ComponentLoader::information(name);
                if (!component.second) {
                    TerminalOutput::output(tr("Invalid usage: no component specified.\n"), true);
                    TerminalOutput::output(tr("Available components:\n"), true);
                    listAllComponents(true);
                    QCoreApplication::exit(1);
                    return -1;
                }
            }
        } else {
            TerminalOutput::output(tr("Invalid usage: no component specified.\n"), true);
            TerminalOutput::output(tr("Available components:\n"), true);
            listAllComponents(true);
            QCoreApplication::exit(1);
            return -1;
        }
    } else {
        arguments.removeFirst();
        QString first(name);
        name = first.section('.', 1);
        if (name == first)
            name = first.section('_', 1);
        if (name == first && name.length() > 3)
            name = name.mid(3);
    }
    QString displayName(name);
    name.replace('.', '_');
    name = name.toLower();

    if (name == "get") {
        ComponentOptions options;
        if (arguments.isEmpty()) {
            arguments << "--help";
        }
        if (int rc = parseOptions(displayName, tr("Archive read"),
                                  tr("This component reads data directly from the archive."),
                                  ARCHIVE_READ, arguments, options)) {
            return rc;
        }
        Q_ASSERT(!arguments.isEmpty());

        Time::Bounds clip;
        auto selections = parseArchiveRead(arguments, &clip);
        if (selections.empty())
            return -1;
        progress.setTimeBounds(clip);

        pipeline.reset(new StreamPipeline(enablePipelineAcceleration, enablePipelineSerialization));
        Q_ASSERT(pipeline);
        if (disableArchiveClipping)
            clip = Time::Bounds();
        if (!pipeline->setInputArchive(selections, clip, &archive)) {
            TerminalOutput::output(
                    tr("Error setting input archive: %1\n").arg(pipeline->getInputError()), true);
            QCoreApplication::exit(1);
            return -1;
        }
        if (!pipeline->setOutputPipeline()) {
            TerminalOutput::output(
                    tr("Error setting output from archive: %1\n").arg(pipeline->getOutputError()),
                    true);
            QCoreApplication::exit(1);
            return -1;
        }
        startPipeline();
        return 0;
    }

    if (!component.second) {
        if (name.length() == 0) {
            TerminalOutput::output(tr("Invalid usage: no component specified.\n"), true);
            TerminalOutput::output(tr("Available components:\n"), true);
            listAllComponents(true);
            QCoreApplication::exit(1);
            return -1;
        } else {
            component = ComponentLoader::information(name);
        }
    }

    if (!component.second) {
        TerminalOutput::output(tr("Can't find component %1\n").arg(name), true);
        TerminalOutput::output(tr("Available components:\n"), true);
        listAllComponents(true);
        QCoreApplication::exit(1);
        return -1;
    }

    ActionComponent *actionComponent;
    if ((actionComponent = qobject_cast<ActionComponent *>(component.second))) {
        ComponentOptions options = actionComponent->getOptions();
        int allowStations = actionComponent->actionAllowStations();
        int requireStations = actionComponent->actionRequireStations();

        options.add("quiet", new ComponentOptionBoolean(tr("quiet", "quiet option name"),
                                                        tr("Suppress output"),
                                                        tr("If set then no progress output is displayed and any "
                                                           "confirmation prompts are bypassed."),
                                                        tr("Disabled")));
        if (int rc =
                parseOptions(displayName, component.first.title(), component.first.description(),
                             ACTION_BASIC, arguments, options, actionComponent->getExamples(),
                             allowStations, requireStations)) {
            return rc;
        }
        auto stations = parseStationSpecification(arguments, allowStations, requireStations);

        if (arguments.length() != 0) {
            if (allowStations == 0 && requireStations == 0) {
                TerminalOutput::output(
                        tr("da.%1 does not accept additional arguments.\nUse \"da.%1 --help\" for more information.\n")
                                .arg(name), true);
            } else {
                TerminalOutput::output(
                        tr("da.%1 does not accept additional arguments.\nUse \"da.%1 --help\" for more information.\nThis may be the result of an invalid station.\n")
                                .arg(name), true);
            }
            QCoreApplication::exit(1);
            return -1;
        }
        if (static_cast<int>(stations.size()) < requireStations) {
            TerminalOutput::output(
                    tr("da.%1 requires at least %n station(s).\nUse \"da.%1 --help\" for more information.\n",
                       "", requireStations).arg(name), true);
            QCoreApplication::exit(1);
            return -1;
        }
        if (static_cast<int>(stations.size()) > allowStations) {
            TerminalOutput::output(
                    tr("da.%1 requires only accepts %n station(s).\nUse \"da.%1 --help\" for more information.\n",
                       "", allowStations).arg(name), true);
            QCoreApplication::exit(1);
            return -1;
        }

        bool supressProgress = false;
        if (options.isSet("quiet")) {
            supressProgress = qobject_cast<ComponentOptionBoolean *>(options.get("quiet"))->get();
        }
        if (!supressProgress) {
            QString prompt(actionComponent->promptActionContinue(options, stations));
            if (prompt.length() > 0) {
                TerminalOutput::simpleWordWrap(prompt);
                TerminalOutput::output(tr("\nContinue? [y/N] "));
                fflush(stdout);
                char in[64];
                if (::fgets(in, sizeof(in), stdin) == NULL || (in[0] != 'y' && in[0] != 'Y')) {
                    TerminalOutput::output(tr("\nOperation aborted by request.\n"));
                    QCoreApplication::exit(1);
                    return -1;
                }
            }
        }

        action.reset(actionComponent->createAction(options, stations));
        if (!supressProgress) {
            progress.attach(action->feedback);
            progress.start();
        }

        startAction();
        return 0;
    }

    ActionComponentTime *actionComponentTime;
    if ((actionComponentTime = qobject_cast<ActionComponentTime *>(component.second))) {
        ComponentOptions options = actionComponentTime->getOptions();
        int allowStations = actionComponentTime->actionAllowStations();
        int requireStations = actionComponentTime->actionRequireStations();
        bool requireTime = actionComponentTime->actionRequiresTime();
        bool allowUndefinedTimes = actionComponentTime->actionAcceptsUndefinedBounds();
        Time::LogicalTimeUnit defaultUnits = actionComponentTime->actionDefaultTimeUnit();

        options.add("quiet", new ComponentOptionBoolean(tr("quiet", "quiet option name"),
                                                        tr("Suppress output"),
                                                        tr("If set then no progress output is displayed and any "
                                                           "confirmation prompts are bypassed."),
                                                        tr("Disabled")));
        if (int rc =
                parseOptions(displayName, component.first.title(), component.first.description(),
                             requireTime ? ACTION_TIME : ACTION_TIME_OPTIONAL, arguments, options,
                             actionComponentTime->getExamples(), allowStations, requireStations,
                             allowUndefinedTimes, defaultUnits)) {
            return rc;
        }
        auto stations = parseStationSpecification(arguments, allowStations, requireStations);

        bool supressProgress = false;
        if (options.isSet("quiet")) {
            supressProgress = qobject_cast<ComponentOptionBoolean *>(options.get("quiet"))->get();
        }

        if (static_cast<int>(stations.size()) < requireStations) {
            TerminalOutput::output(
                    tr("da.%1 requires at least %n station(s).\nUse \"da.%1 --help\" for more information.\n",
                       "", requireStations).arg(name), true);
            QCoreApplication::exit(1);
            return -1;
        }
        if (static_cast<int>(stations.size()) > allowStations) {
            TerminalOutput::output(
                    tr("da.%1 requires only accepts %n station(s).\nUse \"da.%1 --help\" for more information.\n",
                       "", allowStations).arg(name), true);
            QCoreApplication::exit(1);
            return -1;
        }

        if (!requireTime && arguments.length() == 0) {
            if (!supressProgress) {
                QString prompt(actionComponentTime->promptTimeActionContinue(options));
                if (prompt.length() > 0 && shouldPromptForInput()) {
                    TerminalOutput::simpleWordWrap(prompt);
                    TerminalOutput::output(tr("\nContinue? [y/N] "));
                    ::fflush(stdout);
                    char in[64];
                    if (::fgets(in, sizeof(in), stdin) == NULL || (in[0] != 'y' && in[0] != 'Y')) {
                        TerminalOutput::output(tr("\nOperation aborted by request.\n"));
                        QCoreApplication::exit(1);
                        return -1;
                    }
                }
            }
            action.reset(actionComponentTime->createTimeAction(options, stations));
        } else if (arguments.length() == 0) {
            TerminalOutput::output(
                    tr("da.%1 requires a time specification.\nUse \"da.%1 --help\" for more information.\n")
                            .arg(name), true);
            QCoreApplication::exit(1);
            return -1;
        } else {
            Time::Bounds bounds;
            try {
                bounds = TimeParse::parseListBounds(arguments, false, allowUndefinedTimes,
                                                    defaultUnits);
            } catch (const TimeParsingException &tpe) {
                TerminalOutput::simpleWordWrap(
                        tr("Error parsing time: %1\n").arg(tpe.getDescription()), true);
                QCoreApplication::exit(1);
                return -1;
            }
            if (!allowUndefinedTimes && (!FP::defined(bounds.start) || !FP::defined(bounds.end))) {
                TerminalOutput::output(
                        tr("da.%1 does not accept infinite or undefined times.\nUse \"da.%1 --help\" for more information.\n")
                                .arg(name), true);
                QCoreApplication::exit(1);
                return -1;
            }

            if (!supressProgress) {
                QString prompt(actionComponentTime->promptTimeActionContinue(options, bounds.start,
                                                                             bounds.end));
                if (prompt.length() > 0 && shouldPromptForInput()) {
                    TerminalOutput::simpleWordWrap(prompt);
                    TerminalOutput::output(tr("\nContinue? [y/N] "));
                    ::fflush(stdout);
                    char in[64];
                    if (::fgets(in, sizeof(in), stdin) == NULL || (in[0] != 'y' && in[0] != 'Y')) {
                        TerminalOutput::output(tr("\nOperation aborted by request.\n"));
                        QCoreApplication::exit(1);
                        return -1;
                    }
                }
            }

            progress.setTimeBounds(bounds);
            action.reset(actionComponentTime->createTimeAction(options, bounds.start, bounds.end,
                                                               stations));
        }
        if (!supressProgress) {
            progress.attach(action->feedback);
            progress.start();
        }

        startAction();
        return 0;
    }

    ExternalSinkComponent *dataEgress;
    if ((dataEgress = qobject_cast<ExternalSinkComponent *>(component.second))) {
        bool outputDeviceDisabled = !dataEgress->requiresOutputDevice();
        ComponentOptions options = dataEgress->getOptions();
        if (outputDeviceDisabled) {
            options.add("quiet", new ComponentOptionBoolean(tr("quiet", "quiet option name"),
                                                            tr("Suppress output"),
                                                            tr("If set then no progress output is displayed."),
                                                            tr("Disabled")));
        }
        if (int rc =
                parseOptions(displayName, component.first.title(), component.first.description(),
                             outputDeviceDisabled ? ARCHIVE_FILTER_SILENT : ARCHIVE_FILTER_OUTPUT,
                             arguments, options, dataEgress->getExamples())) {
            return rc;
        }

        bool supressProgress = true;
        if (outputDeviceDisabled) {
            if (options.isSet("quiet")) {
                supressProgress =
                        qobject_cast<ComponentOptionBoolean *>(options.get("quiet"))->get();
            } else {
                supressProgress = false;
            }
        }

        pipeline.reset(new StreamPipeline(enablePipelineAcceleration, enablePipelineSerialization));
        Q_ASSERT(pipeline);
        if (!outputDeviceDisabled) {
            auto device = pipeline->pipelineOutputDevice();
            if (!device) {
                TerminalOutput::output(
                        tr("Error getting output device: %1\n").arg(pipeline->getOutputError()),
                        true);
                QCoreApplication::exit(1);
                return -1;
            }
            auto stream = device->stream();
            if (!stream) {
                TerminalOutput::output(tr("Error getting output stream\n"), true);
                QCoreApplication::exit(1);
                return -1;
            }
            std::unique_ptr<ExternalSink>
                    output(dataEgress->createDataSink(std::move(stream), options));
            if (!output) {
                TerminalOutput::output(tr("Error creating data output.\n"), true);
                QCoreApplication::exit(1);
                return -1;
            }
            if (!pipeline->setOutputGeneral(std::move(output))) {
                TerminalOutput::output(
                        tr("Error setting data output: %1\n").arg(pipeline->getOutputError()),
                        true);
                QCoreApplication::exit(1);
                return -1;
            }
        } else {
            std::unique_ptr<ExternalSink> output(dataEgress->createDataSink({}, options));
            if (!output) {
                TerminalOutput::output(tr("Error creating data output.\n"), true);
                QCoreApplication::exit(1);
                return -1;
            }
            if (!supressProgress) {
                progress.attach(pipeline->feedback);
            }

            if (!pipeline->setOutputGeneral(std::move(output))) {
                TerminalOutput::output(
                        tr("Error setting data output: %1\n").arg(pipeline->getOutputError()),
                        true);
                QCoreApplication::exit(1);
                return -1;
            }
        }

        if (arguments.isEmpty() ||
                (arguments.size() == 1 && arguments.at(0) == tr("-", "standard input file name"))) {
            if (!pipeline->setInputPipeline()) {
                TerminalOutput::output(tr("Error setting input from standard input: %1\n").arg(
                        pipeline->getInputError()), true);
                QCoreApplication::exit(1);
                return -1;
            }
        } else if (arguments.size() == 1 && QFile::exists(arguments.at(0))) {
            if (!pipeline->setInputFile(arguments.at(0))) {
                TerminalOutput::output(tr("Can't open file %1: %2\n").arg(arguments.at(0))
                                                                     .arg(pipeline->getInputError()),
                                       true);
                QCoreApplication::exit(1);
                return -1;
            }
        } else {
            Time::Bounds clip;
            auto selections = parseArchiveRead(arguments, &clip);
            if (selections.empty())
                return -1;
            progress.setTimeBounds(clip);
            if (disableArchiveClipping)
                clip = Time::Bounds();
            if (!pipeline->setInputArchive(selections, clip, &archive)) {
                TerminalOutput::output(
                        tr("Error setting input from archive: %1\n").arg(pipeline->getInputError()),
                        true);
                QCoreApplication::exit(1);
                return -1;
            }
        }

        if (!supressProgress && outputDeviceDisabled)
            progress.start();

        startPipeline();
        return 0;
    }


    ExternalConverterComponent *dataIngress;
    if ((dataIngress = qobject_cast<ExternalConverterComponent *>(component.second))) {
        bool needInputDevice = dataIngress->requiresInputDevice();
        ComponentOptions options = dataIngress->getOptions();
        if (int rc =
                parseOptions(displayName, component.first.title(), component.first.description(),
                             needInputDevice ? OTHER_READ : OTHER_EXTERNAL, arguments, options,
                             dataIngress->getExamples())) {
            return rc;
        }

        std::unique_ptr<ExternalConverter> input(dataIngress->createDataIngress(options));
        if (!input) {
            TerminalOutput::output(tr("Error creating input.\n"), true);
            QCoreApplication::exit(1);
            return -1;
        }
        pipeline.reset(new StreamPipeline(enablePipelineAcceleration, enablePipelineSerialization));
        Q_ASSERT(pipeline);
        if (!pipeline->setOutputPipeline()) {
            TerminalOutput::output(tr("Error setting output: %1\n").arg(pipeline->getOutputError()),
                                   true);
            QCoreApplication::exit(1);
            return -1;
        }

        if (needInputDevice) {
            if (arguments.isEmpty() ||
                    (arguments.size() == 1 &&
                            arguments.at(0) == tr("-", "standard input file name"))) {
                if (!pipeline->setInputGeneralPipeline(std::move(input))) {
                    TerminalOutput::output(
                            tr("Error setting standard input: %1\n").arg(pipeline->getInputError()),
                            true);
                    QCoreApplication::exit(1);
                    return -1;
                }
            } else if (arguments.size() == 1 && QFile::exists(arguments.at(0))) {
                if (!pipeline->setInputGeneral(std::move(input), arguments.at(0))) {
                    TerminalOutput::output(tr("Can't open file %1: %2\n").arg(arguments.at(0))
                                                                         .arg(pipeline->getInputError()),
                                           true);
                    QCoreApplication::exit(1);
                    return -1;
                }
            } else if (arguments.size() == 1) {
                TerminalOutput::output(tr("The file \"%1\" does not exist.\n").arg(arguments.at(0)),
                                       true);
                QCoreApplication::exit(1);
                return -1;
            } else {
                TerminalOutput::output(tr("You cannot specify more than one input file.\n"), true);
                QCoreApplication::exit(1);
                return -1;
            }
        } else {
            if (!arguments.isEmpty()) {
                TerminalOutput::output(
                        tr("da.%1 does not accept any bare word arguments.\n").arg(name), true);
                QCoreApplication::exit(1);
                return -1;
            }
            if (!pipeline->setInputGeneral(std::move(input))) {
                TerminalOutput::output(
                        tr("Error setting general input: %1\n").arg(pipeline->getInputError()),
                        true);
                QCoreApplication::exit(1);
                return -1;
            }
        }

        startPipeline();
        return 0;
    }

    ExternalSourceComponent *dataIngressExternal;
    if ((dataIngressExternal = qobject_cast<ExternalSourceComponent *>(component.second))) {
        ComponentOptions options = dataIngressExternal->getOptions();
        int allowStations = dataIngressExternal->ingressAllowStations();
        int requireStations = dataIngressExternal->ingressRequireStations();
        bool requireTime = dataIngressExternal->ingressRequiresTime();
        bool allowUndefinedTimes = dataIngressExternal->ingressAcceptsUndefinedBounds();
        Time::LogicalTimeUnit defaultUnits = dataIngressExternal->ingressDefaultTimeUnit();

        if (int rc =
                parseOptions(displayName, component.first.title(), component.first.description(),
                             requireTime ? EXTERNAL_TIME : EXTERNAL_TIME_OPTIONAL, arguments,
                             options, dataIngressExternal->getExamples(), allowStations,
                             requireStations, allowUndefinedTimes, defaultUnits)) {
            return rc;
        }
        auto stations = parseStationSpecification(arguments, allowStations, requireStations);

        if (static_cast<int>(stations.size()) < requireStations) {
            TerminalOutput::output(
                    tr("da.%1 requires at least %n station(s).\nUse \"da.%1 --help\" for more information.\n",
                       "", requireStations).arg(name), true);
            QCoreApplication::exit(1);
            return -1;
        }
        if (static_cast<int>(stations.size()) > allowStations) {
            TerminalOutput::output(
                    tr("da.%1 requires only accepts %n station(s).\nUse \"da.%1 --help\" for more information.\n",
                       "", allowStations).arg(name), true);
            QCoreApplication::exit(1);
            return -1;
        }

        std::unique_ptr<ExternalConverter> input;
        if (!requireTime && arguments.length() == 0) {
            input.reset(dataIngressExternal->createExternalIngress(options, stations));
        } else if (arguments.length() == 0) {
            TerminalOutput::output(
                    tr("da.%1 requires a time specification.\nUse \"da.%1 --help\" for more information.\n")
                            .arg(name), true);
            QCoreApplication::exit(1);
            return -1;
        } else {
            Time::Bounds bounds;
            try {
                bounds = TimeParse::parseListBounds(arguments, false, allowUndefinedTimes,
                                                    defaultUnits);
            } catch (const TimeParsingException &tpe) {
                TerminalOutput::simpleWordWrap(
                        tr("Error parsing time: %1\n").arg(tpe.getDescription()), true);
                QCoreApplication::exit(1);
                return -1;
            }
            if (!allowUndefinedTimes && (!FP::defined(bounds.start) || !FP::defined(bounds.end))) {
                TerminalOutput::output(
                        tr("da.%1 does not accept infinite or undefined times.\nUse \"da.%1 --help\" for more information.\n")
                                .arg(name), true);
                QCoreApplication::exit(1);
                return -1;
            }

            input.reset(
                    dataIngressExternal->createExternalIngress(options, bounds.start, bounds.end,
                                                               stations));
        }

        if (input == NULL) {
            TerminalOutput::output(tr("Error creating input.\n"), true);
            QCoreApplication::exit(1);
            return -1;
        }

        pipeline.reset(new StreamPipeline(enablePipelineAcceleration, enablePipelineSerialization));
        Q_ASSERT(pipeline);
        if (!pipeline->setInputGeneral(std::move(input))) {
            TerminalOutput::output(
                    tr("Error setting general input: %1\n").arg(pipeline->getInputError()), true);
            QCoreApplication::exit(1);
            return -1;
        }
        if (!pipeline->setOutputPipeline()) {
            TerminalOutput::output(tr("Error setting output: %1\n").arg(pipeline->getOutputError()),
                                   true);
            QCoreApplication::exit(1);
            return -1;
        }

        startPipeline();
        return 0;
    }


    ProcessingStageComponent *generalFilter;
    if ((generalFilter = qobject_cast<ProcessingStageComponent *>(component.second))) {
        ComponentOptions options = generalFilter->getOptions();
        if (int rc =
                parseOptions(displayName, component.first.title(), component.first.description(),
                             ARCHIVE_FILTER, arguments, options, generalFilter->getExamples())) {
            return rc;
        }

        std::unique_ptr<ProcessingStage> filter(generalFilter->createGeneralFilterDynamic(options));
        if (!filter) {
            TerminalOutput::output(tr("Error creating filter.\n"), true);
            QCoreApplication::exit(1);
            return -1;
        }
        pipeline.reset(new StreamPipeline(enablePipelineAcceleration, enablePipelineSerialization));
        Q_ASSERT(pipeline);
        if (!pipeline->addProcessingStage(std::move(filter), generalFilter)) {
            TerminalOutput::output(tr("Error adding filter: %1.\n").arg(pipeline->getChainError()),
                                   true);
            QCoreApplication::exit(1);
            return -1;
        }
        if (!pipeline->setOutputPipeline()) {
            TerminalOutput::output(
                    tr("Error setting output pipeline: %1.\n").arg(pipeline->getOutputError()),
                    true);
            QCoreApplication::exit(1);
            return -1;
        }

        if (arguments.isEmpty() ||
                (arguments.size() == 1 && arguments.at(0) == tr("-", "standard input file name"))) {
            if (!pipeline->setInputPipeline()) {
                TerminalOutput::output(
                        tr("Error setting input pipeline: %1.\n").arg(pipeline->getInputError()),
                        true);
                QCoreApplication::exit(1);
                return -1;
            }
        } else if (arguments.size() == 1 && QFile::exists(arguments.at(0))) {
            if (!pipeline->setInputFile(arguments.at(0))) {
                TerminalOutput::output(tr("Can't open file %1: %2\n").arg(arguments.at(0))
                                                                     .arg(pipeline->getInputError()),
                                       true);
                QCoreApplication::exit(1);
                return -1;
            }
        } else {
            Time::Bounds clip;
            auto selections = parseArchiveRead(arguments, &clip);
            if (selections.empty())
                return -1;
            progress.setTimeBounds(clip);
            if (disableArchiveClipping)
                clip = Time::Bounds();
            if (!pipeline->setInputArchive(selections, clip, &archive)) {
                TerminalOutput::output(
                        tr("Error setting input archive: %1.\n").arg(pipeline->getInputError()),
                        true);
                QCoreApplication::exit(1);
                return -1;
            }
        }

        startPipeline();
        return 0;
    }

    TerminalOutput::output(tr("Cannot use %1 from the command line.\n").arg(name), true);
    QCoreApplication::exit(1);
    return -1;
}
