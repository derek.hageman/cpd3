/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QGridLayout>
#include <QLineEdit>
#include <QLabel>
#include <QDialogButtonBox>
#include <QPushButton>
#include <QShowEvent>

#include "guidata/realtimemessagelog.hxx"
#include "guicore/guicore.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace GUI {
namespace Data {

/** @file guidata/realtimemessagelog.hxx
 * A dialog for entering message log entries on the realtime system.
 */

RealtimeMessageLog::RealtimeMessageLog(QWidget *parent) : QDialog(parent),
                                                          settings(CPD3GUI_ORGANIZATION,
                                                                   CPD3GUI_APPLICATION)
{

    QGridLayout *layout = new QGridLayout;
    setLayout(layout);

    editor = new QTextEdit(this);
    editor->setAcceptRichText(false);
    editor->setReadOnly(false);
    editor->setPlainText(QString());
    editor->setToolTip(tr("Enter the text of the message log entry."));
    editor->setStatusTip(tr("Message log text"));
    editor->setWhatsThis(
            tr("This is the text associated with the message log entry.  This is the default for what is displayed about the message log event."));
    layout->addWidget(editor, 0, 0, 1, 2);

    QLabel *label = new QLabel(tr("Author (Initials):"), this);
    layout->addWidget(label, 1, 0, 1, 1, Qt::AlignRight);

    author = new QLineEdit(this);
    author->setPlaceholderText(tr("Enter your initials"));
    author->setToolTip(
            tr("Enter your initials for identifying the author of the message log entry."));
    author->setStatusTip(tr("Message log author"));
    author->setWhatsThis(
            tr("The text of this field is attached to the message log entry as the author of it.  Generally this should be the initials of the person who entered it."));
    author->setText(QString());
    layout->addWidget(author, 1, 1, 1, 1, Qt::AlignLeft);


    buttons = new QDialogButtonBox(this);
    buttons->addButton(QDialogButtonBox::Ok);
    buttons->addButton(QDialogButtonBox::Cancel);
    buttons->button(QDialogButtonBox::Ok)->setEnabled(false);
    connect(buttons, SIGNAL(accepted()), this, SLOT(emitMessageLog()));
    connect(buttons, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttons, SIGNAL(rejected()), this, SLOT(reject()));
    layout->addWidget(buttons, 2, 0, 1, 2);

    layout->setRowStretch(0, 1);
    layout->setColumnStretch(1, 1);

    connect(author, SIGNAL(textEdited(
                                   const QString &)), this, SLOT(updateAcceptable()));
    connect(author, SIGNAL(textEdited(
                                   const QString &)), this, SLOT(updateSavedAuthor()));
    connect(editor, SIGNAL(textChanged()), this, SLOT(updateAcceptable()));
}

RealtimeMessageLog::~RealtimeMessageLog()
{ }

void RealtimeMessageLog::emitMessageLog()
{
    QString authorText(author->text().trimmed());
    QString messageText(editor->toPlainText().trimmed());
    if (authorText.isEmpty() || messageText.isEmpty())
        return;

    Variant::Root event;
    event["Author"].setString(authorText);
    event["Text"].setString(messageText);
    emit messageLogEvent(std::move(event));
}

void RealtimeMessageLog::updateSavedAuthor()
{
    QString authorText(author->text().trimmed());
    if (authorText.isEmpty())
        return;

    settings.setValue("user/initials", QVariant(authorText));
}

void RealtimeMessageLog::updateAcceptable()
{
    QString authorText(author->text().trimmed());
    QString messageText(editor->toPlainText().trimmed());
    if (authorText.isEmpty() || messageText.isEmpty()) {
        buttons->button(QDialogButtonBox::Ok)->setEnabled(false);
    } else {
        buttons->button(QDialogButtonBox::Ok)->setEnabled(true);
    }
}

void RealtimeMessageLog::showEvent(QShowEvent *event)
{
    QDialog::showEvent(event);
    if (event->spontaneous())
        return;

    buttons->button(QDialogButtonBox::Ok)->setEnabled(false);
    editor->setText(QString());
    if (author->text().isEmpty())
        author->setText(settings.value("user/initials").toString());
}

}
}
}
