/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <memory>
#include <QGridLayout>
#include <QTabWidget>
#include <QLineEdit>
#include <QHeaderView>
#include <QPushButton>
#include <QInputDialog>
#include <QTimer>
#include <QAbstractItemModel>

#include "guidata/variableselect.hxx"
#include "core/threading.hxx"
#include "datacore/archive/access.hxx"

using namespace CPD3::Data;
using namespace CPD3::GUI::Data::Internal;

namespace CPD3 {
namespace GUI {
namespace Data {

/** @file guidata/variableselect.hxx
 * A component providing a variable selection list.
 */

namespace Internal {
struct AdvancedFlavorsData {
    enum {
        Defaults, HasLacks, Exact,
    } mode;
    QSet<QString> exactFlavors;
    QSet<QString> hasFlavors;
    QSet<QString> lacksFlavors;

    AdvancedFlavorsData() : mode(Defaults)
    { }

    bool operator==(const AdvancedFlavorsData &other) const
    {
        if (mode != other.mode)
            return false;
        switch (mode) {
        case Defaults:
            break;
        case HasLacks:
            if (hasFlavors != other.hasFlavors)
                return false;
            if (lacksFlavors != other.lacksFlavors)
                return false;
            break;
        case Exact:
            if (exactFlavors != other.exactFlavors)
                return false;
            break;
        }
        return true;
    }

    bool operator!=(const AdvancedFlavorsData &other) const
    { return !(*this == other); }

};
}

VariableSelect::VariableSelect(QWidget *parent) : QWidget(parent)
{
    QVBoxLayout *topLayout = new QVBoxLayout;
    setLayout(topLayout);
    topLayout->setContentsMargins(0, 0, 0, 0);

    tabWidget = new QTabWidget(this);
    topLayout->addWidget(tabWidget);

    QWidget *pageWidget = new QWidget;
    QVBoxLayout *pageLayout = new QVBoxLayout;
    pageWidget->setLayout(pageLayout);

    basicList = new QListWidget(pageWidget);
    pageLayout->addWidget(basicList);

    basicList->setToolTip(tr("The list of selected variables."));
    basicList->setStatusTip(tr("Variable selection"));
    basicList->setWhatsThis(
            tr("This is a list of variables available for basic selection.  Choose one or more by clicking on them to toggle selection."));
    basicList->setSelectionMode(QAbstractItemView::MultiSelection);
    basicList->setSortingEnabled(true);

    QPushButton *groupSelection = new QPushButton(tr("Select"), pageWidget);
    pageLayout->addWidget(groupSelection);
    groupSelection->setToolTip(tr("Select all variables matching a specific grouping."));
    groupSelection->setStatusTip(tr("Group selection"));
    groupSelection->setWhatsThis(
            tr("Using this button will display a list of classes of variables that can be selected.  Selecting a specific class will select all variables belonging to that class."));

    QMenu *groupMenu = new QMenu(groupSelection);
    connect(groupMenu, SIGNAL(triggered(QAction * )), this, SLOT(groupSelected(QAction * )));
    QAction *action;

    action = new QAction(tr("&Scattering"), groupMenu);
    action->setToolTip(tr("Select all scattering variables."));
    action->setStatusTip(tr("Select scattering"));
    action->setWhatsThis(
            tr("Selecting this option will add all scattering coefficient variables to the selected set."));
    action->setProperty("selection-expression", "Bb?s[BGRQ0-9]*_.*");
    groupMenu->addAction(action);

    action = new QAction(tr("&Absorption"), groupMenu);
    action->setToolTip(tr("Select all absorption variables."));
    action->setStatusTip(tr("Select absorption"));
    action->setWhatsThis(
            tr("Selecting this option will add all absorption coefficient variables to the selected set."));
    action->setProperty("selection-expression", "Ba[BGRQ0-9]*_.*");
    groupMenu->addAction(action);

    action = new QAction(tr("&Counts"), groupMenu);
    action->setToolTip(tr("Select all count variables."));
    action->setStatusTip(tr("Select counts"));
    action->setWhatsThis(
            tr("Selecting this option will add all count variables to the selected set."));
    action->setProperty("selection-expression", "N[pbn]?[0-9]*_.*");
    groupMenu->addAction(action);

    action = new QAction(tr("Sca&ttering, Absorption, and Counts"), groupMenu);
    action->setToolTip(tr("Select extensive aerosol variables."));
    action->setStatusTip(tr("Select extensive variables"));
    action->setWhatsThis(
            tr("Selecting this option will add all scattering coefficient, absorption coefficient, and count variables to the selected set."));
    action->setProperty("selection-expression",
                        "(?:(?:Bb?s[BGRQ0-9]*_.*)|(?:Ba[BGRQ0-9]*_.*)|(?:N[pbn]?[0-9]*_.*))");
    groupMenu->addAction(action);

    action = new QAction(tr("&Nephelometer P, T, and RH"), groupMenu);
    action->setToolTip(tr("Select nephelometer diagnostic variables."));
    action->setStatusTip(tr("Select nephelometer diagnostics"));
    action->setWhatsThis(
            tr("Selecting this option will add all nephelometer temperature, pressure and relative humidity variables to the selected set."));
    action->setProperty("selection-expression", "[TUP][0-9]*u?_S[0-9]+");
    groupMenu->addAction(action);

    action = new QAction(tr("C&oncentration (EBC, Aethalometer)"), groupMenu);
    action->setToolTip(tr("Select concentration variables."));
    action->setStatusTip(tr("Select concentration"));
    action->setWhatsThis(
            tr("Selecting this option will add all concentration variables to the selected set.  This includes aethalometer EBC concentrations."));
    action->setProperty("selection-expression", "X[BGRQ0-9]*c?_.*");
    groupMenu->addAction(action);

    action = new QAction(tr("&Wind speed and direction"), groupMenu);
    action->setToolTip(tr("Select wind speed and direction variables."));
    action->setStatusTip(tr("Select wind speed and direction"));
    action->setWhatsThis(
            tr("Selecting this option will add all wind speed and direction variables to the selected set."));
    action->setProperty("selection-expression", "W[SD][0-9]*_.*");
    groupMenu->addAction(action);

    action = new QAction(tr("System &flags"), groupMenu);
    action->setToolTip(tr("Select system flags."));
    action->setStatusTip(tr("Select system flags"));
    action->setWhatsThis(
            tr("Selecting this option will add all instrument system flags variables to the selection set."));
    action->setProperty("selection-expression", "F1?_.*");
    groupMenu->addAction(action);

    groupMenu->addSeparator();

    instrumentSelectionTypes << InstrumentSelection(tr("%1 - Scattering"), "Bb?s[BGRQ0-9]*_%1")
                             << InstrumentSelection(tr("%1 - Absorption"), "Bac?[BGRQ0-9]*_%1")
                             << InstrumentSelection(tr("%1 - Counts"), "N[pbn]?[0-9]*_%1")
                             << InstrumentSelection(tr("%1 - Concentration"), "Xc?[BGRQ0-9]*_%1")
                             << InstrumentSelection(tr("%1 - Optical"),
                                                    "(?:Bb?[sae]?|X)c?[BGRQ0-9]*_%1")
                             << InstrumentSelection(tr("%1 - Conditions"), "(?:T|P|U)1?_%1",
                                                    QStringList() << "P1?_%1" << "T1?_%1"
                                                                  << "(?:Bb?[sae]?|X)[BGRQ0-9]*c?_%1");
    instrumentSelectionMenu = groupMenu->addMenu(tr("&Instruments"));
    instrumentSelectionMenu->setToolTip(tr("Select by instrument and parameter."));
    instrumentSelectionMenu->setStatusTip(tr("Select instruments"));
    instrumentSelectionMenu->setWhatsThis(
            tr("This menu contains a list of parameters by instrument name.  Selecting one will select all associated parameters for that instrument."));
    instrumentSelectionMenu->menuAction()->setVisible(false);
    connect(instrumentSelectionMenu, SIGNAL(triggered(QAction * )), this,
            SLOT(groupSelected(QAction * )));

    action = new QAction(tr("C&lear"), groupMenu);
    action->setToolTip(tr("Unselect all variables."));
    action->setStatusTip(tr("Clear selection"));
    action->setWhatsThis(
            tr("This option clears all selected variables.  The resulting selection will be empty."));
    connect(action, SIGNAL(triggered(bool)), this, SLOT(clearBasicSelection()));
    groupMenu->addAction(action);

    groupSelection->setMenu(groupMenu);

    tabWidget->addTab(pageWidget, tr("&Normal"));


    pageWidget = new QWidget;
    pageLayout = new QVBoxLayout;
    pageWidget->setLayout(pageLayout);

    advancedModel = new QStandardItemModel(this);
    advancedTable = new QTableView(pageWidget);
    advancedTable->setModel(advancedModel);
    pageLayout->addWidget(advancedTable);

    advancedTable->setToolTip(tr("The table of all selections."));
    advancedTable->setStatusTip(tr("Advanced variable selection"));
    advancedTable->setWhatsThis(
            tr("This table represents all components of the selection.  Each row is matching entry.  Use the minus button to remove a row or the add button at the bottom to add new one."));

    advancedModel->setColumnCount(5);
    advancedTable->verticalHeader()->hide();

    advancedModel->setHorizontalHeaderItem(0, new QStandardItem(QString()));
    advancedTable->setItemDelegateForColumn(0, new VariableSelectAddRemoveDelegate(this,
                                                                                   advancedTable));
    advancedTable->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);

    advancedModel->setHorizontalHeaderItem(1, new QStandardItem(tr("Station")));
    advancedTable->setItemDelegateForColumn(1, new VariableSelectComboBoxDelegate(this,
                                                                                  &stationEditors,
                                                                                  advancedTable));

    advancedModel->setHorizontalHeaderItem(2, new QStandardItem(tr("Archive")));
    advancedTable->setItemDelegateForColumn(2, new VariableSelectComboBoxDelegate(this,
                                                                                  &archiveEditors,
                                                                                  advancedTable));

    advancedModel->setHorizontalHeaderItem(3, new QStandardItem(tr("Variable")));
    advancedTable->setItemDelegateForColumn(3, new VariableSelectComboBoxDelegate(this,
                                                                                  &variableEditors,
                                                                                  advancedTable));

    advancedTable->horizontalHeader()->setStretchLastSection(true);
    advancedModel->setHorizontalHeaderItem(4, new QStandardItem(tr("Flavors")));
    advancedTable->setItemDelegateForColumn(4,
                                            new VariableSelectFlavorsDelegate(this, &flavorsEditors,
                                                                              advancedTable));

    QList<QStandardItem *> items;
    QStandardItem *item;
    item = new QStandardItem;
    items << item;
    item->setData(1, Qt::EditRole);
    advancedModel->insertRow(0, items);
    advancedTable->setSpan(0, 0, 1, 5);

    openAllEditors();
    populateStations();
    populateArchives();
    populateVariables();
    populateFlavors();

    tabWidget->addTab(pageWidget, tr("&Advanced"));

    connectUpdateEvents();
}

VariableSelect::~VariableSelect()
{ }

void VariableSelect::disconnectUpdateEvents()
{
    tabWidget->disconnect(this);
    basicList->selectionModel()->disconnect(this);
    advancedModel->disconnect(this);
}

void VariableSelect::connectUpdateEvents()
{
    connect(tabWidget, SIGNAL(currentChanged(int)), this, SIGNAL(selectionChanged()));

    connect(basicList->selectionModel(), SIGNAL(selectionChanged(
                                                        const QItemSelection &, const QItemSelection &)),
            this, SIGNAL(selectionChanged()));

    connect(advancedModel, SIGNAL(dataChanged(
                                          const QModelIndex &, const QModelIndex &)), this,
            SIGNAL(selectionChanged()));
}

void VariableSelect::updateBasicList()
{
    QSet<QString> selected;
    for (int i = 0, max = basicList->count(); i < max; i++) {
        QListWidgetItem *item = basicList->item(i);
        if (!item->isSelected())
            continue;
        selected |= item->data(Qt::UserRole).toString();
    }
    basicList->clear();

    instrumentSelectionMenu->clear();
    QHash<QString, QSet<int> > createMatchers;

    for (QList<QString>::const_iterator var = availableVariables.constBegin(),
            endVar = availableVariables.constEnd(); var != endVar; ++var) {
        for (int i = 0, max = instrumentSelectionTypes.size(); i < max; i++) {
            QRegExp re(instrumentSelectionTypes.at(i).match.arg("(.+)"));
            if (!re.exactMatch(*var))
                continue;
            QString instrument(re.cap(1));
            if (instrument.isEmpty())
                continue;
            bool valid = true;
            for (QStringList::const_iterator
                    r = instrumentSelectionTypes.at(i).require.constBegin(),
                    endR = instrumentSelectionTypes.at(i).require.constEnd(); r != endR; ++r) {
                QRegExp requireCheck(r->arg(QRegExp::escape(instrument)));
                bool hit = false;
                for (QList<QString>::const_iterator checkVar = availableVariables.constBegin();
                        checkVar != endVar;
                        ++checkVar) {
                    if (!requireCheck.exactMatch(*checkVar))
                        continue;
                    hit = true;
                    break;
                }
                if (!hit) {
                    valid = false;
                    break;
                }
            }
            if (!valid)
                continue;
            createMatchers[instrument].insert(i);
        }

        QListWidgetItem *item = new QListWidgetItem(*var);
        item->setData(Qt::UserRole, *var);
        basicList->addItem(item);

        if (selected.contains(*var)) {
            selected.remove(*var);
            item->setSelected(true);
        }
    }

    for (QSet<QString>::const_iterator add = selected.constBegin(), endAdd = selected.constEnd();
            add != endAdd;
            ++add) {
        QListWidgetItem *item = new QListWidgetItem(*add);
        item->setData(Qt::UserRole, *add);
        basicList->addItem(item);
        item->setSelected(true);
    }

    if (createMatchers.isEmpty()) {
        instrumentSelectionMenu->menuAction()->setVisible(false);
    } else {
        QStringList instruments(createMatchers.keys());
        std::sort(instruments.begin(), instruments.end());
        for (QList<QString>::const_iterator instrument = instruments.constBegin(),
                endInst = instruments.constEnd(); instrument != endInst; ++instrument) {
            QList<int> matchIndices(createMatchers.value(*instrument).values());
            std::sort(matchIndices.begin(), matchIndices.end());

            for (QList<int>::const_iterator index = matchIndices.constBegin(),
                    endIdx = matchIndices.constEnd(); index != endIdx; ++index) {
                InstrumentSelection sel(instrumentSelectionTypes.at(*index));

                QAction *action = new QAction(sel.name.arg(*instrument), instrumentSelectionMenu);
                action->setProperty("selection-expression",
                                    sel.match.arg(QRegExp::escape(*instrument)));
                instrumentSelectionMenu->addAction(action);
            }
        }

        instrumentSelectionMenu->menuAction()->setVisible(true);
    }
}

void VariableSelect::scrollToBasicSelection()
{
    QListWidgetItem *firstSelectedItem = NULL;
    QListWidgetItem *lastSelectedItem = NULL;
    for (int i = 0; i < basicList->count(); i++) {
        QListWidgetItem *item = basicList->item(i);
        if (!item->isSelected())
            continue;
        if (firstSelectedItem == NULL)
            firstSelectedItem = item;
        lastSelectedItem = item;
    }

    if (lastSelectedItem != NULL)
        basicList->scrollToItem(lastSelectedItem);
    if (firstSelectedItem != NULL)
        basicList->scrollToItem(firstSelectedItem);
}

void VariableSelect::selectEffectiveTab()
{
    tabWidget->setCurrentIndex(1);
    basicList->selectionModel()->clear();

    for (int row = 0; row < advancedModel->rowCount() - 1; row++) {
        if (advancedModel->index(row, 1).data(Qt::EditRole).toInt() != -1)
            return;
        if (advancedModel->index(row, 2).data(Qt::EditRole).toInt() != -1)
            return;

        AdvancedFlavorsData flavors
                (advancedModel->index(row, 4).data(Qt::EditRole).value<AdvancedFlavorsData>());
        if (flavors.mode != AdvancedFlavorsData::Defaults)
            return;

        QVariant v(advancedModel->index(row, 3).data(Qt::EditRole));
        QString vName;
        if (v.type() != QVariant::String) {
            int vIdx = v.toInt();
            if (vIdx < 0 || vIdx >= availableVariables.size())
                return;
            vName = availableVariables.at(vIdx);
        } else {
            vName = v.toString();
            if (QRegExp::escape(vName) != vName)
                return;
        }
        if (vName.isEmpty())
            return;

        bool hit = false;
        for (int i = 0, max = basicList->count(); i < max; i++) {
            QListWidgetItem *item = basicList->item(i);

            QString checkName(item->data(Qt::UserRole).toString());
            if (checkName != vName)
                continue;
            item->setSelected(true);
            hit = true;
            break;
        }
        if (!hit) {
            QListWidgetItem *item = new QListWidgetItem(vName);
            item->setData(Qt::UserRole, vName);
            basicList->addItem(item);
            item->setSelected(true);
        }
    }

    /* Has to happen after the initial show, so we can't do it right now */
    QTimer::singleShot(0, this, SLOT(scrollToBasicSelection()));

    tabWidget->setCurrentIndex(0);
}

static QVariant indexOrString(const QString &str,
                              const QStringList &available,
                              Qt::CaseSensitivity cs = Qt::CaseSensitive)
{
    if (str.isEmpty())
        return -2;

    if (!available.contains(str, cs))
        return str;

    for (int i = 0, max = available.size(); i < max; i++) {
        if (str.compare(available.at(i), cs) == 0)
            return i;
    }

    return str;
}

static QVariant indexOrStringDefault(const QString &str,
                                     const QStringList &available,
                                     const CPD3::Data::SequenceName::Component &def,
                                     Qt::CaseSensitivity cs = Qt::CaseSensitive)
{
    if (str.isEmpty())
        return -2;
    if (str.compare(QString::fromStdString(def), cs) == 0)
        return -1;

    if (!available.contains(str, cs))
        return str;

    for (int i = 0, max = available.size(); i < max; i++) {
        if (str.compare(available.at(i), cs) == 0)
            return i;
    }

    return str;
}

void VariableSelect::addEntryFromString(const QString &str, const SequenceMatchDefaults &defaults)
{
    Q_UNUSED(defaults);

    QList<QStandardItem *> items;
    QStandardItem *item = new QStandardItem;
    items << item;
    item->setData(0, Qt::EditRole);

    QStringList components(str.split(':', QString::KeepEmptyParts));
    if (components.isEmpty() || (components.size() == 1 && components.at(0).isEmpty())) {
        item = new QStandardItem;
        items << item;
        item->setData(-1, Qt::EditRole);

        item = new QStandardItem;
        items << item;
        item->setData(-1, Qt::EditRole);

        item = new QStandardItem;
        items << item;
        item->setData(-2, Qt::EditRole);

        item = new QStandardItem;
        items << item;
        item->setData(QVariant::fromValue(AdvancedFlavorsData()), Qt::EditRole);
    } else if (components.size() == 1) {
        item = new QStandardItem;
        items << item;
        item->setData(-1, Qt::EditRole);

        item = new QStandardItem;
        items << item;
        item->setData(-1, Qt::EditRole);

        item = new QStandardItem;
        items << item;
        item->setData(indexOrString(components.at(0), availableVariables), Qt::EditRole);

        item = new QStandardItem;
        items << item;
        item->setData(QVariant::fromValue(AdvancedFlavorsData()), Qt::EditRole);
    } else if (components.size() == 2) {
        item = new QStandardItem;
        items << item;
        item->setData(-1, Qt::EditRole);

        item = new QStandardItem;
        items << item;
        item->setData(indexOrString(components.at(0), availableArchives, Qt::CaseInsensitive),
                      Qt::EditRole);

        item = new QStandardItem;
        items << item;
        item->setData(indexOrString(components.at(1), availableVariables), Qt::EditRole);

        item = new QStandardItem;
        items << item;
        item->setData(QVariant::fromValue(AdvancedFlavorsData()), Qt::EditRole);
    } else if (components.size() > 0) {
        item = new QStandardItem;
        items << item;
        item->setData(indexOrString(components.at(0), availableArchives, Qt::CaseInsensitive),
                      Qt::EditRole);

        item = new QStandardItem;
        items << item;
        item->setData(indexOrString(components.at(1), availableArchives, Qt::CaseInsensitive),
                      Qt::EditRole);

        item = new QStandardItem;
        items << item;
        item->setData(indexOrString(components.at(2), availableVariables), Qt::EditRole);

        AdvancedFlavorsData flavors;
        for (int i = 3, max = components.size(); i < max; i++) {
            if (flavors.mode == AdvancedFlavorsData::Defaults)
                flavors.mode = AdvancedFlavorsData::HasLacks;
            QString c(components.at(i));
            if ((c.startsWith('!') || c.startsWith('-')) && c.length() > 1) {
                flavors.lacksFlavors.insert(c.mid(1));
            } else if (c.startsWith('=')) {
                flavors.mode = AdvancedFlavorsData::Exact;
                if (c.length() > 1)
                    flavors.exactFlavors.insert(c.mid(1));
            } else {
                if (c.startsWith('+') && c.length() > 1) {
                    flavors.hasFlavors |= c.mid(1);
                } else {
                    flavors.hasFlavors |= c;
                }
            }
        }

        item = new QStandardItem;
        items << item;
        item->setData(QVariant::fromValue(flavors), Qt::EditRole);
    }

    advancedModel->insertRow(advancedModel->rowCount() - 1, items);
}

void VariableSelect::addSelectionFromValue(const Variant::Read &config,
                                           const SequenceMatchDefaults &defaults)
{
    Q_UNUSED(defaults);

    if (!config.exists())
        return;

    if (config.getType() == Variant::Type::String) {
        addEntryFromString(config.toQString(), defaults);
        return;
    }

    QList<QStandardItem *> items;
    QStandardItem *item = new QStandardItem;
    items << item;
    item->setData(0, Qt::EditRole);

    item = new QStandardItem;
    items << item;
    if (config.hash("Station").exists()) {
        QString str(config.hash("Station").toQString());
        if (str.isEmpty()) {
            item->setData(-2, Qt::EditRole);
        } else {
            item->setData(indexOrString(str, availableStations, Qt::CaseInsensitive), Qt::EditRole);
        }
    } else {
        item->setData(-1, Qt::EditRole);
    }

    item = new QStandardItem;
    items << item;
    if (config.hash("Archive").exists()) {
        QString str(config.hash("Archive").toQString());
        if (str.isEmpty()) {
            item->setData(-2, Qt::EditRole);
        } else {
            item->setData(indexOrString(str, availableArchives, Qt::CaseInsensitive), Qt::EditRole);
        }
    } else {
        item->setData(-1, Qt::EditRole);
    }

    item = new QStandardItem;
    items << item;
    if (config.hash("Variable").exists()) {
        QString str(config.hash("Variable").toQString());
        if (str.isEmpty()) {
            item->setData(-2, Qt::EditRole);
        } else {
            item->setData(indexOrString(str, availableVariables), Qt::EditRole);
        }
    } else {
        item->setData(-1, Qt::EditRole);
    }

    AdvancedFlavorsData flavors;
    auto sub = config.hash("Flavors");
    if (sub.exists()) {
        flavors.mode = AdvancedFlavorsData::Exact;
        for (const auto &add : sub.toChildren().keys()) {
            flavors.exactFlavors.insert(QString::fromStdString(add));
        }
    } else {
        sub = config.hash("HasFlavors");
        if (sub.exists()) {
            flavors.mode = AdvancedFlavorsData::HasLacks;
            for (const auto &add : sub.toChildren().keys()) {
                flavors.hasFlavors.insert(QString::fromStdString(add));
            }
        }

        sub = config.hash("LacksFlavors");
        if (sub.exists()) {
            flavors.mode = AdvancedFlavorsData::HasLacks;
            for (const auto &add : sub.toChildren().keys()) {
                flavors.lacksFlavors.insert(QString::fromStdString(add));
            }
        }
    }

    item = new QStandardItem;
    items << item;
    item->setData(QVariant::fromValue(flavors), Qt::EditRole);

    advancedModel->insertRow(advancedModel->rowCount() - 1, items);
}

static QString indexToString(const QVariant &value, const QStringList &available)
{
    if (value.type() != QVariant::String) {
        int i = value.toInt();
        if (i < 0 || i >= available.size())
            return QString();
        return available.at(i);
    }
    return value.toString();
}

static bool modelIndicesEqual(const QVariant &a,
                              const QVariant &b,
                              const QStringList &available,
                              Qt::CaseSensitivity cs = Qt::CaseSensitive)
{
    QString cmpA;
    if (a.type() != QVariant::String) {
        int i = a.toInt();
        if (i < 0 || i >= available.size()) {
            if (b.type() != QVariant::String)
                return i == b.toInt();
            return false;
        }
        cmpA = available.at(i);
    } else {
        cmpA = a.toString();
    }

    QString cmpB;
    if (b.type() != QVariant::String) {
        int i = a.toInt();
        if (i < 0 || i >= available.size())
            return false;
        cmpB = available.at(i);
    } else {
        cmpB = b.toString();
    }

    return cmpA.compare(cmpB, cs) == 0;
}

void VariableSelect::simplifySelectionDefaults(const SequenceMatchDefaults &defaults)
{
    if (defaults.archiveFanoutEnabled) {
        for (int i = 0; i < advancedModel->rowCount() - 1; i++) {
            QList<int> simplifyRows;
            QSet<QString> remainingArchives(QSet<QString>::fromList(defaults.archiveFanout));

            if (!remainingArchives.remove(
                    indexToString(advancedModel->index(i, 2).data(Qt::EditRole),
                                  availableArchives).toLower()))
                continue;

            for (int j = i + 1; j < advancedModel->rowCount() - 1; j++) {
                if (!modelIndicesEqual(advancedModel->index(i, 1).data(Qt::EditRole),
                                       advancedModel->index(j, 1).data(Qt::EditRole),
                                       availableStations, Qt::CaseInsensitive))
                    continue;
                if (!modelIndicesEqual(advancedModel->index(i, 3).data(Qt::EditRole),
                                       advancedModel->index(j, 3).data(Qt::EditRole),
                                       availableVariables))
                    continue;
                if (advancedModel->index(i, 4).data(Qt::EditRole).value<AdvancedFlavorsData>() !=
                        advancedModel->index(j, 4).data(Qt::EditRole).value<AdvancedFlavorsData>())
                    continue;

                if (!remainingArchives.remove(
                        indexToString(advancedModel->index(j, 2).data(Qt::EditRole),
                                      availableArchives).toLower()))
                    continue;
                simplifyRows.append(j);
            }
            if (!remainingArchives.isEmpty())
                continue;

            int offset = 0;
            for (QList<int>::const_iterator row = simplifyRows.constBegin(),
                    end = simplifyRows.constEnd(); row != end; ++row) {
                advancedModel->removeRows(*row - offset, 1);
                offset++;
            }

            advancedModel->setData(advancedModel->index(i, 2), -1, Qt::EditRole);
        }
    }

    if (defaults.forceHasLacks) {
        for (int i = 0; i < advancedModel->rowCount() - 1; i++) {
            AdvancedFlavorsData flavors
                    (advancedModel->index(i, 4).data(Qt::EditRole).value<AdvancedFlavorsData>());
            if (flavors.mode != AdvancedFlavorsData::HasLacks)
                continue;
            if (QSet<QString>::fromList(defaults.forceHasFlavors) != flavors.hasFlavors)
                continue;
            if (QSet<QString>::fromList(defaults.forceLacksFlavors) != flavors.lacksFlavors)
                continue;

            advancedModel->setData(advancedModel->index(i, 4),
                                   QVariant::fromValue(AdvancedFlavorsData()), Qt::EditRole);
        }
    }
}

void VariableSelect::configureFromSequenceMatch(const Variant::Read &config,
                                                const SequenceMatchDefaults &defaults)
{
    disconnectUpdateEvents();

    if (advancedModel->rowCount() > 1) {
        advancedModel->removeRows(0, advancedModel->rowCount() - 1);
    }

    switch (config.getType()) {
    case Variant::Type::String: {
        QStringList parts(config.toQString().split(QRegExp("[;,]+"), QString::SkipEmptyParts));
        for (QList<QString>::const_iterator p = parts.constBegin(), end = parts.constEnd();
                p != end;
                ++p) {
            addEntryFromString(*p, defaults);
        }
        break;
    }
    case Variant::Type::Array: {
        for (auto add : config.toArray()) {
            addSelectionFromValue(add, defaults);
        }
        break;
    }
    default:
        addSelectionFromValue(config, defaults);
        break;
    }

    simplifySelectionDefaults(defaults);
    selectEffectiveTab();

    openAllEditors();
    populateStations();
    populateArchives();
    populateVariables();
    populateFlavors();
    connectUpdateEvents();
}

static bool setIfNotDefault(Variant::Write &target,
                            const QVariant &value,
                            const QStringList &available)
{
    if (value.type() != QVariant::String) {
        int idx = value.toInt();
        if (idx == -1)
            return false;
        if (idx == -2)
            return true;
        if (idx >= 0 || idx < available.size()) {
            target.setString(available.at(idx));
            return true;
        }
    }

    QString str(value.toString());
    if (!str.isEmpty()) {
        target.setString(str);
    }
    return true;
}

static bool setIfNotDefault(Variant::Write &&target,
                            const QVariant &value,
                            const QStringList &available)
{ return setIfNotDefault(target, value, available); }

static bool setIfNotDefault(QString &target, const QVariant &value, const QStringList &available)
{
    if (value.type() != QVariant::String) {
        int idx = value.toInt();
        if (idx == -1)
            return false;
        if (idx == -2)
            return true;
        if (idx >= 0 || idx < available.size()) {
            target = available.at(idx);
            return true;
        }
    }

    QString str(value.toString());
    if (!str.isEmpty()) {
        target = std::move(str);
    }
    return true;
}

void VariableSelect::writeSequenceMatch(Variant::Write &target,
                                        const SequenceMatchDefaults &defaults)
{
    target.setEmpty();

    if (tabWidget->currentIndex() == 0) {
        QList<QListWidgetItem *> selected(basicList->selectedItems());
        for (QList<QListWidgetItem *>::const_iterator item = selected.constBegin(),
                endSel = selected.constEnd(); item != endSel; ++item) {
            Variant::Write add = Variant::Write::empty();
            add.hash("Variable").setString((*item)->data(Qt::UserRole).toString());

            if (defaults.forceHasLacks) {
                add.hash("HasFlavors").setType(Variant::Type::Array);
                for (const auto &flavor : defaults.forceHasFlavors) {
                    add.hash("HasFlavors").toArray().after_back().setString(flavor);
                }
                add.hash("LacksFlavors").setType(Variant::Type::Array);
                for (const auto &flavor : defaults.forceLacksFlavors) {
                    add.hash("LacksFlavors").toArray().after_back().setString(flavor);
                }
            }

            if (defaults.archiveFanoutEnabled) {
                for (const auto &archive : defaults.archiveFanout) {
                    add.hash("Archive").setString(archive);
                    target.toArray().after_back().set(add);
                }
            } else {
                target.toArray().after_back().set(add);
            }
        }
    } else {
        for (int row = 0; row < advancedModel->rowCount() - 1; row++) {
            Variant::Write add = Variant::Write::empty();
            setIfNotDefault(add.hash("Station"), advancedModel->index(row, 1).data(Qt::EditRole),
                            availableStations);
            setIfNotDefault(add.hash("Variable"), advancedModel->index(row, 3).data(Qt::EditRole),
                            availableVariables);

            AdvancedFlavorsData flavors
                    (advancedModel->index(row, 4).data(Qt::EditRole).value<AdvancedFlavorsData>());
            switch (flavors.mode) {
            case AdvancedFlavorsData::Defaults:
                if (defaults.forceHasLacks) {
                    add.hash("HasFlavors").setType(Variant::Type::Array);
                    for (const auto &flavor : defaults.forceHasFlavors) {
                        add.hash("HasFlavors").toArray().after_back().setString(flavor);
                    }
                    add.hash("LacksFlavors").setType(Variant::Type::Array);
                    for (const auto &flavor : defaults.forceLacksFlavors) {
                        add.hash("LacksFlavors").toArray().after_back().setString(flavor);
                    }
                }
                break;

            case AdvancedFlavorsData::HasLacks:
                add.hash("HasFlavors").setType(Variant::Type::Array);
                for (const auto &flavor : flavors.hasFlavors) {
                    add.hash("HasFlavors").toArray().after_back().setString(flavor);
                }
                add.hash("LacksFlavors").setType(Variant::Type::Array);
                for (const auto &flavor : flavors.lacksFlavors) {
                    add.hash("LacksFlavors").toArray().after_back().setString(flavor);
                }
                break;

            case AdvancedFlavorsData::Exact:
                add.hash("Flavors").setType(Variant::Type::Array);
                for (const auto &flavor : flavors.exactFlavors) {
                    add.hash("Flavors").toArray().after_back().setString(flavor);
                }
                break;
            }

            if (!setIfNotDefault(add.hash("Archive"),
                                 advancedModel->index(row, 2).data(Qt::EditRole),
                                 availableArchives)) {
                if (defaults.archiveFanoutEnabled) {
                    for (const auto &archive : defaults.archiveFanout) {
                        add.hash("Archive").setString(archive);
                        target.toArray().after_back().set(add);
                    }
                } else {
                    target.toArray().after_back().set(add);
                }
            } else {
                target.toArray().after_back().set(add);
            }
        }
    }
}

void VariableSelect::writeSequenceMatch(QString &target, const SequenceMatchDefaults &defaults)
{
    target.clear();

    if (tabWidget->currentIndex() == 0) {
        QList<QListWidgetItem *> selected(basicList->selectedItems());
        for (QList<QListWidgetItem *>::const_iterator item = selected.constBegin(),
                endSel = selected.constEnd(); item != endSel; ++item) {
            QStringList components{(*item)->data(Qt::UserRole).toString()};

            if (defaults.forceHasLacks) {
                components.prepend(QString());
                components.prepend(QString());
                for (const auto &flavor : defaults.forceHasFlavors) {
                    components.push_back(QString("+%1").arg(flavor));
                }
                for (const auto &flavor : defaults.forceLacksFlavors) {
                    components.push_back(QString("-%1").arg(flavor));
                }
            }

            if (defaults.archiveFanoutEnabled) {
                if (components.size() < 2)
                    components.prepend(QString());
                for (const auto &archive : defaults.archiveFanout) {
                    if (components.size() == 2)
                        components[0] = archive;
                    else
                        components[1] = archive;
                    if (!target.isEmpty())
                        target.append(',');
                    target.append(components.join(':'));
                }
            } else {
                if (!target.isEmpty())
                    target.append(',');
                target.append(components.join(':'));
            }
        }
    } else {
        for (int row = 0; row < advancedModel->rowCount() - 1; row++) {
            QString station;
            setIfNotDefault(station, advancedModel->index(row, 1).data(Qt::EditRole),
                            availableStations);
            QString variable;
            setIfNotDefault(station, advancedModel->index(row, 3).data(Qt::EditRole),
                            availableVariables);

            QStringList flavorsComponents;

            AdvancedFlavorsData flavors
                    (advancedModel->index(row, 4).data(Qt::EditRole).value<AdvancedFlavorsData>());
            switch (flavors.mode) {
            case AdvancedFlavorsData::Defaults:
                if (defaults.forceHasLacks) {
                    for (const auto &flavor : defaults.forceHasFlavors) {
                        flavorsComponents.append(QString("+%1").arg(flavor));
                    }
                    for (const auto &flavor : defaults.forceLacksFlavors) {
                        flavorsComponents.append(QString("-%1").arg(flavor));
                    }
                }
                break;

            case AdvancedFlavorsData::HasLacks:
                for (const auto &flavor : flavors.hasFlavors) {
                    flavorsComponents.append(QString("+%1").arg(flavor));
                }
                for (const auto &flavor : flavors.lacksFlavors) {
                    flavorsComponents.append(QString("-%1").arg(flavor));
                }
                break;

            case AdvancedFlavorsData::Exact:
                for (const auto &flavor : flavors.exactFlavors) {
                    flavorsComponents.append(QString("=%1").arg(flavor));
                }
                break;
            }

            QString archive;
            if (!setIfNotDefault(archive, advancedModel->index(row, 2).data(Qt::EditRole),
                                 availableArchives)) {
                QStringList components{station, QString(), archive};
                components.append(flavorsComponents);
                if (defaults.archiveFanoutEnabled) {
                    for (const auto &fanoutArchive : defaults.archiveFanout) {
                        components[1] = fanoutArchive;
                        if (!target.isEmpty())
                            target.append(',');
                        target.append(components.join(':'));
                    }
                } else {
                    if (!target.isEmpty())
                        target.append(',');
                    target.append(components.join(':'));
                }
            } else {
                QStringList components{station, QString(), archive};
                components.append(flavorsComponents);
                if (!target.isEmpty())
                    target.append(',');
                target.append(components.join(':'));
            }
        }
    }
}


VariableSelect::SequenceMatchDefaults::SequenceMatchDefaults() : archiveFanoutEnabled(false),
                                                                 archiveFanout(),
                                                                 forceHasLacks(false),
                                                                 forceHasFlavors(),
                                                                 forceLacksFlavors()
{ }

VariableSelect::SequenceMatchDefaults::SequenceMatchDefaults(const SequenceMatchDefaults &) = default;

VariableSelect::SequenceMatchDefaults &VariableSelect::SequenceMatchDefaults::operator=(const SequenceMatchDefaults &) = default;

void VariableSelect::SequenceMatchDefaults::setArchiveFanout(const QStringList &archives)
{
    archiveFanoutEnabled = true;
    archiveFanout.clear();
    for (QList<QString>::const_iterator add = archives.constBegin(), end = archives.constEnd();
            add != end;
            ++add) {
        archiveFanout.append(add->toLower());
    }
}

void VariableSelect::SequenceMatchDefaults::setFlavorsSelection(const QStringList &hasFlavors,
                                                                const QStringList &lacksFlavors)
{
    forceHasLacks = true;
    forceHasFlavors = hasFlavors;
    forceLacksFlavors = lacksFlavors;
}


void VariableSelect::configureFromDynamicSelection(const Variant::Read &config,
                                                   const DynamicSelectionDefaults &defaults)
{
    Q_UNUSED(defaults);
    configureFromSequenceMatch(config);
}

void VariableSelect::writeDynamicSelection(Variant::Write &target,
                                           const DynamicSelectionDefaults &defaults)
{
    Q_UNUSED(defaults);
    writeSequenceMatch(target);
}


VariableSelect::DynamicSelectionDefaults::DynamicSelectionDefaults() = default;

VariableSelect::DynamicSelectionDefaults::DynamicSelectionDefaults(const DynamicSelectionDefaults &) = default;

VariableSelect::DynamicSelectionDefaults &VariableSelect::DynamicSelectionDefaults::operator=(const DynamicSelectionDefaults &other) = default;

static void itemCopyFanoutMerge(const QList<QStandardItem *> &base,
                                QList<QList<QStandardItem *> > &output)
{
    Q_ASSERT(!output.isEmpty());

    QList<QStandardItem *> original;
    for (QList<QList<QStandardItem *> >::iterator row = output.begin(), endRow = output.end();
            row != endRow;
            ++row) {
        if (original.isEmpty()) {
            *row = base + *row;
            original = base;
        } else {
            QList<QStandardItem *> copy;
            for (QList<QStandardItem *>::const_iterator add = original.constBegin(),
                    endAdd = original.constEnd(); add != endAdd; ++add) {
                copy.append((*add)->clone());
            }
            copy.append(*row);
            *row = copy;
        }
    }
}

QList<QList<
        QStandardItem *> > VariableSelect::addEntryFromSelectionVariable(const Archive::Selection &selection,
                                                                         const ArchiveDefaults &defaults,
                                                                         const QString &variable)
{
    Q_UNUSED(defaults);
    QList<QStandardItem *> items;

    QStandardItem *item = new QStandardItem;
    items << item;
    item->setData(indexOrString(variable, availableVariables, Qt::CaseSensitive), Qt::EditRole);

    item = new QStandardItem;
    items << item;

    if (!selection.exactFlavors.empty()) {
        AdvancedFlavorsData flavors;
        flavors.mode = AdvancedFlavorsData::Exact;
        for (const auto &add : selection.exactFlavors) {
            flavors.exactFlavors.insert(QString::fromStdString(add));
        }
        item->setData(QVariant::fromValue(flavors), Qt::EditRole);
    } else if (!selection.hasFlavors.empty() || !selection.lacksFlavors.empty()) {
        AdvancedFlavorsData flavors;
        flavors.mode = AdvancedFlavorsData::HasLacks;
        for (const auto &add : selection.hasFlavors) {
            flavors.hasFlavors.insert(QString::fromStdString(add));
        }
        for (const auto &add : selection.lacksFlavors) {
            flavors.lacksFlavors.insert(QString::fromStdString(add));
        }
        item->setData(QVariant::fromValue(flavors), Qt::EditRole);
    } else {
        item->setData(QVariant::fromValue(AdvancedFlavorsData()), Qt::EditRole);
    }

    return QList<QList<QStandardItem *> >() << items;
}

QList<QList<
        QStandardItem *> > VariableSelect::addEntryFromSelectionArchive(const Archive::Selection &selection,
                                                                        const ArchiveDefaults &defaults,
                                                                        const QString &archive)
{
    QList<QStandardItem *> items;

    QStandardItem *item = new QStandardItem;
    items << item;
    item->setData(
            indexOrStringDefault(archive, availableArchives, defaults.archive, Qt::CaseInsensitive),
            Qt::EditRole);

    QList<QList<QStandardItem *> > rows;
    if (selection.variables.empty()) {
        rows = addEntryFromSelectionVariable(selection, defaults);
    } else {
        for (const auto &variable : selection.variables) {
            if (variable.empty())
                continue;
            rows.append(addEntryFromSelectionVariable(selection, defaults,
                                                      QString::fromStdString(variable)));
        }
    }

    itemCopyFanoutMerge(items, rows);

    return rows;
}

void VariableSelect::addEntryFromSelectionStation(const Archive::Selection &selection,
                                                  const ArchiveDefaults &defaults,
                                                  const QString &station)
{
    QList<QStandardItem *> items;

    QStandardItem *item = new QStandardItem;
    items << item;
    item->setData(0, Qt::EditRole);

    item = new QStandardItem;
    items << item;
    item->setData(
            indexOrStringDefault(station, availableStations, defaults.station, Qt::CaseInsensitive),
            Qt::EditRole);

    QList<QList<QStandardItem *> > rows;
    if (selection.archives.empty()) {
        rows = addEntryFromSelectionArchive(selection, defaults);
    } else {
        for (const auto &archive : selection.archives) {
            if (archive.empty())
                continue;
            rows.append(addEntryFromSelectionArchive(selection, defaults,
                                                     QString::fromStdString(archive)));
        }
    }

    itemCopyFanoutMerge(items, rows);

    for (QList<QList<QStandardItem *> >::const_iterator row = rows.constBegin(),
            endRow = rows.constEnd(); row != endRow; ++row) {
        advancedModel->insertRow(advancedModel->rowCount() - 1, *row);
    }
}

void VariableSelect::configureFromArchiveSelection(const Archive::Selection::List &selections,
                                                   const ArchiveDefaults &defaults)
{
    disconnectUpdateEvents();

    if (advancedModel->rowCount() > 1) {
        advancedModel->removeRows(0, advancedModel->rowCount() - 1);
    }

    for (const auto &selection : selections) {
        if (selection.stations.empty()) {
            addEntryFromSelectionStation(selection, defaults);
        } else {
            for (const auto &station : selection.stations) {
                if (station.empty())
                    continue;
                addEntryFromSelectionStation(selection, defaults, QString::fromStdString(station));
            }
        }
    }

    selectEffectiveTab();

    openAllEditors();
    populateStations();
    populateArchives();
    populateVariables();
    populateFlavors();
    connectUpdateEvents();
}

static void setWithDefault(CPD3::Data::Archive::Selection::Match &target,
                           const QVariant &value,
                           const QStringList &available)
{
    if (value.type() != QVariant::String) {
        int idx = value.toInt();
        if (idx == -1)
            return;
        if (idx == -2) {
            target.clear();
            return;
        }
        if (idx >= 0 || idx < available.size()) {
            target = {available.at(idx).toStdString()};
            return;
        }
    }

    QString str(value.toString());
    if (!str.isEmpty())
        target = {str.toStdString()};
}

Archive::Selection::List VariableSelect::getArchiveSelection(const ArchiveDefaults &defaults,
                                                             double start,
                                                             double end)
{
    Archive::Selection::List result;
    if (tabWidget->currentIndex() == 0) {
        Archive::Selection selection(start, end);
        if (!defaults.station.empty())
            selection.stations.push_back(defaults.station);
        if (!defaults.archive.empty())
            selection.archives.push_back(defaults.archive);

        QList<QListWidgetItem *> selected(basicList->selectedItems());
        for (QList<QListWidgetItem *>::const_iterator item = selected.constBegin(),
                endSel = selected.constEnd(); item != endSel; ++item) {
            selection.variables.push_back((*item)->data(Qt::UserRole).toString().toStdString());
        }

        if (defaults.emptySelectsEverything || !selection.variables.empty())
            result.emplace_back(std::move(selection));
    } else {
        for (int row = 0; row < advancedModel->rowCount() - 1; row++) {
            Archive::Selection selection(start, end);

            if (!defaults.station.empty())
                selection.stations.push_back(defaults.station);
            if (!defaults.archive.empty())
                selection.archives.push_back(defaults.archive);

            setWithDefault(selection.stations, advancedModel->index(row, 1).data(Qt::EditRole),
                           availableStations);
            setWithDefault(selection.archives, advancedModel->index(row, 2).data(Qt::EditRole),
                           availableArchives);
            setWithDefault(selection.variables, advancedModel->index(row, 3).data(Qt::EditRole),
                           availableVariables);

            AdvancedFlavorsData flavors
                    (advancedModel->index(row, 4).data(Qt::EditRole).value<AdvancedFlavorsData>());
            switch (flavors.mode) {
            case AdvancedFlavorsData::Defaults:
                break;

            case AdvancedFlavorsData::HasLacks:
                for (QSet<QString>::const_iterator flavor = flavors.hasFlavors.constBegin(),
                        endF = flavors.hasFlavors.constEnd(); flavor != endF; ++flavor) {
                    selection.hasFlavors.emplace_back(flavor->toStdString());
                }
                for (QSet<QString>::const_iterator flavor = flavors.lacksFlavors.constBegin(),
                        endF = flavors.lacksFlavors.constEnd(); flavor != endF; ++flavor) {
                    selection.lacksFlavors.emplace_back(flavor->toStdString());
                }
                break;

            case AdvancedFlavorsData::Exact:
                for (QSet<QString>::const_iterator flavor = flavors.exactFlavors.constBegin(),
                        endF = flavors.exactFlavors.constEnd(); flavor != endF; ++flavor) {
                    selection.exactFlavors.emplace_back(flavor->toStdString());
                }
                break;
            }

            result.emplace_back(std::move(selection));
        }
    }

    return result;
}

VariableSelect::ArchiveDefaults::ArchiveDefaults()
        : station(), archive(), emptySelectsEverything(true)
{ }

VariableSelect::ArchiveDefaults::ArchiveDefaults(const ArchiveDefaults &) = default;

VariableSelect::ArchiveDefaults &VariableSelect::ArchiveDefaults::operator=(const ArchiveDefaults &) = default;

void VariableSelect::ArchiveDefaults::setStation(const CPD3::Data::SequenceName::Component &s)
{ station = Util::to_lower(s); }

void VariableSelect::ArchiveDefaults::setArchive(const CPD3::Data::SequenceName::Component &a)
{ archive = Util::to_lower(a); }

void VariableSelect::ArchiveDefaults::setEmptySelectAll(bool enable)
{ emptySelectsEverything = enable; }


static void populateSelectionBox(VariableSelectComboBoxWidget *box,
                                 const QStringList &available,
                                 bool caseSensitive)
{
    int idx = box->selectedIndex();
    QString str(box->selectedText());

    enum {
        Select_Default, Select_Any, Select_Manual, Select_SpecificBegin,

        Select_Invalid = 0x7FFFFFFF,
    };
    int selection = Select_Invalid;

    if (idx > -1) {
        if (idx == 0) {
            selection = Select_Default;
        } else if (idx == 1) {
            selection = Select_Any;
        } else if (idx > 2) {
            selection = Select_SpecificBegin + (idx - 3);
        }
    }
    if (selection == Select_Invalid) {
        if (str.isEmpty()) {
            selection = Select_Default;
        } else {
            for (int i = 0; i < available.size(); i++) {
                if (available[i].compare(str,
                                         caseSensitive ? Qt::CaseSensitive : Qt::CaseInsensitive) ==
                        0) {
                    selection = Select_SpecificBegin + i;
                }
            }
            if (selection == Select_Invalid) {
                selection = Select_Manual;
            }
        }
    }

    box->disconnectHandlers();

    box->clear();

    box->addItem(VariableSelect::tr("Default", "selection default"));
    box->addItem(VariableSelect::tr("Any", "selection all"));
    box->insertSeparator(2);

    {
        QStringList toInsert = available;
        if (!caseSensitive) {
            for (auto &i : toInsert) {
                i = i.toUpper();
            }
        }
        box->addItems(toInsert);
    }

    switch (selection) {
    case Select_Default:
        box->setCurrentIndex(0);
        break;
    case Select_Any:
        box->setCurrentIndex(1);
        break;
    case Select_Manual:
        box->lineEdit()->setText(str);
        break;
    default:
        box->setCurrentIndex(3 + (selection - Select_SpecificBegin));
        break;
    }

    box->connectHandlers();
}

static void rebuildSelectionBoxes(QList<QPointer<VariableSelectComboBoxWidget> > &boxes,
                                  const QStringList &available,
                                  bool caseSensitive)
{

    for (QList<QPointer<VariableSelectComboBoxWidget> >::iterator box = boxes.begin();
            box != boxes.end();) {
        if (!(*box)) {
            box = boxes.erase(box);
            continue;
        }
        populateSelectionBox(box->data(), available, caseSensitive);
        ++box;
    }
}

void VariableSelect::openAllEditors()
{
    for (int row = 0; row < advancedModel->rowCount(); ++row) {
        advancedTable->openPersistentEditor(advancedModel->index(row, 0));

        if (row == advancedModel->rowCount() - 1)
            continue;

        advancedTable->openPersistentEditor(advancedModel->index(row, 1));
        advancedTable->openPersistentEditor(advancedModel->index(row, 2));
        advancedTable->openPersistentEditor(advancedModel->index(row, 3));
        advancedTable->openPersistentEditor(advancedModel->index(row, 4));
    }

    advancedTable->resizeColumnToContents(0);
}

void VariableSelect::populateStations()
{
    rebuildSelectionBoxes(stationEditors, availableStations, false);
}

void VariableSelect::populateArchives()
{
    rebuildSelectionBoxes(archiveEditors, availableArchives, false);
}

void VariableSelect::populateVariables()
{
    rebuildSelectionBoxes(variableEditors, availableVariables, true);
}

void VariableSelect::populateFlavors()
{
    for (QList<QPointer<VariableSelectFlavorsWidget> >::iterator widget = flavorsEditors.begin();
            widget != flavorsEditors.end();) {
        if (!(*widget)) {
            widget = flavorsEditors.erase(widget);
            continue;
        }
        (*widget)->setAvailable(availableFlavors);
        ++widget;
    }
}

void VariableSelect::reconfigureComboBoxes()
{
    populateStations();
    populateArchives();
    populateVariables();
    populateFlavors();
}

void VariableSelect::removeAdvancedRow()
{
    QObject *s = sender();
    if (!s)
        return;
    QVariant p(s->property("model-row"));
    if (!p.isValid())
        return;
    int row = p.toInt();
    if (row < 0 || row >= advancedModel->rowCount() - 1)
        return;

    disconnectUpdateEvents();

    advancedModel->removeRows(row, 1);

    for (row = 0; row < advancedModel->rowCount(); ++row) {
        advancedTable->closePersistentEditor(advancedModel->index(row, 0));
    }

    openAllEditors();
    populateStations();
    populateArchives();
    populateVariables();
    populateFlavors();
    connectUpdateEvents();

    emit selectionChanged();
}

void VariableSelect::addAdvancedRow()
{
    QList<QStandardItem *> items;

    QStandardItem *item = new QStandardItem;
    items << item;
    item->setData(0, Qt::EditRole);

    item = new QStandardItem;
    items << item;
    item->setData(-1, Qt::EditRole);

    item = new QStandardItem;
    items << item;
    item->setData(-1, Qt::EditRole);

    item = new QStandardItem;
    items << item;
    item->setData(-1, Qt::EditRole);

    item = new QStandardItem;
    items << item;
    item->setData(QVariant::fromValue(AdvancedFlavorsData()), Qt::EditRole);

    disconnectUpdateEvents();

    advancedModel->insertRow(advancedModel->rowCount() - 1, items);

    openAllEditors();
    populateStations();
    populateArchives();
    populateVariables();
    populateFlavors();
    connectUpdateEvents();

    emit selectionChanged();
}

void VariableSelect::groupSelected(QAction *action)
{
    QString selectionExpression(action->property("selection-expression").toString());
    if (selectionExpression.isEmpty())
        return;

    QRegExp reCheck(selectionExpression);

    for (int i = 0; i < basicList->count(); i++) {
        QListWidgetItem *item = basicList->item(i);

        QString checkName(item->data(Qt::UserRole).toString());
        if (!reCheck.exactMatch(checkName))
            continue;
        item->setSelected(true);
    }

    scrollToBasicSelection();

    emit selectionChanged();
}

void VariableSelect::clearBasicSelection()
{
    basicList->selectionModel()->clear();
    emit selectionChanged();
}

void VariableSelect::setAvailableStations(const SequenceName::ComponentSet &set)
{
    availableStations.clear();
    for (const auto &add : set) {
        availableStations.append(QString::fromStdString(add).toLower());
    }
    std::sort(availableStations.begin(), availableStations.end());
    for (int i = 1; i < availableStations.size();) {
        if (availableStations.at(i - 1) == availableStations.at(i)) {
            availableStations.removeAt(i);
            continue;
        }
        i++;
    }

    disconnectUpdateEvents();
    openAllEditors();
    populateStations();
    connectUpdateEvents();
}

void VariableSelect::setAvailableArchives(const SequenceName::ComponentSet &set)
{
    availableArchives.clear();
    for (const auto &add : set) {
        availableArchives.append(QString::fromStdString(add).toLower());
    }
    std::sort(availableArchives.begin(), availableArchives.end());
    for (int i = 1; i < availableArchives.size();) {
        if (availableArchives.at(i - 1) == availableArchives.at(i)) {
            availableArchives.removeAt(i);
            continue;
        }
        i++;
    }

    disconnectUpdateEvents();
    openAllEditors();
    populateArchives();
    connectUpdateEvents();
}

void VariableSelect::setAvailableVariables(const SequenceName::ComponentSet &set)
{
    availableVariables.clear();
    for (const auto &add : set) {
        availableVariables.append(QString::fromStdString(add));
    }
    std::sort(availableVariables.begin(), availableVariables.end());
    for (int i = 1; i < availableVariables.size();) {
        if (availableVariables.at(i - 1) == availableVariables.at(i)) {
            availableVariables.removeAt(i);
            continue;
        }
        i++;
    }

    disconnectUpdateEvents();
    openAllEditors();
    updateBasicList();
    populateVariables();
    connectUpdateEvents();
}

void VariableSelect::setAvailableFlavors(const SequenceName::ComponentSet &set)
{
    availableFlavors.clear();
    for (const auto &add : set) {
        availableFlavors.append(QString::fromStdString(add).toLower());
    }
    std::sort(availableFlavors.begin(), availableFlavors.end());
    for (int i = 0; i < availableFlavors.size();) {
        if (i > 0 && availableFlavors.at(i - 1) == availableFlavors.at(i)) {
            availableFlavors.removeAt(i);
            continue;
        }
        i++;
    }

    disconnectUpdateEvents();
    openAllEditors();
    populateFlavors();
    connectUpdateEvents();
}

void VariableSelect::setDefaultAvailable()
{
    struct Context {
        VariableSelect *select;
        std::shared_ptr<Archive::Access> access;
        std::shared_ptr<Archive::Access::ReadLock> lock;

        Context(VariableSelect *select) : select(select),
                                          access(std::make_shared<Archive::Access>()),
                                          lock(std::make_shared<Archive::Access::ReadLock>(*access))
        { }

        ~Context()
        {
            lock.reset();
            access.reset();
        }
    };

    auto context = std::make_shared<Context>(this);
    Threading::pollFuture(this, context->access->availableSelectedFuture(),
                          [context](const Archive::Access::SelectedComponents &result) {
                              context->select->setAvailableStations(result.stations);
                              context->select->setAvailableArchives(result.archives);
                              context->select->setAvailableVariables(result.variables);
                              context->select->setAvailableFlavors(result.flavors);
                          });
}

namespace Internal {

namespace {
class VariableSelectAddRemoveButton : public QPushButton {
public:
    VariableSelectAddRemoveButton(const QString &text, QWidget *parent = 0) : QPushButton(text,
                                                                                          parent)
    { }

    virtual ~VariableSelectAddRemoveButton()
    { }

    virtual QSize sizeHint() const
    {
        QSize textSize(fontMetrics().size(Qt::TextShowMnemonic, text()));
        QStyleOptionButton opt;
        opt.initFrom(this);
        opt.rect.setSize(textSize);
        return style()->sizeFromContents(QStyle::CT_PushButton, &opt, textSize, this);
    }
};
}

VariableSelectAddRemoveDelegate::VariableSelectAddRemoveDelegate(VariableSelect *vs,
                                                                 QObject *parent) : QItemDelegate(
        parent), select(vs)
{ }

VariableSelectAddRemoveDelegate::~VariableSelectAddRemoveDelegate()
{ }

QWidget *VariableSelectAddRemoveDelegate::createEditor(QWidget *parent,
                                                       const QStyleOptionViewItem &option,
                                                       const QModelIndex &index) const
{
    Q_UNUSED(option);

    QPushButton *button = new VariableSelectAddRemoveButton(tr("-", "remove text"), parent);
    button->setProperty("model-row", index.row());

    return button;
}

void VariableSelectAddRemoveDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QPushButton *button = static_cast<QPushButton *>(editor);

    button->disconnect(select);

    QVariant data(index.data(Qt::EditRole));
    if (data.toInt() != 1) {
        button->setText(tr("-", "remove text"));
        connect(button, SIGNAL(clicked(bool)), select, SLOT(removeAdvancedRow()));
    } else {
        button->setText(tr("Add", "add text"));
        connect(button, SIGNAL(clicked(bool)), select, SLOT(addAdvancedRow()));
    }

    button->setProperty("model-row", index.row());
}

void VariableSelectAddRemoveDelegate::setModelData(QWidget *editor,
                                                   QAbstractItemModel *model,
                                                   const QModelIndex &index) const
{
    Q_UNUSED(editor);
    Q_UNUSED(model);
    Q_UNUSED(index);
}


VariableSelectComboBoxWidget::VariableSelectComboBoxWidget(QWidget *parent) : QComboBox(parent)
{
    setInsertPolicy(QComboBox::NoInsert);
    setEditable(true);
    connectHandlers();
}

void VariableSelectComboBoxWidget::disconnectHandlers()
{
    disconnect(this, SIGNAL(currentIndexChanged(int)), this, SLOT(itemSelected()));
    disconnect(lineEdit(), SIGNAL(editingFinished()), this, SLOT(textEdited()));
}

void VariableSelectComboBoxWidget::connectHandlers()
{
    connect(this, SIGNAL(currentIndexChanged(int)), this, SLOT(itemSelected()));
    connect(lineEdit(), SIGNAL(editingFinished()), this, SLOT(textEdited()));
}

void VariableSelectComboBoxWidget::textEdited()
{
    emit changed(this);
}

void VariableSelectComboBoxWidget::itemSelected()
{
    emit changed(this);
}

int VariableSelectComboBoxWidget::selectedIndex() const
{
    int idx = currentIndex();
    if (idx < 0 || idx >= count())
        return -1;
    QString checkText(itemText(idx));
    if (checkText.isEmpty())
        return -1;
    if (checkText != currentText())
        return -1;
    return idx;
}

QString VariableSelectComboBoxWidget::selectedText() const
{
    return currentText();
}

VariableSelectComboBoxDelegate::VariableSelectComboBoxDelegate(VariableSelect *vs,
                                                               QList<QPointer<
                                                                       VariableSelectComboBoxWidget> > *t,
                                                               QObject *parent) : QItemDelegate(
        parent), select(vs), target(t)
{ }

VariableSelectComboBoxDelegate::~VariableSelectComboBoxDelegate()
{ }

QWidget *VariableSelectComboBoxDelegate::createEditor(QWidget *parent,
                                                      const QStyleOptionViewItem &option,
                                                      const QModelIndex &index) const
{
    Q_UNUSED(option);
    Q_UNUSED(index);

    VariableSelectComboBoxWidget *editor = new VariableSelectComboBoxWidget(parent);
    connect(editor, SIGNAL(changed(QWidget * )), this, SIGNAL(commitData(QWidget * )));

    target->append(QPointer<VariableSelectComboBoxWidget>(editor));
    select->reconfigureComboBoxes();
    return editor;
}

void VariableSelectComboBoxDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    VariableSelectComboBoxWidget *comboBox = static_cast<VariableSelectComboBoxWidget *>(editor);

    QVariant data(index.data(Qt::EditRole));
    QString str(data.toString());
    if (data.type() != QVariant::Int && !str.isEmpty()) {
        comboBox->setCurrentText(str);
        return;
    }

    int idx = data.toInt();
    if (idx == -1) {
        idx = 0;
    } else if (idx == -2) {
        idx = 1;
    } else if (idx >= 0) {
        idx += 3;
    } else if (!str.isEmpty()) {
        comboBox->setCurrentText(str);
        return;
    } else {
        idx = 0;
    }

    comboBox->setCurrentIndex(idx);
}

void VariableSelectComboBoxDelegate::setModelData(QWidget *editor,
                                                  QAbstractItemModel *model,
                                                  const QModelIndex &index) const
{
    VariableSelectComboBoxWidget *comboBox = static_cast<VariableSelectComboBoxWidget *>(editor);

    int idx = comboBox->selectedIndex();
    if (idx > -1) {
        if (idx == 0) {
            model->setData(index, -1, Qt::EditRole);
            return;
        } else if (idx == 1) {
            model->setData(index, -2, Qt::EditRole);
            return;
        } else if (comboBox->selectedText() != comboBox->itemText(idx) || idx == 2) {
            model->setData(index, comboBox->selectedText(), Qt::EditRole);
            return;
        } else {
            idx -= 3;
        }

        model->setData(index, idx, Qt::EditRole);
        return;
    }

    model->setData(index, comboBox->selectedText(), Qt::EditRole);
}


VariableSelectFlavorsListButton::VariableSelectFlavorsListButton(const QString &text,
                                                                 QWidget *parent) : QPushButton(
        text, parent)
{
    menu = new QMenu(this);
    setMenu(menu);
}

VariableSelectFlavorsListButton::~VariableSelectFlavorsListButton()
{ }

void VariableSelectFlavorsListButton::setAvailable(const QStringList &available)
{
    QSet<QString> selected;
    for (QHash<QString, QAction *>::const_iterator check = flavorActions.constBegin(),
            endCheck = flavorActions.constEnd(); check != endCheck; ++check) {
        if (!check.value()->isChecked())
            continue;
        selected |= check.key();
    }

    menu->clear();
    flavorActions.clear();

    for (QList<QString>::const_iterator flavor = available.constBegin(), end = available.constEnd();
            flavor != end;
            ++flavor) {
        QAction *action = menu->addAction(flavor->toUpper());
        flavorActions.insert(flavor->toLower(), action);
        connect(action, SIGNAL(toggled(bool)), this, SIGNAL(changed()));

        action->setCheckable(true);
        if (selected.contains(flavor->toLower())) {
            action->setChecked(true);
            selected.remove(flavor->toLower());
        }
    }

    QStringList sorted(selected.values());
    std::sort(sorted.begin(), sorted.end());
    for (QList<QString>::const_iterator flavor = sorted.constBegin(), end = sorted.constEnd();
            flavor != end;
            ++flavor) {
        QString key(*flavor);
        QString text(*flavor);
        if (QRegExp::escape(*flavor) == *flavor) {
            key = flavor->toLower();
            text = flavor->toUpper();
        }

        QAction *action = menu->addAction(text);
        flavorActions.insert(key, action);
        connect(action, SIGNAL(toggled(bool)), this, SIGNAL(changed()));

        action->setCheckable(true);
        action->setChecked(true);
    }

    addSeparator = menu->addSeparator();

    addFlavor = menu->addAction(VariableSelectFlavorsWidget::tr("&Other"));
    connect(addFlavor, SIGNAL(triggered(bool)), this, SLOT(showAddDialog()));
}

void VariableSelectFlavorsListButton::setSelection(const QSet<QString> &input)
{
    QSet<QString> selected;
    for (QSet<QString>::const_iterator flavor = input.constBegin(), end = input.constEnd();
            flavor != end;
            ++flavor) {
        QString f(*flavor);
        if (QRegExp::escape(f) == f)
            f = f.toLower();
        selected |= f;
    }

    for (QHash<QString, QAction *>::const_iterator check = flavorActions.constBegin(),
            endCheck = flavorActions.constEnd(); check != endCheck; ++check) {
        if (!selected.contains(check.key())) {
            check.value()->setChecked(false);
            continue;
        }

        check.value()->setChecked(true);
        selected.remove(check.key());
    }

    QStringList sorted(selected.values());
    std::sort(sorted.begin(), sorted.end());
    for (QSet<QString>::const_iterator flavor = selected.constBegin(), end = selected.constEnd();
            flavor != end;
            ++flavor) {
        QString key(*flavor);
        QString text(*flavor);
        if (QRegExp::escape(*flavor) == *flavor) {
            key = flavor->toLower();
            text = flavor->toUpper();
        }

        QAction *action = new QAction(text, menu);
        menu->insertAction(addSeparator, action);
        flavorActions.insert(key, action);
        connect(action, SIGNAL(toggled(bool)), this, SIGNAL(changed()));

        action->setCheckable(true);
        action->setChecked(true);
    }
}

QSet<QString> VariableSelectFlavorsListButton::getSelection() const
{
    QSet<QString> result;
    for (QHash<QString, QAction *>::const_iterator check = flavorActions.constBegin(),
            endCheck = flavorActions.constEnd(); check != endCheck; ++check) {
        if (!check.value()->isChecked())
            continue;
        result |= check.key();
    }
    return result;
}

void VariableSelectFlavorsListButton::showAddDialog()
{
    bool ok;
    QString flavor
            (QInputDialog::getText(this, tr("Add Flavor"), tr("Add flavor:"), QLineEdit::Normal,
                                   QString(), &ok));
    if (!ok || flavor.isEmpty())
        return;
    QString key(flavor);
    QString text(flavor);
    if (QRegExp::escape(flavor) == flavor) {
        key = flavor.toLower();
        text = flavor.toUpper();
    }

    QHash<QString, QAction *>::const_iterator check = flavorActions.constFind(key);
    if (check != flavorActions.constEnd()) {
        check.value()->setChecked(true);
        return;
    }

    QAction *action = new QAction(text, menu);
    menu->insertAction(addSeparator, action);
    flavorActions.insert(key, action);
    connect(action, SIGNAL(toggled(bool)), this, SIGNAL(changed()));

    action->setCheckable(true);
    action->setChecked(true);

    emit changed();
}


VariableSelectFlavorsWidget::VariableSelectFlavorsWidget(QWidget *parent) : QWidget(parent)
{
    QHBoxLayout *layout = new QHBoxLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    modeSelect = new QComboBox(this);
    modeSelect->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
    layout->addWidget(modeSelect);
    modeSelect->addItem(tr("Default", "mode default"));
    modeSelect->addItem(tr("Matched", "mode has and lacks"));
    modeSelect->addItem(tr("Exact", "mode exact"));

    has = new VariableSelectFlavorsListButton(tr("Required"), this);
    layout->addWidget(has);
    has->setAvailable(QStringList());

    lacks = new VariableSelectFlavorsListButton(tr("Excluded"), this);
    layout->addWidget(lacks);
    lacks->setAvailable(QStringList());

    exact = new VariableSelectFlavorsListButton(tr("Flavors"), this);
    layout->addWidget(exact);
    exact->setAvailable(QStringList());

    currentIndexChanged();
    connectHandlers();
}

VariableSelectFlavorsWidget::~VariableSelectFlavorsWidget()
{ }

void VariableSelectFlavorsWidget::disconnectHandlers()
{
    modeSelect->disconnect(this);
    has->disconnect(this);
    lacks->disconnect(this);
    exact->disconnect(this);
}

void VariableSelectFlavorsWidget::connectHandlers()
{
    connect(modeSelect, SIGNAL(currentIndexChanged(int)), this, SLOT(currentIndexChanged()));
    connect(modeSelect, SIGNAL(currentIndexChanged(int)), this, SLOT(commitChanges()));

    connect(has, SIGNAL(changed()), this, SLOT(commitChanges()));
    connect(lacks, SIGNAL(changed()), this, SLOT(commitChanges()));
    connect(exact, SIGNAL(changed()), this, SLOT(commitChanges()));
}

void VariableSelectFlavorsWidget::commitChanges()
{ emit changed(this); }

void VariableSelectFlavorsWidget::currentIndexChanged()
{
    switch (modeSelect->currentIndex()) {
    default:
        has->setVisible(false);
        lacks->setVisible(false);
        exact->setVisible(false);
        break;

    case 1:
        has->setVisible(true);
        lacks->setVisible(true);
        exact->setVisible(false);
        break;

    case 2:
        has->setVisible(false);
        lacks->setVisible(false);
        exact->setVisible(true);
        break;
    }
}

QVariant VariableSelectFlavorsWidget::getData() const
{
    AdvancedFlavorsData result;

    switch (modeSelect->currentIndex()) {
    default:
        result.mode = AdvancedFlavorsData::Defaults;
        break;

    case 1:
        result.mode = AdvancedFlavorsData::HasLacks;
        break;

    case 2:
        result.mode = AdvancedFlavorsData::Exact;
        break;
    }

    result.hasFlavors = has->getSelection();
    result.lacksFlavors = lacks->getSelection();
    result.exactFlavors = exact->getSelection();

    return QVariant::fromValue(result);
}

void VariableSelectFlavorsWidget::setData(const QVariant &input)
{
    disconnectHandlers();

    AdvancedFlavorsData data(input.value<AdvancedFlavorsData>());

    switch (data.mode) {
    case AdvancedFlavorsData::Defaults:
        modeSelect->setCurrentIndex(0);
        break;
    case AdvancedFlavorsData::HasLacks:
        modeSelect->setCurrentIndex(1);
        break;
    case AdvancedFlavorsData::Exact:
        modeSelect->setCurrentIndex(2);
        break;
    }

    has->setSelection(data.hasFlavors);
    lacks->setSelection(data.lacksFlavors);
    exact->setSelection(data.exactFlavors);

    currentIndexChanged();

    connectHandlers();
}

void VariableSelectFlavorsWidget::setAvailable(const QStringList &available)
{
    disconnectHandlers();

    has->setAvailable(available);
    lacks->setAvailable(available);
    exact->setAvailable(available);

    connectHandlers();
}

VariableSelectFlavorsDelegate::VariableSelectFlavorsDelegate(VariableSelect *vs,
                                                             QList<QPointer<
                                                                     VariableSelectFlavorsWidget> > *t,
                                                             QObject *parent) : QItemDelegate(
        parent), select(vs), target(t)
{ }

VariableSelectFlavorsDelegate::~VariableSelectFlavorsDelegate()
{ }

QWidget *VariableSelectFlavorsDelegate::createEditor(QWidget *parent,
                                                     const QStyleOptionViewItem &option,
                                                     const QModelIndex &index) const
{
    Q_UNUSED(option);
    Q_UNUSED(index);

    VariableSelectFlavorsWidget *editor = new VariableSelectFlavorsWidget(parent);
    connect(editor, SIGNAL(changed(QWidget * )), this, SIGNAL(commitData(QWidget * )));

    target->append(QPointer<VariableSelectFlavorsWidget>(editor));
    select->reconfigureComboBoxes();
    return editor;
}

void VariableSelectFlavorsDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    VariableSelectFlavorsWidget *flavors = static_cast<VariableSelectFlavorsWidget *>(editor);

    flavors->setData(index.data(Qt::EditRole));

}

void VariableSelectFlavorsDelegate::setModelData(QWidget *editor,
                                                 QAbstractItemModel *model,
                                                 const QModelIndex &index) const
{
    VariableSelectFlavorsWidget *flavors = static_cast<VariableSelectFlavorsWidget *>(editor);

    model->setData(index, flavors->getData(), Qt::EditRole);
}


}

}
}
}

Q_DECLARE_METATYPE(CPD3::GUI::Data::Internal::AdvancedFlavorsData);
