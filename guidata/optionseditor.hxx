/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUIDATAOPTIONSEDITOR_H
#define CPD3GUIDATAOPTIONSEDITOR_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QWidget>
#include <QToolBox>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QTextEdit>
#include <QPushButton>
#include <QCheckBox>
#include <QSpinBox>
#include <QListWidget>
#include <QLineEdit>
#include <QMenu>
#include <QAction>
#include <QFutureWatcher>
#include <QPlainTextEdit>
#include <QButtonGroup>
#include <QStyledItemDelegate>
#include <QSettings>
#include <QPointer>

#include "guidata/guidata.hxx"
#include "core/component.hxx"
#include "core/timeutils.hxx"
#include "datacore/archive/selection.hxx"
#include "datacore/stream.hxx"
#include "datacore/stream.hxx"
#include "guidata/variableselect.hxx"
#include "guidata/editors/baselinesmoother.hxx"
#include "guicore/exponentedit.hxx"
#include "guicore/scriptedit.hxx"
#include "guicore/dynamictimeinterval.hxx"
#include "guicore/timesingleselection.hxx"
#include "guicore/calibrationedit.hxx"

namespace CPD3 {
namespace GUI {
namespace Data {

class OptionEditor;

namespace Internal {

class SingleOptionEditorBase : public QWidget {
Q_OBJECT

    OptionEditor *parent;
    QString id;

    QList<QPointer<VariableSelect> > variableSelectors;

    QCheckBox *isSet;
    QPushButton *select;
    QLabel *selectText;
    QWidget *editorArea;

protected:
    QVBoxLayout *editorLayout;

    void setLabel(bool valid = true);

public:
    SingleOptionEditorBase(OptionEditor *parent, const QString &id);

    virtual ~SingleOptionEditorBase();

    inline QString getID() const
    { return id; }

    inline bool isSelected() const
    { return select->isChecked(); }

    inline void reset()
    { isSet->setChecked(false); }

    inline QPushButton *selectButton() const
    { return select; }

    inline OptionEditor *optionEditor() const
    { return parent; }

    void registerVariableSelect(VariableSelect *s);

    ComponentOptionBase *option() const;

protected slots:

    virtual void apply() = 0;

private slots:

    void setChanged();

    void updateSelected();

    void updateVariableSelections();

signals:

    void changed();

    void selectedChanged(QWidget *editor);
};

class OptionEditorSingleString : public SingleOptionEditorBase {
Q_OBJECT

    QPlainTextEdit *editor;
public:
    OptionEditorSingleString(OptionEditor *parent, const QString &id);

    virtual ~OptionEditorSingleString();

protected slots:

    virtual void apply();
};

class OptionEditorFile : public SingleOptionEditorBase {
Q_OBJECT

    QLineEdit *editor;
public:
    OptionEditorFile(OptionEditor *parent, const QString &id);

    virtual ~OptionEditorFile();

private slots:

    void selectFile();

protected slots:

    virtual void apply();
};

class OptionEditorDirectory : public SingleOptionEditorBase {
Q_OBJECT

    QLineEdit *editor;
public:
    OptionEditorDirectory(OptionEditor *parent, const QString &id);

    virtual ~OptionEditorDirectory();

private slots:

    void selectDirectory();

protected slots:

    virtual void apply();
};

class OptionEditorScript : public SingleOptionEditorBase {
Q_OBJECT

    ScriptEdit *editor;
public:
    OptionEditorScript(OptionEditor *parent, const QString &id);

    virtual ~OptionEditorScript();

protected slots:

    virtual void apply();
};

class OptionEditorEnum : public SingleOptionEditorBase {
Q_OBJECT

    QComboBox *editor;
public:
    OptionEditorEnum(OptionEditor *parent, const QString &id);

    virtual ~OptionEditorEnum();

protected slots:

    virtual void apply();
};

class OptionEditorBoolean : public SingleOptionEditorBase {
Q_OBJECT

    QCheckBox *editor;
public:
    OptionEditorBoolean(OptionEditor *parent, const QString &id);

    virtual ~OptionEditorBoolean();

protected slots:

    virtual void apply();
};

class OptionEditorSingleDouble : public SingleOptionEditorBase {
Q_OBJECT

    QCheckBox *defined;
    QLineEdit *editor;
public:
    OptionEditorSingleDouble(OptionEditor *parent, const QString &id);

    virtual ~OptionEditorSingleDouble();

protected slots:

    virtual void apply();
};

class OptionEditorSingleInteger : public SingleOptionEditorBase {
Q_OBJECT

    QCheckBox *defined;
    QLineEdit *editor;
public:
    OptionEditorSingleInteger(OptionEditor *parent, const QString &id);

    virtual ~OptionEditorSingleInteger();

protected slots:

    virtual void apply();
};


class OptionsEditorList : public QWidget {
Q_OBJECT

    QStandardItemModel *model;
    QTableView *table;

    void openAllEditors();

public:
    OptionsEditorList(QStyledItemDelegate *delegate, QWidget *parent = 0);

    int count() const;

    QVariant itemData(int row) const;

    void addItem(const QVariant &data);

signals:

    void changed();

public slots:

    void addRow();

    void removeRow();
};

class OptionEditorDoubleSetEditor : public QLineEdit {
Q_OBJECT

    SingleOptionEditorBase *option;

public:
    OptionEditorDoubleSetEditor(SingleOptionEditorBase *option, QWidget *parent = 0);

private slots:

    void updated();

signals:

    void changed(QWidget *widget);
};

class OptionEditorDoubleSet : public SingleOptionEditorBase {
Q_OBJECT

    OptionsEditorList *editor;
public:
    OptionEditorDoubleSet(OptionEditor *parent, const QString &id);

    virtual ~OptionEditorDoubleSet();

protected slots:

    virtual void apply();
};

class OptionEditorDoubleListEditor : public QLineEdit {
Q_OBJECT

    SingleOptionEditorBase *option;

public:
    OptionEditorDoubleListEditor(SingleOptionEditorBase *option, QWidget *parent = 0);

private slots:

    void updated();

signals:

    void changed(QWidget *widget);
};

class OptionEditorDoubleList : public SingleOptionEditorBase {
Q_OBJECT

    OptionsEditorList *editor;
public:
    OptionEditorDoubleList(OptionEditor *parent, const QString &id);

    virtual ~OptionEditorDoubleList();

protected slots:

    virtual void apply();
};


class OptionEditorStringSetEditor : public QLineEdit {
Q_OBJECT

    SingleOptionEditorBase *option;

public:
    OptionEditorStringSetEditor(SingleOptionEditorBase *option, QWidget *parent = 0);

private slots:

    void updated();

signals:

    void changed(QWidget *widget);
};

class OptionEditorStringSet : public SingleOptionEditorBase {
Q_OBJECT

    OptionsEditorList *editor;
public:
    OptionEditorStringSet(OptionEditor *parent, const QString &id);

    virtual ~OptionEditorStringSet();

protected slots:

    virtual void apply();
};


class OptionEditorInstrumentSuffixSet : public SingleOptionEditorBase {
Q_OBJECT

    QPushButton *select;
    QMenu *suffixMenu;
    QHash<QString, QAction *> suffixActions;
    QAction *delimiterAction;

    void setButtonLabel();

public:
    OptionEditorInstrumentSuffixSet(OptionEditor *parent, const QString &id);

    virtual ~OptionEditorInstrumentSuffixSet();

protected slots:

    virtual void apply();

private slots:

    void rebuildAvailable();

    void addSuffix();
};


class OptionEditorTimeOffset : public SingleOptionEditorBase {
Q_OBJECT

    CPD3::GUI::TimeIntervalSelection *editor;
public:
    OptionEditorTimeOffset(OptionEditor *parent, const QString &id);

    virtual ~OptionEditorTimeOffset();

protected slots:

    virtual void apply();
};

class OptionEditorSingleTime : public SingleOptionEditorBase {
Q_OBJECT

    TimeSingleSelection *editor;
public:
    OptionEditorSingleTime(OptionEditor *parent, const QString &id);

    virtual ~OptionEditorSingleTime();

protected slots:

    virtual void apply();
};

class OptionEditorSingleCalibration : public SingleOptionEditorBase {
Q_OBJECT

    CalibrationEdit *editor;
public:
    OptionEditorSingleCalibration(OptionEditor *parent, const QString &id);

    virtual ~OptionEditorSingleCalibration();

protected slots:

    virtual void apply();
};

class OptionEditorSequenceMatch : public SingleOptionEditorBase {
Q_OBJECT

    VariableSelect *editor;
public:
    OptionEditorSequenceMatch(OptionEditor *parent, const QString &id);

    virtual ~OptionEditorSequenceMatch();

protected slots:

    virtual void apply();
};


class OptionEditorTimeDependent;

class OptionEditorTimeBlock : public QWidget {
Q_OBJECT

    OptionEditorTimeDependent *parent;
public:
    OptionEditorTimeBlock(OptionEditorTimeDependent *parent);

    virtual ~OptionEditorTimeBlock();

    ComponentOptionBase *option() const;

    OptionEditor *optionEditor() const;

    virtual void apply(double start, double end) = 0;

signals:

    void changed();
};

class OptionEditorTimeDependent : public SingleOptionEditorBase {
Q_OBJECT

    struct Interval : public Time::Bounds {
        OptionEditorTimeBlock *editor;

        Interval(OptionEditorTimeBlock *e, const Time::Bounds &range = Time::Bounds());
    };

    QList<Interval> intervals;
    int selectedInterval;
    QSettings settings;

    QPushButton *timeSelect;
    QMenu *timeSelectMenu;

    QVBoxLayout *intervalEditorArea;

    bool selectTimes(Time::Bounds &range);

    void insertInterval(const Interval &ins);

    void buildMenu();

    void showSelected();

public:
    OptionEditorTimeDependent(OptionEditor *parent, const QString &id);

    virtual ~OptionEditorTimeDependent();

protected:
    virtual OptionEditorTimeBlock *createBlock() = 0;

    virtual void clear() = 0;

protected slots:

    virtual void apply();

private slots:

    void initialize();

    void addTime();

    void modifyInterval();

    void removeInterval();

    void selectTimeRange(QAction *action);
};

class OptionEditorTimeInputString : public OptionEditorTimeDependent {
Q_OBJECT

    class Editor : public OptionEditorTimeBlock {
        QPlainTextEdit *editor;
    public:
        Editor(OptionEditorTimeDependent *parent);

        virtual ~Editor();

        virtual void apply(double start, double end);
    };

public:
    OptionEditorTimeInputString(OptionEditor *parent, const QString &id);

    virtual ~OptionEditorTimeInputString();

protected:
    virtual OptionEditorTimeBlock *createBlock();

    virtual void clear();
};

class OptionEditorTimeInputBool : public OptionEditorTimeDependent {
Q_OBJECT

    class Editor : public OptionEditorTimeBlock {
        QCheckBox *editor;
    public:
        Editor(OptionEditorTimeDependent *parent);

        virtual ~Editor();

        virtual void apply(double start, double end);
    };

public:
    OptionEditorTimeInputBool(OptionEditor *parent, const QString &id);

    virtual ~OptionEditorTimeInputBool();

protected:
    virtual OptionEditorTimeBlock *createBlock();

    virtual void clear();
};

class OptionEditorTimeInputDouble : public OptionEditorTimeDependent {
Q_OBJECT

    class Editor : public OptionEditorTimeBlock {
        QCheckBox *defined;
        QLineEdit *editor;
    public:
        Editor(OptionEditorTimeDependent *parent);

        virtual ~Editor();

        virtual void apply(double start, double end);
    };

public:
    OptionEditorTimeInputDouble(OptionEditor *parent, const QString &id);

    virtual ~OptionEditorTimeInputDouble();

protected:
    virtual OptionEditorTimeBlock *createBlock();

    virtual void clear();
};

class OptionEditorTimeInputInteger : public OptionEditorTimeDependent {
Q_OBJECT

    class Editor : public OptionEditorTimeBlock {
        QCheckBox *defined;
        QLineEdit *editor;
    public:
        Editor(OptionEditorTimeDependent *parent);

        virtual ~Editor();

        virtual void apply(double start, double end);
    };

public:
    OptionEditorTimeInputInteger(OptionEditor *parent, const QString &id);

    virtual ~OptionEditorTimeInputInteger();

protected:
    virtual OptionEditorTimeBlock *createBlock();

    virtual void clear();
};

class OptionEditorTimeInputCalibration : public OptionEditorTimeDependent {
Q_OBJECT

    class Editor : public OptionEditorTimeBlock {
        CalibrationEdit *editor;
    public:
        Editor(OptionEditorTimeDependent *parent);

        virtual ~Editor();

        virtual void apply(double start, double end);
    };

public:
    OptionEditorTimeInputCalibration(OptionEditor *parent, const QString &id);

    virtual ~OptionEditorTimeInputCalibration();

protected:
    virtual OptionEditorTimeBlock *createBlock();

    virtual void clear();
};

class OptionEditorTimeIntervalSelection : public OptionEditorTimeDependent {
Q_OBJECT

    class Editor : public OptionEditorTimeBlock {
        QCheckBox *defined;
        CPD3::GUI::TimeIntervalSelection *editor;
    public:
        Editor(OptionEditorTimeDependent *parent);

        virtual ~Editor();

        virtual void apply(double start, double end);
    };

public:
    OptionEditorTimeIntervalSelection(OptionEditor *parent, const QString &id);

    virtual ~OptionEditorTimeIntervalSelection();

protected:
    virtual OptionEditorTimeBlock *createBlock();

    virtual void clear();
};

class OptionEditorFilterOperate : public OptionEditorTimeDependent {
Q_OBJECT

    class Editor : public OptionEditorTimeBlock {
        VariableSelect *editor;
    public:
        Editor(OptionEditorTimeDependent *parent);

        virtual ~Editor();

        virtual void apply(double start, double end);
    };

public:
    OptionEditorFilterOperate(OptionEditor *parent, const QString &id);

    virtual ~OptionEditorFilterOperate();

protected:
    virtual OptionEditorTimeBlock *createBlock();

    virtual void clear();
};

class OptionEditorFilterInput : public OptionEditorTimeDependent {
Q_OBJECT

    class Editor : public OptionEditorTimeBlock {
        enum {
            ConstantMode_Disabled,
            ConstantMode_Fallback,
            ConstantMode_Primary,
            ConstantMode_Undefined,
        };

        QComboBox *constantMode;
        QLineEdit *constantValue;
        VariableSelect *selection;
        QLineEdit *path;
        CalibrationEdit *calibration;
    public:
        Editor(OptionEditorTimeDependent *parent);

        virtual ~Editor();

        virtual void apply(double start, double end);
    };

public:
    OptionEditorFilterInput(OptionEditor *parent, const QString &id);

    virtual ~OptionEditorFilterInput();

protected:
    virtual OptionEditorTimeBlock *createBlock();

    virtual void clear();
};

class OptionEditorBaselineSmoother : public OptionEditorTimeDependent {
Q_OBJECT

    class Editor : public OptionEditorTimeBlock {
        BaselineSmootherEditor *editor;
    public:
        Editor(OptionEditorTimeDependent *parent);

        virtual ~Editor();

        virtual void apply(double start, double end);
    };

public:
    OptionEditorBaselineSmoother(OptionEditor *parent, const QString &id);

    virtual ~OptionEditorBaselineSmoother();

protected:
    virtual OptionEditorTimeBlock *createBlock();

    virtual void clear();
};


}

/**
 * A widget that provides editing for component options.
 */
class CPD3GUIDATA_EXPORT OptionEditor : public QWidget {
Q_OBJECT

    /**
     * This property holds the set of options the editor is configured to
     * display.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setOptions(const ComponentOptions &)
     *  <li> ComponentOptions getOptions() const
     * </ul>
     */
    Q_PROPERTY(ComponentOptions options
                       READ getOptions
                       WRITE setOptions
                       NOTIFY
                       optionsChanged)
    ComponentOptions options;

    /**
     * This property holds the set of currently available stations that the
     * user can select from.
     * 
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setAvailableStations(const CPD3::Data::SequenceName::ComponentSet &)
     *  <li> CPD3::Data::SequenceName::ComponentSet getAvailableStations() const
     * </ul>
     */
    Q_PROPERTY(CPD3::Data::SequenceName::ComponentSet availableStations
                       READ getAvailableStations
                       WRITE setAvailableStations
                       DESIGNABLE
                       true)
    CPD3::Data::SequenceName::ComponentSet availableStations;

    /**
     * This property holds the set of currently available archives that the
     * user can select from.
     * 
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setAvailableArchives(const CPD3::Data::SequenceName::ComponentSet &)
     *  <li> CPD3::Data::SequenceName::ComponentSet getAvailableArchives() const
     * </ul>
     */
    Q_PROPERTY(CPD3::Data::SequenceName::ComponentSet availableArchives
                       READ getAvailableArchives
                       WRITE setAvailableArchives
                       DESIGNABLE
                       true)
    CPD3::Data::SequenceName::ComponentSet availableArchives;

    /**
    * This property holds the set of currently available variables that the
    * user can select from.
    *
    * <br><br>
    * Access functions:
    * <ul>
    *  <li> void setAvailableVariables(const CPD3::Data::SequenceName::ComponentSet &)
    *  <li> CPD3::Data::SequenceName::ComponentSet getAvailableVariables() const
    * </ul>
    */
    Q_PROPERTY(CPD3::Data::SequenceName::ComponentSet availableVariables
                       READ getAvailableVariables
                       WRITE setAvailableVariables
                       DESIGNABLE
                       true)
    CPD3::Data::SequenceName::ComponentSet availableVariables;

    /**
     * This property holds the set of currently available flavor sets that the
     * user can select from.
     * 
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setAvailableFlavors(const CPD3::Data::SequenceName::ComponentSet &)
     *  <li> CPD3::Data::SequenceName::ComponentSet getAvailableFlavors() const
     * </ul>
     */
    Q_PROPERTY(CPD3::Data::SequenceName::ComponentSet availableFlavors
                       READ getAvailableFlavors
                       WRITE setAvailableFlavors
                       DESIGNABLE
                       true)
    CPD3::Data::SequenceName::ComponentSet availableFlavors;

    /**
     * This property holds the defaults used when configuring variable
     * selections.
     * 
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setVariableSelectionDefaults(const VariableSelect::SelectionDefaults &)
     *  <li> VariableSelect::SelectionDefaults getVariableSelectionDefaults() const
     * </ul>
     */
    Q_PROPERTY(CPD3::GUI::Data::VariableSelect::SequenceMatchDefaults variableSelectionDefaults
                       READ getVariableSelectionDefaults
                       WRITE setVariableSelectionDefaults
                       DESIGNABLE
                       true)
    CPD3::GUI::Data::VariableSelect::SequenceMatchDefaults variableSelectionDefaults;

    QWidget *displayArea;
    QVBoxLayout *displayLayout;
    QList<Internal::SingleOptionEditorBase *> editors;

    friend class Internal::SingleOptionEditorBase;

    ComponentOptionBase *option(const QString &id);

    Internal::SingleOptionEditorBase *create(const QString &id);

public:
    OptionEditor(QWidget *parent = 0);

    virtual ~OptionEditor();

    ComponentOptions getOptions() const;

    void setAvailableStations(const CPD3::Data::SequenceName::ComponentSet &set);

    void setAvailableArchives(const CPD3::Data::SequenceName::ComponentSet &set);

    void setAvailableVariables(const CPD3::Data::SequenceName::ComponentSet &set);

    void setAvailableFlavors(const CPD3::Data::SequenceName::ComponentSet &set);

    CPD3::Data::SequenceName::ComponentSet getAvailableStations() const;

    CPD3::Data::SequenceName::ComponentSet getAvailableArchives() const;

    CPD3::Data::SequenceName::ComponentSet getAvailableVariables() const;

    CPD3::Data::SequenceName::ComponentSet getAvailableFlavors() const;

    void setVariableSelectionDefaults
            (const CPD3::GUI::Data::VariableSelect::SequenceMatchDefaults &selection);

    CPD3::GUI::Data::VariableSelect::SequenceMatchDefaults getVariableSelectionDefaults() const;

    void configureAvailable(const CPD3::Data::Archive::Selection &selection);

    void configureAvailable();

    virtual QSize sizeHint() const;

signals:

    /**
     * Emitted when an option is changed.
     */
    void optionsChanged();

    /* Internal use */
    void availableChanged();

public slots:

    void setOptions(const ComponentOptions &options);

private slots:

    void updateExclude();

    void updateVisible(QWidget *checked);
};

}
}
}

#endif
