/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUIDATADATAEDITOR_H
#define CPD3GUIDATADATAEDITOR_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QWidget>
#include <QList>
#include <QSet>
#include <QComboBox>
#include <QFlags>
#include <QPushButton>
#include <QSettings>
#include <QUndoStack>
#include <QUndoCommand>
#include <QCheckBox>
#include <QSpinBox>
#include <QScrollArea>

#include "guidata/guidata.hxx"
#include "datacore/stream.hxx"
#include "guicore/timeboundselection.hxx"
#include "guidata/datavaluetimeline.hxx"
#include "guidata/valueeditor.hxx"
#include "datacore/stream.hxx"

namespace CPD3 {
namespace GUI {
namespace Data {

/**
 * Provides a widget to edit, select, and view data values.
 */
class CPD3GUIDATA_EXPORT DataEditor : public QWidget {
Q_OBJECT

    Q_FLAGS(EditingFlags
                    SelectionFlags)

public:

    /**
     * Flags on what to show in the selection box.  Some of these can be
     * altered in the advanced dialog if it is enabled.
     */
    enum SelectionFlag {
        SelectStation = 0x1, /** Show the station selection box. */
        SelectArchive = 0x2, /** Show the archive selection box. */
        SelectVariable = 0x4, /** Show the variable selection box. */
        SelectFlavors = 0x8, /** Show the two flavor selection boxes. */
        SelectTimes = 0x10, /** Show the time selection box. */

        SelectDefaultStation = 0x20, /** Show the default station in the box. */

        SelectAllStations = 0x40, /** Show the "all" station in the box. */
        SelectAllArchives = 0x80, /** Show the "all" archive in the box. */
        SelectAllVariables = 0x100,     /** Show the "all" variable in the box. */

        /**
         * Enable all selection components.
         */
                SelectFull =
                SelectStation | SelectArchive | SelectVariable | SelectFlavors | SelectTimes,

        /**
         * Allow manual entry of selection criteria.
         */
                SelectManual = 0x1000,

        /**
         * The default mode for the selection display.  This enables selection
         * for everything except flavors.
         */
                DefaultSelectionFlags =
                SelectStation | SelectArchive | SelectVariable | SelectTimes,

        /**
         * Disable all selection boxes.
         */
                DisableSelection = 0,

        /**
         * Enable all selection options.
         */
                SelectEverything = SelectFull |
                SelectDefaultStation |
                SelectAllStations |
                SelectAllArchives |
                SelectAllVariables |
                SelectManual,
    };
    Q_DECLARE_FLAGS(SelectionFlags, SelectionFlag)

    /**
     * Flags on what to show in the selection box.  Some of these can be
     * altered in the advanced dialog if it is enabled.
     */
    enum ModifyFlag {
        /** Allow modification when a default station value is selected. */
                ModifyDefaultStationValues = 0x1,

        /** Allow modification of the selected value's name. */
                ModifySelectedName = 0x2,

        /** The defaults for modification, allowing changing of everything. */
                DefaultModify = ModifyDefaultStationValues | ModifySelectedName,

        /** Restrict modification and only allow for value changes. */
                ModifyRestricted = 0,
    };
    Q_DECLARE_FLAGS(ModifyFlags, ModifyFlag)

    typedef CPD3::Data::SequenceIdentity::Map<CPD3::Data::Variant::Root> DataSet;

private:

    /**
     * This property holds the current station.  This is empty by default.  
     * This is an inconstant state, so it must be set to something valid.
     * Changing the value will cause the corresponding signals to be emitted.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setStation(const CPD3::Data::SequenceName::Component &)
     *  <li> QString getStation() const
     * </ul>
     * @see stationChanged( const CPD3::Data::SequenceName::Component & ), selectionChanged()
     */
    Q_PROPERTY(CPD3::Data::SequenceName::Component station
                       READ getStation
                       WRITE setStation
                       DESIGNABLE
                       true
                       USER
                       true)
    CPD3::Data::SequenceName::Component station;

    /**
     * This property holds the current archive.  This is empty by default.  
     * This is an inconstant state, so it must be set to something valid.
     * Changing the value will cause the corresponding signals to be emitted.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setArchive(const CPD3::Data::SequenceName::Component &)
     *  <li> QString getArchive() const
     * </ul>
     * @see archiveChanged( const CPD3::Data::SequenceName::Component & ), selectionChanged()
     */
    Q_PROPERTY(CPD3::Data::SequenceName::Component archive
                       READ getArchive
                       WRITE setArchive
                       DESIGNABLE
                       true
                       USER
                       true)
    CPD3::Data::SequenceName::Component archive;

    /**
     * This property holds the current variable.  This is empty by default.  
     * This is an inconstant state, so it must be set to something valid.
     * Changing the value will cause the corresponding signals to be emitted.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setVariable(const CPD3::Data::SequenceName::Component &)
     *  <li> QString getVariable() const
     * </ul>
     * @see variableChanged( const CPD3::Data::SequenceName::Component & ), selectionChanged()
     */
    Q_PROPERTY(CPD3::Data::SequenceName::Component variable
                       READ getVariable
                       WRITE setVariable
                       DESIGNABLE
                       true
                       USER
                       true)
    CPD3::Data::SequenceName::Component variable;

    /**
     * This property holds the current set of flavors to require.  Changing the 
     * value will cause the corresponding signals to be emitted.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setHasFlavors(const std::vector<CPD3::Data::SequenceName::Component> &)
     *  <li> QStringList getHasFlavors() const
     * </ul>
     * @see hasFlavorsChanged( const std::vector<CPD3::Data::SequenceName::Component> & ), selectionChanged()
     */
    Q_PROPERTY(std::vector<CPD3::Data::SequenceName::Component> hasFlavors
                       READ getHasFlavors
                       WRITE setHasFlavors
                       DESIGNABLE
                       true
                       USER
                       true)
    std::vector<CPD3::Data::SequenceName::Component> hasFlavors;

    /**
     * This property holds the current set of flavors that must not be
     * present.  Changing the value will cause the corresponding signals to be 
     * emitted.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setLacksFlavors(const std::vector<CPD3::Data::SequenceName::Component> &)
     *  <li> QStringList getLacksFlavors() const
     * </ul>
     * @see lacksFlavorsChanged( const std::vector<CPD3::Data::SequenceName::Component> & ), selectionChanged()
     */
    Q_PROPERTY(std::vector<CPD3::Data::SequenceName::Component> lacksFlavors
                       READ getLacksFlavors
                       WRITE setLacksFlavors
                       DESIGNABLE
                       true
                       USER
                       true)
    std::vector<CPD3::Data::SequenceName::Component> lacksFlavors;

    /**
     * This property holds the currently selected start time.  This can be
     * undefined to show all data.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setStart(double)
     *  <li> void setBounds(double, double)
     *  <li> double getStart() const
     * </ul>
     * @see startChanged( double ), boundsChanged( double, double ), selectionChanged()
     */
    Q_PROPERTY(double start
                       READ getStart
                       WRITE setStart
                       DESIGNABLE
                       false
                       USER
                       true)
    double start;

    /**
     * This property holds the currently selected end time.  This can be
     * undefined to show all data.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setEnd(double)
     *  <li> void setBounds(double, double)
     *  <li> double getEnd() const
     * </ul>
     * @see endChanged( double ), boundsChanged( double, double ), selectionChanged()
     */
    Q_PROPERTY(double end
                       READ getEnd
                       WRITE setEnd
                       DESIGNABLE
                       false
                       USER
                       true)
    double end;

    QStringList availableStations;
    QStringList availableArchives;
    QStringList availableVariables;
    QStringList availableFlavors;

    /**
     * This property holds the current selection flags.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setSelectionFlags(SelectionFlags)
     *  <li> SelectionFlags getSelectionFlags() const
     * </ul>
     */
    Q_PROPERTY(SelectionFlags selectionFlags
                       READ getSelectionFlags
                       WRITE setSelectionFlags
                       DESIGNABLE
                       true)
    SelectionFlags selectionFlags;

    /**
     * This property sets if the default station values can be changed.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setModifyFlags(ModifyFlags)
     *  <li> ModifyFlags getModifyFlags() const
     * </ul>
     */
    Q_PROPERTY(ModifyFlags modifyFlags
                       READ getModifyFlags
                       WRITE setModifyFlags
                       DESIGNABLE
                       true)
    ModifyFlags modifyFlags;

    QSettings settings;
    bool haveLoadedSettings;

    void updateUnitSelectionBoxContents();

    void updateSelectionVisiblity();

    void updateTimelineValues();

    void valuesUpdated();

    bool valueIsNew(const DataSet::value_type &check) const;

    DataSet values;
    CPD3::Data::SequenceValue::Transfer metadata;
    DataSet pristine;
    bool changesMade;


    QGridLayout *mainLayout;
    QGridLayout *timelineLayout;

    QComboBox *stationSelect;
    QComboBox *archiveSelect;
    QComboBox *variableSelect;
    QComboBox *hasFlavorsSelect;
    QComboBox *lacksFlavorsSelect;
    TimeBoundSelection *timeSelect;
    DataValueTimeline *timeline;
    QPushButton *showUnitEdit;
    TimeBoundSelection *timeEdit;
    QSpinBox *priorityEdit;
    QPushButton *addButton;
    QPushButton *removeButton;
    ValueEditor *editor;

    CPD3::Data::SequenceIdentity selectedIdentity;
    CPD3::Data::Variant::Write selectedContents;

    void updateSelectionSingleBoxContents(QComboBox *box,
                                          const QStringList &contents,
                                          const CPD3::Data::SequenceName::Component &current,
                                          const char *slot,
                                          bool uc,
                                          bool enableAll,
                                          bool enableDefault,
                                          bool enableManual);

    void updateSelectionSingleBoxContents(QComboBox *box,
                                          const QStringList &contents,
                                          const std::vector<
                                                  CPD3::Data::SequenceName::Component> &current,
                                          const char *slot,
                                          bool uc,
                                          bool enableAll,
                                          bool enableDefault,
                                          bool enableManual);

    QString lookupBoxSelection(const QComboBox *box,
                               bool enableAll,
                               bool enableDefault,
                               bool enableManual);

    void clearSelection();

public:
    DataEditor(QWidget *parent = 0);

    const CPD3::Data::SequenceName::Component &getStation() const;

    const CPD3::Data::SequenceName::Component &getArchive() const;

    const CPD3::Data::SequenceName::Component &getVariable() const;

    const std::vector<CPD3::Data::SequenceName::Component> &getHasFlavors() const;

    const std::vector<CPD3::Data::SequenceName::Component> &getLacksFlavors() const;

    double getStart() const;

    double getEnd() const;

    /**
     * Set the currently available stations.
     *
     * @param set   the available stations
     */
    void setAvailableStations(const CPD3::Data::SequenceName::ComponentSet &set);

    /**
     * Set the currently available archives.
     *
     * @param set   the available archives
     */
    void setAvailableArchives(const CPD3::Data::SequenceName::ComponentSet &set);

    /**
     * Set the currently available variables.
     *
     * @param set   the available variables
     */
    void setAvailableVariables(const CPD3::Data::SequenceName::ComponentSet &set);

    /**
     * Set the currently available flavors.
     *
     * @param set   the available flavors
     */
    void setAvailableFlavors(const CPD3::Data::SequenceName::ComponentSet &set);

    void setSelectionFlags(SelectionFlags flags);

    SelectionFlags getSelectionFlags() const;

    void setModifyFlags(ModifyFlags flags);

    ModifyFlags getModifyFlags() const;

    CPD3::Data::SequenceValue::Transfer getData() const;

    CPD3::Data::SequenceValue::Transfer getValues() const;

    CPD3::Data::SequenceValue::Transfer getMetadata() const;

    CPD3::Data::SequenceValue::Transfer getNew() const;

    CPD3::Data::SequenceValue::Transfer getRemoved() const;

    DataSet getNewSet() const;

    DataSet getRemovedSet() const;

    bool isPristine() const;

public slots:

    void setStation(const CPD3::Data::SequenceName::Component &set);

    void setArchive(const CPD3::Data::SequenceName::Component &set);

    void setVariable(const CPD3::Data::SequenceName::Component &set);

    void setHasFlavors(const CPD3::Data::SequenceName::ComponentSet &set);

    void setHasFlavors(const std::vector<CPD3::Data::SequenceName::Component> &set);

    void setLacksFlavors(const CPD3::Data::SequenceName::ComponentSet &set);

    void setLacksFlavors(const std::vector<CPD3::Data::SequenceName::Component> &set);

    void setBounds(double s, double e);

    void setStart(double v);

    void setEnd(double v);

    void setData(const CPD3::Data::SequenceValue::Transfer &data);

    void setValues(const CPD3::Data::SequenceValue::Transfer &data);

    void setMetadata(const CPD3::Data::SequenceValue::Transfer &data);

    void setData(const DataSet &data);

    void setValues(const DataSet &data);

    void setMetadata(const DataSet &data);

    void selectValue(const CPD3::Data::SequenceIdentity &value);

signals:

    /**
     * Emitted whenever a new station is selected.
     * 
     * @param station   the new station
     */
    void stationChanged(const CPD3::Data::SequenceName::Component &station);

    /**
     * Emitted whenever a new archive is selected.
     * 
     * @param archive   the new archive
     */
    void archiveChanged(const CPD3::Data::SequenceName::Component &archive);

    /**
     * Emitted whenever a new variable is selected.
     * 
     * @param variable  the new variable
     */
    void variableChanged(const CPD3::Data::SequenceName::Component &variable);

    /**
     * Emitted whenever a new set of flavors to require is selected.
     * 
     * @param flavors   the selected flavors
     */
    void hasFlavorsChanged(const std::vector<CPD3::Data::SequenceName::Component> &flavors);

    /**
     * Emitted whenever a new set of flavors to require to be absent is 
     * selected.
     * 
     * @param flavors   the selected flavors
     */
    void lacksFlavorsChanged(const std::vector<CPD3::Data::SequenceName::Component> &flavors);

    /**
     * Emitted whenever the start time changes.
     * 
     * @param start     the current start time
     */
    void startChanged(double start);

    /**
     * Emitted whenever the end time changes.
     * 
     * @param start     the current end time
     */
    void endChanged(double end);

    /**
     * Emitted whenever either or both of the start and/or end changes.
     * 
     * @param start     the current start time
     * @param end       the current end time
     */
    void boundsChanged(double start, double end);

    /**
     * Emitted whenever a change to a data value is made, including addition
     * or deletion.
     */
    void dataModified();

    /**
     * Emitted whenever any part of the selection parameters has changed.
     */
    void selectionChanged();

private slots:

    void stationSelected();

    void archiveSelected();

    void variableSelected();

    void hasFlavorsSelected();

    void lacksFlavorsSelected();

    void showUnitEditPressed();

    void removePressed();

    void addPressed();

    void selectedTimesEdited(double s, double e);

    void selectedPriorityEdited(int p);

    void valueModified();
};

}
}
}

Q_DECLARE_OPERATORS_FOR_FLAGS(CPD3::GUI::Data::DataEditor::SelectionFlags)

#endif
