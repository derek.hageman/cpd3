/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <memory>
#include <cctype>
#include <QVBoxLayout>
#include <QTextEdit>
#include <QLabel>
#include <QFileDialog>
#include <QInputDialog>
#include <QtConcurrentRun>
#include <QApplication>
#include <QStyle>
#include <QScrollArea>
#include <QHeaderView>
#include <QDialogButtonBox>

#include "guidata/optionseditor.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicinput.hxx"
#include "smoothing/baseline.hxx"
#include "guicore/timeboundselection.hxx"
#include "guicore/guiformat.hxx"

using namespace CPD3::Data;
using namespace CPD3::Smoothing;
using namespace CPD3::GUI::Data::Internal;

namespace CPD3 {
namespace GUI {
namespace Data {

/** @file guidata/optionseditor.hxx
 * Provides an editor for component options.
 */

namespace Internal {

SingleOptionEditorBase::SingleOptionEditorBase(OptionEditor *p, const QString &i) : QWidget(p),
                                                                                    parent(p),
                                                                                    id(i),
                                                                                    variableSelectors(),
                                                                                    isSet(NULL),
                                                                                    select(NULL),
                                                                                    editorArea(
                                                                                            NULL),
                                                                                    editorLayout(
                                                                                            NULL)
{

    connect(parent, SIGNAL(availableChanged()), this, SLOT(updateVariableSelections()));

    QGridLayout *layout = new QGridLayout(this);
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setColumnStretch(0, 1);
    layout->setRowStretch(1, 1);

    select = new QPushButton(this);
    layout->addWidget(select, 0, 0);
    select->setToolTip(tr("Option enabled"));
    select->setStatusTip(tr("Enable"));
    select->setWhatsThis(
            tr("Use this button to edit the value of the option.  Pressing it will display the editor that allows the value to be set."));
    select->setCheckable(true);
    select->setChecked(false);
    {
        QSizePolicy policy(QSizePolicy::MinimumExpanding, QSizePolicy::Minimum, QSizePolicy::Label);
        select->setSizePolicy(policy);
    }
    connect(select, SIGNAL(toggled(bool)), this, SLOT(updateSelected()));

    selectText = new QLabel(select);
    {
        QVBoxLayout *buttonLayout = new QVBoxLayout(select);
        select->setLayout(buttonLayout);
        buttonLayout->setContentsMargins(5, 5, 5, 5);
        buttonLayout->addWidget(selectText);
    }
    selectText->setWordWrap(true);
    selectText->setAttribute(Qt::WA_TransparentForMouseEvents);
    selectText->setAlignment(Qt::AlignHCenter | Qt::AlignVCenter);

    isSet = new QCheckBox(this);
    layout->addWidget(isSet, 0, 1);
    isSet->setToolTip(tr("Option enabled"));
    isSet->setStatusTip(tr("Enable"));
    isSet->setWhatsThis(
            tr("When selected, the option is considered \"set\" and has a value.  When not set, options may have assumed defaults."));
    isSet->setChecked(option()->isSet());
    {
        QSizePolicy policy(isSet->sizePolicy());
        policy.setVerticalPolicy(QSizePolicy::MinimumExpanding);
        isSet->setSizePolicy(policy);
    }
    connect(isSet, SIGNAL(toggled(bool)), this, SLOT(setChanged()));

    editorArea = new QWidget(this);
    layout->addWidget(editorArea, 1, 0, 1, -1);
    editorLayout = new QVBoxLayout(editorArea);
    editorArea->setLayout(editorLayout);
    //editorLayout->setContentsMargins(0,0,0,0);

    QLabel *label = new QLabel(option()->getDescription());
    label->setWordWrap(true);
    editorLayout->addWidget(label);

    setLabel();

    editorArea->setVisible(false);
}

SingleOptionEditorBase::~SingleOptionEditorBase()
{ }

ComponentOptionBase *SingleOptionEditorBase::option() const
{ return parent->option(id); }

void SingleOptionEditorBase::setLabel(bool valid)
{
    if (option()->isSet() || option()->getDefaultBehavior().isEmpty()) {
        if (valid) {
            selectText->setStyleSheet(QString());
        } else {
            selectText->setStyleSheet("QLabel {color: red;}");
        }
        selectText->setText(tr("%1 - %2", "option set label").arg(option()->getArgumentName(),
                                                                  option()->getDisplayName()));
    } else {
        selectText->setText(
                tr("%1 (%3) - %2", "option unset label").arg(option()->getArgumentName(),
                                                             option()->getDisplayName(),
                                                             option()->getDefaultBehavior()));
    }
}

void SingleOptionEditorBase::setChanged()
{
    if (isSet->isChecked()) {
        apply();
    } else {
        select->setChecked(false);
        editorArea->setVisible(false);
        option()->reset();
        emit changed();
    }
    setLabel();
}

void SingleOptionEditorBase::updateSelected()
{
    if (select->isChecked()) {
        isSet->setChecked(true);
        editorArea->setVisible(true);
        emit selectedChanged(this);
    } else {
        editorArea->setVisible(false);
    }
}

void SingleOptionEditorBase::updateVariableSelections()
{
    for (QList<QPointer<VariableSelect> >::iterator sel = variableSelectors.begin();
            sel != variableSelectors.end();) {
        VariableSelect *s = sel->data();
        if (!s) {
            sel = variableSelectors.erase(sel);
            continue;
        }
        s->setAvailableStations(parent->getAvailableStations());
        s->setAvailableArchives(parent->getAvailableArchives());
        s->setAvailableVariables(parent->getAvailableVariables());
        s->setAvailableFlavors(parent->getAvailableFlavors());

        ++sel;
    }
}

void SingleOptionEditorBase::registerVariableSelect(VariableSelect *s)
{
    variableSelectors.append(QPointer<VariableSelect>(s));
    s->setAvailableStations(parent->getAvailableStations());
    s->setAvailableArchives(parent->getAvailableArchives());
    s->setAvailableVariables(parent->getAvailableVariables());
    s->setAvailableFlavors(parent->getAvailableFlavors());
}


OptionEditorSingleString::OptionEditorSingleString(OptionEditor *parent, const QString &id)
        : SingleOptionEditorBase(parent, id)
{
    editor = new QPlainTextEdit(this);
    {
        QMargins margins(editor->contentsMargins());
        QFontMetrics fm(editor->font());
        editor->setFixedHeight(margins.top() +
                                       margins.bottom() +
                                       editor->document()->documentMargin() +
                                       fm.height() +
                                       5);
    }

    editor->setPlainText(static_cast<ComponentOptionSingleString *>(
                                 option())->get());

    connect(editor, SIGNAL(textChanged()), this, SLOT(apply()));

    editorLayout->addWidget(editor, 0);
}

OptionEditorSingleString::~OptionEditorSingleString()
{ }

void OptionEditorSingleString::apply()
{
    static_cast<ComponentOptionSingleString *>(option())->set(editor->toPlainText());
    emit changed();
}

OptionEditorFile::OptionEditorFile(OptionEditor *parent, const QString &id)
        : SingleOptionEditorBase(parent, id)
{
    QWidget *container = new QWidget(this);
    editorLayout->addWidget(container);
    QHBoxLayout *line = new QHBoxLayout(container);
    container->setLayout(line);
    line->setContentsMargins(0, 0, 0, 0);

    editor = new QLineEdit(this);
    line->addWidget(editor, 1);
    editor->setToolTip(tr("Selected file"));
    editor->setStatusTip(tr("File"));
    editor->setWhatsThis(tr("This is the file currently selected for use."));
    editor->setText(static_cast<ComponentOptionFile *>(option())->get());
    connect(editor, SIGNAL(textChanged(
                                   const QString &)), this, SLOT(apply()));

    QPushButton *button =
            new QPushButton(QApplication::style()->standardIcon(QStyle::SP_DialogSaveButton),
                            QString(), container);
    button->setContentsMargins(0, 0, 0, 0);
    line->addWidget(button);
    button->setToolTip(tr("Select file"));
    button->setStatusTip(tr("File selection"));
    button->setWhatsThis(tr("Press this button to select the file to use."));
    connect(button, SIGNAL(clicked(bool)), this, SLOT(selectFile()));

}

OptionEditorFile::~OptionEditorFile()
{ }


void OptionEditorFile::selectFile()
{
    ComponentOptionFile *o = static_cast<ComponentOptionFile *>(option());

    QStringList filters;
    QStringList extensionOrder;
    QSet<QString> extensions(o->getExtensions());
    if (!o->getTypeDescription().isEmpty()) {
        if (extensions.isEmpty()) {
            filters << QString("%1 (*)").arg(o->getTypeDescription());
        } else {
            QString ef;
            QStringList sorted(extensions.values());
            std::sort(sorted.begin(), sorted.end());
            for (QList<QString>::const_iterator str = sorted.constBegin(), end = sorted.constEnd();
                    str != end;
                    ++str) {
                if (!ef.isEmpty())
                    ef.append(' ');
                ef.append(QString("*.%1").arg(*str));
                extensionOrder.append(*str);
            }

            filters << QString("%1 (%2)").arg(o->getTypeDescription(), ef);
            filters << tr("Any files (*)");
        }
    } else if (!extensions.isEmpty()) {
        QString ef;
        for (QSet<QString>::const_iterator str = extensions.constBegin(),
                end = extensions.constEnd(); str != end; ++str) {
            if (!ef.isEmpty())
                ef.append(' ');
            ef.append(QString("*.%1").arg(*str));
        }
        filters << tr("Requested files (%1)").arg(ef);
        filters << tr("Any files (*)");
    }

    QFileDialog dialog(this);
    dialog.selectFile(editor->text());

    switch (o->getMode()) {
    case ComponentOptionFile::Read:
        dialog.setOptions(
                dialog.options() | QFileDialog::ReadOnly | QFileDialog::DontConfirmOverwrite);
        dialog.setFileMode(QFileDialog::ExistingFile);
        break;
    case ComponentOptionFile::Write: {
        QString selectedFilter;
        QString fileName
                (QFileDialog::getSaveFileName(this, QString(), QString(), filters.join(";;"),
                                              &selectedFilter));
        if (fileName.isEmpty())
            return;

        QFileInfo info(fileName);
        if (info.completeSuffix().isEmpty() && !info.exists()) {
            int index = filters.indexOf(selectedFilter);
            if (index >= 0 && index <= extensionOrder.size()) {
                fileName.append(extensionOrder.at(index));
            }
        }

        o->set(fileName);
        editor->setText(fileName);
        emit changed();
        return;
    }
    case ComponentOptionFile::ReadWrite:
        dialog.setOptions(dialog.options() | QFileDialog::DontConfirmOverwrite);
        dialog.setFileMode(QFileDialog::ExistingFile);
        break;
    }

    if (!filters.isEmpty())
        dialog.setNameFilters(filters);

    if (!dialog.exec())
        return;

    QStringList selected(dialog.selectedFiles());
    if (selected.isEmpty())
        return;
    if (selected.first().isEmpty())
        return;

    editor->setText(selected.first());
}

void OptionEditorFile::apply()
{
    static_cast<ComponentOptionFile *>(option())->set(editor->text());
    emit changed();
}

OptionEditorDirectory::OptionEditorDirectory(OptionEditor *parent, const QString &id)
        : SingleOptionEditorBase(parent, id)
{
    QWidget *container = new QWidget(this);
    editorLayout->addWidget(container);
    QHBoxLayout *line = new QHBoxLayout(container);
    container->setLayout(line);
    line->setContentsMargins(0, 0, 0, 0);

    editor = new QLineEdit(this);
    line->addWidget(editor, 1);
    editor->setToolTip(tr("Selected directory"));
    editor->setStatusTip(tr("Directory"));
    editor->setWhatsThis(tr("This is the directory currently selected for use."));
    editor->setText(static_cast<ComponentOptionFile *>(option())->get());
    connect(editor, SIGNAL(textChanged(
                                   const QString &)), this, SLOT(apply()));

    QPushButton *button =
            new QPushButton(QApplication::style()->standardIcon(QStyle::SP_DialogSaveButton),
                            QString(), container);
    button->setContentsMargins(0, 0, 0, 0);
    line->addWidget(button);
    button->setToolTip(tr("Select directory"));
    button->setStatusTip(tr("Directory selection"));
    button->setWhatsThis(tr("Press this button to select the directory to use."));
    connect(button, SIGNAL(clicked(bool)), this, SLOT(selectDirectory()));
}

OptionEditorDirectory::~OptionEditorDirectory()
{ }

void OptionEditorDirectory::selectDirectory()
{
    QFileDialog dialog(this);
    dialog.selectFile(editor->text());

    dialog.setOptions(dialog.options() | QFileDialog::ReadOnly | QFileDialog::DontConfirmOverwrite);
    dialog.setFileMode(QFileDialog::Directory);
    dialog.setOptions(dialog.options() | QFileDialog::ShowDirsOnly);

    if (!dialog.exec())
        return;

    QStringList selected(dialog.selectedFiles());
    if (selected.isEmpty())
        return;
    if (selected.first().isEmpty())
        return;

    editor->setText(selected.first());
}

void OptionEditorDirectory::apply()
{
    static_cast<ComponentOptionFile *>(option())->set(editor->text());
    emit changed();
}

OptionEditorScript::OptionEditorScript(OptionEditor *parent, const QString &id)
        : SingleOptionEditorBase(parent, id)
{
    editor = new ScriptEdit(this);

    editor->setCode(static_cast<ComponentOptionScript *>(
                            option())->get());

    connect(editor, SIGNAL(scriptChanged(
                                   const QString &)), this, SLOT(apply()));

    editorLayout->addWidget(editor, 1);
}

OptionEditorScript::~OptionEditorScript()
{ }

void OptionEditorScript::apply()
{
    static_cast<ComponentOptionScript *>(option())->set(editor->getCode());
    emit changed();
}


static bool enumOptionSort(const ComponentOptionEnum::EnumValue &a,
                           const ComponentOptionEnum::EnumValue &b)
{ return a.getDescription() < b.getDescription(); }

OptionEditorEnum::OptionEditorEnum(OptionEditor *parent, const QString &id)
        : SingleOptionEditorBase(parent, id)
{
    editor = new QComboBox(this);

    editor->setToolTip(tr("Select from one of multiple options"));
    editor->setStatusTip(tr("Option selection"));
    editor->setWhatsThis(tr("This represents the available possibilities for the option."));

    ComponentOptionEnum *o = static_cast<ComponentOptionEnum *>(option());
    QList<ComponentOptionEnum::EnumValue> choices(o->getAll());
    std::sort(choices.begin(), choices.end(), enumOptionSort);
    for (QList<ComponentOptionEnum::EnumValue>::const_iterator add = choices.constBegin(),
            end = choices.constEnd(); add != end; ++add) {
        editor->addItem(tr("%1 - %2", "enum format").arg(add->getName(), add->getDescription()),
                        add->getInternalName());
        if (add->getInternalName() == o->get().getInternalName())
            editor->setCurrentIndex(editor->count() - 1);
    }

    connect(editor, SIGNAL(currentIndexChanged(int)), this, SLOT(apply()));

    editorLayout->addWidget(editor);
}

OptionEditorEnum::~OptionEditorEnum()
{ }

void OptionEditorEnum::apply()
{
    int idx = editor->currentIndex();
    if (idx < 0 || idx >= editor->count())
        return;
    static_cast<ComponentOptionEnum *>(option())->set(editor->itemData(idx).toString());
    emit changed();
}


OptionEditorBoolean::OptionEditorBoolean(OptionEditor *parent, const QString &id)
        : SingleOptionEditorBase(parent, id)
{
    editor = new QCheckBox(tr("Option set"), this);
    editor->setChecked(static_cast<ComponentOptionBoolean *>(option())->get());
    editor->setToolTip(tr("The option set state"));
    editor->setStatusTip(tr("Option set"));
    editor->setWhatsThis(
            tr("This indicates if the option is set to true.  The option can be set but false when this is unchecked.  This is used to override a default true behavior."));

    connect(editor, SIGNAL(toggled(bool)), this, SLOT(apply()));

    editorLayout->addWidget(editor);
}

OptionEditorBoolean::~OptionEditorBoolean()
{ }

void OptionEditorBoolean::apply()
{
    if (!option()->isSet())
        editor->setChecked(true);
    static_cast<ComponentOptionBoolean *>(option())->set(editor->isChecked());
    emit changed();
}

OptionEditorSingleDouble::OptionEditorSingleDouble(OptionEditor *parent, const QString &id)
        : SingleOptionEditorBase(parent, id)
{
    QWidget *container = new QWidget(this);
    editorLayout->addWidget(container);
    QHBoxLayout *line = new QHBoxLayout(container);
    container->setLayout(line);
    line->setContentsMargins(0, 0, 0, 0);

    ComponentOptionSingleDouble *o = static_cast<ComponentOptionSingleDouble *>(option());

    defined = new QCheckBox(this);
    line->addWidget(defined);
    defined->setToolTip(tr("Value defined"));
    defined->setStatusTip(tr("Value defined"));
    defined->setWhatsThis(
            tr("This represents the defined state of the option.  When not checked the option is set, but has an undefined value."));

    editor = new QLineEdit(this);
    line->addWidget(editor, 1);
    editor->setToolTip(tr("Option value"));
    editor->setStatusTip(tr("Value"));
    editor->setWhatsThis(tr("This is the value of the option."));
    editor->setEnabled(defined->isChecked());

    if (!o->getAllowUndefined()) {
        defined->setChecked(true);
        defined->setVisible(false);
        editor->setStyleSheet("QLineEdit {background-color: red;}");
    } else {
        defined->setChecked(!o->isSet() || FP::defined(o->get()));
        editor->setEnabled(false);
    }

    connect(editor, SIGNAL(textChanged(
                                   const QString &)), this, SLOT(apply()));
    connect(defined, SIGNAL(toggled(bool)), this, SLOT(apply()));
}

OptionEditorSingleDouble::~OptionEditorSingleDouble()
{ }

void OptionEditorSingleDouble::apply()
{
    double v;
    if (defined->isChecked()) {
        editor->setEnabled(true);
        bool ok = false;
        v = editor->text().toDouble(&ok);
        if (!ok) {
            editor->setStyleSheet("QLineEdit {background-color: red;}");
            return;
        }
    } else {
        v = FP::undefined();
        editor->setEnabled(false);
    }

    ComponentOptionSingleDouble *o = static_cast<ComponentOptionSingleDouble *>(option());
    if (!o->isValid(v)) {
        editor->setStyleSheet("QLineEdit {background-color: red;}");
        return;
    } else {
        editor->setStyleSheet(QString());
    }
    o->set(v);
    emit changed();
}

OptionEditorSingleInteger::OptionEditorSingleInteger(OptionEditor *parent, const QString &id)
        : SingleOptionEditorBase(parent, id)
{
    QWidget *container = new QWidget(this);
    editorLayout->addWidget(container);
    QHBoxLayout *line = new QHBoxLayout(container);
    container->setLayout(line);
    line->setContentsMargins(0, 0, 0, 0);

    ComponentOptionSingleInteger *o = static_cast<ComponentOptionSingleInteger *>(option());

    defined = new QCheckBox(this);
    line->addWidget(defined);
    defined->setToolTip(tr("Value defined"));
    defined->setStatusTip(tr("Value defined"));
    defined->setWhatsThis(
            tr("This represents the defined state of the option.  When not checked the option is set, but has an undefined value."));

    editor = new QLineEdit(this);
    line->addWidget(editor, 1);
    editor->setToolTip(tr("Option value"));
    editor->setStatusTip(tr("Value"));
    editor->setWhatsThis(tr("This is the value of the option."));
    editor->setEnabled(defined->isChecked());

    if (!o->getAllowUndefined()) {
        defined->setChecked(true);
        defined->setVisible(false);
        editor->setStyleSheet("QLineEdit {background-color: red;}");
    } else {
        defined->setChecked(!o->isSet() || FP::defined(o->get()));
        editor->setEnabled(false);
    }

    connect(editor, SIGNAL(textChanged(
                                   const QString &)), this, SLOT(apply()));
    connect(defined, SIGNAL(toggled(bool)), this, SLOT(apply()));
}

OptionEditorSingleInteger::~OptionEditorSingleInteger()
{ }

void OptionEditorSingleInteger::apply()
{
    qint64 v;
    if (defined->isChecked()) {
        editor->setEnabled(true);
        bool ok = false;
        v = editor->text().toLongLong(&ok, 10);
        if (!ok) {
            editor->setStyleSheet("QLineEdit {background-color: red;}");
            return;
        }
    } else {
        v = FP::undefined();
        editor->setEnabled(false);
    }

    ComponentOptionSingleInteger *o = static_cast<ComponentOptionSingleInteger *>(option());
    if (!o->isValid(v)) {
        editor->setStyleSheet("QLineEdit {background-color: red;}");
        return;
    } else {
        editor->setStyleSheet(QString());
    }
    o->set(v);
    emit changed();
}


namespace {
class ListAddRemoveDelegate : public QItemDelegate {
    OptionsEditorList *list;

    class Button : public QPushButton {
    public:
        Button(const QString &text, QWidget *parent = 0) : QPushButton(text, parent)
        { }

        virtual ~Button()
        { }

        virtual QSize sizeHint() const
        {
            QSize textSize(fontMetrics().size(Qt::TextShowMnemonic, text()));
            QStyleOptionButton opt;
            opt.initFrom(this);
            opt.rect.setSize(textSize);
            return style()->sizeFromContents(QStyle::CT_PushButton, &opt, textSize, this);
        }
    };

public:
    ListAddRemoveDelegate(OptionsEditorList *l) : QItemDelegate(l), list(l)
    { }

    virtual ~ListAddRemoveDelegate()
    { }

    virtual QWidget *createEditor(QWidget *parent,
                                  const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const
    {
        Q_UNUSED(option);

        QPushButton *button = new Button(tr("-", "remove text"), parent);
        button->setProperty("model-row", index.row());

        return button;
    }

    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
    {
        QPushButton *button = static_cast<QPushButton *>(editor);

        button->disconnect(list);

        QVariant data(index.data(Qt::EditRole));
        if (data.toInt() != 1) {
            button->setText(tr("-", "remove text"));
            connect(button, SIGNAL(clicked(bool)), list, SLOT(removeRow()));
        } else {
            button->setText(tr("Add", "add text"));
            connect(button, SIGNAL(clicked(bool)), list, SLOT(addRow()));
        }

        button->setProperty("model-row", index.row());
    }

    virtual void setModelData(QWidget *editor,
                              QAbstractItemModel *model,
                              const QModelIndex &index) const
    {
        Q_UNUSED(editor);
        Q_UNUSED(model);
        Q_UNUSED(index);
    }
};
}

OptionsEditorList::OptionsEditorList(QStyledItemDelegate *delegate, QWidget *parent) : QWidget(
        parent)
{
    QVBoxLayout *layout = new QVBoxLayout(this);
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    model = new QStandardItemModel(this);
    table = new QTableView(this);
    table->setModel(model);
    layout->addWidget(table);

    model->setColumnCount(2);
    table->verticalHeader()->hide();
    table->horizontalHeader()->hide();

    table->setItemDelegateForColumn(0, new ListAddRemoveDelegate(this));
    table->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);

    table->horizontalHeader()->setStretchLastSection(true);
    table->setItemDelegateForColumn(1, delegate);

    QList<QStandardItem *> items;
    QStandardItem *item;
    item = new QStandardItem;
    items << item;
    item->setData(1, Qt::EditRole);
    model->insertRow(0, items);
    table->setSpan(0, 0, 1, 2);

    table->openPersistentEditor(model->index(0, 0));

    connect(model, SIGNAL(dataChanged(
                                  const QModelIndex &, const QModelIndex &)), this,
            SIGNAL(changed()));
}

void OptionsEditorList::openAllEditors()
{
    for (int row = 0; row < model->rowCount(); ++row) {
        table->openPersistentEditor(model->index(row, 0));

        if (row == model->rowCount() - 1)
            continue;

        table->openPersistentEditor(model->index(row, 1));
    }
    table->resizeColumnToContents(0);
}

int OptionsEditorList::count() const
{ return model->rowCount() - 1; }

QVariant OptionsEditorList::itemData(int row) const
{ return model->index(row, 1).data(); }

void OptionsEditorList::addItem(const QVariant &data)
{
    QList<QStandardItem *> items;
    QStandardItem *item = new QStandardItem;
    items << item;
    item->setData(0, Qt::EditRole);

    item = new QStandardItem;
    items << item;
    item->setData(data, Qt::EditRole);

    model->insertRow(model->rowCount() - 1, items);
}

void OptionsEditorList::addRow()
{
    QList<QStandardItem *> items;

    QStandardItem *item = new QStandardItem;
    items << item;
    item->setData(0, Qt::EditRole);

    item = new QStandardItem;
    items << item;
    item->setData(QVariant(), Qt::EditRole);

    model->insertRow(model->rowCount() - 1, items);

    openAllEditors();
}

void OptionsEditorList::removeRow()
{
    QObject *s = sender();
    if (!s)
        return;
    QVariant p(s->property("model-row"));
    if (!p.isValid())
        return;
    int row = p.toInt();
    if (row < 0 || row >= model->rowCount() - 1)
        return;

    model->removeRows(row, 1);

    for (row = 0; row < model->rowCount(); ++row) {
        table->closePersistentEditor(model->index(row, 0));
    }

    openAllEditors();

    emit changed();
}


OptionEditorDoubleSetEditor::OptionEditorDoubleSetEditor(SingleOptionEditorBase *opt,
                                                         QWidget *parent) : QLineEdit(parent),
                                                                            option(opt)
{
    setStyleSheet("QLineEdit {background-color: red;}");
    connect(this, SIGNAL(textChanged(
                                 const QString &)), this, SLOT(updated()));
}

void OptionEditorDoubleSetEditor::updated()
{
    bool ok = false;
    double v = text().toDouble(&ok);
    if (!ok || !static_cast<ComponentOptionDoubleSet *>(option->option())->isValid(v)) {
        setStyleSheet("QLineEdit {background-color: red;}");
    } else {
        setStyleSheet(QString());
    }
    emit changed(this);
}

namespace {
class DoubleSetDelegate : public QStyledItemDelegate {
    SingleOptionEditorBase *editor;
public:
    DoubleSetDelegate(SingleOptionEditorBase *e) : QStyledItemDelegate(e), editor(e)
    { }

    virtual ~DoubleSetDelegate()
    { }

    virtual QWidget *createEditor(QWidget *parent,
                                  const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const
    {
        Q_UNUSED(option);
        Q_UNUSED(index);

        OptionEditorDoubleSetEditor *editor = new OptionEditorDoubleSetEditor(this->editor, parent);
        connect(editor, SIGNAL(changed(QWidget * )), this, SIGNAL(commitData(QWidget * )));
        /* Bit of a hack, but I don't see a simple way otherwise */
        editor->setFocus(Qt::OtherFocusReason);

        return editor;
    }

    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
    {
        QString text;
        if (index.data(Qt::EditRole).isValid())
            text = QString::number(index.data().toDouble());
        static_cast<OptionEditorDoubleSetEditor *>(editor)->setText(text);
    }

    virtual void setModelData(QWidget *editor,
                              QAbstractItemModel *model,
                              const QModelIndex &index) const
    {
        bool ok = false;
        double v = static_cast<OptionEditorDoubleSetEditor *>(editor)->text().toDouble(&ok);
        if (ok)
            model->setData(index, v, Qt::EditRole);
    }
};
}

OptionEditorDoubleSet::OptionEditorDoubleSet(OptionEditor *parent, const QString &id)
        : SingleOptionEditorBase(parent, id)
{
    ComponentOptionDoubleSet *o = static_cast<ComponentOptionDoubleSet *>(option());

    editor = new OptionsEditorList(new DoubleSetDelegate(this), this);
    editorLayout->addWidget(editor, 1);

    QList<double> sorted(o->get().values());
    std::sort(sorted.begin(), sorted.end());
    for (QList<double>::const_iterator v = sorted.constBegin(), end = sorted.constEnd();
            v != end;
            ++v) {
        editor->addItem(*v);
    }

    connect(editor, SIGNAL(changed()), this, SLOT(apply()));
}

OptionEditorDoubleSet::~OptionEditorDoubleSet()
{ }

void OptionEditorDoubleSet::apply()
{
    ComponentOptionDoubleSet *o = static_cast<ComponentOptionDoubleSet *>(option());

    QSet<double> values;
    for (int i = 0, max = editor->count(); i < max; i++) {
        double v = editor->itemData(i).toDouble();
        if (!o->isValid(v))
            continue;
        values |= v;
    }
    o->set(values);

    emit changed();
}


OptionEditorDoubleListEditor::OptionEditorDoubleListEditor(SingleOptionEditorBase *opt,
                                                           QWidget *parent) : QLineEdit(parent),
                                                                              option(opt)
{
    setStyleSheet("QLineEdit {background-color: red;}");
    connect(this, SIGNAL(textChanged(
                                 const QString &)), this, SLOT(updated()));
}

void OptionEditorDoubleListEditor::updated()
{
    bool ok = false;
    double v = text().toDouble(&ok);
    if (!ok || !FP::defined(v)) {
        setStyleSheet("QLineEdit {background-color: red;}");
    } else {
        setStyleSheet(QString());
    }
    emit changed(this);
}

namespace {
class DoubleListDelegate : public QStyledItemDelegate {
    SingleOptionEditorBase *editor;
public:
    DoubleListDelegate(SingleOptionEditorBase *e) : QStyledItemDelegate(e), editor(e)
    { }

    virtual ~DoubleListDelegate()
    { }

    virtual QWidget *createEditor(QWidget *parent,
                                  const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const
    {
        Q_UNUSED(option);
        Q_UNUSED(index);

        OptionEditorDoubleListEditor
                *editor = new OptionEditorDoubleListEditor(this->editor, parent);
        connect(editor, SIGNAL(changed(QWidget * )), this, SIGNAL(commitData(QWidget * )));
        /* Bit of a hack, but I don't see a simple way otherwise */
        editor->setFocus(Qt::OtherFocusReason);

        return editor;
    }

    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
    {
        QString text;
        if (index.data(Qt::EditRole).isValid())
            text = QString::number(index.data().toDouble());
        static_cast<OptionEditorDoubleListEditor *>(editor)->setText(text);
    }

    virtual void setModelData(QWidget *editor,
                              QAbstractItemModel *model,
                              const QModelIndex &index) const
    {
        bool ok = false;
        double v = static_cast<OptionEditorDoubleListEditor *>(editor)->text().toDouble(&ok);
        if (ok)
            model->setData(index, v, Qt::EditRole);
    }
};
}

OptionEditorDoubleList::OptionEditorDoubleList(OptionEditor *parent, const QString &id)
        : SingleOptionEditorBase(parent, id)
{
    ComponentOptionDoubleList *o = static_cast<ComponentOptionDoubleList *>(option());

    editor = new OptionsEditorList(new DoubleListDelegate(this), this);
    editorLayout->addWidget(editor, 1);

    QList<double> values(o->get());
    for (QList<double>::const_iterator v = values.constBegin(), end = values.constEnd();
            v != end;
            ++v) {
        editor->addItem(*v);
    }

    connect(editor, SIGNAL(changed()), this, SLOT(apply()));
}

OptionEditorDoubleList::~OptionEditorDoubleList()
{ }

void OptionEditorDoubleList::apply()
{
    ComponentOptionDoubleList *o = static_cast<ComponentOptionDoubleList *>(option());

    QList<double> values;
    for (int i = 0, max = editor->count(); i < max; i++) {
        double v = editor->itemData(i).toDouble();
        if (!FP::defined(v))
            break;
        values.append(v);
    }
    if (values.size() >= o->getMinimumComponents()) {
        o->set(values);
        setLabel(true);
    } else {
        setLabel(false);
    }

    emit changed();
}


OptionEditorStringSetEditor::OptionEditorStringSetEditor(SingleOptionEditorBase *opt,
                                                         QWidget *parent) : QLineEdit(parent),
                                                                            option(opt)
{
    connect(this, SIGNAL(textChanged(
                                 const QString &)), this, SLOT(updated()));
}

void OptionEditorStringSetEditor::updated()
{
    emit changed(this);
}

namespace {
class StringSetDelegate : public QStyledItemDelegate {
    SingleOptionEditorBase *editor;
public:
    StringSetDelegate(SingleOptionEditorBase *e) : QStyledItemDelegate(e), editor(e)
    { }

    virtual ~StringSetDelegate()
    { }

    virtual QWidget *createEditor(QWidget *parent,
                                  const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const
    {
        Q_UNUSED(option);
        Q_UNUSED(index);

        OptionEditorStringSetEditor *editor = new OptionEditorStringSetEditor(this->editor, parent);
        connect(editor, SIGNAL(changed(QWidget * )), this, SIGNAL(commitData(QWidget * )));
        /* Bit of a hack, but I don't see a simple way otherwise */
        editor->setFocus(Qt::OtherFocusReason);

        return editor;
    }

    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
    {
        static_cast<OptionEditorStringSetEditor *>(editor)->setText(index.data().toString());
    }

    virtual void setModelData(QWidget *editor,
                              QAbstractItemModel *model,
                              const QModelIndex &index) const
    {
        model->setData(index, static_cast<OptionEditorStringSetEditor *>(
                editor)->text(), Qt::EditRole);
    }
};
}

OptionEditorStringSet::OptionEditorStringSet(OptionEditor *parent, const QString &id)
        : SingleOptionEditorBase(parent, id)
{
    ComponentOptionStringSet *o = static_cast<ComponentOptionStringSet *>(option());

    editor = new OptionsEditorList(new StringSetDelegate(this), this);
    editorLayout->addWidget(editor, 1);

    QStringList sorted(o->get().values());
    std::sort(sorted.begin(), sorted.end());
    for (QList<QString>::const_iterator v = sorted.constBegin(), end = sorted.constEnd();
            v != end;
            ++v) {
        editor->addItem(*v);
    }

    connect(editor, SIGNAL(changed()), this, SLOT(apply()));
}

OptionEditorStringSet::~OptionEditorStringSet()
{ }

void OptionEditorStringSet::apply()
{
    ComponentOptionStringSet *o = static_cast<ComponentOptionStringSet *>(option());

    QSet<QString> values;
    for (int i = 0, max = editor->count(); i < max; i++) {
        values |= editor->itemData(i).toString();
    }
    o->set(values);

    emit changed();
}


OptionEditorInstrumentSuffixSet::OptionEditorInstrumentSuffixSet(OptionEditor *parent,
                                                                 const QString &id)
        : SingleOptionEditorBase(parent, id)
{
    select = new QPushButton(this);
    editorLayout->addWidget(select);
    select->setToolTip(tr("Press to select suffixes"));
    select->setStatusTip(tr("Instrument suffixes"));
    select->setWhatsThis(
            tr("Use this button to open a list of available instrument suffixes to select."));

    suffixMenu = NULL;
    connect(optionEditor(), SIGNAL(availableChanged()), this, SLOT(rebuildAvailable()));
    rebuildAvailable();

    setButtonLabel();
}

OptionEditorInstrumentSuffixSet::~OptionEditorInstrumentSuffixSet()
{ }

void OptionEditorInstrumentSuffixSet::rebuildAvailable()
{
    QSet<QString> suffixes;
    for (const auto &var : optionEditor()->getAvailableVariables()) {
        auto s = Util::suffix(var, '_');
        if (s.empty())
            continue;
        if (!std::isupper(s.front()))
            continue;
        suffixes |= QString::fromStdString(s);
    }
    QSet<QString> selected(static_cast<ComponentOptionInstrumentSuffixSet *>(
                                   option())->get());
    suffixes |= selected;
    QStringList sorted(suffixes.values());
    std::sort(sorted.begin(), sorted.end());

    if (suffixMenu != NULL)
        suffixMenu->deleteLater();
    suffixActions.clear();

    suffixMenu = new QMenu(select);
    for (QList<QString>::const_iterator suffix = sorted.constBegin(), end = sorted.constEnd();
            suffix != end;
            ++suffix) {
        QAction *action = new QAction(*suffix, suffixMenu);
        suffixMenu->addAction(action);
        suffixActions.insert(*suffix, action);
        action->setCheckable(true);
        action->setChecked(selected.contains(*suffix));

        connect(action, SIGNAL(triggered(bool)), this, SLOT(apply()));
    }

    delimiterAction = suffixMenu->addSeparator();

    QAction *add = suffixMenu->addAction(tr("Add Suffix"));
    connect(add, SIGNAL(triggered(bool)), this, SLOT(addSuffix()));

    if (!suffixActions.isEmpty()) {
        select->setMenu(suffixMenu);
    } else {
        connect(select, SIGNAL(clicked(bool)), this, SLOT(addSuffix()));
    }
}

void OptionEditorInstrumentSuffixSet::setButtonLabel()
{
    if (suffixActions.isEmpty()) {
        select->setText(tr("Add Suffix"));
        return;
    }

    QSet<QString> values;
    for (QHash<QString, QAction *>::const_iterator add = suffixActions.constBegin(),
            end = suffixActions.constEnd(); add != end; ++add) {
        if (!add.value()->isChecked())
            continue;
        values |= add.key();
    }
    QStringList sorted(values.values());
    if (sorted.isEmpty()) {
        select->setText(tr("Select Suffixes"));
        return;
    }

    std::sort(sorted.begin(), sorted.end());

    if (sorted.size() > 5) {
        sorted.erase(sorted.begin() + 2, sorted.end() - 2);
        sorted.insert(sorted.begin() + 2, tr("...", "removed items"));
    }

    select->setText(sorted.join(","));
}

void OptionEditorInstrumentSuffixSet::apply()
{
    ComponentOptionInstrumentSuffixSet
            *o = static_cast<ComponentOptionInstrumentSuffixSet *>(option());

    QSet<QString> values;
    for (QHash<QString, QAction *>::const_iterator add = suffixActions.constBegin(),
            end = suffixActions.constEnd(); add != end; ++add) {
        if (!add.value()->isChecked())
            continue;
        values |= add.key();
    }
    o->set(values);

    setButtonLabel();

    emit changed();
}

void OptionEditorInstrumentSuffixSet::addSuffix()
{
    bool ok;
    QString suffix
            (QInputDialog::getText(this, tr("Add Suffix"), tr("Add suffix:"), QLineEdit::Normal,
                                   QString(), &ok));
    if (!ok || suffix.isEmpty())
        return;

    QHash<QString, QAction *>::const_iterator check = suffixActions.constFind(suffix);
    if (check != suffixActions.constEnd()) {
        check.value()->setChecked(true);
        return;
    }

    QAction *action = new QAction(suffix, suffixMenu);
    suffixMenu->insertAction(delimiterAction, action);
    suffixActions.insert(suffix, action);

    action->setCheckable(true);
    action->setChecked(true);

    select->disconnect(this);
    select->setMenu(suffixMenu);

    apply();
}


OptionEditorTimeOffset::OptionEditorTimeOffset(OptionEditor *parent, const QString &id)
        : SingleOptionEditorBase(parent, id)
{
    ComponentOptionTimeOffset *o = static_cast<ComponentOptionTimeOffset *>(option());

    editor = new TimeIntervalSelection(this);
    editorLayout->addWidget(editor);
    editor->setToolTip(tr("Option value"));
    editor->setStatusTip(tr("Value"));
    editor->setWhatsThis(tr("This is the value of the option."));

    if (o->isSet()) {
        editor->setUnit(o->getUnit());
        editor->setCount(o->getCount());
        editor->setAlign(o->getAlign());
    } else {
        editor->setUnit(o->getDefaultUnit());
        editor->setCount(o->getDefaultCount());
        editor->setAlign(o->getDefaultAlign());
    }

    editor->setAllowZero(o->getAllowZero());
    editor->setAllowNegative(o->getAllowNegative());

    connect(editor, SIGNAL(intervalChanged(CPD3::Time::LogicalTimeUnit, int, bool)), this,
            SLOT(apply()));
}

OptionEditorTimeOffset::~OptionEditorTimeOffset()
{ }

void OptionEditorTimeOffset::apply()
{
    static_cast<ComponentOptionTimeOffset *>(option())->set(editor->getUnit(), editor->getCount(),
                                                            editor->getAlign());
    emit changed();
}

OptionEditorSingleTime::OptionEditorSingleTime(OptionEditor *parent, const QString &id)
        : SingleOptionEditorBase(parent, id)
{
    ComponentOptionSingleTime *o = static_cast<ComponentOptionSingleTime *>(option());

    editor = new TimeSingleSelection(this);
    editorLayout->addWidget(editor);
    editor->setToolTip(tr("Option value"));
    editor->setStatusTip(tr("Value"));
    editor->setWhatsThis(tr("This is the value of the option."));
    if (o->isSet())
        editor->setTime(o->get());
    editor->setAllowUndefined(o->getAllowUndefined());

    connect(editor, SIGNAL(changed(double)), this, SLOT(apply()));
}

OptionEditorSingleTime::~OptionEditorSingleTime()
{ }

void OptionEditorSingleTime::apply()
{
    static_cast<ComponentOptionSingleTime *>(option())->set(editor->getTime());
    emit changed();
}

OptionEditorSingleCalibration::OptionEditorSingleCalibration(OptionEditor *parent,
                                                             const QString &id)
        : SingleOptionEditorBase(parent, id)
{
    ComponentOptionSingleCalibration *o = static_cast<ComponentOptionSingleCalibration *>(option());

    editor = new CalibrationEdit(this);
    editorLayout->addWidget(editor);
    if (o->isSet())
        editor->setCalibration(o->get());

    connect(editor, SIGNAL(calibrationChanged(
                                   const CPD3::Calibration &)), this, SLOT(apply()));
}

OptionEditorSingleCalibration::~OptionEditorSingleCalibration()
{ }

void OptionEditorSingleCalibration::apply()
{
    ComponentOptionSingleCalibration *o = static_cast<ComponentOptionSingleCalibration *>(option());

    Calibration cal(editor->getCalibration());
    if (!o->isValid(cal)) {
        editor->setStyleSheet("QLineEdit {background-color: red;}");
        return;
    }

    editor->setStyleSheet(QString());

    o->set(cal);
    emit changed();
}

OptionEditorSequenceMatch::OptionEditorSequenceMatch(OptionEditor *parent, const QString &id)
        : SingleOptionEditorBase(parent, id)
{
    editor = new VariableSelect(this);
    editorLayout->addWidget(editor);
    registerVariableSelect(editor);

    connect(editor, SIGNAL(selectionChanged()), this, SLOT(apply()));
}

OptionEditorSequenceMatch::~OptionEditorSequenceMatch()
{ }

void OptionEditorSequenceMatch::apply()
{
    QString config;
    editor->writeSequenceMatch(config, optionEditor()->getVariableSelectionDefaults());

    ComponentOptionSequenceMatch *o = static_cast<ComponentOptionSequenceMatch *>(option());
    o->set(config);
    emit changed();
}


OptionEditorTimeBlock::OptionEditorTimeBlock(OptionEditorTimeDependent *p) : parent(p)
{ }

OptionEditorTimeBlock::~OptionEditorTimeBlock()
{ }

ComponentOptionBase *OptionEditorTimeBlock::option() const
{ return parent->option(); }

OptionEditor *OptionEditorTimeBlock::optionEditor() const
{ return parent->optionEditor(); }


OptionEditorTimeDependent::Interval::Interval(OptionEditorTimeBlock *e, const Time::Bounds &range)
        : Time::Bounds(range), editor(e)
{ }

OptionEditorTimeDependent::OptionEditorTimeDependent(OptionEditor *parent, const QString &id)
        : SingleOptionEditorBase(parent, id),
          intervals(),
          selectedInterval(0),
          settings(CPD3GUI_ORGANIZATION, CPD3GUI_APPLICATION)
{
    timeSelect = new QPushButton(this);
    timeSelect->setToolTip(tr("Time range operations."));
    timeSelect->setStatusTip(tr("Time range"));
    timeSelect->setWhatsThis(
            tr("Use this button to change the time range being operated on.  Overlapping time ranges are not permitted and will be flattened."));
    editorLayout->addWidget(timeSelect, 0);
    timeSelectMenu = NULL;

    QWidget *area = new QWidget(this);
    intervalEditorArea = new QVBoxLayout(area);
    area->setLayout(intervalEditorArea);
    intervalEditorArea->setContentsMargins(0, 0, 0, 0);
    editorLayout->addWidget(area, 1);

    QMetaObject::invokeMethod(this, "initialize", Qt::QueuedConnection);
}

void OptionEditorTimeDependent::initialize()
{
    intervals.clear();
    intervals.append(Interval(createBlock()));
    intervalEditorArea->addWidget(intervals.last().editor, 1);
    connect(intervals.last().editor, SIGNAL(changed()), this, SLOT(apply()));
    selectedInterval = 0;

    buildMenu();
    showSelected();
}

OptionEditorTimeDependent::~OptionEditorTimeDependent()
{ }

void OptionEditorTimeDependent::apply()
{
    clear();
    for (QList<Interval>::const_iterator i = intervals.constBegin(), endI = intervals.constEnd();
            i != endI;
            ++i) {
        i->editor->apply(i->getStart(), i->getEnd());
    }
    emit changed();
}

bool OptionEditorTimeDependent::selectTimes(Time::Bounds &range)
{
    QDialog dlg(this);
    dlg.setModal(true);
    dlg.setWindowTitle(tr("Add Time Interval"));
    QVBoxLayout *topLayout = new QVBoxLayout;
    dlg.setLayout(topLayout);

    TimeBoundSelection *selection = new TimeBoundSelection(&dlg);
    topLayout->addWidget(selection);
    selection->setAllowUndefinedStart(true);
    selection->setAllowUndefinedEnd(true);
    selection->setAllowZeroSize(false);
    selection->setBounds(range.getStart(), range.getEnd());
    selection->setToolTip(tr("The range of time the selection is applied to."));
    selection->setStatusTip(tr("Time range"));
    selection->setWhatsThis(
            tr("This is the range of time the selected value will be in effect for."));

    QDialogButtonBox *buttons = new QDialogButtonBox(&dlg);
    topLayout->addWidget(buttons);
    connect(buttons, SIGNAL(accepted()), &dlg, SLOT(accept()));
    connect(buttons, SIGNAL(rejected()), &dlg, SLOT(reject()));
    buttons->addButton(QDialogButtonBox::Ok);
    buttons->addButton(QDialogButtonBox::Cancel);

    dlg.exec();
    if (dlg.result() != QDialog::Accepted)
        return false;

    range.setStart(selection->getStart());
    range.setEnd(selection->getEnd());
    return true;
}

void OptionEditorTimeDependent::insertInterval(const Interval &ins)
{
    QList<Interval>::iterator
            position = Range::lowerBound(intervals.begin(), intervals.end(), ins.getStart());
    position = intervals.insert(position, ins);
    if (position != intervals.begin()) {
        if (Range::compareStartEnd(position->getStart(), (position - 1)->getEnd()) < 0) {
            Q_ASSERT(Range::compareStart((position - 1)->getStart(), position->getStart()) < 0);
            (position - 1)->setEnd(position->getStart());
        }
    }
    selectedInterval = position - intervals.begin();
    intervalEditorArea->addWidget(position->editor, 1);
    for (++position; position != intervals.end();) {
        if (Range::compareEnd(position->getEnd(), ins.getEnd()) <= 0) {
            intervalEditorArea->removeWidget(position->editor);
            position->editor->hide();
            position->editor->deleteLater();
            position = intervals.erase(position);
            continue;
        }

        if (Range::compareStartEnd(position->getStart(), ins.getEnd()) < 0) {
            position->setStart(ins.getEnd());
        }

        break;
    }
    buildMenu();
    showSelected();
    apply();
}

void OptionEditorTimeDependent::addTime()
{
    Time::Bounds range;
    if (!selectTimes(range))
        return;

    Interval ins(createBlock(), range);
    connect(ins.editor, SIGNAL(changed()), this, SLOT(apply()));
    insertInterval(ins);
}

void OptionEditorTimeDependent::modifyInterval()
{
    if (selectedInterval < 0 || selectedInterval >= intervals.size())
        return;

    Time::Bounds range(intervals.at(selectedInterval));
    if (!selectTimes(range))
        return;
    if (FP::equal(range.getStart(), intervals.at(selectedInterval).getStart()) &&
            FP::equal(range.getEnd(), intervals.at(selectedInterval).getEnd()))
        return;

    Interval ins(intervals.takeAt(selectedInterval));
    ins.setStart(range.getStart());
    ins.setEnd(range.getEnd());
    insertInterval(ins);
}

void OptionEditorTimeDependent::removeInterval()
{
    if (selectedInterval < 0 || selectedInterval >= intervals.size())
        return;
    OptionEditorTimeBlock *editor = intervals.takeAt(selectedInterval).editor;
    intervalEditorArea->removeWidget(editor);
    editor->hide();
    editor->deleteLater();

    if (!intervals.isEmpty()) {
        if (selectedInterval >= intervals.size())
            selectedInterval = intervals.size() - 1;
    } else {
        selectedInterval = 0;
    }

    buildMenu();
    showSelected();
    apply();
}

void OptionEditorTimeDependent::buildMenu()
{
    if (timeSelectMenu != NULL)
        timeSelectMenu->deleteLater();
    timeSelectMenu = NULL;

    if (intervals.isEmpty()) {
        timeSelect->setText(tr("Add Defined Time"));
    } else {
        Q_ASSERT(selectedInterval >= 0 && selectedInterval < intervals.size());

        timeSelectMenu = new QMenu(timeSelect);

        if (intervals.size() > 1) {
            QActionGroup *group = new QActionGroup(timeSelectMenu);
            group->setExclusive(true);

            int pos = 0;
            for (QList<Interval>::const_iterator i = intervals.constBegin(),
                    endI = intervals.constEnd(); i != endI; ++i, ++pos) {
                QAction *action = timeSelectMenu->addAction(
                        GUITime::formatRange(i->getStart(), i->getEnd(), &settings));
                action->setCheckable(true);
                action->setChecked(pos == selectedInterval);
                action->setProperty("intervalIndex", pos);
                group->addAction(action);
            }

            connect(group, SIGNAL(triggered(QAction * )), this, SLOT(selectTimeRange(QAction * )));

            timeSelectMenu->addSeparator();
        }

        timeSelectMenu->addAction(tr("&Add Time Range"), this, SLOT(addTime()));
        timeSelectMenu->addAction(tr("&Remove Selected Range"), this, SLOT(removeInterval()));
        timeSelectMenu->addAction(tr("&Modify Selected Range"), this, SLOT(modifyInterval()));
    }

    timeSelect->disconnect(this);
    timeSelect->setMenu(timeSelectMenu);
    if (timeSelectMenu == NULL)
        connect(timeSelect, SIGNAL(clicked(bool)), this, SLOT(addTime()));
}

void OptionEditorTimeDependent::showSelected()
{
    if (intervals.isEmpty())
        return;
    Q_ASSERT(selectedInterval >= 0 && selectedInterval < intervals.size());

    if (!FP::defined(intervals.at(selectedInterval).getStart()) &&
            !FP::defined(intervals.at(selectedInterval).getEnd())) {
        timeSelect->setText(tr("All Time"));
    } else {
        timeSelect->setText(GUITime::formatRange(intervals.at(selectedInterval).getStart(),
                                                 intervals.at(selectedInterval).getEnd(),
                                                 &settings));
    }

    int pos = 0;
    for (QList<Interval>::const_iterator i = intervals.constBegin(), endI = intervals.constEnd();
            i != endI;
            ++i, ++pos) {
        i->editor->setVisible(pos == selectedInterval);
    }
}

void OptionEditorTimeDependent::selectTimeRange(QAction *action)
{
    int idx = action->property("intervalIndex").toInt();
    if (idx < 0 || idx >= intervals.size())
        return;
    selectedInterval = idx;
    showSelected();
}


OptionEditorTimeInputString::OptionEditorTimeInputString(OptionEditor *parent, const QString &id)
        : OptionEditorTimeDependent(parent, id)
{ }

OptionEditorTimeInputString::~OptionEditorTimeInputString()
{ }

OptionEditorTimeBlock *OptionEditorTimeInputString::createBlock()
{ return new Editor(this); }

void OptionEditorTimeInputString::clear()
{ static_cast<DynamicStringOption *>(option())->clear(); }

OptionEditorTimeInputString::Editor::Editor(OptionEditorTimeDependent *parent)
        : OptionEditorTimeBlock(parent)
{
    QVBoxLayout *layout = new QVBoxLayout(this);
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    editor = new QPlainTextEdit(this);
    layout->addWidget(editor);
    {
        QMargins margins(editor->contentsMargins());
        QFontMetrics fm(editor->font());
        editor->setFixedHeight(margins.top() +
                                       margins.bottom() +
                                       editor->document()->documentMargin() +
                                       fm.height() +
                                       5);
    }

    connect(editor, SIGNAL(textChanged()), this, SIGNAL(changed()));
}

OptionEditorTimeInputString::Editor::~Editor()
{ }

void OptionEditorTimeInputString::Editor::apply(double start, double end)
{
    static_cast<DynamicStringOption *>(option())->set(editor->toPlainText(), start, end);
}

OptionEditorTimeInputBool::OptionEditorTimeInputBool(OptionEditor *parent, const QString &id)
        : OptionEditorTimeDependent(parent, id)
{ }

OptionEditorTimeInputBool::~OptionEditorTimeInputBool()
{ }

OptionEditorTimeBlock *OptionEditorTimeInputBool::createBlock()
{ return new Editor(this); }

void OptionEditorTimeInputBool::clear()
{ static_cast<DynamicBoolOption *>(option())->clear(); }

OptionEditorTimeInputBool::Editor::Editor(OptionEditorTimeDependent *parent)
        : OptionEditorTimeBlock(parent)
{
    QVBoxLayout *layout = new QVBoxLayout(this);
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    editor = new QCheckBox(tr("Option set"), this);
    editor->setToolTip(tr("The option set state"));
    editor->setStatusTip(tr("Option set"));
    editor->setWhatsThis(
            tr("This indicates if the option is set to true.  The option can be set but false when this is unchecked.  This is used to override a default true behavior."));
    layout->addWidget(editor);

    connect(editor, SIGNAL(toggled(bool)), this, SIGNAL(changed()));
}

OptionEditorTimeInputBool::Editor::~Editor()
{ }

void OptionEditorTimeInputBool::Editor::apply(double start, double end)
{
    static_cast<DynamicBoolOption *>(option())->set(editor->isChecked(), start, end);
}

OptionEditorTimeInputDouble::OptionEditorTimeInputDouble(OptionEditor *parent, const QString &id)
        : OptionEditorTimeDependent(parent, id)
{ }

OptionEditorTimeInputDouble::~OptionEditorTimeInputDouble()
{ }

OptionEditorTimeBlock *OptionEditorTimeInputDouble::createBlock()
{ return new Editor(this); }

void OptionEditorTimeInputDouble::clear()
{ static_cast<DynamicDoubleOption *>(option())->clear(); }

OptionEditorTimeInputDouble::Editor::Editor(OptionEditorTimeDependent *parent)
        : OptionEditorTimeBlock(parent)
{
    QHBoxLayout *line = new QHBoxLayout(this);
    setLayout(line);
    line->setContentsMargins(0, 0, 0, 0);

    DynamicDoubleOption *o = static_cast<DynamicDoubleOption *>(option());

    defined = new QCheckBox(this);
    line->addWidget(defined);
    defined->setToolTip(tr("Value defined"));
    defined->setStatusTip(tr("Value defined"));
    defined->setWhatsThis(
            tr("This represents the defined state of the option.  When not checked the option is set, but has an undefined value."));

    editor = new QLineEdit(this);
    line->addWidget(editor, 1);
    editor->setToolTip(tr("Option value"));
    editor->setStatusTip(tr("Value"));
    editor->setWhatsThis(tr("This is the value of the option."));
    editor->setEnabled(defined->isChecked());

    defined->setChecked(true);
    if (!o->getAllowUndefined()) {
        defined->setVisible(false);
        editor->setStyleSheet("QLineEdit {background-color: red;}");
    } else {
        editor->setEnabled(false);
    }

    connect(editor, SIGNAL(textChanged(
                                   const QString &)), this, SIGNAL(changed()));
    connect(defined, SIGNAL(toggled(bool)), this, SIGNAL(changed()));
}

OptionEditorTimeInputDouble::Editor::~Editor()
{ }

void OptionEditorTimeInputDouble::Editor::apply(double start, double end)
{
    double v;
    if (defined->isChecked()) {
        editor->setEnabled(true);
        bool ok = false;
        v = editor->text().toDouble(&ok);
        if (!ok) {
            editor->setStyleSheet("QLineEdit {background-color: red;}");
            return;
        }
    } else {
        v = FP::undefined();
        editor->setEnabled(false);
    }

    DynamicDoubleOption *o = static_cast<DynamicDoubleOption *>(option());
    if (!o->isValid(v)) {
        editor->setStyleSheet("QLineEdit {background-color: red;}");
        return;
    } else {
        editor->setStyleSheet(QString());
    }

    o->set(v, start, end);
}

OptionEditorTimeInputInteger::OptionEditorTimeInputInteger(OptionEditor *parent, const QString &id)
        : OptionEditorTimeDependent(parent, id)
{ }

OptionEditorTimeInputInteger::~OptionEditorTimeInputInteger()
{ }

OptionEditorTimeBlock *OptionEditorTimeInputInteger::createBlock()
{ return new Editor(this); }

void OptionEditorTimeInputInteger::clear()
{ static_cast<DynamicIntegerOption *>(option())->clear(); }

OptionEditorTimeInputInteger::Editor::Editor(OptionEditorTimeDependent *parent)
        : OptionEditorTimeBlock(parent)
{
    QHBoxLayout *line = new QHBoxLayout(this);
    setLayout(line);
    line->setContentsMargins(0, 0, 0, 0);

    DynamicIntegerOption *o = static_cast<DynamicIntegerOption *>(option());

    defined = new QCheckBox(this);
    line->addWidget(defined);
    defined->setToolTip(tr("Value defined"));
    defined->setStatusTip(tr("Value defined"));
    defined->setWhatsThis(
            tr("This represents the defined state of the option.  When not checked the option is set, but has an undefined value."));

    editor = new QLineEdit(this);
    line->addWidget(editor, 1);
    editor->setToolTip(tr("Option value"));
    editor->setStatusTip(tr("Value"));
    editor->setWhatsThis(tr("This is the value of the option."));
    editor->setEnabled(defined->isChecked());

    defined->setChecked(true);
    if (!o->getAllowUndefined()) {
        defined->setVisible(false);
        editor->setStyleSheet("QLineEdit {background-color: red;}");
    } else {
        editor->setEnabled(false);
    }

    connect(editor, SIGNAL(textChanged(
                                   const QString &)), this, SIGNAL(changed()));
    connect(defined, SIGNAL(toggled(bool)), this, SIGNAL(changed()));
}

OptionEditorTimeInputInteger::Editor::~Editor()
{ }

void OptionEditorTimeInputInteger::Editor::apply(double start, double end)
{
    qint64 v;
    if (defined->isChecked()) {
        editor->setEnabled(true);
        bool ok = false;
        v = editor->text().toLongLong(&ok, 10);
        if (!ok) {
            editor->setStyleSheet("QLineEdit {background-color: red;}");
            return;
        }
    } else {
        v = FP::undefined();
        editor->setEnabled(false);
    }

    DynamicIntegerOption *o = static_cast<DynamicIntegerOption *>(option());
    if (!o->isValid(v)) {
        editor->setStyleSheet("QLineEdit {background-color: red;}");
        return;
    } else {
        editor->setStyleSheet(QString());
    }

    o->set(v, start, end);
}

OptionEditorTimeInputCalibration::OptionEditorTimeInputCalibration(OptionEditor *parent,
                                                                   const QString &id)
        : OptionEditorTimeDependent(parent, id)
{ }

OptionEditorTimeInputCalibration::~OptionEditorTimeInputCalibration()
{ }

OptionEditorTimeBlock *OptionEditorTimeInputCalibration::createBlock()
{ return new Editor(this); }

void OptionEditorTimeInputCalibration::clear()
{ static_cast<DynamicCalibrationOption *>(option())->clear(); }

OptionEditorTimeInputCalibration::Editor::Editor(OptionEditorTimeDependent *parent)
        : OptionEditorTimeBlock(parent)
{
    QVBoxLayout *layout = new QVBoxLayout(this);
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    editor = new CalibrationEdit(this);
    layout->addWidget(editor);

    connect(editor, SIGNAL(calibrationChanged(
                                   const CPD3::Calibration &)), this, SIGNAL(changed()));
}

OptionEditorTimeInputCalibration::Editor::~Editor()
{ }

void OptionEditorTimeInputCalibration::Editor::apply(double start, double end)
{
    DynamicCalibrationOption *o = static_cast<DynamicCalibrationOption *>(option());

    Calibration cal(editor->getCalibration());
    if (!o->isValid(cal)) {
        editor->setStyleSheet("QLineEdit {background-color: red;}");
        return;
    }
    editor->setStyleSheet(QString());

    o->set(cal, start, end);
}

OptionEditorTimeIntervalSelection::OptionEditorTimeIntervalSelection(OptionEditor *parent,
                                                                     const QString &id)
        : OptionEditorTimeDependent(parent, id)
{ }

OptionEditorTimeIntervalSelection::~OptionEditorTimeIntervalSelection()
{ }

OptionEditorTimeBlock *OptionEditorTimeIntervalSelection::createBlock()
{ return new Editor(this); }

void OptionEditorTimeIntervalSelection::clear()
{ static_cast<TimeIntervalSelectionOption *>(option())->clear(); }

OptionEditorTimeIntervalSelection::Editor::Editor(OptionEditorTimeDependent *parent)
        : OptionEditorTimeBlock(parent)
{
    QHBoxLayout *layout = new QHBoxLayout(this);
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    TimeIntervalSelectionOption *o = static_cast<TimeIntervalSelectionOption *>(option());

    defined = new QCheckBox(tr("Defined:"), this);
    layout->addWidget(defined);

    editor = new TimeIntervalSelection(this);
    layout->addWidget(editor, 1);

    if (!o->getAllowUndefined()) {
        defined->setChecked(true);
        defined->setVisible(false);
    } else {
        defined->setChecked(false);
        editor->setEnabled(false);
    }
    editor->setAlign(o->getDefaultAligned());
    editor->setAllowNegative(o->getAllowNegative());
    editor->setAllowZero(o->getAllowZero());

    connect(editor, SIGNAL(intervalChanged(CPD3::Time::LogicalTimeUnit, int, bool)), this,
            SIGNAL(changed()));
    connect(defined, SIGNAL(toggled(bool)), this, SIGNAL(changed()));
}

OptionEditorTimeIntervalSelection::Editor::~Editor()
{ }

void OptionEditorTimeIntervalSelection::Editor::apply(double start, double end)
{
    TimeIntervalSelectionOption *o = static_cast<TimeIntervalSelectionOption *>(option());
    if (defined->isChecked()) {
        editor->setEnabled(true);
        o->set(start, end, editor->getUnit(), editor->getCount(), editor->getAlign());
    } else {
        editor->setEnabled(false);
        o->setUndefined(start, end);
    }
}

OptionEditorFilterOperate::OptionEditorFilterOperate(OptionEditor *parent, const QString &id)
        : OptionEditorTimeDependent(parent, id)
{ }

OptionEditorFilterOperate::~OptionEditorFilterOperate()
{ }

OptionEditorTimeBlock *OptionEditorFilterOperate::createBlock()
{ return new Editor(this); }

void OptionEditorFilterOperate::clear()
{ static_cast<DynamicSequenceSelectionOption *>(option())->clear(); }

OptionEditorFilterOperate::Editor::Editor(OptionEditorTimeDependent *parent)
        : OptionEditorTimeBlock(parent)
{
    QVBoxLayout *layout = new QVBoxLayout(this);
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    editor = new VariableSelect(this);
    layout->addWidget(editor);
    parent->registerVariableSelect(editor);

    connect(editor, SIGNAL(selectionChanged()), this, SIGNAL(changed()));
}

OptionEditorFilterOperate::Editor::~Editor()
{ }

void OptionEditorFilterOperate::Editor::apply(double start, double end)
{
    Variant::Write config = Variant::Write::empty();
    editor->writeDynamicSelection(config);
    static_cast<DynamicSequenceSelectionOption *>(option())->set(start, end, config);
}

OptionEditorFilterInput::OptionEditorFilterInput(OptionEditor *parent, const QString &id)
        : OptionEditorTimeDependent(parent, id)
{ }

OptionEditorFilterInput::~OptionEditorFilterInput()
{ }

OptionEditorTimeBlock *OptionEditorFilterInput::createBlock()
{ return new Editor(this); }

void OptionEditorFilterInput::clear()
{ static_cast<DynamicInputOption *>(option())->clear(); }

OptionEditorFilterInput::Editor::Editor(OptionEditorTimeDependent *parent) : OptionEditorTimeBlock(
        parent)
{
    QVBoxLayout *layout = new QVBoxLayout(this);
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    {
        QWidget *container = new QWidget(this);
        layout->addWidget(container);
        QHBoxLayout *line = new QHBoxLayout(container);
        container->setLayout(line);
        line->setContentsMargins(0, 0, 0, 0);

        constantMode = new QComboBox(container);
        line->addWidget(constantMode);
        constantMode->setToolTip(tr("Select the order of operation of the constant"));
        constantMode->setStatusTip(tr("Constant value mode"));
        constantMode->setWhatsThis(
                tr("This selects how the constant value, if any, is interpreted compared to the dynamic input."));
        constantMode->addItem(tr("Dynamic"), ConstantMode_Disabled);
        constantMode->addItem(tr("Fallback"), ConstantMode_Fallback);
        constantMode->addItem(tr("Constant"), ConstantMode_Primary);
        constantMode->addItem(tr("Undefined"), ConstantMode_Undefined);

        constantValue = new QLineEdit(container);
        line->addWidget(constantValue, 1);
        constantValue->setToolTip(tr("Constant value"));
        constantValue->setStatusTip(tr("Value"));
        constantValue->setWhatsThis(tr("This is the constant value of the option."));
        constantValue->setEnabled(false);

        connect(constantMode, SIGNAL(currentIndexChanged(int)), this, SIGNAL(changed()));
        connect(constantValue, SIGNAL(textChanged(
                                              const QString &)), this, SIGNAL(changed()));
    }

    selection = new VariableSelect(this);
    layout->addWidget(selection, 1);
    parent->registerVariableSelect(selection);
    connect(selection, SIGNAL(selectionChanged()), this, SIGNAL(changed()));

    {
        QWidget *container = new QWidget(this);
        layout->addWidget(container);
        QHBoxLayout *line = new QHBoxLayout(container);
        container->setLayout(line);
        line->setContentsMargins(0, 0, 0, 0);

        QLabel *label = new QLabel(tr("Path:"), container);
        line->addWidget(label);

        path = new QLineEdit(container);
        line->addWidget(path, 1);
        label->setBuddy(path);
        path->setToolTip(tr("Path to read from"));
        path->setStatusTip(tr("Path"));
        path->setWhatsThis(
                tr("This is the path within the data values to extract the final result from."));

        connect(path, SIGNAL(textChanged(
                                     const QString &)), this, SIGNAL(changed()));
    }

    calibration = new CalibrationEdit(this);
    layout->addWidget(calibration);
    connect(calibration, SIGNAL(calibrationChanged(
                                        const CPD3::Calibration &)), this, SIGNAL(changed()));
}

OptionEditorFilterInput::Editor::~Editor() = default;

void OptionEditorFilterInput::Editor::apply(double start, double end)
{
    int idx = constantMode->currentIndex();
    if (idx >= 0 && idx < constantMode->count())
        idx = constantMode->itemData(idx).toInt();

    DynamicInputOption *o = static_cast<DynamicInputOption *>(option());

    double v;
    {
        bool ok = false;
        v = constantValue->text().toDouble(&ok);
        if (!ok)
            v = FP::undefined();
    }

    switch (idx) {
    case ConstantMode_Primary:
        selection->setEnabled(false);
        path->setEnabled(false);
        calibration->setEnabled(false);
        constantValue->setEnabled(true);
        if (FP::defined(v)) {
            constantValue->setStyleSheet(QString());
        } else {
            constantValue->setStyleSheet("QLineEdit {background-color: red;}");
        }
        o->set(start, end, v, false);
        return;

    case ConstantMode_Undefined:
        selection->setEnabled(false);
        path->setEnabled(false);
        calibration->setEnabled(false);
        constantValue->setEnabled(false);
        constantValue->setStyleSheet(QString());
        o->set(start, end, FP::undefined(), false);
        return;

    case ConstantMode_Disabled:
        v = FP::undefined();
        constantValue->setEnabled(false);
        constantValue->setStyleSheet(QString());
        break;
    case ConstantMode_Fallback:
        constantValue->setEnabled(true);
        if (FP::defined(v)) {
            constantValue->setStyleSheet(QString());
        } else {
            constantValue->setStyleSheet("QLineEdit {background-color: red;}");
        }
        break;
    }

    selection->setEnabled(true);
    path->setEnabled(true);
    calibration->setEnabled(true);

    Variant::Write selectConfig = Variant::Write::empty();
    selection->writeSequenceMatch(selectConfig, optionEditor()->getVariableSelectionDefaults());

    o->set(start, end, SequenceMatch::OrderedLookup(selectConfig), calibration->getCalibration(), v,
           path->text().toStdString());
}

OptionEditorBaselineSmoother::OptionEditorBaselineSmoother(OptionEditor *parent, const QString &id)
        : OptionEditorTimeDependent(parent, id)
{ }

OptionEditorBaselineSmoother::~OptionEditorBaselineSmoother() = default;

OptionEditorTimeBlock *OptionEditorBaselineSmoother::createBlock()
{ return new Editor(this); }

void OptionEditorBaselineSmoother::clear()
{ static_cast<BaselineSmootherOption *>(option())->clear(); }

OptionEditorBaselineSmoother::Editor::Editor(OptionEditorTimeDependent *parent)
        : OptionEditorTimeBlock(parent)
{
    QVBoxLayout *layout = new QVBoxLayout(this);
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    Variant::Root meta;
    auto editorConfig = meta.write().metadataHash("Editor");
    editorConfig["EnableStability"] =
            static_cast<BaselineSmootherOption *>(option())->getStabilityDetection();
    editorConfig["EnableSpike"] =
            static_cast<BaselineSmootherOption *>(option())->getSpikeDetection();

    editor = new BaselineSmootherEditor(Variant::Read::empty(), meta, this);
    layout->addWidget(editor);

    connect(editor, &BaselineSmootherEditor::changed, this, &Editor::changed);
}

OptionEditorBaselineSmoother::Editor::~Editor() = default;

void OptionEditorBaselineSmoother::Editor::apply(double start, double end)
{
    static_cast<BaselineSmootherOption *>(option())->overlay(editor->create().release(), start,
                                                             end);
}


}

SingleOptionEditorBase *OptionEditor::create(const QString &id)
{
    ComponentOptionBase *base = options.get(id);

    if (qobject_cast<ComponentOptionFile *>(base))
        return new OptionEditorFile(this, id);
    if (qobject_cast<ComponentOptionDirectory *>(base))
        return new OptionEditorDirectory(this, id);
    if (qobject_cast<ComponentOptionScript *>(base))
        return new OptionEditorScript(this, id);
    if (qobject_cast<ComponentOptionSingleString *>(base))
        return new OptionEditorSingleString(this, id);
    if (qobject_cast<ComponentOptionEnum *>(base))
        return new OptionEditorEnum(this, id);
    if (qobject_cast<ComponentOptionBoolean *>(base))
        return new OptionEditorBoolean(this, id);
    if (qobject_cast<ComponentOptionSingleDouble *>(base))
        return new OptionEditorSingleDouble(this, id);
    if (qobject_cast<ComponentOptionSingleInteger *>(base))
        return new OptionEditorSingleInteger(this, id);
    if (qobject_cast<ComponentOptionDoubleSet *>(base))
        return new OptionEditorDoubleSet(this, id);
    if (qobject_cast<ComponentOptionDoubleList *>(base))
        return new OptionEditorDoubleList(this, id);
    if (qobject_cast<ComponentOptionInstrumentSuffixSet *>(base))
        return new OptionEditorInstrumentSuffixSet(this, id);
    if (qobject_cast<ComponentOptionStringSet *>(base))
        return new OptionEditorStringSet(this, id);
    if (qobject_cast<ComponentOptionTimeOffset *>(base))
        return new OptionEditorTimeOffset(this, id);
    if (qobject_cast<ComponentOptionSingleTime *>(base))
        return new OptionEditorSingleTime(this, id);
    if (qobject_cast<ComponentOptionSingleCalibration *>(base))
        return new OptionEditorSingleCalibration(this, id);
    if (qobject_cast<ComponentOptionSequenceMatch *>(base))
        return new OptionEditorSequenceMatch(this, id);
    if (qobject_cast<DynamicStringOption *>(base))
        return new OptionEditorTimeInputString(this, id);
    if (qobject_cast<DynamicBoolOption *>(base))
        return new OptionEditorTimeInputBool(this, id);
    if (qobject_cast<DynamicDoubleOption *>(base))
        return new OptionEditorTimeInputDouble(this, id);
    if (qobject_cast<DynamicIntegerOption *>(base))
        return new OptionEditorTimeInputInteger(this, id);
    if (qobject_cast<DynamicCalibrationOption *>(base))
        return new OptionEditorTimeInputCalibration(this, id);
    if (qobject_cast<TimeIntervalSelectionOption *>(base))
        return new OptionEditorTimeIntervalSelection(this, id);
    if (qobject_cast<DynamicSequenceSelectionOption *>(base))
        return new OptionEditorFilterOperate(this, id);
    if (qobject_cast<DynamicInputOption *>(base))
        return new OptionEditorFilterInput(this, id);
    if (qobject_cast<BaselineSmootherOption *>(base))
        return new OptionEditorBaselineSmoother(this, id);


    return NULL;
}

OptionEditor::OptionEditor(QWidget *parent) : QWidget(parent),
                                              options(),
                                              availableStations(),
                                              availableArchives(),
                                              availableVariables(),
                                              availableFlavors(),
                                              variableSelectionDefaults(),
                                              displayArea(NULL),
                                              displayLayout(NULL),
                                              editors()
{
    QVBoxLayout *mainLayout = new QVBoxLayout(this);
    mainLayout->setContentsMargins(0, 0, 0, 0);
    setLayout(mainLayout);

    QScrollArea *mainScroll = new QScrollArea(this);
    mainLayout->addWidget(mainScroll, 1);
    mainScroll->setFrameShape(QFrame::NoFrame);
    mainScroll->setWidgetResizable(true);
    //mainScroll->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);    

    displayArea = new QWidget(mainScroll);
    mainScroll->setWidget(displayArea);
    displayLayout = new QVBoxLayout(displayArea);
    displayArea->setLayout(displayLayout);
    displayLayout->setContentsMargins(0, 0, 0, 0);
}

OptionEditor::~OptionEditor()
{ }

QSize OptionEditor::sizeHint() const
{
    return QWidget::sizeHint().expandedTo(QSize(600, 300));
}

ComponentOptions OptionEditor::getOptions() const
{ return options; }

ComponentOptionBase *OptionEditor::option(const QString &id)
{ return options.get(id); }

namespace {
struct OptionSortItem {
    QString id;
    ComponentOptionBase *option;
};

static bool optionSortCompare(const OptionSortItem &a, const OptionSortItem &b)
{
    if (a.option->getSortPriority() != b.option->getSortPriority())
        return a.option->getSortPriority() < b.option->getSortPriority();
    if (a.option->getArgumentName() != b.option->getArgumentName())
        return a.option->getArgumentName() < b.option->getArgumentName();
    if (a.option->getDisplayName() != b.option->getDisplayName())
        return a.option->getDisplayName() < b.option->getDisplayName();
    return a.id < b.id;
}
}

void OptionEditor::setOptions(const ComponentOptions &options)
{
    this->options = options;

    for (QList<SingleOptionEditorBase *>::const_iterator editor = editors.constBegin(),
            end = editors.constEnd(); editor != end; ++editor) {
        displayLayout->removeWidget(*editor);
        (*editor)->hide();
        (*editor)->deleteLater();
    }
    {
        QLayoutItem *child = 0;
        while ((child = displayLayout->takeAt(0)) != 0) {
            delete child;
        }
    }
    editors.clear();

    QHash<QString, ComponentOptionBase *> allOptions(options.getAll());
    QList<OptionSortItem> sorted;
    for (QHash<QString, ComponentOptionBase *>::const_iterator add = allOptions.constBegin(),
            endAdd = allOptions.constEnd(); add != endAdd; ++add) {
        OptionSortItem i;
        i.id = add.key();
        i.option = add.value();
        sorted.append(i);
    }
    std::sort(sorted.begin(), sorted.end(), optionSortCompare);

    for (QList<OptionSortItem>::const_iterator add = sorted.constBegin(),
            endAdd = sorted.constEnd(); add != endAdd; ++add) {
        SingleOptionEditorBase *editor = create(add->id);
        if (editor == NULL)
            continue;

        connect(editor, SIGNAL(changed()), this, SLOT(updateExclude()));
        connect(editor, SIGNAL(changed()), this, SIGNAL(optionsChanged()));
        connect(editor, SIGNAL(selectedChanged(QWidget * )), this, SLOT(updateVisible(QWidget * )));

        editors.append(editor);
        displayLayout->addWidget(editor);
    }

    displayLayout->addStretch(1);

    updateExclude();
}

void OptionEditor::updateExclude()
{
    QSet<QString> excluded;
    QHash<QString, ComponentOptionBase *> allOptions(options.getAll());
    QHash<QString, QSet<QString> > allExclude(options.excluded());
    for (QHash<QString, ComponentOptionBase *>::const_iterator add = allOptions.constBegin(),
            endAdd = allOptions.constEnd(); add != endAdd; ++add) {
        if (!add.value()->isSet())
            continue;
        excluded |= allExclude.value(add.key());
    }

    for (QList<SingleOptionEditorBase *>::const_iterator editor = editors.constBegin(),
            end = editors.constEnd(); editor != end; ++editor) {
        if (!excluded.contains((*editor)->getID())) {
            (*editor)->setEnabled(true);
        } else {
            (*editor)->setEnabled(false);
            (*editor)->reset();
        }
    }
}

void OptionEditor::updateVisible(QWidget *checked)
{
    for (QList<SingleOptionEditorBase *>::const_iterator editor = editors.constBegin(),
            end = editors.constEnd(); editor != end; ++editor) {
        if (*editor == checked)
            continue;
        (*editor)->selectButton()->setChecked(false);
    }
}

void OptionEditor::setAvailableStations(const SequenceName::ComponentSet &set)
{
    availableStations = set;
    emit availableChanged();
}

void OptionEditor::setAvailableArchives(const SequenceName::ComponentSet &set)
{
    availableArchives = set;
    emit availableChanged();
}

void OptionEditor::setAvailableVariables(const SequenceName::ComponentSet &set)
{
    availableVariables = set;
    emit availableChanged();
}

void OptionEditor::setAvailableFlavors(const SequenceName::ComponentSet &set)
{
    availableFlavors = set;
    emit availableChanged();
}

SequenceName::ComponentSet OptionEditor::getAvailableStations() const
{ return availableStations; }

SequenceName::ComponentSet OptionEditor::getAvailableArchives() const
{ return availableArchives; }

SequenceName::ComponentSet OptionEditor::getAvailableVariables() const
{ return availableVariables; }

SequenceName::ComponentSet OptionEditor::getAvailableFlavors() const
{ return availableFlavors; }


void OptionEditor::setVariableSelectionDefaults(const VariableSelect::SequenceMatchDefaults &selection)
{
    variableSelectionDefaults = selection;
}

VariableSelect::SequenceMatchDefaults OptionEditor::getVariableSelectionDefaults() const
{
    return variableSelectionDefaults;
}

/**
 * Configure the available elements displayed based on the given
 * archive selection.
 * 
 * @param selection     the selection to load
 */
void OptionEditor::configureAvailable(const Archive::Selection &selection)
{
    struct Context {
        OptionEditor *editor;
        std::shared_ptr<Archive::Access> access;
        std::shared_ptr<Archive::Access::ReadLock> lock;

        Context(OptionEditor *editor) : editor(editor),
                                        access(std::make_shared<Archive::Access>()),
                                        lock(std::make_shared<Archive::Access::ReadLock>(*access))
        { }

        ~Context()
        {
            lock.reset();
            access.reset();
        }
    };

    auto context = std::make_shared<Context>(this);
    Threading::pollFuture(this, context->access
                                       ->availableSelectedFuture(selection.withMetaArchive(false)),
                          [context](const Archive::Access::SelectedComponents &result) {
                              context->editor->setAvailableStations(result.stations);
                              context->editor->setAvailableArchives(result.archives);
                              context->editor->setAvailableVariables(result.variables);
                              context->editor->setAvailableFlavors(result.flavors);
                          });
}

/**
 * Configure the available elements displayed based on the defaults.
 * 
 * @param selection     the selection to load
 */
void OptionEditor::configureAvailable()
{
    struct Context {
        OptionEditor *editor;
        std::shared_ptr<Archive::Access> access;
        std::shared_ptr<Archive::Access::ReadLock> lock;

        Context(OptionEditor *editor) : editor(editor),
                                        access(std::make_shared<Archive::Access>()),
                                        lock(std::make_shared<Archive::Access::ReadLock>(*access))
        { }

        ~Context()
        {
            lock.reset();
            access.reset();
        }
    };

    auto context = std::make_shared<Context>(this);
    Threading::pollFuture(this, context->access
                                       ->availableSelectedFuture(
                                               CPD3::Data::Archive::Selection(FP::undefined(),
                                                                              FP::undefined(), {},
                                                                              {"raw"},
                                                                              {".+_.+"}).withMetaArchive(
                                                       false)),
                          [context](const Archive::Access::SelectedComponents &result) {
                              context->editor->setAvailableStations(result.stations);
                              context->editor->setAvailableArchives(result.archives);
                              context->editor->setAvailableVariables(result.variables);
                          });
    Threading::pollFuture(this, context->access->availableFlavorsFuture(),
                          [context](const SequenceName::ComponentSet &result) {
                              context->editor->setAvailableFlavors(result);
                          });
}

}
}
}
