/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <functional>
#include <QtDebug>
#include <QValidator>
#include <QLineEdit>
#include <QSettings>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QComboBox>
#include <QPushButton>
#include <QApplication>
#include <QSplitter>
#include <QShowEvent>
#include <QUndoCommand>
#include <QShortcut>
#include <QDialog>
#include <QDialogButtonBox>
#include <QCheckBox>

#include "core/number.hxx"
#include "core/range.hxx"
#include "datacore/stream.hxx"
#include "guidata/dataeditor.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace GUI {
namespace Data {

/** @file guidata/dataeditor.hxx
 * Provides an editor for data values.
 */

DataEditor::DataEditor(QWidget *parent) : QWidget(parent),
                                          station(),
                                          archive(),
                                          variable(),
                                          hasFlavors(),
                                          lacksFlavors(),
                                          start(FP::undefined()),
                                          end(FP::undefined()),
                                          availableStations(),
                                          availableArchives(),
                                          availableVariables(),
                                          availableFlavors(),
                                          selectionFlags(DefaultSelectionFlags),
                                          modifyFlags(DefaultModify),
                                          settings(CPD3GUI_ORGANIZATION, CPD3GUI_APPLICATION),
                                          haveLoadedSettings(false),
                                          values(),
                                          metadata(),
                                          pristine(),
                                          changesMade(false)
{
    mainLayout = new QGridLayout(this);
    mainLayout->setContentsMargins(0, 0, 0, 0);
    setLayout(mainLayout);

    stationSelect = new QComboBox(this);
    stationSelect->setToolTip(tr("The selected station to show."));
    stationSelect->setStatusTip(tr("Select the station filter"));
    stationSelect->setWhatsThis(
            tr("This allows for filtering the visible data by the station code."));
    stationSelect->setSizePolicy(
            QSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding,
                        QSizePolicy::ComboBox));
    stationSelect->setSizeAdjustPolicy(QComboBox::AdjustToContents);
    stationSelect->setInsertPolicy(QComboBox::NoInsert);
    mainLayout->addWidget(stationSelect, 0, 0, Qt::AlignLeft);

    archiveSelect = new QComboBox(this);
    archiveSelect->setToolTip(tr("The selected archive to show."));
    archiveSelect->setStatusTip(tr("Select the archive filter"));
    archiveSelect->setWhatsThis(
            tr("This allows for filtering the visible data by the archive name."));
    archiveSelect->setSizePolicy(
            QSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding,
                        QSizePolicy::ComboBox));
    archiveSelect->setSizeAdjustPolicy(QComboBox::AdjustToContents);
    archiveSelect->setInsertPolicy(QComboBox::NoInsert);
    mainLayout->addWidget(archiveSelect, 0, 1, Qt::AlignLeft);

    variableSelect = new QComboBox(this);
    variableSelect->setToolTip(tr("The selected variable to show."));
    variableSelect->setStatusTip(tr("Select the variable filter"));
    variableSelect->setWhatsThis(tr("This allows for filtering the visible data by the variable."));
    variableSelect->setSizePolicy(
            QSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding,
                        QSizePolicy::ComboBox));
    variableSelect->setSizeAdjustPolicy(QComboBox::AdjustToContents);
    variableSelect->setInsertPolicy(QComboBox::NoInsert);
    mainLayout->addWidget(variableSelect, 0, 2, Qt::AlignLeft);

    hasFlavorsSelect = new QComboBox(this);
    hasFlavorsSelect->setToolTip(tr("The required flavors to display."));
    hasFlavorsSelect->setStatusTip(tr("Select required flavors"));
    hasFlavorsSelect->setWhatsThis(
            tr("This allows for filtering based on if a value possesses certain \"flavors\".  For example \"PM10\" entered here will only show that data, and not data without a cut size or of any other cut size."));
    hasFlavorsSelect->setSizePolicy(
            QSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding,
                        QSizePolicy::ComboBox));
    hasFlavorsSelect->setSizeAdjustPolicy(QComboBox::AdjustToContents);
    hasFlavorsSelect->setInsertPolicy(QComboBox::NoInsert);
    mainLayout->addWidget(hasFlavorsSelect, 0, 3, Qt::AlignLeft);

    lacksFlavorsSelect = new QComboBox(this);
    lacksFlavorsSelect->setToolTip(tr("The flavors to hide."));
    lacksFlavorsSelect->setStatusTip(tr("Select hidden flavors"));
    lacksFlavorsSelect->setWhatsThis(
            tr("This allows for filtering based on if a value does not possess certain \"flavors\".  For example \"PM10\" entered here will hide that data but will continue to show all other cut sizes and data without a cut size."));
    lacksFlavorsSelect->setSizePolicy(
            QSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding,
                        QSizePolicy::ComboBox));
    lacksFlavorsSelect->setSizeAdjustPolicy(QComboBox::AdjustToContents);
    lacksFlavorsSelect->setInsertPolicy(QComboBox::NoInsert);
    mainLayout->addWidget(lacksFlavorsSelect, 0, 4, Qt::AlignLeft);

    timeSelect = new TimeBoundSelection(this);
    timeSelect->setToolTip(tr("The selected range of time."));
    timeSelect->setStatusTip(tr("Select visible times"));
    timeSelect->setWhatsThis(
            tr("This allows for filtering based on if values intersect the a time range.  Any value that intersects this time range will be shown, even if the start or end actually extends beyond the specified range.  The start is inclusive and the end is exclusive."));
    timeSelect->setAllowUndefinedStart(true);
    timeSelect->setAllowUndefinedEnd(true);
    connect(timeSelect, SIGNAL(boundsEdited(double, double)), this,
            SLOT(setBounds(double, double)));
    mainLayout->addWidget(timeSelect, 0, 5);
    mainLayout->setColumnStretch(5, 1);

    QSplitter *timelineSplitter = new QSplitter(Qt::Vertical, this);
    mainLayout->addWidget(timelineSplitter, 1, 0, 1, -1);
    mainLayout->setRowStretch(1, 1);

    QWidget *layoutWidget = new QWidget(timelineSplitter);
    timelineLayout = new QGridLayout(layoutWidget);
    timelineLayout->setContentsMargins(0, 0, 0, 0);
    layoutWidget->setLayout(timelineLayout);
    timelineSplitter->addWidget(layoutWidget);


    selectedIdentity = SequenceIdentity();
    selectedContents = Variant::Write::empty();


    timeline = new DataValueTimeline(layoutWidget);
    timeline->setToolTip(tr("A timeline of available values."));
    timeline->setStatusTip(tr("Value timeline"));
    timeline->setWhatsThis(
            tr("This is a timeline of available values.  Click on one to select it for editing."));
    timeline->setAutoranging(true);
    timelineLayout->addWidget(timeline, 0, 0, 1, -1);
    timelineLayout->setRowStretch(0, 1);

    showUnitEdit =
            new QPushButton(QApplication::style()->standardIcon(QStyle::SP_DriveHDIcon), QString(),
                            layoutWidget);
    showUnitEdit->setToolTip(
            tr("Show the value identifier (station, archive, variable, and flavors) editing dialog."));
    showUnitEdit->setStatusTip(tr("Edit value identifier"));
    showUnitEdit->setWhatsThis(
            tr("Use this button to bring up the value identifier editing dialog."));
    timelineLayout->addWidget(showUnitEdit, 1, 0, Qt::AlignLeft);
    connect(showUnitEdit, SIGNAL(clicked(bool)), this, SLOT(showUnitEditPressed()));

    timeEdit = new TimeBoundSelection(layoutWidget);
    timeEdit->setToolTip(tr("Enter in the time range of the selected value."));
    timeEdit->setStatusTip(tr("Times"));
    timeEdit->setWhatsThis(
            tr("This is the time range that the value is effective during.  The start is inclusive and the end is exclusive."));
    timeEdit->setAllowUndefinedStart(true);
    timeEdit->setAllowUndefinedEnd(true);
    timelineLayout->addWidget(timeEdit, 1, 1);
    timelineLayout->setColumnStretch(2, 1);
    connect(timeEdit, SIGNAL(boundsEdited(double, double)), this,
            SLOT(selectedTimesEdited(double, double)));

    priorityEdit = new QSpinBox(layoutWidget);
    priorityEdit->setToolTip(tr("Enter the priority of the selected value."));
    priorityEdit->setStatusTip(tr("Priority"));
    priorityEdit->setWhatsThis(
            tr("This is the priority of the selected value.  Higher priority values will be overlaid on top of lower priority ones when both would be in effect."));
    /* Explicit in case the system's int isn't 32 bit */
    priorityEdit->setMaximum(INT_MAX > 0x7FFFFFFF ? 0x7FFFFFFF : INT_MAX);
    priorityEdit->setMinimum(INT_MIN < -0x7FFFFFFF ? -0x7FFFFFFF : INT_MIN);
    priorityEdit->setSizePolicy(
            QSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding,
                        QSizePolicy::SpinBox));
    timelineLayout->addWidget(priorityEdit, 1, 2, Qt::AlignLeft);
    connect(priorityEdit, SIGNAL(valueChanged(int)), this, SLOT(selectedPriorityEdited(int)));

    addButton = new QPushButton(QApplication::style()->standardIcon(QStyle::SP_FileIcon), QString(),
                                layoutWidget);
    addButton->setToolTip(tr("Add a new value based on the currently selected value."));
    addButton->setStatusTip(tr("Add a new value"));
    addButton->setWhatsThis(
            tr("This button adds a new empty value with the same identifier (station, archive, variable, and flavors) as the currently selected one.  The newly added value is then selected."));
    timelineLayout->addWidget(addButton, 1, 3, Qt::AlignLeft);
    connect(addButton, SIGNAL(clicked(bool)), this, SLOT(addPressed()));

    removeButton =
            new QPushButton(QApplication::style()->standardIcon(QStyle::SP_TrashIcon), QString(),
                            layoutWidget);
    removeButton->setToolTip(tr("Remove the selected value."));
    removeButton->setStatusTip(tr("Add a new value"));
    removeButton->setWhatsThis(
            tr("This button adds a new empty value with the same identifier (station, archive, variable, and flavors) as the currently selected one.  The newly added value is then selected."));
    removeButton->setEnabled(false);
    timelineLayout->addWidget(removeButton, 1, 4, Qt::AlignLeft);
    connect(removeButton, SIGNAL(clicked(bool)), this, SLOT(removePressed()));

    editor = new ValueEditor(timelineSplitter);
    editor->setEnabled(false);
    timelineSplitter->addWidget(editor);

    connect(timeline, SIGNAL(valueClicked(
                                     const CPD3::Data::SequenceIdentity &)), this, SLOT(selectValue(
                                                                                                const CPD3::Data::SequenceIdentity &)));
    connect(editor, SIGNAL(valueChanged()), this, SLOT(valueModified()));


    /* This will also handle the initial signal connection */
    updateUnitSelectionBoxContents();
    updateSelectionVisiblity();
    clearSelection();
}

void DataEditor::updateSelectionSingleBoxContents(QComboBox *box,
                                                  const QStringList &contents,
                                                  const CPD3::Data::SequenceName::Component &current,
                                                  const char *slot,
                                                  bool uc,
                                                  bool enableAll,
                                                  bool enableDefault,
                                                  bool enableManual)
{
    box->disconnect(this);
    if (box->lineEdit())
        box->lineEdit()->disconnect(this);
    box->clear();

    int idxAll = -1;
    if (enableAll) {
        idxAll = box->count();
        box->addItem(tr("Any", "selection all"), QVariant(1));
    }
    int idxDefault = -1;
    if (enableDefault) {
        idxDefault = box->count();
        box->addItem(tr("Default", "selection default"), QVariant(2));
    }
    if ((enableAll || enableDefault) && !contents.isEmpty())
        box->insertSeparator(3);

    int idx = -1;
    for (QList<QString>::const_iterator add = contents.constBegin(), end = contents.constEnd();
            add != end;
            ++add) {
        if (add->length() == 0)
            continue;
        if (*add == QString('_'))
            continue;
        if (add->compare(QString::fromStdString(current),
                         uc ? Qt::CaseInsensitive : Qt::CaseSensitive) == 0) {
            idx = box->count();
        }
        if (uc)
            box->addItem(add->toUpper());
        else
            box->addItem(*add);
    }

    connect(box, SIGNAL(currentIndexChanged(int)), this, slot);
    if (box->lineEdit())
        connect(box->lineEdit(), SIGNAL(editingFinished()), this, slot);

    if (idx != -1) {
        box->setCurrentIndex(idx);
    } else if (current.length() == 0 && idxAll != -1) {
        box->setCurrentIndex(idxAll);
    } else if (current == "_" && idxDefault != -1) {
        box->setCurrentIndex(idxDefault);
    } else if (current.length() > 0 && enableManual && box->lineEdit()) {
        box->lineEdit()->setText(QString::fromStdString(current));
    } else {
        box->setCurrentIndex(-1);
    }
}

void DataEditor::updateSelectionSingleBoxContents(QComboBox *box,
                                                  const QStringList &contents,
                                                  const std::vector<
                                                          CPD3::Data::SequenceName::Component> &current,
                                                  const char *slot,
                                                  bool uc,
                                                  bool enableAll,
                                                  bool enableDefault,
                                                  bool enableManual)
{
    CPD3::Data::SequenceName::Component combined;
    for (const auto &add : current) {
        if (!combined.empty())
            combined += " ";
        combined += add;
    }
    return updateSelectionSingleBoxContents(box, contents, combined, slot, uc, enableAll,
                                            enableDefault, enableManual);
}

void DataEditor::updateUnitSelectionBoxContents()
{
    updateSelectionSingleBoxContents(stationSelect, availableStations, station,
                                     SLOT(stationSelected()), true,
                                     (selectionFlags & SelectAllStations) != 0,
                                     (selectionFlags & SelectDefaultStation) != 0,
                                     (selectionFlags & SelectManual) != 0);
    updateSelectionSingleBoxContents(archiveSelect, availableArchives, archive,
                                     SLOT(archiveSelected()), true,
                                     (selectionFlags & SelectAllArchives) != 0, false,
                                     (selectionFlags & SelectManual) != 0);
    updateSelectionSingleBoxContents(variableSelect, availableVariables, variable,
                                     SLOT(variableSelected()), false,
                                     (selectionFlags & SelectAllVariables) != 0, false,
                                     (selectionFlags & SelectManual) != 0);
    updateSelectionSingleBoxContents(hasFlavorsSelect, availableFlavors, hasFlavors,
                                     SLOT(hasFlavorsSelected()), true, true, false, true);
    updateSelectionSingleBoxContents(lacksFlavorsSelect, availableFlavors, lacksFlavors,
                                     SLOT(lacksFlavorsSelected()), true, true, false, true);
}

QString DataEditor::lookupBoxSelection(const QComboBox *box,
                                       bool enableAll,
                                       bool enableDefault,
                                       bool enableManual)
{
    int idx = box->currentIndex();
    QString str(box->currentText());
    if (idx > -1) {
        QVariant data(box->itemData(idx));
        QString strAt(box->itemText(idx));
        if (data.isValid()) {
            if (enableAll && data.toInt() == 1) {
                if (!(selectionFlags & SelectManual) || strAt == str)
                    return QString();
            } else if (enableDefault && data.toInt() == 2) {
                if (!(selectionFlags & SelectManual) || strAt == str)
                    return QString('_');
            }
        }
        if (str.length() > 0 && str == strAt)
            return str;
    }
    if (enableManual) {
        if (str.length() > 0)
            return str;
    }

    if (enableAll) {
        return QString();
    } else if (enableDefault) {
        return QString('_');
    } else if (box->count() > 0) {
        QString strAt(box->itemText(0));
        if (strAt.length() > 0)
            return strAt;
    }

    /* Just give up, not possible valid things to return. */
    return QString();
}

void DataEditor::stationSelected()
{
    QString set(lookupBoxSelection(stationSelect, selectionFlags & SelectAllStations,
                                   selectionFlags & SelectDefaultStation,
                                   selectionFlags & SelectManual));
    if (set.compare(QString::fromStdString(station), Qt::CaseInsensitive) == 0)
        return;
    station = set.toLower().toStdString();
    if (!(selectionFlags & SelectStation))
        return;
    emit stationChanged(station);
    emit selectionChanged();
}

void DataEditor::archiveSelected()
{
    QString set(lookupBoxSelection(archiveSelect, selectionFlags & SelectAllArchives, false,
                                   selectionFlags & SelectManual));
    if (set.compare(QString::fromStdString(archive), Qt::CaseInsensitive) == 0)
        return;
    archive = set.toLower().toStdString();
    if (!(selectionFlags & SelectArchive))
        return;
    emit archiveChanged(archive);
    emit selectionChanged();
}

void DataEditor::variableSelected()
{
    QString set(lookupBoxSelection(variableSelect, selectionFlags & SelectAllVariables, false,
                                   selectionFlags & SelectManual));
    if (set.compare(QString::fromStdString(variable), Qt::CaseSensitive) == 0)
        return;
    variable = set.toStdString();
    if (!(selectionFlags & SelectVariable))
        return;
    emit variableChanged(variable);
    emit selectionChanged();
}

void DataEditor::hasFlavorsSelected()
{
    QString raw(lookupBoxSelection(hasFlavorsSelect, true, false, selectionFlags & SelectManual));
    std::vector<CPD3::Data::SequenceName::Component> set;
    if (raw == QRegExp::escape(raw)) {
        CPD3::Data::SequenceName::ComponentSet reduced;
        for (const auto &add : raw.split(QRegExp("[\\s:;,]"), QString::SkipEmptyParts)) {
            reduced.insert(add.toLower().toStdString());
        }
        set = std::vector<CPD3::Data::SequenceName::Component>(reduced.begin(), reduced.end());
    } else {
        set.emplace_back(raw.toStdString());
    }
    if (set == hasFlavors)
        return;
    hasFlavors = std::move(set);
    if (!(selectionFlags & SelectFlavors))
        return;
    emit hasFlavorsChanged(hasFlavors);
    emit selectionChanged();
}

void DataEditor::lacksFlavorsSelected()
{
    QString raw(lookupBoxSelection(lacksFlavorsSelect, true, false, selectionFlags & SelectManual));
    std::vector<CPD3::Data::SequenceName::Component> set;
    if (raw == QRegExp::escape(raw)) {
        CPD3::Data::SequenceName::ComponentSet reduced;
        for (const auto &add : raw.split(QRegExp("[\\s:;,]"), QString::SkipEmptyParts)) {
            reduced.insert(add.toLower().toStdString());
        }
        set = std::vector<CPD3::Data::SequenceName::Component>(reduced.begin(), reduced.end());
    } else {
        set.emplace_back(raw.toStdString());
    }
    if (set == lacksFlavors)
        return;
    lacksFlavors = std::move(set);
    if (!(selectionFlags & SelectFlavors))
        return;
    emit lacksFlavorsChanged(lacksFlavors);
    emit selectionChanged();
}

void DataEditor::setStation(const CPD3::Data::SequenceName::Component &set)
{
    if (Util::equal_insensitive(station, set))
        return;
    station = Util::to_lower(set);
    if (!(selectionFlags & SelectStation))
        return;
    emit stationChanged(station);
    emit selectionChanged();
    updateUnitSelectionBoxContents();
}

void DataEditor::setArchive(const CPD3::Data::SequenceName::Component &set)
{
    if (Util::equal_insensitive(archive, set))
        return;
    archive = Util::to_lower(set);
    if (!(selectionFlags & SelectArchive))
        return;
    emit archiveChanged(archive);
    emit selectionChanged();
    updateUnitSelectionBoxContents();
}

void DataEditor::setVariable(const CPD3::Data::SequenceName::Component &set)
{
    if (variable == set)
        return;
    variable = set;
    if (!(selectionFlags & SelectVariable))
        return;
    emit variableChanged(variable);
    emit selectionChanged();
    updateUnitSelectionBoxContents();
}

void DataEditor::setHasFlavors(const std::vector<CPD3::Data::SequenceName::Component> &set)
{ setHasFlavors(CPD3::Data::SequenceName::ComponentSet(set.begin(), set.end())); }

void DataEditor::setHasFlavors(const CPD3::Data::SequenceName::ComponentSet &set)
{
    std::vector<CPD3::Data::SequenceName::Component> sorted;
    for (const auto &add : set) {
        sorted.emplace_back(Util::to_lower(add));
    }
    std::sort(sorted.begin(), sorted.end());

    if (sorted == hasFlavors)
        return;
    hasFlavors = std::move(sorted);
    if (!(selectionFlags & SelectFlavors))
        return;
    emit hasFlavorsChanged(hasFlavors);
    emit selectionChanged();
    updateUnitSelectionBoxContents();
    setHasFlavors(std::vector<CPD3::Data::SequenceName::Component>(set.begin(), set.end()));
}

void DataEditor::setLacksFlavors(const std::vector<CPD3::Data::SequenceName::Component> &set)
{ setLacksFlavors(CPD3::Data::SequenceName::ComponentSet(set.begin(), set.end())); }

void DataEditor::setLacksFlavors(const CPD3::Data::SequenceName::ComponentSet &set)
{
    std::vector<CPD3::Data::SequenceName::Component> sorted;
    for (const auto &add : set) {
        sorted.emplace_back(Util::to_lower(add));
    }
    std::sort(sorted.begin(), sorted.end());

    if (sorted == lacksFlavors)
        return;
    lacksFlavors = std::move(sorted);
    if (!(selectionFlags & SelectFlavors))
        return;
    emit lacksFlavorsChanged(lacksFlavors);
    emit selectionChanged();
    updateUnitSelectionBoxContents();
}

/**
 * Test if the editor has had any changes made since the last call to
 * set the data.
 *
 * @return true if no changes have been made
 */
bool DataEditor::isPristine() const
{
    return !changesMade;
}


const CPD3::Data::SequenceName::Component &DataEditor::getStation() const
{ return station; }

const CPD3::Data::SequenceName::Component &DataEditor::getArchive() const
{ return archive; }

const CPD3::Data::SequenceName::Component &DataEditor::getVariable() const
{ return variable; }

const std::vector<CPD3::Data::SequenceName::Component> &DataEditor::getHasFlavors() const
{ return hasFlavors; }

const std::vector<CPD3::Data::SequenceName::Component> &DataEditor::getLacksFlavors() const
{ return lacksFlavors; }

void DataEditor::setBounds(double s, double e)
{
    bool se = FP::equal(start, s);
    bool ee = FP::equal(end, e);
    if (se && ee)
        return;
    start = s;
    end = e;

    if (FP::defined(start) || FP::defined(end)) {
        timeline->setVisible(start, end);
        timeline->setAutoranging(false);
    } else {
        timeline->setAutoranging(true);
    }

    timeSelect->setBounds(start, end);

    if (!(selectionFlags & SelectTimes))
        return;
    if (se)
            emit startChanged(start);
    if (ee)
            emit endChanged(end);
    emit boundsChanged(start, end);
    emit selectionChanged();
}

void DataEditor::setStart(double v)
{ setBounds(v, end); }

void DataEditor::setEnd(double v)
{ setBounds(start, v); }

double DataEditor::getStart() const
{ return start; }

double DataEditor::getEnd() const
{ return end; }

void DataEditor::setAvailableStations(const SequenceName::ComponentSet &set)
{
    availableStations.clear();
    for (const auto &add : set) {
        availableStations.append(QString::fromStdString(add).toLower());
    }
    std::sort(availableStations.begin(), availableStations.end());
    for (int i = 1; i < availableStations.size();) {
        if (availableStations.at(i - 1) == availableStations.at(i)) {
            availableStations.removeAt(i);
            continue;
        }
        i++;
    }
    updateUnitSelectionBoxContents();
}

void DataEditor::setAvailableArchives(const SequenceName::ComponentSet &set)
{
    availableArchives.clear();
    for (const auto &add : set) {
        availableArchives.append(QString::fromStdString(add).toLower());
    }
    std::sort(availableArchives.begin(), availableArchives.end());
    for (int i = 1; i < availableArchives.size();) {
        if (availableArchives.at(i - 1) == availableArchives.at(i)) {
            availableArchives.removeAt(i);
            continue;
        }
        i++;
    }
    updateUnitSelectionBoxContents();
}

void DataEditor::setAvailableVariables(const SequenceName::ComponentSet &set)
{
    availableVariables.clear();
    for (const auto &add : set) {
        availableVariables.append(QString::fromStdString(add));
    }
    std::sort(availableVariables.begin(), availableVariables.end());
    for (int i = 1; i < availableVariables.size();) {
        if (availableVariables.at(i - 1) == availableVariables.at(i)) {
            availableVariables.removeAt(i);
            continue;
        }
        i++;
    }
    updateUnitSelectionBoxContents();
}

void DataEditor::setAvailableFlavors(const SequenceName::ComponentSet &set)
{
    availableFlavors.clear();
    for (const auto &add : set) {
        availableFlavors.append(QString::fromStdString(add).toLower());
    }
    std::sort(availableFlavors.begin(), availableFlavors.end());
    for (int i = 1; i < availableFlavors.size();) {
        if (availableFlavors.at(i - 1) == availableFlavors.at(i)) {
            availableFlavors.removeAt(i);
            continue;
        }
        i++;
    }
    updateUnitSelectionBoxContents();
}

void DataEditor::updateSelectionVisiblity()
{
    stationSelect->setVisible(selectionFlags & SelectStation);
    archiveSelect->setVisible(selectionFlags & SelectArchive);
    variableSelect->setVisible(selectionFlags & SelectVariable);
    hasFlavorsSelect->setVisible(selectionFlags & SelectFlavors);
    lacksFlavorsSelect->setVisible(selectionFlags & SelectFlavors);
    timeSelect->setVisible(selectionFlags & SelectTimes);

    stationSelect->setEditable(selectionFlags & SelectManual);
    archiveSelect->setEditable(selectionFlags & SelectManual);
    variableSelect->setEditable(selectionFlags & SelectManual);
    hasFlavorsSelect->setEditable(selectionFlags & SelectManual);
    lacksFlavorsSelect->setEditable(selectionFlags & SelectManual);
}

void DataEditor::setSelectionFlags(SelectionFlags flags)
{
    selectionFlags = flags;
    updateSelectionVisiblity();
    updateUnitSelectionBoxContents();
}

DataEditor::SelectionFlags DataEditor::getSelectionFlags() const
{ return selectionFlags; }


void DataEditor::setModifyFlags(ModifyFlags flags)
{
    modifyFlags = flags;
    showUnitEdit->setVisible((flags & ModifySelectedName) != 0);
    selectValue(selectedIdentity);
}

DataEditor::ModifyFlags DataEditor::getModifyFlags() const
{ return modifyFlags; }

/**
 * Get all values and metadata currently known to the editor.
 * 
 * @return all known values
 */
SequenceValue::Transfer DataEditor::getData() const
{
    SequenceValue::Transfer result;
    Util::append(metadata, result);
    for (const auto &add: values) {
        result.emplace_back(add.first, add.second);
    }
    std::sort(result.begin(), result.end(), SequenceIdentity::OrderOverlay());
    return result;
}

/**
 * Get all values (non-metadata) currently known to the editor.
 * 
 * @return all known values
 */
SequenceValue::Transfer DataEditor::getValues() const
{
    SequenceValue::Transfer result;
    for (const auto &add: values) {
        result.emplace_back(add.first, add.second);
    }
    std::sort(result.begin(), result.end(), SequenceIdentity::OrderOverlay());
    return result;
}

/**
 * Get all metadata currently known to the editor.
 * 
 * @return all known values
 */
SequenceValue::Transfer DataEditor::getMetadata() const
{ return metadata; }

bool DataEditor::valueIsNew(const DataSet::value_type &check) const
{
    auto p = pristine.find(check.first);
    if (p == pristine.end())
        return true;
    return p->second.read() != check.second.read();
}

/**
 * Get all values that need to be updated or inserted.  For values being
 * updated, there will be a corresponding remove entry in the removed list
 * if part of the uniqueness changed.
 * 
 * @return the list of added values
 */
SequenceValue::Transfer DataEditor::getNew() const
{
    SequenceValue::Transfer result;
    for (const auto &add : values) {
        if (!valueIsNew(add))
            continue;
        result.emplace_back(add.first, add.second);
    }
    std::sort(result.begin(), result.end(), SequenceIdentity::OrderOverlay());
    return result;
}

/**
 * Get all values that need to be updated or inserted.  For values being
 * updated, there will be a corresponding remove entry in the removed list
 * if part of the uniqueness changed.
 * 
 * @return the list of added values
 */
DataEditor::DataSet DataEditor::getNewSet() const
{
    DataSet result;
    for (const auto &add : values) {
        if (!valueIsNew(add))
            continue;
        result.emplace(add);
    }
    return result;
}

/**
 * Get all values that need to be removed.  This includes values for which
 * part of the uniqueness changed.
 * 
 * @return the list of removed values
 */
SequenceValue::Transfer DataEditor::getRemoved() const
{
    auto output = pristine;
    for (const auto &rem : values) {
        output.erase(rem.first);
    }
    SequenceValue::Transfer result;
    for (const auto &add : output) {
        result.emplace_back(add.first, add.second);
    }
    std::sort(result.begin(), result.end(), SequenceIdentity::OrderOverlay());
    return result;
}

/**
 * Get all values that need to be removed.  This includes values for which
 * part of the uniqueness changed.
 * 
 * @return the list of removed values
 */
DataEditor::DataSet DataEditor::getRemovedSet() const
{
    auto output = pristine;
    for (const auto &rem : values) {
        output.erase(rem.first);
    }
    return output;
}

void DataEditor::clearSelection()
{
    selectedIdentity = SequenceIdentity();
    selectedContents = Variant::Write::empty();

    timeline->highlightValues(std::unordered_set<CPD3::Data::SequenceIdentity>());

    removeButton->setEnabled(false);
    timeEdit->setEnabled(false);
    priorityEdit->setEnabled(false);
    showUnitEdit->setEnabled(false);
    editor->setEnabled(false);
    editor->setValue(selectedContents);
}

void DataEditor::updateTimelineValues()
{
    SequenceIdentity::Transfer list;
    for (const auto &add : values) {
        list.emplace_back(add.first);
    }
    timeline->setValues(std::move(list));
}

void DataEditor::valuesUpdated()
{
    pristine.clear();
    for (const auto &add : values) {
        pristine.emplace(add);
    }
    changesMade = false;

    updateTimelineValues();

    clearSelection();
}

/**
 * Set both data and metadata (anything who's unit is a metadata archive
 * is assigned as metadata).  This resets the modified tracking, undo history,
 * etc.
 * 
 * @param data  the list of values to set
 */
void DataEditor::setData(const SequenceValue::Transfer &data)
{
    values.clear();
    metadata.clear();
    for (const auto &v : data) {
        if (v.getUnit().isMeta() &&
                (!(selectionFlags & SelectArchive) || !CPD3::Data::SequenceName::isMeta(archive)))
            metadata.emplace_back(v);
        else
            values.emplace(v.getIdentity(), v.root());
    }
    std::sort(metadata.begin(), metadata.end(), SequenceIdentity::OrderOverlay());
    valuesUpdated();
}

/**
 * Set both data and metadata (anything who's unit is a metadata archive
 * is assigned as metadata).  This resets the modified tracking, undo history,
 * etc.
 * 
 * @param data  the list of values to set
 */
void DataEditor::setData(const DataSet &data)
{
    values.clear();
    metadata.clear();
    for (const auto &value : data) {
        if (value.first.getName().isMeta() &&
                (!(selectionFlags & SelectArchive) || !CPD3::Data::SequenceName::isMeta(archive)))
            metadata.emplace_back(SequenceValue(value.first, value.second));
        else
            values.emplace(value);
    }
    std::sort(metadata.begin(), metadata.end(), SequenceIdentity::OrderOverlay());
    valuesUpdated();
}

/**
 * Set the data known the the editor.  This resets the modified tracking, 
 * undo history, etc.
 * 
 * @param data  the list of values to set
 */
void DataEditor::setValues(const SequenceValue::Transfer &data)
{
    values.clear();
    for (const auto &add : data) {
        values.emplace(add.getIdentity(), add.root());
    }
    valuesUpdated();
}

/**
 * Set the data known the the editor.  This resets the modified tracking, 
 * undo history, etc.
 * 
 * @param data  the list of values to set
 */
void DataEditor::setValues(const DataSet &data)
{
    values = data;
    valuesUpdated();
}

/**
 * Set the metadata known the the editor.  This resets the modified tracking, 
 * undo history, etc.
 * 
 * @param data  the list of values to set
 */
void DataEditor::setMetadata(const SequenceValue::Transfer &data)
{
    metadata = data;
    std::sort(metadata.begin(), metadata.end(), SequenceIdentity::OrderOverlay());
    valuesUpdated();
}

/**
 * Set the metadata known the the editor.  This resets the modified tracking, 
 * undo history, etc.
 * 
 * @param data  the list of values to set
 */
void DataEditor::setMetadata(const DataSet &data)
{
    metadata.clear();
    for (const auto &v : data) {
        metadata.push_back(SequenceValue(v.first, v.second));
    }
    std::sort(metadata.begin(), metadata.end(), SequenceIdentity::OrderOverlay());
    valuesUpdated();
}


/**
 * Selects a specific data value for editing.
 */
void DataEditor::selectValue(const SequenceIdentity &value)
{
    auto path = editor->getCursorPath();

    {
        auto f = values.find(value);
        if (f == values.end()) {
            clearSelection();
            return;
        }
        selectedIdentity = f->first;
        selectedContents = f->second.write();
    }

    timeline->highlightValue(selectedIdentity);

    Variant::Root mergedMetadata;
    {
        std::vector<std::reference_wrapper<const Variant::Root>> merge;
        for (const auto &check : metadata) {
            if (!Range::intersects(check.getStart(), check.getEnd(), selectedIdentity.getStart(),
                                   selectedIdentity.getEnd()))
                continue;
            merge.emplace_back(check.root());
        }
        mergedMetadata = Variant::Root::overlay(merge.begin(), merge.end());
    }

    bool canModify = true;
    if (!(modifyFlags & ModifyDefaultStationValues) &&
            selectedIdentity.getName().isDefaultStation()) {
        canModify = false;
    }

    removeButton->setEnabled(canModify);
    showUnitEdit->setEnabled(canModify);

    timeEdit->setBounds(selectedIdentity.getStart(), selectedIdentity.getEnd());
    timeEdit->setEnabled(canModify);

    priorityEdit->setValue(selectedIdentity.getPriority());
    priorityEdit->setEnabled(canModify);

    editor->setValue(selectedContents, mergedMetadata);
    editor->moveCursor(path);
    editor->setEnabled(true);
    editor->setReadOnly(!canModify);
}

void DataEditor::showUnitEditPressed()
{
    if (!selectedIdentity.isValid())
        return;

    QDialog input(this);
    input.setWindowModality(Qt::WindowModal);
    input.setWindowTitle(tr("Modify Value Identifier"));

    QGridLayout *outer = new QGridLayout(&input);
    input.setLayout(outer);
    QDialogButtonBox *buttonBox =
            new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal,
                                 &input);
    connect(buttonBox, SIGNAL(accepted()), &input, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), &input, SLOT(reject()));
    outer->addWidget(buttonBox, 1, 0, 1, -1, Qt::AlignBottom);

    QWidget *innerWidget = new QWidget(&input);
    outer->addWidget(innerWidget, 0, 0, 1, 1);
    QFormLayout *layout = new QFormLayout(innerWidget);
    innerWidget->setLayout(layout);

    QComboBox *station = new QComboBox(innerWidget);
    station->setEditable(true);
    station->lineEdit()->setMaxLength(126);
    station->setInsertPolicy(QComboBox::NoInsert);
    station->clear();
    for (QList<QString>::const_iterator add = availableStations.constBegin(),
            end = availableStations.constEnd(); add != end; ++add) {
        station->addItem(add->toUpper());
    }
    station->lineEdit()->setText(selectedIdentity.getStationQString().toUpper());
    layout->addRow(tr("Station:", "edit unit station"), station);

    QComboBox *archive = new QComboBox(innerWidget);
    archive->setEditable(true);
    archive->lineEdit()->setMaxLength(126);
    archive->setInsertPolicy(QComboBox::NoInsert);
    archive->clear();
    for (QList<QString>::const_iterator add = availableArchives.constBegin(),
            end = availableArchives.constEnd(); add != end; ++add) {
        archive->addItem(add->toUpper());
    }
    archive->lineEdit()->setText(selectedIdentity.getArchiveQString().toUpper());
    layout->addRow(tr("Archive:", "edit unit archive"), archive);

    QComboBox *variable = new QComboBox(innerWidget);
    variable->setEditable(true);
    variable->lineEdit()->setMaxLength(126);
    variable->setInsertPolicy(QComboBox::NoInsert);
    variable->clear();
    for (QList<QString>::const_iterator add = availableVariables.constBegin(),
            end = availableVariables.constEnd(); add != end; ++add) {
        variable->addItem(*add);
    }
    variable->lineEdit()->setText(selectedIdentity.getVariableQString());
    layout->addRow(tr("Variable:", "edit unit variable"), variable);

    QComboBox *flavors = new QComboBox(innerWidget);
    flavors->setEditable(true);
    flavors->lineEdit()->setMaxLength(126);
    flavors->setInsertPolicy(QComboBox::NoInsert);
    flavors->setToolTip(tr("Enter multiple flavors separated by spaces."));
    flavors->clear();
    for (QList<QString>::const_iterator add = availableFlavors.constBegin(),
            end = availableFlavors.constEnd(); add != end; ++add) {
        flavors->addItem(*add);
    }
    {
        QStringList setFlavors;
        for (const auto &add : selectedIdentity.getFlavors()) {
            setFlavors.append(QString::fromStdString(add).toUpper());
        }
        std::sort(setFlavors.begin(), setFlavors.end());
        flavors->lineEdit()->setText(setFlavors.join(QString(' ')));
    }
    layout->addRow(tr("Flavors:", "edit unit variable"), flavors);

    input.exec();
    if (input.result() != QDialog::Accepted)
        return;

    SequenceName name
            (station->lineEdit()->text().toStdString(), archive->lineEdit()->text().toStdString(),
             variable->lineEdit()->text().toStdString(), Util::set_from_qstring(
                    flavors->lineEdit()->text().split(QRegExp("\\s"), QString::SkipEmptyParts)));

    if (name == selectedIdentity.getName())
        return;

    SequenceIdentity modified = selectedIdentity;
    modified.setName(name);
    while (values.count(modified) != 0) {
        modified.setPriority(modified.getPriority() + 1);
    }

    Variant::Root extracted;
    {
        auto f = values.find(selectedIdentity);
        if (f != values.end()) {
            extracted = std::move(f->second);
            values.erase(f);
        }
    }
    values.emplace(modified, std::move(extracted));
    updateTimelineValues();
    selectValue(modified);

    changesMade = true;
    emit dataModified();
}

void DataEditor::addPressed()
{
    SequenceIdentity add;

    if (selectedIdentity.isValid()) {
        add.setName(selectedIdentity.getName());
    } else if (station.length() > 0 &&
            QRegExp::escape(QString::fromStdString(station)).toStdString() == station &&
            archive.length() > 0 &&
            QRegExp::escape(QString::fromStdString(archive)).toStdString() == archive &&
            variable.length() > 0 &&
            QRegExp::escape(QString::fromStdString(variable)).toStdString() == variable) {
        SequenceName::Flavors flavors;
        for (const auto &addF : hasFlavors) {
            if (QRegExp::escape(QString::fromStdString(addF)).toStdString() == addF) {
                flavors.insert(addF);
            }
        }
        add.setName(SequenceName(station, archive, variable, std::move(flavors)));
    } else if (!values.empty()) {
        add.setName(values.cbegin()->first.getName());
    } else {
        add.setName(SequenceName("nil", "configuration", "none"));
    }

    if (selectedIdentity.isValid()) {
        add.setStart(selectedIdentity.getStart());
        add.setEnd(selectedIdentity.getEnd());
        add.setPriority(selectedIdentity.getPriority() + 1);
    } else {
        add.setStart(start);
        add.setEnd(end);
        add.setPriority(0);
    }

    while (values.count(add) != 0) {
        add.setPriority(add.getPriority() + 1);
    }

    values.emplace(add, Variant::Root());
    updateTimelineValues();
    timeline->ensureVisible(add);
    selectValue(add);

    changesMade = true;
    emit dataModified();
}


void DataEditor::removePressed()
{
    if (!selectedIdentity.isValid())
        return;

    values.erase(selectedIdentity);
    clearSelection();
    updateTimelineValues();

    changesMade = true;
    emit dataModified();
}

void DataEditor::selectedTimesEdited(double s, double e)
{
    if (!selectedIdentity.isValid())
        return;
    SequenceValue modified(selectedIdentity);
    if (FP::equal(s, modified.getStart()) && FP::equal(e, modified.getEnd()))
        return;
    modified.setStart(s);
    modified.setEnd(e);
    while (values.count(modified) != 0) {
        modified.setPriority(modified.getPriority() + 1);
    }

    Variant::Root extracted;
    {
        auto f = values.find(selectedIdentity);
        if (f != values.end()) {
            extracted = std::move(f->second);
            values.erase(f);
        }
    }
    values.emplace(modified, std::move(extracted));
    selectedIdentity = modified;
    updateTimelineValues();
    timeline->highlightValue(selectedIdentity);

    changesMade = true;
    emit dataModified();
}

void DataEditor::selectedPriorityEdited(int p)
{
    if (!selectedIdentity.isValid())
        return;
    SequenceValue modified(selectedIdentity);
    int oldPriority = modified.getPriority();
    if (oldPriority == p)
        return;
    modified.setPriority(p);
    if (p > oldPriority) {
        while (values.count(modified) != 0) {
            modified.setPriority(modified.getPriority() + 1);
        }
    } else {
        while (values.count(modified) != 0) {
            modified.setPriority(modified.getPriority() - 1);
        }
    }

    Variant::Root extracted;
    {
        auto f = values.find(selectedIdentity);
        if (f != values.end()) {
            extracted = std::move(f->second);
            values.erase(f);
        }
    }
    values.emplace(modified, std::move(extracted));
    selectedIdentity = modified;
    updateTimelineValues();
    timeline->highlightValue(selectedIdentity);

    changesMade = true;
    emit dataModified();
}

void DataEditor::valueModified()
{
    if (!selectedIdentity.isValid())
        return;

    changesMade = true;
    emit dataModified();
}

}
}
}
