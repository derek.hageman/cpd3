/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUIDATAVARIABLESELECT_H
#define CPD3GUIDATAVARIABLESELECT_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QWidget>
#include <QStringList>
#include <QSet>
#include <QTabWidget>
#include <QListWidget>
#include <QTableView>
#include <QItemDelegate>
#include <QPointer>
#include <QComboBox>
#include <QStandardItemModel>
#include <QPushButton>
#include <QLabel>
#include <QMenu>
#include <QAction>
#include <QFuture>

#include "guidata/guidata.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/stream.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/selection.hxx"

namespace CPD3 {
namespace GUI {
namespace Data {

class VariableSelect;

namespace Internal {

class VariableSelectAddRemoveDelegate : public QItemDelegate {
Q_OBJECT

    VariableSelect *select;
public:
    VariableSelectAddRemoveDelegate(VariableSelect *vs, QObject *parent = 0);

    virtual ~VariableSelectAddRemoveDelegate();

    virtual QWidget *createEditor(QWidget *parent,
                                  const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const;

    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const;

    virtual void setModelData(QWidget *editor,
                              QAbstractItemModel *model,
                              const QModelIndex &index) const;
};

class VariableSelectComboBoxWidget : public QComboBox {
Q_OBJECT

public:
    VariableSelectComboBoxWidget(QWidget *parent = 0);

    int selectedIndex() const;

    QString selectedText() const;

    void connectHandlers();

    void disconnectHandlers();

private slots:

    void textEdited();

    void itemSelected();

signals:

    void changed(QWidget *widget);
};

class VariableSelectComboBoxDelegate : public QItemDelegate {
Q_OBJECT

    VariableSelect *select;
    QList<QPointer<VariableSelectComboBoxWidget> > *target;
public:
    VariableSelectComboBoxDelegate(VariableSelect *vs,
                                   QList<QPointer<VariableSelectComboBoxWidget> > *target,
                                   QObject *parent = 0);

    virtual ~VariableSelectComboBoxDelegate();

    virtual QWidget *createEditor(QWidget *parent,
                                  const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const;

    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const;

    virtual void setModelData(QWidget *editor,
                              QAbstractItemModel *model,
                              const QModelIndex &index) const;
};

class VariableSelectFlavorsListButton : public QPushButton {
Q_OBJECT

    QMenu *menu;
    QHash<QString, QAction *> flavorActions;
    QAction *addSeparator;
    QAction *addFlavor;
public:
    VariableSelectFlavorsListButton(const QString &text, QWidget *parent = 0);

    virtual ~VariableSelectFlavorsListButton();

    void setAvailable(const QStringList &available);

    void setSelection(const QSet<QString> &selected);

    QSet<QString> getSelection() const;

signals:

    void changed();

private slots:

    void showAddDialog();
};

class VariableSelectFlavorsWidget : public QWidget {
Q_OBJECT

    QComboBox *modeSelect;

    VariableSelectFlavorsListButton *has;
    VariableSelectFlavorsListButton *lacks;
    VariableSelectFlavorsListButton *exact;

    void connectHandlers();

    void disconnectHandlers();

public:
    VariableSelectFlavorsWidget(QWidget *parent = 0);

    virtual ~VariableSelectFlavorsWidget();

    QVariant getData() const;

    void setData(const QVariant &data);

    void setAvailable(const QStringList &available);

private slots:

    void commitChanges();

    void currentIndexChanged();

signals:

    void changed(QWidget *widget);
};

class VariableSelectFlavorsDelegate : public QItemDelegate {
Q_OBJECT

    VariableSelect *select;
    QList<QPointer<VariableSelectFlavorsWidget> > *target;
public:
    VariableSelectFlavorsDelegate(VariableSelect *vs,
                                  QList<QPointer<VariableSelectFlavorsWidget> > *target,
                                  QObject *parent = 0);

    virtual ~VariableSelectFlavorsDelegate();

    virtual QWidget *createEditor(QWidget *parent,
                                  const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const;

    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const;

    virtual void setModelData(QWidget *editor,
                              QAbstractItemModel *model,
                              const QModelIndex &index) const;
};

}

/**
 * A widget providing variable selection.
 */
class CPD3GUIDATA_EXPORT VariableSelect : public QWidget {
Q_OBJECT


    QStringList availableStations;
    QStringList availableArchives;
    QStringList availableVariables;
    QStringList availableFlavors;


    QTabWidget *tabWidget;
    QListWidget *basicList;

    QStandardItemModel *advancedModel;
    QTableView *advancedTable;

    struct InstrumentSelection {
        QString name;
        QString match;
        QStringList require;

        inline InstrumentSelection(const QString &n,
                                   const QString &m,
                                   const QStringList &r = QStringList()) : name(n),
                                                                           match(m),
                                                                           require(r)
        { }
    };

    QList<InstrumentSelection> instrumentSelectionTypes;
    QMenu *instrumentSelectionMenu;

    QList<QPointer<Internal::VariableSelectComboBoxWidget> > stationEditors;
    QList<QPointer<Internal::VariableSelectComboBoxWidget> > archiveEditors;
    QList<QPointer<Internal::VariableSelectComboBoxWidget> > variableEditors;
    QList<QPointer<Internal::VariableSelectFlavorsWidget> > flavorsEditors;

    void disconnectUpdateEvents();

    void connectUpdateEvents();

    void updateBasicList();

    void openAllEditors();

    void populateStations();

    void populateArchives();

    void populateVariables();

    void populateFlavors();

    void selectEffectiveTab();

    void reconfigureComboBoxes();

    friend class Internal::VariableSelectComboBoxDelegate;

    friend class Internal::VariableSelectFlavorsDelegate;

public:
    VariableSelect(QWidget *parent = 0);

    virtual ~VariableSelect();

    /**
     * A class describing the defaults used when a variable selection is
     * instantiated.
     */
    class CPD3GUIDATA_EXPORT SequenceMatchDefaults {
        bool archiveFanoutEnabled;
        QStringList archiveFanout;

        bool forceHasLacks;
        QStringList forceHasFlavors;
        QStringList forceLacksFlavors;

        friend class VariableSelect;

    public:
        SequenceMatchDefaults();

        SequenceMatchDefaults(const SequenceMatchDefaults &other);

        SequenceMatchDefaults &operator=(const SequenceMatchDefaults &other);

        /**
         * Set the selection to default to fanning out to the given archives.
         * 
         * @param archives  the archives to fan out to
         */
        void setArchiveFanout(const QStringList &archives);

        /**
         * Set the selection to default to given has and lacks flavors
         * settings.
         * 
         * @param hasFlavors    the flavors that are required
         * @param lacksFlavors  the flavors that are excluded
         */
        void setFlavorsSelection(const QStringList &hasFlavors, const QStringList &lacksFlavors);


    };

    /**
     * Configure the selection from settings describing a variable selection.
     * 
     * @param config        the configuration
     * @param defaults      the defaults used when the selection is created
     */
    void configureFromSequenceMatch(const CPD3::Data::Variant::Read &config,
                                    const SequenceMatchDefaults &defaults = SequenceMatchDefaults());

    /**
     * Write out the configuration to a composite sequence match.
     * 
     * @param target        the output target
     * @param defaults      the defaults that will be used when the selection is created
     */
    void writeSequenceMatch(CPD3::Data::Variant::Write &target,
                            const SequenceMatchDefaults &defaults = SequenceMatchDefaults());

    inline void writeSequenceMatch(CPD3::Data::Variant::Write &&target,
                                   const SequenceMatchDefaults &defaults = SequenceMatchDefaults())
    { return writeSequenceMatch(target, defaults); }

    /**
     * Write out the configuration of a sequence match to a string specification.
     *
     * @param target        the output target
     * @param defaults      the defaults that will be used when the selection is created
     */
    void writeSequenceMatch(QString &target,
                            const SequenceMatchDefaults &defaults = SequenceMatchDefaults());

    /**
     * A class describing the defaults used when a dynamic selection
     * is instantiated.
     */
    class CPD3GUIDATA_EXPORT DynamicSelectionDefaults {
    public:
        DynamicSelectionDefaults();

        DynamicSelectionDefaults(const DynamicSelectionDefaults &other);

        DynamicSelectionDefaults &operator=(const DynamicSelectionDefaults &other);
    };

    /**
     * Configure the selection from settings describing a dynamic operate
     * selection.
     * 
     * @param config        the section configuration
     * @param defaults      the defaults used by the filter operate
     */
    void configureFromDynamicSelection(const CPD3::Data::Variant::Read &config,
                                       const DynamicSelectionDefaults &defaults = DynamicSelectionDefaults());

    /**
     * Write out the configuration for a dynamic operate section.
     * 
     * @param target        the target to write out to
     * @param defaults      the defaults used by the filter operate
     */
    void writeDynamicSelection(CPD3::Data::Variant::Write &target,
                               const DynamicSelectionDefaults &defaults = DynamicSelectionDefaults());

    inline void writeDynamicSelection(CPD3::Data::Variant::Write &&target,
                                      const DynamicSelectionDefaults &defaults = DynamicSelectionDefaults())
    { return writeDynamicSelection(target, defaults); }


    /**
     * A class describing the defaults used when a archive selection
     * is instantiated.
     */
    class CPD3GUIDATA_EXPORT ArchiveDefaults {
        CPD3::Data::SequenceName::Component station;
        CPD3::Data::SequenceName::Component archive;
        bool emptySelectsEverything;

        friend class VariableSelect;

    public:
        ArchiveDefaults();

        ArchiveDefaults(const ArchiveDefaults &other);

        ArchiveDefaults &operator=(const ArchiveDefaults &other);

        /**
         * Set the station used in basic mode when none is specified.
         * 
         * @param station   the station to use
         */
        void setStation(const CPD3::Data::SequenceName::Component &station);

        /**
         * Set the archive used in basic mode when none is specified.
         * 
         * @param archive   the archive to use
         */
        void setArchive(const CPD3::Data::SequenceName::Component &archive);

        /**
         * Set the if an empty selection applies to everything.
         * 
         * @param enable    true if the selection applies to everything
         */
        void setEmptySelectAll(bool enable);
    };

    /**
     * Configure the selection from a list of archive selections.
     * 
     * @param selections    the archive selections
     * @param defaults      the defaults used by the archive
     */
    void configureFromArchiveSelection(const CPD3::Data::Archive::Selection::List &selections,
                                       const ArchiveDefaults &defaults = ArchiveDefaults());

    /**
     * Get the archive selections.
     * 
     * @param defaults      the defaults used by the archive selections
     * @param start         the start time to set
     * @param end           the end time to set
     * @return              a list of selections
     */
    CPD3::Data::Archive::Selection::List getArchiveSelection(const ArchiveDefaults &defaults = ArchiveDefaults(),
                                                             double start = FP::undefined(),
                                                             double end = FP::undefined());

public slots:

    /**
     * Set the currently available stations.
     *
     * @param set   the available stations
     */
    void setAvailableStations(const CPD3::Data::SequenceName::ComponentSet &set);

    /**
     * Set the currently available archives.
     *
     * @param set   the available archives
     */
    void setAvailableArchives(const CPD3::Data::SequenceName::ComponentSet &set);

    /**
     * Set the currently available variables.
     *
     * @param set   the available variables
     */
    void setAvailableVariables(const CPD3::Data::SequenceName::ComponentSet &set);

    /**
     * Set the currently available flavors.
     *
     * @param set   the available flavors
     */
    void setAvailableFlavors(const CPD3::Data::SequenceName::ComponentSet &set);

    /**
     * Load all available components from the default archive.
     */
    void setDefaultAvailable();

private:
    void addEntryFromString(const QString &str, const SequenceMatchDefaults &defaults);

    void addSelectionFromValue(const CPD3::Data::Variant::Read &config,
                               const SequenceMatchDefaults &defaults);

    void simplifySelectionDefaults(const SequenceMatchDefaults &defaults);

    void addEntryFromSelectionStation(const CPD3::Data::Archive::Selection &selection,
                                      const ArchiveDefaults &defaults,
                                      const QString &station = QString());

    QList<QList<
            QStandardItem *> > addEntryFromSelectionArchive(const CPD3::Data::Archive::Selection &selection,
                                                            const ArchiveDefaults &defaults,
                                                            const QString &archive = QString());

    QList<QList<
            QStandardItem *> > addEntryFromSelectionVariable(const CPD3::Data::Archive::Selection &selection,
                                                             const ArchiveDefaults &defaults,
                                                             const QString &variable = QString());

signals:

    /**
     * Emitted whenever the selection has been altered.
     */
    void selectionChanged();

private slots:

    void removeAdvancedRow();

    void addAdvancedRow();

    void groupSelected(QAction *action);

    void clearBasicSelection();

    void scrollToBasicSelection();
};

}
}
}

#endif
