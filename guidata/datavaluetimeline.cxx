/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <algorithm>
#include <QtAlgorithms>
#include <QGridLayout>
#include <QWidget>
#include <QPainter>
#include <QScrollArea>
#include <QMouseEvent>
#include <QToolTip>
#include <QApplication>
#include <QPainterPath>

#include "guidata/datavaluetimeline.hxx"
#include "guicore/guiformat.hxx"
#include "graphing/axislabelengine2d.hxx"
#include "core/timeutils.hxx"
#include "core/range.hxx"

using namespace CPD3::Data;
using namespace CPD3::Graphing;
using namespace CPD3::GUI::Data::Internal;

namespace CPD3 {
namespace GUI {
namespace Data {

/** @file guidata/datavaluetimeline.hxx
 * Provides a timeline widget for SequenceValue display.
 */



static bool listContains(const SequenceIdentity::Transfer &list, const SequenceIdentity &check)
{
    auto e = list.end();
    return std::find(list.begin(), e, check) != e;
}

namespace Internal {

namespace {
class Transformer {
    double before;
    double after;
    double scale;
public:
    Transformer(const QRect &area, double begin, double end)
    {
        Q_ASSERT(FP::defined(begin));
        Q_ASSERT(FP::defined(end));
        Q_ASSERT(begin != end);
        scale = area.width() / (end - begin);
        before = -begin;
        after = area.left();
    }

    inline double get(double position) const
    {
        Q_ASSERT(FP::defined(position));
        return (position + before) * scale + after;
    }
};
}

static const QChar infinity(0x221E);

class DataValueTimelineAxis : public QWidget {
    DataValueTimeline *parent;

    AxisLabelEngine2D *engine;
    double majorTickHeight;
    double minorTickHeight;
    double labelInset;

public:
    DataValueTimelineAxis(DataValueTimeline *p) : QWidget(p), parent(p)
    {
        setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum));

        engine = new AxisLabelEngine2D(Axis2DTop, this);

        QFont baseFont(engine->getLabelFont());
        QFontMetrics fmBase(baseFont);
        majorTickHeight = ::ceil(fmBase.height() * 0.25);
        minorTickHeight = ::ceil(majorTickHeight * 0.5);
        labelInset = ::ceil(majorTickHeight);
    }

    virtual QSize sizeHint() const
    {
        double labelHeight = engine->predictSize(parent->majorTicks);

        if (parent->haveInfiniteStart || parent->haveInfiniteEnd) {
            QFontMetrics fm(engine->getLabelFont());
            QRect cDims(GUIStringLayout::maximumCharacterDimensions(infinity, fm));
            if (labelHeight < cDims.height() + 2)
                labelHeight = cDims.height() + 2;
        }

        double totalHeight = labelHeight;

        if (!parent->majorTicks.empty() || !parent->minorTicks.empty()) {
            if (labelHeight > 0.0)
                totalHeight += labelInset;
            totalHeight += qMax(majorTickHeight, minorTickHeight) + 1;
        }

        totalHeight += 1.0;
        return QSize(600, (int) ::ceil(totalHeight));
    }

protected:
    virtual void paintEvent(QPaintEvent *event)
    {
        Q_UNUSED(event);

        QPainter painter(this);

        QRect totalArea(geometry());
        painter.drawLine(0, totalArea.height() - 1, totalArea.width(), totalArea.height() - 1);

        totalArea.setLeft(totalArea.left() + 1);
        totalArea.setRight(totalArea.right() - 1);

        qreal baseline = totalArea.height() - 1;

        if (parent->haveInfiniteStart) {
            QFontMetrics fm(QApplication::font());
            qreal width = fm.width(infinity);
            width *= 2;

            painter.setFont(engine->getLabelFont());
            QRect cDims(GUIStringLayout::maximumCharacterDimensions(infinity, fm));
            QRectF textPos(painter.boundingRect(QRectF(totalArea.left() + width / 2.0, 0, 0, 0),
                                                Qt::TextDontClip | Qt::AlignHCenter, infinity));
            textPos.moveTop(baseline + cDims.y() - 2);
            if (textPos.left() < totalArea.left())
                textPos.moveLeft(totalArea.left());
            painter.drawText(textPos, Qt::TextDontClip, infinity);

            totalArea.setLeft((int) ::ceil(totalArea.left() + width));

            QPen pen(palette().color(QPalette::WindowText));
            pen.setStyle(Qt::DotLine);
            painter.setPen(pen);
            painter.drawLine(totalArea.left(), 0, totalArea.left(), totalArea.height() - 1);
        }

        if (parent->haveInfiniteEnd) {
            QFontMetrics fm(QApplication::font());
            qreal width = fm.width(infinity);
            width *= 2;

            painter.setFont(engine->getLabelFont());
            QRect cDims(GUIStringLayout::maximumCharacterDimensions(infinity, fm));
            QRectF textPos(painter.boundingRect(QRectF(totalArea.right() - width / 2.0, 0, 0, 0),
                                                Qt::TextDontClip | Qt::AlignHCenter, infinity));
            textPos.moveTop(baseline + cDims.y() - 2);
            if (textPos.right() > totalArea.right())
                textPos.moveLeft(totalArea.right());
            painter.drawText(textPos, Qt::TextDontClip, infinity);

            totalArea.setRight((int) ::floor(totalArea.right() - width));

            QPen pen(palette().color(QPalette::WindowText));
            pen.setStyle(Qt::DotLine);
            painter.setPen(pen);
            painter.drawLine(totalArea.right(), 0, totalArea.right(), totalArea.height() - 1);
        }

        if (!FP::defined(parent->displayStart) ||
                !FP::defined(parent->displayEnd) ||
                parent->displayStart >= parent->displayEnd)
            return;
        if (!totalArea.isValid() || totalArea.width() <= 1)
            return;

        painter.setPen(palette().color(QPalette::WindowText));

        Transformer tr(totalArea, parent->displayStart, parent->displayEnd);

        auto minorTicks = parent->minorTicks;
        for (auto &t : minorTicks) {
            t.point = tr.get(t.point);
            if (!FP::defined(t.point))
                continue;

            int pos = (int) ::floor(t.point + 0.5);
            int top = (int) ::ceil(totalArea.bottom() - minorTickHeight);
            painter.drawLine(pos, totalArea.bottom(), pos, top);
        }

        auto majorTicks = parent->majorTicks;
        for (auto &t : majorTicks) {
            t.point = tr.get(t.point);
            if (!FP::defined(t.point))
                continue;

            int pos = (int) ::floor(t.point + 0.5);
            int top = (int) ::ceil(totalArea.bottom() - majorTickHeight);
            painter.drawLine(pos, totalArea.bottom(), pos, top);
        }

        if (!majorTicks.empty() || !minorTicks.empty()) {
            baseline -= qMax(majorTickHeight, minorTickHeight) + 1;
        }

        engine->paint(majorTicks, &painter, baseline, totalArea.left(), totalArea.right() + 1);
    }
};


class DataValueTimelineValues : public QWidget {
    DataValueTimeline *parent;

    double layerHeight;
    double layerSpacing;

    void paintValue(QPainter &painter,
                    double top,
                    double left,
                    double right,
                    bool highlighted,
                    bool isDefault) const
    {
        QPainterPath path;

        double bottom = top + layerHeight;

        path.moveTo(left, top);
        path.lineTo(right, top);
        path.lineTo(right, bottom);
        path.lineTo(left, bottom);
        path.closeSubpath();

        QBrush brush;
        QPen pen;
        if (highlighted) {
            //brush = palette().color(QPalette::Midlight);
            brush = palette().color(QPalette::Dark);
            pen = palette().color(QPalette::BrightText);
        } else {
            brush = palette().color(QPalette::Mid);
            pen = palette().color(QPalette::WindowText);
        }

        if (isDefault) {
            pen.setStyle(Qt::DashLine);
        }

        painter.setBrush(brush);
        painter.setPen(pen);
        painter.drawPath(path);
    }

    static bool transformValue(const QRect &totalArea,
                               const QRect &timeArea,
                               const Transformer &tr,
                               double minimumWidth,
                               const SequenceIdentity &value,
                               double &left,
                               double &right)
    {
        if (!FP::defined(value.getStart())) {
            left = totalArea.left();
        } else {
            left = tr.get(value.getStart());
            if (left < timeArea.left())
                left = timeArea.left();
            if (left >= timeArea.right())
                return false;
        }

        if (!FP::defined(value.getEnd())) {
            right = totalArea.right();
        } else {
            right = tr.get(value.getEnd());
            if (right > timeArea.right())
                right = timeArea.right();
            if (right <= timeArea.left())
                return false;
        }

        left += 1;
        right -= 1;
        double width = right - left;
        if (width < minimumWidth) {
            double add = minimumWidth - width;
            add /= 2;
            left -= add;
            right += add;
        }

        return true;
    }

    bool displayLevel(int level) const
    {
        if (parent->scrollBar->maximum() > 0) {
            if (level < parent->scrollBar->value())
                return false;
        }
        return true;
    }

    void initializeDrawArea(QRect &totalArea, QRect &timeArea, double &minimumWidth) const
    {
        totalArea = geometry();
        timeArea = totalArea;

        if (parent->haveInfiniteStart) {
            QFontMetrics fm(QApplication::font());
            qreal width = fm.width(infinity);
            width *= 2;

            timeArea.setLeft((int) ::ceil(timeArea.left() + width));
        }

        if (parent->haveInfiniteEnd) {
            QFontMetrics fm(QApplication::font());
            qreal width = fm.width(infinity);
            width *= 2;

            timeArea.setRight((int) ::floor(timeArea.right() - width));
        }

        minimumWidth = timeArea.width() * 0.025;
        if (minimumWidth < 3)
            minimumWidth = 3;
    }

    bool valueForPoint(const QPoint &point, SequenceIdentity &value) const
    {
        QRect totalArea;
        QRect timeArea;
        double minimumWidth;
        initializeDrawArea(totalArea, timeArea, minimumWidth);

        if (!FP::defined(parent->displayStart) ||
                !FP::defined(parent->displayEnd) ||
                parent->displayStart >= parent->displayEnd ||
                !timeArea.isValid() ||
                timeArea.width() <= 1) {
            double top = 0;
            int levelCounter = 0;
            for (auto level = parent->defaultLayoutValues.cbegin(),
                    endL = parent->defaultLayoutValues.cend();
                    level != endL;
                    ++level, ++levelCounter) {
                if (top >= timeArea.height())
                    break;
                if (point.y() < top)
                    break;
                if (displayLevel(levelCounter)) {
                    double bottom = top + layerHeight;
                    if (point.y() >= top && point.y() <= bottom) {
                        for (const auto &v : *level) {
                            value = v;
                            return true;
                        }
                    }
                    top += layerHeight + layerSpacing;
                }
            }
            for (auto level = parent->mainLayoutValues.cbegin(),
                    endL = parent->mainLayoutValues.cend();
                    level != endL;
                    ++level, ++levelCounter) {
                if (top >= timeArea.height())
                    break;
                if (displayLevel(levelCounter)) {
                    double bottom = top + layerHeight;
                    if (point.y() >= top && point.y() <= bottom) {
                        for (const auto &v : *level) {
                            value = v;
                            return true;
                        }
                    }
                    top += layerHeight + layerSpacing;
                }
            }
            return false;
        }

        Transformer tr(timeArea, parent->displayStart, parent->displayEnd);

        double top = 0;
        int levelCounter = 0;
        for (auto level = parent->defaultLayoutValues.cbegin(),
                endL = parent->defaultLayoutValues.cend(); level != endL; ++level, ++levelCounter) {
            if (top >= timeArea.height())
                break;
            if (point.y() < top)
                break;
            if (displayLevel(levelCounter)) {
                double bottom = top + layerHeight;
                if (point.y() >= top && point.y() <= bottom) {
                    for (const auto &v : *level) {
                        double left;
                        double right;
                        if (!transformValue(totalArea, timeArea, tr, minimumWidth, v, left, right))
                            continue;
                        if (point.x() < left || point.x() > right)
                            continue;
                        value = v;
                        return true;
                    }
                }
                top += layerHeight + layerSpacing;
            }
        }
        for (auto level = parent->mainLayoutValues.cbegin(), endL = parent->mainLayoutValues.cend();
                level != endL;
                ++level, ++levelCounter) {
            if (top >= timeArea.height())
                break;
            if (point.y() < top)
                break;
            if (displayLevel(levelCounter)) {
                double bottom = top + layerHeight;
                if (point.y() >= top && point.y() <= bottom) {
                    for (const auto &v : *level) {
                        double left;
                        double right;
                        if (!transformValue(totalArea, timeArea, tr, minimumWidth, v, left, right))
                            continue;
                        if (point.x() < left || point.x() > right)
                            continue;
                        value = v;
                        return true;
                    }
                }
                top += layerHeight + layerSpacing;
            }
        }

        return false;
    }

    QString itemToolTip(const SequenceIdentity &item) const
    {
        QString result(DataValueTimeline::tr("Effective: <b>%1</b> to: <b>%2</b><br>Priority: %3",
                                             "data value tooltip base").arg(GUITime::formatTime(
                                                                               item.getStart(),
                                                                               &parent->settings))
                                                                       .arg(GUITime::formatTime(
                                                                               item.getEnd(),
                                                                               &parent->settings))
                                                                       .arg(item.getPriority()));
        if (item.getName().isDefaultStation())
            result.append(DataValueTimeline::tr("<br><br><b>Global default</b>",
                                                "data value default station"));
        return result;
    }

public:
    DataValueTimelineValues(DataValueTimeline *p) : QWidget(p), parent(p)
    {
        setSizePolicy(QSizePolicy(QSizePolicy::Minimum, QSizePolicy::MinimumExpanding));

        QFont baseFont(QApplication::font());
        QFontMetrics fmBase(baseFont);
        layerHeight = ::ceil(fmBase.height() * 1.25);
        layerSpacing = ::ceil(layerHeight * 0.1);
    }

    virtual QSize sizeHint() const
    {
        int totalLayers = parent->defaultLayoutValues.size() + parent->mainLayoutValues.size();
        if (totalLayers > 6)
            totalLayers = 6;
        if (totalLayers < 1)
            totalLayers = 1;

        double totalHeight = layerHeight * totalLayers;
        if (totalLayers > 1)
            totalHeight += layerSpacing * (totalLayers - 1);

        return QSize(100, (int) ::ceil(totalHeight) + 2);
    }

protected:
    virtual void paintEvent(QPaintEvent *event)
    {
        Q_UNUSED(event);

        QPainter painter(this);

        QRect totalArea;
        QRect timeArea;
        double minimumWidth;
        initializeDrawArea(totalArea, timeArea, minimumWidth);

        if (!FP::defined(parent->displayStart) ||
                !FP::defined(parent->displayEnd) ||
                parent->displayStart >= parent->displayEnd ||
                !timeArea.isValid() ||
                timeArea.width() <= 1) {
            double top = 0;
            int levelCounter = 0;
            for (auto level = parent->defaultLayoutValues.cbegin(),
                    endL = parent->defaultLayoutValues.cend();
                    level != endL;
                    ++level, ++levelCounter) {
                if (top >= timeArea.height())
                    break;
                if (displayLevel(levelCounter)) {
                    for (const auto &v : *level) {
                        paintValue(painter, top, totalArea.left(), totalArea.right(),
                                   parent->highlight.count(v) != 0, true);
                    }
                    top += layerHeight + layerSpacing;
                }
            }
            for (auto level = parent->mainLayoutValues.cbegin(),
                    endL = parent->mainLayoutValues.cend();
                    level != endL;
                    ++level, ++levelCounter) {
                if (top >= timeArea.height())
                    break;
                if (displayLevel(levelCounter)) {
                    for (const auto &v : *level) {
                        paintValue(painter, top, totalArea.left(), totalArea.right(),
                                   parent->highlight.count(v) != 0, false);
                    }
                    top += layerHeight + layerSpacing;
                }
            }
            return;
        }

        Transformer tr(timeArea, parent->displayStart, parent->displayEnd);

        double top = 0;
        int levelCounter = 0;
        for (auto level = parent->defaultLayoutValues.cbegin(),
                endL = parent->defaultLayoutValues.cend(); level != endL; ++level, ++levelCounter) {
            if (top >= timeArea.height())
                break;
            if (displayLevel(levelCounter)) {
                for (const auto &v : *level) {
                    double left;
                    double right;
                    if (!transformValue(totalArea, timeArea, tr, minimumWidth, v, left, right))
                        continue;

                    paintValue(painter, top, left, right, parent->highlight.count(v) != 0, true);
                }
                top += layerHeight + layerSpacing;
            }
        }
        for (auto level = parent->mainLayoutValues.cbegin(), endL = parent->mainLayoutValues.cend();
                level != endL;
                ++level, ++levelCounter) {
            if (top >= timeArea.height())
                break;
            if (displayLevel(levelCounter)) {
                for (const auto &v : *level) {
                    double left;
                    double right;
                    if (!transformValue(totalArea, timeArea, tr, minimumWidth, v, left, right))
                        continue;

                    paintValue(painter, top, left, right, parent->highlight.count(v) != 0, false);
                }
                top += layerHeight + layerSpacing;
            }
        }
    }

    virtual void mouseReleaseEvent(QMouseEvent *event)
    {
        SequenceIdentity v;
        if (valueForPoint(event->pos(), v)) {
            parent->clickValue(v, event->button());
            return;
        }
        QWidget::mouseReleaseEvent(event);
    }

    virtual bool event(QEvent *event)
    {
        if (event->type() == QEvent::ToolTip) {
            SequenceIdentity v;
            QHelpEvent *helpEvent = static_cast<QHelpEvent *>(event);
            if (valueForPoint(helpEvent->pos(), v)) {
                QToolTip::showText(helpEvent->globalPos(), itemToolTip(v));
                return true;
            } else {
                QToolTip::hideText();
                event->ignore();
                return true;
            }

        }
        return QWidget::event(event);
    }
};

}

DataValueTimeline::DataValueTimeline(QWidget *parent) : QWidget(parent),
                                                        values(),
                                                        highlight(),
                                                        autoranging(true),
                                                        visibleStart(FP::undefined()),
                                                        visibleEnd(FP::undefined()),
                                                        displayStart(FP::undefined()),
                                                        displayEnd(FP::undefined()),
                                                        haveInfiniteStart(true),
                                                        haveInfiniteEnd(true),
                                                        settings(CPD3GUI_ORGANIZATION,
                                                                 CPD3GUI_APPLICATION)
{

    QGridLayout *layout = new QGridLayout(this);
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);


    axisDisplay = new DataValueTimelineAxis(this);
    layout->addWidget(axisDisplay, 0, 0);

    valuesDisplay = new DataValueTimelineValues(this);
    layout->addWidget(valuesDisplay, 1, 0);

    scrollBar = new QScrollBar(this);
    layout->addWidget(scrollBar, 1, 1);
    scrollBar->setRange(0, 0);
    scrollBar->hide();
    scrollBar->setTracking(true);
    connect(scrollBar, SIGNAL(valueChanged(int)), valuesDisplay, SLOT(update()));

    layout->setRowStretch(1, 1);
    layout->setColumnStretch(0, 1);

    axisGenerator = new AxisTickGeneratorTime(&settings, this);
}

DataValueTimeline::~DataValueTimeline() = default;

void DataValueTimeline::regenerateTicks()
{
    majorTicks.clear();
    minorTicks.clear();

    if (!FP::defined(displayStart) || !FP::defined(displayEnd))
        return;

    majorTicks = axisGenerator->generate(displayStart, displayEnd, AxisTickGenerator::MajorTick);
    if (majorTicks.size() < 2)
        return;

    for (auto prior = majorTicks.cbegin(), tick = prior + 1, endTicks = majorTicks.cend();
            tick != endTicks;
            prior = tick, ++tick) {
        Util::append(axisGenerator->generate(prior->getPoint(), tick->getPoint(),
                                             AxisTickGenerator::MinorTick), minorTicks);
    }
}

void DataValueTimeline::updateRanges()
{

    haveInfiniteStart = false;
    haveInfiniteEnd = false;
    displayStart = FP::undefined();
    displayEnd = FP::undefined();

    for (const auto &v : values) {
        double s = v.getStart();
        if (!FP::defined(s)) {
            haveInfiniteStart = true;
        } else {
            if (!FP::defined(displayStart) || s < displayStart)
                displayStart = s;
            if (!FP::defined(displayEnd) || s > displayEnd)
                displayEnd = s;
        }

        double e = v.getEnd();
        if (!FP::defined(e)) {
            haveInfiniteEnd = true;
        } else {
            if (!FP::defined(displayEnd) || e > displayEnd)
                displayEnd = e;
            if (!FP::defined(displayStart) || e < displayStart)
                displayStart = e;
        }
    }

    if (FP::defined(displayStart) && FP::defined(displayEnd)) {
        double width = displayEnd - displayStart;
        width *= 0.05;
        if (width <= 0.0)
            width = 86400.0;
        displayStart -= width;
        displayEnd += width;
    }

    if (!autoranging) {
        haveInfiniteStart = !FP::defined(visibleStart);
        haveInfiniteEnd = !FP::defined(visibleEnd);
        if (!haveInfiniteStart)
            displayStart = visibleStart;
        if (!haveInfiniteEnd)
            displayEnd = visibleEnd;
    }

    regenerateTicks();

    axisDisplay->updateGeometry();
    axisDisplay->update();
    updateGeometry();
}

static void injectToLevel(QList<QList<DataValueTimelineLayoutItem> > &levels,
                          const SequenceIdentity &value,
                          double start,
                          double end)
{
    DataValueTimelineLayoutItem add(value, start, end);

    if (levels.isEmpty()) {
        levels.append(QList<DataValueTimelineLayoutItem>() << add);
        return;
    }

    QList<QList<DataValueTimelineLayoutItem> >::iterator lastValidLevel;
    QList<DataValueTimelineLayoutItem>::iterator lastValidPosition;
    bool haveValid = false;
    for (QList<QList<DataValueTimelineLayoutItem> >::iterator level = levels.end() - 1,
            first = levels.begin();;) {

        QList<DataValueTimelineLayoutItem>::iterator position;
        if (!Range::insertionPosition(level->begin(), level->end(), start, end, &position))
            break;

        lastValidLevel = level;
        lastValidPosition = position;
        haveValid = true;

        if (level == first)
            break;
        --level;
    }

    if (haveValid) {
        lastValidLevel->insert(lastValidPosition, add);
        return;
    }

    levels.append(QList<DataValueTimelineLayoutItem>() << add);
}

void DataValueTimeline::updateLayout()
{
    defaultLayoutValues.clear();
    mainLayoutValues.clear();

    double totalTime = FP::undefined();
    if (FP::defined(displayStart) && FP::defined(displayEnd)) {
        totalTime = displayEnd - displayStart;
        if (totalTime <= 0.0)
            totalTime = FP::undefined();
    }

    {
        QList<QList<DataValueTimelineLayoutItem> > defaultLevels;
        QList<QList<DataValueTimelineLayoutItem> > mainLevels;
        for (const auto &v : values) {
            double start = v.getStart();
            double end = v.getEnd();

            if (!autoranging) {
                if (!Range::intersects(start, end, visibleStart, visibleEnd))
                    continue;
            }

            if (FP::defined(start) && FP::defined(end) && FP::defined(totalTime)) {
                double time = end - start;
                double minimum = totalTime * 0.05;
                if (time < minimum) {
                    double add = minimum - time;
                    add /= 2;
                    start -= add;
                    end += add;
                }
            }

            if (v.getName().isDefaultStation()) {
                injectToLevel(defaultLevels, v, start, end);
            } else {
                injectToLevel(mainLevels, v, start, end);
            }
        }

        for (auto l = defaultLevels.constBegin(), endL = defaultLevels.constEnd(); l != endL; ++l) {
            defaultLayoutValues.append(SequenceIdentity::Transfer());
            for (auto v = l->constBegin(), endV = l->constEnd(); v != endV; ++v) {
                defaultLayoutValues.back().emplace_back(v->value);
            }
        }
        for (auto l = mainLevels.constBegin(), endL = mainLevels.constEnd(); l != endL; ++l) {
            mainLayoutValues.append(SequenceIdentity::Transfer());
            for (auto v = l->constBegin(), endV = l->constEnd(); v != endV; ++v) {
                mainLayoutValues.back().emplace_back(v->value);
            }
        }
    }

    int totalLayers = defaultLayoutValues.size() + mainLayoutValues.size();
    if (totalLayers < 2) {
        scrollBar->setRange(0, 0);
        scrollBar->hide();
    } else {
        scrollBar->setRange(0, totalLayers - 1);
        scrollBar->show();
    }

    valuesDisplay->updateGeometry();
    valuesDisplay->update();
    updateGeometry();
}


SequenceIdentity::Transfer DataValueTimeline::getValues() const
{ return values; }

bool DataValueTimeline::getAutoranging() const
{ return autoranging; }

void DataValueTimeline::setAutoranging(bool a)
{
    autoranging = a;
    updateRanges();
    updateLayout();
    updateGeometry();
    update();
}

double DataValueTimeline::getVisibleStart() const
{ return visibleStart; }

void DataValueTimeline::setVisibleStart(double v)
{
    visibleStart = v;
    updateRanges();
    updateLayout();
    updateGeometry();
    update();
}

double DataValueTimeline::getVisibleEnd() const
{ return visibleEnd; }

void DataValueTimeline::setVisibleEnd(double v)
{
    visibleEnd = v;
    updateRanges();
    updateLayout();
    update();
}

void DataValueTimeline::setVisible(double start, double end)
{
    visibleStart = start;
    visibleEnd = end;
    updateRanges();
    updateLayout();
    update();
}

/**
 * Set the list of values displayed by the timeline.
 * 
 * @param values a list of values to display
 */
void DataValueTimeline::setValues(const SequenceIdentity::Transfer &values)
{
    this->values = values;
    std::sort(this->values.begin(), this->values.end(), SequenceIdentity::OrderOverlay());
    updateRanges();
    updateLayout();
    update();
}

/**
 * Set the list of values displayed by the timeline.
 *
 * @param values a list of values to display
 */
void DataValueTimeline::setValues(const SequenceValue::Transfer &values)
{
    this->values.clear();
    for (const auto &v : values) {
        this->values.emplace_back(v.getIdentity());
    }
    std::sort(this->values.begin(), this->values.end(), SequenceIdentity::OrderOverlay());
    updateRanges();
    updateLayout();
    update();
}

/**
 * Ensure that the timeline is scrolled such that a given value is visible.
 * This will scroll to the first instance where the value is "equal" if
 * that equality occurs multiple times.
 *
 * @param value the value to make visible
 */
void DataValueTimeline::ensureVisible(const SequenceValue &value)
{
    ensureVisible(value.getIdentity());
}

/**
 * Ensure that the timeline is scrolled such that a given value is visible.
 * This will scroll to the first instance where the value is "equal" if
 * that equality occurs multiple times.
 *
 * @param value the value to make visible
 */
void DataValueTimeline::ensureVisible(const SequenceIdentity &value)
{
    if (scrollBar->maximum() < 1)
        return;

    int levelCounter = 0;
    for (auto level = defaultLayoutValues.constBegin(), endL = defaultLayoutValues.constEnd();
            level != endL;
            ++level, ++levelCounter) {
        if (listContains(*level, value)) {
            scrollBar->setValue(levelCounter);
            valuesDisplay->update();
            return;
        }
    }
    for (auto level = mainLayoutValues.constBegin(), endL = mainLayoutValues.constEnd();
            level != endL;
            ++level, ++levelCounter) {
        if (listContains(*level, value)) {
            scrollBar->setValue(levelCounter);
            valuesDisplay->update();
            return;
        }
    }
}

/**
 * Change the highlight to be all values "equal" to the given single
 * value.
 * 
 * @param value the value to highlight
 */
void DataValueTimeline::highlightValue(const SequenceIdentity &value)
{
    highlight.clear();
    highlight.insert(value);
    update();
}

/**
 * Change the highlight to be all values "equal" to the given single
 * value.
 *
 * @param value the value to highlight
 */
void DataValueTimeline::highlightValue(const SequenceValue &value)
{
    highlightValue(value.getIdentity());
}

/**
 * Change the highlight to be all values "equal" to anything in the given list.
 * 
 * @param values    the values to highlight
 */
void DataValueTimeline::highlightValues(const SequenceValue::Transfer &values)
{
    highlight.clear();
    for (const auto &add : values) {
        highlight.insert(add.getIdentity());
    }
    update();
}

/**
 * Change the highlight to be all values "equal" to anything in the given set.
 * 
 * @param values    the values to highlight
 */
void DataValueTimeline::highlightValues(const SequenceIdentity::Set &values)
{
    highlight = values;
    update();
}

void DataValueTimeline::clickValue(const SequenceIdentity &value, Qt::MouseButton button)
{
    if (button == Qt::LeftButton) {
        emit valueClicked(value);
    } else if (button == Qt::RightButton) {
        emit valueRightClicked(value);
    }
}


}
}
}
