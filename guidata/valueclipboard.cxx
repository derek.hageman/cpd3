/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QApplication>
#include <QClipboard>
#include <QXmlSimpleReader>
#include <QXmlStreamWriter>
#include <QBuffer>
#include <QLoggingCategory>

#include "datacore/variant/composite.hxx"
#include "datacore/dataioxml.hxx"
#include "guidata/valueclipboard.hxx"


Q_LOGGING_CATEGORY(log_guidata_valueclipboard, "cpd3.guidata.valueclipboard", QtWarningMsg)

using namespace CPD3::Data;

namespace CPD3 {
namespace GUI {
namespace Data {

/** @file guidata/valueclipboard.hxx
 * Provides interfaces for copy-paste functionality for value.
 */

/**
 * Encode a value to a string (as a byte array) suitable for text usage.
 * 
 * @param value     the value to encode
 * @return          the UTF8 XML encoding of the value
 * @see decodeSingle(const QByteArray &)
 */
QByteArray ValueClipboard::encode(const Variant::Read &value)
{
    return encode(std::vector<Variant::Read>{value});
}

/**
 * Encode a list of values to a string (as a byte array) suitable for text 
 * usage.
 * 
 * @param values    the values to encode
 * @return          the UTF8 XML encoding of the values
 * @see decode(const QByteArray &)
 */
QByteArray ValueClipboard::encode(const std::vector<Variant::Read> &values)
{
    if (values.empty())
        return QByteArray();

    QByteArray data;
    QBuffer outBuffer(&data);
    outBuffer.open(QIODevice::WriteOnly);
    QXmlStreamWriter *xml = new QXmlStreamWriter(&outBuffer);
    xml->writeStartDocument();
    xml->writeCharacters(QString('\n'));
    xml->writeDTD("<!DOCTYPE cpd3values>");
    xml->writeCharacters(QString('\n'));
    xml->writeStartElement("cpd3values");
    xml->writeCharacters(QString('\n'));

    for (const auto &v : values) {
        xml->writeCharacters("    ");

        xml->writeStartElement("data");

        xml->writeCharacters("\n        ");
        xml->writeStartElement("path");
        XMLDataOutput::writePath(xml, v.currentPath(), 8, 4);
        xml->writeEndElement();
        xml->writeCharacters("\n        ");

        xml->writeStartElement("value");
        XMLDataOutput::write(xml, v, 8, 4);
        xml->writeEndElement();

        xml->writeCharacters("\n    ");
        xml->writeEndElement();
        xml->writeCharacters(QString('\n'));
    }

    xml->writeEndDocument();
    delete xml;

    return data;
}

QByteArray ValueClipboard::encode(const std::vector<Variant::Root> &values)
{
    if (values.empty())
        return QByteArray();

    QByteArray data;
    QBuffer outBuffer(&data);
    outBuffer.open(QIODevice::WriteOnly);
    QXmlStreamWriter *xml = new QXmlStreamWriter(&outBuffer);
    xml->writeStartDocument();
    xml->writeCharacters(QString('\n'));
    xml->writeDTD("<!DOCTYPE cpd3values>");
    xml->writeCharacters(QString('\n'));
    xml->writeStartElement("cpd3values");
    xml->writeCharacters(QString('\n'));

    for (const auto &v : values) {
        xml->writeCharacters("    ");

        xml->writeStartElement("data");

        xml->writeCharacters("\n        ");
        xml->writeStartElement("path");
        XMLDataOutput::writePath(xml, v.read().currentPath(), 8, 4);
        xml->writeEndElement();
        xml->writeCharacters("\n        ");

        xml->writeStartElement("value");
        XMLDataOutput::write(xml, v.read(), 8, 4);
        xml->writeEndElement();

        xml->writeCharacters("\n    ");
        xml->writeEndElement();
        xml->writeCharacters(QString('\n'));
    }

    xml->writeEndDocument();
    delete xml;

    return data;
}


/**
 * Decode a single value from the clipboard encoding of it.
 * 
 * @param data      the data to decode
 * @see encode(const Variant::Read &)
 */
Variant::Root ValueClipboard::decodeSingle(const QByteArray &data)
{
    auto result = decode(data);
    if (result.empty())
        return Variant::Root();
    return std::move(result.front());
}


namespace {
class ClipboardDecodeParser : public QXmlDefaultHandler {
public:
    std::vector<Variant::Root> output;
    int level;
    enum Mode {
        TopLevel = 0, ClipboardData = 0x0001, InPath = 0x1000, InValue = 0x2000,
    };
    int mode;

    Variant::Root outputValue;
    Variant::Path outputPath;
    XMLDataInputParser dataParser;
    XMLDataPathParser pathParser;


    virtual QString errorString() const
    {
        QString s(dataParser.errorString());
        if (!s.isEmpty()) return s;
        return pathParser.errorString();
    }

    virtual bool fatalError(const QXmlParseException &exception)
    {
        qCDebug(log_guidata_valueclipboard) << "Error parsing input XML at line"
                                            << exception.lineNumber() << "column"
                                            << exception.columnNumber() << ':'
                                            << exception.message();
        return false;
    }

    virtual bool startElement(const QString &namespaceURI,
                              const QString &localName,
                              const QString &qName,
                              const QXmlAttributes &atts)
    {
        level++;
        if (level == 1) {
        } else if (level == 2) {
            if (localName == "data") {
                mode = ClipboardData;
            } else if (localName == "value") {
                mode = InValue;
                outputValue = Variant::Root();
                dataParser = XMLDataInputParser(outputValue.write());
                return dataParser.startElement(namespaceURI, localName, qName, atts) !=
                        XMLDataInputParser::ERROR;
            }
        } else {
            if (mode == ClipboardData) {
                if (localName == "path") {
                    mode |= InPath;
                    outputPath.clear();
                    pathParser = XMLDataPathParser(&outputPath);
                    return true;
                } else if (localName == "value") {
                    mode |= InValue;
                    outputValue = Variant::Root();
                    dataParser = XMLDataInputParser(outputValue.write());
                } else {
                    return false;
                }
            }
            if (mode & InPath) {
                return pathParser.startElement(namespaceURI, localName, qName, atts) !=
                        XMLDataPathParser::ERROR;
            } else if (mode & InValue) {
                return dataParser.startElement(namespaceURI, localName, qName, atts) !=
                        XMLDataInputParser::ERROR;
            } else {
                return false;
            }
        }

        return true;
    }

    virtual bool endElement(const QString &namespaceURI,
                            const QString &localName,
                            const QString &qName)
    {
        level--;

        if (level < 0)
            return false;
        if (level == 0)
            return true;

        if (mode & InPath) {
            if (pathParser.endElement(namespaceURI, localName, qName) == XMLDataPathParser::ERROR)
                return false;
        } else if (mode & InValue) {
            if (dataParser.endElement(namespaceURI, localName, qName) == XMLDataInputParser::ERROR)
                return false;
        }

        if (level == 1) {
            if (outputPath.empty()) {
                output.emplace_back(outputValue);
            } else {
                Variant::Root v;
                v.write().getPath(outputPath).set(outputValue);
                output.emplace_back(std::move(v));
            }
            mode = TopLevel;
            return true;
        }

        if (level == 2 && (mode & ClipboardData)) {
            mode &= ~(InPath | InValue);
        }

        return true;
    }

    virtual bool characters(const QString &ch)
    {
        if (mode & InPath)
            return pathParser.characters(ch) != XMLDataPathParser::ERROR;
        else if (mode & InValue)
            return dataParser.characters(ch) != XMLDataInputParser::ERROR;
        return true;
    }

    ClipboardDecodeParser() : output(), level(0), mode(TopLevel)
    { }
};
}

/**
 * Decode a list of values from the clipboard encoding of it.
 * 
 * @param data      the data to decode
 * @see encode(const QList<Value> &)
 */
std::vector<Variant::Root> ValueClipboard::decode(const QByteArray &data)
{
    QXmlSimpleReader xml;
    QXmlInputSource xmlData;
    ClipboardDecodeParser handler;
    xml.setContentHandler(&handler);
    xml.setErrorHandler(&handler);

    xmlData.setData(data);
    if (!xml.parse(&xmlData, true))
        return std::vector<Variant::Root>();
    xml.parseContinue();

    return std::move(handler.output);
}


/**
 * Insert a list of values as a children of the given parent.  This is used to
 * perform the paste or duplication operations on a value tree.
 * 
 * @param parent    the parent to insert the values under
 * @param children  the values to be inserted
 * @param metadata  the metadata to consider
 * @param strictChildren do not insert children that do not match the metadata
 * @return          the list of inserted paths
 */
std::vector<Variant::Path> ValueClipboard::insert(Variant::Write &parent,
                                                  const std::vector<Variant::Read> &children,
                                                  const Variant::Read &metadata,
                                                  bool strictChildren)
{
    std::vector<Variant::Path> result;
    for (const auto &c : children) {
        auto path = insert(parent, c, metadata, strictChildren);
        if (path.empty())
            continue;
        result.emplace_back(std::move(path));
    }
    return result;
}

static bool verifyValidInsert(const Variant::Read &value, const Variant::Read &metadata)
{
    if (!metadata.exists())
        return false;

    switch (value.getType()) {
    case Variant::Type::Empty:
        /* Don't care if this is the end of the line */
        return true;

        /* Terminal cases */
    case Variant::Type::Real:
        return metadata.getType() == Variant::Type::MetadataReal;
    case Variant::Type::Integer:
        return metadata.getType() == Variant::Type::MetadataInteger;
    case Variant::Type::Boolean:
        return metadata.getType() == Variant::Type::MetadataBoolean;
    case Variant::Type::String:
        return metadata.getType() == Variant::Type::MetadataString;
    case Variant::Type::Bytes:
        return metadata.getType() == Variant::Type::MetadataBytes;
    case Variant::Type::Flags:
        return metadata.getType() == Variant::Type::MetadataFlags;

    case Variant::Type::Overlay:
        /* Don't try to figure this one out, just consider it bad. */
        return false;

    case Variant::Type::MetadataReal:
    case Variant::Type::MetadataInteger:
    case Variant::Type::MetadataBoolean:
    case Variant::Type::MetadataString:
    case Variant::Type::MetadataBytes:
    case Variant::Type::MetadataFlags:
    case Variant::Type::MetadataArray:
    case Variant::Type::MetadataMatrix:
    case Variant::Type::MetadataHash:
    case Variant::Type::MetadataKeyframe:
        /*  Don't check meta-metadata */
        return true;

    case Variant::Type::Array: {
        if (metadata.getType() != Variant::Type::MetadataArray)
            return false;

        auto children = metadata.metadataArray("Children");
        for (auto check : value.toArray()) {
            if (!verifyValidInsert(check, children))
                return false;
        }
        return true;
    }

    case Variant::Type::Matrix: {
        if (metadata.getType() != Variant::Type::MetadataMatrix)
            return false;

        auto children = metadata.metadataMatrix("Children");
        for (auto check : value.toMatrix()) {
            if (!verifyValidInsert(check.second, children))
                return false;
        }
        return true;
    }

    case Variant::Type::Hash: {
        if (metadata.getType() != Variant::Type::MetadataHash)
            return false;

        for (auto check : value.toHash()) {
            if (!verifyValidInsert(check.second, metadata.metadataHashChild(check.first)))
                return false;
        }
        return true;
    }

    case Variant::Type::Keyframe: {
        if (metadata.getType() != Variant::Type::MetadataKeyframe)
            return false;

        auto children = metadata.metadataKeyframe("Children");
        for (auto check : value.toKeyframe()) {
            if (!verifyValidInsert(check.second, children))
                return false;
        }
        return true;
    }
    }

    Q_ASSERT(false);
    return false;
}

/**
 * Insert a single value as a child of the given parent.  This is used to
 * perform the paste or duplication operations on a value tree.
 * 
 * @param parent    the parent to insert the values under
 * @param child     the value to be inserted
 * @param metadata  the metadata to consider
 * @param strictChildren do not insert children that do not match the metadata
 * @return          the path the child was inserted on, empty if it wasn't
 */
Variant::Path ValueClipboard::insert(Variant::Write &parent,
                                     const Variant::Read &child,
                                     const Variant::Read &metadata,
                                     bool strictChildren)
{
    bool childOk = true;
    auto childPath =
            Variant::Composite::uniqueKey(parent, metadata, strictChildren ? &childOk : nullptr);
    if (!childOk)
        return Variant::Path();

    if (strictChildren) {
        Variant::Type effectiveType;
        if (parent.exists()) {
            effectiveType = parent.getType();
        } else {
            effectiveType = metadata.getType();
            switch (effectiveType) {
            case Variant::Type::Empty:
            case Variant::Type::Real:
            case Variant::Type::Integer:
            case Variant::Type::Boolean:
            case Variant::Type::String:
            case Variant::Type::Bytes:
            case Variant::Type::Flags:
            case Variant::Type::Array:
            case Variant::Type::Matrix:
            case Variant::Type::Hash:
            case Variant::Type::Keyframe:
            case Variant::Type::Overlay:
                /* None of these are valid metadata */
                effectiveType = Variant::Type::Empty;
                break;
            case Variant::Type::MetadataReal:
                effectiveType = Variant::Type::Real;
                break;
            case Variant::Type::MetadataInteger:
                effectiveType = Variant::Type::Integer;
                break;
            case Variant::Type::MetadataBoolean:
                effectiveType = Variant::Type::Boolean;
                break;
            case Variant::Type::MetadataString:
                effectiveType = Variant::Type::String;
                break;
            case Variant::Type::MetadataBytes:
                effectiveType = Variant::Type::Bytes;
                break;
            case Variant::Type::MetadataFlags:
                effectiveType = Variant::Type::Flags;
                break;
            case Variant::Type::MetadataArray:
                effectiveType = Variant::Type::Array;
                break;
            case Variant::Type::MetadataMatrix:
                effectiveType = Variant::Type::Matrix;
                break;
            case Variant::Type::MetadataHash:
                effectiveType = Variant::Type::Hash;
                break;
            case Variant::Type::MetadataKeyframe:
                effectiveType = Variant::Type::Keyframe;
                break;
            }
        }

        switch (effectiveType) {
        case Variant::Type::Empty:
        case Variant::Type::Real:
        case Variant::Type::Integer:
        case Variant::Type::Boolean:
        case Variant::Type::String:
        case Variant::Type::Bytes:
        case Variant::Type::Flags:
        case Variant::Type::Overlay:
            /* These can't have children, so this can't be valid. */
            return Variant::Path();

        case Variant::Type::MetadataReal:
        case Variant::Type::MetadataInteger:
        case Variant::Type::MetadataBoolean:
        case Variant::Type::MetadataString:
        case Variant::Type::MetadataBytes:
        case Variant::Type::MetadataFlags:
        case Variant::Type::MetadataArray:
        case Variant::Type::MetadataMatrix:
        case Variant::Type::MetadataHash:
        case Variant::Type::MetadataKeyframe:
            /* Don't do any checking for meta-metadata */
            break;

        case Variant::Type::Array:
            if (!verifyValidInsert(child, metadata.metadataArray("Children")))
                return Variant::Path();
            break;

        case Variant::Type::Matrix:
            if (!verifyValidInsert(child, metadata.metadataMatrix("Children")))
                return Variant::Path();
            break;

        case Variant::Type::Hash:
            if (!verifyValidInsert(child, metadata.getPath(childPath)))
                return Variant::Path();
            break;

        case Variant::Type::Keyframe:
            if (!verifyValidInsert(child, metadata.metadataKeyframe("Children")))
                return Variant::Path();
            break;
        }
    }

    parent.getPath(childPath).set(child);

    auto path = parent.currentPath();
    path.emplace_back(std::move(childPath));
    return path;
}


ValueMimeData::ValueMimeData() : QMimeData(), values()
{ }

ValueMimeData::ValueMimeData(const ValueMimeData &other) : QMimeData(), values(other.values)
{ }

ValueMimeData &ValueMimeData::operator=(const ValueMimeData &other)
{
    values = other.values;
    return *this;
}

ValueMimeData::ValueMimeData(ValueMimeData &&other) : QMimeData(), values(std::move(other.values))
{ }

ValueMimeData &ValueMimeData::operator=(ValueMimeData &&other)
{
    values = std::move(other.values);
    return *this;
}

ValueMimeData::ValueMimeData(const Variant::Root &value) : values{value}
{ }

ValueMimeData::ValueMimeData(Variant::Root &&value) : values{std::move(value)}
{ }

ValueMimeData::ValueMimeData(const std::vector<Variant::Root> &setValues) : values(setValues)
{ }

ValueMimeData::ValueMimeData(std::vector<Variant::Root> &&setValues) : values(std::move(setValues))
{ }

bool ValueMimeData::hasFormat(const QString &mimeType) const
{
    if (mimeType == "text/plain")
        return true;
    if (mimeType == "application/xml")
        return true;
    if (mimeType == "text/xml")
        return true;
    if (mimeType == "application/cpd3values+xml")
        return true;

    return false;
}

QStringList ValueMimeData::formats() const
{
    QStringList f;
    f << "text/plain";
    f << "application/xml";
    f << "text/xml";
    f << "application/cpd3values+xml";
    return f;
}

QVariant ValueMimeData::retrieveData(const QString &mimeType, QVariant::Type type) const
{
    if (mimeType != "application/cpd3values+xml" &&
            mimeType != "text/xml" &&
            mimeType != "text/plain" &&
            mimeType != "application/xml")
        return QVariant();

    if (type == QVariant::ByteArray)
        return QVariant(ValueClipboard::encode(values));
    else if (type == QVariant::String)
        return QVariant(QString(ValueClipboard::encode(values)));
    return QVariant();
}

std::vector<Variant::Root> ValueMimeData::fromMimeData(const QMimeData *data)
{
    if (data->hasFormat("application/cpd3values+xml")) {
        return ValueClipboard::decode(data->data("application/cpd3values+xml"));
    } else if (data->hasFormat("text/xml")) {
        return ValueClipboard::decode(data->data("text/xml"));
    } else if (data->hasFormat("text/plain")) {
        return ValueClipboard::decode(data->data("text/plain"));
    } else if (data->hasFormat("application/xml")) {
        return ValueClipboard::decode(data->data("application/xml"));
    }
    return std::vector<Variant::Root>();
}

}
}
}
