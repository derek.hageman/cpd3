/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QVBoxLayout>
#include <QCommandLinkButton>
#include <QFormLayout>
#include <QRegExpValidator>
#include <QVariant>
#include <QSettings>
#include <QtConcurrentRun>
#include <QSslCertificate>
#include <QFile>
#include <QFileInfo>
#include <QMessageBox>
#include <QHeaderView>
#include <QLoggingCategory>

#include "guidata/wizards/initialsetup.hxx"
#include "guidata/editors/acquisition/component.hxx"
#include "guicore/guicore.hxx"
#include "guicore/guiformat.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/segment.hxx"
#include "datacore/streampipeline.hxx"
#include "datacore/stream.hxx"
#include "algorithms/cryptography.hxx"
#include "transfer/upload.hxx"
#include "transfer/download.hxx"
#include "core/textsubstitution.hxx"


Q_LOGGING_CATEGORY(log_guidata_initialsetup, "cpd3.guidata.initialsetup", QtWarningMsg)

using namespace CPD3::GUI::Data::Internal;

using namespace CPD3::Data;

#define SETTINGS_APPLICATION    "CPD3Setup"


namespace CPD3 {
namespace GUI {
namespace Data {

/** @file guidata/wizards/initalsetup.hxx
 * Provides a tool to perform initial CPD3 setup.
 */

static bool allowChangeDefaultStation()
{
    QByteArray check(qgetenv("CPD3STATION"));
    if (check.isEmpty())
        check = qgetenv("MYSTN");
    if (!check.isEmpty())
        return false;
    QSettings settings("NOAA ESRL GMD", "CPD3Default");
    auto existing = settings.value("station").toString().toLower().toStdString();
    if (existing.empty())
        return false;
    auto defined = Archive::Access().availableStations();
    defined.erase("_");
    defined.erase(SequenceName::Component());
    if (defined.empty())
        return false;
    if (defined.count(existing) == 0)
        return true;
    if (defined.size() == 1)
        return false;
    return true;
}


namespace {
class BootstrapDownloader : public CPD3::Transfer::FileDownloader {
    TextSubstitutionStack substitutions;
public:
    BootstrapDownloader(const QString &type) : substitutions()
    {
        substitutions.setString("type", type);
    }

    virtual ~BootstrapDownloader()
    { }

protected:
    QString applySubstitutions(const QString &input) const override
    { return substitutions.apply(input); }

    void pushTemporaryOutput(const QFileInfo &fileName) override
    {
        substitutions.push();
        substitutions.setFile("file", fileName);
    }

    void popTemporaryOutput() override
    {
        substitutions.pop();
    }
};
}

static bool performBootstrap(const QString &type, InitialSetupProgress *page)
{
    SequenceValue bootstrapFlag({"_", "bootstrap", "bootstrap"}, Variant::Root());
    Archive::Access access;
    {
        Archive::Access::ReadLock lock(access, true);
        auto existing = access.readSynchronous(
                Archive::Selection(Time::time(), FP::undefined(), {"_"}, {"bootstrap"},
                                   {"bootstrap"}).withDefaultStation(false));
        if (!existing.empty())
            bootstrapFlag = existing.back();
    }
    if (bootstrapFlag.getValue().hash(type).toBool()) {
        qCDebug(log_guidata_initialsetup) << "Bootstrap already completed for type:" << type;
        return true;
    }

    qCDebug(log_guidata_initialsetup) << "Bootstrapping type:" << type;

    Variant::Root config;
    config["Type"] = "Single";
    config["Source"] = "GET";
    config["File"] = BOOTSTRAP_URL;

    BootstrapDownloader download(type);
    QObject context;
    QObject::connect(page, &InitialSetupProgress::aborted, &context,
                     std::bind(&BootstrapDownloader::signalTerminate, &download),
                     Qt::DirectConnection);
    page->feedbackAttach(download.feedback);

    if (page->isAborted())
        return false;

    if (!download.exec(config))
        return false;
    auto files = download.takeFiles();
    if (files.empty())
        return false;

    for (auto &f : files) {
        qCDebug(log_guidata_initialsetup) << "Processing bootstrap file" << f->fileName()
                                          << "with" << f->fileInfo().size() << "byte(s) of data";

        StreamSink::Buffer buffer;
        StreamPipeline chain;
        QEventLoop loop;
        if (!chain.setInputFile(f->fileInfo().absoluteFilePath())) {
            qCDebug(log_guidata_initialsetup) << "Error setting bootstrap input file:"
                                              << chain.getInputError();
            continue;
        }
        if (!chain.setOutputIngress(&buffer)) {
            qCDebug(log_guidata_initialsetup) << "Error setting bootstrap output:"
                                              << chain.getOutputError();
            continue;
        }
        QObject::connect(page, &InitialSetupProgress::aborted, &loop,
                         std::bind(&StreamPipeline::signalTerminate, &chain), Qt::DirectConnection);
        QObject::connect(page, &InitialSetupProgress::aborted, &loop, &QEventLoop::quit,
                         Qt::QueuedConnection);
        chain.finished.connect(&loop, std::bind(&QEventLoop::quit, &loop), true);
        page->feedbackAttach(chain.feedback);
        if (page->isAborted())
            break;
        if (!chain.start())
            continue;
        loop.exec();
        if (!chain.wait())
            continue;

        auto add = buffer.take();

        qCDebug(log_guidata_initialsetup) << "Bootstrap writing" << add.size()
                                          << "value(s) for type:" << type;

        page->spinStage(InitialSetup::tr("Updating database"), InitialSetup::tr(
                "The system is writing the downloaded bootstrap data to the local database."));

        for (;;) {
            if (page->isAborted())
                return false;
            Archive::Access::WriteLock lock(access);
            access.writeSynchronous(add);
            if (lock.commit())
                break;
        }
    }

    if (page->isAborted())
        return false;

    bootstrapFlag.write().hash(type).setBool(true);
    for (;;) {
        if (page->isAborted())
            return false;
        Archive::Access::WriteLock lock(access);
        access.writeSynchronous(SequenceValue::Transfer{bootstrapFlag});
        if (lock.commit())
            break;
    }

    qCDebug(log_guidata_initialsetup) << "Bootstrap completed for type:" << type;

    return true;
}

static bool isBootstrapped(const QString &type)
{
    SequenceValue bootstrapFlag({"_", "bootstrap", "bootstrap"}, Variant::Root());
    Archive::Access access;
    Archive::Access::ReadLock lock(access, true);
    auto existing = access.readSynchronous(
            Archive::Selection(Time::time(), FP::undefined(), {"_"}, {"bootstrap"},
                               {"bootstrap"}).withDefaultStation(false));
    if (existing.empty())
        return false;
    return existing.back().getValue().hash(type).toBool();
}

InitialSetup::InitialSetup(SetupMode mode, QWidget *parent, Qt::WindowFlags flags) : QWizard(parent,
                                                                                             flags)
{
    setWindowTitle(tr("CPD3 Setup"));

    switch (mode) {
    case Mode_Any:
        setPage(Page_ModeSelect, new InitialSetupModeSelect(this));
        break;
    case Mode_SetStation:
    case Mode_Synchronize:
    case Mode_Acquisition:
        break;
    }

    if ((mode == Mode_Any && allowChangeDefaultStation()) || mode == Mode_SetStation) {
        setPage(Page_SetStation_Select, new InitialSetupSetDefaultStation(this));
    }
    if (mode == Mode_Any || mode == Mode_Synchronize) {
        setPage(Page_Sync_EnterContact, new InitialSetupSyncEnterContact(this));
        setPage(Page_Sync_Initialize, new InitialSetupSyncInitialize(this));
        setPage(Page_Sync_AddStation, new InitialSetupSyncAddStation(this));
        setPage(Page_Sync_Processing, new InitialSetupSyncProcessing(this));
    }
    if (mode == Mode_Any || mode == Mode_Acquisition) {
        setPage(Page_Acquisition_Station, new InitialSetupAcquisitionStation(this));
        setPage(Page_Acquisition_Initialize, new InitialSetupAcquisitionInitialize(this));
        setPage(Page_Acquisition_Components, new InitialSetupAcquisitionComponents(this));
        setPage(Page_Acquisition_Advanced, new InitialSetupAcquisitionAdvanced(this));
        setPage(Page_Acquisition_Processing, new InitialSetupAcquisitionProcessing(this));
    }

    setOptions(options() | QWizard::NoBackButtonOnLastPage);
}

InitialSetup::~InitialSetup()
{ }

void InitialSetup::initializePage(int id)
{
    QWizard::initializePage(id);
    switch (id) {
    case Page_ModeSelect:
        setButtonLayout(QList<QWizard::WizardButton>() << QWizard::Stretch << QWizard::BackButton
                                                       << QWizard::CancelButton
                                                       << QWizard::FinishButton);
        break;
    default:
        setButtonLayout(QList<QWizard::WizardButton>() << QWizard::Stretch << QWizard::BackButton
                                                       << QWizard::NextButton
                                                       << QWizard::CancelButton
                                                       << QWizard::FinishButton);
        break;
    }
}


static void registerDefaultStation(const QString &station)
{
    if (station.isEmpty())
        return;
    QSettings settings("NOAA ESRL GMD", "CPD3Default");
    QString check(settings.value("station").toString());
    if (!check.isEmpty())
        return;
    settings.setValue("station", station);
    qCDebug(log_guidata_initialsetup) << "Default station set to:" << station;
}

namespace Internal {

InitialSetupModeSelect::InitialSetupModeSelect(InitialSetup *parent) : QWizardPage(parent)
{
    setTitle(tr("Select Setup Mode"));
    setSubTitle(
            tr("Once you have selected a general operation mode, you will be asked for additional information for that mode."));

    mode = InitialSetup::Mode_Any;

    QVBoxLayout *layout = new QVBoxLayout(this);
    setLayout(layout);

    QCommandLinkButton *button = new QCommandLinkButton(tr("Synchronized data editing"),
                                                        tr("Select this to set up CPD3 for editing data that is synchronized from the central server.  This will walk you through submitting a request for synchronization authorization, once authorization is granted you will be able to synchronize and edit data."),
                                                        this);
    connect(button, SIGNAL(clicked()), this, SLOT(selectSynchronize()));
    layout->addWidget(button);

    button = new QCommandLinkButton(tr("Local data acquisition"),
                                    tr("Select this to set up CPD3 for communications and control of locally connected acquisition instruments.  This will allow you to configure the instruments connected to the computer."),
                                    this);
    connect(button, SIGNAL(clicked()), this, SLOT(selectAcquisition()));
    layout->addWidget(button);

    if (allowChangeDefaultStation()) {
        button = new QCommandLinkButton(tr("Change default station"),
                                        tr("Select this to change the default station in use.  This allows you to alter the station CPD3 starts up with when none is specified."),
                                        this);
        connect(button, SIGNAL(clicked()), this, SLOT(selectStation()));
        layout->addWidget(button);
    }

    layout->addStretch(1);
}

InitialSetupModeSelect::~InitialSetupModeSelect() = default;

int InitialSetupModeSelect::nextId() const
{
    switch (mode) {
    case InitialSetup::Mode_Synchronize:
        return InitialSetup::Page_Sync_EnterContact;
    case InitialSetup::Mode_Acquisition:
        return InitialSetup::Page_Acquisition_Station;
    case InitialSetup::Mode_SetStation:
        return InitialSetup::Page_SetStation_Select;
    case InitialSetup::Mode_Any:
        break;
    }
    return InitialSetup::Page_ModeSelect;
}

void InitialSetupModeSelect::selectSynchronize()
{
    mode = InitialSetup::Mode_Synchronize;
    wizard()->next();
}

void InitialSetupModeSelect::selectAcquisition()
{
    mode = InitialSetup::Mode_Acquisition;
    wizard()->next();
}


void InitialSetupModeSelect::selectStation()
{
    mode = InitialSetup::Mode_SetStation;
    wizard()->next();
}


InitialSetupSetDefaultStation::InitialSetupSetDefaultStation(InitialSetup *parent) : QWizardPage(
        parent)
{
    setTitle(tr("Select a Station"));
    setSubTitle(tr("Select an a station to use as the default."));

    QFormLayout *layout = new QFormLayout(this);
    setLayout(layout);

    station = new QComboBox(this);
    station->setEditable(false);
    layout->addRow(tr("&Station"), station);

    connect(parent, SIGNAL(accepted()), this, SLOT(commitSettings()));
}

InitialSetupSetDefaultStation::~InitialSetupSetDefaultStation() = default;

void InitialSetupSetDefaultStation::initializePage()
{
    QWizardPage::initializePage();

    std::vector<SequenceName::Component> stations;
    {
        auto all = Archive::Access().availableStations();
        std::copy(all.begin(), all.end(), Util::back_emplacer(stations));
    }
    std::sort(stations.begin(), stations.end());
    QSettings settings("NOAA ESRL GMD", "CPD3Default");
    auto check = settings.value("station").toString().toLower();
    for (const auto &add : stations) {
        if (add.empty() || add == "_")
            continue;
        auto displayStation = QString::fromStdString(add);
        if (station->findText(displayStation, Qt::MatchFixedString) != -1)
            continue;
        station->addItem(displayStation.toUpper());
        if (check == displayStation.toLower())
            station->setCurrentIndex(station->count() - 1);
    }
}

void InitialSetupSetDefaultStation::commitSettings()
{
    QString stn(station->currentText());
    if (stn.isEmpty())
        return;
    QSettings settings("NOAA ESRL GMD", "CPD3Default");
    settings.setValue("station", stn);
}

bool InitialSetupSetDefaultStation::isComplete() const
{
    if (!QWizardPage::isComplete())
        return false;
    QRegExp re("[a-zA-Z][a-zA-Z0-9]{2,}");
    if (!re.exactMatch(station->currentText()))
        return false;
    return true;
}

int InitialSetupSetDefaultStation::nextId() const
{ return -1; }


InitialSetupSyncEnterContact::InitialSetupSyncEnterContact(InitialSetup *parent) : QWizardPage(
        parent), needToBootstrap(true)
{
    setTitle(tr("Enter Contact Information"));
    setSubTitle(
            tr("Enter your contact information.  This will be submitted along with the synchronization request and you will receive an email to the given address once it is granted."));

    QFormLayout *layout = new QFormLayout(this);
    setLayout(layout);

    name = new QLineEdit(this);
    registerField("name*", name);
    layout->addRow(tr("&Name"), name);

    email = new QLineEdit(this);
    email->setValidator(new QRegExpValidator(QRegExp(".+@.+\\..+"), email));
    registerField("email*", email);
    layout->addRow(tr("&Email"), email);

    note = new QLineEdit(this);
    note->setPlaceholderText(tr("Optional (can be use to distinguish between computers)"));
    note->setToolTip(tr("An optional note included in the request"));
    note->setWhatsThis(
            tr("This is an optional note included in the request.  It can be used to distinguish between multiple computers used by the same person for the same station."));
    registerField("note", note);
    layout->addRow(tr("No&te"), note);

    connect(parent, SIGNAL(accepted()), this, SLOT(commitSettings()));
}

InitialSetupSyncEnterContact::~InitialSetupSyncEnterContact() = default;

void InitialSetupSyncEnterContact::initializePage()
{
    QWizardPage::initializePage();

    QSettings settings(CPD3GUI_ORGANIZATION, SETTINGS_APPLICATION);
    name->setText(settings.value("contact/name").toString());
    email->setText(settings.value("contact/email").toString());
    note->setText(settings.value("contact/note").toString());

    needToBootstrap = !isBootstrapped("synchronize");
}

int InitialSetupSyncEnterContact::nextId() const
{
    if (needToBootstrap)
        return InitialSetup::Page_Sync_Initialize;
    return InitialSetup::Page_Sync_AddStation;
}

void InitialSetupSyncEnterContact::commitSettings()
{
    QSettings settings(CPD3GUI_ORGANIZATION, SETTINGS_APPLICATION);
    if (!name->text().isEmpty())
        settings.setValue("contact/name", name->text());
    if (!email->text().isEmpty())
        settings.setValue("contact/email", email->text());
    if (!note->text().isEmpty())
        settings.setValue("contact/note", note->text());
}

InitialSetupSyncAddStation::InitialSetupSyncAddStation(InitialSetup *parent) : QWizardPage(parent)
{
    setTitle(tr("Add a Station"));
    setSubTitle(
            tr("Select an existing station or add another one.  Enter the GAW ID of the station you want to request synchronization for."));

    auto outer = new QVBoxLayout(this);
    setLayout(outer);

    auto inner = new QWidget(this);
    outer->addWidget(inner, 0);
    auto layout = new QFormLayout(inner);
    inner->setLayout(layout);

    station = new QComboBox(inner);
    station->setEditable(true);
    station->setInsertPolicy(QComboBox::InsertAlphabetically);
    station->setSizeAdjustPolicy(QComboBox::AdjustToContents);
    registerField("sync_station*", station, "currentText", SIGNAL(editTextChanged(
                                                                          const QString &)));
    layout->addRow(tr("&Station"), station);
    connect(station, SIGNAL(editTextChanged(
                                    const QString &)), this, SIGNAL(completeChanged()));

    profile = new QComboBox(inner);
    profile->setEditable(false);
    profile->addItem(tr("Everything"), QVariant());
    profile->setCurrentIndex(0);
    profile->setSizeAdjustPolicy(QComboBox::AdjustToContents);
    registerField("sync_profile*", profile, "currentData", SIGNAL(currentIndexChanged(int)));
    layout->addRow(tr("&Data"), profile);
    connect(profile, static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this,
            &InitialSetupSyncAddStation::updateDescription);

    description = new QLabel(this);
    outer->addWidget(description, 0);
    description->setWordWrap(true);

    outer->addSpacing(10);
    outer->addStretch(1);

    connect(parent, SIGNAL(accepted()), this, SLOT(commitSettings()));
}

InitialSetupSyncAddStation::~InitialSetupSyncAddStation() = default;

void InitialSetupSyncAddStation::initializePage()
{
    QWizardPage::initializePage();

    struct Result {
        std::vector<SequenceName::Component> stations;
        std::map<QString, std::string> profiles;
        std::unordered_map<std::string, QString> descriptions;
    };

    Threading::pollFuture(this, std::async(std::launch::async, [=]() {
        Result result;

        {
            Archive::Access access;
            {
                auto all = access.availableStations();
                std::copy(all.begin(), all.end(), Util::back_emplacer(result.stations));
            }


            Variant::Read config = Variant::Read::empty();
            {
                double tnow = Time::time();
                auto confList = SequenceSegment::Stream::read(
                        Archive::Selection(tnow - 1.0, tnow + 1.0, {"_"}, {"configuration"},
                                           {"synchronize"}));
                auto f = Range::findIntersecting(confList.begin(), confList.end(), tnow);
                if (f != confList.end()) {
                    config = f->getValue(SequenceName("_", "configuration",
                                                      "synchronize"))["Processing/Request/Profiles"];
                }

                result.profiles
                      .emplace(config.hash("").hash("Name").toDisplayString(), std::string());
                for (auto child : config.toHash()) {
                    result.descriptions
                          .emplace(child.first, child.second.hash("Description").toDisplayString());
                    if (child.first.empty())
                        continue;
                    result.profiles
                          .emplace(child.second.hash("Name").toDisplayString(), child.first);
                }
            }
        }
        std::sort(result.stations.begin(), result.stations.end());

        return result;
    }), [this](Result result) {
        profileToDescription = std::move(result.descriptions);

        for (const auto &add : result.stations) {
            if (add.empty() || add == "_")
                continue;
            auto displayStation = QString::fromStdString(add);
            if (station->findText(displayStation, Qt::MatchFixedString) != -1)
                continue;
            station->addItem(displayStation.toUpper());
        }

        while (profile->count() > 1) {
            profile->removeItem(profile->count() - 1);
        }
        for (const auto &add : result.profiles) {
            if (add.second.empty()) {
                profile->setItemText(0, add.first);
                continue;
            }
            profile->addItem(add.first, QVariant(QString::fromStdString(add.second)));
        }

        updateDescription();
    });
}

bool InitialSetupSyncAddStation::isComplete() const
{
    QRegExp re("[a-zA-Z][a-zA-Z0-9]{2,}");
    if (!re.exactMatch(station->currentText()))
        return false;
    return true;
}

void InitialSetupSyncAddStation::commitSettings()
{
    registerDefaultStation(station->currentText());
}


void InitialSetupSyncAddStation::updateDescription()
{
    QString desc;
    {
        auto check = profileToDescription.find(profile->currentData().toString().toStdString());
        if (check != profileToDescription.end()) {
            desc = check->second;
        }
    }
    description->setText(desc);
}


InitialSetupProgress::InitialSetupProgress(InitialSetup *parent) : QWizardPage(parent),
                                                                   settings(CPD3GUI_ORGANIZATION,
                                                                            CPD3GUI_APPLICATION),
                                                                   start(FP::undefined()),
                                                                   end(FP::undefined()),
                                                                   incoming(),
                                                                   label(NULL),
                                                                   bar(NULL),
                                                                   mutex(),
                                                                   hasBeenAborted(false)
{

    QVBoxLayout *layout = new QVBoxLayout(this);
    setLayout(layout);

    label = new QLabel(this);
    label->setAlignment(Qt::AlignCenter);
    layout->addWidget(label);

    bar = new QProgressBar(this);
    layout->addWidget(bar);

    layout->addStretch(1);

    connect(parent, SIGNAL(rejected()), this, SLOT(abort()), Qt::DirectConnection);

    incoming.updated.connect(this, std::bind(&InitialSetupProgress::feedbackUpdate, this), true);
    incoming.attach(feedback);
}

InitialSetupProgress::~InitialSetupProgress() = default;

void InitialSetupProgress::initializePage()
{
    QWizardPage::initializePage();
    initialize();
}

void InitialSetupProgress::feedbackAttach(ActionFeedback::Source &source)
{ incoming.attach(source); }

void InitialSetupProgress::feedbackUpdate()
{
    auto all = incoming.process();
    if (all.size() > 1) {
        for (auto check = all.cbegin(); check != all.cend() - 1; ++check) {
            if (check->state() != ActionFeedback::Serializer::Stage::Failure)
                continue;

            QString message;
            const auto &reason = check->failureReason();
            if (!reason.isEmpty()) {
                message = tr("FAILURE: %1: %2", "failure").arg(check->title(), reason);
            } else {
                message = tr("FAILURE: %1", "failure").arg(check->title());
            }

            QMessageBox::warning(this, tr("Failure Detected"), message);
        }
    }

    ActionFeedback::Serializer::Stage stage = all.back();

    if (bar) {
        double fraction = stage.toFraction(start, end);
        if (stage.state() == ActionFeedback::Serializer::Stage::Spin && FP::defined(fraction)) {
            bar->setMinimum(0);
            bar->setMaximum(0);
            bar->setValue(1);
        } else {
            bar->setMinimum(0);
            bar->setMaximum(1000);
            bar->setValue((int) ::floor(fraction * 1000.0));
        }

        bar->setToolTip(stage.description());
    }

    if (label) {
        if (stage.state() == ActionFeedback::Serializer::Stage::Failure) {
            QString message;
            const auto &reason = stage.failureReason();
            if (!reason.isEmpty()) {
                message = tr("FAILURE -- %1: %2", "failure").arg(stage.title(), reason);
            } else {
                message = tr("FAILURE -- %1", "failure").arg(stage.title());
            }
            label->setText(message);
        } else {
            QString title = stage.title();
            if (title.isEmpty())
                title = tr("Initializing");

            double time = stage.time();
            if (FP::defined(time)) {
                if (!stage.stageStatus().isEmpty()) {
                    label->setText(tr("%1 -- %2 (%3)", "stage state label time").arg(title,
                                                                                     GUITime::formatTime(
                                                                                             time,
                                                                                             &settings),
                                                                                     stage.stageStatus()));
                } else {
                    label->setText(tr("%1 -- %2", "stage label time").arg(title,
                                                                          GUITime::formatTime(time,
                                                                                              &settings)));
                }
            } else {
                if (!stage.stageStatus().isEmpty()) {
                    label->setText(
                            tr("%1 (%2)", "stage state label").arg(title, stage.stageStatus()));
                } else {
                    label->setText(title);
                }
            }
        }
    }
}

void InitialSetupProgress::setBounds(double start, double end)
{
    this->start = start;
    this->end = end;
}

void InitialSetupProgress::initialize()
{
    if (bar != NULL) {
        bar->setTextVisible(false);
        bar->setMinimum(0);
        bar->setMaximum(0);
        bar->setValue(1);
        bar->setToolTip(QString());
        bar->show();
    }
    if (label != NULL) {
        label->setText(QString());
    }
}

void InitialSetupProgress::finished()
{
    if (bar != NULL) {
        bar->setMinimum(0);
        bar->setMaximum(0);
        bar->setValue(1);
        bar->setToolTip(QString());
        bar->hide();
    }
    if (label != NULL) {
        label->setText(QString());
    }
}

void InitialSetupProgress::abort()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        hasBeenAborted = true;
    }
    emit aborted();
}

bool InitialSetupProgress::isAborted()
{
    std::lock_guard<std::mutex> lock(mutex);
    return hasBeenAborted;
}

void InitialSetupProgress::spinStage(const QString &name, const QString &description)
{
    ActionFeedback::Source dummy;
    incoming.attach(dummy);
    dummy.emitStage(name, description);
}


InitialSetupSyncInitialize::InitialSetupSyncInitialize(InitialSetup *parent) : InitialSetupProgress(
        parent), state(State_Bootstrap), bootstrap()
{
    setTitle(tr("Initializing Data Synchronization"));

    connect(&bootstrap, SIGNAL(finished()), this, SLOT(updateState()));
}

InitialSetupSyncInitialize::~InitialSetupSyncInitialize() = default;

void InitialSetupSyncInitialize::initializePage()
{
    InitialSetupProgress::initializePage();

    setSubTitle(tr("The system is initializing and loading the synchronization configuration."));

    state = State_Bootstrap;
    feedback.emitStage(tr("Initializing Database"),
                       tr("The initial database information is being downloaded and written to local storage."),
                       false);

    bootstrap.setFuture(QtConcurrent::run(performBootstrap, QString("synchronize"), this));
}

bool InitialSetupSyncInitialize::isComplete() const
{
    if (!InitialSetupProgress::isComplete())
        return false;
    if (state != State_Completed)
        return false;
    return true;
}

int InitialSetupSyncInitialize::nextId() const
{
    if (state == State_Failed)
        return -1;
    return InitialSetupProgress::nextId();
}

void InitialSetupSyncInitialize::updateState()
{
    switch (state) {
    case State_Bootstrap: {
        if (!bootstrap.isFinished())
            return;
        if (!bootstrap.result()) {
            state = State_Failed;

            qCDebug(log_guidata_initialsetup) << "Synchronization bootstrap failed";

            setSubTitle(
                    tr("Error downloading base configuration.  Please check your network connection and try again."));

            finished();
            return;
        }

        state = State_Completed;
        setTitle(tr("Synchronization Initialized"));
        setSubTitle(
                tr("The synchronization configuration has been initialized.  Data from a station can now be requested."));

        finished();
        emit completeChanged();
        break;
    }

    case State_Failed:
    case State_Completed:
        finished();
        return;
    }
}

InitialSetupSyncProcessing::InitialSetupSyncProcessing(InitialSetup *parent) : InitialSetupProgress(
        parent), state(State_Generate), generate(), upload(), local()
{
    setTitle(tr("Synchronization Request Processing"));

    connect(&generate, SIGNAL(finished()), this, SLOT(updateState()));
    connect(&upload, SIGNAL(finished()), this, SLOT(updateState()));
    connect(&local, SIGNAL(finished()), this, SLOT(updateState()));
}

InitialSetupSyncProcessing::~InitialSetupSyncProcessing()
{
    if (upload.isRunning())
        upload.waitForFinished();
}

void InitialSetupSyncProcessing::initializePage()
{
    InitialSetupProgress::initializePage();

    setSubTitle(tr("The system is generating and submitting the synchronization request."));

    state = State_Generate;

    feedback.emitStage(tr("Generating Certificate"),
                       tr("A certificate and key pair are being generated for use with the "
                          "synchronization request."), false);

    QStringList subject;

    auto name = field("name").toString();
    name.replace(',', QString());
    name.replace('/', QString());
    if (!name.isEmpty())
        subject << "CN=" + name;

    auto email = field("email").toString();
    email.replace(',', QString());
    email.replace('/', QString());
    if (!email.isEmpty())
        subject << "emailAddress=" + email;

    auto note = field("note").toString();
    note.replace(',', QString());
    note.replace('/', QString());
    if (!note.isEmpty())
        subject << "dnQualifier=" + note;

    auto profile = field("sync_profile").toString();
    profile.replace(',', QString());
    profile.replace('/', QString());
    if (!profile.isEmpty())
        subject << "OU=" + profile;

    qCDebug(log_guidata_initialsetup) << "Generating certificate for" << name << "with" << email
                                      << "(" << note << ") requesting profile" << profile;

    generate.setFuture(
            QtConcurrent::run(Algorithms::Cryptography::generateSelfSigned, subject, 2048));
}

bool InitialSetupSyncProcessing::isComplete() const
{
    if (!InitialSetupProgress::isComplete())
        return false;
    if (state != State_Completed)
        return false;
    return true;
}

int InitialSetupSyncProcessing::nextId() const
{ return -1; }

namespace {
class SyncCertificateUploader : public CPD3::Transfer::FileUploader {
    class DigestReplacer : public TextSubstitutionStack::Replacer {
        QByteArray digest;

        QString toLong() const
        {
            static const int length = 64;
            return digest.leftJustified(length, (char) 0, true).toHex().toLower();
        }

        QString toShort() const
        {
            static const char digits[33] = "0123456789abcdefghijklmnopqrstuv";
            static const int length = 16;

            QString result;
            for (const char *add = digest.constData(), *end = add + digest.size();
                    add != end && result.length() < length;
                    ++add) {
                result.append(digits[((quint8) (*add)) >> 3]);
            }
            return result.leftJustified(length, '0', true);
        }

    public:
        DigestReplacer(const QByteArray &d) : digest(d)
        { }

        virtual ~DigestReplacer()
        { }

        virtual QString get(const QStringList &elements) const
        {
            QString type;
            int index = 0;
            if (index < elements.size())
                type = elements.at(index++).toLower();

            if (type == "short") {
                return TextSubstitutionStack::formatString(toShort(), elements.mid(index));
            }
            return TextSubstitutionStack::formatString(toLong(), elements.mid(index));
        }
    };

    TextSubstitutionStack subs;

public:
    SyncCertificateUploader(const Variant::Read &config,
                            const SequenceName::Component &station,
                            const QByteArray &digest,
                            const QByteArray &data) : CPD3::Transfer::FileUploader(config), subs()
    {
        subs.setString("station", QString::fromStdString(station));
        subs.setTime("time", Time::time(), false);
        subs.setString("uid", Random::string(8));

        subs.setReplacement("local", new DigestReplacer(digest));
        subs.setReplacement("decrypt", new DigestReplacer(digest));
        subs.setReplacement("sign", new DigestReplacer(digest));

        addUpload(IO::Access::buffer(Util::ByteArray(data)));
    }

    virtual ~SyncCertificateUploader() = default;

protected:

    virtual QString applySubstitutions(const QString &input) const
    {
        return subs.apply(input);
    }

    virtual void pushTemporaryOutput(const QFileInfo &info)
    {
        subs.push();
        subs.setFile("upload", info);
        subs.setFile("source", info);
        subs.setFile("input", info);
    }

    virtual void popTemporaryOutput()
    {
        subs.pop();
    }

    virtual void pushInputFile(const QFileInfo &info)
    {
        subs.push();
        subs.setFile("input", info);
        subs.setFile("upload", info);
        subs.setFile("source", info);
        subs.setString("file", info.fileName());
    }

    virtual void popInputFile()
    {
        subs.pop();
    }

    virtual QString getMIMEType(const QString &fileName) const
    {
        Q_UNUSED(fileName);
        return "application/x-pem-file";
    }
};
}

static bool performCertificateUpload(const SequenceName::Component &station,
                                     const Variant::Read &certificate,
                                     InitialSetupSyncProcessing *page)
{
    if (station.empty()) {
        qCDebug(log_guidata_initialsetup) << "No station set";
        return false;
    }
    QSslCertificate cert(Algorithms::Cryptography::getCertificate(certificate, false));
    if (cert.isNull()) {
        qCDebug(log_guidata_initialsetup) << "No certificate to upload for" << station;
        return false;
    }

    Variant::Read config;
    {
        double tnow = Time::time();
        auto confList = SequenceSegment::Stream::read(
                Archive::Selection(tnow - 1.0, tnow + 1.0, {station}, {"configuration"},
                                   {"synchronize"}));
        auto f = Range::findIntersecting(confList.begin(), confList.end(), tnow);
        if (f == confList.end()) {
            qCDebug(log_guidata_initialsetup) << "No upload configuration for" << station;
            return false;
        }
        config = f->getValue(SequenceName(station, "configuration",
                                          "synchronize"))["Processing/Request/Transfer"];
    }

    if (!config.exists()) {
        qCDebug(log_guidata_initialsetup) << "Invalid upload configuration for" << station;
        return false;
    }

    qCDebug(log_guidata_initialsetup) << "Starting synchronization certificate upload for:"
                                      << station;

    SyncCertificateUploader
            upload(config, station, Algorithms::Cryptography::sha512(cert), cert.toPem());
    QObject context;
    QObject::connect(page, &InitialSetupSyncProcessing::aborted, &context,
                     std::bind(&SyncCertificateUploader::signalTerminate, &upload),
                     Qt::DirectConnection);
    page->feedbackAttach(upload.feedback);

    if (page->isAborted())
        return false;

    QEventLoop loop;
    bool result = false;
    std::thread thread([&] {
        result = upload.exec();
        Threading::runQueuedFunctor(&loop, std::bind(&QEventLoop::quit, &loop));
    });
    loop.exec();
    thread.join();
    return result;
}

static bool writeSynchronizeCertificate(const SequenceName::Component &station,
                                        const std::string &profile,
                                        const Variant::Read &certificate,
                                        InitialSetupSyncProcessing *page)
{
    if (station.empty()) {
        qCDebug(log_guidata_initialsetup) << "No station set";
        return false;
    }

    Archive::Access access;
    for (;;) {
        if (page->isAborted())
            return false;
        Variant::Root config;

        config["Client/SSL"].set(certificate);

        config["Files/Sign"].set(certificate);
        QSslCertificate cert(Algorithms::Cryptography::getCertificate(certificate, false));
        if (!cert.isNull()) {
            config["Files/Decryption"].hash(QString::fromLatin1(
                                              Algorithms::Cryptography::sha512(cert).toHex()
                                                                                    .toLower()))
                                      .set(certificate);
            qCWarning(log_guidata_initialsetup) << "Setting synchronization certificate for"
                                                << station << "to"
                                                << Algorithms::Cryptography::sha512(cert).toHex()
                                                                                         .toLower();
        } else {
            qCWarning(log_guidata_initialsetup) << "Certificate parse failed";
        }

        Archive::Access::WriteLock lock(access);

        access.writeSynchronous(SequenceValue::Transfer{
                SequenceValue({station, "configuration", "synchronize"}, config, FP::undefined(),
                              FP::undefined(), 1)});
        if (lock.commit())
            break;
    }

    {
        QSettings settings("NOAA ESRL GMD", "CPD3Default");
        auto check = settings.value("profile").toString();
        if (check.isEmpty()) {
            Variant::Read config;
            {
                double tnow = Time::time();
                auto confList = SequenceSegment::Stream::read(
                        Archive::Selection(tnow - 1.0, tnow + 1.0, {station}, {"configuration"},
                                           {"synchronize"}));
                auto f = Range::findIntersecting(confList.begin(), confList.end(), tnow);
                if (f != confList.end()) {
                    config = f->getValue(SequenceName(station, "configuration",
                                                      "synchronize"))["Processing/Request/Profiles"];
                }
            }

            auto set = config.hash(profile).hash("DefaultProfile").toString();
            if (!set.empty()) {
                settings.setValue("profile", QString::fromStdString(set));
                qCDebug(log_guidata_initialsetup) << "Default profile set to:" << set;
            }
        }
    }

    return true;
}

void InitialSetupSyncProcessing::updateState()
{
    switch (state) {
    case State_Generate: {
        if (!generate.isFinished())
            return;

        auto result = generate.result().read();
        if (!result.hash("Certificate").exists() || !result.hash("Key").exists()) {
            state = State_Failed;

            setSubTitle(tr("Error during certificate generation."));

            finished();
            return;
        }

        state = State_Upload;
        feedback.emitStage(tr("Uploading Request"),
                           tr("The upload of the certificate for authorization is starting."),
                           false);

        qCDebug(log_guidata_initialsetup) << "Starting certificate upload";

        upload.setFuture(QtConcurrent::run(performCertificateUpload,
                                           field("sync_station").toString().toStdString(),
                                           result.hash("Certificate"), this));
        break;
    }

    case State_Upload:
        if (!upload.isFinished())
            return;

        if (!upload.result()) {
            state = State_Failed;

            setSubTitle(
                    tr("Error uploading request.  Please check your network connection and try again."));

            finished();
            return;
        }

        state = State_WriteLocal;
        feedback.emitStage(tr("Saving Changes"),
                           tr("The certificate is being saved to the local archive."), false);

        qCDebug(log_guidata_initialsetup) << "Applying certificate locally";

        local.setFuture(QtConcurrent::run(writeSynchronizeCertificate,
                                          field("sync_station").toString().toStdString(),
                                          field("sync_profile").toString().toStdString(),
                                          generate.result(), this));
        break;

    case State_WriteLocal:
        if (!local.isFinished())
            return;

        if (!local.result()) {
            state = State_Failed;

            setSubTitle(tr("Error saving certificate."));

            finished();
            return;
        }

        state = State_Completed;
        setTitle(tr("Synchronization Request Submitted"));
        setSubTitle(
                tr("Synchronization authorization request for %1 submitted.  You will receive an email when it is processed.")
                        .arg(field("sync_station").toString().toUpper()));

        qCDebug(log_guidata_initialsetup) << "Synchronization request complete";

        wizard()->button(QWizard::BackButton)->setEnabled(false);
        wizard()->button(QWizard::CancelButton)->setEnabled(false);

        finished();
        emit completeChanged();
        break;

    case State_Failed:
    case State_Completed:
        finished();
        return;
    }
}


InitialSetupAcquisitionStation::InitialSetupAcquisitionStation(InitialSetup *parent) : QWizardPage(
        parent)
{
    setTitle(tr("Select a Station"));
    setSubTitle(
            tr("Select an existing station or add another one.  Enter the GAW ID of the local station that data are being acquired for."));

    QFormLayout *layout = new QFormLayout(this);
    setLayout(layout);

    station = new QComboBox(this);
    station->setEditable(true);
    station->setInsertPolicy(QComboBox::InsertAlphabetically);
    registerField("acquisition_station*", station, "currentText", SIGNAL(editTextChanged(
                                                                                 const QString &)));
    layout->addRow(tr("&Station"), station);

    connect(station, SIGNAL(editTextChanged(
                                    const QString &)), this, SIGNAL(completeChanged()));

    connect(parent, SIGNAL(accepted()), this, SLOT(commitSettings()));
}

InitialSetupAcquisitionStation::~InitialSetupAcquisitionStation() = default;

void InitialSetupAcquisitionStation::initializePage()
{
    QWizardPage::initializePage();

    std::vector<SequenceName::Component> stations;
    {
        auto all = Archive::Access().availableStations();
        std::copy(all.begin(), all.end(), Util::back_emplacer(stations));
    }
    std::sort(stations.begin(), stations.end());
    for (const auto &add : stations) {
        if (add.empty() || add == "_")
            continue;
        auto displayStation = QString::fromStdString(add);
        if (station->findText(displayStation, Qt::MatchFixedString) != -1)
            continue;
        station->addItem(displayStation.toUpper());
    }
}

bool InitialSetupAcquisitionStation::isComplete() const
{
    if (!QWizardPage::isComplete())
        return false;
    QRegExp re("[a-zA-Z][a-zA-Z0-9]{2,}");
    if (!re.exactMatch(station->currentText()))
        return false;
    return true;
}

void InitialSetupAcquisitionStation::commitSettings()
{
    registerDefaultStation(station->currentText());
}

InitialSetupAcquisitionInitialize::InitialSetupAcquisitionInitialize(InitialSetup *parent)
        : InitialSetupProgress(parent),
          state(State_Bootstrap_Global),
          bootstrapGlobal(),
          bootstrapStation(),
          existing(),
          metadata()
{
    setTitle(tr("Initializing Acquisition System"));

    connect(&bootstrapGlobal, SIGNAL(finished()), this, SLOT(updateState()));
    connect(&bootstrapStation, SIGNAL(finished()), this, SLOT(updateState()));
    connect(&existing, SIGNAL(finished()), this, SLOT(updateState()));
    connect(&metadata, SIGNAL(finished()), this, SLOT(updateState()));
}

InitialSetupAcquisitionInitialize::~InitialSetupAcquisitionInitialize()
{
    if (existing.isRunning())
        existing.waitForFinished();
}

void InitialSetupAcquisitionInitialize::initializePage()
{
    InitialSetupProgress::initializePage();

    setSubTitle(tr("The system is initializing and loading the acquisition configuration."));

    state = State_Bootstrap_Global;
    feedback.emitStage(tr("Initializing Database"),
                       tr("The initial database information is being downloaded and written to local storage."),
                       false);

    bootstrapGlobal.setFuture(QtConcurrent::run(performBootstrap, QString("acquisition"), this));
}

bool InitialSetupAcquisitionInitialize::isComplete() const
{
    if (!InitialSetupProgress::isComplete())
        return false;
    if (state != State_Completed)
        return false;
    return true;
}

int InitialSetupAcquisitionInitialize::nextId() const
{
    if (state == State_Failed)
        return -1;
    return InitialSetupProgress::nextId();
}

static Variant::Root createDefaultAcqusitionConfiguration()
{
    Variant::Root result;
    auto profile = result.write().hash(SequenceName::impliedProfile());
    profile["Autoprobe/DisableDefaultComponents"] = true;
    return result;
}

static Variant::Write loadExistingAcquisition(const SequenceName::Component &station,
                                              InitialSetupAcquisitionInitialize *page)
{
    double now = Time::time();
    auto segments = ValueSegment::Stream::read(
            Archive::Selection(now, now + 0.001, {station}, {"configuration"}, {"acquisition"}));
    auto target = Range::findIntersecting(segments.begin(), segments.end(), now);

    if (target == segments.end())
        return createDefaultAcqusitionConfiguration().write();

    auto v = target->write();
    if (!v.exists())
        return createDefaultAcqusitionConfiguration().write();

    return v;
}

static Variant::Read loadAcquisitionMetadata(const SequenceName::Component &station,
                                             InitialSetupAcquisitionInitialize *page)
{
    double now = Time::time();
    ValueSegment::Transfer segments;

    StreamSink::Iterator data;
    Archive::Access access;
    access.readStream(
            Archive::Selection(now, now + 0.001, {station}, {"configuration_meta"}, {"acquisition"})
                    .withMetaArchive(false), &data)->detach();
    ValueSegment::Stream reader;
    for (;;) {
        auto add = data.all();
        if (add.empty())
            break;
        Util::append(reader.add(std::move(add)), segments);
    }
    Util::append(reader.finish(), segments);

    auto target = Range::findIntersecting(segments, now);
    if (target == segments.end())
        return Variant::Read::empty();

    return target->read();
}

void InitialSetupAcquisitionInitialize::updateState()
{
    switch (state) {
    case State_Bootstrap_Global: {
        if (!bootstrapGlobal.isFinished())
            return;
        if (!bootstrapGlobal.result()) {
            state = State_Failed;

            qCDebug(log_guidata_initialsetup) << "Acquisition global bootstrap failed";

            setSubTitle(
                    tr("Error downloading base configuration.  Please check your network connection and try again."));

            finished();
            return;
        }

        state = State_Bootstrap_Station;

        feedback.emitStage(tr("Initializing Station"),
                           tr("Any station specific configuration is being downloaded and written to local storage."),
                           false);

        bootstrapStation.setFuture(QtConcurrent::run(performBootstrap,
                                                     QString("acquisition-%1").arg(
                                                             field("acquisition_station").toString()
                                                                                         .toLower()),
                                                     this));

        break;
    }

    case State_Bootstrap_Station: {
        if (!bootstrapStation.isFinished())
            return;

        /* Don't check failure, since it's fine if a station has nothing to download */

        state = State_Existing;

        feedback.emitStage(tr("Loading Existing"),
                           tr("The existing acquisition configuration is being loaded from either the local storage."),
                           false);

        qCDebug(log_guidata_initialsetup) << "Loading existing acquisition configuration";

        existing.setFuture(QtConcurrent::run(loadExistingAcquisition,
                                             field("acquisition_station").toString().toStdString(),
                                             this));

        break;
    }

    case State_Existing: {
        if (!existing.isFinished())
            return;
        wizard()->setProperty("acquisition_configuration",
                              QVariant::fromValue<Variant::Write>(existing.result()));

        state = State_Metadata;

        feedback.emitStage(tr("Loading Metadata"),
                           tr("The existing acquisition system metadata is being loaded from the local storage."),
                           false);

        qCDebug(log_guidata_initialsetup) << "Loading acquisition metadata";

        metadata.setFuture(QtConcurrent::run(loadAcquisitionMetadata,
                                             field("acquisition_station").toString().toStdString(),
                                             this));
        break;
    }

    case State_Metadata: {
        if (!metadata.isFinished())
            return;
        wizard()->setProperty("acquisition_metadata",
                              QVariant::fromValue<Variant::Read>(metadata.result()));

        qCDebug(log_guidata_initialsetup) << "Acquisition initialization completed";

        state = State_Completed;
        setTitle(tr("Acquisition Initialized"));
        setSubTitle(
                tr("The current acquisition configuration has been initialized.  Changes can be made on the subsequent pages before finalization."));

        finished();
        emit completeChanged();
        break;
    }

    case State_Failed:
    case State_Completed:
        finished();
        return;
    }
}

namespace {
class ComponentListTableModel : public QAbstractItemModel {

public:
    enum {
        Column_AddRemove, Column_ID, Column_Instrument, Column_Editor,

        Column_TOTAL
    };

private:
    struct ComponentData {
        QString id;
        Variant::Root configuration;
    };
    std::vector<ComponentData> components;

public:

    explicit ComponentListTableModel(InitialSetupAcquisitionComponents *parent)
            : QAbstractItemModel(parent)
    { }

    virtual ~ComponentListTableModel() = default;

    void configure(const Variant::Read &v)
    {
        if (!components.empty())
            removeRows(0, components.size());
        Q_ASSERT(components.empty());

        for (auto child : v.toHash()) {
            if (child.first.empty())
                continue;
            insertRows(components.size(), 1);
            Q_ASSERT(!components.empty());
            components.back().id = QString::fromStdString(child.first);
            components.back().configuration = Variant::Root(child.second);
        }
        emit dataChanged(index(0, 0), index(components.size() - 1, Column_TOTAL - 1));
    }

    inline bool hasDefinedComponents() const
    { return !components.empty(); }

    virtual QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const
    {
        Q_UNUSED(parent);
        return createIndex(row, column);
    }

    virtual QModelIndex parent(const QModelIndex &index) const
    {
        Q_UNUSED(index);
        return QModelIndex();
    }

    virtual int rowCount(const QModelIndex &parent) const
    {
        Q_UNUSED(parent);
        return components.size() + 1;
    }

    virtual int columnCount(const QModelIndex &parent) const
    {
        Q_UNUSED(parent);
        return Column_TOTAL;
    }

    virtual QVariant data(const QModelIndex &index, int role) const
    {
        if (!index.isValid())
            return QVariant();
        if (index.row() >= static_cast<int>(components.size())) {
            if (role == Qt::EditRole) {
                if (index.column() == 0)
                    return 1;
            }
            return QVariant();
        }
        const auto &data = components[index.row()];

        switch (role) {
        case Qt::DisplayRole:
            switch (index.column()) {
            case Column_ID:
                return data.id;
            case Column_Instrument: {
                QString instrument(data.configuration.read().hash("Instrument").toQString());
                if (!instrument.isEmpty())
                    return instrument;
                return tr("Automatic");
            }
            case Column_Editor: {
                QString component(data.configuration.read().hash("Name").toQString());
                if (!component.isEmpty())
                    return component;
                return tr("Automatic");
            }
            default:
                break;
            }
            break;

        case Qt::EditRole:
            switch (index.column()) {
            case Column_ID:
                return data.id;
            case Column_Instrument:
                return data.configuration.read().hash("Instrument").toQString();
            case Column_Editor:
                return QVariant::fromValue<Variant::Write>(
                        const_cast<Variant::Root &>(data.configuration).write());
            default:
                break;
            }
            break;

        case Qt::ForegroundRole:
            switch (index.column()) {
            case Column_Instrument:
                if (data.configuration.read().hash("Instrument").toQString().isEmpty())
                    return QBrush(Qt::lightGray);
                break;
            default:
                break;
            }
            break;

        default:
            break;
        }
        return QVariant();
    }

    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const
    {
        if (role != Qt::DisplayRole)
            return QVariant();
        if (orientation == Qt::Vertical)
            return QVariant(section + 1);

        switch (section) {
        case Column_ID:
            return tr("Identifier");
        case Column_Instrument:
            return tr("Instrument");
        case Column_Editor:
            return tr("Configuration");
        default:
            break;
        }
        return QVariant();
    }

    virtual bool setData(const QModelIndex &index, const QVariant &value, int role)
    {
        if (role != Qt::EditRole)
            return false;

        if (index.row() >= static_cast<int>(components.size()))
            return false;

        switch (index.column()) {
        case Column_ID: {
            QString str(value.toString().trimmed());
            if (str.isEmpty())
                return false;
            for (int i = 0, max = components.size(); i < max; i++) {
                if (i == index.row())
                    continue;
                if (components[index.row()].id == str)
                    return false;
            }
            components[index.row()].id = str;
            emit dataChanged(index, index);
            return true;
        }
        case Column_Instrument: {
            QString str(value.toString().toUpper().trimmed());
            if (str.isEmpty()) {
                components[index.row()].configuration.write().hash("Instrument").remove();
            } else {
                components[index.row()].configuration.write().hash("Instrument").setString(str);
            }
            emit dataChanged(index, index);
            return true;
        }
        case Column_Editor:
            components[index.row()].configuration.write().set(value.value<Variant::Write>());
            emit dataChanged(this->index(index.row(), Column_Instrument),
                             this->index(index.row(), Column_Editor));
            return true;
        default:
            break;
        }

        return false;
    }

    virtual Qt::ItemFlags flags(const QModelIndex &index) const
    {
        if (!index.isValid())
            return 0;

        switch (index.column()) {
        case Column_ID:
        case Column_Instrument:
            return Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled;
        case Column_Editor:
            return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
        default:
            break;
        }

        return 0;
    }

    virtual bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex())
    {
        Q_ASSERT(row + count <= static_cast<int>(components.size()));

        beginRemoveRows(parent, row, row + count - 1);
        auto first = components.begin() + row;
        auto last = first + count;
        components.erase(first, last);
        endRemoveRows();

        return true;
    }

    virtual bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex())
    {
        std::unordered_set<QString> takenIDs;
        for (const auto &c : components) {
            takenIDs.insert(c.id);
        }

        beginInsertRows(parent, row, row + count - 1);
        int counter = 1;
        for (int i = 0; i < count; i++, row++) {
            QString id("New");
            for (; counter > 0 && takenIDs.count(id); ++counter) {
                id = QString("New%1").arg(counter);
            }
            takenIDs.insert(id);

            ComponentData data;
            data.id = id;
            components.emplace(components.begin() + row, std::move(data));
            ++row;
        }
        endInsertRows();

        return true;
    }

    static inline bool compareIDAscending(const ComponentData &a, const ComponentData &b)
    {
        return a.id < b.id;
    }

    static inline bool compareIDDescending(const ComponentData &a, const ComponentData &b)
    {
        return b.id < a.id;
    }

    static inline bool compareInstrumentAscending(const ComponentData &a, const ComponentData &b)
    {
        return a.configuration.read().hash("Instrument").toString() <
                b.configuration.read().hash("Instrument").toString();
    }

    static inline bool compareInstrumentDescending(const ComponentData &a, const ComponentData &b)
    {
        return b.configuration.read().hash("Instrument").toString() <
                a.configuration.read().hash("Instrument").toString();
    }

    virtual void sort(int column, Qt::SortOrder order = Qt::AscendingOrder)
    {
        beginResetModel();
        switch (column) {
        case Column_ID:
            std::stable_sort(components.begin(), components.end(),
                             order == Qt::AscendingOrder ? compareIDAscending
                                                         : compareIDDescending);
            break;
        case Column_Instrument:
            std::stable_sort(components.begin(), components.end(),
                             order == Qt::AscendingOrder ? compareInstrumentAscending
                                                         : compareInstrumentDescending);
            break;
        case Column_Editor:
            break;
        default:
            break;
        }
        endResetModel();
    }

    Variant::Root assembleComponents() const
    {
        Variant::Root result;
        for (const auto &c : components) {
            if (c.id.isEmpty())
                continue;
            result.write().hash(c.id).set(c.configuration);
        }
        return result;
    }
};

class ComponentListAddRemoveButton : public QPushButton {
public:
    explicit ComponentListAddRemoveButton(const QString &text, QWidget *parent = 0) : QPushButton(
            text, parent)
    { }

    virtual ~ComponentListAddRemoveButton() = default;

    virtual QSize sizeHint() const
    {
        QSize textSize(fontMetrics().size(Qt::TextShowMnemonic, text()));
        QStyleOptionButton opt;
        opt.initFrom(this);
        opt.rect.setSize(textSize);
        return style()->sizeFromContents(QStyle::CT_PushButton, &opt, textSize, this);
    }
};

class ComponentListAddRemoveDelegate : public QItemDelegate {
    InitialSetupAcquisitionComponents *editor;
public:
    explicit ComponentListAddRemoveDelegate(InitialSetupAcquisitionComponents *e,
                                            QObject *parent = 0) : QItemDelegate(parent), editor(e)
    { }

    virtual ~ComponentListAddRemoveDelegate() = default;

    virtual QWidget *createEditor(QWidget *parent,
                                  const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const
    {
        Q_UNUSED(option);

        QPushButton *button = new ComponentListAddRemoveButton(tr("-", "remove text"), parent);
        button->setProperty("model-row", index.row());

        return button;
    }

    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
    {
        QPushButton *button = static_cast<QPushButton *>(editor);

        button->disconnect(this->editor);

        QVariant data(index.data(Qt::EditRole));
        if (data.toInt() != 1) {
            button->setText(tr("-", "remove text"));
            connect(button, SIGNAL(clicked(bool)), this->editor, SLOT(removeRow()));
        } else {
            button->setText(tr("Add", "add text"));
            connect(button, SIGNAL(clicked(bool)), this->editor, SLOT(addRow()));
        }

        button->setProperty("model-row", index.row());
    }

    virtual void setModelData(QWidget *editor,
                              QAbstractItemModel *model,
                              const QModelIndex &index) const
    {
        Q_UNUSED(editor);
        Q_UNUSED(model);
        Q_UNUSED(index);
    }
};

class ComponentListEditorDelegate : public QItemDelegate {
public:
    explicit ComponentListEditorDelegate(QObject *parent = 0) : QItemDelegate(parent)
    { }

    virtual ~ComponentListEditorDelegate() = default;

    virtual QWidget *createEditor(QWidget *parent,
                                  const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const
    {
        Q_UNUSED(option);
        Q_UNUSED(index);

        Internal::InitialSetupAcquisitionComponentEditorButton
                *editor = new Internal::InitialSetupAcquisitionComponentEditorButton(parent);
        connect(editor, SIGNAL(commitChanged(QWidget * )), this, SIGNAL(commitData(QWidget * )));
        return editor;
    }

    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
    {
        Internal::InitialSetupAcquisitionComponentEditorButton *selector =
                static_cast<Internal::InitialSetupAcquisitionComponentEditorButton *>(editor);
        selector->setProperty("component-data", index.data(Qt::EditRole));
        selector->setText(index.data(Qt::DisplayRole).toString());
    }

    virtual void setModelData(QWidget *editor,
                              QAbstractItemModel *model,
                              const QModelIndex &index) const
    {
        Internal::InitialSetupAcquisitionComponentEditorButton *selector =
                static_cast<Internal::InitialSetupAcquisitionComponentEditorButton *>(editor);
        model->setData(index, selector->property("component-data"));
    }
};
}

Internal::InitialSetupAcquisitionComponentEditorButton::InitialSetupAcquisitionComponentEditorButton(
        QWidget *parent) : QPushButton(tr("Edit Component"), parent)
{
    connect(this, SIGNAL(clicked(bool)), this, SLOT(showEditor()));
}

Internal::InitialSetupAcquisitionComponentEditorButton::~InitialSetupAcquisitionComponentEditorButton() = default;

void Internal::InitialSetupAcquisitionComponentEditorButton::showEditor()
{
    AcquisitionComponentEditorDialog
            dialog(property("component-data").value<Variant::Write>(), Variant::Root(), this);
    if (dialog.exec() != QDialog::Accepted)
        return;
    setProperty("component-data", QVariant::fromValue<Variant::Write>(dialog.component().write()));
    emit commitChanged(this);
}

InitialSetupAcquisitionComponents::InitialSetupAcquisitionComponents(InitialSetup *parent)
        : QWizardPage(parent)
{
    setTitle(tr("Configure Components"));
    setSubTitle(tr("Select the components of the acquisition system and their settings."));

    QVBoxLayout *layout = new QVBoxLayout(this);
    setLayout(layout);

    disableDefaultComponents = new QCheckBox(tr("&Only use below components"), this);
    layout->addWidget(disableDefaultComponents);
    disableDefaultComponents->setToolTip(tr("Disable additional automatic detection."));
    disableDefaultComponents->setStatusTip(tr("Disable additional components"));
    disableDefaultComponents->setWhatsThis(
            tr("This option causes the system to only consider the listed components for auto-detection.  When not enabled, all components with default settings are considers."));


    ComponentListTableModel *model = new ComponentListTableModel(this);
    componentList = new QTableView(this);
    layout->addWidget(componentList, 1);
    componentList->setModel(model);

    componentList->setToolTip(tr("The table of predefined components."));
    componentList->setStatusTip(tr("Acquisition components"));
    componentList->setWhatsThis(
            tr("This table represents all the predefined components of the acquisition system."));

    componentList->horizontalHeader()
                 ->setSectionResizeMode(ComponentListTableModel::Column_AddRemove,
                                        QHeaderView::ResizeToContents);

    componentList->horizontalHeader()
                 ->setSectionResizeMode(ComponentListTableModel::Column_ID,
                                        QHeaderView::ResizeToContents);

    componentList->horizontalHeader()
                 ->setSectionResizeMode(ComponentListTableModel::Column_Instrument,
                                        QHeaderView::ResizeToContents);

    componentList->horizontalHeader()
                 ->setSectionResizeMode(ComponentListTableModel::Column_Editor,
                                        QHeaderView::Stretch);

    componentList->setItemDelegateForColumn(ComponentListTableModel::Column_AddRemove,
                                            new ComponentListAddRemoveDelegate(this,
                                                                               componentList));

    componentList->setItemDelegateForColumn(ComponentListTableModel::Column_Editor,
                                            new ComponentListEditorDelegate(componentList));

    componentList->setSortingEnabled(true);
    componentList->sortByColumn(ComponentListTableModel::Column_ID, Qt::AscendingOrder);


    componentList->setSpan(model->rowCount(QModelIndex()) - 1, 0, 1,
                           ComponentListTableModel::Column_TOTAL);

    openAllEditors();

    showAdvanced = new QCheckBox(tr("&Advanced configuration"), this);
    layout->addWidget(showAdvanced);
    showAdvanced->setToolTip(tr("Enable the advanced configuration editing step."));
    showAdvanced->setStatusTip(tr("Advanced configuration"));
    showAdvanced->setWhatsThis(
            tr("This enabled the manual advanced configuration editor before completing setup.  This can be used to edit all acquisition settings."));

    connect(showAdvanced, SIGNAL(toggled(bool)), this, SIGNAL(completeChanged()));
}

InitialSetupAcquisitionComponents::~InitialSetupAcquisitionComponents() = default;

void InitialSetupAcquisitionComponents::initializePage()
{
    QWizardPage::initializePage();

    ComponentListTableModel *model = static_cast<ComponentListTableModel *>(componentList->model());
    disableDefaultComponents->disconnect(this);
    model->disconnect(this);

    configuration = wizard()->property("acquisition_configuration")
                            .value<Variant::Write>()
                            .hash(SequenceName::impliedProfile());

    disableDefaultComponents->setChecked(
            configuration["Autoprobe/DisableDefaultComponents"].toBool());

    model->configure(configuration["Components"]);
    componentList->sortByColumn(ComponentListTableModel::Column_ID, Qt::AscendingOrder);

    openAllEditors();

    componentList->resizeColumnToContents(ComponentListTableModel::Column_AddRemove);
    componentList->resizeColumnToContents(ComponentListTableModel::Column_ID);
    componentList->resizeColumnToContents(ComponentListTableModel::Column_Instrument);

    connect(disableDefaultComponents, SIGNAL(toggled(bool)), this, SIGNAL(completeChanged()));
    connect(model, SIGNAL(dataChanged(
                                  const QModelIndex &, const QModelIndex &)), this,
            SIGNAL(completeChanged()));

    connect(disableDefaultComponents, SIGNAL(toggled(bool)), this, SLOT(commitSettings()));
    connect(model, SIGNAL(dataChanged(
                                  const QModelIndex &, const QModelIndex &)), this,
            SLOT(commitSettings()));
}

bool InitialSetupAcquisitionComponents::isComplete() const
{
    if (!QWizardPage::isComplete())
        return false;
    if (showAdvanced->isChecked())
        return true;
    if (!disableDefaultComponents->isChecked())
        return true;
    ComponentListTableModel *model = static_cast<ComponentListTableModel *>(componentList->model());
    return model->hasDefinedComponents();
}

void InitialSetupAcquisitionComponents::commitSettings()
{
    configuration["Autoprobe/DisableDefaultComponents"].setBool(
            disableDefaultComponents->isChecked());

    ComponentListTableModel *model = static_cast<ComponentListTableModel *>(componentList->model());
    auto components = model->assembleComponents();
    if (!components.read().exists())
        configuration["Components"].remove();
    else
        configuration["Components"].set(components);
}

void InitialSetupAcquisitionComponents::addRow()
{
    componentList->model()->insertRows(componentList->model()->rowCount() - 1, 1);

    openAllEditors();
}

void InitialSetupAcquisitionComponents::removeRow()
{
    QObject *s = sender();
    if (!s)
        return;
    QVariant p(s->property("model-row"));
    if (!p.isValid())
        return;
    int row = p.toInt();
    if (row < 0 || row >= componentList->model()->rowCount() - 1)
        return;

    componentList->model()->removeRows(row, 1);

    openAllEditors();
}

void InitialSetupAcquisitionComponents::openAllEditors()
{
    ComponentListTableModel *model = static_cast<ComponentListTableModel *>(componentList->model());
    for (int row = 0; row < model->rowCount(QModelIndex()); ++row) {
        componentList->openPersistentEditor(
                model->index(row, ComponentListTableModel::Column_AddRemove));
        if (row < (model->rowCount(QModelIndex()) - 1)) {
            componentList->openPersistentEditor(
                    model->index(row, ComponentListTableModel::Column_Editor));
        }
    }
}

int InitialSetupAcquisitionComponents::nextId() const
{
    if (showAdvanced->isChecked())
        return InitialSetup::Page_Acquisition_Advanced;
    return InitialSetup::Page_Acquisition_Processing;
}

InitialSetupAcquisitionAdvanced::InitialSetupAcquisitionAdvanced(InitialSetup *parent)
        : QWizardPage(parent)
{
    setTitle(tr("Advanced Configuration"));
    setSubTitle(
            tr("This section allows for manual advanced manipulation of the acquisition system configuration."));

    QVBoxLayout *layout = new QVBoxLayout(this);
    setLayout(layout);

    editor = new ValueEditor(this);
    layout->addWidget(editor, 1);
}

InitialSetupAcquisitionAdvanced::~InitialSetupAcquisitionAdvanced() = default;

void InitialSetupAcquisitionAdvanced::initializePage()
{
    QWizardPage::initializePage();

    editor->setValue(wizard()->property("acquisition_configuration").value<Variant::Write>(),
                     wizard()->property("acquisition_metadata").value<Variant::Read>());
}

static bool writeAcquisitionChanges(const SequenceName::Component &station,
                                    const Variant::Read &configuration,
                                    InitialSetupAcquisitionProcessing *page)
{
    Archive::Access access;
    qCDebug(log_guidata_initialsetup) << "Starting acquisition changes write for" << station;
    for (;;) {
        if (page->isAborted())
            return false;
        Archive::Access::WriteLock lock(access);

        double now = Time::time();

        StreamSink::Iterator existing;
        access.readStream(Archive::Selection(now, FP::undefined(), {station}, {"configuration"},
                                             {"acquisition"}).withMetaArchive(false), &existing)
              ->detach();

        ValueSegment::Transfer segments;
        SequenceValue::Transfer toModify;
        SequenceIdentity::Transfer toRemove;
        {
            ValueSegment::Stream reader;
            while (existing.hasNext()) {
                auto v = existing.next();
                Util::append(reader.add(v), segments);

                toRemove.emplace_back(v);

                if (!FP::defined(v.getStart()) || v.getStart() < now) {
                    SequenceValue mod = v;
                    mod.setEnd(now);
                    toModify.emplace_back(std::move(mod));
                }
            }
            Util::append(reader.finish(), segments);
        }

        auto target = Range::findIntersecting(segments, now);
        if (target != segments.end() && target->getValue() == configuration)
            return true;
        segments.clear();

        toModify.push_back(SequenceValue({station, "configuration", "acquisition"},
                                         Variant::Root(configuration), now, FP::undefined()));

        access.writeSynchronous(toModify, toRemove);
        if (lock.commit())
            break;
    }
    qCDebug(log_guidata_initialsetup) << "Acquisition changes saved for" << station;
    return true;
}

InitialSetupAcquisitionProcessing::InitialSetupAcquisitionProcessing(InitialSetup *parent)
        : InitialSetupProgress(parent), state(State_Write), write()
{
    setTitle(tr("Acquisition Finalization"));

    connect(&write, SIGNAL(finished()), this, SLOT(updateState()));
}

InitialSetupAcquisitionProcessing::~InitialSetupAcquisitionProcessing() = default;

void InitialSetupAcquisitionProcessing::initializePage()
{
    InitialSetupProgress::initializePage();

    setSubTitle(tr("The system is writing the configuration to the local data storage."));

    state = State_Write;
    feedback.emitStage(tr("Write Changes"), tr("The acquisition configuration is being modified."),
                       false);

    write.setFuture(QtConcurrent::run(writeAcquisitionChanges,
                                      field("acquisition_station").toString().toStdString(),
                                      wizard()->property("acquisition_configuration")
                                              .value<Variant::Write>(), this));
}

bool InitialSetupAcquisitionProcessing::isComplete() const
{
    if (!InitialSetupProgress::isComplete())
        return false;
    if (state != State_Completed)
        return false;
    return true;
}

int InitialSetupAcquisitionProcessing::nextId() const
{ return -1; }

void InitialSetupAcquisitionProcessing::updateState()
{
    switch (state) {
    case State_Write: {
        if (!write.isFinished())
            return;

        if (!write.result()) {
            state = State_Failed;

            setSubTitle(tr("Error writing changes."));

            finished();
            return;
        }

        state = State_Completed;
        setTitle(tr("Changes Writen"));
        setSubTitle(
                tr("The requested changed to the acquisition system have been made.  You will need to start or restart it for them to take effect."));

        wizard()->button(QWizard::BackButton)->setEnabled(false);
        wizard()->button(QWizard::CancelButton)->setEnabled(false);

        finished();
        emit completeChanged();
        break;

    case State_Failed:
    case State_Completed:
        finished();
        return;
    }
    }
}

}


}
}
}
