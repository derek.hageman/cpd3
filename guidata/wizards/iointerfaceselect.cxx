/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QWizard>
#include <QHBoxLayout>
#include <QButtonGroup>
#include <QFrame>
#include <QFileDialog>
#include <QPushButton>
#include <QIntValidator>
#include <QHostAddress>
#include <QSslCertificate>
#include <QInputDialog>
#include <QItemDelegate>
#include <QHeaderView>

#include "guidata/wizards/iointerfaceselect.hxx"
#include "acquisition/iotarget.hxx"
#include "algorithms/cryptography.hxx"
#include "core/util.hxx"

using namespace CPD3::Data;
using namespace CPD3::Acquisition;
using namespace CPD3::Algorithms;
using namespace CPD3::GUI::Data::Internal;

namespace CPD3 {
namespace GUI {
namespace Data {

/** @file guidata/wizards/iointerfaceselect.hxx
 * Provides a tool to select an I/O interface for use.
 */


IOInterfaceSelect::IOInterfaceSelect(QWidget *parent, Qt::WindowFlags flags) : QWizard(parent,
                                                                                       flags),
                                                                               interface(),
                                                                               inspectAdvanced(true)
{
    setWindowTitle(tr("Interface Selection"));

    typeSelectPage = new IOInterfaceSelectTypePage(this);
    setPage(Page_TypeSelect, typeSelectPage);

    setPage(Page_LocalSerialPortManual, new IOInterfaceSerialPortManualPage(this));
    serialPortAdvancedPage = new IOInterfaceSerialPortAdvancedPage(this);
    setPage(Page_LocalSerialPortAdvanced, serialPortAdvancedPage);

    {
        IOInterfaceRemoteConnectionPage *conn =
                new IOInterfaceRemoteConnectionPage(this, "remoteServerName", "remoteServerPort",
                                                    "remoteServerSSL");
        conn->setTitle(tr("Select Remote TCP/IP Server"));
        conn->setSubTitle(
                tr("This is most often a RS232 to network converter operated in \"transparent\" mode."));
        setPage(Page_RemoteServer, conn);
    }
    {
        IOInterfaceRemoteConnectionPage *conn =
                new IOInterfaceRemoteConnectionPage(this, "cpd3ServerName", "cpd3ServerPort",
                                                    "cpd3ServerSSL");
        conn->setTitle(tr("Select CPD3 Interface for Remote Access"));
        conn->setSubTitle(
                tr("The remote CPD3 interface must have its own remote access settings compatible with settings below."));
        setPage(Page_RemoteCPD3, conn);
    }

    {
        IOInterfaceNetworkServerPage *conn =
                new IOInterfaceNetworkServerPage(this, "tcpListenAddress", "tcpListenPort", {}, {},
                                                 "tcpListenSSL");
        conn->setTitle(tr("TCP/IP Server Configuration"));
        conn->setSubTitle(
                tr("This configures a server that will wait for incoming connections then transfer data to any open connections."));
        setPage(Page_TCPListen, conn);
    }
    {
        IOInterfaceNetworkServerPage *conn =
                new IOInterfaceNetworkServerPage(this, "udpListenAddress", "udpListenPort",
                                                 "udpServerAddress", "udpServerPort",
                                                 "udpServerFragment", "udpDisableFraming");
        conn->setTitle(tr("UDP Access Configuration"));
        conn->setSubTitle(
                tr("This configures the interface to send and/or receive data with UDP datagrams."));
        setPage(Page_UDP, conn);
    }

    setPage(Page_LocalSocket, new IOInterfaceQtLocalSocketPage(this));
    setPage(Page_Command, new IOInterfaceCommandPage(this));

    multiplexerPage = new IOInterfaceMultiplexerPage(this);
    setPage(Page_Multiplexer, multiplexerPage);

    setPage(Page_URL, new IOInterfaceURLPage(this));

#ifdef Q_OS_UNIX
    setPage(Page_UnixSocket, new IOInterfaceUnixSocketPage(this));
    setPage(Page_UnixFIFO, new IOInterfaceUnixFIFOPage(this));
#endif

    listenSSLPage = new IOInterfaceSSLPage(this, "SSL");
    listenSSLPage->setTitle(tr("Server SSL"));
    setPage(Page_ListenSSL, listenSSLPage);

    listenSSLRestrictionPage = new IOInterfaceSSLRestrictionPage(this, "SSL");
    listenSSLRestrictionPage->setTitle(tr("External CPD3 SSL Access Restriction"));
    setPage(Page_ListenSSLRestrict, listenSSLRestrictionPage);

    remoteSSLPage = new IOInterfaceSSLPage(this);
    setPage(Page_RemoteSSL, remoteSSLPage);

    remoteAccessPage = new IOInterfaceRemoteAccessPage(this);
    setPage(Page_RemoteAccess, remoteAccessPage);

    remoteAccessExternalSSLPage = new IOInterfaceSSLPage(this, "External/SSL");
    remoteAccessExternalSSLPage->setTitle(tr("External CPD3 SSL"));
    setPage(Page_RemoteAccessExternalSSL, remoteAccessExternalSSLPage);

    remoteAccessExternalSSLRestrictionPage =
            new IOInterfaceSSLRestrictionPage(this, "External/SSL");
    remoteAccessExternalSSLRestrictionPage->setTitle(tr("External CPD3 SSL Access Restriction"));
    setPage(Page_RemoteAccessExternalSSLRestrict, remoteAccessExternalSSLRestrictionPage);

    setPage(Page_Final, new IOInterfaceFinalPage(this));
}

IOInterfaceSelect::~IOInterfaceSelect() = default;

int IOInterfaceSelect::nextId() const
{
    switch (currentId()) {
    case Page_TypeSelect:
        switch (typeSelectPage->getType()) {
        case IOInterfaceSelectTypePage::Existing:
            if (typeSelectPage->configureRemoteAccess())
                return Page_RemoteAccess;
            return Page_Final;
        case IOInterfaceSelectTypePage::LocalSerialPort:
            if (typeSelectPage->localSerialPortSelected().isEmpty())
                return Page_LocalSerialPortManual;
            if (typeSelectPage->localSerialPortAdvancedSelected())
                return Page_LocalSerialPortAdvanced;
            if (typeSelectPage->configureRemoteAccess())
                return Page_RemoteAccess;
            return Page_Final;
        case IOInterfaceSelectTypePage::RemoteServer:
            return Page_RemoteServer;
        case IOInterfaceSelectTypePage::RemoteCPD3:
            return Page_RemoteCPD3;
        case IOInterfaceSelectTypePage::LocalSocket:
            return Page_LocalSocket;
        case IOInterfaceSelectTypePage::Command:
            return Page_Command;
        case IOInterfaceSelectTypePage::TCPListen:
            return Page_TCPListen;
        case IOInterfaceSelectTypePage::LocalListen:
            return Page_LocalSocket;
        case IOInterfaceSelectTypePage::UDP:
            return Page_UDP;
        case IOInterfaceSelectTypePage::Multiplexer:
            return Page_Multiplexer;
        case IOInterfaceSelectTypePage::URL:
            return Page_URL;
#ifdef Q_OS_UNIX
        case IOInterfaceSelectTypePage::UnixSocket:
            return Page_UnixSocket;
        case IOInterfaceSelectTypePage::UnixListen:
            return Page_UnixSocket;
        case IOInterfaceSelectTypePage::UnixFIFO:
            return Page_UnixFIFO;
#endif
        }
        return Page_Final;

    case Page_LocalSerialPortManual:
        if (typeSelectPage->localSerialPortAdvancedSelected())
            return Page_LocalSerialPortAdvanced;
    case Page_LocalSerialPortAdvanced:
        if (typeSelectPage->configureRemoteAccess())
            return Page_RemoteAccess;
        return Page_Final;

    case Page_RemoteServer:
        if (field("remoteServerSSL").toBool())
            return Page_RemoteSSL;
        if (typeSelectPage->configureRemoteAccess())
            return Page_RemoteAccess;
        return Page_Final;

    case Page_RemoteCPD3:
        if (field("cpd3ServerSSL").toBool())
            return Page_RemoteSSL;
        if (typeSelectPage->configureRemoteAccess())
            return Page_RemoteAccess;
        return Page_Final;

    case Page_TCPListen:
        if (field("tcpListenSSL").toBool())
            return Page_ListenSSL;
        if (typeSelectPage->configureRemoteAccess())
            return Page_RemoteAccess;
        return Page_Final;

    case Page_LocalSocket:
    case Page_Command:
    case Page_UDP:
    case Page_Multiplexer:
#ifdef Q_OS_UNIX
    case Page_UnixFIFO:
    case Page_UnixSocket:
#endif
        if (typeSelectPage->configureRemoteAccess())
            return Page_RemoteAccess;
        return Page_Final;

    case Page_ListenSSL:
        return Page_ListenSSLRestrict;
    case Page_ListenSSLRestrict:
        if (typeSelectPage->configureRemoteAccess())
            return Page_RemoteAccess;
        return Page_Final;

    case Page_RemoteSSL:
        if (typeSelectPage->configureRemoteAccess())
            return Page_RemoteAccess;
        return Page_Final;

    case Page_RemoteAccess:
        if (remoteAccessPage->enableExternalSSL())
            return Page_RemoteAccessExternalSSL;
    case Page_RemoteAccessExternalSSL:
        return Page_RemoteAccessExternalSSLRestrict;
    case Page_RemoteAccessExternalSSLRestrict:
        return Page_Final;

    case Page_Final:
        return -1;

    default:
        break;
    }

    return -1;
}

void IOInterfaceSelect::updateRealtime(const std::unordered_map<QString,
                                                                Variant::Root> &realtimeInterfaces)
{
    typeSelectPage->setRealtime(realtimeInterfaces);
}

void IOInterfaceSelect::setInterface(const Variant::Read &i)
{
    interface = Variant::Root(i);
    typeSelectPage->selectInterface(interface, inspectAdvanced);
}

const Variant::Root &IOInterfaceSelect::getInterface() const
{ return interface; }

void IOInterfaceSelect::setInspectAdvanced(bool v)
{ inspectAdvanced = v; }

bool IOInterfaceSelect::getInspectAdvanced() const
{ return inspectAdvanced; }

Variant::Root IOInterfaceSelect::buildInterface()
{
    std::vector<Variant::Root> merge;
    switch (typeSelectPage->getType()) {
    case IOInterfaceSelectTypePage::Existing:
        merge.emplace_back(typeSelectPage->existingSelected());
        break;
    case IOInterfaceSelectTypePage::LocalSerialPort: {
        Variant::Root add;
        add["Type"].setString("SerialPort");

        QString local(typeSelectPage->localSerialPortSelected());
        if (!local.isEmpty()) {
            add["Port"].setString(local);
        } else {
            add["Port"].setString(field("serialPortName").toString());
        }
        merge.emplace_back(std::move(add));
        if (typeSelectPage->localSerialPortAdvancedSelected()) {
            add = serialPortAdvancedPage->overlayValue();
            if (add.read().exists())
                merge.emplace_back(std::move(add));
        }
        break;
    }
    case IOInterfaceSelectTypePage::RemoteServer: {
        Variant::Root add;
        add["Type"].setString("Server");
        add["Server"].setString(field("remoteServerName").toString());
        add["ServerPort"].setInt64(field("remoteServerPort").toInt());
        merge.emplace_back(std::move(add));

        if (field("remoteServerSSL").toBool())
            merge.emplace_back(remoteSSLPage->overlayValue());
        break;
    }
    case IOInterfaceSelectTypePage::RemoteCPD3: {
        Variant::Root add;
        add["Type"].setString("Server");
        add["Server"].setString(field("cpd3ServerName").toString());
        add["ServerPort"].setInt64(field("cpd3ServerPort").toInt());
        merge.emplace_back(std::move(add));

        if (field("cpd3ServerSSL").toBool())
            merge.emplace_back(remoteSSLPage->overlayValue());
        break;
    }
    case IOInterfaceSelectTypePage::LocalSocket: {
        Variant::Root add;
        add["Type"].setString("QtLocalSocket");
        add["Name"].setString(field("qtsocketname").toString());
        merge.emplace_back(std::move(add));
        break;
    }
    case IOInterfaceSelectTypePage::Command: {
        Variant::Root add;
        add["Type"].setString("Command");

        QString commandLine(field("command").toString());
        if (commandLine.contains(QRegExp("[\"\'|/<>&$\\(\\)`~;\\{\\}\\[\\]\\*\\?#%=+-]")))
#if defined(Q_OS_UNIX)
        {
            add["Command"].setString("/bin/sh");
            add["Arguments/#0"].setString("-c");
            add["Arguments/#1"].setString(commandLine);
        } else
#elif defined(Q_OS_WIN32)
            {
                add["Command"].setString("cmd");
                add["Arguments/#0"].setString("/c");
                add["Arguments/#1"].setString(commandLine);
            } else
#else
            { }
#endif
        {
            QStringList arguments(commandLine.split(QRegExp("\\s+")));
            add["Command"].setString(arguments.takeFirst());
            for (const auto &arg : arguments) {
                add["Arguments"].toArray().after_back().setString(arg);
            }
        }

        if (field("commandErrorStreamAsEcho").isValid())
            add["ErrorStreamAsEcho"].setBoolean(field("commandErrorStreamAsEcho").toBool());

        merge.emplace_back(std::move(add));
        break;
    }
    case IOInterfaceSelectTypePage::TCPListen: {
        Variant::Root add;
        add["Type"].setString("TCPListen");
        if (!field("tcpListenAddress").toString().isEmpty())
            add["ListenAddress"].setString(field("tcpListenAddress").toString());
        else
            add["ListenAddress"].setEmpty();
        add["LocalPort"].setInt64(field("tcpListenPort").toInt());
        merge.emplace_back(std::move(add));

        if (field("tcpListenSSL").toBool()) {
            add = listenSSLPage->overlayValue();
            if (add.read().exists())
                merge.emplace_back(std::move(add));
            add = listenSSLRestrictionPage->overlayValue();
            if (add.read().exists())
                merge.emplace_back(std::move(add));
        }
        break;
    }
    case IOInterfaceSelectTypePage::LocalListen: {
        Variant::Root add;
        add["Type"].setString("QtLocalListen");
        add["Name"].setString(field("qtsocketname").toString());
        merge.emplace_back(std::move(add));
        break;
    }
    case IOInterfaceSelectTypePage::UDP: {
        Variant::Root add;
        add["Type"].setString("UDP");
        if (!field("udpListenAddress").toString().isEmpty())
            add["ListenAddress"].setString(field("udpListenAddress").toString());
        if (field("udpListenPort").toInt() > 0)
            add["LocalPort"].setInt64(field("udpListenPort").toInt());
        if (!field("udpServerAddress").toString().isEmpty()) {
            add["ServerAddress"].setString(field("udpServerAddress").toString());
            add["ServerPort"].setInt64(field("udpServerPort").toInt());
        }
        if (field("udpServerFragment").toInt() > 0)
            add["FragmentSize"].setInt64(field("udpServerFragment").toInt());
        if (field("udpDisableFraming").isValid())
            add["DisableFraming"].setBoolean(field("udpDisableFraming").toBool());
        merge.emplace_back(std::move(add));
        break;
    }
    case IOInterfaceSelectTypePage::URL: {
        Variant::Root add;
        add["Type"].setString("URL");
        add["URL"].setString(field("urlTarget").toString());
        if (field("urlMethod").isValid())
            add["URL"].setString(field("urlMethod").toString());
        merge.emplace_back(std::move(add));
        break;
    }
    case IOInterfaceSelectTypePage::Multiplexer: {
        merge.emplace_back(multiplexerPage->overlayValue());
        break;
    }
#ifdef Q_OS_UNIX
    case IOInterfaceSelectTypePage::UnixSocket: {
        Variant::Root add;
        add["Type"].setString("LocalSocket");
        add["File"].setString(field("unixSocketName").toString());
        merge.emplace_back(std::move(add));
        break;
    }
    case IOInterfaceSelectTypePage::UnixListen: {
        Variant::Root add;
        add["Type"].setString("LocalListen");
        add["File"].setString(field("unixSocketName").toString());
        merge.emplace_back(std::move(add));
        break;
    }
    case IOInterfaceSelectTypePage::UnixFIFO: {
        Variant::Root add;
        add["Type"].setString("Pipe");
        add["Input"].setString(field("unixFIFOInput").toString());
        add["Output"].setString(field("unixFIFOOutput").toString());
        add["Echo"].setString(field("unixFIFOEcho").toString());
        if (field("unixFIFOAllowMissing").isValid())
            add["AllowMissing"].setBoolean(field("unixFIFOAllowMissing").toBool());
        merge.emplace_back(std::move(add));
        break;
    }
#endif
    }

    if (typeSelectPage->configureRemoteAccess()) {
        Variant::Root add = remoteAccessPage->overlayValue();
        if (add.read().exists()) {
            merge.emplace_back(std::move(add));
            if (remoteAccessPage->enableExternalSSL()) {
                add = remoteAccessExternalSSLPage->overlayValue();
                if (add.read().exists())
                    merge.emplace_back(std::move(add));
                add = remoteAccessExternalSSLRestrictionPage->overlayValue();
                if (add.read().exists())
                    merge.emplace_back(std::move(add));
            }
        }
    }

    return Variant::Root::overlay(merge.begin(), merge.end());
}

namespace Internal {

IOInterfaceSelectTypePage::IOInterfaceSelectTypePage(QWidget *parent) : QWizardPage(parent)
{

    setTitle(tr("Select Interface Type"));
    setSubTitle(
            tr("Once you have selected a communications type, you will be prompted for the detailed settings about it."));

    QGridLayout *layout = new QGridLayout(this);
    setLayout(layout);

    typeSelectionGroup = new QButtonGroup(this);

    QHBoxLayout *lineLayout;

    lineLayout = new QHBoxLayout();
    layout->addLayout(lineLayout, 0, 0, 1, -1);

    existingButton = new QRadioButton(tr("Pr&eexisting interface:"), this);
    existingButton->setToolTip(tr("Use a preexisting interface to the acquisition system."));
    existingButton->setStatusTip(tr("Use a preexisting acquisition interface"));
    existingButton->setWhatsThis(
            tr("This selects an already active interface used by the local acquisition system."));
    existingButton->setSizePolicy(
            QSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum, QSizePolicy::RadioButton));
    lineLayout->addWidget(existingButton, 0, Qt::AlignLeft);
    typeSelectionGroup->addButton(existingButton, Existing);

    existingSelectList = new QComboBox(this);
    existingSelectList->setToolTip(tr("Select the interface to use."));
    existingSelectList->setStatusTip(tr("Acquisition interface selection"));
    existingSelectList->setWhatsThis(
            tr("Use this to select the preexisting acquisition interface to use."));
    existingSelectList->setEnabled(false);
    existingSelectList->setSizePolicy(
            QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum, QSizePolicy::ComboBox));
    lineLayout->addWidget(existingSelectList, 1);
    connect(existingButton, SIGNAL(toggled(bool)), this, SLOT(existingStateChanged()));

    lineLayout = new QHBoxLayout();
    layout->addLayout(lineLayout, 1, 0, 1, -1);

    localSerialPortButton = new QRadioButton(tr("&Local serial port:"), this);
    localSerialPortButton->setToolTip(tr("Use a serial port attached to this system."));
    localSerialPortButton->setStatusTip(tr("Use a local serial port"));
    localSerialPortButton->setWhatsThis(
            tr("This selects a serial port on the local system.  This includes serial ports provided by external converters (e.x. USB->RS232 boxes)."));
    localSerialPortButton->setSizePolicy(
            QSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum, QSizePolicy::RadioButton));
    lineLayout->addWidget(localSerialPortButton, 0, Qt::AlignLeft);
    typeSelectionGroup->addButton(localSerialPortButton, LocalSerialPort);

    localSerialPortSelectList = new QComboBox(this);
    localSerialPortSelectList->setToolTip(tr("Select local serial port."));
    localSerialPortSelectList->setStatusTip(tr("Local serial port selection"));
    localSerialPortSelectList->setWhatsThis(
            tr("Select the local serial port to use.  If an acquisition system is available, then the currently connected system's name is also displayed."));
    localSerialPortSelectList->setSizePolicy(
            QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum, QSizePolicy::ComboBox));
    lineLayout->addWidget(localSerialPortSelectList, 1);

    localSerialPortAdvanced = new QCheckBox(tr("&Advanced", "serial port advanced"), this);
    localSerialPortAdvanced->setToolTip(
            tr("Select this to configure advanced serial port settings (e.x. baud rate)."));
    localSerialPortAdvanced->setStatusTip(tr("Configure advanced serial port settings"));
    localSerialPortAdvanced->setWhatsThis(
            tr("When selected this enabled additional configuration prompts about advanced serial port settings.  Use this option to manually specify baud rates, flow control, etc.  This is not normally required because the acquisition systems themselves can configure it to their supported parameters."));
    localSerialPortAdvanced->setChecked(false);
    layout->addWidget(localSerialPortAdvanced, 2, 1, 1, 1, Qt::AlignLeft);
    connect(localSerialPortButton, SIGNAL(toggled(bool)), this,
            SLOT(localSerialPortStateChanged()));

    QRadioButton *radioButton;

    radioButton = new QRadioButton(tr("&Remote TCP/IP connection"), this);
    radioButton->setToolTip(tr("Connect to a remote host over TCP/IP."));
    radioButton->setStatusTip(tr("Connect to a remote server"));
    radioButton->setWhatsThis(
            tr("This mode allows for connection to a remote host over TCP/IP and bridge the data stream directly to that connection.  This is allows for connection to most network to RS232 converters to be accessed when they are operated in \"transparent\" mode."));
    layout->addWidget(radioButton, 3, 0, 1, -1, Qt::AlignLeft);
    typeSelectionGroup->addButton(radioButton, RemoteServer);

    radioButton = new QRadioButton(tr("Remote C&PD3 connection (advanced)"), this);
    radioButton->setToolTip(tr("Connect to a remote CPD3 I/O server."));
    radioButton->setStatusTip(tr("Connect to a CPD3 instance"));
    radioButton->setWhatsThis(
            tr("This mode instructs a local interface to connect to a remote instance of a CPD3 I/O handler.  When used like this the remote I/O handler is usually started outside of a conventional acquisition system.  This allows for the local system to communicate with interfaces physically connected to a different system and tunneled over the network."));
    layout->addWidget(radioButton, 4, 0, 1, -1, Qt::AlignLeft);
    typeSelectionGroup->addButton(radioButton, RemoteCPD3);

    radioButton = new QRadioButton(tr("&TCP/IP Server (advanced)"), this);
    radioButton->setToolTip(tr("Accept connections on a local TCP/IP server."));
    radioButton->setStatusTip(tr("Accept connections"));
    radioButton->setWhatsThis(
            tr("This mode waits for connections on a TCP/IP server port.  This allows the interface to act as a receveiver for another host connecting to it and generating data."));
    layout->addWidget(radioButton, 5, 0, 1, -1, Qt::AlignLeft);
    typeSelectionGroup->addButton(radioButton, TCPListen);

    radioButton = new QRadioButton(tr("&UDP (advanced)"), this);
    radioButton->setToolTip(tr("Transfer data with connectionless UDP packets."));
    radioButton->setStatusTip(tr("UDP transfer"));
    radioButton->setWhatsThis(
            tr("This mode allows the interface to send and/or receveive data with unreliable connectionless UDP packets.  This allows it to transfer or receive data \"blindly\" from other hosts."));
    layout->addWidget(radioButton, 6, 0, 1, -1, Qt::AlignLeft);
    typeSelectionGroup->addButton(radioButton, UDP);

    radioButton = new QRadioButton(tr("Local &socket (advanced)"), this);
    radioButton->setToolTip(tr("Connect to a local system socket."));
    radioButton->setStatusTip(tr("Connect to a local system socket"));
    radioButton->setWhatsThis(
            tr("This selects a system local socket for communication.  The common use of this mode is to allow the interface to communicate with some other process on the system."));
    layout->addWidget(radioButton, 7, 0, 1, -1, Qt::AlignLeft);
    typeSelectionGroup->addButton(radioButton, LocalSocket);

    radioButton = new QRadioButton(tr("Local ser&ver (advanced)"), this);
    radioButton->setToolTip(tr("Wait for local system socket connections."));
    radioButton->setStatusTip(tr("Listen on a local system socket"));
    radioButton->setWhatsThis(
            tr("This selects a system socket and waits for a connection to it.  The common use of this mode is to allow the interface to communicate with some other process on the system."));
    layout->addWidget(radioButton, 8, 0, 1, -1, Qt::AlignLeft);
    typeSelectionGroup->addButton(radioButton, LocalListen);

    radioButton = new QRadioButton(tr("&Command (advanced)"), this);
    radioButton->setToolTip(tr("Interface with a locally executed command."));
    radioButton->setStatusTip(tr("Execute a local command"));
    radioButton->setWhatsThis(
            tr("This mode allows for the interface to communicate directly with a command launched by the system.  Communication occurs to the command's standard input and output channels."));
    layout->addWidget(radioButton, 9, 0, 1, -1, Qt::AlignLeft);
    typeSelectionGroup->addButton(radioButton, Command);

#ifdef Q_OS_UNIX

    radioButton = new QRadioButton(tr("U&nix socket (advanced)"), this);
    radioButton->setToolTip(tr("Connect to a local Unix socket."));
    radioButton->setStatusTip(tr("Connect to a local Unix socket"));
    radioButton->setWhatsThis(
            tr("This selects a system Unix socket for communication.  The common use of this mode is to allow the interface to communicate with some other process on the system."));
    layout->addWidget(radioButton, 10, 0, 1, -1, Qt::AlignLeft);
    typeSelectionGroup->addButton(radioButton, UnixSocket);

    radioButton = new QRadioButton(tr("Un&ix server (advanced)"), this);
    radioButton->setToolTip(tr("Create a local Unix socket."));
    radioButton->setStatusTip(tr("Listen on a Unix socket"));
    radioButton->setWhatsThis(
            tr("This selects a system Unix socket and waits for a connection to it.  The common use of this mode is to allow the interface to communicate with some other process on the system."));
    layout->addWidget(radioButton, 11, 0, 1, -1, Qt::AlignLeft);
    typeSelectionGroup->addButton(radioButton, UnixListen);

    radioButton = new QRadioButton(tr("Unix &FIFOs (advanced)"), this);
    radioButton->setToolTip(tr("Used one or more FIFOs (named pipes) for communication."));
    radioButton->setStatusTip(tr("Use FIFOs for communication"));
    radioButton->setWhatsThis(
            tr("This mode allows for communication over one or more Unix FIFOs (named pipes).  Separate FIFOs can be defined for the input, output, and echo channels."));
    layout->addWidget(radioButton, 12, 0, 1, -1, Qt::AlignLeft);
    typeSelectionGroup->addButton(radioButton, UnixFIFO);

#endif

    radioButton = new QRadioButton(tr("UR&L (advanced)"), this);
    radioButton->setToolTip(tr("Send and receive data via URL requests."));
    radioButton->setStatusTip(tr("URL request"));
    radioButton->setWhatsThis(
            tr("This mode uses a selectable method on a given URL.  This allows data to be sent via POST or PUT requests or purely received by GET requests."));
    layout->addWidget(radioButton, 13, 0, 1, -1, Qt::AlignLeft);
    typeSelectionGroup->addButton(radioButton, URL);

    radioButton = new QRadioButton(tr("&Multiplexer (advanced)"), this);
    radioButton->setToolTip(tr("Multiplex multiple sub-interfaces."));
    radioButton->setStatusTip(tr("Multiplex other interface"));
    radioButton->setWhatsThis(
            tr("This mode converts the interface into a multiplexing network for other sub-interfaces.  This allows for creating various configurations of input and output channels between the other interface types."));
    layout->addWidget(radioButton, 14, 0, 1, -1, Qt::AlignLeft);
    typeSelectionGroup->addButton(radioButton, Multiplexer);

    QFrame *lineFrame = new QFrame(this);
    lineFrame->setFrameStyle(QFrame::HLine | QFrame::Sunken);
    layout->addWidget(lineFrame, 15, 0, 1, -1);

    remoteAccess = new QCheckBox(tr("Re&mote access (advanced)"), this);
    remoteAccess->setToolTip(tr("Configure remote access to the I/O interface."));
    remoteAccess->setStatusTip(tr("Set interface remote access parameters"));
    remoteAccess->setWhatsThis(
            tr("This enables configuration of remote access to the backend I/O interface.  This is used to allow another instance to use the \"Remote CPD3 connection\" above to access a local interface."));
    remoteAccess->setChecked(false);
    layout->addWidget(remoteAccess, 16, 0, 1, -1, Qt::AlignLeft);

    layout->setColumnMinimumWidth(0, 10);

    existingButton->hide();
    existingSelectList->hide();
    configureLocalSerialPorts({});

    localSerialPortButton->click();
}

void IOInterfaceSelectTypePage::existingStateChanged()
{
    existingSelectList->setEnabled(existingButton->isChecked());
}

void IOInterfaceSelectTypePage::localSerialPortStateChanged()
{
    localSerialPortSelectList->setEnabled(localSerialPortButton->isChecked());
    localSerialPortAdvanced->setEnabled(localSerialPortButton->isChecked());
}

namespace {
struct IOInterfaceSelectionSortItem {
    QString name;
    bool acquisitionClaimed;
    QChar menuCharacter;
    std::unique_ptr<IOTarget> target;
    bool selected;

    bool operator<(const IOInterfaceSelectionSortItem &other) const
    {
        if (acquisitionClaimed) {
            if (!other.acquisitionClaimed)
                return true;
        } else if (other.acquisitionClaimed) {
            return false;
        }
        if (menuCharacter != 0 && other.menuCharacter != 0 && menuCharacter != other.menuCharacter)
            return menuCharacter < other.menuCharacter;
        return name < other.name;
    }
};
}

void IOInterfaceSelectTypePage::configureLocalSerialPorts(const std::vector<
        Variant::Read> &existing)
{
    std::vector<IOInterfaceSelectionSortItem> interfaces;

    QString selection(localSerialPortSelected());
    for (auto &add : IOTarget::enumerateLocal()) {
        IOInterfaceSelectionSortItem entry;

        auto config = add->configuration(true);
        entry.target = std::move(add);
        entry.name = config.read().hash("Port").toQString();
        if (entry.name.isEmpty())
            continue;

        entry.acquisitionClaimed = false;
        entry.selected = (config.read().hash("Port").toQString() == selection);

        interfaces.emplace_back(std::move(entry));
    }

    for (const auto &check : existing) {
        auto iExisting = IOTarget::create(check.hash("Interface"));
        if (!iExisting)
            continue;

        auto hit = interfaces.end();
        for (auto e = interfaces.begin(), endE = interfaces.end(); e != endE; ++e) {
            if (!e->target->matchesDevice(*iExisting))
                continue;
            hit = e;
            break;
        }
        if (hit == interfaces.end()) {
            if (!iExisting->isValid())
                continue;
            IOInterfaceSelectionSortItem entry;

            entry.name = check.hash("Interface").hash("Port").toQString();
            if (entry.name.isEmpty())
                continue;

            entry.target = std::move(iExisting);
            entry.acquisitionClaimed = true;
            entry.selected = (check.hash("Interface").hash("Port").toQString() == selection);

            interfaces.emplace_back(std::move(entry));
            hit = interfaces.end();
            --hit;
        } else {
            hit->acquisitionClaimed = true;
        }


        hit->name = tr("%1 : %2", "claimed interface format").arg(hit->name, check.hash("MenuEntry")
                                                                                  .toDisplayString());

        QString checkMC(check.hash("MenuCharacter").toDisplayString());
        if (!checkMC.isEmpty())
            hit->menuCharacter = checkMC.at(0).toUpper();
    }

    std::stable_sort(interfaces.begin(), interfaces.end());

    localSerialPortSelectList->clear();

    int selectIndex = 0;
    for (const auto &add : interfaces) {
        existingSelectList->setCurrentIndex(selectIndex);
        if (add.selected)
            selectIndex = localSerialPortSelectList->count();
        auto config = add.target->configuration(true);
        localSerialPortSelectList->addItem(add.name, config.read().hash("Port").toQString());
    }

    localSerialPortSelectList->addItem(tr("Other", "manual port entry"), QVariant(QString()));
    localSerialPortSelectList->setCurrentIndex(selectIndex);
}

IOInterfaceSelectTypePage::Type IOInterfaceSelectTypePage::getType() const
{
    int id = typeSelectionGroup->checkedId();
    if (id <= 0)
        return LocalSerialPort;
    return (Type) id;
}

bool IOInterfaceSelectTypePage::configureRemoteAccess() const
{ return remoteAccess->isChecked(); }

Variant::Read IOInterfaceSelectTypePage::existingSelected() const
{
    int sel = existingSelectList->currentIndex();
    if (sel == -1)
        return Variant::Read::empty();
    return existingSelectList->itemData(sel).value<Variant::Read>();
}

QString IOInterfaceSelectTypePage::localSerialPortSelected() const
{
    int sel = localSerialPortSelectList->currentIndex();
    if (sel == -1)
        return QString();
    return localSerialPortSelectList->itemData(sel).toString();
}

bool IOInterfaceSelectTypePage::localSerialPortAdvancedSelected() const
{ return localSerialPortAdvanced->isChecked(); }

void IOInterfaceSelectTypePage::setRealtime(const std::unordered_map<QString,
                                                                     Variant::Root> &realtimeInterfaces)
{
    std::vector<Variant::Read> existing;
    for (const auto &add : realtimeInterfaces) {
        existing.emplace_back(add.second.read());
    }
    configureLocalSerialPorts(existing);

    std::unique_ptr<IOTarget> existingSelected;
    {
        int sel = existingSelectList->currentIndex();
        if (sel != -1) {
            existingSelected =
                    IOTarget::create(existingSelectList->itemData(sel).value<Variant::Read>());
        }
    }

    std::vector<IOInterfaceSelectionSortItem> interfaces;
    for (const auto &add : existing) {
        auto interface = add.hash("Interface");
        if (!interface.exists())
            continue;

        const auto &type = interface.hash("Type").toString();
        QString description;
        if (Util::equal_insensitive(type, "SerialPort")) {
            description = tr("%1", "serial port format").arg(interface.hash("Port").toQString());
        } else if (Util::equal_insensitive(type, "RemoteServer")) {
            description = tr("Server: %2 Port %1", "remote server format").arg(
                    interface.hash("ServerPort").toInt64()).arg(interface.hash("Port").toQString());
        } else if (Util::equal_insensitive(type, "QtLocalSocket")) {
            description = tr("Local Socket: %1", "local socket format").arg(
                    interface.hash("Name").toQString());
        } else if (Util::equal_insensitive(type, "Command")) {
            QStringList arguments;
            for (auto ae : interface.hash("Arguments").toArray()) {
                arguments.append(ae.toQString());
            }
            description = tr("Command: %1 %2", "command format").arg(
                    interface.hash("Command").toQString(), arguments.join(" "));
        } else if (Util::equal_insensitive(type, "LocalSocket")) {
            description = tr("Unix Socket: %1", "unix socket format").arg(
                    interface.hash("File").toQString());
        } else if (Util::equal_insensitive(type, "Pipe")) {
            description = tr("Unix FIFO(s): %1:%2:%3", "unix fifo format").arg(
                    interface.hash("Input").toQString(), interface.hash("Output").toQString(),
                    interface.hash("Echo").toQString());
        } else {
            continue;
        }

        auto ioIF = IOTarget::create(interface);
        if (!ioIF || !ioIF->isValid())
            continue;

        IOInterfaceSelectionSortItem entry;

        entry.selected = existingSelected && existingSelected->matchesDevice(*ioIF);
        entry.name = tr("%1 : %2", "existing list format").arg(description, add.hash("MenuEntry")
                                                                               .toDisplayString()),
                entry.acquisitionClaimed = true;

        QString checkMC(add.hash("MenuCharacter").toDisplayString());
        if (!checkMC.isEmpty())
            entry.menuCharacter = checkMC.at(0).toUpper();
        entry.target = std::move(ioIF);
        interfaces.emplace_back(std::move(entry));
    }

    existingSelectList->clear();
    if (interfaces.empty()) {
        if (getType() == Existing)
            localSerialPortButton->click();
        existingButton->hide();
        existingSelectList->hide();
        return;
    }

    std::stable_sort(interfaces.begin(), interfaces.end());

    int selectIndex = 0;
    for (const auto &add : interfaces) {
        if (add.selected)
            selectIndex = existingSelectList->count();
        existingSelectList->addItem(add.name, QVariant::fromValue<Variant::Read>(
                add.target->configuration(true)));
    }
    existingSelectList->setCurrentIndex(selectIndex);

    existingButton->show();
    existingSelectList->show();
}

void IOInterfaceSelectTypePage::selectInterface(const Variant::Read &interface, bool checkAdvanced)
{
    remoteAccess->setChecked(
            interface.hash("External").exists() || interface.hash("DisableLocal").toBool());

    if (existingSelectList->count() > 0) {
        auto check = IOTarget::create(interface);
        if (check) {
            for (int i = 0, max = existingSelectList->count(); i < max; i++) {
                auto ioIF =
                        IOTarget::create(existingSelectList->itemData(i).value<Variant::Read>());
                if (!ioIF || !ioIF->isValid())
                    continue;

                if (ioIF->matchesDevice(*check)) {
                    existingSelectList->setCurrentIndex(i);
                    existingButton->click();
                    return;
                }
            }
        }
    }

    const auto &type = interface.hash("Type").toString();
    if (Util::equal_insensitive(type, "SerialPort")) {
        typeSelectionGroup->button(LocalSerialPort)->click();

        QString check(interface.hash("Port").toQString());
        for (int i = 0, max = localSerialPortSelectList->count(); i < max; i++) {
            QString cmp(localSerialPortSelectList->itemData(i).toString());
            if (!cmp.isEmpty() && cmp != check)
                continue;

            localSerialPortSelectList->setCurrentIndex(i);
            break;
        }

        if (checkAdvanced) {
            localSerialPortAdvanced->setChecked(interface.hash("Baud").exists() ||
                                                        interface.hash("Parity").exists() ||
                                                        interface.hash("DataBits").exists() ||
                                                        interface.hash("StopBits").exists() ||
                                                        interface.hash("HardwareFlowControl")
                                                                 .exists() ||
                                                        interface.hash("SoftwareFlowControl")
                                                                 .exists() ||
                                                        interface.hash("SoftwareFlowControlResume")
                                                                 .exists() ||
                                                        interface.hash("NoInitialBreak").exists() ||
                                                        interface.hash("PulseRTS").exists() ||
                                                        interface.hash("PulseDTR").exists());
        }
    } else if (Util::equal_insensitive(type, "RemoteServer")) {
        typeSelectionGroup->button(RemoteServer)->click();
    } else if (Util::equal_insensitive(type, "CPD3Server")) {
        typeSelectionGroup->button(RemoteCPD3)->click();
    } else if (Util::equal_insensitive(type, "QtLocalSocket")) {
        typeSelectionGroup->button(LocalSocket)->click();
    } else if (Util::equal_insensitive(type, "Command")) {
        typeSelectionGroup->button(Command)->click();
    } else if (Util::equal_insensitive(type, "TCPListen")) {
        typeSelectionGroup->button(TCPListen)->click();
    } else if (Util::equal_insensitive(type, "UDP")) {
        typeSelectionGroup->button(UDP)->click();
    } else if (Util::equal_insensitive(type, "Multiplexer")) {
        typeSelectionGroup->button(Multiplexer)->click();
    } else if (Util::equal_insensitive(type, "URL")) {
        typeSelectionGroup->button(URL)->click();
    }
#ifdef Q_OS_UNIX
    else if (Util::equal_insensitive(type, "LocalSocket")) {
        typeSelectionGroup->button(UnixSocket)->click();
    } else if (Util::equal_insensitive(type, "LocalListen")) {
        typeSelectionGroup->button(UnixListen)->click();
    } else if (Util::equal_insensitive(type, "Pipe")) {
        typeSelectionGroup->button(UnixFIFO)->click();
    }
#endif
}


IOInterfaceSerialPortManualPage::IOInterfaceSerialPortManualPage(IOInterfaceSelect *p)
        : QWizardPage(p), parent(p)
{
    setTitle(tr("Select Serial Port"));
    setSubTitle(tr("Choose a serial port that was not automatically detected on the system."));

    QGridLayout *layout = new QGridLayout(this);
    setLayout(layout);

    QLabel *label = new QLabel(tr("&Port:"), this);
    layout->addWidget(label, 0, 0, 1, 1);
    portEntry = new QLineEdit(this);
#if defined(Q_OS_MAC)
    portEntry->setToolTip(tr("Enter the port to use.  For example: /dev/tty.usbserial"));
    portEntry->setStatusTip(tr("Enter serial port"));
    portEntry->setWhatsThis(tr("Enter the full path name of the serial port to use.  For example: \"/dev/tty.usbserial\" (without quotes) for a USB to serial converter."));
#elif defined(Q_OS_UNIX)
    portEntry->setToolTip(tr("Enter the port to use.  For example: /dev/ttyUSB0"));
    portEntry->setStatusTip(tr("Enter serial port"));
    portEntry->setWhatsThis(
            tr("Enter the full path name of the serial port to use.  For example: \"/dev/ttyUSB0\" (without quotes) for the first USB to serial port converter under Linux."));
#elif defined(Q_OS_WIN32)
    portEntry->setToolTip(tr("Enter the port to use.  For example: COM1"));
    portEntry->setStatusTip(tr("Enter serial port name"));
    portEntry->setWhatsThis(tr("Enter the name of the serial port to use.  For example: \"COM1\" (without quotes) for the first serial port on the system."));
#else
    portEntry->setToolTip(tr("Enter the port to use."));
    portEntry->setStatusTip(tr("Enter serial port"));
    portEntry->setWhatsThis(tr("Enter the name of the serial port to use."));
#endif
    label->setBuddy(portEntry);
    layout->addWidget(portEntry, 0, 1, 1, 1);

    registerField("serialPortName", portEntry);


#if defined(Q_OS_UNIX)
    QPushButton *manualSelect = new QPushButton(tr("&Select File"), this);
    connect(manualSelect, SIGNAL(clicked()), this, SLOT(showFileSelectDialog())), layout->addWidget(
            manualSelect, 0, 2, 1, 1);
#endif

    layout->setColumnStretch(1, 1);

    connect(portEntry, SIGNAL(textChanged(
                                      const QString &)), this, SIGNAL(completeChanged()));
}

IOInterfaceSerialPortManualPage::~IOInterfaceSerialPortManualPage() = default;

void IOInterfaceSerialPortManualPage::initializePage()
{
    if (portEntry->text().isEmpty()) {
        portEntry->setText(parent->getInterface().read().hash("Port").toQString());
    }
}

void IOInterfaceSerialPortManualPage::cleanupPage()
{ portEntry->setText(QString()); }

bool IOInterfaceSerialPortManualPage::isComplete() const
{ return !portEntry->text().isEmpty(); }

void IOInterfaceSerialPortManualPage::showFileSelectDialog()
{
    QString result(QFileDialog::getOpenFileName(this, tr("Open Serial Port"), portEntry->text()));
    if (result.isEmpty())
        return;
    portEntry->setText(result);
}


static const int baudRates[] =
        {50, 75, 110, 134, 150, 200, 300, 600, 1200, 1800, 2400, 4800, 9600, 19200, 38400, 57600,
         115200, 230400};

IOInterfaceSerialPortAdvancedPage::IOInterfaceSerialPortAdvancedPage(IOInterfaceSelect *p)
        : QWizardPage(p), parent(p)
{
    setTitle(tr("Set Serial Port Parameters"));
    setSubTitle(
            tr("Set the detailed serial port parameters.  These are not normally required for acquisition."));

    QGridLayout *layout = new QGridLayout(this);
    setLayout(layout);

    QLabel *label = new QLabel(tr("&Baud:"), this);
    layout->addWidget(label, 0, 0, 1, 1);
    baudSelect = new QComboBox(this);
    baudSelect->setToolTip(tr("Select the serial port baud rate."));
    baudSelect->setStatusTip(tr("Serial port baud rate"));
    baudSelect->setWhatsThis(
            tr("Set the serial port baud rate.  This is the number of symbols per second the port transmits."));
    baudSelect->setSizePolicy(
            QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum, QSizePolicy::ComboBox));
    layout->addWidget(baudSelect, 0, 1, 1, -1);
    label->setBuddy(baudSelect);
    baudSelect->addItem(tr("Default", "baud rate default"), QVariant());
    baudSelect->setCurrentIndex(0);
    for (int i = 0, max = sizeof(baudRates) / sizeof(baudRates[0]); i < max; i++) {
        baudSelect->addItem(tr("%1", "baud rate format").arg(baudRates[i]), QVariant(baudRates[i]));
    }
#ifdef Q_OS_WIN32
    baudSelect->setEditable(true);
    baudSelect->setInsertPolicy(QComboBox::NoInsert);
    baudSelect->setValidator(new QIntValidator(1, INT_MAX, baudSelect));
#endif

    label = new QLabel(tr("&Parity:"), this);
    layout->addWidget(label, 1, 0, 1, 1);
    paritySelect = new QComboBox(this);
    paritySelect->setToolTip(tr("Select the serial port parity used."));
    paritySelect->setStatusTip(tr("Serial port parity"));
    paritySelect->setWhatsThis(
            tr("Set the parity used on the serial port.  This is an optional part of the data frame used to verify the integrity of that frame."));
    paritySelect->setSizePolicy(
            QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum, QSizePolicy::ComboBox));
    layout->addWidget(paritySelect, 1, 1, 1, -1);
    label->setBuddy(paritySelect);
    paritySelect->addItem(tr("Default", "parity default"), QVariant());
    paritySelect->setCurrentIndex(0);
    paritySelect->addItem(tr("None", "parity"), QVariant("None"));
    paritySelect->addItem(tr("Even", "parity"), QVariant("Even"));
    paritySelect->addItem(tr("Odd", "parity"), QVariant("Odd"));
#ifdef Q_OS_WIN32
    paritySelect->addItem(tr("Space", "parity"), QVariant("Space"));
    paritySelect->addItem(tr("Mark", "parity"), QVariant("Mark"));
#endif
    connect(paritySelect, SIGNAL(activated(int)), this, SLOT(paritySet()));

    label = new QLabel(tr("&Data bits:"), this);
    layout->addWidget(label, 2, 0, 1, 1);
    dataBitsSelect = new QComboBox(this);
    dataBitsSelect->setToolTip(tr("Select the number of data bits in a frame."));
    dataBitsSelect->setStatusTip(tr("Set the number data bits"));
    dataBitsSelect->setWhatsThis(
            tr("Set the number of data bits in a single frame.  This defines the range of data that can be represented.  In generally this is either 7 or 8 to represent most of ASCII."));
    dataBitsSelect->setSizePolicy(
            QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum, QSizePolicy::ComboBox));
    layout->addWidget(dataBitsSelect, 2, 1, 1, -1);
    label->setBuddy(dataBitsSelect);
    dataBitsSelect->addItem(tr("Default", "data bits default"), QVariant());
    dataBitsSelect->setCurrentIndex(0);
    dataBitsSelect->addItem(tr("5", "data bits"), QVariant(5));
    dataBitsSelect->addItem(tr("6", "data bits"), QVariant(6));
    dataBitsSelect->addItem(tr("7", "data bits"), QVariant(7));
    dataBitsSelect->addItem(tr("8", "data bits"), QVariant(8));
    userSetDataBits = false;
    connect(dataBitsSelect, SIGNAL(activated(int)), this, SLOT(dataBitsSet()));

    label = new QLabel(tr("&Stop bits:"), this);
    layout->addWidget(label, 3, 0, 1, 1);
    stopBitsSelect = new QComboBox(this);
    stopBitsSelect->setToolTip(tr("Select the number of stop bits at the end of a frame."));
    stopBitsSelect->setStatusTip(tr("Set the number stop bits"));
    stopBitsSelect->setWhatsThis(
            tr("Set the number of stop bits used to end a frame.  This defines the amount of time the line is required to be idle at the end of a frame."));
    stopBitsSelect->setSizePolicy(
            QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum, QSizePolicy::ComboBox));
    layout->addWidget(stopBitsSelect, 3, 1, 1, -1);
    label->setBuddy(stopBitsSelect);
    stopBitsSelect->addItem(tr("Default", "stop bits default"), QVariant());
    stopBitsSelect->setCurrentIndex(0);
    stopBitsSelect->addItem(tr("1", "stop bits"), QVariant(1));
    stopBitsSelect->addItem(tr("2", "stop bits"), QVariant(2));
#ifdef Q_OS_WIN32
    stopBitsSelect->addItem(tr("1.5", "stop bits"), QVariant(3));
#endif

    label = new QLabel(tr("&Hardware flow control:"), this);
    layout->addWidget(label, 4, 0, 1, 1);
    hardwareFlowControl = new QComboBox(this);
    hardwareFlowControl->setToolTip(tr("Select the type of hardware flow control."));
    hardwareFlowControl->setStatusTip(tr("Set hardware flow control"));
    hardwareFlowControl->setWhatsThis(
            tr("Set the hardware flow control mode in use.  This controls the manipulation and usage of the RTS and CTS lines."));
    hardwareFlowControl->setSizePolicy(
            QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum, QSizePolicy::ComboBox));
    layout->addWidget(hardwareFlowControl, 5, 1, 1, 1);
    label->setBuddy(hardwareFlowControl);
    hardwareFlowControl->addItem(tr("Default", "hardware flow control default"), QVariant());
    hardwareFlowControl->setCurrentIndex(0);
    hardwareFlowControl->addItem(tr("Disabled", "hardware flow control"), QVariant("None"));
    hardwareFlowControl->addItem(tr("Standard", "hardware flow control"), QVariant("Standard"));
    hardwareFlowControl->addItem(tr("Explicit RTS (RS485)", "hardware flow control"),
                                 QVariant("Explicit"));

    label = new QLabel(tr("&Software flow control:"), this);
    layout->addWidget(label, 5, 0, 1, 1);
    softwareFlowControl = new QComboBox(this);
    softwareFlowControl->setToolTip(tr("Select the type of software flow control."));
    softwareFlowControl->setStatusTip(tr("Set software flow control"));
    softwareFlowControl->setWhatsThis(
            tr("Set the software flow control characters in use.  This allows the device on the other side of the serial port to request transmission throttling if it cannot accept more data as well as allowing the computer to do the same."));
    softwareFlowControl->setSizePolicy(
            QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Minimum, QSizePolicy::ComboBox));
    layout->addWidget(softwareFlowControl, 5, 1, 1, 1);
    label->setBuddy(softwareFlowControl);
    softwareFlowControl->addItem(tr("Default", "software flow control default"), QVariant());
    softwareFlowControl->setCurrentIndex(0);
    softwareFlowControl->addItem(tr("Disabled", "software flow control"), QVariant("None"));
    softwareFlowControl->addItem(tr("Bi-directional (XON/XOFF)", "software flow control"),
                                 QVariant("Bidirectional"));
    softwareFlowControl->addItem(tr("Input only (XOFF)", "software flow control"),
                                 QVariant("Input"));
    softwareFlowControl->addItem(tr("Output only (XON)", "software flow control"),
                                 QVariant("Output"));

#ifdef Q_OS_UNIX
    softwareFlowControlResume = new QCheckBox(tr("&Resume"), this);
    softwareFlowControlResume->setToolTip(tr("Enable software flow control resume (XANY)."));
    softwareFlowControlResume->setStatusTip(tr("Enable software flow control resume"));
    softwareFlowControlResume->setWhatsThis(
            tr("This enables the XANY character to resume data transfer regardless of the current state."));
    softwareFlowControlResume->setTristate(true);
    softwareFlowControlResume->setCheckState(Qt::PartiallyChecked);
    layout->addWidget(softwareFlowControlResume, 5, 2, 1, 1);
#endif

    initialBreak = new QCheckBox(tr("Send &initial break"), this);
    initialBreak->setToolTip(tr("This enables the generation of the initial break condition."));
    initialBreak->setStatusTip(tr("Generate break condition initially"));
    initialBreak->setWhatsThis(
            tr("If this option is set then a break condition will be generated on the serial port when it is first configured.  That is, when this option is set a stream of zero valued bits is generated to inform the device that the line is being reset."));
    initialBreak->setTristate(true);
    initialBreak->setCheckState(Qt::PartiallyChecked);
    layout->addWidget(initialBreak, 6, 0, 1, -1);
}

IOInterfaceSerialPortAdvancedPage::~IOInterfaceSerialPortAdvancedPage() = default;

void IOInterfaceSerialPortAdvancedPage::cleanupPage()
{
    baudSelect->setCurrentIndex(0);
    paritySelect->setCurrentIndex(0);
    dataBitsSelect->setCurrentIndex(0);
    userSetDataBits = false;
    hardwareFlowControl->setCurrentIndex(0);
    softwareFlowControl->setCurrentIndex(0);
}

void IOInterfaceSerialPortAdvancedPage::initializePage()
{
    if (!parent->getInspectAdvanced())
        return;

    int idx = baudSelect->currentIndex();
    if ((idx == -1 && baudSelect->currentText().isEmpty()) || idx == 0) {
        qint64 setBaud = parent->getInterface().read().hash("Baud").toInt64();
        if (INTEGER::defined(setBaud) && setBaud > 0) {
            const int *begin = baudRates;
            const int *end = begin + sizeof(baudRates) / sizeof(baudRates[0]);
            const int *target = qBinaryFind(begin, end, (int) setBaud);
            if (target != end) {
                baudSelect->setCurrentIndex((target - begin) + 1);
            } else if (baudSelect->isEditable()) {
                baudSelect->setEditText(QString::number((int) setBaud));
            }
        }
    }

    if (paritySelect->currentIndex() <= 0) {
        const auto &check = parent->getInterface().read().hash("Parity").toString();
        if (Util::equal_insensitive(check, "none")) {
            paritySelect->setCurrentIndex(1);
        } else if (Util::equal_insensitive(check, "even")) {
            paritySelect->setCurrentIndex(2);
        } else if (Util::equal_insensitive(check, "odd")) {
            paritySelect->setCurrentIndex(3);
        }
#ifdef Q_OS_WIN32
        else if (Util::equal_insensitive(check, "space")) {
            paritySelect->setCurrentIndex(4);
        } else if (Util::equal_insensitive(check, "mark")) {
            paritySelect->setCurrentIndex(5);
        }
#endif
    }

    if (dataBitsSelect->currentIndex() <= 0) {
        qint64 setBits = parent->getInterface().read().hash("DataBits").toInt64();
        if (INTEGER::defined(setBits) && setBits >= 5 && setBits <= 8) {
            dataBitsSelect->setCurrentIndex((int) setBits - 4);
        }
    }

    if (stopBitsSelect->currentIndex() <= 0) {
        qint64 setBits = parent->getInterface().read().hash("StopBits").toInt64();
        if (INTEGER::defined(setBits) && setBits >= 1 &&
#ifdef Q_OS_WIN32
                setBits <= 3
#else
                setBits <= 2
#endif
                ) {
            stopBitsSelect->setCurrentIndex((int) setBits);
        }
    }

    if (hardwareFlowControl->currentIndex() <= 0) {
        const auto &check = parent->getInterface().read().hash("HardwareFlowControl").toString();
        if (Util::equal_insensitive(check, "Standard", "RS232")) {
            hardwareFlowControl->setCurrentIndex(2);
        } else if (Util::equal_insensitive(check, "Explicit", "RTSOnSend", "RS485")) {
            hardwareFlowControl->setCurrentIndex(3);
        } else if (Util::equal_insensitive(check, "None", "Off", "Disable")) {
            hardwareFlowControl->setCurrentIndex(1);
        }
    }

    if (softwareFlowControl->currentIndex() <= 0) {
        const auto &check = parent->getInterface().read().hash("SoftwareFlowControl").toString();
        if (Util::equal_insensitive(check, "xonxoff", "xoffxon", "both", "bidirectional")) {
            softwareFlowControl->setCurrentIndex(2);
        } else if (Util::equal_insensitive(check, "xoff", "input")) {
            softwareFlowControl->setCurrentIndex(3);
        } else if (Util::equal_insensitive(check, "xon", "output")) {
            softwareFlowControl->setCurrentIndex(4);
        } else if (Util::equal_insensitive(check, "none", "disabled")) {
            softwareFlowControl->setCurrentIndex(1);
        }
    }

#ifdef Q_OS_UNIX
    if (softwareFlowControlResume->checkState() == Qt::PartiallyChecked) {
        if (parent->getInterface().read().hash("SoftwareFlowControlResume").toBool()) {
            softwareFlowControlResume->setCheckState(Qt::Checked);
        } else {
            softwareFlowControlResume->setCheckState(Qt::Unchecked);
        }
    }
#endif

    if (initialBreak->checkState() == Qt::PartiallyChecked) {
        if (parent->getInterface().read().hash("NoInitialBreak").toBool()) {
            initialBreak->setCheckState(Qt::Unchecked);
        } else {
            initialBreak->setCheckState(Qt::Checked);
        }
    }
}

void IOInterfaceSerialPortAdvancedPage::paritySet()
{
    if (userSetDataBits)
        return;
    int idx = paritySelect->currentIndex();
    if (idx <= 0)
        return;
    QString check(paritySelect->itemData(idx).toString().toLower());
    if (check == "none") {
        dataBitsSelect->setCurrentIndex(4);
    } else if (check == "even") {
        dataBitsSelect->setCurrentIndex(3);
    } else if (check == "odd") {
        dataBitsSelect->setCurrentIndex(3);
    }
}

void IOInterfaceSerialPortAdvancedPage::dataBitsSet()
{
    userSetDataBits = true;
}

Variant::Root IOInterfaceSerialPortAdvancedPage::overlayValue() const
{
    Variant::Root result;
    auto overlay = result.write();

    int idx = baudSelect->currentIndex();
    if (idx > 0) {
        overlay.hash("Baud").setInt64(baudSelect->itemData(idx).toInt());
    } else if (idx < 0 && baudSelect->isEditable()) {
        bool ok = false;
        qint64 setBaud = baudSelect->currentText().toLongLong(&ok);
        if (ok && setBaud > 0) {
            overlay.hash("Baud").setInt64(setBaud);
        }
    }

    idx = paritySelect->currentIndex();
    if (idx > 0) {
        overlay.hash("Parity").setString(paritySelect->itemData(idx).toString());
    }

    idx = dataBitsSelect->currentIndex();
    if (idx > 0) {
        overlay.hash("DataBits").setInt64(dataBitsSelect->itemData(idx).toInt());
    }

    idx = stopBitsSelect->currentIndex();
    if (idx > 0) {
        overlay.hash("StopBits").setInt64(stopBitsSelect->itemData(idx).toInt());
    }

    idx = hardwareFlowControl->currentIndex();
    if (idx > 0) {
        overlay.hash("HardwareFlowControl")
               .setString(hardwareFlowControl->itemData(idx).toString());
    }

    idx = softwareFlowControl->currentIndex();
    if (idx > 0) {
        overlay.hash("SoftwareFlowControl")
               .setString(softwareFlowControl->itemData(idx).toString());
    }

#ifdef Q_OS_UNIX
    switch (softwareFlowControlResume->checkState()) {
    case Qt::Checked:
        overlay.hash("SoftwareFlowControlResume").setBool(true);
        break;
    case Qt::Unchecked:
        overlay.hash("SoftwareFlowControlResume").setBool(false);
        break;
    default:
        break;
    }
#endif

    switch (initialBreak->checkState()) {
    case Qt::Checked:
        overlay.hash("NoInitialBreak").setBool(false);
        break;
    case Qt::Unchecked:
        overlay.hash("NoInitialBreak").setBool(true);
        break;
    default:
        break;
    }

    return result;
}


IOInterfaceRemoteConnectionPage::IOInterfaceRemoteConnectionPage(IOInterfaceSelect *p,
                                                                 const QString &serverFieldName,
                                                                 const QString &portFieldName,
                                                                 const QString &sslFieldName)
        : QWizardPage(p), parent(p)
{
    QGridLayout *layout = new QGridLayout(this);
    setLayout(layout);

    QLabel *label = new QLabel(tr("&Server:"), this);
    layout->addWidget(label, 0, 0, 1, 1);
    serverName = new QLineEdit(this);
    serverName->setToolTip(tr("Server name to connect to."));
    serverName->setStatusTip(tr("Server name to connect to"));
    serverName->setWhatsThis(
            tr("This is the name or IP address of the server to connect to.  If it is specified as a host name then at name will be resolved at run time."));
    layout->addWidget(serverName, 0, 1, 1, 1);
    label->setBuddy(serverName);
    registerField(serverFieldName, serverName);
    connect(serverName, SIGNAL(textChanged(
                                       const QString &)), this, SIGNAL(completeChanged()));

    label = new QLabel(tr("&Port:"), this);
    layout->addWidget(label, 1, 0, 1, 1);
    serverPort = new QSpinBox(this);
    serverPort->setToolTip(tr("Server port number."));
    serverPort->setStatusTip(tr("Server port"));
    serverPort->setWhatsThis(tr("This is the numeric port on the server to connect to."));
    serverPort->setRange(1, 65535);
    serverPort->setValue(23);
    layout->addWidget(serverPort, 1, 1, 1, 1);
    label->setBuddy(serverPort);
    registerField(portFieldName, serverPort);
    connect(serverPort, SIGNAL(valueChanged(int)), this, SLOT(portChanged()));
    userPortSet = false;

    userSSLSet = false;
    if (!sslFieldName.isEmpty()) {
        enableSSL = new QCheckBox(tr("Enable SS&L"), this);
        enableSSL->setToolTip(tr("Enable SSL encryption on the connection."));
        enableSSL->setStatusTip(tr("Enable SSL"));
        enableSSL->setWhatsThis(
                tr("This enables SSL encryption of the connection.  The certificate and key are configured on a subsequent page."));
        layout->addWidget(enableSSL, 2, 0, 1, -1);
        enableSSL->setChecked(false);
        registerField(sslFieldName, enableSSL);
        connect(enableSSL, SIGNAL(toggled(bool)), this, SLOT(sslChanged()));
    } else {
        enableSSL = nullptr;
    }
}

IOInterfaceRemoteConnectionPage::~IOInterfaceRemoteConnectionPage() = default;

void IOInterfaceRemoteConnectionPage::initializePage()
{
    if (serverName->text().isEmpty()) {
        serverName->setText(parent->getInterface().read().hash("ServerName").toQString());
    }

    if (!userPortSet) {
        qint64 port = parent->getInterface().read().hash("ServerPort").toInt64();
        if (INTEGER::defined(port) && port >= 1 && port <= 65535) {
            disconnect(serverPort, SIGNAL(valueChanged(int)), this, SLOT(portChanged()));
            serverPort->setValue((int) port);
            connect(serverPort, SIGNAL(valueChanged(int)), this, SLOT(portChanged()));
        }
    }

    if (!userSSLSet && enableSSL) {
        disconnect(enableSSL, SIGNAL(toggled(bool)), this, SLOT(sslChanged()));
        enableSSL->setChecked(parent->getInterface().read().hash("SSL").exists());
        connect(enableSSL, SIGNAL(toggled(bool)), this, SLOT(sslChanged()));
    }
}

void IOInterfaceRemoteConnectionPage::cleanupPage()
{
    serverName->setText(QString());
    serverPort->setValue(23);
    userPortSet = false;
    enableSSL->setChecked(false);
    userSSLSet = false;
}

bool IOInterfaceRemoteConnectionPage::isComplete() const
{ return !serverName->text().isEmpty(); }

void IOInterfaceRemoteConnectionPage::portChanged()
{ userPortSet = true; }

void IOInterfaceRemoteConnectionPage::sslChanged()
{ userSSLSet = true; }


IOInterfaceNetworkServerPage::IOInterfaceNetworkServerPage(IOInterfaceSelect *p,
                                                           const QString &listenAddressFieldName,
                                                           const QString &listenPortFieldName,
                                                           const QString &serverAddressFieldName,
                                                           const QString &serverPortFieldName,
                                                           const QString &fragmentFieldName,
                                                           const QString &disableFramingFieldName,
                                                           const QString &sslFieldName)
        : QWizardPage(p), parent(p)
{
    QGridLayout *layout = new QGridLayout(this);
    setLayout(layout);

    userListenSet = false;
    userServerSet = false;
    userFragmentSet = false;
    userSSLSet = false;

    if (!listenAddressFieldName.isEmpty() && !listenPortFieldName.isEmpty()) {
        QLabel *label = new QLabel(tr("&Port:"), this);
        layout->addWidget(label, 0, 0, 1, 1);
        listenPort = new QSpinBox(this);
        listenPort->setToolTip(tr("Listen port number."));
        listenPort->setStatusTip(tr("Listen port"));
        listenPort->setWhatsThis(tr("This is the numeric port to listen for connections on."));
        if (!serverAddressFieldName.isEmpty()) {
            listenPort->setRange(0, 65535);
            listenPort->setSpecialValueText(tr("Automatic"));
            listenPort->setValue(0);
        } else {
            listenPort->setRange(1, 65535);
            listenPort->setValue(23);
        }
        layout->addWidget(listenPort, 0, 1, 1, 1);
        label->setBuddy(listenPort);
        registerField(listenPortFieldName, listenPort);
        connect(listenPort, SIGNAL(valueChanged(int)), this, SLOT(listenChanged()));
        connect(listenPort, SIGNAL(valueChanged(int)), this, SIGNAL(completeChanged()));

        label = new QLabel(tr("&Listen Address:"), this);
        layout->addWidget(label, 1, 0, 1, 1);
        listenAddress = new QLineEdit(this);
        listenAddress->setToolTip(tr("Address to listen for connections on."));
        listenAddress->setStatusTip(tr("Local listen address"));
        listenAddress->setWhatsThis(
                tr("This is the IP address to listen for incoming connections on.  This is used to restrict the network interface that accepts connections."));
        listenAddress->setPlaceholderText(tr("All"));
        layout->addWidget(listenAddress, 1, 1, 1, 1);
        label->setBuddy(listenAddress);
        registerField(listenAddressFieldName, listenAddress);
        connect(listenAddress, SIGNAL(textChanged(
                                              const QString &)), this, SLOT(listenChanged()));
    } else {
        listenPort = nullptr;
        listenAddress = nullptr;
    }

    if (!serverAddressFieldName.isEmpty() && !serverPortFieldName.isEmpty()) {
        QLabel *label = new QLabel(tr("&Server:"), this);
        layout->addWidget(label, 4, 0, 1, 1);
        serverAddress = new QLineEdit(this);
        serverAddress->setToolTip(tr("Server IP to connect to."));
        serverAddress->setStatusTip(tr("Server IP to connect to"));
        serverAddress->setWhatsThis(
                tr("This is the name or IP address of the server to send data to."));
        serverAddress->setPlaceholderText(tr("None"));
        layout->addWidget(serverAddress, 4, 1, 1, 1);
        label->setBuddy(serverAddress);
        registerField(serverAddressFieldName, serverAddress);
        connect(listenAddress, SIGNAL(textChanged(
                                              const QString &)), this, SIGNAL(completeChanged()));
        connect(listenAddress, SIGNAL(textChanged(
                                              const QString &)), this, SLOT(serverChanged()));

        label = new QLabel(tr("&Remote port:"), this);
        layout->addWidget(label, 5, 0, 1, 1);
        serverPort = new QSpinBox(this);
        serverPort->setToolTip(tr("Server port number."));
        serverPort->setStatusTip(tr("Server port"));
        serverPort->setWhatsThis(tr("This is the numeric port on the server to send data to."));
        serverPort->setRange(1, 65535);
        serverPort->setValue(23);
        layout->addWidget(serverPort, 5, 1, 1, 1);
        label->setBuddy(serverPort);
        registerField(serverPortFieldName, serverPort);
        connect(serverPort, SIGNAL(valueChanged(int)), this, SLOT(serverChanged()));
        connect(serverPort, SIGNAL(valueChanged(int)), this, SIGNAL(completeChanged()));
    } else {
        serverAddress = nullptr;
        serverPort = nullptr;
    }

    if (!fragmentFieldName.isEmpty()) {
        QLabel *label = new QLabel(tr("&Fragment size:"), this);
        layout->addWidget(label, 6, 0, 1, 1);
        fragment = new QSpinBox(this);
        fragment->setToolTip(tr("Size in bytes to break datagrams up at."));
        fragment->setStatusTip(tr("Datagram fragment size"));
        fragment->setWhatsThis(
                tr("This is the maximum size of a single packet sent as a datagram.  This is also subject to hardware level fragmenting."));
        fragment->setRange(0, 65535);
        fragment->setSpecialValueText(tr("Disabled"));
        fragment->setValue(0);
        layout->addWidget(fragment, 6, 1, 1, 1);
        label->setBuddy(fragment);
        registerField(fragmentFieldName, fragment);
        connect(fragment, SIGNAL(valueChanged(int)), this, SLOT(fragmentChanged()));
    } else {
        fragment = nullptr;
    }

    if (!disableFramingFieldName.isEmpty()) {
        disableFraming = new QCheckBox(tr("Disable F&raming"), this);
        disableFraming->setToolTip(tr("Do not send packets as complete frames."));
        disableFraming->setStatusTip(tr("Disable packet framing"));
        disableFraming->setWhatsThis(
                tr("This disables sending packets as complete frames.  For example, set this when lines can arrive as multiple fragmented packets."));
        layout->addWidget(disableFraming, 7, 0, 1, -1);
        disableFraming->setTristate(true);
        disableFraming->setCheckState(Qt::PartiallyChecked);
        registerField(disableFramingFieldName, disableFraming);
        connect(disableFraming, &QCheckBox::stateChanged, this,
                &IOInterfaceNetworkServerPage::disableFramingChanged);
    } else {
        disableFraming = nullptr;
    }

    if (!sslFieldName.isEmpty()) {
        enableSSL = new QCheckBox(tr("Enable SS&L"), this);
        enableSSL->setToolTip(tr("Enable the use of SSL/TLS encryption."));
        enableSSL->setStatusTip(tr("Enable SSL encryption"));
        enableSSL->setWhatsThis(
                tr("This enables SSL encryption of the connection.  The certificate and key are configured on a subsequent page."));
        layout->addWidget(enableSSL, 7, 0, 1, -1);
        enableSSL->setChecked(false);
        registerField(sslFieldName, enableSSL);
        connect(enableSSL, SIGNAL(toggled(bool)), this, SLOT(sslChanged()));
    } else {
        enableSSL = nullptr;
    }
}

IOInterfaceNetworkServerPage::~IOInterfaceNetworkServerPage() = default;

static QString formatAddress(const Variant::Read &addr)
{
    QString str(addr.toQString());
    if (!str.contains('/')) {
        return QHostAddress(str).toString();
    }
    QPair<QHostAddress, int> result(QHostAddress::parseSubnet(str));
#ifndef QT_NO_IPV6
    if (result.first.protocol() == QAbstractSocket::IPv6Protocol && result.second == 128)
        return result.first.toString();
#endif
#ifndef QT_NO_IPV4
    if (result.first.protocol() == QAbstractSocket::IPv4Protocol && result.second == 32)
        return result.first.toString();
#endif
    QString out(result.first.toString());
    out.append('/');
    out.append(QString::number(result.second));
    return out;
}

static QString formatRestriction(const Variant::Read &addr)
{
    if (!addr.exists())
        return QString();
    if (addr.getType() == Variant::Type::Array) {
        QString output;
        for (auto add : addr.toArray()) {
            if (!output.isEmpty())
                output.append(' ');
            output.append(formatAddress(add));
        }
        return output;
    }
    return formatAddress(addr);
}

void IOInterfaceNetworkServerPage::initializePage()
{
    if (!userListenSet && listenAddress) {
        disconnect(listenAddress, SIGNAL(textChanged(
                                                 const QString &)), this, SLOT(listenChanged()));
        listenAddress->setText(parent->getInterface().read().hash("ListenAddress").toQString());
        connect(listenAddress, SIGNAL(textChanged(
                                              const QString &)), this, SLOT(listenChanged()));
    }

    if (!userListenSet && listenPort) {
        qint64 port = parent->getInterface().read().hash("LocalPort").toInt64();
        disconnect(listenPort, SIGNAL(valueChanged(int)), this, SLOT(listenChanged()));
        if (INTEGER::defined(port) && port >= 1 && port <= 65535) {
            listenPort->setValue((int) port);
        } else {
            listenPort->setValue(0);
        }
        connect(listenPort, SIGNAL(valueChanged(int)), this, SLOT(listenChanged()));
    }

    if (!userServerSet && serverAddress) {
        disconnect(serverAddress, SIGNAL(textChanged(
                                                 const QString &)), this, SLOT(serverChanged()));
        serverAddress->setText(parent->getInterface().read().hash("ServerAddress").toQString());
        connect(serverAddress, SIGNAL(textChanged(
                                              const QString &)), this, SLOT(serverChanged()));
    }

    if (!userServerSet && serverPort) {
        qint64 port = parent->getInterface().read().hash("ServerPort").toInt64();
        if (INTEGER::defined(port) && port >= 1 && port <= 65535) {
            disconnect(serverPort, SIGNAL(valueChanged(int)), this, SLOT(serverChanged()));
            serverPort->setValue((int) port);
            connect(serverPort, SIGNAL(valueChanged(int)), this, SLOT(serverChanged()));
        }
    }

    if (!userFragmentSet && fragment) {
        qint64 sz = parent->getInterface().read().hash("FragmentSize").toInt64();
        disconnect(fragment, SIGNAL(valueChanged(int)), this, SLOT(fragmentChanged()));
        if (INTEGER::defined(sz)) {
            fragment->setValue((int) sz);
        } else {
            fragment->setValue(0);
        }
        connect(fragment, SIGNAL(valueChanged(int)), this, SLOT(fragmentChanged()));
    }

    if (!userDisableFraming && disableFraming) {
        disconnect(disableFraming, &QCheckBox::stateChanged, this,
                   &IOInterfaceNetworkServerPage::disableFramingChanged);
        if (parent->getInterface().read().hash("DisableFraming").exists()) {
            disableFraming->setCheckState(
                    parent->getInterface().read().hash("DisableFraming").toBoolean() ? Qt::Checked
                                                                                     : Qt::Unchecked);
        } else {
            disableFraming->setCheckState(Qt::PartiallyChecked);
        }
        connect(disableFraming, &QCheckBox::stateChanged, this,
                &IOInterfaceNetworkServerPage::disableFramingChanged);
    }

    if (!userSSLSet && enableSSL) {
        disconnect(enableSSL, SIGNAL(toggled(bool)), this, SLOT(sslChanged()));
        enableSSL->setChecked(parent->getInterface().read().hash("SSL").exists());
        connect(enableSSL, SIGNAL(toggled(bool)), this, SLOT(sslChanged()));
    }
}

void IOInterfaceNetworkServerPage::cleanupPage()
{
    if (listenPort) {
        if (listenPort->minimum() == 0)
            listenPort->setValue(0);
        else
            listenPort->setValue(23);
    }
    if (listenAddress)
        listenAddress->setText(QString());
    userListenSet = false;

    if (serverAddress)
        serverAddress->setText(QString());
    if (serverPort)
        serverPort->setValue(23);
    userServerSet = false;

    if (fragment)
        fragment->setValue(0);
    userFragmentSet = false;

    if (disableFraming)
        disableFraming->setCheckState(Qt::PartiallyChecked);
    userDisableFraming = false;

    if (enableSSL)
        enableSSL->setChecked(false);
    userSSLSet = false;
}

bool IOInterfaceNetworkServerPage::isComplete() const
{
    if (listenPort && listenPort->value() > 0)
        return true;
    if (serverAddress && serverPort && !serverAddress->text().isEmpty() && serverPort->value() > 0)
        return true;
    return false;
}

void IOInterfaceNetworkServerPage::listenChanged()
{ userListenSet = true; }

void IOInterfaceNetworkServerPage::serverChanged()
{ userServerSet = true; }

void IOInterfaceNetworkServerPage::fragmentChanged()
{ userFragmentSet = true; }

void IOInterfaceNetworkServerPage::disableFramingChanged()
{ userDisableFraming = true; }

void IOInterfaceNetworkServerPage::sslChanged()
{ userSSLSet = true; }


IOInterfaceQtLocalSocketPage::IOInterfaceQtLocalSocketPage(IOInterfaceSelect *p) : parent(p)
{
    setTitle(tr("Select Qt Socket Name"));
    setSubTitle(tr("This is the internal name of a Qt wrapped local system socket."));

    QGridLayout *layout = new QGridLayout(this);
    setLayout(layout);

    QLabel *label = new QLabel(tr("&Socket:"), this);
    layout->addWidget(label, 0, 0, 1, 1);
    socketName = new QLineEdit(this);
    socketName->setToolTip(tr("Qt local socket name to connect to."));
    socketName->setStatusTip(tr("Qt local socket"));
    socketName->setWhatsThis(
            tr("This is the internal name of a Qt socket to read and write data to.  This will be subject to Qt's own operating system specific changes, so it will likely only be accessible from another program using Qt to perform the same transformation."));
    layout->addWidget(socketName, 0, 1, 1, 1);
    label->setBuddy(socketName);
    registerField("qtsocketname", socketName);
    connect(socketName, SIGNAL(textChanged(
                                       const QString &)), this, SIGNAL(completeChanged()));
}

IOInterfaceQtLocalSocketPage::~IOInterfaceQtLocalSocketPage() = default;

void IOInterfaceQtLocalSocketPage::initializePage()
{
    if (socketName->text().isEmpty()) {
        socketName->setText(parent->getInterface().read().hash("Name").toQString());
    }
}

void IOInterfaceQtLocalSocketPage::cleanupPage()
{
    socketName->setText(QString());
}

bool IOInterfaceQtLocalSocketPage::isComplete() const
{ return !socketName->text().isEmpty(); }


IOInterfaceCommandPage::IOInterfaceCommandPage(IOInterfaceSelect *p) : parent(p)
{
    setTitle(tr("Select Command"));
    setSubTitle(tr("This is a command to execute and read and write data to and from."));

    QGridLayout *layout = new QGridLayout(this);
    setLayout(layout);

    QLabel *label = new QLabel(tr("&Command:"), this);
    layout->addWidget(label, 0, 0, 1, 1);
    commandLine = new QLineEdit(this);
    commandLine->setToolTip(tr("The command line to execute."));
    commandLine->setStatusTip(tr("Command line to execute"));
    commandLine->setWhatsThis(
            tr("This is a command line to execute and re-direct input and output to and from.  If it contains any special characters it is wrapped in a system shell."));
    layout->addWidget(commandLine, 0, 1, 1, 1);
    label->setBuddy(commandLine);
    registerField("command", commandLine);
    connect(commandLine, SIGNAL(textChanged(
                                        const QString &)), this, SIGNAL(completeChanged()));

    errorStreamAsEcho = new QCheckBox(tr("Use &error stream as echo"), this);
    errorStreamAsEcho->setToolTip(tr("Use the command error output stream as the echo."));
    errorStreamAsEcho->setStatusTip(tr("Error stream as echo"));
    errorStreamAsEcho->setWhatsThis(
            tr("This causes the error output generated by the command to be interpreted as echo/control from another source.  For example, if the command is snooping on another device, anything written to it by another source could be output on standard error."));
    layout->addWidget(errorStreamAsEcho, 1, 0, 1, 1);
    errorStreamAsEcho->setTristate(true);
    errorStreamAsEcho->setCheckState(Qt::PartiallyChecked);
    registerField("commandErrorStreamAsEcho", errorStreamAsEcho);
}

IOInterfaceCommandPage::~IOInterfaceCommandPage() = default;

void IOInterfaceCommandPage::initializePage()
{
    if (commandLine->text().isEmpty()) {
        QString command(parent->getInterface().read().hash("Command").toQString());
        QStringList arguments;
        auto rawArgs = parent->getInterface().read().hash("Arguments");
        if (rawArgs.getType() != Variant::Type::Array) {
            arguments = rawArgs.toQString().split(QRegExp("\\s+"));
        } else {
            for (auto add : rawArgs.toArray()) {
                arguments.append(add.toQString());
            }
        }

#if defined(Q_OS_UNIX)
        if (arguments.size() == 2 &&
                (command == "sh" ||
                        command == "bash" ||
                        command == "/bin/sh" ||
                        command == "/bin/bash") &&
                arguments.at(0) == "-c") {
            commandLine->setText(arguments.at(1));
        } else
#elif defined(Q_OS_WIN32)
            if (arguments.size() == 2 &&
                    (command.compare("cmd", Qt::CaseInsensitive) ||
                    command.compare("cmd.exe", Qt::CaseInsensitive)) &&
                    arguments.at(0) == "/c") {
                commandLine->setText(arguments.at(1));
            } else
#endif
        {
            for (QList<QString>::const_iterator add = arguments.constBegin(),
                    end = arguments.constEnd(); add != end; ++add) {
                QString arg(*add);
                if (!command.endsWith(' '))
                    command.append(' ');
                if (arg.contains('\"') || arg.contains(' ')) {
                    arg.replace('\"', "\\\"");
                    command.append('\"');
                    command.append(arg);
                    command.append('\"');
                } else {
                    command.append(arg);
                }
            }
            commandLine->setText(command);
        }
    }

    if (errorStreamAsEcho->checkState() == Qt::PartiallyChecked) {
        if (parent->getInterface().read().hash("ErrorStreamAsEcho").exists()) {
            errorStreamAsEcho->setCheckState(
                    parent->getInterface().read().hash("ErrorStreamAsEcho").toBoolean()
                    ? Qt::Checked : Qt::Unchecked);
        }
    }
}

void IOInterfaceCommandPage::cleanupPage()
{
    commandLine->setText(QString());
    errorStreamAsEcho->setCheckState(Qt::PartiallyChecked);
}

bool IOInterfaceCommandPage::isComplete() const
{ return !commandLine->text().isEmpty(); }


static QSet<QString> toChannels(const Variant::Read &input)
{
    QSet<QString> result;
    for (const auto &add : input.toFlags()) {
        result.insert(QString::fromStdString(add));
    }
    return result;
}

namespace {
class MultiplexerListAddRemoveDelegate : public QItemDelegate {
    IOInterfaceMultiplexerPage *list;

    class Button : public QPushButton {
    public:
        explicit Button(const QString &text, QWidget *parent = 0) : QPushButton(text, parent)
        { }

        virtual ~Button() = default;

        virtual QSize sizeHint() const
        {
            QSize textSize(fontMetrics().size(Qt::TextShowMnemonic, text()));
            QStyleOptionButton opt;
            opt.initFrom(this);
            opt.rect.setSize(textSize);
            return style()->sizeFromContents(QStyle::CT_PushButton, &opt, textSize, this);
        }
    };

public:
    explicit MultiplexerListAddRemoveDelegate(IOInterfaceMultiplexerPage *l) : QItemDelegate(l),
                                                                               list(l)
    { }

    virtual ~MultiplexerListAddRemoveDelegate() = default;

    virtual QWidget *createEditor(QWidget *parent,
                                  const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const
    {
        Q_UNUSED(option);

        QPushButton *button = new Button(tr("-", "remove text"), parent);
        button->setProperty("model-row", index.row());

        return button;
    }

    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
    {
        QPushButton *button = static_cast<QPushButton *>(editor);

        button->disconnect(list);

        QVariant data(index.data(Qt::EditRole));
        if (data.toInt() != 1) {
            button->setText(tr("-", "remove text"));
            connect(button, SIGNAL(clicked(bool)), list, SLOT(removeRow()));
        } else {
            button->setText(tr("Add", "add text"));
            connect(button, SIGNAL(clicked(bool)), list, SLOT(addRow()));
        }

        button->setProperty("model-row", index.row());
    }

    virtual void setModelData(QWidget *editor,
                              QAbstractItemModel *model,
                              const QModelIndex &index) const
    {
        Q_UNUSED(editor);
        Q_UNUSED(model);
        Q_UNUSED(index);
    }
};

class MultiplexerListEditDelegate : public QItemDelegate {
    IOInterfaceMultiplexerPage *list;

public:
    explicit MultiplexerListEditDelegate(IOInterfaceMultiplexerPage *l) : QItemDelegate(l), list(l)
    { }

    virtual ~MultiplexerListEditDelegate() = default;

    virtual QWidget *createEditor(QWidget *parent,
                                  const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const
    {
        Q_UNUSED(option);

        QPushButton *button = new QPushButton(tr("Interface", "edit text"), parent);
        button->setToolTip(tr("Interface configuration"));
        button->setStatusTip(tr("Interface configuration"));
        button->setWhatsThis(
                tr("This will show another dialog that allows for the interface of the multiplexer to be configured."));
        button->setProperty("model-row", index.row());

        return button;
    }

    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
    {
        QPushButton *button = static_cast<QPushButton *>(editor);

        button->disconnect(list);
        connect(button, SIGNAL(clicked(bool)), list, SLOT(editRow()));

        button->setProperty("model-row", index.row());
    }

    virtual void setModelData(QWidget *editor,
                              QAbstractItemModel *model,
                              const QModelIndex &index) const
    {
        Q_UNUSED(editor);
        Q_UNUSED(model);
        Q_UNUSED(index);
    }
};

class MultiplexerListOutputDelegate : public QItemDelegate {
    IOInterfaceMultiplexerPage *list;
public:
    explicit MultiplexerListOutputDelegate(IOInterfaceMultiplexerPage *l) : QItemDelegate(l),
                                                                            list(l)
    { }

    virtual ~MultiplexerListOutputDelegate() = default;

    virtual QWidget *createEditor(QWidget *parent,
                                  const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const
    {
        Q_UNUSED(option);

        IOInterfaceMultiplexerChannelsButton
                *button = new IOInterfaceMultiplexerChannelsButton(tr("Output"), list, parent);
        button->setToolTip(tr("Output write channel selection"));
        button->setStatusTip(tr("Output"));
        button->setWhatsThis(
                tr("This determines the channels that this interface receives output from.  Any data on the given channels will be written to this interface as output."));
        button->setProperty("model-row", index.row());
        button->setProperty("model-column", index.column());

        return button;
    }

    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
    {
        IOInterfaceMultiplexerChannelsButton
                *button = static_cast<IOInterfaceMultiplexerChannelsButton *>(editor);

        button->disconnect(list);
        button->configure(toChannels(index.data().value<Variant::Write>()),
                          list->availableChannels());
        connect(button, SIGNAL(changed()), list, SLOT(setRowChannels()));

        button->setProperty("model-row", index.row());
        button->setProperty("model-column", index.column());
    }

    virtual void setModelData(QWidget *editor,
                              QAbstractItemModel *model,
                              const QModelIndex &index) const
    {
        Q_UNUSED(editor);
        Q_UNUSED(model);
        Q_UNUSED(index);
    }
};

class MultiplexerListInputDelegate : public QItemDelegate {
    IOInterfaceMultiplexerPage *list;
public:
    explicit MultiplexerListInputDelegate(IOInterfaceMultiplexerPage *l) : QItemDelegate(l), list(l)
    { }

    virtual ~MultiplexerListInputDelegate() = default;

    virtual QWidget *createEditor(QWidget *parent,
                                  const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const
    {
        Q_UNUSED(option);

        IOInterfaceMultiplexerChannelsButton
                *button = new IOInterfaceMultiplexerChannelsButton(tr("Input"), list, parent);
        button->setToolTip(tr("Input read channel selection"));
        button->setStatusTip(tr("Input"));
        button->setWhatsThis(
                tr("This determines the channels that are assigned to any data that this interface generates."));
        button->setProperty("model-row", index.row());
        button->setProperty("model-column", index.column());

        return button;
    }

    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
    {
        IOInterfaceMultiplexerChannelsButton
                *button = static_cast<IOInterfaceMultiplexerChannelsButton *>(editor);

        button->disconnect(list);
        button->configure(toChannels(index.data().value<Variant::Write>()),
                          list->availableChannels());
        connect(button, SIGNAL(changed()), list, SLOT(setRowChannels()));

        button->setProperty("model-row", index.row());
        button->setProperty("model-column", index.column());
    }

    virtual void setModelData(QWidget *editor,
                              QAbstractItemModel *model,
                              const QModelIndex &index) const
    {
        Q_UNUSED(editor);
        Q_UNUSED(model);
        Q_UNUSED(index);
    }
};

class MultiplexerListEchoDelegate : public QItemDelegate {
    IOInterfaceMultiplexerPage *list;
public:
    explicit MultiplexerListEchoDelegate(IOInterfaceMultiplexerPage *l) : QItemDelegate(l), list(l)
    { }

    virtual ~MultiplexerListEchoDelegate() = default;

    virtual QWidget *createEditor(QWidget *parent,
                                  const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const
    {
        Q_UNUSED(option);

        IOInterfaceMultiplexerChannelsButton
                *button = new IOInterfaceMultiplexerChannelsButton(tr("Echo"), list, parent);
        button->setToolTip(tr("Control echo read channel selection"));
        button->setStatusTip(tr("Echo"));
        button->setWhatsThis(
                tr("This determines the channels that are assigned to any control echo that this interface generates."));
        button->setProperty("model-row", index.row());
        button->setProperty("model-column", index.column());

        return button;
    }

    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
    {
        IOInterfaceMultiplexerChannelsButton
                *button = static_cast<IOInterfaceMultiplexerChannelsButton *>(editor);

        button->disconnect(list);
        button->configure(toChannels(index.data().value<Variant::Write>()),
                          list->availableChannels());
        connect(button, SIGNAL(changed()), list, SLOT(setRowChannels()));

        button->setProperty("model-row", index.row());
        button->setProperty("model-column", index.column());
    }

    virtual void setModelData(QWidget *editor,
                              QAbstractItemModel *model,
                              const QModelIndex &index) const
    {
        Q_UNUSED(editor);
        Q_UNUSED(model);
        Q_UNUSED(index);
    }
};
}

IOInterfaceMultiplexerChannelsButton::IOInterfaceMultiplexerChannelsButton(const QString &text,
                                                                           IOInterfaceMultiplexerPage *p,
                                                                           QWidget *parent)
        : QWidget(parent),
          page(p),
          button(NULL),
          menu(NULL),
          separator(NULL),
          channels(),
          userChanged(false)
{
    QHBoxLayout *layout = new QHBoxLayout(this);
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    button = new QPushButton(text, this);
    layout->addWidget(button);
    connect(button, SIGNAL(clicked(bool)), this, SLOT(addSelected()));

    page->channelButtons.append(QPointer<IOInterfaceMultiplexerChannelsButton>(this));
}

void IOInterfaceMultiplexerChannelsButton::configure(const QSet<QString> &selected,
                                                     const QSet<QString> &available)
{
    userChanged = false;

    if (menu != NULL) {
        menu->deleteLater();
        button->setMenu(0);
    }
    button->disconnect(this);

    menu = new QMenu(button);

    QStringList sorted(available.values());
    std::sort(sorted.begin(), sorted.end());
    channels.clear();
    for (QStringList::const_iterator add = sorted.constBegin(), endAdd = sorted.constEnd();
            add != endAdd;
            ++add) {
        QAction *action = new QAction(*add, menu);
        action->setCheckable(true);
        action->setChecked(selected.contains(*add));
        menu->addAction(action);
        channels.insert(*add, action);

        connect(action, SIGNAL(toggled(bool)), this, SLOT(selectedChanged()));
        connect(action, SIGNAL(toggled(bool)), this, SIGNAL(changed()));
    }

    separator = menu->addSeparator();

    QAction *action = new QAction(tr("&Add"), menu);
    action->setToolTip(tr("Add a new channel"));
    action->setStatusTip(tr("Add channel"));
    action->setWhatsThis(tr("Add a new channel to the available list."));
    menu->addAction(action);
    connect(action, SIGNAL(triggered(bool)), this, SLOT(addSelected()));

    if (!channels.isEmpty()) {
        button->setMenu(menu);
        return;
    }

    connect(button, SIGNAL(clicked(bool)), this, SLOT(addSelected()));
}

void IOInterfaceMultiplexerChannelsButton::configure(const Variant::Read &from,
                                                     const QSet<QString> &available)
{
    if (userChanged)
        return;
    configure(toChannels(from), available);
}

void IOInterfaceMultiplexerChannelsButton::addChannel(const QString &name, bool select)
{
    if (menu == NULL) {
        menu = new QMenu(button);

        separator = menu->addSeparator();

        QAction *action = new QAction(tr("&Add"), menu);
        action->setToolTip(tr("Add a new channel"));
        action->setStatusTip(tr("Add channel"));
        action->setWhatsThis(tr("Add a new channel to the available list."));
        menu->addAction(action);
        connect(action, SIGNAL(triggered(bool)), this, SLOT(addSelected()));
    }

    button->setMenu(menu);
    button->disconnect(this);

    QAction *action = new QAction(name, menu);
    action->setCheckable(true);
    action->setChecked(select);
    menu->insertAction(separator, action);
    channels.insert(name, action);

    connect(action, SIGNAL(toggled(bool)), this, SLOT(selectedChanged()));
}

QSet<QString> IOInterfaceMultiplexerChannelsButton::getAvailable(const Variant::Read &from) const
{
    if (userChanged)
        return QSet<QString>::fromList(channels.keys());
    return toChannels(from);
}

Variant::Root IOInterfaceMultiplexerChannelsButton::getSelected() const
{
    Variant::Flags selected;
    for (QHash<QString, QAction *>::const_iterator check = channels.constBegin(),
            end = channels.constEnd(); check != end; ++check) {
        if (!check.value()->isChecked())
            continue;
        selected.insert(check.key().toStdString());
    }
    return Variant::Root(std::move(selected));
}

void IOInterfaceMultiplexerChannelsButton::addSelected()
{
    bool ok = false;
    QString name = QInputDialog::getText(this, tr("Channel Add"), tr("Channel"), QLineEdit::Normal,
                                         QString(), &ok);
    if (!ok || name.isEmpty())
        return;

    userChanged = true;
    page->addChannel(name, this);
    emit changed();
}

void IOInterfaceMultiplexerChannelsButton::selectedChanged()
{ userChanged = true; }

IOInterfaceMultiplexerPage::IOInterfaceMultiplexerPage(IOInterfaceSelect *p) : parent(p),
                                                                               output(NULL),
                                                                               input(NULL),
                                                                               echo(NULL)
{
    setTitle(tr("Interface Multiplexer"));
    setSubTitle(
            tr("This configured the interface to multiplex channels from other sub-interfaces.  This allows complex routing between multiple inputs and outputs."));

    QGridLayout *layout = new QGridLayout(this);
    setLayout(layout);

    output = new IOInterfaceMultiplexerChannelsButton(tr("Output"), this, this);
    output->setToolTip(tr("Output writing channel selection"));
    output->setStatusTip(tr("Output"));
    output->setWhatsThis(tr("This determines the channels that output data are written to."));
    layout->addWidget(output, 0, 0, 1, 1);

    input = new IOInterfaceMultiplexerChannelsButton(tr("Input"), this, this);
    input->setToolTip(tr("Input reading channel selection"));
    input->setStatusTip(tr("Input"));
    input->setWhatsThis(tr("This determines the channels that input data are read from."));
    layout->addWidget(input, 0, 1, 1, 1);

    echo = new IOInterfaceMultiplexerChannelsButton(tr("Echo"), this, this);
    echo->setToolTip(tr("Control echo reading channel selection"));
    echo->setStatusTip(tr("Echo"));
    echo->setWhatsThis(tr("This determines the channels that control echo data are read from."));
    layout->addWidget(echo, 0, 2, 1, 1);

    model = new QStandardItemModel(this);
    table = new QTableView(this);
    layout->addWidget(table, 1, 0, 1, -1);
    layout->setRowStretch(1, 1);
    layout->setColumnStretch(0, 1);
    layout->setColumnStretch(1, 1);
    layout->setColumnStretch(2, 1);

    table->setModel(model);

    model->setColumnCount(5);
    table->verticalHeader()->hide();
    table->horizontalHeader()->hide();

    table->setItemDelegateForColumn(0, new MultiplexerListAddRemoveDelegate(this));
    table->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    table->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);

    table->setItemDelegateForColumn(1, new MultiplexerListEditDelegate(this));

    table->setItemDelegateForColumn(2, new MultiplexerListOutputDelegate(this));
    table->setItemDelegateForColumn(3, new MultiplexerListInputDelegate(this));
    table->setItemDelegateForColumn(4, new MultiplexerListEchoDelegate(this));


    QList<QStandardItem *> items;
    QStandardItem *item;
    item = new QStandardItem;
    items << item;
    item->setData(1, Qt::EditRole);
    model->insertRow(0, items);
    table->setSpan(0, 0, 1, 5);

    table->openPersistentEditor(model->index(0, 0));
}

IOInterfaceMultiplexerPage::~IOInterfaceMultiplexerPage() = default;

void IOInterfaceMultiplexerPage::addChannel(const QString &name,
                                            IOInterfaceMultiplexerChannelsButton *origin)
{
    for (QList<QPointer<IOInterfaceMultiplexerChannelsButton> >::iterator
            b = channelButtons.begin(); b != channelButtons.end();) {
        if (b->isNull()) {
            b = channelButtons.erase(b);
            continue;
        }

        (*b)->addChannel(name, *b == origin);
        ++b;
    }
}

void IOInterfaceMultiplexerPage::openAllEditors()
{
    for (int row = 0; row < model->rowCount(); ++row) {
        table->openPersistentEditor(model->index(row, 0));

        if (row == model->rowCount() - 1)
            continue;

        table->openPersistentEditor(model->index(row, 1));
        table->openPersistentEditor(model->index(row, 2));
        table->openPersistentEditor(model->index(row, 3));
        table->openPersistentEditor(model->index(row, 4));
    }
    table->resizeColumnToContents(0);
}

void IOInterfaceMultiplexerPage::addRow()
{
    QList<QStandardItem *> items;

    QStandardItem *item = new QStandardItem;
    items << item;
    item->setData(0, Qt::EditRole);

    Variant::Write add = Variant::Write::empty();

    item = new QStandardItem;
    items << item;
    item->setData(QVariant::fromValue(add), Qt::EditRole);

    item = new QStandardItem;
    items << item;
    item->setData(QVariant::fromValue(add.getPath("Channels/Output")), Qt::EditRole);

    item = new QStandardItem;
    items << item;
    item->setData(QVariant::fromValue(add.getPath("Channels/Input")), Qt::EditRole);

    item = new QStandardItem;
    items << item;
    item->setData(QVariant::fromValue(add.getPath("Channels/Echo")), Qt::EditRole);

    model->insertRow(model->rowCount() - 1, items);

    openAllEditors();

    userInterfacesChanged = true;
}

void IOInterfaceMultiplexerPage::removeRow()
{
    QObject *s = sender();
    if (!s)
        return;
    QVariant p(s->property("model-row"));
    if (!p.isValid())
        return;
    int row = p.toInt();
    if (row < 0 || row >= model->rowCount() - 1)
        return;

    model->removeRows(row, 1);

    for (row = 0; row < model->rowCount(); ++row) {
        table->closePersistentEditor(model->index(row, 0));
    }

    openAllEditors();

    userInterfacesChanged = true;
}

void IOInterfaceMultiplexerPage::editRow()
{
    QObject *s = sender();
    if (!s)
        return;
    QVariant p(s->property("model-row"));
    if (!p.isValid())
        return;
    int row = p.toInt();
    if (row < 0 || row >= model->rowCount() - 1)
        return;

    Variant::Write interface = model->index(row, 1).data().value<Variant::Write>();
    interface.detachFromRoot();
    Variant::Write channels = interface.hash("Channels");
    channels.detachFromRoot();

    IOInterfaceSelect selection(this);
    selection.setInterface(interface);
    if (selection.exec() != QDialog::Accepted)
        return;

    model->index(row, 1).data().value<Variant::Write>().set(selection.getInterface());
    model->index(row, 1).data().value<Variant::Write>().hash("Channels").set(channels);

    userInterfacesChanged = true;
}

void IOInterfaceMultiplexerPage::setRowChannels()
{
    IOInterfaceMultiplexerChannelsButton
            *s = qobject_cast<IOInterfaceMultiplexerChannelsButton *>(sender());
    if (!s)
        return;
    QVariant p(s->property("model-row"));
    if (!p.isValid())
        return;
    int row = p.toInt();
    if (row < 0 || row >= model->rowCount() - 1)
        return;

    model->index(row, s->property("model-column").toInt())
         .data()
         .value<Variant::Write>()
         .set(s->getSelected());
    userInterfacesChanged = true;
}

QSet<QString> IOInterfaceMultiplexerPage::availableChannels() const
{
    QSet<QString> available;
    available |= output->getAvailable(parent->getInterface()["Targets/Output"]);
    available |= input->getAvailable(parent->getInterface()["Targets/Input"]);
    available |= echo->getAvailable(parent->getInterface()["Targets/Echo"]);

    for (QList<QPointer<IOInterfaceMultiplexerChannelsButton> >::const_iterator
            b = channelButtons.constBegin(), endB = channelButtons.constEnd(); b != endB; ++b) {
        if (b->isNull())
            continue;
        QVariant p((*b)->property("model-row"));
        if (!p.isValid())
            continue;
        int row = p.toInt();
        if (row < 0 || row >= model->rowCount() - 1)
            continue;

        available |= (*b)->getAvailable(model->index(row, 1).data().value<Variant::Write>());
    }

    return available;
}

void IOInterfaceMultiplexerPage::initializePage()
{
    QSet<QString> available(availableChannels());

    output->configure(parent->getInterface()["Targets/Output"], available);
    input->configure(parent->getInterface()["Targets/Input"], available);
    echo->configure(parent->getInterface()["Targets/Echo"], available);

    if (!userInterfacesChanged) {
        for (auto c : parent->getInterface()["Interfaces"].toArray()) {
            QList<QStandardItem *> items;

            QStandardItem *item = new QStandardItem;
            items << item;
            item->setData(0, Qt::EditRole);

            Variant::Write add = Variant::Root(c).write();

            item = new QStandardItem;
            items << item;
            item->setData(QVariant::fromValue(add), Qt::EditRole);

            item = new QStandardItem;
            items << item;
            item->setData(QVariant::fromValue(add.getPath("Channels/Output")), Qt::EditRole);

            item = new QStandardItem;
            items << item;
            item->setData(QVariant::fromValue(add.getPath("Channels/Input")), Qt::EditRole);

            item = new QStandardItem;
            items << item;
            item->setData(QVariant::fromValue(add.getPath("Channels/Echo")), Qt::EditRole);

            model->insertRow(model->rowCount() - 1, items);
        }

        openAllEditors();
    }
}

void IOInterfaceMultiplexerPage::cleanupPage()
{
    output->configure();
    input->configure();
    echo->configure();

    userInterfacesChanged = false;
    if (model->rowCount() > 1)
        model->removeRows(1, model->rowCount() - 1);
}

bool IOInterfaceMultiplexerPage::isComplete() const
{
    return true;
}

Variant::Root IOInterfaceMultiplexerPage::overlayValue() const
{
    Variant::Root result;
    result["Type"].setString("Multiplexer");
    result["Targets/Output"].set(output->getSelected());
    result["Targets/Input"].set(input->getSelected());
    result["Targets/Echo"].set(echo->getSelected());

    for (int i = 0, max = model->rowCount() - 1; i < max; i++) {
        auto v = model->index(i, 1).data().value<Variant::Write>();
        if (!v.exists())
            continue;
        result["Interfaces"].toArray().after_back().set(v);
    }

    return result;
}

IOInterfaceURLPage::IOInterfaceURLPage(IOInterfaceSelect *p) : parent(p)
{
    setTitle(tr("Select URL"));
    setSubTitle(tr("This is a URL to perform requests on."));

    QGridLayout *layout = new QGridLayout(this);
    setLayout(layout);

    QLabel *label = new QLabel(tr("&URL:"), this);
    layout->addWidget(label, 0, 0, 1, 1);
    url = new QLineEdit(this);
    url->setToolTip(tr("The URL to use."));
    url->setStatusTip(tr("URL"));
    url->setWhatsThis(tr("This is the URL requests are sent to and data received from."));
    layout->addWidget(url, 0, 1, 1, 1);
    label->setBuddy(url);
    registerField("urlTarget", url);
    connect(url, SIGNAL(textChanged(const QString &)), this, SIGNAL(completeChanged()));

    label = new QLabel(tr("&Method:"), this);
    layout->addWidget(label, 1, 0, 1, 1);
    methodSelect = new QComboBox(this);
    methodSelect->setToolTip(tr("URL access method."));
    methodSelect->setStatusTip(tr("Access method"));
    methodSelect->setWhatsThis(
            tr("This is the method requested on the URL.  POST and PUT allow for data to be sent, while GET is receive only, but issued every time data are requested to be sent."));
    layout->addWidget(methodSelect, 1, 1, 1, 1);
    label->setBuddy(methodSelect);
    methodSelect->addItem("GET");
    methodSelect->addItem("POST");
    methodSelect->addItem("PUT");
    registerField("urlMethod", methodSelect);
}

IOInterfaceURLPage::~IOInterfaceURLPage() = default;

void IOInterfaceURLPage::initializePage()
{
    if (url->text().isEmpty()) {
        url->setText(parent->getInterface().read().hash("URL").toQString());
    }

    if (parent->getInterface().read().hash("Method").exists()) {
        auto method = parent->getInterface().read().hash("Method").toQString();
        if (!method.isEmpty()) {
            methodSelect->setCurrentText(method.toUpper());
        }
    }
}

void IOInterfaceURLPage::cleanupPage()
{
    url->setText(QString());
}

bool IOInterfaceURLPage::isComplete() const
{ return !url->text().isEmpty(); }


#ifdef Q_OS_UNIX


IOInterfaceUnixSocketPage::IOInterfaceUnixSocketPage(IOInterfaceSelect *p) : QWizardPage(p),
                                                                             parent(p)
{
    setTitle(tr("Select a Unix Domain Socket"));
    setSubTitle(tr("Choose a Unix domain socket to communicate with."));

    QGridLayout *layout = new QGridLayout(this);
    setLayout(layout);

    QLabel *label = new QLabel(tr("&Socket:"), this);
    layout->addWidget(label, 0, 0, 1, 1);
    socketEntry = new QLineEdit(this);
    socketEntry->setToolTip(tr("Enter the Unix socket name.  For example: /var/run/input.socket"));
    socketEntry->setStatusTip(tr("Enter Unix socket path"));
    socketEntry->setWhatsThis(
            tr("Enter the full path name of the Unix domain socket to use.  For example: \"/var/run/input.socket\" (without quotes)."));
    label->setBuddy(socketEntry);
    layout->addWidget(socketEntry, 0, 1, 1, 1);

    registerField("unixSocketName", socketEntry);

    QPushButton *manualSelect = new QPushButton(tr("&Select File"), this);
    connect(manualSelect, SIGNAL(clicked()), this, SLOT(showFileSelectDialog())), layout->addWidget(
            manualSelect, 0, 2, 1, 1);

    layout->setColumnStretch(1, 1);

    connect(socketEntry, SIGNAL(textChanged(
                                        const QString &)), this, SIGNAL(completeChanged()));
}

IOInterfaceUnixSocketPage::~IOInterfaceUnixSocketPage() = default;

void IOInterfaceUnixSocketPage::initializePage()
{
    if (socketEntry->text().isEmpty()) {
        socketEntry->setText(parent->getInterface().read().hash("File").toQString());
    }
}

void IOInterfaceUnixSocketPage::cleanupPage()
{ socketEntry->setText(QString()); }

bool IOInterfaceUnixSocketPage::isComplete() const
{ return !socketEntry->text().isEmpty(); }

void IOInterfaceUnixSocketPage::showFileSelectDialog()
{
    QString result(QFileDialog::getOpenFileName(this, tr("Open Unix Socket"), socketEntry->text()));
    if (result.isEmpty())
        return;
    socketEntry->setText(result);
}


IOInterfaceUnixFIFOPage::IOInterfaceUnixFIFOPage(IOInterfaceSelect *p) : QWizardPage(p), parent(p)
{
    setTitle(tr("Select a set of Unix FIFOs"));
    setSubTitle(tr("Choose a set of Unix FIFOs to interface with."));

    QGridLayout *layout = new QGridLayout(this);
    setLayout(layout);

    QLabel *label = new QLabel(tr("&Input:"), this);
    layout->addWidget(label, 0, 0, 1, 1);
    inputEntry = new QLineEdit(this);
    inputEntry->setToolTip(tr("Enter the full path of the FIFO to read data from."));
    inputEntry->setStatusTip(tr("Data input FIFO name"));
    inputEntry->setWhatsThis(
            tr("Enter the full path name of the Unix FIFO to read data from.  For example: /tmp/input"));
    label->setBuddy(inputEntry);
    layout->addWidget(inputEntry, 0, 1, 1, 1);
    connect(inputEntry, SIGNAL(textChanged(
                                       const QString &)), this, SIGNAL(completeChanged()));
    registerField("unixFIFOInput", inputEntry);
    QPushButton *manualSelect = new QPushButton(tr("Select File"), this);
    connect(manualSelect, SIGNAL(clicked()), this,
            SLOT(showInputFileSelectDialog())), layout->addWidget(manualSelect, 0, 2, 1, 1);

    label = new QLabel(tr("&Output:"), this);
    layout->addWidget(label, 1, 0, 1, 1);
    outputEntry = new QLineEdit(this);
    outputEntry->setToolTip(tr("Enter the full path of the FIFO to write data to."));
    outputEntry->setStatusTip(tr("Data output FIFO name"));
    outputEntry->setWhatsThis(
            tr("Enter the full path name of the Unix FIFO to write data to.  For example: /tmp/output"));
    label->setBuddy(outputEntry);
    layout->addWidget(outputEntry, 1, 1, 1, 1);
    connect(outputEntry, SIGNAL(textChanged(
                                        const QString &)), this, SIGNAL(completeChanged()));
    registerField("unixFIFOOutput", outputEntry);
    manualSelect = new QPushButton(tr("Select File"), this);
    connect(manualSelect, SIGNAL(clicked()), this,
            SLOT(showOutputFileSelectDialog())), layout->addWidget(manualSelect, 1, 2, 1, 1);

    label = new QLabel(tr("&Echo:"), this);
    layout->addWidget(label, 2, 0, 1, 1);
    echoEntry = new QLineEdit(this);
    echoEntry->setToolTip(tr("Enter the full path of the FIFO to read echo data from."));
    echoEntry->setStatusTip(tr("Data echo FIFO name"));
    echoEntry->setWhatsThis(
            tr("Enter the full path name of the Unix FIFO to read the echo of data written by another source.  For example: /tmp/echo"));
    label->setBuddy(echoEntry);
    layout->addWidget(echoEntry, 2, 1, 1, 1);
    connect(echoEntry, SIGNAL(textChanged(
                                      const QString &)), this, SIGNAL(completeChanged()));
    registerField("unixFIFOEcho", echoEntry);
    manualSelect = new QPushButton(tr("Select File"), this);
    connect(manualSelect, SIGNAL(clicked()), this,
            SLOT(showEchoFileSelectDialog())), layout->addWidget(manualSelect, 2, 2, 1, 1);

    layout->setColumnStretch(1, 1);
}

IOInterfaceUnixFIFOPage::~IOInterfaceUnixFIFOPage() = default;

void IOInterfaceUnixFIFOPage::initializePage()
{
    if (inputEntry->text().isEmpty()) {
        inputEntry->setText(parent->getInterface().read().hash("Input").toQString());
    }
    if (outputEntry->text().isEmpty()) {
        outputEntry->setText(parent->getInterface().read().hash("Output").toQString());
    }
    if (echoEntry->text().isEmpty()) {
        echoEntry->setText(parent->getInterface().read().hash("Echo").toQString());
    }
}

void IOInterfaceUnixFIFOPage::cleanupPage()
{
    inputEntry->setText(QString());
    outputEntry->setText(QString());
    echoEntry->setText(QString());
}

bool IOInterfaceUnixFIFOPage::isComplete() const
{
    return !inputEntry->text().isEmpty() ||
            !outputEntry->text().isEmpty() ||
            !echoEntry->text().isEmpty();
}

void IOInterfaceUnixFIFOPage::showInputFileSelectDialog()
{
    QString result(QFileDialog::getOpenFileName(this, tr("Open Input FIFO"), inputEntry->text()));
    if (result.isEmpty())
        return;
    inputEntry->setText(result);
}

void IOInterfaceUnixFIFOPage::showOutputFileSelectDialog()
{
    QString result(QFileDialog::getOpenFileName(this, tr("Open Output FIFO"), outputEntry->text()));
    if (result.isEmpty())
        return;
    outputEntry->setText(result);
}

void IOInterfaceUnixFIFOPage::showEchoFileSelectDialog()
{
    QString result
            (QFileDialog::getOpenFileName(this, tr("Open Echo Reading FIFO"), echoEntry->text()));
    if (result.isEmpty())
        return;
    echoEntry->setText(result);
}

#endif


IOInterfaceSSLPage::IOInterfaceSSLPage(IOInterfaceSelect *p, const QString &sslPath) : QWizardPage(
        p), parent(p), path(sslPath)
{
    setTitle(tr("Enter SSL Information"));
    setSubTitle(tr("Select the SSL key and matching certificate to use for authentication."));

    QGridLayout *layout = new QGridLayout(this);
    setLayout(layout);

    QLabel *label = new QLabel(tr("&Certificate:"), this);
    layout->addWidget(label, 0, 0, 1, 1);
    certificateEntry = new QLineEdit(this);
    certificateEntry->setToolTip(tr("Enter the SSL certificate to use."));
    certificateEntry->setStatusTip(tr("SSL Certificate"));
    certificateEntry->setWhatsThis(
            tr("Enter the SSL certificate to use.  This can be either a file name to load from or the raw data."));
    label->setBuddy(certificateEntry);
    layout->addWidget(certificateEntry, 0, 1, 1, 1);
    connect(certificateEntry, SIGNAL(textChanged(
                                             const QString &)), this, SIGNAL(completeChanged()));
    QPushButton *manualSelect = new QPushButton(tr("Select File"), this);
    connect(manualSelect, SIGNAL(clicked()), this, SLOT(showCertificateFileSelectDialog())), layout
            ->addWidget(manualSelect, 0, 2, 1, 1);

    label = new QLabel(tr("&Key:"), this);
    layout->addWidget(label, 1, 0, 1, 1);
    keyEntry = new QLineEdit(this);
    keyEntry->setToolTip(tr("Enter the SSL key that matches the certificate."));
    keyEntry->setStatusTip(tr("SSL Key"));
    keyEntry->setWhatsThis(
            tr("Enter the key that matches the SSL certificate above.  This can be either a file name to load or the raw data."));
    label->setBuddy(keyEntry);
    layout->addWidget(keyEntry, 1, 1, 1, 1);
    connect(keyEntry, SIGNAL(textChanged(
                                     const QString &)), this, SIGNAL(completeChanged()));
    manualSelect = new QPushButton(tr("Select File"), this);
    connect(manualSelect, SIGNAL(clicked()), this,
            SLOT(showKeyFileSelectDialog())), layout->addWidget(manualSelect, 1, 2, 1, 1);

    label = new QLabel(tr("Key &Password:"), this);
    layout->addWidget(label, 2, 0, 1, 1);
    keyPassword = new QLineEdit(this);
    keyPassword->setToolTip(tr("Enter the password for the SSL Key."));
    keyPassword->setStatusTip(tr("SSL Key Password"));
    keyPassword->setWhatsThis(
            tr("If the SSL key is locked with a password, enter it here.  Leave this empty if there is no password."));
    keyPassword->setEchoMode(QLineEdit::Password);
    keyPassword->setPlaceholderText(tr("Leave empty for none"));
    label->setBuddy(keyPassword);
    layout->addWidget(keyPassword, 2, 1, 1, -1);
    userSetKeyPassword = false;
    connect(keyPassword, SIGNAL(textEdited(
                                        const QString &)), this, SLOT(keyPasswordChanged()));

    requireValid = new QCheckBox(tr("Require valid peer certificate"), this);
    requireValid->setToolTip(tr("If set then the peer must have a valid certificate."));
    requireValid->setStatusTip(tr("Require valid peer certificate"));
    requireValid->setWhatsThis(
            tr("If this is set then the peer must present a certificate that is valid with the local certificate authority.  Generally this means that if the peer does not present a certificate or presents a self signed one then the connection will be aborted."));
    layout->addWidget(requireValid, 3, 0, 1, -1);
    userSetRequireValid = false;
    connect(requireValid, SIGNAL(clicked(bool)), this, SLOT(requireValidChanged()));

    layout->setColumnStretch(1, 1);
}

IOInterfaceSSLPage::~IOInterfaceSSLPage() = default;

void IOInterfaceSSLPage::initializePage()
{
    if (certificateEntry->text().isEmpty()) {
        certificateEntry->setText(parent->getInterface()[path].hash("Certificate").toQString());
    }
    if (keyEntry->text().isEmpty()) {
        auto check = parent->getInterface()[path].hash("Key");
        if (check.getType() == Variant::Type::String) {
            keyEntry->setText(check.toQString());
        } else {
            keyEntry->setText(check.hash("Key").toQString());
        }
    }
    if (!userSetKeyPassword) {
        keyPassword->setText(parent->getInterface()[path].hash("Key").hash("Password").toQString());
    }

    if (!userSetRequireValid) {
        requireValid->setChecked(parent->getInterface()[path].hash("RequireValid").toBool());
    }
}

void IOInterfaceSSLPage::cleanupPage()
{
    certificateEntry->setText(QString());
    keyEntry->setText(QString());
    keyPassword->setText(QString());
    userSetKeyPassword = false;
    requireValid->setChecked(false);
    userSetRequireValid = false;
}

bool IOInterfaceSSLPage::isComplete() const
{
    return !certificateEntry->text().isEmpty() && !keyEntry->text().isEmpty();
}

Variant::Root IOInterfaceSSLPage::overlayValue() const
{
    Variant::Root result;
    if (!certificateEntry->text().isEmpty()) {
        result[path].hash("Certificate").setString(certificateEntry->text());
    }
    if (!keyEntry->text().isEmpty()) {
        if (!keyPassword->text().isEmpty()) {
            result[path].hash("Key").hash("Key").setString(keyEntry->text());
            result[path].hash("Key").hash("Password").setString(keyPassword->text());
        } else {
            result[path].hash("Key").setString(keyEntry->text());
        }
    }
    if (requireValid->isChecked()) {
        result[path].hash("RequireValid").setBool(true);
    }
    return result;
}

void IOInterfaceSSLPage::showCertificateFileSelectDialog()
{
    QString result(QFileDialog::getOpenFileName(this, tr("Open SSL Certificate"),
                                                certificateEntry->text(),
                                                tr("Certificates (*.pem *.der)")));
    if (result.isEmpty())
        return;
    certificateEntry->setText(result);
}

void IOInterfaceSSLPage::showKeyFileSelectDialog()
{
    QString result(QFileDialog::getOpenFileName(this, tr("Open SSL Key"), keyEntry->text(),
                                                tr("Keys (*.pem *.der *.key)")));
    if (result.isEmpty())
        return;
    keyEntry->setText(result);
}

void IOInterfaceSSLPage::keyPasswordChanged()
{ userSetKeyPassword = true; }

void IOInterfaceSSLPage::requireValidChanged()
{ userSetRequireValid = true; }


IOInterfaceRemoteAccessPage::IOInterfaceRemoteAccessPage(IOInterfaceSelect *p) : QWizardPage(p),
                                                                                 parent(p)
{
    setTitle(tr("Remote Access Configuration"));
    setSubTitle(
            tr("Set permissions to access the interface from outside the machine it is physically located on."));

    QGridLayout *layout = new QGridLayout(this);
    setLayout(layout);

    disableLocal = new QCheckBox(tr("Disable Local &Access"), this);
    disableLocal->setToolTip(tr("If selected then local access to the interface is disabled."));
    disableLocal->setStatusTip(tr("Disable local interface access"));
    disableLocal->setWhatsThis(
            tr("If this is set then the interface will only be accessible through the remote interface."));
    layout->addWidget(disableLocal, 0, 0, 1, -1);
    connect(disableLocal, SIGNAL(toggled(bool)), this, SIGNAL(completeChanged()));

    QFrame *lineFrame = new QFrame(this);
    lineFrame->setFrameStyle(QFrame::HLine | QFrame::Sunken);
    layout->addWidget(lineFrame, 1, 0, 1, -1);

    external = new QCheckBox(tr("&External CPD3"), this);
    external->setToolTip(tr("If set the external CPD3 protocol access is enabled."));
    external->setStatusTip(tr("Enable external CPD3 access"));
    external->setWhatsThis(
            tr("If selected then external access through the CPD3 protocol is enabled.  This permits another remote interface to use the \"Remote CPD3\" option to connect to it."));
    layout->addWidget(external, 2, 0, 1, 2);
    connect(external, SIGNAL(toggled(bool)), this, SIGNAL(completeChanged()));
    connect(external, SIGNAL(toggled(bool)), this, SLOT(setExternal()));

    QLabel *label = new QLabel(tr("&Port:"), this);
    layout->addWidget(label, 2, 2, 1, 1);
    externalPort = new QSpinBox(this);
    externalPort->setToolTip(
            tr("This is the port number to listen for external CPD3 connections on."));
    externalPort->setStatusTip(tr("External port"));
    externalPort->setWhatsThis(
            tr("This sets the port to listen for external CPD3 connections on."));
    externalPort->setRange(1, 65535);
    externalPort->setValue(55444);
    layout->addWidget(externalPort, 2, 3, 1, 1);
    label->setBuddy(externalPort);
    externalPort->setEnabled(false);

    externalSSL = new QCheckBox(tr("&SSL"), this);
    externalSSL->setToolTip(tr("Enable SSL for CPD3 remote connections."));
    externalSSL->setStatusTip(tr("Enable CPD3 SSL"));
    externalSSL->setWhatsThis(tr("If set then the CPD3 connection will use SSL encryption."));
    layout->addWidget(externalSSL, 2, 4, 1, 1);
    externalSSL->setEnabled(false);

    label = new QLabel(tr("L&isten:"), this);
    layout->addWidget(label, 3, 1, 1, 1);
    externalListen = new QLineEdit(this);
    externalListen->setToolTip(
            tr("The address to listen for CPD3 connections on.  Leave empty to listen on all addresses."));
    externalListen->setStatusTip(tr("CPD3 listen address"));
    externalListen->setWhatsThis(
            tr("This is the address to listen for CPD3 connections on.  If left empty then connections from any local address are accepted.  This is used to restrict the network interface being listened to."));
    externalListen->setPlaceholderText(tr("All network interfaces"));
    label->setBuddy(externalListen);
    layout->addWidget(externalListen, 3, 2, 1, -1);
    externalListen->setEnabled(false);

    label = new QLabel(tr("&Accept:"), this);
    layout->addWidget(label, 4, 1, 1, 1);
    externalAccept = new QLineEdit(this);
    externalAccept->setToolTip(
            tr("If not empty this is the list of addresses to accept connections from."));
    externalAccept->setStatusTip(tr("CPD3 address accept"));
    externalAccept->setWhatsThis(
            tr("If this is not empty then it is treated as a space separated list of addresses with optional masks or widths to accept connections from.  If the remote address is not on the list, the connection is closed."));
    externalAccept->setPlaceholderText(tr("All addresses"));
    label->setBuddy(externalAccept);
    layout->addWidget(externalAccept, 4, 2, 1, -1);
    externalAccept->setEnabled(false);

    label = new QLabel(tr("&Reject:"), this);
    layout->addWidget(label, 5, 1, 1, 1);
    externalReject = new QLineEdit(this);
    externalReject->setToolTip(
            tr("If not empty this is the list of addresses to reject connections from."));
    externalReject->setStatusTip(tr("CPD3 address reject"));
    externalReject->setWhatsThis(
            tr("If this is not empty then it is treated as a space separated list of addresses with optional masks or widths to reject connections from.  If the remote address is on the list, the connection is closed."));
    externalReject->setPlaceholderText(tr("No addresses"));
    label->setBuddy(externalReject);
    layout->addWidget(externalReject, 5, 2, 1, -1);
    externalReject->setEnabled(false);


    lineFrame = new QFrame(this);
    lineFrame->setFrameStyle(QFrame::HLine | QFrame::Sunken);
    layout->addWidget(lineFrame, 6, 0, 1, -1);

    layout->setColumnMinimumWidth(0, 10);
    layout->setColumnStretch(3, 1);

    external->setChecked(false);
}

IOInterfaceRemoteAccessPage::~IOInterfaceRemoteAccessPage()
{ }

void IOInterfaceRemoteAccessPage::initializePage()
{
    disableLocal->setChecked(parent->getInterface().read().hash("DisableLocal").toBool());

    auto cfg = parent->getInterface().read().hash("External");
    if (cfg.exists()) {
        external->setChecked(true);

        qint64 port = cfg.hash("Port").toInt64();
        if (INTEGER::defined(port) && port > 0 && port < 65536)
            externalPort->setValue((int) port);

        externalListen->setText(cfg.hash("ListenAddress").toQString());
        externalAccept->setText(formatRestriction(cfg.hash("Accept")));
        externalReject->setText(formatRestriction(cfg.hash("Reject")));
        externalSSL->setChecked(cfg.hash("SSL").exists());
    } else {
        external->setChecked(false);
    }
}

void IOInterfaceRemoteAccessPage::cleanupPage()
{
    disableLocal->setChecked(false);
    external->setChecked(false);
}

bool IOInterfaceRemoteAccessPage::isComplete() const
{
    return !disableLocal->isChecked() || external->isChecked();
}

Variant::Root IOInterfaceRemoteAccessPage::overlayValue() const
{
    Variant::Root result;

    if (disableLocal->isChecked())
        result["DisableLocal"].setBool(true);

    if (external->isChecked()) {
        auto cfg = result["External"];

        cfg.hash("Port").setInt64(externalPort->value());

        QString text(externalListen->text());
        if (!text.isEmpty())
            cfg.hash("ListenAddress").setString(text);
        text = externalAccept->text();
        if (!text.isEmpty()) {
            for (const auto &add : text.split(QRegExp("\\s+"))) {
                cfg.hash("Accept").toArray().after_back().setString(add);
            }
        }
        text = externalReject->text();
        if (!text.isEmpty()) {
            for (const auto &add : text.split(QRegExp("\\s+"))) {
                cfg.hash("Reject").toArray().after_back().setString(add);
            }
        }
    }

    return result;
}

bool IOInterfaceRemoteAccessPage::enableExternalSSL() const
{ return external->isChecked() && externalSSL->isChecked(); }

void IOInterfaceRemoteAccessPage::setExternal()
{
    bool enabled = external->isChecked();
    externalPort->setEnabled(enabled);
    externalListen->setEnabled(enabled);
    externalAccept->setEnabled(enabled);
    externalReject->setEnabled(enabled);
    externalSSL->setEnabled(enabled);
}


IOInterfaceSSLRestrictionPage::IOInterfaceSSLRestrictionPage(IOInterfaceSelect *p,
                                                             const QString &sslPath) : QWizardPage(
        p), parent(p), path(sslPath)
{
    setTitle(tr("SSL Access Restriction"));
    setSubTitle(
            tr("Set the list of certificates that are authorized to connect.  If none are listed then there is no restriction."));

    QGridLayout *layout = new QGridLayout(this);
    setLayout(layout);

    QPushButton *button = new QPushButton(tr("Add certificate"), this);
    button->setToolTip(tr("Add an authorized certificate."));
    button->setStatusTip(tr("Add a certificate"));
    button->setWhatsThis(
            tr("Add an authorized certificate.  If there are any authorized certificates, then clients are required to present on or the connection is dropped."));
    layout->addWidget(button, 0, 1, 1, 1);
    connect(button, SIGNAL(clicked(bool)), this, SLOT(addCertificate()));

    removeButton = new QPushButton(tr("Remove certificate"), this);
    removeButton->setToolTip(tr("Remove the selected authorized certificate."));
    removeButton->setStatusTip(tr("Remove a certificate"));
    removeButton->setWhatsThis(
            tr("Remove the selected certificate from the authorized list.  If there are any authorized certificates, then clients are required to present on or the connection is dropped."));
    layout->addWidget(removeButton, 0, 2, 1, 1);
    connect(removeButton, SIGNAL(clicked(bool)), this, SLOT(removeSelected()));
    removeButton->setEnabled(false);

    list = new QListWidget(this);
    list->setToolTip(tr("This is the list of authorized certificates."));
    list->setStatusTip(tr("Authorized certificates"));
    list->setWhatsThis(
            tr("This is the list of authorized certificates.  If there are any certificates listed, then a client must present a certificate on this list or the connection will be dropped.  If the list is empty then any certificate is accepted."));
    list->setSizePolicy(QSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding));
    layout->addWidget(list, 2, 0, 1, -1);
    list->setSelectionMode(QAbstractItemView::ExtendedSelection);
    connect(list, SIGNAL(itemSelectionChanged()), this, SLOT(selectionChanged()));
}

IOInterfaceSSLRestrictionPage::~IOInterfaceSSLRestrictionPage() = default;

static QString certInfo(const QStringList &input)
{
    if (input.isEmpty())
        return QString();
    return input.first();
}

static QListWidgetItem *constructRestrictItem(const QSslCertificate &cert)
{
    if (cert.isNull())
        return nullptr;
    QListWidgetItem *result = new QListWidgetItem(
            IOInterfaceSSLRestrictionPage::tr("%1 %2 %3", "cert name format").arg(
                    certInfo(cert.subjectInfo(QSslCertificate::Organization)),
                    certInfo(cert.subjectInfo(QSslCertificate::OrganizationalUnitName)),
                    certInfo(cert.subjectInfo(QSslCertificate::CommonName))));
    result->setData(Qt::UserRole, QVariant::fromValue<QSslCertificate>(cert));
    return result;
}

void IOInterfaceSSLRestrictionPage::initializePage()
{
    list->clear();
    removeButton->setEnabled(false);

    auto certList = parent->getInterface()[path].hash("Authorized");
    switch (certList.getType()) {
    case Variant::Type::Hash:
        for (auto add : certList.toHash()) {
            QListWidgetItem
                    *item = constructRestrictItem(Cryptography::getCertificate(add.second, false));
            if (!item)
                continue;
            list->addItem(item);
        }
        break;
    case Variant::Type::Array:
        for (auto add : certList.toArray()) {
            QListWidgetItem *item = constructRestrictItem(Cryptography::getCertificate(add, false));
            if (!item)
                continue;
            list->addItem(item);
        }
        break;
    default: {
        QListWidgetItem
                *item = constructRestrictItem(Cryptography::getCertificate(certList, false));
        if (!item)
            break;
        list->addItem(item);
        break;
    }
    }
}

void IOInterfaceSSLRestrictionPage::cleanupPage()
{
    removeButton->setEnabled(false);
    list->clear();
}

Variant::Root IOInterfaceSSLRestrictionPage::overlayValue() const
{
    Variant::Root result;
    for (int i = 0, max = list->count(); i < max; i++) {
        QSslCertificate cert(list->item(i)->data(Qt::UserRole).value<QSslCertificate>());
        if (cert.isNull())
            continue;
        QString key(Cryptography::sha512(cert).toHex().toLower());
        result["Authorized"].hash(key).setBytes(cert.toDer());
    }
    return result;
}

void IOInterfaceSSLRestrictionPage::selectionChanged()
{
    removeButton->setEnabled(!list->selectedItems().isEmpty());
}

void IOInterfaceSSLRestrictionPage::addCertificate()
{
    QString result(QFileDialog::getOpenFileName(this, tr("Open SSL Certificate"), QString(),
                                                tr("Certificates (*.pem *.der)")));
    if (result.isEmpty())
        return;
    QFile file(result);
    if (!file.open(QIODevice::ReadOnly))
        return;

    QSslCertificate cert(&file, QSsl::Pem);
    file.close();
    if (cert.isNull()) {
        if (file.open(QIODevice::ReadOnly)) {
            cert = QSslCertificate(&file, QSsl::Der);
            file.close();
        }
    }

    QListWidgetItem *item = constructRestrictItem(cert);
    if (item == NULL)
        return;
    list->addItem(item);
}

void IOInterfaceSSLRestrictionPage::removeSelected()
{
    removeButton->setEnabled(false);
    QList<QListWidgetItem *> selected(list->selectedItems());
    for (QList<QListWidgetItem *>::const_iterator remove = selected.constBegin(),
            end = selected.constEnd(); remove != end; ++remove) {
        list->removeItemWidget(*remove);
    }
}

IOInterfaceFinalPage::IOInterfaceFinalPage(IOInterfaceSelect *p) : QWizardPage(p), parent(p)
{
    setTitle(tr("Confirm Interface Settings"));
    setSubTitle(tr("Please verify that the settings below are are correct."));

    QGridLayout *layout = new QGridLayout(this);
    setLayout(layout);

    interfaceText = new QLabel(this);
    interfaceText->setWordWrap(true);
    layout->addWidget(interfaceText, 0, 0, 1, -1);

    remoteAccessText = new QLabel(this);
    remoteAccessText->setWordWrap(true);
    layout->addWidget(remoteAccessText, 1, 0, 1, -1);

    remoteDisableLocal = new QLabel(this);
    remoteDisableLocal->setWordWrap(true);
    layout->addWidget(remoteDisableLocal, 2, 0, 1, -1);
}

IOInterfaceFinalPage::~IOInterfaceFinalPage() = default;

void IOInterfaceFinalPage::initializePage()
{
    auto interface = parent->buildInterface();

    const auto &type = interface["Type"].toString();
    if (Util::equal_insensitive(type, "SerialPort")) {
        QString suffix;

        qint64 i = interface["Baud"].toInt64();
        if (INTEGER::defined(i)) {
            if (!suffix.isEmpty())
                suffix.append(tr(", ", "description suffix delimiter"));
            suffix.append(tr("%1 baud").arg(i));
        }

        i = interface["DataBits"].toInt64();
        if (INTEGER::defined(i)) {
            if (!suffix.isEmpty())
                suffix.append(tr(", ", "description suffix delimiter"));
            suffix.append(tr("%1 data bits").arg(i));
        }

        i = interface["StopBits"].toInt64();
        if (INTEGER::defined(i)) {
            if (!suffix.isEmpty())
                suffix.append(tr(", ", "description suffix delimiter"));
            suffix.append(tr("%n stop bit(s)", "data bits", (int) i));
        }

        {
            const auto &s = interface["Parity"].toString();
            if (Util::equal_insensitive(s, "even")) {
                if (!suffix.isEmpty())
                    suffix.append(tr(", and even parity", "parity non-empty"));
                else
                    suffix.append(tr("even parity", "parity non-empty"));
            } else if (Util::equal_insensitive(s, "odd")) {
                if (!suffix.isEmpty())
                    suffix.append(tr(", and odd parity", "parity non-empty"));
                else
                    suffix.append(tr("odd parity", "parity non-empty"));
            } else if (interface["Parity"].exists()) {
                if (!suffix.isEmpty())
                    suffix.append(tr(", and no parity", "parity non-empty"));
                else
                    suffix.append(tr("no parity", "parity non-empty"));
            }
        }

        QString combined;
        if (!suffix.isEmpty()) {
            combined = tr("The selected interface is the local serial port on %1 with %2.").arg(
                    interface["Port"].toQString(), suffix);
        } else {
            combined = tr("The selected interface is the local serial port on %1.").arg(
                    interface["Port"].toQString());
        }

        if (interface["HardwareFlowControl"].toBool()) {
            combined = tr("%1  Hardware flow control (CTS and RTS) is enabled.").arg(combined);
        }

        {
            const auto &s = interface["SoftwareFlowControl"].toString();
            if (Util::equal_insensitive(s, "xonxoff", "xoffxon", "both")) {
                combined = tr("%1  Software flow control (XON and XOFF) is enabled.").arg(combined);
            } else if (Util::equal_insensitive(s, "xon", "input")) {
                combined = tr("%1  Software flow control (XON only) is enabled.").arg(combined);
            } else if (Util::equal_insensitive(s, "xoff", "output")) {
                combined = tr("%1  Software flow control (XOFF only) is enabled.").arg(combined);
            }
        }
        if (interface["SoftwareFlowControlResume"].toBool()) {
            combined = tr("%1  Software flow control resume (XANY) is enabled.").arg(combined);
        }

        interfaceText->setText(combined);
    } else if (Util::equal_insensitive(type, "RemoteServer")) {
        QString combined(tr("The selected interface is a TCP connection to %2 on port %1.").arg(
                interface["ServerPort"].toInt64()).arg(interface["Server"].toQString()));
        if (interface["SSL"].exists()) {
            combined = tr("%1  SSL authentication is in use.").arg(combined);
        }
        interfaceText->setText(combined);
    } else if (Util::equal_insensitive(type, "CPD3erver")) {
        QString combined
                (tr("The selected interface is a connection to a remote access enabled CPD3 interface on %2, port %1.")
                         .arg(interface["ServerPort"].toInt64())
                         .arg(interface["Server"].toQString()));
        if (interface["SSL"].exists()) {
            combined = tr("%1  SSL authentication is in use.").arg(combined);
        }
        interfaceText->setText(combined);
    } else if (Util::equal_insensitive(type, "TCPListen")) {
        QString combined;
        if (interface["LocalPort"].exists()) {
            combined = tr("The selected interface is a TCP server on port %1.").arg(
                    interface["LocalPort"].toInt64());
        } else {
            combined =
                    tr("The selected interface is a TCP/IP server using an automatically selected port.");
        }
        if (interface["SSL"].exists()) {
            combined = tr("%1  SSL authentication is in use.").arg(combined);
        }
        interfaceText->setText(combined);
    } else if (Util::equal_insensitive(type, "UDP")) {
        QString combined(tr("The selected interface is using UDP datagrams."));
        if (interface["ServerPort"].exists() && !interface["ServerAddress"].toString().empty()) {
            combined =
                    tr("%3  Outgoing data are sent to %2 on port %1.").arg(interface["ServerPort"].toInt64())
                                                                      .arg(interface["ServerAddress"]
                                                                                   .toQString())
                                                                      .arg(combined);
        }
        if (interface["LocalPort"].exists()) {
            combined =
                    tr("%2  Incoming data are accepted from port %1.").arg(interface["LocalPort"].toInt64())
                                                                      .arg(combined);
        }
        interfaceText->setText(combined);
    } else if (Util::equal_insensitive(type, "QtLocalSocket")) {
        interfaceText->setText(tr("The selected interface is the Qt local socket %1.").arg(
                interface["Name"].toQString()));
    } else if (Util::equal_insensitive(type, "QtLocalListen")) {
        interfaceText->setText(tr("The selected interface is the Qt local server %1.").arg(
                interface["Name"].toQString()));
    } else if (Util::equal_insensitive(type, "Command")) {
        QStringList arguments;
        for (auto add : interface["Arguments"].toArray()) {
            arguments.append(add.toQString());
        }

        interfaceText->setText(tr("The selected interface is the command: %1 %2").arg(
                interface["Command"].toQString(), arguments.join(" ")));
    } else if (Util::equal_insensitive(type, "Multiplexer")) {
        interfaceText->setText(
                tr("The interface is configured as a multiplexer for sub-interfaces."));
    } else if (Util::equal_insensitive(type, "LocalSocket")) {
        interfaceText->setText(tr("The selected interface is the Unix domain socket at %1.").arg(
                interface["File"].toQString()));
    } else if (Util::equal_insensitive(type, "LocalListen")) {
        interfaceText->setText(tr("The selected interface is the Unix domain server at %1.").arg(
                interface["File"].toQString()));
    } else if (Util::equal_insensitive(type, "URL")) {
        interfaceText->setText(
                tr("The selected interface is URL acess to %1.").arg(interface["URL"].toQString()));
    } else if (Util::equal_insensitive(type, "Pipe")) {
        QString input(interface["Input"].toQString());
        QString output(interface["Output"].toQString());
        QString echo(interface["Echo"].toQString());
        if (input.isEmpty()) {
            if (output.isEmpty()) {
                if (echo.isEmpty()) {
                    interfaceText->setText(
                            tr("The selected interface a Unix FIFO disconnected from everything (BUG)."));
                } else {
                    interfaceText->setText(
                            tr("The selected interface a Unix FIFO reading echoed command data from %1.")
                                    .arg(echo));
                }
            } else if (echo.isEmpty()) {
                interfaceText->setText(
                        tr("The selected interface a Unix FIFO with output to %1.").arg(output));
            } else {
                interfaceText->setText(
                        tr("The selected interface a Unix FIFO with output to %1 and reading echoed command data from %2.")
                                .arg(output, echo));
            }
        } else {
            if (output.isEmpty()) {
                if (echo.isEmpty()) {
                    interfaceText->setText(
                            tr("The selected interface a Unix FIFO reading from %1.").arg(input));
                } else {
                    interfaceText->setText(tr("The selected interface a Unix FIFO reading data from %1 and echoed command data %2.")
                                                   .arg(input, echo));
                }
            } else if (echo.isEmpty()) {
                interfaceText->setText(tr("The selected interface a Unix FIFO with output to %1 and reading data from %2.")
                                               .arg(output, input));
            } else {
                interfaceText->setText(tr("The selected interface a Unix FIFO with output to %1, reading data from %2, and reading echoed command data from %3.")
                                               .arg(output, input, echo));
            }
        }
    }

    if (interface["External"].exists()) {
        qint64 port = interface["External"].hash("Port").toInt64();

        QString text;
        if (INTEGER::defined(port) && port > 0) {
            text = tr("Remote access is enabled on port %1.").arg(port);
        } else {
            text = tr("Remote access is enabled with automatic port assignment.");
        }
        if (interface["External"].hash("SSL").exists()) {
            text = tr("%1  SSL is enabled for remote access.").arg(text);
        }
        remoteAccessText->setText(text);
        remoteAccessText->show();
    } else {
        remoteAccessText->hide();
    }

    if (interface["DisableLocal"].toBool()) {
        remoteDisableLocal->setText(
                tr("Local access to the interface is disabled, only connections using remote access will be accepted."));
        remoteDisableLocal->show();
    } else {
        remoteDisableLocal->hide();
    }
}

bool IOInterfaceFinalPage::validatePage()
{
    parent->setInterface(parent->buildInterface());
    return QWizardPage::validatePage();
}

}

}
}
}
