/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUIDATAINITIALSETUP_H
#define CPD3GUIDATAINITIALSETUP_H

#include "core/first.hxx"

#include <mutex>
#include <unordered_map>
#include <QtGlobal>
#include <QWizard>
#include <QWizardPage>
#include <QLineEdit>
#include <QComboBox>
#include <QProgressBar>
#include <QLabel>
#include <QSettings>
#include <QFuture>
#include <QFutureWatcher>
#include <QCheckBox>
#include <QTableView>
#include <QPushButton>

#include "guidata/guidata.hxx"
#include "core/number.hxx"
#include "core/actioncomponent.hxx"
#include "datacore/variant/root.hxx"
#include "guidata/valueeditor.hxx"

namespace CPD3 {
namespace GUI {
namespace Data {

namespace Internal {
class InitialSetupModeSelect;

class InitialSetupSyncEnterContact;

class InitialSetupAcquisitionComponents;
}


/**
 * A wizard that performs initial setup for CPD3.
 */
class CPD3GUIDATA_EXPORT InitialSetup : public QWizard {
Q_OBJECT

    enum {
        Page_ModeSelect,

        Page_SetStation_Select,

        Page_Sync_EnterContact,
        Page_Sync_Initialize,
        Page_Sync_AddStation,
        Page_Sync_Processing,

        Page_Acquisition_Station,
        Page_Acquisition_Initialize,
        Page_Acquisition_Components,
        Page_Acquisition_Advanced,
        Page_Acquisition_Processing
    };

    friend class Internal::InitialSetupModeSelect;

    friend class Internal::InitialSetupSyncEnterContact;

    friend class Internal::InitialSetupAcquisitionComponents;

public:
    enum SetupMode {
        Mode_Any, Mode_SetStation, Mode_Synchronize, Mode_Acquisition
    };

    InitialSetup(SetupMode mode = Mode_Any, QWidget *parent = 0, Qt::WindowFlags flags = 0);

    virtual ~InitialSetup();

protected:
    virtual void initializePage(int id);
};


namespace Internal {

class InitialSetupModeSelect : public QWizardPage {
Q_OBJECT

    InitialSetup::SetupMode mode;
public:
    InitialSetupModeSelect(InitialSetup *parent);

    virtual ~InitialSetupModeSelect();

    virtual int nextId() const;

private slots:

    void selectSynchronize();

    void selectAcquisition();

    void selectStation();
};


class InitialSetupSetDefaultStation : public QWizardPage {
Q_OBJECT

    QComboBox *station;

public:
    InitialSetupSetDefaultStation(InitialSetup *parent);

    virtual ~InitialSetupSetDefaultStation();

    virtual void initializePage();

    virtual bool isComplete() const;

    virtual int nextId() const;

private slots:

    void commitSettings();
};


class InitialSetupSyncEnterContact : public QWizardPage {
Q_OBJECT

    QLineEdit *name;
    QLineEdit *email;
    QLineEdit *note;
    bool needToBootstrap;
public:
    InitialSetupSyncEnterContact(InitialSetup *parent);

    virtual ~InitialSetupSyncEnterContact();

    virtual void initializePage();

    int nextId() const override;

private slots:

    void commitSettings();
};

class InitialSetupSyncAddStation : public QWizardPage {
Q_OBJECT

    QComboBox *station;
    QComboBox *profile;
    QLabel *description;
    std::unordered_map<std::string,QString> profileToDescription;

public:
    InitialSetupSyncAddStation(InitialSetup *parent);

    virtual ~InitialSetupSyncAddStation();

    virtual void initializePage();

    virtual bool isComplete() const;

private slots:

    void commitSettings();

    void updateDescription();
};


class InitialSetupProgress : public QWizardPage {
Q_OBJECT

    QSettings settings;

    double start;
    double end;

    ActionFeedback::Serializer incoming;

    QLabel *label;
    QProgressBar *bar;

    std::mutex mutex;
    bool hasBeenAborted;

    void feedbackUpdate();

public:
    InitialSetupProgress(InitialSetup *parent);

    virtual ~InitialSetupProgress();

    virtual void initializePage();

    void setBounds(double start, double end);

    void feedbackAttach(ActionFeedback::Source &source);

    bool isAborted();

    void spinStage(const QString &name, const QString &description);

protected:
    void initialize();

    void finished();

    ActionFeedback::Source feedback;

public slots:

    void abort();

signals:

    void aborted();
};

class InitialSetupSyncInitialize : public InitialSetupProgress {
Q_OBJECT

    enum {
        State_Bootstrap,

        State_Completed, State_Failed,
    } state;

    QFutureWatcher<bool> bootstrap;
public:
    InitialSetupSyncInitialize(InitialSetup *parent);

    virtual ~InitialSetupSyncInitialize();

    virtual void initializePage();

    virtual bool isComplete() const;

    virtual int nextId() const;

signals:

    void aborted();

private slots:

    void updateState();
};

class InitialSetupSyncProcessing : public InitialSetupProgress {
Q_OBJECT

    enum {
        State_Generate, State_Upload, State_WriteLocal,

        State_Completed, State_Failed,
    } state;

    QFutureWatcher<CPD3::Data::Variant::Root> generate;
    QFutureWatcher<bool> upload;
    QFutureWatcher<bool> local;
public:
    InitialSetupSyncProcessing(InitialSetup *parent);

    virtual ~InitialSetupSyncProcessing();

    virtual void initializePage();

    virtual bool isComplete() const;

    virtual int nextId() const;

signals:

    void aborted();

private slots:

    void updateState();
};


class InitialSetupAcquisitionStation : public QWizardPage {
Q_OBJECT

    QComboBox *station;

public:
    InitialSetupAcquisitionStation(InitialSetup *parent);

    virtual ~InitialSetupAcquisitionStation();

    virtual void initializePage();

    virtual bool isComplete() const;

private slots:

    void commitSettings();
};

class InitialSetupAcquisitionInitialize : public InitialSetupProgress {
Q_OBJECT

    enum {
        State_Bootstrap_Global, State_Bootstrap_Station, State_Existing, State_Metadata,

        State_Completed, State_Failed,
    } state;

    QFutureWatcher<bool> bootstrapGlobal;
    QFutureWatcher<bool> bootstrapStation;
    QFutureWatcher<CPD3::Data::Variant::Write> existing;
    QFutureWatcher<CPD3::Data::Variant::Read> metadata;
public:
    InitialSetupAcquisitionInitialize(InitialSetup *parent);

    virtual ~InitialSetupAcquisitionInitialize();

    virtual void initializePage();

    virtual bool isComplete() const;

    virtual int nextId() const;

signals:

    void aborted();

private slots:

    void updateState();
};

class InitialSetupAcquisitionComponentEditorButton : public QPushButton {
Q_OBJECT
public:
    InitialSetupAcquisitionComponentEditorButton(QWidget *parent = 0);

    virtual ~InitialSetupAcquisitionComponentEditorButton();

public slots:

    void showEditor();

signals:

    void commitChanged(QWidget *source);
};

class InitialSetupAcquisitionComponents : public QWizardPage {
Q_OBJECT

    QCheckBox *disableDefaultComponents;
    QCheckBox *showAdvanced;

    QTableView *componentList;

    CPD3::Data::Variant::Write configuration;

    void openAllEditors();

public:
    InitialSetupAcquisitionComponents(InitialSetup *parent);

    virtual ~InitialSetupAcquisitionComponents();

    virtual void initializePage();

    virtual bool isComplete() const;

    virtual int nextId() const;

private slots:

    void commitSettings();

    void addRow();

    void removeRow();
};

class InitialSetupAcquisitionAdvanced : public QWizardPage {
Q_OBJECT

    ValueEditor *editor;

public:
    InitialSetupAcquisitionAdvanced(InitialSetup *parent);

    virtual ~InitialSetupAcquisitionAdvanced();

    virtual void initializePage();
};

class InitialSetupAcquisitionProcessing : public InitialSetupProgress {
Q_OBJECT

    enum {
        State_Write,

        State_Completed, State_Failed,
    } state;

    QFutureWatcher<bool> write;
public:
    InitialSetupAcquisitionProcessing(InitialSetup *parent);

    virtual ~InitialSetupAcquisitionProcessing();

    virtual void initializePage();

    virtual bool isComplete() const;

    virtual int nextId() const;

signals:

    void aborted();

private slots:

    void updateState();
};

}

}
}
}

#endif
