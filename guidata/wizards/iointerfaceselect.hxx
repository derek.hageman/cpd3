/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUIDATAIOINTERFACESELECT_H
#define CPD3GUIDATAIOINTERFACESELECT_H

#include "core/first.hxx"

#include <unordered_map>
#include <QtGlobal>
#include <QWizard>
#include <QWizardPage>
#include <QRadioButton>
#include <QCheckBox>
#include <QComboBox>
#include <QPushButton>
#include <QMenu>
#include <QAction>
#include <QString>
#include <QHash>
#include <QLabel>
#include <QLineEdit>
#include <QSpinBox>
#include <QListWidget>
#include <QTableView>
#include <QStandardItemModel>
#include <QPointer>

#include "guidata/guidata.hxx"
#include "datacore/variant/root.hxx"


namespace CPD3 {
namespace GUI {
namespace Data {

namespace Internal {
class IOInterfaceSelectTypePage;

class IOInterfaceSerialPortAdvancedPage;

class IOInterfaceMultiplexerPage;

class IOInterfaceSSLPage;

class IOInterfaceRemoteAccessPage;

class IOInterfaceFinalPage;

class IOInterfaceSSLRestrictionPage;
}

/**
 * A wizard that allows for selection of an I/O interface.
 */
class CPD3GUIDATA_EXPORT IOInterfaceSelect : public QWizard {
Q_OBJECT

    /**
     * This property holds the current interface configuration.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setInterface(const CPD3::Data::Variant::Read &)
     *  <li> CPD3::Data::Value getInterface() const
     * </ul>
     */
    Q_PROPERTY(CPD3::Data::Variant::Root interface
                       READ getInterface
                       WRITE setInterface
                       DESIGNABLE
                       false
                       USER
                       true)
    CPD3::Data::Variant::Root interface;

    /**
     * This property determines if advanced options are set from the
     * interface when it is selected.  This is used to allow for simple
     * selection of eavesdropper interfaces without defaulting to resetting
     * the advanced options.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setInspectAdvance(const CPD3::Data::Variant::Read &)
     *  <li> bool getInspectAdvanced() const
     * </ul>
     */
    Q_PROPERTY(bool inspectAdvanced
                       READ getInspectAdvanced
                       WRITE setInspectAdvanced
                       DESIGNABLE
                       true
                       USER
                       true)
    bool inspectAdvanced;


    enum {
        Page_TypeSelect,

        Page_LocalSerialPortManual,
        Page_LocalSerialPortAdvanced,

        Page_RemoteServer, Page_RemoteCPD3,

        Page_LocalSocket, Page_Command,

        Page_TCPListen, Page_UDP,

        Page_Multiplexer,

        Page_URL,

#ifdef Q_OS_UNIX
        Page_UnixSocket, Page_UnixFIFO,
#endif

        Page_ListenSSL, Page_ListenSSLRestrict,

        Page_RemoteSSL, Page_RemoteAccess,
        Page_RemoteAccessExternalSSL,
        Page_RemoteAccessExternalSSLRestrict,
        Page_Final,
    };

    Internal::IOInterfaceMultiplexerPage *multiplexerPage;
    Internal::IOInterfaceSelectTypePage *typeSelectPage;
    Internal::IOInterfaceSerialPortAdvancedPage *serialPortAdvancedPage;
    Internal::IOInterfaceSSLPage *listenSSLPage;
    Internal::IOInterfaceSSLRestrictionPage *listenSSLRestrictionPage;
    Internal::IOInterfaceSSLPage *remoteSSLPage;
    Internal::IOInterfaceRemoteAccessPage *remoteAccessPage;
    Internal::IOInterfaceSSLPage *remoteAccessExternalSSLPage;
    Internal::IOInterfaceSSLRestrictionPage *remoteAccessExternalSSLRestrictionPage;

    friend class Internal::IOInterfaceFinalPage;

    CPD3::Data::Variant::Root buildInterface();

public:
    IOInterfaceSelect(QWidget *parent = 0, Qt::WindowFlags flags = 0);

    virtual ~IOInterfaceSelect();

    /**
     * Update the realtime interface data.  This is used to label the local
     * interfaces in the selection list.
     * 
     * @param realtimeInterfaces    the current realtime interfaces
     */
    void updateRealtime(const std::unordered_map<QString,
                                                 CPD3::Data::Variant::Root> &realtimeInterfaces);

    void setInterface(const CPD3::Data::Variant::Read &i);

    const CPD3::Data::Variant::Root &getInterface() const;

    void setInspectAdvanced(bool v);

    bool getInspectAdvanced() const;

protected:
    virtual int nextId() const;
};


namespace Internal {

class IOInterfaceSelectTypePage : public QWizardPage {
Q_OBJECT

    QButtonGroup *typeSelectionGroup;
    QRadioButton *existingButton;
    QRadioButton *localSerialPortButton;

    QComboBox *existingSelectList;
    QComboBox *localSerialPortSelectList;
    QCheckBox *localSerialPortAdvanced;

    QCheckBox *remoteAccess;

    void configureLocalSerialPorts(const std::vector<CPD3::Data::Variant::Read> &existing);

public:
    enum Type {
        Existing = 1,
        LocalSerialPort,
        RemoteServer,
        RemoteCPD3,
        LocalSocket,
        Command,
        TCPListen,
        LocalListen,
        UDP,
        URL,
        Multiplexer,
#ifdef Q_OS_UNIX
        UnixSocket,
        UnixListen,
        UnixFIFO,
#endif
    };

    IOInterfaceSelectTypePage(QWidget *parent);

    Type getType() const;

    bool configureRemoteAccess() const;

    CPD3::Data::Variant::Read existingSelected() const;

    QString localSerialPortSelected() const;

    bool localSerialPortAdvancedSelected() const;

    void setRealtime(const std::unordered_map<QString,
                                              CPD3::Data::Variant::Root> &realtimeInterfaces);

    void selectInterface(const CPD3::Data::Variant::Read &interface, bool checkAdvanced);

private slots:

    void existingStateChanged();

    void localSerialPortStateChanged();
};

class IOInterfaceSerialPortManualPage : public QWizardPage {
Q_OBJECT

    IOInterfaceSelect *parent;

    QLineEdit *portEntry;
public:
    IOInterfaceSerialPortManualPage(IOInterfaceSelect *p);

    virtual ~IOInterfaceSerialPortManualPage();

    virtual void initializePage();

    virtual void cleanupPage();

    virtual bool isComplete() const;

private slots:

    void showFileSelectDialog();
};

class IOInterfaceSerialPortAdvancedPage : public QWizardPage {
Q_OBJECT

    IOInterfaceSelect *parent;

    QComboBox *baudSelect;
    QComboBox *paritySelect;
    QComboBox *dataBitsSelect;
    bool userSetDataBits;
    QComboBox *stopBitsSelect;
    QComboBox *hardwareFlowControl;
    QComboBox *softwareFlowControl;
#ifdef Q_OS_UNIX
    QCheckBox *softwareFlowControlResume;
#endif
    QCheckBox *initialBreak;
public:
    IOInterfaceSerialPortAdvancedPage(IOInterfaceSelect *p);

    virtual ~IOInterfaceSerialPortAdvancedPage();

    virtual void initializePage();

    virtual void cleanupPage();

    CPD3::Data::Variant::Root overlayValue() const;

private slots:

    void paritySet();

    void dataBitsSet();
};

class IOInterfaceRemoteConnectionPage : public QWizardPage {
Q_OBJECT

    IOInterfaceSelect *parent;

    QLineEdit *serverName;
    QSpinBox *serverPort;
    bool userPortSet;
    QCheckBox *enableSSL;
    bool userSSLSet;
public:
    IOInterfaceRemoteConnectionPage(IOInterfaceSelect *p,
                                    const QString &serverFieldName,
                                    const QString &portFieldName,
                                    const QString &sslFieldName = QString());

    virtual ~IOInterfaceRemoteConnectionPage();

    virtual void initializePage();

    virtual void cleanupPage();

    virtual bool isComplete() const;

private slots:

    void portChanged();

    void sslChanged();
};

class IOInterfaceNetworkServerPage : public QWizardPage {
Q_OBJECT

    IOInterfaceSelect *parent;

    QLineEdit *listenAddress;
    QSpinBox *listenPort;
    bool userListenSet;

    QLineEdit *serverAddress;
    QSpinBox *serverPort;
    bool userServerSet;

    QSpinBox *fragment;
    bool userFragmentSet;

    QCheckBox *disableFraming;
    bool userDisableFraming;

    QCheckBox *enableSSL;
    bool userSSLSet;
public:
    IOInterfaceNetworkServerPage(IOInterfaceSelect *p,
                                 const QString &listenAddressFieldName,
                                 const QString &listenPortFieldName,
                                 const QString &serverAddressFieldName = QString(),
                                 const QString &serverPortFieldName = QString(),
                                 const QString &fragmentFieldName = QString(),
                                 const QString &disableFramingFieldName = QString(),
                                 const QString &sslFieldName = QString());

    virtual ~IOInterfaceNetworkServerPage();

    virtual void initializePage();

    virtual void cleanupPage();

    virtual bool isComplete() const;

private slots:

    void listenChanged();

    void serverChanged();

    void fragmentChanged();

    void disableFramingChanged();

    void sslChanged();
};

class IOInterfaceQtLocalSocketPage : public QWizardPage {
Q_OBJECT

    IOInterfaceSelect *parent;

    QLineEdit *socketName;
public:
    IOInterfaceQtLocalSocketPage(IOInterfaceSelect *p);

    virtual ~IOInterfaceQtLocalSocketPage();

    virtual void initializePage();

    virtual void cleanupPage();

    virtual bool isComplete() const;
};

class IOInterfaceCommandPage : public QWizardPage {
Q_OBJECT

    IOInterfaceSelect *parent;

    QLineEdit *commandLine;

    QCheckBox *errorStreamAsEcho;
public:
    IOInterfaceCommandPage(IOInterfaceSelect *p);

    virtual ~IOInterfaceCommandPage();

    virtual void initializePage();

    virtual void cleanupPage();

    virtual bool isComplete() const;
};

class IOInterfaceMultiplexerPage;

class IOInterfaceMultiplexerChannelsButton : public QWidget {
Q_OBJECT

    IOInterfaceMultiplexerPage *page;

    QPushButton *button;
    QMenu *menu;
    QAction *separator;
    QHash<QString, QAction *> channels;

    bool userChanged;
public:
    IOInterfaceMultiplexerChannelsButton
            (const QString &text, IOInterfaceMultiplexerPage *page, QWidget *parent = 0);

    void configure(const QSet<QString> &selected = QSet<QString>(),
                   const QSet<QString> &available = QSet<QString>());

    void configure(const CPD3::Data::Variant::Read &from,
                   const QSet<QString> &available = QSet<QString>());

    void addChannel(const QString &name, bool select = false);

    CPD3::Data::Variant::Root getSelected() const;

    QSet<QString> getAvailable(const CPD3::Data::Variant::Read &from) const;

signals:

    void changed();

private slots:

    void addSelected();

    void selectedChanged();
};

class IOInterfaceMultiplexerPage : public QWizardPage {
Q_OBJECT

    IOInterfaceSelect *parent;

    IOInterfaceMultiplexerChannelsButton *output;
    IOInterfaceMultiplexerChannelsButton *input;
    IOInterfaceMultiplexerChannelsButton *echo;

    QTableView *table;
    QStandardItemModel *model;
    bool userInterfacesChanged;

    friend class IOInterfaceMultiplexerChannelsButton;

    QList<QPointer<IOInterfaceMultiplexerChannelsButton> > channelButtons;

    void addChannel(const QString &name, IOInterfaceMultiplexerChannelsButton *origin);

    void openAllEditors();

public:
    IOInterfaceMultiplexerPage(IOInterfaceSelect *p);

    virtual ~IOInterfaceMultiplexerPage();

    virtual void initializePage();

    virtual void cleanupPage();

    virtual bool isComplete() const;

    CPD3::Data::Variant::Root overlayValue() const;

    QSet<QString> availableChannels() const;

private slots:

    void addRow();

    void removeRow();

    void editRow();

    void setRowChannels();
};

class IOInterfaceURLPage : public QWizardPage {
Q_OBJECT

    IOInterfaceSelect *parent;

    QLineEdit *url;

    QComboBox *methodSelect;
public:
    IOInterfaceURLPage(IOInterfaceSelect *p);

    virtual ~IOInterfaceURLPage();

    virtual void initializePage();

    virtual void cleanupPage();

    virtual bool isComplete() const;
};

#ifdef Q_OS_UNIX

class IOInterfaceUnixSocketPage : public QWizardPage {
Q_OBJECT

    IOInterfaceSelect *parent;

    QLineEdit *socketEntry;
public:
    IOInterfaceUnixSocketPage(IOInterfaceSelect *p);

    virtual ~IOInterfaceUnixSocketPage();

    virtual void initializePage();

    virtual void cleanupPage();

    virtual bool isComplete() const;

private slots:

    void showFileSelectDialog();
};

class IOInterfaceUnixFIFOPage : public QWizardPage {
Q_OBJECT

    IOInterfaceSelect *parent;

    QLineEdit *inputEntry;
    QLineEdit *outputEntry;
    QLineEdit *echoEntry;
public:
    IOInterfaceUnixFIFOPage(IOInterfaceSelect *p);

    virtual ~IOInterfaceUnixFIFOPage();

    virtual void initializePage();

    virtual void cleanupPage();

    virtual bool isComplete() const;

private slots:

    void showInputFileSelectDialog();

    void showOutputFileSelectDialog();

    void showEchoFileSelectDialog();
};

#endif


class IOInterfaceSSLPage : public QWizardPage {
Q_OBJECT

    IOInterfaceSelect *parent;
    QString path;

    QLineEdit *certificateEntry;
    QLineEdit *keyEntry;
    QLineEdit *keyPassword;
    bool userSetKeyPassword;
    QCheckBox *requireValid;
    bool userSetRequireValid;
public:
    IOInterfaceSSLPage(IOInterfaceSelect *p, const QString &sslPath = QString("SSL"));

    virtual ~IOInterfaceSSLPage();

    virtual void initializePage();

    virtual void cleanupPage();

    virtual bool isComplete() const;

    CPD3::Data::Variant::Root overlayValue() const;

private slots:

    void showCertificateFileSelectDialog();

    void showKeyFileSelectDialog();

    void keyPasswordChanged();

    void requireValidChanged();
};


class IOInterfaceRemoteAccessPage : public QWizardPage {
Q_OBJECT

    IOInterfaceSelect *parent;

    QCheckBox *disableLocal;

    QCheckBox *external;
    QSpinBox *externalPort;
    QLineEdit *externalListen;
    QLineEdit *externalAccept;
    QLineEdit *externalReject;
    QCheckBox *externalSSL;
public:
    IOInterfaceRemoteAccessPage(IOInterfaceSelect *p);

    virtual ~IOInterfaceRemoteAccessPage();

    virtual void initializePage();

    virtual void cleanupPage();

    virtual bool isComplete() const;

    CPD3::Data::Variant::Root overlayValue() const;

    bool enableExternalSSL() const;

private slots:

    void setExternal();
};

class IOInterfaceSSLRestrictionPage : public QWizardPage {
Q_OBJECT

    IOInterfaceSelect *parent;
    QString path;

    QListWidget *list;
    QPushButton *removeButton;
public:
    IOInterfaceSSLRestrictionPage(IOInterfaceSelect *p, const QString &sslPath = QString("SSL"));

    virtual ~IOInterfaceSSLRestrictionPage();

    virtual void initializePage();

    virtual void cleanupPage();

    CPD3::Data::Variant::Root overlayValue() const;

private slots:

    void addCertificate();

    void removeSelected();

    void selectionChanged();
};


class IOInterfaceFinalPage : public QWizardPage {
Q_OBJECT

    IOInterfaceSelect *parent;

    QLabel *interfaceText;
    QLabel *remoteAccessText;
    QLabel *remoteDisableLocal;

public:
    IOInterfaceFinalPage(IOInterfaceSelect *p);

    virtual ~IOInterfaceFinalPage();

    virtual void initializePage();

    virtual bool validatePage();
};

}

}
}
}

#endif
