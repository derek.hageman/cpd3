/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3GUIDATAWIZARDS_DISPLAYSETUP_HXX
#define CPD3GUIDATAWIZARDS_DISPLAYSETUP_HXX

#include "core/first.hxx"

#include <QWidget>
#include <QWizard>
#include <QWizardPage>

#include "guidata/guidata.hxx"
#include "datacore/archive/access.hxx"

namespace CPD3 {
namespace GUI {
namespace Data {

/**
 * A wizard that allows for configuring and setup of the main CPX3 displays.
 */
class CPD3GUIDATA_EXPORT DisplaySetup : public QWizard {
Q_OBJECT
    CPD3::Data::SequenceName::Component station;
    QString profile;
    QString mode;
    CPD3::Data::Archive::Access access;
public:
    DisplaySetup(const CPD3::Data::SequenceName::Component &station,
                 const QString &profile,
                 const QString &mode,
                 QWidget *parent = 0,
                 Qt::WindowFlags flags = 0);

    virtual ~DisplaySetup();

    inline const CPD3::Data::SequenceName::Component &getStation() const
    { return station; }

    inline const QString &getProfile() const
    { return profile; }

    inline const QString &getMode() const
    { return mode; }

    inline CPD3::Data::Archive::Access &archive()
    { return access; }

protected:
    void initializePage(int id) override;
};

}
}
}

#endif //CPD3GUIDATAWIZARDS_DISPLAYSETUP_HXX
