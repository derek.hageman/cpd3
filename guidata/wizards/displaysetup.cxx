/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <functional>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QCommandLinkButton>
#include <QProgressDialog>
#include <QProgressBar>
#include <QTableWidget>
#include <QHeaderView>
#include <QInputDialog>
#include <QFormLayout>
#include <QSpinBox>
#include <QComboBox>
#include <QPushButton>
#include <QApplication>
#include <QTableView>
#include <QItemDelegate>
#include <QStyle>

#include "core/threading.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/segment.hxx"
#include "guidata/dataeditor.hxx"
#include "displaysetup.hxx"


namespace CPD3 {
namespace GUI {
namespace Data {

namespace {

enum class Page : int {
    ModeSelect,

    Basic_SelectDisplay, Basic_ConfigureDisplay, Basic_SelectPane, Basic_ConfigurePane,

    Advanced_Editor,

    SaveChanges,
};

static constexpr int archiveWritePriority = 1000000;

class BasePage : public QWizardPage {
    DisplaySetup *parent;
public:
    explicit BasePage(DisplaySetup *parent) : QWizardPage(parent), parent(parent)
    { }

    virtual ~BasePage() = default;

    inline const CPD3::Data::SequenceName::Component &getStation() const
    { return parent->getStation(); }

    inline const QString &getProfile() const
    { return parent->getProfile(); }

    inline const QString &getMode() const
    { return parent->getMode(); }

    inline CPD3::Data::Archive::Access &archive()
    { return parent->archive(); }

    template<typename T>
    inline T *page(Page page) const
    { return static_cast<T *>(parent->page(static_cast<int>(page))); }

    virtual std::function<void(CPD3::Data::Archive::Access &)> applyToArchive() const
    { return {}; }
};

class ModeSelect : public BasePage {

    enum class Mode {
        Basic, Advanced,
    };
    Mode mode;

public:
    explicit ModeSelect(DisplaySetup *parent) : BasePage(parent), mode(Mode::Basic)
    {
        setTitle(tr("Select Setup Mode"));
        setSubTitle(
                tr("Once you have selected a general setup mode, you will be asked for additional information for that mode."));

        auto layout = new QVBoxLayout(this);
        setLayout(layout);

        auto button = new QCommandLinkButton(tr("Basic"),
                                             tr("Select this to configure plots in basic mode.  This will prompt you for a tab and pane and allow for creation and simple changes to plots."),
                                             this);
        connect(button, &QCommandLinkButton::clicked, this, [this]() {
            mode = Mode::Basic;
            wizard()->next();
        });
        layout->addWidget(button);

        button = new QCommandLinkButton(tr("Advanced"),
                                        tr("Select this to display the full advanced display configuration.  This mode allows full editing of the configuration, but requires knowledge of the internal operating process."),
                                        this);
        connect(button, &QCommandLinkButton::clicked, this, [this]() {
            mode = Mode::Advanced;
            wizard()->next();
        });
        layout->addWidget(button);

        layout->addStretch(1);
    }

    virtual ~ModeSelect() = default;

    int nextId() const override
    {
        switch (mode) {
        case Mode::Basic:
            return static_cast<int>(Page::Basic_SelectDisplay);
        case Mode::Advanced:
            return static_cast<int>(Page::Advanced_Editor);
        }
        Q_ASSERT(false);
        return static_cast<int>(Page::Basic_SelectDisplay);
    }
};

class SaveChanges : public BasePage {
    std::future<void> applyResult;
    QProgressBar *bar;

    void setActive()
    {
        setSubTitle(
                tr("The changes to the display configuration are being written to the archive."));

        bar->show();
    }

    void setInactive()
    {
        setSubTitle(
                tr("The changes to the display configuration haven been written to the archive."));

        bar->hide();
    }

    void setNoAction()
    {
        setSubTitle(tr("There are no changes to write to the archive."));

        bar->hide();
    }

public:
    explicit SaveChanges(DisplaySetup *parent) : BasePage(parent)
    {
        setTitle(tr("Saving Changes"));

        auto layout = new QVBoxLayout(this);
        setLayout(layout);

        bar = new QProgressBar(this);
        layout->addWidget(bar);
        bar->setTextVisible(false);
        bar->setMinimum(0);
        bar->setMaximum(0);
        bar->setValue(1);
        bar->setToolTip({});

        layout->addStretch(1);

        setActive();
    }

    virtual ~SaveChanges() = default;

    void initializePage() override
    {
        BasePage::initializePage();

        struct Context {
            std::vector<std::function<void(CPD3::Data::Archive::Access &)>> actions;
        };
        auto context = std::make_shared<Context>();

        for (auto id : wizard()->visitedPages()) {
            auto page = dynamic_cast<BasePage *>(wizard()->page(id));
            if (!page)
                continue;
            auto add = page->applyToArchive();
            if (!add)
                continue;
            context->actions.emplace_back(std::move(add));
        }

        if (context->actions.empty()) {
            applyResult = std::future<void>();
            setNoAction();
            emit completeChanged();
            return;
        }

        applyResult = std::async(std::launch::async, [context, this] {
            for (;;) {
                CPD3::Data::Archive::Access::WriteLock lock(archive());

                for (const auto &apply : context->actions) {
                    apply(archive());
                }

                if (lock.commit())
                    break;
            }
        });
        context.reset();

        setActive();
        Threading::pollFunctor(this, [this]() {
            if (applyResult.wait_for(std::chrono::seconds(0)) == std::future_status::timeout)
                return true;
            setInactive();
            emit completeChanged();
            return false;
        });

        emit completeChanged();
    }

    bool isComplete() const override
    {
        if (!applyResult.valid())
            return true;
        return applyResult.wait_for(std::chrono::seconds(0)) != std::future_status::timeout;
    }

    bool validatePage() override
    {
        if (applyResult.valid())
            applyResult.get();
        return BasePage::validatePage();
    }

    int nextId() const override
    { return -1; }
};

static void runBlockingLoad(QWidget *parent, const std::function<void()> &call)
{
    std::future<void> applyResult = std::async(std::launch::async, call);

    QProgressDialog waitDialog(DisplaySetup::tr("Loading"), QString(), 0, 0, parent);
    waitDialog.setWindowModality(Qt::ApplicationModal);
    waitDialog.setCancelButton(nullptr);

    Threading::pollFunctor(&waitDialog, [&applyResult, &waitDialog]() {
        if (applyResult.wait_for(std::chrono::seconds(0)) == std::future_status::timeout)
            return true;
        waitDialog.close();
        return false;
    });

    if (applyResult.wait_for(std::chrono::seconds(0)) == std::future_status::timeout)
        waitDialog.exec();

    applyResult.wait();
}

class AdvancedEditor : public BasePage {
    DataEditor *editor;
public:
    explicit AdvancedEditor(DisplaySetup *parent) : BasePage(parent)
    {
        setTitle(tr("Display Configuration"));
        setSubTitle(tr("Here you can edit the full configuration for the display system."));

        auto layout = new QVBoxLayout(this);
        setLayout(layout);

        editor = new DataEditor(this);
        layout->addWidget(editor, 1);

        editor->setStation(getStation());
        editor->setArchive("configuration");
        editor->setVariable("displays");
        editor->setSelectionFlags(DataEditor::DisableSelection);
        editor->setModifyFlags(DataEditor::ModifyRestricted);

        connect(editor, &DataEditor::dataModified, this, &AdvancedEditor::completeChanged);
    }

    virtual ~AdvancedEditor() = default;

    void initializePage() override
    {
        BasePage::initializePage();

        CPD3::Data::SequenceValue::Transfer values;
        runBlockingLoad(this, [this, &values] {
            values = archive().readSynchronous(
                    CPD3::Data::Archive::Selection(FP::undefined(), FP::undefined(), {getStation()},
                                                   {"configuration"}, {"displays"}));
        });
        editor->setData(std::move(values));
        emit completeChanged();
    }

    bool isComplete() const override
    {
        return !editor->isPristine();
    }

    int nextId() const override
    { return static_cast<int>(Page::SaveChanges); }

    std::function<void(CPD3::Data::Archive::Access &)> applyToArchive() const override
    {
        if (editor->isPristine())
            return {};
        struct Context {
            CPD3::Data::SequenceValue::Transfer modify;
            CPD3::Data::SequenceValue::Transfer remove;

            inline Context(CPD3::Data::SequenceValue::Transfer modify,
                           CPD3::Data::SequenceValue::Transfer remove) : modify(std::move(modify)),
                                                                         remove(std::move(remove))
            { }
        };
        auto context = std::make_shared<Context>(editor->getNew(), editor->getRemoved());
        return [context](CPD3::Data::Archive::Access &access) {
            access.writeSynchronous(context->modify, context->remove);
        };
    }
};

class BasicSelectDisplay : public BasePage {
    CPD3::Data::Variant::Read existingRoot;
    CPD3::Data::Variant::Path selectedDisplay;

    QTableWidget *existingList;

    void addNew()
    {
        existingList->disconnect(this);
        existingList->clearSelection();
        connect(existingList, &QTableWidget::itemSelectionChanged, this,
                &BasicSelectDisplay::existingSelected);

        auto id = QInputDialog::getText(this, tr("Display Identifier"), tr("ID"));
        if (id.isEmpty())
            return;

        selectedDisplay = {{CPD3::Data::Variant::PathElement::Type::Hash, "Profiles"},
                           {CPD3::Data::Variant::PathElement::Type::Hash, getProfile()},
                           {CPD3::Data::Variant::PathElement::Type::Hash, getMode()},
                           {CPD3::Data::Variant::PathElement::Type::Hash, "Displays"},
                           {CPD3::Data::Variant::PathElement::Type::Hash, id},};

        wizard()->next();
    }

    void existingSelected()
    {
        int row = existingList->currentRow();
        if (row != -1) {
            auto id = existingList->item(row, 0)->data(Qt::UserRole).toString();
            selectedDisplay = {{CPD3::Data::Variant::PathElement::Type::Hash, "Profiles"},
                               {CPD3::Data::Variant::PathElement::Type::Hash, getProfile()},
                               {CPD3::Data::Variant::PathElement::Type::Hash, getMode()},
                               {CPD3::Data::Variant::PathElement::Type::Hash, "Displays"},
                               {CPD3::Data::Variant::PathElement::Type::Hash, id},};
        }

        emit completeChanged();
    }

public:
    explicit BasicSelectDisplay(DisplaySetup *parent) : BasePage(parent),
                                                        existingRoot(
                                                                CPD3::Data::Variant::Read::empty())
    {
        setTitle(tr("Select Display"));
        setSubTitle(tr("Select or add a tab to edit or add graph panes within."));

        auto layout = new QVBoxLayout(this);
        setLayout(layout);

        auto button = new QCommandLinkButton(tr("New"),
                                             tr("Add a new display, instead of modifying an existing one."),
                                             this);
        connect(button, &QCommandLinkButton::clicked, this, &BasicSelectDisplay::addNew);
        layout->addWidget(button);

        existingList = new QTableWidget(this);
        layout->addWidget(existingList, 1);
        existingList->setSelectionMode(QAbstractItemView::SingleSelection);
        existingList->setSelectionBehavior(QAbstractItemView::SelectRows);
        existingList->setColumnCount(3);
        connect(existingList, &QTableWidget::itemSelectionChanged, this,
                &BasicSelectDisplay::existingSelected);

        existingList->verticalHeader()->hide();

        auto item = new QTableWidgetItem(tr("ID"));
        item->setData(Qt::ToolTipRole, tr("The unique ID of the display"));
        item->setData(Qt::WhatsThisRole,
                      tr("This is unique identification of the display in the system.  No two display configurations can share a unqiue ID."));
        existingList->setHorizontalHeaderItem(0, item);

        item = new QTableWidgetItem(tr("Title"));
        item->setData(Qt::ToolTipRole, tr("The display title"));
        item->setData(Qt::WhatsThisRole,
                      tr("This is the title shown for the display.  This appears in the list of tabs at the top of the main window."));
        existingList->setHorizontalHeaderItem(1, item);

        item = new QTableWidgetItem(tr("Order"));
        item->setData(Qt::ToolTipRole, tr("The display order"));
        item->setData(Qt::WhatsThisRole,
                      tr("This is the ordering of the display.  Displays with a lower order are shown more to the left side of the main window."));
        existingList->setHorizontalHeaderItem(2, item);

        existingList->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
        existingList->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
        existingList->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
    }

    virtual ~BasicSelectDisplay() = default;

    void initializePage() override
    {
        BasePage::initializePage();

        CPD3::Data::SequenceSegment active;
        runBlockingLoad(this, [this, &active] {
            auto now = Time::time();
            auto segments = CPD3::Data::SequenceSegment::Stream::read(
                    CPD3::Data::Archive::Selection(now, now + 1, {getStation()}, {"configuration"},
                                                   {"displays"}));
            auto hit = Range::findIntersecting(segments, now);
            if (hit == segments.end())
                return;
            active = std::move(*hit);
        });


        existingRoot = active.value({getStation(), "configuration", "displays"});
        auto existingDisplays =
                existingRoot.hash("Profiles").hash(getProfile()).hash(getMode()).hash("Displays");

        for (int i = existingList->rowCount() - 1; i >= 0; --i) {
            existingList->removeRow(i);
        }
        std::vector<std::string> sorted;
        Util::append(existingDisplays.toHash().keys(), sorted);
        std::sort(sorted.begin(), sorted.end());
        for (const auto &id : sorted) {
            if (id.empty())
                continue;
            auto display = existingDisplays.hash(id);
            if (!display.hash("Layout").exists())
                continue;
            auto title = display.hash("WindowTitle").toDisplayString();
            if (title.isEmpty())
                title = QString::fromStdString(id);

            int row = existingList->rowCount();
            existingList->insertRow(row);

            auto item = new QTableWidgetItem(QString::fromStdString(id));
            item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            item->setData(Qt::UserRole, QString::fromStdString(id));
            item->setData(Qt::ToolTipRole, tr("The unique ID of the display"));
            item->setData(Qt::WhatsThisRole,
                          tr("This is the unique identification of the display within the configuration."));
            existingList->setItem(row, 0, item);

            item = new QTableWidgetItem(title);
            item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            item->setData(Qt::ToolTipRole, tr("The title of the display"));
            item->setData(Qt::WhatsThisRole,
                          tr("This is the title text shown in the list of displays."));
            existingList->setItem(row, 1, item);

            auto priority = display.hash("SortPriority").toInteger();
            if (!INTEGER::defined(priority))
                priority = 0;
            item = new QTableWidgetItem(tr("%1", "order format").arg(static_cast<int>(priority)));
            item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            item->setData(Qt::ToolTipRole, tr("The sort order of the display"));
            item->setData(Qt::WhatsThisRole,
                          tr("This is the sorting order of the display.  Displays with a lower order are shown to the left."));
            existingList->setItem(row, 2, item);
        }
    }

    bool isComplete() const override
    {
        return existingList->currentRow() != -1;
    }

    inline const CPD3::Data::Variant::Path &getSelectedPath() const
    { return selectedDisplay; }

    inline CPD3::Data::Variant::Read getSelectedDisplay() const
    { return existingRoot.getPath(selectedDisplay); }

    inline CPD3::Data::Variant::Read getSetupInfo() const
    { return existingRoot.hash("Setup"); }
};

static void overlayAt(CPD3::Data::Archive::Access &access,
                      const CPD3::Data::SequenceName::Component &station,
                      const CPD3::Data::Variant::Path &path,
                      const CPD3::Data::Variant::Read &value,
                      int priority = archiveWritePriority,
                      bool direct = false)
{
    CPD3::Data::Archive::Selection
            selection(FP::undefined(), FP::undefined(), {station}, {"configuration"}, {"displays"});
    selection.includeDefaultStation = false;
    selection.includeMetaArchive = false;

    CPD3::Data::SequenceValue::Transfer contents;
    {
        CPD3::Data::StreamSink::Iterator incoming;
        access.readStream(selection, &incoming)->detach();
        for (;;) {
            auto add = incoming.all();
            if (add.empty())
                break;
            for (auto &v : add) {
                if (v.getPriority() != priority)
                    continue;
                if (v.getStation() != station)
                    continue;
                if (v.getArchive() != "configuration")
                    continue;
                if (v.getVariable() != "displays")
                    continue;
                contents.emplace_back(std::move(v));
            }
        }
    }
    if (contents.empty()) {
        CPD3::Data::Variant::Root root;
        root.write().getPath(path).set(value);
        contents.emplace_back(CPD3::Data::SequenceIdentity({station, "configuration", "displays"},
                                                           FP::undefined(), FP::undefined(),
                                                           priority), std::move(root));
    } else {
        double finalEnd = contents.front().getEnd();

        for (auto &mod : contents) {
            if (Range::compareEnd(mod.getEnd(), finalEnd) > 0)
                finalEnd = mod.getEnd();

            auto target = mod.write().getPath(path);

            if (direct) {
                target.set(value);
                continue;
            }
            if (!value.exists()) {
                target.remove();
                continue;
            }

            auto output = CPD3::Data::Variant::Root::overlay(CPD3::Data::Variant::Root(target),
                                                             CPD3::Data::Variant::Root(value));
            target.set(output);
        }

        if (FP::defined(contents.front().getStart())) {
            CPD3::Data::Variant::Root root;
            root.write().getPath(path).set(value);
            contents.emplace(contents.begin(),
                             CPD3::Data::SequenceIdentity({station, "configuration", "displays"},
                                                          FP::undefined(),
                                                          contents.front().getStart(), priority),
                             std::move(root));
        }
        if (FP::defined(finalEnd)) {
            Q_ASSERT(Range::compareStart(contents.back().getStart(), finalEnd) <= 0);
            CPD3::Data::Variant::Root root;
            root.write().getPath(path).set(value);
            contents.emplace_back(
                    CPD3::Data::SequenceIdentity({station, "configuration", "displays"}, finalEnd,
                                                 FP::undefined(), priority), std::move(root));
        }
    }

    access.writeSynchronous(contents, true);
}

class BasicConfigureDisplay : public BasePage {
    CPD3::Data::Variant::Root changes;
    QLineEdit *title;
    QSpinBox *sortPriority;
public:
    explicit BasicConfigureDisplay(DisplaySetup *parent) : BasePage(parent)
    {
        setTitle(tr("Configure Display"));
        setSubTitle(tr("Here you can set general parameters for the display."));

        auto layout = new QFormLayout(this);
        setLayout(layout);

        title = new QLineEdit(this);
        layout->addRow(tr("Title:"), title);
        title->setToolTip(tr("The title of the display"));
        title->setStatusTip(tr("Display title"));
        title->setWhatsThis(tr("This is the title text shown in the list of displays."));
        connect(title, &QLineEdit::textEdited, this, &BasicConfigureDisplay::completeChanged);
        connect(title, &QLineEdit::textEdited, this, [this] {
            changes.write().hash("WindowTitle").setString(title->text());
        });

        sortPriority = new QSpinBox(this);
        layout->addRow(tr("Order:"), sortPriority);
        sortPriority->setToolTip(tr("The order of the display, with lower to the left"));
        sortPriority->setStatusTip(tr("Display order"));
        sortPriority->setWhatsThis(
                tr("This is the sort priority of the display.  Lower values result in it being more to the left in the list of displays."));
        sortPriority->setRange(-999999, 999999);
    }

    virtual ~BasicConfigureDisplay() = default;

    void initializePage() override
    {
        BasePage::initializePage();

        auto existing = page<BasicSelectDisplay>(Page::Basic_SelectDisplay)->getSelectedDisplay();

        title->disconnect(this);
        title->setText(existing.hash("WindowTitle").toDisplayString());

        auto priority = existing.hash("SortPriority").toInteger();
        if (!INTEGER::defined(priority))
            priority = 0;
        sortPriority->disconnect(this);
        sortPriority->setValue(static_cast<int>(priority));
        connect(sortPriority, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged), this,
                [this] {
                    changes.write().hash("SortPriority").setInteger(sortPriority->value());
                });
    }

    bool isComplete() const override
    {
        if (title->text().isEmpty())
            return false;

        return BasePage::isComplete();
    }

    std::function<void(CPD3::Data::Archive::Access &)> applyToArchive() const override
    {
        if (!changes.read().exists())
            return {};
        CPD3::Data::Variant::Root set(changes);
        auto target = page<BasicSelectDisplay>(Page::Basic_SelectDisplay)->getSelectedPath();
        auto station = getStation();
        return [set, target, station](CPD3::Data::Archive::Access &access) {
            overlayAt(access, station, target, set);
        };
    }
};

class BasicSelectPane : public BasePage {
    CPD3::Data::Variant::Read existingRoot;
    CPD3::Data::Variant::Path selectedPane;
    std::unordered_set<CPD3::Data::Variant::PathElement> removePanes;
    std::unordered_set<CPD3::Data::Variant::PathElement> addPanes;
    CPD3::Data::Variant::Root paneChanges;

    QComboBox *existingList;
    QPushButton *removeButton;

    void addNew()
    {
        std::unordered_set<CPD3::Data::Variant::PathElement> existing;
        for (int i = 0, end = existingList->count(); i < end; ++i) {
            existing.insert(existingList->itemData(i).value<CPD3::Data::Variant::PathElement>());
        }

        CPD3::Data::Variant::PathElement key;
        for (int unusedIndex = 1; unusedIndex > 0; ++unusedIndex) {
            key = CPD3::Data::Variant::PathElement(CPD3::Data::Variant::PathElement::Type::Hash,
                                                   std::to_string(unusedIndex));
            if (!existing.count(key))
                break;
        }

        removePanes.erase(key);
        addPanes.insert(key);
        existingList->addItem(tr("%1", "initial title").arg(QString::fromStdString(key.toString())),
                              QVariant::fromValue(key));

        selectedPane = page<BasicSelectDisplay>(Page::Basic_SelectDisplay)->getSelectedPath();
        selectedPane.emplace_back(CPD3::Data::Variant::PathElement::Type::Hash, "Layout");
        selectedPane.emplace_back(std::move(key));

        removeButton->setEnabled(true);

        wizard()->next();
    }

    void removeSelected()
    {
        int index = existingList->count();
        if (index == 0)
            return;
        --index;

        auto key = existingList->itemData(index).value<CPD3::Data::Variant::PathElement>();
        addPanes.erase(key);
        removePanes.insert(std::move(key));
        existingList->removeItem(index);
        removeButton->setEnabled(index != 0);
    }

    void existingSelected()
    {
        int index = existingList->currentIndex();
        if (index != -1) {
            selectedPane = page<BasicSelectDisplay>(Page::Basic_SelectDisplay)->getSelectedPath();
            selectedPane.emplace_back(CPD3::Data::Variant::PathElement::Type::Hash, "Layout");
            selectedPane.emplace_back(
                    existingList->currentData().value<CPD3::Data::Variant::PathElement>());
        }

        emit completeChanged();
    }

    void updatePaneSelection()
    {
        auto children = existingRoot.toChildren();
        std::vector<CPD3::Data::Variant::PathElement> sorted;
        for (auto c = children.begin(), endC = children.end(); c != endC; ++c) {
            if (removePanes.count(c.key()))
                continue;
            sorted.emplace_back(c.key());
        }
        std::sort(sorted.begin(), sorted.end(), CPD3::Data::Variant::PathElement::OrderLogical());

        removeButton->setEnabled(!sorted.empty());
        existingList->clear();
        for (auto &key : sorted) {
            auto existing = existingRoot.getPath(key);

            auto title = existing.hash("Title").hash("Text").toDisplayString();
            if (title.isEmpty()) {
                title = tr("%1", "initial title").arg(QString::fromStdString(key.toString()));
            }

            existingList->addItem(title, QVariant::fromValue(key));
        }
    }

public:
    explicit BasicSelectPane(DisplaySetup *parent) : BasePage(parent),
                                                     existingRoot(
                                                             CPD3::Data::Variant::Read::empty())
    {
        setTitle(tr("Select Pane"));
        setSubTitle(tr("Select or add a pane within the tab to edit."));

        auto layout = new QVBoxLayout(this);
        setLayout(layout);

        auto button = new QCommandLinkButton(tr("New"),
                                             tr("Add a new pane, instead of modifying an existing one."),
                                             this);
        connect(button, &QCommandLinkButton::clicked, this, &BasicSelectPane::addNew);
        layout->addWidget(button);

        auto container = new QWidget(this);
        layout->addWidget(container);
        auto line = new QHBoxLayout(container);

        existingList = new QComboBox(container);
        line->addWidget(existingList, 1);
        existingList->setToolTip(tr("Select a pane to edit"));
        existingList->setStatusTip(tr("Select pane"));
        existingList->setWhatsThis(tr("This is the list of existing panes, from top to bottom."));
        connect(existingList,
                static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this,
                &BasicSelectPane::existingSelected);

        removeButton = new QPushButton(QApplication::style()->standardIcon(QStyle::SP_TrashIcon),
                                       QString(), container);
        line->addWidget(removeButton);
        {
            QSizePolicy policy(removeButton->sizePolicy());
            policy.setVerticalPolicy(QSizePolicy::MinimumExpanding);
            removeButton->setSizePolicy(policy);
        }
        removeButton->setContentsMargins(0, 0, 0, 0);
        removeButton->setToolTip(tr("Remove the final pane"));
        removeButton->setStatusTip(tr("Remove pane"));
        removeButton->setWhatsThis(tr("Use this button to remove the final pane."));
        connect(removeButton, &QPushButton::clicked, this, &BasicSelectPane::removeSelected);
        removeButton->setEnabled(false);

        layout->addStretch(1);
    }

    virtual ~BasicSelectPane() = default;

    void initializePage() override
    {
        BasePage::initializePage();

        existingRoot = page<BasicSelectDisplay>(Page::Basic_SelectDisplay)->getSelectedDisplay()
                                                                          .hash("Layout");

        removePanes.clear();
        addPanes.clear();
        paneChanges = CPD3::Data::Variant::Root();

        updatePaneSelection();
    }

    bool isComplete() const override
    {
        return existingList->currentIndex() != -1;
    }

    inline CPD3::Data::Variant::Write getChangeTarget()
    {
        if (selectedPane.empty())
            return CPD3::Data::Variant::Write::empty();
        return paneChanges.write().getPath(selectedPane.back());
    }

    inline CPD3::Data::Variant::Read getSelectedPane() const
    {
        if (selectedPane.empty())
            return CPD3::Data::Variant::Read::empty();
        return existingRoot.getPath(selectedPane.back());
    }

    void selectAnotherPane()
    {
        if (selectedPane.empty())
            return;
        auto updated = CPD3::Data::Variant::Root::overlay(CPD3::Data::Variant::Root(existingRoot),
                                                          CPD3::Data::Variant::Root(paneChanges));
        existingRoot = updated.read();

        updatePaneSelection();
    }

    std::function<void(CPD3::Data::Archive::Access &)> applyToArchive() const override
    {
        CPD3::Data::Variant::Root set = paneChanges;
        for (const auto &rem : removePanes) {
            set.write().getPath(rem).setEmpty();
        }
        std::vector<std::pair<CPD3::Data::Variant::PathElement, std::string>> underlays;
        auto underlayPath = page<BasicSelectDisplay>(Page::Basic_SelectDisplay)->getSetupInfo()
                                                                               .hash("Basic")
                                                                               .hash("NewUnderlay")
                                                                               .toString();
        if (!underlayPath.empty()) {
            for (const auto &add : addPanes) {
                underlays.emplace_back(add, underlayPath);
            }
        }
        if (!set.read().exists() && underlays.empty())
            return {};

        auto target = page<BasicSelectDisplay>(Page::Basic_SelectDisplay)->getSelectedPath();
        target.emplace_back(CPD3::Data::Variant::PathElement::Type::Hash, "Layout");
        auto station = getStation();
        return [set, underlays, target, station](CPD3::Data::Archive::Access &access) {
            for (auto &add : underlays) {
                auto path = target;
                path.emplace_back(std::move(add.first));
                CPD3::Data::Variant::Root under;
                under.write().setOverlay(add.second);
                overlayAt(access, station, path, under, archiveWritePriority - 1000, true);
            }
            overlayAt(access, station, target, set);
        };
    }
};

class BasicConfigurePane : public BasePage {
    QLineEdit *title;

    QTableView *tracesList;

    class TraceListModel : public QAbstractItemModel {
    public:
        enum {
            Column_AddRemove, Column_ID, Column_Variable, Column_Legend,

            Column_TOTAL
        };
    private:
        BasicConfigurePane *page;
        struct TraceData {
            std::string id;
            std::string variable;
            QString legend;
        };
        std::vector<TraceData> traces;
    public:
        explicit TraceListModel(BasicConfigurePane *parent) : QAbstractItemModel(parent),
                                                              page(parent)
        { }

        virtual ~TraceListModel() = default;

        void configure(const CPD3::Data::Variant::Read &v)
        {
            if (!traces.empty())
                removeRows(0, traces.size());
            Q_ASSERT(traces.empty());

            for (auto child : v.hash("Traces").toHash()) {
                if (child.first.empty())
                    continue;
                insertRows(traces.size(), 1);
                Q_ASSERT(!traces.empty());
                auto &target = traces.back();
                target.id = child.first;
                target.variable = child.second
                                       .hash("Data")
                                       .hash("Dimensions")
                                       .array(0)
                                       .hash("Input")
                                       .array(0)
                                       .hash("Variable")
                                       .toString();
                if (target.variable.empty()) {
                    target.variable = child.second
                                           .hash("Data")
                                           .hash("Dimensions")
                                           .array(0)
                                           .hash("Input")
                                           .hash("Variable")
                                           .toString();
                }
                if (target.variable.empty()) {
                    target.variable = child.second
                                           .hash("Data")
                                           .hash("Dimensions")
                                           .array(0)
                                           .hash("Input")
                                           .toString();
                }

                target.legend =
                        child.second.hash("Settings").hash("Legend").hash("Text").toDisplayString();
            }
            emit dataChanged(index(0, 0), index(traces.size() - 1, Column_TOTAL - 1));
        }

        QModelIndex index(int row, int column, const QModelIndex & = QModelIndex()) const override
        { return createIndex(row, column); }

        QModelIndex parent(const QModelIndex &) const override
        { return QModelIndex(); }

        int rowCount(const QModelIndex & = QModelIndex()) const override
        { return static_cast<int>(traces.size() + 1); }

        int columnCount(const QModelIndex & = QModelIndex()) const override
        { return Column_TOTAL; }

        QVariant data(const QModelIndex &index, int role) const override
        {
            if (!index.isValid())
                return {};
            if (index.row() >= static_cast<int>(traces.size())) {
                if (role == Qt::EditRole) {
                    if (index.column() == 0)
                        return 1;
                }
                return {};
            }
            const auto &data = traces[index.row()];

            switch (role) {
            case Qt::DisplayRole:
                switch (index.column()) {
                case Column_ID:
                    return QString::fromStdString(data.id);
                case Column_Variable:
                    return QString::fromStdString(data.variable);
                case Column_Legend:
                    if (data.legend.isEmpty())
                        return tr("Automatic");
                    return data.legend;
                default:
                    break;
                }
                break;

            case Qt::EditRole:
                switch (index.column()) {
                case Column_ID:
                    return {};
                case Column_Variable:
                    return QString::fromStdString(data.variable);
                case Column_Legend:
                    return data.legend;
                default:
                    break;
                }
                break;

            case Qt::ForegroundRole:
                switch (index.column()) {
                case Column_Legend:
                    if (data.legend.isEmpty())
                        return QBrush(Qt::lightGray);
                    break;
                default:
                    break;
                }
                break;

            default:
                break;
            }
            return {};
        }

        QVariant headerData(int section, Qt::Orientation orientation, int role) const override
        {
            if (role != Qt::DisplayRole)
                return {};
            if (orientation == Qt::Vertical)
                return QVariant(section + 1);

            switch (section) {
            case Column_ID:
                return tr("Identifier");
            case Column_Variable:
                return tr("Variable");
            case Column_Legend:
                return tr("Legend");
            default:
                break;
            }
            return QVariant();
        }

        bool setData(const QModelIndex &index, const QVariant &value, int role) override
        {
            if (role != Qt::EditRole)
                return false;

            if (index.row() >= static_cast<int>(traces.size()))
                return false;

            switch (index.column()) {
            case Column_Variable: {
                auto str = value.toString().trimmed().toStdString();
                auto &trace = traces[index.row()];
                trace.variable = str;
                auto target = page->page<BasicSelectPane>(Page::Basic_SelectPane)
                                  ->getChangeTarget()
                                  .hash("Traces")
                                  .hash(trace.id)
                                  .hash("Data")
                                  .hash("Dimensions")
                                  .array(0)
                                  .hash("Input")
                                  .array(0);
                target.hash("Variable").setString(str);
                auto lacksFlavors = target.hash("LacksFlavors");
                lacksFlavors.remove();
                lacksFlavors.array(0).setString(CPD3::Data::SequenceName::flavor_cover);
                lacksFlavors.array(1).setString(CPD3::Data::SequenceName::flavor_end);
                lacksFlavors.array(2).setString(CPD3::Data::SequenceName::flavor_stats);
                emit dataChanged(index, index);
                return true;
            }
            case Column_Legend: {
                auto str = value.toString().trimmed();
                auto &trace = traces[index.row()];
                trace.legend = str;
                auto target = page->page<BasicSelectPane>(Page::Basic_SelectPane)
                                  ->getChangeTarget()
                                  .hash("Traces")
                                  .hash(trace.id)
                                  .hash("Settings")
                                  .hash("Legend")
                                  .hash("Text");
                if (str.isEmpty())
                    target.setEmpty();
                else
                    target.setString(str);
                emit dataChanged(index, index);
                return true;
            }
            default:
                break;
            }

            return false;
        }

        Qt::ItemFlags flags(const QModelIndex &index) const override
        {
            if (!index.isValid())
                return 0;

            switch (index.column()) {
            case Column_ID:
                return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
            case Column_Variable:
            case Column_Legend:
                return Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled;
            default:
                break;
            }

            return 0;
        }

        void applyRemove(int row)
        {
            Q_ASSERT(row < static_cast<int>(traces.size()));

            const auto &trace = traces[row];
            page->page<BasicSelectPane>(Page::Basic_SelectPane)
                ->getChangeTarget()
                .hash("Traces")
                .hash(trace.id)
                .setEmpty();
        }

        bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex()) override
        {
            Q_ASSERT(row + count <= static_cast<int>(traces.size()));

            beginRemoveRows(parent, row, row + count - 1);
            auto first = traces.begin() + row;
            auto last = first + count;
            traces.erase(first, last);
            endRemoveRows();

            return true;
        }

        bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex()) override
        {
            std::unordered_set<std::string> takenIDs;
            for (const auto &c : traces) {
                takenIDs.insert(c.id);
            }

            beginInsertRows(parent, row, row + count - 1);
            int counter = 1;
            for (int i = 0; i < count; i++, row++) {
                std::string id = "All";
                for (; counter > 0 && takenIDs.count(id); ++counter) {
                    id = std::string("New") + std::to_string(counter);
                }
                takenIDs.insert(id);

                TraceData data;
                data.id = id;
                traces.emplace(traces.begin() + row, std::move(data));
                ++row;
            }
            endInsertRows();

            return true;
        }

        void sort(int column, Qt::SortOrder order = Qt::AscendingOrder) override
        {
            beginResetModel();
            switch (column) {
            case Column_ID:
                if (order == Qt::AscendingOrder) {
                    std::stable_sort(traces.begin(), traces.end(),
                                     [](const TraceData &a, const TraceData &b) {
                                         return a.id < b.id;
                                     });
                } else {
                    std::stable_sort(traces.begin(), traces.end(),
                                     [](const TraceData &a, const TraceData &b) {
                                         return b.id < a.id;
                                     });
                }
                break;
            case Column_Variable:
                if (order == Qt::AscendingOrder) {
                    std::stable_sort(traces.begin(), traces.end(),
                                     [](const TraceData &a, const TraceData &b) {
                                         return a.variable < b.variable;
                                     });
                } else {
                    std::stable_sort(traces.begin(), traces.end(),
                                     [](const TraceData &a, const TraceData &b) {
                                         return b.variable < a.variable;
                                     });
                }
                break;
            case Column_Legend:
                if (order == Qt::AscendingOrder) {
                    std::stable_sort(traces.begin(), traces.end(),
                                     [](const TraceData &a, const TraceData &b) {
                                         return a.legend < b.legend;
                                     });
                } else {
                    std::stable_sort(traces.begin(), traces.end(),
                                     [](const TraceData &a, const TraceData &b) {
                                         return b.legend < a.legend;
                                     });
                }
                break;
            default:
                break;
            }
            endResetModel();
        }
    };

    class AddRemoveButton : public QPushButton {
    public:
        explicit AddRemoveButton(const QString &text, QWidget *parent = nullptr) : QPushButton(text,
                                                                                               parent)
        { }

        virtual ~AddRemoveButton() = default;

        QSize sizeHint() const override
        {
            QSize textSize(fontMetrics().size(Qt::TextShowMnemonic, text()));
            QStyleOptionButton opt;
            opt.initFrom(this);
            opt.rect.setSize(textSize);
            return style()->sizeFromContents(QStyle::CT_PushButton, &opt, textSize, this);
        }
    };

    class AddRemoveDelegate : public QItemDelegate {
        BasicConfigurePane *editor;
    public:
        explicit AddRemoveDelegate(BasicConfigurePane *e, QObject *parent = nullptr)
                : QItemDelegate(parent), editor(e)
        { }

        virtual ~AddRemoveDelegate() = default;

        QWidget *createEditor(QWidget *parent,
                              const QStyleOptionViewItem &,
                              const QModelIndex &index) const override
        {
            auto button = new AddRemoveButton(tr("-", "remove text"), parent);
            button->setProperty("model-row", index.row());

            return button;
        }

        void setEditorData(QWidget *editor, const QModelIndex &index) const override
        {
            auto button = static_cast<QPushButton *>(editor);

            button->disconnect(this->editor);

            QVariant data(index.data(Qt::EditRole));
            if (data.toInt() != 1) {
                button->setText(tr("-", "remove text"));
                connect(button, &QPushButton::clicked, this->editor,
                        &BasicConfigurePane::removeRow);
            } else {
                button->setText(tr("Add", "add text"));
                connect(button, &QPushButton::clicked, this->editor, &BasicConfigurePane::addRow);
            }

            button->setProperty("model-row", index.row());
        }

        void setModelData(QWidget *, QAbstractItemModel *, const QModelIndex &) const override
        { }
    };

    class VariableSelectDelegate : public QItemDelegate {
        std::shared_future<CPD3::Data::Archive::Access::SelectedComponents> knownVariables;

        static void applyKnown(QComboBox *box,
                               const CPD3::Data::Archive::Access::SelectedComponents &known)
        {
            QStringList sorted;
            for (const auto &add : known.variables) {
                sorted.append(QString::fromStdString(add));
            }
            std::sort(sorted.begin(), sorted.end());
            for (const auto &add : sorted) {
                box->addItem(add);
            }
        }

    public:
        explicit VariableSelectDelegate(BasicConfigurePane *editor, QObject *parent = nullptr)
                : QItemDelegate(parent),
                  knownVariables(editor->archive()
                                       .availableSelectedFuture(
                                               CPD3::Data::Archive::Selection(FP::undefined(),
                                                                              FP::undefined(),
                                                                              {editor->getStation()},
                                                                              {"raw", "clean"})))
        { }

        virtual ~VariableSelectDelegate() = default;

        QWidget *createEditor(QWidget *parent,
                              const QStyleOptionViewItem &,
                              const QModelIndex &index) const override
        {
            auto box = new QComboBox(parent);

            box->setEditable(true);
            box->setInsertPolicy(QComboBox::NoInsert);

            if (knownVariables.wait_for(std::chrono::seconds(0)) != std::future_status::timeout) {
                applyKnown(box, knownVariables.get());
            } else {
                auto future = knownVariables;
                Threading::pollFunctor(box, [future, box]() {
                    if (future.wait_for(std::chrono::seconds(0)) == std::future_status::timeout)
                        return true;
                    applyKnown(box, future.get());
                    return false;
                });
            }

            return box;
        }

        void setEditorData(QWidget *editor, const QModelIndex &index) const override
        {
            auto box = static_cast<QComboBox *>(editor);
            auto contents = index.data(Qt::EditRole).toString();
            auto idx = box->findText(contents);
            if (idx >= 0)
                box->setCurrentIndex(idx);
            else
                box->setCurrentText(contents);
        }

        void setModelData(QWidget *editor,
                          QAbstractItemModel *model,
                          const QModelIndex &index) const override
        {
            auto box = static_cast<QComboBox *>(editor);
            model->setData(index, box->currentText(), Qt::EditRole);
        }
    };

    void openAllEditors()
    {
        auto model = static_cast<TraceListModel *>(tracesList->model());
        for (int row = 0; row < model->rowCount(QModelIndex()); ++row) {
            tracesList->openPersistentEditor(model->index(row, TraceListModel::Column_AddRemove));
        }
    }

    void removeRow()
    {
        QObject *s = sender();
        if (!s)
            return;
        QVariant p(s->property("model-row"));
        if (!p.isValid())
            return;
        int row = p.toInt();
        auto model = static_cast<TraceListModel *>(tracesList->model());
        if (row < 0 || row >= model->rowCount() - 1)
            return;

        model->applyRemove(row);
        model->removeRows(row, 1);

        openAllEditors();
    }

    void addRow()
    {
        tracesList->model()->insertRows(tracesList->model()->rowCount() - 1, 1);

        openAllEditors();
    }

public:
    explicit BasicConfigurePane(DisplaySetup *parent) : BasePage(parent)
    {
        setTitle(tr("Configure Pane Graph"));
        setSubTitle(tr("Here you can configure the pane graph and its traces."));

        auto layout = new QVBoxLayout(this);
        setLayout(layout);

        auto container = new QWidget(this);
        layout->addWidget(container);
        auto line = new QHBoxLayout(container);

        auto label = new QLabel(tr("Title:"), container);
        line->addWidget(label);
        title = new QLineEdit(this);
        line->addWidget(title, 1);
        label->setBuddy(title);
        title->setToolTip(tr("The title of the pane"));
        title->setStatusTip(tr("Pane title"));
        title->setWhatsThis(tr("This is the title text shown at the top of the pane graph."));
        connect(title, &QLineEdit::textEdited, this, [this] {
            page<BasicSelectPane>(Page::Basic_SelectPane)->getChangeTarget()
                                                         .hash("Title")
                                                         .hash("Text")
                                                         .setString(title->text());
        });

        auto model = new TraceListModel(this);
        tracesList = new QTableView(this);
        layout->addWidget(tracesList, 1);
        tracesList->setModel(model);

        tracesList->setToolTip(tr("The table of trace sets"));
        tracesList->setStatusTip(tr("Trace sets"));
        tracesList->setWhatsThis(
                tr("This table represents all the sets of traces in the graph.  A trace will automatically be created if the variable selection matches multiple inputs."));

        tracesList->horizontalHeader()
                  ->setSectionResizeMode(TraceListModel::Column_AddRemove,
                                         QHeaderView::ResizeToContents);

        tracesList->horizontalHeader()
                  ->setSectionResizeMode(TraceListModel::Column_ID, QHeaderView::ResizeToContents);

        tracesList->horizontalHeader()
                  ->setSectionResizeMode(TraceListModel::Column_Variable,
                                         QHeaderView::ResizeToContents);
        tracesList->setItemDelegateForColumn(TraceListModel::Column_Variable,
                                             new VariableSelectDelegate(this, tracesList));

        tracesList->horizontalHeader()
                  ->setSectionResizeMode(TraceListModel::Column_Legend, QHeaderView::Stretch);

        tracesList->setItemDelegateForColumn(TraceListModel::Column_AddRemove,
                                             new AddRemoveDelegate(this, tracesList));

        tracesList->setSortingEnabled(true);
        tracesList->sortByColumn(TraceListModel::Column_ID, Qt::AscendingOrder);


        tracesList->setSpan(model->rowCount(QModelIndex()) - 1, 0, 1, TraceListModel::Column_TOTAL);

        openAllEditors();

        auto button = new QCommandLinkButton(tr("Select another pane"),
                                             tr("Select or add another pane to the display tab."),
                                             this);
        connect(button, &QCommandLinkButton::clicked, this, [this] {
            page<BasicSelectPane>(Page::Basic_SelectPane)->selectAnotherPane();
            wizard()->back();
        });
        layout->addWidget(button);
    }

    virtual ~BasicConfigurePane() = default;

    void initializePage() override
    {
        BasePage::initializePage();

        auto existing = page<BasicSelectPane>(Page::Basic_SelectPane)->getSelectedPane();
        title->setText(existing.hash("Title").hash("Text").toDisplayString());

        auto model = static_cast<TraceListModel *>(tracesList->model());
        model->configure(existing);

        openAllEditors();
    }

    bool isComplete() const override
    {
        auto model = static_cast<TraceListModel *>(tracesList->model());
        if (model->rowCount() == 0)
            return false;

        return BasePage::isComplete();
    }

    int nextId() const override
    { return static_cast<int>(Page::SaveChanges); }
};

}

DisplaySetup::DisplaySetup(const CPD3::Data::SequenceName::Component &station,
                           const QString &profile,
                           const QString &mode,
                           QWidget *parent, Qt::WindowFlags flags) : QWizard(parent, flags),
                                                                     station(station),
                                                                     profile(profile),
                                                                     mode(mode)
{
    setWindowTitle(tr("CPD3 Display Setup"));

    setPage(static_cast<int>(Page::ModeSelect), new ModeSelect(this));

    setPage(static_cast<int>(Page::Basic_SelectDisplay), new BasicSelectDisplay(this));
    setPage(static_cast<int>(Page::Basic_ConfigureDisplay), new BasicConfigureDisplay(this));
    setPage(static_cast<int>(Page::Basic_SelectPane), new BasicSelectPane(this));
    setPage(static_cast<int>(Page::Basic_ConfigurePane), new BasicConfigurePane(this));

    setPage(static_cast<int>(Page::Advanced_Editor), new AdvancedEditor(this));

    setPage(static_cast<int>(Page::SaveChanges), new SaveChanges(this));

    setOptions(options() | QWizard::NoBackButtonOnLastPage);
}

DisplaySetup::~DisplaySetup() = default;

void DisplaySetup::initializePage(int id)
{
    QWizard::initializePage(id);
    switch (static_cast<Page>(id)) {
    case Page::ModeSelect:
        setButtonLayout({QWizard::Stretch, QWizard::BackButton, QWizard::CancelButton});
        break;
    case Page::SaveChanges:
        setButtonLayout({QWizard::Stretch, QWizard::FinishButton});
        break;
    default:
        setButtonLayout(
                {QWizard::Stretch, QWizard::BackButton, QWizard::NextButton, QWizard::CancelButton,
                 QWizard::FinishButton});
        break;
    }
}

}
}
}