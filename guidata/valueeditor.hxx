/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUIDATAVALUEEDITOR_H
#define CPD3GUIDATAVALUEEDITOR_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QWidget>
#include <QTextEdit>
#include <QTextDocument>
#include <QTextObjectInterface>
#include <QTextCharFormat>
#include <QTextCursor>
#include <QTimer>
#include <QDialog>
#include <QPlainTextEdit>
#include <QComboBox>
#include <QCheckBox>
#include <QLineEdit>
#include <QPlainTextEdit>
#include <QPushButton>
#include <QLabel>

#include "guidata/guidata.hxx"
#include "core/number.hxx"
#include "datacore/variant/root.hxx"
#include "guidata/valueeditor.hxx"
#include "guicore/setselect.hxx"

class QHexEdit;

namespace CPD3 {
namespace GUI {
namespace Data {

class ValueEditor;

class ValueEndpointEditor;

namespace Internal {
class ValueEditorText : public QTextEdit {
Q_OBJECT

    ValueEditor *parent;

    bool insertPromptNamed(const std::vector<CPD3::Data::Variant::Read> &values,
                           CPD3::Data::Variant::Write &parent,
                           const CPD3::Data::Variant::Read &metadata);

    inline bool insertPromptNamed(const CPD3::Data::Variant::Read &value,
                                  CPD3::Data::Variant::Write &parent,
                                  const CPD3::Data::Variant::Read &metadata)
    {
        return insertPromptNamed(std::vector<CPD3::Data::Variant::Read>{value}, parent, metadata);
    }

    void updateInserted(const std::vector<CPD3::Data::Variant::Path> &paths);

    inline void updateInserted(const CPD3::Data::Variant::Path &path)
    {
        updateInserted(std::vector<CPD3::Data::Variant::Path>{path});
    }

    bool metadataHashPromptPossible(CPD3::Data::Variant::Write &parent,
                                    const CPD3::Data::Variant::Read &metadata);

public:
    ValueEditorText(ValueEditor *parent);

    virtual ~ValueEditorText();

protected:
    virtual void mouseReleaseEvent(QMouseEvent *event);

    virtual void contextMenuEvent(QContextMenuEvent *event);

    virtual void focusOutEvent(QFocusEvent *e);

    virtual bool event(QEvent *event);

    void insertFromMimeData(const QMimeData *source) override;

private slots:

    void contextMenuEdit();

    void contextMenuExpand();

    void contextMenuAdd();

    void contextMenuDuplicate();

    void contextMenuImport();

    void contextMenuExport();

    void contextMenuCopyValue();

    void contextMenuPasteValue();
};

class ValueEditorDocument : public QTextDocument {
Q_OBJECT

    ValueEditor *parent;
public:
    ValueEditorDocument(ValueEditor *parent);

    virtual ~ValueEditorDocument();
};

class ValueEditorCollapsedObject : public QObject, public QTextObjectInterface {
Q_OBJECT
    Q_INTERFACES(QTextObjectInterface)

    ValueEditor *parent;
public:
    ValueEditorCollapsedObject(ValueEditor *parent);

    virtual ~ValueEditorCollapsedObject();

    virtual void drawObject(QPainter *painter,
                            const QRectF &rect,
                            QTextDocument *doc,
                            int posInDocument,
                            const QTextFormat &format);

    virtual QSizeF intrinsicSize(QTextDocument *doc, int posInDocument, const QTextFormat &format);
};

}


/**
 * A widget that provides a dynamic editor for values.
 */
class CPD3GUIDATA_EXPORT ValueEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;
    CPD3::Data::Variant::Read metadata;

    Internal::ValueEditorDocument *document;
    Internal::ValueEditorText *editor;
    Internal::ValueEditorCollapsedObject *collapsed;

    QTimer reparseTimer;

    QTextCharFormat formatNormal;
    QTextCharFormat formatOutsideMetadata;
    QTextCharFormat formatValueProblem;
    QTextCharFormat formatCollapsed;

    QSet<QString> expandedPaths;

    QList<ValueEndpointEditor *> editors;

    friend class Internal::ValueEditorText;

    QTextCharFormat getPathFormat(const CPD3::Data::Variant::Read &value,
                                  const CPD3::Data::Variant::Read &metadata);

    QTextCharFormat getValueFormat(const CPD3::Data::Variant::Read &value,
                                   const CPD3::Data::Variant::Read &metadata,
                                   bool ambiguous = false);

    bool insertCollapsed(QTextCursor &cursor,
                         CPD3::Data::Variant::Write &value,
                         const CPD3::Data::Variant::Read &metadata);

    void insertValue(QTextCursor &cursor,
                     CPD3::Data::Variant::Write &value,
                     const CPD3::Data::Variant::Read &metadata,
                     int collapseDepth = 0,
                     bool arbitraryChildren = false);

    void expandValue(const CPD3::Data::Variant::Read &value);

    void rebuildContents();

    void regenerateAndSelect(const std::vector<CPD3::Data::Variant::Path> &select);

    void showValueEditor(CPD3::Data::Variant::Write value,
                         const CPD3::Data::Variant::Read &metadata);

    QString getToolTip(const CPD3::Data::Variant::Read &value,
                       const CPD3::Data::Variant::Read &metadata);

    void beginProgrammatic();

    void endProgrammatic();

public:

    ValueEditor(QWidget *parent = 0);

    virtual ~ValueEditor();

    CPD3::Data::Variant::Path getCursorPath() const;

    void moveCursor(const CPD3::Data::Variant::Path &path);

    void setReadOnly(bool readOnly);

    inline void setValue(CPD3::Data::Variant::Write &&value,
                         const CPD3::Data::Variant::Read &metadata = CPD3::Data::Variant::Read::empty())
    { return setValue(value, metadata); }

signals:

    /**
     * Emitted whenever the value is changed by user action.
     * 
     * @param value     the value (after change)
     */
    void valueChanged();

public slots:

    /**
     * Set the value the editor is manipulating.
     *
     * @param value     the value
     * @param metadata  the base metadata if any
     */
    void setValue(CPD3::Data::Variant::Write &value,
                  const CPD3::Data::Variant::Read &metadata = CPD3::Data::Variant::Read::empty());

private slots:

    void reparse();

};

/**
 * The base class that value endpoint editors are derived from.
 */
class CPD3GUIDATA_EXPORT ValueEndpointEditor {
public:
    ValueEndpointEditor();

    virtual ~ValueEndpointEditor();

    /**
     * Test if the editor applies to a given value and metadata
     *
     * @param value     the value being examined
     * @param metadata  the metadata for the value
     * @return          true if the editor is applicable
     */
    virtual bool matches(const CPD3::Data::Variant::Read &value,
                         const CPD3::Data::Variant::Read &metadata) = 0;

    /**
     * Test if a given value has an acceptable value.
     *
     * @param value     the value being examined
     * @param metadata  the metadata for the value
     * @return          true if the value is valid
     */
    virtual bool valid(const CPD3::Data::Variant::Read &value,
                       const CPD3::Data::Variant::Read &metadata);

    /**
     * Perform interactive editing for a value.  This normally opens a modal dialog.
     *
     * @param value     the value to be edited
     * @param metadata  the metadata for the value
     * @param parent    the parent widget of any editor
     * @return          true if the value was changed by editing
     */
    virtual bool edit(CPD3::Data::Variant::Write &value, const CPD3::Data::Variant::Read &metadata,
                      QWidget *parent = 0) = 0;

    /**
     * Get the tool tip of the value.
     *
     * @param value     the value being examined
     * @param metadata  the metadata for the value
     * @return          a description of the value
     */
    virtual QString toolTip(const CPD3::Data::Variant::Read &value,
                            const CPD3::Data::Variant::Read &metadata);

    /**
     * Get the collapsed text for a value.
     *
     * @param value     the value being examined
     * @param metadata  the metadata for the value
     * @return          the collapsed text or empty for the default
     */
    virtual QString collapsedText(const CPD3::Data::Variant::Read &value,
                                  const CPD3::Data::Variant::Read &metadata);
};

namespace Internal {

class ValueEndpointPrimitiveEditorDialog : public QDialog {
Q_OBJECT

    CPD3::Data::Variant::Write value;
    CPD3::Data::Variant::Read metadata;

    QComboBox *type;
    QComboBox *advancedType;
    QLineEdit *singleEditor;
    QCheckBox *toggle;
    SetSelector *flagsSelect;
    QPlainTextEdit *multipleEdit;
    QPushButton *localeSelect;
    QHexEdit *hexEdit;

    QWidget *filler;

    std::unordered_map<QString, QString> localizedStrings;

    CPD3::Data::Variant::Type getSelectedType() const;

    void rebuildLocaleMenu(const QString &add = QString());

public:
    ValueEndpointPrimitiveEditorDialog(CPD3::Data::Variant::Write &value,
                                       const CPD3::Data::Variant::Read &metadata,
                                       QWidget *parent = 0);

private slots:

    void typeChanged();

    void localeTextEdited();

    void localeSelected();

    void addLocale();

    void removeLocale(QAction *action);

    void singleTextChanged();

    void toggleChanged();

    void acceptChanges();
};

class ValueEditorAddChildSelection : public QDialog {
Q_OBJECT
    CPD3::Data::Variant::Write parent;
    std::unordered_map<std::string, CPD3::Data::Variant::Read> children;
    CPD3::Data::Variant::Read fallback;

    CPD3::Data::Variant::Write inserted;

    QComboBox *childSelect;
    QLineEdit *other;
    QLabel *description;

public:
    ValueEditorAddChildSelection(const std::unordered_map<std::string,
                                                          CPD3::Data::Variant::Read> &children,
                                 const CPD3::Data::Variant::Read &fallback,
                                 CPD3::Data::Variant::Write &parent,
                                 QWidget *wp = 0);

    inline CPD3::Data::Variant::Write getInserted() const
    { return inserted; }

private slots:

    void selectionChanged();

    void acceptAdd();
};

}

}
}
}

#endif
