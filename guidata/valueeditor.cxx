/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <functional>
#include <QApplication>
#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QTextCursor>
#include <QPainter>
#include <QColor>
#include <QMenu>
#include <QTextBlock>
#include <QTextLayout>
#include <QDialogButtonBox>
#include <QLabel>
#include <QButtonGroup>
#include <QInputDialog>
#include <QFileDialog>
#include <QMessageBox>
#include <QThread>
#include <QToolTip>

#include "qhexedit.h"

#include "datacore/variant/root.hxx"
#include "datacore/variant/parse.hxx"
#include "datacore/externalsink.hxx"
#include "guidata/valueeditor.hxx"
#include "guidata/valueclipboard.hxx"
#include "guicore/exponentedit.hxx"

#include "guidata/editors/enumeration.hxx"
#include "guidata/editors/calibration.hxx"
#include "guidata/editors/timeinterval.hxx"
#include "guidata/editors/dynamictimeinterval.hxx"
#include "guidata/editors/baselinesmoother.hxx"
#include "guidata/editors/sequenceselection.hxx"
#include "guidata/editors/dynamicsequenceselection.hxx"
#include "guidata/editors/dynamicinput.hxx"
#include "guidata/editors/graphing.hxx"
#include "guidata/editors/editaction.hxx"
#include "guidata/editors/edittrigger.hxx"
#include "guidata/editors/acquisition/component.hxx"
#include "guidata/editors/acquisition/variablegroups.hxx"

using namespace CPD3::Data;
using namespace CPD3::GUI::Data::Internal;

namespace CPD3 {
namespace GUI {
namespace Data {

/** @file guidata/valueditor.hxx
 * Provides utilities and interfaces to edit Values.
 */

enum {
    Type_Collapsed = QTextFormat::UserObject,
};

enum {
    Property_Value = QTextFormat::UserProperty, Property_Metadata, Property_CollapsedText
};


namespace {
class ValueEndpointPrimitiveEditor : public ValueEndpointEditor {
public:
    ValueEndpointPrimitiveEditor() = default;

    virtual ~ValueEndpointPrimitiveEditor() = default;

    bool matches(const Variant::Read &value, const Variant::Read &metadata) override
    {
        Q_UNUSED(value);
        Q_UNUSED(metadata);
        return true;
    }

    bool valid(const Variant::Read &value, const Variant::Read &metadata) override
    {
        if (!metadata.exists())
            return true;

        auto editor = metadata.metadata("Editor");

        switch (value.getType()) {
        case Variant::Type::Real: {
            if (metadata.getType() != Variant::Type::MetadataReal)
                return false;
            double v = value.toDouble();
            if (!FP::defined(v)) {
                if (editor["AllowUndefined"].exists() && !editor["AllowUndefined"].toBool())
                    return false;
                break;
            }

            double bound = editor["Minimum"].toDouble();
            if (FP::defined(bound)) {
                if (editor["MinimumExclusive"].toBool()) {
                    if (v <= bound)
                        return false;
                } else {
                    if (v < bound)
                        return false;
                }
            }

            bound = editor["Maximum"].toDouble();
            if (FP::defined(bound)) {
                if (editor["MaximumExclusive"].toBool()) {
                    if (v >= bound)
                        return false;
                } else {
                    if (v > bound)
                        return false;
                }
            }

            break;
        }
        case Variant::Type::Integer: {
            if (metadata.getType() != Variant::Type::MetadataInteger)
                return false;

            qint64 v = value.toInt64();
            if (!INTEGER::defined(v)) {
                if (editor["AllowUndefined"].exists() && !editor["AllowUndefined"].toBool())
                    return false;
                break;
            }

            qint64 bound = editor["Minimum"].toInt64();
            if (INTEGER::defined(bound) && v < bound)
                return false;

            bound = editor["Maximum"].toInt64();
            if (INTEGER::defined(bound) && v > bound)
                return false;

            break;
        }
        case Variant::Type::Boolean: {
            if (metadata.getType() != Variant::Type::MetadataBoolean)
                return false;
            break;
        }
        case Variant::Type::String: {
            if (metadata.getType() != Variant::Type::MetadataString)
                return false;
            break;
        }
        case Variant::Type::Bytes: {
            if (metadata.getType() != Variant::Type::MetadataBytes)
                return false;
            break;
        }
        case Variant::Type::Flags: {
            if (metadata.getType() != Variant::Type::MetadataFlags)
                return false;
            if (!editor["AllowArbitrary"].toBool()) {
                for (const auto &check : value.toFlags()) {
                    if (!metadata.metadataSingleFlag(check).exists())
                        return false;
                }
            }
            break;
        }
        case Variant::Type::Array: {
            if (metadata.getType() != Variant::Type::MetadataArray)
                return false;
            break;
        }
        case Variant::Type::Matrix: {
            if (metadata.getType() != Variant::Type::MetadataMatrix)
                return false;
            break;
        }
        case Variant::Type::Hash: {
            if (metadata.getType() != Variant::Type::MetadataHash)
                return false;
            break;
        }
        case Variant::Type::Keyframe: {
            if (metadata.getType() != Variant::Type::MetadataKeyframe)
                return false;
            break;
        }

        case Variant::Type::Overlay:
            break;

        case Variant::Type::Empty:
            break;

        case Variant::Type::MetadataReal:
        case Variant::Type::MetadataInteger:
        case Variant::Type::MetadataBoolean:
        case Variant::Type::MetadataString:
        case Variant::Type::MetadataBytes:
        case Variant::Type::MetadataFlags:
        case Variant::Type::MetadataArray:
        case Variant::Type::MetadataMatrix:
        case Variant::Type::MetadataHash:
        case Variant::Type::MetadataKeyframe:
            break;
        }

        return true;
    }

    bool edit(Variant::Write &value, const Variant::Read &metadata, QWidget *parent) override
    {
        ValueEndpointPrimitiveEditorDialog dialog(value, metadata, parent);
        return dialog.exec() == QDialog::Accepted;
    }
};
}


ValueEditor::ValueEditor(QWidget *parent) : QWidget(parent)
{
    QVBoxLayout *layout = new QVBoxLayout(this);
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    document = new ValueEditorDocument(this);
    editor = new ValueEditorText(this);
    layout->addWidget(editor);
    editor->setDocument(document);
    editor->setWordWrapMode(QTextOption::NoWrap);

    reparseTimer.setSingleShot(true);
    reparseTimer.setInterval(250);

    collapsed = new ValueEditorCollapsedObject(this);
    document->documentLayout()->registerHandler(QTextFormat::UserObject, collapsed);

    {
        QFont font(formatNormal.font());
        font.setFamily("Monospace");
        font.setStyleHint(QFont::TypeWriter);
        formatNormal.setFont(font);
    }
    editor->setCurrentCharFormat(formatNormal);

    formatOutsideMetadata.setFont(formatNormal.font());
    formatOutsideMetadata.setForeground(QColor(192, 0, 0));

    formatValueProblem.setFont(formatNormal.font());
    formatValueProblem.setForeground(QColor(192, 0, 0));

    {
        QFont font(formatNormal.font());
        font.setUnderline(true);
        formatCollapsed.setFont(font);
    }
    formatCollapsed.setForeground(QColor(Qt::blue));
    formatCollapsed.setObjectType(Type_Collapsed);


    editors.append(new EnumerationEndpoint);
    editors.append(new CalibrationEndpoint);
    editors.append(new TimeIntervalEndpoint);
    editors.append(new DynamicTimeIntervalEndpoint);
    editors.append(new BaselineSmootherEndpoint);
    editors.append(new SequenceSelectionEndpoint);
    editors.append(new DynamicSequenceSelectionEndpoint);
    editors.append(new DynamicInputEndpoint);
    editors.append(new GraphingAxisTransformerBoundEndpoint);
    editors.append(new AcquisitionComponentEndpoint);
    editors.append(new AcquisitionVariableGroupsEndpoint);
    editors.append(new EditActionEndpoint);
    editors.append(new EditTriggerEndpoint);

    editors.append(new ValueEndpointPrimitiveEditor);

    endProgrammatic();
}

ValueEditor::~ValueEditor()
{
    qDeleteAll(editors);
}

void ValueEditor::beginProgrammatic()
{
    document->disconnect(&reparseTimer);
    reparseTimer.disconnect(this);
    reparseTimer.stop();
}

void ValueEditor::endProgrammatic()
{
    connect(document, SIGNAL(contentsChange(int, int, int)), &reparseTimer, SLOT(start()));
    connect(&reparseTimer, SIGNAL(timeout()), this, SLOT(reparse()));
}

QTextCharFormat ValueEditor::getPathFormat(const Variant::Read &value,
                                           const Variant::Read &metadata)
{
    if (this->metadata.exists() && !metadata.exists())
        return formatOutsideMetadata;

    return formatNormal;
}

QTextCharFormat ValueEditor::getValueFormat(const Variant::Read &value,
                                            const Variant::Read &metadata,
                                            bool ambiguous)
{
    if (metadata.metadata("ArbitraryStructure").toBool())
        return formatNormal;

    if (ambiguous)
        return formatValueProblem;

    for (QList<ValueEndpointEditor *>::const_iterator e = editors.constBegin(),
            endE = editors.constEnd(); e != endE; ++e) {
        if (!(*e)->matches(value, metadata))
            continue;

        if ((*e)->valid(value, metadata))
            break;

        return formatValueProblem;
    }

    return formatNormal;
}

namespace {
class BlockFormatter {
    QTextLayout *layout;
#if QT_VERSION >= QT_VERSION_CHECK(5, 6, 0)
    QVector<QTextLayout::FormatRange> ranges;
#else
    QList<QTextLayout::FormatRange> ranges;
#endif
public:
    explicit BlockFormatter(const QTextCursor &cursor) : layout(cursor.block().layout()), ranges()
    { }

    ~BlockFormatter()
    {
#if QT_VERSION >= QT_VERSION_CHECK(5, 6, 0)
        layout->setFormats(ranges);
#else
        layout->setAdditionalFormats(ranges);
#endif
    }

    void apply(int start, int length, const QTextCharFormat &format)
    {
        QTextLayout::FormatRange r;
        r.start = start;
        r.length = length;
        r.format = format;
        ranges.append(r);
    }
};
}

static QString toPath(const Variant::Path &input)
{
    QString path;
    for (const auto &add : input) {
        path += '/';
        std::string pathString = add.toString();
        Util::replace_all(pathString, ",", "\\,");
        path += QString::fromStdString(pathString);
    }
    if (path.isEmpty())
        path = "/";
    return path;
}

bool ValueEditor::insertCollapsed(QTextCursor &cursor,
                                  Variant::Write &value,
                                  const Variant::Read &metadata)
{
    if (!metadata.metadata("Editor").hash("DefaultCollapsed").toBool())
        return false;

    {
        auto path = toPath(value.currentPath());
        if (expandedPaths.contains(path))
            return false;

        cursor.insertText(path, formatNormal);
        cursor.insertText(",", formatNormal);

        BlockFormatter format(cursor);
        format.apply(0, path.length(), getPathFormat(value, metadata));
    }

    static const QChar ellipsis(0x2026);
    QString collapsedText;
    for (QList<ValueEndpointEditor *>::const_iterator e = editors.constBegin(),
            endE = editors.constEnd(); e != endE; ++e) {
        if (!(*e)->matches(value, metadata))
            continue;

        collapsedText = (*e)->collapsedText(value, metadata);
        if (!collapsedText.isEmpty())
            break;
    }
    if (collapsedText.isEmpty())
        collapsedText = QString(ellipsis);

    QTextCharFormat format(formatCollapsed);
    format.setProperty(Property_Value, QVariant::fromValue<Variant::Write>(value));
    format.setProperty(Property_Metadata, QVariant::fromValue<Variant::Read>(metadata));
    format.setProperty(Property_CollapsedText, QVariant::fromValue<QString>(collapsedText));
    cursor.insertText(QString(QChar::ObjectReplacementCharacter), format);

    cursor.insertText("\n", formatNormal);
    return true;
}

static bool integerStringCompare(const QString &a, const QString &b)
{
    qint64 va = a.toLongLong();
    qint64 vb = b.toLongLong();
    return va < vb;
}

static void sortValueKeys(QStringList &list)
{
    bool allIntegers = true;
    for (QStringList::const_iterator child = list.constBegin(), endC = list.constEnd();
            child != endC;
            ++child) {
        bool ok = false;
        qint64 v = child->toLongLong(&ok);
        if (!ok || v < 0) {
            allIntegers = false;
            break;
        }
    }
    if (allIntegers) {
        std::sort(list.begin(), list.end(), integerStringCompare);
        return;
    }

    std::sort(list.begin(), list.end());
}

static Variant::Read childMetadata(const Variant::Read &child, const Variant::Read &parentMetadata)
{
    return Variant::Parse::evaluateMetadata(
            parentMetadata.getPath(Variant::PathElement::toMetadata(child.currentPath().back())),
            child);
}

void ValueEditor::insertValue(QTextCursor &cursor,
                              Variant::Write &value,
                              const Variant::Read &metadata,
                              int collapseDepth,
                              bool arbitraryChildren)
{
    if (collapseDepth <= 0 && insertCollapsed(cursor, value, metadata))
        return;

    if (metadata.metadata("ArbitraryStructure").toBool())
        arbitraryChildren = true;

    switch (value.getType()) {
    case Variant::Type::Empty:
    case Variant::Type::Real:
    case Variant::Type::Integer:
    case Variant::Type::Boolean:
    case Variant::Type::String:
    case Variant::Type::Bytes:
    case Variant::Type::Flags:
    case Variant::Type::Overlay:
        break;

    case Variant::Type::Array: {
        bool hasChildren = false;
        for (auto child : value.toArray()) {
            hasChildren = true;
            insertValue(cursor, child, childMetadata(child, metadata), collapseDepth - 1,
                        arbitraryChildren);
        }
        if (hasChildren)
            return;
        break;
    }
    case Variant::Type::Hash: {
        QStringList sorted;
        for (auto child : value.toHash()) {
            sorted.push_back(QString::fromStdString(child.first));
        }
        sortValueKeys(sorted);
        for (const auto &key : sorted) {
            auto child = value.hash(key);
            insertValue(cursor, child, childMetadata(child, metadata), collapseDepth - 1,
                        arbitraryChildren);
        }
        if (!sorted.isEmpty())
            return;
        break;
    }
    case Variant::Type::Keyframe: {
        bool hasChildren = false;
        for (auto child : value.toKeyframe()) {
            hasChildren = true;
            insertValue(cursor, child.second, childMetadata(child.second, metadata),
                        collapseDepth - 1, arbitraryChildren);
        }
        if (hasChildren)
            return;
        break;
    }

    case Variant::Type::Matrix: {
        bool hasChildren = false;
        for (auto child : value.toMatrix()) {
            hasChildren = true;
            insertValue(cursor, child.second, childMetadata(child.second, metadata),
                        collapseDepth - 1, arbitraryChildren);
        }
        if (hasChildren)
            return;
        break;
    }

    case Variant::Type::MetadataHash: {
        QStringList sorted;
        for (auto child : value.toMetadataHashChild()) {
            sorted.push_back(QString::fromStdString(child.first));
        }
        sortValueKeys(sorted);
        for (const auto &key : sorted) {
            auto child = value.metadataHash(key);
            insertValue(cursor, child, childMetadata(child, metadata), collapseDepth - 1,
                        arbitraryChildren);
        }
        bool haveChildren = !sorted.empty();

        sorted.clear();
        for (auto child : value.toMetadataHash()) {
            sorted.push_back(QString::fromStdString(child.first));
        }
        sortValueKeys(sorted);
        arbitraryChildren = true;
        for (const auto &key : sorted) {
            auto child = value.metadataHash(key);
            insertValue(cursor, child, childMetadata(child, metadata), collapseDepth - 1,
                        arbitraryChildren);
        }
        haveChildren = haveChildren || !sorted.empty();

        if (haveChildren)
            return;
        break;
    }

    case Variant::Type::MetadataFlags: {
        QStringList sorted;
        for (auto child : value.toMetadataSingleFlag()) {
            sorted.push_back(QString::fromStdString(child.first));
        }
        sortValueKeys(sorted);
        for (const auto &key : sorted) {
            auto child = value.metadataSingleFlag(key);
            insertValue(cursor, child, childMetadata(child, metadata), collapseDepth - 1,
                        arbitraryChildren);
        }
        bool haveChildren = !sorted.empty();

        sorted.clear();
        for (auto child : value.toMetadataFlags()) {
            sorted.push_back(QString::fromStdString(child.first));
        }
        sortValueKeys(sorted);
        arbitraryChildren = true;
        for (const auto &key : sorted) {
            auto child = value.metadataFlags(key);
            insertValue(cursor, child, childMetadata(child, metadata), collapseDepth - 1,
                        arbitraryChildren);
        }
        haveChildren = haveChildren || !sorted.empty();

        if (haveChildren)
            return;
        break;
    }

    case Variant::Type::MetadataReal:
    case Variant::Type::MetadataInteger:
    case Variant::Type::MetadataBoolean:
    case Variant::Type::MetadataString:
    case Variant::Type::MetadataBytes:
    case Variant::Type::MetadataArray:
    case Variant::Type::MetadataMatrix:
    case Variant::Type::MetadataKeyframe: {
        QStringList sorted;
        for (auto child : value.toMetadataSpecialized()) {
            sorted.push_back(QString::fromStdString(child.first));
        }
        sortValueKeys(sorted);
        arbitraryChildren = true;
        for (const auto &key : sorted) {
            auto child = value.metadata(key);
            insertValue(cursor, child, childMetadata(child, metadata), collapseDepth - 1,
                        arbitraryChildren);
        }
        if (!sorted.isEmpty())
            return;
        break;
    }
    }

    auto path = toPath(value.currentPath());
    cursor.insertText(path, formatNormal);
    cursor.insertText(",", formatNormal);

    BlockFormatter format(cursor);
    if (!arbitraryChildren)
        format.apply(0, path.length(), getPathFormat(value, metadata));

    QString text(Variant::Parse::toInputString(value));
    cursor.insertText(text, formatNormal);
    if (!arbitraryChildren)
        format.apply(path.length() + 1, text.length(), getValueFormat(value, metadata));

    cursor.insertText("\n", formatNormal);
}

void ValueEditor::rebuildContents()
{
    beginProgrammatic();
    document->clear();
    QTextCursor cursor(document);
    cursor.beginEditBlock();

    insertValue(cursor, value, metadata);

    editor->setCurrentCharFormat(formatNormal);

    cursor.endEditBlock();
    document->markContentsDirty(0, cursor.position());
    document->clearUndoRedoStacks();
    endProgrammatic();
}

static void parseLine(const QString &line,
                      QString &path,
                      QString *value = nullptr,
                      int *valueDelimiter = nullptr)
{
    for (int index = 0, max = line.length(); index < max; ++index) {
        QChar check(line.at(index));
        if (check == '\\') {
            path.append(check);
            ++index;
            if (index >= max)
                break;
            path.append(check);
            continue;
        } else if (check == ',') {
            if (valueDelimiter)
                *valueDelimiter = index;
            if (value)
                *value = line.mid(index + 1).trimmed();
            break;
        } else if (check == '\n' || check == '\r') {
            continue;
        }
        path.append(check);
    }
}

static void advanceIfNotAtEnd(QTextCursor &cursor)
{
    if (cursor.atEnd())
        return;
    cursor.setPosition(cursor.position() + 1);
}

void ValueEditor::expandValue(const Variant::Read &value)
{
    auto path = toPath(value.currentPath());
    expandedPaths.insert(path);

    rebuildContents();
    path.append(",");
    for (QTextCursor cursor(document); !cursor.atEnd(); advanceIfNotAtEnd(cursor)) {
        cursor.movePosition(QTextCursor::EndOfBlock, QTextCursor::KeepAnchor);
        QString line(cursor.selectedText());
        if (line.trimmed().isEmpty())
            continue;
        if (!line.startsWith(path))
            continue;
        cursor.setPosition(cursor.position());
        editor->setTextCursor(cursor);
        break;
    }
}

void ValueEditor::reparse()
{
    class CollapsedReplacementNode : public Variant::Parse::InputNode {
        Variant::Write value;
    public:
        CollapsedReplacementNode(const QString &path, Variant::Write &&value)
                : Variant::Parse::InputNode(Variant::PathElement::parse(path.toStdString())),
                  value(std::move(value))
        { }

        virtual ~CollapsedReplacementNode() = default;

        bool attemptWrite(Variant::Write &value,
                          const Variant::Read &,
                          const Variant::Read &) override
        {
            value.set(this->value);
            return true;
        }

        void fallbackWrite(Variant::Write &value,
                           const Variant::Read &,
                           const Variant::Read &) override
        { value.set(this->value); }
    };

    using ApplyPathFormat = std::function<void(const Variant::Read &, const Variant::Read &)>;
    using ApplyValueFormat = std::function<
            void(const Variant::Read &, const Variant::Read &, bool)>;

    class ValueParseNode : public Variant::Parse::InputNode {
        QString value;
        ApplyPathFormat formatPath;
        ApplyValueFormat formatValue;
    public:
        ValueParseNode(const QString &path,
                       const QString &value,
                       ApplyPathFormat formatPath,
                       ApplyValueFormat formatValue) : Variant::Parse::InputNode(
                Variant::PathElement::parse(path.toStdString())),
                                                       value(value),
                                                       formatPath(std::move(formatPath)),
                                                       formatValue(std::move(formatValue))
        { }

        virtual ~ValueParseNode() = default;

        bool attemptWrite(Variant::Write &value,
                          const Variant::Read &metadata,
                          const Variant::Read &reference) override
        {
            if (!Variant::Parse::fromInputString(value, this->value, metadata, reference, false))
                return false;

            formatPath(value, metadata);
            formatValue(value, metadata, false);
            return true;
        }

        void fallbackWrite(Variant::Write &value,
                           const Variant::Read &metadata,
                           const Variant::Read &reference) override
        {
            /* Try a non-ambiguous write first, to see if another evaluation has clarified
             * things (e.x. enum values) */
            bool ambiguous = true;
            if (Variant::Parse::fromInputString(value, this->value, metadata, reference, false)) {
                ambiguous = false;
            } else {
                Variant::Parse::fromInputString(value, this->value, metadata, reference, true);
            }

            formatPath(value, metadata);
            formatValue(value, metadata, ambiguous);
        }
    };

    std::vector<std::unique_ptr<Variant::Parse::InputNode>> parseNodes;
    for (QTextCursor cursor(document); !cursor.atEnd(); advanceIfNotAtEnd(cursor)) {
        int startOfLine = cursor.position();
        cursor.movePosition(QTextCursor::EndOfBlock, QTextCursor::KeepAnchor);
        auto line = cursor.selectedText();

        std::shared_ptr<BlockFormatter> format(std::make_shared<BlockFormatter>(cursor));
        QString path;
        QString contents;
        int valueDelimiter = -1;
        parseLine(line, path, &contents, &valueDelimiter);

        if (line.contains(QChar::ObjectReplacementCharacter)) {
            while (cursor.position() > startOfLine) {
                cursor.setPosition(cursor.position());
                cursor.setPosition(cursor.position() - 1, QTextCursor::KeepAnchor);
                if (cursor.selectedText().contains(QChar::ObjectReplacementCharacter))
                    break;
            }

            cursor.setPosition(cursor.position());
            cursor.setPosition(cursor.position() + 1, QTextCursor::KeepAnchor);

            QTextCharFormat objectFormat(cursor.charFormat());
            switch (objectFormat.objectType()) {
            case Type_Collapsed: {
                parseNodes.emplace_back(new CollapsedReplacementNode(path, objectFormat.property(
                                                                                               Property_Value)
                                                                                       .value<Variant::Write>()));
                break;
            }
            default:
                break;
            }

            format->apply(path.length(), 1, formatNormal);

            cursor.movePosition(QTextCursor::EndOfBlock);
            continue;
        }

        if (line.trimmed().isEmpty())
            continue;

        if (valueDelimiter < 0 || contents.isEmpty()) {
            format->apply(0, line.length(), formatOutsideMetadata);
            continue;
        }

        parseNodes.emplace_back(new ValueParseNode(path, contents, [=](const Variant::Read &value,
                                                                       const Variant::Read &metadata) {
            format->apply(0, path.length(), getPathFormat(value, metadata));
        }, [=](const Variant::Read &value, const Variant::Read &metadata, bool ambiguous) {
            format->apply(valueDelimiter + 1, line.length() - (valueDelimiter + 1),
                          getValueFormat(value, metadata, ambiguous));
        }));
    }

    Variant::Root prior(value);
    value.set(
            Variant::Parse::InputNode::fromPointers(parseNodes.begin(), parseNodes.end(), metadata,
                                                    prior));
    parseNodes.clear();

    {
        QTextCursor cursor(document);
        cursor.movePosition(QTextCursor::End);
        document->markContentsDirty(0, cursor.position());
    }

    if (prior.read() != value) {
        emit valueChanged();
    }
}

void ValueEditor::setValue(Variant::Write &setValue, const Variant::Read &setMetadata)
{
    value = setValue;
    metadata = setMetadata;
    expandedPaths.clear();
    rebuildContents();
}

void ValueEditor::showValueEditor(Variant::Write value, const Variant::Read &metadata)
{
    for (QList<ValueEndpointEditor *>::const_iterator e = editors.constBegin(),
            endE = editors.constEnd(); e != endE; ++e) {
        if (!(*e)->matches(value, metadata))
            continue;

        if ((*e)->edit(value, metadata, this)) {
            auto editPath = toPath(value.currentPath());

            rebuildContents();
            for (QTextCursor cursor(document); !cursor.atEnd(); advanceIfNotAtEnd(cursor)) {
                cursor.movePosition(QTextCursor::EndOfBlock, QTextCursor::KeepAnchor);
                QString line(cursor.selectedText());
                if (line.trimmed().isEmpty())
                    continue;
                if (!line.startsWith(editPath))
                    continue;
                cursor.setPosition(cursor.position());
                editor->setTextCursor(cursor);
                break;
            }

            emit valueChanged();
        }
        break;
    }
}

void ValueEditor::regenerateAndSelect(const std::vector<Variant::Path> &select)
{
    rebuildContents();

    if (select.empty()) {
        QTextCursor cursor(document);
        editor->setTextCursor(cursor);
        return;
    }

    QStringList paths;
    for (const auto &add : select) {
        paths.append(toPath(add));
    }

    for (QTextCursor cursor(document); !cursor.atEnd(); advanceIfNotAtEnd(cursor)) {
        cursor.movePosition(QTextCursor::EndOfBlock, QTextCursor::KeepAnchor);
        QString line(cursor.selectedText());
        if (line.trimmed().isEmpty())
            continue;
        for (QStringList::const_iterator check = paths.constBegin(), endC = paths.constEnd();
                check != endC;
                ++check) {
            if (!line.startsWith(*check))
                continue;
            cursor.setPosition(cursor.position());
            editor->setTextCursor(cursor);
            return;
        }
    }
}

QString ValueEditor::getToolTip(const Variant::Read &value, const Variant::Read &metadata)
{
    for (QList<ValueEndpointEditor *>::const_iterator e = editors.constBegin(),
            endE = editors.constEnd(); e != endE; ++e) {
        if (!(*e)->matches(value, metadata))
            continue;

        return (*e)->toolTip(value, metadata);
    }

    return metadata.metadata("Description").toDisplayString();
}

Variant::Path ValueEditor::getCursorPath() const
{
    QTextCursor cursor(editor->textCursor());
    cursor.movePosition(QTextCursor::StartOfBlock);
    cursor.movePosition(QTextCursor::EndOfBlock, QTextCursor::KeepAnchor);
    QString line(cursor.selectedText().trimmed());
    if (line.isEmpty())
        return Variant::Path();
    QString path;
    parseLine(line, path);
    if (path.isEmpty())
        return Variant::Path();
    return value.getPath(path).currentPath();
}

void ValueEditor::moveCursor(const Variant::Path &path)
{
    if (path.empty())
        return;
    auto check = toPath(path);
    if (check.isEmpty())
        return;
    for (QTextCursor cursor(document); !cursor.atEnd(); advanceIfNotAtEnd(cursor)) {
        cursor.movePosition(QTextCursor::EndOfBlock, QTextCursor::KeepAnchor);
        QString line(cursor.selectedText());
        if (line.trimmed().isEmpty())
            continue;
        if (!line.startsWith(check))
            continue;
        cursor.setPosition(cursor.position());
        editor->setTextCursor(cursor);
        return;
    }
}

void ValueEditor::setReadOnly(bool readOnly)
{
    editor->setReadOnly(readOnly);
}

ValueEndpointEditor::ValueEndpointEditor() = default;

ValueEndpointEditor::~ValueEndpointEditor() = default;

QString ValueEndpointEditor::toolTip(const Variant::Read &value, const Variant::Read &metadata)
{
    Q_UNUSED(value);
    return metadata.metadata("Description").toDisplayString();
}

bool ValueEndpointEditor::valid(const Variant::Read &value, const Variant::Read &metadata)
{
    Q_UNUSED(value);
    Q_UNUSED(metadata);
    return true;
}

QString ValueEndpointEditor::collapsedText(const CPD3::Data::Variant::Read &value,
                                           const CPD3::Data::Variant::Read &metadata)
{
    Q_UNUSED(value);
    Q_UNUSED(metadata);
    return QString();
}


namespace Internal {

ValueEditorText::ValueEditorText(ValueEditor *setParent) : QTextEdit(setParent), parent(setParent)
{ }

ValueEditorText::~ValueEditorText() = default;

void ValueEditorText::mouseReleaseEvent(QMouseEvent *event)
{
    QTextCursor cursor(cursorForPosition(event->pos()));
    if (!cursor.atEnd() && !(cursor.atBlockStart() && cursor.atBlockEnd())) {
        if (cursor.atBlockEnd() && !cursor.atStart()) {
            cursor.setPosition(cursor.position() - 1);
            cursor.setPosition(cursor.position() + 1, QTextCursor::KeepAnchor);
        } else {
            cursor.setPosition(cursor.position() + 1, QTextCursor::KeepAnchor);
        }
        if (cursor.selectedText().contains(QChar::ObjectReplacementCharacter)) {
            QTextCharFormat format(cursor.charFormat());

            switch (format.objectType()) {
            case Type_Collapsed:
                parent->showValueEditor(format.property(Property_Value).value<Variant::Write>(),
                                        format.property(Property_Metadata).value<Variant::Read>());
                break;
            default:
                break;
            }
        }
    }

    QTextEdit::mouseReleaseEvent(event);
}

static void extractMousePath(const QString &before,
                             const QString &line,
                             QString &linePath,
                             QString &completedPath,
                             QString &elementUnderMouse)
{
    for (int index = 0, max = line.length(), clickedPoint = before.length(); index < max; ++index) {
        QChar check(line.at(index));
        if (check == '\\') {
            linePath.append(check);
            ++index;
            if (index >= max)
                break;
            check = line.at(index);
        } else if (check == ',') {
            if (completedPath.isEmpty()) {
                completedPath = line.mid(0, index);
                elementUnderMouse = QString();
            } else if (elementUnderMouse.isEmpty()) {
                elementUnderMouse =
                        line.mid(completedPath.length(), index - completedPath.length());
            }
            break;
        } else if (check == '/') {
            if (index >= clickedPoint && completedPath.isEmpty()) {
                completedPath = line.mid(0, index + 1);
            } else if (elementUnderMouse.isEmpty()) {
                elementUnderMouse =
                        line.mid(completedPath.length(), index - completedPath.length());
            }
        }

        linePath.append(check);
    }
}

void ValueEditorText::contextMenuEvent(QContextMenuEvent *event)
{
    QTextCursor cursor(cursorForPosition(event->pos()));
    QMenu *menu = createStandardContextMenu(event->pos());

    if (!cursor.atEnd()) {
        cursor.movePosition(QTextCursor::StartOfBlock, QTextCursor::KeepAnchor);
        QString before(cursor.selectedText());

        cursor.movePosition(QTextCursor::StartOfBlock);
        cursor.movePosition(QTextCursor::EndOfBlock, QTextCursor::KeepAnchor);
        QString line(cursor.selectedText());

        QString linePath;
        QString completedPath;
        QString elementUnderMouse;
        extractMousePath(before, line, linePath, completedPath, elementUnderMouse);

        auto parentValue = parent->value.getPath(completedPath);
        auto parentMetadata = parent->metadata
                                    .getPath(Variant::PathElement::toMetadata(
                                            parentValue.currentPath()));
        auto clickedValue = parentValue.getPath(elementUnderMouse);
        auto clickedMetadata = parent->metadata
                                     .getPath(Variant::PathElement::toMetadata(
                                             clickedValue.currentPath()));
        auto lineValue = parent->value.getPath(linePath);
        auto lineMetadata =
                parent->metadata.getPath(Variant::PathElement::toMetadata(lineValue.currentPath()));

        if (!menu->isEmpty())
            menu->addSeparator();

        {
            QAction *action = new QAction(tr("Edit value"), menu);
            action->setProperty("value", QVariant::fromValue<Variant::Write>(lineValue));
            action->setProperty("metadata", QVariant::fromValue<Variant::Read>(lineMetadata));
            connect(action, SIGNAL(triggered(bool)), this, SLOT(contextMenuEdit()));
            menu->addAction(action);
        }

        if (line.contains(QChar::ObjectReplacementCharacter)) {
            QAction *action = new QAction(tr("Expand"), menu);
            action->setProperty("value", QVariant::fromValue<Variant::Write>(lineValue));
            action->setProperty("metadata", QVariant::fromValue<Variant::Read>(lineMetadata));
            connect(action, SIGNAL(triggered(bool)), this, SLOT(contextMenuExpand()));
            menu->addAction(action);
        }

        {
            QAction *action = new QAction(tr("Add value"), menu);
            action->setProperty("value", QVariant::fromValue<Variant::Write>(parentValue));
            action->setProperty("metadata", QVariant::fromValue<Variant::Read>(parentMetadata));
            connect(action, SIGNAL(triggered(bool)), this, SLOT(contextMenuAdd()));
            menu->addAction(action);
        }

        {
            QAction *action = new QAction(tr("Duplicate"), menu);
            action->setProperty("value", QVariant::fromValue<Variant::Write>(clickedValue));
            action->setProperty("metadata", QVariant::fromValue<Variant::Read>(parentMetadata));
            action->setProperty("parent", QVariant::fromValue(parentValue));
            connect(action, SIGNAL(triggered(bool)), this, SLOT(contextMenuDuplicate()));
            menu->addAction(action);
        }

        {
            QAction *action = new QAction(tr("Export"), menu);
            action->setProperty("value", QVariant::fromValue<Variant::Write>(clickedValue));
            action->setProperty("metadata", QVariant::fromValue<Variant::Read>(clickedMetadata));
            connect(action, SIGNAL(triggered(bool)), this, SLOT(contextMenuExport()));
            menu->addAction(action);
        }

        {
            QAction *action = new QAction(tr("Import"), menu);
            action->setProperty("metadata", QVariant::fromValue<Variant::Read>(clickedMetadata));
            action->setProperty("parent", QVariant::fromValue<Variant::Write>(parentValue));
            connect(action, SIGNAL(triggered(bool)), this, SLOT(contextMenuImport()));
            menu->addAction(action);
        }

        {
            QAction *action = new QAction(tr("Copy value"), menu);
            action->setProperty("value", QVariant::fromValue<Variant::Write>(clickedValue));
            action->setProperty("metadata", QVariant::fromValue<Variant::Read>(clickedMetadata));
            connect(action, SIGNAL(triggered(bool)), this, SLOT(contextMenuCopyValue()));
            menu->addAction(action);
        }

        {
            QAction *action = new QAction(tr("Paste value"), menu);
            action->setProperty("metadata", QVariant::fromValue<Variant::Read>(parentMetadata));
            action->setProperty("parent", QVariant::fromValue<Variant::Write>(parentValue));
            connect(action, SIGNAL(triggered(bool)), this, SLOT(contextMenuPasteValue()));
            menu->addAction(action);
        }
    }

    menu->exec(mapToGlobal(event->pos()));
}

void ValueEditorText::focusOutEvent(QFocusEvent *e)
{
    parent->reparse();
    QTextEdit::focusOutEvent(e);
}

bool ValueEditorText::event(QEvent *event)
{
    if (event->type() == QEvent::ToolTip) {
        QHelpEvent *helpEvent = static_cast<QHelpEvent *>(event);
        Q_ASSERT(helpEvent);
        QTextCursor cursor(cursorForPosition(helpEvent->pos()));

        if (!cursor.atEnd()) {
            cursor.movePosition(QTextCursor::StartOfBlock, QTextCursor::KeepAnchor);
            QString before(cursor.selectedText());

            cursor.movePosition(QTextCursor::StartOfBlock);
            cursor.movePosition(QTextCursor::EndOfBlock, QTextCursor::KeepAnchor);
            QString line(cursor.selectedText());

            QString linePath;
            QString completedPath;
            QString elementUnderMouse;
            extractMousePath(before, line, linePath, completedPath, elementUnderMouse);

            auto value = parent->value.getPath(completedPath).getPath(elementUnderMouse);
            auto metadata =
                    parent->metadata.getPath(Variant::PathElement::toMetadata(value.currentPath()));
            QString toolTip(parent->getToolTip(value, metadata));
            if (!toolTip.isEmpty()) {
                QToolTip::showText(helpEvent->globalPos(), toolTip);
                return true;
            }
        }
    }
    return QTextEdit::event(event);
}

void ValueEditorText::insertFromMimeData(const QMimeData *source)
{
    auto text = source->text();
    ExternalSink::normalizeUnicode(text);
    QTextEdit::insertPlainText(text);
}

void ValueEditorText::contextMenuEdit()
{
    auto value = sender()->property("value").value<Variant::Write>();
    parent->showValueEditor(value, Variant::Parse::evaluateMetadataTree(
            sender()->property("metadata").value<Variant::Read>(), value));
}

void ValueEditorText::contextMenuExpand()
{
    parent->expandValue(sender()->property("value").value<Variant::Write>());
}

void ValueEditorText::updateInserted(const std::vector<Variant::Path> &paths)
{
    parent->regenerateAndSelect(paths);
}

static void assignNewChild(Variant::Write &value, const Variant::Read &metadata)
{
    auto editor = metadata.metadata("Editor");

    switch (metadata.getType()) {
    case Variant::Type::Empty:
    case Variant::Type::Real:
    case Variant::Type::Integer:
    case Variant::Type::Boolean:
    case Variant::Type::String:
    case Variant::Type::Bytes:
    case Variant::Type::Flags:
    case Variant::Type::Array:
    case Variant::Type::Matrix:
    case Variant::Type::Hash:
    case Variant::Type::Keyframe:
    case Variant::Type::Overlay:
        break;

    case Variant::Type::MetadataReal:
        value.setType(Variant::Type::Real);
        if (editor["Default"].getType() == Variant::Type::Real) {
            value.set(editor["Default"]);
            return;
        }
        break;
    case Variant::Type::MetadataInteger:
        value.setType(Variant::Type::Integer);
        if (editor["Default"].getType() == Variant::Type::Integer) {
            value.set(editor["Default"]);
            return;
        }
        break;
    case Variant::Type::MetadataBoolean:
        value.setType(Variant::Type::Boolean);
        if (editor["Default"].getType() == Variant::Type::Boolean) {
            value.set(editor["Default"]);
            return;
        }
        break;
    case Variant::Type::MetadataString:
        value.setType(Variant::Type::String);
        if (editor["Default"].getType() == Variant::Type::String) {
            value.set(editor["Default"]);
            return;
        }
        break;
    case Variant::Type::MetadataBytes:
        value.setType(Variant::Type::Bytes);
        if (editor["Default"].getType() == Variant::Type::Bytes) {
            value.set(editor["Default"]);
            return;
        }
        break;
    case Variant::Type::MetadataFlags:
        value.setType(Variant::Type::Flags);
        if (editor["Default"].getType() == Variant::Type::Flags) {
            value.set(editor["Default"]);
            return;
        }
        break;
    case Variant::Type::MetadataArray:
        value.setType(Variant::Type::Array);
        if (editor["Default"].getType() == Variant::Type::Array) {
            value.set(editor["Default"]);
            return;
        }
        break;
    case Variant::Type::MetadataMatrix:
        value.setType(Variant::Type::Matrix);
        if (editor["Default"].getType() == Variant::Type::Matrix) {
            value.set(editor["Default"]);
            return;
        }
        break;
    case Variant::Type::MetadataHash:
        value.setType(Variant::Type::Hash);
        if (editor["Default"].getType() == Variant::Type::Hash) {
            value.set(editor["Default"]);
            return;
        }
        break;
    case Variant::Type::MetadataKeyframe:
        value.setType(Variant::Type::Keyframe);
        if (editor["Default"].getType() == Variant::Type::Keyframe) {
            value.set(editor["Default"]);
            return;
        }
        break;
    }
}

bool ValueEditorText::metadataHashPromptPossible(Variant::Write &parent,
                                                 const Variant::Read &metadata)
{
    std::unordered_map<std::string, Variant::Read> children;
    auto existing = parent.toHash();
    for (auto add : metadata.toMetadataHashChild()) {
        if (add.first.empty())
            continue;
        if (existing.find(add.first) != existing.end())
            continue;
        children.emplace(add.first, add.second);
    }

    if (children.empty())
        return false;

    ValueEditorAddChildSelection dialog(children, metadata.metadataHashChild(""), parent, this);
    if (!dialog.exec())
        return true;

    auto target = dialog.getInserted();
    auto targetMetadata =
            Variant::Parse::evaluateMetadataTree(metadata.getPath(target.currentPath().back()),
                                                 target);

    updateInserted(target.currentPath());
    this->parent->showValueEditor(target, targetMetadata);
    return true;
}

void ValueEditorText::contextMenuAdd()
{
    auto parent = sender()->property("value").value<Variant::Write>();
    auto metadata = sender()->property("metadata").value<Variant::Read>();

    Variant::Write value = Variant::Write::empty();
    switch (metadata.getType()) {
    case Variant::Type::MetadataHash:
        if (metadataHashPromptPossible(parent, metadata))
            return;
        assignNewChild(value, metadata.metadataHashChild(""));
        break;
    case Variant::Type::MetadataArray:
    case Variant::Type::MetadataMatrix:
    case Variant::Type::MetadataKeyframe:
        assignNewChild(value, metadata.metadata("Children"));
        break;

    case Variant::Type::Empty:
    case Variant::Type::Real:
    case Variant::Type::Integer:
    case Variant::Type::Boolean:
    case Variant::Type::String:
    case Variant::Type::Bytes:
    case Variant::Type::Flags:
    case Variant::Type::Array:
    case Variant::Type::Matrix:
    case Variant::Type::Hash:
    case Variant::Type::Keyframe:
    case Variant::Type::MetadataReal:
    case Variant::Type::MetadataInteger:
    case Variant::Type::MetadataBoolean:
    case Variant::Type::MetadataString:
    case Variant::Type::MetadataBytes:
    case Variant::Type::MetadataFlags:
    case Variant::Type::Overlay:
        break;
    }

    auto path = ValueClipboard::insert(parent, value, metadata);
    updateInserted(path);

    auto target = this->parent->value.getPath(path);
    auto targetMetadata = Variant::Parse::evaluateMetadataTree(
            this->parent->metadata.getPath(Variant::PathElement::toMetadata(path)), target);

    this->parent->showValueEditor(target, targetMetadata);
}

bool ValueEditorText::insertPromptNamed(const std::vector<CPD3::Data::Variant::Read> &values,
                                        Variant::Write &parent,
                                        const Variant::Read &metadata)
{
    if (values.size() != 1)
        return false;
    /* See if this is a generic any value hash, and if so, prompt for a name */
    if (metadata.getType() != Variant::Type::MetadataHash)
        return false;
    auto possible = metadata.toMetadataHashChild().keys();
    possible.erase("");
    if (!possible.empty())
        return false;

    bool ok;
    QString name(QInputDialog::getText(this, tr("Select Name"), tr("Name:"), QLineEdit::Normal,
                                       QString(), &ok));
    if (!ok || name.isEmpty())
        return false;

    auto target = parent.hash(name);
    target.set(values.front());

    updateInserted(target.currentPath());
    return true;
}

void ValueEditorText::contextMenuDuplicate()
{
    auto value = sender()->property("value").value<Variant::Write>();
    auto parent = sender()->property("parent").value<Variant::Write>();
    auto metadata = sender()->property("metadata").value<Variant::Read>();

    metadata = Variant::Parse::evaluateMetadataTree(metadata, parent);
    if (insertPromptNamed(value, parent, metadata))
        return;

    updateInserted(ValueClipboard::insert(parent, value, metadata, true));
}

void ValueEditorText::contextMenuImport()
{
    auto parent = sender()->property("parent").value<Variant::Write>();
    auto metadata = sender()->property("metadata").value<Variant::Read>();

    QString fileName =
            QFileDialog::getOpenFileName(this, tr("Load Items", "import items title"), QString(),
                                         tr("CPD3 data file (*.xml *.c3d)"));
    if (fileName.isEmpty())
        return;

    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly)) {
        QMessageBox::information(this, tr("Error Opening File"),
                                 tr("Can't open file %1 for import: %2").arg(fileName,
                                                                             file.errorString()));
        return;
    }

    QByteArray data;
    char *buffer = (char *) malloc(65520);
    Q_ASSERT(buffer);
    while (!file.atEnd() || file.bytesAvailable() > 0) {
        qint64 code = file.read(buffer, 65520);
        if (code == -1) {
            free(buffer);
            QMessageBox::information(this, tr("Error Writing to File"),
                                     tr("Error writing to file %1: %2").arg(fileName,
                                                                            file.errorString()));
            return;
        } else if (code > 0) {
            data.append(buffer, (int) code);
        } else {
            QThread::yieldCurrentThread();
        }
    }
    free(buffer);
    file.close();

    std::vector<Variant::Read> values;
    for (const auto &add : ValueClipboard::decode(data)) {
        values.emplace_back(add.read());
    }
    if (values.empty())
        return;

    metadata = Variant::Parse::evaluateMetadataTree(metadata, parent);
    if (insertPromptNamed(values, parent, metadata))
        return;

    updateInserted(ValueClipboard::insert(parent, values, metadata));
}

void ValueEditorText::contextMenuExport()
{
    auto value = sender()->property("value").value<Variant::Write>();

    QString fileName =
            QFileDialog::getSaveFileName(this, tr("Save Items", "export items title"), QString(),
                                         tr("CPD3 data file (*.xml *.c3d)"));
    if (fileName.isEmpty())
        return;

    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Truncate)) {
        QMessageBox::information(this, tr("Error Opening File"),
                                 tr("Can't open file %1 for export: %2").arg(fileName,
                                                                             file.errorString()));
        return;
    }

    QByteArray rawData(ValueClipboard::encode(value));
    const char *data = rawData.constData();
    qint64 length = rawData.length();
    while (length > 0) {
        qint64 code = file.write(data, length);
        if (code == -1) {
            QMessageBox::information(this, tr("Error Writing File"),
                                     tr("Error writing to file %1 for export: %2").arg(fileName,
                                                                                       file.errorString()));
            file.close();
            return;
        } else if (code > 0) {
            length -= code;
            data += code;
        } else {
            QThread::yieldCurrentThread();
        }
    }
    file.close();
}

void ValueEditorText::contextMenuCopyValue()
{
    auto value = sender()->property("value").value<Variant::Write>();
    QApplication::clipboard()->setMimeData(new ValueMimeData(Variant::Root(value)));
}

void ValueEditorText::contextMenuPasteValue()
{
    auto parent = sender()->property("parent").value<Variant::Write>();
    auto metadata = sender()->property("metadata").value<Variant::Read>();
    std::vector<Variant::Read> values;
    for (const auto &add : ValueMimeData::fromMimeData(QApplication::clipboard()->mimeData())) {
        values.emplace_back(add.read());
    }
    if (values.empty())
        return;
    if (insertPromptNamed(values, parent, metadata))
        return;

    updateInserted(ValueClipboard::insert(parent, values, metadata));
}


ValueEditorDocument::ValueEditorDocument(ValueEditor *setParent) : QTextDocument(setParent),
                                                                   parent(setParent)
{ }

ValueEditorDocument::~ValueEditorDocument() = default;


ValueEditorCollapsedObject::ValueEditorCollapsedObject(ValueEditor *setParent) : QObject(setParent),
                                                                                 parent(setParent)
{ }

ValueEditorCollapsedObject::~ValueEditorCollapsedObject() = default;

void ValueEditorCollapsedObject::drawObject(QPainter *painter,
                                            const QRectF &rect,
                                            QTextDocument *doc,
                                            int posInDocument,
                                            const QTextFormat &format)
{
    Q_UNUSED(doc);
    Q_UNUSED(posInDocument);
    Q_ASSERT(format.type() == QTextFormat::CharFormat);
    const QTextCharFormat &tf = static_cast<const QTextCharFormat &>(format);
    painter->save();
    painter->setFont(tf.font());
    painter->setBrush(tf.foreground());
    painter->drawText(rect.bottomLeft(), tf.property(Property_CollapsedText).toString());
    painter->restore();
}

QSizeF ValueEditorCollapsedObject::intrinsicSize(QTextDocument *doc,
                                                 int posInDocument,
                                                 const QTextFormat &format)
{
    Q_UNUSED(doc);
    Q_UNUSED(posInDocument);
    Q_ASSERT(format.type() == QTextFormat::CharFormat);
    const QTextCharFormat &tf = static_cast<const QTextCharFormat &>(format);
    return QFontMetrics(tf.font()).boundingRect(tf.property(Property_CollapsedText).toString())
                                  .size();
}


ValueEndpointPrimitiveEditorDialog::ValueEndpointPrimitiveEditorDialog(Variant::Write &sv,
                                                                       const Variant::Read &sm,
                                                                       QWidget *parent) : QDialog(
        parent), value(sv), metadata(sm)
{
    auto editor = metadata.metadata("Editor");

    setWindowTitle(tr("Edit Value"));

    QVBoxLayout *layout = new QVBoxLayout(this);
    setLayout(layout);

    {
        QString description(metadata.metadata("Description").toDisplayString());
        if (!description.isEmpty()) {
            QLabel *label = new QLabel(description, this);
            layout->addWidget(label);
            label->setWordWrap(true);
        }
    }

    QWidget *container = new QWidget(this);
    layout->addWidget(container);
    QHBoxLayout *line = new QHBoxLayout(container);
    container->setLayout(line);
    line->setContentsMargins(0, 0, 0, 0);

    QLabel *label = new QLabel(tr("&Type:"), container);
    line->addWidget(label);

    type = new QComboBox(container);
    line->addWidget(type);
    label->setBuddy(type);
    type->addItem(tr("Real number"), QVariant::fromValue(Variant::Type::Real));
    type->addItem(tr("Integer"), QVariant::fromValue(Variant::Type::Integer));
    type->addItem(tr("Text"), QVariant::fromValue(Variant::Type::String));
    type->addItem(tr("Boolean"), QVariant::fromValue(Variant::Type::Boolean));
    type->addItem(tr("Flags"), QVariant::fromValue(Variant::Type::Flags));
    type->addItem(tr("Byte data"), QVariant::fromValue(Variant::Type::Bytes));
    type->addItem(tr("Overlay"), QVariant::fromValue(Variant::Type::Overlay));
    type->addItem(tr("Advanced"), INT_MAX);
    type->setCurrentIndex(type->count() - 1);
    type->addItem(tr("Empty"), QVariant::fromValue(Variant::Type::Empty));
    {
        auto check = value.getType();
        for (int i = 0, max = type->count(); i < max; i++) {
            auto d = type->itemData(i);
            if (!d.isValid())
                continue;
            if (!d.canConvert<Variant::Type>())
                continue;
            if (d.value<Variant::Type>() == check) {
                type->setCurrentIndex(i);
                break;
            }
        }
    }
    connect(type, SIGNAL(currentIndexChanged(int)), this, SLOT(typeChanged()));

    advancedType = new QComboBox(container);
    line->addWidget(advancedType);
    advancedType->addItem(tr("Hash"), QVariant::fromValue(Variant::Type::Hash));
    advancedType->addItem(tr("Array"), QVariant::fromValue(Variant::Type::Array));
    advancedType->addItem(tr("Matrix"), QVariant::fromValue(Variant::Type::Matrix));
    advancedType->addItem(tr("Keyframe"), QVariant::fromValue(Variant::Type::Keyframe));
    advancedType->insertSeparator(type->count());
    advancedType->addItem(tr("Real number metadata"),
                          QVariant::fromValue(Variant::Type::MetadataReal));
    advancedType->addItem(tr("Integer metadata"),
                          QVariant::fromValue(Variant::Type::MetadataInteger));
    advancedType->addItem(tr("Text metadata"), QVariant::fromValue(Variant::Type::MetadataString));
    advancedType->addItem(tr("Boolean metadata"),
                          QVariant::fromValue(Variant::Type::MetadataBoolean));
    advancedType->addItem(tr("Flags metadata"), QVariant::fromValue(Variant::Type::MetadataFlags));
    advancedType->addItem(tr("Byte metadata"), QVariant::fromValue(Variant::Type::MetadataBytes));
    advancedType->addItem(tr("Hash metadata"), QVariant::fromValue(Variant::Type::MetadataHash));
    advancedType->addItem(tr("Array metadata"), QVariant::fromValue(Variant::Type::MetadataArray));
    advancedType->addItem(tr("Matrix metadata"),
                          QVariant::fromValue(Variant::Type::MetadataMatrix));
    advancedType->addItem(tr("Keyframe metadata"),
                          QVariant::fromValue(Variant::Type::MetadataKeyframe));
    {
        auto check = value.getType();
        for (int i = 0, max = advancedType->count(); i < max; i++) {
            auto d = advancedType->itemData(i);
            if (!d.isValid())
                continue;
            if (!d.canConvert<Variant::Type>())
                continue;
            if (d.value<Variant::Type>() == check) {
                advancedType->setCurrentIndex(i);
                break;
            }
        }
    }
    connect(advancedType, SIGNAL(currentIndexChanged(int)), this, SLOT(typeChanged()));

    line->addStretch(1);

    singleEditor = new QLineEdit(this);
    layout->addWidget(singleEditor);
    {
        QString text(editor["Empty"].toDisplayString());
        if (text.isEmpty())
            text = metadata.metadata("UndefinedDescription").toDisplayString();
        if (!text.isEmpty())
            singleEditor->setPlaceholderText(text);
    }
    connect(singleEditor, SIGNAL(textEdited(
                                         const QString &)), this, SLOT(singleTextChanged()));

    {
        QString text(editor["Text"].toDisplayString());
        if (text.isEmpty())
            text = tr("Set");
        toggle = new QCheckBox(text, this);
    }
    layout->addWidget(toggle);
    connect(toggle, SIGNAL(stateChanged(int)), this, SLOT(toggleChanged()));

    flagsSelect = new SetSelector(this);
    layout->addWidget(flagsSelect);
    {
        QString text(editor["Text"].toDisplayString());
        if (text.isEmpty())
            text = tr("Selected");
        flagsSelect->setEmptyText(text);
    }

    multipleEdit = new QPlainTextEdit(this);
    layout->addWidget(multipleEdit, 1);

    localeSelect = new QPushButton(tr("Locale"), this);
    layout->addWidget(localeSelect);

    hexEdit = new QHexEdit(this);
    layout->addWidget(hexEdit, 1);

    filler = new QWidget(this);
    layout->addWidget(filler, 1);


    QDialogButtonBox *buttonBox =
            new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal,
                                 this);
    layout->addWidget(buttonBox);
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(acceptChanges()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

    typeChanged();
}

Variant::Type ValueEndpointPrimitiveEditorDialog::getSelectedType() const
{
    int index = type->currentIndex();
    if (index < 0 || index >= type->count())
        return Variant::Type::Empty;
    {
        auto check = type->itemData(index);
        if (check.canConvert<Variant::Type>())
            return check.value<Variant::Type>();
    }

    index = advancedType->currentIndex();
    if (index < 0 || index >= advancedType->count())
        return Variant::Type::Empty;
    {
        auto check = advancedType->itemData(index);
        if (check.canConvert<Variant::Type>())
            return check.value<Variant::Type>();
    }

    return Variant::Type::Empty;
}

void ValueEndpointPrimitiveEditorDialog::typeChanged()
{
    switch (getSelectedType()) {
    case Variant::Type::Empty:
        advancedType->hide();
        singleEditor->hide();
        toggle->hide();
        flagsSelect->hide();
        multipleEdit->hide();
        localeSelect->hide();
        hexEdit->hide();
        filler->show();
        break;
    case Variant::Type::Real: {
        advancedType->hide();
        singleEditor->show();
        toggle->hide();
        flagsSelect->hide();
        multipleEdit->hide();
        localeSelect->hide();
        hexEdit->hide();
        filler->show();

        double v = value.toDouble();
        if (!FP::defined(v))
            singleEditor->setText(QString());
        else
            singleEditor->setText(QString::number(v));
        singleTextChanged();
        break;
    }
    case Variant::Type::Integer: {
        advancedType->hide();
        singleEditor->show();
        toggle->hide();
        flagsSelect->hide();
        multipleEdit->hide();
        localeSelect->hide();
        hexEdit->hide();
        filler->show();

        qint64 v = value.toInt64();
        if (!INTEGER::defined(v))
            singleEditor->setText(QString());
        else
            singleEditor->setText(QString::number(v));
        singleTextChanged();
        break;
    }
    case Variant::Type::Flags: {
        advancedType->hide();
        singleEditor->hide();
        toggle->hide();
        flagsSelect->show();
        multipleEdit->hide();
        localeSelect->hide();
        hexEdit->hide();
        filler->show();

        flagsSelect->clear();
        if (metadata.getType() == Variant::Type::MetadataFlags) {
            QStringList sorted;
            for (auto add : metadata.toMetadataSingleFlag()) {
                sorted.push_back(QString::fromStdString(add.first));
            }
            std::sort(sorted.begin(), sorted.end());
            for (const auto &add : sorted) {
                QString text(metadata.metadataSingleFlag(add.toStdString())
                                     .hash("Description")
                                     .toDisplayString());
                if (!text.isEmpty()) {
                    text = tr("%1 - %2", "flag text").arg(add, text);
                } else {
                    text = add;
                }
                flagsSelect->addItem(add, text);
            }
        }
        QSet<QString> selected;
        for (const auto &add : value.toFlags()) {
            selected.insert(QString::fromStdString(add));
        }
        flagsSelect->setSelected(std::move(selected));
        break;
    }

    case Variant::Type::String:
        advancedType->hide();
        singleEditor->hide();
        toggle->hide();
        flagsSelect->hide();
        multipleEdit->show();
        localeSelect->show();
        hexEdit->hide();
        filler->hide();
        localizedStrings = value.allLocalizedStrings();
        rebuildLocaleMenu();
        break;
    case Variant::Type::Boolean: {
        advancedType->hide();
        singleEditor->hide();
        toggle->show();
        flagsSelect->hide();
        multipleEdit->hide();
        localeSelect->hide();
        hexEdit->hide();
        filler->show();
        bool checked = metadata.metadata("Editor").hash("Default").toBool();
        if (value.exists())
            checked = value.toBool();
        toggle->setChecked(checked);
        break;
    }
    case Variant::Type::Bytes:
        advancedType->hide();
        singleEditor->hide();
        toggle->hide();
        flagsSelect->hide();
        multipleEdit->hide();
        localeSelect->hide();
        hexEdit->show();
        filler->hide();
        hexEdit->setData(value.toBinary());
        break;
    case Variant::Type::Overlay:
        advancedType->hide();
        singleEditor->show();
        toggle->hide();
        flagsSelect->hide();
        multipleEdit->hide();
        localeSelect->hide();
        hexEdit->hide();
        filler->show();
        singleEditor->setText(value.toQString());
        singleTextChanged();
        break;

    case Variant::Type::Array:
    case Variant::Type::Matrix:
    case Variant::Type::Hash:
    case Variant::Type::Keyframe:
    case Variant::Type::MetadataReal:
    case Variant::Type::MetadataInteger:
    case Variant::Type::MetadataBoolean:
    case Variant::Type::MetadataString:
    case Variant::Type::MetadataBytes:
    case Variant::Type::MetadataFlags:
    case Variant::Type::MetadataArray:
    case Variant::Type::MetadataMatrix:
    case Variant::Type::MetadataHash:
    case Variant::Type::MetadataKeyframe:
        advancedType->show();
        singleEditor->hide();
        toggle->hide();
        flagsSelect->hide();
        multipleEdit->hide();
        localeSelect->hide();
        hexEdit->hide();
        filler->show();
        break;
    }

    update();
}

void ValueEndpointPrimitiveEditorDialog::rebuildLocaleMenu(const QString &addLocale)
{
    QMenu *menu = localeSelect->menu();
    QString selectedLocale;
    if (menu) {
        QList<QAction *> actions(menu->actions());
        for (QList<QAction *>::const_iterator a = actions.constBegin(), endA = actions.constEnd();
                a != endA;
                ++a) {
            if (!(*a)->isCheckable())
                continue;
            if (!(*a)->isChecked())
                continue;
            QVariant v((*a)->property("selectLocale"));
            if (!v.isValid())
                continue;
            selectedLocale = v.toString();
            break;
        }
        localeSelect->setMenu(0);
        menu->deleteLater();
    }
    if (!addLocale.isEmpty()) {
        if (!localizedStrings.count(addLocale))
            localizedStrings.emplace(addLocale, QString());
        selectedLocale = addLocale;
    }
    if (!localizedStrings.count(selectedLocale))
        selectedLocale = QString();
    multipleEdit->disconnect(this);
    {
        auto set = localizedStrings.find(selectedLocale);
        if (set != localizedStrings.end())
            multipleEdit->setPlainText(set->second);
        else
            multipleEdit->setPlainText(QString());
    }
    connect(multipleEdit, SIGNAL(textChanged()), this, SLOT(localeTextEdited()));


    menu = new QMenu(localeSelect);
    localeSelect->setMenu(menu);

    QActionGroup *group = new QActionGroup(menu);
    group->setExclusive(true);

    QAction *action = menu->addAction(tr("&Default"));
    action->setProperty("selectLocale", QString());
    action->setCheckable(true);
    action->setChecked(selectedLocale.isEmpty());
    connect(action, SIGNAL(toggled(bool)), this, SLOT(localeSelected()));
    group->addAction(action);

    QStringList sorted;
    for (const auto &add : localizedStrings) {
        sorted.push_back(add.first);
    }
    std::sort(sorted.begin(), sorted.end());
    bool hasLocales = false;
    for (const auto &l : sorted) {
        if (l.isEmpty())
            continue;
        action = menu->addAction(l);
        action->setProperty("selectLocale", l);
        action->setCheckable(true);
        action->setChecked(l == selectedLocale);
        connect(action, SIGNAL(toggled(bool)), this, SLOT(localeSelected()));
        group->addAction(action);
        hasLocales = true;
    }

    menu->addSeparator();

    action = menu->addAction(tr("&Add"));
    connect(action, SIGNAL(triggered(bool)), this, SLOT(addLocale()));

    if (hasLocales) {
        QMenu *submenu = menu->addMenu(tr("&Remove"));
        connect(submenu, SIGNAL(triggered(QAction * )), this, SLOT(removeLocale(QAction * )));
        for (QStringList::const_iterator l = sorted.constBegin(), endL = sorted.constEnd();
                l != endL;
                ++l) {
            if (l->isEmpty())
                continue;
            action = submenu->addAction(*l);
            action->setProperty("removeLocale", *l);
        }
    }
}

void ValueEndpointPrimitiveEditorDialog::localeTextEdited()
{
    QMenu *menu = localeSelect->menu();
    QString selectedLocale;
    if (menu) {
        QList<QAction *> actions(menu->actions());
        for (QList<QAction *>::const_iterator a = actions.constBegin(), endA = actions.constEnd();
                a != endA;
                ++a) {
            if (!(*a)->isCheckable())
                continue;
            if (!(*a)->isChecked())
                continue;
            QVariant v((*a)->property("selectLocale"));
            if (!v.isValid())
                continue;
            selectedLocale = v.toString();
            break;
        }
    }
    localizedStrings.emplace(selectedLocale, multipleEdit->toPlainText());
}

void ValueEndpointPrimitiveEditorDialog::localeSelected()
{
    QMenu *menu = localeSelect->menu();
    QString selectedLocale;
    if (!menu)
        return;

    QList<QAction *> actions(menu->actions());
    for (QList<QAction *>::const_iterator a = actions.constBegin(), endA = actions.constEnd();
            a != endA;
            ++a) {
        if (!(*a)->isCheckable())
            continue;
        if (!(*a)->isChecked())
            continue;
        QVariant v((*a)->property("selectLocale"));
        if (!v.isValid())
            continue;
        auto set = localizedStrings.find(selectedLocale);
        if (set != localizedStrings.end())
            multipleEdit->setPlainText(set->second);
        else
            multipleEdit->setPlainText(QString());
        break;
    }
}

void ValueEndpointPrimitiveEditorDialog::addLocale()
{
    bool ok;
    QString locale
            (QInputDialog::getText(this, tr("Add Locale"), tr("Add locale:"), QLineEdit::Normal,
                                   QString(), &ok));
    if (!ok || locale.isEmpty())
        return;
    rebuildLocaleMenu(locale);
}

void ValueEndpointPrimitiveEditorDialog::removeLocale(QAction *action)
{
    QVariant actionLocale(action->property("removeLocale"));
    if (!actionLocale.isValid())
        return;
    QString selectedLocale(actionLocale.toString());
    if (selectedLocale.isEmpty())
        return;
    localizedStrings.erase(selectedLocale);
    rebuildLocaleMenu();
}

static qint64 convertInteger(const QString &text, bool &ok, const Variant::Read &metadata)
{
    if (text.startsWith("0i")) {
        return text.mid(2).toLongLong(&ok);
    } else if (text.startsWith("0x")) {
        return text.mid(2).toLongLong(&ok, 16);
    } else if (text.startsWith("0o")) {
        return text.mid(2).toLongLong(&ok, 8);
    } else if (text.startsWith("0b")) {
        return text.mid(2).toLongLong(&ok, 2);
    } else {
        QString format(metadata.metadata("Format").toDisplayString());
        int base = 10;
        if (!format.isEmpty()) {
            NumberFormat nf(format);
            switch (nf.getMode()) {
            case NumberFormat::Decimal:
            case NumberFormat::Scientific:
                break;
            case NumberFormat::Hex:
                base = 16;
                break;
            case NumberFormat::Octal:
                base = 8;
                break;
            }
        }
        return text.toLongLong(&ok, base);
    }
}

void ValueEndpointPrimitiveEditorDialog::singleTextChanged()
{
    auto editor = metadata.metadata("Editor");

    switch (getSelectedType()) {
    case Variant::Type::Real: {
        QString text(singleEditor->text());
        if (text.isEmpty()) {
            if (editor["AllowUndefined"].exists() && !editor["AllowUndefined"].toBool())
                singleEditor->setStyleSheet("QLineEdit {background-color: red;}");
            else
                singleEditor->setStyleSheet(QString());
            return;
        }

        bool ok = false;
        double v = text.toDouble(&ok);
        if (!ok || !FP::defined(v)) {
            singleEditor->setStyleSheet("QLineEdit {background-color: red;}");
            return;
        }

        double bound = editor["Minimum"].toDouble();
        if (FP::defined(bound)) {
            if (editor["MinimumExclusive"].toBool()) {
                if (v <= bound) {
                    singleEditor->setStyleSheet("QLineEdit {background-color: red;}");
                    return;
                }
            } else {
                if (v < bound) {
                    singleEditor->setStyleSheet("QLineEdit {background-color: red;}");
                    return;
                }
            }
        }

        bound = editor["Maximum"].toDouble();
        if (FP::defined(bound)) {
            if (editor["MaximumExclusive"].toBool()) {
                if (v >= bound) {
                    singleEditor->setStyleSheet("QLineEdit {background-color: red;}");
                    return;
                }
            } else {
                if (v > bound) {
                    singleEditor->setStyleSheet("QLineEdit {background-color: red;}");
                    return;
                }
            }
        }
        singleEditor->setStyleSheet(QString());
        return;
    }
    case Variant::Type::Integer: {
        QString text(singleEditor->text());
        if (text.isEmpty()) {
            if (editor["AllowUndefined"].exists() && !editor["AllowUndefined"].toBool())
                singleEditor->setStyleSheet("QLineEdit {background-color: red;}");
            else
                singleEditor->setStyleSheet(QString());
            return;
        }

        bool ok = false;
        qint64 v = convertInteger(text, ok, metadata);
        if (!ok || !INTEGER::defined(v)) {
            singleEditor->setStyleSheet("QLineEdit {background-color: red;}");
            return;
        }

        qint64 bound = editor["Minimum"].toInt64();
        if (INTEGER::defined(bound) && v < bound) {
            singleEditor->setStyleSheet("QLineEdit {background-color: red;}");
            return;
        }

        bound = editor["Maximum"].toInt64();
        if (INTEGER::defined(bound) && v > bound) {
            singleEditor->setStyleSheet("QLineEdit {background-color: red;}");
            return;
        }
        singleEditor->setStyleSheet(QString());
        return;
    }
    case Variant::Type::Overlay: {
        QString text(singleEditor->text());
        if (!text.startsWith('/') && !text.startsWith("../")) {
            singleEditor->setStyleSheet("QLineEdit {background-color: red;}");
            return;
        }
        singleEditor->setStyleSheet(QString());
        return;
    }
    default:
        break;
    }
}

void ValueEndpointPrimitiveEditorDialog::toggleChanged()
{
    auto editor = metadata.metadata("Editor");

    if (toggle->checkState() == Qt::Checked) {
        QString text(editor["Set"].toDisplayString());
        if (text.isEmpty())
            text = editor["Text"].toDisplayString();
        if (text.isEmpty())
            text = tr("Set");
        toggle->setText(text);
    } else {
        QString text(editor["Clear"].toDisplayString());
        if (text.isEmpty())
            text = editor["Text"].toDisplayString();
        if (text.isEmpty())
            text = tr("Set");
        toggle->setText(text);
    }
}

void ValueEndpointPrimitiveEditorDialog::acceptChanges()
{
    auto type = getSelectedType();
    value.setType(type);

    switch (getSelectedType()) {
    case Variant::Type::Real: {
        QString text(singleEditor->text());
        if (text.isEmpty()) {
            value.setDouble(FP::undefined());
        } else {
            bool ok = false;
            double v = text.toDouble(&ok);
            if (!ok)
                v = FP::undefined();
            value.setDouble(v);
        }
        break;
    }
    case Variant::Type::Integer: {
        QString text(singleEditor->text());
        if (text.isEmpty()) {
            value.setInt64(INTEGER::undefined());
        } else {
            bool ok = false;
            qint64 v = convertInteger(text, ok, metadata);
            if (!ok)
                v = INTEGER::undefined();
            value.setInt64(v);
        }
        break;
    }
    case Variant::Type::Flags:
        value.setFlags(Util::set_from_qstring(flagsSelect->getSelected()));
        break;

    case Variant::Type::String: {
        value.clear();
        for (const auto &add : localizedStrings) {
            value.setDisplayString(add.first, add.second);
        }
        break;
    }
    case Variant::Type::Boolean:
        value.setBool(toggle->isChecked());
        break;
    case Variant::Type::Bytes:
        value.setBinary(hexEdit->data());
        break;
    case Variant::Type::Overlay:
        value.setOverlay(singleEditor->text());
        break;

    case Variant::Type::Empty:
    case Variant::Type::Array:
    case Variant::Type::Matrix:
    case Variant::Type::Hash:
    case Variant::Type::Keyframe:
    case Variant::Type::MetadataReal:
    case Variant::Type::MetadataInteger:
    case Variant::Type::MetadataBoolean:
    case Variant::Type::MetadataString:
    case Variant::Type::MetadataBytes:
    case Variant::Type::MetadataFlags:
    case Variant::Type::MetadataArray:
    case Variant::Type::MetadataMatrix:
    case Variant::Type::MetadataHash:
    case Variant::Type::MetadataKeyframe:
        break;
    }

    accept();
}


ValueEditorAddChildSelection::ValueEditorAddChildSelection(const std::unordered_map<std::string,
                                                                                    Variant::Read> &ch,
                                                           const Variant::Read &fb,
                                                           Variant::Write &vp,
                                                           QWidget *wp) : QDialog(wp),
                                                                          parent(vp),
                                                                          children(ch),
                                                                          fallback(fb)
{
    QVBoxLayout *layout = new QVBoxLayout(this);
    setLayout(layout);

    setWindowTitle(tr("Add Value"));


    childSelect = new QComboBox(this);
    layout->addWidget(childSelect);
    QStringList sorted;
    for (const auto &add : children) {
        sorted.push_back(QString::fromStdString(add.first));
    }
    std::sort(sorted.begin(), sorted.end());
    for (const auto &add : sorted) {
        childSelect->addItem(add, add);
    }
    childSelect->insertSeparator(childSelect->count());
    childSelect->addItem(tr("Manual Entry"));
    connect(childSelect, SIGNAL(currentIndexChanged(int)), this, SLOT(selectionChanged()));


    description = new QLabel(this);
    layout->addWidget(description);
    description->setWordWrap(true);


    other = new QLineEdit(this);
    layout->addWidget(other);


    layout->addStretch(1);

    QDialogButtonBox *buttonBox =
            new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal,
                                 this);
    layout->addWidget(buttonBox);
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(acceptAdd()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));

    selectionChanged();
}

void ValueEditorAddChildSelection::selectionChanged()
{
    int index = childSelect->currentIndex();
    if (index < 0 || index >= childSelect->count())
        return;
    QVariant v(childSelect->itemData(index));
    if (!v.isValid()) {
        other->show();
        QString text(fallback.metadata("Description").toDisplayString());
        if (!text.isEmpty()) {
            description->setText(text);
            description->show();
        } else {
            description->hide();
        }
        update();
        return;
    }

    QString child(v.toString());

    other->hide();
    QString text;
    {
        auto add = children.find(child.toStdString());
        if (add != children.end())
            text = add->second.metadata("Description").toDisplayString();
    }
    if (!text.isEmpty()) {
        description->setText(text);
        description->show();
    } else {
        description->hide();
    }
    update();
}

void ValueEditorAddChildSelection::acceptAdd()
{
    int index = childSelect->currentIndex();
    if (index < 0 || index >= childSelect->count()) {
        reject();
        return;
    }

    QVariant v(childSelect->itemData(index));
    Variant::Read metadata = Variant::Read::empty();
    if (!v.isValid()) {
        inserted = parent.hash(other->text());
        metadata = fallback;
    } else {
        QString child(v.toString());
        inserted = parent.hash(child);
        auto check = children.find(child.toStdString());
        if (check != children.end())
            metadata = check->second;
    }

    inserted.setEmpty();
    assignNewChild(inserted, metadata);

    accept();
}


}

}
}
}
