/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QVBoxLayout>
#include <QDialog>
#include <QDialogButtonBox>
#include <QVBoxLayout>

#include "guidata/editors/dynamictimeinterval.hxx"
#include "guicore/dynamictimeinterval.hxx"
#include "core/timeutils.hxx"
#include "core/timeparse.hxx"
#include "core/util.hxx"
#include "datacore/variant/composite.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace GUI {
namespace Data {

/** @file guidata/editors/timeinterval.hxx
 * Value editors for dynamic time interval selections.
 */


bool DynamicTimeIntervalEndpoint::matches(const Variant::Read &value, const Variant::Read &metadata)
{
    return Util::equal_insensitive(metadata.metadata("Editor").hash("Type").toString(),
                                   "DynamicTimeInterval");
}

static bool convertTimeInteravalSelection(const Variant::Read &value,
                                          Time::LogicalTimeUnit &units,
                                          int &count,
                                          bool &aligned,
                                          bool &defined)
{
    units = Time::Second;
    count = 0;
    aligned = false;
    defined = true;

    if (!value.exists()) {
        defined = false;
        return true;
    }

    if (value.getType() == Variant::Type::Real || value.getType() == Variant::Type::Integer) {
        qint64 i = value.toInt64();
        count = (int) i;
        if (!INTEGER::defined(i))
            return false;
        return true;
    }

    if (value.getType() == Variant::Type::String) {
        const auto &check = value.toString();
        if (check.empty() || Util::equal_insensitive(check, "undefined", "undef")) {
            defined = false;
            return true;
        } else if (Util::equal_insensitive(check, "none")) {
            return true;
        } else {
            try {
                TimeParse::parseOffset(value.toQString(), &units, &count, &aligned);
            } catch (TimeParsingException tpe) {
                return false;
            }
        }
        return true;
    }

    if (value["Undefined"].toBool()) {
        defined = false;
        return true;
    }

    return Variant::Composite::toTimeInterval(value, &units, &count, &aligned);
}

bool DynamicTimeIntervalEndpoint::valid(const Variant::Read &value, const Variant::Read &metadata)
{
    auto editor = metadata.metadata("Editor");

    Time::LogicalTimeUnit unit;
    int count = 1;
    bool align = false;
    bool defined = false;
    if (!convertTimeInteravalSelection(value, unit, count, align, defined))
        return false;

    if (count == 0) {
        if (!editor.hash("AllowZero").toBool())
            return false;
    } else if (count < 0) {
        if (!editor.hash("AllowNegative").toBool())
            return false;
    }

    if (align) {
        if (!editor.hash("DisableAlign").toBool())
            return false;
    }

    if (!defined) {
        if (!editor.hash("AllowUndefined").toBool())
            return false;
    }

    return ValueEndpointEditor::valid(value, metadata);
}

QString DynamicTimeIntervalEndpoint::collapsedText(const Variant::Read &value,
                                                   const Variant::Read &metadata)
{
    Time::LogicalTimeUnit unit;
    int count = 1;
    bool align = false;
    bool defined = false;
    convertTimeInteravalSelection(value, unit, count, align, defined);

    if (!defined) {
        QString text(metadata.metadata("UndefinedDescription").toDisplayString());
        if (!text.isEmpty())
            return text;
        return ValueEditor::tr("Undefined");
    }

    return Time::describeOffset(unit, count, align);
}

bool DynamicTimeIntervalEndpoint::edit(Variant::Write &value,
                                       const Variant::Read &metadata,
                                       QWidget *parent)
{
    auto editor = metadata.metadata("Editor");

    QDialog dialog(parent);
    dialog.setWindowTitle(QObject::tr("Time Interval"));
    QVBoxLayout *layout = new QVBoxLayout(&dialog);
    dialog.setLayout(layout);

    QLabel *label = new QLabel(metadata.metadata("Description").toDisplayString());
    label->setWordWrap(true);
    layout->addWidget(label);

    DynamicTimeIntervalEditor *selector =
            new DynamicTimeIntervalEditor(value, metadata, editor.hash("DefaultTimeInterval"),
                                          editor, &dialog);
    label->setBuddy(selector);
    layout->addWidget(selector);

    layout->addStretch(1);

    QDialogButtonBox *buttons =
            new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal,
                                 &dialog);
    layout->addWidget(buttons);
    QObject::connect(buttons, SIGNAL(accepted()), &dialog, SLOT(accept()));
    QObject::connect(buttons, SIGNAL(rejected()), &dialog, SLOT(reject()));

    if (dialog.exec() != QDialog::Accepted)
        return false;

    value.setEmpty();
    value.set(selector->timeInterval());
    return true;
}


DynamicTimeIntervalEditor::DynamicTimeIntervalEditor(const Variant::Read &config,
                                                     const Variant::Read &metadata,
                                                     const Variant::Read &defaultConfiguration,
                                                     const CPD3::Data::Variant::Read &editor,
                                                     QWidget *parent) : QWidget(parent)
{
    Time::LogicalTimeUnit unit;
    int count = 1;
    bool align = false;
    bool defined = false;
    if (!convertTimeInteravalSelection(config, unit, count, align, defined))
        convertTimeInteravalSelection(defaultConfiguration, unit, count, align, defined);

    QHBoxLayout *layout = new QHBoxLayout(this);
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    QString labelText(tr("&Enable:"));
    if (editor.hash("EnabledText").exists())
        labelText = editor.hash("EnabledText").toDisplayString();
    enabled = new QCheckBox(labelText, this);
    layout->addWidget(enabled);
    if (!editor.hash("AllowUndefined").toBool()) {
        enabled->setChecked(true);
        enabled->hide();
    } else {
        enabled->show();
        enabled->setChecked(defined);
    }
    connect(enabled, SIGNAL(toggled(bool)), this, SLOT(updateEnabled()));
    QObject::connect(enabled, &QCheckBox::toggled, this, &DynamicTimeIntervalEditor::changed);

    interval = new TimeIntervalSelection(unit, count, align, this);
    interval->setAllowZero(editor.hash("AllowZero").toBool());
    interval->setAllowAlign(!editor.hash("DisableAlign").toBool());
    interval->setAllowNegative(editor.hash("AllowNegative").toBool());
    layout->addWidget(interval, 1);
    QObject::connect(interval, &TimeIntervalSelection::intervalChanged, this,
                     &DynamicTimeIntervalEditor::changed);

    labelText = tr("Disabled");
    if (metadata.metadata("UndefinedDescription").exists())
        labelText = metadata.metadata("UndefinedDescription").toDisplayString();
    undefinedText = new QLabel(labelText, this);
    layout->addWidget(undefinedText, 1);
    undefinedText->hide();

    updateEnabled();
}

void DynamicTimeIntervalEditor::setEnabledText(const QString &text)
{
    enabled->setText(text);
}

void DynamicTimeIntervalEditor::updateEnabled()
{
    if (enabled->isChecked()) {
        interval->show();
        undefinedText->hide();
    } else {
        interval->hide();
        undefinedText->show();
    }
}

Variant::Root DynamicTimeIntervalEditor::timeInterval() const
{
    if (!enabled->isChecked()) {
        return Variant::Root();
    }
    Variant::Root result;
    Variant::Composite::fromTimeInterval(result.write(), interval->getUnit(), interval->getCount(),
                                         interval->getAlign());
    return result;
}

std::unique_ptr<DynamicTimeInterval> DynamicTimeIntervalEditor::create() const
{
    if (!enabled->isChecked()) {
        return std::unique_ptr<DynamicTimeInterval>(new DynamicTimeInterval::Undefined);
    }
    return std::unique_ptr<DynamicTimeInterval>(
            new DynamicTimeInterval::Constant(interval->getUnit(), interval->getCount(),
                                              interval->getAlign()));
}

}
}
}

