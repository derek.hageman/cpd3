/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUIDATAEDITACTION_H
#define CPD3GUIDATAEDITACTION_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QHash>
#include <QSet>
#include <QString>
#include <QWidget>
#include <QVBoxLayout>
#include <QComboBox>
#include <QCheckBox>
#include <QTabWidget>
#include <QTableWidget>
#include <QMenu>
#include <QAction>
#include <QPushButton>
#include <QListWidget>

#include "guidata/guidata.hxx"
#include "datacore/variant/root.hxx"
#include "guicore/dynamictimeinterval.hxx"
#include "guicore/calibrationedit.hxx"
#include "guidata/variableselect.hxx"
#include "guidata/valueeditor.hxx"

namespace CPD3 {
namespace GUI {
namespace Data {

class EditActionEditor;

namespace Internal {

class EditActionType {
public:
    EditActionType();

    virtual ~EditActionType();

    virtual QString getDisplayName() const = 0;

    virtual QString getInternalType() const = 0;

    virtual bool matchesType(const std::string &type) const = 0;

    virtual QString getToolTip() const;

    virtual QString getStatusTip() const;

    virtual QString getWhatsThis() const;

    virtual QWidget *createEditor(CPD3::Data::Variant::Write &target,
                                  EditActionEditor *editor,
                                  const char *updateSlot) const = 0;
};

class EditActionSelectionEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;

    VariableSelect::SequenceMatchDefaults selectionDefaults;
    VariableSelect *selection;
public:
    enum OperatorMode {
        Operate_Target, Operate_Input, Operate_Output,
    };

    EditActionSelectionEditor(CPD3::Data::Variant::Write value,
                              OperatorMode mode,
                              EditActionEditor *editor,
                              QWidget *parent = 0);

    virtual ~EditActionSelectionEditor();

signals:

    void changed();

private slots:

    void selectionChanged();
};

class EditActionInstrumentOverrideEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;

    QComboBox *selection;
    QWidget *instrumentPane;
    QWidget *manualPane;

    VariableSelect::SequenceMatchDefaults selectionDefaults;
    QHash<QString, VariableSelect *> manualSelections;

public:
    struct ParameterData {
        QString configPath;
        QString label;
        QString toolTip;
        QString whatsThis;
    };

    EditActionInstrumentOverrideEditor(CPD3::Data::Variant::Write value,
                                       EditActionEditor *editor,
                                       const QList<ParameterData> &parameters,
                                       QWidget *parent = 0);

    virtual ~EditActionInstrumentOverrideEditor();

signals:

    void changed();

private slots:

    void selectionChanged();
};

class EditActionBooleanEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;
    QString path;
public:
    EditActionBooleanEditor(CPD3::Data::Variant::Write value,
                            const QString &path,
                            const QString &label,
                            const QString &toolTip,
                            const QString &statusTip,
                            const QString &whatsThis,
                            bool defaultState,
                            QWidget *parent = 0);

    virtual ~EditActionBooleanEditor();

signals:

    void changed();

private slots:

    void stateChanged(bool state);
};

class EditActionEnumEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;
    QString path;
    QComboBox *selection;
public:
    EditActionEnumEditor(CPD3::Data::Variant::Write value,
                         const QString &path,
                         const QString &label,
                         const QString &toolTip,
                         const QString &statusTip,
                         const QString &whatsThis,
                         QWidget *parent = 0);

    virtual ~EditActionEnumEditor();

    void addItem(const QString &authoritativeName,
                 const QStringList &aliases,
                 const QString &label,
                 const QString &toolTip,
                 const QString &statusTip,
                 const QString &whatsThis);

signals:

    void changed();

private slots:

    void indexSelected(int index);
};

class EditActionCalibrationEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;

    QCheckBox *enableMinimum;
    QLineEdit *minimumBound;
    QCheckBox *enableMaximum;
    QLineEdit *maximumBound;
public:
    EditActionCalibrationEditor(CPD3::Data::Variant::Write value,
                                bool enableLimits = false,
                                QWidget *parent = 0);

    virtual ~EditActionCalibrationEditor();

signals:

    void changed();

private slots:

    void setCalibration(const CPD3::Calibration &cal);

    void minimumChanged();

    void maximumChanged();
};

class EditActionRecalibrationEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;

    QCheckBox *enableMinimum;
    QLineEdit *minimumBound;
    QCheckBox *enableMaximum;
    QLineEdit *maximumBound;
public:
    EditActionRecalibrationEditor(CPD3::Data::Variant::Write value, QWidget *parent = 0);

    virtual ~EditActionRecalibrationEditor();

signals:

    void changed();

private slots:

    void setCalibration(const CPD3::Calibration &cal);

    void setOriginal(const CPD3::Calibration &cal);

    void minimumChanged();

    void maximumChanged();
};

class EditActionMultiCalibrationPointEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;

    QTableView *table;
    QPushButton *removeButton;

    CalibrationEdit *calibration;

public:
    EditActionMultiCalibrationPointEditor(CPD3::Data::Variant::Write value, QWidget *parent = 0);

    virtual ~EditActionMultiCalibrationPointEditor();

signals:

    void changed();

private slots:

    void addPressed();

    void removePressed();

    void selectedChanged();

    void setCalibration(const CPD3::Calibration &cal);
};

class EditActionMultiCalibrationInterpolationEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;
    QComboBox *method;

    QLineEdit *dX0;
    QLineEdit *dXN;

    QList<QWidget *> details;
public:
    EditActionMultiCalibrationInterpolationEditor(CPD3::Data::Variant::Write value,
                                                  QWidget *parent = 0);

    virtual ~EditActionMultiCalibrationInterpolationEditor();

signals:

    void changed();

private slots:

    void methodChanged(int index);

    void setClampStartEnabled(bool enabled);

    void clampStartChanged(const QString &text);

    void setClampEndEnabled(bool enabled);

    void clampEndChanged(const QString &text);

    void sinChanged(const QString &text);

    void cosChanged(const QString &text);

    void sigmoidChanged(const QString &text);
};

class EditActionWrapModularEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;

    QLineEdit *startBound;
    QLineEdit *endBound;
public:
    EditActionWrapModularEditor(CPD3::Data::Variant::Write value, QWidget *parent = 0);

    virtual ~EditActionWrapModularEditor();

signals:

    void changed();

private slots:

    void startChanged();

    void endChanged();
};

class EditActionContaminateEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;

    QCheckBox *showAdvanced;
    QWidget *basicPane;
    QWidget *advancedPane;
    QLineEdit *originEdit;
    QLineEdit *bitsEdit;
    QLineEdit *descriptionEdit;
public:
    EditActionContaminateEditor(CPD3::Data::Variant::Write value, QWidget *parent = 0);

    virtual ~EditActionContaminateEditor();

signals:

    void changed();

private slots:

    void advancedToggled(bool enable);

    void setOrigin(const QString &origin);

    void setBits(const QString &bits);

    void setDescription(const QString &desc);
};

class EditActionSpotCorrectionEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;

    QLineEdit *original;
public:
    EditActionSpotCorrectionEditor(CPD3::Data::Variant::Write value, QWidget *parent = 0);

    virtual ~EditActionSpotCorrectionEditor();

signals:

    void changed();

private slots:

    void enableOriginalToggled(bool enable);

    void setOriginalValue(const QString &v);

    void setCorrectedValue(const QString &v);
};

class EditActionMultiSpotCorrectionEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;

    QTableWidget *table;
    QPushButton *removeButton;
public:
    EditActionMultiSpotCorrectionEditor(CPD3::Data::Variant::Write value, QWidget *parent = 0);

    virtual ~EditActionMultiSpotCorrectionEditor();

signals:

    void changed();

private slots:

    void addPressed();

    void removePressed();

    void itemChanged(const QModelIndex &topLeft);
};

class EditActionFlagsEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;

    QPushButton *flagsButton;
    QMenu *flagsMenu;
    QHash<QString, QAction *> flagActions;
    QAction *delimiterAction;

public:
    EditActionFlagsEditor(CPD3::Data::Variant::Write value, QWidget *parent = 0);

    virtual ~EditActionFlagsEditor();

signals:

    void changed();

private slots:

    void flagToggled();

    void addFlag();
};

class EditActionFlagsParametersEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;
public:
    EditActionFlagsParametersEditor(CPD3::Data::Variant::Write value, QWidget *parent = 0);

    virtual ~EditActionFlagsParametersEditor();

signals:

    void changed();

private slots:

    void setBits(const QString &bits);

    void setDescription(const QString &desc);
};

class EditActionMatchedFlagsEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;

    QListWidget *list;
    QPushButton *removeButton;

    void writePatterns();

public:
    EditActionMatchedFlagsEditor(CPD3::Data::Variant::Write value, QWidget *parent = 0);

    virtual ~EditActionMatchedFlagsEditor();

signals:

    void changed();

private slots:

    void addPressed();

    void removePressed();

    void itemChanged();

    void selectionChanged();
};

class EditActionCutSizeEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;
    QComboBox *selection;
public:
    EditActionCutSizeEditor(CPD3::Data::Variant::Write value, QWidget *parent = 0);

    virtual ~EditActionCutSizeEditor();

signals:

    void changed();

private slots:

    void indexSelected();

    void manualSelected();
};

class EditActionFlavorsEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;

    QPushButton *flavorsButton;
    QMenu *flavorsMenu;
    QHash<QString, QAction *> flavorsActions;
    QAction *delimiterAction;
public:
    EditActionFlavorsEditor(CPD3::Data::Variant::Write value,
                            EditActionEditor *editor,
                            const QString &label,
                            QWidget *parent = 0);

    virtual ~EditActionFlavorsEditor();

signals:

    void changed();

private slots:

    void flavorToggled();

    void addFlavor();

    void clearFlavors();
};

class EditActionUnitEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;

    QCheckBox *enableStation;
    QComboBox *selectedStation;
    QCheckBox *enableArchive;
    QComboBox *selectedArchive;
    QCheckBox *enableVariable;
    QComboBox *selectedVariable;

    QCheckBox *enableFlavors;
    QPushButton *flavorsButton;
    QMenu *flavorsMenu;
    QHash<QString, QAction *> flavorsActions;
    QAction *delimiterAction;
public:
    EditActionUnitEditor(CPD3::Data::Variant::Write value,
                         EditActionEditor *editor,
                         QWidget *parent = 0);

    virtual ~EditActionUnitEditor();

signals:

    void changed();

private slots:

    void stationChanged();

    void archiveChanged();

    void variableChanged();

    void flavorsChanged();

    void addFlavor();

    void clearFlavors();
};

class EditActionUnitReplaceEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;

    QCheckBox *enableStation;
    QLineEdit *fromStation;
    QLineEdit *toStation;
    QCheckBox *enableArchive;
    QLineEdit *fromArchive;
    QLineEdit *toArchive;
    QCheckBox *enableVariable;
    QLineEdit *fromVariable;
    QLineEdit *toVariable;
    QCheckBox *enableFlavor;
    QLineEdit *fromFlavor;
    QLineEdit *toFlavor;
public:
    EditActionUnitReplaceEditor(CPD3::Data::Variant::Write value, QWidget *parent = 0);

    virtual ~EditActionUnitReplaceEditor();

signals:

    void changed();

private slots:

    void stationChanged();

    void archiveChanged();

    void variableChanged();

    void flavorChanged();
};

class EditActionSetSerialEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;
public:
    EditActionSetSerialEditor(CPD3::Data::Variant::Write value, QWidget *parent = 0);

    virtual ~EditActionSetSerialEditor();

signals:

    void changed();

private slots:

    void setSerial(const QString &serial);
};

class EditActionOverlayValueEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;
    CPD3::Data::Variant::Write editingValue;

    ValueEditor *editor;
public:
    EditActionOverlayValueEditor(CPD3::Data::Variant::Write value, QWidget *parent = 0);

    virtual ~EditActionOverlayValueEditor();

signals:

    void changed();

private slots:

    void setPath(const QString &path);

    void valueChanged();
};

class EditActionFunctionEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;
    QComboBox *operation;
public:
    EditActionFunctionEditor(CPD3::Data::Variant::Write value, QWidget *parent = 0);

    virtual ~EditActionFunctionEditor();

signals:

    void changed();

private slots:

    void setOperation(int index);

    void setPath(const QString &path);
};

class EditActionScriptEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;
public:
    EditActionScriptEditor(CPD3::Data::Variant::Write value, QWidget *parent = 0);

    virtual ~EditActionScriptEditor();

signals:

    void changed();

private slots:

    void setCode(const QString &code);
};

class EditActionAbnormalDataEpisodeEditor : public QWidget {
Q_OBJECT
public:
    EditActionAbnormalDataEpisodeEditor(CPD3::Data::Variant::Write value, QWidget *parent = 0);

    virtual ~EditActionAbnormalDataEpisodeEditor();

signals:

    void changed();
};

};

/**
 * A widget for editing the action taken by an edit directive.
 */
class CPD3GUIDATA_EXPORT EditActionEditor : public QWidget {
Q_OBJECT

    /**
     * This property holds the set of currently available stations that the
     * user can select from.
     * 
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setAvailableStations(const CPD3::Data::SequenceName::ComponentSet &)
     *  <li> CPD3::Data::SequenceName::ComponentSet getAvailableStations() const
     * </ul>
     */
    Q_PROPERTY(CPD3::Data::SequenceName::ComponentSet availableStations
                       READ getAvailableStations
                       WRITE setAvailableStations
                       DESIGNABLE
                       true)
    CPD3::Data::SequenceName::ComponentSet availableStations;

    /**
     * This property holds the set of currently available archives that the
     * user can select from.
     * 
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setAvailableArchives(const CPD3::Data::SequenceName::ComponentSet &)
     *  <li> CPD3::Data::SequenceName::ComponentSet getAvailableArchives() const
     * </ul>
     */
    Q_PROPERTY(CPD3::Data::SequenceName::ComponentSet availableArchives
                       READ getAvailableArchives
                       WRITE setAvailableArchives
                       DESIGNABLE
                       true)
    CPD3::Data::SequenceName::ComponentSet availableArchives;

    /**
    * This property holds the set of currently available variables that the
    * user can select from.
    *
    * <br><br>
    * Access functions:
    * <ul>
    *  <li> void setAvailableVariables(const CPD3::Data::SequenceName::ComponentSet &)
    *  <li> CPD3::Data::SequenceName::ComponentSet getAvailableVariables() const
    * </ul>
    */
    Q_PROPERTY(CPD3::Data::SequenceName::ComponentSet availableVariables
                       READ getAvailableVariables
                       WRITE setAvailableVariables
                       DESIGNABLE
                       true)
    CPD3::Data::SequenceName::ComponentSet availableVariables;

    /**
     * This property holds the set of currently available flavor sets that the
     * user can select from.
     * 
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setAvailableFlavors(const CPD3::Data::SequenceName::ComponentSet &)
     *  <li> CPD3::Data::SequenceName::ComponentSet getAvailableFlavors() const
     * </ul>
     */
    Q_PROPERTY(CPD3::Data::SequenceName::ComponentSet availableFlavors
                       READ getAvailableFlavors
                       WRITE setAvailableFlavors
                       DESIGNABLE
                       true)
    CPD3::Data::SequenceName::ComponentSet availableFlavors;

    CPD3::Data::Variant::Write config;

    QComboBox *actionSelect;
    QList<Internal::EditActionType *> actionTypes;

    QWidget *editorPane;
    QVBoxLayout *editorLayout;
    QWidget *editorContents;

    void availableUpdated();

public:
    EditActionEditor(QWidget *parent = 0);

    virtual ~EditActionEditor();

    const CPD3::Data::Variant::Write &getValue() const;

    void setAvailableStations(const CPD3::Data::SequenceName::ComponentSet &set);

    void setAvailableArchives(const CPD3::Data::SequenceName::ComponentSet &set);

    void setAvailableVariables(const CPD3::Data::SequenceName::ComponentSet &set);

    void setAvailableFlavors(const CPD3::Data::SequenceName::ComponentSet &set);

    const CPD3::Data::SequenceName::ComponentSet &getAvailableStations() const;

    const CPD3::Data::SequenceName::ComponentSet &getAvailableArchives() const;

    const CPD3::Data::SequenceName::ComponentSet &getAvailableVariables() const;

    const CPD3::Data::SequenceName::ComponentSet &getAvailableFlavors() const;

public slots:

    void configure(CPD3::Data::Variant::Write value);

signals:

    /**
     * Emitted whenever the trigger changes.
     */
    void changed();

private slots:

    void editorChanged();

    void typeSelected(int index);
};


/**
 * An editor endpoint for edit actions.
 */
class CPD3GUIDATA_EXPORT EditActionEndpoint : public ValueEndpointEditor {
public:

    bool matches(const CPD3::Data::Variant::Read &value,
                 const CPD3::Data::Variant::Read &metadata) override;

    bool edit(CPD3::Data::Variant::Write &value,
              const CPD3::Data::Variant::Read &metadata,
              QWidget *parent = nullptr) override;

    QString collapsedText(const CPD3::Data::Variant::Read &value,
                          const CPD3::Data::Variant::Read &metadata) override;
};

}
}
}

#endif
