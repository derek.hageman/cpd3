/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QSplitter>
#include <QFormLayout>
#include <QInputDialog>
#include <QDoubleValidator>
#include <QDialog>
#include <QDialogButtonBox>

#include "guidata/editors/edittrigger.hxx"
#include "guicore/scriptedit.hxx"
#include "core/util.hxx"
#include "datacore/variant/composite.hxx"

using namespace CPD3::Data;
using namespace CPD3::GUI::Data::Internal;

namespace CPD3 {
namespace GUI {
namespace Data {

/** @file guidata/editors/edittrigger.hxx
 * An editor for edit directive triggers.
 */

namespace {

enum {
    Role_Value = Qt::UserRole, Role_Type, Role_Format,
};

class TriggerTypeBase : public EditTriggerType {
public:
    TriggerTypeBase() = default;

    virtual ~TriggerTypeBase() = default;

    virtual bool isTrigger() const
    { return true; }
};

static QWidget *createDescriptionWidget(const QString &description)
{
    QWidget *widget = new QWidget;
    QVBoxLayout *layout = new QVBoxLayout;
    widget->setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    QLabel *text = new QLabel(description, widget);
    text->setWordWrap(true);
    layout->addWidget(text);
    layout->insertStretch(-1, 1);

    return widget;
}

class TriggerAlways : public TriggerTypeBase {
public:
    TriggerAlways() = default;

    virtual ~TriggerAlways() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Always"); }

    virtual QString getInternalType() const
    { return "Always"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Always"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Always")));
        item->setToolTip(0, EditTriggerEditor::tr("Unconditionally trigger."));
        item->setStatusTip(0, EditTriggerEditor::tr("Unconditionally trigger"));
        item->setWhatsThis(0,
                           EditTriggerEditor::tr("This trigger item will unconditionally be met."));
    }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        Q_UNUSED(target);
        Q_UNUSED(updateTarget);
        Q_UNUSED(updateSlot);
        return createDescriptionWidget(EditTriggerEditor::tr("This trigger type is always met."));
    }
};

class TriggerNever : public TriggerTypeBase {
public:
    TriggerNever() = default;

    virtual ~TriggerNever() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Disable"); }

    virtual QString getInternalType() const
    { return "Never"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Never", "Disable"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Disable")));
        item->setToolTip(0, EditTriggerEditor::tr("Never trigger."));
        item->setStatusTip(0, EditTriggerEditor::tr("Never trigger"));
        item->setWhatsThis(0, EditTriggerEditor::tr("This trigger item will never be triggered."));
    }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        Q_UNUSED(target);
        Q_UNUSED(updateTarget);
        Q_UNUSED(updateSlot);
        return createDescriptionWidget(EditTriggerEditor::tr(
                "This trigger type is never met.  It can be used to disable a component of "
                        "the whole trigger."));
    }
};

class TriggerChildren : public TriggerTypeBase {
public:
    TriggerChildren() = default;

    virtual ~TriggerChildren() = default;

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        EditTriggerComponentList *result = nullptr;
        switch (target.getType()) {
        case Variant::Type::Array:
            result = new EditTriggerComponentList(target);
            break;
        default:
            result = new EditTriggerComponentList(target.hash("Components"));
            break;
        }
        QObject::connect(result, SIGNAL(changed()), updateTarget, updateSlot);
        return result;
    }

    std::vector<EditTriggerChildData> getTriggerChildren(Variant::Write &data) override
    {
        switch (data.getType()) {
        case Variant::Type::Array: {
            std::vector<EditTriggerChildData> result;
            int index = 1;
            for (auto add : data.toArray()) {
                result.emplace_back(
                        EditTriggerEditor::tr("%1: %%1", "trigger child format").arg(index), add);
                ++index;
            }
            return result;
        }
        default:
            break;
        }

        std::vector<EditTriggerChildData> result;
        int index = 1;
        for (auto add : data.hash("Components").toArray()) {
            result.emplace_back(EditTriggerEditor::tr("%1: %%1", "trigger child format").arg(index),
                                add);
            ++index;
        }
        return result;
    }
};

class TriggerAND : public TriggerChildren {
public:
    TriggerAND() = default;

    virtual ~TriggerAND() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Logical AND"); }

    virtual QString getInternalType() const
    { return "AND"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "AND", "All"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Logical AND")));
        item->setToolTip(0, EditTriggerEditor::tr("Trigger when all conditions are met."));
        item->setStatusTip(0, EditTriggerEditor::tr("Logical AND"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This trigger item will activate when the conditions of all of its children are met."));
    }
};

class TriggerOR : public TriggerChildren {
public:
    TriggerOR() = default;

    virtual ~TriggerOR() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Logical OR"); }

    virtual QString getInternalType() const
    { return "OR"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "OR", "Any"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Logical OR")));
        item->setToolTip(0, EditTriggerEditor::tr("Trigger when any condition is met."));
        item->setStatusTip(0, EditTriggerEditor::tr("Logical OR"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This trigger item will activate when the condition of any of its children is met."));
    }
};

class TriggerPeriodic : public TriggerTypeBase {
public:
    TriggerPeriodic() = default;

    virtual ~TriggerPeriodic() = default;

protected:
    QWidget *createInterval(Variant::Write &target,
                            QObject *updateTarget,
                            const char *updateSlot) const
    {
        EditTriggerIntervalEditor *result = new EditTriggerIntervalEditor(target);
        QObject::connect(result, SIGNAL(changed()), updateTarget, updateSlot);

        QSizePolicy policy(result->sizePolicy());
        policy.setVerticalStretch(0);
        result->setSizePolicy(policy);

        return result;
    }

    inline QWidget *createInterval(Variant::Write &&target,
                                   QObject *updateTarget,
                                   const char *updateSlot) const
    { return createInterval(target, updateTarget, updateSlot); }
};

class TriggerPeriodicMoments : public TriggerPeriodic {
public:
    TriggerPeriodicMoments() = default;

    virtual ~TriggerPeriodicMoments() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Periodic"); }

    virtual QString getInternalType() const
    { return "Periodic"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Periodic", "Moment", "Instant"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        int intervalCount = 1;
        bool intervalAligned = false;
        Time::LogicalTimeUnit intervalUnit =
                Variant::Composite::toTimeInterval(data.hash("Interval"), &intervalCount,
                                                   &intervalAligned);

        item->setText(0, format.arg(EditTriggerEditor::tr("Periodic (%1)").arg(
                Time::describeOffset(intervalUnit, intervalCount, intervalAligned))));
        item->setToolTip(0, EditTriggerEditor::tr(
                "Trigger at specific moments in a periodic interval."));
        item->setStatusTip(0, EditTriggerEditor::tr("Periodic"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This trigger activates at specific moments within a larger periodic range.  For example, it can activate on the 30th minute of each hour."));
    }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        QWidget *widget = new QWidget;
        QVBoxLayout *layout = new QVBoxLayout;
        widget->setLayout(layout);
        layout->setContentsMargins(0, 0, 0, 0);

        QWidget *interval = createInterval(target.hash("Interval"), updateTarget, updateSlot);
        interval->setParent(widget);
        layout->addWidget(interval);

        EditTriggerMomentEditor *moments = new EditTriggerMomentEditor(target, widget);
        layout->addWidget(moments);
        QObject::connect(moments, SIGNAL(changed()), updateTarget, updateSlot);

        QSizePolicy policy(moments->sizePolicy());
        policy.setVerticalStretch(1);
        moments->setSizePolicy(policy);

        return widget;
    }
};

class TriggerPeriodicRange : public TriggerPeriodic {
public:
    TriggerPeriodicRange() = default;

    virtual ~TriggerPeriodicRange() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Periodic Range"); }

    virtual QString getInternalType() const
    { return "PeriodicRange"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "PeriodicRange", "Timerange"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        int intervalCount = 1;
        bool intervalAligned = false;
        Time::LogicalTimeUnit intervalUnit =
                Variant::Composite::toTimeInterval(data.hash("Interval"), &intervalCount,
                                                   &intervalAligned);

        item->setText(0, format.arg(EditTriggerEditor::tr("Periodic Range (%1)").arg(
                Time::describeOffset(intervalUnit, intervalCount, intervalAligned))));
        item->setToolTip(0, EditTriggerEditor::tr(
                "Trigger at specific moments in a periodic interval."));
        item->setStatusTip(0, EditTriggerEditor::tr("Periodic range"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This trigger activates for all time within larger periodic range.  For example, it can activate for the first 30 minutes of the hour."));
    }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        QWidget *widget = new QWidget;
        QVBoxLayout *layout = new QVBoxLayout;
        widget->setLayout(layout);
        layout->setContentsMargins(0, 0, 0, 0);

        QWidget *interval = createInterval(target.hash("Interval"), updateTarget, updateSlot);
        interval->setParent(widget);
        layout->addWidget(interval);

        EditTriggerPeriodicRangeEditor *range = new EditTriggerPeriodicRangeEditor(target, widget);
        layout->addWidget(range);
        QObject::connect(range, SIGNAL(changed()), updateTarget, updateSlot);

        QSizePolicy policy(range->sizePolicy());
        policy.setVerticalStretch(1);
        range->setSizePolicy(policy);

        return widget;
    }
};


class TriggerFlags : public TriggerTypeBase {
public:
    TriggerFlags() = default;

    virtual ~TriggerFlags() = default;

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        EditTriggerFlagsEditor *result = new EditTriggerFlagsEditor(target, updateTarget);
        QObject::connect(result, SIGNAL(changed()), updateTarget, updateSlot);
        return result;
    }
};

static Variant::Flags toFlags(const Variant::Read &config)
{
    switch (config.getType()) {
    case Variant::Type::String: {
        const auto &flag = config.toString();
        if (flag.empty())
            return Variant::Flags();
        return Variant::Flags{flag};
    }
    default:
        break;
    }
    return config.toFlags();
}

class TriggerHasFlags : public TriggerFlags {
public:
    TriggerHasFlags() = default;

    virtual ~TriggerHasFlags() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Flags"); }

    virtual QString getInternalType() const
    { return "HasFlags"; }

    bool matchesType(const std::string &type) const override
    {
        return Util::equal_insensitive(type, "HasFlag", "HasFlags", "Flag", "Flags");
    }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        QString flagsName;
        {
            auto allFlags = toFlags(data["Flags"]);
            if (allFlags.size() == 1)
                flagsName = QString::fromStdString(*allFlags.begin());
        }

        QString varName;
        if (data.hash("Input").toArray().size() == 1)
            varName = data["Input/#0/Variable"].toQString();

        if (!varName.isEmpty() && !flagsName.isEmpty()) {
            item->setText(0, format.arg(
                    EditTriggerEditor::tr("%1 (%2)", "flags both format").arg(varName, flagsName)));
        } else if (!varName.isEmpty()) {
            item->setText(0, format.arg(
                    EditTriggerEditor::tr("Flags (%1)", "flags variable format").arg(varName)));
        } else if (!flagsName.isEmpty()) {
            item->setText(0, format.arg(
                    EditTriggerEditor::tr("Flags (%1)", "flags no variable format").arg(
                            flagsName)));
        } else {
            item->setText(0, format.arg(EditTriggerEditor::tr("Flags")));
        }
        item->setToolTip(0, EditTriggerEditor::tr("Trigger when a value contains flags."));
        item->setStatusTip(0, EditTriggerEditor::tr("Trigger on flag presence"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This will be met when a value contains all the specified flags."));
    }
};

class TriggerLacksFlags : public TriggerFlags {
public:
    TriggerLacksFlags() = default;

    virtual ~TriggerLacksFlags() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Exclude Flags"); }

    virtual QString getInternalType() const
    { return "LacksFlags"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "LacksFlag", "LacksFlags"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        QString flagsName;
        {
            auto allFlags = toFlags(data["Flags"]);
            if (allFlags.size() == 1)
                flagsName = QString::fromStdString(*allFlags.begin());
        }

        QString varName;
        if (data.hash("Input").toArray().size() == 1)
            varName = data["Input/#0/Variable"].toQString();

        if (!varName.isEmpty() && !flagsName.isEmpty()) {
            item->setText(0, format.arg(
                    EditTriggerEditor::tr("%1 (lacks %2)", "flags both format").arg(varName,
                                                                                    flagsName)));
        } else if (!varName.isEmpty()) {
            item->setText(0, format.arg(
                    EditTriggerEditor::tr("Lacks Flags (%1)", "flags variable format").arg(
                            varName)));
        } else if (!flagsName.isEmpty()) {
            item->setText(0, format.arg(
                    EditTriggerEditor::tr("Lacks Flags (%1)", "flags no variable format").arg(
                            flagsName)));
        } else {
            item->setText(0, format.arg(EditTriggerEditor::tr("Lacks Flags")));
        }
        item->setToolTip(0, EditTriggerEditor::tr("Trigger when a value does not contain flags."));
        item->setStatusTip(0, EditTriggerEditor::tr("Trigger on flag absence"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This will be met when a value does not contain any of the specified flags."));
    }
};

class TriggerExists : public TriggerTypeBase {
public:
    TriggerExists() = default;

    virtual ~TriggerExists() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Value Exists"); }

    virtual QString getInternalType() const
    { return "Exists"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Exists"); }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        EditTriggerExistsEditor *result = new EditTriggerExistsEditor(target, updateTarget);
        QObject::connect(result, SIGNAL(changed()), updateTarget, updateSlot);
        return result;
    }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        QString varName(data["Input/#0/Variable"].toQString());
        if (data.hash("Input").toArray().size() == 1 && !varName.isEmpty()) {
            item->setText(0, format.arg(EditTriggerEditor::tr("Exists: %1").arg(varName)));
        } else {
            item->setText(0, format.arg(EditTriggerEditor::tr("Value Exists")));
        }
        item->setToolTip(0, EditTriggerEditor::tr("Trigger when a value exists."));
        item->setStatusTip(0, EditTriggerEditor::tr("Trigger on value existence"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This will be met when a value or path within a complex value exists at all."));
    }
};

class TriggerScript : public TriggerTypeBase {
public:
    TriggerScript() = default;

    virtual ~TriggerScript() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Script"); }

    virtual QString getInternalType() const
    { return "Script"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Script"); }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        EditTriggerScriptEditor *result = new EditTriggerScriptEditor(target, updateTarget);
        QObject::connect(result, SIGNAL(changed()), updateTarget, updateSlot);
        return result;
    }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Script")));
        item->setToolTip(0, EditTriggerEditor::tr("Trigger from generic script evaluation."));
        item->setStatusTip(0, EditTriggerEditor::tr("Generic script trigger"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This trigger is defined by a section of generic CPD3 script code."));
    }
};

class TriggerComparison : public TriggerTypeBase {
public:
    TriggerComparison() = default;

    virtual ~TriggerComparison() = default;


    std::vector<EditTriggerChildData> getInputChildren(Variant::Write &data) override
    {
        return std::vector<EditTriggerChildData>{{EditTriggerEditor::tr("Left: %1",
                                                                        "compare left format"),  data.hash(
                "Left")},
                                                 {EditTriggerEditor::tr("Right: %1",
                                                                        "compare right format"), data.hash(
                                                         "Right")}};
    }
};

class TriggerLessThan : public TriggerComparison {
public:
    TriggerLessThan() = default;

    virtual ~TriggerLessThan() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Less Than"); }

    virtual QString getInternalType() const
    { return "Less"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Less", "LessThan"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Less Than")));
        item->setToolTip(0, EditTriggerEditor::tr(
                "Trigger when the left value is less than the right value.  Both sides must be defined."));
        item->setStatusTip(0, EditTriggerEditor::tr("Less than"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This trigger will activate when the left hand side is strictly less than the right hand side.  If either side is undefined the trigger will not activate."));
    }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        Q_UNUSED(target);
        Q_UNUSED(updateTarget);
        Q_UNUSED(updateSlot);
        return createDescriptionWidget(EditTriggerEditor::tr(
                "This trigger will activate when the left hand side is strictly less than the right hand side.  If either side is undefined the trigger will not activate."));
    }
};

class TriggerLessThanEqual : public TriggerComparison {
public:
    TriggerLessThanEqual() = default;

    virtual ~TriggerLessThanEqual() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Less Than or Equal"); }

    virtual QString getInternalType() const
    { return "LessEqual"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "LessEqual", "LessThanOrEqual"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Less Than or Equal")));
        item->setToolTip(0, EditTriggerEditor::tr(
                "Trigger when the left value is less than or equal to the right value.  Both sides must be defined."));
        item->setStatusTip(0, EditTriggerEditor::tr("Less than or equal"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This trigger will activate when the left hand side is less than or equal to the right hand side.  If either side is undefined the trigger will not activate."));
    }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        Q_UNUSED(target);
        Q_UNUSED(updateTarget);
        Q_UNUSED(updateSlot);
        return createDescriptionWidget(EditTriggerEditor::tr(
                "This trigger will activate when the left hand side is less than or equal to the right hand side.  If either side is undefined the trigger will not activate."));
    }
};

class TriggerGreaterThan : public TriggerComparison {
public:
    TriggerGreaterThan() = default;

    virtual ~TriggerGreaterThan() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Greater Than"); }

    virtual QString getInternalType() const
    { return "Greater"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Greater", "GreaterThan"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Greater Than")));
        item->setToolTip(0, EditTriggerEditor::tr(
                "Trigger when the left value is greater than the right value.  Both sides must be defined."));
        item->setStatusTip(0, EditTriggerEditor::tr("Greater than"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This trigger will activate when the left hand side is strictly greater than the right hand side.  If either side is undefined the trigger will not activate."));
    }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        Q_UNUSED(target);
        Q_UNUSED(updateTarget);
        Q_UNUSED(updateSlot);
        return createDescriptionWidget(EditTriggerEditor::tr(
                "This trigger will activate when the left hand side is strictly greater than the right hand side.  If either side is undefined the trigger will not activate."));
    }
};

class TriggerGreaterThanEqual : public TriggerComparison {
public:
    TriggerGreaterThanEqual() = default;

    virtual ~TriggerGreaterThanEqual() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Greater Than or Equal"); }

    virtual QString getInternalType() const
    { return "GreaterEqual"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "GreaterEqual", "GreaterThanOrEqual"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Greater Than or Equal")));
        item->setToolTip(0, EditTriggerEditor::tr(
                "Trigger when the left value is greater than or equal to the right value.  Both sides must be defined."));
        item->setStatusTip(0, EditTriggerEditor::tr("Greater than or equal"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This trigger will activate when the left hand side is greater than or equal to the right hand side.  If either side is undefined the trigger will not activate."));
    }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        Q_UNUSED(target);
        Q_UNUSED(updateTarget);
        Q_UNUSED(updateSlot);
        return createDescriptionWidget(EditTriggerEditor::tr(
                "This trigger will activate when the left hand side is greater than or equal to the right hand side.  If either side is undefined the trigger will not activate."));
    }
};

class TriggerEqual : public TriggerComparison {
public:
    TriggerEqual() = default;

    virtual ~TriggerEqual() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Equal"); }

    virtual QString getInternalType() const
    { return "Equal"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Equal", "EqualTo"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Equal")));
        item->setToolTip(0, EditTriggerEditor::tr(
                "Trigger when the left and right sides are equal.  Both sides must be defined."));
        item->setStatusTip(0, EditTriggerEditor::tr("Equality"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This trigger will activate when the left and right hand sides are exactly equal.  If either side is undefined the trigger will not activate."));
    }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        Q_UNUSED(target);
        Q_UNUSED(updateTarget);
        Q_UNUSED(updateSlot);
        return createDescriptionWidget(EditTriggerEditor::tr(
                "This trigger will activate when the left and right hand sides are exactly equal.  If either side is undefined the trigger will not activate."));
    }
};

class TriggerNotEqual : public TriggerComparison {
public:
    TriggerNotEqual() = default;

    virtual ~TriggerNotEqual() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Not Equal"); }

    virtual QString getInternalType() const
    { return "NotEqual"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "NotEqual", "NotEqualTo"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Not Equal")));
        item->setToolTip(0, EditTriggerEditor::tr(
                "Trigger when the left and right sides are not equal.  Both sides must be defined."));
        item->setStatusTip(0, EditTriggerEditor::tr("Inequality"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This trigger will activate when the left and right hand sides are not equal.  If either side is undefined the trigger will not activate."));
    }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        Q_UNUSED(target);
        Q_UNUSED(updateTarget);
        Q_UNUSED(updateSlot);
        return createDescriptionWidget(EditTriggerEditor::tr(
                "This trigger will activate when the left and right hand sides are not equal.  If either side is undefined the trigger will not activate."));
    }
};

class TriggerRangeBase : public TriggerTypeBase {
public:
    TriggerRangeBase() = default;

    virtual ~TriggerRangeBase() = default;

    std::vector<EditTriggerChildData> getInputChildren(Variant::Write &data) override
    {
        return std::vector<EditTriggerChildData> {{EditTriggerEditor::tr("Start: %1",
                                                                         "range lower format"), data.hash(
                "Start")},
                                                  {EditTriggerEditor::tr("Value: %1",
                                                                         "range value format"), data.hash(
                                                          "Value")},
                                                  {EditTriggerEditor::tr("End: %1",
                                                                         "range upper"),        data.hash(
                                                          "End")}};
    }
};

class TriggerInsideRange : public TriggerRangeBase {
public:
    TriggerInsideRange() = default;

    virtual ~TriggerInsideRange() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Inside Range"); }

    virtual QString getInternalType() const
    { return "Range"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Range", "InsideRange"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Range")));
        item->setToolTip(0, EditTriggerEditor::tr(
                "Trigger when the middle value is within the range defined by the start and end.  All values must be defined."));
        item->setStatusTip(0, EditTriggerEditor::tr("Inside range"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This trigger will activate when the middle comparison value is greater than the start and less than the end.  If any value is undefined it will not activate."));
    }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        Q_UNUSED(target);
        Q_UNUSED(updateTarget);
        Q_UNUSED(updateSlot);
        return createDescriptionWidget(EditTriggerEditor::tr(
                "This trigger will activate when the middle comparison value is greater than the start and less than the end.  If any value is undefined it will not activate."));
    }
};

class TriggerOutsideRange : public TriggerRangeBase {
public:
    TriggerOutsideRange() = default;

    virtual ~TriggerOutsideRange() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Outside Range"); }

    virtual QString getInternalType() const
    { return "OutsideRange"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "OutsideRange"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Outside Range")));
        item->setToolTip(0, EditTriggerEditor::tr(
                "Trigger when the middle value is outside of the range defined by the start and end.  All values must be defined."));
        item->setStatusTip(0, EditTriggerEditor::tr("Outside range"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This trigger will activate when the middle comparison value is less than the start or greater than the end.  If any value is undefined it will not activate."));
    }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        Q_UNUSED(target);
        Q_UNUSED(updateTarget);
        Q_UNUSED(updateSlot);
        return createDescriptionWidget(EditTriggerEditor::tr(
                "This trigger will activate when the middle comparison value is less than the start or greater than the end.  If any value is undefined it will not activate."));
    }
};

class TriggerModularRangeBase : public TriggerTypeBase {
public:
    TriggerModularRangeBase() = default;

    virtual ~TriggerModularRangeBase() = default;

    std::vector<EditTriggerChildData> getInputChildren(Variant::Write &data) override
    {
        return std::vector<EditTriggerChildData>{{EditTriggerEditor::tr("Start: %1",
                                                                        "range lower format"),         data.hash(
                "Start")},
                                                 {EditTriggerEditor::tr("Value: %1",
                                                                        "range value format"),         data.hash(
                                                         "Value")},
                                                 {EditTriggerEditor::tr("End: %1",
                                                                        "range upper"),                data.hash(
                                                         "End")},
                                                 {EditTriggerEditor::tr("Modular Start: %1",
                                                                        "range modular start format"), data.hash(
                                                         "Modulus").hash("Start")},
                                                 {EditTriggerEditor::tr("Modular Start: %1",
                                                                        "range modular start format"), data.hash(
                                                         "Modulus").hash("End")}};
    }
};

class TriggerInsideModularRange : public TriggerModularRangeBase {
public:
    TriggerInsideModularRange() = default;

    virtual ~TriggerInsideModularRange() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Inside Modular Range"); }

    virtual QString getInternalType() const
    { return "ModularRange"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "ModularRange", "InsideModularRange"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Modular Range")));
        item->setToolTip(0, EditTriggerEditor::tr(
                "Trigger when the middle value is within the range defined by the start and end wrapped to the modular bounds.  All values must be defined."));
        item->setStatusTip(0, EditTriggerEditor::tr("Inside range"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This trigger will activate when the middle comparison value is greater than the start and less than the end.  The start, end, and value are wrapped to the range defined by the modular limits.  If the end is greater than the start the range is assumed to wrap around.  If any value is undefined it will not activate."));
    }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        Q_UNUSED(target);
        Q_UNUSED(updateTarget);
        Q_UNUSED(updateSlot);
        return createDescriptionWidget(EditTriggerEditor::tr(
                "This trigger activates when a value is within a range that is modular or "
                        "circular.  That is, it activates when the value is within the limits defined "
                        "by the start and end but with all values wrapped to the bounds defined by "
                        "the modular limits.  If the end is greater than the start the window is "
                        "wrapped around the total limits."));
    }
};

class TriggerOutsideModularRange : public TriggerModularRangeBase {
public:
    TriggerOutsideModularRange() = default;

    virtual ~TriggerOutsideModularRange() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Outside Modular Range"); }

    virtual QString getInternalType() const
    { return "OutsideModularRange"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "OutsideModularRange"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Outside Modular Range")));
        item->setToolTip(0, EditTriggerEditor::tr(
                "Trigger when the middle value is outside the range defined by the start and end wrapped to the modular bounds.  All values must be defined."));
        item->setStatusTip(0, EditTriggerEditor::tr("Inside range"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This trigger will activate when the middle comparison value less than the start or greater than the end.  The start, end, and value are wrapped to the range defined by the modular limits.  If the end is greater than the start the range is assumed to wrap around.  If any value is undefined it will not activate."));
    }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        Q_UNUSED(target);
        Q_UNUSED(updateTarget);
        Q_UNUSED(updateSlot);
        return createDescriptionWidget(EditTriggerEditor::tr(
                "This trigger activates when a value is outside a range that is modular or "
                        "circular.  That is, it activates when the value is outside the limits defined "
                        "by the start and end but with all values wrapped to the bounds defined by "
                        "the modular limits.  If the end is greater than the start the window is "
                        "wrapped around the total limits."));
    }
};

class TriggerDefined : public TriggerTypeBase {
public:
    TriggerDefined() = default;

    virtual ~TriggerDefined() = default;

    std::vector<EditTriggerChildData> getInputChildren(Variant::Write &data) override
    {
        return std::vector<EditTriggerChildData>{{EditTriggerEditor::tr("%1",
                                                                        "defined value format"), data.hash(
                "Value")}};
    }

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Defined"); }

    virtual QString getInternalType() const
    { return "Defined"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Defined"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Defined")));
        item->setToolTip(0, EditTriggerEditor::tr("Trigger when the value is defined."));
        item->setStatusTip(0, EditTriggerEditor::tr("Defined"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This trigger will activate when the value is defined."));
    }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        Q_UNUSED(target);
        Q_UNUSED(updateTarget);
        Q_UNUSED(updateSlot);
        return createDescriptionWidget(
                EditTriggerEditor::tr("This will activate whenever the value is defined."));
    }
};

class TriggerUndefined : public TriggerTypeBase {
public:
    TriggerUndefined() = default;

    virtual ~TriggerUndefined() = default;

    std::vector<EditTriggerChildData> getInputChildren(Variant::Write &data) override
    {
        return std::vector<EditTriggerChildData>{{EditTriggerEditor::tr("%1",
                                                                        "defined value format"), data.hash(
                "Value")}};
    }

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Undefined"); }

    virtual QString getInternalType() const
    { return "Undefined"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Undefined"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Undefined")));
        item->setToolTip(0, EditTriggerEditor::tr("Trigger when the value is undefined."));
        item->setStatusTip(0, EditTriggerEditor::tr("Undefined"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This trigger will activate when the value is undefined."));
    }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        Q_UNUSED(target);
        Q_UNUSED(updateTarget);
        Q_UNUSED(updateSlot);
        return createDescriptionWidget(
                EditTriggerEditor::tr("This will activate whenever the value is undefined."));
    }
};


class InputTypeBase : public EditTriggerType {
public:
    InputTypeBase() = default;

    virtual ~InputTypeBase() = default;

    virtual bool isTrigger() const
    { return false; }
};

class InputValue : public InputTypeBase {
public:
    InputValue() = default;

    virtual ~InputValue() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Data Value"); }

    virtual QString getInternalType() const
    { return "Value"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Value"); }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        EditTriggerInputValueEditor *result = new EditTriggerInputValueEditor(target, updateTarget);
        QObject::connect(result, SIGNAL(changed()), updateTarget, updateSlot);
        return result;
    }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        QString varName(data["Value/#0/Variable"].toQString());
        if (data.hash("Value").toArray().size() == 1 && !varName.isEmpty()) {
            item->setText(0, format.arg(
                    EditTriggerEditor::tr("%1", "data value format").arg(varName)));
        } else {
            item->setText(0, format.arg(EditTriggerEditor::tr("Data Value")));
        }
        item->setToolTip(0, EditTriggerEditor::tr("Get a numeric value from a value in the data."));
        item->setStatusTip(0, EditTriggerEditor::tr("Data value input"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This input takes its value directly from inputs in the data stream.  Values are converted to numbers with any matching value being used even if it is undefined."));
    }
};

class InputConstant : public InputTypeBase {
public:
    InputConstant() = default;

    virtual ~InputConstant() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Constant"); }

    virtual QString getInternalType() const
    { return "Constant"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Constant"); }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        EditTriggerInputConstantEditor
                *result = new EditTriggerInputConstantEditor(target, updateTarget);
        QObject::connect(result, SIGNAL(changed()), updateTarget, updateSlot);
        return result;
    }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        double dataValue = FP::undefined();
        switch (data.getType()) {
        case Variant::Type::Real:
            dataValue = data.toDouble();
            break;
        case Variant::Type::Integer: {
            qint64 i = data.toInt64();
            if (INTEGER::defined(i))
                dataValue = (double) i;
            break;
        }
        default:
            dataValue = data.hash("Value").toDouble();
            break;
        }
        if (FP::defined(dataValue)) {
            item->setText(0, format.arg(
                    EditTriggerEditor::tr("%1", "constant format").arg(dataValue)));
        } else {
            item->setText(0, format.arg(EditTriggerEditor::tr("Constant Undefined")));
        }
        item->setToolTip(0, EditTriggerEditor::tr("A constant value at all times."));
        item->setStatusTip(0, EditTriggerEditor::tr("Constant input"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This input results in a constant value at all times."));
    }
};

class InputFunction : public InputTypeBase {
public:
    InputFunction() = default;

    virtual ~InputFunction() = default;

    std::vector<EditTriggerChildData> getInputChildren(Variant::Write &data) override
    {
        return std::vector<EditTriggerChildData>{{EditTriggerEditor::tr("%1",
                                                                        "function input format"), data.hash(
                "Input")}};
    }
};

class InputSin : public InputFunction {
public:
    InputSin() = default;

    virtual ~InputSin() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Sin"); }

    virtual QString getInternalType() const
    { return "sin"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "sin"); }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        Q_UNUSED(target);
        Q_UNUSED(updateTarget);
        Q_UNUSED(updateSlot);
        return createDescriptionWidget(EditTriggerEditor::tr(
                "This input returns the result of sin(x) on its single sub-input."));
    }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("sin(x)")));
        item->setToolTip(0, EditTriggerEditor::tr("Take the sin(x) of another value."));
        item->setStatusTip(0, EditTriggerEditor::tr("Sin"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This input returns the sin(x) of another value."));
    }
};

class InputCos : public InputFunction {
public:
    InputCos() = default;

    virtual ~InputCos() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Cos"); }

    virtual QString getInternalType() const
    { return "cos"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "cos"); }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        Q_UNUSED(target);
        Q_UNUSED(updateTarget);
        Q_UNUSED(updateSlot);
        return createDescriptionWidget(EditTriggerEditor::tr(
                "This input returns the result of cos(x) on its single sub-input."));
    }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("cos(x)")));
        item->setToolTip(0, EditTriggerEditor::tr("Take the cos(x) of another value."));
        item->setStatusTip(0, EditTriggerEditor::tr("Sin"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This input returns the cos(x) of another value."));
    }
};

class InputLog : public InputFunction {
public:
    InputLog() = default;

    virtual ~InputLog() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("ln"); }

    virtual QString getInternalType() const
    { return "log"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "log", "ln"); }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        Q_UNUSED(target);
        Q_UNUSED(updateTarget);
        Q_UNUSED(updateSlot);
        return createDescriptionWidget(EditTriggerEditor::tr(
                "This input returns the natural logarithm of its single sub-input."));
    }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("ln(x)")));
        item->setToolTip(0, EditTriggerEditor::tr("Take the natural logarithm of another value."));
        item->setStatusTip(0, EditTriggerEditor::tr("Natural logarithm"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This input returns natural logarithm of another value.  The result is undefined if the the input is not positive."));
    }
};

class InputLog10 : public InputFunction {
public:
    InputLog10() = default;

    virtual ~InputLog10() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Log\xE2\x82\x81\xE2\x82\x80"); }

    virtual QString getInternalType() const
    { return "log10"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "log10"); }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        Q_UNUSED(target);
        Q_UNUSED(updateTarget);
        Q_UNUSED(updateSlot);
        return createDescriptionWidget(EditTriggerEditor::tr(
                "This input returns the log base 10 of its single sub-input."));
    }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("log\xE2\x82\x81\xE2\x82\x80(x)")));
        item->setToolTip(0, EditTriggerEditor::tr("Take the log base 10 of another value."));
        item->setStatusTip(0, EditTriggerEditor::tr("Base 10 logarithm"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This input returns base 10 log of another value.  The result is undefined if the the input is not positive."));
    }
};

class InputExp : public InputFunction {
public:
    InputExp() = default;

    virtual ~InputExp() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("e^x"); }

    virtual QString getInternalType() const
    { return "exp"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "exp"); }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        Q_UNUSED(target);
        Q_UNUSED(updateTarget);
        Q_UNUSED(updateSlot);
        return createDescriptionWidget(EditTriggerEditor::tr(
                "This input returns the the result of e^(x) of its single sub-input."));
    }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("e^x")));
        item->setToolTip(0, EditTriggerEditor::tr("Raise e to the power of another value."));
        item->setStatusTip(0, EditTriggerEditor::tr("Exponent"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This input returns the result of raising e to the power of another value."));
    }
};

class InputAbs : public InputFunction {
public:
    InputAbs() = default;

    virtual ~InputAbs() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Absolute Value"); }

    virtual QString getInternalType() const
    { return "Abs"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Abs", "Absolute", "AbsoluteValue"); }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        Q_UNUSED(target);
        Q_UNUSED(updateTarget);
        Q_UNUSED(updateSlot);
        return createDescriptionWidget(EditTriggerEditor::tr(
                "This input returns the absolute value of its single sub-input."));
    }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("|x|")));
        item->setToolTip(0, EditTriggerEditor::tr("Take the absolute value."));
        item->setStatusTip(0, EditTriggerEditor::tr("Absolute value"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This input returns the absolute value of its input."));
    }
};

class InputPoly : public InputFunction {
public:
    InputPoly() = default;

    virtual ~InputPoly() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Calibration"); }

    virtual QString getInternalType() const
    { return "Poly"; }

    bool matchesType(const std::string &type) const override
    {
        return Util::equal_insensitive(type, "Poly", "Polynomial", "Cal", "Calibration");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        EditTriggerInputCalibration *result = new EditTriggerInputCalibration(target);
        QObject::connect(result, SIGNAL(changed()), updateTarget, updateSlot);
        return result;
    }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Calibration")));
        item->setToolTip(0, EditTriggerEditor::tr("Apply a calibration polynomial."));
        item->setStatusTip(0, EditTriggerEditor::tr("Calibration polynomial"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This input returns the result of applying a calibration polynomial to its single sub-input.  If the input is undefined the result is also undefined."));
    }
};

class InputPolyInvert : public InputFunction {
public:
    InputPolyInvert() = default;

    virtual ~InputPolyInvert() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Invert Calibration"); }

    virtual QString getInternalType() const
    { return "PolyInvert"; }

    bool matchesType(const std::string &type) const override
    {
        return Util::equal_insensitive(type, "PolyInvert", "PolynomialInvert", "InvertCal",
                                       "InvertCalibration");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        EditTriggerInputCalibration *result = new EditTriggerInputCalibration(target, true);
        QObject::connect(result, SIGNAL(changed()), updateTarget, updateSlot);
        return result;
    }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Inverse Calibration")));
        item->setToolTip(0, EditTriggerEditor::tr("Invert a calibration polynomial."));
        item->setStatusTip(0, EditTriggerEditor::tr("Inverse calibration polynomial"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This input returns the result of applying a backing out a calibration from its single sub-input.  If the input is undefined the result is also undefined."));
    }
};

class InputSimpleBuffer : public InputFunction {
public:
    InputSimpleBuffer() = default;

    virtual ~InputSimpleBuffer() = default;

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        QWidget *widget = new QWidget;
        QVBoxLayout *layout = new QVBoxLayout;
        widget->setLayout(layout);
        layout->setContentsMargins(0, 0, 0, 0);

        EditTriggerInputBufferEditor *editor = new EditTriggerInputBufferEditor(target, widget);
        QObject::connect(editor, SIGNAL(changed()), updateTarget, updateSlot);
        layout->addWidget(editor);

        layout->insertStretch(-1, 1);
        return widget;
    }
};

class InputMean : public InputSimpleBuffer {
public:
    InputMean() = default;

    virtual ~InputMean() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Mean"); }

    virtual QString getInternalType() const
    { return "Mean"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Mean"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Mean")));
        item->setToolTip(0, EditTriggerEditor::tr(
                "Return the un-weighted mean of all values in the buffer."));
        item->setStatusTip(0, EditTriggerEditor::tr("Mean"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This input returns the un-weighted arithmetic mean of all defined values in the buffering window."));
    }
};

class InputSD : public InputSimpleBuffer {
public:
    InputSD() = default;

    virtual ~InputSD() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Standard Deviation"); }

    virtual QString getInternalType() const
    { return "StandardDeviation"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "sd", "StandardDeviation"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Standard Deviation")));
        item->setToolTip(0, EditTriggerEditor::tr(
                "Return the standard deviation of all values in the buffer."));
        item->setStatusTip(0, EditTriggerEditor::tr("Standard Deviation"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This input returns the standard deviation of all defined values in the buffering window."));
    }
};

class InputMedian : public InputSimpleBuffer {
public:
    InputMedian() = default;

    virtual ~InputMedian() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Median"); }

    virtual QString getInternalType() const
    { return "Median"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Median"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Median")));
        item->setToolTip(0,
                         EditTriggerEditor::tr("Return the median of all values in the buffer."));
        item->setStatusTip(0, EditTriggerEditor::tr("Median"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This input returns the median of all defined values in the buffering window."));
    }
};

class InputMinimum : public InputSimpleBuffer {
public:
    InputMinimum() = default;

    virtual ~InputMinimum() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Minimum"); }

    virtual QString getInternalType() const
    { return "Minimum"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Minimum", "Min"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Minimum")));
        item->setToolTip(0, EditTriggerEditor::tr("Return the minimum value within the buffer."));
        item->setStatusTip(0, EditTriggerEditor::tr("Minimum"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This input returns the minimum defined value within the buffering window."));
    }
};

class InputMaximum : public InputSimpleBuffer {
public:
    InputMaximum() = default;

    virtual ~InputMaximum() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Maximum"); }

    virtual QString getInternalType() const
    { return "Maximum"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Maximum", "Max"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Maximum")));
        item->setToolTip(0, EditTriggerEditor::tr("Return the maximum value within the buffer."));
        item->setStatusTip(0, EditTriggerEditor::tr("Maximum"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This input returns the maximum defined value within the buffering window."));
    }
};

class InputSlope : public InputSimpleBuffer {
public:
    InputSlope() = default;

    virtual ~InputSlope() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Slope"); }

    virtual QString getInternalType() const
    { return "Slope"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Slope"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Slope")));
        item->setToolTip(0, EditTriggerEditor::tr("Return the least squares slope of the buffer."));
        item->setStatusTip(0, EditTriggerEditor::tr("Slope"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This input returns the slope determined by a least squares fit of all data within the buffering window in units per second."));
    }
};

class InputLength : public InputSimpleBuffer {
public:
    InputLength() = default;

    virtual ~InputLength() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Duration"); }

    virtual QString getInternalType() const
    { return "Duration"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Length", "Duration", "Elapsed"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Duration")));
        item->setToolTip(0, EditTriggerEditor::tr(
                "Return the duration of time in seconds the buffer spans."));
        item->setStatusTip(0, EditTriggerEditor::tr("Duration"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This input returns the current duration in seconds that the buffer spans.  This does not consider gaps or undefined values in the calculation.  It simply returns the end of the last value minus the start of the first one."));
    }
};

class InputQuantile : public InputFunction {
public:
    InputQuantile() = default;

    virtual ~InputQuantile() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Quantile"); }

    virtual QString getInternalType() const
    { return "Quantile"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Quantile"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        double q = data["Quantile"].toDouble();
        if (FP::defined(q) && q >= 0.0 && q <= 1.0) {
            item->setText(0, format.arg(EditTriggerEditor::tr("Quantile: %1").arg(q)));
        } else {
            item->setText(0, format.arg(EditTriggerEditor::tr("Quantile")));
        }
        item->setToolTip(0, EditTriggerEditor::tr(
                "Return a quantile of all defined values in the buffer."));
        item->setStatusTip(0, EditTriggerEditor::tr("Quantile"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This input returns a quantile of all defined values within the buffering window."));
    }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        QWidget *widget = new QWidget;
        QVBoxLayout *layout = new QVBoxLayout;
        widget->setLayout(layout);
        layout->setContentsMargins(0, 0, 0, 0);

        EditTriggerInputBufferEditor *buffer = new EditTriggerInputBufferEditor(target, widget);
        QObject::connect(buffer, SIGNAL(changed()), updateTarget, updateSlot);
        layout->addWidget(buffer);

        EditTriggerInputQuantileEditor *editor = new EditTriggerInputQuantileEditor(target, widget);
        QObject::connect(editor, SIGNAL(changed()), updateTarget, updateSlot);
        layout->addWidget(editor);

        layout->insertStretch(-1, 1);

        return widget;
    }
};

class InputAverage : public InputTypeBase {
public:
    InputAverage() = default;

    virtual ~InputAverage() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Average"); }

    virtual QString getInternalType() const
    { return "Average"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Average", "Smoothed"); }

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        QWidget *widget = new QWidget;
        QVBoxLayout *layout = new QVBoxLayout;
        widget->setLayout(layout);
        layout->setContentsMargins(0, 0, 0, 0);

        EditTriggerInputBufferEditor *buffer = new EditTriggerInputBufferEditor(target, widget);
        QObject::connect(buffer, SIGNAL(changed()), updateTarget, updateSlot);
        layout->addWidget(buffer);

        EditTriggerInputValueEditor
                *value = new EditTriggerInputValueEditor(target, updateTarget, widget);
        QObject::connect(value, SIGNAL(changed()), updateTarget, updateSlot);
        layout->addWidget(value);

        layout->insertStretch(-1, 1);

        return widget;
    }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        QString varName(data["Value/#0/Variable"].toQString());
        if (data.hash("Value").toArray().size() == 1 && !varName.isEmpty()) {
            item->setText(0, format.arg(EditTriggerEditor::tr("Average: %1").arg(varName)));
        } else {
            item->setText(0, format.arg(EditTriggerEditor::tr("Average")));
        }
        item->setToolTip(0, EditTriggerEditor::tr("Average a numeric value from the data."));
        item->setStatusTip(0, EditTriggerEditor::tr("Average value"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This input averages values from the input data.  The averaging ignores undefined values and correctly handles coverage and relative weighting."));
    }
};


class InputChildren : public InputTypeBase {
public:
    InputChildren() = default;

    virtual ~InputChildren() = default;

    QWidget *createEditor(Variant::Write &target,
                          EditTriggerEditor *updateTarget,
                          const char *updateSlot) const override
    {
        QWidget *widget = new QWidget;
        QVBoxLayout *layout = new QVBoxLayout;
        widget->setLayout(layout);
        layout->setContentsMargins(0, 0, 0, 0);

        EditTriggerComponentList *list = NULL;
        switch (target.getType()) {
        case Variant::Type::Array:
            list = new EditTriggerComponentList(target, widget);
            break;
        default:
            list = new EditTriggerComponentList(target.hash("Inputs"), widget);
            break;
        }
        QObject::connect(list, SIGNAL(changed()), updateTarget, updateSlot);

        QSizePolicy policy(list->sizePolicy());
        policy.setVerticalStretch(0);
        list->setSizePolicy(policy);
        layout->addWidget(list);

        if (enableRequireAll()) {
            EditTriggerInputRequireAllEditor *require =
                    new EditTriggerInputRequireAllEditor(target.hash("RequireAll"), widget);
            QObject::connect(require, SIGNAL(changed()), updateTarget, updateSlot);
            layout->addWidget(require);
        }

        layout->insertStretch(-1, 1);
        return widget;
    }

    std::vector<EditTriggerChildData> getInputChildren(Variant::Write &data) override
    {
        switch (data.getType()) {
        case Variant::Type::Array: {
            std::vector<EditTriggerChildData> result;
            int index = 1;
            for (auto add : data.toArray()) {
                result.emplace_back(
                        EditTriggerEditor::tr("%1: %%1", "input child format").arg(index), add);
                ++index;
            }
            return result;
        }
        default:
            break;
        }

        auto childValue = data.hash("Inputs");
        switch (childValue.getType()) {
        case Variant::Type::Hash: {
            std::vector<EditTriggerChildData> result;
            int index = 1;
            for (auto add : childValue.toHash()) {
                result.emplace_back(
                        EditTriggerEditor::tr("%1: %%1", "input child format").arg(index),
                        add.second);
                ++index;
            }
            return result;
        }
        default:
            break;
        }

        std::vector<EditTriggerChildData> result;
        int index = 1;
        for (auto add : childValue.toArray()) {
            result.emplace_back(EditTriggerEditor::tr("%1: %%1", "input child format").arg(index),
                                add);
            ++index;
        }
        return result;
    }

protected:
    virtual bool enableRequireAll() const
    { return true; }
};

class InputSum : public InputChildren {
public:
    InputSum() = default;

    virtual ~InputSum() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Sum"); }

    virtual QString getInternalType() const
    { return "Sum"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Sum", "Add"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Sum")));
        item->setToolTip(0, EditTriggerEditor::tr("Return the sum of all child inputs."));
        item->setStatusTip(0, EditTriggerEditor::tr("Summation"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This input returns the result of adding all its sub-inputs together.  Undefined values are ignored."));
    }
};

class InputDifference : public InputChildren {
public:
    InputDifference() = default;

    virtual ~InputDifference() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Difference"); }

    virtual QString getInternalType() const
    { return "Difference"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Difference", "Subtract"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Difference")));
        item->setToolTip(0, EditTriggerEditor::tr(
                "Return the result of subtracting the second and subsequent inputs from the first."));
        item->setStatusTip(0, EditTriggerEditor::tr("Difference"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This input returns the result of subtracting the second and all subsequent inputs from the first input.  Undefined values are ignored."));
    }
};

class InputProduct : public InputChildren {
public:
    InputProduct() = default;

    virtual ~InputProduct() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Product"); }

    virtual QString getInternalType() const
    { return "Product"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Product", "Multiply"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Product")));
        item->setToolTip(0, EditTriggerEditor::tr("Return the product of all child inputs."));
        item->setStatusTip(0, EditTriggerEditor::tr("Product"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This input returns the result of multiplying all its sub-inputs together.  Undefined values are ignored."));
    }
};

class InputQuotient : public InputChildren {
public:
    InputQuotient() = default;

    virtual ~InputQuotient() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Quotient"); }

    virtual QString getInternalType() const
    { return "Quotient"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Quotient", "Divide"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Quotient")));
        item->setToolTip(0, EditTriggerEditor::tr(
                "Return the result of successively dividing the first input by all subsequent inputs."));
        item->setStatusTip(0, EditTriggerEditor::tr("Quotient"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This input returns the result of dividing the first input successively by all subsequent inputs.  Undefined values are ignored."));
    }
};

class InputPower : public InputChildren {
public:
    InputPower() = default;

    virtual ~InputPower() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Raise to Power"); }

    virtual QString getInternalType() const
    { return "Power"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Power"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Power")));
        item->setToolTip(0, EditTriggerEditor::tr(
                "Return the result of successively raising the first input to the power of all subsequent inputs."));
        item->setStatusTip(0, EditTriggerEditor::tr("Power"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This input returns the result of successively raising the first input to the power of each subsequent input.  Undefined values are ignored."));
    }
};

class InputLargest : public InputChildren {
public:
    InputLargest() = default;

    virtual ~InputLargest() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Largest"); }

    virtual QString getInternalType() const
    { return "Largest"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Largest"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Largest")));
        item->setToolTip(0, EditTriggerEditor::tr("Return the the largest of all the inputs."));
        item->setStatusTip(0, EditTriggerEditor::tr("Largest"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This input returns the largest single input of its own inputs.  Undefined values are ignored."));
    }
};

class InputSmallest : public InputChildren {
public:
    InputSmallest() = default;

    virtual ~InputSmallest() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("Smallest"); }

    virtual QString getInternalType() const
    { return "Smallest"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Smallest"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("Smallest")));
        item->setToolTip(0, EditTriggerEditor::tr("Return the the smallest of all the inputs."));
        item->setStatusTip(0, EditTriggerEditor::tr("Smallest"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This input returns the smallest single input of its own inputs.  Undefined values are ignored."));
    }
};

class InputFirstValid : public InputChildren {
public:
    InputFirstValid() = default;

    virtual ~InputFirstValid() = default;

    virtual QString getDisplayName() const
    { return EditTriggerEditor::tr("First Valid Value"); }

    virtual QString getInternalType() const
    { return "FirstValid"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "First", "FirstValid", "Valid"); }

    virtual void setTreeItem(const Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item)
    {
        Q_UNUSED(data);
        item->setText(0, format.arg(EditTriggerEditor::tr("First Valid")));
        item->setToolTip(0, EditTriggerEditor::tr("Return the first defined value."));
        item->setStatusTip(0, EditTriggerEditor::tr("Fist Valid Value"));
        item->setWhatsThis(0, EditTriggerEditor::tr(
                "This input returns the first sub-input that is defined at any given time."));
    }

protected:
    virtual bool enableRequireAll() const
    { return false; }
};


}

EditTriggerEditor::EditTriggerEditor(QWidget *parent) : QWidget(parent)
{
    QVBoxLayout *topLayout = new QVBoxLayout;
    setLayout(topLayout);
    topLayout->setContentsMargins(0, 0, 0, 0);

    QSplitter *mainSplitter = new QSplitter(Qt::Horizontal, this);
    topLayout->addWidget(mainSplitter);

    tree = new QTreeWidget(this);
    tree->setHeaderHidden(true);
    tree->setSelectionBehavior(QAbstractItemView::SelectRows);
    tree->setSelectionMode(QAbstractItemView::SingleSelection);
    tree->setToolTip(tr("The components of the trigger."));
    tree->setStatusTip(tr("Trigger component selection"));
    tree->setWhatsThis(
            tr("This is the hierarchy of the components of the trigger.  The logical operation applies to all the children under that operation."));
    connect(tree, SIGNAL(itemClicked(QTreeWidgetItem * , int)), this,
            SLOT(treeClicked(QTreeWidgetItem * )));

    QSizePolicy policy(tree->sizePolicy());
    policy.setHorizontalStretch(0);
    tree->setSizePolicy(policy);
    mainSplitter->addWidget(tree);

    editorWidget = NULL;

    QWidget *contentPane = new QWidget(mainSplitter);
    policy = contentPane->sizePolicy();
    policy.setHorizontalStretch(1);
    contentPane->setSizePolicy(policy);
    mainSplitter->addWidget(contentPane);

    QGridLayout *contentLayout = new QGridLayout;
    contentPane->setLayout(contentLayout);
    contentLayout->setContentsMargins(0, 0, 0, 0);
    contentLayout->setRowStretch(0, 0);
    contentLayout->setRowStretch(1, 0);
    contentLayout->setRowStretch(2, 1);

    QWidget *typePane = new QWidget(contentPane);
    contentLayout->addWidget(typePane, 0, 0);
    QHBoxLayout *typeLayout = new QHBoxLayout;
    typeLayout->setContentsMargins(0, 0, 0, 0);
    typePane->setLayout(typeLayout);

    type = new QComboBox(typePane);
    policy = type->sizePolicy();
    policy.setHorizontalStretch(1);
    policy.setVerticalStretch(0);
    type->setSizePolicy(policy);
    typeLayout->addWidget(type);

    triggerTypes.append(new TriggerAlways);
    triggerTypes.append(new TriggerPeriodicMoments);
    triggerTypes.append(new TriggerPeriodicRange);
    triggerTypes.append(new TriggerHasFlags);
    triggerTypes.append(new TriggerLacksFlags);
    triggerTypes.append(new TriggerLessThan);
    triggerTypes.append(new TriggerLessThanEqual);
    triggerTypes.append(new TriggerGreaterThan);
    triggerTypes.append(new TriggerGreaterThanEqual);
    triggerTypes.append(new TriggerEqual);
    triggerTypes.append(new TriggerNotEqual);
    triggerTypes.append(new TriggerInsideRange);
    triggerTypes.append(new TriggerInsideModularRange);
    triggerTypes.append(new TriggerOutsideRange);
    triggerTypes.append(new TriggerOutsideModularRange);
    triggerTypes.append(new TriggerDefined);
    triggerTypes.append(new TriggerUndefined);
    triggerTypes.append(new TriggerExists);
    triggerTypes.append(new TriggerScript);
    triggerTypes.append(new TriggerAND);
    triggerTypes.append(new TriggerOR);
    triggerTypes.append(new TriggerNever);

    for (QList<EditTriggerType *>::const_iterator add = triggerTypes.constBegin(),
            end = triggerTypes.constEnd(); add != end; ++add) {
        type->addItem((*add)->getDisplayName());
    }
    connect(type, SIGNAL(activated(int)), this, SLOT(triggerTypeActivated(int)));

    invert = new QCheckBox(tr("Invert"), typePane);
    typeLayout->addWidget(invert);
    connect(invert, SIGNAL(toggled(bool)), this, SLOT(invertChanged(bool)));

    input = new QComboBox(typePane);
    input->setVisible(false);
    policy = input->sizePolicy();
    policy.setHorizontalStretch(1);
    policy.setVerticalStretch(0);
    input->setSizePolicy(policy);
    typeLayout->addWidget(input);

    inputTypes.append(new InputValue);
    inputTypes.append(new InputConstant);
    inputTypes.append(new InputSin);
    inputTypes.append(new InputCos);
    inputTypes.append(new InputLog);
    inputTypes.append(new InputLog10);
    inputTypes.append(new InputExp);
    inputTypes.append(new InputAbs);
    inputTypes.append(new InputPoly);
    inputTypes.append(new InputPolyInvert);
    inputTypes.append(new InputMean);
    inputTypes.append(new InputSD);
    inputTypes.append(new InputMedian);
    inputTypes.append(new InputMinimum);
    inputTypes.append(new InputMaximum);
    inputTypes.append(new InputQuantile);
    inputTypes.append(new InputAverage);
    inputTypes.append(new InputSlope);
    inputTypes.append(new InputLength);
    inputTypes.append(new InputLargest);
    inputTypes.append(new InputSmallest);
    inputTypes.append(new InputSum);
    inputTypes.append(new InputDifference);
    inputTypes.append(new InputProduct);
    inputTypes.append(new InputQuotient);
    inputTypes.append(new InputPower);
    inputTypes.append(new InputFirstValid);

    for (QList<EditTriggerType *>::const_iterator add = inputTypes.constBegin(),
            end = inputTypes.constEnd(); add != end; ++add) {
        input->addItem((*add)->getDisplayName());
    }
    connect(input, SIGNAL(activated(int)), this, SLOT(inputTypeActivated(int)));


    extendPane = new QWidget(contentPane);
    contentLayout->addWidget(extendPane, 1, 0, 1, -1);
    QHBoxLayout *extendLayout = new QHBoxLayout;
    extendLayout->setContentsMargins(0, 0, 0, 0);
    extendPane->setLayout(extendLayout);

    extendType = new QComboBox(extendPane);
    extendType->setToolTip(tr("The method the trigger condition is extended in time."));
    extendType->setStatusTip(tr("Trigger extension mode"));
    extendType->setWhatsThis(
            tr("This selects the method that the trigger condition is extended forward and/or backward from the moment it is met."));
    extendType->addItem(tr("No Extension"));
    extendType->addItem(tr("Symmetric Extension"));
    extendType->addItem(tr("Asymmetric Extension"));
    extendLayout->addWidget(extendType);
    connect(extendType, SIGNAL(activated(int)), this, SLOT(extendActivated(int)));

    extendBoth = new TimeIntervalSelection(extendPane);
    extendBoth->setVisible(false);
    extendBoth->setToolTip(
            tr("The amount of time the trigger is extended forward and backward in time."));
    extendBoth->setStatusTip(tr("Trigger time extension"));
    extendBoth->setWhatsThis(
            tr("This is the amount of time the trigger condition is extended forward and backward in time from when it is met."));
    extendLayout->addWidget(extendBoth);
    connect(extendBoth, SIGNAL(intervalChanged(CPD3::Time::LogicalTimeUnit, int, bool)), this,
            SLOT(extendBothChanged(CPD3::Time::LogicalTimeUnit, int, bool)));

    extendBeforeLabel = new QLabel(tr("Before:"), extendPane);
    extendBeforeLabel->setVisible(false);
    extendLayout->addWidget(extendBeforeLabel);

    extendBefore = new TimeIntervalSelection(extendPane);
    extendBeforeLabel->setBuddy(extendBefore);
    extendBefore->setVisible(false);
    extendBefore->setToolTip(tr("The amount of time the trigger is extended backward in time."));
    extendBefore->setStatusTip(tr("Trigger time extension"));
    extendBefore->setWhatsThis(
            tr("This is the amount of time the trigger condition is extended backward in time from when it is met."));
    extendLayout->addWidget(extendBefore);
    connect(extendBefore, SIGNAL(intervalChanged(CPD3::Time::LogicalTimeUnit, int, bool)), this,
            SLOT(extendBeforeChanged(CPD3::Time::LogicalTimeUnit, int, bool)));

    extendAfterLabel = new QLabel(tr("After:"), extendPane);
    extendAfterLabel->setVisible(false);
    extendLayout->addWidget(extendAfterLabel);

    extendAfter = new TimeIntervalSelection(extendPane);
    extendAfterLabel->setBuddy(extendAfter);
    extendAfter->setVisible(false);
    extendAfter->setToolTip(tr("The amount of time the trigger is extended forward in time."));
    extendAfter->setStatusTip(tr("Trigger time extension"));
    extendAfter->setWhatsThis(
            tr("This is the amount of time the trigger condition is extended forward in time from when it is met."));
    extendLayout->addWidget(extendAfter);
    connect(extendAfter, SIGNAL(intervalChanged(CPD3::Time::LogicalTimeUnit, int, bool)), this,
            SLOT(extendAfterChanged(CPD3::Time::LogicalTimeUnit, int, bool)));


    editorPane = new QWidget(contentPane);
    contentLayout->addWidget(editorPane, 2, 0, 1, -1);
    editorLayout = new QVBoxLayout;
    editorLayout->setContentsMargins(0, 0, 0, 0);
    editorPane->setLayout(editorLayout);

    configure(Variant::Write::empty());
}

EditTriggerEditor::~EditTriggerEditor()
{
    qDeleteAll(triggerTypes);
    qDeleteAll(inputTypes);

    if (editorWidget != NULL)
        editorWidget->deleteLater();
}

/**
 * Get the configured trigger value.
 * 
 * @return  the trigger value
 */
const Variant::Write &EditTriggerEditor::getValue() const
{ return config; }

/**
 * Configure the editor with the given value.  This value should be the
 * trigger contents.
 * 
 * @param value     the value to configure with
 */
void EditTriggerEditor::configure(CPD3::Data::Variant::Write value)
{
    config = std::move(value);

    invert->disconnect(this);
    invert->setChecked(config.hash("Invert").toBool());
    connect(invert, SIGNAL(toggled(bool)), this, SLOT(invertChanged(bool)));

    extendType->disconnect(this);

    if (!config.hash("Extend").exists()) {
        extendType->setCurrentIndex(0);
        extendBoth->setVisible(false);
        extendBeforeLabel->setVisible(false);
        extendBefore->setVisible(false);
        extendAfterLabel->setVisible(false);
        extendAfter->setVisible(false);
    } else {
        int count = 1;
        bool align = false;
        Time::LogicalTimeUnit
                unit = Variant::Composite::toTimeInterval(config.hash("Extend"), &count, &align);

        extendBoth->disconnect(this);
        extendBoth->setInterval(unit, count, align);
        connect(extendBoth, SIGNAL(intervalChanged(CPD3::Time::LogicalTimeUnit, int, bool)), this,
                SLOT(extendBothChanged(CPD3::Time::LogicalTimeUnit, int, bool)));

        bool showAsym = false;

        int beforeCount = count;
        bool beforeAlign = align;
        Time::LogicalTimeUnit beforeUnit = unit;
        if (config.hash("Extend").hash("Before").exists()) {
            beforeUnit = Variant::Composite::toTimeInterval(config.hash("Extend").hash("Before"),
                                                            &beforeCount, &beforeAlign);
            showAsym = true;
        }

        extendBefore->disconnect(this);
        extendBefore->setInterval(beforeUnit, beforeCount, beforeAlign);
        connect(extendBefore, SIGNAL(intervalChanged(CPD3::Time::LogicalTimeUnit, int, bool)), this,
                SLOT(extendBeforeChanged(CPD3::Time::LogicalTimeUnit, int, bool)));

        int afterCount = count;
        bool afterAlign = align;
        Time::LogicalTimeUnit afterUnit = unit;
        if (config.hash("Extend").hash("After").exists()) {
            afterUnit = Variant::Composite::toTimeInterval(config.hash("Extend").hash("After"),
                                                           &afterCount, &afterAlign);
            showAsym = true;
        }

        extendAfter->disconnect(this);
        extendAfter->setInterval(afterUnit, afterCount, afterAlign);
        connect(extendAfter, SIGNAL(intervalChanged(CPD3::Time::LogicalTimeUnit, int, bool)), this,
                SLOT(extendAfterChanged(CPD3::Time::LogicalTimeUnit, int, bool)));

        if (showAsym) {
            extendType->setCurrentIndex(2);
            extendBoth->setVisible(false);
            extendBeforeLabel->setVisible(true);
            extendBefore->setVisible(true);
            extendAfterLabel->setVisible(true);
            extendAfter->setVisible(true);
        } else {
            extendType->setCurrentIndex(1);
            extendBoth->setVisible(true);
            extendBeforeLabel->setVisible(false);
            extendBefore->setVisible(false);
            extendAfterLabel->setVisible(false);
            extendAfter->setVisible(false);
        }
    }

    connect(extendType, SIGNAL(activated(int)), this, SLOT(extendActivated(int)));

    if (editorWidget != NULL) {
        editorLayout->removeWidget(editorWidget);
        editorWidget->hide();
        editorWidget->deleteLater();
        editorWidget = NULL;
    }

    tree->clear();
    buildTriggerTree((QTreeWidgetItem *) 0, tr("%1", "toplevel format"), config);

    updated();
}

void EditTriggerEditor::buildTriggerTree(QTreeWidgetItem *parent,
                                         const QString &format, Variant::Write &data)
{
    QTreeWidgetItem *item = new QTreeWidgetItem(parent);
    item->setData(0, Role_Value, QVariant::fromValue<Variant::Write>(data));
    item->setData(0, Role_Format, QVariant::fromValue(format));

    EditTriggerType *type = NULL;
    switch (data.getType()) {
    case Variant::Type::Empty:
        for (QList<EditTriggerType *>::const_iterator check = triggerTypes.constBegin(),
                end = triggerTypes.constEnd(); check != end; ++check) {
            if (!(*check)->matchesType("always"))
                continue;
            type = *check;
            break;
        }
        break;
    case Variant::Type::Boolean:
        if (config.toBool()) {
            for (QList<EditTriggerType *>::const_iterator check = triggerTypes.constBegin(),
                    end = triggerTypes.constEnd(); check != end; ++check) {
                if (!(*check)->matchesType("always"))
                    continue;
                type = *check;
                break;
            }
        } else {
            for (QList<EditTriggerType *>::const_iterator check = triggerTypes.constBegin(),
                    end = triggerTypes.constEnd(); check != end; ++check) {
                if (!(*check)->matchesType("never"))
                    continue;
                type = *check;
                break;
            }
        }
        break;
    case Variant::Type::Array:
        for (QList<EditTriggerType *>::const_iterator check = triggerTypes.constBegin(),
                end = triggerTypes.constEnd(); check != end; ++check) {
            if (!(*check)->matchesType("or"))
                continue;
            type = *check;
            break;
        }
        break;
    default: {
        const auto &checkType = data.hash("Type").toString();
        for (QList<EditTriggerType *>::const_iterator check = triggerTypes.constBegin(),
                end = triggerTypes.constEnd(); check != end; ++check) {
            if (!(*check)->matchesType(checkType))
                continue;
            type = *check;
            break;
        }
        break;
    }
    }

    if (type == NULL)
        type = triggerTypes.first();

    item->setData(0, Role_Type, QVariant::fromValue<void *>(static_cast<void *>(type)));
    type->setTreeItem(data, format, item);

    if (!parent) {
        tree->insertTopLevelItem(0, item);
        treeClicked(item);
    }

    auto children = type->getTriggerChildren(data);
    for (auto &child : children) {
        buildTriggerTree(item, child.first, child.second);
    }
    children = type->getInputChildren(data);
    for (auto &child : children) {
        buildInputTree(item, child.first, child.second);
    }
}

void EditTriggerEditor::buildInputTree(QTreeWidgetItem *parent,
                                       const QString &format, Variant::Write &data)
{
    QTreeWidgetItem *item = new QTreeWidgetItem(parent);
    item->setData(0, Role_Value, QVariant::fromValue<Variant::Write>(data));
    item->setData(0, Role_Format, QVariant::fromValue(format));

    EditTriggerType *type = NULL;
    switch (data.getType()) {
    case Variant::Type::Integer:
    case Variant::Type::Real:
        for (QList<EditTriggerType *>::const_iterator check = inputTypes.constBegin(),
                end = inputTypes.constEnd(); check != end; ++check) {
            if (!(*check)->matchesType("constant"))
                continue;
            type = *check;
            break;
        }
        break;

    case Variant::Type::String:
        for (QList<EditTriggerType *>::const_iterator check = inputTypes.constBegin(),
                end = inputTypes.constEnd(); check != end; ++check) {
            if (!(*check)->matchesType("value"))
                continue;
            type = *check;
            break;
        }
        break;

    case Variant::Type::Array:
        for (QList<EditTriggerType *>::const_iterator check = inputTypes.constBegin(),
                end = inputTypes.constEnd(); check != end; ++check) {
            if (!(*check)->matchesType("firstvalid"))
                continue;
            type = *check;
            break;
        }
        break;
    default: {
        const auto &checkType = data.hash("Type").toString();
        for (QList<EditTriggerType *>::const_iterator check = inputTypes.constBegin(),
                end = inputTypes.constEnd(); check != end; ++check) {
            if (!(*check)->matchesType(checkType))
                continue;
            type = *check;
            break;
        }
        break;
    }
    }

    if (type == NULL)
        type = inputTypes.first();

    item->setData(0, Role_Type, QVariant::fromValue<void *>(static_cast<void *>(type)));
    type->setTreeItem(data, format, item);

    if (!parent)
        tree->insertTopLevelItem(0, item);

    auto children = type->getTriggerChildren(data);
    for (auto &child : children) {
        buildTriggerTree(item, child.first, child.second);
    }
    children = type->getInputChildren(data);
    for (auto &child : children) {
        buildInputTree(item, child.first, child.second);
    }
}

void EditTriggerEditor::invertChanged(bool state)
{
    config.hash("Invert").setBool(state);
    updated();
}

void EditTriggerEditor::extendActivated(int index)
{
    switch (index) {
    default:
        extendBoth->setVisible(false);
        extendBeforeLabel->setVisible(false);
        extendBefore->setVisible(false);
        extendAfterLabel->setVisible(false);
        extendAfter->setVisible(false);

        config.hash("Extend").remove();
        updated();
        break;

    case 1: {
        extendBoth->setVisible(true);
        extendBeforeLabel->setVisible(false);
        extendBefore->setVisible(false);
        extendAfterLabel->setVisible(false);
        extendAfter->setVisible(false);

        config.hash("Extend").setEmpty();

        int count;
        bool align;
        Time::LogicalTimeUnit unit = extendBoth->getInterval(count, align);

        Variant::Composite::fromTimeInterval(config.hash("Extend"), unit, count, align);
        updated();
        break;
    }

    case 2: {
        extendBoth->setVisible(false);
        extendBeforeLabel->setVisible(true);
        extendBefore->setVisible(true);
        extendAfterLabel->setVisible(true);
        extendAfter->setVisible(true);

        config.hash("Extend").setEmpty();

        int count;
        bool align;
        Time::LogicalTimeUnit unit = extendBefore->getInterval(count, align);
        Variant::Composite::fromTimeInterval(config.hash("Extend").hash("Before"), unit, count,
                                             align);

        unit = extendAfter->getInterval(count, align);
        Variant::Composite::fromTimeInterval(config.hash("Extend").hash("After"), unit, count,
                                             align);

        updated();
        break;
    }
    }
}

Variant::Write EditTriggerEditor::selectedTreeItem() const
{
    QList<QTreeWidgetItem *> selected(tree->selectedItems());
    if (selected.isEmpty())
        return config;
    return selected.at(0)->data(0, Role_Value).value<Variant::Write>();
}

void EditTriggerEditor::extendBothChanged(CPD3::Time::LogicalTimeUnit unit, int count, bool align)
{
    Variant::Composite::fromTimeInterval(selectedTreeItem().hash("Extend"), unit, count, align);
    updated();
}

void EditTriggerEditor::extendBeforeChanged(CPD3::Time::LogicalTimeUnit unit, int count, bool align)
{
    Variant::Composite::fromTimeInterval(selectedTreeItem().hash("Extend").hash("Before"), unit,
                                         count, align);
    updated();
}

void EditTriggerEditor::extendAfterChanged(CPD3::Time::LogicalTimeUnit unit, int count, bool align)
{
    Variant::Composite::fromTimeInterval(selectedTreeItem().hash("Extend").hash("After"), unit,
                                         count, align);
    updated();
}

void EditTriggerEditor::updated()
{
    emit changed();
}

void EditTriggerEditor::treeClicked(QTreeWidgetItem *item)
{
    if (editorWidget != NULL) {
        editorLayout->removeWidget(editorWidget);
        editorWidget->hide();
        editorWidget->deleteLater();
        editorWidget = NULL;
    }

    EditTriggerType *type = static_cast<EditTriggerType *>(
            item->data(0, Role_Type).value<void *>());

    auto target = item->data(0, Role_Value).value<Variant::Write>();
    editorWidget = type->createEditor(target, this, SLOT(treeSelectedUpdated()));

    if (editorWidget != NULL) {
        editorWidget->setParent(editorPane);
        editorLayout->addWidget(editorWidget);
    }

    if (type->isTrigger()) {
        extendPane->setVisible(true);
        invert->setVisible(true);
        this->type->setVisible(true);
        input->setVisible(false);

        for (int i = 0; i < triggerTypes.size(); i++) {
            if (triggerTypes.at(i) == type) {
                this->type->setCurrentIndex(i);
                break;
            }
        }
    } else {
        extendPane->setVisible(false);
        invert->setVisible(false);
        this->type->setVisible(false);
        input->setVisible(true);

        for (int i = 0; i < inputTypes.size(); i++) {
            if (inputTypes.at(i) == type) {
                this->input->setCurrentIndex(i);
                break;
            }
        }
    }
}

void EditTriggerEditor::treeSelectedUpdated()
{
    QList<QTreeWidgetItem *> selected(tree->selectedItems());
    if (selected.isEmpty()) {
        QTreeWidgetItem *check = tree->topLevelItem(0);
        if (!check)
            return;
        selected.append(check);
    }
    QTreeWidgetItem *item = selected.at(0);
    auto data = item->data(0, Role_Value).value<Variant::Write>();
    QString format(item->data(0, Role_Format).toString());
    EditTriggerType *type = static_cast<EditTriggerType *>(
            item->data(0, Role_Type).value<void *>());

    type->setTreeItem(data, format, item);

    qDeleteAll(item->takeChildren());

    auto children = type->getTriggerChildren(data);
    for (auto &child : children) {
        buildTriggerTree(item, child.first, child.second);
    }
    children = type->getInputChildren(data);
    for (auto &child : children) {
        buildInputTree(item, child.first, child.second);
    }

    updated();
}

void EditTriggerEditor::triggerTypeActivated(int index)
{
    if (index < 0 || index >= triggerTypes.size())
        return;
    QList<QTreeWidgetItem *> selected(tree->selectedItems());
    if (selected.isEmpty()) {
        QTreeWidgetItem *check = tree->topLevelItem(0);
        if (!check)
            return;
        selected.append(check);
    }
    QTreeWidgetItem *item = selected.at(0);
    auto data = item->data(0, Role_Value).value<Variant::Write>();
    QString format(item->data(0, Role_Format).toString());
    EditTriggerType *type = triggerTypes.at(index);

    item->setData(0, Role_Type, QVariant::fromValue<void *>(static_cast<void *>(type)));
    data.hash("Type").setString(type->getInternalType());
    type->setTreeItem(data, format, item);

    qDeleteAll(item->takeChildren());

    auto children = type->getTriggerChildren(data);
    for (auto &child : children) {
        buildTriggerTree(item, child.first, child.second);
    }
    children = type->getInputChildren(data);
    for (auto &child : children) {
        buildInputTree(item, child.first, child.second);
    }

    treeClicked(item);

    updated();
}

void EditTriggerEditor::inputTypeActivated(int index)
{
    if (index < 0 || index >= inputTypes.size())
        return;
    QList<QTreeWidgetItem *> selected(tree->selectedItems());
    if (selected.isEmpty()) {
        QTreeWidgetItem *check = tree->topLevelItem(0);
        if (!check)
            return;
        selected.append(check);
    }
    QTreeWidgetItem *item = selected.at(0);
    auto data = item->data(0, Role_Value).value<Variant::Write>();
    QString format(item->data(0, Role_Format).toString());
    EditTriggerType *type = inputTypes.at(index);

    item->setData(0, Role_Type, QVariant::fromValue<void *>(static_cast<void *>(type)));
    data.hash("Type").setString(type->getInternalType());
    type->setTreeItem(data, format, item);

    qDeleteAll(item->takeChildren());

    auto children = type->getTriggerChildren(data);
    for (auto &child : children) {
        buildTriggerTree(item, child.first, child.second);
    }
    children = type->getInputChildren(data);
    for (auto &child : children) {
        buildInputTree(item, child.first, child.second);
    }

    treeClicked(item);

    updated();
}


void EditTriggerEditor::availableUpdated()
{
    QList<QTreeWidgetItem *> selected(tree->selectedItems());
    if (selected.isEmpty()) {
        QTreeWidgetItem *check = tree->topLevelItem(0);
        if (!check)
            return;
        selected.append(check);
    }
    QTreeWidgetItem *item = selected.at(0);

    if (editorWidget != NULL) {
        editorLayout->removeWidget(editorWidget);
        editorWidget->hide();
        editorWidget->deleteLater();
        editorWidget = NULL;
    }

    EditTriggerType *type = static_cast<EditTriggerType *>(
            item->data(0, Role_Type).value<void *>());

    auto target = item->data(0, Role_Value).value<Variant::Write>();
    editorWidget = type->createEditor(target, this, SLOT(treeSelectedUpdated()));

    if (editorWidget != NULL) {
        editorWidget->setParent(editorPane);
        editorLayout->addWidget(editorWidget);
    }
}

void EditTriggerEditor::setAvailableStations(const SequenceName::ComponentSet &set)
{
    availableStations = set;
    availableUpdated();
}

void EditTriggerEditor::setAvailableArchives(const SequenceName::ComponentSet &set)
{
    availableArchives = set;
    availableUpdated();
}

void EditTriggerEditor::setAvailableVariables(const SequenceName::ComponentSet &set)
{
    availableVariables = set;
    availableUpdated();
}

void EditTriggerEditor::setAvailableFlavors(const SequenceName::ComponentSet &set)
{
    availableFlavors = set;
    availableUpdated();
}

SequenceName::ComponentSet EditTriggerEditor::getAvailableStations() const
{ return availableStations; }

SequenceName::ComponentSet EditTriggerEditor::getAvailableArchives() const
{ return availableArchives; }

SequenceName::ComponentSet EditTriggerEditor::getAvailableVariables() const
{ return availableVariables; }

SequenceName::ComponentSet EditTriggerEditor::getAvailableFlavors() const
{ return availableFlavors; }


bool EditTriggerEndpoint::matches(const Variant::Read &, const Variant::Read &metadata)
{
    return Util::equal_insensitive(metadata.metadata("Editor").hash("Type").toString(),
                                   "EditTrigger");
}

QString EditTriggerEndpoint::collapsedText(const Variant::Read &value, const Variant::Read &)
{
    switch (value.getType()) {
    case Variant::Type::Empty:
        return EditTriggerEditor::tr("Always");
    case Variant::Type::Boolean:
        if (value.toBool()) {
            return EditTriggerEditor::tr("Always");
        } else {
            return EditTriggerEditor::tr("Never");
        }
    case Variant::Type::Array:
        return EditTriggerEditor::tr("Logical OR");
    default:
        break;
    }

    const auto &type = value.hash("Type").toString();
    if (Util::equal_insensitive(type, "OR", "Any")) {
        return EditTriggerEditor::tr("Logical OR");
    } else if (Util::equal_insensitive(type, "And", "All")) {
        return EditTriggerEditor::tr("Logical AND");
    } else if (Util::equal_insensitive(type, "Never", "Disable")) {
        return EditTriggerEditor::tr("Never");
    } else if (Util::equal_insensitive(type, "Periodic", "Moment", "Instant", "PeriodicRange",
                                       "TimeRange")) {
        return EditTriggerEditor::tr("Periodic");
    } else if (Util::equal_insensitive(type, "Less", "Lessthan", "LessEqual", "LessThanOrEqual",
                                       "Greater", "GreaterThan", "GreaterEqual",
                                       "GreaterThanOrEqual", "Equal", "EqualTo", "NotEqual",
                                       "NotEqualTo")) {
        return EditTriggerEditor::tr("Value comparison");
    } else if (Util::equal_insensitive(type, "Range", "InsideRange", "ModularRange",
                                       "InsideModularRange", "OutsideRange",
                                       "OutsideModularRange")) {
        return EditTriggerEditor::tr("Value range");
    } else if (Util::equal_insensitive(type, "HasFlag", "HasFlags", "Flag", "Flags", "Lacksflag",
                                       "LacksFlags")) {
        return EditTriggerEditor::tr("Flags");
    } else if (Util::equal_insensitive(type, "Exists")) {
        return EditTriggerEditor::tr("Value exists");
    } else if (Util::equal_insensitive(type, "Defined")) {
        return EditTriggerEditor::tr("Value defined");
    } else if (Util::equal_insensitive(type, "Undefined")) {
        return EditTriggerEditor::tr("Value undefined");
    } else if (Util::equal_insensitive(type, "Script")) {
        return EditTriggerEditor::tr("Script");
    }

    return EditTriggerEditor::tr("Always");
}

bool EditTriggerEndpoint::edit(Variant::Write &value,
                               const Variant::Read &metadata,
                               QWidget *parent)
{
    auto editor = metadata.metadata("Editor");

    QDialog dialog(parent);
    dialog.setWindowTitle(QObject::tr("Trigger"));
    auto layout = new QVBoxLayout(&dialog);
    dialog.setLayout(layout);

    QLabel *label = new QLabel(metadata.metadata("Description").toDisplayString());
    label->setWordWrap(true);
    layout->addWidget(label);

    auto trigger = new EditTriggerEditor(&dialog);
    label->setBuddy(trigger);
    layout->addWidget(trigger, 1);

    Variant::Root output(value);
    trigger->configure(output.write());

    auto buttons =
            new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal,
                                 &dialog);
    layout->addWidget(buttons);
    QObject::connect(buttons, &QDialogButtonBox::accepted, &dialog, &QDialog::accept);
    QObject::connect(buttons, &QDialogButtonBox::rejected, &dialog, &QDialog::reject);
    if (dialog.exec() != QDialog::Accepted)
        return false;

    value.setEmpty();
    value.set(output);
    return true;
}


namespace Internal {

EditTriggerType::EditTriggerType() = default;

EditTriggerType::~EditTriggerType() = default;

std::vector<EditTriggerChildData> EditTriggerType::getTriggerChildren(Variant::Write &)
{ return std::vector<EditTriggerChildData>(); }

std::vector<EditTriggerChildData> EditTriggerType::getInputChildren(Variant::Write &)
{ return std::vector<EditTriggerChildData>(); }


EditTriggerComponentList::EditTriggerComponentList(Variant::Write v, QWidget *parent) : QWidget(
        parent), value(std::move(v))
{
    QVBoxLayout *layout = new QVBoxLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    addButton = new QPushButton(tr("&Add"), this);
    layout->addWidget(addButton);
    connect(addButton, SIGNAL(clicked(bool)), this, SLOT(add()));

    removeButton = new QPushButton(tr("&Remove"), this);
    layout->addWidget(removeButton);

    removeMenu = new QMenu(removeButton);
    connect(removeMenu, SIGNAL(triggered(QAction * )), this, SLOT(remove(QAction * )));
    removeButton->setMenu(removeMenu);
    removeButton->setEnabled(false);

    layout->insertStretch(-1, 1);
    rebuildMenu();
}

EditTriggerComponentList::~EditTriggerComponentList() = default;

void EditTriggerComponentList::rebuildMenu()
{
    removeMenu->clear();

    auto children = value.toArray();
    if (children.empty()) {
        removeButton->setEnabled(false);
        return;
    }
    removeButton->setEnabled(true);

    for (int i = 0; i < static_cast<int>(children.size()); i++) {
        QAction *action = new QAction(tr("%1", "remove menu entry").arg(i + 1), removeMenu);
        action->setProperty("remove-index", i);
        removeMenu->addAction(action);
    }
}

void EditTriggerComponentList::add()
{
    value.toArray().after_back().setEmpty();
    rebuildMenu();
    emit changed();
}

void EditTriggerComponentList::remove(QAction *action)
{
    int index = action->property("remove-index").toInt();
    if (index < 0 || index >= static_cast<int>(value.toArray().size()))
        return;
    value.array(index).remove(false);
    rebuildMenu();
    emit changed();
}

EditTriggerIntervalEditor::EditTriggerIntervalEditor(Variant::Write v, QWidget *parent)
        : TimeIntervalSelection(parent), value(std::move(v))
{
    int count = 1;
    bool aligned = false;
    Time::LogicalTimeUnit unit = Variant::Composite::toTimeInterval(value, &count, &aligned);
    setInterval(unit, count, aligned);
    connect(this, SIGNAL(intervalChanged(CPD3::Time::LogicalTimeUnit, int, bool)), this,
            SLOT(handleInterval(CPD3::Time::LogicalTimeUnit, int, bool)));

    setToolTip(tr("The repeated interval the trigger is applied to."));
    setStatusTip(tr("Repeat interval"));
    setWhatsThis(
            tr("This is the interval the trigger repeated periodically at.  All times of the trigger are defined from the start of one of the repeating intervals."));
}

EditTriggerIntervalEditor::~EditTriggerIntervalEditor() = default;

void EditTriggerIntervalEditor::handleInterval(Time::LogicalTimeUnit unit, int count, bool align)
{
    Variant::Composite::fromTimeInterval(value, unit, count, align);
    emit changed();
}


static void configureTimeUnitSelection(QComboBox *target)
{
    target->addItem(EditTriggerEditor::tr("Milliseconds"),
                    QVariant::fromValue<int>(Time::Millisecond));
    target->addItem(EditTriggerEditor::tr("Seconds"), QVariant::fromValue<int>(Time::Second));
    target->addItem(EditTriggerEditor::tr("Minutes"), QVariant::fromValue<int>(Time::Minute));
    target->addItem(EditTriggerEditor::tr("Hours"), QVariant::fromValue<int>(Time::Hour));
    target->addItem(EditTriggerEditor::tr("Days"), QVariant::fromValue<int>(Time::Day));
    target->addItem(EditTriggerEditor::tr("Weeks"), QVariant::fromValue<int>(Time::Week));
    target->addItem(EditTriggerEditor::tr("Months"), QVariant::fromValue<int>(Time::Month));
    target->addItem(EditTriggerEditor::tr("Quarters"), QVariant::fromValue<int>(Time::Quarter));
    target->addItem(EditTriggerEditor::tr("Years"), QVariant::fromValue<int>(Time::Year));
}

static void setTimeUnitSelection(const QVariant &var, Variant::Write &target)
{
    switch ((Time::LogicalTimeUnit) var.value<int>()) {
    case Time::Millisecond:
        target.setString("Millisecond");
        break;
    case Time::Second:
        target.setString("Second");
        break;
    case Time::Minute:
        target.setString("Minute");
        break;
    case Time::Hour:
        target.setString("Hour");
        break;
    case Time::Day:
        target.setString("Day");
        break;
    case Time::Week:
        target.setString("Week");
        break;
    case Time::Month:
        target.setString("Month");
        break;
    case Time::Quarter:
        target.setString("Quarter");
        break;
    case Time::Year:
        target.setString("Year");
        break;
    }
}

static void setTimeUnitSelection(const QVariant &var, Variant::Write &&target)
{ return setTimeUnitSelection(var, target); }

EditTriggerMomentEditor::EditTriggerMomentEditor(Variant::Write v, QWidget *parent) : QWidget(
        parent), value(std::move(v))
{
    QGridLayout *layout = new QGridLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    setLayout(layout);

    layout->setRowStretch(4, 1);

    momentsDisplay = new QListWidget(this);
    layout->addWidget(momentsDisplay, 0, 0, 5, 1);
    connect(momentsDisplay, SIGNAL(itemSelectionChanged()), this, SLOT(selectionChanged()));

    momentUnit = new QComboBox(this);
    momentUnit->setToolTip(tr("The unit the moments are specified in."));
    momentUnit->setStatusTip(tr("Moment time unit"));
    momentUnit->setWhatsThis(
            tr("This defines the logical time units the moments are specified in from the start of the intervals."));
    configureTimeUnitSelection(momentUnit);
    layout->addWidget(momentUnit, 0, 1, 1, 1);
    momentUnit->setCurrentIndex(momentUnit->findData(QVariant::fromValue<int>(
            Variant::Composite::toTimeInterval(value.hash("MomentUnit")))));
    connect(momentUnit, SIGNAL(currentIndexChanged(int)), this, SLOT(unitChanged(int)));

    entryValue = new QSpinBox(this);
    entryValue->setEnabled(false);
    entryValue->setMinimum(0);
    entryValue->setMaximum(INT_MAX);
    entryValue->setToolTip(tr("The number of units from the start of the interval to trigger at."));
    entryValue->setStatusTip(tr("Periodic moment"));
    entryValue->setWhatsThis(
            tr("This is the number of logical time units from the start of the interval that the trigger activates at."));
    layout->addWidget(entryValue, 1, 1, 1, 1);

    addButton = new QPushButton(tr("&Add"), this);
    addButton->setToolTip(tr("Add another triggering moment."));
    addButton->setStatusTip(tr("Add moment"));
    addButton->setWhatsThis(tr("Use this to add another moment that the trigger activates at."));
    layout->addWidget(addButton, 2, 1, 1, 1);
    connect(addButton, SIGNAL(clicked(bool)), this, SLOT(addMoment()));

    removeButton = new QPushButton(tr("&Remove"), this);
    removeButton->setEnabled(false);
    removeButton->setToolTip(tr("Remove the selected triggering moment."));
    removeButton->setStatusTip(tr("Remove moment"));
    removeButton->setWhatsThis(
            tr("Use this to remove the selected moment from the times the trigger activates at."));
    layout->addWidget(removeButton, 3, 1, 1, 1);
    connect(removeButton, SIGNAL(clicked(bool)), this, SLOT(removeMoment()));

    QSet<int> moments;
    for (auto add : value.hash("Moments").toChildren()) {
        qint64 i = add.toInt64();
        if (!INTEGER::defined(i) || i < 0)
            continue;
        moments.insert((int) i);
    }
    if (moments.empty()) {
        qint64 i = value.hash("Moments").toInt64();
        if (INTEGER::defined(i) && i >= 0)
            moments.insert((int) i);
    }
    QList<int> sortedMoments(moments.values());
    std::sort(sortedMoments.begin(), sortedMoments.end());
    for (QList<int>::const_iterator m = sortedMoments.constBegin(), end = sortedMoments.constEnd();
            m != end;
            ++m) {
        QListWidgetItem *item = new QListWidgetItem(tr("%1", "moment list format").arg(*m));
        item->setData(Qt::UserRole, *m);
        momentsDisplay->addItem(item);
    }
}

EditTriggerMomentEditor::~EditTriggerMomentEditor() = default;

void EditTriggerMomentEditor::unitChanged(int index)
{
    setTimeUnitSelection(momentUnit->itemData(index), value.hash("MomentUnit"));
    emit changed();
}

void EditTriggerMomentEditor::selectionChanged()
{
    entryValue->disconnect(this);

    QList<QListWidgetItem *> selected(momentsDisplay->selectedItems());
    if (selected.isEmpty()) {
        removeButton->setEnabled(false);
        entryValue->setEnabled(false);
        return;
    }
    QListWidgetItem *item = selected.at(0);

    removeButton->setEnabled(true);
    entryValue->setEnabled(true);
    entryValue->setValue(item->data(Qt::UserRole).toInt());

    connect(entryValue, SIGNAL(valueChanged(int)), this, SLOT(spinnerChanged(int)));
}

void EditTriggerMomentEditor::rebuildMoments()
{
    value.hash("Moments").setEmpty();
    for (int i = 0; i < momentsDisplay->count(); i++) {
        QListWidgetItem *item = momentsDisplay->item(i);
        value.hash("Moments").toArray().after_back().setInteger(item->data(Qt::UserRole).toInt());
    }
}

void EditTriggerMomentEditor::spinnerChanged(int m)
{
    QList<QListWidgetItem *> selected(momentsDisplay->selectedItems());
    if (selected.isEmpty())
        return;
    QListWidgetItem *item = selected.at(0);

    item->setData(Qt::UserRole, m);
    item->setText(tr("%1", "moment list format").arg(m));

    rebuildMoments();
    emit changed();
}

void EditTriggerMomentEditor::addMoment()
{
    int m = entryValue->value();
    QListWidgetItem *item = new QListWidgetItem(tr("%1", "moment list format").arg(m));
    item->setData(Qt::UserRole, m);
    momentsDisplay->addItem(item);

    momentsDisplay->setCurrentItem(item);

    rebuildMoments();
    emit changed();
}

void EditTriggerMomentEditor::removeMoment()
{
    QList<QListWidgetItem *> selected(momentsDisplay->selectedItems());
    if (selected.isEmpty())
        return;
    QListWidgetItem *item = selected.at(0);
    delete momentsDisplay->takeItem(momentsDisplay->row(item));

    rebuildMoments();
    emit changed();
}

EditTriggerPeriodicRangeEditor::EditTriggerPeriodicRangeEditor(Variant::Write v, QWidget *parent)
        : QWidget(parent), value(std::move(v))
{
    QFormLayout *layout = new QFormLayout;
    layout->setContentsMargins(0, 0, 0, 0);

    TimeIntervalSelection *start = new TimeIntervalSelection(this);
    int startCount = 1;
    bool startAligned = false;
    Time::LogicalTimeUnit startUnit =
            Variant::Composite::toTimeInterval(value.hash("Start"), &startCount, &startAligned);
    start->setInterval(startUnit, startCount, startAligned);
    start->setAllowZero(true);
    connect(start, SIGNAL(intervalChanged(CPD3::Time::LogicalTimeUnit, int, bool)), this,
            SLOT(startChanged(CPD3::Time::LogicalTimeUnit, int, bool)));
    layout->addRow(tr("&Start:"), start);
    start->setToolTip(
            tr("The start of the triggering within the interval.  If greater than the end, the interval wraps."));
    start->setStatusTip(tr("Trigger start"));
    start->setWhatsThis(
            tr("This is the start time from the beginning of the repeat interval that the trigger takes effect.  If this is greater than the end the trigger wraps around from this time until the end of the next interval."));

    TimeIntervalSelection *end = new TimeIntervalSelection(this);
    int endCount = 1;
    bool endAligned = false;
    Time::LogicalTimeUnit
            endUnit = Variant::Composite::toTimeInterval(value.hash("End"), &endCount, &endAligned);
    end->setInterval(endUnit, endCount, endAligned);
    end->setAllowZero(true);
    connect(end, SIGNAL(intervalChanged(CPD3::Time::LogicalTimeUnit, int, bool)), this,
            SLOT(endChanged(CPD3::Time::LogicalTimeUnit, int, bool)));
    layout->addRow(tr("&End:"), end);
    end->setToolTip(
            tr("The end of the triggering within the interval.  If less than the start, the interval wraps."));
    end->setStatusTip(tr("Trigger end"));
    end->setWhatsThis(
            tr("This is the end time from the beginning of the repeat interval that the trigger takes effect.  If this is less than the start the trigger wraps around from the start time in the previous interval until this time."));

    setLayout(layout);
}

EditTriggerPeriodicRangeEditor::~EditTriggerPeriodicRangeEditor() = default;

void EditTriggerPeriodicRangeEditor::startChanged(Time::LogicalTimeUnit unit, int count, bool align)
{
    Variant::Composite::fromTimeInterval(value.hash("Start"), unit, count, align);
    emit changed();
}

void EditTriggerPeriodicRangeEditor::endChanged(Time::LogicalTimeUnit unit, int count, bool align)
{
    Variant::Composite::fromTimeInterval(value.hash("End"), unit, count, align);
    emit changed();
}

EditTriggerFlagsEditor::EditTriggerFlagsEditor(Variant::Write v,
                                               EditTriggerEditor *editor, QWidget *parent)
        : QWidget(parent), value(std::move(v))
{
    QGridLayout *layout = new QGridLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    flagsButton = new QPushButton(tr("&Flags"), this);
    layout->addWidget(flagsButton, 0, 0, 1, -1);
    flagsButton->setToolTip(tr("Flag trigger selection."));
    flagsButton->setStatusTip(tr("Flags"));
    flagsButton->setWhatsThis(
            tr("Use this button to configure the flags the trigger is affected by.  Flags can be checked or unchecked to add or remove them from the effect.  New flags can be added with the option at the bottom."));

    flagsMenu = new QMenu(flagsButton);
    delimiterAction = flagsMenu->addSeparator();
    QAction *addFlagAction = flagsMenu->addAction(tr("&Add"));
    connect(addFlagAction, SIGNAL(triggered(bool)), this, SLOT(addFlag()));

    selection = new VariableSelect(this);
    layout->addWidget(selection, 1, 0, 1, -1);
    selection->setToolTip(tr("The variable to test for flags in."));
    selection->setStatusTip(tr("Input variable"));
    selection->setWhatsThis(
            tr("This selection determines the variables that are converted into flags to be tested."));
    selection->setAvailableStations(editor->getAvailableStations());
    selection->setAvailableArchives(editor->getAvailableArchives());
    selection->setAvailableVariables(editor->getAvailableVariables());
    selection->setAvailableFlavors(editor->getAvailableFlavors());
    selection->configureFromSequenceMatch(value.hash("Input"));
    connect(selection, SIGNAL(selectionChanged()), this, SLOT(selectionChanged()));

    QLabel *pathLabel = new QLabel(tr("Path:"), this);
    layout->addWidget(pathLabel, 2, 0);
    QLineEdit *pathEditor = new QLineEdit(this);
    pathLabel->setBuddy(pathEditor);
    layout->addWidget(pathEditor, 2, 1);
    pathEditor->setToolTip(tr("Path selection within the value."));
    pathEditor->setStatusTip(tr("Flags path"));
    pathEditor->setWhatsThis(
            tr("This is the path that is looked up within the value before performing the flags conversion.  Setting this allows a flag test within a complex value.  For most cases this should be left empty."));
    pathEditor->setText(value.hash("Path").toQString());
    connect(pathEditor, SIGNAL(textEdited(
                                       const QString &)), this, SLOT(setPath(
                                                                             const QString &)));

    auto selectedFlags = toFlags(value.hash("Flags"));
    Util::merge(toFlags(value.hash("Flag")), selectedFlags);
    if (selectedFlags.empty()) {
        connect(flagsButton, SIGNAL(clicked(bool)), this, SLOT(addFlag()));
    } else {
        flagsButton->setMenu(flagsMenu);
        QStringList sorted;
        for (const auto &add : selectedFlags) {
            sorted.push_back(QString::fromStdString(add));
        }
        std::sort(sorted.begin(), sorted.end());
        for (QList<QString>::const_iterator flag = sorted.constBegin(), end = sorted.constEnd();
                flag != end;
                ++flag) {
            QAction *action = new QAction(*flag, flagsMenu);
            flagsMenu->insertAction(delimiterAction, action);
            flagActions.insert(*flag, action);

            action->setCheckable(true);
            action->setChecked(true);

            connect(action, SIGNAL(toggled(bool)), this, SLOT(flagToggled()));
        }
    }
}

EditTriggerFlagsEditor::~EditTriggerFlagsEditor() = default;

void EditTriggerFlagsEditor::addFlag()
{
    bool ok;
    QString flag(QInputDialog::getText(this, tr("Add Flag"), tr("Add flag:"), QLineEdit::Normal,
                                       QString(), &ok));
    if (!ok || flag.isEmpty())
        return;

    QHash<QString, QAction *>::const_iterator check = flagActions.constFind(flag);
    if (check != flagActions.constEnd()) {
        check.value()->setChecked(true);
        return;
    }

    QAction *action = new QAction(flag, flagsMenu);
    flagsMenu->insertAction(delimiterAction, action);
    flagActions.insert(flag, action);

    action->setCheckable(true);
    action->setChecked(true);

    flagsButton->disconnect(this);
    flagsButton->setMenu(flagsMenu);
    connect(action, SIGNAL(toggled(bool)), this, SLOT(flagToggled()));

    flagToggled();
}

void EditTriggerFlagsEditor::flagToggled()
{
    Variant::Flags result;
    for (QHash<QString, QAction *>::const_iterator flag = flagActions.constBegin(),
            endFlag = flagActions.constEnd(); flag != endFlag; ++flag) {
        if (!flag.value()->isChecked())
            continue;
        result.insert(flag.key().toStdString());
    }
    value.hash("Flag").remove();
    value.hash("Flags").setFlags(result);
    emit changed();
}

void EditTriggerFlagsEditor::setPath(const QString &path)
{
    if (path.isEmpty()) {
        value.hash("Path").remove();
    } else {
        value.hash("Path").setString(path);
    }
    emit changed();
}

void EditTriggerFlagsEditor::selectionChanged()
{
    selection->writeSequenceMatch(value.hash("Input"));
    emit changed();
}


EditTriggerExistsEditor::EditTriggerExistsEditor(Variant::Write v,
                                                 EditTriggerEditor *editor, QWidget *parent)
        : QWidget(parent), value(std::move(v))
{
    QGridLayout *layout = new QGridLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    selection = new VariableSelect(this);
    layout->addWidget(selection, 0, 0, 1, -1);
    selection->setToolTip(tr("The variable to check for existence."));
    selection->setStatusTip(tr("Input variable"));
    selection->setWhatsThis(
            tr("This selection determines the variable that is checked for existence."));
    selection->setAvailableStations(editor->getAvailableStations());
    selection->setAvailableArchives(editor->getAvailableArchives());
    selection->setAvailableVariables(editor->getAvailableVariables());
    selection->setAvailableFlavors(editor->getAvailableFlavors());
    selection->configureFromSequenceMatch(value.hash("Input"));
    connect(selection, SIGNAL(selectionChanged()), this, SLOT(selectionChanged()));

    QLabel *pathLabel = new QLabel(tr("Path:"), this);
    layout->addWidget(pathLabel, 1, 0);
    QLineEdit *pathEditor = new QLineEdit(this);
    layout->addWidget(pathEditor, 1, 1, 1, -1);
    pathLabel->setBuddy(pathEditor);
    pathEditor->setToolTip(tr("Path selection within the value."));
    pathEditor->setStatusTip(tr("Exists path"));
    pathEditor->setWhatsThis(
            tr("This is the path that is looked up within the value before checking for existence.  If left empty the value is only required to be present."));
    pathEditor->setText(value.hash("Path").toQString());
    connect(pathEditor, SIGNAL(textEdited(
                                       const QString &)), this, SLOT(setPath(
                                                                             const QString &)));
}

EditTriggerExistsEditor::~EditTriggerExistsEditor() = default;

void EditTriggerExistsEditor::setPath(const QString &path)
{
    if (path.isEmpty()) {
        value.hash("Path").remove();
    } else {
        value.hash("Path").setString(path);
    }
    emit changed();
}

void EditTriggerExistsEditor::selectionChanged()
{
    selection->writeSequenceMatch(value.hash("Input"));
    emit changed();
}

EditTriggerScriptEditor::EditTriggerScriptEditor(Variant::Write v,
                                                 EditTriggerEditor *editor, QWidget *parent)
        : QWidget(parent), value(std::move(v))
{
    QHBoxLayout *topLayout = new QHBoxLayout;
    setLayout(topLayout);
    topLayout->setContentsMargins(0, 0, 0, 0);

    QTabWidget *tabs = new QTabWidget(this);
    topLayout->addWidget(tabs);

    ScriptEdit *script = new ScriptEdit(tabs);
    tabs->addTab(script, tr("&Script"));
    script->setToolTip(
            tr("The script code evaluated by the trigger.  This is a multisegment handler."));
    script->setStatusTip(tr("Script code"));
    script->setWhatsThis(
            tr("This is the CPD3 script code evaluated by the trigger.  The evaluation context is a multisement handler, so access to variables can be done just by using them as a global variable.  The result of the handler must be true for the trigger to activate."));
    script->setCode(value.hash("Code").toQString());
    connect(script, SIGNAL(scriptChanged(
                                   const QString &)), this, SLOT(setCode(
                                                                         const QString &)));


    selection = new VariableSelect(tabs);
    tabs->addTab(selection, tr("&Input"));
    selection->setToolTip(tr("The variables that are used as inputs for the script code."));
    selection->setStatusTip(tr("Script inputs"));
    selection->setWhatsThis(
            tr("This selection determines the variables that the script receives as inputs."));
    selection->setAvailableStations(editor->getAvailableStations());
    selection->setAvailableArchives(editor->getAvailableArchives());
    selection->setAvailableVariables(editor->getAvailableVariables());
    selection->setAvailableFlavors(editor->getAvailableFlavors());
    selection->configureFromSequenceMatch(value.hash("Input"));
    connect(selection, SIGNAL(selectionChanged()), this, SLOT(selectionChanged()));
}

EditTriggerScriptEditor::~EditTriggerScriptEditor() = default;

void EditTriggerScriptEditor::setCode(const QString &code)
{
    value.hash("Code").setString(code);
    emit changed();
}

void EditTriggerScriptEditor::selectionChanged()
{
    selection->writeSequenceMatch(value.hash("Input"));
    emit changed();
}


EditTriggerInputValueEditor::EditTriggerInputValueEditor(Variant::Write v,
                                                         EditTriggerEditor *editor,
                                                         QWidget *parent) : QWidget(parent),
                                                                            value(std::move(v))
{
    QGridLayout *layout = new QGridLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    selection = new VariableSelect(this);
    layout->addWidget(selection, 0, 0, 1, -1);
    selection->setToolTip(tr("The input variable."));
    selection->setStatusTip(tr("Input variable"));
    selection->setWhatsThis(
            tr("This selection determines the variable that gives this input its value."));
    selection->setAvailableStations(editor->getAvailableStations());
    selection->setAvailableArchives(editor->getAvailableArchives());
    selection->setAvailableVariables(editor->getAvailableVariables());
    selection->setAvailableFlavors(editor->getAvailableFlavors());

    switch (value.getType()) {
    case Variant::Type::String:
        selection->configureFromSequenceMatch(value);
        break;
    default:
        selection->configureFromSequenceMatch(value.hash("Value"));
        break;
    }

    QLabel *pathLabel = new QLabel(tr("Path:"), this);
    layout->addWidget(pathLabel, 1, 0);
    QLineEdit *pathEditor = new QLineEdit(this);
    pathLabel->setBuddy(pathEditor);
    layout->addWidget(pathEditor, 1, 1);
    pathEditor->setToolTip(tr("Path selection within the value."));
    pathEditor->setStatusTip(tr("Input path"));
    pathEditor->setWhatsThis(
            tr("This is the path that is looked up within the value before converting it to a number."));
    pathEditor->setText(value.hash("Path").toQString());

    connect(selection, SIGNAL(selectionChanged()), this, SLOT(selectionChanged()));
    connect(pathEditor, SIGNAL(textEdited(
                                       const QString &)), this, SLOT(setPath(
                                                                             const QString &)));
}

EditTriggerInputValueEditor::~EditTriggerInputValueEditor() = default;

void EditTriggerInputValueEditor::setPath(const QString &path)
{
    if (path.isEmpty()) {
        value.hash("Path").remove();
    } else {
        value.hash("Path").setString(path);
    }
    emit changed();
}

void EditTriggerInputValueEditor::selectionChanged()
{
    selection->writeSequenceMatch(value.hash("Value"));
    emit changed();
}

EditTriggerInputConstantEditor::EditTriggerInputConstantEditor(Variant::Write v, QWidget *parent)
        : QWidget(parent), value(std::move(v))
{
    QGridLayout *layout = new QGridLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    defined = new QCheckBox(tr("Value:"), this);
    layout->addWidget(defined, 0, 0);
    defined->setToolTip(tr("The defined state of the value."));
    defined->setStatusTip(tr("Value defined"));
    defined->setWhatsThis(
            tr("If this is unchecked then the result of the input is an undefined value."));

    editor = new QLineEdit(this);
    layout->addWidget(editor, 0, 1);
    editor->setToolTip(tr("The value of the input."));
    editor->setStatusTip(tr("Input value"));
    editor->setWhatsThis(tr("This is the value generated by the input at all times."));
    editor->setValidator(new QDoubleValidator(editor));
    double dataValue = FP::undefined();
    switch (value.getType()) {
    case Variant::Type::Real:
        dataValue = value.toDouble();
        break;
    case Variant::Type::Integer: {
        qint64 i = value.toInt64();
        if (INTEGER::defined(i))
            dataValue = (double) i;
        break;
    }
    default:
        dataValue = value.hash("Value").toDouble();
        break;
    }
    if (FP::defined(dataValue)) {
        editor->setText(QString::number(dataValue));
        defined->setChecked(true);
        editor->setEnabled(true);
    } else {
        defined->setChecked(false);
        editor->setEnabled(false);
    }

    layout->addWidget(new QWidget(this), 1, 0, 1, -1);

    connect(editor, SIGNAL(textEdited(
                                   const QString &)), this, SLOT(handleValue()));
    connect(defined, SIGNAL(toggled(bool)), this, SLOT(handleValue()));
}

EditTriggerInputConstantEditor::~EditTriggerInputConstantEditor() = default;

void EditTriggerInputConstantEditor::handleValue()
{
    if (defined->isChecked()) {
        bool ok = false;
        double v = editor->text().toDouble(&ok);
        if (ok && FP::defined(v)) {
            value.hash("Value").setDouble(v);
        } else {
            value.hash("Value").setDouble(FP::undefined());
        }
        editor->setEnabled(true);
    } else {
        value.hash("Value").setDouble(FP::undefined());
        editor->setEnabled(false);
    }
    emit changed();
}

EditTriggerInputCalibration::EditTriggerInputCalibration(Variant::Write v,
                                                         bool enableLimits,
                                                         QWidget *parent) : QWidget(parent),
                                                                            value(std::move(v)),
                                                                            enableMinimum(nullptr),
                                                                            minimumBound(nullptr)
{
    QGridLayout *layout = new QGridLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setColumnStretch(0, 0);
    layout->setColumnStretch(1, 1);
    layout->setColumnStretch(2, 0);
    layout->setColumnStretch(3, 1);
    layout->setRowStretch(0, 0);
    layout->setRowStretch(1, 0);
    layout->setRowStretch(2, 1);

    CalibrationEdit *editor = new CalibrationEdit(this);
    layout->addWidget(editor, 0, 0, 1, -1);
    editor->setToolTip(tr("The calibration in effect."));
    editor->setStatusTip(tr("Calibration"));
    editor->setWhatsThis(tr("This is the calibration polynomial that is in effect."));
    editor->setCalibration(Variant::Composite::toCalibration(value.hash("Calibration")));
    connect(editor, SIGNAL(calibrationChanged(
                                   const CPD3::Calibration &)), this, SLOT(setCalibration(
                                                                                   const CPD3::Calibration &)));

    layout->addWidget(new QWidget(this), 2, 0, 1, -1);

    if (!enableLimits)
        return;

    enableMinimum = new QCheckBox(tr("Minimum:"), this);
    layout->addWidget(enableMinimum, 1, 0);
    enableMinimum->setToolTip(tr("Enable the minimum bound for the result."));
    enableMinimum->setStatusTip(tr("Enable minimum"));
    enableMinimum->setWhatsThis(
            tr("This enables or disables the application of the minimum bound."));
    enableMinimum->setChecked(FP::defined(value.hash("Minimum").toDouble()));
    connect(enableMinimum, SIGNAL(toggled(bool)), this, SLOT(minimumChanged()));

    minimumBound = new QLineEdit(this);
    layout->addWidget(minimumBound, 1, 1);
    minimumBound->setToolTip(
            tr("The minimum bound for the result.  This is not strictly enforced but is used in calculation."));
    minimumBound->setStatusTip(tr("Minimum bound"));
    minimumBound->setWhatsThis(
            tr("This sets the minimum bound for the result.  The bound is not strictly enforced but is used during the inverse calculation for root selection."));
    minimumBound->setValidator(new QDoubleValidator(minimumBound));
    if (enableMinimum->isChecked()) {
        minimumBound->setText(QString::number(value.hash("Minimum").toDouble()));
        minimumBound->setEnabled(true);
    } else {
        minimumBound->setEnabled(false);
    }
    connect(minimumBound, SIGNAL(textEdited(
                                         const QString &)), this, SLOT(minimumChanged()));

    enableMaximum = new QCheckBox(tr("Maximum:"), this);
    layout->addWidget(enableMaximum, 1, 2);
    enableMaximum->setToolTip(tr("Enable the maximum bound for the result."));
    enableMaximum->setStatusTip(tr("Enable maximum"));
    enableMaximum->setWhatsThis(
            tr("This enables or disables the application of the maximum bound."));
    enableMaximum->setChecked(FP::defined(value.hash("Maximum").toDouble()));
    connect(enableMaximum, SIGNAL(toggled(bool)), this, SLOT(maximumChanged()));

    maximumBound = new QLineEdit(this);
    layout->addWidget(maximumBound, 1, 3);
    maximumBound->setToolTip(
            tr("The maximum bound for the result.  This is not strictly enforced but is used in calculation."));
    maximumBound->setStatusTip(tr("Maximum bound"));
    maximumBound->setWhatsThis(
            tr("This sets the maximum bound for the result.  The bound is not strictly enforced but is used during the inverse calculation for root selection."));
    maximumBound->setValidator(new QDoubleValidator(maximumBound));
    if (enableMaximum->isChecked()) {
        maximumBound->setText(QString::number(value.hash("Maximum").toDouble()));
        maximumBound->setEnabled(true);
    } else {
        maximumBound->setEnabled(false);
    }
    connect(maximumBound, SIGNAL(textEdited(
                                         const QString &)), this, SLOT(maximumChanged()));
}

EditTriggerInputCalibration::~EditTriggerInputCalibration() = default;

void EditTriggerInputCalibration::setCalibration(const Calibration &cal)
{
    Variant::Composite::fromCalibration(value.hash("Calibration"), cal);
    emit changed();
}

void EditTriggerInputCalibration::minimumChanged()
{
    if (enableMinimum == NULL || minimumBound == NULL)
        return;
    if (!enableMinimum->isChecked()) {
        minimumBound->setEnabled(false);
        value.hash("Minimum").remove();
    } else {
        minimumBound->setEnabled(true);
        bool ok = false;
        double v = minimumBound->text().toDouble(&ok);
        if (!ok || !FP::defined(v)) {
            value.hash("Minimum").remove();
        } else {
            value.hash("Minimum").setDouble(v);
        }
    }
    emit changed();
}

void EditTriggerInputCalibration::maximumChanged()
{
    if (enableMaximum == NULL || maximumBound == NULL)
        return;
    if (!enableMaximum->isChecked()) {
        maximumBound->setEnabled(false);
        value.hash("Maximum").remove();
    } else {
        maximumBound->setEnabled(true);
        bool ok = false;
        double v = maximumBound->text().toDouble(&ok);
        if (!ok || !FP::defined(v)) {
            value.hash("Maximum").remove();
        } else {
            value.hash("Maximum").setDouble(v);
        }
    }
    emit changed();
}

EditTriggerInputBufferEditor::EditTriggerInputBufferEditor(Variant::Write v, QWidget *parent)
        : QWidget(parent), value(std::move(v))
{
    QVBoxLayout *layout = new QVBoxLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    QWidget *row = new QWidget(this);
    QHBoxLayout *rowLayout = new QHBoxLayout;
    row->setLayout(rowLayout);
    rowLayout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(row);

    bufferType = new QComboBox(row);
    bufferType->setToolTip(tr("The type of buffering being used."));
    bufferType->setStatusTip(tr("Buffering mode"));
    bufferType->setWhatsThis(
            tr("This selects the method that data is buffered before and after the triggering point."));
    bufferType->addItem(tr("Symmetric Buffering"));
    bufferType->addItem(tr("Asymmetric Buffering"));
    rowLayout->addWidget(bufferType);
    if (value.hash("Before").exists() || value.hash("After").exists()) {
        bufferType->setCurrentIndex(1);
    } else {
        bufferType->setCurrentIndex(0);
    }
    connect(bufferType, SIGNAL(activated(int)), this, SLOT(bufferChanged()));


    bufferBoth = new TimeIntervalSelection(row);
    bufferBoth->setVisible(bufferType->currentIndex() == 0);
    bufferBoth->setToolTip(
            tr("The amount of time buffer extends forward and backward in time around the triggering point."));
    bufferBoth->setStatusTip(tr("Buffer time"));
    bufferBoth->setWhatsThis(
            tr("This is the amount of time around the trigger point being evaluated that the buffer considers.  This extends both forward and backward in time."));
    bufferBoth->setAllowZero(true);
    rowLayout->addWidget(bufferBoth);
    {
        int count = 0;
        bool align = false;
        Time::LogicalTimeUnit
                unit = Variant::Composite::toTimeInterval(value["Interval"], &count, &align);
        if (count >= 0)
            bufferBoth->setInterval(unit, count, align);
    }
    connect(bufferBoth, SIGNAL(intervalChanged(CPD3::Time::LogicalTimeUnit, int, bool)), this,
            SLOT(bufferChanged()));


    bufferBeforeLabel = new QLabel(tr("Before:"), row);
    bufferBeforeLabel->setVisible(bufferType->currentIndex() == 1);
    rowLayout->addWidget(bufferBeforeLabel);
    bufferBefore = new TimeIntervalSelection(row);
    bufferBefore->setVisible(bufferType->currentIndex() == 1);
    bufferBeforeLabel->setBuddy(bufferBefore);
    bufferBefore->setToolTip(
            tr("The amount of time before the triggering point that data is buffered."));
    bufferBefore->setStatusTip(tr("Backwards buffering time"));
    bufferBefore->setWhatsThis(
            tr("This is the amount of time before the data evaluation point that is retained in the buffer."));
    bufferBefore->setAllowZero(true);
    rowLayout->addWidget(bufferBefore);
    {
        int count = 0;
        bool align = false;
        Time::LogicalTimeUnit
                unit = Variant::Composite::toTimeInterval(value["Before"], &count, &align);
        if (count >= 0)
            bufferBefore->setInterval(unit, count, align);
    }
    connect(bufferBefore, SIGNAL(intervalChanged(CPD3::Time::LogicalTimeUnit, int, bool)), this,
            SLOT(bufferChanged()));

    bufferAfterLabel = new QLabel(tr("After:"), row);
    bufferAfterLabel->setVisible(bufferType->currentIndex() == 1);
    rowLayout->addWidget(bufferAfterLabel);
    bufferAfter = new TimeIntervalSelection(row);
    bufferAfter->setVisible(bufferType->currentIndex() == 1);
    bufferAfterLabel->setBuddy(bufferAfter);
    bufferAfter->setToolTip(
            tr("The amount of time after the triggering point that data is buffered."));
    bufferAfter->setStatusTip(tr("Forwards buffering time"));
    bufferAfter->setWhatsThis(
            tr("This is the amount of time after the data evaluation point that is retained in the buffer."));
    bufferAfter->setAllowZero(true);
    rowLayout->addWidget(bufferAfter);
    {
        int count = 0;
        bool align = false;
        Time::LogicalTimeUnit
                unit = Variant::Composite::toTimeInterval(value["After"], &count, &align);
        if (count >= 0)
            bufferAfter->setInterval(unit, count, align);
    }
    connect(bufferAfter, SIGNAL(intervalChanged(CPD3::Time::LogicalTimeUnit, int, bool)), this,
            SLOT(bufferChanged()));


    row = new QWidget(this);
    rowLayout = new QHBoxLayout;
    row->setLayout(rowLayout);
    rowLayout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(row);

    enableGap = new QCheckBox(tr("Gap:"), row);
    rowLayout->addWidget(enableGap);
    enableGap->setToolTip(tr("Enable terminating the buffering on a gap in data."));
    enableGap->setStatusTip(tr("Enable buffer gap handling"));
    enableGap->setWhatsThis(
            tr("When enabled this causes the buffer to treat a gap of the given interval as an end of data and restart on the other side.  When gap handling is disabled or the gap is below the threshold the buffering simply contains data on both sides of the gap if it is within the buffering limits."));
    enableGap->setChecked(value.hash("Gap").exists());
    connect(enableGap, SIGNAL(toggled(bool)), this, SLOT(gapChanged()));

    QSizePolicy policy(enableGap->sizePolicy());
    policy.setHorizontalStretch(0);
    enableGap->setSizePolicy(policy);

    gapInterval = new TimeIntervalSelection(row);
    gapInterval->setEnabled(enableGap->isChecked());
    gapInterval->setToolTip(
            tr("The maximum amount of time between data values before a gap is detected."));
    gapInterval->setStatusTip(tr("Gap time"));
    gapInterval->setWhatsThis(
            tr("This is the maximum amount of time between consecutive data values before a gap is assumed.  That is, if two data values are separated by more than this amount of time the buffer will terminate on the beginning of the gap and resume empty on the end of the gap.  Setting this to zero causes a break on any amount of gap."));
    gapInterval->setAllowZero(true);
    rowLayout->addWidget(gapInterval);
    if (enableGap->isChecked()) {
        int count = 0;
        bool align = false;
        Time::LogicalTimeUnit
                unit = Variant::Composite::toTimeInterval(value["Gap"], &count, &align);
        if (count >= 0)
            gapInterval->setInterval(unit, count, align);
    } else {
        gapInterval->setInterval(Time::Second, 0, false);
    }
    connect(gapInterval, SIGNAL(intervalChanged(CPD3::Time::LogicalTimeUnit, int, bool)), this,
            SLOT(gapChanged()));
}

EditTriggerInputBufferEditor::~EditTriggerInputBufferEditor() = default;

void EditTriggerInputBufferEditor::bufferChanged()
{
    switch (bufferType->currentIndex()) {
    case 0:
    default: {
        value.hash("Before").remove();
        value.hash("After").remove();

        int count = 0;
        bool align = false;
        Time::LogicalTimeUnit unit = bufferBoth->getInterval(&count, &align);
        Variant::Composite::fromTimeInterval(value.hash("Interval"), unit, count, align);

        bufferBoth->setVisible(true);
        bufferBeforeLabel->setVisible(false);
        bufferBefore->setVisible(false);
        bufferAfterLabel->setVisible(false);
        bufferAfter->setVisible(false);
        break;
    }
    case 1: {
        value.hash("Interval").remove();

        int count = 0;
        bool align = false;
        Time::LogicalTimeUnit unit = bufferBefore->getInterval(&count, &align);
        Variant::Composite::fromTimeInterval(value.hash("Before"), unit, count, align);

        count = 0;
        align = false;
        unit = bufferAfter->getInterval(&count, &align);
        Variant::Composite::fromTimeInterval(value.hash("After"), unit, count, align);

        bufferBoth->setVisible(false);
        bufferBeforeLabel->setVisible(true);
        bufferBefore->setVisible(true);
        bufferAfterLabel->setVisible(true);
        bufferAfter->setVisible(true);
        break;
    }
    }

    emit changed();
}

void EditTriggerInputBufferEditor::gapChanged()
{
    if (!enableGap->isChecked()) {
        value.hash("Gap").remove();
        gapInterval->setEnabled(false);
        emit changed();
        return;
    }

    int count = 0;
    bool align = false;
    Time::LogicalTimeUnit unit = gapInterval->getInterval(&count, &align);
    Variant::Composite::fromTimeInterval(value.hash("Gap"), unit, count, align);

    gapInterval->setEnabled(true);

    emit changed();
}

EditTriggerInputQuantileEditor::EditTriggerInputQuantileEditor(Variant::Write v, QWidget *parent)
        : QWidget(parent), value(std::move(v))
{
    QFormLayout *layout = new QFormLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    QDoubleSpinBox *quantile = new QDoubleSpinBox(this);
    layout->addRow(tr("Quantile:"), quantile);
    quantile->setToolTip(tr("The quantile of the data in the buffer."));
    quantile->setStatusTip(tr("Quantile"));
    quantile->setWhatsThis(
            tr("This is the quantile returned of all the defined data in the buffering range.  This ranges from 0.0 (the smallest value) to 1.0 (the largest value)."));
    quantile->setMinimum(0.0);
    quantile->setMaximum(1.0);
    quantile->setDecimals(4);
    quantile->setSingleStep(0.1);
    double q = value.hash("Quantile").toDouble();
    if (FP::defined(q))
        quantile->setValue(q);
    connect(quantile, SIGNAL(valueChanged(double)), this, SLOT(setQuantile(double)));
}

EditTriggerInputQuantileEditor::~EditTriggerInputQuantileEditor() = default;

void EditTriggerInputQuantileEditor::setQuantile(double v)
{
    value.hash("Quantile").setDouble(v);
    emit changed();
}


EditTriggerInputRequireAllEditor::EditTriggerInputRequireAllEditor(Variant::Write v,
                                                                   QWidget *parent) : QWidget(
        parent), value(std::move(v))
{
    QHBoxLayout *layout = new QHBoxLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    QCheckBox *box = new QCheckBox(tr("Require all valid"), this);
    layout->addWidget(box);
    box->setToolTip(tr("Require all inputs to be valid."));
    box->setStatusTip(tr("Require all inputs"));
    box->setWhatsThis(
            tr("If this option is set then the output is undefined if any input is also undefined.  That is, it requires all sub-inputs to be present and defined to produce a valid output."));
    box->setChecked(!value.exists() || value.toBool());
    connect(box, SIGNAL(toggled(bool)), this, SLOT(setState(bool)));
}

EditTriggerInputRequireAllEditor::~EditTriggerInputRequireAllEditor() = default;

void EditTriggerInputRequireAllEditor::setState(bool enabled)
{
    value.setBool(enabled);
    emit changed();
}

};

}
}
}

