/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUIDATAVARIABLEGROUPS_H
#define CPD3GUIDATAVARIABLEGROUPS_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QWidget>
#include <QTableView>
#include <QItemDelegate>

#include "guidata/guidata.hxx"
#include "guidata/valueeditor.hxx"
#include "guicore/setselect.hxx"

namespace CPD3 {
namespace GUI {
namespace Data {

/**
 * A widget to edit time interval selections.
 */
class CPD3GUIDATA_EXPORT AcquisitionVariableGroupsEditor : public QWidget {
Q_OBJECT

    QTableView *table;

    void openAllEditors();

public:

    /**
     * Create the acquisition variable groups editor.
     *
     * @param config    the current configuration
     * @param metadata  the metadata
     * @param editor    the editor data
     * @param parent    the parent widget
     */
    AcquisitionVariableGroupsEditor(const CPD3::Data::Variant::Read &config,
                                    const CPD3::Data::Variant::Read &metadata = CPD3::Data::Variant::Read::empty(),
                                    const CPD3::Data::Variant::Read &editor = CPD3::Data::Variant::Read::empty(),
                                    QWidget *parent = 0);

    /**
     * Get the result of editing.
     *
     * @return  the resulting configuration
     */
    CPD3::Data::Variant::Root variableGroups() const;

private slots:

    void addRow();

    void removeRow();
};

namespace Internal {
class AcquisitionVariableGroupsSelector : public SetSelector {
Q_OBJECT
public:
    AcquisitionVariableGroupsSelector(QWidget *parent = 0);

    virtual ~AcquisitionVariableGroupsSelector();

public slots:

    void groupsChanged();

signals:

    void commitChanged(QWidget *source);
};
}

/**
 * An editor endpoint for acquisition variable groups.
 */
class CPD3GUIDATA_EXPORT AcquisitionVariableGroupsEndpoint : public ValueEndpointEditor {
public:

    virtual bool matches(const CPD3::Data::Variant::Read &value,
                         const CPD3::Data::Variant::Read &metadata);

    bool edit(CPD3::Data::Variant::Write &value,
              const CPD3::Data::Variant::Read &metadata,
              QWidget *parent = 0) override;
};

}
}
}

#endif
