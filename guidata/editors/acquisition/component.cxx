/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <future>
#include <unordered_map>
#include <QtConcurrent>
#include <QVBoxLayout>
#include <QDialog>
#include <QDialogButtonBox>
#include <QHBoxLayout>
#include <QButtonGroup>
#include <QRadioButton>
#include <QFuture>
#include <QFutureWatcher>
#include <QProgressDialog>
#include <QAbstractTableModel>
#include <QHeaderView>
#include <QItemEditorFactory>

#include "guidata/editors/acquisition/component.hxx"
#include "guidata/wizards/iointerfaceselect.hxx"
#include "acquisition/iotarget.hxx"
#include "guicore/setselect.hxx"
#include "core/component.hxx"
#include "core/threading.hxx"
#include "core/util.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "datacore/variant/composite.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace GUI {
namespace Data {

/** @file guidata/editors/acquisition/component.hxx
 * Value editors for acquisition components.
 */


bool AcquisitionComponentEndpoint::matches(const Variant::Read &value,
                                           const Variant::Read &metadata)
{
    return Util::equal_insensitive(metadata.metadata("Editor").hash("Type").toString(),
                                   "AcquisitionComponent");
}

QString AcquisitionComponentEndpoint::collapsedText(const Variant::Read &value,
                                                    const Variant::Read &metadata)
{
    QString component(value.hash("Name").toQString().trimmed());
    QString instrument(value.hash("Instrument").toQString().trimmed());
    if (!component.isEmpty()) {
        if (!instrument.isEmpty()) {
            return QString("%1 (%2)").arg(component, instrument);
        }
        return component;
    }
    if (!instrument.isEmpty())
        return instrument;
    return ValueEndpointEditor::collapsedText(value, metadata);
}

bool AcquisitionComponentEndpoint::edit(Variant::Write &value,
                                        const Variant::Read &metadata,
                                        QWidget *parent)
{
    AcquisitionComponentEditorDialog dialog(value, metadata, parent);
    if (dialog.exec() != QDialog::Accepted)
        return false;
    value.setEmpty();
    value.set(dialog.component());
    return true;
}

AcquisitionComponentPage::AcquisitionComponentPage(const Variant::Read &, QWidget *parent)
        : QWidget(parent)
{ }

AcquisitionComponentPage::~AcquisitionComponentPage() = default;

AcquisitionComponentPageGeneral::AcquisitionComponentPageGeneral(const Variant::Read &value,
                                                                 QWidget *parent)
        : AcquisitionComponentPage(value, parent)
{
    QVBoxLayout *topLevel = new QVBoxLayout(this);
    setLayout(topLevel);

    {
        QWidget *container = new QWidget(this);
        topLevel->addWidget(container);
        QHBoxLayout *line = new QHBoxLayout(container);
        container->setLayout(line);
        line->setContentsMargins(0, 0, 0, 0);

        QButtonGroup *group = new QButtonGroup(container);
        group->setExclusive(true);

        instrumentSelected = new QRadioButton(tr("&Instrument:"), container);
        line->addWidget(instrumentSelected);
        group->addButton(instrumentSelected);

        instrument = new QLineEdit(value["Instrument"].toQString(), container);
        line->addWidget(instrument, 1);
        instrument->setToolTip(tr("Instrument code entry (e.x. S11)."));
        instrument->setStatusTip(tr("Instrument code"));
        instrument->setWhatsThis(
                tr("This sets the instrument code suffix for the component.  This suffix is appended to raw variables to generate their final name.  When left empty it is automatically generated."));


        QRadioButton *lineSelected = new QRadioButton(tr("Line:"), container);
        line->addWidget(lineSelected);
        group->addButton(lineSelected);

        sampleLine = new QSpinBox(container);
        line->addWidget(sampleLine);
        qint64 i = value["Line"].toInt64();
        if (!INTEGER::defined(i) || i < 1 || i > 9)
            sampleLine->setValue(1);
        else
            sampleLine->setValue((int) i);
        sampleLine->setRange(1, 9);
        sampleLine->setToolTip(tr("Sample &line number."));
        sampleLine->setStatusTip(tr("Sample line number"));
        sampleLine->setWhatsThis(
                tr("This is the sample line number.  This is used in automatic instrument suffix generation to designate which sample line the instrument is connected on."));

        if (!INTEGER::defined(i)) {
            lineSelected->setChecked(false);
            instrumentSelected->setChecked(true);
        } else {
            instrumentSelected->setChecked(false);
            lineSelected->setChecked(true);
        }
        connect(instrumentSelected, SIGNAL(toggled(bool)), this, SLOT(nameTypeSelected(bool)));
        nameTypeSelected(instrumentSelected->isChecked());
    }

    {
        fixed = new QCheckBox(tr("&Static definition"), this);
        topLevel->addWidget(fixed);
        fixed->setToolTip(tr("Always create component and disable auto-detection."));
        fixed->setStatusTip(tr("Fixed interface"));
        fixed->setWhatsThis(
                tr("When enabled, this component bypasses auto-detection and is always created.  It most define a communications interface if required."));
        fixed->setChecked(value["Fixed"].toBool());
        connect(fixed, SIGNAL(toggled(bool)), this, SIGNAL(fixedChanged(bool)));
        connect(fixed, SIGNAL(toggled(bool)), this, SLOT(updateFixedState(bool)));
    }

    {
        QWidget *container = new QWidget(this);
        topLevel->addWidget(container);
        QHBoxLayout *line = new QHBoxLayout(container);
        container->setLayout(line);
        line->setContentsMargins(0, 0, 0, 0);

        interfaceData = Variant::Root(value["Interface"]);

        definedInterface = new QCheckBox(container);
        line->addWidget(definedInterface);
        definedInterface->setToolTip(tr("Communications interface definition."));
        definedInterface->setStatusTip(tr("Static communications interface"));
        definedInterface->setWhatsThis(
                tr("When enabled, the communications interface is fixed by the parameters set."));
        definedInterface->setChecked(interfaceData.read().exists());

        selectInterface = new QPushButton(tr("&Communications"), container);
        line->addWidget(selectInterface, 1);
        selectInterface->setToolTip(tr("Select the communications interface."));
        selectInterface->setStatusTip(tr("Communications interface"));
        selectInterface->setWhatsThis(
                tr("Use this to select the communications interface in use."));
        connect(selectInterface, SIGNAL(clicked(bool)), this, SLOT(showInterfaceSelection()));
        selectInterface->setEnabled(definedInterface->isChecked());
        connect(definedInterface, SIGNAL(toggled(bool)), selectInterface, SLOT(setEnabled(bool)));

        fixedInterface = new QCheckBox(tr("F&ixed parameters"), container);
        line->addWidget(fixedInterface);
        fixedInterface->setToolTip(
                tr("Do not permit auto-detection to change interface parameters."));
        fixedInterface->setStatusTip(tr("Static interface parameters"));
        fixedInterface->setWhatsThis(
                tr("When enabled, the auto-detection sequence will not alter any parameters of the communications interface."));
        fixedInterface->setChecked(interfaceData["FixedInterface"].toBool());

        updateInterfaceText();
    }

    {
        passive = new QCheckBox(tr("Read only communications"), this);
        topLevel->addWidget(passive);
        passive->setToolTip(tr("Do not permit output to the communications interface."));
        passive->setStatusTip(tr("Passive communications"));
        passive->setWhatsThis(
                tr("When enabled, this prevents the component from writing any output to the communications interface.  This allows some components to operate in a pure \"eavesdropper\" configuration."));
        passive->setChecked(value["Passive"].toBool());
    }

    {
        QWidget *container = new QWidget(this);
        topLevel->addWidget(container);
        QHBoxLayout *line = new QHBoxLayout(container);
        container->setLayout(line);
        line->setContentsMargins(0, 0, 0, 0);

        priorityEnable = new QCheckBox(tr("&Priority:"), container);
        line->addWidget(priorityEnable);
        priorityEnable->setToolTip(tr("Enable component priority."));
        priorityEnable->setStatusTip(tr("Component priority enable"));
        priorityEnable->setWhatsThis(
                tr("When component priority is disabled, they are created or checked for auto-detection after any components with a defined priority."));

        priority = new QSpinBox(container);
        line->addWidget(priority, 1);
        priority->setRange(INT_MIN, INT_MAX);
        priority->setToolTip(tr("Component priority."));
        priority->setStatusTip(tr("Component priority"));
        priority->setWhatsThis(
                tr("This sets the priority (sort ordering) used by the component for auto-detection and fixed creation ordering.  Lower priority components are checked or created first."));

        qint64 i = value["Priority"].toInt64();
        if (!INTEGER::defined(i)) {
            priorityEnable->setChecked(false);
            priority->setEnabled(false);
            priority->setValue(0);
        } else {
            priorityEnable->setChecked(true);
            priority->setEnabled(true);
            priority->setValue((int) i);
        }
        connect(priorityEnable, SIGNAL(toggled(bool)), priority, SLOT(setEnabled(bool)));
    }

    updateFixedState(fixed->isChecked());
    topLevel->addStretch(1);
}

AcquisitionComponentPageGeneral::~AcquisitionComponentPageGeneral() = default;

void AcquisitionComponentPageGeneral::commit(CPD3::Data::Variant::Write &target)
{
    if (instrumentSelected->isChecked()) {
        QString name(instrument->text().trimmed());
        if (!name.isEmpty())
            target["Instrument"].setString(name);
        else
            target["Instrument"].remove(false);
    } else {
        target["Line"].setInt64(sampleLine->value());
    }

    if (fixed->isChecked()) {
        target["Fixed"].setBool(true);
    } else if (target["Fixed"].toBool()) {
        target["Fixed"].remove(false);
    }

    if (!definedInterface->isChecked()) {
        target["Interface"].remove(false);
    } else {
        target["Interface"].set(interfaceData);
    }

    if (!fixed->isChecked()) {
        if (fixedInterface->isChecked()) {
            target["FixedInterface"].setBool(true);
        } else if (target["FixedInterface"].toBool()) {
            target["FixedInterface"].remove(false);
        }
    } else {
        target["FixedInterface"].remove(false);
    }

    if (passive->isChecked()) {
        target["Passive"].setBool(true);
    } else if (target["Passive"].toBool()) {
        target["Passive"].remove(false);
    }

    if (!priorityEnable->isChecked()) {
        target["Priority"].remove(false);
    } else {
        target["Priority"].setInt64(priority->value());
    }
}

QString AcquisitionComponentPageGeneral::title() const
{ return tr("General"); }

bool AcquisitionComponentPageGeneral::fixedState() const
{ return fixed->isChecked(); }

void AcquisitionComponentPageGeneral::disableFixedMode(bool forceOff)
{
    fixed->setChecked(!forceOff);
    fixed->setEnabled(false);
}

void AcquisitionComponentPageGeneral::nameTypeSelected(bool fullName)
{
    instrument->setEnabled(fullName);
    sampleLine->setEnabled(!fullName);
}

void AcquisitionComponentPageGeneral::updateFixedState(bool fixed)
{
    fixedInterface->setEnabled(!fixed);
}

void AcquisitionComponentPageGeneral::showInterfaceSelection()
{
    IOInterfaceSelect selector(this);
    selector.setModal(true);
    selector.setInspectAdvanced(true);
    selector.setInterface(interfaceData);

    if (selector.exec() != QDialog::Accepted)
        return;

    interfaceData = selector.getInterface();
    updateInterfaceText();
}

void AcquisitionComponentPageGeneral::updateInterfaceText()
{
    if (!interfaceData.read().exists()) {
        selectInterface->setText(tr("Communications"));
        return;
    }

    auto target = CPD3::Acquisition::IOTarget::create(interfaceData);
    if (!target)
        return;
    selectInterface->setText(target->description());
}

static QSet<QString> toGroups(const Variant::Read &input)
{
    QSet<QString> result;
    for (const auto &add : input.toFlags()) {
        result.insert(QString::fromStdString(add));
    }
    return result;
}

AcquisitionComponentPageGroups::AcquisitionComponentPageGroups(const Variant::Read &value,
                                                               QWidget *parent)
        : AcquisitionComponentPage(value, parent)
{
    QVBoxLayout *topLevel = new QVBoxLayout(this);
    setLayout(topLevel);

    {
        QWidget *container = new QWidget(this);
        topLevel->addWidget(container);
        QHBoxLayout *line = new QHBoxLayout(container);
        container->setLayout(line);
        line->setContentsMargins(0, 0, 0, 0);

        QLabel *label = new QLabel(tr("&Groups:"), container);
        line->addWidget(label);

        groups = new SetSelector(container);
        line->addWidget(groups, 1);
        label->setBuddy(groups);
        groups->setToolTip(tr("The base groups the component belongs to."));
        groups->setStatusTip(tr("Component groups"));
        groups->setWhatsThis(
                tr("This set the base groups the component belongs to.  These groups are used to define what affects the component and what it can affect by default."));
        groups->setSelected(toGroups(value["Groups"]));
    }

    {
        QWidget *container = new QWidget(this);
        topLevel->addWidget(container);
        QHBoxLayout *line = new QHBoxLayout(container);
        container->setLayout(line);
        line->setContentsMargins(0, 0, 0, 0);

        enableOutputGroups = new QCheckBox(tr("&Output groups:"), container);
        line->addWidget(enableOutputGroups);
        enableOutputGroups->setToolTip(
                tr("Use a separate set of groups for what the component controls."));
        enableOutputGroups->setStatusTip(tr("Separate output groups"));
        enableOutputGroups->setWhatsThis(
                tr("When enabled, the component has a different set of groups that it generates control for from what it receives control from."));
        enableOutputGroups->setChecked(value["OutputGroups"].exists());

        outputGroups = new SetSelector(container);
        line->addWidget(outputGroups, 1);
        outputGroups->setToolTip(tr("The groups the component generates control for."));
        outputGroups->setStatusTip(tr("Output groups"));
        outputGroups->setWhatsThis(
                tr("This set the output groups the component belongs to.  These groups are used to define what the component affects."));
        outputGroups->setSelected(toGroups(value["OutputGroups"]));
        outputGroups->setEnabled(enableOutputGroups->isChecked());
        connect(enableOutputGroups, SIGNAL(toggled(bool)), outputGroups, SLOT(setEnabled(bool)));
    }

    variableGroups = new AcquisitionVariableGroupsEditor(value["VariableGroups"], Variant::Root(),
                                                         Variant::Root(), this);
    topLevel->addWidget(variableGroups, 1);
}

AcquisitionComponentPageGroups::~AcquisitionComponentPageGroups() = default;

static Variant::Flags fromGroups(const QSet<QString> &set)
{ return Util::set_from_qstring(set); }

void AcquisitionComponentPageGroups::commit(Variant::Write &target)
{
    QSet<QString> selected(groups->getSelected());
    if (selected.isEmpty()) {
        target["Groups"].remove(false);
    } else {
        target["Groups"].setFlags(fromGroups(selected));
    }

    if (!enableOutputGroups->isChecked()) {
        target["OutputGroups"].remove(false);
    } else {
        target["OutputGroups"].setFlags(fromGroups(outputGroups->getSelected()));
    }

    auto vg = variableGroups->variableGroups();
    if (vg.read().exists()) {
        target["VariableGroups"].set(vg);
    } else {
        target["VariableGroups"].remove(false);
    }
}

QString AcquisitionComponentPageGroups::title() const
{ return tr("Groups"); }


AcquisitionComponentPageDisplay::AcquisitionComponentPageDisplay(const Variant::Read &value,
                                                                 QWidget *parent)
        : AcquisitionComponentPage(value, parent)
{
    QVBoxLayout *topLevel = new QVBoxLayout(this);
    setLayout(topLevel);

    menuEnable = new QGroupBox(tr("&Menu"), this);
    topLevel->addWidget(menuEnable);
    menuEnable->setCheckable(true);

    QVBoxLayout *groupLayout = new QVBoxLayout(menuEnable);
    menuEnable->setLayout(groupLayout);

    {
        QWidget *container = new QWidget(menuEnable);
        groupLayout->addWidget(container);
        QHBoxLayout *line = new QHBoxLayout(container);
        container->setLayout(line);
        line->setContentsMargins(0, 0, 0, 0);

        menuCharacterEnable = new QCheckBox(tr("&Hotkey:"), container);
        line->addWidget(menuCharacterEnable);
        menuCharacterEnable->setToolTip(tr("Enable menu hotkey override."));
        menuCharacterEnable->setStatusTip(tr("Menu hotkey set"));
        menuCharacterEnable->setWhatsThis(
                tr("When not enabled the menu entry hotkey is automatically generated.  This sets it to the given value, even if that value is empty (no hotkey)."));
        menuCharacterEnable->setChecked(value["MenuCharacter"].exists());

        menuCharacter = new QLineEdit(value["MenuCharacter"].toDisplayString());
        line->addWidget(menuCharacter);
        menuCharacter->setToolTip(tr("Menu hotkey."));
        menuCharacter->setStatusTip(tr("Menu hotkey"));
        menuCharacter->setWhatsThis(
                tr("This sets the hotkey string used for the component on the main menu."));
        menuCharacter->setEnabled(menuCharacterEnable->isChecked());
        connect(menuCharacterEnable, SIGNAL(toggled(bool)), menuCharacter, SLOT(setEnabled(bool)));
    }
    {
        QWidget *container = new QWidget(menuEnable);
        groupLayout->addWidget(container);
        QHBoxLayout *line = new QHBoxLayout(container);
        container->setLayout(line);
        line->setContentsMargins(0, 0, 0, 0);

        QLabel *label = new QLabel(tr("&Menu text:"), container);
        line->addWidget(label);

        menuEntry = new QLineEdit(value["MenuEntry"].toDisplayString());
        line->addWidget(menuEntry);
        label->setBuddy(menuEntry);
        menuEntry->setToolTip(tr("Menu text."));
        menuEntry->setStatusTip(tr("Menu text"));
        menuEntry->setWhatsThis(
                tr("This sets the text displayed for the component in the main menu."));
        menuEntry->setPlaceholderText(tr("Automatically generated"));
    }
    menuEnable->setChecked(!value["MenuHide"].toBool());

    {
        QWidget *container = new QWidget(this);
        topLevel->addWidget(container);
        QHBoxLayout *line = new QHBoxLayout(container);
        container->setLayout(line);
        line->setContentsMargins(0, 0, 0, 0);

        QLabel *label = new QLabel(tr("&Window title:"), container);
        line->addWidget(label);

        windowTitle = new QLineEdit(value["WindowTitle"].toDisplayString());
        line->addWidget(windowTitle);
        label->setBuddy(windowTitle);
        windowTitle->setToolTip(tr("Window title text."));
        windowTitle->setStatusTip(tr("Window title"));
        windowTitle->setWhatsThis(
                tr("This sets the text displayed for the window or table title."));
        windowTitle->setPlaceholderText(tr("Automatically generated"));
    }

    topLevel->addStretch(1);
}

AcquisitionComponentPageDisplay::~AcquisitionComponentPageDisplay() = default;

void AcquisitionComponentPageDisplay::commit(Variant::Write &target)
{
    if (!menuEnable->isChecked()) {
        target["MenuHide"].setBool(true);
    } else if (target["MenuHide"].toBool()) {
        target["MenuHide"].remove(false);
    }

    {
        QString name(menuCharacter->text().trimmed());
        if (!menuCharacterEnable->isChecked()) {
            target["MenuCharacter"].remove(false);
        } else {
            target["MenuCharacter"].setString(name);
        }
    }

    {
        QString name(menuEntry->text().trimmed());
        if (!name.isEmpty())
            target["MenuEntry"].setString(name);
        else
            target["MenuEntry"].remove(false);
    }

    {
        QString name(windowTitle->text().trimmed());
        if (!name.isEmpty())
            target["WindowTitle"].setString(name);
        else
            target["WindowTitle"].remove(false);
    }
}

QString AcquisitionComponentPageDisplay::title() const
{ return tr("Display"); }


namespace {
class VariableMetadataTableModel : public QAbstractItemModel {

public:
    struct VariableData {
        Variant::Root selection;
        Variant::Root metadata;

        VariableData() = default;

        inline VariableData(Variant::Root &&s, Variant::Root &&m) : selection(std::move(s)),
                                                                    metadata(std::move(m))
        { }
    };

    enum {
        Column_AddRemove, Column_Variable, Column_Metadata,

        Column_TOTAL
    };

private:

    std::vector<VariableData> metadata;

    static QString convertVariableSelection(const Variant::Read &sel)
    {
        switch (sel.getType()) {
        case Variant::Type::String:
            return sel.toQString();
        case Variant::Type::Hash:
            return sel.hash("Variable").toQString();
        default:
            break;
        }
        return QString();
    }

public:

    VariableMetadataTableModel(const Variant::Read &v, AcquisitionComponentPageMetadata *parent)
            : QAbstractItemModel(parent)
    {
        switch (v.getType()) {
        case Variant::Type::Hash:
            for (auto child : v.toHash()) {
                if (child.first.empty())
                    continue;
                metadata.emplace_back(Variant::Root(child.first), Variant::Root(child.second));
            }
            break;
        default:
            for (auto child : v.toChildren()) {
                metadata.emplace_back(Variant::Root(child.getPath("Variable")),
                                      Variant::Root(child.getPath("Metadata")));
            }
            break;
        }
    }

    virtual QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const
    {
        Q_UNUSED(parent);
        return createIndex(row, column);
    }

    virtual QModelIndex parent(const QModelIndex &index) const
    {
        Q_UNUSED(index);
        return QModelIndex();
    }

    virtual int rowCount(const QModelIndex &parent) const
    {
        Q_UNUSED(parent);
        return metadata.size() + 1;
    }

    virtual int columnCount(const QModelIndex &parent) const
    {
        Q_UNUSED(parent);
        return Column_TOTAL;
    }

    virtual QVariant data(const QModelIndex &index, int role) const
    {
        if (!index.isValid())
            return QVariant();
        if (index.row() >= static_cast<int>(metadata.size())) {
            if (role == Qt::EditRole) {
                if (index.column() == 0)
                    return 1;
            }
            return QVariant();
        }
        const auto &data = metadata[index.row()];

        switch (role) {
        case Qt::DisplayRole:
            switch (index.column()) {
            case Column_Variable:
                return convertVariableSelection(data.selection);
            case Column_Metadata:
                return tr("...");
            default:
                break;
            }
            break;

        case Qt::EditRole:
            switch (index.column()) {
            case Column_Variable:
                return convertVariableSelection(data.selection);
            case Column_Metadata:
                return QVariant::fromValue<Variant::Write>(Variant::Root(data.metadata).write());
            default:
                break;
            }
            break;

        default:
            break;
        }
        return QVariant();
    }

    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const
    {
        if (role != Qt::DisplayRole)
            return QVariant();
        if (orientation == Qt::Vertical)
            return QVariant(section + 1);

        switch (section) {
        case Column_Variable:
            return tr("Variable");
        case Column_Metadata:
            return tr("Metadata");
        default:
            break;
        }
        return QVariant();
    }

    virtual bool setData(const QModelIndex &index, const QVariant &value, int role)
    {
        if (role != Qt::EditRole)
            return false;

        if (index.row() >= static_cast<int>(metadata.size()))
            return false;

        switch (index.column()) {
        case Column_Variable: {
            QString str(value.toString().trimmed());
            if (str.isEmpty())
                return false;
            metadata[index.row()].selection.write().setString(str);
            emit dataChanged(index, index);
            return true;
        }
        case Column_Metadata:
            metadata[index.row()].metadata.write().set(value.value<Variant::Write>());
            return true;
        default:
            break;
        }

        return false;
    }

    virtual Qt::ItemFlags flags(const QModelIndex &index) const
    {
        if (!index.isValid())
            return 0;

        switch (index.column()) {
        case Column_Variable:
            return Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled;
        case Column_Metadata:
            return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
        default:
            break;
        }

        return 0;
    }

    virtual bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex())
    {
        Q_ASSERT(row + count <= static_cast<int>(metadata.size()));

        beginRemoveRows(parent, row, row + count - 1);
        auto first = metadata.begin() + row;
        auto last = first + count;
        metadata.erase(first, last);
        endRemoveRows();

        return true;
    }

    virtual bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex())
    {
        beginInsertRows(parent, row, row + count - 1);
        VariableData item(Variant::Root(".*"), Variant::Root());
        metadata.insert(metadata.begin() + row, static_cast<std::size_t>(count), item);
        endInsertRows();

        return true;
    }

    inline const std::vector<VariableData> &getMetadata() const
    { return metadata; }
};

class VariableMetadataAddRemoveButton : public QPushButton {
public:
    VariableMetadataAddRemoveButton(const QString &text, QWidget *parent = 0) : QPushButton(text,
                                                                                            parent)
    { }

    virtual ~VariableMetadataAddRemoveButton() = default;

    virtual QSize sizeHint() const
    {
        QSize textSize(fontMetrics().size(Qt::TextShowMnemonic, text()));
        QStyleOptionButton opt;
        opt.initFrom(this);
        opt.rect.setSize(textSize);
        return style()->sizeFromContents(QStyle::CT_PushButton, &opt, textSize, this);
    }
};

class VariableMetadataAddRemoveDelegate : public QItemDelegate {
    AcquisitionComponentPageMetadata *editor;
public:
    VariableMetadataAddRemoveDelegate(AcquisitionComponentPageMetadata *e, QObject *parent = 0)
            : QItemDelegate(parent), editor(e)
    { }

    virtual ~VariableMetadataAddRemoveDelegate() = default;

    virtual QWidget *createEditor(QWidget *parent,
                                  const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const
    {
        Q_UNUSED(option);

        QPushButton *button = new VariableMetadataAddRemoveButton(tr("-", "remove text"), parent);
        button->setProperty("model-row", index.row());

        return button;
    }

    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
    {
        QPushButton *button = static_cast<QPushButton *>(editor);

        button->disconnect(editor);

        QVariant data(index.data(Qt::EditRole));
        if (data.toInt() != 1) {
            button->setText(tr("-", "remove text"));
            connect(button, SIGNAL(clicked(bool)), this->editor, SLOT(removeVariableMetadataRow()));
        } else {
            button->setText(tr("Add", "add text"));
            connect(button, SIGNAL(clicked(bool)), this->editor, SLOT(addVariableMetadataRow()));
        }

        button->setProperty("model-row", index.row());
    }

    virtual void setModelData(QWidget *editor,
                              QAbstractItemModel *model,
                              const QModelIndex &index) const
    {
        Q_UNUSED(editor);
        Q_UNUSED(model);
        Q_UNUSED(index);
    }
};

class VariableMetadataEditorDelegate : public QItemDelegate {
public:
    VariableMetadataEditorDelegate(QObject *parent = 0) : QItemDelegate(parent)
    { }

    virtual ~VariableMetadataEditorDelegate() = default;

    virtual QWidget *createEditor(QWidget *parent,
                                  const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const
    {
        Q_UNUSED(option);
        Q_UNUSED(index);

        Internal::AcquisitionVariableMetadataEditorButton
                *editor = new Internal::AcquisitionVariableMetadataEditorButton(parent);
        connect(editor, SIGNAL(commitChanged(QWidget * )), this, SIGNAL(commitData(QWidget * )));
        return editor;
    }

    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
    {
        Internal::AcquisitionVariableMetadataEditorButton *selector =
                static_cast<Internal::AcquisitionVariableMetadataEditorButton *>(editor);
        selector->setProperty("variable-metadata", index.data(Qt::EditRole));
    }

    virtual void setModelData(QWidget *editor,
                              QAbstractItemModel *model,
                              const QModelIndex &index) const
    {
        Internal::AcquisitionVariableMetadataEditorButton *selector =
                static_cast<Internal::AcquisitionVariableMetadataEditorButton *>(editor);
        model->setData(index, selector->property("variable-metadata"));
    }
};
}

AcquisitionComponentPageMetadata::AcquisitionComponentPageMetadata(const Variant::Read &value,
                                                                   QWidget *parent)
        : AcquisitionComponentPage(value, parent)
{
    QVBoxLayout *topLevel = new QVBoxLayout(this);
    setLayout(topLevel);

    {
        QWidget *container = new QWidget(this);
        topLevel->addWidget(container);
        QHBoxLayout *line = new QHBoxLayout(container);
        container->setLayout(line);
        line->setContentsMargins(0, 0, 0, 0);

        QButtonGroup *group = new QButtonGroup(container);
        group->setExclusive(true);

        disableMatch = new QRadioButton(tr("Match an&y"), container);
        line->addWidget(disableMatch);
        group->addButton(disableMatch);
        disableMatch->setToolTip(tr("Disable matching and accept any candidate."));
        disableMatch->setStatusTip(tr("Disable matching"));
        disableMatch->setWhatsThis(
                tr("This disables all auto-detection matching and any candidate component is accepted."));

        matchSerialNumber = new QRadioButton(tr("&Serial Number:"), container);
        line->addWidget(matchSerialNumber);
        group->addButton(matchSerialNumber);
        matchSerialNumber->setToolTip(tr("Match based on an integer serial number."));
        matchSerialNumber->setStatusTip(tr("Serial number matching"));
        matchSerialNumber->setWhatsThis(
                tr("Accept only a component with the specified integer serial number."));

        matchedSerialNumber = new QSpinBox(container);
        line->addWidget(matchedSerialNumber, 1);
        matchedSerialNumber->setToolTip(tr("Required serial number."));
        matchedSerialNumber->setStatusTip(tr("Serial number"));
        matchedSerialNumber->setWhatsThis(
                tr("This sets the serial number required to be queried from the component before it is accepted by auto-detection."));
        matchedSerialNumber->setRange(1, INT_MAX);
        connect(matchSerialNumber, SIGNAL(toggled(bool)), matchedSerialNumber,
                SLOT(setEnabled(bool)));

        matchAdvanced = new QRadioButton(tr("&Advanced"), container);
        line->addWidget(matchAdvanced);
        group->addButton(matchAdvanced);
        matchAdvanced->setToolTip(tr("Match based on a full selection of source parameters."));
        matchAdvanced->setStatusTip(tr("Advanced matching"));
        matchAdvanced->setWhatsThis(
                tr("When in use, this allows the full origin matching based on all information queried by the component during startup."));

        matchAdvancedData = Variant::Root(value["Match"]);
        auto toMatch = matchAdvancedData.read().toHash();
        if (toMatch.size() < 1) {
            disableMatch->setChecked(true);
        } else {
            qint64 i = INTEGER::undefined();
            if (toMatch.size() == 1)
                i = toMatch["SerialNumber"].toInt64();
            if (INTEGER::defined(i) && i > 0 && i < INT_MAX) {
                matchSerialNumber->setChecked(true);
                matchedSerialNumber->setValue((int) i);
            } else {
                matchAdvanced->setChecked(true);
            }
        }

        connect(matchAdvanced, SIGNAL(clicked(bool)), this, SLOT(showMatchedAdvancedDialog(bool)));
    }

    {
        QWidget *container = new QWidget(this);
        topLevel->addWidget(container);
        QHBoxLayout *line = new QHBoxLayout(container);
        container->setLayout(line);
        line->setContentsMargins(0, 0, 0, 0);

        QButtonGroup *group = new QButtonGroup(container);
        group->setExclusive(true);

        disableSource = new QRadioButton(tr("No source &override"), container);
        line->addWidget(disableSource);
        group->addButton(disableSource);
        disableSource->setToolTip(tr("Use only the queried or implied source metadata."));
        disableSource->setStatusTip(tr("Disable source override"));
        disableSource->setWhatsThis(
                tr("This disables all metadata source overriding.  The source of data is set entirely by the component in use."));

        sourceSerialNumber = new QRadioButton(tr("Serial &Number:"), container);
        line->addWidget(sourceSerialNumber);
        group->addButton(sourceSerialNumber);
        sourceSerialNumber->setToolTip(tr("Set the source serial number."));
        sourceSerialNumber->setStatusTip(tr("Serial number override"));
        sourceSerialNumber->setWhatsThis(
                tr("This sets the origin serial number to the given value, overriding any that may have been queried from by the component."));

        sourceSetSerialNumber = new QSpinBox(container);
        line->addWidget(sourceSetSerialNumber, 1);
        sourceSetSerialNumber->setToolTip(tr("Source serial number."));
        sourceSetSerialNumber->setStatusTip(tr("Serial number"));
        sourceSetSerialNumber->setWhatsThis(
                tr("This is the serial number all data are overriden to originate from."));
        sourceSetSerialNumber->setRange(1, INT_MAX);
        connect(sourceSerialNumber, SIGNAL(toggled(bool)), sourceSetSerialNumber,
                SLOT(setEnabled(bool)));

        sourceAdvanced = new QRadioButton(tr("A&dvanced"), container);
        line->addWidget(sourceAdvanced);
        group->addButton(sourceAdvanced);
        sourceAdvanced->setToolTip(tr("Advanced source metadata override."));
        sourceAdvanced->setStatusTip(tr("Advanced source override"));
        sourceAdvanced->setWhatsThis(
                tr("This option allows all source metadata to be overridden."));

        sourceAdvancedData = Variant::Root(value["Source"]);
        auto toSource = sourceAdvancedData.read().toHash();
        if (toSource.size() < 1) {
            disableSource->setChecked(true);
        } else {
            qint64 i = INTEGER::undefined();
            if (toSource.size() == 1)
                i = toSource["SerialNumber"].toInt64();
            if (INTEGER::defined(i) && i > 0 && i < INT_MAX) {
                sourceSerialNumber->setChecked(true);
                sourceSetSerialNumber->setValue((int) i);
            } else {
                sourceAdvanced->setChecked(true);
            }
        }

        connect(sourceAdvanced, SIGNAL(clicked(bool)), this, SLOT(showSourceAdvancedDialog(bool)));
    }

    {
        QWidget *container = new QWidget(this);
        topLevel->addWidget(container);
        QHBoxLayout *line = new QHBoxLayout(container);
        container->setLayout(line);
        line->setContentsMargins(0, 0, 0, 0);

        QPushButton *button = new QPushButton(tr("Global Metadata Overlay"), container);
        line->addWidget(button);
        button->setToolTip(tr("Set metadata overlaid to all values generated."));
        button->setStatusTip(tr("Global metadata overlay"));
        button->setWhatsThis(
                tr("This sets the metadata overlaid to all values generated by the component."));
        globalMetadata = Variant::Root(value["GlobalMetadata"]);
        connect(button, SIGNAL(clicked(bool)), this, SLOT(showGlobalMetadataDialog()));

        QLabel *label = new QLabel(tr("S&tation override:"), container);
        line->addWidget(label);

        stationOverride = new QLineEdit(value["Station"].toQString(), container);
        line->addWidget(stationOverride, 1);
        label->setBuddy(stationOverride);
        stationOverride->setToolTip(tr("Override the station for the component."));
        stationOverride->setStatusTip(tr("Station override"));
        stationOverride->setWhatsThis(
                tr("This allows the station assigned to data generated by the component to be altered."));
        stationOverride->setPlaceholderText(tr("System default"));
    }

    VariableMetadataTableModel
            *model = new VariableMetadataTableModel(value["VariableMetadata"], this);
    variableMetadata = new QTableView(this);
    topLevel->addWidget(variableMetadata, 1);
    variableMetadata->setModel(model);

    variableMetadata->setToolTip(tr("The table of variable groupings."));
    variableMetadata->setStatusTip(tr("Group selections"));
    variableMetadata->setWhatsThis(
            tr("This table represents all the variable-group mappings present."));

    variableMetadata->horizontalHeader()
                    ->setSectionResizeMode(VariableMetadataTableModel::Column_AddRemove,
                                           QHeaderView::ResizeToContents);

    variableMetadata->horizontalHeader()
                    ->setSectionResizeMode(VariableMetadataTableModel::Column_Variable,
                                           QHeaderView::Stretch);

    variableMetadata->setItemDelegateForColumn(VariableMetadataTableModel::Column_AddRemove,
                                               new VariableMetadataAddRemoveDelegate(this,
                                                                                     variableMetadata));

    variableMetadata->setItemDelegateForColumn(VariableMetadataTableModel::Column_Metadata,
                                               new VariableMetadataEditorDelegate(
                                                       variableMetadata));


    variableMetadata->setSpan(model->rowCount(QModelIndex()) - 1, 0, 1,
                              VariableMetadataTableModel::Column_TOTAL);

    openAllEditors();
}

AcquisitionComponentPageMetadata::~AcquisitionComponentPageMetadata() = default;

void AcquisitionComponentPageMetadata::openAllEditors()
{
    VariableMetadataTableModel
            *model = static_cast<VariableMetadataTableModel *>(variableMetadata->model());
    for (int row = 0; row < model->rowCount(QModelIndex()); ++row) {
        variableMetadata->openPersistentEditor(
                model->index(row, VariableMetadataTableModel::Column_AddRemove));

        if (row == model->rowCount(QModelIndex()) - 1)
            continue;

        variableMetadata->openPersistentEditor(
                model->index(row, VariableMetadataTableModel::Column_Metadata));
    }

    variableMetadata->resizeColumnToContents(0);
}

void AcquisitionComponentPageMetadata::commit(Variant::Write &target)
{
    if (matchSerialNumber->isChecked()) {
        target["Match"].remove(false);
        target["Match/SerialNumber"].setInt64(matchedSerialNumber->value());
    } else if (matchAdvanced->isChecked()) {
        target["Match"].set(matchAdvancedData);
    } else {
        target["Match"].remove(false);
    }

    if (sourceSerialNumber->isChecked()) {
        target["Source"].remove(false);
        target["Source/SerialNumber"].setInt64(sourceSetSerialNumber->value());
    } else if (sourceAdvanced->isChecked()) {
        target["Source"].set(sourceAdvancedData);
    } else {
        target["Source"].remove(false);
    }

    if (globalMetadata.read().exists()) {
        target["GlobalMetadata"].set(globalMetadata);
    } else {
        target["GlobalMetadata"].remove(false);
    }

    {
        QString data(stationOverride->text().trimmed());
        if (!data.isEmpty()) {
            target["Station"].setString(data);
        } else {
            target["Station"].remove(false);
        }
    }

    const auto &vmeta =
            static_cast<VariableMetadataTableModel *>(variableMetadata->model())->getMetadata();
    target["VariableMetadata"].remove(false);
    if (!vmeta.empty()) {
        std::unordered_map<QString, Variant::Read> variableOnlyMetadata;
        for (const auto &check : vmeta) {
            if (check.selection.read().getType() != Variant::Type::String)
                break;
            QString vn(check.selection.read().toQString().trimmed());
            if (vn.isEmpty())
                break;
            if (variableOnlyMetadata.count(vn))
                break;
            variableOnlyMetadata.emplace(vn, check.metadata.read());
        }

        if (variableOnlyMetadata.size() == vmeta.size()) {
            for (const auto &add : variableOnlyMetadata) {
                target["VariableMetadata"].hash(add.first).set(add.second);
            }
        } else {
            auto allMeta = target["VariableMetadata"].toArray();
            for (const auto &add : vmeta) {
                auto vt = allMeta.after_back();
                vt.hash("Variable").set(add.selection);
                vt.hash("Metadata").set(add.metadata);
            }
        }
    }
}

QString AcquisitionComponentPageMetadata::title() const
{ return tr("Metadata"); }

void AcquisitionComponentPageMetadata::setFixed(bool enabled)
{
    if (enabled) {
        disableMatch->setChecked(true);
        disableMatch->setEnabled(false);
        matchSerialNumber->setEnabled(false);
        matchAdvanced->setEnabled(false);
    } else {
        disableMatch->setEnabled(true);
        matchSerialNumber->setEnabled(true);
        matchAdvanced->setEnabled(true);
    }
}

static bool advancedDataEntryDialog(QWidget *parent,
                                    const QString &title,
                                    Variant::Write &data,
                                    const Variant::Read &metadata = Variant::Read::empty())
{
    QDialog dialog(parent);
    dialog.setWindowTitle(QObject::tr("Data"));
    dialog.setWindowTitle(title);
    QVBoxLayout *layout = new QVBoxLayout(&dialog);
    dialog.setLayout(layout);

    Variant::Write base = data;
    base.detachFromRoot();
    Variant::Read remeta = metadata;
    remeta.detachFromRoot();

    ValueEditor *editor = new ValueEditor(&dialog);
    editor->setValue(base, remeta);
    layout->addWidget(editor);

    QDialogButtonBox *buttons =
            new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal,
                                 &dialog);
    layout->addWidget(buttons);
    QObject::connect(buttons, SIGNAL(accepted()), &dialog, SLOT(accept()));
    QObject::connect(buttons, SIGNAL(rejected()), &dialog, SLOT(reject()));

    if (dialog.exec() != QDialog::Accepted)
        return false;

    data.set(base);
    return true;
}

static bool advancedDataEntryDialog(QWidget *parent,
                                    const QString &title,
                                    Variant::Write &&data,
                                    const Variant::Read &metadata = Variant::Read::empty())
{ return advancedDataEntryDialog(parent, title, data, metadata); }

void AcquisitionComponentPageMetadata::showMatchedAdvancedDialog(bool toggled)
{
    if (!toggled)
        return;

    Variant::Write meta = Variant::Write::empty();
    meta.metadataHash("Editor").hash("Type").setString("Arbitrary");
    meta.metadataHashChild("Manufacturer").setType(Variant::Type::MetadataString);
    meta.metadataHashChild("Model").setType(Variant::Type::MetadataString);
    meta.metadataHashChild("SerialNumber").setType(Variant::Type::MetadataInteger);
    advancedDataEntryDialog(this, tr("Matched Source Data"), matchAdvancedData.write(), meta);
}

void AcquisitionComponentPageMetadata::showSourceAdvancedDialog(bool toggled)
{
    if (!toggled)
        return;

    Variant::Write meta = Variant::Write::empty();
    meta.metadataHash("Editor").hash("Type").setString("Arbitrary");
    meta.metadataHashChild("Manufacturer").setType(Variant::Type::MetadataString);
    meta.metadataHashChild("Model").setType(Variant::Type::MetadataString);
    meta.metadataHashChild("SerialNumber").setType(Variant::Type::MetadataInteger);
    advancedDataEntryDialog(this, tr("Source Override"), sourceAdvancedData.write(), meta);
}

void AcquisitionComponentPageMetadata::showGlobalMetadataDialog()
{
    advancedDataEntryDialog(this, tr("Global Metadata Overlay"), globalMetadata.write());
}

void AcquisitionComponentPageMetadata::addVariableMetadataRow()
{
    variableMetadata->model()->insertRows(variableMetadata->model()->rowCount() - 1, 1);

    openAllEditors();
}

void AcquisitionComponentPageMetadata::removeVariableMetadataRow()
{
    QObject *s = sender();
    if (!s)
        return;
    QVariant p(s->property("model-row"));
    if (!p.isValid())
        return;
    int row = p.toInt();
    if (row < 0 || row >= variableMetadata->model()->rowCount() - 1)
        return;

    variableMetadata->model()->removeRows(row, 1);

    openAllEditors();
}

Internal::AcquisitionVariableMetadataEditorButton::AcquisitionVariableMetadataEditorButton(QWidget *parent)
        : QPushButton(tr("Edit Metadata"), parent)
{
    connect(this, SIGNAL(clicked(bool)), this, SLOT(showEditor()));
}

Internal::AcquisitionVariableMetadataEditorButton::~AcquisitionVariableMetadataEditorButton() = default;

void Internal::AcquisitionVariableMetadataEditorButton::showEditor()
{
    auto v = property("variable-metadata").value<Variant::Write>();
    if (!advancedDataEntryDialog(this, tr("Variable Metadata Overlay"), v))
        return;
    emit commitChanged(this);
}


namespace {
class RemappingTableModel : public QAbstractItemModel {

public:
    struct RemapData {
        QString name;
        Variant::Root input;
        Calibration calibration;

        RemapData() = default;

        RemapData(const QString &name, const Variant::Read &config) : name(name),
                                                                      input(config.hash("Input"))
        {
            if (config.hash("Calibration").exists())
                calibration = Variant::Composite::toCalibration(config.hash("Calibration"));
            else
                calibration.clear();
        }
    };
    enum {
        Column_AddRemove, Column_Name, Column_Input, Column_Calibration,

        Column_TOTAL
    };

private:

    std::vector<RemapData> remap;

    static QString convertVariableSelection(const Variant::Read &sel)
    {
        switch (sel.getType()) {
        case Variant::Type::String:
            return sel.toQString();
        case Variant::Type::Hash:
            return sel.hash("Variable").toQString();
        default:
            break;
        }
        return QString();
    }

    static bool remapSort(const RemapData &a, const RemapData &b)
    {
        return a.name < b.name;
    }

    QString buildCalibrationString(const Calibration &calibration) const
    {
        QString result;

        NumberFormat ef(1, 0);
        for (int i = 0, max = calibration.size(); i < max; i++) {
            double v = calibration.get(i);
            if (v == 0.0)
                continue;

            if (result.isEmpty()) {
                result.append(QString::number(v));
            } else {
                if (v < 0.0) {
                    result.append(" - ");
                    v = -v;
                } else {
                    result.append(" + ");
                }
                result.append(QString::number(v));
            }

            if (i > 0)
                result.append('x');
            if (i > 1)
                result.append(ef.superscript(i));
        }
        return result;
    }

public:

    RemappingTableModel(const Variant::Read &v, AcquisitionComponentPageRemapping *parent)
            : QAbstractItemModel(parent)
    {
        for (const auto &child : v.toHash()) {
            if (child.first.empty())
                continue;
            remap.emplace_back(QString::fromStdString(child.first), child.second);
        }
        std::sort(remap.begin(), remap.end(), remapSort);
    }

    virtual QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const
    {
        Q_UNUSED(parent);
        return createIndex(row, column);
    }

    virtual QModelIndex parent(const QModelIndex &index) const
    {
        Q_UNUSED(index);
        return QModelIndex();
    }

    virtual int rowCount(const QModelIndex &parent) const
    {
        Q_UNUSED(parent);
        return remap.size() + 1;
    }

    virtual int columnCount(const QModelIndex &parent) const
    {
        Q_UNUSED(parent);
        return Column_TOTAL;
    }

    virtual QVariant data(const QModelIndex &index, int role) const
    {
        if (!index.isValid())
            return QVariant();
        if (index.row() >= static_cast<int>(remap.size())) {
            if (role == Qt::EditRole) {
                if (index.column() == 0)
                    return 1;
            }
            return QVariant();
        }
        const auto &data = remap[index.row()];

        switch (role) {
        case Qt::DisplayRole:
            switch (index.column()) {
            case Column_Name:
                return data.name;
            case Column_Input:
                return convertVariableSelection(data.input);
            case Column_Calibration:
                return buildCalibrationString(data.calibration);
            default:
                break;
            }
            break;

        case Qt::EditRole:
            switch (index.column()) {
            case Column_Input:
                return convertVariableSelection(data.input);
            default:
                break;
            }
            break;

        default:
            break;
        }
        return QVariant();
    }

    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const
    {
        if (role != Qt::DisplayRole)
            return QVariant();
        if (orientation == Qt::Vertical)
            return QVariant(section + 1);

        switch (section) {
        case Column_Name:
            return tr("Remapping");
        case Column_Input:
            return tr("Input override");
        case Column_Calibration:
            return tr("Calibration");
        default:
            break;
        }
        return QVariant();
    }

    virtual bool setData(const QModelIndex &index, const QVariant &value, int role)
    {
        if (role != Qt::EditRole)
            return false;

        if (index.row() >= static_cast<int>(remap.size()))
            return false;

        switch (index.column()) {
        case Column_Name: {
            QString str(value.toString().trimmed());
            if (str.isEmpty())
                return false;
            remap[index.row()].name = str;
            emit dataChanged(index, index);
            return true;
        }
        case Column_Input: {
            QString str(value.toString().trimmed());
            if (str.isEmpty())
                return false;
            remap[index.row()].input.write().setString(str);
            emit dataChanged(index, index);
            return true;
        }
        default:
            break;
        }

        return false;
    }

    virtual Qt::ItemFlags flags(const QModelIndex &index) const
    {
        if (!index.isValid())
            return 0;

        switch (index.column()) {
        case Column_Name:
        case Column_Input:
            return Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled;
        case Column_Calibration:
            return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
        default:
            break;
        }

        return 0;
    }

    virtual bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex())
    {
        Q_ASSERT(row + count <= static_cast<int>(remap.size()));

        beginRemoveRows(parent, row, row + count - 1);
        auto first = remap.begin() + row;
        auto last = first + count;
        remap.erase(first, last);
        endRemoveRows();

        return true;
    }

    virtual bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex())
    {
        beginInsertRows(parent, row, row + count - 1);
        remap.insert(remap.begin() + row, static_cast<std::size_t>(count), RemapData());
        endInsertRows();

        return true;
    }

    inline const std::vector<RemapData> &getRemapping() const
    { return remap; }

    Calibration getCalibration(const QModelIndex &index) const
    {
        if (index.row() >= static_cast<int>(remap.size()))
            return Calibration();
        return remap[index.row()].calibration;
    }

    void setCalibration(const QModelIndex &index, const Calibration &calibration)
    {
        if (index.row() >= static_cast<int>(remap.size()))
            return;

        remap[index.row()].calibration = calibration;
        QModelIndex changed(this->index(index.row(), 1));

        emit dataChanged(changed, changed);
    }
};

class RemappingAddRemoveButton : public QPushButton {
public:
    RemappingAddRemoveButton(const QString &text, QWidget *parent = 0) : QPushButton(text, parent)
    { }

    virtual ~RemappingAddRemoveButton() = default;

    virtual QSize sizeHint() const
    {
        QSize textSize(fontMetrics().size(Qt::TextShowMnemonic, text()));
        QStyleOptionButton opt;
        opt.initFrom(this);
        opt.rect.setSize(textSize);
        return style()->sizeFromContents(QStyle::CT_PushButton, &opt, textSize, this);
    }
};

class RemappingAddRemoveDelegate : public QItemDelegate {
    AcquisitionComponentPageRemapping *editor;
public:
    RemappingAddRemoveDelegate(AcquisitionComponentPageRemapping *e, QObject *parent = 0)
            : QItemDelegate(parent), editor(e)
    { }

    virtual ~RemappingAddRemoveDelegate() = default;

    virtual QWidget *createEditor(QWidget *parent,
                                  const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const
    {
        Q_UNUSED(option);

        QPushButton *button = new RemappingAddRemoveButton(tr("-", "remove text"), parent);
        button->setProperty("model-row", index.row());

        return button;
    }

    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
    {
        QPushButton *button = static_cast<QPushButton *>(editor);

        button->disconnect(editor);

        QVariant data(index.data(Qt::EditRole));
        if (data.toInt() != 1) {
            button->setText(tr("-", "remove text"));
            connect(button, SIGNAL(clicked(bool)), this->editor, SLOT(removeRow()));
        } else {
            button->setText(tr("Add", "add text"));
            connect(button, SIGNAL(clicked(bool)), this->editor, SLOT(addRow()));
        }

        button->setProperty("model-row", index.row());
    }

    virtual void setModelData(QWidget *editor,
                              QAbstractItemModel *model,
                              const QModelIndex &index) const
    {
        Q_UNUSED(editor);
        Q_UNUSED(model);
        Q_UNUSED(index);
    }
};
}

AcquisitionComponentPageRemapping::AcquisitionComponentPageRemapping(const Variant::Read &value,
                                                                     QWidget *parent)
        : AcquisitionComponentPage(value, parent)
{
    QVBoxLayout *topLevel = new QVBoxLayout(this);
    setLayout(topLevel);

    RemappingTableModel *model = new RemappingTableModel(value["Remap"], this);
    table = new QTableView(this);
    topLevel->addWidget(table, 1);
    table->setModel(model);

    table->setToolTip(tr("The table of all remapping data."));
    table->setStatusTip(tr("Remappings"));
    table->setWhatsThis(
            tr("This is the list of all remappings in use for the component.  Each remapping provides a means to alter an internal parameter before it is logged as data."));

    /* table->verticalHeader()->hide(); */
    table->setSelectionMode(QAbstractItemView::SingleSelection);
    table->setSelectionBehavior(QAbstractItemView::SelectRows);
    connect(table->selectionModel(), SIGNAL(selectionChanged(
                                                    const QItemSelection &, const QItemSelection &)),
            this, SLOT(selectedChanged()));

    table->horizontalHeader()
         ->setSectionResizeMode(RemappingTableModel::Column_AddRemove,
                                QHeaderView::ResizeToContents);

    table->horizontalHeader()
         ->setSectionResizeMode(RemappingTableModel::Column_Name, QHeaderView::ResizeToContents);

    table->horizontalHeader()
         ->setSectionResizeMode(RemappingTableModel::Column_Input, QHeaderView::ResizeToContents);

    table->horizontalHeader()->setStretchLastSection(true);
    table->horizontalHeader()
         ->setSectionResizeMode(RemappingTableModel::Column_Calibration, QHeaderView::Stretch);

    table->setItemDelegateForColumn(RemappingTableModel::Column_AddRemove,
                                    new RemappingAddRemoveDelegate(this, table));


    table->setSpan(model->rowCount(QModelIndex()) - 1, 0, 1, RemappingTableModel::Column_TOTAL);


    calibration = new CalibrationEdit(this);
    topLevel->addWidget(calibration);
    calibration->setToolTip(tr("The calibration in effect at the selected time."));
    calibration->setStatusTip(tr("Calibration"));
    calibration->setWhatsThis(
            tr("This is the calibration polynomial that is applied to data at the selected time."));
    calibration->setEnabled(false);
    connect(calibration, SIGNAL(calibrationChanged(
                                        const CPD3::Calibration &)), this, SLOT(setCalibration(
                                                                                        const CPD3::Calibration &)));

    openAllEditors();
}

AcquisitionComponentPageRemapping::~AcquisitionComponentPageRemapping() = default;

void AcquisitionComponentPageRemapping::commit(Variant::Write &target)
{
    const auto &remappings = static_cast<RemappingTableModel *>(table->model())->getRemapping();
    target["Remap"].remove(false);
    for (const auto &add : remappings) {
        QString name(add.name.trimmed());
        if (name.isEmpty())
            continue;
        auto rd = target["Remap"].hash(name);
        if (add.calibration.size() > 0) {
            Variant::Composite::fromCalibration(rd.hash("Calibration"), add.calibration);
        }
        if (add.input.read().exists()) {
            rd.hash("Input").set(add.input);
        }
    }
}

QString AcquisitionComponentPageRemapping::title() const
{ return tr("Remapping"); }

void AcquisitionComponentPageRemapping::addRow()
{
    table->model()->insertRows(table->model()->rowCount() - 1, 1);

    openAllEditors();
}

void AcquisitionComponentPageRemapping::removeRow()
{
    QObject *s = sender();
    if (!s)
        return;
    QVariant p(s->property("model-row"));
    if (!p.isValid())
        return;
    int row = p.toInt();
    if (row < 0 || row >= table->model()->rowCount() - 1)
        return;

    table->model()->removeRows(row, 1);

    openAllEditors();
}

void AcquisitionComponentPageRemapping::openAllEditors()
{
    RemappingTableModel *model = static_cast<RemappingTableModel *>(table->model());
    for (int row = 0; row < model->rowCount(QModelIndex()); ++row) {
        table->openPersistentEditor(model->index(row, RemappingTableModel::Column_AddRemove));
    }

    table->resizeColumnToContents(0);
}

void AcquisitionComponentPageRemapping::selectedChanged()
{
    QModelIndexList selectedRows(table->selectionModel()->selectedRows());
    if (selectedRows.size() == 1) {
        calibration->setEnabled(true);

        calibration->disconnect(this);
        for (QList<QModelIndex>::const_iterator index = selectedRows.constBegin(),
                end = selectedRows.constEnd(); index != end; ++index) {
            calibration->setCalibration(
                    static_cast<RemappingTableModel *>(table->model())->getCalibration(*index));
        }
        connect(calibration, SIGNAL(calibrationChanged(
                                            const CPD3::Calibration &)), this, SLOT(setCalibration(
                                                                                            const CPD3::Calibration &)));
    } else {
        calibration->setEnabled(false);
    }
}

void AcquisitionComponentPageRemapping::setCalibration(const CPD3::Calibration &cal)
{
    QModelIndexList selectedRows(table->selectionModel()->selectedRows());
    for (QModelIndexList::const_iterator index = selectedRows.constBegin(),
            end = selectedRows.constEnd(); index != end; ++index) {
        static_cast<RemappingTableModel *>(table->model())->setCalibration(*index, cal);
    }
}


AcquisitionComponentEditorDialog::AcquisitionComponentEditorDialog(const Variant::Read &config,
                                                                   const Variant::Read &metadata,
                                                                   QWidget *parent) : QDialog(
        parent), initialConfig(config), firstShow(true)
{
    initialConfig.detachFromRoot();

    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

    QVBoxLayout *topLevel = new QVBoxLayout(this);
    setLayout(topLevel);

    componentSelection = new QComboBox(this);
    topLevel->addWidget(componentSelection, 0);

    componentSelection->addItem(tr("General - Auto-detect and match any component type"));

    tabArea = new QTabWidget(this);
    topLevel->addWidget(tabArea, 1);

    QDialogButtonBox *buttons =
            new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal,
                                 this);
    topLevel->addWidget(buttons);
    connect(buttons, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttons, SIGNAL(rejected()), this, SLOT(reject()));

    connect(componentSelection, SIGNAL(currentIndexChanged(int)), this, SLOT(updateDisplay()));
    updateDisplay();
}

QSize AcquisitionComponentEditorDialog::sizeHint() const
{ return QDialog::sizeHint().expandedTo(QSize(800, 600)); }

void AcquisitionComponentEditorDialog::addComponentType(const QString &type,
                                                        const QString &text,
                                                        bool autodetectable)
{
    componentSelection->addItem(text, type);
    componentSelection->setItemData(componentSelection->count() - 1, autodetectable,
                                    Qt::UserRole + 1);
    if (initialConfig.hash("Name").toQString().toLower() == type)
        componentSelection->setCurrentIndex(componentSelection->count() - 1);
}

CPD3::Data::Variant::Root AcquisitionComponentEditorDialog::component() const
{
    Variant::Root result;
    auto wr = result.write();
    wr.setType(Variant::Type::Hash);

    {
        int idx = componentSelection->currentIndex();
        if (idx >= 0 && idx < componentSelection->count()) {
            QString name(componentSelection->itemData(idx).toString());
            if (!name.isEmpty()) {
                wr.hash("Name").setString(name);
            }
        }
    }

    for (QList<AcquisitionComponentPage *>::const_iterator p = currentPages.constBegin(),
            endP = currentPages.constEnd(); p != endP; ++p) {
        (*p)->commit(wr);
    }

    return result;
}

void AcquisitionComponentEditorDialog::updateDisplay()
{
    tabArea->clear();
    for (QList<AcquisitionComponentPage *>::const_iterator p = currentPages.constBegin(),
            endP = currentPages.constEnd(); p != endP; ++p) {
        delete (*p);
    }
    currentPages.clear();

    AcquisitionComponentPageGeneral
            *general = new AcquisitionComponentPageGeneral(initialConfig, tabArea);
    currentPages.append(general);
    tabArea->addTab(general, general->title());

    AcquisitionComponentPageGroups
            *groups = new AcquisitionComponentPageGroups(initialConfig, tabArea);
    currentPages.append(groups);
    tabArea->addTab(groups, groups->title());

    AcquisitionComponentPageDisplay
            *display = new AcquisitionComponentPageDisplay(initialConfig, tabArea);
    currentPages.append(display);
    tabArea->addTab(display, display->title());

    QList<AcquisitionComponentPage *> componentPages;
    {
        int idx = componentSelection->currentIndex();
        if (idx >= 0 && idx < componentSelection->count()) {
            QString name(componentSelection->itemData(idx).toString());
            if (!name.isEmpty()) {
                if (!componentSelection->itemData(idx, Qt::UserRole + 1).toBool()) {
                    general->disableFixedMode(false);
                }

                componentPages.append(
                        AcquisitionComponentEndpoint::createComponentPages(name, initialConfig,
                                                                           tabArea));
            } else {
                general->disableFixedMode();
            }
        } else {
            general->disableFixedMode();
        }
    }
    for (QList<AcquisitionComponentPage *>::const_iterator p = componentPages.constBegin(),
            endP = componentPages.constEnd(); p != endP; ++p) {
        currentPages.append(*p);
        tabArea->addTab(*p, (*p)->title());
    }

    AcquisitionComponentPageMetadata
            *metadata = new AcquisitionComponentPageMetadata(initialConfig, tabArea);
    currentPages.append(metadata);
    tabArea->addTab(metadata, metadata->title());

    AcquisitionComponentPageRemapping
            *remapping = new AcquisitionComponentPageRemapping(initialConfig, tabArea);
    currentPages.append(remapping);
    tabArea->addTab(remapping, remapping->title());

    connect(general, SIGNAL(fixedChanged(bool)), metadata, SLOT(setFixed(bool)));
    metadata->setFixed(general->fixedState());

    updateGeometry();
}

static bool isAutodetectable(const ComponentLoader::Information &info)
{
    return !info.excludeFromList("acquisition") && info.interface("acquisitionautoprobe");
}

void AcquisitionComponentEditorDialog::showEvent(QShowEvent *event)
{
    if (firstShow) {
        firstShow = false;

        auto loader = std::async(std::launch::async, [] {
            std::unordered_map<QString, ComponentLoader::Information> result;
            for (auto &add : ComponentLoader::list()) {
                if (!add.second.interface("acquisition"))
                    continue;

                result.emplace(add.first, std::move(add.second));
            }
            return result;
        });

        if (loader.wait_for(std::chrono::seconds(0)) == std::future_status::timeout) {
            QProgressDialog waitDialog(tr("Loading Components"), QString(), 0, 0, this);
            waitDialog.setWindowModality(Qt::ApplicationModal);
            waitDialog.setCancelButton(0);

            Threading::pollFunctor(&waitDialog, [&loader, &waitDialog]() {
                if (loader.wait_for(std::chrono::seconds(0)) == std::future_status::timeout)
                    return true;
                waitDialog.close();
                return false;
            });

            if (loader.wait_for(std::chrono::seconds(0)) == std::future_status::timeout)
                waitDialog.exec();
        }

        std::vector<ComponentLoader::Information> sorted;
        for (auto &add : loader.get()) {
            sorted.emplace_back(std::move(add.second));
        }
        std::sort(sorted.begin(), sorted.end(), ComponentLoader::Information::DisplayOrder());
        for (const auto &add : sorted) {
            addComponentType(add.componentName(),
                             tr("%1 - %2", "list entry").arg(add.componentName(), add.title()),
                             isAutodetectable(add));
        }

        updateDisplay();
        updateGeometry();
    }
    QDialog::showEvent(event);
}

}
}
}
