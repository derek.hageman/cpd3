/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3_GUIDATAACQUISITIONCOMPONENT_HXX
#define CPD3_GUIDATAACQUISITIONCOMPONENT_HXX

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QDialog>
#include <QTabWidget>
#include <QRadioButton>
#include <QLineEdit>
#include <QSpinBox>
#include <QCheckBox>
#include <QPushButton>
#include <QGroupBox>
#include <QTableView>

#include "guidata/guidata.hxx"
#include "guidata/valueeditor.hxx"
#include "guicore/setselect.hxx"
#include "guicore/calibrationedit.hxx"
#include "guidata/editors/acquisition/variablegroups.hxx"

namespace CPD3 {
namespace GUI {
namespace Data {

/**
 * A page used for configuring an acquisition component.
 */
class CPD3GUIDATA_EXPORT AcquisitionComponentPage : public QWidget {
Q_OBJECT
public:
    /**
     * Create the acquisition component page
     *
     * @param value     the current settings
     * @param parent    the parent widget
     */
    AcquisitionComponentPage(const CPD3::Data::Variant::Read &value, QWidget *parent = 0);

    virtual ~AcquisitionComponentPage();

    /**
     * Commit changes to the given value.
     *
     * @param target    the target value
     */
    virtual void commit(CPD3::Data::Variant::Write &target) = 0;

    /**
     * Get the title of the page.
     *
     * @return      the page title
     */
    virtual QString title() const = 0;
};

/**
 * A page for the general settings for the acquisition component.
 */
class CPD3GUIDATA_EXPORT AcquisitionComponentPageGeneral : public AcquisitionComponentPage {
Q_OBJECT

    QRadioButton *instrumentSelected;
    QLineEdit *instrument;
    QSpinBox *sampleLine;

    QCheckBox *fixed;

    QCheckBox *definedInterface;
    QPushButton *selectInterface;
    CPD3::Data::Variant::Root interfaceData;
    QCheckBox *fixedInterface;

    QCheckBox *passive;

    QCheckBox *priorityEnable;
    QSpinBox *priority;

public:
    AcquisitionComponentPageGeneral(const CPD3::Data::Variant::Read &value, QWidget *parent = 0);

    virtual ~AcquisitionComponentPageGeneral();

    void commit(CPD3::Data::Variant::Write &target) override;

    virtual QString title() const;

    /**
     * Set fixed mode to disabled.
     *
     * @param forceOff  force the fixed state to off
     */
    void disableFixedMode(bool forceOff = true);

    /**
     * Get the current state of the fixed property of the component.
     *
     * @return  true if the component is fixed
     */
    bool fixedState() const;

signals:

    /**
     * Emitted when the state of the fixed property of the component changes
     *
     * @param enabled   true when the component is in fixed mode
     */
    void fixedChanged(bool enabled);

private slots:

    void nameTypeSelected(bool fullName);

    void updateFixedState(bool fixed);

    void showInterfaceSelection();

    void updateInterfaceText();
};

/**
 * A page for the grouping settings for the acquisition component.
 */
class CPD3GUIDATA_EXPORT AcquisitionComponentPageGroups : public AcquisitionComponentPage {
Q_OBJECT

    SetSelector *groups;

    QCheckBox *enableOutputGroups;
    SetSelector *outputGroups;

    AcquisitionVariableGroupsEditor *variableGroups;
public:
    AcquisitionComponentPageGroups(const CPD3::Data::Variant::Read &value, QWidget *parent = 0);

    virtual ~AcquisitionComponentPageGroups();

    void commit(CPD3::Data::Variant::Write &target) override;

    virtual QString title() const;

};

/**
 * A page for the display settings for the acquisition component.
 */
class CPD3GUIDATA_EXPORT AcquisitionComponentPageDisplay : public AcquisitionComponentPage {
Q_OBJECT

    QGroupBox *menuEnable;
    QCheckBox *menuCharacterEnable;
    QLineEdit *menuCharacter;
    QLineEdit *menuEntry;
    QLineEdit *windowTitle;
public:
    AcquisitionComponentPageDisplay(const CPD3::Data::Variant::Read &value, QWidget *parent = 0);

    virtual ~AcquisitionComponentPageDisplay();

    void commit(CPD3::Data::Variant::Write &target) override;

    virtual QString title() const;

};

/**
 * A page for the metadata settings for the acquisition component.
 */
class CPD3GUIDATA_EXPORT AcquisitionComponentPageMetadata : public AcquisitionComponentPage {
Q_OBJECT

    QRadioButton *disableMatch;
    QRadioButton *matchSerialNumber;
    QSpinBox *matchedSerialNumber;
    QRadioButton *matchAdvanced;
    CPD3::Data::Variant::Root matchAdvancedData;

    QRadioButton *disableSource;
    QRadioButton *sourceSerialNumber;
    QSpinBox *sourceSetSerialNumber;
    QRadioButton *sourceAdvanced;
    CPD3::Data::Variant::Root sourceAdvancedData;

    CPD3::Data::Variant::Root globalMetadata;
    QLineEdit *stationOverride;

    QTableView *variableMetadata;

    void openAllEditors();

public:
    AcquisitionComponentPageMetadata(const CPD3::Data::Variant::Read &value, QWidget *parent = 0);

    virtual ~AcquisitionComponentPageMetadata();

    void commit(CPD3::Data::Variant::Write &target) override;

    virtual QString title() const;

public slots:

    /**
     * Set the fixed state of the interface.
     *
     * @param enabled the interface is in fixed mode.
     */
    void setFixed(bool enabled);

private slots:

    void showMatchedAdvancedDialog(bool toggled);

    void showSourceAdvancedDialog(bool toggled);

    void showGlobalMetadataDialog();

    void addVariableMetadataRow();

    void removeVariableMetadataRow();
};

/**
 * A page for the remapping settings for the acquisition component.
 */
class CPD3GUIDATA_EXPORT AcquisitionComponentPageRemapping : public AcquisitionComponentPage {
Q_OBJECT

    QTableView *table;
    CalibrationEdit *calibration;

    void openAllEditors();

public:
    AcquisitionComponentPageRemapping(const CPD3::Data::Variant::Read &value, QWidget *parent = 0);

    virtual ~AcquisitionComponentPageRemapping();

    void commit(CPD3::Data::Variant::Write &target) override;

    virtual QString title() const;

private slots:

    void addRow();

    void removeRow();

    void selectedChanged();

    void setCalibration(const CPD3::Calibration &cal);
};

namespace Internal {
class AcquisitionVariableMetadataEditorButton : public QPushButton {
Q_OBJECT
public:
    AcquisitionVariableMetadataEditorButton(QWidget *parent = 0);

    virtual ~AcquisitionVariableMetadataEditorButton();

public slots:

    void showEditor();

signals:

    void commitChanged(QWidget *source);
};
}

/**
 * An editor endpoint for acquisition components.
 */
class CPD3GUIDATA_EXPORT AcquisitionComponentEndpoint : public ValueEndpointEditor {
public:

    virtual bool matches(const CPD3::Data::Variant::Read &value,
                         const CPD3::Data::Variant::Read &metadata);

    bool edit(CPD3::Data::Variant::Write &value,
              const CPD3::Data::Variant::Read &metadata,
              QWidget *parent = 0) override;

    virtual QString collapsedText(const CPD3::Data::Variant::Read &value,
                                  const CPD3::Data::Variant::Read &metadata);

    /**
     * Create the pages for a given component type
     *
     * @param component the component type name
     * @param value     the current settings
     * @param parent    the parent widget
     * @return          a list of zero or more pages specific to that component
     */
    static QList<AcquisitionComponentPage *> createComponentPages(const QString &component,
                                                                  const CPD3::Data::Variant::Read &value = CPD3::Data::Variant::Read::empty(),
                                                                  QWidget *parent = 0);
};

/**
 * A dialog for editing an acquisition component.
 */
class CPD3GUIDATA_EXPORT AcquisitionComponentEditorDialog : public QDialog {
Q_OBJECT
    CPD3::Data::Variant::Read initialConfig;
    bool firstShow;

    QComboBox *componentSelection;
    QTabWidget *tabArea;
    QList<AcquisitionComponentPage *> currentPages;

    void addComponentType(const QString &type, const QString &text, bool autodetectable);

public:
    /**
     * Create the acquisition component editing dialog.
     *
     * @param config    the current configuration
     * @param metadata  the metadata for the smoother
     * @param parent    the parent widget
     */
    AcquisitionComponentEditorDialog(const CPD3::Data::Variant::Read &config,
                                     const CPD3::Data::Variant::Read &metadata = CPD3::Data::Variant::Read::empty(),
                                     QWidget *parent = 0);


    /**
     * Get the result of editing.
     *
     * @return  the resulting smoother configuration
     */
    CPD3::Data::Variant::Root component() const;

    virtual QSize sizeHint() const;

private slots:

    void updateDisplay();

protected:
    virtual void showEvent(QShowEvent *event);
};

}
}
}


#endif //CPD3_GUIDATAACQUISITIONCOMPONENT_HXX
