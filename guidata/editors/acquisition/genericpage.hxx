/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3GUIDATAGENERICPAGE_HXX
#define CPD3GUIDATAGENERICPAGE_HXX

#include "core/first.hxx"

#include <functional>
#include <QtGlobal>
#include <QObject>
#include <QCheckBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QLabel>
#include <QPushButton>
#include <QMenu>
#include <QAction>

#include "guidata/guidata.hxx"
#include "guidata/editors/acquisition/component.hxx"
#include "core/number.hxx"

namespace CPD3 {
namespace GUI {
namespace Data {

namespace Internal {
class AcquisitionComponentPageGenericWidget : public QWidget {
Q_OBJECT
    QString path;
    CPD3::Data::Variant::Read initialConfiguration;
public:
    AcquisitionComponentPageGenericWidget(const QString &path,
                                          const CPD3::Data::Variant::Read &config);

    virtual ~AcquisitionComponentPageGenericWidget();

    virtual void commit(CPD3::Data::Variant::Write &target) = 0;

    virtual void setUnassigned(bool unassigned);

    virtual void setUnassigned(double unassigned);

    virtual void setUnassigned(qint64 unassigned);

    virtual void setUnassigned(const QString &unassigned);

    virtual void setExplicitUndefined(bool enable = true);

    virtual void setDoubleSpinBox(int decimals, double minimum, double maximum, double step);

    virtual void setIntegerSpinBox(int minimum,
                                   int maximum,
                                   const NumberFormat &format = NumberFormat(1, 0));

    virtual void appendEnumEntry(const QString &label,
                                 const QStringList &keys,
                                 const QString &toolTip = QString());

protected:

    inline CPD3::Data::Variant::Write output(CPD3::Data::Variant::Write &input) const
    { return input.getPath(path); }

    inline CPD3::Data::Variant::Read input() const
    { return initialConfiguration.getPath(path); }

protected slots:

    virtual void changed();
};

class AcquisitionComponentPageGenericOneLine : public AcquisitionComponentPageGenericWidget {
Q_OBJECT
    QCheckBox *enabledChecked;
    QHBoxLayout *activeLayout;

    void reconfigure();

public:
    AcquisitionComponentPageGenericOneLine(const QString &label,
                                           const QString &path,
                                           const CPD3::Data::Variant::Read &config);

    virtual ~AcquisitionComponentPageGenericOneLine();

    virtual void setExplicitUndefined(bool enable = true);

protected:
    void addLineItem(QWidget *item, int stretch = 1);

    void addStretch(int stretch = 1);

    inline bool isExplicitlyUndefined() const
    { return enabledChecked->checkState() == Qt::PartiallyChecked; }

    inline bool isSet() const
    { return enabledChecked->checkState() != Qt::Unchecked; }

    inline QHBoxLayout *lineLayout() const
    { return activeLayout; }

    virtual void setComponentEnabled(bool enabled);

private slots:

    void checkedChanged();
};

class AcquisitionComponentPageGenericBox : public AcquisitionComponentPageGenericWidget {
Q_OBJECT
    QGroupBox *box;
public:
    AcquisitionComponentPageGenericBox(const QString &label,
                                       const QString &path,
                                       const CPD3::Data::Variant::Read &config);

    virtual ~AcquisitionComponentPageGenericBox();

protected:
    inline QWidget *layoutTarget() const
    { return box; }

    virtual void setComponentEnabled(bool enabled);

    inline bool isSet() const
    { return box->isChecked(); }

private slots:

    void checkedChanged();
};

class AcquisitionComponentPageGenericFlagsSelection
        : public AcquisitionComponentPageGenericOneLine {
Q_OBJECT

    QPushButton *button;
    QMenu *menu;
    QAction *delimiter;
    QHash<QString, QAction *> flags;
public:

    AcquisitionComponentPageGenericFlagsSelection(const QString &label,
                                                  const QString &path,
                                                  const CPD3::Data::Variant::Read &config);

    virtual ~AcquisitionComponentPageGenericFlagsSelection();

    void commit(CPD3::Data::Variant::Write &target) override;

private slots:

    void addFlag();

    void flagToggled();
};

}

/**
 * A generic page implementation allowing for simple customization for acquisition
 * components.
 */
class CPD3GUIDATA_EXPORT AcquisitionComponentPageGeneric : public AcquisitionComponentPage {
Q_OBJECT

    QString pageTitle;
    CPD3::Data::Variant::Read initialConfiguration;
    QVBoxLayout *topLevelLayout;

    QGroupBox *activeBox;
    QVBoxLayout *layoutTarget;

    QList<Internal::AcquisitionComponentPageGenericWidget *> components;

    void addWidget(Internal::AcquisitionComponentPageGenericWidget *widget);

public:

    /**
     * Create the generic page.
     *
     * @param title     the title
     * @param value     the configuration value
     * @param parent    the parent widget
     */
    AcquisitionComponentPageGeneric(const QString &title, const CPD3::Data::Variant::Read &value,
                                    QWidget *parent = 0);

    virtual ~AcquisitionComponentPageGeneric();

    /**
     * Commit changes to the given value.
     *
     * @param target    the target value
     */
    void commit(CPD3::Data::Variant::Write &target) override;

    /**
     * Get the title of the page.
     *
     * @return      the page title
     */
    virtual QString title() const;

    /**
     * Begin a grouping box.
     *
     * @param label the box label
     */
    void beginBox(const QString &label);

    /**
     * End the active grouping box.
     */
    void endBox();

    /**
     * Set the unassigned boolean value.
     *
     * @param unassigned    the unassigned value
     */
    void setLastUnassigned(bool unassigned);

    /**
     * Set the unassigned double value.
     *
     * @param unassigned    the unassigned value
     */
    void setLastUnassigned(double unassigned);

    /**
     * Set the unassigned integer value.
     *
     * @param unassigned    the unassigned value
     */
    void setLastUnassigned(qint64 unassigned);

    /**
     * Set the unassigned string value.
     *
     * @param unassigned    the unassigned value
     */

    void setLastUnassigned(const QString &unassigned);

    /**
     * Set if the component can be explicitly undefined.
     *
     * @param enable    enable explicit undefined
     */
    void setLastExplicitUndefined(bool enable = true);

    /**
     * Add a boolean element.
     *
     * @param label     the label
     * @param path      the configuration path
     */
    void addBoolean(const QString &label, const QString &path);

    /**
     * Add an enum combobox.
     *
     * @param label     the label
     * @param path      the configuration path
     */
    void addEnum(const QString &label, const QString &path);

    /**
     * Add a possible value the the enumeration selection.
     *
     * @param label     the entry label
     * @param keys      the possible keys
     * @param toolTip   the tool tip of the entry
     */
    void appendLastEnumEntry(const QString &label,
                             const QStringList &keys,
                             const QString &toolTip = QString());

    inline void appendLastEnumEntry(const QString &label,
                                    const QString &key,
                                    const QString &toolTip = QString())
    { return appendLastEnumEntry(label, QStringList(key), toolTip); }

    /**
     * Add a single string entry.
     *
     * @param label     the label
     * @param path      the configuration path
     */
    void addSingleLineString(const QString &label, const QString &path);

    /**
     * Add a multiple line string entry.
     *
     * @param label     the label
     * @param path      the configuration path
     */
    void addMultipleLineString(const QString &label, const QString &path);

    /**
     * Add a script string entry.
     *
     * @param label     the label
     * @param path      the configuration path
     */
    void addScript(const QString &label, const QString &path);

    /**
     * Add a manual entry double element.
     *
     * @param label     the label
     * @param path      the configuration path
     * @param override  the override value
     */
    void addDouble(const QString &label, const QString &path, double override = FP::undefined());


    /**
     * Add a double spin box entry.
     *
     * @param label     the label
     * @param path      the configuration path
     * @param override  the the override value
     */
    void addDoubleSpinBox(const QString &label,
                          const QString &path,
                          double override = FP::undefined());

    /**
     * Add an array of double spin box entries.
     *
     * @param label     the label
     * @param path      the configuration path
     * @param count     the array size
     * @param override  the override value
     */
    void addDoubleSpinBoxArray(const QString &label,
                               const QString &path,
                               int count,
                               double override = FP::undefined());

    /**
     * Configure the double spin box.
     *
     * @param decimals  the decimal places
     * @param minimum   the minimum value
     * @param maximum   the maximum value
     */
    void setLastDoubleSpinBox(int decimals, double minimum, double maximum, double step = 0.1);

    /**
     * Add an integer spin box.
     *
     * @param label     the label
     * @param path      the configuration path
     * @param override  the override value
     */
    void addIntegerSpinBox(const QString &label,
                           const QString &path,
                           qint64 override = INTEGER::undefined());

    /**
     * Configure the integer spin box.
     *
     * @param minimum   the minimum value
     * @param maximum   the maximum value
     * @param format    the number format
     */
    void setLastIntegerSpinBox(int minimum,
                               int maximum,
                               const NumberFormat &format = NumberFormat(1, 0));


    /**
     * Add a calibration entry.
     *
     * @param label     the label
     * @param path      the configuration path
     */
    void addCalibration(const QString &label, const QString &path);

    /**
     * Add a flags selection editor.
     *
     * @param label     the label
     * @param path      the configuration path
     */
    void addFlagsSelection(const QString &label, const QString &path);

    /**
     * Add a time interval selection.
     *
     * @param label     the label
     * @param path      the configuration path
     * @param metadata  the metadata used to configure the editor
     */
    void addTimeIntervalSelection(const QString &label,
                                  const QString &path,
                                  const CPD3::Data::Variant::Read &metadata = CPD3::Data::Variant::Read::empty());

    /**
     * Add a smoother selection.
     *
     * @param label     the label
     * @param path      the configuration path
     * @param metadata  the metadata used to configure the editor
     */
    void addSmoother(const QString &label,
                     const QString &path,
                     const CPD3::Data::Variant::Read &metadata = CPD3::Data::Variant::Read::empty());

    /**
     * Add a dyanmic input selection.
     *
     * @param label     the label
     * @param path      the configuration path
     * @param metadata  the metadata used to configure the editor
     */
    void addDynamicInput(const QString &label,
                         const QString &path,
                         const CPD3::Data::Variant::Read &metadata = CPD3::Data::Variant::Read::empty());

    /**
     * Add a dyanmic selection selection.
     *
     * @param label     the label
     * @param path      the configuration path
     * @param metadata  the metadata used to configure the editor
     */
    void addDynamicSelection(const QString &label, const QString &path);

    /**
     * Add a generic value editor.
     *
     * @param label     the label
     * @param path      the configuration path
     * @param metadata  the metadata used to configure the editor
     */
    void addGenericValue(const QString &label,
                         const QString &path,
                         const CPD3::Data::Variant::Read &metadata = CPD3::Data::Variant::Read::empty());

    /**
     * Add an editor for system wide commands.
     *
     * @param label     the label
     * @param path      the configuration path
     */
    inline void addGlobalCommands(const QString &label, const QString &path)
    { addGenericValue(label, path); }

    /**
     * Add an editor for instrument specific commands.
     *
     * @param label     the label
     * @param path      the configuration path
     */
    inline void addInstrumentCommands(const QString &label, const QString &path)
    { addGenericValue(label, path); }

    /**
     * Add an editor tap chain processing.
     *
     * @param label     the label
     * @param path      the configuration path
     */
    inline void addTapChainProcessing(const QString &label, const QString &path)
    { addGenericValue(label, path); }

    /**
     * Add an editor that shows a dialog to do the editing.
     *
     * @param label     the label
     * @param path      the configuration path
     * @param dialog    the dialog callback
     * @param buttonText the text shown on the editing button
     */
    void addGenericDialog(const QString &label,
                          const QString &path,
                          std::function<void(CPD3::Data::Variant::Write &, QWidget *)> dialog,
                          const QString &buttonText = tr("Edit"));

    /**
     * Set the stretch for the last added entry.
     *
     * @param stretch   the stretch factor
     */
    void setLastStretch(int stretch = 1);


    /**
     * Add a stretch element to the current layout.
     *
     * @param stretch   the stretch factor
     */
    void addStretch(int stretch = 1);


    /**
     * Set the tool tip for the last added element.
     *
     * @param toolTip   the tool tip
     */
    void setLastToolTip(const QString &toolTip);

    /**
     * Set the status tip for the last added element.
     *
     * @param statusTip the status tip
     */
    void setLastStatusTip(const QString &statusTip);

    /**
     * Set the what's this text for the last added element.
     *
     * @param whatsThis the what's this text
     */
    void setLastWhatsThis(const QString &whatsThis);

};

}
}
}

#endif //CPD3GUIDATAGENERICPAGE_HXX
