/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3GUIDATAIOVARIABLES_HXX
#define CPD3GUIDATAIOVARIABLES_HXX

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QWidget>
#include <QAbstractItemDelegate>
#include <QTableView>
#include <QAbstractItemModel>
#include <QPushButton>

#include "guidata/guidata.hxx"
#include "core/number.hxx"
#include "guidata/editors/acquisition/component.hxx"

namespace CPD3 {
namespace GUI {
namespace Data {

namespace Internal {
class AcquisitionComponentGenericTableColumn {
    QString title;
    QString path;
    bool persistent;
    QVariant defaultDisplay;
    CPD3::Data::Variant::Root creationValue;
    QString toolTip;
    QString statusTip;
    QString whatsThis;
public:
    AcquisitionComponentGenericTableColumn(const QString &title,
                                           const QString &path,
                                           bool persistent);

    virtual ~AcquisitionComponentGenericTableColumn();

    virtual void commit(const QVariant &data, CPD3::Data::Variant::Write &target) = 0;

    inline void commit(const QString &key, const QVariant &data, CPD3::Data::Variant::Write &target)
    {
        CPD3::Data::Variant::Write v(target.getPath(path.arg(key)));
        commit(data, v);
    }

    virtual bool isSortable() const;

    virtual bool sortCompare(const QVariant &a, const QVariant &b, bool ascending);

    virtual void setDoubleSpinBox(int decimals,
                                  double minimum,
                                  double maximum,
                                  double step,
                                  bool allowUndefined = true);

    virtual void setIntegerSpinBox(int minimum,
                                   int maximum,
                                   const NumberFormat &format = NumberFormat(1, 0),
                                   bool allowUndefined = true);

    virtual void setBoolean(const QString &enabled, const QString &disabled, bool allowUndefined);

    virtual void setString(bool emptyIsUndefined, bool allowUndefined = true);

    virtual QVariant initializeData(const CPD3::Data::Variant::Read &input);

    inline QVariant initializeData(const QString &key, const CPD3::Data::Variant::Read &input)
    { return initializeData(input.getPath(path.arg(key))); }

    virtual QVariant displayData(const QVariant &data);

    virtual QVariant editData(const QVariant &data);

    bool setData(const QVariant &input, QVariant &output);

    inline bool persistentEditors() const
    { return this->persistent; }

    inline QString getTitle() const
    { return this->title; }

    inline void setDefaults(const QVariant &display, const CPD3::Data::Variant::Read &creation)
    {
        this->defaultDisplay = display;
        this->creationValue.write().set(creation);
    }

    inline QVariant getDefaultDisplay() const
    { return defaultDisplay; }

    inline CPD3::Data::Variant::Read getCreationValue() const
    { return creationValue.read(); }

    inline void setToolTip(const QString &toolTip)
    { this->toolTip = toolTip; }

    inline void setStatusTip(const QString &statusTip)
    { this->statusTip = statusTip; }

    inline void setWhatsThis(const QString &whatsThis)
    { this->whatsThis = whatsThis; }

    inline QString getToolTip() const
    { return this->toolTip; }

    inline QString getStatusTip() const
    { return this->statusTip; }

    inline QString getWhatsThis() const
    { return this->whatsThis; }
};

class AcquisitionComponentGenericTableEditorButton : public QPushButton {
Q_OBJECT

    QVariant attachedValue;
    QModelIndex attachedIndex;
public:
    AcquisitionComponentGenericTableEditorButton(QWidget *parent = 0);

    virtual ~AcquisitionComponentGenericTableEditorButton();

    void attach(const QModelIndex &index, const QVariant &value);

private slots:

    void showEditor();

protected:

    virtual bool editData(QVariant &value) = 0;
};


class AcquisitionComponentGenericTableDelegateSharedFlagsButton : public QPushButton {
Q_OBJECT

    QMenu *menu;
    QAction *delimiter;
    QHash<QString, QAction *> flags;
    QModelIndex attachedIndex;

    void updateState();

public:
    AcquisitionComponentGenericTableDelegateSharedFlagsButton(QWidget *parent = 0);

    virtual ~AcquisitionComponentGenericTableDelegateSharedFlagsButton();

    void attach(const QModelIndex &index, const QVariant &value);

    QSet<QString> getSelected() const;

private slots:

    void addFlag();

    void flagToggled();

public slots:

    void addLocalFlag(const QString &flag);

signals:

    void addSharedFlag(const QString &flag);
};

class AcquisitionComponentGenericTableDelegateSharedFlags : public QItemDelegate {
Q_OBJECT
public:
    AcquisitionComponentGenericTableDelegateSharedFlags(QObject *parent = 0);

    virtual ~AcquisitionComponentGenericTableDelegateSharedFlags();

    virtual QWidget *createEditor(QWidget *parent,
                                  const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const;

    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const;

    virtual void setModelData(QWidget *editor,
                              QAbstractItemModel *model,
                              const QModelIndex &index) const;

signals:

    void addSharedFlag(const QString &flag);
};

}

/**
 * A generic page that represents a table of parameters and settings.  The most common
 * use is a table of variables.
 */
class CPD3GUIDATA_EXPORT AcquisitionComponentGenericTable : public AcquisitionComponentPage {
Q_OBJECT

    QString pageTitle;
    CPD3::Data::Variant::Read initialValue;
    QString masterPath;
    bool arrayMode;
    int arrayFlags;

    typedef QPair<QString, bool> CommitRemove;
    QList<CommitRemove> commitRemove;

    class Model : public QAbstractItemModel {
        AcquisitionComponentGenericTable *table;

        struct Row {
            QString id;
            QList<QVariant> data;
        };
        QList<Row> rows;

        class RowSort {
            Internal::AcquisitionComponentGenericTableColumn *column;
            int index;
            bool ascending;
        public:
            RowSort(Internal::AcquisitionComponentGenericTableColumn *column,
                    int index,
                    bool ascending);

            bool operator()(const Row &a, const Row &b) const;
        };

        static inline bool rowIDAscending(const Row &a, const Row &b)
        { return a.id < b.id; }

        static inline bool rowIDDescending(const Row &a, const Row &b)
        { return a.id > b.id; }

    public:
        Model(AcquisitionComponentGenericTable *table);

        virtual ~Model();

        void initialize();

        virtual QModelIndex index(int row,
                                  int column,
                                  const QModelIndex &parent = QModelIndex()) const;

        virtual QModelIndex parent(const QModelIndex &index) const;

        virtual int rowCount(const QModelIndex &parent = QModelIndex()) const;

        virtual int columnCount(const QModelIndex &parent = QModelIndex()) const;

        virtual QVariant data(const QModelIndex &index, int role) const;

        virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const;

        virtual bool setData(const QModelIndex &index, const QVariant &value, int role);

        virtual Qt::ItemFlags flags(const QModelIndex &index) const;

        virtual bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex());

        virtual bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex());

        virtual void sort(int cIndex, Qt::SortOrder order = Qt::AscendingOrder);

        void commit(CPD3::Data::Variant::Write &target);
    };

    friend class Model;

    friend class Internal::AcquisitionComponentGenericTableEditorButton;

    Model *model;
    QTableView *table;

    QList<Internal::AcquisitionComponentGenericTableColumn *> columns;

    void openAllEditors();

public:

    /**
     * Create the generic table page.
     *
     * @param title     the title
     * @param initial   the initial configuration value
     * @param path      the path within the configuration of the main listing
     * @param parent    the parent widget
     */
    AcquisitionComponentGenericTable(const QString &title, const CPD3::Data::Variant::Read &initial,
                                     const QString &path,
                                     QWidget *parent = 0);

    virtual ~AcquisitionComponentGenericTable();

    void commit(CPD3::Data::Variant::Write &target) override;

    virtual QString title() const;

    enum {
        /** Display zero-based array indices */
                Array_ZeroBased = 0x01,

        /** Interpret a single hash value as an array index */
                Array_SingleHash = 0x02,

        /** Interpret a hash as a set of children */
                Array_HashChildren = 0x04,
    };

    /**
     * Change the table to array primary output mode.
     *
     * @param enable    enable array mode
     */
    void setArrayMode(bool enable = true, int flags = 0);

    /**
     * Add a path to remove on commit.
     *
     * @param path          the path to remove
     * @param emptyOnly     only remove if empty after commit
     */
    void addCommitRemovePath(const QString &path, bool emptyOnly = false);


    /**
     * Add a generic column to the table
     *
     * @param title         the column title
     * @param path          the associated path
     * @param delegate      the delegate for the column
     * @param persistent    if set then open the column as persistent editors
     */
    void addColumn(const QString &title,
                   const QString &path,
                   QAbstractItemDelegate *delegate,
                   bool persistent = true);

    /**
     * Add an integer spin box column.
     *
     * @param title         the column title
     * @param path          the associated path
     * @param persistent    if set then open the column as persistent editors
     */
    void addIntegerSpinBox(const QString &title, const QString &path, bool persistent = false);

    /**
     * Configure the integer spin box.
     *
     * @param minimum   the minimum value
     * @param maximum   the maximum value
     * @param format    the number format
     * @param allowUndefined    allow undefined values
     */
    void setLastIntegerSpinBox(int minimum,
                               int maximum,
                               const NumberFormat &format = NumberFormat(1, 0),
                               bool allowUndefined = true);

    /**
     * Add a double spin box column.
     *
     * @param title         the column title
     * @param path          the associated path
     * @param persistent    if set then open the column as persistent editors
     */
    void addDoubleSpinBox(const QString &title, const QString &path, bool persistent = false);


    /**
     * Configure the double spin box.
     *
     * @param decimals  the decimal places
     * @param minimum   the minimum value
     * @param maximum   the maximum value
     * @param allowUndefined    allow undefined values
     */
    void setLastDoubleSpinBox(int decimals,
                              double minimum,
                              double maximum,
                              double step = 0.1,
                              bool allowUndefined = true);


    /**
     * Add boolean column.
     *
     * @param title         the column title
     * @param path          the associated path
     * @param persistent    if set then open the column as persistent editors
     */
    void addBoolean(const QString &title, const QString &path, bool persistent = false);

    /**
     * Configure the last boolean.
     *
     * @param enabled           the text displayed for editing when true
     * @param disabled          the text displayed for editing when false
     * @param allowUndefined    allow undefined values
     */
    void setLastBoolean(const QString &enabled,
                        const QString &disabled,
                        bool allowUndefined = true);


    /**
     * Add a string column.
     *
     * @param title         the column title
     * @param path          the associated path
     * @param persistent    if set then open the column as persistent editors
     */
    void addString(const QString &title, const QString &path, bool persistent = false);

    /**
     * Configure the last string.
     *
     * @param emptyIsUndefined  treat empty strings as undefined
     * @param allowUndefined    allow undefined values
     */
    void setLastString(bool emptyIsUndefined, bool allowUndefined = true);

    /**
     * Add a string column with multiple line editing.
     *
     * @param title         the column title
     * @param path          the associated path
     * @param persistent    if set then open the column as persistent editors
     */
    void addStringLines(const QString &title, const QString &path);


    /**
     * Add a calibration column.
     *
     * @param title         the column title
     * @param path          the associated path
     */
    void addCalibration(const QString &title, const QString &path);


    /**
     * Add a metadata configuration column.
     *
     * @param title         the column title
     * @param path          the associated path
     */
    void addMetadata(const QString &title, const QString &path);

    /**
     * Add a dynamic input column.
     *
     * @param title         the column title
     * @param path          the associated path
     */
    void addDynamicInput(const QString &title, const QString &path);

    /**
     * Add a generic value column.
     *
     * @param title     the column title
     * @param path      the associated path
     * @param metadata  the metadata used to configure the editor
     */
    void addGenericValue(const QString &title,
                         const QString &path,
                         const CPD3::Data::Variant::Read &metadata = CPD3::Data::Variant::Read::empty());

    /**
     * Add a column for system wide commands.
     *
     * @param title     the column title
     * @param path      the associated path
     */
    void addGlobalCommands(const QString &title, const QString &path)
    { addGenericValue(title, path); }

    /**
     * Add a column for instrument specific commands.
     *
     * @param title     the column title
     * @param path      the associated path
     */
    inline void addInstrumentCommands(const QString &title, const QString &path)
    { addGenericValue(title, path); }

    /**
     * Add a column for combined commands.
     *
     * @param title     the column title
     * @param path      the associated path
     */
    void addCombinedCommands(const QString &title, const QString &path);

    /**
     * Add a column for a time interval.
     *
     * @param title         the column title
     * @param path          the associated path
     * @param persistent    if set then open the column as persistent editors
     */
    void addTimeIntervalSelection(const QString &title,
                                  const QString &path,
                                  bool persistent = false);

    /**
     * Add a flags column.
     *
     * @param title         the column title
     * @param path          the associated path
     */
    void addFlags(const QString &title, const QString &path);


    /**
     * Set the defaults for the last column
     *
     * @param display       the default display value
     * @param creation      the creation value
     */
    void setLastDefaults(const QVariant &display,
                         const CPD3::Data::Variant::Read &creation = CPD3::Data::Variant::Read::empty());


    /**
     * Set the tool tip for the last added element.
     *
     * @param toolTip   the tool tip
     */
    void setLastToolTip(const QString &toolTip);

    /**
     * Set the status tip for the last added element.
     *
     * @param statusTip the status tip
     */
    void setLastStatusTip(const QString &statusTip);

    /**
     * Set the what's this text for the last added element.
     *
     * @param whatsThis the what's this text
     */
    void setLastWhatsThis(const QString &whatsThis);

private slots:

    void initializeTable();

    void addRow();

    void removeRow();
};

}
}
}

#endif //CPD3GUIDATAIOVARIABLES_HXX
