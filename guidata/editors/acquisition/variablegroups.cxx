/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QVBoxLayout>
#include <QDialog>
#include <QDialogButtonBox>
#include <QVBoxLayout>
#include <QAbstractTableModel>
#include <QHeaderView>
#include <QItemEditorFactory>
#include <QPushButton>

#include "guidata/editors/acquisition/variablegroups.hxx"
#include "datacore/sequencematch.hxx"
#include "core/util.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace GUI {
namespace Data {

/** @file guidata/editors/acquisition/variablegroups.hxx
 * Value editors for acquisition variable group assignment.
 */


bool AcquisitionVariableGroupsEndpoint::matches(const Variant::Read &value,
                                                const Variant::Read &metadata)
{
    return Util::equal_insensitive(metadata.metadata("Editor").hash("Type").toString(),
                                   "AcquisitionVariableGroups");
}

bool AcquisitionVariableGroupsEndpoint::edit(Variant::Write &value,
                                             const Variant::Read &metadata,
                                             QWidget *parent)
{
    auto editor = metadata.metadata("Editor");

    QDialog dialog(parent);
    dialog.setWindowTitle(QObject::tr("Groups"));
    QVBoxLayout *layout = new QVBoxLayout(&dialog);
    dialog.setLayout(layout);

    QLabel *label = new QLabel(metadata.metadata("Description").toDisplayString());
    label->setWordWrap(true);
    layout->addWidget(label);

    AcquisitionVariableGroupsEditor
            *selector = new AcquisitionVariableGroupsEditor(value, metadata, editor, &dialog);
    label->setBuddy(selector);
    layout->addWidget(selector);

    layout->addStretch(1);

    QDialogButtonBox *buttons =
            new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal,
                                 &dialog);
    layout->addWidget(buttons);
    QObject::connect(buttons, SIGNAL(accepted()), &dialog, SLOT(accept()));
    QObject::connect(buttons, SIGNAL(rejected()), &dialog, SLOT(reject()));

    if (dialog.exec() != QDialog::Accepted)
        return false;

    value.setEmpty();
    value.set(selector->variableGroups());
    return true;
}


namespace {
class VariableGroupsTableModel : public QAbstractItemModel {

public:
    struct GroupData {
        Variant::Root selection;
        Variant::Flags groups;

        GroupData() = default;

        explicit GroupData(const Variant::Read &group) : selection(group.hash("Variable")),
                                                         groups(group.hash("Groups").toFlags())
        { }
    };
    enum {
        Column_AddRemove, Column_Variable, Column_Groups,

        Column_TOTAL
    };

private:

    std::vector<GroupData> groups;

    static QString convertVariableSelection(const Variant::Read &sel)
    {
        switch (sel.getType()) {
        case Variant::Type::String:
            return sel.toQString();
        case Variant::Type::Hash:
            return sel.hash("Variable").toQString();
        default:
            break;
        }
        return QString();
    }

public:

    VariableGroupsTableModel(const Variant::Read &v, AcquisitionVariableGroupsEditor *parent)
            : QAbstractItemModel(parent)
    {
        for (auto child : v.toChildren()) {
            groups.emplace_back(child);
        }
    }

    virtual QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const
    {
        Q_UNUSED(parent);
        return createIndex(row, column);
    }

    virtual QModelIndex parent(const QModelIndex &index) const
    {
        Q_UNUSED(index);
        return QModelIndex();
    }

    virtual int rowCount(const QModelIndex &parent) const
    {
        Q_UNUSED(parent);
        return groups.size() + 1;
    }

    virtual int columnCount(const QModelIndex &parent) const
    {
        Q_UNUSED(parent);
        return Column_TOTAL;
    }

    virtual QVariant data(const QModelIndex &index, int role) const
    {
        if (!index.isValid())
            return QVariant();
        if (index.row() >= static_cast<int>(groups.size())) {
            if (role == Qt::EditRole) {
                if (index.column() == 0)
                    return 1;
            }
            return QVariant();
        }
        const auto &data = groups[index.row()];

        switch (role) {
        case Qt::DisplayRole:
            switch (index.column()) {
            case Column_Variable:
                return convertVariableSelection(data.selection);
            case Column_Groups: {
                QStringList sorted;
                for (const auto &add : data.groups) {
                    sorted.push_back(QString::fromStdString(add));
                }
                std::sort(sorted.begin(), sorted.end());
                return sorted.join(tr(",", "groups join"));
            }
            default:
                break;
            }
            break;

        case Qt::EditRole:
            switch (index.column()) {
            case Column_Variable:
                return convertVariableSelection(data.selection);
            case Column_Groups: {
                QSet<QString> set;
                for (const auto &add : data.groups) {
                    set.insert(QString::fromStdString(add));
                }
                return QVariant::fromValue<QSet<QString>>(set);
            }
            default:
                break;
            }
            break;

        default:
            break;
        }
        return QVariant();
    }

    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const
    {
        if (role != Qt::DisplayRole)
            return QVariant();
        if (orientation == Qt::Vertical)
            return QVariant(section + 1);

        switch (section) {
        case Column_Variable:
            return tr("Variable");
        case Column_Groups:
            return tr("Groups");
        default:
            break;
        }
        return QVariant();
    }

    virtual bool setData(const QModelIndex &index, const QVariant &value, int role)
    {
        if (role != Qt::EditRole)
            return false;

        if (index.row() >= static_cast<int>(groups.size()))
            return false;

        switch (index.column()) {
        case Column_Variable: {
            QString str(value.toString().trimmed());
            if (str.isEmpty())
                return false;
            groups[index.row()].selection.write().setString(str);
            emit dataChanged(index, index);
            return true;
        }
        case Column_Groups: {
            groups[index.row()].groups = Util::set_from_qstring(value.value<QSet<QString>>());
            emit dataChanged(index, index);
            return true;
        }
        default:
            break;
        }

        return false;
    }

    virtual Qt::ItemFlags flags(const QModelIndex &index) const
    {
        if (!index.isValid())
            return 0;

        switch (index.column()) {
        case Column_Variable:
            return Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled;
        case Column_Groups:
            return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
        default:
            break;
        }

        return 0;
    }

    virtual bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex())
    {
        Q_ASSERT(row + count <= static_cast<int>(groups.size()));

        beginRemoveRows(parent, row, row + count - 1);
        auto first = groups.begin() + row;
        auto last = first + count;
        groups.erase(first, last);
        endRemoveRows();

        return true;
    }

    virtual bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex())
    {
        beginInsertRows(parent, row, row + count - 1);
        GroupData base;
        base.selection.write().setString(".*");
        groups.insert(groups.begin() + row, static_cast<std::size_t>(count), base);
        endInsertRows();

        return true;
    }

    inline const std::vector<GroupData> &getGroupData() const
    { return groups; }
};

class VariableGroupsAddRemoveButton : public QPushButton {
public:
    VariableGroupsAddRemoveButton(const QString &text, QWidget *parent = 0) : QPushButton(text,
                                                                                          parent)
    { }

    virtual ~VariableGroupsAddRemoveButton()
    { }

    virtual QSize sizeHint() const
    {
        QSize textSize(fontMetrics().size(Qt::TextShowMnemonic, text()));
        QStyleOptionButton opt;
        opt.initFrom(this);
        opt.rect.setSize(textSize);
        return style()->sizeFromContents(QStyle::CT_PushButton, &opt, textSize, this);
    }
};

class VariableGroupsAddRemoveDelegate : public QItemDelegate {
    AcquisitionVariableGroupsEditor *editor;
public:
    VariableGroupsAddRemoveDelegate(AcquisitionVariableGroupsEditor *e, QObject *parent = 0)
            : QItemDelegate(parent), editor(e)
    { }

    virtual ~VariableGroupsAddRemoveDelegate()
    { }

    virtual QWidget *createEditor(QWidget *parent,
                                  const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const
    {
        Q_UNUSED(option);

        QPushButton *button = new VariableGroupsAddRemoveButton(tr("-", "remove text"), parent);
        button->setProperty("model-row", index.row());

        return button;
    }

    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
    {
        QPushButton *button = static_cast<QPushButton *>(editor);

        button->disconnect(editor);

        QVariant data(index.data(Qt::EditRole));
        if (data.toInt() != 1) {
            button->setText(tr("-", "remove text"));
            connect(button, SIGNAL(clicked(bool)), this->editor, SLOT(removeRow()));
        } else {
            button->setText(tr("Add", "add text"));
            connect(button, SIGNAL(clicked(bool)), this->editor, SLOT(addRow()));
        }

        button->setProperty("model-row", index.row());
    }

    virtual void setModelData(QWidget *editor,
                              QAbstractItemModel *model,
                              const QModelIndex &index) const
    {
        Q_UNUSED(editor);
        Q_UNUSED(model);
        Q_UNUSED(index);
    }
};

class VariableGroupsSetDelegate : public QItemDelegate {
public:
    VariableGroupsSetDelegate(QObject *parent = 0) : QItemDelegate(parent)
    { }

    virtual ~VariableGroupsSetDelegate()
    { }

    virtual QWidget *createEditor(QWidget *parent,
                                  const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const
    {
        Q_UNUSED(option);
        Q_UNUSED(index);

        Internal::AcquisitionVariableGroupsSelector
                *editor = new Internal::AcquisitionVariableGroupsSelector(parent);
        connect(editor, SIGNAL(commitChanged(QWidget * )), this, SIGNAL(commitData(QWidget * )));
        return editor;
    }

    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
    {
        Internal::AcquisitionVariableGroupsSelector
                *selector = static_cast<Internal::AcquisitionVariableGroupsSelector *>(editor);
        selector->clear();
        selector->setSelected(index.data(Qt::EditRole).value<QSet<QString>>());
    }

    virtual void setModelData(QWidget *editor,
                              QAbstractItemModel *model,
                              const QModelIndex &index) const
    {
        Internal::AcquisitionVariableGroupsSelector
                *selector = static_cast<Internal::AcquisitionVariableGroupsSelector *>(editor);
        model->setData(index, QVariant::fromValue<QSet<QString>>(selector->getSelected()));
    }
};

}

Internal::AcquisitionVariableGroupsSelector::AcquisitionVariableGroupsSelector(QWidget *parent)
        : SetSelector(parent)
{
    connect(this, SIGNAL(selectionChanged()), this, SLOT(groupsChanged()));
}

Internal::AcquisitionVariableGroupsSelector::~AcquisitionVariableGroupsSelector()
{ }

void Internal::AcquisitionVariableGroupsSelector::groupsChanged()
{
    emit commitChanged(this);
}


AcquisitionVariableGroupsEditor::AcquisitionVariableGroupsEditor(const Variant::Read &config,
                                                                 const Variant::Read &metadata,
                                                                 const CPD3::Data::Variant::Read &editor,
                                                                 QWidget *parent) : QWidget(parent)
{
    Q_UNUSED(metadata);
    Q_UNUSED(editor);

    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    setLayout(layout);

    VariableGroupsTableModel *model = new VariableGroupsTableModel(config, this);
    table = new QTableView(this);
    layout->addWidget(table, 1);
    table->setModel(model);

    table->setToolTip(tr("The table of variable groupings."));
    table->setStatusTip(tr("Group selections"));
    table->setWhatsThis(tr("This table represents all the variable-group mappings present."));

    table->horizontalHeader()
         ->setSectionResizeMode(VariableGroupsTableModel::Column_AddRemove,
                                QHeaderView::ResizeToContents);

    table->horizontalHeader()
         ->setSectionResizeMode(VariableGroupsTableModel::Column_Variable, QHeaderView::Stretch);

    table->setItemDelegateForColumn(VariableGroupsTableModel::Column_AddRemove,
                                    new VariableGroupsAddRemoveDelegate(this, table));

    table->setItemDelegateForColumn(VariableGroupsTableModel::Column_Groups,
                                    new VariableGroupsSetDelegate(table));


    table->setSpan(model->rowCount(QModelIndex()) - 1, 0, 1,
                   VariableGroupsTableModel::Column_TOTAL);

    openAllEditors();
}

Variant::Root AcquisitionVariableGroupsEditor::variableGroups() const
{
    const auto
            &data = static_cast<const VariableGroupsTableModel *>(table->model())->getGroupData();
    if (data.empty())
        return Variant::Root();

    Variant::Root result;
    auto base = result.write().toArray();
    for (const auto &g : data) {
        auto target = base.after_back();
        target.hash("Variable").set(g.selection);
        target.hash("Groups").setFlags(g.groups);
    }
    return result;
}

void AcquisitionVariableGroupsEditor::addRow()
{
    table->model()->insertRows(table->model()->rowCount() - 1, 1);

    openAllEditors();
}

void AcquisitionVariableGroupsEditor::removeRow()
{
    QObject *s = sender();
    if (!s)
        return;
    QVariant p(s->property("model-row"));
    if (!p.isValid())
        return;
    int row = p.toInt();
    if (row < 0 || row >= table->model()->rowCount() - 1)
        return;

    table->model()->removeRows(row, 1);

    openAllEditors();
}

void AcquisitionVariableGroupsEditor::openAllEditors()
{
    VariableGroupsTableModel *model = static_cast<VariableGroupsTableModel *>(table->model());
    for (int row = 0; row < model->rowCount(QModelIndex()); ++row) {
        table->openPersistentEditor(model->index(row, 0));

        if (row == model->rowCount(QModelIndex()) - 1)
            continue;

        table->openPersistentEditor(model->index(row, 2));
    }

    table->resizeColumnToContents(0);
}

}
}
}

