/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QDialogButtonBox>
#include <QFormLayout>

#include "guidata/editors/acquisition/component.hxx"
#include "guidata/editors/acquisition/genericpage.hxx"
#include "guidata/editors/acquisition/generictable.hxx"
#include "guidata/editors/acquisition/pages.hxx"
#include "guidata/variableselect.hxx"

using namespace CPD3::Data;
using namespace CPD3::GUI::Data::Internal;

namespace CPD3 {
namespace GUI {
namespace Data {

namespace Internal {

AcquisitionComponentTabCompositePage::AcquisitionComponentTabCompositePage(const QString &title,
                                                                           const Variant::Read &value,
                                                                           QWidget *parent)
        : AcquisitionComponentPage(value, parent), pageTitle(title)
{
    QVBoxLayout *topLevel = new QVBoxLayout(this);
    setLayout(topLevel);
    tabs = new QTabWidget(this);
    topLevel->addWidget(tabs, 1);
}

AcquisitionComponentTabCompositePage::~AcquisitionComponentTabCompositePage()
{ }

void AcquisitionComponentTabCompositePage::commit(Variant::Write &target)
{
    for (QList<AcquisitionComponentPage *>::const_iterator p = subpages.constBegin(),
            endP = subpages.constEnd(); p != endP; ++p) {
        (*p)->commit(target);
    }
}

QString AcquisitionComponentTabCompositePage::title() const
{ return pageTitle; }

void AcquisitionComponentTabCompositePage::addPage(AcquisitionComponentPage *page)
{
    subpages.append(page);
    tabs->addTab(page, page->title());

    updateGeometry();
}


AcquisitionComponentBoxCompositePage::AcquisitionComponentBoxCompositePage(const QString &title,
                                                                           const Variant::Read &value,
                                                                           QWidget *parent)
        : AcquisitionComponentPage(value, parent), pageTitle(title)
{
    layout = new QVBoxLayout(this);
    setLayout(layout);
}

AcquisitionComponentBoxCompositePage::~AcquisitionComponentBoxCompositePage()
{ }

void AcquisitionComponentBoxCompositePage::commit(Variant::Write &target)
{
    for (QList<AcquisitionComponentPage *>::const_iterator p = subpages.constBegin(),
            endP = subpages.constEnd(); p != endP; ++p) {
        (*p)->commit(target);
    }
}

QString AcquisitionComponentBoxCompositePage::title() const
{ return pageTitle; }

void AcquisitionComponentBoxCompositePage::addPage(AcquisitionComponentPage *page, int stretch)
{
    subpages.append(page);
    QGroupBox *box = new QGroupBox(page->title(), this);
    layout->addWidget(box, stretch);

    QVBoxLayout *boxLayout = new QVBoxLayout(box);
    box->setLayout(boxLayout);
    boxLayout->addWidget(page, 1);

    updateGeometry();
}


AcquisitionComponentTypedCompositePage::AcquisitionComponentTypedCompositePage(const QString &title,
                                                                               const Variant::Read &value,
                                                                               const QString &tPath,
                                                                               QWidget *parent)
        : AcquisitionComponentPage(value, parent), pageTitle(title), typePath(tPath)
{
    layout = new QVBoxLayout(this);
    setLayout(layout);

    typeSelect = new QComboBox(this);
    layout->addWidget(typeSelect);
    connect(typeSelect, SIGNAL(currentIndexChanged(int)), this, SLOT(typeChanged()));

    originalType = value.getPath(typePath).toQString().toLower();
}

AcquisitionComponentTypedCompositePage::~AcquisitionComponentTypedCompositePage()
{ }

void AcquisitionComponentTypedCompositePage::commit(Variant::Write &target)
{
    int index = typeSelect->currentIndex();
    if (index < 0 || index >= subpages.size())
        return;
    target.getPath(typePath).setString(typeSelect->currentData(Qt::UserRole).toString());
    subpages.at(index)->commit(target);
}

QString AcquisitionComponentTypedCompositePage::title() const
{ return pageTitle; }

void AcquisitionComponentTypedCompositePage::addPage(AcquisitionComponentPage *page,
                                                     const QStringList &typeCode,
                                                     const QString &toolTip)
{
    subpages.append(page);
    typeSelect->addItem(page->title(), typeCode.first());
    layout->addWidget(page, 1);

    bool selected = subpages.size() == 1;
    for (QStringList::const_iterator t = typeCode.constBegin(), endT = typeCode.constEnd();
            t != endT;
            ++t) {
        if (t->toLower() == originalType) {
            selected = true;
            break;
        }
    }

    if (selected)
        typeSelect->setCurrentIndex(typeSelect->count() - 1);
    page->setVisible(selected);

    updateGeometry();
}

void AcquisitionComponentTypedCompositePage::typeChanged()
{
    int index = typeSelect->currentIndex();
    for (int pi = 0, max = subpages.size(); pi < max; ++pi) {
        subpages.at(pi)->setVisible(pi == index);
    }

    updateGeometry();
}


AcquisitionComponentTableDelegatePages::AcquisitionComponentTableDelegatePages(QObject *parent)
        : QItemDelegate(parent)
{ }

AcquisitionComponentTableDelegatePages::~AcquisitionComponentTableDelegatePages()
{ }

namespace {

class ComponentTableDelegatePagesEditor : public AcquisitionComponentGenericTableEditorButton {
    const AcquisitionComponentTableDelegatePages *delegate;
public:
    ComponentTableDelegatePagesEditor(const AcquisitionComponentTableDelegatePages *d,
                                      QWidget *parent = 0)
            : AcquisitionComponentGenericTableEditorButton(parent), delegate(d)
    { }

    virtual ~ComponentTableDelegatePagesEditor()
    { }

protected:
    virtual bool editData(QVariant &value)
    {
        Variant::Write v = value.value<Variant::Write>();

        QDialog dialog(this);
        dialog.setWindowTitle(tr("Value"));
        QVBoxLayout *layout = new QVBoxLayout(&dialog);
        dialog.setLayout(layout);

        QList<AcquisitionComponentPage *> pages(delegate->createPages(v, &dialog));
        if (pages.isEmpty())
            return false;

        if (pages.size() == 1) {
            layout->addWidget(pages.first(), 1);
            dialog.setWindowTitle(pages.first()->title());
        } else {
            QTabWidget *tabs = new QTabWidget(&dialog);
            layout->addWidget(tabs, 1);
            for (QList<AcquisitionComponentPage *>::const_iterator p = pages.constBegin(),
                    endP = pages.constEnd(); p != endP; ++p) {
                tabs->addTab(*p, (*p)->title());
            }
        }

        QDialogButtonBox *buttons =
                new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                                     Qt::Horizontal, &dialog);
        layout->addWidget(buttons);
        QObject::connect(buttons, SIGNAL(accepted()), &dialog, SLOT(accept()));
        QObject::connect(buttons, SIGNAL(rejected()), &dialog, SLOT(reject()));

        if (dialog.exec() != QDialog::Accepted)
            return false;

        for (QList<AcquisitionComponentPage *>::const_iterator p = pages.constBegin(),
                endP = pages.constEnd(); p != endP; ++p) {
            (*p)->commit(v);
        }
        return true;
    }
};
}

QWidget *AcquisitionComponentTableDelegatePages::createEditor(QWidget *parent,
                                                              const QStyleOptionViewItem &option,
                                                              const QModelIndex &index) const
{
    Q_UNUSED(option);

    ComponentTableDelegatePagesEditor *button = new ComponentTableDelegatePagesEditor(this, parent);
    QVariant value(index.data(Qt::EditRole));
    button->attach(index, value);
    button->setText(buttonText(value.value<Variant::Write>()));

    return button;
}

void AcquisitionComponentTableDelegatePages::setEditorData(QWidget *editor,
                                                           const QModelIndex &index) const
{
    ComponentTableDelegatePagesEditor
            *button = static_cast<ComponentTableDelegatePagesEditor *>(editor);
    QVariant value(index.data(Qt::EditRole));
    button->attach(index, value);
    button->setText(buttonText(value.value<Variant::Write>()));
}

void AcquisitionComponentTableDelegatePages::setModelData(QWidget *editor,
                                                          QAbstractItemModel *model,
                                                          const QModelIndex &index) const
{
    Q_UNUSED(editor);
    Q_UNUSED(model);
    Q_UNUSED(index);
}

QString AcquisitionComponentTableDelegatePages::buttonText(const Variant::Read &value) const
{ return tr("Value"); }

}


enum {
    SpancheckFlags_None = 0x00,
    SpancheckFlags_Chopper = 0x01,
    SpancheckFlags_NoCounts = 0x02,
    SpancheckFlags_Ecotech = 0x04,
    SpancheckFlags_Default = SpancheckFlags_None,
};

static AcquisitionComponentPage *createSpancheckPage(const Variant::Read &value,
                                                     quint32 flags = SpancheckFlags_Default,
                                                     QWidget *parent = 0)
{
    AcquisitionComponentTabCompositePage *composite =
            new AcquisitionComponentTabCompositePage(AcquisitionComponentPage::tr("Spancheck"),
                                                     value, parent);
    {
        AcquisitionComponentPageGeneric *page;
        Variant::Write meta = Variant::Write::empty();

        page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Air Phase"), value,
                                                   composite);
        composite->addPage(page);

        meta.setEmpty();
        meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("10 minutes");
        page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Minimum sampling time:"),
                                       "Spancheck/Air/MinimumSample", meta);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This set the minimum amount of time air is sampled for."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Minimum air sampling time"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the minimum amount of time that air is sampled for, before even considering stability."));

        meta.setEmpty();
        meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("Unlimited");
        page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Maximum sampling time:"),
                                       "Spancheck/Air/MaximumSample", meta);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This set the maximum amount of time air is sampled for."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Maximum air sampling time"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the maximum amount of time that air is sampled for, before the stability indicator is ignored and the phase is ended anyway."));

        meta.setEmpty();
        if (flags & SpancheckFlags_Ecotech)
            meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("8 minutes");
        else
            meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("2 minutes");
        page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Flush duration:"),
                                       "Spancheck/Air/Flush", meta);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The length of time that air is flushed through the instrument before sampling."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Air flush time"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the length of time that air is flushed through the instrument before any smoothing or sampling.  This is also used at the start of the spancheck before the gas phase begins."));

        page->beginBox(AcquisitionComponentPage::tr("Activation"));

        page->addGlobalCommands(AcquisitionComponentPage::tr("Global commands:"),
                                "Spancheck/Air/Activate/Commands");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "Command data sent to all instruments on air phase entry."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Air phase entry commands"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the command data sent to all instruments when the system is switched to an air phase (flush or sample) during the spancheck."));

        page->addInstrumentCommands(AcquisitionComponentPage::tr("Instrument commands:"),
                                    "Spancheck/Air/Activate/InstrumentCommands");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "Command data sent to specific instruments on air phase entry."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Air phase entry commands"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the command data sent to individual instruments (specified by name) when the system is switched to an air phase (flush or sample) during the spancheck."));

        page->endBox();

        page->beginBox(AcquisitionComponentPage::tr("Deactivation"));

        page->addGlobalCommands(AcquisitionComponentPage::tr("Global commands:"),
                                "Spancheck/Air/Deactivate/Commands");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "Command data sent to all instruments on air phase exit."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Air phase exit commands"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the command data sent to all instruments when the system is leaves an air phase (flush or sample) during the spancheck."));

        page->addInstrumentCommands(AcquisitionComponentPage::tr("Instrument commands:"),
                                    "Spancheck/Air/Deactivate/InstrumentCommands");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "Command data sent to specific instruments on air phase exit."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Air phase exit commands"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the command data sent to individual instruments (specified by name) when the system is leaves an air phase (flush or sample) during the spancheck."));

        page->endBox();

        page->addStretch();
    }

    {
        AcquisitionComponentPageGeneric *page;
        Variant::Write meta = Variant::Write::empty();

        page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Gas Phase"), value,
                                                   composite);
        composite->addPage(page);

        meta.setEmpty();
        if (flags & SpancheckFlags_Ecotech)
            meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("10 minutes");
        else
            meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("5 minutes");
        page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Minimum sampling time:"),
                                       "Spancheck/Gas/MinimumSample", meta);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This set the minimum amount of time the gas is sampled for."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Minimum gas sampling time"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the minimum amount of time that the gas is sampled for, before even considering stability."));

        meta.setEmpty();
        meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("Unlimited");
        page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Maximum sampling time:"),
                                       "Spancheck/Gas/MaximumSample", meta);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This set the maximum amount of time the gas is sampled for."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Maximum gas sampling time"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the maximum amount of time that the gas is sampled for, before the stability indicator is ignored and the phase is ended anyway."));

        meta.setEmpty();
        if (flags & SpancheckFlags_Ecotech)
            meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("8 minutes");
        else
            meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("10 minutes");
        page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Flush duration:"),
                                       "Spancheck/Gas/Flush", meta);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The length of time that the gas is flushed through the instrument before sampling."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Gas flush time"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the length of time that the gas is flushed through the instrument before any smoothing or sampling.  This is also used at the start of the spancheck before the gas phase begins."));

        page->addEnum(AcquisitionComponentPage::tr("Gas type:"), "Spancheck/Gas/Type");
        page->setLastToolTip(AcquisitionComponentPage::tr("The type of span gas in use."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Gas type"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the type of span gas being used.  This is used to determine the expected difference from air Rayleigh scattering."));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("CO₂"), "CO2",
                                  AcquisitionComponentPage::tr(
                                          "Carbon dioxide, 2.61 times air Rayleigh scattering."));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("FM-200"), "FM200",
                                  AcquisitionComponentPage::tr(
                                          "Apaflurane (C₃HF₇), 15.3 times air Rayleigh scattering."));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("SF₆"), "SF6",
                                  AcquisitionComponentPage::tr(
                                          "Sulfur hexafluoride, 6.74 times air Rayleigh scattering."));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("Freon-12"), "R12",
                                  AcquisitionComponentPage::tr(
                                          "Freon-12 (CCl₂F₂), 15.31 times air Rayleigh scattering."));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("R-22"), "R22",
                                  AcquisitionComponentPage::tr(
                                          "Chlorodifluoromethane (CHClF₂), 7.53 times air Rayleigh scattering."));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("R-134"), "R134",
                                  AcquisitionComponentPage::tr(
                                          "Norflurane (CH₂FCF₃), 7.35 times air Rayleigh scattering."));

        page->beginBox(AcquisitionComponentPage::tr("Activation"));

        page->addGlobalCommands(AcquisitionComponentPage::tr("Global commands:"),
                                "Spancheck/Gas/Activate/Commands");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "Command data sent to all instruments on gas phase entry."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Gas phase entry commands"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the command data sent to all instruments when the system is switched to the gas phase (flush or sample) during the spancheck."));

        page->addInstrumentCommands(AcquisitionComponentPage::tr("Instrument commands:"),
                                    "Spancheck/Gas/Activate/InstrumentCommands");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "Command data sent to specific instruments on gas phase entry."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Gas phase entry commands"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the command data sent to individual instruments (specified by name) when the system is switched to an gas phase (flush or sample) during the spancheck."));

        page->endBox();

        page->beginBox(AcquisitionComponentPage::tr("Deactivation"));

        page->addGlobalCommands(AcquisitionComponentPage::tr("Global commands:"),
                                "Spancheck/Gas/Deactivate/Commands");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "Command data sent to all instruments on gas phase exit."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Gas phase exit commands"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the command data sent to all instruments when the system is leaves the gas phase (flush or sample) during the spancheck."));

        page->addInstrumentCommands(AcquisitionComponentPage::tr("Instrument commands:"),
                                    "Spancheck/Gas/Deactivate/InstrumentCommands");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "Command data sent to specific instruments on gas phase exit."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Gas phase exit commands"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the command data sent to individual instruments (specified by name) when the system is leaves the gas phase (flush or sample) during the spancheck."));

        page->endBox();

        page->addStretch();
    }

    {
        AcquisitionComponentPageGeneric *page;
        Variant::Write meta = Variant::Write::empty();

        page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Other"), value,
                                                   composite);
        composite->addPage(page);

        page->addDoubleSpinBox(AcquisitionComponentPage::tr("Terminal angle (degrees):"),
                               "Spancheck/TerminalAngle");
        page->setLastDoubleSpinBox(0, 0, 360, 1);
        page->setLastUnassigned(180.0);
        page->setLastToolTip(
                AcquisitionComponentPage::tr("The terminal angle of the measurement hemisphere."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Terminal angle"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the termination angle of the measurement hemisphere.  Rayleigh scattering are integrated from the start angle (e.x. 0 for forward scattering) to this angle."));

        page->beginBox(AcquisitionComponentPage::tr("Measurement Smoothing"));

        meta.setEmpty();
        meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("Indefinite");
        meta["*hEditor/EnableStability"] = true;
        meta["*hEditor/DefaultSmoother/Type"] = "Forever";
        page->addSmoother(AcquisitionComponentPage::tr("Scattering:"),
                          "Spancheck/Smoothing/Scattering", meta);
        page->setLastToolTip(AcquisitionComponentPage::tr("Scattering smoothing parameters."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Scattering smoothing"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the smoothing and stability checking for scattering measurements."));

        page->addSmoother(AcquisitionComponentPage::tr("Temperature:"),
                          "Spancheck/Smoothing/Temperature", meta);
        page->setLastToolTip(AcquisitionComponentPage::tr("Temperature smoothing parameters."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Temperature smoothing"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the smoothing and stability checking for temperature measurements."));

        page->addSmoother(AcquisitionComponentPage::tr("Pressure:"), "Spancheck/Smoothing/Pressure",
                          meta);
        page->setLastToolTip(AcquisitionComponentPage::tr("Pressure smoothing parameters."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Temperature smoothing"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the smoothing and stability checking for pressure measurements."));

        if (!(flags & SpancheckFlags_NoCounts)) {
            page->addSmoother(AcquisitionComponentPage::tr("Measurement count rate:"),
                              "Spancheck/Smoothing/Measurement", meta);
            page->setLastToolTip(
                    AcquisitionComponentPage::tr("Measurement count rate smoothing parameters."));
            page->setLastStatusTip(AcquisitionComponentPage::tr("Measurement counts smoothing"));
            page->setLastWhatsThis(AcquisitionComponentPage::tr(
                    "This sets the smoothing and stability checking for the measurement count rate."));

            page->addSmoother(AcquisitionComponentPage::tr("Reference count rate:"),
                              "Spancheck/Smoothing/Reference", meta);
            page->setLastToolTip(
                    AcquisitionComponentPage::tr("Reference count rate smoothing parameters."));
            page->setLastStatusTip(AcquisitionComponentPage::tr("Reference counts smoothing"));
            page->setLastWhatsThis(AcquisitionComponentPage::tr(
                    "This sets the smoothing and stability checking for the reference count rate."));

            page->addSmoother(AcquisitionComponentPage::tr("Dark count rate:"),
                              "Spancheck/Smoothing/Dark", meta);
            page->setLastToolTip(
                    AcquisitionComponentPage::tr("Dark count rate smoothing parameters."));
            page->setLastStatusTip(AcquisitionComponentPage::tr("Dark counts smoothing"));
            page->setLastWhatsThis(AcquisitionComponentPage::tr(
                    "This sets the smoothing and stability checking for the dark count rate."));
        }

        if (flags & SpancheckFlags_Chopper) {
            page->addSmoother(AcquisitionComponentPage::tr("Chopper revolution rate:"),
                              "Spancheck/Smoothing/Revolutions", meta);
            page->setLastToolTip(
                    AcquisitionComponentPage::tr("Chopper revolution rate smoothing parameters."));
            page->setLastStatusTip(AcquisitionComponentPage::tr("Chopper revolutions smoothing"));
            page->setLastWhatsThis(AcquisitionComponentPage::tr(
                    "This sets the smoothing and stability checking for the chopper revolution rate measurement."));
        }

        page->endBox();

        page->addStretch();
    }
    return composite;
}


static QList<AcquisitionComponentPage *> create_acquire_2b_ozone205(const Variant::Read &value,
                                                                    QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Report interval (s):"), "ReportInterval");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(2.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The expected reporting interval in seconds."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Report interval"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the expected reporting interval.  When possible, the instrument will be configured to attempt to match this setting."));

    page->addBoolean(AcquisitionComponentPage::tr("Enable strict parsing"), "StrictMode");
    page->setLastUnassigned(true);
    page->setLastToolTip(AcquisitionComponentPage::tr("Enable strict mode parsing."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Strict parsing"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This enables additional checks during data parsing."));

    page->addStretch();

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_ad_cpcmagic200(const Variant::Read &value,
                                                                       QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Sample flow rate (lpm):"), "Flow");
    page->setLastDoubleSpinBox(3, 0.001, 99.999, 0.1);
    page->setLastUnassigned(AcquisitionComponentPage::tr("Instrument reported"));
    page->setLastExplicitUndefined();
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The sample flow rate used to convert the counts to a concentration."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Sample flow rate"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the sample flow rate in lpm that is used to convert the raw count rate into a number concentration."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Report interval (s):"), "ReportInterval");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(2.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The expected reporting interval in seconds."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Report interval"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the expected reporting interval.  When possible, the instrument will be configured to attempt to match this setting."));

    page->addBoolean(AcquisitionComponentPage::tr("Recalculate concentration"),
                     "CalculateConcentration");
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "Enable recalculation of the concentration from the count rate."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Concentration recalculation"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This enables recalculation of the concentration instead of simply using the reported one."));

    page->addBoolean(AcquisitionComponentPage::tr("Enable strict parsing"), "StrictMode");
    page->setLastUnassigned(true);
    page->setLastToolTip(AcquisitionComponentPage::tr("Enable strict mode parsing."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Strict parsing"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This enables additional checks during data parsing."));

    page->addStretch();

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_aerodyne_caps(const Variant::Read &value,
                                                                      QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->addBoolean(AcquisitionComponentPage::tr("Enable instrument controlled zeroing"),
                     "Autozero");
    page->setLastUnassigned(true);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("Enable instrument controlled automatic zeroing."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Instrument autozero"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This enables the instrument controlled automatic zero functionality.  When disabled, zeros are only performed when requested by the acquisition system."));

    page->addIntegerSpinBox(AcquisitionComponentPage::tr("Zero duration (s):"), "ZeroDuration");
    page->setLastIntegerSpinBox(1, 86400);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "This sets the duration of the zero the instrument is configured with."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Zero duration"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the length of time zero air is measured during an instrument zero.  When not set, the instrument setting is not altered."));

    page->addIntegerSpinBox(AcquisitionComponentPage::tr("Flush duration (s):"), "FlushDuration");
    page->setLastIntegerSpinBox(1, 86400);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "This sets the duration of time air is flushed before and after a zero."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Zero duration"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the length of time that air is flushed before and after a zero.  When not set, the instrument setting is not altered."));

    page->addDouble(AcquisitionComponentPage::tr("Measurement wavelength (nm):"),
                    "Autodetect/Start/Intensity");
    page->setLastUnassigned(AcquisitionComponentPage::tr("Automatically determined"));
    page->setLastToolTip(
            AcquisitionComponentPage::tr("This overrides the reported measurement wavelength."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Measurement wavelength"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "Then set, this causes the acquisition system to disregard the reported wavelength and use this value instead."));

    /* Basetime not set */

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Report interval (s):"), "ReportInterval");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(1.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The expected reporting interval in seconds."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Report interval"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This sets the expected reporting interval."));

    page->addBoolean(AcquisitionComponentPage::tr("Enable strict parsing"), "StrictMode");
    page->setLastUnassigned(true);
    page->setLastToolTip(AcquisitionComponentPage::tr("Enable strict mode parsing."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Strict parsing"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This enables additional checks during data parsing."));

    page->addStretch();

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_azonix_umac1050(const Variant::Read &value,
                                                                        QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;

    {
        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Digital"), value,
                                                     "DigitalOutputs", parent);
        result.append(table);

        table->addIntegerSpinBox(AcquisitionComponentPage::tr("Channel"), "DigitalOutputs/%1");
        table->setLastDefaults(0, Variant::Root(0));
        table->setLastIntegerSpinBox(0, 71, NumberFormat(2, 0), false);
        table->setLastToolTip(AcquisitionComponentPage::tr("The channel number to control."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Digital output channel"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the integer number of the digital output channel that is being controlled."));

        table->addBoolean(AcquisitionComponentPage::tr("Initialize"), "Initialize/Digital/%1");
        table->addCommitRemovePath("Initialize/Digital");
        table->setLastBoolean(AcquisitionComponentPage::tr("On"),
                              AcquisitionComponentPage::tr("Off"));
        table->setLastDefaults(AcquisitionComponentPage::tr("Unchanged"));
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The output value set on system initialization."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Initial digital output value"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the initial value of the channel set when the acquisition system starts up."));

        table->addBoolean(AcquisitionComponentPage::tr("Shutdown"), "Exit/Digital/%1");
        table->addCommitRemovePath("Exit/Digital");
        table->setLastBoolean(AcquisitionComponentPage::tr("On"),
                              AcquisitionComponentPage::tr("Off"));
        table->setLastDefaults(AcquisitionComponentPage::tr("Unchanged"));
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The output value set on system shutdown."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Shutdown digital output value"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the value that the channel is set to immediately before system shutdown."));

        table->addBoolean(AcquisitionComponentPage::tr("Bypass"), "Bypass/Digital/%1");
        table->addCommitRemovePath("Bypass/Digital");
        table->setLastBoolean(AcquisitionComponentPage::tr("On"),
                              AcquisitionComponentPage::tr("Off"));
        table->setLastDefaults(AcquisitionComponentPage::tr("Unchanged"));
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The output value set on system bypass."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Bypass digital output value"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the value that the channel is set to when the instrument enters a bypass state."));

        table->addBoolean(AcquisitionComponentPage::tr("Un-bypass"), "UnBypass/Digital/%1");
        table->addCommitRemovePath("UnBypass/Digital");
        table->setLastBoolean(AcquisitionComponentPage::tr("On"),
                              AcquisitionComponentPage::tr("Off"));
        table->setLastDefaults(AcquisitionComponentPage::tr("Unchanged"));
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The output value set on system bypass release."));
        table->setLastStatusTip(
                AcquisitionComponentPage::tr("Bypass release digital output value"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the value that the channel is set to when the instrument leaves a bypass state."));

        table->addCommitRemovePath("Initialize", true);
        table->addCommitRemovePath("Exit", true);
        table->addCommitRemovePath("Bypass", true);
        table->addCommitRemovePath("UnBypass", true);
    }

    {
        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Analog Input"),
                                                     value, "Variables", parent);
        result.append(table);

        table->addString(AcquisitionComponentPage::tr("Variable"), "Variables/%1/Name");
        table->setLastDefaults(QChar(QChar::ObjectReplacementCharacter));
        table->setLastString(true);
        table->setLastToolTip(AcquisitionComponentPage::tr("The output variable name."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Variable name"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the output variable name that the data are logged as."));

        table->addIntegerSpinBox(AcquisitionComponentPage::tr("Channel"), "Variables/%1/Channel");
        table->setLastDefaults(0, Variant::Root(0));
        table->setLastIntegerSpinBox(0, 61, NumberFormat(2, 0), false);
        table->setLastToolTip(AcquisitionComponentPage::tr("The channel number to read from."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Analog input channel"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the integer number of the analog input channel to read from."));

        table->addCalibration(AcquisitionComponentPage::tr("Calibration"),
                              "Variables/%1/Calibration");
        table->setLastToolTip(AcquisitionComponentPage::tr(
                "The calibration applied to the input value (from voltage to physical units)."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Calibration"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the calibration applied to the input voltage to convert it to physical units."));

        table->addMetadata(AcquisitionComponentPage::tr("Metadata"), "Variables/%1/Metadata");
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The metadata base for the analog input."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Variable metadata"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "Use this to set the base metadata generated along with the analog input value."));
    }

    {
        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Analog Output"),
                                                     value, "AnalogOutputs", parent);
        result.append(table);

        table->addIntegerSpinBox(AcquisitionComponentPage::tr("Channel"), "AnalogOutputs/%1");
        table->setLastDefaults(0, Variant::Root(0));
        table->setLastIntegerSpinBox(0, 9, NumberFormat(1, 0), false);
        table->setLastToolTip(AcquisitionComponentPage::tr("The channel number to control."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Analog output channel"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the integer number of the analog output channel that is being controlled."));

        table->addDoubleSpinBox(AcquisitionComponentPage::tr("Initialize"), "Initialize/Analog/%1");
        table->addCommitRemovePath("Initialize/Analog");
        table->setLastDefaults(AcquisitionComponentPage::tr("Unchanged"));
        table->setLastDoubleSpinBox(3, -10, 10, 0.1);
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The output value set on system initialization."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Initial analog output value"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the initial value of the channel set when the acquisition system starts up."));

        table->addDoubleSpinBox(AcquisitionComponentPage::tr("Shutdown"), "Exit/Analog/%1");
        table->addCommitRemovePath("Exit/Analog");
        table->setLastDefaults(AcquisitionComponentPage::tr("Unchanged"));
        table->setLastDoubleSpinBox(3, -10, 10, 0.1);
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The output value set on system shutdown."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Shutdown analog output value"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the value that the channel is set to immediately before system shutdown."));

        table->addDoubleSpinBox(AcquisitionComponentPage::tr("Bypass"), "Bypass/Analog/%1");
        table->addCommitRemovePath("Bypass/Analog");
        table->setLastDefaults(AcquisitionComponentPage::tr("Unchanged"));
        table->setLastDoubleSpinBox(3, -10, 10, 0.1);
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The output value set on system bypass."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Bypass analog output value"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the value that the channel is set to when the instrument enters a bypass state."));

        table->addDoubleSpinBox(AcquisitionComponentPage::tr("Un-bypass"), "UnBypass/Analog/%1");
        table->addCommitRemovePath("UnBypass/Analog");
        table->setLastDefaults(AcquisitionComponentPage::tr("Unchanged"));
        table->setLastDoubleSpinBox(3, -10, 10, 0.1);
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The output value set on system bypass release."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Bypass release analog output value"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the value that the channel is set to when the instrument leaves a bypass state."));

        table->addCommitRemovePath("Initialize", true);
        table->addCommitRemovePath("Exit", true);
        table->addCommitRemovePath("Bypass", true);
        table->addCommitRemovePath("UnBypass", true);
    }

    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                                    parent);
        result.append(page);

        page->addIntegerSpinBox(AcquisitionComponentPage::tr("Address:"), "Address");
        page->setLastIntegerSpinBox(0, 63);
        page->setLastToolTip(
                AcquisitionComponentPage::tr("This is the internal address of the uMAC."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Instrument address"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the address code that the acquisition system attempts to communicate with.  This must match the setting on the uMAC hardware."));

        page->addDoubleSpinBox(AcquisitionComponentPage::tr("Poll interval (s):"), "PollInterval");
        page->setLastDoubleSpinBox(1, 0.1, 9999, 0.1);
        page->setLastUnassigned(1.0);
        page->setLastToolTip(AcquisitionComponentPage::tr("The polling interval in seconds."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Polling interval"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the delay between instrument data polling.  This determines how frequently the data values are updated."));

        page->addBoolean(AcquisitionComponentPage::tr("Log output channels"), "LogOutputs");
        page->setLastToolTip(
                AcquisitionComponentPage::tr("Enable logging of the output only channels."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Log outputs"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This enables logging of the outputs, rather than just the inputs.  When enabled, variables are generated that reflect the current output state of the uMAC."));

        page->addIntegerSpinBox(AcquisitionComponentPage::tr("Analog input index offset:"),
                                "AnalogChannelStart");
        page->setLastIntegerSpinBox(0, 61);
        page->setLastToolTip(
                AcquisitionComponentPage::tr("This is the offset of the analog input channels."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Analog input offset"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the index of the first analog channel that is treated as \"zero\" in channel assignments."));

        page->addIntegerSpinBox(AcquisitionComponentPage::tr("Analog input count:"),
                                "AnalogChannelCount");
        page->setLastIntegerSpinBox(1, 62);
        page->setLastUnassigned((qint64) 24);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This is the total number of available analog inputs."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Analog input count"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the total number of analog input channels read after the starting index."));

        page->addStretch();
    }

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_bmi_cpc17x0(const Variant::Read &value,
                                                                    QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Poll interval (s):"), "PollInterval");
    page->setLastDoubleSpinBox(1, 0.1, 9999, 0.1);
    page->setLastUnassigned(0.5);
    page->setLastToolTip(AcquisitionComponentPage::tr("The polling interval in seconds."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Polling interval"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the delay between instrument data polling.  This determines how frequently the data values are updated."));

    page->addBoolean(AcquisitionComponentPage::tr("Recalculate concentration"),
                     "CalculateConcentration");
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "Enable recalculation of the concentration from the count rate and flow."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Concentration recalculation"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This enables recalculation of the concentration instead of simply using the reported one."));

    page->addStretch();

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_brooks_pid0254(const Variant::Read &value,
                                                                       QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;

    {
        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Input"), value,
                                                     "Variables", parent);
        result.append(table);

        table->addString(AcquisitionComponentPage::tr("Variable"), "Variables/%1/Name");
        table->setLastDefaults(QChar(QChar::ObjectReplacementCharacter));
        table->setLastString(true);
        table->setLastToolTip(AcquisitionComponentPage::tr("The output variable name."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Variable name"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the output variable name that the data are logged as."));

        table->addIntegerSpinBox(AcquisitionComponentPage::tr("Channel"), "Variables/%1/Channel");
        table->setLastDefaults(1, Variant::Root(1));
        table->setLastIntegerSpinBox(1, 4, NumberFormat(1, 0), false);
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The controller channel number to read from."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Controller channel number"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the integer number of the controller channel to read from."));

        table->addCalibration(AcquisitionComponentPage::tr("Calibration"),
                              "Variables/%1/Calibration");
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The calibration applied to the input value."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Calibration"));
        table->setLastWhatsThis(
                AcquisitionComponentPage::tr("This is the calibration applied to the input."));

        table->addMetadata(AcquisitionComponentPage::tr("Metadata"), "Variables/%1/Metadata");
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The metadata base for the analog input."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Variable metadata"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "Use this to set the base metadata generated along with the input value."));
    }

    {
        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Output"), value,
                                                     "SetpointOutputs", parent);
        result.append(table);

        table->addIntegerSpinBox(AcquisitionComponentPage::tr("Channel"), "SetpointOutputs/%1");
        table->setLastDefaults(1, Variant::Root(1));
        table->setLastIntegerSpinBox(1, 4, NumberFormat(1, 0), false);
        table->setLastToolTip(AcquisitionComponentPage::tr("The channel number to control."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Setpoint output channel"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the integer number of the setpoint channel that is being controlled."));

        table->addDoubleSpinBox(AcquisitionComponentPage::tr("Initialize"),
                                "Initialize/Setpoint/%1");
        table->addCommitRemovePath("Initialize/Setpoint");
        table->setLastDefaults(AcquisitionComponentPage::tr("Unchanged"));
        table->setLastDoubleSpinBox(1, -100, 100, 1);
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The output value set on system initialization."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Initial setpoint output value"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the initial value of the channel set when the acquisition system starts up."));

        table->addDoubleSpinBox(AcquisitionComponentPage::tr("Shutdown"), "Exit/Setpoint/%1");
        table->addCommitRemovePath("Exit/Setpoint");
        table->setLastDefaults(AcquisitionComponentPage::tr("Unchanged"));
        table->setLastDoubleSpinBox(1, -100, 100, 1);
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The output value set on system shutdown."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Shutdown setpoint output value"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the value that the channel is set to immediately before system shutdown."));

        table->addDoubleSpinBox(AcquisitionComponentPage::tr("Bypass"), "Bypass/Setpoint/%1");
        table->addCommitRemovePath("Bypass/Analog");
        table->setLastDefaults(AcquisitionComponentPage::tr("Unchanged"));
        table->setLastDoubleSpinBox(1, -100, 100, 1);
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The output value set on system bypass."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Bypass setpoint output value"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the value that the channel is set to when the instrument enters a bypass state."));

        table->addDoubleSpinBox(AcquisitionComponentPage::tr("Un-bypass"), "UnBypass/Setpoint/%1");
        table->addCommitRemovePath("UnBypass/Analog");
        table->setLastDefaults(AcquisitionComponentPage::tr("Unchanged"));
        table->setLastDoubleSpinBox(1, -100, 100, 1);
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The output value set on system bypass release."));
        table->setLastStatusTip(
                AcquisitionComponentPage::tr("Bypass release setpoint output value"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the value that the channel is set to when the instrument leaves a bypass state."));

        table->addCommitRemovePath("Initialize", true);
        table->addCommitRemovePath("Exit", true);
        table->addCommitRemovePath("Bypass", true);
        table->addCommitRemovePath("UnBypass", true);
    }

    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                                    parent);
        result.append(page);

        page->addIntegerSpinBox(AcquisitionComponentPage::tr("Address:"), "Address");
        page->setLastIntegerSpinBox(-1, 65535);
        page->setLastUnassigned((qint64) -1);
        page->setLastToolTip(
                AcquisitionComponentPage::tr("This is the internal address of the controller."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Instrument address"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the address code that the acquisition system attempts to communicate with.  If set to -1 then the address is automatically determined."));

        page->addDoubleSpinBox(AcquisitionComponentPage::tr("Poll interval (s):"), "PollInterval");
        page->setLastDoubleSpinBox(1, 0.1, 9999, 0.5);
        page->setLastUnassigned(1.0);
        page->setLastToolTip(AcquisitionComponentPage::tr("The polling interval in seconds."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Polling interval"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the delay between instrument data polling.  This determines how frequently the data values are updated."));

        page->addBoolean(AcquisitionComponentPage::tr("Log output channels"), "LogOutputs");
        page->setLastToolTip(
                AcquisitionComponentPage::tr("Enable logging of the output only channels."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Log outputs"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This enables logging of the outputs, rather than just the inputs.  When enabled, variables are generated that reflect the current output state of the uMAC."));

        page->addBoolean(AcquisitionComponentPage::tr("Enable strict parsing"), "StrictMode");
        page->setLastUnassigned(true);
        page->setLastToolTip(AcquisitionComponentPage::tr("Enable strict mode parsing."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Strict parsing"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This enables additional checks during data parsing."));

        page->addStretch();
    }

    return result;
}

static QList<
        AcquisitionComponentPage *> create_acquire_campbell_cr1000gmd(const Variant::Read &value,
                                                                      QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;

    {
        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Digital"), value,
                                                     "DigitalOutputs", parent);
        result.append(table);

        table->addIntegerSpinBox(AcquisitionComponentPage::tr("Channel"), "DigitalOutputs/%1");
        table->setLastDefaults(0, Variant::Root(0));
        table->setLastIntegerSpinBox(0, 20, NumberFormat(2, 0), false);
        table->setLastToolTip(AcquisitionComponentPage::tr("The channel number to control."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Digital output channel"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the integer number of the digital output channel that is being controlled."));

        table->addBoolean(AcquisitionComponentPage::tr("Initialize"), "Initialize/Digital/%1");
        table->addCommitRemovePath("Initialize/Digital");
        table->setLastBoolean(AcquisitionComponentPage::tr("On"),
                              AcquisitionComponentPage::tr("Off"));
        table->setLastDefaults(AcquisitionComponentPage::tr("Unchanged"));
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The output value set on system initialization."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Initial digital output value"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the initial value of the channel set when the acquisition system starts up."));

        table->addBoolean(AcquisitionComponentPage::tr("Shutdown"), "Exit/Digital/%1");
        table->addCommitRemovePath("Exit/Digital");
        table->setLastBoolean(AcquisitionComponentPage::tr("On"),
                              AcquisitionComponentPage::tr("Off"));
        table->setLastDefaults(AcquisitionComponentPage::tr("Unchanged"));
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The output value set on system shutdown."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Shutdown digital output value"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the value that the channel is set to immediately before system shutdown."));

        table->addBoolean(AcquisitionComponentPage::tr("Bypass"), "Bypass/Digital/%1");
        table->addCommitRemovePath("Bypass/Digital");
        table->setLastBoolean(AcquisitionComponentPage::tr("On"),
                              AcquisitionComponentPage::tr("Off"));
        table->setLastDefaults(AcquisitionComponentPage::tr("Unchanged"));
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The output value set on system bypass."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Bypass digital output value"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the value that the channel is set to when the instrument enters a bypass state."));

        table->addBoolean(AcquisitionComponentPage::tr("Un-bypass"), "UnBypass/Digital/%1");
        table->addCommitRemovePath("UnBypass/Digital");
        table->setLastBoolean(AcquisitionComponentPage::tr("On"),
                              AcquisitionComponentPage::tr("Off"));
        table->setLastDefaults(AcquisitionComponentPage::tr("Unchanged"));
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The output value set on system bypass release."));
        table->setLastStatusTip(
                AcquisitionComponentPage::tr("Bypass release digital output value"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the value that the channel is set to when the instrument leaves a bypass state."));

        table->addCommitRemovePath("Initialize", true);
        table->addCommitRemovePath("Exit", true);
        table->addCommitRemovePath("Bypass", true);
        table->addCommitRemovePath("UnBypass", true);
    }

    {
        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Analog Input"),
                                                     value, "Variables", parent);
        result.append(table);

        table->addString(AcquisitionComponentPage::tr("Variable"), "Variables/%1/Name");
        table->setLastDefaults(QChar(QChar::ObjectReplacementCharacter));
        table->setLastString(true);
        table->setLastToolTip(AcquisitionComponentPage::tr("The output variable name."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Variable name"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the output variable name that the data are logged as."));

        table->addIntegerSpinBox(AcquisitionComponentPage::tr("Channel"), "Variables/%1/Channel");
        table->setLastDefaults(1, Variant::Root(1));
        table->setLastIntegerSpinBox(1, 32, NumberFormat(2, 0), false);
        table->setLastToolTip(AcquisitionComponentPage::tr("The channel number to read from."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Analog input channel"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the integer number of the analog input channel to read from."));

        table->addCalibration(AcquisitionComponentPage::tr("Calibration"),
                              "Variables/%1/Calibration");
        table->setLastToolTip(AcquisitionComponentPage::tr(
                "The calibration applied to the input value (from voltage to physical units)."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Calibration"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the calibration applied to the input voltage to convert it to physical units."));

        table->addMetadata(AcquisitionComponentPage::tr("Metadata"), "Variables/%1/Metadata");
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The metadata base for the analog input."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Variable metadata"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "Use this to set the base metadata generated along with the analog input value."));
    }

    /* Currently disabled because the present implementation does not do anything with it */
#if 0
    {
        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Analog Output"),
                                                      value, "AnalogOutputs", parent);
        result.append(table);

        table->addIntegerSpinBox(AcquisitionComponentPage::tr("Channel"), "AnalogOutputs/%1");
        table->setLastDefaults(0, Value((qint64) 0));
        table->setLastIntegerSpinBox(0, 7, NumberFormat(1, 0), false);
        table->setLastToolTip(AcquisitionComponentPage::tr("The channel number to control."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Analog output channel"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the integer number of the analog output channel that is being controlled."));

        table->addDoubleSpinBox(AcquisitionComponentPage::tr("Initialize"), "Initialize/Analog/%1");
        table->addCommitRemovePath("Initialize/Analog");
        table->setLastDefaults(AcquisitionComponentPage::tr("Unchanged"));
        table->setLastDoubleSpinBox(3, 0, 5, 0.1);
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The output value set on system initialization."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Initial analog output value"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the initial value of the channel set when the acquisition system starts up."));

        table->addDoubleSpinBox(AcquisitionComponentPage::tr("Shutdown"), "Exit/Analog/%1");
        table->addCommitRemovePath("Exit/Analog");
        table->setLastDefaults(AcquisitionComponentPage::tr("Unchanged"));
        table->setLastDoubleSpinBox(3, 0, 5, 0.1);
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The output value set on system shutdown."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Shutdown analog output value"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the value that the channel is set to immediately before system shutdown."));

        table->addDoubleSpinBox(AcquisitionComponentPage::tr("Bypass"), "Bypass/Analog/%1");
        table->addCommitRemovePath("Bypass/Analog");
        table->setLastDefaults(AcquisitionComponentPage::tr("Unchanged"));
        table->setLastDoubleSpinBox(3, 0, 5, 0.1);
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The output value set on system bypass."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Bypass analog output value"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the value that the channel is set to when the instrument enters a bypass state."));

        table->addDoubleSpinBox(AcquisitionComponentPage::tr("Un-bypass"), "UnBypass/Analog/%1");
        table->addCommitRemovePath("UnBypass/Analog");
        table->setLastDefaults(AcquisitionComponentPage::tr("Unchanged"));
        table->setLastDoubleSpinBox(3, 0, 5, 0.1);
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The output value set on system bypass release."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Bypass release analog output value"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the value that the channel is set to when the instrument leaves a bypass state."));

        table->addCommitRemovePath("Initialize", true);
        table->addCommitRemovePath("Exit", true);
        table->addCommitRemovePath("Bypass", true);
        table->addCommitRemovePath("UnBypass", true);
    }
#endif

    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                                    parent);
        result.append(page);

        page->addDoubleSpinBox(AcquisitionComponentPage::tr("Poll interval (s):"), "PollInterval");
        page->setLastDoubleSpinBox(1, 0.1, 9999, 0.1);
        page->setLastUnassigned(1.0);
        page->setLastToolTip(AcquisitionComponentPage::tr("The polling interval in seconds."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Polling interval"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the delay between instrument data polling.  This determines how frequently the data values are updated."));

        page->addBoolean(AcquisitionComponentPage::tr("Log output channels"), "LogOutputs");
        page->setLastToolTip(
                AcquisitionComponentPage::tr("Enable logging of the output only channels."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Log outputs"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This enables logging of the outputs, rather than just the inputs.  When enabled, variables are generated that reflect the current output state of the uMAC."));

        page->addBoolean(AcquisitionComponentPage::tr("Enable strict parsing"), "StrictMode");
        page->setLastUnassigned(true);
        page->setLastToolTip(AcquisitionComponentPage::tr("Enable strict mode parsing."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Strict parsing"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This enables additional checks during data parsing."));

        page->addStretch();
    }

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_csd_pops(const Variant::Read &value,
                                                                 QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;

    {
        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Bins"), value,
                                                     "Diameter", parent);
        result.append(table);
        table->setArrayMode();

        table->addDoubleSpinBox(AcquisitionComponentPage::tr("Central Diameter (\xCE\xBCm)"),
                                "Diameter/%1");
        table->setLastDoubleSpinBox(3, 0.001, 100, 0.1, false);
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The central diameter of the bin in \xCE\xBCm."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Bin diameter"));
        table->setLastWhatsThis(
                AcquisitionComponentPage::tr("The central bin diameter in \xCE\xBCm."));
    }

    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                                    parent);
        result.append(page);

        page->addBoolean(AcquisitionComponentPage::tr("Use measured time between records"),
                         "UseMeasuredTime");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "Use the time measured between records instead of assuming 1 Hz."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Use measured report time"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This enables causes the acquisition system to use the measured time between reports instead of assuming 1 Hz when calculating count rates."));

        page->addBoolean(AcquisitionComponentPage::tr("Enable strict parsing"), "StrictMode");
        page->setLastUnassigned(true);
        page->setLastToolTip(AcquisitionComponentPage::tr("Enable strict mode parsing."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Strict parsing"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This enables additional checks during data parsing."));

        page->addStretch();
    }

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_dmt_bcp(const Variant::Read &value,
                                                                QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;

    {
        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Bins"), value,
                                                     "Diameter", parent);
        result.append(table);
        table->setArrayMode();

        table->addDoubleSpinBox(AcquisitionComponentPage::tr("Central Diameter (\xCE\xBCm)"),
                                "Diameter/%1");
        table->setLastDoubleSpinBox(3, 0.001, 100, 0.1, false);
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The central diameter of the bin in \xCE\xBCm."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Bin diameter"));
        table->setLastWhatsThis(
                AcquisitionComponentPage::tr("The central bin diameter in \xCE\xBCm."));
    }

    {
        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Bins"), value,
                                                     "Hardware/Bins", parent);
        result.append(table);
        table->setArrayMode();

        table->addIntegerSpinBox(AcquisitionComponentPage::tr("ADC upper threshold"),
                                 "Diameter/%1");
        table->setLastIntegerSpinBox(0, 4095, NumberFormat(1, 0), false);
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The upper threshold for the bin in ADC counts."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Bin threshold"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "The upper threshold that defines the internal bin in ADC counts."));
    }

    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                                    parent);
        result.append(page);

        page->addDoubleSpinBox(AcquisitionComponentPage::tr("Sample flow rate (lpm):"), "Flow");
        page->setLastUnassigned(1.0);
        page->setLastExplicitUndefined(false);
        page->setLastDoubleSpinBox(3, 0.001, 99.999, 0.1);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The sample flow rate used to convert the counts to a concentration."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Sample flow rate"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the sample flow rate in lpm that is used to convert the raw count rate into a number concentration.  This should be the flow through the effective sample volume."));

        page->addIntegerSpinBox(AcquisitionComponentPage::tr("Count threshold:"),
                                "Hardware/ADCThreshold");
        page->setLastUnassigned(Q_INT64_C(1024));
        page->setLastIntegerSpinBox(1, 65520);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This sets minimum ADC threshold for particle detection."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Detection threshold"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the lower limit in ADC counts for particle detection.  Data below this threshold are considered background."));

        page->addBoolean(AcquisitionComponentPage::tr("Count from particle peak"),
                         "Hardware/CountFromPeak");
        page->setLastToolTip(AcquisitionComponentPage::tr("Count from the particle peak signal."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Alternate count mode"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This enables count from the particle peak instead of the particle width."));

        page->addDoubleSpinBox(AcquisitionComponentPage::tr("Poll interval (s):"), "PollInterval");
        page->setLastDoubleSpinBox(1, 0.1, 9999, 0.1);
        page->setLastUnassigned(0.5);
        page->setLastToolTip(AcquisitionComponentPage::tr("The polling interval in seconds."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Polling interval"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the delay between instrument data polling.  This determines how frequently the data values are updated."));

        page->addDoubleSpinBox(AcquisitionComponentPage::tr("Assumed accumulation time (s):"),
                               "ConstantTime");
        page->setLastDoubleSpinBox(1, 0.1, 9999, 0.1);
        page->setLastExplicitUndefined(true);
        page->setLastToolTip(
                AcquisitionComponentPage::tr("The assumed accumulation time for counts."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Assumed accumulation time"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "When set, this causes the acquisition system to use the specified time for converting count rates, instead of using the measured time."));

        page->addStretch();
    }

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_dmt_ccn(const Variant::Read &value,
                                                                QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;
    Variant::Write meta = Variant::Write::empty();


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->beginBox(AcquisitionComponentPage::tr("Model"));

    page->addBoolean(AcquisitionComponentPage::tr("Enable supersaturation calculation"),
                     "Model/Enable");
    page->setLastUnassigned(true);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "Enable calculation of column supersaturation based on the external model."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Enable model"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This enables the use of the external model program to calculate the internal supersaturation of the column."));

    page->addIntegerSpinBox(AcquisitionComponentPage::tr("Maximum backlog:"),
                            "Model/MaximumBacklog");
    page->setLastIntegerSpinBox(-1, 9999);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "This sets maximum number of outstanding model runs before new ones are dropped."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Maximum model backlog"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the limit on the number of uncompleted model runs before new ones are denied.  This is used to prevent unlimited accumulation of pending model runs."));

    meta.setEmpty();
    meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("1 minute aligned");
    page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Interval:"), "Model/Interval",
                                   meta);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("This sets the interval between model runs."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Model interval"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the interval of data that are incorporated into a single model run.  It normally makes sense to set this equal to the averaging time."));

    /* Model program and parameters not set */

    page->endBox();

    page->beginBox(AcquisitionComponentPage::tr("Processing"));

    page->addBoolean(AcquisitionComponentPage::tr("Exclude reported unstable data"),
                     "Processing/UseReportedStability");
    page->setLastUnassigned(true);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "Enable the use of the instrument reported stability flag to exclude data."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Exclude reported unstable data"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This causes the reported counts to not include any data that the instrument reports as being unstable."));

    page->addBoolean(AcquisitionComponentPage::tr("Exclude calculated unstable data"),
                     "Processing/UseCalculatedStability");
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "Enable the use of the acquisition system calculated stability to exclude data."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Exclude calculated unstable data"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This causes the reported counts to not include any data when the calculated smoothed temperature difference is unstable."));

    meta.setEmpty();
    meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("1 minute aligned");
    page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Interval:"), "Processing/Interval",
                                   meta);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("This sets the interval between model runs."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Processing interval"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the interval of data that incorporated into processed values (e.x. temperature difference standard deviation).  It normally makes sense to set this equal to the averaging time."));

    meta.setEmpty();
    meta["*hUndefinedDescription"] =
            AcquisitionComponentPage::tr("Stable with a single valid point");
    meta["*hEditor/EnableStability"] = true;
    meta["*hEditor/DefaultSmoother/Type"] = "SinglePoint";
    page->addSmoother(AcquisitionComponentPage::tr("Temperature stability:"),
                      "Processing/TemperatureStability", meta);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "This sets smoother used for calculation of temperature stability."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Temperature stability smoother"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets smoother used to to calculate the internal stability indicator from the temperature difference."));

    page->endBox();

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Report interval (s):"), "ReportInterval");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(2.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The expected reporting interval in seconds."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Report interval"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This sets the expected reporting interval."));

    /* Basetime not set */

    page->addBoolean(AcquisitionComponentPage::tr("Enable strict parsing"), "StrictMode");
    page->setLastToolTip(AcquisitionComponentPage::tr("Enable strict mode parsing."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Strict parsing"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This enables additional checks during data parsing."));

    page->addStretch();

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_dmt_pax(const Variant::Read &value,
                                                                QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;

    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Measurement wavelength (nm):"),
                           "Wavelength");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(532.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("This overrides the reported measurement wavelength."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Measurement wavelength"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "Then set, this causes the acquisition system to disregard the reported wavelength and use this value instead."));

    page->addBoolean(AcquisitionComponentPage::tr("Enable strict parsing"), "StrictMode");
    page->setLastUnassigned(true);
    page->setLastToolTip(AcquisitionComponentPage::tr("Enable strict mode parsing."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Strict parsing"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This enables additional checks during data parsing."));

    page->addStretch();

    result.append(createSpancheckPage(value, SpancheckFlags_NoCounts, parent));

    return result;
}

static QList<
        AcquisitionComponentPage *> create_acquire_ecotech_nephaurora(const Variant::Read &value,
                                                                      QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;
    Variant::Write meta = Variant::Write::empty();


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Zero"), value, parent);
    result.append(page);

    meta.setEmpty();
    meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("62 seconds");
    page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Blanking time:"), "BlankTime",
                                   meta);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "This sets the amount of time data are removed after an instrument zero."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Blank time"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the amount of time data are removed after the instrument resumes from a zero."));

    page->addEnum(AcquisitionComponentPage::tr("Mode:"), "Zero/Mode");
    page->setLastToolTip(
            AcquisitionComponentPage::tr("This is the mode by which zeros are calculated."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Zero mode"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This sets how zeros are handled by the system."));
    page->appendLastEnumEntry(AcquisitionComponentPage::tr("Software offset"),
                              QStringList("Offset") << "Software", AcquisitionComponentPage::tr(
                    "Calculate the zero offset by controlling the sampling in software without adjusting the instrument."));
    page->appendLastEnumEntry(AcquisitionComponentPage::tr("Manipulate Kalman filter"),
                              QStringList("EnableFilter"), AcquisitionComponentPage::tr(
                    "Enable the Kalman filter during zeroing, disabling it after the zero completes."));
    page->appendLastEnumEntry(AcquisitionComponentPage::tr("Instrument native"),
                              QStringList("Native") << "Instrument", AcquisitionComponentPage::tr(
                    "Simply issue the zero command to the instrument and allow it to proceed normally."));

    meta.setEmpty();
    meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("240 seconds");
    page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Fill time:"), "Zero/Fill", meta);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The time to wait for the instrument to fill with filtered air in software offset mode."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Fill time"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "When in software offset zero mode, this sets the amount of time to wait for the nephelometer to fill with filtered air.  This is effectively an additional blank time at the start of the zero."));

    meta.setEmpty();
    meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("300 seconds");
    page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Measurement time:"),
                                   "Zero/Measure", meta);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The time to measure filtered air in software offset zero mode."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Measure time"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "When in software offset zero mode, this sets the amount of time to sample filtered air in order to calculate the effective zero offset."));

    page->addEnum(AcquisitionComponentPage::tr("Instrument smoothing:"), "DataSmoothing");
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "Instrument smoothing mode set by the acquisition system."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Smoothing mode"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets how the instrument will smooth data when it is operating in a mode that requires data smoothing for proper operation."));
    page->appendLastEnumEntry(AcquisitionComponentPage::tr("Kalman filter"),
                              QStringList("Kalman") << "KalmanFilter", AcquisitionComponentPage::tr(
                    "Use an infinite response Kalman filter."));
    page->appendLastEnumEntry(AcquisitionComponentPage::tr("Moving average"),
                              QStringList("Average") << "MovingAverage",
                              AcquisitionComponentPage::tr("Use a moving average window."));
    page->appendLastEnumEntry(AcquisitionComponentPage::tr("No smoothing"),
                              QStringList("None") << "Disable",
                              AcquisitionComponentPage::tr("Disable smoothing entirely."));

    page->addStretch();


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->beginBox("Sample wavelengths");

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Blue (nm):"), "Wavelength/B");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(AcquisitionComponentPage::tr("Automatically determined"));
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The wavelength of the blue sampling channel in nm."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Blue wavelength"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This allows the blue sampling wavelength in nm to be overridden."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Green (nm):"), "Wavelength/G");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(AcquisitionComponentPage::tr("Automatically determined"));
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The wavelength of the green sampling channel in nm."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Green wavelength"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This allows the green sampling wavelength in nm to be overridden."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Red (nm):"), "Wavelength/R");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(AcquisitionComponentPage::tr("Automatically determined"));
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The wavelength of the red sampling channel in nm."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Red wavelength"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This allows the red sampling wavelength in nm to be overridden."));

    page->endBox();

    page->beginBox("Retry");

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Delay (s):"), "Retry/Delay");
    page->setLastDoubleSpinBox(1, 0, 9999, 0.1);
    page->setLastUnassigned(2.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("Delay time before retrying a failed or busy command."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Retry delay"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the time to wait after a failed or busy command before any communications are attempted."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Busy timeout (s):"), "Retry/BusyTimeout");
    page->setLastDoubleSpinBox(0, 0, 99999, 1);
    page->setLastUnassigned(1800.0);
    page->setLastToolTip(AcquisitionComponentPage::tr("Maximum instrument busy response time."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Busy timeout"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the maximum time the instrument can respond with busy indicators before communications are dropped."));

    page->addIntegerSpinBox(AcquisitionComponentPage::tr("Maximum failures:"),
                            "Retry/MaximumFailures");
    page->setLastIntegerSpinBox(0, 9999);
    page->setLastUnassigned(static_cast<qint64>(5));
    page->setLastToolTip(AcquisitionComponentPage::tr("Maximum number of failed commands."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Maximum failed commands"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the maximum number of failed or invalid command responses to accept before communications are dropped."));

    page->endBox();

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Interrogation delay (s):"),
                           "InterrogationDelay");
    page->setLastDoubleSpinBox(1, 0, 9999, 0.1);
    page->setLastUnassigned(1.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The time between interrogation command bursts."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Interrogation delay"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the delay between parameter reads.  This effectively controls how often data are read from the instrument.  Actual data updates only occur when a change is detected in synchronized parameters (e.x. scattering)."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Command delay (s):"), "BurstDelay");
    page->setLastDoubleSpinBox(3, 0, 99, 0.1);
    page->setLastUnassigned(0.125);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The time between commands send to the instrument."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Command delay"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the delay between individual commands.  Commands are only sent to the instrument after this long has elapsed from the previous one."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Optical update timeout (s):"),
                           "OpticalUpdateTimeout");
    page->setLastDoubleSpinBox(1, 0, 120, 0.1);
    page->setLastUnassigned(AcquisitionComponentPage::tr("Automatic"));
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The maximum time to wait for a detected optical (scattering) change."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Optical update timeout"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This set the maximum time to wait for a detected change in scattering before outputting an update anyway.  Normally this is determined by the number of angles, since it depends on the shutter movement."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Reference update timeout (s):"),
                           "ReferenceUpdateTimeout");
    page->setLastDoubleSpinBox(1, 0, 120, 0.1);
    page->setLastUnassigned(AcquisitionComponentPage::tr("Automatic"));
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The maximum time to wait for a detected reference count rate change."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Reference update timeout"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This set the maximum time to wait for a detected change in reference count rates before outputting an update anyway.  Normally this is determined by the number of angles, since it depends on the shutter movement with a minimum time between reference engagements."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Normalization temperature (°C):"),
                           "NormalizationTemperature");
    page->setLastDoubleSpinBox(0, -273.15, 100, 1);
    page->setLastExplicitUndefined(true);
    page->setLastUnassigned(AcquisitionComponentPage::tr("0"));
    page->setLastToolTip(AcquisitionComponentPage::tr("The STP temperature to set."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Instrument STP temperature"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the temperature to request the instrument to normalize at."));

    page->addIntegerSpinBox(AcquisitionComponentPage::tr("Address:"), "Address");
    page->setLastIntegerSpinBox(0, 6);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("This is the internal address of the nephelometer."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Instrument address"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the address code that the acquisition system attempts to communicate with.  This must match the setting on the nephelometer."));

    page->addStretch();

    result.append(createSpancheckPage(value, SpancheckFlags_Ecotech, parent));

    return result;
}

static QList<
        AcquisitionComponentPage *> create_acquire_eigenbrodt_nmo191(const Variant::Read &value,
                                                                     QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Poll interval (s):"), "PollInterval");
    page->setLastDoubleSpinBox(1, 0.1, 9999, 0.1);
    page->setLastUnassigned(1.0);
    page->setLastToolTip(AcquisitionComponentPage::tr("The polling interval in seconds."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Polling interval"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the delay between instrument data polling.  This determines how frequently the data values are updated."));

    page->addIntegerSpinBox(AcquisitionComponentPage::tr("Address:"), "Address");
    page->setLastIntegerSpinBox(0, 99999);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The instrument address or undefined for any."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Instrument address"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This sets a the address of the instrument to control."));

    page->addBoolean(AcquisitionComponentPage::tr("Enable strict parsing"), "StrictMode");
    page->setLastUnassigned(true);
    page->setLastToolTip(AcquisitionComponentPage::tr("Enable strict mode parsing."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Strict parsing"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This enables additional checks during data parsing."));

    page->addStretch();

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_generic_metar(const Variant::Read &value,
                                                                      QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Station altitude (m):"), "Altitude");
    page->setLastDoubleSpinBox(0, 1, 99999, 1);
    page->setLastUnassigned(0.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The station altitude used in pressure conversions."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Station altitude"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the the station elevation.  This must match the altitude used to generate the METAR records and is used to conver the pressure back to absolute."));

    /* Basetime not set */

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Report interval (s):"), "ReportInterval");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(1.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The expected reporting interval in seconds."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Report interval"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This sets the expected reporting interval."));

    page->addBoolean(AcquisitionComponentPage::tr("Report every METAR record"),
                     "ReportEveryRecord");
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "Generate data for every METAR record instead of only ones with an elapsed time."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Report every record"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "When enabled, this generated a data update whenever a METAR record is processed, instead of waiting for one with a non-zero elapsed time."));

    page->addStretch();

    return result;
}

static QList<AcquisitionComponentPage *> createGenericPassiveRecordPages(const Variant::Read &value,
                                                                         QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;

    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("General"), value,
                                                    parent);
        result.append(page);

        page->addBoolean(AcquisitionComponentPage::tr("Case insensitive matching"),
                         "MatchCaseInsensitive");
        page->setLastToolTip(
                AcquisitionComponentPage::tr("Do not consider case during record matching."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Match case insensitive"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This option means that the record matching regular expression ignores case."));

        page->addBoolean(AcquisitionComponentPage::tr("Require at least one defined variable"),
                         "RequireValidVariable");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "Require at least one variable to be valid for the record to match."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Require a valid variable"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "When enabled, this setting means the record must contain at least on valid value of a variable for it to match."));

        page->beginBox(AcquisitionComponentPage::tr("Header"));

        page->addSingleLineString(AcquisitionComponentPage::tr("Match:"), "Header/Match");
        page->setLastUnassigned(AcquisitionComponentPage::tr("Disabled"));
        page->setLastToolTip(
                AcquisitionComponentPage::tr("The regular expression used to match headers."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Header pattern"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "If this regular expression matches a line, it is considered a header for the record and the fields interpreted by any variable header matches."));

        page->addBoolean(AcquisitionComponentPage::tr("Case insensitive matching"),
                         "Header/MatchCaseInsensitive");
        page->setLastToolTip(
                AcquisitionComponentPage::tr("Do not consider case during header matching."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Match case insensitive"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This option means that the header matching regular expression ignores case."));

        page->endBox();

        page->beginBox(AcquisitionComponentPage::tr("Field splitting"));

        page->addSingleLineString(AcquisitionComponentPage::tr("Separator:"), "Fields/Separator");
        page->setLastUnassigned(AcquisitionComponentPage::tr(","));
        page->setLastToolTip(AcquisitionComponentPage::tr("The separator between fields."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Field separator"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the text between fields in a line that join them together."));

        page->addSingleLineString(AcquisitionComponentPage::tr("Quote:"), "Fields/Quote");
        page->setLastUnassigned(AcquisitionComponentPage::tr("\""));
        page->setLastToolTip(
                AcquisitionComponentPage::tr("The quote used when a field contains a separator."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Field quote"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the text that is added to the start and end of fields when the field data contains the separator character."));

        page->addSingleLineString(AcquisitionComponentPage::tr("Quote escape:"),
                                  "Fields/QuoteEscape");
        page->setLastUnassigned(AcquisitionComponentPage::tr("\"\"\""));
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The escape used in a quoted field to escape another instance of the quote itself."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Join quote escape"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the text that existing quotes within a quoted field are replaced with."));

        page->endBox();

        page->addStretch();
    }

    {
        AcquisitionComponentBoxCompositePage *box =
                new AcquisitionComponentBoxCompositePage(AcquisitionComponentPage::tr("Time"),
                                                         value, parent);
        result.append(box);

        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Output"), value,
                                                    box);
        box->addPage(page, 0);

        page->addDoubleSpinBox(AcquisitionComponentPage::tr("Interval (s):"), "Time/Interval");
        page->setLastDoubleSpinBox(1, 0.1, 9999.9, 1);
        page->setLastToolTip(
                AcquisitionComponentPage::tr("The expected interval between records."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Record interval"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This option sets the expected time between instances of the record."));

        page->addDoubleSpinBox(AcquisitionComponentPage::tr("Rounding (s):"), "Time/Interval");
        page->setLastDoubleSpinBox(1, 0.1, 9999.9, 1);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The expected rounding base applied when calculating times."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Record time rounding"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This option sets the rounding base used when not dealing with realtime data."));

        page->addBoolean(AcquisitionComponentPage::tr("Require a valid time"), "Time/RequireValid");
        page->setLastToolTip(
                AcquisitionComponentPage::tr("Require a valid time for the record to match."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Require a valid time"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "When enabled, this setting means the record must contain a valid time to match."));

        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Fields"), value,
                                                     "Time/Fields", box);
        table->setArrayMode();
        box->addPage(table, 1);

        table->addIntegerSpinBox(AcquisitionComponentPage::tr("Index"), "Time/Fields/%1");
        table->setLastIntegerSpinBox(1, 9999);
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The fields assembled into a time for parsing."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Time fields"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "These are the field indices (one based) that are assembled into a time for parsing."));

        table = new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Captures"),
                                                     value, "Time/Captures", box);
        table->setArrayMode();
        box->addPage(table, 1);

        table->addIntegerSpinBox(AcquisitionComponentPage::tr("Index"), "Time/Captures/%1");
        table->setLastIntegerSpinBox(0, 9999);
        table->setLastToolTip(AcquisitionComponentPage::tr(
                "The match expression captures assembled into a time for parsing."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Time captures"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "These are the match regular expression captures that are assembled into a time for parsing."));
    }

    return result;
}

static QList<
        AcquisitionComponentPage *> createGenericPassiveSingleVariablePages(const Variant::Read &value,
                                                                            QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;

    {
        AcquisitionComponentBoxCompositePage *box =
                new AcquisitionComponentBoxCompositePage(AcquisitionComponentPage::tr("Input"),
                                                         value, parent);
        result.append(box);

        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Missing values"),
                                                    value, box);
        box->addPage(page, 0);

        page->addSingleLineString(AcquisitionComponentPage::tr("Match:"), "MVC");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The regular expression used to check if the value is missing."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Missing value expression"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the regular expression used to check if the value result is missing."));

        page->addBoolean(AcquisitionComponentPage::tr("Case insensitive"), "MVCCaseInsensitive");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "Do not consider case during missing value matching."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Match case insensitive"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This option means that the missing value matching regular expression ignores case."));

        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Fields"), value,
                                                     "Fields", box);
        table->setArrayMode();
        box->addPage(table, 1);

        table->addIntegerSpinBox(AcquisitionComponentPage::tr("Index"), "Fields/%1");
        table->setLastIntegerSpinBox(1, 9999);
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The fields assembled into a time for parsing."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Time fields"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "These are the field indices (one based) that are assembled into a time for parsing."));

        table = new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Captures"),
                                                     value, "Captures", box);
        table->setArrayMode();
        box->addPage(table, 1);

        table->addIntegerSpinBox(AcquisitionComponentPage::tr("Index"), "Captures/%1");
        table->setLastIntegerSpinBox(0, 9999);
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The match expression captures used for parsing."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Value captures"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "These are the match regular expression captures that are use for value parsing."));

        table = new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Headers"), value,
                                                     "Headers", box);
        table->setArrayMode();
        box->addPage(table, 1);

        table->addString(AcquisitionComponentPage::tr("Index"), "Headers/%1");
        table->setLastToolTip(AcquisitionComponentPage::tr(
                "The header matched regular expressions used for parsing."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Header matches"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "These are the regular expressions matched against a header and the field matched fields used for value parsing."));
    }

    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Behavior"), value,
                                                    parent);
        result.append(page);

        page->addDoubleSpinBox(AcquisitionComponentPage::tr("Maximum age (s):"), "MaximumAge");
        page->setLastDoubleSpinBox(1, 0.1, 9999.9, 1);
        page->setLastUnassigned(10.0);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The maximum time between variable updates to retain values."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Maximum variable age"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This option sets the maximum time between two variable updates with valid values before a gap is created."));

        page->addEnum(AcquisitionComponentPage::tr("Value selection:"), "SearchBehavior");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This controls the selection behaviour for defined input possibilities."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Value selection behavior"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This option sets how the value field is selected from the list of fields and captures used as inputs."));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("First present"),
                                  QStringList("FirstPresent") << "FirstNonEmpty",
                                  AcquisitionComponentPage::tr(
                                          "Use the first value available that is not an empty field."));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("First defined"),
                                  QStringList("Defined") << "FirstValid",
                                  AcquisitionComponentPage::tr(
                                          "Use the first value that does not match the missing value matcher."));

        page->addEnum(AcquisitionComponentPage::tr("Absent values:"), "MissingBehavior");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This controls how to handle records that do not contain the variable."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Absent value behavior"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This option control how the variable output value responds when the record does not contain it at all."));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("Invalid record"),
                                  QStringList("Invalid") << "InvalidRecord",
                                  AcquisitionComponentPage::tr(
                                          "Consider the entire record invalid."));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("Invalidate"),
                                  QStringList("MVC") << "OutputMVC" << "OutputInvalid",
                                  AcquisitionComponentPage::tr(
                                          "Output a specifically invalidated value for the variable."));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("Gap"),
                                  QStringList("Gap") << "SkipWithGap" << "IgnoreWithGap",
                                  AcquisitionComponentPage::tr(
                                          "Introduce an explicit gap in the output for the variable."));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("Ignore"),
                                  QStringList("Ignore") << "Skip", AcquisitionComponentPage::tr(
                        "Ignore the invalid value and continue reporting the prior output."));

        page->addEnum(AcquisitionComponentPage::tr("Undefined values:"), "MVCBehavior");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This controls how to handle values that match the specified missing value."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Undefined value behavior"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This option control how the variable output value responds when it matches the explicitly missing regular expression."));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("Invalidate"),
                                  QStringList("MVC") << "OutputMVC" << "OutputInvalid",
                                  AcquisitionComponentPage::tr(
                                          "Output a specifically invalidated value for the variable."));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("Invalid record"),
                                  QStringList("Invalid") << "InvalidRecord",
                                  AcquisitionComponentPage::tr(
                                          "Consider the entire record invalid."));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("Gap"),
                                  QStringList("Gap") << "SkipWithGap" << "IgnoreWithGap",
                                  AcquisitionComponentPage::tr(
                                          "Introduce an explicit gap in the output for the variable."));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("Ignore"),
                                  QStringList("Ignore") << "Skip", AcquisitionComponentPage::tr(
                        "Ignore the invalid value and continue reporting the prior output."));

        page->addEnum(AcquisitionComponentPage::tr("Accumulation conversion:"), "Accumulation");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This controls how to convert an accumulated variable."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Variable accumulation conversion"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This option sets how an accumulation variable is converted into final values."));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("Disabled"),
                                  QStringList("Disabled") << "None", AcquisitionComponentPage::tr(
                        "No accumulation to rate conversion is performed for the variable."));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("Rate"), "Rate",
                                  AcquisitionComponentPage::tr(
                                          "Convert the value to a rate by taking the difference between the current input and the prior, then dividing by the time between the two readings."));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("Absolute"),
                                  QStringList("Absolute") << "Difference",
                                  AcquisitionComponentPage::tr(
                                          "Convert the value by taking the difference between the current reading and the prior one."));

        page->addStretch();
    }

    return result;
}

namespace {
class ControlGenericPassiveVariableInputDelegate : public AcquisitionComponentTableDelegatePages {
public:
    ControlGenericPassiveVariableInputDelegate(QObject *parent = 0)
            : AcquisitionComponentTableDelegatePages(parent)
    { }

    virtual ~ControlGenericPassiveVariableInputDelegate()
    { }

    virtual QList<AcquisitionComponentPage *> createPages(const CPD3::Data::Variant::Read &value,
                                                          QWidget *parent = 0) const
    { return createGenericPassiveSingleVariablePages(value, parent); }

    virtual QString buttonText(const CPD3::Data::Variant::Read &value) const
    { return tr("Edit"); }
};
}

static QList<
        AcquisitionComponentPage *> createGenericPassiveVariablesPages(const Variant::Read &value,
                                                                       QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;

    AcquisitionComponentGenericTable *table =
            new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Record Variables"),
                                                 value, "", parent);
    result.append(table);

    table->addString(AcquisitionComponentPage::tr("Variable"), "%1/Name");
    table->setLastDefaults(QChar(QChar::ObjectReplacementCharacter));
    table->setLastString(true);
    table->setLastToolTip(AcquisitionComponentPage::tr("The output variable name."));
    table->setLastStatusTip(AcquisitionComponentPage::tr("Variable name"));
    table->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This is the output variable name that the data are logged as."));

    table->addCalibration(AcquisitionComponentPage::tr("Calibration"), "%1/Calibration");
    table->setLastToolTip(AcquisitionComponentPage::tr(
            "The calibration applied to the input value (from voltage to physical units)."));
    table->setLastStatusTip(AcquisitionComponentPage::tr("Calibration"));
    table->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This is the calibration applied to the input voltage to convert it to physical units."));

    table->addColumn(AcquisitionComponentPage::tr("Input"), "%1",
                     new ControlGenericPassiveVariableInputDelegate(table));
    table->setLastToolTip(AcquisitionComponentPage::tr("Variable input settings."));
    table->setLastStatusTip(AcquisitionComponentPage::tr("Variable input"));
    table->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This controls the input used to calculate the variable value."));

    table->addMetadata(AcquisitionComponentPage::tr("Metadata"), "%1/Metadata");
    table->setLastToolTip(AcquisitionComponentPage::tr("The metadata base for the analog input."));
    table->setLastStatusTip(AcquisitionComponentPage::tr("Variable metadata"));
    table->setLastWhatsThis(AcquisitionComponentPage::tr(
            "Use this to set the base metadata generated along with the analog input value."));

    return result;
}

namespace {
class ControlGenericPassiveRecordDelegate : public AcquisitionComponentTableDelegatePages {
public:
    ControlGenericPassiveRecordDelegate(QObject *parent = 0)
            : AcquisitionComponentTableDelegatePages(parent)
    { }

    virtual ~ControlGenericPassiveRecordDelegate()
    { }

    virtual QList<AcquisitionComponentPage *> createPages(const CPD3::Data::Variant::Read &value,
                                                          QWidget *parent = 0) const
    { return createGenericPassiveRecordPages(value, parent); }

    virtual QString buttonText(const CPD3::Data::Variant::Read &value) const
    { return tr("Edit"); }
};

class ControlGenericPassiveVariablesDelegate : public AcquisitionComponentTableDelegatePages {
public:
    ControlGenericPassiveVariablesDelegate(QObject *parent = 0)
            : AcquisitionComponentTableDelegatePages(parent)
    { }

    virtual ~ControlGenericPassiveVariablesDelegate()
    { }

    virtual QList<AcquisitionComponentPage *> createPages(const CPD3::Data::Variant::Read &value,
                                                          QWidget *parent = 0) const
    { return createGenericPassiveVariablesPages(value, parent); }

    virtual QString buttonText(const CPD3::Data::Variant::Read &value) const
    { return tr("Edit"); }
};
}

static QList<AcquisitionComponentPage *> create_acquire_generic_passive(const Variant::Read &value,
                                                                        QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;

    {
        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Records"), value,
                                                     "Records", parent);
        table->setArrayMode(true, AcquisitionComponentGenericTable::Array_SingleHash);
        result.append(table);

        table->addString(AcquisitionComponentPage::tr("Match"), "Records/%1/Match");
        table->setLastString(true);
        table->setLastDefaults(AcquisitionComponentPage::tr("Any"));
        table->setLastToolTip(AcquisitionComponentPage::tr(
                "The regular expression matched against the line to locate the record."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Matching regular expression"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the regular expression that a line must match to be considered for the record."));

        table->addColumn(AcquisitionComponentPage::tr("Record"), "Records/%1",
                         new ControlGenericPassiveRecordDelegate(table));
        table->setLastToolTip(AcquisitionComponentPage::tr("General record settings."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Record settings"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This controls general settings for the whole record."));

        table->addColumn(AcquisitionComponentPage::tr("Variables"), "Records/%1/Variables",
                         new ControlGenericPassiveVariablesDelegate(table));
        table->setLastToolTip(AcquisitionComponentPage::tr("The variables in the record."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Variables"));
        table->setLastWhatsThis(
                AcquisitionComponentPage::tr("This controls the variables defined by the record."));
    }

    {
        Variant::Write meta = Variant::Write::empty();
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                                    parent);
        result.append(page);

        page->addDoubleSpinBox(AcquisitionComponentPage::tr("Timeout (s):"), "Timeout");
        page->setLastDoubleSpinBox(0, 1, 9999, 1);
        page->setLastUnassigned(10.0);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The maximum amount of time to wait before reporting lost communications."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Communications timeout"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the maximum time without a matched record before communications loss is reported."));

        page->addBoolean(AcquisitionComponentPage::tr("Process multiple matches"),
                         "AllowMultipleMatches");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This enables multiple record matches for a single line."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Allow multiple matches"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "When enabled, record processing continues even after there is a match.  This allows multiple record definitions to match the same line."));

        page->addBoolean(AcquisitionComponentPage::tr("Allow unmatched lines"),
                         "AllowUnmatchedLines");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "Do not report communications loss when a line matches no records."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Accept unknown lines"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This prevents communications loss from being reported when a line that matches no lines is received."));

        meta.setType(Variant::Type::MetadataHash);
        meta["*cManufacturer/*sDescription"] =
                AcquisitionComponentPage::tr("The instrument manufacturer");
        meta["*cModel/*sDescription"] =
                AcquisitionComponentPage::tr("The instrument model identifier");
        meta["*cSerialNumber/*iDescription"] =
                AcquisitionComponentPage::tr("The instrument serial number");
        page->addGenericValue(AcquisitionComponentPage::tr("Instrument source metadata"),
                              "InstrumentMetadata", meta);
        page->setLastToolTip(AcquisitionComponentPage::tr("The instrument source metadata."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Source metadata"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the source recorded in the metadata for all data generated by the instrument."));

        page->beginBox(AcquisitionComponentPage::tr("Auto-detection"));

        page->addIntegerSpinBox(AcquisitionComponentPage::tr("Required valid records:"),
                                "Autoprobe/RequiredRecords");
        page->setLastIntegerSpinBox(1, 9999);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The number of valid records records for auto-detection success."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Required auto-detection records"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This controls the number of valid required required before auto-detection succeeds."));

        page->addBoolean(AcquisitionComponentPage::tr("Require all records"),
                         "Autoprobe/RequireAllRecords");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "Require all defined records to be seen for auto-detection success."));
        page->setLastStatusTip(
                AcquisitionComponentPage::tr("Require all records for auto-detection"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This option requires that all defined records be seen at least once before auto-detection succeeds."));

        page->endBox();

        page->addStretch();
    }

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_gmd_clap3w(const Variant::Read &value,
                                                                   QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    Variant::Write meta = Variant::Write::empty();
    AcquisitionComponentPageGeneric *page;

    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Calibration"), value,
                                               parent);
    result.append(page);

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Flow scale factor:"), "FlowScale");
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The scaling factor applied to the flow read from the instrument."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Flow scale"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This is the scaling factor applied to the flow and volume as read from the instrument.  It can be used to correct for minor differences from the calibration curve at normal operating conditions."));

    page->addCalibration(AcquisitionComponentPage::tr("Voltage to flow calibration"),
                         "HardwareFlowCalibration");
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The calibration polynomial used to convert the MFM voltage to a flow rate."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Voltage to flow calibration"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This is the (fourth order maximum) calibration polynomial used to convert the voltage read from the internal MFM into a flow rate.  This is applied before the scaling factor."));

    page->addDoubleSpinBoxArray(AcquisitionComponentPage::tr("Spot area (mm²)"), "Area", 8);
    page->setLastDoubleSpinBox(2, 0.1, 99.99, 0.01);
    page->setLastUnassigned(19.9);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The area of each sampling spot in mm², clockwise."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Spot area"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "These are the areas of each sampling spot in mm², starting from the right of the reference and going clockwise."));

    page->setLastStretch();


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Filter"), value,
                                               parent);
    result.append(page);

    page->beginBox(AcquisitionComponentPage::tr("Automatic spot advance"));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Blue:"), "Spot/AdvanceTransmittance/B",
                           value["Spot/AdvanceTransmittance"].toDouble());
    page->setLastDoubleSpinBox(3, 0.001, 0.999, 0.1);
    page->setLastUnassigned(0.5);
    page->setLastExplicitUndefined();
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The minimum blue transmittance before an automatic spot advance is initiated."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Blue spot advance transmittance"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the threshold for the blue transmittance below which an automatic spot advance is attempted.  When set but undefined, the threshold is disabled (no automatic advance attempted)."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Green:"), "Spot/AdvanceTransmittance/G",
                           value["Spot/AdvanceTransmittance"].toDouble());
    page->setLastDoubleSpinBox(3, 0.001, 0.999, 0.1);
    page->setLastUnassigned(0.7);
    page->setLastExplicitUndefined();
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The minimum green transmittance before an automatic spot advance is initiated."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Green spot advance transmittance"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the threshold for the green transmittance below which an automatic spot advance is attempted.  When set but undefined, the threshold is disabled (no automatic advance attempted)."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Red:"), "Spot/AdvanceTransmittance/R",
                           value["Spot/AdvanceTransmittance"].toDouble());
    page->setLastDoubleSpinBox(3, 0.001, 0.999, 0.1);
    page->setLastUnassigned(0.5);
    page->setLastExplicitUndefined();
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The minimum red transmittance before an automatic spot advance is initiated."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Blue spot advance transmittance"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the threshold for the red transmittance below which an automatic spot advance is attempted.  When set but undefined, the threshold is disabled (no automatic advance attempted)."));

    page->endBox();

    page->beginBox(AcquisitionComponentPage::tr("Filter change start auto-detection"));

    page->addBoolean(AcquisitionComponentPage::tr("Enable start of filter detection"),
                     "Autodetect/Start/Enable");
    page->setLastUnassigned(true);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("Enable start of filter change auto-detection."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Filter start auto-detection"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This enables the automatic start of filter change detection.  That is, this controls if a filter change is initiated automatically (not the start of the filter itself)."));

    page->addDouble(AcquisitionComponentPage::tr("Sample intensity threshold:"),
                    "Autodetect/Start/Intensity");
    page->setLastUnassigned(1000.0);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The lower limit for sample intensities before a filter change is started."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Sample intensity limit"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This is the threshold for sample intensities below which a filter change is initiated.  That is, the filter change begins when any sample intensity falls below this value."));

    page->addDouble(AcquisitionComponentPage::tr("Dark intensity threshold:"),
                    "Autodetect/Start/Dark");
    page->setLastUnassigned(1000.0);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The upper limit for dark intensities before a filter change is started."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Dark intensity limit"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This is the threshold for dark intensities above which a filter change is initiated.  That is, the filter change begins when any dark intensity exceeds this value."));

    page->endBox();

    page->beginBox(AcquisitionComponentPage::tr("Filter change end auto-detection"));

    page->addBoolean(AcquisitionComponentPage::tr("Enable end of filter detection"),
                     "Autodetect/End/Enable");
    page->setLastUnassigned(true);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("Enable end of filter change auto-detection."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Filter end auto-detection"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This enables the automatic end of filter change detection.  That is, this controls if a filter change is completed automatically (not the end of the filter itself)."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("White filter intensity band:"),
                           "Autodetect/End/Band");
    page->setLastDoubleSpinBox(3, 0.001, 99.999, 0.1);
    page->setLastUnassigned(0.9);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The scaling band that all intensities must be in relative to a white filter."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Filter change end intensity band"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the relative scaling band with respect to a white filter that all intensities must fall within before the filter change is ended.  For example, 0.9 sets the band as white/(1+0.9) to white*(1+0.9)."));

    page->endBox();

    meta.setEmpty();
    meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr(
            "The smoother applied to the spot readings when initializing to determine stability and set the normalization for transmittance 1.0");
    meta["*hEditor/EnableStability"] = true;
    meta["*hEditor/DefaultSmoother/Type"] = "FixedTime";
    meta["*hEditor/DefaultSmoother/Time"] = 60.0;
    meta["*hEditor/DefaultSmoother/MinimumTime"] = 30.0;
    meta["*hEditor/DefaultSmoother/DiscardTime"] = 8.0;
    meta["*hEditor/DefaultSmoother/RSD"] = 0.001;
    page->addSmoother(AcquisitionComponentPage::tr("Spot normalization:"), "Spot/Normalize", meta);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("This sets smoother used for active spot normalization."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Spot normalization"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets smoother used to to calculate the normalized intensities established as transmittance 1.0 when a new spot starts sampling.  This is also used to ensure stability of the spot once air is being pulled through it."));


    meta.setEmpty();
    meta["*hUndefinedDescription"] =
            AcquisitionComponentPage::tr("A 90 second boxcar with a maximum RSD of 0.001");
    meta["*hEditor/EnableStability"] = true;
    meta["*hEditor/DefaultSmoother/Type"] = "FixedTime";
    meta["*hEditor/DefaultSmoother/Time"] = 90.0;
    meta["*hEditor/DefaultSmoother/MinimumTime"] = 90.0;
    meta["*hEditor/DefaultSmoother/RSD"] = 0.001;
    page->addSmoother(AcquisitionComponentPage::tr("Filter baseline:"), "Filter/Baseline", meta);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "This sets smoother used to establish a new filter baseline."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Filter baseline"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets smoother used to establish a new filter baseline before any air is pulled through it.  This is used for comparison with respect to a white filter and to ensure that the filter parameters are stable."));


    page->addDoubleSpinBox(AcquisitionComponentPage::tr("White filter confirmation band:"),
                           "Filter/VerifyWhiteBand");
    page->setLastDoubleSpinBox(3, 0.001, 99.999, 0.1);
    page->setLastUnassigned(0.9);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The scaling band for normalized spot intensities relative to a white filter."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("White filter band"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the relative scaling band with respect to a white filter that all normalized must fall within or a diagnostic message and flagging are generated.  For example, 0.9 sets the band as white/(1+0.9) to white*(1+0.9)."));


    page->addStretch();


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->beginBox("Sample wavelengths");

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Blue (nm):"), "Wavelength/B");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(467.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The wavelength of the blue sampling channel in nm."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Blue wavelength"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This allows the blue sampling wavelength in nm to be overridden."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Green (nm):"), "Wavelength/G");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(528.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The wavelength of the green sampling channel in nm."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Green wavelength"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This allows the green sampling wavelength in nm to be overridden."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Red (nm):"), "Wavelength/R");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(652.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The wavelength of the red sampling channel in nm."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Red wavelength"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This allows the red sampling wavelength in nm to be overridden."));

    page->endBox();

    page->addBoolean(AcquisitionComponentPage::tr("Reintegrate volume"), "IntegrateVolume");
    page->setLastToolTip(
            AcquisitionComponentPage::tr("Enable reintegration of the sampled volume."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Reintegrate volume"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "When enabled, this causes the acquisition to reintegrate the volume instead of using the instrument reported one."));

    meta.setEmpty();
    meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("30 seconds aligned");
    page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Contamination time after bypass:"),
                                   "BypassResumeContaminate", meta);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "This sets the amount of time data are flagged as contaminated after resuming from a bypass."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Contamination time after bypass"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the amount of time data are flagged as contaminated after the instrument resumes from a bypass state.  This is used to compinsate for any filter flexing effects caused by the intrerruption of flow."));

    page->addBoolean(AcquisitionComponentPage::tr("Verify filter instrument ID"),
                     "Filter/CheckInstrumentID");
    page->setLastUnassigned(true);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "Verify the instrument identifier after startup before accepting an old filter."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Saved filter ID verification"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This controls if the saved filter parameters are checked against the current instrument ID (serial number, etc) before the filter is accepted after an acquisition system restart."));

    meta.setEmpty();
    meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("30 minutes");
    page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Spot resume timeout:"),
                                   "Spot/ResumeTimeout", meta);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "This set the maximum amount of downtime allowed before advancing to the next spot."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Spot resume timeout"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the maximum amount of downtime allowed before the system will attempt to advance to the next spot."));

    page->addBoolean(AcquisitionComponentPage::tr("Enable realtime diagnostics"),
                     "RealtimeDiagnostics");
    page->setLastToolTip(AcquisitionComponentPage::tr("Generate realtime diagnostic data."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Realtime diagnostics"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "When enabled, this causes a large amount of diagnostic data to be generated (e.x. all spot intensities).  This greatly increased the volume of data generated and should only be used for troubleshooting."));

    page->addBoolean(AcquisitionComponentPage::tr("Enable clock verification"),
                     "CheckInstrumentTiming");
    page->setLastUnassigned(true);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("Generate verification of the instrument clock."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Instrument clock verification"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "When enabled, this makes the acquisition system generate warnings if it detects an unexpected number of reports in a given time interval."));

    page->addBoolean(AcquisitionComponentPage::tr("Enable strict parsing"), "StrictMode");
    page->setLastToolTip(AcquisitionComponentPage::tr("Enable strict mode parsing."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Strict parsing"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This enables additional checks during data parsing."));

    page->addStretch();

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_gmd_cpcpulse(const Variant::Read &value,
                                                                     QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Sample flow rate (lpm):"), "Flow");
    page->setLastDoubleSpinBox(3, 0.001, 99.999, 0.1);
    page->setLastUnassigned(1.421);
    page->setLastExplicitUndefined();
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The sample flow rate used to convert the counts to a concentration."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Sample flow rate"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the sample flow rate in lpm that is used to convert the raw count rate into a number concentration."));

    page->addBoolean(AcquisitionComponentPage::tr("Use measured time between records"),
                     "UseMeasuredTime");
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "Use the time measured between records instead of assuming 1 Hz."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Use measured report time"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This enables causes the acquisition system to use the measured time between reports instead of assuming 1 Hz when calculating count rates."));

    page->addIntegerSpinBox(AcquisitionComponentPage::tr("Channel:"), "Channel");
    page->setLastIntegerSpinBox(1, 2);
    page->setLastUnassigned((qint64) 1);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("This is the sampling channel on the pulse counter box."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Sampling channel"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets input channel being used on the pulse counter converter box."));

    page->addStretch();

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_grimm_opc110x(const Variant::Read &value,
                                                                      QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;

    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->addBoolean(AcquisitionComponentPage::tr("Use measured time between records"),
                     "UseMeasuredTime");
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "Use the time measured between records instead of assuming 1/6 Hz."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Use measured report time"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This enables causes the acquisition system to use the measured time between reports instead of assuming 1/6 Hz when calculating count rates."));

    page->addBoolean(AcquisitionComponentPage::tr("Enable strict parsing"), "StrictMode");
    page->setLastToolTip(AcquisitionComponentPage::tr("Enable strict mode parsing."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Strict parsing"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This enables additional checks during data parsing."));

    page->addSingleLineString(AcquisitionComponentPage::tr("Base model number:"), "Model");
    page->setLastUnassigned(AcquisitionComponentPage::tr("Automatically determined"));
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The base model number used to determine the expected data."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Parsing model number"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This set the model number used to parse data, which determines the number and sizes of the bins."));

    page->addSingleLineString(AcquisitionComponentPage::tr("Base firmware version:"), "Firmware");
    page->setLastUnassigned(AcquisitionComponentPage::tr("Automatically determined"));
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The base firmware version use to interpret data."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Parsing firmware version"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This set the firmware version used to parse data, which determines some conversion factors that have changed in different versions."));

    page->addStretch();

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_love_pid(const Variant::Read &value,
                                                                 QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;

    {
        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Input"), value,
                                                     "Variables", parent);
        result.append(table);

        table->addString(AcquisitionComponentPage::tr("Variable"), "Variables/%1/Name");
        table->setLastDefaults(QChar(QChar::ObjectReplacementCharacter));
        table->setLastString(true);
        table->setLastToolTip(AcquisitionComponentPage::tr("The output variable name."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Variable name"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the output variable name that the data are logged as."));

        table->addIntegerSpinBox(AcquisitionComponentPage::tr("Address"), "Variables/%1/Address");
        table->setLastDefaults(0x31, Variant::Root(0x31));
        table->setLastIntegerSpinBox(1, 1023, NumberFormat("0xFF"), false);
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The controller address number to read from."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Controller address"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the controller address to read data from.  This is normally displayed in hexadecimal both on the controller and here."));

        table->addCalibration(AcquisitionComponentPage::tr("Calibration"),
                              "Variables/%1/Calibration");
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The calibration applied to the input value."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Calibration"));
        table->setLastWhatsThis(
                AcquisitionComponentPage::tr("This is the calibration applied to the input."));

        table->addMetadata(AcquisitionComponentPage::tr("Metadata"), "Variables/%1/Metadata");
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The metadata base for the analog input."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Variable metadata"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "Use this to set the base metadata generated along with the input value."));
    }

    {
        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Output"), value,
                                                     "SetpointOutputs", parent);
        result.append(table);

        table->addIntegerSpinBox(AcquisitionComponentPage::tr("Address"), "SetpointOutputs/%1");
        table->setLastDefaults(0x31, Variant::Root(0x31));
        table->setLastIntegerSpinBox(1, 1023, NumberFormat("0xFF"), false);
        table->setLastToolTip(AcquisitionComponentPage::tr("The channel number to control."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Setpoint output channel"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the controller address to change the setpoint on.  This is normally displayed in hexadecimal both on the controller and here."));
    }

    {
        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Controllers"),
                                                     value, "Controllers", parent);
        table->setArrayMode();
        result.append(table);

        table->addIntegerSpinBox(AcquisitionComponentPage::tr("Address"), "Controllers/%1/Address");
        table->setLastDefaults(0x31, Variant::Root(0x31));
        table->setLastIntegerSpinBox(1, 1023, NumberFormat("0xFF"), false);
        table->setLastToolTip(AcquisitionComponentPage::tr("The controller address."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Controller address"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "The controller address to declare.  This is normally displayed in hexadecimal both on the controller and here."));

        table->addBoolean(AcquisitionComponentPage::tr("Manual"), "Controllers/%1/ManualMode");
        table->setLastToolTip(
                AcquisitionComponentPage::tr("Operate the controller in \"manual\" model."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Manual mode"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This enables \"manual\" mode for the controller.  This effectively converts the controller into a simple analog output by disabling all PID control based on its current input."));

        table->addDoubleSpinBox(AcquisitionComponentPage::tr("Initialize"),
                                "Controllers/%1/Setpoint");
        table->setLastDefaults(AcquisitionComponentPage::tr("Unchanged"));
        table->setLastDoubleSpinBox(1, -100, 100, 1);
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The output value set on system initialization."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Initial setpoint output value"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the initial value of the controller setpoint set when the acquisition system starts up."));

        table->addDoubleSpinBox(AcquisitionComponentPage::tr("Shutdown"),
                                "Controllers/%1/Shutdown");
        table->setLastDefaults(AcquisitionComponentPage::tr("Unchanged"));
        table->setLastDoubleSpinBox(1, -100, 100, 1);
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The output value set on system shutdown."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Shutdown setpoint output value"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the value that the controller setpoint is set to immediately before system shutdown."));

        table->addDoubleSpinBox(AcquisitionComponentPage::tr("Bypass"), "Controllers/%1/Bypass");
        table->setLastDefaults(AcquisitionComponentPage::tr("Unchanged"));
        table->setLastDoubleSpinBox(1, -100, 100, 1);
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The output value set on system bypass."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Bypass setpoint output value"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the value that the controller setpoint is set to when the instrument enters a bypass state."));

        table->addDoubleSpinBox(AcquisitionComponentPage::tr("Un-bypass"),
                                "Controllers/%1/UnBypass");
        table->setLastDefaults(AcquisitionComponentPage::tr("Unchanged"));
        table->setLastDoubleSpinBox(1, -100, 100, 1);
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The output value set on system bypass release."));
        table->setLastStatusTip(
                AcquisitionComponentPage::tr("Bypass release setpoint output value"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the value that the controller setpoint is set to when the instrument leaves a bypass state."));

        table->addIntegerSpinBox(AcquisitionComponentPage::tr("Digits"),
                                 "Controllers/%1/DecimalDigits");
        table->setLastDefaults(AcquisitionComponentPage::tr("Unchanged"));
        table->setLastIntegerSpinBox(0, 3);
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The number digits after the decimal point."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Decimal digits"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This controls the number of digits after the decimal point the controller is changed to on startup."));
    }

    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                                    parent);
        result.append(page);

        page->addDoubleSpinBox(AcquisitionComponentPage::tr("Poll interval (s):"), "PollInterval");
        page->setLastDoubleSpinBox(1, 0.1, 9999, 0.1);
        page->setLastUnassigned(1.0);
        page->setLastToolTip(AcquisitionComponentPage::tr("The polling interval in seconds."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Polling interval"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the delay between instrument data polling.  This determines how frequently the data values are updated."));

        page->addDoubleSpinBox(AcquisitionComponentPage::tr("Provisional timeout (s):"),
                               "ProvisionalTimeout");
        page->setLastDoubleSpinBox(0, 0, 9999, 1);
        page->setLastUnassigned(120.0);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The maximum time to wait for an implied controller before removing it."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Provisional timeout"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the maximum time to wait for a provisionally created implied controller before giving up and removing it."));

        page->addIntegerSpinBox(AcquisitionComponentPage::tr("Maximum command retries:"),
                                "RetryTimes");
        page->setLastIntegerSpinBox(0, 9999);
        page->setLastUnassigned((qint64) 3);
        page->setLastExplicitUndefined(true);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The maximum number of times to retry a command before reporting failure."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Command retries"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the maximum number of times to retry a command before it is considered a failure (resulting in lost communications, etc)."));

        page->addStretch();
    }

    return result;
}

static QList<
        AcquisitionComponentPage *> create_acquire_magee_aethalometer33(const Variant::Read &value,
                                                                        QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;

    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Calibration"), value,
                                               parent);
    result.append(page);

    page->addCalibration(AcquisitionComponentPage::tr("Total flow calibration"),
                         "TotalFlowCalibration");
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The calibration polynomial applied to the total flow rate."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Total flow calibration"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This is calibration polynomial applied to the combined total flow before calculating the separated spot two flow."));

    page->addCalibration(AcquisitionComponentPage::tr("Spot one flow calibration"),
                         "Spot1FlowCalibration");
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The calibration polynomial applied to the flow through spot one."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Spot one flow calibration"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This is calibration polynomial applied to the high flow spot."));

    page->addCalibration(AcquisitionComponentPage::tr("Spot two flow calibration"),
                         "Spot2FlowCalibration");
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The calibration polynomial applied to the flow through spot two."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Spot one flow calibration"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This is calibration polynomial applied to the low flow spot."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Spot one area (mm²):"), "Area/#0");
    page->setLastDoubleSpinBox(2, 0.1, 99.99, 0.01);
    page->setLastExplicitUndefined(true);
    page->setLastUnassigned(78.5);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The area though the first, high flow, spot in mm²."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Spot area"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This is the area of the first (high flow) spot in mm²."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Spot two area (mm²):"), "Area/#1");
    page->setLastDoubleSpinBox(2, 0.1, 99.99, 0.01);
    page->setLastExplicitUndefined(true);
    page->setLastUnassigned(78.5);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The area though the second, low flow, spot in mm²."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Spot area"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This is the area of the second (low flow) spot in mm²."));

    page->addStretch();

    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Parameters"), value,
                                               parent);
    result.append(page);

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Leakage factor (\xCE\xB6):"), "Leakage");
    page->setLastDoubleSpinBox(3, 0.000, 0.999, 0.01);
    page->setLastUnassigned(0.0);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The leakage factor set on the instrument on start up and used in the correction."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Leakage factor"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This controls the leakage factor set on the instrument and used in the correction algorithm."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Weingartner constant (C):"),
                           "WeingartnerConstant");
    page->setLastDoubleSpinBox(3, 0.001, 0.999, 0.01);
    page->setLastUnassigned(1.57);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The Weingartner constant set on the instrument on start up and used in the correction."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Leakage factor"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This controls the Weingartner constant set on the instrument and used in the correction algorithm."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Lower attenuation threshold:"), "ATNf1");
    page->setLastDoubleSpinBox(1, 0.1, 999.9, 0.1);
    page->setLastUnassigned(10.0);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The lower attenuation threshold set on the instrument on start up and used in the correction."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Lower attenuation threshold"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This controls the lower attenuation threshold set on the instrument and used in the correction algorithm.  This determines the start of data used in the initial FVRF calculation."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Upper attenuation threshold:"), "ATNf2");
    page->setLastDoubleSpinBox(1, 0.1, 999.9, 0.1);
    page->setLastUnassigned(30.0);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The upper attenuation threshold set on the instrument on start up and used in the correction."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Upper attenuation threshold"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This controls the upper attenuation threshold set on the instrument and used in the correction algorithm.  This determines the end of data used in the initial FVRF calculation and when the newly calculated constant beings to take effect."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Smallest valid correction constants:"),
                           "kMin");
    page->setLastDoubleSpinBox(6, -0.1, 0.1, 0.1);
    page->setLastUnassigned(-0.005);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The smallest accepted correction constant before it is clipped to the minimum."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Correction constant minimum"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This controls the minimum correction constant set on the instrument and used in the algorithm.  This determines the lower limit of the constant, below which it is forced to be equal to the limit."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Largest valid correction constants:"),
                           "kMax");
    page->setLastDoubleSpinBox(6, -0.1, 0.1, 0.1);
    page->setLastUnassigned(0.015);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The largest accepted correction constant before it is clipped to the maximum."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Correction constant maximum"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This controls the maximum correction constant set on the instrument and used in the algorithm.  This determines the upper limit of the constant, above which it is forced to be equal to the limit."));

    page->addStretch();


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Optical"), value,
                                               parent);
    result.append(page);

    page->addDoubleSpinBoxArray(AcquisitionComponentPage::tr("Wavelengths"), "Wavelengths", 7);
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The wavelength of each sampling channel (nm)."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Channel wavelength"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "These are the wavelengths of each sampling channel, overriding the defaults."));

    page->setLastStretch();

    page->addDoubleSpinBoxArray(AcquisitionComponentPage::tr("Absorption efficiencies"),
                                "AbsorptionEfficiency", 7);
    page->setLastDoubleSpinBox(2, 0.01, 99.99, 0.1);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The absorption efficiency used to convert to EBC (m²/g)."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Absorption efficiency"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "These are the absorption efficiency factors used to calculated EBCs from absorption coefficients in m²/g."));

    page->setLastStretch();


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->addIntegerSpinBox(
            AcquisitionComponentPage::tr("Instrument calculation/averaging time(s):"), "TimeBase");
    page->setLastIntegerSpinBox(1, 99999);
    page->setLastExplicitUndefined(true);
    page->setLastUnassigned((qint64) 1);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The time base used by the instrument for calculation and averaging."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Time base"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This controls the time base set on the instrument that is used for its reporting calculation and averaging.  That is, this controls the update rate of data that the instrument provides."));

    page->addBoolean(AcquisitionComponentPage::tr("Enable realtime diagnostics"),
                     "RealtimeDiagnostics");
    page->setLastToolTip(AcquisitionComponentPage::tr("Generate realtime diagnostic data."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Realtime diagnostics"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "When enabled, this causes a large amount of diagnostic data to be generated (e.x. all spot intensities).  This greatly increased the volume of data generated and should only be used for troubleshooting."));

    page->addBoolean(AcquisitionComponentPage::tr("Set instrument clock"), "SetInstrumentTime");
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "Enable changing the instrument clock to match system time."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Set instrument clock"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "When enabled, the acquisition system will attempt to set the instrument clock if it differs by more than 30 seconds.  This sometimes causes the instrument to fault, requiring a manual power cycle before it will respond again."));

    page->addEnum(AcquisitionComponentPage::tr("Measurement timing handling:"), "UseMeasuredTime");
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The method used to determine the time between measurements for integration."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Measurement timing"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This controls the how the acquisition system determines the time between reports."));
    page->appendLastEnumEntry(AcquisitionComponentPage::tr("Measured but rounded"), "Rounded",
                              AcquisitionComponentPage::tr(
                                      "Round the time to the nearest multiple of the reported time base."));
    page->appendLastEnumEntry(AcquisitionComponentPage::tr("As reported"), "Reported",
                              AcquisitionComponentPage::tr(
                                      "Assume exactly the reported time base."));
    page->appendLastEnumEntry(AcquisitionComponentPage::tr("As measured"), "Measured",
                              AcquisitionComponentPage::tr(
                                      "Use the exact acquisition measured time between reports."));

    page->addBoolean(AcquisitionComponentPage::tr("Disable EBC zero missing check"),
                     "DisableEBCZeroCheck");
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "Disable the check for all EBCs being zero indicating a tape advance."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Disable the EBC zero check"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This disables the check that interprets all EBCs being exactly zero to indicate missing values and a tape advance.  When enabled, all EBCs being exactly zero will be interpreted as-is."));

    page->addBoolean(AcquisitionComponentPage::tr("Enable strict parsing"), "StrictMode");
    page->setLastToolTip(AcquisitionComponentPage::tr("Enable strict mode parsing."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Strict parsing"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This enables additional checks during data parsing."));

    page->addStretch();

    return result;
}

static QList<
        AcquisitionComponentPage *> create_acquire_magee_aethalometer162131(const Variant::Read &value,
                                                                            QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;

    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Calibration"), value,
                                               parent);
    result.append(page);

    page->addCalibration(AcquisitionComponentPage::tr("Flow calibration"), "FlowCalibration");
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The calibration polynomial applied to the flow rate."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Fow calibration"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This is calibration polynomial applied to the flow rate as reported by the instrument."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Spot area (mm²):"), "Area");
    page->setLastDoubleSpinBox(2, 0.1, 99.99, 0.01);
    page->setLastUnassigned(50.0);
    page->setLastToolTip(AcquisitionComponentPage::tr("The area of the sampling spot in mm²."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Spot area"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This is the area of the sampling spot in mm²."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Mean ratio:"), "MeanRatio");
    page->setLastDoubleSpinBox(2, 0.01, 1.00, 0.01);
    page->setLastUnassigned(1.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The instrument mean ratio multiplication factor."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Mean ratio"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This is the mean ratio setting on the instrument."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Sample temperature:"),
                           "SampleTemperature");
    page->setLastDoubleSpinBox(1, -99.99, 99.99, 1.0);
    page->setLastUnassigned(20.0);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The instrument sample temperature setting in \xC2\xB0\x43."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Sample temperature"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This is the sample temperature setting on the instrument in \xC2\xB0\x43."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Sample pressure:"), "SamplePressure");
    page->setLastDoubleSpinBox(1, 0.1, 1100.0, 1.0);
    page->setLastUnassigned(1013.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The instrument sample pressure setting in hPa."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Sample pressure"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This is the sample pressure setting on the instrument in hPa."));

    page->addStretch();


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Optical"), value,
                                               parent);
    result.append(page);

    page->addDoubleSpinBoxArray(AcquisitionComponentPage::tr("Wavelengths"), "Wavelengths", 7);
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The wavelength of each sampling channel (nm)."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Channel wavelength"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "These are the wavelengths of each sampling channel, overriding the defaults."));

    page->setLastStretch();

    page->addDoubleSpinBoxArray(AcquisitionComponentPage::tr("Absorption efficiencies"),
                                "AbsorptionEfficiency", 7);
    page->setLastDoubleSpinBox(2, 0.01, 99.99, 0.1);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The absorption efficiency used to convert to EBC (m²/g)."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Absorption efficiency"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "These are the absorption efficiency factors used to calculated EBCs from absorption coefficients in m²/g."));

    page->setLastStretch();


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Report interval (s):"), "ReportInterval");
    page->setLastDoubleSpinBox(0, 1, 86400, 1);
    page->setLastUnassigned(300.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The expected reporting interval in seconds."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Report interval"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the expected reporting interval.  This is used to determine how long to wait for responses."));

    page->addBoolean(AcquisitionComponentPage::tr("Ignore reported bypass fraction"),
                     "IgnoreFraction");
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "When enabled, ignore the reported bypass fraction and assume all data was sampled."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Ignore bypass fraction"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "Then this setting is enabled, the acquisition system will disregard the reported bypass fraction.  This causes it to assume that the instrument was sampling the whole time even if it reports a bypass fraction less than one."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Spot change detection threshold:"),
                           "FilterChangeDetectThreshold");
    page->setLastDoubleSpinBox(2, 0.01, 1.0, 0.01);
    page->setLastUnassigned(0.1);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The minimum change in transmittance required to detect a spot/filter change."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Spot transmittance change threshold"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the minimum jump in transmittance required for the acquisition system to determine that the spot or filter has been changed by the instrument."));

    page->addEnum(AcquisitionComponentPage::tr("Reported EBC units:"), "BCUnits");
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The handling mode used for the reported EBC values."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("EBC unit handling"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This controls how the acquisition system handles the units of the instrument reported EBCs."));
    page->appendLastEnumEntry(AcquisitionComponentPage::tr("Automatic"),
                              QStringList("Automatic") << "Auto", AcquisitionComponentPage::tr(
                    "Attempt to automatically determine the units by comparing them against the calculated values."));
    page->appendLastEnumEntry(AcquisitionComponentPage::tr("\xCE\xBCg/m\xC2\xB3"),
                              QStringList("ug") << "u" << "ugmg" << "ug/m3",
                              AcquisitionComponentPage::tr(
                                      "Always assume the instrument is reporting in \xCE\xBCg/m\xC2\xB3."));
    page->appendLastEnumEntry(AcquisitionComponentPage::tr("ng/m\xC2\xB3"),
                              QStringList("ng") << "n" << "ngmg" << "ng/m3",
                              AcquisitionComponentPage::tr(
                                      "Always assume the instrument is reporting in ng/m\xC2\xB3."));

    page->addBoolean(AcquisitionComponentPage::tr("Enable strict parsing"), "StrictMode");
    page->setLastToolTip(AcquisitionComponentPage::tr("Enable strict mode parsing."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Strict parsing"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This enables additional checks during data parsing."));

    page->addStretch();

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_magee_tca08(const Variant::Read &value,
                                                                    QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;

    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Poll interval (s):"), "PollInterval");
    page->setLastDoubleSpinBox(1, 0.1, 9999, 0.1);
    page->setLastUnassigned(0.5);
    page->setLastToolTip(AcquisitionComponentPage::tr("The polling interval in seconds."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Polling interval"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the delay between instrument data polling.  This determines how frequently the data values are updated."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Report interval (s):"), "ReportInterval");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(3600.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The expected reporting interval in seconds."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Report interval"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the expected reporting interval.  This is used to determine how long to wait for data to update."));

    page->addBoolean(AcquisitionComponentPage::tr("Enable strict parsing"), "StrictMode");
    page->setLastToolTip(AcquisitionComponentPage::tr("Enable strict mode parsing."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Strict parsing"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This enables additional checks during data parsing."));

    page->addStretch();

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_pms_lasair(const Variant::Read &value,
                                                                   QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;

    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Report interval (s):"), "ReportInterval");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(6.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The expected reporting interval in seconds."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Report interval"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the expected reporting interval.  This is used to determine how long to wait for responses."));

    page->addBoolean(AcquisitionComponentPage::tr("Enable strict parsing"), "StrictMode");
    page->setLastToolTip(AcquisitionComponentPage::tr("Enable strict mode parsing."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Strict parsing"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This enables additional checks during data parsing."));

    page->addSingleLineString(AcquisitionComponentPage::tr("Base model number:"), "Model");
    page->setLastUnassigned(AcquisitionComponentPage::tr("Automatically determined"));
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The base model number used to determine the expected data."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Parsing model number"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This set the model number used to parse data, which determines the sizes of the bins."));

    page->addDoubleSpinBoxArray(AcquisitionComponentPage::tr("Central diameter (\xCE\xBCm)"),
                                "Diameter", 8);
    page->setLastDoubleSpinBox(3, 0.001, 100, 0.1);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The central diameter of the bin in \xCE\xBCm."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Bin diameter"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "These are the center bin diameters in \xCE\xBCm, overriding the model specific values."));

    page->setLastStretch();

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_purpleair_pa2(const Variant::Read &value,
                                                                      QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;

    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Poll interval (s):"), "PollInterval");
    page->setLastDoubleSpinBox(1, 0.1, 9999, 0.1);
    page->setLastUnassigned(0.5);
    page->setLastToolTip(AcquisitionComponentPage::tr("The polling interval in seconds."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Polling interval"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the delay between instrument data polling.  This determines how frequently the data values are updated."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Report interval (s):"), "ReportInterval");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(120.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The expected reporting interval in seconds."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Report interval"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the expected reporting interval.  This is used to determine how long to wait for data to update."));

    page->addStretch();

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_rmy_wind86xxx(const Variant::Read &value,
                                                                      QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;

    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Report interval (s):"), "ReportInterval");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(1.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The expected reporting interval in seconds."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Report interval"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the expected reporting interval.  This is used to determine how long to wait for responses."));

    page->addBoolean(AcquisitionComponentPage::tr("Enable strict parsing"), "StrictMode");
    page->setLastToolTip(AcquisitionComponentPage::tr("Enable strict mode parsing."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Strict parsing"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This enables additional checks during data parsing."));

    page->addSingleLineString(AcquisitionComponentPage::tr("Instrument address:"), "Address");
    page->setLastUnassigned(AcquisitionComponentPage::tr("Any"));
    page->setLastToolTip(AcquisitionComponentPage::tr("The address code to require."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Instrument address"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the required address code for data to be accepted."));

    page->setLastStretch();

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_rr_neph903(const Variant::Read &value,
                                                                   QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;
    Variant::Write meta = Variant::Write::empty();


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Measurement wavelength (nm):"),
                           "Wavelength");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(530.0);
    page->setLastToolTip(AcquisitionComponentPage::tr("The wavelength of the light source in nm."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Wavelength"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This allows the sampling wavelength in nm to be overridden."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Report interval (s):"), "ReportInterval");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(1.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The expected reporting interval in seconds."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Report interval"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the expected reporting interval.  This is used to determine how long to wait for responses."));

    page->addBoolean(AcquisitionComponentPage::tr("Output missing calibrator values"),
                     "CalibratorFillMissing");
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "Output explicitly missing values during calibrator engagement."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Fill calibrator values"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This causes explicitly missing values to be output when the calibrator is engaged.  This disrupts native time resolution data but is required for continous "));

    page->addBoolean(AcquisitionComponentPage::tr("Enable strict parsing"), "StrictMode");
    page->setLastToolTip(AcquisitionComponentPage::tr("Enable strict mode parsing."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Strict parsing"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This enables additional checks during data parsing."));

    page->beginBox(AcquisitionComponentPage::tr("Relay control"));

    page->addIntegerSpinBox(AcquisitionComponentPage::tr("Zero:"), "Zero");
    page->setLastIntegerSpinBox(1, 2);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The relay to engage during zero air measurement."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Zero relay"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the relay number to engage when sampling zero air."));

    page->addIntegerSpinBox(AcquisitionComponentPage::tr("Span gas:"), "Spancheck");
    page->setLastIntegerSpinBox(1, 2);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The relay to engage during spancheck gas measurement."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Spancheck relay"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the relay number to engage when sampling the gas during a spancheck."));

    page->endBox();

    page->beginBox(AcquisitionComponentPage::tr("Zero parameters"));

    meta.setEmpty();
    meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("62 seconds");
    page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Flush duration:"), "Zero/Flush",
                                   meta);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "This sets the amount of time to flush before and after a zero measurement."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Zero flush time"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the amount of time to discard data before and after a zero measurement."));

    meta.setEmpty();
    meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("5 minutes");
    page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Sample duration:"), "Zero/Sample",
                                   meta);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "This sets the amount of time to sample and average zero air."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Zero sample time"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the amount of time to sample and average air during a zero measurement."));

    page->endBox();

    page->addStretch();

    result.append(createSpancheckPage(value, SpancheckFlags_Default, parent));

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_rr_psap1w(const Variant::Read &value,
                                                                  QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    Variant::Write meta = Variant::Write::empty();
    AcquisitionComponentPageGeneric *page;

    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Calibration"), value,
                                               parent);
    result.append(page);

    page->addCalibration(AcquisitionComponentPage::tr("Flow calibration"), "FlowCalibration");
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The calibration polynomial applied to the flow rate."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Fow calibration"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This is calibration polynomial applied to the flow rate as reported by the instrument."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Spot area (mm²):"), "Area");
    page->setLastDoubleSpinBox(2, 0.1, 99.99, 0.01);
    page->setLastUnassigned(17.83);
    page->setLastToolTip(AcquisitionComponentPage::tr("The area of the sampling spot in mm²."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Spot area"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This is the area of the sampling spot in mm²."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Measurement wavelength (nm):"),
                           "Wavelength");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(574.0);
    page->setLastToolTip(AcquisitionComponentPage::tr("The wavelength of the light source in nm."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Wavelength"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This allows the sampling wavelength in nm to be overridden."));

    page->setLastStretch();


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Filter"), value,
                                               parent);
    result.append(page);

    page->beginBox(AcquisitionComponentPage::tr("Filter change auto-detection"));

    page->addBoolean(AcquisitionComponentPage::tr("Enable start of filter detection"),
                     "Autodetect/Start/Enable");
    page->setLastUnassigned(true);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("Enable start of filter change auto-detection."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Filter start auto-detection"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This enables the automatic start of filter change detection.  That is, this controls if a filter change is initiated automatically (not the start of the filter itself)."));

    page->beginBox(AcquisitionComponentPage::tr("Filter change end auto-detection"));

    page->addBoolean(AcquisitionComponentPage::tr("Enable end of filter detection"),
                     "Autodetect/End/Enable");
    page->setLastUnassigned(true);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("Enable end of filter change auto-detection."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Filter end auto-detection"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This enables the automatic end of filter change detection.  That is, this controls if a filter change is completed automatically (not the end of the filter itself)."));

    meta.setEmpty();
    meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr(
            "A 60 second boxcar, requiring at least thirty second of data, with a maximum RSD of 0.02 and a spike band of 2.0");
    meta["*hEditor/EnableStability"] = true;
    meta["*hEditor/EnableSpike"] = true;
    meta["*hEditor/DefaultSmoother/Type"] = "FixedTime";
    meta["*hEditor/DefaultSmoother/Time"] = 60.0;
    meta["*hEditor/DefaultSmoother/MinimumTime"] = 30.0;
    meta["*hEditor/DefaultSmoother/RSD"] = 0.02;
    meta["*hEditor/DefaultSmoother/Band"] = 2.0;
    page->addSmoother(AcquisitionComponentPage::tr("Detection smoother:"), "Filter/Smoother", meta);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "This sets smoother used to determine the start and end of the change."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Filter parameter smoother"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets smoother used to for spike and stability detection for auto-detection."));

    page->endBox();

    page->beginBox(AcquisitionComponentPage::tr("Parameters"));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Normalization acceptance band:"),
                           "Filter/VerifyNormalizationBand");
    page->setLastDoubleSpinBox(3, 0.001, 99.999, 0.1);
    page->setLastUnassigned(0.3);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The scaling band for reference intensities for resuming a saved normalization."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Normalization acceptance band"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the relative scaling band of reference intensities relative to the saved normalization before accepting them.  For example, 0.9 sets the band as original/(1+0.9) to original*(1+0.9)."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("White filter confirmation band:"),
                           "Filter/VerifyWhiteBand");
    page->setLastDoubleSpinBox(3, 0.001, 99.999, 0.1);
    page->setLastUnassigned(0.9);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The scaling band for normalized spot intensities relative to a white filter."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("White filter band"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the relative scaling band with respect to a white filter that all normalized must fall within or a diagnostic message and flagging are generated.  For example, 0.9 sets the band as white/(1+0.9) to white*(1+0.9)."));

    page->endBox();

    page->addStretch();


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    meta.setEmpty();
    meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("2 hours");
    page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Filter resume timeout:"),
                                   "Filter/ResumeTimeout", meta);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "This set the maximum amount of downtime allowed before rejecting the saved normalization."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Spot resume timeout"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the maximum amount of downtime allowed before the system will reject any saved normalization."));

    page->addBoolean(AcquisitionComponentPage::tr("Enable normalization recovery"),
                     "Filter/EnableNormalizationRecovery");
    page->setLastUnassigned(true);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "Attempt to reconstruct the normalization from the reported transmittances."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Normalization recovery"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "When enabled, this causes the system to attempt to reconstruct the normalization based on the current reported transmittances, when a calculated one is not available."));

    page->addBoolean(AcquisitionComponentPage::tr("Use measured time between records"),
                     "UseMeasuredTime");
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "Use the time measured between records instead of assuming 1 Hz."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Use measured report time"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This enables causes the acquisition system to use the measured time between reports instead of assuming 1 Hz when calculating count rates."));

    page->addBoolean(AcquisitionComponentPage::tr("Enable strict parsing"), "StrictMode");
    page->setLastUnassigned(true);
    page->setLastToolTip(AcquisitionComponentPage::tr("Enable strict mode parsing."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Strict parsing"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This enables additional checks during data parsing."));

    page->addStretch();

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_rr_psap3w(const Variant::Read &value,
                                                                  QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    Variant::Write meta = Variant::Write::empty();
    AcquisitionComponentPageGeneric *page;

    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Calibration"), value,
                                               parent);
    result.append(page);

    page->addCalibration(AcquisitionComponentPage::tr("Flow calibration"), "FlowCalibration");
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The calibration polynomial applied to the flow rate."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Fow calibration"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This is calibration polynomial applied to the flow rate as reported by the instrument."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Spot area (mm²):"), "Area");
    page->setLastDoubleSpinBox(2, 0.1, 99.99, 0.01);
    page->setLastUnassigned(17.81);
    page->setLastToolTip(AcquisitionComponentPage::tr("The area of the sampling spot in mm²."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Spot area"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This is the area of the sampling spot in mm²."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Instrument area setting (mm²):"),
                           "InstrumentArea");
    page->setLastDoubleSpinBox(2, 0.1, 99.99, 0.01);
    page->setLastUnassigned(17.81);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The area of the sampling spot in mm² as set on the instrument."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Instrument spot area"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This is the area of the sampling spot in mm² as it is set on the instrument."));

    page->setLastStretch();


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Filter"), value,
                                               parent);
    result.append(page);

    page->beginBox(AcquisitionComponentPage::tr("Filter change auto-detection"));

    page->addBoolean(AcquisitionComponentPage::tr("Enable start of filter detection"),
                     "Autodetect/Start/Enable");
    page->setLastUnassigned(true);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("Enable start of filter change auto-detection."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Filter start auto-detection"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This enables the automatic start of filter change detection.  That is, this controls if a filter change is initiated automatically (not the start of the filter itself)."));

    page->beginBox(AcquisitionComponentPage::tr("Filter change end auto-detection"));

    page->addBoolean(AcquisitionComponentPage::tr("Enable end of filter detection"),
                     "Autodetect/End/Enable");
    page->setLastUnassigned(true);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("Enable end of filter change auto-detection."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Filter end auto-detection"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This enables the automatic end of filter change detection.  That is, this controls if a filter change is completed automatically (not the end of the filter itself)."));

    meta.setEmpty();
    meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr(
            "A 60 second boxcar, requiring at least thirty second of data, with a maximum RSD of 0.02 and a spike band of 2.0");
    meta["*hEditor/EnableStability"] = true;
    meta["*hEditor/EnableSpike"] = true;
    meta["*hEditor/DefaultSmoother/Type"] = "FixedTime";
    meta["*hEditor/DefaultSmoother/Time"] = 60.0;
    meta["*hEditor/DefaultSmoother/MinimumTime"] = 30.0;
    meta["*hEditor/DefaultSmoother/RSD"] = 0.02;
    meta["*hEditor/DefaultSmoother/Band"] = 2.0;
    page->addSmoother(AcquisitionComponentPage::tr("Detection smoother:"), "Filter/Smoother", meta);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "This sets smoother used to determine the start and end of the change."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Filter parameter smoother"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets smoother used to for spike and stability detection for auto-detection."));

    page->endBox();

    page->beginBox(AcquisitionComponentPage::tr("Parameters"));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Normalization acceptance band:"),
                           "Filter/VerifyNormalizationBand");
    page->setLastDoubleSpinBox(3, 0.001, 99.999, 0.1);
    page->setLastUnassigned(0.3);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The scaling band for reference intensities for resuming a saved normalization."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Normalization acceptance band"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the relative scaling band of reference intensities relative to the saved normalization before accepting them.  For example, 0.9 sets the band as original/(1+0.9) to original*(1+0.9)."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("White filter confirmation band:"),
                           "Filter/VerifyWhiteBand");
    page->setLastDoubleSpinBox(3, 0.001, 99.999, 0.1);
    page->setLastUnassigned(0.9);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The scaling band for normalized spot intensities relative to a white filter."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("White filter band"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the relative scaling band with respect to a white filter that all normalized must fall within or a diagnostic message and flagging are generated.  For example, 0.9 sets the band as white/(1+0.9) to white*(1+0.9)."));

    page->endBox();

    page->addStretch();


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Calculation"), value,
                                               parent);
    result.append(page);

    page->beginBox(AcquisitionComponentPage::tr("Absorption"));

    page->addBoolean(AcquisitionComponentPage::tr("Recovery from reported"),
                     "Absorption/EnableRecovery");
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "Attempt absorption recovery from the reported ones when they cannot be calculated."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Absorption recovery"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This enables an attempt at reconstructing the absorption coefficients from the instrument reported ones when the full calculation is not possible."));

    page->addBoolean(AcquisitionComponentPage::tr("Use instrument calculation"),
                     "Absorption/UseReported");
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "Use the reported absorption coefficients, instead of calculating them from the intensities."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Use instrument absorptions"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This causes the acquisition system to use the instrument reported absorption coefficients instead of attempting to calculate them from the intensities."));

    page->endBox();

    page->beginBox(AcquisitionComponentPage::tr("Weiss correction"));

    page->addBoolean(AcquisitionComponentPage::tr("Reverse from reported"), "Weiss/Reverse");
    page->setLastUnassigned(true);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "Back out the Weiss correction when using instrument reported absorption coefficients."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Weiss back out"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This option controls if the acquisition system will attempt to back out the Weiss correction from the reported absorption coefficients when they are in use."));

    page->addBoolean(AcquisitionComponentPage::tr("Use instrument coefficients"),
                     "Weiss/UseReported");
    page->setLastUnassigned(true);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "Use the instrument reported coefficients when required."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Use instrument Weiss coefficients"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This option controls if the instrument reported Weiss coefficients are used, when available."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("A:"), "Weiss/A");
    page->setLastDoubleSpinBox(3, -99.999, 99.999, 0.1);
    page->setLastUnassigned(0.814);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The \"A\" constant as it is set on instrument."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Weiss \"A\" constant"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the Weiss \"A\" (offset) constant as it is set on the instrument."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("B:"), "Weiss/B");
    page->setLastDoubleSpinBox(3, -99.999, 99.999, 0.1);
    page->setLastUnassigned(1.237);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The \"B\" constant as it is set on instrument."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Weiss \"B\" constant"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the Weiss \"A\" (scale) constant as it is set on the instrument."));

    page->endBox();

    page->addStretch();


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->beginBox("Sample wavelengths");

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Blue (nm):"), "Wavelength/B");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(467.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The wavelength of the blue sampling channel in nm."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Blue wavelength"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This allows the blue sampling wavelength in nm to be overridden."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Green (nm):"), "Wavelength/G");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(530.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The wavelength of the green sampling channel in nm."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Green wavelength"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This allows the green sampling wavelength in nm to be overridden."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Red (nm):"), "Wavelength/R");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(660.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The wavelength of the red sampling channel in nm."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Red wavelength"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This allows the red sampling wavelength in nm to be overridden."));

    page->endBox();

    meta.setEmpty();
    meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("2 hours");
    page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Filter resume timeout:"),
                                   "Filter/ResumeTimeout", meta);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "This set the maximum amount of downtime allowed before rejecting the saved normalization."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Spot resume timeout"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the maximum amount of downtime allowed before the system will reject any saved normalization."));

    page->addBoolean(AcquisitionComponentPage::tr("Enable normalization recovery"),
                     "Filter/EnableNormalizationRecovery");
    page->setLastUnassigned(true);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "Attempt to reconstruct the normalization from the reported transmittances."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Normalization recovery"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "When enabled, this causes the system to attempt to reconstruct the normalization based on the current reported transmittances, when a calculated one is not available."));

    page->addBoolean(AcquisitionComponentPage::tr("Use measured time between records"),
                     "UseMeasuredTime");
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "Use the time measured between records instead of assuming 1 Hz."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Use measured report time"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This enables causes the acquisition system to use the measured time between reports instead of assuming 1 Hz when calculating count rates."));

    page->addBoolean(AcquisitionComponentPage::tr("Enable strict parsing"), "StrictMode");
    page->setLastUnassigned(true);
    page->setLastToolTip(AcquisitionComponentPage::tr("Enable strict mode parsing."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Strict parsing"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This enables additional checks during data parsing."));

    page->addStretch();

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_thermo_maap5012(const Variant::Read &value,
                                                                        QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    Variant::Write meta = Variant::Write::empty();
    AcquisitionComponentPageGeneric *page;

    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Calibration"), value,
                                               parent);
    result.append(page);

    page->addCalibration(AcquisitionComponentPage::tr("Flow calibration"), "FlowCalibration");
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The calibration polynomial applied to the flow rate."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Fow calibration"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This is calibration polynomial applied to the flow rate as reported by the instrument."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Spot area (mm²):"), "Area");
    page->setLastDoubleSpinBox(2, 0.1, 999.99, 0.1);
    page->setLastUnassigned(200.0);
    page->setLastToolTip(AcquisitionComponentPage::tr("The area of the sampling spot in mm²."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Spot area"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This is the area of the sampling spot in mm²."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Target flow rate (lpm):"), "TargetFlow");
    page->setLastDoubleSpinBox(3, 0.001, 99.999, 0.1);
    page->setLastUnassigned(1.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The target flow rate set on the instrument."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Target flow"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This control the target flow rate set on the instrument during startup.  Note that this is before any calibration applied to the flow."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Measurement wavelength (nm):"),
                           "Wavelength");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(670.0);
    page->setLastToolTip(AcquisitionComponentPage::tr("The wavelength of the light source in nm."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Wavelength"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This allows the sampling wavelength in nm to be overridden."));

    page->setLastStretch();


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Filter"), value,
                                               parent);
    result.append(page);

    page->beginBox(AcquisitionComponentPage::tr("Automatic spot advance"));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Transmittance:"),
                           "Spot/AdvanceTransmittance",
                           value["Spot/AdvanceTransmittance/R"].toDouble());
    page->setLastDoubleSpinBox(3, 0.001, 0.999, 0.1);
    page->setLastUnassigned(0.7);
    page->setLastExplicitUndefined();
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The minimum transmittance before an automatic spot advance is initiated."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Spot advance transmittance"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the threshold for the transmittance below which an automatic spot advance is attempted.  When set but undefined, the instrument setting is not altered."));

    page->addIntegerSpinBox(AcquisitionComponentPage::tr("Maximum length (hours):"),
                            "Spot/AdvanceHours");
    page->setLastIntegerSpinBox(1, 100);
    page->setLastUnassigned((qint64) 100);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The maximum number of hours to sample a spot before advancing."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Spot advance hours"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the maximum number of hours the instrument will sample on the same spot before advancing to the next one."));

    page->addIntegerSpinBox(AcquisitionComponentPage::tr("Hour of day (1-24):"),
                            "Spot/AdvanceAtHour");
    page->setLastIntegerSpinBox(1, 24);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The fixed hour of day (by the instrument clock) to advance the spot at."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Spot fixed time advance"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets a fixed hour of the day, as measured on the instrument clock, to advance the sampling spot at.  When undefined, the advance is disabled."));

    page->endBox();

    page->beginBox(AcquisitionComponentPage::tr("Filter change auto-detection"));

    page->addBoolean(AcquisitionComponentPage::tr("Enable detection"), "Autodetect/Enable");
    page->setLastUnassigned(false);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "Enable filter/spot change auto-detection, beyond the instrument reporting."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Filter auto-detection"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This controls if auto-detection is used in addition to the filter changing status flags.  This is normally only required if the status flags are incomplete or unavailable."));

    meta.setEmpty();
    meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr(
            "A 600 second boxcar, discarding the first 120 and requiring 120 minimum, with a stable RSD of 0.05 and a spike band of 1.5");
    meta["*hEditor/EnableStability"] = true;
    meta["*hEditor/EnableSpike"] = true;
    meta["*hEditor/DefaultSmoother/Type"] = "FixedTime";
    meta["*hEditor/DefaultSmoother/Time"] = 060.0;
    meta["*hEditor/DefaultSmoother/MinimumTime"] = 120.0;
    meta["*hEditor/DefaultSmoother/DiscardTime"] = 120.0;
    meta["*hEditor/DefaultSmoother/RSD"] = 0.05;
    meta["*hEditor/DefaultSmoother/Band"] = 1.5;
    page->addSmoother(AcquisitionComponentPage::tr("Detection smoother:"), "Filter/Smoother", meta);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "This sets smoother used to determine the start and end of the change."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Filter parameter smoother"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets smoother used to for spike and stability detection for auto-detection."));

    page->endBox();

    page->beginBox(AcquisitionComponentPage::tr("Parameters"));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Normalization acceptance band:"),
                           "Filter/VerifyNormalizationBand");
    page->setLastDoubleSpinBox(3, 0.001, 99.999, 0.1);
    page->setLastUnassigned(0.3);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The scaling band for reference intensities for resuming a saved normalization."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Normalization acceptance band"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the relative scaling band of reference intensities relative to the saved normalization before accepting them.  For example, 0.9 sets the band as original/(1+0.9) to original*(1+0.9)."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("White filter confirmation band:"),
                           "Filter/VerifyWhiteBand");
    page->setLastDoubleSpinBox(3, 0.001, 99.999, 0.1);
    page->setLastUnassigned(0.9);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The scaling band for normalized spot intensities relative to a white filter."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("White filter band"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the relative scaling band with respect to a white filter that all normalized must fall within or a diagnostic message and flagging are generated.  For example, 0.9 sets the band as white/(1+0.9) to white*(1+0.9)."));

    page->endBox();

    page->addStretch();


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->addBoolean(AcquisitionComponentPage::tr("Verify filter instrument ID"),
                     "Filter/CheckInstrumentID");
    page->setLastUnassigned(true);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "Verify the instrument identifier after startup before accepting an old filter."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Saved filter ID verification"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This controls if the saved filter parameters are checked against the current instrument ID (serial number, etc) before the filter is accepted after an acquisition system restart."));

    meta.setEmpty();
    meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("30 minutes");
    page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Filter resume timeout:"),
                                   "Filter/ResumeTimeout", meta);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "This set the maximum amount of downtime allowed before rejecting the saved normalization."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Spot resume timeout"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the maximum amount of downtime allowed before the system will reject any saved normalization."));

    page->addEnum(AcquisitionComponentPage::tr("Reported EBC units:"), "BCUnits");
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The handling mode used for the reported EBC values."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("EBC unit handling"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This controls how the acquisition system handles the units of the instrument reported EBCs."));
    page->appendLastEnumEntry(AcquisitionComponentPage::tr("Automatic"),
                              QStringList("Automatic") << "Auto", AcquisitionComponentPage::tr(
                    "Attempt to automatically determine the units by comparing them against the calculated values."));
    page->appendLastEnumEntry(AcquisitionComponentPage::tr("\xCE\xBCg/m\xC2\xB3"),
                              QStringList("ug") << "u" << "ugmg" << "ug/m3",
                              AcquisitionComponentPage::tr(
                                      "Always assume the instrument is reporting in \xCE\xBCg/m\xC2\xB3."));
    page->appendLastEnumEntry(AcquisitionComponentPage::tr("ng/m\xC2\xB3"),
                              QStringList("ng") << "n" << "ngmg" << "ng/m3",
                              AcquisitionComponentPage::tr(
                                      "Always assume the instrument is reporting in ng/m\xC2\xB3."));

    page->addIntegerSpinBox(AcquisitionComponentPage::tr("Address:"), "Address");
    page->setLastIntegerSpinBox(0, 9999);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The instrument address or undefined for any."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Instrument address"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This sets a the address of the instrument to control."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Report interval (s):"), "ReportInterval");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(120.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The expected reporting interval in seconds."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Report interval"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the expected reporting interval.  This controls how long the acquisition system will wait for records before reporting an error."));

    page->addStretch();

    return result;
}


static AcquisitionComponentPage *createOzone49SpancheckPage(const Variant::Read &value,
                                                            QWidget *parent = 0)
{
    AcquisitionComponentTabCompositePage *composite =
            new AcquisitionComponentTabCompositePage(AcquisitionComponentPage::tr("Spancheck"),
                                                     value, parent);

    {
        AcquisitionComponentGenericTable *page =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Levels"), value,
                                                     "Spancheck/Levels", composite);
        composite->addPage(page);
        page->setArrayMode();

        page->addIntegerSpinBox(AcquisitionComponentPage::tr("Ozonator setting"),
                                "Spancheck/Levels/%1");
        page->setLastIntegerSpinBox(1, 5, NumberFormat(1, 0), false);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The ozonator preset level used in the spancheck phase."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Ozonator preset"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the ozonator preset level used for the phase of the spancheck."));
    }

    {
        AcquisitionComponentPageGeneric *page;
        Variant::Write meta = Variant::Write::empty();

        page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Smoothing"), value,
                                                   composite);
        composite->addPage(page);

        page->beginBox(AcquisitionComponentPage::tr("Sample time"));

        meta.setEmpty();
        meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("12 minutes");
        page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Minimum sampling time:"),
                                       "Spancheck/MinimumSample", meta);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This set the minimum amount of time a level is sampled for."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Minimum sampling time"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the minimum amount of time that a level is sampled for, before even considering stability."));

        meta.setEmpty();
        meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("Unlimited");
        page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Maximum sampling time:"),
                                       "Spancheck/MaximumSample", meta);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This set the maximum amount of time a level is sampled for."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Maximum sampling time"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the maximum amount of time that a level is sampled for, before the stability indicator is ignored and the phase is ended anyway."));

        meta.setEmpty();
        meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("2 minutes");
        page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Flush duration:"),
                                       "Spancheck/Flush", meta);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The length of time that air is flushed through the instrument before sampling."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Air flush time"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the length of time that air is flushed through the instrument before any smoothing or sampling."));

        page->endBox();

        page->beginBox(AcquisitionComponentPage::tr("Measurement Smoothing"));

        meta.setEmpty();
        meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("Indefinite");
        meta["*hEditor/EnableStability"] = true;
        meta["*hEditor/DefaultSmoother/Type"] = "Forever";
        page->addSmoother(AcquisitionComponentPage::tr("Concentration:"),
                          "Spancheck/Smoothing/Concentration", meta);
        page->setLastToolTip(
                AcquisitionComponentPage::tr("Ozone concentration smoothing parameters."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Concentration smoothing"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the smoothing and stability checking for ozone concentration measurements."));

        page->addSmoother(AcquisitionComponentPage::tr("Temperature:"),
                          "Spancheck/Smoothing/Temperature", meta);
        page->setLastToolTip(AcquisitionComponentPage::tr("Temperature smoothing parameters."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Temperature smoothing"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the smoothing and stability checking for temperature measurements."));

        page->addSmoother(AcquisitionComponentPage::tr("Pressure:"), "Spancheck/Smoothing/Pressure",
                          meta);
        page->setLastToolTip(AcquisitionComponentPage::tr("Pressure smoothing parameters."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Temperature smoothing"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the smoothing and stability checking for pressure measurements."));

        page->endBox();

        page->addStretch();
    }

    return composite;
}

static QList<AcquisitionComponentPage *> create_acquire_thermo_ozone49(const Variant::Read &value,
                                                                       QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;
    Variant::Write meta = Variant::Write::empty();

    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Poll interval (s):"), "PollInterval");
    page->setLastDoubleSpinBox(1, 0.1, 9999, 0.1);
    page->setLastUnassigned(10.0);
    page->setLastToolTip(AcquisitionComponentPage::tr("The polling interval in seconds."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Polling interval"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the delay between instrument data polling.  This determines how frequently the data values are updated and what averaging time the instrument is set to on startup."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Command delay (s):"), "CommandDelay");
    page->setLastDoubleSpinBox(2, 0.01, 9999.99, 0.1);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The delay after receiving a response before issuing the next command."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Command delay"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the time after receiving a response before the acquisition system will issue another command.  Without this, some instruments can fail to respond to the new command entirely.  This is normally automatically set based on the instrument type."));

    page->addIntegerSpinBox(AcquisitionComponentPage::tr("Address:"), "Address");
    page->setLastIntegerSpinBox(0, 126);
    page->setLastToolTip(AcquisitionComponentPage::tr("The instrument address."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Instrument address"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets a the address of the instrument to control.  When not set or equal to zero no identifier is set and normally only some models of instruments with an address of zero will respond."));

    page->addStretch();


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Zero"), value, parent);
    result.append(page);

    page->beginBox(AcquisitionComponentPage::tr("Sample time"));

    meta.setEmpty();
    meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("12 minutes");
    page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Minimum sampling time:"),
                                   "Zero/MinimumSample", meta);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "This set the minimum amount of time zero air is sampled for."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Minimum sampling time"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the minimum amount of time that zero air is sampled for, before even considering stability."));

    meta.setEmpty();
    meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("Unlimited");
    page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Maximum sampling time:"),
                                   "Zero/MaximumSample", meta);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "This set the maximum amount of time zero air is sampled for."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Maximum sampling time"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the maximum amount of time that zero air is sampled for, before the stability indicator is ignored and the phase is ended anyway."));

    meta.setEmpty();
    meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("2 minutes");
    page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Flush duration:"), "Zero/Flush",
                                   meta);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The length of time that air is flushed through the instrument before sampling."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Air flush time"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the length of time that air is flushed through the instrument before any smoothing or sampling."));

    page->endBox();

    page->beginBox(AcquisitionComponentPage::tr("Measurement Smoothing"));

    meta.setEmpty();
    meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("Indefinite");
    meta["*hEditor/EnableStability"] = true;
    meta["*hEditor/DefaultSmoother/Type"] = "Forever";
    page->addSmoother(AcquisitionComponentPage::tr("Concentration:"),
                      "Zero/Smoothing/Concentration", meta);
    page->setLastToolTip(AcquisitionComponentPage::tr("Ozone concentration smoothing parameters."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Concentration smoothing"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the smoothing and stability checking for ozone concentration measurements."));

    page->addSmoother(AcquisitionComponentPage::tr("Temperature:"), "Zero/Smoothing/Temperature",
                      meta);
    page->setLastToolTip(AcquisitionComponentPage::tr("Temperature smoothing parameters."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Temperature smoothing"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the smoothing and stability checking for temperature measurements."));

    page->addSmoother(AcquisitionComponentPage::tr("Pressure:"), "Zero/Smoothing/Pressure", meta);
    page->setLastToolTip(AcquisitionComponentPage::tr("Pressure smoothing parameters."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Temperature smoothing"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the smoothing and stability checking for pressure measurements."));

    page->endBox();

    page->addStretch();

    result.append(createOzone49SpancheckPage(value, parent));

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_thermo_ozone49iq(const Variant::Read &value,
                                                                         QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;

    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Poll interval (s):"), "PollInterval");
    page->setLastDoubleSpinBox(1, 0.1, 9999, 0.1);
    page->setLastUnassigned(0.5);
    page->setLastToolTip(AcquisitionComponentPage::tr("The polling interval in seconds."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Polling interval"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the delay between instrument data polling.  This determines how frequently the data values are updated."));

    page->addIntegerSpinBox(AcquisitionComponentPage::tr("Address:"), "Address");
    page->setLastIntegerSpinBox(0x01, 0xFF);
    page->setLastToolTip(AcquisitionComponentPage::tr("The instrument address."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Instrument address"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets a the address of the instrument to control.  This is optional for TCP operation, but required for serial control."));

    page->addBoolean(AcquisitionComponentPage::tr("Use MODBUS over TCP"), "TCPFraming");
    page->setLastExplicitUndefined();
    page->setLastToolTip(AcquisitionComponentPage::tr("Enable or disable MODBUS over TCP mode."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("MODBUS TCP mode"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This enables or disables the use of MODBUS over TCP instead of MODBUS RTU over serial.  When not set, it is determined based on the interface type selected."));

    page->addStretch();

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_tsi_cpc302x(const Variant::Read &value,
                                                                    QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Sample flow rate (lpm):"), "Flow");
    page->setLastDoubleSpinBox(3, 0.001, 99.999, 0.1);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The sample flow rate used to convert the counts to a concentration."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Sample flow rate"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the sample flow rate in lpm that is used to convert the raw count rate into a number concentration.  When unavailable the instrument reported flow rate is used."));

    page->addBoolean(AcquisitionComponentPage::tr("Use measured time between records"),
                     "UseMeasuredTime");
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "Use the time measured between records instead of using the instrument reported rate."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Use measured report time"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This enables causes the acquisition system to use the measured time between reports instead of using the reported elapsed time calculating count rates."));

    page->addStretch();

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_tsi_cpc377x(const Variant::Read &value,
                                                                    QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Sample flow rate (lpm):"), "Flow");
    page->setLastDoubleSpinBox(3, 0.001, 99.999, 0.1);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The sample flow rate used to convert the counts to a concentration."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Sample flow rate"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the sample flow rate in lpm that is used to convert the raw count rate into a number concentration.  When unavailable the instrument reported flow rate is used."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Poll interval (s):"), "PollInterval");
    page->setLastDoubleSpinBox(1, 0.1, 9999, 0.1);
    page->setLastUnassigned(0.5);
    page->setLastToolTip(AcquisitionComponentPage::tr("The polling interval in seconds."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Polling interval"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the delay between instrument data polling.  This determines how frequently the data values are updated."));

    page->addBoolean(AcquisitionComponentPage::tr("Set instrument clock"), "SetInstrumentTime");
    page->setLastToolTip(AcquisitionComponentPage::tr("Set the instrument clock on startup."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Set instrument clock"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "When enabled, the acquisition system will attempt to synchronize the instrument's internal clock on startup.  Note that this causes some instruments to lock up and require a 30-minute power down before they will communicate again."));

    page->addStretch();

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_tsi_cpc3010(const Variant::Read &value,
                                                                    QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Sample flow rate (lpm):"), "Flow");
    page->setLastDoubleSpinBox(3, 0.001, 99.999, 0.1);
    page->setLastUnassigned(1.0);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The sample flow rate used to convert the counts to a concentration."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Sample flow rate"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the sample flow rate in lpm that is used to convert the raw count rate into a number concentration."));

    page->addBoolean(AcquisitionComponentPage::tr("Use measured time between records"),
                     "UseMeasuredTime");
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "Use the time measured between records instead of using the instrument reported rate."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Use measured report time"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This enables causes the acquisition system to use the measured time between reports instead of using the reported elapsed time calculating count rates."));

    page->addStretch();

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_tsi_cpc378x(const Variant::Read &value,
                                                                    QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Sample flow rate (lpm):"), "Flow");
    page->setLastDoubleSpinBox(4, 0.0001, 99.9999, 0.01);
    page->setLastUnassigned(0.12);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The sample flow rate used to convert the counts to a concentration."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Sample flow rate"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the sample flow rate in lpm that is used to convert the raw count rate into a number concentration.  When not set, the instrument reported flow rate is used."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Poll interval (s):"), "PollInterval");
    page->setLastDoubleSpinBox(1, 0.1, 9999, 0.1);
    page->setLastUnassigned(0.5);
    page->setLastToolTip(AcquisitionComponentPage::tr("The polling interval in seconds."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Polling interval"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the delay between instrument data polling.  This determines how frequently the data values are updated."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Sample time (s):"), "SampleTime");
    page->setLastDoubleSpinBox(1, 0.1, 3600, 1.0);
    page->setLastUnassigned(1.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The instrument sampling and averaging time."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Sample time"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the instrument sample time setting.  This is effectively the averaging time in use by the instrument."));

    page->addBoolean(AcquisitionComponentPage::tr("Enable strict parsing"), "StrictMode");
    page->setLastUnassigned(true);
    page->setLastToolTip(AcquisitionComponentPage::tr("Enable strict mode parsing."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Strict parsing"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This enables additional checks during data parsing."));

    page->addStretch();

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_tsi_mfm4xxx(const Variant::Read &value,
                                                                    QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Sample time (s):"), "SampleTime");
    page->setLastDoubleSpinBox(3, 0.001, 1.000, 1.0);
    page->setLastUnassigned(1.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The instrument sampling and averaging time."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Sample time"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the instrument sample time setting.  This is effectively the averaging time in use by the instrument."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Binary flow rate scale factor:"),
                           "BinaryFlowScale");
    page->setLastDoubleSpinBox(0, 1, 9999, 1.0);
    page->setLastUnassigned(100.0);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The scale factor used to convert binary responses to flow rates."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Binary response flow scale"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the scale factor used when receiving binary data to convert it back to flow rates.  This is not used in ASCII communications and can normally be set based on the instrument type automatically."));

    page->addBoolean(AcquisitionComponentPage::tr("Enable strict parsing"), "StrictMode");
    page->setLastUnassigned(true);
    page->setLastToolTip(AcquisitionComponentPage::tr("Enable strict mode parsing."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Strict parsing"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This enables additional checks during data parsing."));

    page->addStretch();

    return result;
}


static void createTSINeph3563OpticalCalibration(AcquisitionComponentPageGeneric *page,
                                                const QString &path)
{
    page->addIntegerSpinBox(AcquisitionComponentPage::tr("K1: PMT deat time (ps):"), path + "K1");
    page->setLastIntegerSpinBox(1, 65535);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The minimum time between PMT pulses, in picoseconds."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("K1"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This changes the minimum time between PMT pulses in picoseconds."));

    page->addDouble(
            AcquisitionComponentPage::tr("K2: Total scattering calibration (m\xE2\x81\xBB¹):"),
            path + "K2");
    page->setLastToolTip(
            AcquisitionComponentPage::tr("Total scatter calibration of the reference chopper."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("K2"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This changes the total scatter calibration of the reference chopper."));

    page->addDouble(
            AcquisitionComponentPage::tr("K3: Rayleigh scattering at STP (m\xE2\x81\xBB¹):"),
            path + "K3");
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The Rayleigh scattering at 273 K and 1013.25 hPa."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("K3"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This changes the Rayleigh scattering of the channel."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("K4: Backscatter calibration fraction:"),
                           path + "K4");
    page->setLastDoubleSpinBox(3, 0.0, 1.0, 0.001);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The backscatter calibration fraction from the total scattering."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("K4"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This changes the backscatter calibration fraction."));
}

static void createTSINeph3563SensorCalibration(AcquisitionComponentPageGeneric *page,
                                               const QString &path,
                                               const QString &units,
                                               double valueMax,
                                               int adcMax = 65535)
{
    page->addIntegerSpinBox(AcquisitionComponentPage::tr("Low ADC reading:"), path + "Low/ADC");
    page->setLastIntegerSpinBox(0, adcMax - 1);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The ADC reading for the lower calibration point."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Low ADC reading"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This changes the ADC reading for the lower calibration point."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Low value (%1):").arg(units),
                           path + "Low/Value");
    page->setLastDoubleSpinBox(1, 0.0, valueMax - 0.1, 0.1);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The value in physical units for the lower calibration point."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Low physical value"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This changes the value in physical units for the lower calibration point."));

    page->addIntegerSpinBox(AcquisitionComponentPage::tr("High ADC reading:"), path + "High/ADC");
    page->setLastIntegerSpinBox(1, adcMax);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The ADC reading for the upper calibration point."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("High ADC reading"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This changes the ADC reading for the upper calibration point."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("High value (%1):").arg(units),
                           path + "High/Value");
    page->setLastDoubleSpinBox(1, 0.1, valueMax, 0.1);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The value in physical units for the upper calibration point."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("High physical value"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This changes the value in physical units for the upper calibration point."));
}

static AcquisitionComponentPage *createTSINeph3563ParametersPage(const Variant::Read &value,
                                                                 QWidget *parent = 0)
{
    AcquisitionComponentTabCompositePage *composite =
            new AcquisitionComponentTabCompositePage(AcquisitionComponentPage::tr("Parameters"),
                                                     value, parent);
    {
        AcquisitionComponentPageGeneric *page;

        page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("General"), value,
                                                   composite);
        composite->addPage(page);

        page->addIntegerSpinBox(AcquisitionComponentPage::tr("SP: Lamp power (watts):"),
                                "Parameters/SP");
        page->setLastIntegerSpinBox(0, 150);
        page->setLastUnassigned((qint64) 75);
        page->setLastToolTip(AcquisitionComponentPage::tr("Lamp power setpoint in watts."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("SP"));
        page->setLastWhatsThis(
                AcquisitionComponentPage::tr("This sets the target lamp power in watts."));

        page->addIntegerSpinBox(AcquisitionComponentPage::tr("B: Blower fraction:"),
                                "Parameters/B");
        page->setLastIntegerSpinBox(0, 255);
        page->setLastUnassigned((qint64) 255);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "Blower power fraction from 0 (off) to 255 (full speed)."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("B"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This controls the blower power fraction  from 0 (off) to 255 (full speed)."));

        page->addBoolean(AcquisitionComponentPage::tr("SMB: Enable backscatter shutter"),
                         "Parameters/SMB");
        page->setLastUnassigned(true);
        page->setLastToolTip(AcquisitionComponentPage::tr("Enable the backscatter shutter."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("SMB"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This controls the if the backscatter shutter is enabled.  When disabled, the nephelometer only measures total scattering."));

        page->addBoolean(AcquisitionComponentPage::tr("V: Zero valve override"), "Parameters/V");
        page->setLastExplicitUndefined();
        page->setLastToolTip(
                AcquisitionComponentPage::tr("Override the automatic zero valve control."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("V"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This settings allows the automatic control of the zero valve to be overridden.  Enabling it forces the instrument to sample filtered air."));

        page->addBoolean(AcquisitionComponentPage::tr("H: Enable sample heater"), "Parameters/H");
        page->setLastExplicitUndefined();
        page->setLastToolTip(AcquisitionComponentPage::tr("Enable the sample heater."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("H"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This settings controls if the sample heater is enabled or disabled on initialization."));

        page->addIntegerSpinBox(AcquisitionComponentPage::tr("STA: Averaging time (s):"),
                                "Parameters/STA");
        page->setLastIntegerSpinBox(1, 9960);
        page->setLastUnassigned((qint64) 1);
        page->setLastToolTip(AcquisitionComponentPage::tr("Instrument averaging time in seconds."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("STA"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This controls the onboard instrument averaging time."));

        page->beginBox(AcquisitionComponentPage::tr("PMT Voltage"));

        page->addIntegerSpinBox(AcquisitionComponentPage::tr("SVB: Blue (V):"), "Parameters/SVB");
        page->setLastIntegerSpinBox(800, 1200);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The blue PMT voltage or undefined to leave it as-is."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("SVG"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This controls the voltage the blue PMT is set to on initialization."));

        page->addIntegerSpinBox(AcquisitionComponentPage::tr("SVG: Green (V):"), "Parameters/SVG");
        page->setLastIntegerSpinBox(800, 1200);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The green PMT voltage or undefined to leave it as-is."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("SVG"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This controls the voltage the green PMT is set to on initialization."));

        page->addIntegerSpinBox(AcquisitionComponentPage::tr("SVR: Red (V):"), "Parameters/SVR");
        page->setLastIntegerSpinBox(800, 1200);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The red PMT voltage or undefined to leave it as-is."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("SVR"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This controls the voltage the red PMT is set to on initialization."));

        page->endBox();

        page->addStretch();
    }

    {
        AcquisitionComponentPageGeneric *page;

        page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Zeroing"), value,
                                                   composite);
        composite->addPage(page);

        page->addIntegerSpinBox(AcquisitionComponentPage::tr("SMZ: Auto-zero mode:"),
                                "Parameters/SMZ");
        page->setLastIntegerSpinBox(0, 24);
        page->setLastUnassigned((qint64) 1);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "Auto-zero mode (0=manual, 1=single, 2-24=averaged)."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("SMZ"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This controls the auto-zero mode and zero averaging performed.  The number zero disables automatic zeroing, while 1-24 average that many zeros together to calcualte the final zero offset."));

        page->addIntegerSpinBox(AcquisitionComponentPage::tr("STB: Blank duration (s):"),
                                "Parameters/STB");
        page->setLastIntegerSpinBox(15, 999);
        page->setLastUnassigned((qint64) 62);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The time the instrument disregards data when entering or leaving zeros."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("STB"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This controls how long before and after a zero valve switch that data are disregarded."));

        page->addIntegerSpinBox(AcquisitionComponentPage::tr("STZ: Zero measurement time (s):"),
                                "Parameters/STB");
        page->setLastIntegerSpinBox(1, 9999);
        page->setLastUnassigned((qint64) 300);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The time the instrument measures zero air when performing the calculation."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("STZ"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This controls how long the instrument samples zero air before calculating the final offset."));

        page->addIntegerSpinBox(
                AcquisitionComponentPage::tr("STP: Time between instrument auto-zeros (s):"),
                "Parameters/STP");
        page->setLastIntegerSpinBox(10, 32767);
        page->setLastUnassigned((qint64) 32000);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The time between instrument initiated zero measurements."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("STP"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This controls how long after a zero before the instrument will initiate another one on its own, assuming SMZ is non-zero."));

        page->addStretch();
    }

    {
        AcquisitionComponentPageGeneric *page;

        page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Calibration"),
                                                   value, composite);
        composite->addPage(page);

        page->addSingleLineString(AcquisitionComponentPage::tr("SL: Calibration label:"),
                                  "Parameters/SL");
        page->setLastUnassigned(AcquisitionComponentPage::tr("Unchanged"));
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The label of the calibration stored on the instrument."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("SL"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This setting changes the instrument stored calibration label."));

        page->beginBox(AcquisitionComponentPage::tr("Blue"));
        createTSINeph3563OpticalCalibration(page, "Parameters/SKB/");
        page->endBox();

        page->beginBox(AcquisitionComponentPage::tr("Green"));
        createTSINeph3563OpticalCalibration(page, "Parameters/SKG/");
        page->endBox();

        page->beginBox(AcquisitionComponentPage::tr("Red"));
        createTSINeph3563OpticalCalibration(page, "Parameters/SKR/");
        page->endBox();

        page->addStretch();
    }

    {
        AcquisitionComponentPageGeneric *page;

        page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Sensors"), value,
                                                   composite);
        composite->addPage(page);

        page->beginBox(AcquisitionComponentPage::tr("Inlet temperature"));
        createTSINeph3563SensorCalibration(page, "Parameters/SCI/",
                                           AcquisitionComponentPage::tr("K"), 400.0);
        page->endBox();

        page->beginBox(AcquisitionComponentPage::tr("Sample temperature"));
        createTSINeph3563SensorCalibration(page, "Parameters/SCS/",
                                           AcquisitionComponentPage::tr("K"), 400.0);
        page->endBox();

        page->beginBox(AcquisitionComponentPage::tr("Sample relative humidity"));
        createTSINeph3563SensorCalibration(page, "Parameters/SCR/",
                                           AcquisitionComponentPage::tr("%"), 100.0, 1023);
        page->endBox();

        page->beginBox(AcquisitionComponentPage::tr("Pressure"));
        createTSINeph3563SensorCalibration(page, "Parameters/SCP/",
                                           AcquisitionComponentPage::tr("hPa"), 1200.0);
        page->endBox();

        page->addStretch();
    }

    return composite;
}

static QList<AcquisitionComponentPage *> create_acquire_tsi_neph3563(const Variant::Read &value,
                                                                     QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;

    result.append(createTSINeph3563ParametersPage(value, parent));

    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->beginBox("Sample wavelengths");

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Blue (nm):"), "Wavelength/B");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(450.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The wavelength of the blue sampling channel in nm."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Blue wavelength"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This allows the blue sampling wavelength in nm to be overridden."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Green (nm):"), "Wavelength/G");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(550.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The wavelength of the green sampling channel in nm."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Green wavelength"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This allows the green sampling wavelength in nm to be overridden."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Red (nm):"), "Wavelength/R");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(700.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The wavelength of the red sampling channel in nm."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Red wavelength"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This allows the red sampling wavelength in nm to be overridden."));

    page->endBox();

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Lamp current limit (A):"),
                           "MaximumLampCurrent");
    page->setLastDoubleSpinBox(1, 0.1, 99.99, 0.1);
    page->setLastUnassigned(7.0);
    page->setLastToolTip(AcquisitionComponentPage::tr(
            "The maximum current through the lamp before the total power is reduced."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Maximum lamp current"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the maximum current through the lamp before the acquisition system will attempt to reduce the total power."));

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Report interval (s):"), "ReportInterval");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(1.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The expected reporting interval in seconds."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Report interval"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This sets the expected reporting interval."));

    page->addBoolean(AcquisitionComponentPage::tr("Enable strict parsing"), "StrictMode");
    page->setLastToolTip(AcquisitionComponentPage::tr("Enable strict mode parsing."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Strict parsing"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This enables additional checks during data parsing."));

    page->addStretch();

    result.append(createSpancheckPage(value, SpancheckFlags_Chopper, parent));

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_vaisala_pwdx2(const Variant::Read &value,
                                                                      QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;

    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Poll interval (s):"), "PollInterval");
    page->setLastDoubleSpinBox(0, 0.0, 99999, 1);
    page->setLastUnassigned(15.0);
    page->setLastToolTip(AcquisitionComponentPage::tr("The polling interval in seconds."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Polling interval"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the delay between instrument data polling.  This determines how frequently the data values are updated."));

    page->addSingleLineString(AcquisitionComponentPage::tr("Instrument identifier:"), "ID");
    page->setLastUnassigned(AcquisitionComponentPage::tr("Automatic"));
    page->setLastToolTip(AcquisitionComponentPage::tr("The instrument identifier code."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Identifier code"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This sets the instrument identification code."));

    page->addEnum(AcquisitionComponentPage::tr("Visibility source:"), "Visibility");
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The selected source to get visibility data from."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Visibility"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This controls how the acquisition system selects which visibility reading to report."));
    page->appendLastEnumEntry(AcquisitionComponentPage::tr("Automatic"),
                              QStringList("Automatic") << "Auto", AcquisitionComponentPage::tr(
                    "Select the visibility source based on the polling interval."));
    page->appendLastEnumEntry(AcquisitionComponentPage::tr("One minute"),
                              QStringList("1Minute") << "Minute" << "1Min",
                              AcquisitionComponentPage::tr(
                                      "Always use the one-minute visibility reading."));
    page->appendLastEnumEntry(AcquisitionComponentPage::tr("Ten minute"),
                              QStringList("10Minute") << "10Min", AcquisitionComponentPage::tr(
                    "Always use the ten-minute visibility reading."));

    page->addEnum(AcquisitionComponentPage::tr("Present weather source:"), "PresentWeather");
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The selected source to get present weather data from."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Present weather"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This controls how the acquisition system selects which present weather code to report."));
    page->appendLastEnumEntry(AcquisitionComponentPage::tr("Automatic"),
                              QStringList("Automatic") << "Auto", AcquisitionComponentPage::tr(
                    "Select the weather source based on the polling interval."));
    page->appendLastEnumEntry(AcquisitionComponentPage::tr("Instantaneous"),
                              QStringList("Instantaneous") << "Instant",
                              AcquisitionComponentPage::tr(
                                      "Always use the instantaneous weather code."));
    page->appendLastEnumEntry(AcquisitionComponentPage::tr("Fifteen minute"),
                              QStringList("15Minute") << "15Min", AcquisitionComponentPage::tr(
                    "Always use the 15-minute weather code."));
    page->appendLastEnumEntry(AcquisitionComponentPage::tr("One hour"),
                              QStringList("1Hour") << "Hour", AcquisitionComponentPage::tr(
                    "Always use the one hour weather code."));

    page->addBoolean(AcquisitionComponentPage::tr("Enable strict parsing"), "StrictMode");
    page->setLastUnassigned(true);
    page->setLastToolTip(AcquisitionComponentPage::tr("Enable strict mode parsing."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Strict parsing"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This enables additional checks during data parsing."));

    page->addStretch();

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_vaisala_wmt700(const Variant::Read &value,
                                                                       QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;

    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Report interval (s):"), "ReportInterval");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(1.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The expected reporting interval in seconds."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Report interval"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the expected reporting interval.  This is used to determine how long to wait for responses."));

    page->addBoolean(AcquisitionComponentPage::tr("Enable strict parsing"), "StrictMode");
    page->setLastToolTip(AcquisitionComponentPage::tr("Enable strict mode parsing."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Strict parsing"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This enables additional checks during data parsing."));

    page->addSingleLineString(AcquisitionComponentPage::tr("Instrument address:"), "Address");
    page->setLastUnassigned(AcquisitionComponentPage::tr("Any"));
    page->setLastToolTip(AcquisitionComponentPage::tr("The address code to require."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Instrument address"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the required address code for data to be accepted."));

    page->setLastStretch();

    return result;
}

static QList<AcquisitionComponentPage *> create_acquire_vaisala_wxt5xx(const Variant::Read &value,
                                                                       QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentPageGeneric *page;

    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Sensors"), value,
                                               parent);
    result.append(page);

    page->addBoolean(AcquisitionComponentPage::tr("Auxiliary temperature"),
                     "Auxiliary/Temperature");
    page->setLastExplicitUndefined();
    page->setLastToolTip(AcquisitionComponentPage::tr("Enable the auxiliary temperature sensor."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Temperature sensor"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This controls the state of the auxiliary temperature sensor on the analog input.  When undefined, it is left unchanged and reported if the instrument is configured for it."));

    page->addBoolean(AcquisitionComponentPage::tr("Solar radiation"), "Auxiliary/SolarRadiation");
    page->setLastExplicitUndefined();
    page->setLastToolTip(AcquisitionComponentPage::tr("Enable the solar radiation sensor."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Solar radiation sensor"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This controls the state of the solar radiation sensor on the analog input.  When undefined, it is left unchanged and reported if the instrument is configured for it."));

    page->addBoolean(AcquisitionComponentPage::tr("Ultrasonic level"), "Auxiliary/Level");
    page->setLastExplicitUndefined();
    page->setLastToolTip(AcquisitionComponentPage::tr("Enable the ultrasonic level sensor."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Level sensor"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This controls the state of the ultrasonic level sensor on the analog input.  When undefined, it is left unchanged and reported if the instrument is configured for it."));

    page->addBoolean(AcquisitionComponentPage::tr("Tipping bucket"), "Auxiliary/Rain");
    page->setLastExplicitUndefined();
    page->setLastToolTip(
            AcquisitionComponentPage::tr("Enable the auxiliary tipping bucket rainfall sensor."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Tipping bucket"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This controls the state of the auxiliary tipping bucket rainfall sensor.  When undefined, it is not used unless the internal capacitive sensor is invalid."));

    page->addStretch();


    page = new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                               parent);
    result.append(page);

    page->addDoubleSpinBox(AcquisitionComponentPage::tr("Report interval (s):"), "ReportInterval");
    page->setLastDoubleSpinBox(0, 1, 9999, 1);
    page->setLastUnassigned(1.0);
    page->setLastToolTip(
            AcquisitionComponentPage::tr("The expected reporting interval in seconds."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Report interval"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This sets the expected reporting interval."));

    page->addIntegerSpinBox(AcquisitionComponentPage::tr("Address:"), "Address");
    page->setLastIntegerSpinBox(0, 61);
    page->setLastToolTip(AcquisitionComponentPage::tr("This is the address of the instrument."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Instrument address"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the address code that the acquisition system attempts to communicate with."));

    page->addBoolean(AcquisitionComponentPage::tr("Instrument heater"), "Heating");
    page->setLastExplicitUndefined();
    page->setLastToolTip(AcquisitionComponentPage::tr("Enable the instrument heater."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Heating"));
    page->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This controls the state of the instrument heater.  When undefined, it is left unchanged."));

    page->addBoolean(AcquisitionComponentPage::tr("Enable strict parsing"), "StrictMode");
    page->setLastUnassigned(true);
    page->setLastToolTip(AcquisitionComponentPage::tr("Enable strict mode parsing."));
    page->setLastStatusTip(AcquisitionComponentPage::tr("Strict parsing"));
    page->setLastWhatsThis(
            AcquisitionComponentPage::tr("This enables additional checks during data parsing."));

    page->addStretch();

    return result;
}

static QList<AcquisitionComponentPage *> create_control_accumulate(const Variant::Read &value,
                                                                   QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;

    {
        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Accumulators"),
                                                     value, "Accumulators", parent);
        table->setArrayMode(true, AcquisitionComponentGenericTable::Array_ZeroBased);
        result.append(table);

        table->addString(AcquisitionComponentPage::tr("Name"), "Accumulators/%1/Name");
        table->setLastString(true, false);
        table->setLastToolTip(AcquisitionComponentPage::tr("The output variable name."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Variable name"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the output variable name that the data are logged as."));

        table->addDynamicInput(AcquisitionComponentPage::tr("Rate"), "Accumulators/%1/Rate");
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The accumulation rate input in units/second."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Accumulation rate"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the input that is integrated into the final output value by multiplying it by the elapsed time in seconds."));

        table->addMetadata(AcquisitionComponentPage::tr("Metadata"), "Accumulators/%1/Metadata");
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The metadata base for the analog input."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Variable metadata"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "Use this to set the base metadata generated along with the analog input value."));

        table->addCombinedCommands(AcquisitionComponentPage::tr("Activation"),
                                   "Accumulators/%1/Activate");
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The commands issued on accumulator bin activation."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Activation commands"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This set the commands generated whenever the accumulator bin is activated.  For example, these would be the commands to turn on flow to a specific valve."));

        table->addCombinedCommands(AcquisitionComponentPage::tr("Deactivate"),
                                   "Accumulators/%1/Deactivate");
        table->setLastToolTip(AcquisitionComponentPage::tr(
                "The commands issued on accumulator bin deactivation."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Deactivation commands"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This set the commands generated whenever the accumulator bin is deactivated.  For example, these would be the commands to turn off flow to a specific valve."));
    }

    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Control"), value,
                                                    parent);
        result.append(page);

        page->addIntegerSpinBox(AcquisitionComponentPage::tr("Bypass accumulator:"), "BypassIndex");
        page->setLastIntegerSpinBox(0, 9999);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This is the index of the accumulator activated when the system is in bypass mode."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Bypass index"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the index of the accumulator that is activated when the system is in bypass mode."));

        page->addIntegerSpinBox(AcquisitionComponentPage::tr("Blank accumulator:"), "BlankIndex");
        page->setLastIntegerSpinBox(0, 9999);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This is the index of the accumulator active during the initial blanking period."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Blank index"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the index of the accumulator that is activated when during the initial blanking period."));

        page->addIntegerSpinBox(AcquisitionComponentPage::tr("Initial accumulator:"), "StartIndex");
        page->setLastIntegerSpinBox(0, 9999);
        page->setLastUnassigned((qint64) 0);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This is the index of the accumulator to start the rotation on."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Start Index"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the index of the accumulator that is activated after a change."));

        page->addIntegerSpinBox(AcquisitionComponentPage::tr("Final accumulator:"), "EndIndex");
        page->setLastIntegerSpinBox(0, 9999);
        page->setLastUnassigned((qint64) 0);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This is the index of the accumulator to sample on once the rotation has completed."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("End index"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the index of the accumulator that is activated once the cycle finishes."));

        page->addBoolean(AcquisitionComponentPage::tr("Bypass system while changing"),
                         "BypassWhileChanging");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "Put the system into bypass mode while the accumulator is changing."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Bypass while changing"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "Then enabled, this causes the accumulator control to bypass the system when it enters into a changing state."));

        page->addBoolean(AcquisitionComponentPage::tr("Bypass system after completion"),
                         "BypassOnEnd");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "Put the system into bypass mode once sampling is completed."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Bypass on completion"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "Then enabled, this causes the accumulator control to bypass the system after it has finished sampling all accumulators."));

        page->beginBox(AcquisitionComponentPage::tr("Sampling time"));

        page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Advance:"), "Advance");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This sets the normal sampling time between accumulator advances."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Advance time"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This controls the normal amount of time an accumulator is sampled for."));

        page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Minimum:"), "MinimumTime");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This sets the minimum amount of time an accumulator is sampled on."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Minimum time"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This controls the minimum amount of time an accumulator is schedule for sampling.  When the time is less than this, the advance time is repeated so that the accumulator is sampled for longer."));

        page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Initial blank:"), "BlankTime");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This sets the amount of time the accumulator is on the blank selection immediately after a change."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Blank time"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This controls how long the system will spend on the specified blank index immediately after a change, before entering normal sampling."));

        page->endBox();

        page->addTapChainProcessing(AcquisitionComponentPage::tr("Rate processing:"), "Processing");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The processing applied to the inputs used for accumulation rates."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Rate processing"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This controls the tap chain processing applied to realtime data before the rates are integrated into totals."));

        page->addDoubleSpinBox(AcquisitionComponentPage::tr("Timeout (s):"), "UpdateTimeout");
        page->setLastDoubleSpinBox(0, 1, 9999, 1);
        page->setLastUnassigned(120.0);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The maximum amount of time to wait for an accumulator update before discontinuing the current value."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Update timeout"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the maximum time the control will wait for a rate update before introducing a gap in the accumulated values."));

        page->addStretch();
    }

    return result;
}


static QList<AcquisitionComponentPage *> createControlCommandStatePages(const Variant::Read &value,
                                                                        QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;

    {
        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Bit Set"), value,
                                                     "BitSet", parent);
        result.append(table);

        table->addIntegerSpinBox(AcquisitionComponentPage::tr("Bits"), "BitSet/%1");
        table->setLastIntegerSpinBox(0, INT_MAX, NumberFormat("FFFFFFFF"));
        table->setLastToolTip(AcquisitionComponentPage::tr("The bits to set on state operation."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Bit set"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This causes the identifier to be interpreted as an integer, then the specified value bitwise OR'ed into it."));
    }

    {
        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Bit Clear"),
                                                     value, "BitClear", parent);
        result.append(table);

        table->addIntegerSpinBox(AcquisitionComponentPage::tr("Bits"), "BitClear/%1");
        table->setLastIntegerSpinBox(0, INT_MAX, NumberFormat("FFFFFFFF"));
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The bits to clear on state operation."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Bit clear"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This causes the identifier to be interpreted as an integer, then the specified value to be removed bitwise from it (AND of the inverse)."));
    }

    {
        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Value Overlay"),
                                                     value, "Overlay", parent);
        result.append(table);

        table->addGenericValue(AcquisitionComponentPage::tr("Value"), "Overlay/%1");
        table->setLastToolTip(AcquisitionComponentPage::tr(
                "The full value to overlay into the state identifier."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Value overlay"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This defines a full value to be overlaid into the identifier state key."));
    }

    return result;
}

namespace {
class ControlCommandStateDelegate : public AcquisitionComponentTableDelegatePages {
public:
    ControlCommandStateDelegate(QObject *parent = 0) : AcquisitionComponentTableDelegatePages(
            parent)
    { }

    virtual ~ControlCommandStateDelegate()
    { }

    virtual QList<AcquisitionComponentPage *> createPages(const CPD3::Data::Variant::Read &value,
                                                          QWidget *parent = 0) const
    { return createControlCommandStatePages(value, parent); }

    virtual QString buttonText(const CPD3::Data::Variant::Read &value) const
    { return tr("Edit"); }
};


class ControlCommandInputEditor : public AcquisitionComponentGenericTableEditorButton {
public:
    ControlCommandInputEditor(QWidget *parent = 0) : AcquisitionComponentGenericTableEditorButton(
            parent)
    { }

    virtual ~ControlCommandInputEditor()
    { }

protected:
    virtual bool editData(QVariant &value)
    {
        Variant::Write v = value.value<Variant::Write>();

        QDialog dialog(this);
        dialog.setWindowTitle(tr("Value"));
        QVBoxLayout *layout = new QVBoxLayout(&dialog);
        dialog.setLayout(layout);

        VariableSelect *selection = new VariableSelect(&dialog);
        selection->configureFromDynamicSelection(v);
        selection->setDefaultAvailable();

        QDialogButtonBox *buttons =
                new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                                     Qt::Horizontal, &dialog);
        layout->addWidget(buttons);
        QObject::connect(buttons, SIGNAL(accepted()), &dialog, SLOT(accept()));
        QObject::connect(buttons, SIGNAL(rejected()), &dialog, SLOT(reject()));

        if (dialog.exec() != QDialog::Accepted)
            return false;

        v.remove(false);
        selection->writeDynamicSelection(v);
        return true;
    }
};

class ControlCommandInputDelegate : public QItemDelegate {
public:
    ControlCommandInputDelegate(QObject *parent = 0) : QItemDelegate(parent)
    { }

    virtual ~ControlCommandInputDelegate()
    { }

    virtual QWidget *createEditor(QWidget *parent,
                                  const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const
    {
        ControlCommandInputEditor *button = new ControlCommandInputEditor(parent);
        QVariant value(index.data(Qt::EditRole));
        button->attach(index, value);
        button->setText(tr("Edit"));
        return button;
    }

    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
    {
        ControlCommandInputEditor *button = static_cast<ControlCommandInputEditor *>(editor);
        QVariant value(index.data(Qt::EditRole));
        button->attach(index, value);
        button->setText(tr("Edit"));
    }

    virtual void setModelData(QWidget *editor,
                              QAbstractItemModel *model,
                              const QModelIndex &index) const
    {
        Q_UNUSED(editor);
        Q_UNUSED(model);
        Q_UNUSED(index);
    }
};
}

static QList<AcquisitionComponentPage *> create_control_command(const Variant::Read &value,
                                                                QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;

    {
        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Commands"),
                                                     value, "Commands", parent);
        result.append(table);

        table->addColumn(AcquisitionComponentPage::tr("Before"), "Commands/%1/Before",
                         new ControlCommandStateDelegate(table));
        table->setLastToolTip(AcquisitionComponentPage::tr(
                "The state operations to perform before command output."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("State before"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the state operations performed before the command is output."));

        table->addColumn(AcquisitionComponentPage::tr("After"), "Commands/%1/After",
                         new ControlCommandStateDelegate(table));
        table->setLastToolTip(AcquisitionComponentPage::tr(
                "The state operations to perform after command output."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("State after"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the state operations performed after the command is output."));

        table->addStringLines(AcquisitionComponentPage::tr("Output"), "Commands/%1/Output");
        table->setLastToolTip(AcquisitionComponentPage::tr("The data to write out."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Command output"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the data to write out when the command is received."));
    }

    {
        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Inputs"), value,
                                                     "Inputs", parent);
        result.append(table);

        table->addColumn(AcquisitionComponentPage::tr("Input"), "Inputs/%1",
                         new ControlCommandInputDelegate(table));
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The input values used for the identifier."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Inputs"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the inputs assigned to the given identifier value for substitutions."));
    }

    return result;
}


static QList<AcquisitionComponentPage *> createActionPages(const Variant::Read &value,
                                                           QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentBoxCompositePage *box;

    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("General"), value,
                                                    parent);
        result.append(page);

        page->addSingleLineString(AcquisitionComponentPage::tr("Description"), "Description");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "A human readable description of the action for display."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Description"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets a human readable description of the action used for menus and interaction."));

        page->addDoubleSpinBox(AcquisitionComponentPage::tr("Flush time (s):"), "FlushTime");
        page->setLastDoubleSpinBox(1, 0, 9999, 1);
        page->setLastUnassigned(62.0);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The time the system is flushed after action execution."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Flush time"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the time the system flushes data (values invalid) after the action executes."));

        page->addEnum(AcquisitionComponentPage::tr("Cut size:"), "SetCut");
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("No size selection"),
                                  QStringList("None"), AcquisitionComponentPage::tr(
                        "Set the system to no size selection."));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("PM1"),
                                  QStringList("PM1") << "1" << "fine", AcquisitionComponentPage::tr(
                        "Set the system to a size selection of less than 1 \xCE\xBCm"));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("PM10"),
                                  QStringList("PM10") << "10" << "coarse",
                                  AcquisitionComponentPage::tr(
                                          "Set the system to a size selection of less than 10 \xCE\xBCm"));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("PM2.5"),
                                  QStringList("PM25") << "25" << "2.5" << "PM2.5",
                                  AcquisitionComponentPage::tr(
                                          "Set the system to a size selection of less than 2.5 \xCE\xBCm"));
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The change, if any, to the system cut size selection."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Cut size selection"));
        page->setLastWhatsThis(
                AcquisitionComponentPage::tr("This changes the system cut size selection."));

        page->addGlobalCommands(AcquisitionComponentPage::tr("Global commands:"), "Commands");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "Command data sent to all instruments on action execution."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Action commands"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the command data sent to all instruments when the action is executed."));

        page->addInstrumentCommands(AcquisitionComponentPage::tr("Instrument commands:"),
                                    "InstrumentCommands");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "Command data sent to specific instruments on action execution."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Action commands"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the command data sent to individual instruments (specified by name) when the action is executed."));

        page->addFlagsSelection(AcquisitionComponentPage::tr("Additional data flavors:"),
                                "SetFlavors");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This sets any additional flavors added to the data stream."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("System flavors"));
        page->setLastWhatsThis(
                AcquisitionComponentPage::tr("This adds additional flavors to the acquired data."));

        page->addStretch();
    }

    box = new AcquisitionComponentBoxCompositePage(AcquisitionComponentPage::tr("Contamination"),
                                                   value, parent);
    result.append(box);
    {
        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Set"), value,
                                                     "Contaminate", box);
        box->addPage(table);

        table->addGenericValue(AcquisitionComponentPage::tr("Metadata"), "Contaminate/%1");
        table->setLastToolTip(AcquisitionComponentPage::tr("The flag definition metadata."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Flag metadata"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the metadata about the specific contamination flag being set."));
    }
    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Clear"), value,
                                                    box);
        box->addPage(page, 0);

        page->addFlagsSelection(AcquisitionComponentPage::tr("Identifiers:"), "ClearContaminate");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This sets the contamination flags to clear on action execution."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Contamination clear"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the contamination flags to remove when the action executes."));
    }

    box = new AcquisitionComponentBoxCompositePage(AcquisitionComponentPage::tr("Flags"), value,
                                                   parent);
    result.append(box);
    {
        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Set"), value,
                                                     "SetFlags", box);
        box->addPage(table);

        table->addGenericValue(AcquisitionComponentPage::tr("Metadata"), "SetFlags/%1");
        table->setLastToolTip(AcquisitionComponentPage::tr("The flag definition metadata."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Flag metadata"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the metadata about the specific system flag being set."));
    }
    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Clear"), value,
                                                    box);
        box->addPage(page, 0);

        page->addFlagsSelection(AcquisitionComponentPage::tr("Identifiers:"), "ClearFlags");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This sets the system flags to clear on action execution."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Contamination clear"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the system flags to remove when the action executes."));
    }

    box = new AcquisitionComponentBoxCompositePage(AcquisitionComponentPage::tr("Bypass"), value,
                                                   parent);
    result.append(box);
    {
        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Set"), value,
                                                     "Bypass", box);
        box->addPage(table);

        table->addGenericValue(AcquisitionComponentPage::tr("Metadata"), "Bypass/%1");
        table->setLastToolTip(AcquisitionComponentPage::tr("The bypass lock definition metadata."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Lock metadata"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the metadata about the specific bypass lock being set."));
    }
    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Clear"), value,
                                                    box);
        box->addPage(page, 0);

        page->addFlagsSelection(AcquisitionComponentPage::tr("Identifiers:"), "ClearBypass");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This sets the bypass locks to clear on action execution."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Contamination clear"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the bypass locks to remove when the action executes."));
    }

    return result;
}

namespace {
class CycleActionEditorDelegate : public AcquisitionComponentTableDelegatePages {
public:
    CycleActionEditorDelegate(QObject *parent = 0) : AcquisitionComponentTableDelegatePages(parent)
    { }

    virtual ~CycleActionEditorDelegate()
    { }

    virtual QList<AcquisitionComponentPage *> createPages(const CPD3::Data::Variant::Read &value,
                                                          QWidget *parent = 0) const
    { return createActionPages(value, parent); }

    virtual QString buttonText(const CPD3::Data::Variant::Read &value) const
    { return tr("Edit"); }
};
}

static void showCycleActionDialog(Variant::Write &output, QWidget *parent)
{
    QDialog dialog(parent);
    dialog.setWindowTitle(AcquisitionComponentPage::tr("Action"));
    QVBoxLayout *layout = new QVBoxLayout(&dialog);
    dialog.setLayout(layout);

    QList<AcquisitionComponentPage *> pages(createActionPages(output, &dialog));
    if (pages.isEmpty())
        return;

    if (pages.size() == 1) {
        layout->addWidget(pages.first(), 1);
    } else {
        QTabWidget *tabs = new QTabWidget(&dialog);
        layout->addWidget(tabs, 1);
        for (QList<AcquisitionComponentPage *>::const_iterator p = pages.constBegin(),
                endP = pages.constEnd(); p != endP; ++p) {
            tabs->addTab(*p, (*p)->title());
        }
    }

    QDialogButtonBox *buttons =
            new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal,
                                 &dialog);
    layout->addWidget(buttons);
    QObject::connect(buttons, SIGNAL(accepted()), &dialog, SLOT(accept()));
    QObject::connect(buttons, SIGNAL(rejected()), &dialog, SLOT(reject()));

    if (dialog.exec() != QDialog::Accepted)
        return;

    for (QList<AcquisitionComponentPage *>::const_iterator p = pages.constBegin(),
            endP = pages.constEnd(); p != endP; ++p) {
        (*p)->commit(output);
    }
}

static QList<AcquisitionComponentPage *> create_control_cycle(const Variant::Read &value,
                                                              QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;

    {
        Variant::Write meta = Variant::Write::empty();
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Cycle"), value,
                                                    parent);
        result.append(page);

        page->addBoolean(AcquisitionComponentPage::tr("Enable cycle"), "EnableCycle");
        page->setLastUnassigned(true);
        page->setLastToolTip(
                AcquisitionComponentPage::tr("Initially enable the cycle on start up."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Cycle enable"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This setting controls if the cycle execution is enabled on acquisition system start up."));

        meta.setEmpty();
        meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr("One hour aligned");
        page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Cycle period:"), "Period",
                                       meta);
        page->setLastToolTip(AcquisitionComponentPage::tr("The total period of the cycle."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Cycle period"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the total period that the cycle occupies."));

        meta.setEmpty();
        page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Minimum cycle time:"),
                                       "MinimumTime", meta);
        page->setLastToolTip(AcquisitionComponentPage::tr("The minimum time on initialization."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Cycle period"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "When the time of the cycle end is below this threshold on initialization, the cycle is extended by another instance of the total period."));

        page->addBoolean(AcquisitionComponentPage::tr("Suspend cycle during system bypass"),
                         "SuspendOnBypass");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "Do not execute any actions while the system is bypassed."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Suspend while bypassed"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This setting causes the cycle to not execute any actions while the system is in bypass mode."));

        page->beginBox(AcquisitionComponentPage::tr("Spinup"));

        meta.setEmpty();
        page->addTimeIntervalSelection(AcquisitionComponentPage::tr("Duration:"), "Spinup/Duration",
                                       meta);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This is the time on initial start up that the cycle is held in a spin up state."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Spin up time"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This option controls how long the cycle is held in a spin up state on initial start up.  During spinup, the active action is in effect and no other actions are executed."));

        page->addGenericDialog(AcquisitionComponentPage::tr("Active:"), "Spinup/Active",
                               showCycleActionDialog, AcquisitionComponentPage::tr("Action"));
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "While the system is in spin up state, this action will be executed."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Spin up action"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the action to execute while in spin up state."));

        page->addGenericDialog(AcquisitionComponentPage::tr("End:"), "Spinup/End",
                               showCycleActionDialog, AcquisitionComponentPage::tr("Action"));
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This is the action executed as the system leaves spin up state."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Spin up end action"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the action to execute when the spin up of the cycle has completed."));

        page->endBox();

        page->addStretch();
    }

    {
        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Actions"), value,
                                                     "Actions", parent);
        table->setArrayMode();
        result.append(table);

        table->addString(AcquisitionComponentPage::tr("ID"), "Actions/%1/Identifier");
        table->setLastString(true);
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The internal identifier of the action."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Action identifier"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the optional internal identifier of the action."));

        table->addColumn(AcquisitionComponentPage::tr("Action"), "Actions/%1",
                         new CycleActionEditorDelegate(table));
        table->setLastToolTip(AcquisitionComponentPage::tr("The action taken."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Action"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This controls the operations performed to the system at the specified point in time of the cycle."));

        table->addTimeIntervalSelection(AcquisitionComponentPage::tr("Time"), "Actions/%1");
        table->setLastToolTip(AcquisitionComponentPage::tr(
                "The time after the cycle start to execute the action at."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Execution time"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the time after the cycle start point that the action is executed at."));
    }

    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                                    parent);
        result.append(page);

        page->addDoubleSpinBox(AcquisitionComponentPage::tr("Default flush time (s):"),
                               "DefaultFlushTime");
        page->setLastDoubleSpinBox(1, 0.1, 9999.9, 1);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This is the system flush time applied for every action in the cycle."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Flush time"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This option controls the flush time associated with every action in the cycle that does not explicitly set one.  This is normally used with cycles that change the system requiring a flush for every action in them."));

        page->addGlobalCommands(AcquisitionComponentPage::tr("Global commands"),
                                "BaselineCommands");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The commands executed with every action in the cycle."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Commands"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This option sets the base commands executed for every action in the cycle."));

        page->addInstrumentCommands(AcquisitionComponentPage::tr("Global commands"),
                                    "BaselineInstrumentCommands");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The commands executed with every action in the cycle for specific components."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Instrument commands"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This option sets the base commands executed for every action in the cycle for specific components."));

        page->addBoolean(AcquisitionComponentPage::tr("Log activation marker"), "LogActiveAction");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "Write out a data value reflecting the currently executed cycle action."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Log cycle"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "When enabled, this setting causes a marker to be written out reflecting the currently executed action in the cycle."));

        page->addBoolean(AcquisitionComponentPage::tr("Initialize all actions completed"),
                         "InitializeInactive");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "When set, all actions will be marked as completed and not executed until the next cycle."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Initialize as inactive"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This setting causes all actions in the cycle to be initialized as already completed.  This means that none will be executed until the next cycle after start up that resets all their states."));

        page->addStretch();
    }

    return result;
}


static QList<AcquisitionComponentPage *> createTriggerActionPages(const Variant::Read &value,
                                                                  QWidget *parent)
{
    QList<AcquisitionComponentPage *> result(createActionPages(value, parent));

    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Activation"),
                                                    value, parent);
        result.append(page);

        page->addSingleLineString(AcquisitionComponentPage::tr("Log event text:"), "Event");
        page->setLastToolTip(AcquisitionComponentPage::tr("The text of a realtime event to log."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Event"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the text of a realtime event to log when the action is executed."));

        page->addBoolean(AcquisitionComponentPage::tr("Show event in realtime display"),
                         "EventRealtime");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This enables the display of the event on the operator screen in real time."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Realtime event"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "When enabled, this will cause the event text to be shown on the realtime acquisition display."));

        page->addBoolean(AcquisitionComponentPage::tr("Continually apply the action"),
                         "Continuous");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This causes the system to contually apply the action as long as the trigger is met."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Continually apply"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "When enabled, the operations taken by the action are continually applied as long as the trigger is met.  When disabled, they are only applied when the trigger is first met."));

        page->addDoubleSpinBox(AcquisitionComponentPage::tr("Dependency hold time (s):"),
                               "Dependencies/Hold");
        page->setLastDoubleSpinBox(1, 0.1, 9999.9, 1);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The amount of time dependencies set or cleared by the action are held in limbo."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Hold time"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the amount of time after action execution that all dependency tags set or cleared by it are held in limbo.  This prevents any other actions that interact with those tags from executing until the hold time has completed."));

        page->addStretch();
    }

    return result;
}

static void showCompositeTriggerCompositeEditor(Variant::Write &value, QWidget *parent);

static QList<AcquisitionComponentPage *> createTriggerConditionPages(const Variant::Read &value,
                                                                     QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentTypedCompositePage *composite =
            new AcquisitionComponentTypedCompositePage(AcquisitionComponentPage::tr("Condition"),
                                                       value, "Type", parent);
    result.append(composite);

    {
        AcquisitionComponentPageGeneric *page = new AcquisitionComponentPageGeneric(
                AcquisitionComponentPage::tr("Threshold Comparison"), value, composite);
        composite->addPage(page, QStringList("Threshold") << "Limit", AcquisitionComponentPage::tr(
                "Test a realtime value against a static threshold."));

        page->addEnum(AcquisitionComponentPage::tr("Operation"), "Operation");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The comparison operation, with the dynamic input being the left hand side."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Comparison operation"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the type of comparison being done.  The dynamic input is the left hand size while the threshold is the right."));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("Greater than"), "GreaterThan",
                                  AcquisitionComponentPage::tr(
                                          "Check if the input is greater than the threshold."));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("Greater than or equal"),
                                  "GreaterThanOrEqual", AcquisitionComponentPage::tr(
                        "Check if the input is greater than or equal to the threshold."));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("Less than"), "LessThan",
                                  AcquisitionComponentPage::tr(
                                          "Check if the input is less than the threshold."));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("Less than or equal"),
                                  "LessThanOrEqual", AcquisitionComponentPage::tr(
                        "Check if the input is less than or equal to the threshold."));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("Equal"), "Equal",
                                  AcquisitionComponentPage::tr(
                                          "Check if the input is equal to the threshold."));
        page->appendLastEnumEntry(AcquisitionComponentPage::tr("Not equal"), "NotEqual",
                                  AcquisitionComponentPage::tr(
                                          "Check if the input is not equal to the threshold."));

        page->addDouble(AcquisitionComponentPage::tr("Threshold:"), "Threshold");
        page->setLastExplicitUndefined(true);
        page->setLastToolTip(
                AcquisitionComponentPage::tr("The right hand side of the comparison operation."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Comparison threshold"));
        page->setLastWhatsThis(
                AcquisitionComponentPage::tr("This sets the threshold to compare against."));

        page->addDynamicInput(AcquisitionComponentPage::tr("Input:"), "Input");
        page->setLastToolTip(AcquisitionComponentPage::tr("The comparison input."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Comparison input"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the dynamic input to compare with the threshold."));

        page->addStretch();
    }

    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Logical AND"),
                                                    value, composite);
        composite->addPage(page, QStringList("AND") << "Composite", AcquisitionComponentPage::tr(
                "Combine conditions and require all of them to be met."));

        page->addGenericDialog(AcquisitionComponentPage::tr("Triggers:"), "Triggers",
                               showCompositeTriggerCompositeEditor);
        page->setLastToolTip(AcquisitionComponentPage::tr("The component triggers."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Required triggers"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr("The triggers to check."));

        page->addStretch();
    }

    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Logical OR"),
                                                    value, composite);
        composite->addPage(page, QStringList("OR") << "Any", AcquisitionComponentPage::tr(
                "Combine conditions and require all of them to be met."));

        page->addGenericDialog(AcquisitionComponentPage::tr("Triggers:"), "Triggers",
                               showCompositeTriggerCompositeEditor);
        page->setLastToolTip(AcquisitionComponentPage::tr("The component triggers."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Required triggers"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr("The triggers to check."));

        page->addStretch();
    }

    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Always"), value,
                                                    composite);
        composite->addPage(page, QStringList("Always"),
                           AcquisitionComponentPage::tr("This condition is always met."));

        page->addStretch();
    }

    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Never"), value,
                                                    composite);
        composite->addPage(page, QStringList("Never"),
                           AcquisitionComponentPage::tr("This condition is never met."));

        page->addStretch();
    }

    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Flags present"),
                                                    value, composite);
        composite->addPage(page, QStringList("Flags") << "Flag", AcquisitionComponentPage::tr(
                "Test if a realtime value contains a set of flags."));

        page->addFlagsSelection(AcquisitionComponentPage::tr("Flags:"), "Flags");
        page->setLastToolTip(AcquisitionComponentPage::tr("The required flags."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Condition flags"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "The condition is met if all flags are present in the input."));

        page->addDynamicSelection(AcquisitionComponentPage::tr("Input:"), "Input");
        page->setLastToolTip(AcquisitionComponentPage::tr("The flags input."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Flags input"));
        page->setLastWhatsThis(
                AcquisitionComponentPage::tr("This sets the parameter to read the flags from."));

        page->addStretch();
    }

    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Spike detected"),
                                                    value, composite);
        composite->addPage(page, QStringList("Spike") << "Risk", AcquisitionComponentPage::tr(
                "Trigger when a smoother reports a spike in realtime value."));

        Variant::Write meta = Variant::Write::empty();
        meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr(
                "A 30 minute long fixed time smoother, with a threshold of -4.0 (four times greater than the smoothed value)");
        meta["*hEditor/EnableSpike"] = true;
        meta["*hEditor/DefaultSmoother/Type"] = "FixedTime";
        meta["*hEditor/DefaultSmoother/Time"] = 1800.0;
        meta["*hEditor/DefaultSmoother/MinimumTime"] = 1800.0;
        meta["*hEditor/DefaultSmoother/Band"] = -4.0;
        page->addSmoother(AcquisitionComponentPage::tr("Smoother:"), "Smoother", meta);
        page->setLastToolTip(
                AcquisitionComponentPage::tr("The smoother used for spike detection."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Smoother applied"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the smoother applied to the input value and used for spike detection."));

        page->addDynamicInput(AcquisitionComponentPage::tr("Input:"), "Input");
        page->setLastToolTip(AcquisitionComponentPage::tr("The realtime input to be smoothed."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Smoother input"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the realtime input that the smoother is applied to."));

        page->addStretch();
    }

    {
        AcquisitionComponentPageGeneric *page = new AcquisitionComponentPageGeneric(
                AcquisitionComponentPage::tr("Stability detection"), value, composite);
        composite->addPage(page, QStringList("Stable") << "Smoother", AcquisitionComponentPage::tr(
                "Trigger when a smoother reports a stability in realtime value."));

        Variant::Write meta = Variant::Write::empty();
        meta["*hUndefinedDescription"] = AcquisitionComponentPage::tr(
                "A 90 second long smoother with a required RSD of less than 0.001");
        meta["*hEditor/EnableStability"] = true;
        meta["*hEditor/DefaultSmoother/Type"] = "FixedTime";
        meta["*hEditor/DefaultSmoother/Time"] = 90.0;
        meta["*hEditor/DefaultSmoother/MinimumTime"] = 90.0;
        meta["*hEditor/DefaultSmoother/RSD"] = 0.001;
        page->addSmoother(AcquisitionComponentPage::tr("Smoother:"), "Smoother", meta);
        page->setLastToolTip(
                AcquisitionComponentPage::tr("The smoother used for stability detection."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Smoother applied"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the smoother applied to the input value and used for stability detection."));

        page->addDynamicInput(AcquisitionComponentPage::tr("Input:"), "Input");
        page->setLastToolTip(AcquisitionComponentPage::tr("The realtime input to be smoothed."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Smoother input"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the realtime input that the smoother is applied to."));

        page->addStretch();
    }

    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Inside range"),
                                                    value, composite);
        composite->addPage(page, QStringList("Range"), AcquisitionComponentPage::tr(
                "Trigger when a realtime value is inside of a range."));

        page->addDouble(AcquisitionComponentPage::tr("Minimum:"), "Minimum");
        page->setLastExplicitUndefined(true);
        page->setLastToolTip(AcquisitionComponentPage::tr("The minimum accepted value."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Minimum"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "The minimum that the input must be greater than for the condition to be met."));

        page->addDouble(AcquisitionComponentPage::tr("Maximum:"), "Maximum");
        page->setLastExplicitUndefined(true);
        page->setLastToolTip(AcquisitionComponentPage::tr("The maximum accepted value."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Maximum"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "The maximum that the input must be less than for the condition to be met."));

        page->addDynamicInput(AcquisitionComponentPage::tr("Input:"), "Input");
        page->setLastToolTip(AcquisitionComponentPage::tr("The realtime input to be check."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Range input"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the realtime input that the condition is tested against."));

        page->addStretch();
    }

    {
        AcquisitionComponentPageGeneric *page = new AcquisitionComponentPageGeneric(
                AcquisitionComponentPage::tr("Angle inside range"), value, composite);
        composite->addPage(page, QStringList("Angle") << "Wind" << "Winds",
                           AcquisitionComponentPage::tr(
                                   "Trigger when a realtime value is within an angular range, wrapping around if required."));

        page->addDouble(AcquisitionComponentPage::tr("Start:"), "Start");
        page->setLastExplicitUndefined(true);
        page->setLastToolTip(AcquisitionComponentPage::tr("The start of the range."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Start"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "The start angle to check against.  If the end angle is less than the start, the range wraps around through 360."));

        page->addDouble(AcquisitionComponentPage::tr("End:"), "End");
        page->setLastExplicitUndefined(true);
        page->setLastToolTip(AcquisitionComponentPage::tr("The end of the range."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("End"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "The end angle to check against.  If the end angle is less than the start, the range wraps around through 360."));

        page->addDynamicInput(AcquisitionComponentPage::tr("Input:"), "Input");
        page->setLastToolTip(AcquisitionComponentPage::tr("The realtime angle to be check."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Range input"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the realtime angle that the condition is tested against."));

        page->addStretch();
    }

    {
        AcquisitionComponentPageGeneric *page = new AcquisitionComponentPageGeneric(
                AcquisitionComponentPage::tr("Script condition"), value, composite);
        composite->addPage(page, QStringList("Script"), AcquisitionComponentPage::tr(
                "Evaluate a segment of script code as the condition."));

        page->addDynamicSelection(AcquisitionComponentPage::tr("Input:"), "Input");
        page->setLastToolTip(
                AcquisitionComponentPage::tr("The inputs available to the script code."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Script input"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the input values available to the script code during evaluation."));

        page->addScript(AcquisitionComponentPage::tr("Code"), "Code");
        page->setLastToolTip(AcquisitionComponentPage::tr("The script code to evaluate."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Script"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the segment of script code that is evaluated.  Its return result is the boolean value of the condition."));

        page->setLastStretch(1);
    }

    return result;
}

namespace {
class TriggerConditionEditorDelegate : public AcquisitionComponentTableDelegatePages {
public:
    TriggerConditionEditorDelegate(QObject *parent = 0) : AcquisitionComponentTableDelegatePages(
            parent)
    { }

    virtual ~TriggerConditionEditorDelegate()
    { }

    virtual QList<AcquisitionComponentPage *> createPages(const CPD3::Data::Variant::Read &value,
                                                          QWidget *parent = 0) const
    { return createTriggerConditionPages(value, parent); }

    virtual QString buttonText(const CPD3::Data::Variant::Read &value) const
    { return tr("Edit"); }
};
}

static QList<AcquisitionComponentPage *> createTriggerPages(const Variant::Read &value,
                                                            QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentGenericTable *table =
            new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Triggers"), value,
                                                 "", parent);
    table->setArrayMode(true, AcquisitionComponentGenericTable::Array_HashChildren);
    result.append(table);

    table->addBoolean(AcquisitionComponentPage::tr("Invert"), "%1/Invert");
    table->setLastToolTip(AcquisitionComponentPage::tr(
            "Invert the trigger and run the action when the condition is not met, but the dependencies are."));
    table->setLastStatusTip(AcquisitionComponentPage::tr("Invert trigger condition"));
    table->setLastWhatsThis(AcquisitionComponentPage::tr(
            "When set, the trigger is considered met if its condition is not true, but the dependencies are."));

    table->addColumn(AcquisitionComponentPage::tr("Condition"), "%1",
                     new TriggerConditionEditorDelegate(table));
    table->setLastToolTip(AcquisitionComponentPage::tr("The condition of the trigger."));
    table->setLastStatusTip(AcquisitionComponentPage::tr("Condition"));
    table->setLastWhatsThis(AcquisitionComponentPage::tr("This sets the condition being checked."));

    table->addFlags(AcquisitionComponentPage::tr("Require"), "%1/Require");
    table->setLastToolTip(AcquisitionComponentPage::tr(
            "The dependency tags required to be present for the trigger to run."));
    table->setLastStatusTip(AcquisitionComponentPage::tr("Required dependency tags"));
    table->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the tags that must be present before the trigger is even considered."));

    table->addFlags(AcquisitionComponentPage::tr("Exclude"), "%1/Exclude");
    table->setLastToolTip(AcquisitionComponentPage::tr(
            "The dependency tags required to be absent for the trigger to run."));
    table->setLastStatusTip(AcquisitionComponentPage::tr("Excluded dependency tags"));
    table->setLastWhatsThis(AcquisitionComponentPage::tr(
            "This sets the tags that must be not be present before the trigger is even considered."));

    return result;
}

static void showCompositeTriggerCompositeEditor(Variant::Write &value, QWidget *parent)
{
    QDialog dialog(parent);
    dialog.setWindowTitle(AcquisitionComponentPageGeneric::tr("Triggers"));
    QVBoxLayout *layout = new QVBoxLayout(&dialog);
    dialog.setLayout(layout);

    QList<AcquisitionComponentPage *> pages(createTriggerPages(value, &dialog));
    if (pages.isEmpty())
        return;

    if (pages.size() == 1) {
        layout->addWidget(pages.first(), 1);
        dialog.setWindowTitle(pages.first()->title());
    } else {
        QTabWidget *tabs = new QTabWidget(&dialog);
        layout->addWidget(tabs, 1);
        for (QList<AcquisitionComponentPage *>::const_iterator p = pages.constBegin(),
                endP = pages.constEnd(); p != endP; ++p) {
            tabs->addTab(*p, (*p)->title());
        }
    }

    QDialogButtonBox *buttons =
            new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal,
                                 &dialog);
    layout->addWidget(buttons);
    QObject::connect(buttons, SIGNAL(accepted()), &dialog, SLOT(accept()));
    QObject::connect(buttons, SIGNAL(rejected()), &dialog, SLOT(reject()));

    if (dialog.exec() != QDialog::Accepted)
        return;

    for (QList<AcquisitionComponentPage *>::const_iterator p = pages.constBegin(),
            endP = pages.constEnd(); p != endP; ++p) {
        (*p)->commit(value);
    }
}

namespace {
class TriggerActionEditorDelegate : public AcquisitionComponentTableDelegatePages {
public:
    TriggerActionEditorDelegate(QObject *parent = 0) : AcquisitionComponentTableDelegatePages(
            parent)
    { }

    virtual ~TriggerActionEditorDelegate()
    { }

    virtual QList<AcquisitionComponentPage *> createPages(const CPD3::Data::Variant::Read &value,
                                                          QWidget *parent = 0) const
    { return createTriggerActionPages(value, parent); }

    virtual QString buttonText(const CPD3::Data::Variant::Read &value) const
    { return tr("Edit"); }
};

class TriggerEditorDelegate : public AcquisitionComponentTableDelegatePages {
public:
    TriggerEditorDelegate(QObject *parent = 0) : AcquisitionComponentTableDelegatePages(parent)
    { }

    virtual ~TriggerEditorDelegate()
    { }

    virtual QList<AcquisitionComponentPage *> createPages(const CPD3::Data::Variant::Read &value,
                                                          QWidget *parent = 0) const
    { return createTriggerPages(value, parent); }

    virtual QString buttonText(const CPD3::Data::Variant::Read &value) const
    { return tr("Edit"); }
};
}

static QList<AcquisitionComponentPage *> create_control_trigger(const Variant::Read &value,
                                                                QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;

    {
        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Actions"), value,
                                                     "Actions", parent);
        table->setArrayMode();
        result.append(table);

        table->addString(AcquisitionComponentPage::tr("ID"), "Actions/%1/Identifier");
        table->setLastString(true);
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The internal identifier of the action."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Action identifier"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the optional internal identifier of the action."));

        table->addColumn(AcquisitionComponentPage::tr("Action"), "Actions/%1",
                         new TriggerActionEditorDelegate(table));
        table->setLastToolTip(AcquisitionComponentPage::tr("The action taken."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Action"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This controls the operations performed to the system when the trigger is met."));

        table->addColumn(AcquisitionComponentPage::tr("Trigger"), "Actions/%1/Triggers",
                         new TriggerEditorDelegate(table));
        table->setLastToolTip(AcquisitionComponentPage::tr("The trigger for the action."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Trigger"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the trigger that control when the action is executed."));

        table->addFlags(AcquisitionComponentPage::tr("Acquire"), "Actions/%1/Dependencies/Acquire");
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The dependency tags set on action execution."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Acquired dependency tags"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the tags that are set when the action is executed."));

        table->addFlags(AcquisitionComponentPage::tr("Release"), "Actions/%1/Dependencies/Release");
        table->setLastToolTip(
                AcquisitionComponentPage::tr("The dependency tags clear on action execution."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Released dependency tags"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the tags that are cleared when the action is executed."));
    }

    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                                    parent);
        result.append(page);

        page->addBoolean(AcquisitionComponentPage::tr("Enable triggers"), "EnableTrigger");
        page->setLastUnassigned(true);
        page->setLastToolTip(
                AcquisitionComponentPage::tr("Initially enable the trigger set on start up."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Trigger enable"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This setting controls if trigger execution is enabled on acquisition system start up."));

        page->addDoubleSpinBox(AcquisitionComponentPage::tr("Default flush time (s):"),
                               "DefaultFlushTime");
        page->setLastDoubleSpinBox(1, 0.1, 9999.9, 1);
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "This is the system flush time applied for every action in the cycle."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Flush time"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This option controls the flush time associated with every action in the cycle that does not explicitly set one.  This is normally used with cycles that change the system requiring a flush for every action in them."));

        page->addGlobalCommands(AcquisitionComponentPage::tr("Global commands"),
                                "BaselineCommands");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The commands executed with every action in the cycle."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Commands"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This option sets the base commands executed for every action in the cycle."));

        page->addInstrumentCommands(AcquisitionComponentPage::tr("Global commands"),
                                    "BaselineInstrumentCommands");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The commands executed with every action in the cycle for specific components."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Instrument commands"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This option sets the base commands executed for every action in the cycle for specific components."));

        page->addBoolean(AcquisitionComponentPage::tr("Log activation marker"), "LogActiveAction");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "Write out a data value reflecting the currently executed cycle action."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Log cycle"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "When enabled, this setting causes a marker to be written out reflecting the currently executed action in the cycle."));
        page->addStretch();
    }

    return result;
}


static void showFlagsBitsEditor(Variant::Write &value, QWidget *parent)
{
    QDialog dialog(parent);
    QVBoxLayout *layout = new QVBoxLayout(&dialog);
    dialog.setLayout(layout);

    AcquisitionComponentGenericTable *table =
            new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Flags"), value, "",
                                                 &dialog);

    table->addIntegerSpinBox(AcquisitionComponentPage::tr("Bits"), "%1");
    table->setLastIntegerSpinBox(0, INT_MAX, NumberFormat("FFFFFFFF"), false);
    table->setLastDefaults(AcquisitionComponentPage::tr("Automatic"));
    table->setLastToolTip(AcquisitionComponentPage::tr("The output bits for the flag."));
    table->setLastStatusTip(AcquisitionComponentPage::tr("Output bits"));
    table->setLastWhatsThis(
            AcquisitionComponentPage::tr("This is the output bits for the identified flag."));

    layout->addWidget(table, 1);
    dialog.setWindowTitle(table->title());


    QDialogButtonBox *buttons =
            new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal,
                                 &dialog);
    layout->addWidget(buttons);
    QObject::connect(buttons, SIGNAL(accepted()), &dialog, SLOT(accept()));
    QObject::connect(buttons, SIGNAL(rejected()), &dialog, SLOT(reject()));

    if (dialog.exec() != QDialog::Accepted)
        return;

    table->commit(value);
}

static void showFlagsRenameEditor(Variant::Write &value, QWidget *parent)
{
    QDialog dialog(parent);
    QVBoxLayout *layout = new QVBoxLayout(&dialog);
    dialog.setLayout(layout);

    AcquisitionComponentGenericTable *table =
            new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Flags"), value, "",
                                                 &dialog);

    table->addString(AcquisitionComponentPage::tr("Bits"), "%1");
    table->setLastString(false);
    table->setLastToolTip(AcquisitionComponentPage::tr("The output text for the flag."));
    table->setLastStatusTip(AcquisitionComponentPage::tr("Output text"));
    table->setLastWhatsThis(
            AcquisitionComponentPage::tr("This is the output text for the identified flag."));

    layout->addWidget(table, 1);
    dialog.setWindowTitle(table->title());


    QDialogButtonBox *buttons =
            new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal,
                                 &dialog);
    layout->addWidget(buttons);
    QObject::connect(buttons, SIGNAL(accepted()), &dialog, SLOT(accept()));
    QObject::connect(buttons, SIGNAL(rejected()), &dialog, SLOT(reject()));

    if (dialog.exec() != QDialog::Accepted)
        return;

    table->commit(value);
}

static QList<AcquisitionComponentPage *> createControlExportColumnPages(const Variant::Read &value,
                                                                        QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;
    AcquisitionComponentTypedCompositePage *composite =
            new AcquisitionComponentTypedCompositePage(AcquisitionComponentPage::tr("Column"),
                                                       value, "Type", parent);
    result.append(composite);

    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Numeric"), value,
                                                    composite);
        composite->addPage(page, QStringList("Real") << "Value",
                           AcquisitionComponentPage::tr("A single real number output column."));

        page->addDynamicSelection(AcquisitionComponentPage::tr("Input:"), "Input");
        page->setLastToolTip(AcquisitionComponentPage::tr("The column input."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Value input"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr("This sets the input to the column."));

        page->addSingleLineString(AcquisitionComponentPage::tr("Path:"), "Path");
        page->setLastToolTip(AcquisitionComponentPage::tr("The path extracted from the input."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Input path"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the path within the input values that the number is extracted from."));

        page->addCalibration(AcquisitionComponentPage::tr("Calibration:"), "Calibration");
        page->setLastToolTip(
                AcquisitionComponentPage::tr("The calibration applied to the input values."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Calibration"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the calibration applied to the input after path extraction."));

        page->addSingleLineString(AcquisitionComponentPage::tr("Format:"), "Format");
        page->setLastUnassigned(AcquisitionComponentPage::tr("Derived from the metadata"));
        page->setLastToolTip(AcquisitionComponentPage::tr("The output format of the number."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Number format"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the formatting applied to the number before output."));

        page->addSingleLineString(AcquisitionComponentPage::tr("Missing values:"), "MVC");
        page->setLastUnassigned(AcquisitionComponentPage::tr("Derived from the metadata"));
        page->setLastToolTip(
                AcquisitionComponentPage::tr("The missing value code for the column."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Missing value code"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is contents of the column that are output when no value is available from the input."));

        page->addSingleLineString(AcquisitionComponentPage::tr("Padding:"), "Pad");
        page->setLastUnassigned(AcquisitionComponentPage::tr("0"));
        page->setLastExplicitUndefined(true);
        page->setLastToolTip(AcquisitionComponentPage::tr("The padding used with the format."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Output padding"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the character used to pad the format when the final value has fewer integer digits than the format specifies."));

        page->addStretch();
    }

    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Time"), value,
                                                    composite);
        composite->addPage(page, QStringList("Time") << "DateTime" << "Date",
                           AcquisitionComponentPage::tr("The record time."));

        page->addSingleLineString(AcquisitionComponentPage::tr("Format:"), "TimeFormat");
        page->setLastUnassigned(AcquisitionComponentPage::tr("ISO 8601 compliant"));
        page->setLastToolTip(AcquisitionComponentPage::tr("The time format specifier."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Time format"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "The output time format specifier.  Refer to Qt's documentation for the available syntax."));

        page->addBoolean(AcquisitionComponentPage::tr("Local time"), "LocalTime");
        page->setLastToolTip(AcquisitionComponentPage::tr("Output in local time instead of UTC."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Local time"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "Use a local time (as determined by the system time zone) instead of the normal UTC time."));

        page->addStretch();
    }

    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Bitwise flags"),
                                                    value, composite);
        composite->addPage(page, QStringList("Flags") << "Bits" << "FlagsBits" << "BitFlags",
                           AcquisitionComponentPage::tr(
                                   "Interpret flags into integer bits OR'd together."));

        page->addDynamicSelection(AcquisitionComponentPage::tr("Input:"), "Input");
        page->setLastToolTip(AcquisitionComponentPage::tr("The column input."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Value input"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr("This sets the input to the column."));

        page->addSingleLineString(AcquisitionComponentPage::tr("Path:"), "Path");
        page->setLastToolTip(AcquisitionComponentPage::tr("The path extracted from the input."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Input path"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the path within the input values that the flags are extracted from."));

        page->addGenericDialog(AcquisitionComponentPage::tr("Bit mapping:"), "Bits",
                               showFlagsBitsEditor);
        page->setLastUnassigned(AcquisitionComponentPage::tr("Derived from the metadata"));
        page->setLastToolTip(AcquisitionComponentPage::tr("Flag name to bits mappings."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Flag to bit mapping"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This defines flag name to numeric bit mappings used for translation."));

        page->addSingleLineString(AcquisitionComponentPage::tr("Format:"), "Format");
        page->setLastUnassigned(AcquisitionComponentPage::tr("Derived from the metadata"));
        page->setLastToolTip(AcquisitionComponentPage::tr("The output format of the number."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Number format"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the formatting applied to the number before output."));

        page->addSingleLineString(AcquisitionComponentPage::tr("Missing values:"), "MVC");
        page->setLastUnassigned(AcquisitionComponentPage::tr("Derived from the metadata"));
        page->setLastToolTip(
                AcquisitionComponentPage::tr("The missing value code for the column."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Missing value code"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is contents of the column that are output when no value is available from the input."));

        page->addSingleLineString(AcquisitionComponentPage::tr("Padding:"), "Pad");
        page->setLastUnassigned(AcquisitionComponentPage::tr("0"));
        page->setLastExplicitUndefined(true);
        page->setLastToolTip(AcquisitionComponentPage::tr("The padding used with the format."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Output padding"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the character used to pad the format when the final value has fewer integer digits than the format specifies."));

        page->addStretch();
    }

    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Named flags"),
                                                    value, composite);
        composite->addPage(page, QStringList("FlagsName") << "FlagsString" << "StringFlags",
                           AcquisitionComponentPage::tr("Output text strings for flags."));

        page->addDynamicSelection(AcquisitionComponentPage::tr("Input:"), "Input");
        page->setLastToolTip(AcquisitionComponentPage::tr("The column input."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Value input"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr("This sets the input to the column."));

        page->addSingleLineString(AcquisitionComponentPage::tr("Path:"), "Path");
        page->setLastToolTip(AcquisitionComponentPage::tr("The path extracted from the input."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Input path"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the path within the input values that the flags are extracted from."));

        page->addGenericDialog(AcquisitionComponentPage::tr("Flag mapping:"), "FlagsRename",
                               showFlagsRenameEditor);
        page->setLastToolTip(AcquisitionComponentPage::tr("Flag renaming control."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Flag renaming"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This allows flags to be renamed into different text than their internal identifiers."));

        page->addSingleLineString(AcquisitionComponentPage::tr("Join:"), "Join");
        page->setLastUnassigned(AcquisitionComponentPage::tr(";"));
        page->setLastToolTip(
                AcquisitionComponentPage::tr("The text used to combine multiple flags."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Flags join"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the text that is used as a delimiter when there are multiple flags."));

        page->addSingleLineString(AcquisitionComponentPage::tr("Missing values:"), "MVC");
        page->setLastUnassigned(AcquisitionComponentPage::tr("Derived from the metadata"));
        page->setLastToolTip(
                AcquisitionComponentPage::tr("The missing value code for the column."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Missing value code"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is contents of the column that are output when no value is available from the input."));

        page->addStretch();
    }

    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Script"), value,
                                                    composite);
        composite->addPage(page, QStringList("Script"), AcquisitionComponentPage::tr(
                "Evaluate script code to generate the column contents."));

        page->addDynamicSelection(AcquisitionComponentPage::tr("Input:"), "Input");
        page->setLastToolTip(
                AcquisitionComponentPage::tr("The inputs available to the script code."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Script input"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the input values available to the script code during evaluation."));

        page->addScript(AcquisitionComponentPage::tr("Code"), "Code");
        page->setLastToolTip(AcquisitionComponentPage::tr("The script code to evaluate."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Script"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the segment of script code that is evaluated.  Its return result is the string value that is output."));

        page->setLastStretch(1);
    }

    return result;
}

namespace {
class ControlExportColumnDelegate : public AcquisitionComponentTableDelegatePages {
public:
    ControlExportColumnDelegate(QObject *parent = 0) : AcquisitionComponentTableDelegatePages(
            parent)
    { }

    virtual ~ControlExportColumnDelegate()
    { }

    virtual QList<AcquisitionComponentPage *> createPages(const CPD3::Data::Variant::Read &value,
                                                          QWidget *parent = 0) const
    { return createControlExportColumnPages(value, parent); }

    virtual QString buttonText(const CPD3::Data::Variant::Read &value) const
    { return tr("Edit"); }
};
}

static QList<AcquisitionComponentPage *> create_control_export(const Variant::Read &value,
                                                               QWidget *parent)
{
    QList<AcquisitionComponentPage *> result;

    {
        AcquisitionComponentGenericTable *table =
                new AcquisitionComponentGenericTable(AcquisitionComponentPage::tr("Columns"), value,
                                                     "Columns", parent);
        table->setArrayMode(true, AcquisitionComponentGenericTable::Array_HashChildren);
        result.append(table);

        table->addString(AcquisitionComponentPage::tr("Header"), "Columns/%1/Header");
        table->setLastString(true);
        table->setLastDefaults(AcquisitionComponentPage::tr("Automatic"));
        table->setLastToolTip(AcquisitionComponentPage::tr("The header entry of the column."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Column header"));
        table->setLastWhatsThis(
                AcquisitionComponentPage::tr("This is the header entry of the column."));

        table->addColumn(AcquisitionComponentPage::tr("Value"), "Columns/%1",
                         new ControlExportColumnDelegate(table));
        table->setLastToolTip(AcquisitionComponentPage::tr("The column data value."));
        table->setLastStatusTip(AcquisitionComponentPage::tr("Value"));
        table->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This controls the contents of the column in the output."));
    }

    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Output"), value,
                                                    parent);
        result.append(page);

        page->addBoolean(AcquisitionComponentPage::tr("Write header"), "Header");
        page->setLastUnassigned(true);
        page->setLastToolTip(
                AcquisitionComponentPage::tr("Write a header line during file output."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Write header line"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "Write a header line with the column names to the output on every update."));

        page->beginBox(AcquisitionComponentPage::tr("Line"));

        page->addSingleLineString(AcquisitionComponentPage::tr("Prefix:"), "Line/Prefix");
        page->setLastToolTip(AcquisitionComponentPage::tr("Text added before every line."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Line prefix"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the text inserted before the field data is written."));

        page->addSingleLineString(AcquisitionComponentPage::tr("Suffix:"), "Line/Suffix");
        page->setLastUnassigned(AcquisitionComponentPage::tr("A single newline"));
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The text added to the end of each line after field data is written."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Line suffix"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the text inserted after the field data when each line is written."));

        page->endBox();

        page->beginBox(AcquisitionComponentPage::tr("Field join"));

        page->addSingleLineString(AcquisitionComponentPage::tr("Separator:"), "Join/Separator");
        page->setLastUnassigned(AcquisitionComponentPage::tr(","));
        page->setLastToolTip(AcquisitionComponentPage::tr("The separator between fields."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Join separator"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the text inserted between fields in a line to join them together."));

        page->addSingleLineString(AcquisitionComponentPage::tr("Quote:"), "Join/Quote");
        page->setLastUnassigned(AcquisitionComponentPage::tr("\""));
        page->setLastToolTip(
                AcquisitionComponentPage::tr("The quote used when a field contains a separator."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Join quote"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the text added to the start and end of fields when the field data contains the separator character."));

        page->addSingleLineString(AcquisitionComponentPage::tr("Quote escape:"),
                                  "Join/QuoteEscape");
        page->setLastUnassigned(AcquisitionComponentPage::tr("\"\"\""));
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "The escape used in a quoted field to escape another instance of the quote itself."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Join quote escape"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the text that existing quotes within a quoted field are replaced with."));

        page->endBox();

        page->addStretch();
    }

    {
        AcquisitionComponentPageGeneric *page =
                new AcquisitionComponentPageGeneric(AcquisitionComponentPage::tr("Advanced"), value,
                                                    parent);
        result.append(page);

        page->addBoolean(AcquisitionComponentPage::tr("Load archived data"), "LoadArchived");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "Load the back log of data from the archive on start up."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Load data backlog"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This setting enables loading data from the archive on start up, instead of only exporting new realtime values."));

        page->addSingleLineString(AcquisitionComponentPage::tr("File:"), "File");
        page->setLastUnassigned(AcquisitionComponentPage::tr("A temporary file"));
        page->setLastToolTip(AcquisitionComponentPage::tr("The output file name to write into."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Output file"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This option sets the file name to write the output into.  If not set and a file is required, then a temporary one is created."));

        page->addBoolean(AcquisitionComponentPage::tr("Append mode"), "Append");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "Open the output file in in append mode, instead of overwriting it."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Output append mode"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This option causes the output file to be opened in append mode, rather than overwriting it.  This is used to continually update a file instead of regenerating it every time new data is received."));

        page->addBoolean(AcquisitionComponentPage::tr("Disable file output"), "NoFile");
        page->setLastToolTip(AcquisitionComponentPage::tr("Disable writing to a file."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Disable file"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This option disables output to a file and is normally used in conjunction with an output interface."));

        page->addBoolean(AcquisitionComponentPage::tr("Open file in binary mode"), "BinaryMode");
        page->setLastToolTip(AcquisitionComponentPage::tr(
                "Open the output file in binary mode, instead of text mode."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Output in binary mode"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This option causes the output file to be opened in binary mode, instead of text mode.  This prevents the translation of newlines to the operating system specific equivalent."));

        page->addSingleLineString(AcquisitionComponentPage::tr("Command:"), "Command");
        page->setLastUnassigned(AcquisitionComponentPage::tr("None"));
        page->setLastToolTip(AcquisitionComponentPage::tr("A command to run after file export."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Output command"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This is the command to run after every time the export file is generated.  If no export file is specified, a temporary one is generated before the command is run."));

        page->addTapChainProcessing(AcquisitionComponentPage::tr("Processing:"), "Processing");
        page->setLastToolTip(
                AcquisitionComponentPage::tr("The processing applied to data before export."));
        page->setLastStatusTip(AcquisitionComponentPage::tr("Output processing"));
        page->setLastWhatsThis(AcquisitionComponentPage::tr(
                "This sets the processing applied to all data before it is exported."));

        page->addStretch();
    }

    return result;
}


QList<AcquisitionComponentPage *> AcquisitionComponentEndpoint::createComponentPages(const QString &component,
                                                                                     const Variant::Read &value,
                                                                                     QWidget *parent)
{
    QString name(component.toLower());
    QList<AcquisitionComponentPage *> result;

    if (name == "acquire_2b_ozone205") {
        result.append(create_acquire_2b_ozone205(value, parent));
    } else if (name == "acquire_ad_cpcmagic200") {
        result.append(create_acquire_ad_cpcmagic200(value, parent));
    } else if (name == "acquire_aerodyne_caps") {
        result.append(create_acquire_aerodyne_caps(value, parent));
    } else if (name == "acquire_azonix_umac1050") {
        result.append(create_acquire_azonix_umac1050(value, parent));
    } else if (name == "acquire_bmi_cpc1710" || name == "acquire_bmi_cpc1720") {
        result.append(create_acquire_bmi_cpc17x0(value, parent));
    } else if (name == "acquire_brooks_pid0254") {
        result.append(create_acquire_brooks_pid0254(value, parent));
    } else if (name == "acquire_campbell_cr1000gmd") {
        result.append(create_acquire_campbell_cr1000gmd(value, parent));
    } else if (name == "acquire_csd_pops") {
        result.append(create_acquire_csd_pops(value, parent));
    } else if (name == "acquire_dmt_bcp") {
        result.append(create_acquire_dmt_bcp(value, parent));
    } else if (name == "acquire_dmt_ccn") {
        result.append(create_acquire_dmt_ccn(value, parent));
    } else if (name == "acquire_dmt_pax") {
        result.append(create_acquire_dmt_pax(value, parent));
    } else if (name == "acquire_ecotech_nephaurora") {
        result.append(create_acquire_ecotech_nephaurora(value, parent));
    } else if (name == "acquire_eigenbrodt_nmo191") {
        result.append(create_acquire_eigenbrodt_nmo191(value, parent));
    } else if (name == "acquire_generic_metar") {
        result.append(create_acquire_generic_metar(value, parent));
    } else if (name == "acquire_generic_passive") {
        result.append(create_acquire_generic_passive(value, parent));
    } else if (name == "acquire_gmd_clap3w") {
        result.append(create_acquire_gmd_clap3w(value, parent));
    } else if (name == "acquire_gmd_cpcpulse") {
        result.append(create_acquire_gmd_cpcpulse(value, parent));
    } else if (name == "acquire_grimm_opc110x") {
        result.append(create_acquire_grimm_opc110x(value, parent));
    } else if (name == "acquire_love_pid") {
        result.append(create_acquire_love_pid(value, parent));
    } else if (name == "acquire_magee_aethalometer33") {
        result.append(create_acquire_magee_aethalometer33(value, parent));
    } else if (name == "acquire_magee_aethalometer162131") {
        result.append(create_acquire_magee_aethalometer162131(value, parent));
    } else if (name == "acquire_magee_tca08") {
        result.append(create_acquire_magee_tca08(value, parent));
    } else if (name == "acquire_maycomm_tdl") {
        /* No settings */
    } else if (name == "acquire_pms_lasair") {
        result.append(create_acquire_pms_lasair(value, parent));
    } else if (name == "acquire_purpleair_pa2") {
        result.append(create_acquire_purpleair_pa2(value, parent));
    } else if (name == "acquire_rmy_wind86xxx") {
        result.append(create_acquire_rmy_wind86xxx(value, parent));
    } else if (name == "acquire_rr_neph903") {
        result.append(create_acquire_rr_neph903(value, parent));
    } else if (name == "acquire_rr_psap1w") {
        result.append(create_acquire_rr_psap1w(value, parent));
    } else if (name == "acquire_rr_psap3w") {
        result.append(create_acquire_rr_psap3w(value, parent));
    } else if (name == "acquire_thermo_maap5012") {
        result.append(create_acquire_thermo_maap5012(value, parent));
    } else if (name == "acquire_thermo_ozone49") {
        result.append(create_acquire_thermo_ozone49(value, parent));
    } else if (name == "acquire_thermo_ozone49iq") {
        result.append(create_acquire_thermo_ozone49iq(value, parent));
    } else if (name == "acquire_tsi_cpc302x") {
        result.append(create_acquire_tsi_cpc302x(value, parent));
    } else if (name == "acquire_tsi_cpc377x") {
        result.append(create_acquire_tsi_cpc377x(value, parent));
    } else if (name == "acquire_tsi_cpc3010") {
        result.append(create_acquire_tsi_cpc3010(value, parent));
    } else if (name == "acquire_tsi_cpc3781" || name == "acquire_tsi_cpc3783") {
        result.append(create_acquire_tsi_cpc378x(value, parent));
    } else if (name == "acquire_tsi_mfm4xxx") {
        result.append(create_acquire_tsi_mfm4xxx(value, parent));
    } else if (name == "acquire_tsi_neph3563") {
        result.append(create_acquire_tsi_neph3563(value, parent));
    } else if (name == "acquire_vaisala_pwdx2") {
        result.append(create_acquire_vaisala_pwdx2(value, parent));
    } else if (name == "acquire_vaisala_wmt700") {
        result.append(create_acquire_vaisala_wmt700(value, parent));
    } else if (name == "acquire_vaisala_wxt5xx") {
        result.append(create_acquire_vaisala_wxt5xx(value, parent));
    } else if (name == "control_accumulate") {
        result.append(create_control_accumulate(value, parent));
    } else if (name == "control_command") {
        result.append(create_control_command(value, parent));
    } else if (name == "control_cycle") {
        result.append(create_control_cycle(value, parent));
    } else if (name == "control_export") {
        result.append(create_control_export(value, parent));
    } else if (name == "control_trigger") {
        result.append(create_control_trigger(value, parent));
    }

    return result;
}


}
}
}