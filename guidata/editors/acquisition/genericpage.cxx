/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QVBoxLayout>
#include <QLineEdit>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QDialog>
#include <QDialogButtonBox>
#include <QTableView>
#include <QStyledItemDelegate>
#include <QStandardItemModel>
#include <QInputDialog>
#include <QPlainTextEdit>

#include "guidata/editors/acquisition/genericpage.hxx"
#include "guidata/editors/baselinesmoother.hxx"
#include "guidata/editors/dynamictimeinterval.hxx"
#include "guidata/editors/dynamicinput.hxx"
#include "guicore/calibrationedit.hxx"
#include "guicore/scriptedit.hxx"
#include "datacore/variant/composite.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace GUI {
namespace Data {

/** @file guidata/editors/acquisition/genericpage.hxx
 * Generic component page generatation.
 */

AcquisitionComponentPageGeneric::AcquisitionComponentPageGeneric(const QString &title,
                                                                 const Variant::Read &value,
                                                                 QWidget *parent)
        : AcquisitionComponentPage(value, parent),
          pageTitle(title), initialConfiguration(value),
          topLevelLayout(NULL),
          activeBox(NULL),
          layoutTarget(NULL)
{
    initialConfiguration.detachFromRoot();

    topLevelLayout = new QVBoxLayout(this);
    setLayout(topLevelLayout);
    layoutTarget = topLevelLayout;
}

AcquisitionComponentPageGeneric::~AcquisitionComponentPageGeneric() = default;

QString AcquisitionComponentPageGeneric::title() const
{ return pageTitle; }

void AcquisitionComponentPageGeneric::commit(Variant::Write &target)
{
    for (QList<Internal::AcquisitionComponentPageGenericWidget *>::const_iterator
            w = components.constBegin(), endW = components.constEnd(); w != endW; ++w) {
        (*w)->commit(target);
    }
}

void AcquisitionComponentPageGeneric::addWidget(Internal::AcquisitionComponentPageGenericWidget *widget)
{
    if (activeBox != NULL)
        widget->setParent(activeBox);
    else
        widget->setParent(this);
    components.append(widget);
    layoutTarget->addWidget(widget);
}

void AcquisitionComponentPageGeneric::beginBox(const QString &label)
{
    Q_ASSERT(activeBox == NULL);

    activeBox = new QGroupBox(label, this);
    topLevelLayout->addWidget(activeBox);
    layoutTarget = new QVBoxLayout(activeBox);
    activeBox->setLayout(layoutTarget);
}

void AcquisitionComponentPageGeneric::endBox()
{
    Q_ASSERT(activeBox != NULL);
    activeBox = NULL;
    layoutTarget = topLevelLayout;
}

void AcquisitionComponentPageGeneric::addStretch(int stretch)
{ layoutTarget->addStretch(stretch); }

void AcquisitionComponentPageGeneric::setLastStretch(int stretch)
{ layoutTarget->setStretchFactor(components.last(), stretch); }

void AcquisitionComponentPageGeneric::setLastToolTip(const QString &toolTip)
{ components.last()->setToolTip(toolTip); }

void AcquisitionComponentPageGeneric::setLastStatusTip(const QString &statusTip)
{ components.last()->setStatusTip(statusTip); }

void AcquisitionComponentPageGeneric::setLastWhatsThis(const QString &whatsThis)
{ components.last()->setWhatsThis(whatsThis); }

void AcquisitionComponentPageGeneric::setLastExplicitUndefined(bool enable)
{ components.last()->setExplicitUndefined(enable); }

void AcquisitionComponentPageGeneric::setLastUnassigned(bool unassigned)
{ components.last()->setUnassigned(unassigned); }

void AcquisitionComponentPageGeneric::setLastUnassigned(double unassigned)
{ components.last()->setUnassigned(unassigned); }

void AcquisitionComponentPageGeneric::setLastUnassigned(qint64 unassigned)
{ components.last()->setUnassigned(unassigned); }

void AcquisitionComponentPageGeneric::setLastUnassigned(const QString &unassigned)
{ components.last()->setUnassigned(unassigned); }

void AcquisitionComponentPageGeneric::appendLastEnumEntry(const QString &label,
                                                          const QStringList &keys,
                                                          const QString &toolTip)
{ components.last()->appendEnumEntry(label, keys, toolTip); }

void AcquisitionComponentPageGeneric::setLastDoubleSpinBox(int decimals,
                                                           double minimum,
                                                           double maximum,
                                                           double step)
{ components.last()->setDoubleSpinBox(decimals, minimum, maximum, step); }

void AcquisitionComponentPageGeneric::setLastIntegerSpinBox(int minimum,
                                                            int maximum,
                                                            const NumberFormat &format)
{ components.last()->setIntegerSpinBox(minimum, maximum, format); }

namespace {
class PageWidgetBoolean : public Internal::AcquisitionComponentPageGenericWidget {
    bool unassigned;
    QCheckBox *check;

    void reconfigure()
    {
        auto v = input();
        if (!check->isTristate()) {
            if (!v.exists())
                check->setChecked(unassigned);
            else
                check->setChecked(input().toBool());
        } else {
            if (v.exists()) {
                check->setChecked(v.toBool());
            } else {
                check->setCheckState(Qt::PartiallyChecked);
            }
        }
    }

public:
    virtual ~PageWidgetBoolean() = default;

    PageWidgetBoolean(const QString &label, const QString &path, const Variant::Read &config)
            : Internal::AcquisitionComponentPageGenericWidget(path, config), unassigned(false)
    {
        QHBoxLayout *layout = new QHBoxLayout(this);
        layout->setContentsMargins(0, 0, 0, 0);
        setLayout(layout);

        check = new QCheckBox(label, this);
        layout->addWidget(check);

        layout->addStretch(1);

        reconfigure();
    }

    void commit(Variant::Write &target) override
    {
        if (!check->isTristate()) {
            if (check->isChecked() == unassigned) {
                output(target).remove(false);
            } else {
                output(target).setBool(!unassigned);
            }
        } else {
            switch (check->checkState()) {
            case Qt::Unchecked:
                output(target).setBool(false);
                break;
            case Qt::Checked:
                output(target).setBool(true);
                break;
            case Qt::PartiallyChecked:
                output(target).remove(false);
                break;
            }
        }
    }

    virtual void setUnassigned(bool unassigned)
    {
        this->unassigned = unassigned;
        reconfigure();
    }

    virtual void setExplicitUndefined(bool enable = true)
    {
        check->setTristate(enable);
        reconfigure();
    }
};
}

void AcquisitionComponentPageGeneric::addBoolean(const QString &label, const QString &path)
{ addWidget(new PageWidgetBoolean(label, path, initialConfiguration)); }


namespace {
class PageWidgetEnum : public Internal::AcquisitionComponentPageGenericOneLine {
    QComboBox *editor;

public:
    virtual ~PageWidgetEnum() = default;

    PageWidgetEnum(const QString &label, const QString &path, const Variant::Read &config)
            : Internal::AcquisitionComponentPageGenericOneLine(label, path, config)
    {
        editor = new QComboBox(this);
        addLineItem(editor);
    }

    virtual void appendEnumEntry(const QString &label,
                                 const QStringList &keys,
                                 const QString &toolTip = QString())
    {
        int index = editor->count();
        editor->addItem(label, QVariant::fromValue(keys));
        editor->setItemData(index, toolTip, Qt::ToolTipRole);

        QString check(input().toQString().toLower());
        for (QStringList::const_iterator k = keys.constBegin(), endK = keys.constEnd();
                k != endK;
                ++k) {
            if (k->toLower() == check) {
                editor->setCurrentIndex(index);
                return;
            }
        }
    }

    void commit(Variant::Write &target) override
    {
        output(target).remove(false);
        if (!isSet())
            return;
        if (isExplicitlyUndefined()) {
            output(target).setType(Variant::Type::String);
            return;
        }
        int i = editor->currentIndex();
        if (i < 0 || i >= editor->count())
            return;
        QStringList keys(editor->itemData(i).value<QStringList>());
        if (keys.isEmpty())
            return;
        output(target).setString(keys.first());
    }

protected:
    virtual void setComponentEnabled(bool enabled)
    { editor->setEnabled(enabled); }
};
}

void AcquisitionComponentPageGeneric::addEnum(const QString &label, const QString &path)
{ addWidget(new PageWidgetEnum(label, path, initialConfiguration)); }


namespace {
class PageWidgetSingleLineString : public Internal::AcquisitionComponentPageGenericOneLine {
    QLineEdit *editor;

public:
    virtual ~PageWidgetSingleLineString() = default;

    PageWidgetSingleLineString(const QString &label,
                               const QString &path,
                               const Variant::Read &config)
            : Internal::AcquisitionComponentPageGenericOneLine(label, path, config)
    {
        editor = new QLineEdit(this);
        addLineItem(editor);
        editor->setText(input().toQString().trimmed());
    }


    void commit(Variant::Write &target) override
    {
        output(target).remove(false);
        if (!isSet())
            return;
        if (isExplicitlyUndefined()) {
            output(target).setType(Variant::Type::String);
            return;
        }
        QString text(editor->text().trimmed());
        if (text.isEmpty())
            return;
        output(target).setString(text);
    }

    virtual void setUnassigned(const QString &unassigned)
    {
        editor->setPlaceholderText(unassigned);
    }

protected:
    virtual void setComponentEnabled(bool enabled)
    { editor->setEnabled(enabled); }
};
}

void AcquisitionComponentPageGeneric::addSingleLineString(const QString &label, const QString &path)
{ addWidget(new PageWidgetSingleLineString(label, path, initialConfiguration)); }


namespace {
class PageWidgetMultipleLineString : public Internal::AcquisitionComponentPageGenericBox {
    QPlainTextEdit *editor;

public:
    virtual ~PageWidgetMultipleLineString() = default;

    PageWidgetMultipleLineString(const QString &label,
                                 const QString &path,
                                 const Variant::Read &config)
            : Internal::AcquisitionComponentPageGenericBox(label, path, config)
    {
        QVBoxLayout *layout = new QVBoxLayout(layoutTarget());
        layoutTarget()->setLayout(layout);

        editor = new QPlainTextEdit(this);
        layout->addWidget(editor);
        editor->setPlainText(input().toQString());
    }


    void commit(Variant::Write &target) override
    {
        output(target).remove(false);
        if (!isSet())
            return;
        QString text(editor->toPlainText());
        output(target).setString(text);
    }

protected:
    virtual void setComponentEnabled(bool enabled)
    { editor->setEnabled(enabled); }
};
}

void AcquisitionComponentPageGeneric::addMultipleLineString(const QString &label,
                                                            const QString &path)
{ addWidget(new PageWidgetMultipleLineString(label, path, initialConfiguration)); }


namespace {
class PageWidgetScriptString : public Internal::AcquisitionComponentPageGenericBox {
    ScriptEdit *editor;

public:
    virtual ~PageWidgetScriptString() = default;

    PageWidgetScriptString(const QString &label, const QString &path, const Variant::Read &config)
            : Internal::AcquisitionComponentPageGenericBox(label, path, config)
    {
        QVBoxLayout *layout = new QVBoxLayout(layoutTarget());
        layoutTarget()->setLayout(layout);

        editor = new ScriptEdit(this);
        layout->addWidget(editor);
        editor->setCode(input().toQString());
    }


    void commit(Variant::Write &target) override
    {
        output(target).remove(false);
        if (!isSet())
            return;
        QString text(editor->getCode());
        output(target).setString(text);
    }

protected:
    virtual void setComponentEnabled(bool enabled)
    { editor->setEnabled(enabled); }
};
}

void AcquisitionComponentPageGeneric::addScript(const QString &label, const QString &path)
{ addWidget(new PageWidgetScriptString(label, path, initialConfiguration)); }


namespace {
class PageWidgetDouble : public Internal::AcquisitionComponentPageGenericOneLine {
    QLineEdit *editor;
public:
    virtual ~PageWidgetDouble() = default;

    PageWidgetDouble(const QString &label,
                     const QString &path, const Variant::Read &config,
                     double override) : Internal::AcquisitionComponentPageGenericOneLine(label,
                                                                                         path,
                                                                                         config)
    {
        editor = new QLineEdit(this);
        addLineItem(editor);
        double v = input().toDouble();
        if (FP::defined(override))
            v = override;
        if (FP::defined(v))
            editor->setText(QString::number(v));
    }

    void commit(Variant::Write &target) override
    {
        output(target).remove(false);
        if (!isSet())
            return;
        if (isExplicitlyUndefined()) {
            output(target).setDouble(FP::undefined());
            return;
        }
        QString text(editor->text().trimmed());
        if (text.isEmpty()) {
            output(target).setDouble(FP::undefined());
            return;
        }
        bool ok = false;
        double v = text.toDouble(&ok);
        if (!ok)
            v = FP::undefined();
        output(target).setDouble(v);
    }

    virtual void setUnassigned(double unassigned)
    { setUnassigned(QString::number(unassigned)); }

    virtual void setUnassigned(qint64 unassigned)
    { setUnassigned(QString::number(unassigned)); }

    virtual void setUnassigned(const QString &unassigned)
    {
        editor->setPlaceholderText(unassigned);
    }

protected:
    virtual void setComponentEnabled(bool enabled)
    { editor->setEnabled(enabled); }
};
}

void AcquisitionComponentPageGeneric::addDouble(const QString &label,
                                                const QString &path,
                                                double override)
{ addWidget(new PageWidgetDouble(label, path, initialConfiguration, override)); }


namespace {
class PageWidgetDoubleSpinBox : public Internal::AcquisitionComponentPageGenericOneLine {
    double override;
    QDoubleSpinBox *editor;
public:
    virtual ~PageWidgetDoubleSpinBox() = default;

    PageWidgetDoubleSpinBox(const QString &label,
                            const QString &path, const Variant::Read &config,
                            double o) : Internal::AcquisitionComponentPageGenericOneLine(label,
                                                                                         path,
                                                                                         config),
                                        override(o)
    {
        editor = new QDoubleSpinBox(this);
        editor->setDecimals(3);
        editor->setRange(0.001, 99.999);
        editor->setSingleStep(0.1);
        addLineItem(editor);
        double v = input().toDouble();
        if (FP::defined(override))
            v = override;
        if (!FP::defined(v))
            v = 1.0;
        editor->setValue(v);
    }

    virtual void setDoubleSpinBox(int decimals, double minimum, double maximum, double step)
    {
        editor->setDecimals(decimals);
        editor->setRange(minimum, maximum);
        editor->setSingleStep(step);
        double v = input().toDouble();
        if (FP::defined(override))
            v = override;
        if (!FP::defined(v))
            v = 1.0;
        editor->setValue(v);
    }

    void commit(Variant::Write &target) override
    {
        output(target).remove(false);
        if (!isSet())
            return;
        if (isExplicitlyUndefined()) {
            output(target).setDouble(FP::undefined());
            return;
        }
        output(target).setDouble(editor->value());
    }

    virtual void setUnassigned(double unassigned)
    {
        if (FP::defined(override))
            return;
        if (!FP::defined(input().toDouble()))
            editor->setValue(unassigned);
    }

protected:
    virtual void setComponentEnabled(bool enabled)
    { editor->setEnabled(enabled); }
};
}

void AcquisitionComponentPageGeneric::addDoubleSpinBox(const QString &label,
                                                       const QString &path,
                                                       double override)
{ addWidget(new PageWidgetDoubleSpinBox(label, path, initialConfiguration, override)); }


namespace {
class PageWidgetSpinBoxArrayDelegate : public QStyledItemDelegate {
public:
    int decimals;
    double minimum;
    double maximum;
    double step;
    double unassigned;

    PageWidgetSpinBoxArrayDelegate(QObject *parent) : QStyledItemDelegate(parent),
                                                      decimals(3),
                                                      minimum(0.001),
                                                      maximum(99.999),
                                                      step(0.1),
                                                      unassigned(1.0)
    { }

    virtual ~PageWidgetSpinBoxArrayDelegate() = default;

    virtual QWidget *createEditor(QWidget *parent,
                                  const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const
    {
        Q_UNUSED(option);
        Q_UNUSED(index);

        QDoubleSpinBox *editor = new QDoubleSpinBox(parent);
        editor->setFrame(false);
        editor->setDecimals(decimals);
        editor->setRange(minimum, maximum);
        editor->setSingleStep(step);

        return editor;
    }

    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
    {
        double value = index.model()->data(index, Qt::EditRole).toDouble();
        if (!FP::defined(value))
            value = unassigned;
        QDoubleSpinBox *spinBox = static_cast<QDoubleSpinBox *>(editor);
        spinBox->setDecimals(decimals);
        spinBox->setRange(minimum, maximum);
        spinBox->setSingleStep(step);
        spinBox->setValue(value);
    }

    virtual void setModelData(QWidget *editor,
                              QAbstractItemModel *model,
                              const QModelIndex &index) const
    {
        QDoubleSpinBox *spinBox = static_cast<QDoubleSpinBox *>(editor);
        spinBox->interpretText();
        setValue(model, index, spinBox->value());
    }

    virtual void updateEditorGeometry(QWidget *editor,
                                      const QStyleOptionViewItem &option,
                                      const QModelIndex &index) const
    {
        Q_UNUSED(index);
        editor->setGeometry(option.rect);
    }

    void setValue(QAbstractItemModel *model, const QModelIndex &index, double value) const
    {
        if (!FP::defined(value))
            value = unassigned;
        model->setData(index, value, Qt::EditRole);

        NumberFormat fmt(1, decimals);
        model->setData(index, fmt.apply(value), Qt::DisplayRole);
    }
};

class PageWidgetSpinBoxArray : public Internal::AcquisitionComponentPageGenericBox {
    double override;
    QTableView *editor;
    QStandardItemModel *model;
    PageWidgetSpinBoxArrayDelegate *delegate;

    void reconfigure()
    {
        for (int i = 0, max = model->rowCount(); i < max; i++) {
            delegate->setValue(model, model->index(i, 0), input().array(i).toDouble());
        }
    }

public:
    virtual ~PageWidgetSpinBoxArray() = default;

    PageWidgetSpinBoxArray(const QString &label,
                           const QString &path, const Variant::Read &config,
                           int count,
                           double o) : Internal::AcquisitionComponentPageGenericBox(label, path,
                                                                                    config),
                                       override(o)
    {
        QVBoxLayout *layout = new QVBoxLayout(layoutTarget());
        layoutTarget()->setLayout(layout);

        model = new QStandardItemModel(count, 1, this);
        editor = new QTableView(this);
        layout->addWidget(editor);
        editor->setModel(model);

        delegate = new PageWidgetSpinBoxArrayDelegate(editor);
        editor->setItemDelegate(delegate);

        reconfigure();
    }

    void commit(Variant::Write &target) override
    {
        output(target).remove(false);
        if (!isSet())
            return;
        for (int i = 0, max = model->rowCount(); i < max; i++) {
            bool ok = false;
            double v = model->data(model->index(i, 0)).toDouble(&ok);
            if (!ok)
                v = FP::undefined();
            if (!FP::defined(v))
                v = delegate->unassigned;
            output(target).array(i).setDouble(v);
        }
    }

    virtual void setDoubleSpinBox(int decimals, double minimum, double maximum, double step)
    {
        delegate->decimals = decimals;
        delegate->minimum = minimum;
        delegate->maximum = maximum;
        delegate->step = step;
        reconfigure();
    }

    virtual void setUnassigned(double unassigned)
    {
        if (FP::defined(override))
            return;
        delegate->unassigned = unassigned;
        reconfigure();
    }

protected:
    virtual void setComponentEnabled(bool enabled)
    { editor->setEnabled(enabled); }
};
}

void AcquisitionComponentPageGeneric::addDoubleSpinBoxArray(const QString &label,
                                                            const QString &path,
                                                            int count,
                                                            double override)
{ addWidget(new PageWidgetSpinBoxArray(label, path, initialConfiguration, count, override)); }

namespace {
class PageWidgetIntegerSpinBox : public Internal::AcquisitionComponentPageGenericOneLine {
    qint64 override;

    class FormattedSpinBox : public QSpinBox {
    public:
        NumberFormat format;

        FormattedSpinBox(QWidget *parent = 0) : QSpinBox(parent), format(1, 0)
        { }

        virtual ~FormattedSpinBox() = default;

        virtual QValidator::State validate(QString &text, int &pos) const
        {
            if (text.startsWith("0i", Qt::CaseInsensitive)) {
                if (text.length() == 2)
                    return QValidator::Intermediate;
                bool ok = false;
                text.mid(2).toInt(&ok, 10);
                return ok ? QValidator::Acceptable : QValidator::Invalid;
            } else if (text.startsWith("0x")) {
                if (text.length() == 2)
                    return QValidator::Intermediate;
                bool ok = false;
                text.mid(2).toInt(&ok, 16);
                return ok ? QValidator::Acceptable : QValidator::Invalid;
            } else if (text.startsWith("0o")) {
                if (text.length() == 2)
                    return QValidator::Intermediate;
                bool ok = false;
                text.mid(2).toInt(&ok, 8);
                return ok ? QValidator::Acceptable : QValidator::Invalid;
            } else if (text.startsWith("0b")) {
                if (text.length() == 2)
                    return QValidator::Intermediate;
                bool ok = false;
                text.mid(2).toInt(&ok, 2);
                return ok ? QValidator::Acceptable : QValidator::Invalid;
            }

            bool ok = false;
            switch (format.getMode()) {
            case NumberFormat::Decimal:
            case NumberFormat::Scientific:
                text.toInt(&ok, 10);
                break;
            case NumberFormat::Hex:
                text.toInt(&ok, 16);
                break;
            case NumberFormat::Octal:
                text.toInt(&ok, 8);
                break;
            }
            return ok ? QValidator::Acceptable : QValidator::Invalid;
        }

    protected:
        virtual QString textFromValue(int value) const
        { return format.apply(value, QChar()); }

        virtual int valueFromText(const QString &text) const
        {
            if (text.startsWith("0i", Qt::CaseInsensitive)) {
                return text.mid(2).toInt(NULL, 10);
            } else if (text.startsWith("0x")) {
                return text.mid(2).toInt(NULL, 16);
            } else if (text.startsWith("0o")) {
                return text.mid(2).toInt(NULL, 8);
            } else if (text.startsWith("0b")) {
                return text.mid(2).toInt(NULL, 2);
            }
            switch (format.getMode()) {
            case NumberFormat::Decimal:
            case NumberFormat::Scientific:
                return text.toInt(NULL, 10);
            case NumberFormat::Hex:
                return text.toInt(NULL, 16);
            case NumberFormat::Octal:
                return text.toInt(NULL, 8);
            }
            return text.toInt();
        }
    };

    FormattedSpinBox *editor;
public:
    virtual ~PageWidgetIntegerSpinBox() = default;

    PageWidgetIntegerSpinBox(const QString &label,
                             const QString &path, const Variant::Read &config,
                             qint64 o) : Internal::AcquisitionComponentPageGenericOneLine(label,
                                                                                          path,
                                                                                          config),
                                         override(o)
    {
        editor = new FormattedSpinBox(this);
        addLineItem(editor);
        qint64 v = input().toInt64();
        if (INTEGER::defined(override))
            v = override;
        if (!INTEGER::defined(v))
            v = 1;
        editor->setValue((int) v);
    }

    virtual void setIntegerSpinBox(int minimum,
                                   int maximum,
                                   const NumberFormat &format = NumberFormat(1, 0))
    {
        editor->setRange(minimum, maximum);
        qint64 v = input().toInt64();
        if (INTEGER::defined(override))
            v = override;
        if (!INTEGER::defined(v))
            v = 1;
        editor->format = format;
        editor->setValue((int) v);
    }

    void commit(Variant::Write &target) override
    {
        output(target).remove(false);
        if (!isSet())
            return;
        if (isExplicitlyUndefined()) {
            output(target).setInt64(INTEGER::undefined());
            return;
        }
        output(target).setInt64(editor->value());
    }

    virtual void setUnassigned(qint64 unassigned)
    {
        if (FP::defined(override))
            return;
        if (!FP::defined(input().toDouble()))
            editor->setValue((int) unassigned);
    }

protected:
    virtual void setComponentEnabled(bool enabled)
    { editor->setEnabled(enabled); }
};
}

void AcquisitionComponentPageGeneric::addIntegerSpinBox(const QString &label,
                                                        const QString &path,
                                                        qint64 override)
{ addWidget(new PageWidgetIntegerSpinBox(label, path, initialConfiguration, override)); }


namespace {
class PageWidgetCalibration : public Internal::AcquisitionComponentPageGenericBox {
    CalibrationEdit *editor;
public:
    virtual ~PageWidgetCalibration() = default;

    PageWidgetCalibration(const QString &label, const QString &path, const Variant::Read &config)
            : Internal::AcquisitionComponentPageGenericBox(label, path, config)
    {
        QVBoxLayout *layout = new QVBoxLayout(layoutTarget());
        layoutTarget()->setLayout(layout);

        editor = new CalibrationEdit(layoutTarget());
        layout->addWidget(editor);
        editor->setCalibration(Variant::Composite::toCalibration(input()));
    }

    void commit(Variant::Write &target) override
    {
        output(target).remove(false);
        if (!isSet())
            return;
        Variant::Composite::fromCalibration(output(target), editor->getCalibration());
    }

protected:
    virtual void setComponentEnabled(bool enabled)
    { editor->setEnabled(enabled); }
};
}

void AcquisitionComponentPageGeneric::addCalibration(const QString &label, const QString &path)
{ addWidget(new PageWidgetCalibration(label, path, initialConfiguration)); }


void AcquisitionComponentPageGeneric::addFlagsSelection(const QString &label, const QString &path)
{
    addWidget(new Internal::AcquisitionComponentPageGenericFlagsSelection(label, path,
                                                                          initialConfiguration));
}


namespace {
class PageWidgetTimeIntervalSelection : public Internal::AcquisitionComponentPageGenericOneLine {
    CPD3::Data::Variant::Read metadata;
    QPushButton *editor;
    CPD3::Data::Variant::Root outputConfiguration;

    void updateText()
    {
        DynamicTimeIntervalEndpoint ep;
        editor->setText(ep.collapsedText(outputConfiguration, metadata));
    }

public:
    virtual ~PageWidgetTimeIntervalSelection() = default;

    PageWidgetTimeIntervalSelection(const QString &label,
                                    const QString &path,
                                    const Variant::Read &config,
                                    const Variant::Read &m)
            : Internal::AcquisitionComponentPageGenericOneLine(label, path, config), metadata(m)
    {
        metadata.detachFromRoot();
        editor = new QPushButton(this);
        addLineItem(editor);
        connect(editor, SIGNAL(clicked(bool)), this, SLOT(changed()));
        outputConfiguration.write().set(input());
        updateText();
    }


    void commit(Variant::Write &target) override
    {
        output(target).remove(false);
        if (!isSet())
            return;
        if (isExplicitlyUndefined()) {
            output(target).setType(Variant::Type::Hash);
            return;
        }
        output(target).set(outputConfiguration);
    }

protected:
    virtual void setComponentEnabled(bool enabled)
    { editor->setEnabled(enabled); }

    virtual void changed()
    {
        auto editor = metadata.metadata("Editor");

        QDialog dialog(this);
        dialog.setWindowTitle(QObject::tr("Time Interval"));
        QVBoxLayout *layout = new QVBoxLayout(&dialog);
        dialog.setLayout(layout);

        DynamicTimeIntervalEditor
                *selector = new DynamicTimeIntervalEditor(outputConfiguration, metadata,
                                                editor.hash("DefaultTimeInterval"), editor,
                                                &dialog);
        layout->addWidget(selector);

        layout->addStretch(1);

        QDialogButtonBox *buttons =
                new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                                     Qt::Horizontal, &dialog);
        layout->addWidget(buttons);
        QObject::connect(buttons, SIGNAL(accepted()), &dialog, SLOT(accept()));
        QObject::connect(buttons, SIGNAL(rejected()), &dialog, SLOT(reject()));

        if (dialog.exec() != QDialog::Accepted)
            return;

        outputConfiguration.write().setEmpty();
        outputConfiguration.write().set(selector->timeInterval());
        updateText();
    }
};
}

void AcquisitionComponentPageGeneric::addTimeIntervalSelection(const QString &label,
                                                               const QString &path,
                                                               const Variant::Read &metadata)
{ addWidget(new PageWidgetTimeIntervalSelection(label, path, initialConfiguration, metadata)); }


namespace {
class PageWidgetSmoother : public Internal::AcquisitionComponentPageGenericOneLine {
    CPD3::Data::Variant::Read metadata;
    QPushButton *editor;
    CPD3::Data::Variant::Root outputConfiguration;

public:
    virtual ~PageWidgetSmoother() = default;

    PageWidgetSmoother(const QString &label,
                       const QString &path, const Variant::Read &config, const Variant::Read &m)
            : Internal::AcquisitionComponentPageGenericOneLine(label,
                                                                                          path,
                                                                                          config),
                                         metadata(m)
    {
        metadata.detachFromRoot();
        editor = new QPushButton(tr("Configure"), this);
        addLineItem(editor);
        connect(editor, SIGNAL(clicked(bool)), this, SLOT(changed()));
        outputConfiguration.write().set(input());
    }


    void commit(Variant::Write &target) override
    {
        output(target).remove(false);
        if (!isSet())
            return;
        if (isExplicitlyUndefined()) {
            output(target).setType(Variant::Type::Hash);
            return;
        }
        output(target).set(outputConfiguration);
    }

protected:
    virtual void setComponentEnabled(bool enabled)
    { editor->setEnabled(enabled); }

    virtual void changed()
    {
        QDialog dialog(this);
        dialog.setWindowTitle(QObject::tr("Smoother"));
        auto layout = new QVBoxLayout(&dialog);
        dialog.setLayout(layout);

        auto editor = new BaselineSmootherEditor(outputConfiguration, metadata, &dialog);
        layout->addWidget(editor, 1);

        auto buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                                            Qt::Horizontal, &dialog);
        layout->addWidget(buttons);
        QObject::connect(buttons, &QDialogButtonBox::accepted, &dialog, &QDialog::accept);
        QObject::connect(buttons, &QDialogButtonBox::rejected, &dialog, &QDialog::reject);

        if (dialog.exec() != QDialog::Accepted)
            return;

        outputConfiguration.write().setEmpty();
        outputConfiguration.write().set(editor->smoother());
    }
};
}

void AcquisitionComponentPageGeneric::addSmoother(const QString &label,
                                                  const QString &path,
                                                  const Variant::Read &metadata)
{ addWidget(new PageWidgetSmoother(label, path, initialConfiguration, metadata)); }


namespace {
class PageWidgetDynamicInput : public Internal::AcquisitionComponentPageGenericOneLine {
    CPD3::Data::Variant::Read metadata;
    QPushButton *editor;
    CPD3::Data::Variant::Root outputConfiguration;

public:
    virtual ~PageWidgetDynamicInput() = default;

    PageWidgetDynamicInput(const QString &label,
                           const QString &path, const Variant::Read &config, const Variant::Read &m)
            : Internal::AcquisitionComponentPageGenericOneLine(label,
                                                                                              path,
                                                                                              config),
                                             metadata(m)
    {
        metadata.detachFromRoot();
        editor = new QPushButton(tr("Configure"), this);
        addLineItem(editor);
        connect(editor, SIGNAL(clicked(bool)), this, SLOT(changed()));
        outputConfiguration.write().set(input());
    }


    void commit(Variant::Write &target) override
    {
        output(target).remove(false);
        if (!isSet())
            return;
        if (isExplicitlyUndefined()) {
            output(target).setType(Variant::Type::Hash);
            return;
        }
        output(target).set(outputConfiguration);
    }

protected:
    virtual void setComponentEnabled(bool enabled)
    { editor->setEnabled(enabled); }

    virtual void changed()
    {
        DynamicInputEditorDialog dialog(outputConfiguration, metadata, this);
        if (dialog.exec() != QDialog::Accepted)
            return;
        outputConfiguration.write().setEmpty();
        outputConfiguration.write().set(dialog.input());
    }
};
}

void AcquisitionComponentPageGeneric::addDynamicInput(const QString &label,
                                                      const QString &path,
                                                      const Variant::Read &metadata)
{ addWidget(new PageWidgetDynamicInput(label, path, initialConfiguration, metadata)); }


namespace {
class PageWidgetDynamicSelection : public Internal::AcquisitionComponentPageGenericOneLine {
    QPushButton *editor;
    CPD3::Data::Variant::Root outputConfiguration;

public:
    virtual ~PageWidgetDynamicSelection() = default;

    PageWidgetDynamicSelection(const QString &label,
                               const QString &path,
                               const Variant::Read &config)
            : Internal::AcquisitionComponentPageGenericOneLine(label, path, config)
    {
        editor = new QPushButton(tr("Select"), this);
        addLineItem(editor);
        connect(editor, SIGNAL(clicked(bool)), this, SLOT(changed()));
        outputConfiguration.write().set(input());
    }


    void commit(Variant::Write &target) override
    {
        output(target).remove(false);
        if (!isSet())
            return;
        if (isExplicitlyUndefined()) {
            output(target).setType(Variant::Type::Hash);
            return;
        }
        output(target).set(outputConfiguration);
    }

protected:
    virtual void setComponentEnabled(bool enabled)
    { editor->setEnabled(enabled); }

    virtual void changed()
    {
        QDialog dialog(this);
        dialog.setWindowTitle(QObject::tr("Dynamic Sequence Selection"));
        QVBoxLayout *layout = new QVBoxLayout(&dialog);
        dialog.setLayout(layout);

        VariableSelect *selector = new VariableSelect(&dialog);
        selector->configureFromDynamicSelection(outputConfiguration);
        selector->setDefaultAvailable();
        layout->addWidget(selector, 1);

        QDialogButtonBox *buttons =
                new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                                     Qt::Horizontal, &dialog);
        layout->addWidget(buttons);
        QObject::connect(buttons, SIGNAL(accepted()), &dialog, SLOT(accept()));
        QObject::connect(buttons, SIGNAL(rejected()), &dialog, SLOT(reject()));

        if (dialog.exec() != QDialog::Accepted)
            return;

        outputConfiguration.write().setEmpty();
        selector->writeDynamicSelection(outputConfiguration.write());
    }
};
}

void AcquisitionComponentPageGeneric::addDynamicSelection(const QString &label, const QString &path)
{ addWidget(new PageWidgetDynamicSelection(label, path, initialConfiguration)); }


namespace {
class PageWidgetGenericValue : public Internal::AcquisitionComponentPageGenericOneLine {
    CPD3::Data::Variant::Read metadata;
    QPushButton *editor;
    CPD3::Data::Variant::Root outputConfiguration;

public:
    virtual ~PageWidgetGenericValue() = default;

    PageWidgetGenericValue(const QString &label,
                           const QString &path, const Variant::Read &config, const Variant::Read &m)
            : Internal::AcquisitionComponentPageGenericOneLine(label,
                                                                                              path,
                                                                                              config),
                                             metadata(m)
    {
        metadata.detachFromRoot();
        editor = new QPushButton(tr("Set"), this);
        addLineItem(editor);
        connect(editor, SIGNAL(clicked(bool)), this, SLOT(changed()));
        outputConfiguration.write().set(input());
    }


    void commit(Variant::Write &target) override
    {
        output(target).remove(false);
        if (!isSet())
            return;
        if (isExplicitlyUndefined()) {
            output(target).setType(Variant::Type::Hash);
            return;
        }
        output(target).set(outputConfiguration);
    }

protected:
    virtual void setComponentEnabled(bool enabled)
    { editor->setEnabled(enabled); }

    virtual void changed()
    {
        QDialog dialog(this);
        dialog.setWindowTitle(tr("Edit Value"));
        QVBoxLayout *layout = new QVBoxLayout(&dialog);
        dialog.setLayout(layout);

        ValueEditor *advancedEditor = new ValueEditor(&dialog);
        Variant::Root editedConfiguration(outputConfiguration);
        advancedEditor->setValue(editedConfiguration.write(), metadata);
        layout->addWidget(advancedEditor, 1);

        QDialogButtonBox *buttons =
                new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                                     Qt::Horizontal, &dialog);
        layout->addWidget(buttons);
        QObject::connect(buttons, SIGNAL(accepted()), &dialog, SLOT(accept()));
        QObject::connect(buttons, SIGNAL(rejected()), &dialog, SLOT(reject()));

        if (dialog.exec() != QDialog::Accepted)
            return;
        outputConfiguration.write().setEmpty();
        outputConfiguration.write().set(editedConfiguration);
    }
};
}

void AcquisitionComponentPageGeneric::addGenericValue(const QString &label,
                                                      const QString &path,
                                                      const Variant::Read &metadata)
{ addWidget(new PageWidgetGenericValue(label, path, initialConfiguration, metadata)); }


namespace {
class PageWidgetGenericDialog : public Internal::AcquisitionComponentPageGenericOneLine {
    std::function<void(Variant::Write &, QWidget *)> showDialog;
    QPushButton *editor;
    CPD3::Data::Variant::Write outputConfiguration;

public:
    virtual ~PageWidgetGenericDialog() = default;

    PageWidgetGenericDialog(const QString &label,
                            const QString &path,
                            const Variant::Read &config,
                            std::function<void(Variant::Write &, QWidget *)> show,
                            const QString &buttonText)
            : Internal::AcquisitionComponentPageGenericOneLine(label, path, config),
              showDialog(std::move(show)),
              editor(nullptr),
              outputConfiguration(Variant::Write::empty())
    {
        editor = new QPushButton(buttonText, this);
        addLineItem(editor);
        connect(editor, SIGNAL(clicked(bool)), this, SLOT(changed()));
        outputConfiguration.set(input());
    }


    void commit(Variant::Write &target) override
    {
        output(target).remove(false);
        if (!isSet())
            return;
        if (isExplicitlyUndefined()) {
            output(target).setEmpty();
            return;
        }
        output(target).set(outputConfiguration);
    }

protected:
    virtual void setComponentEnabled(bool enabled)
    { editor->setEnabled(enabled); }

    virtual void changed()
    { this->showDialog(outputConfiguration, this); }
};
}

void AcquisitionComponentPageGeneric::addGenericDialog(const QString &label,
                                                       const QString &path,
                                                       std::function<void(Variant::Write &,
                                                                          QWidget *)> dialog,
                                                       const QString &buttonText)
{
    addWidget(new PageWidgetGenericDialog(label, path, initialConfiguration, std::move(dialog),
                                          buttonText));
}


namespace Internal {

AcquisitionComponentPageGenericWidget::AcquisitionComponentPageGenericWidget(const QString &p,
                                                                             const Variant::Read &config)
        : path(p), initialConfiguration(config)
{ }

AcquisitionComponentPageGenericWidget::~AcquisitionComponentPageGenericWidget() = default;

void AcquisitionComponentPageGenericWidget::setUnassigned(bool unassigned)
{ Q_UNUSED(unassigned); }

void AcquisitionComponentPageGenericWidget::setUnassigned(double unassigned)
{ Q_UNUSED(unassigned); }

void AcquisitionComponentPageGenericWidget::setUnassigned(qint64 unassigned)
{ Q_UNUSED(unassigned); }

void AcquisitionComponentPageGenericWidget::setUnassigned(const QString &unassigned)
{ Q_UNUSED(unassigned); }

void AcquisitionComponentPageGenericWidget::setExplicitUndefined(bool enable)
{ Q_UNUSED(enable); }

void AcquisitionComponentPageGenericWidget::setDoubleSpinBox(int decimals,
                                                             double minimum,
                                                             double maximum,
                                                             double step)
{
    Q_UNUSED(decimals);
    Q_UNUSED(minimum);
    Q_UNUSED(maximum);
    Q_UNUSED(step);
}

void AcquisitionComponentPageGenericWidget::setIntegerSpinBox(int minimum,
                                                              int maximum,
                                                              const NumberFormat &format)
{
    Q_UNUSED(minimum);
    Q_UNUSED(maximum);
    Q_UNUSED(format);
}

void AcquisitionComponentPageGenericWidget::appendEnumEntry(const QString &label,
                                                            const QStringList &keys,
                                                            const QString &toolTip)
{
    Q_UNUSED(label);
    Q_UNUSED(keys);
    Q_UNUSED(toolTip);
}

void AcquisitionComponentPageGenericWidget::changed()
{ }


AcquisitionComponentPageGenericOneLine::AcquisitionComponentPageGenericOneLine(const QString &label,
                                                                               const QString &path,
                                                                               const Variant::Read &config)
        : AcquisitionComponentPageGenericWidget(path, config)
{
    activeLayout = new QHBoxLayout(this);
    activeLayout->setContentsMargins(0, 0, 0, 0);
    setLayout(activeLayout);

    enabledChecked = new QCheckBox(label, this);
    activeLayout->addWidget(enabledChecked);

    reconfigure();
    connect(enabledChecked, SIGNAL(stateChanged(int)), this, SLOT(checkedChanged()));
    QMetaObject::invokeMethod(this, "checkedChanged", Qt::QueuedConnection);
}

AcquisitionComponentPageGenericOneLine::~AcquisitionComponentPageGenericOneLine() = default;

void AcquisitionComponentPageGenericOneLine::addLineItem(QWidget *item, int stretch)
{ activeLayout->addWidget(item, stretch); }

void AcquisitionComponentPageGenericOneLine::addStretch(int stretch)
{ activeLayout->addStretch(stretch); }

void AcquisitionComponentPageGenericOneLine::setComponentEnabled(bool enabled)
{ Q_UNUSED(enabled); }

void AcquisitionComponentPageGenericOneLine::checkedChanged()
{ setComponentEnabled(enabledChecked->checkState() == Qt::Checked); }

void AcquisitionComponentPageGenericOneLine::setExplicitUndefined(bool enable)
{
    enabledChecked->setTristate(enable);
    reconfigure();
}

void AcquisitionComponentPageGenericOneLine::reconfigure()
{
    if (!enabledChecked->isTristate()) {
        enabledChecked->setChecked(input().exists());
    } else {
        auto v = input();
        if (!v.exists()) {
            enabledChecked->setCheckState(Qt::Unchecked);
        } else {
            enabledChecked->setCheckState(v.exists() ? Qt::Checked : Qt::PartiallyChecked);
        }
    }
}

AcquisitionComponentPageGenericBox::AcquisitionComponentPageGenericBox(const QString &label,
                                                                       const QString &path,
                                                                       const Variant::Read &config)
        : AcquisitionComponentPageGenericWidget(path, config)
{
    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    setLayout(layout);

    box = new QGroupBox(label, this);
    layout->addWidget(box);
    box->setCheckable(true);
    box->setChecked(input().exists());

    connect(box, SIGNAL(toggled(bool)), this, SLOT(checkedChanged()));
    QMetaObject::invokeMethod(this, "checkedChanged", Qt::QueuedConnection);
}

AcquisitionComponentPageGenericBox::~AcquisitionComponentPageGenericBox() = default;

void AcquisitionComponentPageGenericBox::setComponentEnabled(bool enabled)
{ Q_UNUSED(enabled); }

void AcquisitionComponentPageGenericBox::checkedChanged()
{ setComponentEnabled(box->isChecked()); }

AcquisitionComponentPageGenericFlagsSelection::AcquisitionComponentPageGenericFlagsSelection(const QString &label,
                                                                                             const QString &path,
                                                                                             const CPD3::Data::Variant::Read &config)
        : AcquisitionComponentPageGenericOneLine(label, path, config)
{
    button = new QPushButton(this);
    addLineItem(button);

    menu = new QMenu(button);
    delimiter = menu->addSeparator();
    QAction *addFlagAction = menu->addAction(tr("&Add"));
    connect(addFlagAction, SIGNAL(triggered(bool)), this, SLOT(addFlag()));

    const auto &selectedFlags = input().toFlags();
    if (selectedFlags.empty()) {
        connect(button, SIGNAL(clicked(bool)), this, SLOT(addFlag()));
    } else {
        button->setMenu(menu);
        QStringList sorted;
        for (const auto &add : selectedFlags) {
            sorted.push_back(QString::fromStdString(add));
        }
        std::sort(sorted.begin(), sorted.end());
        for (const auto &flag : sorted) {
            QAction *action = new QAction(flag, menu);
            menu->insertAction(delimiter, action);
            flags.insert(flag, action);

            action->setCheckable(true);
            action->setChecked(true);

            connect(action, SIGNAL(toggled(bool)), this, SLOT(flagToggled()));
        }
    }
    flagToggled();
}

AcquisitionComponentPageGenericFlagsSelection::~AcquisitionComponentPageGenericFlagsSelection() = default;

void AcquisitionComponentPageGenericFlagsSelection::addFlag()
{
    bool ok;
    QString flag
            (QInputDialog::getText(this, tr("Add"), tr("Add:"), QLineEdit::Normal, QString(), &ok));
    if (!ok || flag.isEmpty())
        return;

    QHash<QString, QAction *>::const_iterator check = flags.constFind(flag);
    if (check != flags.constEnd()) {
        check.value()->setChecked(true);
        return;
    }

    QAction *action = new QAction(flag, menu);
    menu->insertAction(delimiter, action);
    flags.insert(flag, action);

    action->setCheckable(true);
    action->setChecked(true);

    button->disconnect(this);
    button->setMenu(menu);
    connect(action, SIGNAL(toggled(bool)), this, SLOT(flagToggled()));
}

void AcquisitionComponentPageGenericFlagsSelection::flagToggled()
{
    QSet<QString> selectedFlags;
    for (QHash<QString, QAction *>::const_iterator flag = flags.constBegin(),
            endF = flags.constEnd(); flag != endF; ++flag) {
        if (!flag.value()->isChecked())
            continue;
        selectedFlags.insert(flag.key());
    }
    if (selectedFlags.size() != 1) {
        button->setText(tr("Select"));
    } else {
        button->setText(*(selectedFlags.constBegin()));
    }
}

void AcquisitionComponentPageGenericFlagsSelection::commit(CPD3::Data::Variant::Write &target)
{
    output(target).remove(false);
    if (!isSet())
        return;
    if (isExplicitlyUndefined()) {
        output(target).setType(Variant::Type::Flags);
        return;
    }
    Variant::Flags selectedFlags;
    for (QHash<QString, QAction *>::const_iterator flag = flags.constBegin(),
            endF = flags.constEnd(); flag != endF; ++flag) {
        if (!flag.value()->isChecked())
            continue;
        selectedFlags.insert(flag.key().toStdString());
    }
    output(target).setFlags(selectedFlags);
}

}

}
}
}