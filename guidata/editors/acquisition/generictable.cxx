/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QVBoxLayout>
#include <QDialog>
#include <QDialogButtonBox>
#include <QPushButton>
#include <QHeaderView>
#include <QStyledItemDelegate>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QComboBox>
#include <QDialog>
#include <QDialogButtonBox>
#include <QTabWidget>
#include <QFormLayout>
#include <QInputDialog>
#include <QRegularExpression>

#include "guidata/editors/acquisition/generictable.hxx"
#include "guidata/editors/dynamicinput.hxx"
#include "guicore/calibrationedit.hxx"
#include "guicore/dynamictimeinterval.hxx"
#include "guidata/valueeditor.hxx"
#include "datacore/variant/composite.hxx"

using namespace CPD3::Data;
using namespace CPD3::GUI::Data::Internal;

namespace CPD3 {
namespace GUI {
namespace Data {

/** @file guidata/editors/acquisition/generictable.hxx
 * A general purpose editor for acquisition table based configuration.
 */


namespace {
class VariableTableAddRemoveButton : public QPushButton {
public:
    VariableTableAddRemoveButton(const QString &text, QWidget *parent = 0) : QPushButton(text,
                                                                                         parent)
    { }

    virtual ~VariableTableAddRemoveButton()
    { }

    virtual QSize sizeHint() const
    {
        QSize textSize(fontMetrics().size(Qt::TextShowMnemonic, text()));
        QStyleOptionButton opt;
        opt.initFrom(this);
        opt.rect.setSize(textSize);
        return style()->sizeFromContents(QStyle::CT_PushButton, &opt, textSize, this);
    }
};

class VariableTableAddRemoveDelegate : public QItemDelegate {
    AcquisitionComponentGenericTable *table;
public:
    VariableTableAddRemoveDelegate(AcquisitionComponentGenericTable *t, QObject *parent = 0)
            : QItemDelegate(parent), table(t)
    { }

    virtual ~VariableTableAddRemoveDelegate()
    { }

    virtual QWidget *createEditor(QWidget *parent,
                                  const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const
    {
        Q_UNUSED(option);

        QPushButton *button = new VariableTableAddRemoveButton(tr("-", "remove text"), parent);
        button->setProperty("model-row", index.row());

        return button;
    }

    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
    {
        QPushButton *button = static_cast<QPushButton *>(editor);

        button->disconnect(editor);

        QVariant data(index.data(Qt::EditRole));
        if (data.toInt() != 1) {
            button->setText(tr("-", "remove text"));
            connect(button, SIGNAL(clicked(bool)), this->table, SLOT(removeRow()));
        } else {
            button->setText(tr("Add", "add text"));
            connect(button, SIGNAL(clicked(bool)), this->table, SLOT(addRow()));
        }

        button->setProperty("model-row", index.row());
    }

    virtual void setModelData(QWidget *editor,
                              QAbstractItemModel *model,
                              const QModelIndex &index) const
    {
        Q_UNUSED(editor);
        Q_UNUSED(model);
        Q_UNUSED(index);
    }
};

}

AcquisitionComponentGenericTable::AcquisitionComponentGenericTable(const QString &title,
                                                                   const Variant::Read &initial,
                                                                   const QString &path,
                                                                   QWidget *parent)
        : AcquisitionComponentPage(initial, parent),
          pageTitle(title),
          initialValue(initial),
          masterPath(path),
          arrayMode(false),
          arrayFlags(0)
{
    initialValue.detachFromRoot();

    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    setLayout(layout);

    model = new Model(this);
    table = new QTableView(this);
    layout->addWidget(table, 1);
    table->setModel(model);

    table->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    table->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);

    table->setItemDelegateForColumn(0, new VariableTableAddRemoveDelegate(this, table));

    QMetaObject::invokeMethod(this, "initializeTable", Qt::QueuedConnection);
}

AcquisitionComponentGenericTable::~AcquisitionComponentGenericTable()
{
    delete table;
    qDeleteAll(columns);
}

QString AcquisitionComponentGenericTable::title() const
{ return pageTitle; }

void AcquisitionComponentGenericTable::setArrayMode(bool enable, int flags)
{
    this->arrayMode = enable;
    this->arrayFlags = flags;
}

void AcquisitionComponentGenericTable::openAllEditors()
{
    for (int row = 0; row < model->rowCount(); ++row) {
        table->openPersistentEditor(model->index(row, 0));
        if (row == model->rowCount() - 1)
            continue;

        int col = 2;
        for (QList<AcquisitionComponentGenericTableColumn *>::const_iterator
                c = columns.constBegin(), endC = columns.constEnd(); c != endC; ++c, ++col) {
            if (!(*c)->persistentEditors())
                continue;
            table->openPersistentEditor(model->index(row, col));
        }
    }

    table->resizeColumnToContents(0);
}

void AcquisitionComponentGenericTable::addCommitRemovePath(const QString &path, bool emptyOnly)
{ commitRemove.append(CommitRemove(path, emptyOnly)); }

void AcquisitionComponentGenericTable::setLastIntegerSpinBox(int minimum,
                                                             int maximum,
                                                             const NumberFormat &format,
                                                             bool allowUndefined)
{ columns.last()->setIntegerSpinBox(minimum, maximum, format, allowUndefined); }

void AcquisitionComponentGenericTable::setLastDoubleSpinBox(int decimals,
                                                            double minimum,
                                                            double maximum,
                                                            double step,
                                                            bool allowUndefined)
{ columns.last()->setDoubleSpinBox(decimals, minimum, maximum, step, allowUndefined); }

void AcquisitionComponentGenericTable::setLastBoolean(const QString &enabled,
                                                      const QString &disabled,
                                                      bool allowUndefined)
{ columns.last()->setBoolean(enabled, disabled, allowUndefined); }

void AcquisitionComponentGenericTable::setLastString(bool emptyIsUndefined, bool allowUndefined)
{ columns.last()->setString(emptyIsUndefined, allowUndefined); }

void AcquisitionComponentGenericTable::setLastDefaults(const QVariant &display,
                                                       const Variant::Read &creation)
{ columns.last()->setDefaults(display, creation); }

void AcquisitionComponentGenericTable::setLastToolTip(const QString &toolTip)
{ columns.last()->setToolTip(toolTip); }

void AcquisitionComponentGenericTable::setLastStatusTip(const QString &statusTip)
{ columns.last()->setStatusTip(statusTip); }

void AcquisitionComponentGenericTable::setLastWhatsThis(const QString &whatsThis)
{ columns.last()->setWhatsThis(whatsThis); }

void AcquisitionComponentGenericTable::addRow()
{
    model->insertRows(model->rowCount() - 1, 1);
    openAllEditors();
}

void AcquisitionComponentGenericTable::removeRow()
{
    QObject *s = sender();
    if (!s)
        return;
    QVariant p(s->property("model-row"));
    if (!p.isValid())
        return;
    int row = p.toInt();
    if (row < 0 || row >= model->rowCount() - 1)
        return;

    model->removeRows(row, 1);
    openAllEditors();
}

void AcquisitionComponentGenericTable::commit(CPD3::Data::Variant::Write &target)
{
    for (QList<CommitRemove>::const_iterator r = commitRemove.constBegin(),
            endR = commitRemove.constEnd(); r != endR; ++r) {
        if (r->second)
            continue;
        target.getPath(r->first).remove(false);
    }

    model->commit(target);

    for (QList<CommitRemove>::const_iterator r = commitRemove.constBegin(),
            endR = commitRemove.constEnd(); r != endR; ++r) {
        if (!r->second)
            continue;
        auto v = target.getPath(r->first);
        switch (v.getType()) {
        case Variant::Type::Hash:
            if (!v.toHash().empty())
                continue;
            break;
        case Variant::Type::Array:
        case Variant::Type::Matrix:
            if (!v.toArray().empty())
                continue;
            break;
        case Variant::Type::Empty:
            break;
        default:
            continue;
        }
        v.remove(false);
    }
}

void AcquisitionComponentGenericTable::initializeTable()
{
    model->initialize();

    table->setSpan(model->rowCount() - 1, 0, 1, model->columnCount());

    for (int i = 0, max = columns.size() - 1; i < max; i++) {
        table->horizontalHeader()->setSectionResizeMode(i + 2, QHeaderView::ResizeToContents);
        table->resizeColumnToContents(i + 2);
    }
    table->horizontalHeader()->setSectionResizeMode(columns.size() + 1, QHeaderView::Stretch);

    openAllEditors();

    if (!arrayMode) {
        table->setSortingEnabled(true);
        table->sortByColumn(1, Qt::AscendingOrder);
    } else {
        table->setSortingEnabled(false);
    }

    table->resizeColumnToContents(1);
    for (int i = 0, max = columns.size() - 1; i < max; i++) {
        table->resizeColumnToContents(i + 2);
    }
}


namespace {
class GenericColumn : public AcquisitionComponentGenericTableColumn {
public:
    GenericColumn(const QString &title, const QString &path, bool persistent)
            : AcquisitionComponentGenericTableColumn(title, path, persistent)
    { }

    virtual ~GenericColumn()
    { }

    virtual QVariant initializeData(const Variant::Read &input)
    { return QVariant::fromValue(input); }

    void commit(const QVariant &data, Variant::Write &target) override
    {
        target.remove(false);
        if (!data.isValid())
            return;
        target.set(data.value<Variant::Write>());
    }
};
}

void AcquisitionComponentGenericTable::addColumn(const QString &title,
                                                 const QString &path,
                                                 QAbstractItemDelegate *delegate,
                                                 bool persistent)
{
    columns.append(new GenericColumn(title, path, persistent));
    table->setItemDelegateForColumn(columns.size() + 1, delegate);
}


namespace {
class StringColumn : public AcquisitionComponentGenericTableColumn {
    bool emptyIsUndefined;
    bool allowUndefined;
public:
    StringColumn(const QString &title, const QString &path, bool persistent)
            : AcquisitionComponentGenericTableColumn(title, path, persistent),
              emptyIsUndefined(false),
              allowUndefined(true)
    { }

    virtual ~StringColumn()
    { }

    virtual void setString(bool emptyIsUndefined, bool allowUndefined = true)
    {
        this->emptyIsUndefined = emptyIsUndefined;
        this->allowUndefined = allowUndefined;
    }

    virtual QVariant initializeData(const Variant::Read &input)
    {
        if (input.getType() != Variant::Type::String) {
            if (allowUndefined)
                return QVariant();
            if (getCreationValue().exists())
                return getCreationValue().toQString().trimmed();
        }
        QString text(input.toQString().trimmed());
        if (emptyIsUndefined && allowUndefined && text.isEmpty())
            return QVariant();
        return input.toQString();
    }

    void commit(const QVariant &data, Variant::Write &target) override
    {
        target.remove(false);
        if (!data.isValid() && allowUndefined)
            return;
        target.setString(data.toString());
    }

    class Delegate : public QStyledItemDelegate {
        StringColumn *column;
    public:
        Delegate(StringColumn *c, QObject *parent = 0) : QStyledItemDelegate(parent), column(c)
        { }

        virtual ~Delegate()
        { }

        virtual void setModelData(QWidget *editor,
                                  QAbstractItemModel *model,
                                  const QModelIndex &index) const
        {
            QVariant v(editor->property("text"));
            if (v.type() == QVariant::String) {
                QString text(v.toString().trimmed());
                if (column->emptyIsUndefined && text.isEmpty()) {
                    if (!column->allowUndefined)
                        return;
                    model->setData(index, QVariant(), Qt::EditRole);
                    return;
                }
                model->setData(index, text, Qt::EditRole);
                return;
            }
            return QStyledItemDelegate::setModelData(editor, model, index);
        }
    };

    friend class Delegate;
};
}

void AcquisitionComponentGenericTable::addString(const QString &title,
                                                 const QString &path,
                                                 bool persistent)
{
    StringColumn *column = new StringColumn(title, path, persistent);
    columns.append(column);
    table->setItemDelegateForColumn(columns.size() + 1, new StringColumn::Delegate(column, table));
}


namespace {
class IntegerSpinBoxColumn : public AcquisitionComponentGenericTableColumn {
    int minimum;
    int maximum;
    NumberFormat format;
    bool allowUndefined;
public:
    IntegerSpinBoxColumn(const QString &title, const QString &path, bool persistent)
            : AcquisitionComponentGenericTableColumn(title, path, persistent),
              minimum(0),
              maximum(9999),
              format(4, 0),
              allowUndefined(true)
    { }

    virtual ~IntegerSpinBoxColumn()
    { }

    virtual void setIntegerSpinBox(int minimum,
                                   int maximum,
                                   const NumberFormat &format = NumberFormat(1, 0),
                                   bool allowUndefined = true)
    {
        this->minimum = minimum;
        this->maximum = maximum;
        this->format = format;
        this->allowUndefined = allowUndefined;
    }

    virtual QVariant initializeData(const Variant::Read &input)
    {
        qint64 i = input.toInt64();
        if (!INTEGER::defined(i)) {
            if (allowUndefined)
                return QVariant();
            i = getCreationValue().toInt64();
            if (!INTEGER::defined(i))
                i = this->minimum;
        }
        return QVariant((int) i);
    }

    void commit(const QVariant &data, Variant::Write &target) override
    {
        target.remove(false);
        if (!data.isValid() && allowUndefined)
            return;
        target.setInt64(data.toInt());
    }

    virtual QVariant displayData(const QVariant &data)
    {
        if (!data.isValid())
            return AcquisitionComponentGenericTableColumn::displayData(data);
        int i = data.toInt();
        return format.apply(i, QChar());
    }

    class Delegate : public QStyledItemDelegate {
        IntegerSpinBoxColumn *column;

        class FormattedSpinBox : public QSpinBox {
        public:
            NumberFormat format;

            FormattedSpinBox(QWidget *parent = 0) : QSpinBox(parent)
            { }

            virtual ~FormattedSpinBox()
            { }

            virtual QValidator::State validate(QString &text, int &pos) const
            {
                if (text.startsWith("0i", Qt::CaseInsensitive)) {
                    if (text.length() == 2)
                        return QValidator::Intermediate;
                    bool ok = false;
                    text.mid(2).toInt(&ok, 10);
                    return ok ? QValidator::Acceptable : QValidator::Invalid;
                } else if (text.startsWith("0x")) {
                    if (text.length() == 2)
                        return QValidator::Intermediate;
                    bool ok = false;
                    text.mid(2).toInt(&ok, 16);
                    return ok ? QValidator::Acceptable : QValidator::Invalid;
                } else if (text.startsWith("0o")) {
                    if (text.length() == 2)
                        return QValidator::Intermediate;
                    bool ok = false;
                    text.mid(2).toInt(&ok, 8);
                    return ok ? QValidator::Acceptable : QValidator::Invalid;
                } else if (text.startsWith("0b")) {
                    if (text.length() == 2)
                        return QValidator::Intermediate;
                    bool ok = false;
                    text.mid(2).toInt(&ok, 2);
                    return ok ? QValidator::Acceptable : QValidator::Invalid;
                }

                bool ok = false;
                switch (format.getMode()) {
                case NumberFormat::Decimal:
                case NumberFormat::Scientific:
                    text.toInt(&ok, 10);
                    break;
                case NumberFormat::Hex:
                    text.toInt(&ok, 16);
                    break;
                case NumberFormat::Octal:
                    text.toInt(&ok, 8);
                    break;
                }
                return ok ? QValidator::Acceptable : QValidator::Invalid;
            }

        protected:
            virtual QString textFromValue(int value) const
            { return format.apply(value, QChar()); }

            virtual int valueFromText(const QString &text) const
            {
                if (text.startsWith("0i", Qt::CaseInsensitive)) {
                    return text.mid(2).toInt(NULL, 10);
                } else if (text.startsWith("0x")) {
                    return text.mid(2).toInt(NULL, 16);
                } else if (text.startsWith("0o")) {
                    return text.mid(2).toInt(NULL, 8);
                } else if (text.startsWith("0b")) {
                    return text.mid(2).toInt(NULL, 2);
                }
                switch (format.getMode()) {
                case NumberFormat::Decimal:
                case NumberFormat::Scientific:
                    return text.toInt(NULL, 10);
                case NumberFormat::Hex:
                    return text.toInt(NULL, 16);
                case NumberFormat::Octal:
                    return text.toInt(NULL, 8);
                }
                return text.toInt();
            }
        };

    public:
        Delegate(IntegerSpinBoxColumn *c, QObject *parent = 0) : QStyledItemDelegate(parent),
                                                                 column(c)
        { }

        virtual ~Delegate()
        { }

        virtual QWidget *createEditor(QWidget *parent,
                                      const QStyleOptionViewItem &option,
                                      const QModelIndex &index) const
        {
            Q_UNUSED(option);
            Q_UNUSED(index);

            FormattedSpinBox *editor = new FormattedSpinBox(parent);
            editor->format = column->format;
            editor->setFrame(false);
            if (column->allowUndefined) {
                editor->setRange(column->minimum - 1, column->maximum);
                QString text(column->getDefaultDisplay().toString());
                if (text.isEmpty())
                    text = tr("Undefined");
                editor->setSpecialValueText(text);
            } else {
                editor->setRange(column->minimum, column->maximum);
                editor->setSpecialValueText(QString());
            }

            return editor;
        }

        virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
        {
            QVariant value(index.model()->data(index, Qt::EditRole));
            FormattedSpinBox *spinBox = static_cast<FormattedSpinBox *>(editor);
            spinBox->format = column->format;
            if (column->allowUndefined) {
                spinBox->setRange(column->minimum - 1, column->maximum);
                QString text(column->getDefaultDisplay().toString());
                if (text.isEmpty())
                    text = tr("Undefined");
                spinBox->setSpecialValueText(text);
            } else {
                spinBox->setRange(column->minimum, column->maximum);
                spinBox->setSpecialValueText(QString());
            }
            int i = 0;
            if (!value.isValid())
                i = spinBox->minimum();
            else
                i = value.toInt();
            spinBox->setValue(i);
        }

        virtual void setModelData(QWidget *editor,
                                  QAbstractItemModel *model,
                                  const QModelIndex &index) const
        {
            FormattedSpinBox *spinBox = static_cast<FormattedSpinBox *>(editor);
            spinBox->interpretText();
            if (column->allowUndefined && spinBox->value() == spinBox->minimum()) {
                model->setData(index, QVariant());
            } else {
                model->setData(index, QVariant(spinBox->value()));
            }
        }

        virtual void updateEditorGeometry(QWidget *editor,
                                          const QStyleOptionViewItem &option,
                                          const QModelIndex &index) const
        {
            Q_UNUSED(index);
            editor->setGeometry(option.rect);
        }
    };

    friend class Delegate;
};
}

void AcquisitionComponentGenericTable::addIntegerSpinBox(const QString &title,
                                                         const QString &path,
                                                         bool persistent)
{
    IntegerSpinBoxColumn *column = new IntegerSpinBoxColumn(title, path, persistent);
    columns.append(column);
    table->setItemDelegateForColumn(columns.size() + 1,
                                    new IntegerSpinBoxColumn::Delegate(column, table));
}


namespace {
class DoubleSpinBoxColumn : public AcquisitionComponentGenericTableColumn {
    int decimals;
    double minimum;
    double maximum;
    double step;
    bool allowUndefined;
public:
    DoubleSpinBoxColumn(const QString &title, const QString &path, bool persistent)
            : AcquisitionComponentGenericTableColumn(title, path, persistent),
              decimals(1),
              minimum(0),
              maximum(1),
              step(0.1),
              allowUndefined(true)
    { }

    virtual ~DoubleSpinBoxColumn()
    { }

    virtual void setDoubleSpinBox(int decimals,
                                  double minimum,
                                  double maximum,
                                  double step,
                                  bool allowUndefined = true)
    {
        this->decimals = decimals;
        this->minimum = minimum;
        this->maximum = maximum;
        this->step = step;
        this->allowUndefined = allowUndefined;
    }

    virtual QVariant initializeData(const Variant::Read &input)
    {
        double d = input.toDouble();
        if (!FP::defined(d)) {
            if (allowUndefined)
                return QVariant();
            d = getCreationValue().toDouble();
            if (!FP::defined(d))
                d = this->minimum;
        }
        return QVariant((double) d);
    }

    void commit(const QVariant &data, Variant::Write &target) override
    {
        target.remove(false);
        if (!data.isValid() && allowUndefined)
            return;
        target.setDouble(data.toDouble());
    }

    class Delegate : public QStyledItemDelegate {
        DoubleSpinBoxColumn *column;

        double undefinedMinimum() const
        {
            return column->minimum - ::pow(10, column->decimals * -1);
        }

    public:
        Delegate(DoubleSpinBoxColumn *c, QObject *parent = 0) : QStyledItemDelegate(parent),
                                                                column(c)
        { }

        virtual ~Delegate()
        { }

        virtual QWidget *createEditor(QWidget *parent,
                                      const QStyleOptionViewItem &option,
                                      const QModelIndex &index) const
        {
            Q_UNUSED(option);
            Q_UNUSED(index);

            QDoubleSpinBox *editor = new QDoubleSpinBox(parent);
            editor->setFrame(false);
            editor->setDecimals(column->decimals);
            if (column->allowUndefined) {
                editor->setRange(undefinedMinimum(), column->maximum);
                QString text(column->getDefaultDisplay().toString());
                if (text.isEmpty())
                    text = tr("Undefined");
                editor->setSpecialValueText(text);
            } else {
                editor->setRange(column->minimum, column->maximum);
                editor->setSpecialValueText(QString());
            }

            return editor;
        }

        virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
        {
            QVariant value(index.model()->data(index, Qt::EditRole));
            QDoubleSpinBox *spinBox = static_cast<QDoubleSpinBox *>(editor);
            spinBox->setDecimals(column->decimals);
            if (column->allowUndefined) {
                spinBox->setRange(undefinedMinimum(), column->maximum);
                QString text(column->getDefaultDisplay().toString());
                if (text.isEmpty())
                    text = tr("Undefined");
                spinBox->setSpecialValueText(text);
            } else {
                spinBox->setRange(column->minimum, column->maximum);
                spinBox->setSpecialValueText(QString());
            }
            double d = 0;
            if (!value.isValid())
                d = spinBox->minimum();
            else
                d = value.toDouble();
            spinBox->setValue(d);
        }

        virtual void setModelData(QWidget *editor,
                                  QAbstractItemModel *model,
                                  const QModelIndex &index) const
        {
            QDoubleSpinBox *spinBox = static_cast<QDoubleSpinBox *>(editor);
            spinBox->interpretText();
            if (column->allowUndefined && spinBox->value() == spinBox->minimum()) {
                model->setData(index, QVariant());
            } else {
                model->setData(index, QVariant(spinBox->value()));
            }
        }

        virtual void updateEditorGeometry(QWidget *editor,
                                          const QStyleOptionViewItem &option,
                                          const QModelIndex &index) const
        {
            Q_UNUSED(index);
            editor->setGeometry(option.rect);
        }
    };

    friend class Delegate;
};
}

void AcquisitionComponentGenericTable::addDoubleSpinBox(const QString &title,
                                                        const QString &path,
                                                        bool persistent)
{
    DoubleSpinBoxColumn *column = new DoubleSpinBoxColumn(title, path, persistent);
    columns.append(column);
    table->setItemDelegateForColumn(columns.size() + 1,
                                    new DoubleSpinBoxColumn::Delegate(column, table));
}


namespace {
class BooleanColumn : public AcquisitionComponentGenericTableColumn {
    QString enabledText;
    QString disabledText;
    bool allowUndefined;
public:
    BooleanColumn(const QString &title, const QString &path, bool persistent)
            : AcquisitionComponentGenericTableColumn(title, path, persistent),
              enabledText(AcquisitionComponentGenericTable::tr("Enabled")),
              disabledText(AcquisitionComponentGenericTable::tr("Disabled")),
              allowUndefined(true)
    { }

    virtual ~BooleanColumn()
    { }

    virtual void setBoolean(const QString &enabled, const QString &disabled, bool allowUndefined)
    {
        this->enabledText = enabled;
        this->disabledText = disabled;
        this->allowUndefined = allowUndefined;
    }

    virtual QVariant initializeData(const Variant::Read &input)
    {
        if (input.getType() != Variant::Type::Boolean) {
            if (allowUndefined)
                return QVariant();
        }
        return QVariant(input.toBool());
    }

    virtual QVariant displayData(const QVariant &data)
    {
        if (!data.isValid())
            return AcquisitionComponentGenericTableColumn::displayData(data);
        if (data.toBool())
            return enabledText;
        return disabledText;
    }

    void commit(const QVariant &data, Variant::Write &target) override
    {
        target.remove(false);
        if (!data.isValid() && allowUndefined)
            return;
        target.setBool(data.toBool());
    }

    class Delegate : public QStyledItemDelegate {
        BooleanColumn *column;
    public:
        Delegate(BooleanColumn *c, QObject *parent = 0) : QStyledItemDelegate(parent), column(c)
        { }

        virtual ~Delegate()
        { }

        virtual QWidget *createEditor(QWidget *parent,
                                      const QStyleOptionViewItem &option,
                                      const QModelIndex &index) const
        {
            Q_UNUSED(option);
            Q_UNUSED(index);

            QComboBox *editor = new QComboBox(parent);
            editor->setFrame(false);
            editor->addItem(column->disabledText);
            editor->addItem(column->enabledText);
            if (column->allowUndefined)
                editor->addItem(column->getDefaultDisplay().toString());
            return editor;
        }

        virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
        {
            QVariant value(index.model()->data(index, Qt::EditRole));
            QComboBox *box = static_cast<QComboBox *>(editor);
            box->clear();
            box->addItem(column->disabledText);
            box->addItem(column->enabledText);
            if (value.toBool())
                box->setCurrentIndex(1);
            if (column->allowUndefined) {
                box->addItem(column->getDefaultDisplay().toString());
                if (!value.isValid())
                    box->setCurrentIndex(2);
            }
        }

        virtual void setModelData(QWidget *editor,
                                  QAbstractItemModel *model,
                                  const QModelIndex &index) const
        {
            QComboBox *box = static_cast<QComboBox *>(editor);
            QVariant v;
            switch (box->currentIndex()) {
            case 0:
                v = QVariant(false);
                break;
            case 1:
                v = QVariant(true);
                break;
            default:
                break;
            }
            model->setData(index, v);
        }

        virtual void updateEditorGeometry(QWidget *editor,
                                          const QStyleOptionViewItem &option,
                                          const QModelIndex &index) const
        {
            Q_UNUSED(index);
            editor->setGeometry(option.rect);
        }
    };

    friend class Delegate;
};
}

void AcquisitionComponentGenericTable::addBoolean(const QString &title,
                                                  const QString &path,
                                                  bool persistent)
{
    BooleanColumn *column = new BooleanColumn(title, path, persistent);
    columns.append(column);
    table->setItemDelegateForColumn(columns.size() + 1, new BooleanColumn::Delegate(column, table));
}


namespace {
class MultilineStringColumn : public AcquisitionComponentGenericTableColumn {
    bool emptyIsUndefined;
    bool allowUndefined;
public:
    MultilineStringColumn(const QString &title, const QString &path)
            : AcquisitionComponentGenericTableColumn(title, path, true),
              emptyIsUndefined(false),
              allowUndefined(true)
    { }

    virtual ~MultilineStringColumn()
    { }

    virtual void setString(bool emptyIsUndefined, bool allowUndefined = true)
    {
        this->emptyIsUndefined = emptyIsUndefined;
        this->allowUndefined = allowUndefined;
    }

    virtual QVariant initializeData(const Variant::Read &input)
    {
        if (input.getType() != Variant::Type::String) {
            if (allowUndefined)
                return QVariant();
            if (getCreationValue().exists())
                return getCreationValue().toQString().trimmed();
        }
        QString text(input.toQString().trimmed());
        if (emptyIsUndefined && allowUndefined && text.isEmpty())
            return QVariant();
        return input.toQString();
    }

    void commit(const QVariant &data, Variant::Write &target) override
    {
        target.remove(false);
        if (!data.isValid() && allowUndefined)
            return;
        target.setString(data.toString());
    }

    virtual QVariant displayData(const QVariant &data)
    {
        QString str(data.toString());
        str.replace('\r', "\\r");
        str.replace('\n', "\\n");
        return str;
    }

    class Delegate : public QItemDelegate {
        MultilineStringColumn *column;

        class Editor : public AcquisitionComponentGenericTableEditorButton {
        public:
            Editor(QWidget *parent = 0) : AcquisitionComponentGenericTableEditorButton(parent)
            { }

            virtual ~Editor() = default;

        protected:
            virtual bool editData(QVariant &value)
            {
                QDialog dialog(this);
                dialog.setWindowTitle(tr("Text"));
                QVBoxLayout *layout = new QVBoxLayout(&dialog);
                dialog.setLayout(layout);

                QPlainTextEdit *editor = new QPlainTextEdit(&dialog);
                editor->setPlainText(value.toString());
                layout->addWidget(editor, 1);

                QDialogButtonBox *buttons =
                        new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                                             Qt::Horizontal, &dialog);
                layout->addWidget(buttons);
                QObject::connect(buttons, SIGNAL(accepted()), &dialog, SLOT(accept()));
                QObject::connect(buttons, SIGNAL(rejected()), &dialog, SLOT(reject()));

                if (dialog.exec() != QDialog::Accepted)
                    return false;

                value = QVariant::fromValue(editor->toPlainText());
                return true;
            }
        };

    public:
        Delegate(MultilineStringColumn *c, QObject *parent = 0) : QItemDelegate(parent), column(c)
        { }

        virtual ~Delegate() = default;

        virtual QWidget *createEditor(QWidget *parent,
                                      const QStyleOptionViewItem &option,
                                      const QModelIndex &index) const
        {
            Q_UNUSED(option);

            Editor *button = new Editor(parent);
            button->attach(index, index.data(Qt::EditRole));
            button->setText(index.data(Qt::DisplayRole).toString());

            return button;
        }

        virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
        {
            Editor *button = static_cast<Editor *>(editor);
            button->attach(index, index.data(Qt::EditRole));
            button->setText(index.data(Qt::DisplayRole).toString());
        }

        virtual void setModelData(QWidget *editor,
                                  QAbstractItemModel *model,
                                  const QModelIndex &index) const
        {
            Q_UNUSED(editor);
            Q_UNUSED(model);
            Q_UNUSED(index);
        }
    };

    friend class Delegate;
};
}

void AcquisitionComponentGenericTable::addStringLines(const QString &title, const QString &path)
{
    MultilineStringColumn *column = new MultilineStringColumn(title, path);
    columns.append(column);
    table->setItemDelegateForColumn(columns.size() + 1,
                                    new MultilineStringColumn::Delegate(column, table));
}


namespace {
class CalibrationColumn : public AcquisitionComponentGenericTableColumn {
    static QString buildCalibrationString(const Calibration &calibration)
    {
        QString result;

        NumberFormat ef(1, 0);
        for (int i = 0, max = calibration.size(); i < max; i++) {
            double v = calibration.get(i);
            if (v == 0.0)
                continue;

            if (result.isEmpty()) {
                result.append(QString::number(v));
            } else {
                if (v < 0.0) {
                    result.append(" - ");
                    v = -v;
                } else {
                    result.append(" + ");
                }
                result.append(QString::number(v));
            }

            if (i > 0)
                result.append('x');
            if (i > 1)
                result.append(ef.superscript(i));
        }
        return result;
    }

public:
    CalibrationColumn(const QString &title, const QString &path)
            : AcquisitionComponentGenericTableColumn(title, path, true)
    { }

    virtual ~CalibrationColumn() = default;

    virtual QVariant initializeData(const Variant::Read &input)
    {
        return QVariant::fromValue(Variant::Composite::toCalibration(input));
    }

    void commit(const QVariant &data, Variant::Write &target) override
    {
        target.remove(false);
        Variant::Composite::fromCalibration(target, data.value<Calibration>());
    }

    virtual QVariant displayData(const QVariant &data)
    {
        return buildCalibrationString(data.value<Calibration>());
    }

    class Delegate : public QItemDelegate {
        CalibrationColumn *column;

        class Editor : public AcquisitionComponentGenericTableEditorButton {
        public:
            Editor(QWidget *parent = 0) : AcquisitionComponentGenericTableEditorButton(parent)
            { }

            virtual ~Editor()
            { }

        protected:
            virtual bool editData(QVariant &value)
            {
                QDialog dialog(this);
                dialog.setWindowTitle(tr("Calibration"));
                QVBoxLayout *layout = new QVBoxLayout(&dialog);
                dialog.setLayout(layout);

                CalibrationEdit *editor = new CalibrationEdit(&dialog);
                editor->setCalibration(value.value<Calibration>());
                layout->addWidget(editor);

                layout->addStretch(1);

                QDialogButtonBox *buttons =
                        new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                                             Qt::Horizontal, &dialog);
                layout->addWidget(buttons);
                QObject::connect(buttons, SIGNAL(accepted()), &dialog, SLOT(accept()));
                QObject::connect(buttons, SIGNAL(rejected()), &dialog, SLOT(reject()));

                if (dialog.exec() != QDialog::Accepted)
                    return false;

                value = QVariant::fromValue(editor->getCalibration());
                return true;
            }
        };

    public:
        Delegate(CalibrationColumn *c, QObject *parent = 0) : QItemDelegate(parent), column(c)
        { }

        virtual ~Delegate()
        { }

        virtual QWidget *createEditor(QWidget *parent,
                                      const QStyleOptionViewItem &option,
                                      const QModelIndex &index) const
        {
            Q_UNUSED(option);

            Editor *button = new Editor(parent);
            button->attach(index, index.data(Qt::EditRole));
            button->setText(index.data(Qt::DisplayRole).toString());

            return button;
        }

        virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
        {
            Editor *button = static_cast<Editor *>(editor);
            button->attach(index, index.data(Qt::EditRole));
            button->setText(index.data(Qt::DisplayRole).toString());
        }

        virtual void setModelData(QWidget *editor,
                                  QAbstractItemModel *model,
                                  const QModelIndex &index) const
        {
            Q_UNUSED(editor);
            Q_UNUSED(model);
            Q_UNUSED(index);
        }
    };

    friend class Delegate;
};
}

void AcquisitionComponentGenericTable::addCalibration(const QString &title, const QString &path)
{
    CalibrationColumn *column = new CalibrationColumn(title, path);
    columns.append(column);
    table->setItemDelegateForColumn(columns.size() + 1,
                                    new CalibrationColumn::Delegate(column, table));
}


namespace {
class MetadataColumn : public AcquisitionComponentGenericTableColumn {
public:
    MetadataColumn(const QString &title, const QString &path)
            : AcquisitionComponentGenericTableColumn(title, path, true)
    { }

    virtual ~MetadataColumn()
    { }

    virtual QVariant initializeData(const Variant::Read &input)
    { return QVariant::fromValue(input); }

    void commit(const QVariant &data, Variant::Write &target) override
    {
        target.remove(false);
        Variant::Write v(data.value<Variant::Write>());
        if (!v.exists())
            return;
        target.set(v);
    }

    class Delegate : public QItemDelegate {
        MetadataColumn *column;

        class Editor : public AcquisitionComponentGenericTableEditorButton {
            static void declareFormMetadata(QHash<QString, QLineEdit *> &editors,
                                            Variant::Write &value,
                                            QWidget *tab,
                                            QFormLayout *form,
                                            const QString &key,
                                            const QString &label,
                                            const QString &toolTip = QString())
            {
                QLineEdit *editor = new QLineEdit(tab);
                if (!toolTip.isEmpty())
                    editor->setToolTip(toolTip);
                editor->setText(value.metadata(key).toQString());
                form->addRow(label, editor);
                editors.insert(key, editor);
            }

            static bool canSelectBasic(const QHash<QString, QLineEdit *> &editors,
                                       const Variant::Read &value)
            {
                switch (value.getType()) {
                case Variant::Type::MetadataReal:
                case Variant::Type::MetadataInteger:
                case Variant::Type::MetadataBoolean:
                case Variant::Type::MetadataString:
                case Variant::Type::MetadataBytes:
                case Variant::Type::MetadataArray:
                case Variant::Type::MetadataMatrix:
                    break;
                case Variant::Type::MetadataHash:
                    if (!value.toMetadataHashChild().empty())
                        return false;
                case Variant::Type::MetadataFlags:
                    if (!value.toMetadataSingleFlag().empty())
                        return false;
                    break;
                default:
                    return false;
                }

                auto meta = value.toMetadata().keys();
                for (QHash<QString, QLineEdit *>::const_iterator i = editors.constBegin(),
                        endI = editors.constEnd(); i != endI; ++i) {
                    meta.erase(i.key().toStdString());
                }
                if (!meta.empty())
                    return false;
                return true;
            }

            static void applyBasic(const QHash<QString, QLineEdit *> &editors,
                                   Variant::Write &value)
            {
                for (QHash<QString, QLineEdit *>::const_iterator i = editors.constBegin(),
                        endI = editors.constEnd(); i != endI; ++i) {
                    QString text(i.value()->text().trimmed());
                    if (text.isEmpty())
                        continue;
                    value.metadata(i.key()).setString(text);
                }
            }

        public:
            Editor(QWidget *parent = 0) : AcquisitionComponentGenericTableEditorButton(parent)
            { }

            virtual ~Editor() = default;

        protected:
            virtual bool editData(QVariant &value)
            {
                Variant::Write v = value.value<Variant::Write>();

                QDialog dialog(this);
                dialog.setWindowTitle(tr("Metadata"));
                QVBoxLayout *layout = new QVBoxLayout(&dialog);
                dialog.setLayout(layout);

                QTabWidget *tabs = new QTabWidget(&dialog);
                layout->addWidget(tabs, 1);

                QDialogButtonBox *buttons =
                        new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                                             Qt::Horizontal, &dialog);
                layout->addWidget(buttons);
                QObject::connect(buttons, SIGNAL(accepted()), &dialog, SLOT(accept()));
                QObject::connect(buttons, SIGNAL(rejected()), &dialog, SLOT(reject()));


                QWidget *tab = new QWidget(tabs);
                QFormLayout *form = new QFormLayout(tab);
                tab->setLayout(form);
                tabs->addTab(tab, tr("Basic"));

                QHash<QString, QLineEdit *> basicEditors;
                declareFormMetadata(basicEditors, v, tab, form, "Description", tr("&Description:"),
                                    tr("A basic description of the parameter."));
                declareFormMetadata(basicEditors, v, tab, form, "Units", tr("&Units:"),
                                    tr("The units of the value."));
                declareFormMetadata(basicEditors, v, tab, form, "NoteCalibration",
                                    tr("&Calibration note:"),
                                    tr("Any notes about the calibration (e.x. the date and location)."));
                declareFormMetadata(basicEditors, v, tab, form, "GroupUnits",
                                    tr("&Grouping units:"),
                                    tr("The units the value is grouped with for display (e.x. Transmittance)."));


                tab = new QWidget(tabs);
                layout = new QVBoxLayout(tab);
                tab->setLayout(layout);
                tabs->addTab(tab, tr("Advanced"));

                Variant::Write advancedData = v;
                advancedData.detachFromRoot();

                ValueEditor *advancedEditor = new ValueEditor(&dialog);
                advancedEditor->setValue(advancedData);
                layout->addWidget(advancedEditor, 1);

                if (canSelectBasic(basicEditors, v))
                    tabs->setCurrentIndex(0);
                else
                    tabs->setCurrentIndex(1);
                if (dialog.exec() != QDialog::Accepted)
                    return false;
                if (tabs->currentIndex() == 0) {
                    Variant::Write result = Variant::Write::empty();
                    result.setType(Variant::Type::MetadataReal);
                    applyBasic(basicEditors, result);
                    value = QVariant::fromValue<Variant::Write>(result);
                } else {
                    value = QVariant::fromValue<Variant::Write>(advancedData);
                }
                return true;
            }
        };

    public:
        Delegate(MetadataColumn *c, QObject *parent = 0) : QItemDelegate(parent), column(c)
        { }

        virtual ~Delegate() = default;

        virtual QWidget *createEditor(QWidget *parent,
                                      const QStyleOptionViewItem &option,
                                      const QModelIndex &index) const
        {
            Q_UNUSED(option);

            Editor *button = new Editor(parent);
            button->attach(index, index.data(Qt::EditRole));
            button->setText(tr("Metadata"));

            return button;
        }

        virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
        {
            Editor *button = static_cast<Editor *>(editor);
            button->attach(index, index.data(Qt::EditRole));
        }

        virtual void setModelData(QWidget *editor,
                                  QAbstractItemModel *model,
                                  const QModelIndex &index) const
        {
            Q_UNUSED(editor);
            Q_UNUSED(model);
            Q_UNUSED(index);
        }
    };

    friend class Delegate;
};
}

void AcquisitionComponentGenericTable::addMetadata(const QString &title, const QString &path)
{
    MetadataColumn *column = new MetadataColumn(title, path);
    columns.append(column);
    table->setItemDelegateForColumn(columns.size() + 1,
                                    new MetadataColumn::Delegate(column, table));
}


namespace {
class DynamicInputColumn : public AcquisitionComponentGenericTableColumn {
public:
    DynamicInputColumn(const QString &title, const QString &path)
            : AcquisitionComponentGenericTableColumn(title, path, true)
    { }

    virtual ~DynamicInputColumn() = default;

    virtual QVariant initializeData(const Variant::Read &input)
    { return QVariant::fromValue(input); }

    void commit(const QVariant &data, Variant::Write &target) override
    {
        target.remove(false);
        Variant::Write v(data.value<Variant::Write>());
        if (!v.exists())
            return;
        target.set(v);
    }

    class Delegate : public QItemDelegate {
        DynamicInputColumn *column;

        class Editor : public AcquisitionComponentGenericTableEditorButton {
        public:
            Editor(QWidget *parent = 0) : AcquisitionComponentGenericTableEditorButton(parent)
            { }

            virtual ~Editor() = default;

        protected:
            virtual bool editData(QVariant &value)
            {
                Variant::Write v = value.value<Variant::Write>();

                DynamicInputEditorDialog dialog(v, Variant::Root(), this);
                dialog.setWindowTitle(tr("Input"));
                if (dialog.exec() != QDialog::Accepted)
                    return false;
                value = QVariant::fromValue<Variant::Write>(dialog.input().write());
                return true;
            }
        };

    public:
        Delegate(DynamicInputColumn *c, QObject *parent = 0) : QItemDelegate(parent), column(c)
        { }

        virtual ~Delegate() = default;

        virtual QWidget *createEditor(QWidget *parent,
                                      const QStyleOptionViewItem &option,
                                      const QModelIndex &index) const
        {
            Q_UNUSED(option);

            Editor *button = new Editor(parent);
            button->attach(index, index.data(Qt::EditRole));
            button->setText(tr("Input"));

            return button;
        }

        virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
        {
            Editor *button = static_cast<Editor *>(editor);
            button->attach(index, index.data(Qt::EditRole));
        }

        virtual void setModelData(QWidget *editor,
                                  QAbstractItemModel *model,
                                  const QModelIndex &index) const
        {
            Q_UNUSED(editor);
            Q_UNUSED(model);
            Q_UNUSED(index);
        }
    };

    friend class Delegate;
};
}

void AcquisitionComponentGenericTable::addDynamicInput(const QString &title, const QString &path)
{
    DynamicInputColumn *column = new DynamicInputColumn(title, path);
    columns.append(column);
    table->setItemDelegateForColumn(columns.size() + 1,
                                    new DynamicInputColumn::Delegate(column, table));
}


namespace {
class GenericValueColumn : public AcquisitionComponentGenericTableColumn {
public:
    GenericValueColumn(const QString &title, const QString &path)
            : AcquisitionComponentGenericTableColumn(title, path, true)
    { }

    virtual ~GenericValueColumn() = default;

    virtual QVariant initializeData(const Variant::Read &input)
    { return QVariant::fromValue(input); }

    void commit(const QVariant &data, Variant::Write &target) override
    {
        target.remove(false);
        Variant::Write v = data.value<Variant::Write>();
        if (!v.exists())
            return;
        target.set(v);
    }

    class Delegate : public QItemDelegate {
        GenericValueColumn *column;
        Variant::Read metadata;

        class Editor : public AcquisitionComponentGenericTableEditorButton {
        public:
            Variant::Read metadata;

            Editor(QWidget *parent = 0) : AcquisitionComponentGenericTableEditorButton(parent)
            { }

            virtual ~Editor() = default;

        protected:
            virtual bool editData(QVariant &value)
            {
                Variant::Write v = value.value<Variant::Write>();

                QDialog dialog(this);
                dialog.setWindowTitle(tr("Value"));
                QVBoxLayout *layout = new QVBoxLayout(&dialog);
                dialog.setLayout(layout);

                ValueEditor *editor = new ValueEditor(&dialog);
                layout->addWidget(editor, 1);
                Variant::Write edit = v;
                edit.detachFromRoot();
                editor->setValue(edit, metadata);

                QDialogButtonBox *buttons =
                        new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                                             Qt::Horizontal, &dialog);
                layout->addWidget(buttons);
                QObject::connect(buttons, SIGNAL(accepted()), &dialog, SLOT(accept()));
                QObject::connect(buttons, SIGNAL(rejected()), &dialog, SLOT(reject()));

                if (dialog.exec() != QDialog::Accepted)
                    return false;
                value = QVariant::fromValue<Variant::Write>(edit);
                return true;
            }
        };

    public:
        Delegate(GenericValueColumn *c, const Variant::Read &m, QObject *parent = 0)
                : QItemDelegate(parent), column(c), metadata(m)
        {
            metadata.detachFromRoot();
        }

        virtual ~Delegate() = default;

        virtual QWidget *createEditor(QWidget *parent,
                                      const QStyleOptionViewItem &option,
                                      const QModelIndex &index) const
        {
            Q_UNUSED(option);

            Editor *button = new Editor(parent);
            button->metadata = metadata;
            button->attach(index, index.data(Qt::EditRole));
            button->setText(tr("Value"));

            return button;
        }

        virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
        {
            Editor *button = static_cast<Editor *>(editor);
            button->metadata = metadata;
            button->attach(index, index.data(Qt::EditRole));
        }

        virtual void setModelData(QWidget *editor,
                                  QAbstractItemModel *model,
                                  const QModelIndex &index) const
        {
            Q_UNUSED(editor);
            Q_UNUSED(model);
            Q_UNUSED(index);
        }
    };

    friend class Delegate;
};
}

void AcquisitionComponentGenericTable::addGenericValue(const QString &title,
                                                       const QString &path,
                                                       const Variant::Read &metadata)
{
    GenericValueColumn *column = new GenericValueColumn(title, path);
    columns.append(column);
    table->setItemDelegateForColumn(columns.size() + 1,
                                    new GenericValueColumn::Delegate(column, metadata, table));
}


namespace {
class CombinedCommandsColumn : public AcquisitionComponentGenericTableColumn {
public:
    CombinedCommandsColumn(const QString &title, const QString &path)
            : AcquisitionComponentGenericTableColumn(title, path, true)
    { }

    virtual ~CombinedCommandsColumn() = default;

    virtual QVariant initializeData(const Variant::Read &input)
    { return QVariant::fromValue(input); }

    void commit(const QVariant &data, Variant::Write &target) override
    {
        target.remove(false);
        Variant::Write v = data.value<Variant::Write>();
        if (!v.exists())
            return;
        target.set(v);
    }

    class Delegate : public QItemDelegate {
        CombinedCommandsColumn *column;

        class Editor : public AcquisitionComponentGenericTableEditorButton {
        public:
            Editor(QWidget *parent = 0) : AcquisitionComponentGenericTableEditorButton(parent)
            { }

            virtual ~Editor() = default;

        protected:
            virtual bool editData(QVariant &value)
            {
                Variant::Write v = value.value<Variant::Write>();

                QDialog dialog(this);
                dialog.setWindowTitle(tr("Commands"));
                QVBoxLayout *layout = new QVBoxLayout(&dialog);
                dialog.setLayout(layout);

                QTabWidget *tabs = new QTabWidget(&dialog);
                layout->addWidget(tabs, 1);

                ValueEditor *globalEditor = new ValueEditor(tabs);
                tabs->addTab(globalEditor, tr("Global"));
                Variant::Write globalEdit = v.hash("Commands");
                globalEdit.detachFromRoot();
                globalEditor->setValue(globalEdit);

                ValueEditor *instrumentEditor = new ValueEditor(tabs);
                tabs->addTab(instrumentEditor, tr("Instrument"));
                Variant::Write instrumentEdit = v.hash("InstrumentCommands");
                instrumentEdit.detachFromRoot();
                instrumentEditor->setValue(instrumentEdit);

                if (!globalEdit.exists())
                    tabs->setCurrentIndex(1);
                else
                    tabs->setCurrentIndex(0);

                QDialogButtonBox *buttons =
                        new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                                             Qt::Horizontal, &dialog);
                layout->addWidget(buttons);
                QObject::connect(buttons, SIGNAL(accepted()), &dialog, SLOT(accept()));
                QObject::connect(buttons, SIGNAL(rejected()), &dialog, SLOT(reject()));

                if (dialog.exec() != QDialog::Accepted)
                    return false;

                v.hash("InstrumentCommands").remove(false);
                v.hash("Commands").remove(false);

                v.hash("InstrumentCommands").set(instrumentEdit);
                v.hash("Commands").set(globalEdit);
                return true;
            }
        };

    public:
        Delegate(CombinedCommandsColumn *c, QObject *parent = 0) : QItemDelegate(parent), column(c)
        { }

        virtual ~Delegate() = default;

        virtual QWidget *createEditor(QWidget *parent,
                                      const QStyleOptionViewItem &option,
                                      const QModelIndex &index) const
        {
            Q_UNUSED(option);

            Editor *button = new Editor(parent);
            button->attach(index, index.data(Qt::EditRole));
            button->setText(tr("Commands"));

            return button;
        }

        virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
        {
            Editor *button = static_cast<Editor *>(editor);
            button->attach(index, index.data(Qt::EditRole));
        }

        virtual void setModelData(QWidget *editor,
                                  QAbstractItemModel *model,
                                  const QModelIndex &index) const
        {
            Q_UNUSED(editor);
            Q_UNUSED(model);
            Q_UNUSED(index);
        }
    };

    friend class Delegate;
};
}

void AcquisitionComponentGenericTable::addCombinedCommands(const QString &title,
                                                           const QString &path)
{
    CombinedCommandsColumn *column = new CombinedCommandsColumn(title, path);
    columns.append(column);
    table->setItemDelegateForColumn(columns.size() + 1,
                                    new CombinedCommandsColumn::Delegate(column, table));
}


namespace Internal {
struct TimeIntervalColumnData {
    Time::LogicalTimeUnit unit;
    int count;
    bool align;

    TimeIntervalColumnData() : unit(Time::Second), count(1), align(false)
    { }

    explicit TimeIntervalColumnData(const Variant::Read &value) : unit(
            Variant::Composite::toTimeInterval(value, &count, &align))
    { }
};
}

namespace {
class TimeIntervalColumn : public AcquisitionComponentGenericTableColumn {
    bool allowZero;
    bool allowNegative;
    bool allowAlign;
public:
    TimeIntervalColumn(const QString &title, const QString &path, bool persistent)
            : AcquisitionComponentGenericTableColumn(title, path, persistent),
              allowZero(true),
              allowNegative(false),
              allowAlign(true)
    { }

    virtual ~TimeIntervalColumn() = default;

    virtual QVariant initializeData(const Variant::Read &input)
    {
        TimeIntervalColumnData data(input);
        if (data.count == 0 && !allowZero)
            return QVariant();
        if (data.align && !allowAlign)
            return QVariant();
        return QVariant::fromValue(data);
    }

    void commit(const QVariant &data, Variant::Write &target) override
    {
        target.hash("Unit").remove(false);
        target.hash("Units").remove(false);
        target.hash("Count").remove(false);
        target.hash("Number").remove(false);
        target.hash("Offset").remove(false);
        target.hash("Align").remove(false);
        target.hash("Aligned").remove(false);
        if (!data.isValid())
            return;

        TimeIntervalColumnData td = data.value<TimeIntervalColumnData>();
        switch (td.unit) {
        case Time::Millisecond:
            target.hash("Units").setString("Milliseconds");
            break;
        case Time::Second:
            target.hash("Units").setString("Seconds");
            break;
        case Time::Minute:
            target.hash("Units").setString("Minutes");
            break;
        case Time::Hour:
            target.hash("Units").setString("Hours");
            break;
        case Time::Day:
            target.hash("Units").setString("Days");
            break;
        case Time::Week:
            target.hash("Units").setString("Weeks");
            break;
        case Time::Month:
            target.hash("Units").setString("Months");
            break;
        case Time::Quarter:
            target.hash("Units").setString("Quarters");
            break;
        case Time::Year:
            target.hash("Units").setString("Years");
            break;
        }
        target.hash("Count").setInt64(td.count);
        target.hash("Align").setBool(td.align);
    }

    virtual QVariant displayData(const QVariant &data)
    {
        if (!data.isValid())
            return AcquisitionComponentGenericTableColumn::displayData(data);
        TimeIntervalColumnData td = data.value<TimeIntervalColumnData>();
        return Time::describeOffset(td.unit, td.count, td.align);
    }

    class Delegate : public QStyledItemDelegate {
        TimeIntervalColumn *column;

    public:
        Delegate(TimeIntervalColumn *c, QObject *parent = 0) : QStyledItemDelegate(parent),
                                                               column(c)
        { }

        virtual ~Delegate() = default;

        virtual QWidget *createEditor(QWidget *parent,
                                      const QStyleOptionViewItem &option,
                                      const QModelIndex &index) const
        {
            Q_UNUSED(option);
            Q_UNUSED(index);

            TimeIntervalSelection *editor = new TimeIntervalSelection(parent);
            editor->setFrame(false);
            editor->setAllowZero(column->allowZero);
            editor->setAllowNegative(column->allowNegative);
            editor->setAllowAlign(column->allowAlign);

            return editor;
        }

        virtual void setEditorData(QWidget *editor, const QModelIndex &index) const
        {
            QVariant value(index.model()->data(index, Qt::EditRole));
            TimeIntervalSelection *interval = static_cast<TimeIntervalSelection *>(editor);
            interval->setAllowZero(column->allowZero);
            interval->setAllowNegative(column->allowNegative);
            interval->setAllowAlign(column->allowAlign);
            if (!value.isValid())
                return;
            TimeIntervalColumnData td = value.value<TimeIntervalColumnData>();
            interval->setInterval(td.unit, td.count, td.align);
        }

        virtual void setModelData(QWidget *editor,
                                  QAbstractItemModel *model,
                                  const QModelIndex &index) const
        {
            TimeIntervalSelection *interval = static_cast<TimeIntervalSelection *>(editor);
            interval->interpretText();

            TimeIntervalColumnData td;
            td.unit = interval->getUnit();
            td.count = interval->getCount();
            td.align = interval->getAlign();
            model->setData(index, QVariant::fromValue(td));
        }

        virtual void updateEditorGeometry(QWidget *editor,
                                          const QStyleOptionViewItem &option,
                                          const QModelIndex &index) const
        {
            Q_UNUSED(index);
            editor->setGeometry(option.rect);
        }
    };

    friend class Delegate;
};
}

void AcquisitionComponentGenericTable::addTimeIntervalSelection(const QString &title,
                                                                const QString &path,
                                                                bool persistent)
{
    TimeIntervalColumn *column = new TimeIntervalColumn(title, path, persistent);
    columns.append(column);
    table->setItemDelegateForColumn(columns.size() + 1,
                                    new TimeIntervalColumn::Delegate(column, table));
}


namespace {
class FlagsColumn : public AcquisitionComponentGenericTableColumn {
public:
    FlagsColumn(const QString &title, const QString &path) : AcquisitionComponentGenericTableColumn(
            title, path, true)
    { }

    virtual ~FlagsColumn() = default;

    virtual QVariant initializeData(const Variant::Read &input)
    {
        QSet<QString> flags;
        for (const auto &add : input.toFlags()) {
            flags.insert(QString::fromStdString(add));
        }
        return QVariant::fromValue(flags);
    }

    void commit(const QVariant &data, Variant::Write &target) override
    {
        target.remove(false);
        if (!data.isValid())
            return;
        QSet<QString> flags = data.value<QSet<QString>>();
        if (flags.isEmpty())
            return;
        target.setFlags(Util::set_from_qstring(flags));
    }
};
}

void AcquisitionComponentGenericTable::addFlags(const QString &title, const QString &path)
{
    FlagsColumn *column = new FlagsColumn(title, path);
    columns.append(column);
    table->setItemDelegateForColumn(columns.size() + 1,
                                    new AcquisitionComponentGenericTableDelegateSharedFlags(table));
}


namespace Internal {

AcquisitionComponentGenericTableColumn::AcquisitionComponentGenericTableColumn(const QString &t,
                                                                               const QString &p,
                                                                               bool pe) : title(t),
                                                                                          path(p),
                                                                                          persistent(
                                                                                                  pe)
{ }

AcquisitionComponentGenericTableColumn::~AcquisitionComponentGenericTableColumn() = default;

bool AcquisitionComponentGenericTableColumn::isSortable() const
{ return false; }

bool AcquisitionComponentGenericTableColumn::sortCompare(const QVariant &a,
                                                         const QVariant &b,
                                                         bool ascending)
{
    Q_UNUSED(a);
    Q_UNUSED(b);
    Q_UNUSED(ascending);
    return false;
}

void AcquisitionComponentGenericTableColumn::setDoubleSpinBox(int decimals,
                                                              double minimum,
                                                              double maximum,
                                                              double step,
                                                              bool allowUndefined)
{
    Q_UNUSED(decimals);
    Q_UNUSED(minimum);
    Q_UNUSED(maximum);
    Q_UNUSED(step);
    Q_UNUSED(allowUndefined);
}

void AcquisitionComponentGenericTableColumn::setIntegerSpinBox(int minimum,
                                                               int maximum,
                                                               const NumberFormat &format,
                                                               bool allowUndefined)
{
    Q_UNUSED(minimum);
    Q_UNUSED(maximum);
    Q_UNUSED(format);
    Q_UNUSED(allowUndefined);
}

void AcquisitionComponentGenericTableColumn::setBoolean(const QString &enabled,
                                                        const QString &disabled,
                                                        bool allowUndefined)
{
    Q_UNUSED(allowUndefined);
}

void AcquisitionComponentGenericTableColumn::setString(bool emptyIsUndefined, bool allowUndefined)
{
    Q_UNUSED(emptyIsUndefined);
    Q_UNUSED(allowUndefined);
}

QVariant AcquisitionComponentGenericTableColumn::displayData(const QVariant &data)
{
    if (!data.isValid())
        return defaultDisplay;
    return data;
}

QVariant AcquisitionComponentGenericTableColumn::editData(const QVariant &data)
{ return data; }

bool AcquisitionComponentGenericTableColumn::setData(const QVariant &input, QVariant &output)
{
    output = input;
    return true;
}

QVariant AcquisitionComponentGenericTableColumn::initializeData(const Variant::Read &input)
{
    switch (input.getType()) {
    case Variant::Type::String:
        return input.toQString();
    case Variant::Type::Real:
        return input.toDouble();
    case Variant::Type::Integer:
        return input.toInt64();
    case Variant::Type::Boolean:
        return input.toBool();
    case Variant::Type::Empty:
        return QVariant();
    default:
        break;
    }
    return QVariant::fromValue(input);
}


AcquisitionComponentGenericTableEditorButton::AcquisitionComponentGenericTableEditorButton(QWidget *parent)
        : QPushButton(parent)
{
    connect(this, SIGNAL(clicked(bool)), this, SLOT(showEditor()));
}

AcquisitionComponentGenericTableEditorButton::~AcquisitionComponentGenericTableEditorButton() = default;

void AcquisitionComponentGenericTableEditorButton::showEditor()
{
    if (!editData(attachedValue))
        return;
    QAbstractItemModel *model = const_cast<QAbstractItemModel *>(attachedIndex.model());
    model->setData(attachedIndex, attachedValue);
}

void AcquisitionComponentGenericTableEditorButton::attach(const QModelIndex &index,
                                                          const QVariant &value)
{
    this->attachedIndex = index;
    this->attachedValue = value;
}


AcquisitionComponentGenericTableDelegateSharedFlagsButton::AcquisitionComponentGenericTableDelegateSharedFlagsButton(
        QWidget *parent) : QPushButton(parent)
{
    menu = new QMenu(this);
    delimiter = menu->addSeparator();
    QAction *addFlagAction = menu->addAction(tr("&Add"));
    connect(addFlagAction, SIGNAL(triggered(bool)), this, SLOT(addFlag()));

    updateState();
}

AcquisitionComponentGenericTableDelegateSharedFlagsButton::~AcquisitionComponentGenericTableDelegateSharedFlagsButton() = default;

void AcquisitionComponentGenericTableDelegateSharedFlagsButton::attach(const QModelIndex &index,
                                                                       const QVariant &value)
{
    this->attachedIndex = index;

    QSet<QString> selectedFlags(value.value<QSet<QString> >());
    for (QHash<QString, QAction *>::const_iterator f = flags.constBegin(), endF = flags.constEnd();
            f != endF;
            ++f) {
        f.value()->disconnect(this);
        f.value()->setChecked(false);
        connect(f.value(), SIGNAL(toggled(bool)), this, SLOT(flagToggled()));
    }

    QStringList sorted(selectedFlags.values());
    std::sort(sorted.begin(), sorted.end());
    for (QStringList::const_iterator flag = sorted.constBegin(), end = sorted.constEnd();
            flag != end;
            ++flag) {
        emit addSharedFlag(*flag);
    }

    for (QStringList::const_iterator flag = sorted.constBegin(), end = sorted.constEnd();
            flag != end;
            ++flag) {
        QAction *action = flags.value(*flag, NULL);
        Q_ASSERT(action != NULL);

        action->disconnect(this);
        action->setChecked(true);
        connect(action, SIGNAL(toggled(bool)), this, SLOT(flagToggled()));
    }

    updateState();
}

void AcquisitionComponentGenericTableDelegateSharedFlagsButton::addFlag()
{
    bool ok;
    QString flag
            (QInputDialog::getText(this, tr("Add"), tr("Add:"), QLineEdit::Normal, QString(), &ok));
    if (!ok || flag.isEmpty())
        return;

    QHash<QString, QAction *>::const_iterator check = flags.constFind(flag);
    if (check != flags.constEnd()) {
        check.value()->setChecked(true);
        return;
    }

    emit addSharedFlag(flag);

    QAction *action = flags.value(flag, NULL);
    Q_ASSERT(action != NULL);
    action->setChecked(true);
}

void AcquisitionComponentGenericTableDelegateSharedFlagsButton::updateState()
{
    disconnect(this, SIGNAL(clicked(bool)), this, SLOT(addFlag()));

    if (flags.isEmpty()) {
        setText(tr("Add..."));
        setMenu(0);
        connect(this, SIGNAL(clicked(bool)), this, SLOT(addFlag()));
        return;
    }

    QSet<QString> selected(getSelected());
    if (selected.size() == 1) {
        setText(*selected.constBegin());
    } else {
        setText(tr("Set..."));
    }
    setMenu(menu);
}

QSet<QString> AcquisitionComponentGenericTableDelegateSharedFlagsButton::getSelected() const
{
    QSet<QString> selected;
    for (QHash<QString, QAction *>::const_iterator f = flags.constBegin(), endF = flags.constEnd();
            f != endF;
            ++f) {
        if (!f.value()->isChecked())
            continue;
        selected.insert(f.key());
    }
    return selected;
}

void AcquisitionComponentGenericTableDelegateSharedFlagsButton::flagToggled()
{
    updateState();

    QAbstractItemModel *model = const_cast<QAbstractItemModel *>(attachedIndex.model());
    model->setData(attachedIndex, QVariant::fromValue(getSelected()), Qt::EditRole);
}

void AcquisitionComponentGenericTableDelegateSharedFlagsButton::addLocalFlag(const QString &flag)
{
    if (flags.contains(flag))
        return;

    QAction *action = new QAction(flag, menu);
    menu->insertAction(delimiter, action);
    flags.insert(flag, action);

    action->setCheckable(true);
    connect(action, SIGNAL(toggled(bool)), this, SLOT(flagToggled()));
}


AcquisitionComponentGenericTableDelegateSharedFlags::AcquisitionComponentGenericTableDelegateSharedFlags(
        QObject *parent) : QItemDelegate(parent)
{ }

AcquisitionComponentGenericTableDelegateSharedFlags::~AcquisitionComponentGenericTableDelegateSharedFlags() = default;

QWidget *AcquisitionComponentGenericTableDelegateSharedFlags::createEditor(QWidget *parent,
                                                                           const QStyleOptionViewItem &option,
                                                                           const QModelIndex &index) const
{
    Q_UNUSED(option);

    AcquisitionComponentGenericTableDelegateSharedFlagsButton
            *button = new AcquisitionComponentGenericTableDelegateSharedFlagsButton(parent);
    connect(button, SIGNAL(addSharedFlag(
                                   const QString &)), this, SIGNAL(addSharedFlag(
                                                                           const QString &)),
            Qt::DirectConnection);
    connect(this, SIGNAL(addSharedFlag(
                                 const QString &)), button, SLOT(addLocalFlag(
                                                                         const QString &)),
            Qt::DirectConnection);
    button->attach(index, index.data(Qt::EditRole));

    return button;
}

void AcquisitionComponentGenericTableDelegateSharedFlags::setEditorData(QWidget *editor,
                                                                        const QModelIndex &index) const
{
    AcquisitionComponentGenericTableDelegateSharedFlagsButton *button =
            static_cast<AcquisitionComponentGenericTableDelegateSharedFlagsButton *>(editor);
    QVariant value(index.model()->data(index, Qt::EditRole));
    button->attach(index, value);
}

void AcquisitionComponentGenericTableDelegateSharedFlags::setModelData(QWidget *editor,
                                                                       QAbstractItemModel *model,
                                                                       const QModelIndex &index) const
{
    Q_UNUSED(editor);
    Q_UNUSED(model);
    Q_UNUSED(index);
}

}

AcquisitionComponentGenericTable::Model::Model(AcquisitionComponentGenericTable *t) : table(t)
{ }

AcquisitionComponentGenericTable::Model::~Model() = default;

QModelIndex AcquisitionComponentGenericTable::Model::index(int row,
                                                           int column,
                                                           const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return createIndex(row, column);
}

QModelIndex AcquisitionComponentGenericTable::Model::parent(const QModelIndex &index) const
{
    Q_UNUSED(index);
    return QModelIndex();
}

int AcquisitionComponentGenericTable::Model::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return rows.size() + 1;
}

int AcquisitionComponentGenericTable::Model::columnCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return table->columns.size() + 2;
}

QVariant AcquisitionComponentGenericTable::Model::headerData(int section,
                                                             Qt::Orientation orientation,
                                                             int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();
    if (orientation == Qt::Vertical)
        return QVariant(section + 1);

    if (section <= 0)
        return QVariant();
    --section;

    if (section <= 0) {
        if (table->arrayMode)
            return tr("Index");
        return tr("Identifier");
    }
    --section;

    if (section >= table->columns.size())
        return QVariant();
    return table->columns.at(section)->getTitle();
}

Qt::ItemFlags AcquisitionComponentGenericTable::Model::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    int cIndex = index.column();
    if (cIndex <= 0)
        return 0;
    --cIndex;

    if (cIndex <= 0) {
        if (table->arrayMode)
            return 0;
        return Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled;
    }
    --cIndex;

    if (cIndex >= table->columns.size())
        return 0;

    if (table->columns.at(cIndex)->persistentEditors())
        return Qt::ItemIsSelectable | Qt::ItemIsEnabled;

    return Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled;
}

bool AcquisitionComponentGenericTable::Model::removeRows(int row,
                                                         int count,
                                                         const QModelIndex &parent)
{
    Q_ASSERT(row + count <= rows.size());

    beginRemoveRows(parent, row, row + count - 1);
    for (int i = 0; i < count; i++) {
        rows.removeAt(row);
    }
    endRemoveRows();

    return true;
}

bool AcquisitionComponentGenericTable::Model::insertRows(int row,
                                                         int count,
                                                         const QModelIndex &parent)
{
    Q_ASSERT(count == 1);
    Q_ASSERT(row == rows.size());

    if (!table->arrayMode) {
        QSet<QString> takenIDs;
        for (QList<Row>::const_iterator c = rows.constBegin(), endC = rows.constEnd();
                c != endC;
                ++c) {
            takenIDs.insert(c->id);
        }

        beginInsertRows(parent, row, row + count - 1);
        int counter = 1;
        for (int i = 0; i < count; i++, row++) {
            QString id("New");
            for (; takenIDs.contains(id); ++counter) {
                id = QString("New%1").arg(counter);
            }

            Row data;
            data.id = id;
            for (QList<AcquisitionComponentGenericTableColumn *>::const_iterator
                    c = table->columns.constBegin(), endC = table->columns.constEnd();
                    c != endC;
                    ++c) {
                data.data.append((*c)->initializeData((*c)->getCreationValue()));
            }
            rows.insert(row++, data);
        }
        endInsertRows();
    } else {
        beginInsertRows(parent, row, row + count - 1);
        for (int i = 0; i < count; i++, row++) {
            Row data;
            data.id = QString::number(i + 1);
            for (QList<AcquisitionComponentGenericTableColumn *>::const_iterator
                    c = table->columns.constBegin(), endC = table->columns.constEnd();
                    c != endC;
                    ++c) {
                data.data.append((*c)->initializeData((*c)->getCreationValue()));
            }
            rows.insert(row++, data);
        }
        endInsertRows();
    }
    return true;
}

AcquisitionComponentGenericTable::Model::RowSort::RowSort(AcquisitionComponentGenericTableColumn *c,
                                                          int i,
                                                          bool a) : column(c),
                                                                    index(i),
                                                                    ascending(a)
{ }

bool AcquisitionComponentGenericTable::Model::RowSort::operator()(const AcquisitionComponentGenericTable::Model::Row &a,
                                                                  const AcquisitionComponentGenericTable::Model::Row &b) const
{ return column->sortCompare(a.data.at(index), b.data.at(index), ascending); }


void AcquisitionComponentGenericTable::Model::sort(int cIndex, Qt::SortOrder order)
{
    if (table->arrayMode)
        return;
    if (cIndex <= 0)
        return;
    --cIndex;

    if (cIndex <= 0) {
        beginResetModel();
        std::stable_sort(rows.begin(), rows.end(),
                    order == Qt::AscendingOrder ? rowIDAscending : rowIDDescending);
        endResetModel();
        table->openAllEditors();
        return;
    }
    --cIndex;

    if (cIndex < 0 || cIndex > table->columns.size())
        return;

    AcquisitionComponentGenericTableColumn *column = table->columns.at(cIndex);
    if (!column->isSortable())
        return;
    beginResetModel();
    std::stable_sort(rows.begin(), rows.end(), RowSort(column, cIndex, order == Qt::AscendingOrder));
    endResetModel();
    table->openAllEditors();
}


QVariant AcquisitionComponentGenericTable::Model::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (index.row() >= rows.size()) {
        if (role == Qt::EditRole) {
            if (index.column() == 0)
                return 1;
        }
        return QVariant();
    }
    Row data(rows.at(index.row()));

    int cIndex = index.column();
    if (cIndex <= 0) {
        if (role == Qt::EditRole)
            return 0;
        return QVariant();
    }
    --cIndex;

    if (cIndex <= 0) {
        switch (role) {
        case Qt::DisplayRole:
        case Qt::EditRole:
            if (table->arrayMode) {
                if (table->arrayFlags & Array_ZeroBased)
                    return index.row();
                return index.row() + 1;
            }
            return data.id;
        default:
            break;
        }
        return QVariant();
    }
    --cIndex;

    if (cIndex >= table->columns.size())
        return QVariant();
    AcquisitionComponentGenericTableColumn *column = table->columns.at(cIndex);

    switch (role) {
    case Qt::DisplayRole: {
        QVariant v(column->displayData(data.data.at(cIndex)));
        if (v.type() == QVariant::Char && v.toChar() == QChar(QChar::ObjectReplacementCharacter))
            return data.id;
        return v;
    }
    case Qt::EditRole:
        return column->editData(data.data.at(cIndex));
    case Qt::ForegroundRole:
        if (!data.data.at(cIndex).isValid())
            return QBrush(Qt::lightGray);
        break;
    default:
        break;
    }

    return QVariant();
}

bool AcquisitionComponentGenericTable::Model::setData(const QModelIndex &index,
                                                      const QVariant &value,
                                                      int role)
{
    if (role != Qt::EditRole)
        return false;
    if (index.row() >= rows.size())
        return false;

    int cIndex = index.column();
    if (cIndex <= 0)
        return false;
    --cIndex;

    if (cIndex <= 0) {
        if (table->arrayMode)
            return false;
        QString newID(value.toString().trimmed());
        QRegExp validName("[a-z][0-9a-z_]*", Qt::CaseInsensitive);
        if (newID.isEmpty() || !validName.exactMatch(newID))
            return false;
        int checkRow = index.row();
        int row = 0;
        for (QList<Row>::const_iterator c = rows.constBegin(), endC = rows.constEnd();
                c != endC;
                ++c, ++row) {
            if (row == checkRow)
                continue;
            if (c->id == newID)
                return false;
        }
        rows[checkRow].id = newID;
        emit dataChanged(this->index(index.row(), 2),
                         this->index(index.row(), 2 + table->columns.size()));
        table->openAllEditors();
        return true;
    }
    --cIndex;

    AcquisitionComponentGenericTableColumn *column = table->columns.at(cIndex);
    if (!column->setData(value, rows[index.row()].data[cIndex]))
        return false;
    emit dataChanged(this->index(index.row(), 2),
                     this->index(index.row(), 2 + table->columns.size()));
    table->openAllEditors();
    return true;
}

void AcquisitionComponentGenericTable::Model::commit(CPD3::Data::Variant::Write &target)
{
    target.getPath(table->masterPath).remove(false);

    int rIndex = 0;
    for (QList<Row>::const_iterator r = rows.constBegin(), endR = rows.constEnd();
            r != endR;
            ++r, ++rIndex) {
        QString id;
        Variant::Write rowTarget;
        if (table->arrayMode) {
            rowTarget = target.getPath(table->masterPath).array(rIndex);
            id = QString("#%1").arg(rIndex);
        } else {
            rowTarget = target.getPath(table->masterPath).hash(r->id);
            id = r->id;
            if (id.isEmpty())
                continue;
        }
        rowTarget.setEmpty();

        QList<QVariant>::const_iterator cData = r->data.constBegin();
        for (QList<AcquisitionComponentGenericTableColumn *>::const_iterator
                c = table->columns.constBegin(), endC = table->columns.constEnd();
                c != endC;
                ++c, ++cData) {
            Q_ASSERT(cData != r->data.constEnd());
            (*c)->commit(id, *cData, target);
        }
    }
}

void AcquisitionComponentGenericTable::Model::initialize()
{
    auto baseData = table->initialValue.getPath(table->masterPath);

    beginResetModel();

    switch (baseData.getType()) {
    case Variant::Type::Hash: {
        if (table->arrayMode) {
            if (table->arrayFlags & Array_SingleHash) {
                Row r;
                r.id = "#0";
                for (QList<AcquisitionComponentGenericTableColumn *>::const_iterator
                        column = table->columns.constBegin(),
                        endColumns = table->columns.constEnd(); column != endColumns; ++column) {
                    r.data.append((*column)->initializeData(QString("."), table->initialValue));
                }
                rows.append(r);
            } else if (table->arrayFlags & Array_HashChildren) {
                QStringList sorted;
                for (const auto &add : baseData.toHash().keys()) {
                    sorted.push_back(QString::fromStdString(add));
                }
                std::sort(sorted.begin(), sorted.end());
                int index = 0;
                for (QStringList::const_iterator c = sorted.constBegin(), endC = sorted.constEnd();
                        c != endC;
                        ++c, ++index) {
                    if (c->isEmpty())
                        continue;

                    Row r;
                    r.id = QString("#%1").arg(index);
                    for (QList<AcquisitionComponentGenericTableColumn *>::const_iterator
                            column = table->columns.constBegin(),
                            endColumns = table->columns.constEnd();
                            column != endColumns;
                            ++column) {
                        r.data.append((*column)->initializeData(*c, table->initialValue));
                    }
                    rows.append(r);
                }
            }
            break;
        }
        QRegularExpression validName("[a-z][0-9a-z_]*", QRegularExpression::CaseInsensitiveOption);

        for (auto c : baseData.toHash()) {
            if (c.first.empty())
                continue;
            if (!Util::exact_match(QString::fromStdString(c.first), validName))
                continue;

            Row r;
            r.id = QString::fromStdString(c.first);
            for (QList<AcquisitionComponentGenericTableColumn *>::const_iterator
                    column = table->columns.constBegin(), endColumns = table->columns.constEnd();
                    column != endColumns;
                    ++column) {
                r.data.append((*column)->initializeData(r.id, table->initialValue));
            }
            rows.append(r);
        }
        break;
    }
    case Variant::Type::Array: {
        for (std::size_t index = 0, max = baseData.toArray().size(); index < max; ++index) {
            Row r;
            r.id = QString("#%1").arg(static_cast<int>(index));

            for (QList<AcquisitionComponentGenericTableColumn *>::const_iterator
                    column = table->columns.constBegin(), endColumns = table->columns.constEnd();
                    column != endColumns;
                    ++column) {
                r.data.append((*column)->initializeData(r.id, table->initialValue));
            }
            rows.append(r);
        }
        break;
    }
    default:
        break;
    }

    endResetModel();
}

}
}
}

Q_DECLARE_METATYPE(CPD3::GUI::Data::Internal::TimeIntervalColumnData);
