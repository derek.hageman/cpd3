/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3GUIDATAPAGES_HXX
#define CPD3GUIDATAPAGES_HXX

#include "core/first.hxx"

#include <QWidget>
#include <QDialog>
#include <QItemDelegate>
#include <QVBoxLayout>

#include "guidata/guidata.hxx"
#include "guidata/editors/acquisition/component.hxx"

namespace CPD3 {
namespace GUI {
namespace Data {
namespace Internal {

class AcquisitionComponentTabCompositePage : public AcquisitionComponentPage {
    QString pageTitle;
    QList<AcquisitionComponentPage *> subpages;
    QTabWidget *tabs;
public:
    AcquisitionComponentTabCompositePage(const QString &title,
                                         const CPD3::Data::Variant::Read &value,
                                         QWidget *parent = 0);

    virtual ~AcquisitionComponentTabCompositePage();

    void commit(CPD3::Data::Variant::Write &target) override;

    virtual QString title() const;

    void addPage(AcquisitionComponentPage *page);
};

class AcquisitionComponentBoxCompositePage : public AcquisitionComponentPage {
    QString pageTitle;
    QList<AcquisitionComponentPage *> subpages;
    QVBoxLayout *layout;
public:
    AcquisitionComponentBoxCompositePage(const QString &title,
                                         const CPD3::Data::Variant::Read &value,
                                         QWidget *parent = 0);

    virtual ~AcquisitionComponentBoxCompositePage();

    void commit(CPD3::Data::Variant::Write &target) override;

    virtual QString title() const;

    void addPage(AcquisitionComponentPage *page, int stretch = 1);
};

class AcquisitionComponentTypedCompositePage : public AcquisitionComponentPage {
Q_OBJECT

    QString pageTitle;
    QString typePath;
    QComboBox *typeSelect;
    QList<AcquisitionComponentPage *> subpages;
    QVBoxLayout *layout;

    QString originalType;
public:
    AcquisitionComponentTypedCompositePage(const QString &title,
                                           const CPD3::Data::Variant::Read &value,
                                           const QString &typePath,
                                           QWidget *parent = 0);

    virtual ~AcquisitionComponentTypedCompositePage();

    void commit(CPD3::Data::Variant::Write &target) override;

    virtual QString title() const;

    void addPage(AcquisitionComponentPage *page,
                 const QStringList &typeCode,
                 const QString &toolTip = QString());

private slots:

    void typeChanged();
};

class AcquisitionComponentTableDelegatePages : public QItemDelegate {
public:

    AcquisitionComponentTableDelegatePages(QObject *parent = 0);

    virtual ~AcquisitionComponentTableDelegatePages();

    virtual QWidget *createEditor(QWidget *parent,
                                  const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const;

    virtual void setEditorData(QWidget *editor, const QModelIndex &index) const;

    virtual void setModelData(QWidget *editor,
                              QAbstractItemModel *model,
                              const QModelIndex &index) const;


    virtual QList<AcquisitionComponentPage *> createPages(const CPD3::Data::Variant::Read &value,
                                                          QWidget *parent = 0) const = 0;

    virtual QString buttonText(const CPD3::Data::Variant::Read &value) const;
};

}
}
}
}

#endif //CPD3GUIDATEPAGES_HXX
