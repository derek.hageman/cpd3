/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtAlgorithms>
#include <QVBoxLayout>
#include <QDialog>
#include <QDialogButtonBox>
#include <QVBoxLayout>
#include <QComboBox>

#include "guidata/editors/enumeration.hxx"
#include "core/util.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace GUI {
namespace Data {

/** @file guidata/editors/enumeration.hxx
 * Value editors for enumerations.
 */


bool EnumerationEndpoint::matches(const Variant::Read &value, const Variant::Read &metadata)
{
    return Util::equal_insensitive(metadata.metadata("Editor").hash("Type").toString(),
                                   "Enumeration");
}

static Variant::Read findEnumerationValue(const Variant::Read &value,
                                          const Variant::Read &metadata,
                                          std::string *keyName = nullptr)
{
    const auto &text = value.toString();
    if (text.empty())
        return Variant::Read::empty();

    for (auto check : metadata.metadata("EnumerationValues").toHash()) {
        if (!Util::equal_insensitive(check.first, text)) {
            bool hit = false;
            for (const auto &alias : check.second.hash("Alias").toFlags()) {
                if (!Util::equal_insensitive(alias, text))
                    continue;
                hit = true;
                break;
            }
            if (!hit)
                continue;
        }

        if (keyName)
            *keyName = check.first;
        return std::move(check.second);
    }

    return Variant::Read::empty();
}

bool EnumerationEndpoint::valid(const Variant::Read &value, const Variant::Read &metadata)
{
    auto ev = findEnumerationValue(value, metadata);
    if (!ev.exists())
        return false;

    return ValueEndpointEditor::valid(value, metadata);
}

QString EnumerationEndpoint::collapsedText(const Variant::Read &value,
                                           const Variant::Read &metadata)
{
    std::string key;
    auto ev = findEnumerationValue(value, metadata, &key);
    if (!ev.exists())
        return ValueEndpointEditor::collapsedText(value, metadata);

    QString check(ev.hash("Text").toDisplayString());
    if (!check.isEmpty())
        return check;

    return QString::fromStdString(key);
}

bool EnumerationEndpoint::edit(Variant::Write &value,
                               const Variant::Read &metadata,
                               QWidget *parent)
{
    const auto &text = value.toString();
    std::vector<std::pair<QString, Variant::Read>> sortedValues;
    {
        for (auto add : metadata.metadata("EnumerationValues").toHash()) {
            if (add.first.empty())
                continue;
            sortedValues.emplace_back(QString::fromStdString(add.first), std::move(add.second));
        }
    }
    if (sortedValues.empty())
        return false;
    std::sort(sortedValues.begin(), sortedValues.end(),
              [](const std::pair<QString, Variant::Read> &a,
                 const std::pair<QString, Variant::Read> &b) {
                  qint64 pa = a.second.hash("SortPriority").toInt64();
                  qint64 pb = b.second.hash("SortPriority").toInt64();
                  if (INTEGER::defined(pa)) {
                      if (!INTEGER::defined(pb))
                          return true;
                      if (pa != pb)
                          return pa < pb;
                  } else if (INTEGER::defined(pb)) {
                      return false;
                  }

                  return a.first < b.first;
              });

    QDialog dialog(parent);
    dialog.setWindowTitle(QObject::tr("Enumeration"));
    QVBoxLayout *layout = new QVBoxLayout(&dialog);
    dialog.setLayout(layout);

    QComboBox *selector = new QComboBox(&dialog);
    layout->addWidget(selector, 1);
    for (const auto &add : sortedValues) {
        bool isSelected = Util::equal_insensitive(add.first.toStdString(), text);
        if (!isSelected) {
            for (const auto &alias : add.second.hash("Alias").toFlags()) {
                if (!Util::equal_insensitive(alias, text))
                    continue;
                isSelected = true;
                break;
            }
        }

        QString itemText(add.second.hash("Text").toDisplayString());
        if (itemText.isEmpty())
            itemText = add.first;

        selector->addItem(itemText, add.first);
        if (isSelected) {
            selector->setCurrentIndex(selector->count() - 1);
        }

        itemText = add.second.hash("Description").toDisplayString();
        if (!itemText.isEmpty()) {
            selector->setItemData(selector->count() - 1, itemText, Qt::ToolTipRole);
        }
    }

    QDialogButtonBox *buttons =
            new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal,
                                 &dialog);
    layout->addWidget(buttons);
    QObject::connect(buttons, SIGNAL(accepted()), &dialog, SLOT(accept()));
    QObject::connect(buttons, SIGNAL(rejected()), &dialog, SLOT(reject()));

    if (dialog.exec() != QDialog::Accepted)
        return false;

    int index = selector->currentIndex();
    if (index < 0 || index >= selector->count())
        return false;
    value.setString(selector->itemData(index).toString());
    return true;
}


}
}
}

