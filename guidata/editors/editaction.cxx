/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QGroupBox>
#include <QFormLayout>
#include <QHeaderView>
#include <QInputDialog>
#include <QApplication>
#include <QSplitter>
#include <QSettings>
#include <QStyledItemDelegate>
#include <QItemEditorFactory>
#include <QDialog>
#include <QDialogButtonBox>

#include "guidata/editors/editaction.hxx"
#include "guicore/scriptedit.hxx"
#include "guicore/guiformat.hxx"
#include "core/timeparse.hxx"
#include "core/range.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/composite.hxx"

using namespace CPD3::Data;
using namespace CPD3::GUI::Data::Internal;

namespace CPD3 {
namespace GUI {
namespace Data {

/** @file guidata/editors/editaction.hxx
 * An editor for edit directive actions.
 */


namespace {

static QWidget *createDescriptionWidget(const QString &description)
{
    QWidget *widget = new QWidget;
    QVBoxLayout *layout = new QVBoxLayout;
    widget->setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    QLabel *text = new QLabel(description, widget);
    text->setWordWrap(true);
    layout->addWidget(text);
    layout->insertStretch(-1, 1);

    return widget;
}

class EditActionNOOP : public EditActionType {
public:
    EditActionNOOP() = default;

    virtual ~EditActionNOOP() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("No operation"); }

    virtual QString getInternalType() const
    { return "NOOP"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "NOOP", "None"); }

    virtual QString getToolTip() const
    { return EditActionEditor::tr("Do nothing to the data."); }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("No operation"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type do not perform any action directly.  This can be used as a placeholder or for EBAS flagging only.");
    }

    QWidget *createEditor(Variant::Write &, EditActionEditor *, const char *) const override
    {
        return createDescriptionWidget(EditActionEditor::tr(
                "This edit perform no action directly.  This can be used either as a "
                        "placeholder or to create edits that only operate on EBAS flags."));
    }
};

class EditActionInvalidate : public EditActionType {
public:
    EditActionInvalidate() = default;

    virtual ~EditActionInvalidate() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Invalidate"); }

    virtual QString getInternalType() const
    { return "Invalidate"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Invalidate"); }

    virtual QString getToolTip() const
    { return EditActionEditor::tr("Set values to an explicit missing code."); }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Invalidate data"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type change the values in the data to be explicitly missing.  The record of their existence in time remains but the actual meaningful value is erased.");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditActionEditor *editor,
                          const char *updateSlot) const override
    {
        QWidget *widget = new QWidget;
        QVBoxLayout *layout = new QVBoxLayout;
        layout->setContentsMargins(0, 0, 0, 0);
        widget->setLayout(layout);

        EditActionSelectionEditor *selection =
                new EditActionSelectionEditor(target.hash("Selection"),
                                              EditActionSelectionEditor::Operate_Target, editor,
                                              widget);
        layout->addWidget(selection);
        QObject::connect(selection, SIGNAL(changed()), editor, updateSlot);

        EditActionBooleanEditor *preserve = new EditActionBooleanEditor(target, "PreserveType",
                                                                        EditActionEditor::tr(
                                                                                "&Preserve type"),
                                                                        EditActionEditor::tr(
                                                                                "Preserve the data type of the value after invalidation."),
                                                                        EditActionEditor::tr(
                                                                                "Preserve data type"),
                                                                        EditActionEditor::tr(
                                                                                "If this is set then the data type of the input value is preserved if a specific missing value code exists for it.  That is, when this is not set the value is replaced with a globally undefined value instead of an internal missing code."),
                                                                        true, widget);
        layout->addWidget(preserve);
        QObject::connect(preserve, SIGNAL(changed()), editor, updateSlot);

        return widget;
    }
};

class EditActionRemove : public EditActionType {
public:
    EditActionRemove() = default;

    virtual ~EditActionRemove() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Remove"); }

    virtual QString getInternalType() const
    { return "Remove"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Remove"); }

    virtual QString getToolTip() const
    { return EditActionEditor::tr("Remove values from the data."); }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Remove data"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type modify the data so that the affected variables are removed completely.  No record of their presence is retained and only a gap remains.");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditActionEditor *editor,
                          const char *updateSlot) const override
    {
        QWidget *widget = new QWidget;
        QVBoxLayout *layout = new QVBoxLayout;
        layout->setContentsMargins(0, 0, 0, 0);
        widget->setLayout(layout);

        EditActionSelectionEditor *selection =
                new EditActionSelectionEditor(target.hash("Selection"),
                                              EditActionSelectionEditor::Operate_Target, editor,
                                              widget);
        layout->addWidget(selection);
        QObject::connect(selection, SIGNAL(changed()), editor, updateSlot);

        return widget;
    }
};

class EditActionPoly : public EditActionType {
public:
    EditActionPoly() = default;

    virtual ~EditActionPoly() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Apply Calibration"); }

    virtual QString getInternalType() const
    { return "Polynomial"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Poly", "Polynomial", "Cal", "Calibration"); }

    virtual QString getToolTip() const
    { return EditActionEditor::tr("Apply a calibration polynomial to the data."); }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Apply calibration"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type change the values in the data by applying a calibration polynomial.");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditActionEditor *editor,
                          const char *updateSlot) const override
    {
        QWidget *widget = new QWidget;
        QVBoxLayout *layout = new QVBoxLayout;
        layout->setContentsMargins(0, 0, 0, 0);
        widget->setLayout(layout);

        EditActionSelectionEditor *selection =
                new EditActionSelectionEditor(target.hash("Selection"),
                                              EditActionSelectionEditor::Operate_Input, editor,
                                              widget);
        QSizePolicy policy(selection->sizePolicy());
        policy.setVerticalStretch(1);
        selection->setSizePolicy(policy);
        layout->addWidget(selection);
        QObject::connect(selection, SIGNAL(changed()), editor, updateSlot);

        EditActionCalibrationEditor *poly = new EditActionCalibrationEditor(target, false, widget);
        policy = poly->sizePolicy();
        policy.setVerticalStretch(0);
        poly->setSizePolicy(policy);
        layout->addWidget(poly);
        QObject::connect(poly, SIGNAL(changed()), editor, updateSlot);

        return widget;
    }
};

class EditActionInvertPoly : public EditActionType {
public:
    EditActionInvertPoly() = default;

    virtual ~EditActionInvertPoly() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Reverse Calibration"); }

    virtual QString getInternalType() const
    { return "PolynomialInvert"; }

    bool matchesType(const std::string &type) const override
    {
        return Util::equal_insensitive(type, "PolyInvert", "PolynomialInvert", "InvertCal",
                                       "InvertCalibration");
    }

    virtual QString getToolTip() const
    { return EditActionEditor::tr("Reverse the effect of a calibration polynomial."); }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Reverse calibration"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type reverse the effects of a previously applied calibration polynomial.  The data is changed so that it has the original values before the polynomial was applied.");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditActionEditor *editor,
                          const char *updateSlot) const override
    {
        QWidget *widget = new QWidget;
        QVBoxLayout *layout = new QVBoxLayout;
        layout->setContentsMargins(0, 0, 0, 0);
        widget->setLayout(layout);

        EditActionSelectionEditor *selection =
                new EditActionSelectionEditor(target.hash("Selection"),
                                              EditActionSelectionEditor::Operate_Input, editor,
                                              widget);
        QSizePolicy policy(selection->sizePolicy());
        policy.setVerticalStretch(1);
        selection->setSizePolicy(policy);
        layout->addWidget(selection);
        QObject::connect(selection, SIGNAL(changed()), editor, updateSlot);

        EditActionCalibrationEditor *poly = new EditActionCalibrationEditor(target, true, widget);
        policy = poly->sizePolicy();
        policy.setVerticalStretch(0);
        poly->setSizePolicy(policy);
        layout->addWidget(poly);
        QObject::connect(poly, SIGNAL(changed()), editor, updateSlot);

        return widget;
    }
};

class EditActionRecalibrate : public EditActionType {
public:
    EditActionRecalibrate() = default;

    virtual ~EditActionRecalibrate() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Alter Calibration"); }

    virtual QString getInternalType() const
    { return "Recalibrate"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Recalibrate"); }

    virtual QString getToolTip() const
    {
        return EditActionEditor::tr(
                "Alter a calibration by reversing the original and applying a new one.");
    }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Alter calibration"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type first back out an original (incorrect) calibration then apply a new (corrected) one to the data.");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditActionEditor *editor,
                          const char *updateSlot) const override
    {
        QWidget *widget = new QWidget;
        QVBoxLayout *layout = new QVBoxLayout;
        layout->setContentsMargins(0, 0, 0, 0);
        widget->setLayout(layout);

        EditActionSelectionEditor *selection =
                new EditActionSelectionEditor(target.hash("Selection"),
                                              EditActionSelectionEditor::Operate_Input, editor,
                                              widget);
        QSizePolicy policy(selection->sizePolicy());
        policy.setVerticalStretch(1);
        selection->setSizePolicy(policy);
        layout->addWidget(selection);
        QObject::connect(selection, SIGNAL(changed()), editor, updateSlot);

        EditActionRecalibrationEditor *poly = new EditActionRecalibrationEditor(target, widget);
        policy = poly->sizePolicy();
        policy.setVerticalStretch(0);
        poly->setSizePolicy(policy);
        layout->addWidget(poly);
        QObject::connect(poly, SIGNAL(changed()), editor, updateSlot);

        return widget;
    }
};

class EditActionMultiPoly : public EditActionType {
public:
    EditActionMultiPoly() = default;

    virtual ~EditActionMultiPoly() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Interpolate Calibration"); }

    virtual QString getInternalType() const
    { return "MultiPolynomial"; }

    bool matchesType(const std::string &type) const override
    {
        return Util::equal_insensitive(type, "MultiPoly", "MultiPolynomial", "MultiCal",
                                       "MultiCalibration");
    }

    virtual QString getToolTip() const
    {
        return EditActionEditor::tr(
                "Apply a calibration polynomial interpolated in time to the data.");
    }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Interpolate calibrations"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type change the values in the data by applying a series of calibration polynomials interpolated together in time.");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditActionEditor *editor,
                          const char *updateSlot) const override
    {
        QTabWidget *widget = new QTabWidget;

        EditActionSelectionEditor *selection =
                new EditActionSelectionEditor(target.hash("Selection"),
                                              EditActionSelectionEditor::Operate_Input, editor,
                                              widget);
        widget->addTab(selection, EditActionEditor::tr("&Data"));
        widget->setTabToolTip(0, EditActionEditor::tr("The selection of data to calibrate."));
        widget->setTabWhatsThis(0, EditActionEditor::tr(
                "This is the selection of data that the interpolated calibrations will apply to."));
        QObject::connect(selection, SIGNAL(changed()), editor, updateSlot);

        EditActionMultiCalibrationPointEditor
                *points = new EditActionMultiCalibrationPointEditor(target, widget);
        widget->addTab(points, EditActionEditor::tr("&Calibrations"));
        widget->setTabToolTip(1, EditActionEditor::tr("The calibrations to interpolate between."));
        widget->setTabWhatsThis(1, EditActionEditor::tr(
                "This is the listing of calibrations that will be interpolated together to produce the calibration applied to the selected data."));
        QObject::connect(points, SIGNAL(changed()), editor, updateSlot);

        EditActionMultiCalibrationInterpolationEditor
                *interpolation = new EditActionMultiCalibrationInterpolationEditor(target, widget);
        widget->addTab(interpolation, EditActionEditor::tr("&Interpolation"));
        widget->setTabToolTip(2, EditActionEditor::tr("The interpolation settings."));
        widget->setTabWhatsThis(2, EditActionEditor::tr(
                "This controls the specific interpolation settings used to generate the final functions that define the calibration as a function of time."));
        QObject::connect(interpolation, SIGNAL(changed()), editor, updateSlot);

        return widget;
    }
};

class EditActionWrapModular : public EditActionType {
public:
    EditActionWrapModular() = default;

    virtual ~EditActionWrapModular() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Wrap data"); }

    virtual QString getInternalType() const
    { return "Wrap"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Wrap", "Modular", "Modulus"); }

    virtual QString getToolTip() const
    { return EditActionEditor::tr("Wrap data to within a range."); }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Wrap data"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type change the values in the data by wrapping it to be within a range.  If the data are below the start of the range then the width of the range is added until they are within it.  If they are above the end the width is subtracted.");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditActionEditor *editor,
                          const char *updateSlot) const override
    {
        QWidget *widget = new QWidget;
        QVBoxLayout *layout = new QVBoxLayout;
        layout->setContentsMargins(0, 0, 0, 0);
        widget->setLayout(layout);

        EditActionSelectionEditor *selection =
                new EditActionSelectionEditor(target.hash("Selection"),
                                              EditActionSelectionEditor::Operate_Input, editor,
                                              widget);
        QSizePolicy policy(selection->sizePolicy());
        policy.setVerticalStretch(1);
        selection->setSizePolicy(policy);
        layout->addWidget(selection);
        QObject::connect(selection, SIGNAL(changed()), editor, updateSlot);

        EditActionWrapModularEditor *poly = new EditActionWrapModularEditor(target, widget);
        policy = poly->sizePolicy();
        policy.setVerticalStretch(0);
        poly->setSizePolicy(policy);
        layout->addWidget(poly);
        QObject::connect(poly, SIGNAL(changed()), editor, updateSlot);

        return widget;
    }
};

class EditActionContaminate : public EditActionType {
public:
    EditActionContaminate() = default;

    virtual ~EditActionContaminate() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Apply Contamination"); }

    virtual QString getInternalType() const
    { return "Contaminate"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Contaminate", "Contam"); }

    virtual QString getToolTip() const
    { return EditActionEditor::tr("Set a contamination flag."); }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Set contamination"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type set a contamination flag in the data.  Contaminated data is still considered valid but is not averaged in the final output.");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditActionEditor *editor,
                          const char *updateSlot) const override
    {
        QWidget *widget = new QWidget;
        QVBoxLayout *layout = new QVBoxLayout;
        layout->setContentsMargins(0, 0, 0, 0);
        widget->setLayout(layout);

        EditActionContaminateEditor *cont = new EditActionContaminateEditor(target, widget);
        QSizePolicy policy(cont->sizePolicy());
        policy.setVerticalStretch(0);
        cont->setSizePolicy(policy);
        layout->addWidget(cont);
        QObject::connect(cont, SIGNAL(changed()), editor, updateSlot);

        layout->insertStretch(-1, 1);

        return widget;
    }
};

class EditActionUncontaminate : public EditActionType {
public:
    EditActionUncontaminate() = default;

    virtual ~EditActionUncontaminate() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Clear Contamination"); }

    virtual QString getInternalType() const
    { return "Uncontaminate"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Uncontaminate", "ClearContam"); }

    virtual QString getToolTip() const
    { return EditActionEditor::tr("Clear all data contamination flags."); }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Clear contamination"); }

    virtual QString getWhatsThis() const
    { return EditActionEditor::tr("Edits of this type clear all system contamination flags."); }

    QWidget *createEditor(Variant::Write &, EditActionEditor *, const char *) const override
    { return new QWidget; }
};

class EditActionFlowCorrection : public EditActionType {
public:
    EditActionFlowCorrection() = default;

    virtual ~EditActionFlowCorrection() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Flow Calibration Correction"); }

    virtual QString getInternalType() const
    { return "FlowCorrection"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "FlowCorrection", "Flowcalibration"); }

    virtual QString getToolTip() const
    { return EditActionEditor::tr("Alter the flow calibration correction in use."); }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Flow calibration"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type alter the flow calibration in use.  This changes not only the flow but the parameters derived from the air volume passing through the instrument.");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditActionEditor *editor,
                          const char *updateSlot) const override
    {
        QWidget *widget = new QWidget;
        QVBoxLayout *layout = new QVBoxLayout;
        layout->setContentsMargins(0, 0, 0, 0);
        widget->setLayout(layout);

        QList<EditActionInstrumentOverrideEditor::ParameterData> parameters;
        EditActionInstrumentOverrideEditor::ParameterData add;

        add.configPath = "Flow";
        add.label = EditActionEditor::tr("&Flow");
        add.toolTip = EditActionEditor::tr("The flow variable.");
        add.whatsThis = EditActionEditor::tr(
                "This is the variable that represents the flow measurement to be changed.");
        parameters.append(add);

        add.configPath = "Accumulators";
        add.label = EditActionEditor::tr("&Accumulators");
        add.toolTip = EditActionEditor::tr("The accumulated variables dependent on the flow.");
        add.whatsThis = EditActionEditor::tr(
                "This is variable or variables that are accumulated based on integration of the flow.  For example the sample length and volume.");
        parameters.append(add);

        add.configPath = "Spot";
        add.label = EditActionEditor::tr("&Spot");
        add.toolTip = EditActionEditor::tr("The spot parameter variables.");
        add.whatsThis = EditActionEditor::tr(
                "This is variable or variables that represent total spot information.");
        parameters.append(add);

        add.configPath = "Selection";
        add.label = EditActionEditor::tr("&Variables");
        add.toolTip =
                EditActionEditor::tr("The variables that are dependent on the sample volume.");
        add.whatsThis = EditActionEditor::tr(
                "This is the variable or variables that are dependent on the volume of air sampled.  For example, the absorption coefficients.");
        parameters.append(add);


        EditActionRecalibrationEditor *poly = new EditActionRecalibrationEditor(target, widget);
        QSizePolicy policy(poly->sizePolicy());
        policy.setVerticalStretch(0);
        poly->setSizePolicy(policy);
        layout->addWidget(poly);
        QObject::connect(poly, SIGNAL(changed()), editor, updateSlot);

        EditActionBooleanEditor *combine = new EditActionBooleanEditor(target, "CombineCut",
                                                                       EditActionEditor::tr(
                                                                               "&Unified cut size handling"),
                                                                       EditActionEditor::tr(
                                                                               "Unify the handling of the accumulators across all cut sizes."),
                                                                       EditActionEditor::tr(
                                                                               "Unified cut size"),
                                                                       EditActionEditor::tr(
                                                                               "If this is set then the handling of accumulators is unified across cut sizes.  That means that the accumulator total ignores the cut size currently in effect.  This is the normal handling of accumulators so this option should usually be enabled."),
                                                                       true, widget);
        policy = combine->sizePolicy();
        policy.setVerticalStretch(0);
        combine->setSizePolicy(policy);
        layout->addWidget(combine);
        QObject::connect(combine, SIGNAL(changed()), editor, updateSlot);

        EditActionInstrumentOverrideEditor *selection =
                new EditActionInstrumentOverrideEditor(target, editor, parameters, widget);
        policy = selection->sizePolicy();
        policy.setVerticalStretch(1);
        selection->setSizePolicy(policy);
        layout->addWidget(selection);
        QObject::connect(selection, SIGNAL(changed()), editor, updateSlot);

        return widget;
    }
};

class EditActionSpotCorrection : public EditActionType {
public:
    EditActionSpotCorrection() = default;

    virtual ~EditActionSpotCorrection() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Single Spot Size Correction"); }

    virtual QString getInternalType() const
    { return "SpotSize"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Spot", "SpotSize"); }

    virtual QString getToolTip() const
    { return EditActionEditor::tr("Alter the spot size use in the data."); }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Spot size correction"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type alter the spot size used in the generation of physical units.  This changes the accumulated values for integrated parameters (e.x. sample length) as the derived values (e.x. absorption coefficients).");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditActionEditor *editor,
                          const char *updateSlot) const override
    {
        QWidget *widget = new QWidget;
        QVBoxLayout *layout = new QVBoxLayout;
        layout->setContentsMargins(0, 0, 0, 0);
        widget->setLayout(layout);

        QList<EditActionInstrumentOverrideEditor::ParameterData> parameters;
        EditActionInstrumentOverrideEditor::ParameterData add;

        add.configPath = "Spot";
        add.label = EditActionEditor::tr("&Spot");
        add.toolTip = EditActionEditor::tr("The spot parameter variables.");
        add.whatsThis = EditActionEditor::tr(
                "This is variable or variables that represent total spot information.");
        parameters.append(add);

        add.configPath = "Length";
        add.label = EditActionEditor::tr("&Length");
        add.toolTip =
                EditActionEditor::tr("The accumulated length variables dependent on the flow.");
        add.whatsThis = EditActionEditor::tr(
                "This is variable or variables that are accumulated based on integration of the flow and spot size.  For example the sample length.");
        parameters.append(add);

        add.configPath = "Selection";
        add.label = EditActionEditor::tr("&Variables");
        add.toolTip =
                EditActionEditor::tr("The variables that are dependent on the sample volume.");
        add.whatsThis = EditActionEditor::tr(
                "This is the variable or variables that are dependent on the volume of air sampled.  For example, the absorption coefficients.");
        parameters.append(add);


        EditActionSpotCorrectionEditor *spot = new EditActionSpotCorrectionEditor(target, widget);
        QSizePolicy policy(spot->sizePolicy());
        policy.setVerticalStretch(0);
        spot->setSizePolicy(policy);
        layout->addWidget(spot);
        QObject::connect(spot, SIGNAL(changed()), editor, updateSlot);

        EditActionInstrumentOverrideEditor *selection =
                new EditActionInstrumentOverrideEditor(target, editor, parameters, widget);
        policy = selection->sizePolicy();
        policy.setVerticalStretch(1);
        selection->setSizePolicy(policy);
        layout->addWidget(selection);
        QObject::connect(selection, SIGNAL(changed()), editor, updateSlot);

        return widget;
    }
};

class EditActionMultiSpotCorrection : public EditActionType {
public:
    EditActionMultiSpotCorrection() = default;

    virtual ~EditActionMultiSpotCorrection() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Multiple Spot Size Correction"); }

    virtual QString getInternalType() const
    { return "MultiSpotSize"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "MultiSpot", "MultiSpotSize", "CLAPSpot"); }

    virtual QString getToolTip() const
    {
        return EditActionEditor::tr(
                "Alter the spot size use in the data for multiple spot instruments.");
    }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Multiple spot size correction"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type alter the spot size used in the generation of physical units for instruments with multiple spots (e.x. the CLAP-3W).  This changes the accumulated values for integrated parameters (e.x. sample length) as the derived values (e.x. absorption coefficients).");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditActionEditor *editor,
                          const char *updateSlot) const override
    {
        QWidget *widget = new QWidget;
        QVBoxLayout *layout = new QVBoxLayout;
        layout->setContentsMargins(0, 0, 0, 0);
        widget->setLayout(layout);

        QList<EditActionInstrumentOverrideEditor::ParameterData> parameters;
        EditActionInstrumentOverrideEditor::ParameterData add;

        add.configPath = "Spot";
        add.label = EditActionEditor::tr("&Spot");
        add.toolTip = EditActionEditor::tr("The spot parameter variables.");
        add.whatsThis = EditActionEditor::tr(
                "This is variable or variables that represent total spot information.");
        parameters.append(add);

        add.configPath = "Index";
        add.label = EditActionEditor::tr("&Index");
        add.toolTip = EditActionEditor::tr("The active spot index variable.");
        add.whatsThis =
                EditActionEditor::tr("This is variable that contains the active spot index.");
        parameters.append(add);

        add.configPath = "Length";
        add.label = EditActionEditor::tr("&Length");
        add.toolTip =
                EditActionEditor::tr("The accumulated length variables dependent on the flow.");
        add.whatsThis = EditActionEditor::tr(
                "This is variable or variables that are accumulated based on integration of the flow and spot size.  For example the sample length.");
        parameters.append(add);

        add.configPath = "Selection";
        add.label = EditActionEditor::tr("&Variables");
        add.toolTip =
                EditActionEditor::tr("The variables that are dependent on the sample volume.");
        add.whatsThis = EditActionEditor::tr(
                "This is the variable or variables that are dependent on the volume of air sampled.  For example, the absorption coefficients.");
        parameters.append(add);


        EditActionMultiSpotCorrectionEditor
                *spot = new EditActionMultiSpotCorrectionEditor(target, widget);
        QSizePolicy policy(spot->sizePolicy());
        policy.setVerticalStretch(0);
        spot->setSizePolicy(policy);
        layout->addWidget(spot);
        QObject::connect(spot, SIGNAL(changed()), editor, updateSlot);

        EditActionInstrumentOverrideEditor *selection =
                new EditActionInstrumentOverrideEditor(target, editor, parameters, widget);
        policy = selection->sizePolicy();
        policy.setVerticalStretch(1);
        selection->setSizePolicy(policy);
        layout->addWidget(selection);
        QObject::connect(selection, SIGNAL(changed()), editor, updateSlot);

        return widget;
    }
};

class EditActionRemoveFlags : public EditActionType {
public:
    EditActionRemoveFlags() = default;

    virtual ~EditActionRemoveFlags() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Unset Specific Flags"); }

    virtual QString getInternalType() const
    { return "RemoveFlags"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "RemoveFlag", "RemoveFlags"); }

    virtual QString getToolTip() const
    { return EditActionEditor::tr("Unset flags in the data."); }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Unset flags"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type unset/remove one or more flags from the data.  The flag continues to exist in the metadata but it is unset for the duration of the edit.");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditActionEditor *editor,
                          const char *updateSlot) const override
    {
        QWidget *widget = new QWidget;
        QVBoxLayout *layout = new QVBoxLayout;
        layout->setContentsMargins(0, 0, 0, 0);
        widget->setLayout(layout);

        EditActionSelectionEditor *selection =
                new EditActionSelectionEditor(target.hash("Selection"),
                                              EditActionSelectionEditor::Operate_Input, editor,
                                              widget);
        QSizePolicy policy(selection->sizePolicy());
        policy.setVerticalStretch(1);
        selection->setSizePolicy(policy);
        layout->addWidget(selection);
        QObject::connect(selection, SIGNAL(changed()), editor, updateSlot);

        EditActionFlagsEditor *flags = new EditActionFlagsEditor(target, widget);
        policy = flags->sizePolicy();
        policy.setVerticalStretch(0);
        flags->setSizePolicy(policy);
        layout->addWidget(flags);
        QObject::connect(flags, SIGNAL(changed()), editor, updateSlot);

        return widget;
    }
};

class EditActionAddFlags : public EditActionType {
public:
    EditActionAddFlags() = default;

    virtual ~EditActionAddFlags() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Set Flags"); }

    virtual QString getInternalType() const
    { return "AddFlags"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Flag", "AddFlag", "AddFlags"); }

    virtual QString getToolTip() const
    { return EditActionEditor::tr("Set flags in the data."); }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Set flags"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type set one or more flags.  The flags being set can be new flags introduced entirely by the edit.");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditActionEditor *editor,
                          const char *updateSlot) const override
    {
        QWidget *widget = new QWidget;
        QVBoxLayout *layout = new QVBoxLayout;
        layout->setContentsMargins(0, 0, 0, 0);
        widget->setLayout(layout);

        EditActionSelectionEditor *selection =
                new EditActionSelectionEditor(target.hash("Selection"),
                                              EditActionSelectionEditor::Operate_Input, editor,
                                              widget);
        QSizePolicy policy(selection->sizePolicy());
        policy.setVerticalStretch(1);
        selection->setSizePolicy(policy);
        layout->addWidget(selection);
        QObject::connect(selection, SIGNAL(changed()), editor, updateSlot);

        EditActionFlagsEditor *flags = new EditActionFlagsEditor(target, widget);
        policy = flags->sizePolicy();
        policy.setVerticalStretch(0);
        flags->setSizePolicy(policy);
        layout->addWidget(flags);
        QObject::connect(flags, SIGNAL(changed()), editor, updateSlot);

        EditActionFlagsParametersEditor
                *params = new EditActionFlagsParametersEditor(target, widget);
        policy = params->sizePolicy();
        policy.setVerticalStretch(0);
        params->setSizePolicy(policy);
        layout->addWidget(params);
        QObject::connect(params, SIGNAL(changed()), editor, updateSlot);

        return widget;
    }
};

class EditActionRemoveMatchedFlags : public EditActionType {
public:
    EditActionRemoveMatchedFlags() = default;

    virtual ~EditActionRemoveMatchedFlags() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Unset Matching Flags"); }

    virtual QString getInternalType() const
    { return "RemoveMatchingFlags"; }

    bool matchesType(const std::string &type) const override
    {
        return Util::equal_insensitive(type, "RemoveAnyFlag", "RemoveAnyFlags",
                                       "RemoveMatchingFlags");
    }

    virtual QString getToolTip() const
    { return EditActionEditor::tr("Unset flags that match patterns."); }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Unset matching flags"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type unset/remove flags that match one or more patterns from the data.  The flag continues to exist in the metadata but it is unset for the duration of the edit.");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditActionEditor *editor,
                          const char *updateSlot) const override
    {
        QWidget *widget = new QWidget;
        QVBoxLayout *layout = new QVBoxLayout;
        layout->setContentsMargins(0, 0, 0, 0);
        widget->setLayout(layout);

        EditActionSelectionEditor *selection =
                new EditActionSelectionEditor(target.hash("Selection"),
                                              EditActionSelectionEditor::Operate_Input, editor,
                                              widget);
        QSizePolicy policy(selection->sizePolicy());
        policy.setVerticalStretch(1);
        selection->setSizePolicy(policy);
        layout->addWidget(selection);
        QObject::connect(selection, SIGNAL(changed()), editor, updateSlot);

        EditActionMatchedFlagsEditor *flags = new EditActionMatchedFlagsEditor(target, widget);
        policy = flags->sizePolicy();
        policy.setVerticalStretch(0);
        flags->setSizePolicy(policy);
        layout->addWidget(flags);
        QObject::connect(flags, SIGNAL(changed()), editor, updateSlot);

        EditActionBooleanEditor *caseSensitive =
                new EditActionBooleanEditor(target, "CaseSensitive",
                                            EditActionEditor::tr("&Case sensitive"),
                                            EditActionEditor::tr(
                                                    "Match patterns considering the case of the characters."),
                                            EditActionEditor::tr("Pattern case sensitivity"),
                                            EditActionEditor::tr(
                                                    "If this is set then the patterns are required to match the case of the flags."),
                                            false, widget);
        policy = caseSensitive->sizePolicy();
        policy.setVerticalStretch(0);
        caseSensitive->setSizePolicy(policy);
        layout->addWidget(caseSensitive);
        QObject::connect(caseSensitive, SIGNAL(changed()), editor, updateSlot);

        EditActionBooleanEditor *checkAll =
                new EditActionBooleanEditor(target, "CheckAll", EditActionEditor::tr("Check &all"),
                                            EditActionEditor::tr(
                                                    "Check all flags instead of only declared ones."),
                                            EditActionEditor::tr("Check all flags"),
                                            EditActionEditor::tr(
                                                    "If this option is set all flags in the data are checked against all patterns.  When unset only flags declared in the metadata are considered.  Checking all flags incurs a speed penalty and is not normally necessary."),
                                            false, widget);
        policy = checkAll->sizePolicy();
        policy.setVerticalStretch(0);
        checkAll->setSizePolicy(policy);
        layout->addWidget(checkAll);
        QObject::connect(checkAll, SIGNAL(changed()), editor, updateSlot);

        return widget;
    }
};

class EditActionSetCut : public EditActionType {
public:
    EditActionSetCut() = default;

    virtual ~EditActionSetCut() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Change Cut Size Selection"); }

    virtual QString getInternalType() const
    { return "SetCut"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "SetCut", "Cut"); }

    virtual QString getToolTip() const
    { return EditActionEditor::tr("Change the cut size selection indicator in the data."); }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Set cut size selection"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type alter the cut size selection in the data.  The values are not altered but the cut size indicator is changed.");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditActionEditor *editor,
                          const char *updateSlot) const override
    {
        QWidget *widget = new QWidget;
        QVBoxLayout *layout = new QVBoxLayout;
        layout->setContentsMargins(0, 0, 0, 0);
        widget->setLayout(layout);

        EditActionSelectionEditor *selection =
                new EditActionSelectionEditor(target.hash("Selection"),
                                              EditActionSelectionEditor::Operate_Target, editor,
                                              widget);
        QSizePolicy policy(selection->sizePolicy());
        policy.setVerticalStretch(1);
        selection->setSizePolicy(policy);
        layout->addWidget(selection);
        QObject::connect(selection, SIGNAL(changed()), editor, updateSlot);

        EditActionCutSizeEditor *cut = new EditActionCutSizeEditor(target, widget);
        policy = cut->sizePolicy();
        policy.setVerticalStretch(0);
        cut->setSizePolicy(policy);
        layout->addWidget(cut);
        QObject::connect(cut, SIGNAL(changed()), editor, updateSlot);

        EditActionBooleanEditor *applyToMeta =
                new EditActionBooleanEditor(target, "ApplyToMetadata",
                                            EditActionEditor::tr("Apply to &metadata"),
                                            EditActionEditor::tr(
                                                    "Apply the alteration to the metdata as well."),
                                            EditActionEditor::tr("Apply alteration to metadata"),
                                            EditActionEditor::tr(
                                                    "If this option is set then any intersecting metadata is altered as well.  This is normally only required if changing to a cut size that was not part of the existing switched system."),
                                            false, widget);
        policy = applyToMeta->sizePolicy();
        policy.setVerticalStretch(0);
        applyToMeta->setSizePolicy(policy);
        layout->addWidget(applyToMeta);
        QObject::connect(applyToMeta, SIGNAL(changed()), editor, updateSlot);

        return widget;
    }
};

class EditActionFlavors : public EditActionType {
public:
    EditActionFlavors() = default;

    virtual ~EditActionFlavors() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Alter Data Flavors"); }

    virtual QString getInternalType() const
    { return "Flavors"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Flavors"); }

    virtual QString getToolTip() const
    { return EditActionEditor::tr("Add or remove flavors from the data."); }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Alter data flavors"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type alter the flavors of the selected data.  The values are unchanged but flavors can be added and/or removed from the data.");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditActionEditor *editor,
                          const char *updateSlot) const override
    {
        QWidget *widget = new QWidget;
        QVBoxLayout *layout = new QVBoxLayout;
        layout->setContentsMargins(0, 0, 0, 0);
        widget->setLayout(layout);

        EditActionSelectionEditor *selection =
                new EditActionSelectionEditor(target.hash("Selection"),
                                              EditActionSelectionEditor::Operate_Target, editor,
                                              widget);
        QSizePolicy policy(selection->sizePolicy());
        policy.setVerticalStretch(1);
        selection->setSizePolicy(policy);
        layout->addWidget(selection);
        QObject::connect(selection, SIGNAL(changed()), editor, updateSlot);

        EditActionFlavorsEditor *addFlavors =
                new EditActionFlavorsEditor(target.hash("Add"), editor, EditActionEditor::tr("Add"),
                                            widget);
        addFlavors->setToolTip(EditActionEditor::tr("Flavors to be added to the data."));
        addFlavors->setStatusTip(EditActionEditor::tr("Flavors to add"));
        addFlavors->setWhatsThis(EditActionEditor::tr(
                "Use this button to select the flavors that are added to the data."));
        policy = addFlavors->sizePolicy();
        policy.setVerticalStretch(0);
        addFlavors->setSizePolicy(policy);
        layout->addWidget(addFlavors);
        QObject::connect(addFlavors, SIGNAL(changed()), editor, updateSlot);

        EditActionFlavorsEditor *removeFlavors =
                new EditActionFlavorsEditor(target.hash("Remove"), editor,
                                            EditActionEditor::tr("Remove"), widget);
        removeFlavors->setToolTip(EditActionEditor::tr("Flavors to be removed from the data."));
        removeFlavors->setStatusTip(EditActionEditor::tr("Flavors to remove"));
        removeFlavors->setWhatsThis(EditActionEditor::tr(
                "Use this button to select the flavors that are removed from the data."));
        policy = removeFlavors->sizePolicy();
        policy.setVerticalStretch(0);
        removeFlavors->setSizePolicy(policy);
        layout->addWidget(removeFlavors);
        QObject::connect(removeFlavors, SIGNAL(changed()), editor, updateSlot);

        EditActionBooleanEditor *applyToMeta =
                new EditActionBooleanEditor(target, "ApplyToMetadata",
                                            EditActionEditor::tr("Apply to &metadata"),
                                            EditActionEditor::tr(
                                                    "Apply the alteration to the metdata as well."),
                                            EditActionEditor::tr("Apply alteration to metadata"),
                                            EditActionEditor::tr(
                                                    "If this option is set then any intersecting metadata is altered as well.  This is required if the resulting data does not already have metadata defined.  This is usually the case if the flavors being altered where not already covered in cut size switching."),
                                            false, widget);
        policy = applyToMeta->sizePolicy();
        policy.setVerticalStretch(0);
        applyToMeta->setSizePolicy(policy);
        layout->addWidget(applyToMeta);
        QObject::connect(applyToMeta, SIGNAL(changed()), editor, updateSlot);

        return widget;
    }
};

class EditActionUnit : public EditActionType {
public:
    EditActionUnit() = default;

    virtual ~EditActionUnit() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Set Data Identifier"); }

    virtual QString getInternalType() const
    { return "SetUnit"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Unit", "SetUnit"); }

    virtual QString getToolTip() const
    { return EditActionEditor::tr("Set the identifier of data."); }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Set data identifier"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type alter the identifier units of the data.  The values are not changed but the \"name\" is altered.");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditActionEditor *editor,
                          const char *updateSlot) const override
    {
        QWidget *widget = new QWidget;
        QVBoxLayout *layout = new QVBoxLayout;
        layout->setContentsMargins(0, 0, 0, 0);
        widget->setLayout(layout);

        EditActionSelectionEditor *selection =
                new EditActionSelectionEditor(target.hash("Selection"),
                                              EditActionSelectionEditor::Operate_Target, editor,
                                              widget);
        QSizePolicy policy(selection->sizePolicy());
        policy.setVerticalStretch(1);
        selection->setSizePolicy(policy);
        layout->addWidget(selection);
        QObject::connect(selection, SIGNAL(changed()), editor, updateSlot);

        EditActionUnitEditor *unit = new EditActionUnitEditor(target, editor, widget);
        policy = unit->sizePolicy();
        policy.setVerticalStretch(0);
        unit->setSizePolicy(policy);
        layout->addWidget(unit);
        QObject::connect(unit, SIGNAL(changed()), editor, updateSlot);

        EditActionBooleanEditor *applyToMeta =
                new EditActionBooleanEditor(target, "ApplyToMetadata",
                                            EditActionEditor::tr("Apply to &metadata"),
                                            EditActionEditor::tr(
                                                    "Apply the alteration to the metdata as well."),
                                            EditActionEditor::tr("Apply alteration to metadata"),
                                            EditActionEditor::tr(
                                                    "If this option is set then any intersecting metadata is altered as well.  This is required if the resulting data does not already have metadata defined."),
                                            true, widget);
        policy = applyToMeta->sizePolicy();
        policy.setVerticalStretch(0);
        applyToMeta->setSizePolicy(policy);
        layout->addWidget(applyToMeta);
        QObject::connect(applyToMeta, SIGNAL(changed()), editor, updateSlot);

        return widget;
    }
};

class EditActionDuplicate : public EditActionUnit {
public:
    EditActionDuplicate() = default;

    virtual ~EditActionDuplicate() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Duplicate Data"); }

    virtual QString getInternalType() const
    { return "Duplicate"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Duplicate"); }

    virtual QString getToolTip() const
    { return EditActionEditor::tr("Duplicate data values."); }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Duplicate values"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type duplicate all their inputs to another literal identifier.");
    }
};

class EditActionUnitReplace : public EditActionType {
public:
    EditActionUnitReplace() = default;

    virtual ~EditActionUnitReplace() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Translate Data Identifier"); }

    virtual QString getInternalType() const
    { return "Translate"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "UnitReplace", "Translate"); }

    virtual QString getToolTip() const
    { return EditActionEditor::tr("Translate the data identifier based on regular expressions."); }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Translate data identifier"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type alter the identifier units of the data based on regular expressions with pattern capture.  The values are not changed but the \"name\" is altered.");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditActionEditor *editor,
                          const char *updateSlot) const override
    {
        QWidget *widget = new QWidget;
        QVBoxLayout *layout = new QVBoxLayout;
        layout->setContentsMargins(0, 0, 0, 0);
        widget->setLayout(layout);

        EditActionSelectionEditor *selection =
                new EditActionSelectionEditor(target.hash("Selection"),
                                              EditActionSelectionEditor::Operate_Target, editor,
                                              widget);
        QSizePolicy policy(selection->sizePolicy());
        policy.setVerticalStretch(1);
        selection->setSizePolicy(policy);
        layout->addWidget(selection);
        QObject::connect(selection, SIGNAL(changed()), editor, updateSlot);

        EditActionUnitReplaceEditor *unit = new EditActionUnitReplaceEditor(target, widget);
        policy = unit->sizePolicy();
        policy.setVerticalStretch(0);
        unit->setSizePolicy(policy);
        layout->addWidget(unit);
        QObject::connect(unit, SIGNAL(changed()), editor, updateSlot);

        EditActionBooleanEditor *applyToMeta =
                new EditActionBooleanEditor(target, "ApplyToMetadata",
                                            EditActionEditor::tr("Apply to &metadata"),
                                            EditActionEditor::tr(
                                                    "Apply the alteration to the metdata as well."),
                                            EditActionEditor::tr("Apply alteration to metadata"),
                                            EditActionEditor::tr(
                                                    "If this option is set then any intersecting metadata is altered as well.  This is required if the resulting data does not already have metadata defined."),
                                            true, widget);
        policy = applyToMeta->sizePolicy();
        policy.setVerticalStretch(0);
        applyToMeta->setSizePolicy(policy);
        layout->addWidget(applyToMeta);
        QObject::connect(applyToMeta, SIGNAL(changed()), editor, updateSlot);

        return widget;
    }
};

class EditActionUnitReplaceDuplicate : public EditActionUnitReplace {
public:
    EditActionUnitReplaceDuplicate() = default;

    virtual ~EditActionUnitReplaceDuplicate() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Duplicate Translated Data"); }

    virtual QString getInternalType() const
    { return "DuplicateTranslate"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "DuplicateTranslate"); }

    virtual QString getToolTip() const
    {
        return EditActionEditor::tr(
                "Duplicate data values based on regular expression translation.");
    }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Duplicate translated values"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type duplicate all their inputs based on regular expressions with pattern capture.");
    }
};

class EditActionSetSerial : public EditActionType {
public:
    EditActionSetSerial() = default;

    virtual ~EditActionSetSerial() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Set Serial Number"); }

    virtual QString getInternalType() const
    { return "SetSerial"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Serial", "Setserial"); }

    virtual QString getToolTip() const
    { return EditActionEditor::tr("Set the serial number in the metadata."); }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Set serial number"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type alter the metadata for the selected variables to change the serial number recorded.  This is used to fix incorrect configurations of the original logged data.");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditActionEditor *editor,
                          const char *updateSlot) const override
    {
        QWidget *widget = new QWidget;
        QVBoxLayout *layout = new QVBoxLayout;
        layout->setContentsMargins(0, 0, 0, 0);
        widget->setLayout(layout);

        QList<EditActionInstrumentOverrideEditor::ParameterData> parameters;
        EditActionInstrumentOverrideEditor::ParameterData add;

        add.configPath = "Selection";
        parameters.append(add);

        EditActionSetSerialEditor *serial = new EditActionSetSerialEditor(target, widget);
        QSizePolicy policy(serial->sizePolicy());
        policy.setVerticalStretch(0);
        serial->setSizePolicy(policy);
        layout->addWidget(serial);
        QObject::connect(serial, SIGNAL(changed()), editor, updateSlot);

        EditActionInstrumentOverrideEditor *selection =
                new EditActionInstrumentOverrideEditor(target, editor, parameters, widget);
        policy = selection->sizePolicy();
        policy.setVerticalStretch(1);
        selection->setSizePolicy(policy);
        layout->addWidget(selection);
        QObject::connect(selection, SIGNAL(changed()), editor, updateSlot);

        return widget;
    }
};

class EditActionOverlayValue : public EditActionType {
public:
    EditActionOverlayValue() = default;

    virtual ~EditActionOverlayValue() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Overlay Value"); }

    virtual QString getInternalType() const
    { return "Overlay"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Overlay", "Set"); }

    virtual QString getToolTip() const
    { return EditActionEditor::tr("Set a value within the data."); }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Set value"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type directly overlay (set) a portion of a value tree onto the data.  This is used to directly set (parts of) data to specific values.");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditActionEditor *editor,
                          const char *updateSlot) const override
    {
        QTabWidget *widget = new QTabWidget;

        EditActionOverlayValueEditor *value = new EditActionOverlayValueEditor(target, widget);
        widget->addTab(value, EditActionEditor::tr("&Value"));
        widget->setTabToolTip(0, EditActionEditor::tr("The value to be overlaid/set."));
        widget->setTabWhatsThis(0, EditActionEditor::tr(
                "This is the definition of the value and path that is set within the data."));
        QObject::connect(value, SIGNAL(changed()), editor, updateSlot);

        EditActionSelectionEditor *selection =
                new EditActionSelectionEditor(target.hash("Selection"),
                                              EditActionSelectionEditor::Operate_Input, editor,
                                              widget);
        widget->addTab(selection, EditActionEditor::tr("&Data Selection"));
        widget->setTabToolTip(1,
                              EditActionEditor::tr("The variables in the data that are changed."));
        widget->setTabWhatsThis(1, EditActionEditor::tr(
                "This is the selection of variables within the data that the value will bet overlaid on to."));
        QObject::connect(selection, SIGNAL(changed()), editor, updateSlot);

        return widget;
    }
};

class EditActionOverlayMetadata : public EditActionOverlayValue {
public:
    EditActionOverlayMetadata() = default;

    virtual ~EditActionOverlayMetadata() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Overlay Metadata"); }

    virtual QString getInternalType() const
    { return "OverlayMetadata"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Meta", "Metadata", "OverlayMeta", "OverlayMetadata"); }

    virtual QString getToolTip() const
    { return EditActionEditor::tr("Set metadata."); }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Set metadata"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type directly overlay (set) the metadata for the selected variables.");
    }
};

class EditActionFunction : public EditActionType {
public:
    EditActionFunction() = default;

    virtual ~EditActionFunction() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Arithmetic Operation"); }

    virtual QString getInternalType() const
    { return "Arithmetic"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "Arithmetic", "Math", "Function"); }

    virtual QString getToolTip() const
    { return EditActionEditor::tr("Perform simple arithmetic on the data."); }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Perform arithmetic"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type alter the data by performing simple arithmetic operations using another data value.  For example, edits can add a value to another.");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditActionEditor *editor,
                          const char *updateSlot) const override
    {
        QWidget *widget = new QWidget;
        QVBoxLayout *layout = new QVBoxLayout;
        layout->setContentsMargins(0, 0, 0, 0);
        widget->setLayout(layout);

        QGroupBox *box = new QGroupBox(EditActionSelectionEditor::tr("Data"), widget);
        layout->addWidget(box);
        QVBoxLayout *subLayout = new QVBoxLayout;
        box->setLayout(subLayout);
        QSizePolicy policy(box->sizePolicy());
        policy.setVerticalStretch(1);
        box->setSizePolicy(policy);

        EditActionSelectionEditor *selection =
                new EditActionSelectionEditor(target.hash("Selection"),
                                              EditActionSelectionEditor::Operate_Input, editor,
                                              box);
        policy = selection->sizePolicy();
        policy.setVerticalStretch(1);
        selection->setSizePolicy(policy);
        subLayout->addWidget(selection);
        QObject::connect(selection, SIGNAL(changed()), editor, updateSlot);

        box = new QGroupBox(EditActionSelectionEditor::tr("Operation"), widget);
        layout->addWidget(box);
        subLayout = new QVBoxLayout;
        box->setLayout(subLayout);
        policy = box->sizePolicy();
        policy.setVerticalStretch(1);
        box->setSizePolicy(policy);

        EditActionFunctionEditor *function = new EditActionFunctionEditor(target, box);
        policy = function->sizePolicy();
        policy.setVerticalStretch(0);
        function->setSizePolicy(policy);
        subLayout->addWidget(function);
        QObject::connect(function, SIGNAL(changed()), editor, updateSlot);

        selection = new EditActionSelectionEditor(target.hash("Inputs"),
                                                  EditActionSelectionEditor::Operate_Input, editor,
                                                  box);
        policy = selection->sizePolicy();
        policy.setVerticalStretch(1);
        selection->setSizePolicy(policy);
        subLayout->addWidget(selection);
        QObject::connect(selection, SIGNAL(changed()), editor, updateSlot);


        EditActionCalibrationEditor *poly = new EditActionCalibrationEditor(target, false, box);
        policy = poly->sizePolicy();
        policy.setVerticalStretch(0);
        poly->setSizePolicy(policy);
        subLayout->addWidget(poly);
        QObject::connect(poly, SIGNAL(changed()), editor, updateSlot);

        return widget;
    }
};

class EditActionScriptValueProcessor : public EditActionType {
public:
    EditActionScriptValueProcessor() = default;

    virtual ~EditActionScriptValueProcessor() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Script Value Processor"); }

    virtual QString getInternalType() const
    { return "ScriptValue"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "ScriptValue", "ScriptValues"); }

    virtual QString getToolTip() const
    { return EditActionEditor::tr("Process script code on a direct value stream."); }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Script value processor"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type evaluate a CPD3 script handling interface for the direct value stream selected as inputs.");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditActionEditor *editor,
                          const char *updateSlot) const override
    {
        QTabWidget *widget = new QTabWidget;

        EditActionScriptEditor *script = new EditActionScriptEditor(target, widget);
        widget->addTab(script, EditActionEditor::tr("&Script"));
        widget->setTabToolTip(0, EditActionEditor::tr("The script code evaluated."));
        widget->setTabWhatsThis(0, EditActionEditor::tr(
                "This is the CPD3 script code evaluated by the edit."));
        QObject::connect(script, SIGNAL(changed()), editor, updateSlot);

        EditActionSelectionEditor *selection =
                new EditActionSelectionEditor(target.hash("Selection"),
                                              EditActionSelectionEditor::Operate_Input, editor,
                                              widget);
        widget->addTab(selection, EditActionEditor::tr("&Input"));
        widget->setTabToolTip(1, EditActionEditor::tr("The inputs fed to the script code."));
        widget->setTabWhatsThis(1, EditActionEditor::tr(
                "This is the selection of variables within the data that are fed as inputs to the script code.  The script code can produce additional outputs but it begin with these as inputs."));
        QObject::connect(selection, SIGNAL(changed()), editor, updateSlot);

        return widget;
    }
};

class EditActionScriptValueGeneral : public EditActionScriptValueProcessor {
public:
    EditActionScriptValueGeneral() = default;

    virtual ~EditActionScriptValueGeneral() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Script Value General Handler"); }

    virtual QString getInternalType() const
    { return "ScriptGeneralValue"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "ScriptGeneralValue", "ScriptGeneralValues"); }

    virtual QString getToolTip() const
    {
        return EditActionEditor::tr(
                "Invoke script code with a general value handler available as the global 'data'.");
    }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Script general value processor"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type evaluate a CPD3 script with the the selected inputs available on the global 'data' as a general single value stream.");
    }
};

class EditActionScriptDemultiplexer : public EditActionType {
public:
    EditActionScriptDemultiplexer() = default;

    virtual ~EditActionScriptDemultiplexer() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Script Demultiplexer"); }

    virtual QString getInternalType() const
    { return "ScriptDemultiplexer"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "ScriptDemultiplexer", "Demultiplexer"); }

    virtual QString getToolTip() const
    { return EditActionEditor::tr("Process values with a script demultiplexer."); }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Script demultiplexer"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type evaluate a CPD3 script with a demultiplexer available as the global 'data'.  All data is fed into the demultiplexer so the inputs/outputs used by it must be set by the script code.");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditActionEditor *editor,
                          const char *updateSlot) const override
    {
        EditActionScriptEditor *script = new EditActionScriptEditor(target);
        QObject::connect(script, SIGNAL(changed()), editor, updateSlot);
        return script;
    }
};

class EditActionScriptSegmentProcessor : public EditActionType {
public:
    EditActionScriptSegmentProcessor() = default;

    virtual ~EditActionScriptSegmentProcessor() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Script Segment Processor"); }

    virtual QString getInternalType() const
    { return "ScriptSegment"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "ScriptSegment", "ScriptSegments", "Script"); }

    virtual QString getToolTip() const
    { return EditActionEditor::tr("Process script code on with a segment handler."); }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Script segment handler"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type evaluate a CPD3 script handling interface for multi-value segments.");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditActionEditor *editor,
                          const char *updateSlot) const override
    {
        QTabWidget *widget = new QTabWidget;

        EditActionScriptEditor *script = new EditActionScriptEditor(target, widget);
        widget->addTab(script, EditActionEditor::tr("&Script"));
        widget->setTabToolTip(0, EditActionEditor::tr("The script code evaluated."));
        widget->setTabWhatsThis(0, EditActionEditor::tr(
                "This is the CPD3 script code evaluated by the edit."));
        QObject::connect(script, SIGNAL(changed()), editor, updateSlot);

        EditActionSelectionEditor *selection = new EditActionSelectionEditor(target.hash("Inputs"),
                                                                             EditActionSelectionEditor::Operate_Input,
                                                                             editor, widget);
        widget->addTab(selection, EditActionEditor::tr("&Inputs"));
        widget->setTabToolTip(1, EditActionEditor::tr("The inputs to the script handler."));
        widget->setTabWhatsThis(1, EditActionEditor::tr(
                "This is the listing of inputs fed into the script handler."));
        QObject::connect(selection, SIGNAL(changed()), editor, updateSlot);

        selection = new EditActionSelectionEditor(target.hash("Outputs"),
                                                  EditActionSelectionEditor::Operate_Output, editor,
                                                  widget);
        widget->addTab(selection, EditActionEditor::tr("&Outputs"));
        widget->setTabToolTip(1, EditActionEditor::tr("The outputs of the script handler."));
        widget->setTabWhatsThis(1, EditActionEditor::tr(
                "This is the listing of outputs extracted from the script handler.  These are normally only extracted from the \"raw\" archive."));
        QObject::connect(selection, SIGNAL(changed()), editor, updateSlot);

        QWidget *advanced = new QWidget(widget);
        widget->addTab(advanced, EditActionEditor::tr("&Advanced"));
        QVBoxLayout *advancedLayout = new QVBoxLayout;
        advancedLayout->setContentsMargins(0, 0, 0, 0);
        advanced->setLayout(advancedLayout);

        EditActionBooleanEditor *checkResult = new EditActionBooleanEditor(target, "CheckResult",
                                                                           EditActionEditor::tr(
                                                                                   "&Require true result"),
                                                                           EditActionEditor::tr(
                                                                                   "Check the result of the script handler evaluation and discard data if it is false."),
                                                                           EditActionEditor::tr(
                                                                                   "Check handler result"),
                                                                           EditActionEditor::tr(
                                                                                   "If this is set then any evaluations of the handler that return a false value are discarded."),
                                                                           false, advanced);
        advancedLayout->addWidget(checkResult);
        QObject::connect(checkResult, SIGNAL(changed()), editor, updateSlot);

        EditActionBooleanEditor *implicitMeta =
                new EditActionBooleanEditor(target, "ImplicitMetadata",
                                            EditActionEditor::tr("&Include metadata implicitly"),
                                            EditActionEditor::tr(
                                                    "Include metadata as an input for any values marked as inputs."),
                                            EditActionEditor::tr("Implicit metadata"),
                                            EditActionEditor::tr(
                                                    "If this is set then metadata for any values that match as inputs is automatically included in the processing segments."),
                                            true, advanced);
        advancedLayout->addWidget(implicitMeta);
        QObject::connect(implicitMeta, SIGNAL(changed()), editor, updateSlot);

        advancedLayout->addStretch(1);

        return widget;
    }
};

class EditActionScriptSegmentGeneral : public EditActionType {
public:
    EditActionScriptSegmentGeneral() = default;

    virtual ~EditActionScriptSegmentGeneral() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Script Segment General Handler"); }

    virtual QString getInternalType() const
    { return "ScriptGeneralSegment"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "ScriptGeneralSegment", "ScriptGeneralSegments"); }

    virtual QString getToolTip() const
    {
        return EditActionEditor::tr(
                "Invoke script code with a multi-value segment handler available as the global 'data'.");
    }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Script general segment processor"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type evaluate a CPD3 script with the the selected inputs available on the global 'data' as a general multi-value segment stream.");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditActionEditor *editor,
                          const char *updateSlot) const override
    {
        QTabWidget *widget = new QTabWidget;

        EditActionScriptEditor *script = new EditActionScriptEditor(target, widget);
        widget->addTab(script, EditActionEditor::tr("&Script"));
        widget->setTabToolTip(0, EditActionEditor::tr("The script code evaluated."));
        widget->setTabWhatsThis(0, EditActionEditor::tr(
                "This is the CPD3 script code evaluated by the edit."));
        QObject::connect(script, SIGNAL(changed()), editor, updateSlot);

        EditActionSelectionEditor *selection = new EditActionSelectionEditor(target.hash("Inputs"),
                                                                             EditActionSelectionEditor::Operate_Input,
                                                                             editor, widget);
        widget->addTab(selection, EditActionEditor::tr("&Inputs"));
        widget->setTabToolTip(1, EditActionEditor::tr("The inputs to the script handler."));
        widget->setTabWhatsThis(1, EditActionEditor::tr(
                "This is the listing of inputs fed into the script handler."));
        QObject::connect(selection, SIGNAL(changed()), editor, updateSlot);

        selection = new EditActionSelectionEditor(target.hash("Outputs"),
                                                  EditActionSelectionEditor::Operate_Output, editor,
                                                  widget);
        widget->addTab(selection, EditActionEditor::tr("&Outputs"));
        widget->setTabToolTip(1, EditActionEditor::tr("The outputs of the script handler."));
        widget->setTabWhatsThis(1, EditActionEditor::tr(
                "This is the listing of outputs extracted from the script handler.  These are normally only extracted from the \"raw\" archive."));
        QObject::connect(selection, SIGNAL(changed()), editor, updateSlot);

        QWidget *advanced = new QWidget(widget);
        widget->addTab(advanced, EditActionEditor::tr("&Advanced"));
        QVBoxLayout *advancedLayout = new QVBoxLayout;
        advancedLayout->setContentsMargins(0, 0, 0, 0);
        advanced->setLayout(advancedLayout);

        EditActionBooleanEditor *implicitMeta =
                new EditActionBooleanEditor(target, "ImplicitMetadata",
                                            EditActionEditor::tr("&Include metadata implicitly"),
                                            EditActionEditor::tr(
                                                    "Include metadata as an input for any values marked as inputs."),
                                            EditActionEditor::tr("Implicit metadata"),
                                            EditActionEditor::tr(
                                                    "If this is set then metadata for any values that match as inputs is automatically included in the processing segments."),
                                            true, advanced);
        advancedLayout->addWidget(implicitMeta);
        QObject::connect(implicitMeta, SIGNAL(changed()), editor, updateSlot);

        advancedLayout->addStretch(1);

        return widget;
    }
};

class EditActionAbnormalDataEpisode : public EditActionType {
public:
    EditActionAbnormalDataEpisode() = default;

    virtual ~EditActionAbnormalDataEpisode() = default;

    virtual QString getDisplayName() const
    { return EditActionEditor::tr("Abnormal Data Episode"); }

    virtual QString getInternalType() const
    { return "AbnormalDataEpisode"; }

    bool matchesType(const std::string &type) const override
    { return Util::equal_insensitive(type, "AbnormalDataEpisode"); }

    virtual QString getToolTip() const
    { return EditActionEditor::tr("Mark data as part of an abnormal data episode."); }

    virtual QString getStatusTip() const
    { return EditActionEditor::tr("Abnormal Data"); }

    virtual QString getWhatsThis() const
    {
        return EditActionEditor::tr(
                "Edits of this type indicate that data are part of an abnormal data episode that does not represent regular station data.  This edit applies to all instruments at the station.  Interpretation depends on the specific station and event, but generally results in additional information sent to data archives.");
    }

    QWidget *createEditor(Variant::Write &target,
                          EditActionEditor *editor,
                          const char *updateSlot) const override
    {
        QWidget *widget = new QWidget;
        QVBoxLayout *layout = new QVBoxLayout;
        layout->setContentsMargins(0, 0, 0, 0);
        widget->setLayout(layout);

        EditActionAbnormalDataEpisodeEditor
                *ep = new EditActionAbnormalDataEpisodeEditor(target, widget);
        QSizePolicy policy(ep->sizePolicy());
        policy.setVerticalStretch(0);
        ep->setSizePolicy(policy);
        layout->addWidget(ep);
        QObject::connect(ep, SIGNAL(changed()), editor, updateSlot);

        layout->insertStretch(-1, 1);

        return widget;
    }
};

}

EditActionEditor::EditActionEditor(QWidget *parent) : QWidget(parent)
{
    QVBoxLayout *topLayout = new QVBoxLayout;
    setLayout(topLayout);
    topLayout->setContentsMargins(0, 0, 0, 0);

    actionSelect = new QComboBox(this);
    QSizePolicy policy(actionSelect->sizePolicy());
    policy.setVerticalStretch(0);
    actionSelect->setSizePolicy(policy);
    topLayout->addWidget(actionSelect);
    actionSelect->setToolTip(tr("The type of action applied to the data."));
    actionSelect->setStatusTip(tr("Action type"));
    actionSelect->setWhatsThis(tr("This is the type of action that is applied to the data."));

    actionTypes.append(new EditActionInvalidate);
    actionTypes.append(new EditActionContaminate);
    actionTypes.append(new EditActionUncontaminate);
    actionTypes.append(new EditActionFlowCorrection);
    actionTypes.append(new EditActionSpotCorrection);
    actionTypes.append(new EditActionMultiSpotCorrection);
    actionTypes.append(new EditActionPoly);
    actionTypes.append(new EditActionSetCut);
    actionTypes.append(new EditActionSetSerial);
    actionTypes.append(new EditActionAbnormalDataEpisode);
    actionTypes.append(new EditActionAddFlags);
    actionTypes.append(new EditActionRemoveFlags);
    actionTypes.append(new EditActionRemoveMatchedFlags);
    actionTypes.append(new EditActionInvertPoly);
    actionTypes.append(new EditActionRecalibrate);
    actionTypes.append(new EditActionMultiPoly);
    actionTypes.append(new EditActionWrapModular);
    actionTypes.append(new EditActionRemove);
    actionTypes.append(new EditActionFlavors);
    actionTypes.append(new EditActionUnit);
    actionTypes.append(new EditActionUnitReplace);
    actionTypes.append(new EditActionDuplicate);
    actionTypes.append(new EditActionUnitReplaceDuplicate);
    actionTypes.append(new EditActionOverlayValue);
    actionTypes.append(new EditActionOverlayMetadata);
    actionTypes.append(new EditActionFunction);
    actionTypes.append(new EditActionScriptDemultiplexer);
    actionTypes.append(new EditActionScriptSegmentProcessor);
    actionTypes.append(new EditActionScriptSegmentGeneral);
    actionTypes.append(new EditActionScriptValueProcessor);
    actionTypes.append(new EditActionScriptValueGeneral);
    actionTypes.append(new EditActionNOOP);

    for (QList<EditActionType *>::const_iterator action = actionTypes.constBegin(),
            end = actionTypes.constEnd(); action != end; ++action) {
        actionSelect->addItem((*action)->getDisplayName());

        QString tip((*action)->getToolTip());
        if (!tip.isEmpty()) {
            actionSelect->setItemData(actionSelect->count() - 1, tip, Qt::ToolTipRole);
        }
        tip = (*action)->getStatusTip();
        if (!tip.isEmpty()) {
            actionSelect->setItemData(actionSelect->count() - 1, tip, Qt::StatusTipRole);
        }
        tip = (*action)->getWhatsThis();
        if (!tip.isEmpty()) {
            actionSelect->setItemData(actionSelect->count() - 1, tip, Qt::WhatsThisRole);
        }
    }
    connect(actionSelect, SIGNAL(currentIndexChanged(int)), this, SLOT(typeSelected(int)));


    editorPane = new QWidget(this);
    topLayout->addWidget(editorPane);
    editorLayout = new QVBoxLayout;
    editorLayout->setContentsMargins(0, 0, 0, 0);
    editorPane->setLayout(editorLayout);
    policy = editorPane->sizePolicy();
    policy.setVerticalStretch(1);
    editorPane->setSizePolicy(policy);

    editorContents = NULL;

    configure(Variant::Write::empty());
}

EditActionEditor::~EditActionEditor()
{
    qDeleteAll(actionTypes);

    if (editorContents != NULL)
        editorContents->deleteLater();
}

/**
 * Get the configured action value.
 * 
 * @return  the trigger value
 */
const Variant::Write &EditActionEditor::getValue() const
{ return config; }

/**
 * Configure the editor with the given value.  This value should be the
 * action contents.
 * 
 * @param value     the value to configure with
 */
void EditActionEditor::configure(CPD3::Data::Variant::Write value)
{
    config = std::move(value);

    const auto &code = config.hash("Type").toString();
    int index = -1;
    for (int i = 0; i < actionTypes.size(); i++) {
        if (!actionTypes.at(i)->matchesType(code))
            continue;
        index = i;
    }
    if (index < 0)
        index = 0;

    if (editorContents != NULL) {
        editorLayout->removeWidget(editorContents);
        editorContents->hide();
        editorContents->deleteLater();
        editorContents = NULL;
    }
    if (index < 0 || index >= actionTypes.size())
        return;

    actionSelect->disconnect(this);
    actionSelect->setCurrentIndex(index);
    connect(actionSelect, SIGNAL(currentIndexChanged(int)), this, SLOT(typeSelected(int)));

    EditActionType *type = actionTypes.at(index);
    editorContents = type->createEditor(config, this, SLOT(editorChanged()));
    editorContents->setParent(editorPane);
    editorLayout->addWidget(editorContents);
}

void EditActionEditor::availableUpdated()
{
    if (editorContents != NULL) {
        editorLayout->removeWidget(editorContents);
        editorContents->hide();
        editorContents->deleteLater();
        editorContents = NULL;
    }
    int index = actionSelect->currentIndex();
    if (index < 0 || index >= actionTypes.size())
        return;

    EditActionType *type = actionTypes.at(index);
    editorContents = type->createEditor(config, this, SLOT(editorChanged()));
    editorContents->setParent(editorPane);
    editorLayout->addWidget(editorContents);
}

void EditActionEditor::setAvailableStations(const SequenceName::ComponentSet &set)
{
    availableStations = set;
    availableUpdated();
}

void EditActionEditor::setAvailableArchives(const SequenceName::ComponentSet &set)
{
    availableArchives = set;
    availableUpdated();
}

void EditActionEditor::setAvailableVariables(const SequenceName::ComponentSet &set)
{
    availableVariables = set;
    availableUpdated();
}

void EditActionEditor::setAvailableFlavors(const SequenceName::ComponentSet &set)
{
    availableFlavors = set;
    availableUpdated();
}

const SequenceName::ComponentSet &EditActionEditor::getAvailableStations() const
{ return availableStations; }

const SequenceName::ComponentSet &EditActionEditor::getAvailableArchives() const
{ return availableArchives; }

const SequenceName::ComponentSet &EditActionEditor::getAvailableVariables() const
{ return availableVariables; }

const SequenceName::ComponentSet &EditActionEditor::getAvailableFlavors() const
{ return availableFlavors; }


void EditActionEditor::typeSelected(int index)
{
    if (editorContents != NULL) {
        editorLayout->removeWidget(editorContents);
        editorContents->hide();
        editorContents->deleteLater();
        editorContents = NULL;
    }
    if (index < 0 || index >= actionTypes.size())
        return;

    EditActionType *type = actionTypes.at(index);
    editorContents = type->createEditor(config, this, SLOT(editorChanged()));
    editorContents->setParent(editorPane);
    editorLayout->addWidget(editorContents);

    config.hash("Type").setString(type->getInternalType());
    emit changed();
}

void EditActionEditor::editorChanged()
{
    emit changed();
}


bool EditActionEndpoint::matches(const Variant::Read &, const Variant::Read &metadata)
{
    return Util::equal_insensitive(metadata.metadata("Editor").hash("Type").toString(),
                                   "EditAction");
}

QString EditActionEndpoint::collapsedText(const Variant::Read &value, const Variant::Read &)
{
    const auto &type = value.hash("Type").toString();
    if (Util::equal_insensitive(type, "remove")) {
        return EditActionEditor::tr("Remove");
    } else if (Util::equal_insensitive(type, "poly", "polynomial", "cal", "calibration",
                                       "multipoly", "multipolynomial", "multical",
                                       "multicalibration")) {
        return EditActionEditor::tr("Calibration");
    } else if (Util::equal_insensitive(type, "polyinvert", "polynomialinvert", "invertcal"
                                                                               "invertcalibration")) {
        return EditActionEditor::tr("Inverse calibration");
    } else if (Util::equal_insensitive(type, "recalibrate")) {
        return EditActionEditor::tr("Re-calibration");
    } else if (Util::equal_insensitive(type, "wrap", "modular", "modulus")) {
        return EditActionEditor::tr("Wrap");
    } else if (Util::equal_insensitive(type, "overlay", "set")) {
        return EditActionEditor::tr("Value change");
    } else if (Util::equal_insensitive(type, "meta", "metadata", "overlaymeta",
                                       "overlaymetadata")) {
        return EditActionEditor::tr("Metadata");
    } else if (Util::equal_insensitive(type, "serial", "setserial")) {
        return EditActionEditor::tr("Serial number");
    } else if (Util::equal_insensitive(type, "flag", "addflag", "addflags", "removeflag",
                                       "removeflags")) {
        return EditActionEditor::tr("Flags");
    } else if (Util::equal_insensitive(type, "contaminate", "contam")) {
        return EditActionEditor::tr("Contaminate");
    } else if (Util::equal_insensitive(type, "uncontaminate", "clearcontam")) {
        return EditActionEditor::tr("Un-contaminate");
    } else if (Util::equal_insensitive(type, "flowcorrection", "flowcalibration")) {
        return EditActionEditor::tr("Flow");
    } else if (Util::equal_insensitive(type, "spot", "spotsize", "multispot", "multispotsize",
                                       "clapspot")) {
        return EditActionEditor::tr("Spot");
    } else if (Util::equal_insensitive(type, "unit", "setunit", "unitreplace", "translate")) {
        return EditActionEditor::tr("Identifier change");
    } else if (Util::equal_insensitive(type, "duplicate", "duplicatetranslate")) {
        return EditActionEditor::tr("Duplication");
    } else if (Util::equal_insensitive(type, "flavors")) {
        return EditActionEditor::tr("Flavor change");
    } else if (Util::equal_insensitive(type, "setcut", "cut")) {
        return EditActionEditor::tr("Cut size");
    } else if (Util::equal_insensitive(type, "arithmetic", "math", "function")) {
        return EditActionEditor::tr("Arithmetic");
    } else if (Util::equal_insensitive(type, "scriptvalue", "scriptvalues", "scriptsegment",
                                       "scriptsegments", "script", "scriptdemultiplexer",
                                       "demultiplexer", "scriptgeneralvalue", "scriptgeneralvalues",
                                       "scriptgeneralsegment", "scriptgeneralsegments")) {
        return EditActionEditor::tr("Script");
    } else if (Util::equal_insensitive(type, "NOOP", "None")) {
        return EditActionEditor::tr("None");
    } else if (Util::equal_insensitive(type, "AbnormalDataEpisode")) {
        return EditActionEditor::tr("Abnormal");
    }
    return EditActionEditor::tr("Invalidate");
}

bool EditActionEndpoint::edit(Variant::Write &value, const Variant::Read &metadata, QWidget *parent)
{
    auto editor = metadata.metadata("Editor");

    QDialog dialog(parent);
    dialog.setWindowTitle(QObject::tr("Action"));
    auto layout = new QVBoxLayout(&dialog);
    dialog.setLayout(layout);

    QLabel *label = new QLabel(metadata.metadata("Description").toDisplayString());
    label->setWordWrap(true);
    layout->addWidget(label);

    auto action = new EditActionEditor(&dialog);
    label->setBuddy(action);
    layout->addWidget(action, 1);

    Variant::Root output(value);
    action->configure(output.write());

    auto buttons =
            new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal,
                                 &dialog);
    layout->addWidget(buttons);
    QObject::connect(buttons, &QDialogButtonBox::accepted, &dialog, &QDialog::accept);
    QObject::connect(buttons, &QDialogButtonBox::rejected, &dialog, &QDialog::reject);
    if (dialog.exec() != QDialog::Accepted)
        return false;

    value.setEmpty();
    value.set(output);
    return true;
}


namespace Internal {

EditActionType::EditActionType() = default;

EditActionType::~EditActionType() = default;

QString EditActionType::getToolTip() const
{ return QString(); }

QString EditActionType::getStatusTip() const
{ return QString(); }

QString EditActionType::getWhatsThis() const
{ return QString(); }

EditActionSelectionEditor::EditActionSelectionEditor(Variant::Write v,
                                                     OperatorMode mode,
                                                     EditActionEditor *editor, QWidget *parent)
        : QWidget(parent), value(std::move(v))
{
    QVBoxLayout *layout = new QVBoxLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    selection = new VariableSelect(this);
    layout->addWidget(selection);
    selection->setAvailableStations(editor->getAvailableStations());
    selection->setAvailableArchives(editor->getAvailableArchives());
    selection->setAvailableVariables(editor->getAvailableVariables());
    selection->setAvailableFlavors(editor->getAvailableFlavors());

    switch (mode) {
    case Operate_Target:
        selectionDefaults.setFlavorsSelection(QStringList(), QStringList());
        break;
    case Operate_Input:
        break;
    case Operate_Output:
        selectionDefaults.setArchiveFanout(QStringList() << "raw");
        break;
    }

    selection->configureFromSequenceMatch(value, selectionDefaults);

    connect(selection, SIGNAL(selectionChanged()), this, SLOT(selectionChanged()));
}

EditActionSelectionEditor::~EditActionSelectionEditor() = default;

void EditActionSelectionEditor::selectionChanged()
{
    selection->writeSequenceMatch(value, selectionDefaults);
    emit changed();
}


EditActionInstrumentOverrideEditor::EditActionInstrumentOverrideEditor(Variant::Write v,
                                                                       EditActionEditor *editor,
                                                                       const QList<
                                                                               ParameterData> &parameters,
                                                                       QWidget *parent) : QWidget(
        parent), value(std::move(v))
{
    QSet<QString> instrumentSuffixes;
    for (const auto var : editor->getAvailableVariables()) {
        auto idx = var.find('_');
        if (idx == var.npos)
            continue;
        QString sx(QString::fromStdString(var.substr(idx + 1)));
        if (sx.isEmpty())
            continue;
        instrumentSuffixes.insert(sx);
    }
    QStringList sortedSuffixes(instrumentSuffixes.values());
    std::sort(sortedSuffixes.begin(), sortedSuffixes.end());

    QGridLayout *layout = new QGridLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    QLabel *label = new QLabel(tr("&Instrument:"), this);
    layout->addWidget(label, 0, 0);
    selection = new QComboBox(this);
    label->setBuddy(selection);
    layout->addWidget(selection, 0, 1);
    selection->setEditable(true);
    selection->setInsertPolicy(QComboBox::NoInsert);
    selection->setToolTip(tr("Instrument code selection."));
    selection->setStatusTip(tr("Instrument code"));
    selection->setWhatsThis(
            tr("This sets the instrument code to derive parameters from.  For example \"A11\" will use the default relevant parameters ending in that instrument code."));

    QString checkMatch(value.hash("Instrument").toQString());
    int matchedIndex = -1;
    for (int i = 0; i < sortedSuffixes.size(); i++) {
        QString add(sortedSuffixes.at(i));
        if (add == checkMatch)
            matchedIndex = i;

        selection->addItem(add);
    }
    selection->insertSeparator(sortedSuffixes.size());
    selection->addItem(tr("Manual Selection"));


    instrumentPane = new QWidget(this);
    layout->addWidget(instrumentPane, 1, 0, 1, -1);

    bool anyParameterDefined = false;
    if (parameters.size() == 1 && parameters.at(0).label.isEmpty()) {
        VariableSelect *vs = new VariableSelect(this);
        manualPane = vs;

        ParameterData param(parameters.at(0));
        vs->setAvailableStations(editor->getAvailableStations());
        vs->setAvailableArchives(editor->getAvailableArchives());
        vs->setAvailableVariables(editor->getAvailableVariables());
        vs->setAvailableFlavors(editor->getAvailableFlavors());

        vs->configureFromSequenceMatch(value.getPath(param.configPath), selectionDefaults);
        if (value.getPath(param.configPath).exists())
            anyParameterDefined = true;

        if (!param.toolTip.isEmpty())
            vs->setToolTip(param.toolTip);
        if (param.whatsThis.isEmpty())
            vs->setWhatsThis(param.whatsThis);

        connect(vs, SIGNAL(selectionChanged()), this, SLOT(selectionChanged()));
    } else {
        QTabWidget *tabs = new QTabWidget(this);
        manualPane = tabs;
        for (QList<ParameterData>::const_iterator param = parameters.constBegin(),
                end = parameters.constEnd(); param != end; ++param) {
            VariableSelect *vs = new VariableSelect(manualPane);
            vs->setAvailableStations(editor->getAvailableStations());
            vs->setAvailableArchives(editor->getAvailableArchives());
            vs->setAvailableVariables(editor->getAvailableVariables());
            vs->setAvailableFlavors(editor->getAvailableFlavors());

            vs->configureFromSequenceMatch(value.getPath(param->configPath), selectionDefaults);
            if (value.getPath(param->configPath).exists())
                anyParameterDefined = true;

            connect(vs, SIGNAL(selectionChanged()), this, SLOT(selectionChanged()));

            manualSelections.insert(param->configPath, vs);

            int index = tabs->addTab(vs, param->label);
            tabs->setTabToolTip(index, param->toolTip);
            tabs->setTabWhatsThis(index, param->whatsThis);
        }
    }
    layout->addWidget(manualPane, 1, 0, 1, -1);

    if (!checkMatch.isEmpty()) {
        manualPane->setVisible(false);
        instrumentPane->setVisible(true);
        if (matchedIndex != -1) {
            selection->setCurrentIndex(matchedIndex);
        } else {
            selection->setCurrentIndex(-1);
            selection->lineEdit()->setText(checkMatch);
        }
    } else if (!anyParameterDefined) {
        manualPane->setVisible(false);
        instrumentPane->setVisible(true);
        selection->setCurrentIndex(0);
        if (!sortedSuffixes.isEmpty())
            value.hash("Instrument").setString(sortedSuffixes.at(0));
    } else {
        manualPane->setVisible(true);
        instrumentPane->setVisible(false);
        selection->setCurrentIndex(-1);
    }
    connect(selection, SIGNAL(currentIndexChanged(
                                      const QString &)), this, SLOT(selectionChanged()));
    connect(selection->lineEdit(), SIGNAL(textEdited(
                                                  const QString &)), this,
            SLOT(selectionChanged()));
}

EditActionInstrumentOverrideEditor::~EditActionInstrumentOverrideEditor() = default;

void EditActionInstrumentOverrideEditor::selectionChanged()
{
    if (selection->currentIndex() == selection->count() - 1) {
        manualPane->setVisible(true);
        instrumentPane->setVisible(false);
        value.hash("Instrument").remove();

        for (QHash<QString, VariableSelect *>::const_iterator sel = manualSelections.constBegin(),
                endSel = manualSelections.constEnd(); sel != endSel; ++sel) {
            sel.value()->writeSequenceMatch(value.getPath(sel.key()), selectionDefaults);
        }
        emit changed();
        return;
    }

    manualPane->setVisible(false);
    instrumentPane->setVisible(true);

    QString inst(selection->currentText());
    if (inst.isEmpty())
        return;

    for (QHash<QString, VariableSelect *>::const_iterator sel = manualSelections.constBegin(),
            endSel = manualSelections.constEnd(); sel != endSel; ++sel) {
        value.getPath(sel.key()).remove();
    }
    value.hash("Instrument").setString(inst);

    emit changed();
}


EditActionBooleanEditor::EditActionBooleanEditor(Variant::Write v,
                                                 const QString &p,
                                                 const QString &label,
                                                 const QString &toolTip,
                                                 const QString &statusTip,
                                                 const QString &whatsThis,
                                                 bool defaultState,
                                                 QWidget *parent) : QWidget(parent),
                                                                    value(std::move(v)),
                                                                    path(p)
{
    QVBoxLayout *layout = new QVBoxLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    QCheckBox *box = new QCheckBox(label, this);
    layout->addWidget(box);
    if (value.getPath(path).exists())
        defaultState = value.getPath(path).toBool();
    box->setChecked(defaultState);
    box->setToolTip(toolTip);
    box->setStatusTip(statusTip);
    box->setWhatsThis(whatsThis);

    connect(box, SIGNAL(toggled(bool)), this, SLOT(stateChanged(bool)));
}

EditActionBooleanEditor::~EditActionBooleanEditor() = default;

void EditActionBooleanEditor::stateChanged(bool state)
{
    value.getPath(path).setBool(state);
    emit changed();
}

EditActionEnumEditor::EditActionEnumEditor(Variant::Write v,
                                           const QString &p,
                                           const QString &label,
                                           const QString &toolTip,
                                           const QString &statusTip,
                                           const QString &whatsThis, QWidget *parent) : QWidget(
        parent), value(std::move(v)), path(p)
{
    QFormLayout *layout = new QFormLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    selection = new QComboBox(this);
    layout->addRow(label, selection);
    selection->setToolTip(toolTip);
    selection->setStatusTip(statusTip);
    selection->setWhatsThis(whatsThis);

    connect(selection, SIGNAL(currentIndexChanged(int)), this, SLOT(indexSelected(int)));
}

EditActionEnumEditor::~EditActionEnumEditor() = default;

void EditActionEnumEditor::indexSelected(int index)
{
    value.getPath(path).setString(selection->itemData(index).toString());
    emit changed();
}

void EditActionEnumEditor::addItem(const QString &authoritativeName,
                                   const QStringList &aliases,
                                   const QString &label,
                                   const QString &toolTip,
                                   const QString &statusTip,
                                   const QString &whatsThis)
{
    bool select = (selection->count() == 0);
    if (!select) {
        QString check(value.getPath(path).toQString().toLower());

        if (check == authoritativeName.toLower()) {
            select = true;
        } else {
            for (QStringList::const_iterator i = aliases.constBegin(), endI = aliases.constEnd();
                    i != endI;
                    ++i) {
                if (*i == check) {
                    select = true;
                    break;
                }
            }
        }
    }

    selection->disconnect(this);

    selection->addItem(label, authoritativeName);
    int index = selection->count() - 1;
    selection->setItemData(index, toolTip, Qt::ToolTipRole);
    selection->setItemData(index, statusTip, Qt::StatusTipRole);
    selection->setItemData(index, whatsThis, Qt::WhatsThisRole);
    if (select)
        selection->setCurrentIndex(index);

    connect(selection, SIGNAL(currentIndexChanged(int)), this, SLOT(indexSelected(int)));
}

EditActionCalibrationEditor::EditActionCalibrationEditor(Variant::Write v,
                                                         bool enableLimits,
                                                         QWidget *parent) : QWidget(parent),
                                                                            value(std::move(v)),
                                                                            enableMinimum(nullptr),
                                                                            minimumBound(nullptr)
{
    QGridLayout *layout = new QGridLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setColumnStretch(0, 0);
    layout->setColumnStretch(1, 1);
    layout->setColumnStretch(2, 0);
    layout->setColumnStretch(3, 1);

    CalibrationEdit *editor = new CalibrationEdit(this);
    layout->addWidget(editor, 0, 0, 1, -1);
    editor->setToolTip(tr("The calibration in effect."));
    editor->setStatusTip(tr("Calibration"));
    editor->setWhatsThis(tr("This is the calibration polynomial that is applied to data."));
    editor->setCalibration(Variant::Composite::toCalibration(value.hash("Calibration")));
    connect(editor, SIGNAL(calibrationChanged(
                                   const CPD3::Calibration &)), this, SLOT(setCalibration(
                                                                                   const CPD3::Calibration &)));

    if (!enableLimits)
        return;

    enableMinimum = new QCheckBox(tr("Minimum:"), this);
    layout->addWidget(enableMinimum, 1, 0);
    enableMinimum->setToolTip(tr("Enable the minimum bound for the result."));
    enableMinimum->setStatusTip(tr("Enable minimum"));
    enableMinimum->setWhatsThis(
            tr("This enables or disables the application of the minimum bound."));
    enableMinimum->setChecked(FP::defined(value.hash("Minimum").toDouble()));
    connect(enableMinimum, SIGNAL(toggled(bool)), this, SLOT(minimumChanged()));

    minimumBound = new QLineEdit(this);
    layout->addWidget(minimumBound, 1, 1);
    minimumBound->setToolTip(
            tr("The minimum bound for the result.  This is not strictly enforced but is used in calculation."));
    minimumBound->setStatusTip(tr("Minimum bound"));
    minimumBound->setWhatsThis(
            tr("This sets the minimum bound for the result.  The bound is not strictly enforced but is used during the inverse calculation for root selection."));
    minimumBound->setValidator(new QDoubleValidator(minimumBound));
    if (enableMinimum->isChecked()) {
        minimumBound->setText(QString::number(value.hash("Minimum").toDouble()));
        minimumBound->setEnabled(true);
    } else {
        minimumBound->setEnabled(false);
    }
    connect(minimumBound, SIGNAL(textEdited(
                                         const QString &)), this, SLOT(minimumChanged()));

    enableMaximum = new QCheckBox(tr("Maximum:"), this);
    layout->addWidget(enableMaximum, 1, 2);
    enableMaximum->setToolTip(tr("Enable the maximum bound for the result."));
    enableMaximum->setStatusTip(tr("Enable maximum"));
    enableMaximum->setWhatsThis(
            tr("This enables or disables the application of the maximum bound."));
    enableMaximum->setChecked(FP::defined(value.hash("Maximum").toDouble()));
    connect(enableMaximum, SIGNAL(toggled(bool)), this, SLOT(maximumChanged()));

    maximumBound = new QLineEdit(this);
    layout->addWidget(maximumBound, 1, 3);
    maximumBound->setToolTip(
            tr("The maximum bound for the result.  This is not strictly enforced but is used in calculation."));
    maximumBound->setStatusTip(tr("Maximum bound"));
    maximumBound->setWhatsThis(
            tr("This sets the maximum bound for the result.  The bound is not strictly enforced but is used during the inverse calculation for root selection."));
    maximumBound->setValidator(new QDoubleValidator(maximumBound));
    if (enableMaximum->isChecked()) {
        maximumBound->setText(QString::number(value.hash("Maximum").toDouble()));
        maximumBound->setEnabled(true);
    } else {
        maximumBound->setEnabled(false);
    }
    connect(maximumBound, SIGNAL(textEdited(
                                         const QString &)), this, SLOT(maximumChanged()));
}

EditActionCalibrationEditor::~EditActionCalibrationEditor() = default;

void EditActionCalibrationEditor::setCalibration(const Calibration &cal)
{
    Variant::Composite::fromCalibration(value.hash("Calibration"), cal);
    emit changed();
}

void EditActionCalibrationEditor::minimumChanged()
{
    if (enableMinimum == NULL || minimumBound == NULL)
        return;
    if (!enableMinimum->isChecked()) {
        minimumBound->setEnabled(false);
        value.hash("Minimum").remove();
    } else {
        minimumBound->setEnabled(true);
        bool ok = false;
        double v = minimumBound->text().toDouble(&ok);
        if (!ok || !FP::defined(v)) {
            value.hash("Minimum").remove();
        } else {
            value.hash("Minimum").setDouble(v);
        }
    }
    emit changed();
}

void EditActionCalibrationEditor::maximumChanged()
{
    if (enableMaximum == NULL || maximumBound == NULL)
        return;
    if (!enableMaximum->isChecked()) {
        maximumBound->setEnabled(false);
        value.hash("Maximum").remove();
    } else {
        maximumBound->setEnabled(true);
        bool ok = false;
        double v = maximumBound->text().toDouble(&ok);
        if (!ok || !FP::defined(v)) {
            value.hash("Maximum").remove();
        } else {
            value.hash("Maximum").setDouble(v);
        }
    }
    emit changed();
}

EditActionRecalibrationEditor::EditActionRecalibrationEditor(Variant::Write v, QWidget *parent)
        : QWidget(parent), value(std::move(v))
{
    QVBoxLayout *topLayout = new QVBoxLayout;
    setLayout(topLayout);
    topLayout->setContentsMargins(0, 0, 0, 0);

    QGroupBox *box = new QGroupBox(tr("Corrected"), this);
    topLayout->addWidget(box);

    QGridLayout *layout = new QGridLayout;
    box->setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setColumnStretch(0, 0);

    CalibrationEdit *editor = new CalibrationEdit(box);
    layout->addWidget(editor, 0, 0);
    editor->setToolTip(tr("The corrected calibration to be applied."));
    editor->setStatusTip(tr("Corrected calibration"));
    editor->setWhatsThis(
            tr("This is the corrected (true) calibration that will be applied to the inverted original data."));
    editor->setCalibration(Variant::Composite::toCalibration(value.hash("Calibration")));
    connect(editor, SIGNAL(calibrationChanged(
                                   const CPD3::Calibration &)), this, SLOT(setCalibration(
                                                                                   const CPD3::Calibration &)));


    box = new QGroupBox(tr("Original"), this);
    topLayout->addWidget(box);

    layout = new QGridLayout;
    box->setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setColumnStretch(0, 0);
    layout->setColumnStretch(1, 1);
    layout->setColumnStretch(2, 0);
    layout->setColumnStretch(3, 1);

    editor = new CalibrationEdit(box);
    layout->addWidget(editor, 0, 0, 1, -1);
    editor->setToolTip(tr("The original calibration in the input data."));
    editor->setStatusTip(tr("Original calibration"));
    editor->setWhatsThis(
            tr("This is the original (incorrect) calibration that is in effect in the input data."));
    editor->setCalibration(Variant::Composite::toCalibration(value.hash("Original")));
    connect(editor, SIGNAL(calibrationChanged(
                                   const CPD3::Calibration &)), this, SLOT(setOriginal(
                                                                                   const CPD3::Calibration &)));

    enableMinimum = new QCheckBox(tr("Minimum:"), box);
    layout->addWidget(enableMinimum, 2, 0);
    enableMinimum->setToolTip(tr("Enable the minimum bound for the result."));
    enableMinimum->setStatusTip(tr("Enable minimum"));
    enableMinimum->setWhatsThis(
            tr("This enables or disables the application of the minimum bound."));
    enableMinimum->setChecked(FP::defined(value.hash("Minimum").toDouble()));
    connect(enableMinimum, SIGNAL(toggled(bool)), this, SLOT(minimumChanged()));

    minimumBound = new QLineEdit(box);
    layout->addWidget(minimumBound, 2, 1);
    minimumBound->setToolTip(
            tr("The minimum bound for the result.  This is not strictly enforced but is used in calculation."));
    minimumBound->setStatusTip(tr("Minimum bound"));
    minimumBound->setWhatsThis(
            tr("This sets the minimum bound for the result.  The bound is not strictly enforced but is used during the inverse calculation for root selection."));
    minimumBound->setValidator(new QDoubleValidator(minimumBound));
    if (enableMinimum->isChecked()) {
        minimumBound->setText(QString::number(value.hash("Minimum").toDouble()));
        minimumBound->setEnabled(true);
    } else {
        minimumBound->setEnabled(false);
    }
    connect(minimumBound, SIGNAL(textEdited(
                                         const QString &)), this, SLOT(minimumChanged()));

    enableMaximum = new QCheckBox(tr("Maximum:"), box);
    layout->addWidget(enableMaximum, 2, 2);
    enableMaximum->setToolTip(tr("Enable the maximum bound for the result."));
    enableMaximum->setStatusTip(tr("Enable maximum"));
    enableMaximum->setWhatsThis(
            tr("This enables or disables the application of the maximum bound."));
    enableMaximum->setChecked(FP::defined(value.hash("Maximum").toDouble()));
    connect(enableMaximum, SIGNAL(toggled(bool)), this, SLOT(maximumChanged()));

    maximumBound = new QLineEdit(box);
    layout->addWidget(maximumBound, 2, 3);
    maximumBound->setToolTip(
            tr("The maximum bound for the result.  This is not strictly enforced but is used in calculation."));
    maximumBound->setStatusTip(tr("Maximum bound"));
    maximumBound->setWhatsThis(
            tr("This sets the maximum bound for the result.  The bound is not strictly enforced but is used during the inverse calculation for root selection."));
    maximumBound->setValidator(new QDoubleValidator(maximumBound));
    if (enableMaximum->isChecked()) {
        maximumBound->setText(QString::number(value.hash("Maximum").toDouble()));
        maximumBound->setEnabled(true);
    } else {
        maximumBound->setEnabled(false);
    }
    connect(maximumBound, SIGNAL(textEdited(
                                         const QString &)), this, SLOT(maximumChanged()));
}

EditActionRecalibrationEditor::~EditActionRecalibrationEditor() = default;

void EditActionRecalibrationEditor::setCalibration(const Calibration &cal)
{
    Variant::Composite::fromCalibration(value.hash("Calibration"), cal);
    emit changed();
}

void EditActionRecalibrationEditor::setOriginal(const Calibration &cal)
{
    Variant::Composite::fromCalibration(value.hash("Original"), cal);
    emit changed();
}

void EditActionRecalibrationEditor::minimumChanged()
{
    if (enableMinimum == NULL || minimumBound == NULL)
        return;
    if (!enableMinimum->isChecked()) {
        minimumBound->setEnabled(false);
        value.hash("Minimum").remove();
    } else {
        minimumBound->setEnabled(true);
        bool ok = false;
        double v = minimumBound->text().toDouble(&ok);
        if (!ok || !FP::defined(v)) {
            value.hash("Minimum").remove();
        } else {
            value.hash("Minimum").setDouble(v);
        }
    }
    emit changed();
}

void EditActionRecalibrationEditor::maximumChanged()
{
    if (enableMaximum == NULL || maximumBound == NULL)
        return;
    if (!enableMaximum->isChecked()) {
        maximumBound->setEnabled(false);
        value.hash("Maximum").remove();
    } else {
        maximumBound->setEnabled(true);
        bool ok = false;
        double v = maximumBound->text().toDouble(&ok);
        if (!ok || !FP::defined(v)) {
            value.hash("Maximum").remove();
        } else {
            value.hash("Maximum").setDouble(v);
        }
    }
    emit changed();
}

namespace {
class MultiCalibrationTableModel : public QAbstractItemModel {
    QSettings settings;
    Variant::Write value;

    struct DisplayData {
        double time;
        Calibration calibration;
    };
    QList<DisplayData> displayedPoints;

    QString buildTimeString(double time) const
    {
        return GUITime::formatTime(time, &settings, true);
    }

    QString buildCalibrationString(const Calibration &calibration) const
    {
        QString result;

        NumberFormat ef(1, 0);
        for (int i = 0, max = calibration.size(); i < max; i++) {
            double v = calibration.get(i);
            if (v == 0.0)
                continue;

            if (result.isEmpty()) {
                result.append(QString::number(v));
            } else {
                if (v < 0.0) {
                    result.append(" - ");
                    v = -v;
                } else {
                    result.append(" + ");
                }
                result.append(QString::number(v));
            }

            if (i > 0)
                result.append('x');
            if (i > 1)
                result.append(ef.superscript(i));
        }
        if (result.isEmpty())
            return QString::number(0.0);
        return result;
    }

public:
    enum {
        Column_Time, Column_Calibration,

        Column_TOTAL
    };

    MultiCalibrationTableModel(Variant::Write v, EditActionMultiCalibrationPointEditor *parent)
            : QAbstractItemModel(parent),
              settings(CPD3GUI_ORGANIZATION, CPD3GUI_APPLICATION), value(std::move(v))
    {
        for (auto add : value.hash("Calibrations").toKeyframe()) {
            if (!FP::defined(add.first) || !add.second.exists())
                continue;
            DisplayData data;
            data.time = add.first;
            data.calibration = Variant::Composite::toCalibration(add.second);
            displayedPoints.append(data);
        }
    }

    virtual ~MultiCalibrationTableModel() = default;

    virtual QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const
    {
        Q_UNUSED(parent);
        return createIndex(row, column);
    }

    virtual QModelIndex parent(const QModelIndex &index) const
    {
        Q_UNUSED(index);
        return QModelIndex();
    }

    virtual int rowCount(const QModelIndex &parent) const
    {
        Q_UNUSED(parent);
        return displayedPoints.size();
    }

    virtual int columnCount(const QModelIndex &parent) const
    {
        Q_UNUSED(parent);
        return Column_TOTAL;
    }

    virtual QVariant data(const QModelIndex &index, int role) const
    {
        if (!index.isValid())
            return QVariant();
        if (index.row() >= displayedPoints.size())
            return QVariant();
        DisplayData data(displayedPoints.at(index.row()));

        switch (role) {
        case Qt::DisplayRole:
            switch (index.column()) {
            case Column_Time:
                return buildTimeString(data.time);
            case Column_Calibration:
                return buildCalibrationString(data.calibration);
            default:
                break;
            }
            break;

        case Qt::EditRole:
            switch (index.column()) {
            case Column_Time:
                return buildTimeString(data.time);
            case Column_Calibration:
                return QVariant();
            default:
                break;
            }
            break;

        case Qt::FontRole:
            switch (index.column()) {
            case Column_Time: {
                QFont font;
                font.setFamily("Monospace");
                font.setStyleHint(QFont::TypeWriter);
                /* font.setBold(true); */
                return font;
            }
            default:
                break;
            }
            break;

        case Qt::UserRole:
            return data.time;

        default:
            break;
        }
        return QVariant();
    }

    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const
    {
        if (role != Qt::DisplayRole)
            return QVariant();
        if (orientation == Qt::Vertical)
            return QVariant(section + 1);

        switch (section) {
        case Column_Time:
            return tr("Time");
        case Column_Calibration:
            return tr("Calibration");
        default:
            break;
        }
        return QVariant();
    }

    virtual bool setData(const QModelIndex &index, const QVariant &value, int role)
    {
        if (role != Qt::EditRole)
            return false;

        if (index.row() >= displayedPoints.size())
            return false;

        switch (index.column()) {
        case Column_Time: {
            QString str(value.toString().trimmed());
            if (!str.isEmpty()) {
                try {
                    double time = TimeParse::parseTime(str);
                    if (!FP::defined(time))
                        return false;
                    if (this->value.hash("Calibrations").keyframe(time).exists())
                        return false;

                    double oldTime = displayedPoints.at(index.row()).time;
                    displayedPoints[index.row()].time = time;

                    this->value.hash("Calibrations").keyframe(oldTime).remove();
                    Variant::Composite::fromCalibration(
                            this->value.hash("Calibrations").keyframe(time),
                            displayedPoints[index.row()].calibration);
                } catch (TimeParsingException tpe) {
                    return false;
                }
            } else {
                return false;
            }
            emit dataChanged(index, index);
            return true;
        }
        default:
            break;
        }

        return false;
    }

    virtual Qt::ItemFlags flags(const QModelIndex &index) const
    {
        if (!index.isValid())
            return 0;

        switch (index.column()) {
        case Column_Time:
            return Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled;
        case Column_Calibration:
            return Qt::ItemIsSelectable | Qt::ItemIsEnabled;
        default:
            break;
        }

        return 0;
    }

    virtual bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex())
    {
        Q_ASSERT(row + count <= displayedPoints.size());

        beginRemoveRows(parent, row, row + count - 1);
        for (int i = 0; i < count; i++) {
            value.hash("Calibrations").keyframe(displayedPoints.at(row).time).remove();
            displayedPoints.removeAt(row);
        }
        endRemoveRows();

        return true;
    }

    virtual bool insertRows(int row, int count, const QModelIndex &parent = QModelIndex())
    {
        beginInsertRows(parent, row, row + count - 1);
        for (int i = 0; i < count; i++, row++) {
            DisplayData data;
            int from = row - 1;
            if (from < 0)
                from = 0;
            else if (from >= displayedPoints.size())
                from = displayedPoints.size() - 1;
            if (from >= 0 && from < displayedPoints.size()) {
                data = displayedPoints.at(from);
            } else {
                data.time = Time::time();
                data.calibration = Calibration();
            }
            while (value.hash("Calibrations").keyframe(data.time).exists()) {
                data.time += 3600.0;
            }

            displayedPoints.insert(row, data);

            Variant::Composite::fromCalibration(value.hash("Calibrations").keyframe(data.time),
                                                data.calibration);
        }
        endInsertRows();

        return true;
    }

    static inline bool compareTimeAscending(const DisplayData &a, const DisplayData &b)
    {
        return Range::compareStart(a.time, b.time) < 0;
    }

    static inline bool compareTimeDescending(const DisplayData &a, const DisplayData &b)
    {
        return Range::compareStart(a.time, b.time) > 0;
    }

    virtual void sort(int column, Qt::SortOrder order = Qt::AscendingOrder)
    {
        beginResetModel();
        switch (column) {
        case Column_Time:
            std::stable_sort(displayedPoints.begin(), displayedPoints.end(),
                        order == Qt::AscendingOrder ? compareTimeAscending : compareTimeDescending);
            break;
        case Column_Calibration:
            break;
        default:
            break;
        }
        endResetModel();
    }

    void setCalibration(const QModelIndex &index, const Calibration &calibration)
    {
        if (index.row() >= displayedPoints.size())
            return;

        displayedPoints[index.row()].calibration = calibration;
        Variant::Composite::fromCalibration(
                value.hash("Calibrations").keyframe(displayedPoints.at(index.row()).time),
                calibration);
        QModelIndex changed(this->index(index.row(), 1));

        emit dataChanged(changed, changed);
    }

    Calibration getCalibration(const QModelIndex &index) const
    {
        if (index.row() >= displayedPoints.size())
            return Calibration();
        return displayedPoints.at(index.row()).calibration;
    }
};

class MultiCalibrationTimeDelegate : public QStyledItemDelegate {
public:
    MultiCalibrationTimeDelegate(QObject *parent = 0) : QStyledItemDelegate(parent)
    { }

    virtual ~MultiCalibrationTimeDelegate() = default;

    virtual QWidget *createEditor(QWidget *parent,
                                  const QStyleOptionViewItem &option,
                                  const QModelIndex &index) const
    {

        QWidget *result =
                QItemEditorFactory::defaultFactory()->createEditor(static_cast<QVariant::Type>(
                                                                           index.data(Qt::EditRole)
                                                                                .userType()),
                                                                   parent);

        QFont font(option.font);
        font.setFamily("Monospace");
        font.setStyleHint(QFont::TypeWriter);
        result->setFont(font);

        return result;
    }
};

}

EditActionMultiCalibrationPointEditor::EditActionMultiCalibrationPointEditor(Variant::Write v,
                                                                             QWidget *parent)
        : QWidget(parent), value(std::move(v))
{
    QGridLayout *layout = new QGridLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setRowStretch(0, 0);
    layout->setRowStretch(1, 1);
    layout->setRowStretch(2, 0);

    QPushButton *addButton = new QPushButton(tr("&Add Point"), this);
    layout->addWidget(addButton, 0, 0);
    addButton->setToolTip(tr("Add another time point."));
    addButton->setStatusTip(tr("Add time"));
    addButton->setWhatsThis(
            tr("This adds another time point to the interpolated calibration list."));
    connect(addButton, SIGNAL(clicked(bool)), this, SLOT(addPressed()));

    removeButton = new QPushButton(tr("&Remove Point"), this);
    layout->addWidget(removeButton, 0, 1);
    removeButton->setToolTip(tr("Remove a time point."));
    removeButton->setStatusTip(tr("Remove time"));
    removeButton->setWhatsThis(tr("This removes the selected calibration time point."));
    removeButton->setEnabled(false);
    connect(removeButton, SIGNAL(clicked(bool)), this, SLOT(removePressed()));

    MultiCalibrationTableModel *model = new MultiCalibrationTableModel(value, this);
    table = new QTableView(this);
    layout->addWidget(table, 1, 0, 1, -1);
    table->setModel(model);

    connect(model, SIGNAL(dataChanged(
                                  const QModelIndex &, const QModelIndex &)), this,
            SIGNAL(changed()));

    table->setToolTip(tr("The table of all calibration points."));
    table->setStatusTip(tr("Calibration points"));
    table->setWhatsThis(tr("This table represents all the time-calibration points in use."));

    /* table->verticalHeader()->hide(); */
    table->setSelectionMode(QAbstractItemView::SingleSelection);
    table->setSelectionBehavior(QAbstractItemView::SelectRows);
    table->setSortingEnabled(true);
    table->sortByColumn(MultiCalibrationTableModel::Column_Time, Qt::AscendingOrder);
    connect(table->selectionModel(), SIGNAL(selectionChanged(
                                                    const QItemSelection &, const QItemSelection &)),
            this, SLOT(selectedChanged()));

    table->horizontalHeader()
         ->setSectionResizeMode(MultiCalibrationTableModel::Column_Time,
                                QHeaderView::ResizeToContents);

    table->horizontalHeader()->setStretchLastSection(true);
    table->horizontalHeader()
         ->setSectionResizeMode(MultiCalibrationTableModel::Column_Calibration,
                                QHeaderView::Stretch);

    table->setItemDelegateForColumn(MultiCalibrationTableModel::Column_Time,
                                    new MultiCalibrationTimeDelegate(table));

    calibration = new CalibrationEdit(this);
    layout->addWidget(calibration, 2, 0, 1, -1);
    calibration->setToolTip(tr("The calibration in effect at the selected time."));
    calibration->setStatusTip(tr("Calibration"));
    calibration->setWhatsThis(
            tr("This is the calibration polynomial that is applied to data at the selected time."));
    calibration->setEnabled(false);
    connect(calibration, SIGNAL(calibrationChanged(
                                        const CPD3::Calibration &)), this, SLOT(setCalibration(
                                                                                        const CPD3::Calibration &)));
}

EditActionMultiCalibrationPointEditor::~EditActionMultiCalibrationPointEditor() = default;

void EditActionMultiCalibrationPointEditor::addPressed()
{
    int row = table->model()->rowCount();
    table->model()->insertRows(row, 1);
    table->selectionModel()
         ->select(table->model()->index(row, 0),
                  QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);

    emit changed();
}

void EditActionMultiCalibrationPointEditor::removePressed()
{
    QList<int> rows;
    QModelIndexList selectedRows(table->selectionModel()->selectedRows());
    for (QModelIndexList::const_iterator index = selectedRows.constBegin(),
            end = selectedRows.constEnd(); index != end; ++index) {
        rows.append(index->row());
    }
    std::sort(rows.begin(), rows.end());
    for (int i = rows.size() - 1; i >= 0; i--) {
        table->model()->removeRows(rows.at(i), 1);
    }

    emit changed();
}

void EditActionMultiCalibrationPointEditor::setCalibration(const CPD3::Calibration &cal)
{
    QModelIndexList selectedRows(table->selectionModel()->selectedRows());
    for (QModelIndexList::const_iterator index = selectedRows.constBegin(),
            end = selectedRows.constEnd(); index != end; ++index) {
        static_cast<MultiCalibrationTableModel *>(table->model())->setCalibration(*index, cal);
    }
}

void EditActionMultiCalibrationPointEditor::selectedChanged()
{
    QModelIndexList selectedRows(table->selectionModel()->selectedRows());
    removeButton->setEnabled(!selectedRows.isEmpty());

    if (selectedRows.size() == 1) {
        calibration->setEnabled(true);

        calibration->disconnect(this);
        for (QList<QModelIndex>::const_iterator index = selectedRows.constBegin(),
                end = selectedRows.constEnd(); index != end; ++index) {
            calibration->setCalibration(
                    static_cast<MultiCalibrationTableModel *>(table->model())->getCalibration(
                            *index));
        }
        connect(calibration, SIGNAL(calibrationChanged(
                                            const CPD3::Calibration &)), this, SLOT(setCalibration(
                                                                                            const CPD3::Calibration &)));
    } else {
        calibration->setEnabled(false);
    }
}


EditActionMultiCalibrationInterpolationEditor::EditActionMultiCalibrationInterpolationEditor(Variant::Write v,
                                                                                             QWidget *parent)
        : QWidget(parent), value(std::move(v))
{
    QVBoxLayout *topLayout = new QVBoxLayout;
    setLayout(topLayout);
    topLayout->setContentsMargins(0, 0, 0, 0);

    method = new QComboBox(this);
    topLayout->addWidget(method);
    method->setToolTip(tr("The interpolation method applied to the calibrations."));
    method->setStatusTip(tr("Interpolation method"));
    method->setWhatsThis(
            tr("This is the method used to derive the calibration actually applied to the data."));

    {
        EditActionEnumEditor *timePoint = new EditActionEnumEditor(value, "Time", tr("Time:"),
                                                                   tr("Select the origin in time that interpolation is done against."),
                                                                   tr("Time origin"),
                                                                   tr("This sets the origin of the time that the interpolation function uses as an input.  For example, setting this to the start means that the start time of each data point is what is used as the interpolation input."),
                                                                   this);
        topLayout->addWidget(timePoint);
        connect(timePoint, SIGNAL(changed()), this, SIGNAL(changed()));
        timePoint->addItem("Middle", QStringList("Center"), tr("Center"),
                           tr("Use the midpoint between the start and end."), tr("Middle"),
                           tr("Use the exact midpoint between the start and end times of each data point."));
        timePoint->addItem("Start", QStringList("Begin"), tr("Start"),
                           tr("Use the data value start time."), tr("Start"),
                           tr("Use the start time of data values."));
        timePoint->addItem("End", QStringList(), tr("End"), tr("Use the data value end time."),
                           tr("End"), tr("Use the end time of data values."));
    }

    {
        EditActionBooleanEditor *extrapolate =
                new EditActionBooleanEditor(value, "Extrapolate", tr("Extrapolate after ends"),
                                            tr("Enable extrapolation beyond the first and last calibrations."),
                                            tr("Enable extrapolation"),
                                            tr("When this is set, calibrations will be extrapolated beyond the first and last ones specified, if available.  When disabled, the value at the end points are used instead."),
                                            false, this);
        topLayout->addWidget(extrapolate);
        connect(extrapolate, SIGNAL(changed()), this, SIGNAL(changed()));
    }

    const auto &current = value.hash("Interpolation").toString();

    int index = method->count();
    method->addItem(tr("Linear"), "Linear");
    method->setItemData(index, tr("Piecewise linear interpolation."), Qt::ToolTipRole);
    method->setItemData(index, tr("Linear interpolation"), Qt::StatusTipRole);
    method->setItemData(index,
                        tr("The calibration is interpolated by generating a series of line segments connecting all the specified calibration time points."),
                        Qt::WhatsThisRole);
    method->setCurrentIndex(index);
    {
        QLabel *description = new QLabel(
                tr("The specified series of calibrations is used to generate a series of line segments that connect them with their neighbors.  These line segments are then used to derive the intermediate calibrations that are applied to the data."),
                this);
        description->setWordWrap(true);
        details.append(description);
    }

    index = method->count();
    method->addItem(tr("Cubic spline"), "CubicSpline");
    method->setItemData(index, tr("Interpolate using a cubic spline."), Qt::ToolTipRole);
    method->setItemData(index, tr("Cubic spline"), Qt::StatusTipRole);
    method->setItemData(index,
                        tr("The calibrations specified are used as the points to generate a cubic spline curve.  That curve is then used to derive the calibration applied to the data as a function of time."),
                        Qt::WhatsThisRole);
    if (Util::equal_insensitive(current, "CSpline", "CubicSpline"))
        method->setCurrentIndex(index);
    {
        QWidget *d = new QWidget(this);
        QGridLayout *layout = new QGridLayout;
        d->setLayout(layout);
        layout->setContentsMargins(0, 0, 0, 0);
        details.append(d);
        layout->setColumnStretch(1, 1);

        QCheckBox *enable = new QCheckBox(tr("Clamp start:"), d);
        layout->addWidget(enable, 0, 0);
        enable->setToolTip(tr("Clamp the first point in the spline."));
        enable->setStatusTip(tr("Spline clamp start"));
        enable->setWhatsThis(
                tr("When enabled, this clamps the derivative of the spline to the specified value at the first point."));
        double k = value.hash("ClampStart").toDouble();
        enable->setChecked(FP::defined(k));
        connect(enable, SIGNAL(toggled(bool)), this, SLOT(setClampStartEnabled(bool)));

        dX0 = new QLineEdit(d);
        layout->addWidget(dX0, 0, 1);
        dX0->setToolTip(
                tr("The value to clamp the derivative of the spline at the first point to."));
        dX0->setStatusTip(tr("Spline clamp value"));
        dX0->setWhatsThis(
                tr("This sets the derivative of the spline at the first point to a specific value."));
        dX0->setValidator(new QDoubleValidator(dX0));
        if (enable->isChecked()) {
            dX0->setText(QString::number(k));
            dX0->setEnabled(true);
        } else {
            dX0->setText(QString::number(0.0));
            dX0->setEnabled(false);
        }
        connect(dX0, SIGNAL(textEdited(
                                    const QString &)), this, SLOT(clampStartChanged(
                                                                          const QString &)));


        enable = new QCheckBox(tr("Clamp end:"), d);
        layout->addWidget(enable, 1, 0);
        enable->setToolTip(tr("Clamp the last point in the spline."));
        enable->setStatusTip(tr("Spline clamp end"));
        enable->setWhatsThis(
                tr("When enabled, this clamps the derivative of the spline to the specified value at the last point."));
        k = value.hash("ClampEnd").toDouble();
        enable->setChecked(FP::defined(k));
        connect(enable, SIGNAL(toggled(bool)), this, SLOT(setClampEndEnabled(bool)));

        dXN = new QLineEdit(d);
        layout->addWidget(dXN, 1, 1);
        dXN->setToolTip(
                tr("The value to clamp the derivative of the spline at the last point to."));
        dXN->setStatusTip(tr("Spline clamp value"));
        dXN->setWhatsThis(
                tr("This sets the derivative of the spline at the last point to a specific value."));
        dXN->setValidator(new QDoubleValidator(dXN));
        if (enable->isChecked()) {
            dXN->setText(QString::number(k));
            dXN->setEnabled(true);
        } else {
            dXN->setText(QString::number(0.0));
            dXN->setEnabled(false);
        }
        connect(dXN, SIGNAL(textEdited(
                                    const QString &)), this, SLOT(clampEndChanged(
                                                                          const QString &)));

        QLabel *label = new QLabel(
                tr("The specified series of calibrations is used as the points to derive a cubic spline as a function of time.  That spline is then used to dervice the calibration applied to data as a function of time.  The clamping parameters set the derviative of the spline at the endpoints.  When not set, the spline uses the \"natural\" value."),
                d);
        label->setWordWrap(true);
        layout->addWidget(label, 2, 0, 1, -1);
    }


    index = method->count();
    method->addItem(tr("Sin Transfer Function"), "Sin");
    method->setItemData(index, tr("Piecewise with sin base transfer functions."), Qt::ToolTipRole);
    method->setItemData(index, tr("Piecewise sin transfer"), Qt::StatusTipRole);
    method->setItemData(index,
                        tr("The calibrations are used to generate a series of linear segments, with the final output value being derived by applying those linear segments to a sin-based transfer function."),
                        Qt::WhatsThisRole);
    if (Util::equal_insensitive(current, "sin"))
        method->setCurrentIndex(index);
    {
        QWidget *d = new QWidget(this);
        QGridLayout *layout = new QGridLayout;
        d->setLayout(layout);
        layout->setContentsMargins(0, 0, 0, 0);
        details.append(d);
        layout->setColumnStretch(1, 1);

        QLabel *label = new QLabel(tr("Power:"), d);
        layout->addWidget(label, 0, 0);

        QLineEdit *p = new QLineEdit(d);
        double k = value.hash("SinPower").toDouble();
        label->setBuddy(p);
        layout->addWidget(p, 0, 1);
        p->setToolTip(tr("The power the sin function is raised to."));
        p->setStatusTip(tr("Sin transfer power"));
        p->setWhatsThis(tr("This sets the power that the sin transfer function is raised to."));
        p->setValidator(new QDoubleValidator(0.0, 100.0, 1000, p));
        if (FP::defined(k))
            p->setText(QString::number(k));
        connect(p, SIGNAL(textEdited(
                                  const QString &)), this, SLOT(sinChanged(
                                                                        const QString &)));

        label = new QLabel(
                tr("The specified series of calibrations is used to generate a series of linear segments and those linear segments are interpolated using a sin-base transfer function.  The transfer function is of the form <b>sin(x*pi/2)^p</b>.  That is, a smaller parameter results in a midpoint earlier in time and more rapid change to reach it."),
                d);
        label->setWordWrap(true);
        layout->addWidget(label, 1, 0, 1, -1);
    }

    index = method->count();
    method->addItem(tr("Cos Transfer Function"), "Cos");
    method->setItemData(index, tr("Piecewise with cos base transfer functions."), Qt::ToolTipRole);
    method->setItemData(index, tr("Piecewise cos transfer"), Qt::StatusTipRole);
    method->setItemData(index,
                        tr("The calibrations are used to generate a series of linear segments, with the final output value being derived by applying those linear segments to a cos-based transfer function."),
                        Qt::WhatsThisRole);
    if (Util::equal_insensitive(current, "cos"))
        method->setCurrentIndex(index);
    {
        QWidget *d = new QWidget(this);
        QGridLayout *layout = new QGridLayout;
        d->setLayout(layout);
        layout->setContentsMargins(0, 0, 0, 0);
        details.append(d);
        layout->setColumnStretch(1, 1);

        QLabel *label = new QLabel(tr("Power:"), d);
        layout->addWidget(label, 0, 0);

        QLineEdit *p = new QLineEdit(d);
        double k = value.hash("CosPower").toDouble();
        label->setBuddy(p);
        layout->addWidget(p, 0, 1);
        p->setToolTip(tr("The power the cos function is raised to."));
        p->setStatusTip(tr("Cos transfer power"));
        p->setWhatsThis(tr("This sets the power that the cos transfer function is raised to."));
        p->setValidator(new QDoubleValidator(0.0, 100.0, 1000, p));
        if (FP::defined(k))
            p->setText(QString::number(k));
        connect(p, SIGNAL(textEdited(
                                  const QString &)), this, SLOT(cosChanged(
                                                                        const QString &)));

        label = new QLabel(
                tr("The specified series of calibrations is used to generate a series of linear segments and those linear segments are interpolated using a cos-base transfer function.  The transfer function is of the form <b>1-cos(x*pi/2)^p</b>.  That is, a smaller parameter results in a midpoint later in time and less rapid change to reach it."),
                p);
        label->setWordWrap(true);
        layout->addWidget(label, 1, 0, 1, -1);
    }

    index = method->count();
    method->addItem(tr("Normalized Sigmoid Transfer Function"), "Sigmoid");
    method->setItemData(index, tr("Piecewise with normalized sigmoid transfer functions."),
                        Qt::ToolTipRole);
    method->setItemData(index, tr("Piecewise sigmoid transfer"), Qt::StatusTipRole);
    method->setItemData(index,
                        tr("The calibrations are used to generate a series of linear segments, with the final output value being derived by applying those linear segments to a normalized sigmoid transfer function."),
                        Qt::WhatsThisRole);
    if (Util::equal_insensitive(current, "sigmoid"))
        method->setCurrentIndex(index);
    {
        QWidget *d = new QWidget(this);
        QGridLayout *layout = new QGridLayout;
        d->setLayout(layout);
        layout->setContentsMargins(0, 0, 0, 0);
        details.append(d);
        layout->setColumnStretch(1, 1);

        QLabel *label = new QLabel(tr("Parameter:"), d);
        layout->addWidget(label, 0, 0);

        QLineEdit *p = new QLineEdit(d);
        double k = value.hash("SigmoidSlope").toDouble();
        label->setBuddy(p);
        layout->addWidget(p, 0, 1);
        p->setToolTip(tr("The parameter of the sigmoid."));
        p->setStatusTip(tr("Sigmoid parameter"));
        p->setWhatsThis(tr("This sets the tunable parameter of the normalized sigmoid."));
        p->setValidator(new QDoubleValidator(-1.0, 1.0, 1000, p));
        if (FP::defined(k))
            p->setText(QString::number(k));
        connect(p, SIGNAL(textEdited(
                                  const QString &)), this, SLOT(sigmoidChanged(
                                                                        const QString &)));

        label = new QLabel(
                tr("The specified series of calibrations is used to generate a series of linear segments and those linear segments are interpolated using a sigmoid transfer function.  The parameter ranges from -1 to +1.  Values more negative result in slower change before the inflection point half way and an infinite derivative at it.  Zero results in linear interpolation.  Values more positive result in rapid change before the inflection point and a zero deviative at it."),
                d);
        label->setWordWrap(true);
        layout->addWidget(label, 1, 0, 1, -1);
    }

    connect(method, SIGNAL(currentIndexChanged(int)), this, SLOT(methodChanged(int)));

    index = 0;
    for (QList<QWidget *>::const_iterator add = details.constBegin(), endAdd = details.constEnd();
            add != endAdd;
            ++add, ++index) {
        topLayout->addWidget(*add);
        if (index == method->currentIndex())
            (*add)->show();
        else
            (*add)->hide();
    }
    topLayout->addStretch(1);
}

EditActionMultiCalibrationInterpolationEditor::~EditActionMultiCalibrationInterpolationEditor() = default;

void EditActionMultiCalibrationInterpolationEditor::methodChanged(int index)
{
    value.hash("Interpolation").setString(method->itemData(index).toString());

    for (int i = 0, max = details.size(); i < max; i++) {
        if (index == i)
            details.at(i)->show();
        else
            details.at(i)->hide();
    }

    emit changed();
}

void EditActionMultiCalibrationInterpolationEditor::setClampStartEnabled(bool enabled)
{
    if (enabled) {
        dX0->setEnabled(true);
        clampStartChanged(dX0->text());
    } else {
        dX0->setEnabled(false);
        value.hash("ClampStart").remove();
        emit changed();
    }
}

void EditActionMultiCalibrationInterpolationEditor::clampStartChanged(const QString &text)
{
    bool ok = false;
    double v = text.toDouble(&ok);
    if (!ok || !FP::defined(v)) {
        value.hash("ClampStart").remove();
    } else {
        value.hash("ClampStart").setDouble(v);
    }

    emit changed();
}

void EditActionMultiCalibrationInterpolationEditor::setClampEndEnabled(bool enabled)
{
    if (enabled) {
        dXN->setEnabled(true);
        clampEndChanged(dXN->text());
    } else {
        dXN->setEnabled(false);
        value.hash("ClampEnd").remove();
        emit changed();
    }
}

void EditActionMultiCalibrationInterpolationEditor::clampEndChanged(const QString &text)
{
    bool ok = false;
    double v = text.toDouble(&ok);
    if (!ok || !FP::defined(v)) {
        value.hash("ClampEnd").remove();
    } else {
        value.hash("ClampEnd").setDouble(v);
    }

    emit changed();
}

void EditActionMultiCalibrationInterpolationEditor::sinChanged(const QString &text)
{
    bool ok = false;
    double v = text.toDouble(&ok);
    if (!ok || !FP::defined(v)) {
        value.hash("SinPower").remove();
    } else {
        value.hash("SinPower").setDouble(v);
    }

    emit changed();
}

void EditActionMultiCalibrationInterpolationEditor::cosChanged(const QString &text)
{
    bool ok = false;
    double v = text.toDouble(&ok);
    if (!ok || !FP::defined(v)) {
        value.hash("CosPower").remove();
    } else {
        value.hash("CosPower").setDouble(v);
    }

    emit changed();
}

void EditActionMultiCalibrationInterpolationEditor::sigmoidChanged(const QString &text)
{
    bool ok = false;
    double v = text.toDouble(&ok);
    if (!ok || !FP::defined(v)) {
        value.hash("SigmoidSlope").remove();
    } else {
        value.hash("SigmoidSlope").setDouble(v);
    }

    emit changed();
}

EditActionWrapModularEditor::EditActionWrapModularEditor(Variant::Write v, QWidget *parent)
        : QWidget(parent), value(std::move(v)), startBound(nullptr), endBound(nullptr)
{
    QGridLayout *layout = new QGridLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setColumnStretch(0, 0);
    layout->setColumnStretch(1, 1);

    QLabel *label = new QLabel(tr("&Start:"), this);
    layout->addWidget(label, 0, 0);

    startBound = new QLineEdit(this);
    label->setBuddy(startBound);
    layout->addWidget(startBound, 0, 1);
    startBound->setToolTip(
            tr("The start bound for the result.  This is not strictly enforced but is used in calculation."));
    startBound->setStatusTip(tr("Start bound"));
    startBound->setWhatsThis(
            tr("This sets the start bound for the result.  The bound is not strictly enforced but is used during the inverse calculation for root selection."));
    startBound->setValidator(new QDoubleValidator(startBound));
    if (!FP::defined(value.hash("Start").toDouble())) {
        startBound->setText("0");
    } else {
        startBound->setText(QString::number(value.hash("Start").toDouble()));
    }
    startBound->setEnabled(true);
    connect(startBound, SIGNAL(textEdited(
                                       const QString &)), this, SLOT(startChanged()));

    label = new QLabel(tr("&End:"), this);
    layout->addWidget(label, 1, 0);

    endBound = new QLineEdit(this);
    label->setBuddy(endBound);
    layout->addWidget(endBound, 1, 1);
    endBound->setToolTip(
            tr("The end bound for the result.  This is not strictly enforced but is used in calculation."));
    endBound->setStatusTip(tr("End bound"));
    endBound->setWhatsThis(
            tr("This sets the end bound for the result.  The bound is not strictly enforced but is used during the inverse calculation for root selection."));
    endBound->setValidator(new QDoubleValidator(endBound));
    if (!FP::defined(value.hash("End").toDouble())) {
        endBound->setText("0");
    } else {
        endBound->setText(QString::number(value.hash("End").toDouble()));
    }
    connect(endBound, SIGNAL(textEdited(
                                     const QString &)), this, SLOT(endChanged()));
}

EditActionWrapModularEditor::~EditActionWrapModularEditor() = default;

void EditActionWrapModularEditor::startChanged()
{
    if (startBound == NULL)
        return;
    bool ok = false;
    double v = startBound->text().toDouble(&ok);
    if (!ok || !FP::defined(v)) {
        return;
    } else {
        value.hash("Start").setDouble(v);
    }
    emit changed();
}

void EditActionWrapModularEditor::endChanged()
{
    if (endBound == NULL)
        return;
    endBound->setEnabled(true);
    bool ok = false;
    double v = endBound->text().toDouble(&ok);
    if (!ok || !FP::defined(v)) {
        return;
    } else {
        value.hash("End").setDouble(v);
    }
    emit changed();
}


static qint64 convertHex(const QString &input)
{
    bool ok = false;
    qint64 result;
    if (!input.startsWith("0x", Qt::CaseInsensitive))
        result = input.toLongLong(&ok, 16);
    else
        result = input.mid(2).toLongLong(&ok, 16);
    if (!ok)
        return INTEGER::undefined();
    return result;
}

namespace {
class HexValidator : public QValidator {
public:
    HexValidator(QObject *parent = 0) : QValidator(parent)
    { }

    virtual ~HexValidator() = default;

    virtual QValidator::State validate(QString &input, int &pos) const
    {
        Q_UNUSED(pos);
        if (input.trimmed().isEmpty())
            return QValidator::Intermediate;
        if (!input.startsWith("0x", Qt::CaseInsensitive)) {
            bool ok = false;
            qint64 i = input.toLongLong(&ok, 16);
            if (!ok || i < 0)
                return QValidator::Invalid;
        } else {
            if (input.length() <= 2)
                return QValidator::Intermediate;
            bool ok = false;
            qint64 i = input.mid(2).toLongLong(&ok, 16);
            if (!ok || i < 0)
                return QValidator::Invalid;
        }
        return QValidator::Acceptable;
    }

    virtual void fixup(QString &input) const
    {
        qint64 i = convertHex(input);
        if (!INTEGER::defined(i))
            i = 0;
        input = QString::fromLatin1("0x%1").arg(QString::number(i).rightJustified(4, '0'));
    }
};

class EmptyHexValidator : public QValidator {
public:
    EmptyHexValidator(QObject *parent = 0) : QValidator(parent)
    { }

    virtual ~EmptyHexValidator() = default;

    virtual QValidator::State validate(QString &input, int &pos) const
    {
        Q_UNUSED(pos);
        if (input.trimmed().isEmpty())
            return QValidator::Acceptable;
        if (!input.startsWith("0x", Qt::CaseInsensitive)) {
            bool ok = false;
            qint64 i = input.toLongLong(&ok, 16);
            if (!ok || i < 0)
                return QValidator::Invalid;
        } else {
            if (input.length() <= 2)
                return QValidator::Intermediate;
            bool ok = false;
            qint64 i = input.mid(2).toLongLong(&ok, 16);
            if (!ok || i < 0)
                return QValidator::Invalid;
        }
        return QValidator::Acceptable;
    }

    virtual void fixup(QString &input) const
    {
        if (input.trimmed().isEmpty())
            return;
        qint64 i = convertHex(input);
        if (!INTEGER::defined(i))
            i = 0;
        input = QString::fromLatin1("0x%1").arg(QString::number(i).rightJustified(4, '0'));
    }
};
}

EditActionContaminateEditor::EditActionContaminateEditor(Variant::Write v, QWidget *parent)
        : QWidget(parent), value(std::move(v))
{
    QVBoxLayout *layout = new QVBoxLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    showAdvanced = new QCheckBox(tr("&Advanced"), this);
    layout->addWidget(showAdvanced);
    showAdvanced->setToolTip(tr("Show advanced options."));
    showAdvanced->setStatusTip(tr("Show advanced"));
    showAdvanced->setWhatsThis(
            tr("This enables or disables the display and alteration of advanced options."));
    showAdvanced->setChecked(value.hash("Description").exists() ||
                                     INTEGER::defined(value.hash("Bits").toInt64()) ||
                                     value.hash("Origin").exists());
    connect(showAdvanced, SIGNAL(toggled(bool)), this, SLOT(advancedToggled(bool)));


    basicPane = new QWidget(this);
    layout->addWidget(basicPane);


    advancedPane = new QWidget(this);
    layout->addWidget(advancedPane);
    QFormLayout *advancedLayout = new QFormLayout;
    advancedPane->setLayout(advancedLayout);

    originEdit = new QLineEdit(advancedPane);
    originEdit->setToolTip(tr("Set the origin code for the contamination."));
    originEdit->setStatusTip(tr("Contamination origin code"));
    originEdit->setWhatsThis(
            tr("This is the origin code for the contamination.  The default is \"Mentor\"."));
    if (value.hash("Origin").exists()) {
        originEdit->setText(value.hash("Origin").toQString());
    } else {
        originEdit->setText("Mentor");
    }
    connect(originEdit, SIGNAL(textEdited(
                                       const QString &)), this, SLOT(setOrigin(
                                                                             const QString &)));
    advancedLayout->addRow(tr("&Origin:"), originEdit);

    bitsEdit = new QLineEdit(advancedPane);
    bitsEdit->setToolTip(tr("Set the export bitmask for the contamination."));
    bitsEdit->setStatusTip(tr("Contamination export bits"));
    bitsEdit->setWhatsThis(tr("This is the bits set when exporting to a bitmasked flags field."));
    bitsEdit->setValidator(new HexValidator(bitsEdit));
    {
        qint64 i = value.hash("Bits").toInt64();
        if (!INTEGER::defined(i) || i < 0)
            i = 0x0002;
        bitsEdit->setText(QString("0x%1").arg(i));
    }
    connect(bitsEdit, SIGNAL(textEdited(
                                     const QString &)), this, SLOT(setBits(
                                                                           const QString &)));
    advancedLayout->addRow(tr("&Bitmask:"), bitsEdit);

    descriptionEdit = new QLineEdit(advancedPane);
    descriptionEdit->setToolTip(tr("Set the description of the contamination."));
    descriptionEdit->setStatusTip(tr("Contamination description"));
    descriptionEdit->setWhatsThis(
            tr("This is the human readable description of the contamination.  This should provide a general idea of why the data is considered unsuitable for averaging."));
    if (value.hash("Description").exists()) {
        descriptionEdit->setText(value.hash("Description").toQString());
    } else {
        descriptionEdit->setText(QObject::tr("Data flagged as contaminated by the station mentor"));
    }
    connect(descriptionEdit, SIGNAL(textEdited(
                                            const QString &)), this, SLOT(setDescription(
                                                                                  const QString &)));
    advancedLayout->addRow(tr("&Description:"), descriptionEdit);


    basicPane->setVisible(!showAdvanced->isChecked());
    advancedPane->setVisible(showAdvanced->isChecked());
}

EditActionContaminateEditor::~EditActionContaminateEditor() = default;

void EditActionContaminateEditor::advancedToggled(bool enable)
{
    if (enable) {
        value.hash("Origin").setString(originEdit->text());
        value.hash("Description").setString(descriptionEdit->text());
        value.hash("Bits").setInt64(convertHex(bitsEdit->text()));

        basicPane->setVisible(false);
        advancedPane->setVisible(true);
    } else {
        value.hash("Description").remove();
        value.hash("Bits").remove();
        value.hash("Origin").remove();

        basicPane->setVisible(true);
        advancedPane->setVisible(false);
    }
    emit changed();
}

void EditActionContaminateEditor::setOrigin(const QString &origin)
{
    value.hash("Origin").setString(origin);
    emit changed();
}

void EditActionContaminateEditor::setBits(const QString &bits)
{
    value.hash("Bits").setInt64(convertHex(bits));
    emit changed();
}

void EditActionContaminateEditor::setDescription(const QString &desc)
{
    value.hash("Description").setString(desc);
    emit changed();
}

EditActionSpotCorrectionEditor::EditActionSpotCorrectionEditor(Variant::Write v, QWidget *parent)
        : QWidget(parent), value(std::move(v))
{
    QGridLayout *layout = new QGridLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    QLabel *label = new QLabel(tr("&Corrected:"), this);
    layout->addWidget(label, 0, 0);
    QLineEdit *corrected = new QLineEdit(this);
    label->setBuddy(corrected);
    layout->addWidget(corrected, 0, 1);
    QDoubleValidator *validator = new QDoubleValidator(corrected);
    validator->setBottom(0.0001);
    validator->setDecimals(4);
    corrected->setValidator(validator);
    corrected->setToolTip(tr("The corrected spot size in mm\xC2\xB2."));
    corrected->setStatusTip(tr("Corrected spot size"));
    corrected->setWhatsThis(
            tr("This is the corrected (true) spot size in mm\xC2\xB2.  Data will be converted so that it appears that this spot size was in effect."));
    {
        double cv = value.hash("Corrected").toDouble();
        if (FP::defined(cv) && cv > 0.0)
            corrected->setText(QString::number(cv));
    }
    connect(corrected, SIGNAL(textEdited(
                                      const QString &)), this, SLOT(setCorrectedValue(
                                                                            const QString &)));

    label = new QLabel(tr("mm\xC2\xB2"), this);
    layout->addWidget(label, 0, 2);

    double originalArea = value.hash("Original").toDouble();

    QCheckBox *enableOriginal = new QCheckBox(tr("&Original:"), this);
    layout->addWidget(enableOriginal, 1, 0);
    enableOriginal->setToolTip(tr("Enable manual entry of the original (incorrect) spot size."));
    enableOriginal->setStatusTip(tr("Enable original entry"));
    enableOriginal->setWhatsThis(
            tr("When enabled, this allows for entry of the original (incorrect) spot size in mm\xC2\xB2.  If it is disabled then the original spot size is read from the data."));
    enableOriginal->setChecked(FP::defined(originalArea) && originalArea > 0.0);
    connect(enableOriginal, SIGNAL(toggled(bool)), this, SLOT(enableOriginalToggled(bool)));

    original = new QLineEdit(this);
    layout->addWidget(original, 1, 1);
    validator = new QDoubleValidator(original);
    validator->setBottom(0.0001);
    validator->setDecimals(4);
    original->setValidator(validator);
    original->setToolTip(tr("The original spot size in mm\xC2\xB2."));
    original->setStatusTip(tr("Uncorrected spot size"));
    original->setWhatsThis(
            tr("This is the original (uncorrected) spot size in mm\xC2\xB2.  That is, this is the spot size the data was originally sampled with."));
    if (FP::defined(originalArea) && originalArea > 0.0)
        original->setText(QString::number(originalArea));
    original->setEnabled(enableOriginal->isChecked());
    connect(original, SIGNAL(textEdited(
                                     const QString &)), this, SLOT(setOriginalValue(
                                                                           const QString &)));

    label = new QLabel(tr("mm\xC2\xB2"), this);
    layout->addWidget(label, 1, 2);
}

EditActionSpotCorrectionEditor::~EditActionSpotCorrectionEditor() = default;

void EditActionSpotCorrectionEditor::enableOriginalToggled(bool enable)
{
    original->setEnabled(enable);
    if (enable) {
        bool ok = false;
        double v = original->text().toDouble(&ok);
        if (ok && FP::defined(v) && v > 0.0) {
            value.hash("Original").setDouble(v);
            emit changed();
        }
    } else {
        value.hash("Original").remove();
        emit changed();
    }
}

void EditActionSpotCorrectionEditor::setOriginalValue(const QString &v)
{
    bool ok = false;
    double vv = v.toDouble(&ok);
    if (!ok || vv <= 0.0)
        return;
    value.hash("Original").setDouble(vv);
    emit changed();
}

void EditActionSpotCorrectionEditor::setCorrectedValue(const QString &v)
{
    bool ok = false;
    double vv = v.toDouble(&ok);
    if (!ok || vv <= 0.0)
        return;
    value.hash("Corrected").setDouble(vv);
    emit changed();
}

EditActionMultiSpotCorrectionEditor::EditActionMultiSpotCorrectionEditor(Variant::Write v,
                                                                         QWidget *parent) : QWidget(
        parent), value(std::move(v))
{
    QGridLayout *layout = new QGridLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setRowStretch(0, 1);
    layout->setRowStretch(1, 0);

    table = new QTableWidget(this);
    layout->addWidget(table, 0, 0, 1, 2);
    table->setColumnCount(3);
    table->setSelectionMode(QAbstractItemView::SingleSelection);

    table->verticalHeader()->hide();

    QTableWidgetItem *item = new QTableWidgetItem(tr("Spot"));
    item->setData(Qt::ToolTipRole, tr("The index of the spot being changed."));
    item->setData(Qt::WhatsThisRole,
                  tr("This is the index of the active spot that determines which area is used."));
    table->setHorizontalHeaderItem(0, item);

    item = new QTableWidgetItem(tr("Corrected"));
    item->setData(Qt::ToolTipRole, tr("The corrected spot size in mm\xC2\xB2."));
    item->setData(Qt::WhatsThisRole,
                  tr("This is the corrected (true) spot size in mm\xC2\xB2.  Data will be converted so that it appears that this spot size was in effect."));
    table->setHorizontalHeaderItem(1, item);

    item = new QTableWidgetItem(tr("Original"));
    item->setData(Qt::ToolTipRole, tr("The original spot size in mm\xC2\xB2."));
    item->setData(Qt::WhatsThisRole,
                  tr("This is the original (uncorrected) spot size in mm\xC2\xB2.  That is, this is the spot size the data was originally sampled with."));
    table->setHorizontalHeaderItem(2, item);

    table->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    table->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    table->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);

    QPushButton *addButton = new QPushButton(tr("&Add"), this);
    layout->addWidget(addButton, 1, 0);
    addButton->setToolTip(tr("Add another spot index."));
    addButton->setStatusTip(tr("Add spot index"));
    addButton->setWhatsThis(
            tr("This adds another spot index to the end of the list of possible ones."));
    connect(addButton, SIGNAL(clicked(bool)), this, SLOT(addPressed()));

    removeButton = new QPushButton(tr("&Remove"), this);
    layout->addWidget(removeButton, 1, 1);
    removeButton->setToolTip(tr("Remove a spot index."));
    removeButton->setStatusTip(tr("Remove spot index"));
    removeButton->setWhatsThis(
            tr("This removes the last spot index from the list of possible ones."));
    removeButton->setEnabled(false);
    connect(removeButton, SIGNAL(clicked(bool)), this, SLOT(removePressed()));


    for (std::size_t i = 0, max = std::max(value.hash("Original").toArray().size(),
                                           value.hash("Corrected").toArray().size());
            i < max;
            i++) {
        table->insertRow(i);
        removeButton->setEnabled(true);

        QTableWidgetItem *titem = new QTableWidgetItem(tr("%1", "spot format").arg(i + 1));
        titem->setFlags(Qt::ItemIsEnabled);
        titem->setData(Qt::ToolTipRole, tr("The index of the spot being changed."));
        titem->setData(Qt::WhatsThisRole,
                       tr("This is the index of the active spot that determines which area is used."));
        table->setItem(i, 0, titem);

        double cv = value.hash("Corrected").array(i).toDouble();
        if (FP::defined(cv) && cv > 0.0) {
            titem = new QTableWidgetItem(tr("%1", "corrected format").arg(cv));
        } else {
            titem = new QTableWidgetItem(QString());
        }
        titem->setFlags(Qt::ItemIsSelectable |
                                Qt::ItemIsEditable |
                                Qt::ItemIsDragEnabled |
                                Qt::ItemIsDropEnabled |
                                Qt::ItemIsEnabled);
        titem->setData(Qt::ToolTipRole, tr("The corrected spot size in mm\xC2\xB2."));
        titem->setData(Qt::WhatsThisRole,
                       tr("This is the corrected (true) spot size in mm\xC2\xB2.  Data will be converted so that it appears that this spot size was in effect."));
        table->setItem(i, 1, titem);

        cv = value.hash("Original").array(i).toDouble();
        if (FP::defined(cv) && cv > 0.0) {
            titem = new QTableWidgetItem(tr("%1", "original format").arg(cv));
        } else {
            titem = new QTableWidgetItem(QString());
        }
        titem->setFlags(Qt::ItemIsSelectable |
                                Qt::ItemIsEditable |
                                Qt::ItemIsDragEnabled |
                                Qt::ItemIsDropEnabled |
                                Qt::ItemIsEnabled);
        titem->setData(Qt::ToolTipRole,
                       tr("The original spot size in mm\xC2\xB2.  Leave empty to use the value recorded in the data."));
        titem->setData(Qt::WhatsThisRole,
                       tr("This is the original (uncorrected) spot size in mm\xC2\xB2.  That is, this is the spot size the data was originally sampled with.  When left empty the value is obtained from the data being processed."));
        table->setItem(i, 2, titem);
    }

    connect(table->model(), SIGNAL(dataChanged(
                                           const QModelIndex &, const QModelIndex &)), this,
            SLOT(itemChanged(
                         const QModelIndex &)));
}

EditActionMultiSpotCorrectionEditor::~EditActionMultiSpotCorrectionEditor() = default;

void EditActionMultiSpotCorrectionEditor::addPressed()
{
    int index = table->rowCount();
    table->insertRow(index);

    table->model()->disconnect(this);

    QTableWidgetItem *item = new QTableWidgetItem(tr("%1", "spot format").arg(index + 1));
    item->setFlags(Qt::ItemIsEnabled);
    item->setData(Qt::ToolTipRole, tr("The index of the spot being changed."));
    item->setData(Qt::WhatsThisRole,
                  tr("This is the index of the active spot that determines which area is used."));
    table->setItem(index, 0, item);

    item = new QTableWidgetItem(QString());
    item->setFlags(Qt::ItemIsSelectable |
                           Qt::ItemIsEditable |
                           Qt::ItemIsDragEnabled |
                           Qt::ItemIsDropEnabled |
                           Qt::ItemIsEnabled);
    item->setData(Qt::ToolTipRole, tr("The corrected spot size in mm\xC2\xB2."));
    item->setData(Qt::WhatsThisRole,
                  tr("This is the corrected (true) spot size in mm\xC2\xB2.  Data will be converted so that it appears that this spot size was in effect."));
    table->setItem(index, 1, item);

    item = new QTableWidgetItem(QString());
    item->setFlags(Qt::ItemIsSelectable |
                           Qt::ItemIsEditable |
                           Qt::ItemIsDragEnabled |
                           Qt::ItemIsDropEnabled |
                           Qt::ItemIsEnabled);
    item->setData(Qt::ToolTipRole,
                  tr("The original spot size in mm\xC2\xB2.  Leave empty to use the value recorded in the data."));
    item->setData(Qt::WhatsThisRole,
                  tr("This is the original (uncorrected) spot size in mm\xC2\xB2.  That is, this is the spot size the data was originally sampled with.  When left empty the value is obtained from the data being processed."));
    table->setItem(index, 2, item);

    connect(table->model(), SIGNAL(dataChanged(
                                           const QModelIndex &, const QModelIndex &)), this,
            SLOT(itemChanged(
                         const QModelIndex &)));

    removeButton->setEnabled(true);

    value.hash("Corrected").array(index).setDouble(FP::undefined());
    value.hash("Original").array(index).setDouble(FP::undefined());

    emit changed();
}

void EditActionMultiSpotCorrectionEditor::removePressed()
{
    int index = table->rowCount();
    if (index <= 0)
        return;

    table->removeRow(index - 1);

    removeButton->setEnabled(index > 1);

    value.hash("Corrected").array(index - 1).remove();
    value.hash("Original").array(index - 1).remove();

    emit changed();
}

void EditActionMultiSpotCorrectionEditor::itemChanged(const QModelIndex &topLeft)
{
    int index = topLeft.row();
    if (index < 0)
        return;
    int col = topLeft.column();

    switch (col) {
    case 1: {
        double v = topLeft.data().toDouble();
        if (FP::defined(v) && v > 0.0) {
            value.hash("Corrected").array(index).setDouble(v);
            emit changed();
        } else {
            table->model()->disconnect(this);
            table->item(index, 1)->setData(Qt::DisplayRole, QString());
            table->item(index, 1)->setData(Qt::EditRole, QString());
            connect(table->model(), SIGNAL(dataChanged(
                                                   const QModelIndex &, const QModelIndex &)), this,
                    SLOT(itemChanged(
                                 const QModelIndex &)));
        }
        break;
    }
    case 2: {
        double v = topLeft.data().toDouble();
        if (FP::defined(v) && v > 0.0) {
            value.hash("Original").array(index).setDouble(v);
        } else {
            value.hash("Original").array(index).setDouble(FP::undefined());

            table->model()->disconnect(this);
            table->item(index, 2)->setData(Qt::DisplayRole, QString());
            table->item(index, 2)->setData(Qt::EditRole, QString());
            connect(table->model(), SIGNAL(dataChanged(
                                                   const QModelIndex &, const QModelIndex &)), this,
                    SLOT(itemChanged(
                                 const QModelIndex &)));
        }
        emit changed();
        break;
    }
    default:
        break;
    }
}

static Variant::Flags toFlags(const Variant::Read &config)
{
    switch (config.getType()) {
    case Variant::Type::String: {
        const auto &flag = config.toString();
        if (flag.empty())
            return Variant::Flags();
        return Variant::Flags{flag};
    }
    default:
        break;
    }
    return config.toFlags();
}

EditActionFlagsEditor::EditActionFlagsEditor(Variant::Write v, QWidget *parent) : QWidget(parent),
                                                                                  value(std::move(
                                                                                          v))
{
    QVBoxLayout *layout = new QVBoxLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    flagsButton = new QPushButton(tr("&Flags"), this);
    layout->addWidget(flagsButton);
    flagsButton->setToolTip(tr("Select flags."));
    flagsButton->setStatusTip(tr("Flags"));
    flagsButton->setWhatsThis(tr("Use this button to select the flags the action operates on."));

    flagsMenu = new QMenu(flagsButton);
    delimiterAction = flagsMenu->addSeparator();
    QAction *addFlagAction = flagsMenu->addAction(tr("&Add"));
    connect(addFlagAction, SIGNAL(triggered(bool)), this, SLOT(addFlag()));

    auto selectedFlags = toFlags(value.hash("Flags"));
    Util::merge(toFlags(value.hash("Flag")), selectedFlags);
    if (selectedFlags.empty()) {
        connect(flagsButton, SIGNAL(clicked(bool)), this, SLOT(addFlag()));
    } else {
        flagsButton->setMenu(flagsMenu);
        QStringList sorted;
        for (const auto &add : selectedFlags) {
            sorted.push_back(QString::fromStdString(add));
        }
        std::sort(sorted.begin(), sorted.end());
        for (const auto &flag : sorted) {
            QAction *action = new QAction(flag, flagsMenu);
            flagsMenu->insertAction(delimiterAction, action);
            flagActions.insert(flag, action);

            action->setCheckable(true);
            action->setChecked(true);

            connect(action, SIGNAL(toggled(bool)), this, SLOT(flagToggled()));
        }
    }
}

EditActionFlagsEditor::~EditActionFlagsEditor() = default;

void EditActionFlagsEditor::addFlag()
{
    bool ok;
    QString flag(QInputDialog::getText(this, tr("Add Flag"), tr("Add flag:"), QLineEdit::Normal,
                                       QString(), &ok));
    if (!ok || flag.isEmpty())
        return;

    QHash<QString, QAction *>::const_iterator check = flagActions.constFind(flag);
    if (check != flagActions.constEnd()) {
        check.value()->setChecked(true);
        return;
    }

    QAction *action = new QAction(flag, flagsMenu);
    flagsMenu->insertAction(delimiterAction, action);
    flagActions.insert(flag, action);

    action->setCheckable(true);
    action->setChecked(true);

    flagsButton->disconnect(this);
    flagsButton->setMenu(flagsMenu);
    connect(action, SIGNAL(toggled(bool)), this, SLOT(flagToggled()));

    flagToggled();
}

void EditActionFlagsEditor::flagToggled()
{
    Variant::Flags result;
    for (QHash<QString, QAction *>::const_iterator flag = flagActions.constBegin(),
            endFlag = flagActions.constEnd(); flag != endFlag; ++flag) {
        if (!flag.value()->isChecked())
            continue;
        result.insert(flag.key().toStdString());
    }
    value.hash("Flag").remove();
    value.hash("Flags").setFlags(std::move(result));
    emit changed();
}

EditActionFlagsParametersEditor::EditActionFlagsParametersEditor(Variant::Write v, QWidget *parent)
        : QWidget(parent), value(std::move(v))
{
    QFormLayout *layout = new QFormLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    QLineEdit *bitsEdit = new QLineEdit(this);
    bitsEdit->setToolTip(tr("Set the export bitmask for the flag."));
    bitsEdit->setStatusTip(tr("Flag export bits"));
    bitsEdit->setWhatsThis(tr("This is the bits set when exporting to a bitmasked flags field."));
    bitsEdit->setValidator(new EmptyHexValidator(bitsEdit));
    {
        qint64 i = value.hash("Bits").toInt64();
        if (INTEGER::defined(i) && i > 0)
            bitsEdit->setText(QString("0x%1").arg(i));
    }
    connect(bitsEdit, SIGNAL(textEdited(
                                     const QString &)), this, SLOT(setBits(
                                                                           const QString &)));
    layout->addRow(tr("&Bitmask:"), bitsEdit);

    QLineEdit *descriptionEdit = new QLineEdit(this);
    descriptionEdit->setToolTip(tr("Set the description of the flag."));
    descriptionEdit->setStatusTip(tr("Flag description"));
    descriptionEdit->setWhatsThis(
            tr("This is the human readable description of the flag.  This should be a general description of what the flag indicates."));
    if (value.hash("Description").exists()) {
        descriptionEdit->setText(value.hash("Description").toQString());
    }
    connect(descriptionEdit, SIGNAL(textEdited(
                                            const QString &)), this, SLOT(setDescription(
                                                                                  const QString &)));
    layout->addRow(tr("&Description:"), descriptionEdit);
}

EditActionFlagsParametersEditor::~EditActionFlagsParametersEditor() = default;

void EditActionFlagsParametersEditor::setBits(const QString &bits)
{
    if (bits.trimmed().isEmpty()) {
        value.hash("Bits").remove();
    } else {
        value.hash("Bits").setInt64(convertHex(bits));
    }
    emit changed();
}

void EditActionFlagsParametersEditor::setDescription(const QString &desc)
{
    value.hash("Description").setString(desc);
    emit changed();
}

EditActionMatchedFlagsEditor::EditActionMatchedFlagsEditor(Variant::Write v, QWidget *parent)
        : QWidget(parent), value(std::move(v))
{
    QGridLayout *layout = new QGridLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setRowStretch(0, 1);
    layout->setRowStretch(1, 0);

    list = new QListWidget(this);
    layout->addWidget(list, 0, 0, 1, 2);
    list->setToolTip(tr("The list of matched patterns."));
    list->setStatusTip(tr("Patterns"));
    list->setWhatsThis(
            tr("This is the list of patterns applied to match flags.  These are regular expressions applied to the whole flag."));
    list->setSelectionMode(QAbstractItemView::SingleSelection);

    QPushButton *addButton = new QPushButton(tr("&Add"), this);
    layout->addWidget(addButton, 1, 0);
    addButton->setToolTip(tr("Add another pattern to match flags."));
    addButton->setStatusTip(tr("Add pattern"));
    addButton->setWhatsThis(tr("This adds another pattern to the list of ones being matched."));
    connect(addButton, SIGNAL(clicked(bool)), this, SLOT(addPressed()));

    removeButton = new QPushButton(tr("&Remove"), this);
    layout->addWidget(removeButton, 1, 1);
    removeButton->setToolTip(tr("Remove the selected pattern."));
    removeButton->setStatusTip(tr("Remove pattern"));
    removeButton->setWhatsThis(tr("This removes the selected pattern from the list."));
    removeButton->setEnabled(false);
    connect(removeButton, SIGNAL(clicked(bool)), this, SLOT(removePressed()));

    for (const auto &add : value.hash("Flags").toChildren().keys()) {
        QListWidgetItem *item = new QListWidgetItem(QString::fromStdString(add));
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled);
        list->addItem(item);
    }

    connect(list->model(), SIGNAL(dataChanged(
                                          const QModelIndex &, const QModelIndex &)), this,
            SLOT(itemChanged()));
    connect(list, SIGNAL(itemSelectionChanged()), this, SLOT(selectionChanged()));
}

EditActionMatchedFlagsEditor::~EditActionMatchedFlagsEditor() = default;

void EditActionMatchedFlagsEditor::writePatterns()
{
    value.hash("Flags").setEmpty();
    for (int i = 0, max = list->count(); i < max; i++) {
        value.hash("Flags").array(i).setString(list->item(i)->data(Qt::EditRole).toString());
    }
}

void EditActionMatchedFlagsEditor::addPressed()
{
    QListWidgetItem *item = new QListWidgetItem(".*", list);
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEditable | Qt::ItemIsEnabled);

    writePatterns();
    emit changed();
}

void EditActionMatchedFlagsEditor::removePressed()
{
    QList<QListWidgetItem *> selected(list->selectedItems());
    if (selected.isEmpty())
        return;
    for (int i = selected.size() - 1; i >= 0; --i) {
        delete list->takeItem(list->row(selected.at(i)));
    }

    writePatterns();
    emit changed();
}

void EditActionMatchedFlagsEditor::itemChanged()
{
    writePatterns();
    emit changed();
}

void EditActionMatchedFlagsEditor::selectionChanged()
{
    removeButton->setEnabled(!list->selectedItems().isEmpty());
}

EditActionCutSizeEditor::EditActionCutSizeEditor(Variant::Write v, QWidget *parent) : QWidget(
        parent), value(std::move(v))
{
    QFormLayout *layout = new QFormLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    selection = new QComboBox(this);
    layout->addRow(tr("Size selection:"), selection);
    selection->setToolTip(tr("The cut size code that the data are changed to."));
    selection->setStatusTip(tr("Cut size"));
    selection->setWhatsThis(
            tr("This is the cut size code applied to all the selected data.  Selecting \"None\" will clear the size selection indicator from the data."));
    selection->setInsertPolicy(QComboBox::NoInsert);
    selection->setEditable(true);

    auto possibleCuts = SequenceName::cutSizeFlavors();
    QString currentCut(value.hash("Cut").toQString().toLower());
    if (!currentCut.isEmpty())
        possibleCuts.insert(currentCut.toLower().toStdString());
    QStringList sorted = Util::to_qstringlist(possibleCuts);
    std::sort(sorted.begin(), sorted.end());
    int selectedIndex = -1;
    for (int i = 0; i < sorted.size(); i++) {
        QString add(sorted.at(i).toLower());
        selection->addItem(add.toUpper(), add);

        if (add == currentCut)
            selectedIndex = i;
    }
    selection->insertSeparator(selection->count());
    selection->addItem(tr("None"), QString());
    if (selectedIndex != -1)
        selection->setCurrentIndex(selectedIndex);
    else
        selection->setCurrentIndex(selection->count() - 1);

    connect(selection, SIGNAL(currentIndexChanged(int)), this, SLOT(indexSelected()));
    connect(selection->lineEdit(), SIGNAL(textEdited(
                                                  const QString &)), this, SLOT(manualSelected()));
}

EditActionCutSizeEditor::~EditActionCutSizeEditor() = default;

void EditActionCutSizeEditor::indexSelected()
{
    QString cut;
    int idx = selection->currentIndex();
    if (idx >= 0) {
        cut = selection->itemData(idx).toString();
    } else {
        cut = selection->currentText();
    }

    value.hash("Cut").setString(cut);
    emit changed();
}

void EditActionCutSizeEditor::manualSelected()
{
    value.hash("Cut").setString(selection->currentText().toLower());
    emit changed();
}

static std::unordered_set<QString> toFlavors(const Variant::Read &config)
{
    std::unordered_set<QString> result;
    for (const auto &add : SequenceName::toFlavors(config)) {
        result.insert(QString::fromStdString(add).toLower());
    }
    return result;
}

EditActionFlavorsEditor::EditActionFlavorsEditor(Variant::Write v,
                                                 EditActionEditor *editor,
                                                 const QString &label, QWidget *parent) : QWidget(
        parent), value(std::move(v))
{
    QVBoxLayout *layout = new QVBoxLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    flavorsButton = new QPushButton(label, this);
    layout->addWidget(flavorsButton);

    flavorsMenu = new QMenu(flavorsButton);
    delimiterAction = flavorsMenu->addSeparator();
    QAction *addFlavorAction = flavorsMenu->addAction(tr("&Add"));
    connect(addFlavorAction, SIGNAL(triggered(bool)), this, SLOT(addFlavor()));
    QAction *clearFlavorsAction = flavorsMenu->addAction(tr("&Clear"));
    connect(clearFlavorsAction, SIGNAL(triggered(bool)), this, SLOT(clearFlavors()));

    auto selectedFlavors = toFlavors(value);
    std::unordered_set<QString> availableFlavors = selectedFlavors;
    for (const auto &add : editor->getAvailableFlavors()) {
        availableFlavors.insert(QString::fromStdString(add).toLower());
    }

    if (availableFlavors.empty()) {
        connect(flavorsButton, SIGNAL(clicked(bool)), this, SLOT(addFlavor()));
    } else {
        flavorsButton->setMenu(flavorsMenu);
        QStringList sorted;
        std::copy(availableFlavors.begin(), availableFlavors.end(), Util::back_emplacer(sorted));
        std::sort(sorted.begin(), sorted.end());
        for (const auto &flavor : sorted) {
            QAction *action = new QAction(flavor.toUpper(), flavorsMenu);
            flavorsMenu->insertAction(delimiterAction, action);
            flavorsActions.insert(flavor.toUpper(), action);

            action->setCheckable(true);
            if (selectedFlavors.count(flavor.toLower()))
                action->setChecked(true);

            connect(action, SIGNAL(toggled(bool)), this, SLOT(flavorToggled()));
        }
    }
}

EditActionFlavorsEditor::~EditActionFlavorsEditor() = default;

void EditActionFlavorsEditor::addFlavor()
{
    bool ok;
    QString flavor
            (QInputDialog::getText(this, tr("Add Flavor"), tr("Add flavor:"), QLineEdit::Normal,
                                   QString(), &ok));
    flavor = flavor.toLower();
    if (!ok || flavor.isEmpty())
        return;

    QHash<QString, QAction *>::const_iterator check = flavorsActions.constFind(flavor);
    if (check != flavorsActions.constEnd()) {
        check.value()->setChecked(true);
        return;
    }

    QAction *action = new QAction(flavor, flavorsMenu);
    flavorsMenu->insertAction(delimiterAction, action);
    flavorsActions.insert(flavor, action);

    action->setCheckable(true);
    action->setChecked(true);

    flavorsButton->disconnect(this);
    flavorsButton->setMenu(flavorsMenu);
    connect(action, SIGNAL(toggled(bool)), this, SLOT(flavorToggled()));

    flavorToggled();
}

void EditActionFlavorsEditor::clearFlavors()
{
    for (QHash<QString, QAction *>::const_iterator flavor = flavorsActions.constBegin(),
            endFlavor = flavorsActions.constEnd(); flavor != endFlavor; ++flavor) {
        flavor.value()->setChecked(false);
    }
}

void EditActionFlavorsEditor::flavorToggled()
{
    Variant::Flags result;
    for (QHash<QString, QAction *>::const_iterator flavor = flavorsActions.constBegin(),
            endFlavor = flavorsActions.constEnd(); flavor != endFlavor; ++flavor) {
        if (!flavor.value()->isChecked())
            continue;
        result.insert(flavor.key().toStdString());
    }
    value.hash("Flavors").setFlags(std::move(result));
    emit changed();
}


EditActionUnitEditor::EditActionUnitEditor(Variant::Write v,
                                           EditActionEditor *editor, QWidget *parent) : QWidget(
        parent), value(std::move(v))
{
    QFormLayout *layout = new QFormLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    enableStation = new QCheckBox(tr("Station:"), this);
    enableStation->setToolTip(tr("Enable alteration of the station."));
    enableStation->setStatusTip(tr("Change station"));
    enableStation->setWhatsThis(
            tr("If this is set then alteration of the station component of the identifier is enabled."));
    enableStation->setChecked(value.hash("Station").exists());
    connect(enableStation, SIGNAL(toggled(bool)), this, SLOT(stationChanged()));

    selectedStation = new QComboBox(this);
    layout->addRow(enableStation, selectedStation);
    selectedStation->setToolTip(tr("The station to set in the output identifier."));
    selectedStation->setStatusTip(tr("Station"));
    selectedStation->setWhatsThis(tr("This is the station that all output data is changed to."));
    selectedStation->setInsertPolicy(QComboBox::NoInsert);
    selectedStation->setEditable(true);
    selectedStation->setEnabled(enableStation->isChecked());
    {
        auto all = editor->getAvailableStations();
        QString active(value.hash("Station").toQString().toLower());
        if (!active.isEmpty())
            all.insert(active.toStdString());
        QStringList sorted = Util::to_qstringlist(all);
        std::sort(sorted.begin(), sorted.end());
        for (const auto &add : sorted) {
            selectedStation->addItem(add.toUpper());
            if (add.toLower() == active)
                selectedStation->setCurrentIndex(selectedStation->count() - 1);
        }
    }
    connect(selectedStation, SIGNAL(editTextChanged(
                                            const QString &)), this, SLOT(stationChanged()));


    enableArchive = new QCheckBox(tr("Archive:"), this);
    enableArchive->setToolTip(tr("Enable alteration of the archive."));
    enableArchive->setStatusTip(tr("Change archive"));
    enableArchive->setWhatsThis(
            tr("If this is set then alteration of the archive component of the identifier is enabled."));
    enableArchive->setChecked(value.hash("Archive").exists());
    connect(enableArchive, SIGNAL(toggled(bool)), this, SLOT(archiveChanged()));

    selectedArchive = new QComboBox(this);
    layout->addRow(enableArchive, selectedArchive);
    selectedArchive->setToolTip(tr("The archive to set in the output identifier."));
    selectedArchive->setStatusTip(tr("Archive"));
    selectedArchive->setWhatsThis(tr("This is the archive that all output data is changed to."));
    selectedArchive->setInsertPolicy(QComboBox::NoInsert);
    selectedArchive->setEditable(true);
    selectedArchive->setEnabled(enableArchive->isChecked());
    {
        auto all = editor->getAvailableArchives();
        QString active(value.hash("Archive").toQString().toLower());
        if (!active.isEmpty())
            all.insert(active.toStdString());
        auto sorted = Util::to_qstringlist(all);
        std::sort(sorted.begin(), sorted.end());
        for (const auto &add : sorted) {
            selectedArchive->addItem(add.toUpper());
            if (add.toLower() == active)
                selectedArchive->setCurrentIndex(selectedArchive->count() - 1);
        }
    }
    connect(selectedArchive, SIGNAL(editTextChanged(
                                            const QString &)), this, SLOT(archiveChanged()));


    enableVariable = new QCheckBox(tr("Variable:"), this);
    enableVariable->setToolTip(tr("Enable alteration of the variable."));
    enableVariable->setStatusTip(tr("Change variable"));
    enableVariable->setWhatsThis(
            tr("If this is set then alteration of the variable component of the identifier is enabled."));
    enableVariable->setChecked(value.hash("Variable").exists());
    connect(enableVariable, SIGNAL(toggled(bool)), this, SLOT(variableChanged()));

    selectedVariable = new QComboBox(this);
    layout->addRow(enableVariable, selectedVariable);
    selectedVariable->setToolTip(tr("The variable to set in the output identifier."));
    selectedVariable->setStatusTip(tr("Variable"));
    selectedVariable->setWhatsThis(tr("This is the variable that all output data is changed to."));
    selectedVariable->setInsertPolicy(QComboBox::NoInsert);
    selectedVariable->setEditable(true);
    selectedVariable->setEnabled(enableVariable->isChecked());
    {
        auto all = editor->getAvailableVariables();
        QString active(value.hash("Variable").toQString());
        all.insert(active.toStdString());
        QStringList sorted = Util::to_qstringlist(all);
        std::sort(sorted.begin(), sorted.end());
        for (const auto &add : sorted) {
            selectedVariable->addItem(add);
            if (add == active)
                selectedVariable->setCurrentIndex(selectedVariable->count() - 1);
        }
    }
    connect(selectedVariable, SIGNAL(editTextChanged(
                                             const QString &)), this, SLOT(variableChanged()));


    enableFlavors = new QCheckBox(tr("Flavors:"), this);
    enableFlavors->setToolTip(tr("Enable alteration of the flavors."));
    enableFlavors->setStatusTip(tr("Change flavors"));
    enableFlavors->setWhatsThis(
            tr("If this is set then alteration of the flavors component of the identifier is enabled."));
    enableFlavors->setChecked(value.hash("Flavors").exists());
    connect(enableFlavors, SIGNAL(toggled(bool)), this, SLOT(flavorsChanged()));

    flavorsButton = new QPushButton(tr("Select"), this);
    layout->addRow(enableFlavors, flavorsButton);
    flavorsButton->setToolTip(tr("The flavors."));
    flavorsButton->setStatusTip(tr("Change flavors"));
    flavorsButton->setWhatsThis(
            tr("If this is set then alteration of the flavors component of the identifier is enabled."));
    flavorsButton->setEnabled(enableFlavors->isChecked());

    flavorsMenu = new QMenu(flavorsButton);
    delimiterAction = flavorsMenu->addSeparator();
    QAction *addFlavorAction = flavorsMenu->addAction(tr("&Add"));
    connect(addFlavorAction, SIGNAL(triggered(bool)), this, SLOT(addFlavor()));
    QAction *clearFlavorsAction = flavorsMenu->addAction(tr("&Clear"));
    connect(clearFlavorsAction, SIGNAL(triggered(bool)), this, SLOT(clearFlavors()));

    auto selectedFlavors = toFlavors(value.hash("Flavors"));
    std::unordered_set<QString> availableFlavors = selectedFlavors;
    for (const auto &add : editor->getAvailableFlavors()) {
        availableFlavors.insert(QString::fromStdString(add).toLower());
    }

    if (availableFlavors.empty()) {
        connect(flavorsButton, SIGNAL(clicked(bool)), this, SLOT(addFlavor()));
    } else {
        flavorsButton->setMenu(flavorsMenu);
        std::vector<QString> sorted(availableFlavors.cbegin(), availableFlavors.cend());
        std::sort(sorted.begin(), sorted.end());
        for (const auto &flavor : sorted) {
            QAction *action = new QAction(flavor.toUpper(), flavorsMenu);
            flavorsMenu->insertAction(delimiterAction, action);
            flavorsActions.insert(flavor.toUpper(), action);

            action->setCheckable(true);
            if (selectedFlavors.count(flavor.toLower()))
                action->setChecked(true);

            connect(action, SIGNAL(toggled(bool)), this, SLOT(flavorsChanged()));
        }
    }
}

EditActionUnitEditor::~EditActionUnitEditor() = default;

void EditActionUnitEditor::stationChanged()
{
    if (!enableStation->isChecked()) {
        value.hash("Station").remove();
        selectedStation->setEnabled(false);
    } else {
        value.hash("Station").setString(selectedStation->currentText());
        selectedStation->setEnabled(true);
    }
    emit changed();
}

void EditActionUnitEditor::archiveChanged()
{
    if (!enableArchive->isChecked()) {
        value.hash("Archive").remove();
        selectedArchive->setEnabled(false);
    } else {
        value.hash("Archive").setString(selectedArchive->currentText());
        selectedArchive->setEnabled(true);
    }
    emit changed();
}

void EditActionUnitEditor::variableChanged()
{
    if (!enableVariable->isChecked()) {
        value.hash("Variable").remove();
        selectedVariable->setEnabled(false);
    } else {
        value.hash("Variable").setString(selectedVariable->currentText());
        selectedVariable->setEnabled(true);
    }
    emit changed();
}

void EditActionUnitEditor::addFlavor()
{
    bool ok;
    QString flavor
            (QInputDialog::getText(this, tr("Add Flavor"), tr("Add flavor:"), QLineEdit::Normal,
                                   QString(), &ok));
    flavor = flavor.toUpper();
    if (!ok || flavor.isEmpty())
        return;

    QHash<QString, QAction *>::const_iterator check = flavorsActions.constFind(flavor);
    if (check != flavorsActions.constEnd()) {
        check.value()->setChecked(true);
        return;
    }

    QAction *action = new QAction(flavor, flavorsMenu);
    flavorsMenu->insertAction(delimiterAction, action);
    flavorsActions.insert(flavor, action);

    action->setCheckable(true);
    action->setChecked(true);

    flavorsButton->disconnect(this);
    flavorsButton->setMenu(flavorsMenu);
    connect(action, SIGNAL(toggled(bool)), this, SLOT(flavorsChanged()));

    flavorsChanged();
}

void EditActionUnitEditor::clearFlavors()
{
    for (QHash<QString, QAction *>::const_iterator flavor = flavorsActions.constBegin(),
            endFlavor = flavorsActions.constEnd(); flavor != endFlavor; ++flavor) {
        flavor.value()->setChecked(false);
    }
}

void EditActionUnitEditor::flavorsChanged()
{
    if (!enableFlavors->isChecked()) {
        flavorsButton->setEnabled(false);
        value.hash("Flavors").remove();
        emit changed();
        return;
    }

    flavorsButton->setEnabled(true);

    Variant::Flags result;
    for (QHash<QString, QAction *>::const_iterator flavor = flavorsActions.constBegin(),
            endFlavor = flavorsActions.constEnd(); flavor != endFlavor; ++flavor) {
        if (!flavor.value()->isChecked())
            continue;
        result.insert(flavor.key().toStdString());
    }
    value.hash("Flavors").setFlags(std::move(result));
    emit changed();
}


EditActionUnitReplaceEditor::EditActionUnitReplaceEditor(Variant::Write v, QWidget *parent)
        : QWidget(parent), value(std::move(v))
{
    QGridLayout *layout = new QGridLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setColumnStretch(0, 0);
    layout->setColumnStretch(1, 1);
    layout->setColumnStretch(2, 0);
    layout->setColumnStretch(3, 1);

    auto from = value.hash("From");
    auto to = value.hash("From");
    QLabel *label;

    enableStation = new QCheckBox(tr("Station:"), this);
    layout->addWidget(enableStation, 0, 0);
    enableStation->setToolTip(tr("Enable alteration of the station."));
    enableStation->setStatusTip(tr("Change station"));
    enableStation->setWhatsThis(
            tr("If this is set then alteration of the station component of the identifier is enabled."));
    enableStation->setChecked(
            from.hash("Station").exists() && !from.hash("Station").toQString().isEmpty());
    connect(enableStation, SIGNAL(toggled(bool)), this, SLOT(stationChanged()));

    fromStation = new QLineEdit(this);
    layout->addWidget(fromStation, 0, 1);
    fromStation->setToolTip(tr("The regular expression applied to the station."));
    fromStation->setStatusTip(tr("Station matcher"));
    fromStation->setWhatsThis(
            tr("This is the regular expression that is matched for replacement in the station."));
    fromStation->setText(from.hash("Station").toQString());
    fromStation->setEnabled(enableStation->isChecked());
    connect(fromStation, SIGNAL(textEdited(
                                        const QString &)), this, SLOT(stationChanged()));

    label = new QLabel(tr("Replacement:"), this);
    layout->addWidget(label, 0, 2);
    toStation = new QLineEdit(this);
    layout->addWidget(toStation, 0, 3);
    toStation->setToolTip(tr("The replacement substituted when the station matches."));
    toStation->setStatusTip(tr("Station replacement"));
    toStation->setWhatsThis(
            tr("This is the replacement substituted when the station pattern matches.  Captured patterns are available as \\1, \\2, etc."));
    toStation->setText(to.hash("Station").toQString());
    toStation->setEnabled(enableStation->isChecked());
    connect(toStation, SIGNAL(textEdited(
                                      const QString &)), this, SLOT(stationChanged()));


    enableArchive = new QCheckBox(tr("Archive:"), this);
    layout->addWidget(enableArchive, 1, 0);
    enableArchive->setToolTip(tr("Enable alteration of the archive."));
    enableArchive->setStatusTip(tr("Change archive"));
    enableArchive->setWhatsThis(
            tr("If this is set then alteration of the archive component of the identifier is enabled."));
    enableArchive->setChecked(
            from.hash("Archive").exists() && !from.hash("Archive").toQString().isEmpty());
    connect(enableArchive, SIGNAL(toggled(bool)), this, SLOT(archiveChanged()));

    fromArchive = new QLineEdit(this);
    layout->addWidget(fromArchive, 1, 1);
    fromArchive->setToolTip(tr("The regular expression applied to the archive."));
    fromArchive->setStatusTip(tr("Archive matcher"));
    fromArchive->setWhatsThis(
            tr("This is the regular expression that is matched for replacement in the archive."));
    fromArchive->setText(from.hash("Archive").toQString());
    fromArchive->setEnabled(enableArchive->isChecked());
    connect(fromArchive, SIGNAL(textEdited(
                                        const QString &)), this, SLOT(archiveChanged()));

    label = new QLabel(tr("Replacement:"), this);
    layout->addWidget(label, 1, 2);
    toArchive = new QLineEdit(this);
    layout->addWidget(toArchive, 1, 3);
    toArchive->setToolTip(tr("The replacement substituted when the archive matches."));
    toArchive->setStatusTip(tr("Archive replacement"));
    toArchive->setWhatsThis(
            tr("This is the replacement substituted when the archive pattern matches.  Captured patterns are available as \\1, \\2, etc."));
    toArchive->setText(to.hash("Archive").toQString());
    toArchive->setEnabled(enableArchive->isChecked());
    connect(toArchive, SIGNAL(textEdited(
                                      const QString &)), this, SLOT(archiveChanged()));


    enableVariable = new QCheckBox(tr("Variable:"), this);
    layout->addWidget(enableVariable, 2, 0);
    enableVariable->setToolTip(tr("Enable alteration of the variable."));
    enableVariable->setStatusTip(tr("Change variable"));
    enableVariable->setWhatsThis(
            tr("If this is set then alteration of the variable component of the identifier is enabled."));
    enableVariable->setChecked(
            from.hash("Variable").exists() && !from.hash("Variable").toQString().isEmpty());
    connect(enableVariable, SIGNAL(toggled(bool)), this, SLOT(variableChanged()));

    fromVariable = new QLineEdit(this);
    layout->addWidget(fromVariable, 2, 1);
    fromVariable->setToolTip(tr("The regular expression applied to the variable."));
    fromVariable->setStatusTip(tr("Variable matcher"));
    fromVariable->setWhatsThis(
            tr("This is the regular expression that is matched for replacement in the variable."));
    fromVariable->setText(from.hash("Variable").toQString());
    fromVariable->setEnabled(enableVariable->isChecked());
    connect(fromVariable, SIGNAL(textEdited(
                                         const QString &)), this, SLOT(variableChanged()));

    label = new QLabel(tr("Replacement:"), this);
    layout->addWidget(label, 2, 2);
    toVariable = new QLineEdit(this);
    layout->addWidget(toVariable, 2, 3);
    toVariable->setToolTip(tr("The replacement substituted when the variable matches."));
    toVariable->setStatusTip(tr("Variable replacement"));
    toVariable->setWhatsThis(
            tr("This is the replacement substituted when the variable pattern matches.  Captured patterns are available as \\1, \\2, etc."));
    toVariable->setText(to.hash("Variable").toQString());
    toVariable->setEnabled(enableVariable->isChecked());
    connect(toVariable, SIGNAL(textEdited(
                                       const QString &)), this, SLOT(variableChanged()));


    enableFlavor = new QCheckBox(tr("Flavor:"), this);
    layout->addWidget(enableFlavor, 3, 0);
    enableFlavor->setToolTip(tr("Enable alteration of the flavors."));
    enableFlavor->setStatusTip(tr("Change flavor"));
    enableFlavor->setWhatsThis(
            tr("If this is set then alteration of the flavors component of the identifier is enabled."));
    enableFlavor->setChecked(
            from.hash("Flavor").exists() && !from.hash("Flavor").toQString().isEmpty());
    connect(enableFlavor, SIGNAL(toggled(bool)), this, SLOT(flavorChanged()));

    fromFlavor = new QLineEdit(this);
    layout->addWidget(fromFlavor, 3, 1);
    fromFlavor->setToolTip(tr("The regular expression applied all flavors."));
    fromFlavor->setStatusTip(tr("Flavor matcher"));
    fromFlavor->setWhatsThis(
            tr("This is the regular expression that is matched for replacement for all flavors the data possess."));
    fromFlavor->setText(from.hash("Flavor").toQString());
    fromFlavor->setEnabled(enableFlavor->isChecked());
    connect(fromFlavor, SIGNAL(textEdited(
                                       const QString &)), this, SLOT(flavorChanged()));

    label = new QLabel(tr("Replacement:"), this);
    layout->addWidget(label, 3, 2);
    toFlavor = new QLineEdit(this);
    layout->addWidget(toFlavor, 3, 3);
    toFlavor->setToolTip(tr("The replacement substituted when a flavor matches."));
    toFlavor->setStatusTip(tr("Flavor replacement"));
    toFlavor->setWhatsThis(
            tr("This is the replacement substituted when the flavor pattern matches.  This is applied to all matching flavors.  Empty flavors are removed.  Captured patterns are available as \\1, \\2, etc."));
    toFlavor->setText(to.hash("Flavor").toQString());
    toFlavor->setEnabled(enableFlavor->isChecked());
    connect(toFlavor, SIGNAL(textEdited(
                                     const QString &)), this, SLOT(flavorChanged()));
}

EditActionUnitReplaceEditor::~EditActionUnitReplaceEditor() = default;

void EditActionUnitReplaceEditor::stationChanged()
{
    if (!enableStation->isChecked()) {
        fromStation->setEnabled(false);
        toStation->setEnabled(false);
        value.hash("From").hash("Station").remove();
        value.hash("To").hash("Station").remove();
        emit changed();
        return;
    }

    fromStation->setEnabled(true);
    toStation->setEnabled(true);

    value.hash("From").hash("Station").setString(fromStation->text());
    value.hash("To").hash("Station").setString(toStation->text());
    emit changed();
}

void EditActionUnitReplaceEditor::archiveChanged()
{
    if (!enableArchive->isChecked()) {
        fromArchive->setEnabled(false);
        toArchive->setEnabled(false);
        value.hash("From").hash("Archive").remove();
        value.hash("To").hash("Archive").remove();
        emit changed();
        return;
    }

    fromArchive->setEnabled(true);
    toArchive->setEnabled(true);

    value.hash("From").hash("Archive").setString(fromArchive->text());
    value.hash("To").hash("Archive").setString(toArchive->text());
    emit changed();
}

void EditActionUnitReplaceEditor::variableChanged()
{
    if (!enableVariable->isChecked()) {
        fromVariable->setEnabled(false);
        toVariable->setEnabled(false);
        value.hash("From").hash("Variable").remove();
        value.hash("To").hash("Variable").remove();
        emit changed();
        return;
    }

    fromVariable->setEnabled(true);
    toVariable->setEnabled(true);

    value.hash("From").hash("Variable").setString(fromVariable->text());
    value.hash("To").hash("Variable").setString(toVariable->text());
    emit changed();
}

void EditActionUnitReplaceEditor::flavorChanged()
{
    if (!enableFlavor->isChecked()) {
        fromFlavor->setEnabled(false);
        toFlavor->setEnabled(false);
        value.hash("From").hash("Flavor").remove();
        value.hash("To").hash("Flavor").remove();
        emit changed();
        return;
    }

    fromFlavor->setEnabled(true);
    toFlavor->setEnabled(true);

    value.hash("From").hash("Flavor").setString(fromFlavor->text());
    value.hash("To").hash("Flavor").setString(toFlavor->text());
    emit changed();
}


EditActionSetSerialEditor::EditActionSetSerialEditor(Variant::Write v, QWidget *parent) : QWidget(
        parent), value(std::move(v))
{
    QFormLayout *layout = new QFormLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    QLineEdit *serial = new QLineEdit(this);
    layout->addRow(tr("&Serial number:"), serial);
    serial->setToolTip(tr("The corrected serial number."));
    serial->setStatusTip(tr("Serial number"));
    serial->setWhatsThis(
            tr("This is the serial number that the selected variable's metadata will be changed to."));
    serial->setText(value.hash("SerialNumber").toQString());
    connect(serial, SIGNAL(textEdited(
                                   const QString &)), this, SLOT(setSerial(
                                                                         const QString &)));
}

EditActionSetSerialEditor::~EditActionSetSerialEditor() = default;

void EditActionSetSerialEditor::setSerial(const QString &serial)
{
    value.hash("SerialNumber").setString(serial);
    emit changed();
}

EditActionOverlayValueEditor::EditActionOverlayValueEditor(Variant::Write v, QWidget *parent)
        : QWidget(parent), value(std::move(v))
{
    QGridLayout *layout = new QGridLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setColumnStretch(0, 0);
    layout->setColumnStretch(1, 0);
    layout->setColumnStretch(2, 1);
    layout->setRowStretch(0, 0);
    layout->setRowStretch(1, 1);

    QLabel *label = new QLabel(tr("Path:"), this);
    layout->addWidget(label, 0, 0, Qt::AlignRight);
    QLineEdit *path = new QLineEdit(this);
    layout->addWidget(path, 0, 1);
    label->setBuddy(path);
    path->setToolTip(tr("The path within the data that the overlay is applied at."));
    path->setStatusTip(tr("Data path"));
    path->setWhatsThis(
            tr("This is path within the data that the overlay is applied at.  That is, the value defined below is set after looking up this path within the original data."));
    path->setText(value.hash("Path").toQString());
    connect(path, SIGNAL(textEdited(
                                 const QString &)), this, SLOT(setPath(
                                                                       const QString &)));

    editingValue = value.hash("Value");
    editingValue.detachFromRoot();
    editor = new ValueEditor(this);
    editor->setValue(editingValue);
    layout->addWidget(editor, 1, 0, 1, -1);

    connect(editor, SIGNAL(valueChanged()), this, SLOT(valueChanged()));
}

EditActionOverlayValueEditor::~EditActionOverlayValueEditor() = default;

void EditActionOverlayValueEditor::setPath(const QString &path)
{
    value.hash("Path").setString(path);
    emit changed();
}

void EditActionOverlayValueEditor::valueChanged()
{
    value.hash("Value").set(editingValue);
    emit changed();
}

EditActionFunctionEditor::EditActionFunctionEditor(Variant::Write v, QWidget *parent) : QWidget(
        parent), value(std::move(v))
{
    QFormLayout *layout = new QFormLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    operation = new QComboBox(this);
    layout->addRow(tr("Type:"), operation);
    operation->setToolTip(tr("The arithmetic operation to perform on the data."));
    operation->setStatusTip(tr("Arithmetic operation"));
    operation->setWhatsThis(
            tr("This is the arithmetic operation that will be performed on the input, using the parameter below."));

    const auto &current = value.hash("Function").toString();

    int index = operation->count();
    operation->addItem(tr("Add"), "Add");
    operation->setItemData(index, tr("Add the input to the value."), Qt::ToolTipRole);
    operation->setItemData(index, tr("Addition"), Qt::StatusTipRole);
    operation->setItemData(index,
                           tr("This operation adds the below input to the data value to produce the final output value."),
                           Qt::WhatsThisRole);
    operation->setCurrentIndex(index);
    if (Util::equal_insensitive(current, "Add", "Plus"))
        operation->setCurrentIndex(index);

    index = operation->count();
    operation->addItem(tr("Subtract"), "Subtract");
    operation->setItemData(index, tr("Subtract the input from the value."), Qt::ToolTipRole);
    operation->setItemData(index, tr("Subtraction"), Qt::StatusTipRole);
    operation->setItemData(index,
                           tr("This operation subtracts the below input from the data value to produce the final output value."),
                           Qt::WhatsThisRole);
    if (Util::equal_insensitive(current, "Subtract", "Minus"))
        operation->setCurrentIndex(index);

    index = operation->count();
    operation->addItem(tr("Multiply"), "Multiply");
    operation->setItemData(index, tr("Multiply the data value with the input."), Qt::ToolTipRole);
    operation->setItemData(index, tr("Multiplication"), Qt::StatusTipRole);
    operation->setItemData(index,
                           tr("This operation multiplies the below input with the data value to produce the final output value."),
                           Qt::WhatsThisRole);
    if (Util::equal_insensitive(current, "Multiply", "Times"))
        operation->setCurrentIndex(index);

    index = operation->count();
    operation->addItem(tr("Divide"), "Divide");
    operation->setItemData(index, tr("Divide the data value by the input."), Qt::ToolTipRole);
    operation->setItemData(index, tr("Division"), Qt::StatusTipRole);
    operation->setItemData(index,
                           tr("This operation divides the data value by the below input to produce the final output value."),
                           Qt::WhatsThisRole);
    if (Util::equal_insensitive(current, "Divide"))
        operation->setCurrentIndex(index);

    index = operation->count();
    operation->addItem(tr("Invert"), "Invert");
    operation->setItemData(index, tr("Divide the input value by the data value."), Qt::ToolTipRole);
    operation->setItemData(index, tr("Inversion"), Qt::StatusTipRole);
    operation->setItemData(index,
                           tr("This operation divides the below input by the data value to produce the final output.  This effectively inverts the data values while multiplying them by the input values."),
                           Qt::WhatsThisRole);
    if (Util::equal_insensitive(current, "Invert", "Inverse"))
        operation->setCurrentIndex(index);

    index = operation->count();
    operation->addItem(tr("Assign"), "Assign");
    operation->setItemData(index, tr("Set the input to the data value."), Qt::ToolTipRole);
    operation->setItemData(index, tr("Replacement"), Qt::StatusTipRole);
    operation->setItemData(index,
                           tr("This operation sets the below value to the data value.  That is, it ignores the current input value and replaces it with the data value entirely."),
                           Qt::WhatsThisRole);
    if (Util::equal_insensitive(current, "Assign", "Set", "Equal"))
        operation->setCurrentIndex(index);

    connect(operation, SIGNAL(currentIndexChanged(int)), this, SLOT(setOperation(int)));

    QLineEdit *path = new QLineEdit(this);
    layout->addRow(tr("Path:"), path);
    path->setToolTip(tr("The path the input value is fetched from."));
    path->setStatusTip(tr("Input path"));
    path->setWhatsThis(
            tr("This is path within the input data that the input value is fetched from.  That is, this is the location that the input value is converted at."));
    path->setText(value.hash("Path").toQString());
    connect(path, SIGNAL(textEdited(
                                 const QString &)), this, SLOT(setPath(
                                                                       const QString &)));
}

EditActionFunctionEditor::~EditActionFunctionEditor() = default;

void EditActionFunctionEditor::setPath(const QString &path)
{
    value.hash("Path").setString(path);
    emit changed();
}

void EditActionFunctionEditor::setOperation(int index)
{
    value.hash("Function").setString(operation->itemData(index).toString());
    emit changed();
}

EditActionScriptEditor::EditActionScriptEditor(Variant::Write v, QWidget *parent) : QWidget(parent),
                                                                                    value(std::move(
                                                                                            v))
{
    QHBoxLayout *layout = new QHBoxLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    ScriptEdit *script = new ScriptEdit(this);
    layout->addWidget(script);
    script->setToolTip(tr("The script code evaluated by the action."));
    script->setStatusTip(tr("Script code"));
    script->setWhatsThis(tr("This is the CPD3 script code evaluated by the action."));
    script->setCode(value.hash("Code").toQString());
    connect(script, SIGNAL(scriptChanged(
                                   const QString &)), this, SLOT(setCode(
                                                                         const QString &)));
}

EditActionScriptEditor::~EditActionScriptEditor() = default;

void EditActionScriptEditor::setCode(const QString &code)
{
    value.hash("Code").setString(code);
    emit changed();
}


EditActionAbnormalDataEpisodeEditor::EditActionAbnormalDataEpisodeEditor(Variant::Write value,
                                                                         QWidget *parent) : QWidget(
        parent)
{
    QVBoxLayout *topLayout = new QVBoxLayout;
    setLayout(topLayout);
    topLayout->setContentsMargins(0, 0, 0, 0);


    {
        EditActionEnumEditor *episodeType =
                new EditActionEnumEditor(value, "EpisodeType", tr("Episode Type:"),
                                         tr("Select the episode type the edit represents."),
                                         tr("Episode Type"),
                                         tr("This sets the type of episode the edit represents.  This may or may not be used to determine the effect of the edit."),
                                         this);
        topLayout->addWidget(episodeType);
        connect(episodeType, SIGNAL(changed()), this, SIGNAL(changed()));
        episodeType->addItem("WildFire", QStringList(), tr("Wild Fire"),
                             tr("A wild fire or wild fire smoke event."), tr("Wild Fire"),
                             tr("Mark data as representing a wild fire or smoke event."));
        episodeType->addItem("Dust", QStringList(), tr("Dust"), tr("A dust transport event."),
                             tr("Dust"), tr("Mark data as representing a dusk transport event."));
    }

    topLayout->addStretch(1);
}

EditActionAbnormalDataEpisodeEditor::~EditActionAbnormalDataEpisodeEditor() = default;

}
}
}
}

