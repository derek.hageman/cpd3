/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QDialog>
#include <QDialogButtonBox>
#include <QVBoxLayout>

#include "guidata/editors/dynamicinput.hxx"
#include "datacore/sequencematch.hxx"
#include "guidata/variableselect.hxx"
#include "datacore/variant/composite.hxx"
#include "core/util.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace GUI {
namespace Data {

/** @file guidata/editors/filterinput.hxx
 * Value editors for filter input definitions.
 */


bool DynamicInputEndpoint::matches(const Variant::Read &value, const Variant::Read &metadata)
{
    return Util::equal_insensitive(metadata.metadata("Editor").hash("Type").toString(),
                                   "DynamicInput");
}

bool DynamicInputEndpoint::edit(Variant::Write &value,
                                const Variant::Read &metadata,
                                QWidget *parent)
{
    DynamicInputEditorDialog dialog(value, metadata, parent);
    if (dialog.exec() != QDialog::Accepted)
        return false;
    value.setEmpty();
    value.set(dialog.input());
    return true;
}

DynamicInputEditorDialog::DynamicInputEditorDialog(const Variant::Read &config,
                                                   const Variant::Read &metadata,
                                                   QWidget *parent)
{
    QVBoxLayout *topLevel = new QVBoxLayout(this);
    setLayout(topLevel);

    typeSelect = new QTabWidget(this);
    topLevel->addWidget(typeSelect);

    {
        QWidget *display = new QWidget(this);
        typeSelect->addTab(display, tr("Data Input"));
        QVBoxLayout *layout = new QVBoxLayout(display);
        display->setLayout(layout);

        QWidget *container;
        QHBoxLayout *line;
        QLabel *label;


        dynamicSelection = new VariableSelect(display);
        layout->addWidget(dynamicSelection, 1);
        if (config.getType() == Variant::Type::String)
            dynamicSelection->configureFromSequenceMatch(config);
        else
            dynamicSelection->configureFromSequenceMatch(config["Input"]);
        dynamicSelection->setDefaultAvailable();

        dynamicCalibration = new CalibrationEdit(display);
        layout->addWidget(dynamicCalibration);
        dynamicCalibration->setCalibration(
                Variant::Composite::toCalibration(config["Calibration"]));


        container = new QWidget(display);
        layout->addWidget(container);
        line = new QHBoxLayout(container);
        container->setLayout(line);
        line->setContentsMargins(0, 0, 0, 0);

        label = new QLabel(tr("&Path:"), container);
        line->addWidget(label);

        dynamicPath = new QLineEdit(container);
        line->addWidget(dynamicPath, 1);
        label->setBuddy(dynamicPath);
        dynamicPath->setToolTip(tr("The path in the input values to look up values from."));
        dynamicPath->setText(config["Path"].toQString());


        container = new QWidget(display);
        layout->addWidget(container);
        line = new QHBoxLayout(container);
        container->setLayout(line);
        line->setContentsMargins(0, 0, 0, 0);

        label = new QLabel(tr("&Default:"), container);
        line->addWidget(label);

        double v = config["Default"].toDouble();
        dynamicDefault = new QLineEdit(container);
        line->addWidget(dynamicDefault, 1);
        label->setBuddy(dynamicDefault);
        dynamicDefault->setToolTip(tr("The constant value of the input."));
        if (FP::defined(v))
            dynamicDefault->setText(QString::number(v));
    }

    {
        QWidget *display = new QWidget(this);
        typeSelect->addTab(display, tr("Constant Value"));
        double v = FP::undefined();
        if (config.getType() == Variant::Type::Real)
            v = config.toDouble();
        else
            v = config["Constant"].toDouble();
        if (FP::defined(v))
            typeSelect->setCurrentIndex(typeSelect->count() - 1);
        QVBoxLayout *layout = new QVBoxLayout(display);
        display->setLayout(layout);

        constantEditor = new QLineEdit(display);
        layout->addWidget(constantEditor);
        constantEditor->setToolTip(tr("The constant value of the input."));
        if (FP::defined(v))
            constantEditor->setText(QString::number(v));
        connect(constantEditor, SIGNAL(textEdited(
                                               const QString &)), this,
                SLOT(constantValueChanged()));
        constantValueChanged();

        layout->addStretch(1);
    }

    QDialogButtonBox *buttons =
            new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal,
                                 this);
    topLevel->addWidget(buttons);
    connect(buttons, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttons, SIGNAL(rejected()), this, SLOT(reject()));
}

void DynamicInputEditorDialog::constantValueChanged()
{
    QString text(constantEditor->text());
    if (text.isEmpty()) {
        constantEditor->setStyleSheet("QLineEdit {background-color: red;}");
        return;
    }
    bool ok = false;
    double v = text.toDouble(&ok);
    if (!ok || !FP::defined(v)) {
        constantEditor->setStyleSheet("QLineEdit {background-color: red;}");
        return;
    }
    constantEditor->setStyleSheet(QString());
}

Variant::Root DynamicInputEditorDialog::input() const
{
    Variant::Root result;

    if (typeSelect->currentIndex() == 1) {
        bool ok = false;
        double v = constantEditor->text().toDouble(&ok);
        if (!ok || !FP::defined(v))
            return result;
        result["Constant"].setDouble(v);
        return result;
    }

    dynamicSelection->writeSequenceMatch(result["Input"]);
    {
        Calibration c(dynamicCalibration->getCalibration());
        if (!c.isIdentity())
            Variant::Composite::fromCalibration(result["Calibration"], c);
    }
    {
        QString path(dynamicPath->text().trimmed());
        if (!path.isEmpty())
            result["Path"].setString(path);
    }
    {
        bool ok = false;
        double v = dynamicDefault->text().toDouble(&ok);
        if (ok && FP::defined(v))
            result["Default"].setDouble(v);
    }

    return result;
}


}
}
}

