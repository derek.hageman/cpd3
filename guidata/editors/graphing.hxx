/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3GUIDATA_GRAPHING_HXX
#define CPD3GUIDATA_GRAPHING_HXX

#include "core/first.hxx"

#include "guidata/guidata.hxx"
#include "guidata/valueeditor.hxx"

namespace CPD3 {
namespace GUI {
namespace Data {

/**
 * An editor endpoint for graphing axis transformers.
 */
class CPD3GUIDATA_EXPORT GraphingAxisTransformerBoundEndpoint : public ValueEndpointEditor {
public:

    bool matches(const CPD3::Data::Variant::Read &value,
                 const CPD3::Data::Variant::Read &metadata) override;

    QString collapsedText(const CPD3::Data::Variant::Read &value,
                          const CPD3::Data::Variant::Read &metadata) override;

    bool edit(CPD3::Data::Variant::Write &value,
              const CPD3::Data::Variant::Read &metadata,
              QWidget *parent = 0) override;
};

/**
 * An editor endpoint for graph color specifications.
 */
class CPD3GUIDATA_EXPORT GraphingColorEndpoint : public ValueEndpointEditor {
public:

    bool matches(const CPD3::Data::Variant::Read &value,
                 const CPD3::Data::Variant::Read &metadata) override;

    QString collapsedText(const CPD3::Data::Variant::Read &value,
                          const CPD3::Data::Variant::Read &metadata) override;

    bool edit(CPD3::Data::Variant::Write &value,
              const CPD3::Data::Variant::Read &metadata,
              QWidget *parent = 0) override;
};

/**
 * An editor endpoint for graph font specifications.
 */
class CPD3GUIDATA_EXPORT GraphingFontEndpoint : public ValueEndpointEditor {
public:

    bool matches(const CPD3::Data::Variant::Read &value, const CPD3::Data::Variant::Read &metadata) override;

    QString collapsedText(const CPD3::Data::Variant::Read &value, const CPD3::Data::Variant::Read &metadata) override;

    bool edit(CPD3::Data::Variant::Write &value, const CPD3::Data::Variant::Read &metadata, QWidget *parent = 0) override;
};

}
}
}

#endif //CPD3GUIDATA_GRAPHING_HXX
