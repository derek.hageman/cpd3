/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QVBoxLayout>
#include <QDialog>
#include <QDialogButtonBox>
#include <QVBoxLayout>
#include <QHBoxLayout>

#include "guidata/editors/baselinesmoother.hxx"
#include "guicore/dynamictimeinterval.hxx"
#include "core/timeutils.hxx"
#include "core/util.hxx"
#include "smoothing/digitalfilter.hxx"
#include "datacore/dynamictimeinterval.hxx"

using namespace CPD3::Data;
using namespace CPD3::Smoothing;

namespace CPD3 {
namespace GUI {
namespace Data {

/** @file guidata/editors/baselinesmoother.hxx
 * Value editors for baseline smoother definitions.
 */


bool BaselineSmootherEndpoint::matches(const Variant::Read &value, const Variant::Read &metadata)
{
    return Util::equal_insensitive(metadata.metadata("Editor").hash("Type").toString(),
                                   "BaselineSmoother");
}

bool BaselineSmootherEndpoint::edit(Variant::Write &value,
                                    const Variant::Read &metadata,
                                    QWidget *parent)
{
    QDialog dialog(parent);
    dialog.setWindowTitle(QObject::tr("Smoother"));
    auto layout = new QVBoxLayout(&dialog);
    dialog.setLayout(layout);

    auto editor = new BaselineSmootherEditor(value, metadata, &dialog);
    layout->addWidget(editor, 1);

    auto buttons =
            new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal,
                                 &dialog);
    layout->addWidget(buttons);
    QObject::connect(buttons, &QDialogButtonBox::accepted, &dialog, &QDialog::accept);
    QObject::connect(buttons, &QDialogButtonBox::rejected, &dialog, &QDialog::reject);

    if (dialog.exec() != QDialog::Accepted)
        return false;

    value.setEmpty();
    value.set(editor->smoother());
    return true;
}


BaselineSmootherEditor::BaselineSmootherEditor(const Variant::Read &config,
                                               const Variant::Read &metadata,
                                               QWidget *parent)
{
    auto editor = metadata.metadata("Editor");

    QVBoxLayout *topLevel = new QVBoxLayout(this);
    topLevel->setContentsMargins(0, 0, 0, 0);
    setLayout(topLevel);

    type = new QComboBox(this);
    topLevel->addWidget(type, 0);

    const auto &currentType = config["Type"].toString();

    type->addItem(tr("Fixed Time Boxcar"), "FixedTime");
    {
        QWidget *display = new QWidget(this);
        typeDisplays.append(display);
        topLevel->addWidget(display);
        display->hide();
        QVBoxLayout *layout = new QVBoxLayout(display);
        display->setLayout(layout);
        layout->setContentsMargins(0, 0, 0, 0);

        QLabel *text =
                new QLabel(tr("A smoother conisting of the moving boxcar mean of incoming values.  "
                              "The smoother considers a maximum number of seconds into the past and discards all undefined values."),
                           display);
        layout->addWidget(text);
        text->setWordWrap(true);

        double maximumTime = config["Time"].toDouble();
        if (!FP::defined(maximumTime) || maximumTime <= 0.0)
            maximumTime = editor["DefaultSmoother/Time"].toDouble();
        if (!FP::defined(maximumTime) || maximumTime <= 0.0)
            maximumTime = FP::undefined();
        double requireTime = config["MinimumTime"].toDouble();
        if (!FP::defined(requireTime) || requireTime <= 0.0)
            requireTime = editor["DefaultSmoother/MinimumTime"].toDouble();
        if (!FP::defined(requireTime) || requireTime <= 0.0)
            requireTime = FP::undefined();
        if (!FP::defined(maximumTime) || maximumTime <= 0.0) {
            if (!FP::defined(requireTime) || requireTime <= 0.0)
                maximumTime = 60.0;
            else
                maximumTime = requireTime;
        }
        if (!FP::defined(requireTime) || requireTime <= 0.0)
            requireTime = FP::undefined();


        {
            QWidget *container = new QWidget(display);
            layout->addWidget(container);
            QHBoxLayout *line = new QHBoxLayout(container);
            line->setContentsMargins(0, 0, 0, 0);

            QLabel *label = new QLabel(tr("Maximum &Time:"), container);
            line->addWidget(label);

            fixedTimeTotalSeconds = new QLineEdit(container);
            line->addWidget(fixedTimeTotalSeconds);
            label->setBuddy(fixedTimeTotalSeconds);
            fixedTimeTotalSeconds->setToolTip(tr("The maximum number of seconds to average"));
            fixedTimeTotalSeconds->setText(QString::number(maximumTime));
            QObject::connect(fixedTimeTotalSeconds, &QLineEdit::textChanged, this,
                             &BaselineSmootherEditor::changed);
        }
        {
            QWidget *container = new QWidget(display);
            layout->addWidget(container);
            QHBoxLayout *line = new QHBoxLayout(container);
            line->setContentsMargins(0, 0, 0, 0);

            QLabel *label = new QLabel(tr("&Minimum Time:"), container);
            line->addWidget(label);

            fixedTimeMinimumSeconds = new QLineEdit(container);
            line->addWidget(fixedTimeMinimumSeconds);
            label->setBuddy(fixedTimeMinimumSeconds);
            fixedTimeMinimumSeconds->setToolTip(
                    tr("The minimum number of seconds before the smoother is ready"));
            if (FP::defined(requireTime))
                fixedTimeMinimumSeconds->setText(QString::number(requireTime));
            fixedTimeMinimumSeconds->setPlaceholderText(tr("Maximum Time"));
            QObject::connect(fixedTimeMinimumSeconds, &QLineEdit::textChanged, this,
                             &BaselineSmootherEditor::changed);
        }

        {
            double discardTime = config["DiscardTime"].toDouble();
            if (!FP::defined(discardTime) || discardTime <= 0.0)
                discardTime = editor["DefaultSmoother/DiscardTime"].toDouble();
            if (!FP::defined(discardTime) || discardTime <= 0.0)
                discardTime = FP::undefined();

            QWidget *container = new QWidget(display);
            layout->addWidget(container);
            QHBoxLayout *line = new QHBoxLayout(container);
            line->setContentsMargins(0, 0, 0, 0);

            QLabel *label = new QLabel(tr("&Discard Time:"), container);
            line->addWidget(label);

            fixedTimeDiscard = new QLineEdit(container);
            line->addWidget(fixedTimeDiscard);
            label->setBuddy(fixedTimeDiscard);
            fixedTimeDiscard->setToolTip(
                    tr("The number of seconds after a reset to discard before adding any data to the average"));
            if (FP::defined(discardTime))
                fixedTimeDiscard->setText(QString::number(discardTime));
            fixedTimeDiscard->setPlaceholderText(tr("None"));
            QObject::connect(fixedTimeDiscard, &QLineEdit::textChanged, this,
                             &BaselineSmootherEditor::changed);
        }

        fixedTimeRSD = NULL;
        if (editor["EnableStability"].toBool()) {
            QWidget *container = new QWidget(display);
            layout->addWidget(container);
            QHBoxLayout *line = new QHBoxLayout(container);
            line->setContentsMargins(0, 0, 0, 0);

            QLabel *label = new QLabel(tr("&Stable RSD:"), container);
            line->addWidget(label);

            fixedTimeRSD = new QLineEdit(container);
            line->addWidget(fixedTimeRSD);
            label->setBuddy(fixedTimeRSD);
            fixedTimeRSD->setToolTip(
                    tr("The maximum relative standard deviation (SD/mean) that the smoother considers stable"));

            double value = FP::undefined();
            if (config["RSD"].getType() == Variant::Type::Real)
                value = config["RSD"].toDouble();
            else
                value = editor["DefaultSmoother/RSD"].toDouble();
            if (FP::defined(value) && value > 0.0)
                fixedTimeRSD->setText(QString::number(value));
            fixedTimeRSD->setPlaceholderText(tr("Always Stable"));
            QObject::connect(fixedTimeRSD, &QLineEdit::textChanged, this,
                             &BaselineSmootherEditor::changed);
        }

        fixedTimeBand = NULL;
        if (editor["EnableSpike"].toBool()) {
            QWidget *container = new QWidget(display);
            layout->addWidget(container);
            QHBoxLayout *line = new QHBoxLayout(container);
            line->setContentsMargins(0, 0, 0, 0);

            QLabel *label = new QLabel(tr("Spike &Band:"), container);
            line->addWidget(label);

            fixedTimeBand = new QLineEdit(container);
            line->addWidget(fixedTimeBand);
            label->setBuddy(fixedTimeBand);
            fixedTimeBand->setToolTip(
                    tr("The maximum band as a fraction of the smoothed value that new points must be within to not be spikes"));

            double value = FP::undefined();
            if (config["Band"].getType() == Variant::Type::Real)
                value = config["Band"].toDouble();
            else
                value = editor["DefaultSmoother/Band"].toDouble();
            if (FP::defined(value) && value != 0.0)
                fixedTimeBand->setText(QString::number(value));
            fixedTimeBand->setPlaceholderText(tr("Disabled"));
            QObject::connect(fixedTimeBand, &QLineEdit::textChanged, this,
                             &BaselineSmootherEditor::changed);
        }

        layout->addStretch(1);
    }


    type->addItem(tr("Single Pole Low Pass Digital Filter"), "SinglePole");
    if (Util::equal_insensitive(currentType, "singlepole", "singlepolelowpass", "1p", "1plp"))
        type->setCurrentIndex(type->count() - 1);
    type->addItem(tr("Four Pole Low Pass Digital Filter"), "FourPole");
    if (Util::equal_insensitive(currentType, "fourpole", "fourpolelowpass", "4p", "4plp"))
        type->setCurrentIndex(type->count() - 1);
    {
        QWidget *display = new QWidget(this);
        typeDisplays.append(display);
        typeDisplays.append(display);
        topLevel->addWidget(display);
        display->hide();
        QVBoxLayout *layout = new QVBoxLayout(display);
        display->setLayout(layout);
        layout->setContentsMargins(0, 0, 0, 0);

        QLabel *text = new QLabel(tr("This is a digital filter based smoother.  "
                                     "The filter consists of an infinite response feedback equation considering all prior values based on their smoothed outputs.  "
                                     "The smoother itself is defined in terms of a time constant, which is itself treated like an R-C time constant.  "
                                     "This can be thought of as the time it would take for the smoother to reach 63.2 percent of the final value after a step change in the input."),
                                  display);
        layout->addWidget(text);
        text->setWordWrap(true);

        {
            QWidget *container = new QWidget(display);
            layout->addWidget(container);
            QHBoxLayout *line = new QHBoxLayout(container);
            line->setContentsMargins(0, 0, 0, 0);

            QLabel *label = new QLabel(tr("&Time Constant:"), container);
            line->addWidget(label);

            Variant::Write selectionEditor = Variant::Write::empty();
            digitalFilterTimeConstant =
                    new DynamicTimeIntervalEditor(config["TimeConstant"], metadata["TimeConstant"],
                                                  editor["DefaultSmoother/TimeConstant"],
                                                  selectionEditor, display);
            line->addWidget(digitalFilterTimeConstant);
            label->setBuddy(digitalFilterTimeConstant);
            digitalFilterTimeConstant->setEnabledText(QString());
            digitalFilterTimeConstant->setToolTip(tr("The RC time constant of the digital filter"));
            QObject::connect(digitalFilterTimeConstant, &DynamicTimeIntervalEditor::changed, this,
                             &BaselineSmootherEditor::changed);
        }

        {
            QWidget *container = new QWidget(display);
            layout->addWidget(container);
            QHBoxLayout *line = new QHBoxLayout(container);
            line->setContentsMargins(0, 0, 0, 0);

            QLabel *label = new QLabel(tr("Maximum &Gap before reset:"), container);
            line->addWidget(label);

            Variant::Write selectionEditor = Variant::Write::empty();
            selectionEditor["AllowUndefined"] = true;
            digitalFilterGap = new DynamicTimeIntervalEditor(config["Gap"], metadata["Gap"],
                                                             editor["DefaultSmoother/Gap"],
                                                             selectionEditor, display);
            line->addWidget(digitalFilterGap);
            label->setBuddy(digitalFilterGap);
            digitalFilterGap->setEnabledText(QString());
            digitalFilterGap->setToolTip(
                    tr("The maximum time between points before the filter is reset"));
            QObject::connect(digitalFilterGap, &DynamicTimeIntervalEditor::changed, this,
                             &BaselineSmootherEditor::changed);
        }

        digitalFilterResetOnUndefined = new QCheckBox(tr("Reset on &undefined values"), display);
        layout->addWidget(digitalFilterResetOnUndefined);
        digitalFilterResetOnUndefined->setToolTip(
                tr("Reset the smoother when any undefined value is received instead of ignoring the undefined value"));
        if (config["ResetOnUndefined"].exists())
            digitalFilterResetOnUndefined->setChecked(config["ResetOnUndefined"].toBool());
        else if (editor["DefaultSmoother/ResetOnUndefined"].toBool())
            digitalFilterResetOnUndefined->setChecked(
                    editor["DefaultSmoother/ResetOnUndefined"].toBool());
        else
            digitalFilterResetOnUndefined->setChecked(false);


        digitalFilterStableBand = NULL;
        if (editor["EnableStability"].toBool()) {
            QWidget *container = new QWidget(display);
            layout->addWidget(container);
            QHBoxLayout *line = new QHBoxLayout(container);
            line->setContentsMargins(0, 0, 0, 0);

            QLabel *label = new QLabel(tr("&Stable Band:"), container);
            line->addWidget(label);

            digitalFilterStableBand = new QLineEdit(container);
            line->addWidget(digitalFilterStableBand);
            label->setBuddy(digitalFilterStableBand);
            digitalFilterStableBand->setToolTip(
                    tr("The maximum band as a fraction of the smoothed value that new values must be within to be stable"));

            double value = FP::undefined();
            if (config["StableBand"].getType() == Variant::Type::Real)
                value = config["StableBand"].toDouble();
            else
                value = editor["DefaultSmoother/StableBand"].toDouble();
            if (FP::defined(value))
                digitalFilterStableBand->setText(QString::number(value));
            digitalFilterStableBand->setPlaceholderText(tr("Always Stable"));
            QObject::connect(digitalFilterStableBand, &QLineEdit::textChanged, this,
                             &BaselineSmootherEditor::changed);
        }

        digitalFilterSpikeBand = NULL;
        if (editor["EnableSpike"].toBool()) {
            QWidget *container = new QWidget(display);
            layout->addWidget(container);
            QHBoxLayout *line = new QHBoxLayout(container);
            line->setContentsMargins(0, 0, 0, 0);

            QLabel *label = new QLabel(tr("S&pike Band:"), container);
            line->addWidget(label);

            digitalFilterSpikeBand = new QLineEdit(container);
            line->addWidget(digitalFilterSpikeBand);
            label->setBuddy(digitalFilterSpikeBand);
            digitalFilterSpikeBand->setToolTip(
                    tr("The maximum band as a fraction of the smoothed value that new points must be within to not be spikes"));

            double value = FP::undefined();
            if (config["SpikeBand"].getType() == Variant::Type::Real)
                value = config["SpikeBand"].toDouble();
            else
                value = editor["DefaultSmoother/SpikeBand"].toDouble();
            if (FP::defined(value))
                digitalFilterSpikeBand->setText(QString::number(value));
            digitalFilterSpikeBand->setPlaceholderText(tr("Disabled"));
            QObject::connect(digitalFilterSpikeBand, &QLineEdit::textChanged, this,
                             &BaselineSmootherEditor::changed);
        }

        layout->addStretch(1);
    }


    type->addItem(tr("Total Running Mean"), "Forever");
    if (Util::equal_insensitive(currentType, "forever", "always"))
        type->setCurrentIndex(type->count() - 1);
    {
        QWidget *display = new QWidget(this);
        typeDisplays.append(display);
        topLevel->addWidget(display);
        display->hide();
        QVBoxLayout *layout = new QVBoxLayout(display);
        display->setLayout(layout);
        layout->setContentsMargins(0, 0, 0, 0);

        QLabel *text = new QLabel(
                tr("A smoother consisting of the mean of all valid values ever received.  "
                   "The smoother is considered stable and ready as soon as it receives a single defined value.  "
                   "It does not have any form of spike detection.  "
                   "Any undefined values are discarded."), display);
        layout->addWidget(text);
        text->setWordWrap(true);

        layout->addStretch(1);
    }


    type->addItem(tr("Single Point"), "SinglePoint");
    if (Util::equal_insensitive(currentType, "singlepoint"))
        type->setCurrentIndex(type->count() - 1);
    {
        QWidget *display = new QWidget(this);
        typeDisplays.append(display);
        topLevel->addWidget(display);
        display->hide();
        QVBoxLayout *layout = new QVBoxLayout(display);
        display->setLayout(layout);
        layout->setContentsMargins(0, 0, 0, 0);

        QLabel *text = new QLabel(tr("A smoother consisting of the last defined value.  "
                                     "The smoother is considered stable and ready as soon as it receives a single defined value.  "
                                     "It does not have any form of spike detection.  "
                                     "Any undefined values are discarded."), display);
        layout->addWidget(text);
        text->setWordWrap(true);

        layout->addStretch(1);
    }

    type->addItem(tr("Latest Value"), "Latest");
    if (Util::equal_insensitive(currentType, "latest"))
        type->setCurrentIndex(type->count() - 1);
    {
        QWidget *display = new QWidget(this);
        typeDisplays.append(display);
        topLevel->addWidget(display);
        display->hide();
        QVBoxLayout *layout = new QVBoxLayout(display);
        display->setLayout(layout);
        layout->setContentsMargins(0, 0, 0, 0);

        QLabel *text = new QLabel(tr("A smoother consisting of the last received value.  "
                                     "The smoother is considered stable and ready if the latest received value is defined.  "
                                     "It does not have any form of spike detection."), display);
        layout->addWidget(text);
        text->setWordWrap(true);

        layout->addStretch(1);
    }

    type->addItem(tr("Disabled"), "Disable");
    if (Util::equal_insensitive(currentType, "disable"))
        type->setCurrentIndex(type->count() - 1);
    {
        QWidget *display = new QWidget(this);
        typeDisplays.append(display);
        topLevel->addWidget(display);
        display->hide();
        QVBoxLayout *layout = new QVBoxLayout(display);
        display->setLayout(layout);
        layout->setContentsMargins(0, 0, 0, 0);

        QLabel *text = new QLabel(tr("The smoother is disabled.  "
                                     "It always returns unstable and not ready and discards all values.  "
                                     "The result is an always undefined output."), display);
        layout->addWidget(text);
        text->setWordWrap(true);

        layout->addStretch(1);
    }

    connect(type, SIGNAL(currentIndexChanged(int)), this, SLOT(updateDisplay()));
    updateDisplay();
}

void BaselineSmootherEditor::updateDisplay()
{
    int index = type->currentIndex();
    if (index < 0 || index >= typeDisplays.size())
        return;

    for (int i = 0, max = typeDisplays.size(); i < max; i++) {
        if (i == index)
            continue;
        typeDisplays.at(i)->hide();
    }
    typeDisplays.at(index)->show();
    updateGeometry();
}

static void setEditorValue(const QLineEdit *editor, Variant::Write &&target)
{
    if (!editor)
        return;
    QString text(editor->text());
    double value = FP::undefined();
    if (!text.isEmpty()) {
        bool ok = false;
        value = text.toDouble(&ok);
        if (!ok || !FP::defined(value))
            value = FP::undefined();
    }
    target.setDouble(value);
}

Variant::Root BaselineSmootherEditor::smoother() const
{
    Variant::Root result;
    int index = type->currentIndex();
    if (index < 0 || index >= type->count())
        return result;

    QString currentType(type->itemData(index).toString());
    result["Type"].setString(currentType);
    currentType = currentType.toLower();

    if (currentType == "singlepole" ||
            currentType == "singlepolelowpass" ||
            currentType == "1p" ||
            currentType == "1plp" ||
            currentType == "fourpole" ||
            currentType == "fourpolelowpass" ||
            currentType == "4p" ||
            currentType == "4plp") {
        result["TimeConstant"].set(digitalFilterTimeConstant->timeInterval());
        result["Gap"].set(digitalFilterGap->timeInterval());
        result["ResetOnUndefined"].setBool(digitalFilterResetOnUndefined->isChecked());
        setEditorValue(digitalFilterStableBand, result["StableBand"]);
        setEditorValue(digitalFilterSpikeBand, result["SpikeBand"]);
    } else if (currentType == "singlepoint" ||
            currentType == "latest" ||
            currentType == "disable" ||
            currentType == "forever" ||
            currentType == "always") {
    } else {
        setEditorValue(fixedTimeTotalSeconds, result["Time"]);
        setEditorValue(fixedTimeMinimumSeconds, result["MinimumTime"]);
        setEditorValue(fixedTimeDiscard, result["DiscardTime"]);
        setEditorValue(fixedTimeRSD, result["RSD"]);
        setEditorValue(fixedTimeBand, result["Band"]);
    }

    return result;
}

static double getEditorValue(const QLineEdit *editor)
{
    if (!editor)
        return FP::undefined();
    QString text(editor->text());
    if (!text.isEmpty()) {
        bool ok = false;
        double value = text.toDouble(&ok);
        if (!ok || !FP::defined(value))
            value = FP::undefined();
        return value;
    }
    return FP::undefined();
}

std::unique_ptr<BaselineSmoother> BaselineSmootherEditor::create() const
{
    int index = type->currentIndex();
    if (index < 0 || index >= type->count())
        return {};

    QString currentType(type->itemData(index).toString());
    currentType = currentType.toLower();

    if (currentType == "singlepole" ||
            currentType == "singlepolelowpass" ||
            currentType == "1p" ||
            currentType == "1plp") {
        auto tc = digitalFilterTimeConstant->create();
        auto gap = digitalFilterGap->create();
        bool resetUndefined = digitalFilterResetOnUndefined->isChecked();
        double stableBand = getEditorValue(digitalFilterStableBand);
        double spikeBand = getEditorValue(digitalFilterSpikeBand);
        return std::unique_ptr<BaselineSmoother>(new BaselineDigitalFilter(
                new DigitalFilterSinglePoleLowPass(tc.release(), gap.release(), resetUndefined),
                stableBand, spikeBand));
    } else if (currentType == "fourpole" ||
            currentType == "fourpolelowpass" ||
            currentType == "4p" ||
            currentType == "4plp") {
        auto tc = digitalFilterTimeConstant->create();
        auto gap = digitalFilterGap->create();
        bool resetUndefined = digitalFilterResetOnUndefined->isChecked();
        double stableBand = getEditorValue(digitalFilterStableBand);
        double spikeBand = getEditorValue(digitalFilterSpikeBand);
        return std::unique_ptr<BaselineSmoother>(new BaselineDigitalFilter(
                new DigitalFilterFourPoleLowPass(tc.release(), gap.release(), resetUndefined),
                stableBand, spikeBand));
    } else if (currentType == "singlepoint") {
        return std::unique_ptr<BaselineSmoother>(new BaselineSinglePoint);
    } else if (currentType == "latest") {
        return std::unique_ptr<BaselineSmoother>(new BaselineLatest);
    } else if (currentType == "disable") {
        return std::unique_ptr<BaselineSmoother>(new BaselineInvalid);
    } else if (currentType == "forever" || currentType == "always") {
        return std::unique_ptr<BaselineSmoother>(new BaselineForever);
    } else {
        double maximumTime = getEditorValue(fixedTimeTotalSeconds);
        double requireTime = getEditorValue(fixedTimeMinimumSeconds);
        if (!FP::defined(requireTime) || requireTime <= 0.0)
            requireTime = maximumTime;
        if (!FP::defined(maximumTime) || maximumTime <= 0.0)
            maximumTime = FP::undefined();
        if (!FP::defined(maximumTime) || maximumTime <= 0.0) {
            if (!FP::defined(requireTime) || requireTime <= 0.0)
                maximumTime = 60.0;
            else
                maximumTime = requireTime;
        }
        if (!FP::defined(requireTime) || requireTime <= 0.0)
            requireTime = maximumTime;
        if (requireTime > maximumTime)
            maximumTime = requireTime;

        double discardTime = getEditorValue(fixedTimeDiscard);
        if (!FP::defined(discardTime) || discardTime <= 0.0)
            discardTime = FP::undefined();

        double stableRSD = getEditorValue(fixedTimeRSD);
        if (FP::defined(stableRSD) && stableRSD < 0.0)
            stableRSD = FP::undefined();
        double spikeBand = getEditorValue(fixedTimeBand);
        if (FP::defined(spikeBand) && spikeBand == 0.0)
            spikeBand = FP::undefined();

        return std::unique_ptr<BaselineSmoother>(
                new BaselineFixedTime(requireTime, maximumTime, stableRSD, spikeBand, discardTime));
    }
}


}
}
}

