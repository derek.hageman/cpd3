/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUIDATAECALIBRATION_H
#define CPD3GUIDATAECALIBRATION_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "guidata/guidata.hxx"
#include "guidata/valueeditor.hxx"

namespace CPD3 {
namespace GUI {
namespace Data {

/**
 * An editor endpoint for calibrations.
 */
class CPD3GUIDATA_EXPORT CalibrationEndpoint : public ValueEndpointEditor {
public:

    virtual bool matches(const CPD3::Data::Variant::Read &value,
                         const CPD3::Data::Variant::Read &metadata);

    virtual bool valid(const CPD3::Data::Variant::Read &value,
                       const CPD3::Data::Variant::Read &metadata);

    bool edit(CPD3::Data::Variant::Write &value,
              const CPD3::Data::Variant::Read &metadata,
              QWidget *parent = 0) override;

    virtual QString collapsedText(const CPD3::Data::Variant::Read &value,
                                  const CPD3::Data::Variant::Read &metadata);
};

}
}
}

#endif
