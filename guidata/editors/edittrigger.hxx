/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUIDATAEDITTRIGGER_H
#define CPD3GUIDATAEDITTRIGGER_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QWidget>
#include <QVBoxLayout>
#include <QComboBox>
#include <QCheckBox>
#include <QTreeWidget>
#include <QLabel>
#include <QPushButton>
#include <QMenu>
#include <QAction>
#include <QListWidget>
#include <QSpinBox>
#include <QLineEdit>

#include "guidata/guidata.hxx"
#include "datacore/variant/root.hxx"
#include "guicore/dynamictimeinterval.hxx"
#include "guicore/calibrationedit.hxx"
#include "guidata/variableselect.hxx"
#include "guidata/valueeditor.hxx"

namespace CPD3 {
namespace GUI {
namespace Data {

class EditTriggerEditor;

namespace Internal {

typedef std::pair<QString, CPD3::Data::Variant::Write> EditTriggerChildData;

class EditTriggerType {
public:
    EditTriggerType();

    virtual ~EditTriggerType();

    virtual QString getDisplayName() const = 0;

    virtual QString getInternalType() const = 0;

    virtual bool matchesType(const std::string &type) const = 0;

    virtual void setTreeItem(const CPD3::Data::Variant::Read &data,
                             const QString &format,
                             QTreeWidgetItem *item) = 0;

    virtual bool isTrigger() const = 0;

    virtual std::vector<EditTriggerChildData> getTriggerChildren(CPD3::Data::Variant::Write &data);

    virtual std::vector<EditTriggerChildData> getInputChildren(CPD3::Data::Variant::Write &data);

    virtual QWidget *createEditor(CPD3::Data::Variant::Write &target,
                                  EditTriggerEditor *updateTarget,
                                  const char *updateSlot) const = 0;
};

class EditTriggerComponentList : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;
    QPushButton *addButton;
    QPushButton *removeButton;
    QMenu *removeMenu;

    void rebuildMenu();

public:
    EditTriggerComponentList(CPD3::Data::Variant::Write value, QWidget *parent = 0);

    virtual ~EditTriggerComponentList();

private slots:

    void add();

    void remove(QAction *action);

signals:

    void changed();
};

class EditTriggerIntervalEditor : public TimeIntervalSelection {
Q_OBJECT

    CPD3::Data::Variant::Write value;
public:
    EditTriggerIntervalEditor(CPD3::Data::Variant::Write value, QWidget *parent = 0);

    virtual ~EditTriggerIntervalEditor();

private slots:

    void handleInterval(CPD3::Time::LogicalTimeUnit unit, int count, bool align);

signals:

    void changed();
};

class EditTriggerMomentEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;
    QListWidget *momentsDisplay;
    QComboBox *momentUnit;
    QSpinBox *entryValue;
    QPushButton *addButton;
    QPushButton *removeButton;

    void rebuildMoments();

public:
    EditTriggerMomentEditor(CPD3::Data::Variant::Write value, QWidget *parent = 0);

    virtual ~EditTriggerMomentEditor();

signals:

    void changed();

private slots:

    void unitChanged(int index);

    void selectionChanged();

    void spinnerChanged(int m);

    void addMoment();

    void removeMoment();
};

class EditTriggerPeriodicRangeEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;
public:
    EditTriggerPeriodicRangeEditor(CPD3::Data::Variant::Write value, QWidget *parent = 0);

    virtual ~EditTriggerPeriodicRangeEditor();

signals:

    void changed();

private slots:

    void startChanged(CPD3::Time::LogicalTimeUnit unit, int count, bool align);

    void endChanged(CPD3::Time::LogicalTimeUnit unit, int count, bool align);
};

class EditTriggerFlagsEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;

    QPushButton *flagsButton;
    QMenu *flagsMenu;
    QAction *delimiterAction;
    QHash<QString, QAction *> flagActions;

    VariableSelect *selection;

public:
    EditTriggerFlagsEditor(CPD3::Data::Variant::Write value,
                           EditTriggerEditor *editor,
                           QWidget *parent = 0);

    virtual ~EditTriggerFlagsEditor();

signals:

    void changed();

private slots:

    void addFlag();

    void flagToggled();

    void setPath(const QString &path);

    void selectionChanged();
};

class EditTriggerExistsEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;

    VariableSelect *selection;
public:
    EditTriggerExistsEditor(CPD3::Data::Variant::Write value,
                            EditTriggerEditor *editor,
                            QWidget *parent = 0);

    virtual ~EditTriggerExistsEditor();

signals:

    void changed();

private slots:

    void setPath(const QString &path);

    void selectionChanged();
};

class EditTriggerScriptEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;

    VariableSelect *selection;
public:
    EditTriggerScriptEditor(CPD3::Data::Variant::Write value,
                            EditTriggerEditor *editor,
                            QWidget *parent = 0);

    virtual ~EditTriggerScriptEditor();

signals:

    void changed();

private slots:

    void setCode(const QString &code);

    void selectionChanged();
};


class EditTriggerInputValueEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;

    VariableSelect *selection;
public:
    EditTriggerInputValueEditor(CPD3::Data::Variant::Write value,
                                EditTriggerEditor *editor,
                                QWidget *parent = 0);

    virtual ~EditTriggerInputValueEditor();

signals:

    void changed();

private slots:

    void setPath(const QString &path);

    void selectionChanged();
};

class EditTriggerInputConstantEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;

    QCheckBox *defined;
    QLineEdit *editor;
public:
    EditTriggerInputConstantEditor(CPD3::Data::Variant::Write value, QWidget *parent = 0);

    virtual ~EditTriggerInputConstantEditor();

private slots:

    void handleValue();

signals:

    void changed();
};

class EditTriggerInputCalibration : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;

    QCheckBox *enableMinimum;
    QLineEdit *minimumBound;
    QCheckBox *enableMaximum;
    QLineEdit *maximumBound;
public:
    EditTriggerInputCalibration(CPD3::Data::Variant::Write value,
                                bool enableLimits = false,
                                QWidget *parent = 0);

    virtual ~EditTriggerInputCalibration();

signals:

    void changed();

private slots:

    void setCalibration(const CPD3::Calibration &cal);

    void minimumChanged();

    void maximumChanged();
};

class EditTriggerInputBufferEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;

    QComboBox *bufferType;
    TimeIntervalSelection *bufferBoth;
    QLabel *bufferBeforeLabel;
    TimeIntervalSelection *bufferBefore;
    QLabel *bufferAfterLabel;
    TimeIntervalSelection *bufferAfter;

    QCheckBox *enableGap;
    TimeIntervalSelection *gapInterval;
public:
    EditTriggerInputBufferEditor(CPD3::Data::Variant::Write value, QWidget *parent = 0);

    virtual ~EditTriggerInputBufferEditor();

signals:

    void changed();

private slots:

    void bufferChanged();

    void gapChanged();
};

class EditTriggerInputQuantileEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;
public:
    EditTriggerInputQuantileEditor(CPD3::Data::Variant::Write value, QWidget *parent = 0);

    virtual ~EditTriggerInputQuantileEditor();

signals:

    void changed();

private slots:

    void setQuantile(double v);
};

class EditTriggerInputRequireAllEditor : public QWidget {
Q_OBJECT

    CPD3::Data::Variant::Write value;
public:
    EditTriggerInputRequireAllEditor(CPD3::Data::Variant::Write value, QWidget *parent = 0);

    virtual ~EditTriggerInputRequireAllEditor();

private slots:

    void setState(bool enabled);

signals:

    void changed();
};


};

/**
 * A widget defining a tree for configuring edit directive triggers.
 */
class CPD3GUIDATA_EXPORT EditTriggerEditor : public QWidget {
Q_OBJECT

    /**
     * This property holds the set of currently available stations that the
     * user can select from.
     * 
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setAvailableStations(const CPD3::Data::SequenceName::ComponentSet &)
     *  <li> CPD3::Data::SequenceName::ComponentSet getAvailableStations() const
     * </ul>
     */
    Q_PROPERTY(CPD3::Data::SequenceName::ComponentSet availableStations
                       READ getAvailableStations
                       WRITE setAvailableStations
                       DESIGNABLE
                       true)
    CPD3::Data::SequenceName::ComponentSet availableStations;

    /**
     * This property holds the set of currently available archives that the
     * user can select from.
     * 
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setAvailableArchives(const CPD3::Data::SequenceName::ComponentSet &)
     *  <li> CPD3::Data::SequenceName::ComponentSet getAvailableArchives() const
     * </ul>
     */
    Q_PROPERTY(CPD3::Data::SequenceName::ComponentSet availableArchives
                       READ getAvailableArchives
                       WRITE setAvailableArchives
                       DESIGNABLE
                       true)
    CPD3::Data::SequenceName::ComponentSet availableArchives;

    /**
    * This property holds the set of currently available variables that the
    * user can select from.
    *
    * <br><br>
    * Access functions:
    * <ul>
    *  <li> void setAvailableVariables(const CPD3::Data::SequenceName::ComponentSet &)
    *  <li> CPD3::Data::SequenceName::ComponentSet getAvailableVariables() const
    * </ul>
    */
    Q_PROPERTY(CPD3::Data::SequenceName::ComponentSet availableVariables
                       READ getAvailableVariables
                       WRITE setAvailableVariables
                       DESIGNABLE
                       true)
    CPD3::Data::SequenceName::ComponentSet availableVariables;

    /**
     * This property holds the set of currently available flavor sets that the
     * user can select from.
     * 
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setAvailableFlavors(const CPD3::Data::SequenceName::ComponentSet &)
     *  <li> CPD3::Data::SequenceName::ComponentSet getAvailableFlavors() const
     * </ul>
     */
    Q_PROPERTY(CPD3::Data::SequenceName::ComponentSet availableFlavors
                       READ getAvailableFlavors
                       WRITE setAvailableFlavors
                       DESIGNABLE
                       true)
    CPD3::Data::SequenceName::ComponentSet availableFlavors;

    CPD3::Data::Variant::Write config;

    QWidget *editorPane;
    QVBoxLayout *editorLayout;
    QWidget *editorWidget;

    QComboBox *type;
    QCheckBox *invert;

    QComboBox *input;

    QWidget *extendPane;
    QComboBox *extendType;
    TimeIntervalSelection *extendBoth;
    QLabel *extendBeforeLabel;
    TimeIntervalSelection *extendBefore;
    QLabel *extendAfterLabel;
    TimeIntervalSelection *extendAfter;

    QTreeWidget *tree;

    QList<Internal::EditTriggerType *> triggerTypes;
    QList<Internal::EditTriggerType *> inputTypes;

    CPD3::Data::Variant::Write selectedTreeItem() const;

    void buildTriggerTree(QTreeWidgetItem *parent,
                          const QString &format,
                          CPD3::Data::Variant::Write &data);

    void buildInputTree(QTreeWidgetItem *parent,
                        const QString &format,
                        CPD3::Data::Variant::Write &data);

    void availableUpdated();

public:
    EditTriggerEditor(QWidget *parent = 0);

    virtual ~EditTriggerEditor();

    const CPD3::Data::Variant::Write &getValue() const;

    void setAvailableStations(const CPD3::Data::SequenceName::ComponentSet &set);

    void setAvailableArchives(const CPD3::Data::SequenceName::ComponentSet &set);

    void setAvailableVariables(const CPD3::Data::SequenceName::ComponentSet &set);

    void setAvailableFlavors(const CPD3::Data::SequenceName::ComponentSet &set);

    CPD3::Data::SequenceName::ComponentSet getAvailableStations() const;

    CPD3::Data::SequenceName::ComponentSet getAvailableArchives() const;

    CPD3::Data::SequenceName::ComponentSet getAvailableVariables() const;

    CPD3::Data::SequenceName::ComponentSet getAvailableFlavors() const;

public slots:

    void configure(CPD3::Data::Variant::Write value);

signals:

    /**
     * Emitted whenever the trigger changes. 
     * 
     * @param value     the trigger content value
     */
    void changed();

private slots:

    void updated();

    void invertChanged(bool state);

    void extendActivated(int index);

    void extendBothChanged(CPD3::Time::LogicalTimeUnit unit, int count, bool align);

    void extendBeforeChanged(CPD3::Time::LogicalTimeUnit unit, int count, bool align);

    void extendAfterChanged(CPD3::Time::LogicalTimeUnit unit, int count, bool align);

    void treeClicked(QTreeWidgetItem *item);

    void treeSelectedUpdated();

    void triggerTypeActivated(int index);

    void inputTypeActivated(int index);
};

/**
 * An editor endpoint for edit triggers.
 */
class CPD3GUIDATA_EXPORT EditTriggerEndpoint : public ValueEndpointEditor {
public:

    bool matches(const CPD3::Data::Variant::Read &value,
                 const CPD3::Data::Variant::Read &metadata) override;

    bool edit(CPD3::Data::Variant::Write &value,
              const CPD3::Data::Variant::Read &metadata,
              QWidget *parent = nullptr) override;

    QString collapsedText(const CPD3::Data::Variant::Read &value,
                          const CPD3::Data::Variant::Read &metadata) override;
};

}
}
}

#endif
