/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QObject>
#include <QColorDialog>
#include <QFontDialog>

#include "graphing.hxx"
#include "graphing/axiscommon.hxx"
#include "graphing/display.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace GUI {
namespace Data {

bool GraphingAxisTransformerBoundEndpoint::matches(const Variant::Read &value,
                                                   const Variant::Read &metadata)
{
    return Util::equal_insensitive(metadata.metadata("Editor").hash("Type").toString(),
                                   "AxisTransformerBound");
}

QString GraphingAxisTransformerBoundEndpoint::collapsedText(const Variant::Read &value,
                                                            const Variant::Read &metadata)
{
    const auto &type = value["Type"].toString();
    if (Util::equal_insensitive(type, "fixed") && value["Value"].exists()) {
        return QObject::tr("Fixed value");
    } else if (Util::equal_insensitive(type, "extendabsolute")) {
        return QObject::tr("Absolute extension");
    } else if (Util::equal_insensitive(type, "powerround")) {
        return QObject::tr("Power rounding");
    }
    return QObject::tr("Fractional extension");
}

bool GraphingAxisTransformerBoundEndpoint::edit(Variant::Write &value,
                                                const Variant::Read &metadata,
                                                QWidget *parent)
{
    CPD3::Graphing::AxisTransformerBoundDialog dialog(parent);
    dialog.setWindowTitle(QObject::tr("Axis Limit"));
    dialog.setFromConfiguration(value);
    if (dialog.exec() != QDialog::Accepted)
        return false;
    value.setEmpty();
    value.set(dialog.toConfiguration());
    return true;
}

bool GraphingColorEndpoint::matches(const Variant::Read &value, const Variant::Read &metadata)
{
    return Util::equal_insensitive(metadata.metadata("Editor").hash("Type").toString(), "Color");
}

QString GraphingColorEndpoint::collapsedText(const Variant::Read &value,
                                             const Variant::Read &metadata)
{
    auto color = CPD3::Graphing::Display::parseColor(value);
    if (!color.isValid()) {
        auto undef = metadata.metadata("UndefinedDescription").toDisplayString();
        if (!undef.isEmpty())
            return undef;
    }
    return color.name();
}

bool GraphingColorEndpoint::edit(Variant::Write &value,
                                 const Variant::Read &metadata,
                                 QWidget *parent)
{
    QColorDialog dialog(CPD3::Graphing::Display::parseColor(value), parent);
    dialog.setWindowTitle(QObject::tr("Color Selection"));
    dialog.setOption(QColorDialog::ShowAlphaChannel);
    if (dialog.exec() != QDialog::Accepted)
        return false;
    value.setEmpty();
    value.set(CPD3::Graphing::Display::formatColor(dialog.currentColor()));
    return true;
}

bool GraphingFontEndpoint::matches(const Variant::Read &value, const Variant::Read &metadata)
{
    return Util::equal_insensitive(metadata.metadata("Editor").hash("Type").toString(), "Font");
}

QString GraphingFontEndpoint::collapsedText(const Variant::Read &value,
                                            const Variant::Read &metadata)
{
    auto font = CPD3::Graphing::Display::parseFont(value);
    auto check = font.family();
    if (!check.isEmpty())
        return check;
    check = font.toString();
    if (!check.isEmpty())
        return check;
    return ValueEndpointEditor::collapsedText(value, metadata);
}

bool GraphingFontEndpoint::edit(Variant::Write &value,
                                const Variant::Read &metadata,
                                QWidget *parent)
{
    QFontDialog dialog(CPD3::Graphing::Display::parseFont(value), parent);
    dialog.setWindowTitle(QObject::tr("Font Selection"));
    if (dialog.exec() != QDialog::Accepted)
        return false;
    value.setEmpty();
    value.set(CPD3::Graphing::Display::formatFont(dialog.selectedFont()));
    return true;
}

}
}
}