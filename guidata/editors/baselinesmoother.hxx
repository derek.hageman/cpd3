/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUIDATABASELINESMOOTHER_H
#define CPD3GUIDATABASELINESMOOTHER_H

#include "core/first.hxx"

#include <memory>
#include <QtGlobal>
#include <QObject>
#include <QDialog>

#include "guidata/guidata.hxx"
#include "guidata/valueeditor.hxx"
#include "guicore/dynamictimeinterval.hxx"
#include "guidata/editors/dynamictimeinterval.hxx"
#include "smoothing/baseline.hxx"

namespace CPD3 {
namespace GUI {
namespace Data {

/**
 * An editor endpoint for baseline smoothers.
 */
class CPD3GUIDATA_EXPORT BaselineSmootherEndpoint : public ValueEndpointEditor {
public:

    virtual bool matches(const CPD3::Data::Variant::Read &value,
                         const CPD3::Data::Variant::Read &metadata);

    bool edit(CPD3::Data::Variant::Write &value,
              const CPD3::Data::Variant::Read &metadata,
              QWidget *parent = 0) override;
};


/**
 * A dialog for editing baseline smoothers.
 */
class CPD3GUIDATA_EXPORT BaselineSmootherEditor : public QWidget {
Q_OBJECT

    QComboBox *type;
    QList<QWidget *> typeDisplays;

    DynamicTimeIntervalEditor *digitalFilterTimeConstant;
    DynamicTimeIntervalEditor *digitalFilterGap;
    QCheckBox *digitalFilterResetOnUndefined;
    QLineEdit *digitalFilterStableBand;
    QLineEdit *digitalFilterSpikeBand;

    QLineEdit *fixedTimeTotalSeconds;
    QLineEdit *fixedTimeMinimumSeconds;
    QLineEdit *fixedTimeDiscard;
    QLineEdit *fixedTimeRSD;
    QLineEdit *fixedTimeBand;

public:
    /**
     * Create the baseline smoother editing dialog.
     *
     * @param config    the current configuration
     * @param metadata  the metadata for the smoother
     * @param parent    the parent widget
     */
    BaselineSmootherEditor(const CPD3::Data::Variant::Read &config = CPD3::Data::Variant::Read::empty(),
                           const CPD3::Data::Variant::Read &metadata = CPD3::Data::Variant::Read::empty(),
                           QWidget *parent = 0);


    /**
     * Get the result of editing.
     *
     * @return  the resulting smoother configuration
     */
    CPD3::Data::Variant::Root smoother() const;


    /**
     * Create a smoother from the contents of the editor.
     *
     * @return  a new smoother
     */
    std::unique_ptr<CPD3::Smoothing::BaselineSmoother> create() const;

signals:

    void changed();


private slots:

    void updateDisplay();

};

}
}
}

#endif
