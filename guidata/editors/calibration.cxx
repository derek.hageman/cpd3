/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QVBoxLayout>
#include <QDialog>
#include <QDialogButtonBox>
#include <QVBoxLayout>

#include "guidata/editors/calibration.hxx"
#include "guicore/calibrationedit.hxx"
#include "core/util.hxx"
#include "datacore/variant/composite.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace GUI {
namespace Data {

/** @file guidata/editors/calibration.hxx
 * Value editors for calibrations.
 */


bool CalibrationEndpoint::matches(const Variant::Read &value, const Variant::Read &metadata)
{
    return Util::equal_insensitive(metadata.metadata("Editor").hash("Type").toString(),
                                   "Calibration");
}

bool CalibrationEndpoint::valid(const Variant::Read &value, const Variant::Read &metadata)
{
    if (!Variant::Composite::toCalibration(value).isValid())
        return false;
    return ValueEndpointEditor::valid(value, metadata);
}

QString CalibrationEndpoint::collapsedText(const Variant::Read &value,
                                           const Variant::Read &metadata)
{
    Calibration cal(Variant::Composite::toCalibration(value));
    if (cal.size() < 1)
        return ValueEndpointEditor::collapsedText(value, metadata);

    QString text;
    NumberFormat exponentFormat(1, 0);
    for (int i = 0, max = cal.size(); i < max; i++) {
        double v = cal.get(i);
        if (v == 0.0)
            continue;

        if (i == 0) {
            text.append(QString::number(v));
            continue;
        }

        if (text.isEmpty()) {
            text.append(QString::number(v));
        } else if (v < 0.0) {
            v = -v;
            text.append(" - ");
            text.append(QString::number(v));
        } else {
            text.append(" + ");
            text.append(QString::number(v));
        }
        text.append("x");
        if (i > 1)
            text.append(exponentFormat.superscript(i));
    }

    if (text.isEmpty())
        return QString::number(0);

    return text;
}

bool CalibrationEndpoint::edit(Variant::Write &value,
                               const Variant::Read &metadata,
                               QWidget *parent)
{
    QDialog dialog(parent);
    dialog.setWindowTitle(QObject::tr("Calibration"));
    QVBoxLayout *layout = new QVBoxLayout(&dialog);
    dialog.setLayout(layout);

    CalibrationEdit *selector = new CalibrationEdit(&dialog);
    selector->setCalibration(Variant::Composite::toCalibration(value));
    layout->addWidget(selector, 1);

    QDialogButtonBox *buttons =
            new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal,
                                 &dialog);
    layout->addWidget(buttons);
    QObject::connect(buttons, SIGNAL(accepted()), &dialog, SLOT(accept()));
    QObject::connect(buttons, SIGNAL(rejected()), &dialog, SLOT(reject()));

    if (dialog.exec() != QDialog::Accepted)
        return false;

    value.setEmpty();
    Variant::Composite::fromCalibration(value, selector->getCalibration());
    return true;
}


}
}
}

