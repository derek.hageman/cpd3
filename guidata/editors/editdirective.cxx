/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTabWidget>
#include <QVBoxLayout>
#include <QHeaderView>
#include <QScrollArea>

#include "guidata/editors/editdirective.hxx"
#include "editing/ebas.hxx"
#include "guicore/guiformat.hxx"
#include "datacore/variant/composite.hxx"

using namespace CPD3::Data;
using namespace CPD3::Editing;

namespace CPD3 {
namespace GUI {
namespace Data {

/** @file guidata/editors/editdirective.hxx
 * An editor for edit directives.
 */

EditDirectiveEditor::EditDirectiveEditor(QWidget *parent) : QWidget(parent),
                                                            settings(CPD3GUI_ORGANIZATION,
                                                                     CPD3GUI_APPLICATION)
{
    QVBoxLayout *topLayout = new QVBoxLayout;
    setLayout(topLayout);
    topLayout->setContentsMargins(0, 0, 0, 0);

    QWidget *lineWidget;
    QHBoxLayout *lineLayout;
    QVBoxLayout *rowsLayout;
    QLabel *label;
    QGroupBox *box;
    QSizePolicy policy;

    box = new QGroupBox(tr("Comments"), this);
    topLayout->addWidget(box);
    policy = box->sizePolicy();
    policy.setVerticalStretch(0);
    box->setSizePolicy(policy);
    lineLayout = new QHBoxLayout;
    box->setLayout(lineLayout);
    lineLayout->setContentsMargins(0, 5, 0, 0);
    comment = new QTextEdit(box);
    lineLayout->addWidget(comment);
    comment->setAcceptRichText(false);
    comment->setMaximumHeight(QFontMetrics(comment->currentFont()).height() * 6);
    comment->setToolTip(tr("Enter a description of the edit directive's purpose."));
    comment->setStatusTip(tr("Edit directive comment"));
    comment->setWhatsThis(
            tr("The text of this field is attached to the edit directive as a comment and description of why the edit was made.  For example, an invalidating edit directive might explain that there was an anomalous spike being removed."));

    QTabWidget *tabs = new QTabWidget(this);
    topLayout->addWidget(tabs);
    policy = tabs->sizePolicy();
    policy.setVerticalStretch(1);
    tabs->setSizePolicy(policy);

    lineWidget = new QWidget(this);
    topLayout->addWidget(lineWidget);
    lineLayout = new QHBoxLayout;
    lineWidget->setLayout(lineLayout);
    lineLayout->setContentsMargins(0, 0, 0, 0);

    label = new QLabel("Author:", lineWidget);
    lineLayout->addWidget(label);

    author = new QLineEdit(lineWidget);
    lineLayout->addWidget(author);
    label->setBuddy(author);
    author->setPlaceholderText(tr("Enter your initials"));
    author->setToolTip(tr("Enter your initials for identifying the author of the edit directive."));
    author->setStatusTip(tr("Edit directive author"));
    author->setWhatsThis(
            tr("The text of this field is attached to edit directive as the author of it.  Generally this should be the initials of the person who created it."));
    author->setText(QString());
    connect(author, SIGNAL(textEdited(
                                   const QString &)), this, SLOT(updateSavedAuthor()));
    connect(author, SIGNAL(textEdited(
                                   const QString &)), this, SLOT(authorUpdated(
                                                                         const QString &)));

    ebasFlags = new QPushButton(tr("EBAS Flags"), lineWidget);
    ebasFlags->setToolTip(tr("The EBAS flags codes applied when the edit directive is active."));
    ebasFlags->setStatusTip(tr("EBAS flags applied"));
    ebasFlags->setWhatsThis(
            tr("This is the set of EBAS flags codes added to the data whenever the edit directive is in effect.  These flags are applied to all data being edited."));
    lineLayout->addWidget(ebasFlags);


    lineWidget = new QWidget(tabs);
    tabs->addTab(lineWidget, tr("Action"));
    tabs->setTabToolTip(tabs->count() - 1,
                        tr("The action performed by the edit directive on the data."));
    tabs->setTabWhatsThis(tabs->count() - 1,
                          tr("This is the action that the edit directive takes when it is active.  The action is only performed when the trigger condition is met."));
    lineLayout = new QHBoxLayout;
    lineWidget->setLayout(lineLayout);
    action = new EditActionEditor(lineWidget);
    lineLayout->addWidget(action);


    lineWidget = new QWidget(tabs);
    tabs->addTab(lineWidget, tr("Trigger"));
    tabs->setTabToolTip(tabs->count() - 1,
                        tr("The trigger that controls when the edit directive is active."));
    tabs->setTabWhatsThis(tabs->count() - 1,
                          tr("This is the trigger that controls when the edit directive is active.  The action is only performed when the trigger condition is met."));
    lineLayout = new QHBoxLayout;
    lineWidget->setLayout(lineLayout);
    trigger = new EditTriggerEditor(lineWidget);
    lineLayout->addWidget(trigger);


    QScrollArea *advancedScroll = new QScrollArea(tabs);
    tabs->addTab(advancedScroll, tr("Advanced"));
    tabs->setTabToolTip(tabs->count() - 1, tr("The advanced properties of the edit directive."));
    tabs->setTabWhatsThis(tabs->count() - 1,
                          tr("This tab controls the advanced or otherwise not normally required properties of the edit directive."));
    QWidget *advancedWidget = new QWidget(advancedScroll);
    advancedScroll->setWidget(advancedWidget);
    QVBoxLayout *advancedLayout = new QVBoxLayout;
    advancedWidget->setLayout(advancedLayout);
    advancedScroll->setWidgetResizable(true);

    lineWidget = new QWidget(tabs);
    tabs->addTab(lineWidget, tr("History"));
    tabs->setTabToolTip(tabs->count() - 1,
                        tr("The history of operations performed to the edit directive."));
    tabs->setTabWhatsThis(tabs->count() - 1,
                          tr("This is the history of changes to the edit directive since its original creation."));
    lineLayout = new QHBoxLayout;
    lineWidget->setLayout(lineLayout);
    historyDisplay = new QTableWidget(lineWidget);
    lineLayout->addWidget(historyDisplay);

    historyDisplay->setToolTip(tr("The history of operations performed to the edit directive."));
    historyDisplay->setStatusTip(tr("Edit directive operation history"));
    historyDisplay->setWhatsThis(
            tr("This is the history of changes to the edit directive since its original creation."));
    historyDisplay->setColumnCount(3);
    historyDisplay->setSelectionMode(QAbstractItemView::NoSelection);
    historyDisplay->verticalHeader()->hide();
    historyDisplay->setWordWrap(true);
    connect(historyDisplay->horizontalHeader(), SIGNAL(sectionResized(int, int, int)),
            historyDisplay, SLOT(resizeRowsToContents()));

    QTableWidgetItem *item = new QTableWidgetItem(tr("Time"));
    item->setData(Qt::ToolTipRole, tr("The time that the change took place."));
    item->setData(Qt::WhatsThisRole,
                  tr("This is the time that the change described in the row took place."));
    historyDisplay->setHorizontalHeaderItem(0, item);

    item = new QTableWidgetItem(tr("User"));
    item->setData(Qt::ToolTipRole, tr("The user name that made the change."));
    item->setData(Qt::WhatsThisRole, tr("This is the user name that made the change."));
    historyDisplay->setHorizontalHeaderItem(1, item);

    item = new QTableWidgetItem(tr("Description"));
    item->setData(Qt::ToolTipRole, tr("The description of the change."));
    item->setData(Qt::WhatsThisRole,
                  tr("This is a short description of what the change to the edit directive was."));
    historyDisplay->setHorizontalHeaderItem(2, item);

    historyDisplay->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    historyDisplay->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    historyDisplay->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);


    lineWidget = new QWidget(advancedWidget);
    advancedLayout->addWidget(lineWidget);
    lineLayout = new QHBoxLayout;
    lineWidget->setLayout(lineLayout);
    lineLayout->setContentsMargins(0, 0, 0, 0);
    policy = lineWidget->sizePolicy();
    policy.setVerticalStretch(0);
    lineWidget->setSizePolicy(policy);

    priorityEnable = new QCheckBox(tr("Priority:"), lineWidget);
    lineLayout->addWidget(priorityEnable);
    policy = priorityEnable->sizePolicy();
    policy.setHorizontalStretch(0);
    priorityEnable->setSizePolicy(policy);
    priorityEnable->setToolTip(tr("Enable the application priority of the edit directive."));
    priorityEnable->setStatusTip(tr("Enable application priority"));
    priorityEnable->setWhatsThis(
            tr("This enables the priority level of the edit directive.  Edits without a priority are applied without an order before anything else is applied."));

    priority = new QSpinBox(lineWidget);
    lineLayout->addWidget(priority);
    policy = priority->sizePolicy();
    policy.setHorizontalStretch(1);
    priority->setSizePolicy(policy);
    priority->setToolTip(
            tr("The priority at which the edit directive is applied.  Lower priories are executed before larger ones."));
    priority->setStatusTip(tr("Edit application priority"));
    priority->setWhatsThis(
            tr("This is the priority level at which the edit directive is applied.  Lower priories are applies before larger ones.  Edits with negative priority are applied before the first averaging step."));
    priority->setRange(INT_MIN, INT_MAX);

    systemInternal = new QCheckBox(tr("Internal"), lineWidget);
    lineLayout->addWidget(systemInternal);
    policy = systemInternal->sizePolicy();
    policy.setHorizontalStretch(0);
    systemInternal->setSizePolicy(policy);
    systemInternal->setToolTip(tr("Set the system internal state of the edit directive."));
    systemInternal->setStatusTip(tr("System internal state"));
    systemInternal->setWhatsThis(
            tr("This sets the system internal state of the edit directive.  This has no effect on the action the edit directive performs but it does hide it from the default display."));

    actionFragment = new QCheckBox(tr("Fragment action data"));
    advancedLayout->addWidget(actionFragment);
    actionFragment->setToolTip(tr("Set the fragmentation mode of data being operated on."));
    actionFragment->setStatusTip(tr("Action fragmentation state"));
    actionFragment->setWhatsThis(
            tr("When enabled, this causes data to be fragmented to the times of the edit directive before operation by the action.  The most common use case of this is long running edits on long running (meta)data to ensure that the (meta)data are only changed while the directive is active."));
    actionFragment->setTristate(true);


    fanoutAction.initialize(advancedWidget, advancedLayout);
    fanoutAction.box->setTitle(tr("Action Fanout"));
    fanoutAction.box
                ->setToolTip(
                        tr("The state fanout for the action component of the edit directive."));
    fanoutAction.box->setStatusTip(tr("Action fanout control"));
    fanoutAction.box
                ->setWhatsThis(
                        tr("This is the fanout performed for the state of the action of the edit directive.  Independent state is maintained for all components of the action that are fanned out."));
    policy = fanoutAction.box->sizePolicy();
    policy.setVerticalStretch(0);
    fanoutAction.box->setSizePolicy(policy);

    fanoutTrigger.initialize(advancedWidget, advancedLayout);
    fanoutTrigger.box->setTitle(tr("Trigger Fanout"));
    fanoutTrigger.box
                 ->setToolTip(
                         tr("The state fanout for the trigger component of the edit directive."));
    fanoutTrigger.box->setStatusTip(tr("Trigger fanout control"));
    fanoutTrigger.box
                 ->setWhatsThis(
                         tr("This is the fanout performed for the state of the trigger of the edit directive.  Independent state is maintained for all components of the trigger that are fanned out."));
    policy = fanoutTrigger.box->sizePolicy();
    policy.setVerticalStretch(0);
    fanoutTrigger.box->setSizePolicy(policy);

    averageContinuous = new QGroupBox(tr("Re-average From Contiguous Data"), advancedWidget);
    advancedLayout->addWidget(averageContinuous);
    lineLayout = new QHBoxLayout;
    averageContinuous->setLayout(lineLayout);
    averageContinuous->setCheckable(true);
    averageContinuous->setToolTip(tr("Enable regeneration of averages from the contiguous data."));
    averageContinuous->setStatusTip(tr("Contiguous re-averaging"));
    averageContinuous->setWhatsThis(
            tr("This enables the selection of variables who's averages are regenerated from the contiguous average segments.  That is, these are the variables that the directive alters in a way that works on contiguous segments but not on full averages.  For example, anything recalculated from difference measurements changed in value by the directive."));
    policy = averageContinuous->sizePolicy();
    policy.setVerticalStretch(1);
    averageContinuous->setSizePolicy(policy);

    averageContinuousSelection = new VariableSelect(averageContinuous);
    lineLayout->addWidget(averageContinuousSelection);

    averageContinuousSelectionDefaults.setArchiveFanout(QStringList() << "cont");


    averageFull = new QGroupBox(tr("Full Re-averaging"), advancedWidget);
    advancedLayout->addWidget(averageFull);
    lineLayout = new QHBoxLayout;
    averageFull->setLayout(lineLayout);
    averageFull->setCheckable(true);
    averageFull->setToolTip(tr("Enable regeneration of averages from the high resolution data."));
    averageFull->setStatusTip(tr("Full re-averaging"));
    averageFull->setWhatsThis(
            tr("This enables the selection of variables who's averages are regenerated from the full high resolution data.  That is, these are the variables that the directive alters in a way that works on high resolution data.  For example, a correction that is not linear."));
    policy = averageFull->sizePolicy();
    policy.setVerticalStretch(1);
    averageFull->setSizePolicy(policy);

    averageFullSelection = new VariableSelect(averageFull);
    lineLayout->addWidget(averageFullSelection);

    averageFullSelectionDefaults.setArchiveFanout(QStringList() << "raw");


    extend = new QGroupBox(tr("Input Extension"), advancedWidget);
    advancedLayout->addWidget(extend);
    rowsLayout = new QVBoxLayout;
    extend->setLayout(rowsLayout);
    extend->setCheckable(true);
    extend->setToolTip(tr("Enable extension of the input when the edit directive is active."));
    extend->setStatusTip(tr("Input extension"));
    extend->setWhatsThis(
            tr("This enables the extension of the input to the editing system.  This is used to allow the edit to establish additional state if required."));
    policy = averageFull->sizePolicy();
    policy.setVerticalStretch(1);
    averageFull->setSizePolicy(policy);

    extendBounds = new TimeBoundSelection(extend);
    rowsLayout->addWidget(extendBounds);
    extendBounds->setAllowUndefinedStart(true);
    extendBounds->setAllowUndefinedEnd(true);
    extendBounds->setToolTip(tr("The fixed time bounds the input is extended to."));
    extendBounds->setStatusTip(tr("Fixed input extension"));
    extendBounds->setWhatsThis(
            tr("These are the bounds the input is extended to always.  If set to infinite they are disabled."));
    policy = extendBounds->sizePolicy();
    policy.setVerticalStretch(0);
    extendBounds->setSizePolicy(policy);

    extendData = new QGroupBox(tr("Extension From Data"), advancedWidget);
    {
        rowsLayout->addWidget(extendData);
        QVBoxLayout *extendLayout = new QVBoxLayout;
        extendData->setLayout(extendLayout);
        extendData->setCheckable(true);
        extendData->setToolTip(
                tr("Enable extension of the input to bounds established by another data value."));
        extendData->setStatusTip(tr("Input extension from data"));
        extendData->setWhatsThis(
                tr("This controls the extension of the input to the editing system to the limits that another data value exists at.  For example, this can be used to extend the input to the times a filter was changed."));
        policy = extendData->sizePolicy();
        policy.setVerticalStretch(1);
        extendData->setSizePolicy(policy);

        lineWidget = new QWidget(extendData);
        extendLayout->addWidget(lineWidget);
        lineLayout = new QHBoxLayout(lineWidget);
        lineLayout->setContentsMargins(0, 0, 0, 0);
        lineWidget->setLayout(lineLayout);

        extendDataStart = new QCheckBox(tr("Start:"), lineWidget);
        lineLayout->addWidget(extendDataStart);
        extendDataStart->setToolTip(tr("Set the start time from the data values."));
        extendDataStart->setStatusTip(tr("Data start extension"));
        extendDataStart->setWhatsThis(
                tr("This controls if the extension sets its start time from the bounds of the data specified."));

        extendDataStartInterval = new TimeIntervalSelection(lineWidget);
        lineLayout->addWidget(extendDataStartInterval);
        extendDataStartInterval->setToolTip(
                tr("The amount of time before the data start that extension is performed for."));
        extendDataStartInterval->setStatusTip(tr("Data start extension additional"));
        extendDataStartInterval->setWhatsThis(
                tr("This controls the amount of time before the first data value that the data used are extended for."));
        extendDataStartInterval->setAllowZero(true);
        extendDataStartInterval->setAllowNegative(true);

        lineWidget = new QWidget(extendData);
        extendLayout->addWidget(lineWidget);
        lineLayout = new QHBoxLayout(lineWidget);
        lineLayout->setContentsMargins(0, 0, 0, 0);
        lineWidget->setLayout(lineLayout);

        extendDataEnd = new QCheckBox(tr("End:"), lineWidget);
        lineLayout->addWidget(extendDataEnd);
        extendDataEnd->setToolTip(tr("Set the end time from the data values."));
        extendDataEnd->setStatusTip(tr("Data end extension"));
        extendDataEnd->setWhatsThis(
                tr("This controls if the extension sets its end time from the bounds of the data specified."));

        extendDataEndInterval = new TimeIntervalSelection(lineWidget);
        lineLayout->addWidget(extendDataEndInterval);
        extendDataEndInterval->setToolTip(
                tr("The amount of time before the data end that extension is performed for."));
        extendDataEndInterval->setStatusTip(tr("Data end extension additional"));
        extendDataEndInterval->setWhatsThis(
                tr("This controls the amount of time before the first data value that the data used are extended for."));
        extendDataEndInterval->setAllowZero(true);
        extendDataEndInterval->setAllowNegative(true);

        extendDataSelection = new VariableSelect(extendData);
        extendLayout->addWidget(extendDataSelection);
    }


    QMenu *ebasMainMenu = new QMenu(ebasFlags);
    ebasFlags->setMenu(ebasMainMenu);
    QMenu *subMenu = ebasMainMenu->addMenu(tr("Invalidated"));
    subMenu->setToolTip(tr("Flags that indicate the cause of invalidated data."));
    subMenu->setStatusTip(tr("Invalidated flags"));
    subMenu->setWhatsThis(
            tr("This menu contains flags that EBAS recognizes as reasons why data was invalidated.  These flags provide the reasoning behind otherwise valid values that where removed."));
    setEBASMenu(subMenu, EBAS::getAllDescriptions(EBAS::Flag_Invalid));

    subMenu = ebasMainMenu->addMenu(tr("Hidden"));
    subMenu->setToolTip(tr("Flags that indicate the cause of hidden data."));
    subMenu->setStatusTip(tr("Hidden flags"));
    subMenu->setWhatsThis(
            tr("This menu contains flags that EBAS recognizes as reasons why data should be hidden.  This data should be valid but not shown on the export."));
    setEBASMenu(subMenu, EBAS::getAllDescriptions(EBAS::Flag_Hidden));

    subMenu = ebasMainMenu->addMenu(tr("Missing"));
    subMenu->setToolTip(tr("Flags that indicate the cause of missing data."));
    subMenu->setStatusTip(tr("Missing flags"));
    subMenu->setWhatsThis(
            tr("This menu contains flags that EBAS recognizes as reasons why data are not present.  These flags indicate the reason why not data can possibly exist."));
    setEBASMenu(subMenu, EBAS::getAllDescriptions(EBAS::Flag_Missing));

    subMenu = ebasMainMenu->addMenu(tr("Suspect"));
    subMenu->setToolTip(tr("Flags that indicate suspect data."));
    subMenu->setStatusTip(tr("Suspect flags"));
    subMenu->setWhatsThis(
            tr("This menu contains flags that indicate to EBAS that data are valid but suspect."));
    setEBASMenu(subMenu, EBAS::getAllDescriptions(EBAS::Flag_Valid));

    ebasNoFlags = ebasMainMenu->addAction(tr("None", "ebas no flag"));
    ebasNoFlags->setToolTip(tr("Disable all EBAS flags."));
    ebasNoFlags->setStatusTip(tr("No EBAS Flags"));
    ebasNoFlags->setWhatsThis(tr("When selected all EBAS flags are un-selected."));
    ebasNoFlags->setCheckable(true);

    configure(Variant::Write::empty());
}

EditDirectiveEditor::~EditDirectiveEditor() = default;

void EditDirectiveEditor::setEBASMenu(QMenu *target, const QHash<int, QString> &flags)
{
    QList<int> order(flags.keys());
    std::sort(order.begin(), order.end());

    QMenu *hundreds = NULL;
    int prior = -100;
    for (QList<int>::const_iterator add = order.constBegin(), end = order.constEnd();
            add != end;
            ++add) {
        if (hundreds == NULL || (prior / 100) != (*add / 100)) {
            hundreds = target->addMenu(tr("%1xx series", "ebas flags series").arg(*add / 100));
        }
        prior = *add;

        QAction *action = hundreds->addAction(
                tr("%1 - %2", "ebas flag text").arg(*add).arg(flags.value(*add)));
        ebasFlagsOptions.insert(QString::fromLatin1("EBASFlag%1").arg(*add), action);

        action->setCheckable(true);
    }
}

void EditDirectiveEditor::Fanout::initialize(QWidget *parent, QVBoxLayout *layout)
{
    box = new QGroupBox(parent);
    layout->addWidget(box);
    QVBoxLayout *boxLayout = new QVBoxLayout;
    box->setLayout(boxLayout);

    QWidget *lineWidget = new QWidget(box);
    boxLayout->addWidget(lineWidget);
    QHBoxLayout *lineLayout = new QHBoxLayout;
    lineLayout->setContentsMargins(0, 0, 0, 0);
    lineWidget->setLayout(lineLayout);

    station = new QCheckBox(tr("Station"), lineWidget);
    lineLayout->addWidget(station);
    station->setTristate(true);
    station->setToolTip(
            tr("When enabled, this causes the edit directive to maintain independent state for each station."));
    station->setStatusTip(tr("Station fanout"));
    station->setWhatsThis(
            tr("When this is enabled, this causes the edit directive to maintain an independent copy of its state for each station in the data.  When left and the intermediate state, the default is determined by the edit directive type."));

    archive = new QCheckBox(tr("Archive"), lineWidget);
    lineLayout->addWidget(archive);
    archive->setTristate(true);
    archive->setToolTip(
            tr("When enabled, this causes the edit directive to maintain independent state for each archive."));
    archive->setStatusTip(tr("Archive fanout"));
    archive->setWhatsThis(
            tr("When this is enabled, this causes the edit directive to maintain an independent copy of its state for each archive in the data.  When left and the intermediate state, the default is determined by the edit directive type."));

    variable = new QCheckBox(tr("Variable"), lineWidget);
    lineLayout->addWidget(variable);
    variable->setTristate(true);
    variable->setToolTip(
            tr("When enabled, this causes the edit directive to maintain independent state for each variable."));
    variable->setStatusTip(tr("Variable fanout"));
    variable->setWhatsThis(
            tr("When this is enabled, this causes the edit directive to maintain an independent copy of its state for each variable in the data.  When left and the intermediate state, the default is determined by the edit directive type."));

    flavors = new QCheckBox(tr("Flavors"), lineWidget);
    lineLayout->addWidget(flavors);
    flavors->setTristate(true);
    flavors->setToolTip(
            tr("When enabled, this causes the edit directive to maintain independent state for each set of flavors."));
    flavors->setStatusTip(tr("Flavors fanout"));
    flavors->setWhatsThis(
            tr("When this is enabled, this causes the edit directive to maintain an independent copy of its state for each set of flavors in the data.  When left and the intermediate state, the default is determined by the edit directive type."));


    lineWidget = new QWidget(box);
    boxLayout->addWidget(lineWidget);
    lineLayout = new QHBoxLayout;
    lineLayout->setContentsMargins(0, 0, 0, 0);
    lineWidget->setLayout(lineLayout);

    flattenStatistics = new QCheckBox(tr("Flatten statistics"), lineWidget);
    lineLayout->addWidget(flattenStatistics);
    flattenStatistics->setTristate(true);
    flattenStatistics->setToolTip(
            tr("When enabled, this causes statistics to be incorporated into the flavors set of their originator."));
    flattenStatistics->setStatusTip(tr("Flatten statistics fanout"));
    flattenStatistics->setWhatsThis(
            tr("When this is enabled, statistics flavors will be considered part of the same set of flavors as the originator.  This effectively causes them to be considered the same flavor set for the purposes of fanout.  When left and the intermediate state, the default is determined by the edit directive type."));

    flattenMetadata = new QCheckBox(tr("Flatten metadata"), lineWidget);
    lineLayout->addWidget(flattenMetadata);
    flattenMetadata->setTristate(true);
    flattenMetadata->setToolTip(
            tr("When enabled, this causes metadata to be incorporated into the archive it is paired with."));
    flattenMetadata->setStatusTip(tr("Flatten statistics fanout"));
    flattenMetadata->setWhatsThis(
            tr("When this is enabled, metadata archives are considered the same as their paired main archive.  This effectively causes them to be incorporated with the main data archive for the purposes of fanout.  When left and the intermediate state, the default is determined by the edit directive type."));
}

void EditDirectiveEditor::Fanout::fromValue(const CPD3::Data::Variant::Read &data, QCheckBox *box)
{
    if (!data.exists()) {
        box->setCheckState(Qt::PartiallyChecked);
        return;
    }
    box->setCheckState(data.toBool() ? Qt::Checked : Qt::Unchecked);
}

void EditDirectiveEditor::Fanout::toValue(CPD3::Data::Variant::Write &data, QCheckBox *box)
{
    switch (box->checkState()) {
    case Qt::PartiallyChecked:
        data.remove(false);
        break;
    case Qt::Checked:
        data.setBool(true);
        break;
    case Qt::Unchecked:
        data.setBool(false);
        break;
    }
}

void EditDirectiveEditor::Fanout::configure(const Variant::Read &data,
                                            QObject *target,
                                            const char *slot)
{
    station->disconnect(target);
    archive->disconnect(target);
    variable->disconnect(target);
    flavors->disconnect(target);
    flattenStatistics->disconnect(target);
    flattenMetadata->disconnect(target);

    fromValue(data.hash("Station"), station);
    fromValue(data.hash("Archive"), archive);
    fromValue(data.hash("Variable"), variable);
    fromValue(data.hash("Flavors"), flavors);
    fromValue(data.hash("FlattenStatistics"), flattenStatistics);
    fromValue(data.hash("FlattenMetadata"), flattenMetadata);

    flattenStatistics->setEnabled(flavors->checkState() != Qt::Unchecked);
    flattenMetadata->setEnabled(archive->checkState() != Qt::Unchecked);

    connect(station, SIGNAL(stateChanged(int)), target, slot);
    connect(archive, SIGNAL(stateChanged(int)), target, slot);
    connect(variable, SIGNAL(stateChanged(int)), target, slot);
    connect(flavors, SIGNAL(stateChanged(int)), target, slot);
    connect(flattenStatistics, SIGNAL(stateChanged(int)), target, slot);
    connect(flattenMetadata, SIGNAL(stateChanged(int)), target, slot);
}

void EditDirectiveEditor::Fanout::write(Variant::Write &data)
{
    toValue(data.hash("Station"), station);
    toValue(data.hash("Archive"), archive);
    toValue(data.hash("Variable"), variable);
    toValue(data.hash("Flavors"), flavors);
    toValue(data.hash("FlattenStatistics"), flattenStatistics);
    toValue(data.hash("FlattenMetadata"), flattenMetadata);

    flattenStatistics->setEnabled(flavors->checkState() != Qt::Unchecked);
    flattenMetadata->setEnabled(archive->checkState() != Qt::Unchecked);
}

/**
 * Get the configured edit value.
 * 
 * @return  the edit value
 */
const Variant::Write &EditDirectiveEditor::getValue() const
{ return data; }

static Variant::Flags toFlags(const Variant::Read &config)
{
    switch (config.getType()) {
    case Variant::Type::String: {
        const auto &flag = config.toString();
        if (flag.empty())
            return Variant::Flags();
        return Variant::Flags{flag};
    }
    default:
        break;
    }
    return config.toFlags();
}

/**
 * Configure the editor with the given value.  This value should be the
 * trigger contents.
 * 
 * @param value     the value to configure with
 */
void EditDirectiveEditor::configure(CPD3::Data::Variant::Write value)
{
    data = std::move(value);

    comment->disconnect(this);
    comment->setPlainText(data.hash("Comment").toQString());
    connect(comment, SIGNAL(textChanged()), this, SLOT(commentUpdated()));

    if (!data.hash("Author").exists()) {
        if (!author->text().isEmpty()) {
            data.hash("Author").setString(author->text());
        } else if (!settings.value("user/initials").toString().isEmpty()) {
            data.hash("Author").setString(settings.value("user/initials").toString());
        }
    }
    author->setText(data.hash("Author").toQString());

    {
        auto flags = toFlags(data.hash("Parameters").hash("EBASFlags"));
        bool anySelected = false;
        for (QHash<QString, QAction *>::const_iterator flag = ebasFlagsOptions.constBegin(),
                endFlag = ebasFlagsOptions.constEnd(); flag != endFlag; ++flag) {
            flag.value()->disconnect(this);
            bool selected = flags.count(flag.key().toStdString()) != 0;
            flag.value()->setChecked(selected);
            anySelected = anySelected || selected;
            connect(flag.value(), SIGNAL(toggled(bool)), this, SLOT(ebasFlagsUpdated()));
        }
        ebasNoFlags->disconnect(this);
        ebasNoFlags->setChecked(!anySelected);
        connect(ebasNoFlags, SIGNAL(toggled(bool)), this, SLOT(ebasNoFlagsUpdated()));
    }

    action->disconnect(this);
    action->configure(data.hash("Parameters").hash("Action"));
    connect(action, SIGNAL(changed()), this, SLOT(updated()));

    trigger->disconnect(this);
    trigger->configure(data.hash("Parameters").hash("Trigger"));
    connect(trigger, SIGNAL(changed()), this, SLOT(updated()));

    priorityEnable->disconnect(this);
    priority->disconnect(this);
    {
        qint64 p = data.hash("Priority").toInt64();
        if (INTEGER::defined(p)) {
            priorityEnable->setChecked(true);
            priority->setEnabled(true);
            priority->setValue((int) p);
        } else {
            priorityEnable->setChecked(false);
            priority->setEnabled(false);
            priority->setValue(-1);
        }
    }
    connect(priorityEnable, SIGNAL(toggled(bool)), this, SLOT(priorityUpdated()));
    connect(priority, SIGNAL(valueChanged(int)), this, SLOT(priorityUpdated()));

    systemInternal->disconnect(this);
    systemInternal->setChecked(data.hash("SystemInternal").toBool());
    connect(systemInternal, SIGNAL(toggled(bool)), this, SLOT(systemInternalUpdated()));

    actionFragment->disconnect(this);
    if (!data.hash("Parameters").hash("Action").hash("Fragment").exists()) {
        actionFragment->setCheckState(Qt::PartiallyChecked);
    } else {
        actionFragment->setCheckState(
                data.hash("Parameters").hash("Action").hash("Fragment").toBool() ? Qt::Checked
                                                                                 : Qt::Unchecked);
    }
    connect(actionFragment, SIGNAL(stateChanged(int)), this, SLOT(actionFragmentUpdated()));


    fanoutAction.configure(data.hash("Parameters").hash("Action").hash("Fanout"), this,
                           SLOT(fanoutActionUpdated()));
    fanoutTrigger.configure(data.hash("Parameters").hash("Trigger").hash("Fanout"), this,
                            SLOT(fanoutTriggerUpdated()));


    averageContinuous->disconnect(this);
    averageContinuousSelection->disconnect(this);
    averageFull->disconnect(this);
    averageFullSelection->disconnect(this);

    averageContinuous->setChecked(data.hash("Average").hash("Continuous").exists());
    averageContinuousSelection->configureFromSequenceMatch(data.hash("Average").hash("Continuous"),
                                                           averageContinuousSelectionDefaults);

    averageFull->setChecked(data.hash("Average").hash("Full").exists());
    averageFullSelection->configureFromSequenceMatch(data.hash("Average").hash("Full"),
                                                     averageFullSelectionDefaults);

    averageContinuousSelection->setEnabled(averageContinuous->isChecked());
    connect(averageContinuous, SIGNAL(toggled(bool)), this, SLOT(averageContinuousUpdated()));
    connect(averageContinuousSelection, SIGNAL(selectionChanged()), this,
            SLOT(averageContinuousUpdated()));

    averageFullSelection->setEnabled(averageFull->isChecked());
    connect(averageFull, SIGNAL(toggled(bool)), this, SLOT(averageFullUpdated()));
    connect(averageFullSelection, SIGNAL(selectionChanged()), this, SLOT(averageFullUpdated()));


    extend->disconnect(this);
    extendBounds->disconnect(this);
    extendData->disconnect(this);
    extendDataSelection->disconnect(this);

    extend->setChecked(data.hash("Extend").exists());
    extendBounds->setEnabled(extend->isChecked());
    extendBounds->setBounds(data.hash("Extend").hash("Start").toDouble(),
                            data.hash("Extend").hash("End").toDouble());

    extendData->setEnabled(extend->isChecked());
    extendData->setChecked(data.hash("Extend").hash("Data").exists() && extend->isChecked());
    extendDataSelection->setEnabled(extendData->isChecked());
    extendDataSelection->configureFromSequenceMatch(data.hash("Extend").hash("Data"),
                                                    extendDataSelectionDefaults);
    extendDataStart->setEnabled(extendData->isChecked());
    extendDataStart->setChecked(data.hash("Extend").hash("DataStart").exists());
    extendDataStartInterval->setEnabled(extendDataStart->isChecked());
    {
        int count = 0;
        bool align = false;
        Time::LogicalTimeUnit units =
                Variant::Composite::toTimeInterval(data.hash("Extend").hash("DataStart"), &count,
                                                   &align);
        extendDataStartInterval->setInterval(units, count, align);
    }
    extendDataEnd->setEnabled(extendData->isChecked());
    extendDataEnd->setChecked(data.hash("Extend").hash("DataEnd").exists());
    extendDataEnd->setEnabled(extendData->isChecked());
    extendDataEndInterval->setEnabled(extendDataEnd->isChecked());
    {
        int count = 0;
        bool align = false;
        Time::LogicalTimeUnit units =
                Variant::Composite::toTimeInterval(data.hash("Extend").hash("DataEnd"), &count,
                                                   &align);
        extendDataEndInterval->setInterval(units, count, align);
    }

    connect(extend, SIGNAL(toggled(bool)), this, SLOT(extendUpdated()));
    connect(extendData, SIGNAL(toggled(bool)), this, SLOT(extendUpdated()));
    connect(extendBounds, SIGNAL(boundsEdited(double, double)), this, SLOT(extendUpdated()));
    connect(extendDataSelection, SIGNAL(selectionChanged()), this, SLOT(extendUpdated()));
    connect(extendDataStart, SIGNAL(toggled(bool)), this, SLOT(extendUpdated()));
    connect(extendDataStartInterval,
            SIGNAL(intervalChanged(CPD3::Time::LogicalTimeUnit, int, bool)), this,
            SLOT(extendUpdated()));
    connect(extendDataEnd, SIGNAL(toggled(bool)), this, SLOT(extendUpdated()));
    connect(extendDataEndInterval, SIGNAL(intervalChanged(CPD3::Time::LogicalTimeUnit, int, bool)),
            this, SLOT(extendUpdated()));




    /* Clear removes the header too */
    for (int i = historyDisplay->rowCount() - 1; i >= 0; i--) {
        historyDisplay->removeRow(i);
    }
    for (auto historyData : data.hash("History").toArray()) {
        int row = historyDisplay->rowCount();
        historyDisplay->insertRow(row);

        QString itemData(GUITime::formatTime(historyData.hash("At").toDouble(), &settings));
        QTableWidgetItem *item = new QTableWidgetItem(itemData);
        item->setFlags(Qt::ItemIsEnabled);
        item->setData(Qt::ToolTipRole,
                      tr("The change to the directive took place at %1.").arg(itemData));
        item->setData(Qt::WhatsThisRole,
                      tr("This indicates that the real time (as determined on the machine that made the change) was %1 when the edit directive was changed.")
                              .arg(itemData));
        historyDisplay->setItem(row, 0, item);

        itemData = historyData.hash("User").toDisplayString().trimmed();
        if (itemData.isEmpty())
            itemData = tr("UNKNOWN", "unknown user");
        item = new QTableWidgetItem(itemData);
        item->setFlags(Qt::ItemIsEnabled);
        item->setData(Qt::ToolTipRole,
                      tr("The user name that made the change was %1.").arg(itemData));
        item->setData(Qt::WhatsThisRole,
                      tr("This indicates that the effective user name of the process that made the change was %1.")
                              .arg(itemData));
        historyDisplay->setItem(row, 1, item);

        const auto &type = historyData.hash("Type").toString();
        item = new QTableWidgetItem(QString::fromStdString(type));
        item->setFlags(Qt::ItemIsEnabled);
        if (Util::equal_insensitive(type, "Created")) {
            item->setData(Qt::DisplayRole, tr("Created", "history item created"));
            item->setData(Qt::ToolTipRole,
                          tr("The edit directive was initially created at this time."));
            item->setData(Qt::WhatsThisRole,
                          tr("This indicates that the edit directive was first created with this history event."));
        } else if (Util::equal_insensitive(type, "profilechanged")) {
            item->setData(Qt::DisplayRole, tr("Profile changed from %1").arg(
                    historyData.hash("OriginalProfile").toQString()));
            item->setData(Qt::ToolTipRole, tr("The profile of the edit directive was altered."));
            item->setData(Qt::WhatsThisRole,
                          tr("This indicates that profile the edit directive belongs to was altered."));
        } else if (Util::equal_insensitive(type, "BoundsChanged")) {
            item->setData(Qt::DisplayRole, tr("Effective times changed from %1 to %2").arg(
                    GUITime::formatRange(
                            historyData.hash("OriginalBounds").hash("Start").toDouble(),
                            historyData.hash("OriginalBounds").hash("End").toDouble(), &settings),
                    GUITime::formatRange(historyData.hash("RevisedBounds").hash("Start").toDouble(),
                                         historyData.hash("RevisedBounds").hash("End").toDouble(),
                                         &settings)));
            item->setData(Qt::ToolTipRole,
                          tr("The edit directive had its effective bounds modified."));
            item->setData(Qt::WhatsThisRole,
                          tr("This indicates that the edit directive bounds where modified to a new effective range."));
        } else if (Util::equal_insensitive(type, "ParametersChanged")) {
            item->setData(Qt::DisplayRole, tr("Operational parameters altered"));
            item->setData(Qt::ToolTipRole,
                          tr("The parameters controlling the edit directive's operations where modified."));
            item->setData(Qt::WhatsThisRole,
                          tr("This indicates that some component of the parameters controlling the edit directive where modified.  This includes the action, trigger, and/or EBAS flagging."));
        } else if (Util::equal_insensitive(type, "ExtendChanged")) {
            item->setData(Qt::DisplayRole, tr("Input extension altered"));
            item->setData(Qt::ToolTipRole,
                          tr("The parameters controlling how much the edit directive requires additional input was modified."));
            item->setData(Qt::WhatsThisRole,
                          tr("This indicates that the parameters controlling the amount of additional data the edit required was modified."));
        } else if (Util::equal_insensitive(type, "AuthorChanged")) {
            item->setData(Qt::DisplayRole, tr("Author changed from \"%1\"").arg(
                    historyData.hash("OriginalAuthor").toDisplayString()));
            item->setData(Qt::ToolTipRole, tr("The author of the edit directive was altered."));
            item->setData(Qt::WhatsThisRole,
                          tr("This indicates that author field of the edit directive was altered."));
        } else if (Util::equal_insensitive(type, "CommentChanged")) {
            item->setData(Qt::DisplayRole, tr("Comment changed"));
            item->setData(Qt::ToolTipRole,
                          tr("The comment field of the edit directive was altered."));
            item->setData(Qt::WhatsThisRole,
                          tr("This indicates that comment field of the edit directive was altered."));
        } else if (Util::equal_insensitive(type, "PriorityChanged")) {
            qint64 p = historyData.hash("OriginalPriority").toInt64();
            if (!INTEGER::defined(p)) {
                item->setData(Qt::DisplayRole, tr("Priority set"));
            } else {
                item->setData(Qt::DisplayRole, tr("Priority changed from %1").arg(p));
            }
            item->setData(Qt::ToolTipRole, tr("The execution priority was altered."));
            item->setData(Qt::WhatsThisRole,
                          tr("This indicates that execution priority level of the edit directive was altered."));
        } else if (Util::equal_insensitive(type, "SystemInternalChanged")) {
            if (historyData.hash("OriginalSystemInternal").toBool()) {
                item->setData(Qt::DisplayRole, tr("Flagged as user visible"));
            } else {
                item->setData(Qt::DisplayRole, tr("Flagged as user hidden"));
            }
            item->setData(Qt::ToolTipRole, tr("The system internal visibility state was altered."));
            item->setData(Qt::WhatsThisRole,
                          tr("This indicates that the system internal flag was changed altering default user visibility."));
        } else if (Util::equal_insensitive(type, "Disabled")) {
            item->setData(Qt::DisplayRole, tr("Deleted", "edit directive disabled"));
            item->setData(Qt::ToolTipRole, tr("The edit directive was removed."));
            item->setData(Qt::WhatsThisRole,
                          tr("This indicates that the disabled flag of the edit directive was set, indicating that it was deleted and no longer in effect."));
        } else if (Util::equal_insensitive(type, "Enabled")) {
            item->setData(Qt::DisplayRole, tr("Resurrected", "edit directive enabled"));
            item->setData(Qt::ToolTipRole,
                          tr("The edit directive was resurrected from a disabled state."));
            item->setData(Qt::WhatsThisRole,
                          tr("This indicates that the disabled flag of the edit directive was removed, indicating that it should be resurrected from a deleted state."));
        } else {
            item->setData(Qt::DisplayRole, tr("UNKNOWN", "history item unknown"));
            item->setData(Qt::ToolTipRole, tr("This history event was not recognized."));
            item->setData(Qt::WhatsThisRole,
                          tr("This history event indicates an action that the system does not recognize."));
        }
        historyDisplay->setItem(row, 2, item);
    }
    historyDisplay->resizeRowsToContents();

    updated();
}

void EditDirectiveEditor::commentUpdated()
{
    data.hash("Comment").setString(comment->toPlainText());
    updated();
}

void EditDirectiveEditor::authorUpdated(const QString &author)
{
    data.hash("Author").setString(author);
    updated();
}

void EditDirectiveEditor::ebasNoFlagsUpdated()
{
    if (!ebasNoFlags->isChecked()) {
        ebasNoFlags->disconnect(this);
        ebasNoFlags->setChecked(true);
        connect(ebasNoFlags, SIGNAL(toggled(bool)), this, SLOT(ebasNoFlagsUpdated()));
    }

    for (QHash<QString, QAction *>::const_iterator flag = ebasFlagsOptions.constBegin(),
            endFlag = ebasFlagsOptions.constEnd(); flag != endFlag; ++flag) {
        flag.value()->disconnect(this);
        flag.value()->setChecked(false);
        connect(flag.value(), SIGNAL(toggled(bool)), this, SLOT(ebasNoFlagsUpdated()));
    }

    emit ebasFlagsAltered();
}

void EditDirectiveEditor::ebasFlagsUpdated()
{
    Variant::Flags flags;
    for (QHash<QString, QAction *>::const_iterator flag = ebasFlagsOptions.constBegin(),
            endFlag = ebasFlagsOptions.constEnd(); flag != endFlag; ++flag) {
        if (!flag.value()->isChecked())
            continue;
        flags.insert(flag.key().toStdString());
    }
    if (flags.empty()) {
        data.hash("Parameters").hash("EBASFlags").remove();

        ebasNoFlags->disconnect(this);
        ebasNoFlags->setChecked(true);
        connect(ebasNoFlags, SIGNAL(toggled(bool)), this, SLOT(ebasNoFlagsUpdated()));

        updated();
        emit ebasFlagsAltered();
        return;
    }
    data.hash("Parameters").hash("EBASFlags").setFlags(std::move(flags));

    ebasNoFlags->disconnect(this);
    ebasNoFlags->setChecked(false);
    connect(ebasNoFlags, SIGNAL(toggled(bool)), this, SLOT(ebasNoFlagsUpdated()));

    updated();
    emit ebasFlagsAltered();
}

void EditDirectiveEditor::priorityUpdated()
{
    if (!priorityEnable->isChecked()) {
        data.hash("Priority").remove();
        priority->setEnabled(false);
        updated();
        return;
    }

    priority->setEnabled(true);
    data.hash("Priority").setInt64(priority->value());
    updated();
    return;
}

void EditDirectiveEditor::systemInternalUpdated()
{
    data.hash("SystemInternal").setBool(systemInternal->isChecked());
    updated();
    return;
}

void EditDirectiveEditor::actionFragmentUpdated()
{
    switch (actionFragment->checkState()) {
    case Qt::PartiallyChecked:
        data.hash("Parameters").hash("Action").hash("Fragment").remove(false);
        break;
    case Qt::Checked:
        data.hash("Parameters").hash("Action").hash("Fragment").setBool(true);
        break;
    case Qt::Unchecked:
        data.hash("Parameters").hash("Action").hash("Fragment").setBool(false);
        break;
    }
    updated();
    return;
}

void EditDirectiveEditor::fanoutActionUpdated()
{
    fanoutAction.write(data.hash("Parameters").hash("Action").hash("Fanout"));
    updated();
}

void EditDirectiveEditor::fanoutTriggerUpdated()
{
    fanoutTrigger.write(data.hash("Parameters").hash("Trigger").hash("Fanout"));
    updated();
}

void EditDirectiveEditor::averageContinuousUpdated()
{
    if (!averageContinuous->isChecked()) {
        averageContinuousSelection->setEnabled(false);
        data.hash("Average").hash("Continuous").remove(false);
        updated();
        return;
    }

    averageContinuousSelection->setEnabled(true);
    averageContinuousSelection->writeSequenceMatch(data.hash("Average").hash("Continuous"),
                                                   averageContinuousSelectionDefaults);

    updated();
}

void EditDirectiveEditor::averageFullUpdated()
{
    if (!averageFull->isChecked()) {
        averageFullSelection->setEnabled(false);
        data.hash("Average").hash("Full").remove(false);
        updated();
        return;
    }

    averageFullSelection->setEnabled(true);
    averageFullSelection->writeSequenceMatch(data.hash("Average").hash("Full"),
                                             averageFullSelectionDefaults);

    updated();
}

void EditDirectiveEditor::extendUpdated()
{
    if (!extend->isChecked()) {
        extendBounds->setEnabled(false);
        extendData->setEnabled(false);
        extendDataSelection->setEnabled(false);
        extendDataStart->setEnabled(false);
        extendDataStartInterval->setEnabled(false);
        extendDataEnd->setEnabled(false);
        extendDataEndInterval->setEnabled(false);
        data.hash("Extend").remove(false);
        updated();
        return;
    }

    extendBounds->setEnabled(true);
    extendData->setEnabled(true);

    data.hash("Extend").hash("Start").setDouble(extendBounds->getStart());
    data.hash("Extend").hash("End").setDouble(extendBounds->getEnd());

    if (!extendData->isChecked()) {
        extendDataSelection->setEnabled(false);
        extendDataStart->setEnabled(false);
        extendDataStartInterval->setEnabled(false);
        extendDataEnd->setEnabled(false);
        extendDataEndInterval->setEnabled(false);
        data.hash("Extend").hash("Data").remove(false);
        data.hash("Extend").hash("DataStart").remove(false);
        data.hash("Extend").hash("DataEnd").remove(false);
        updated();
        return;
    }

    extendDataSelection->setEnabled(true);
    extendDataStart->setEnabled(true);
    extendDataEnd->setEnabled(true);
    extendDataSelection->writeSequenceMatch(data.hash("Extend").hash("Data"),
                                            extendDataSelectionDefaults);
    if (extendDataStart->isChecked()) {
        extendDataStartInterval->setEnabled(true);
        Variant::Composite::fromTimeInterval(data.hash("Extend").hash("DataStart"),
                                             extendDataStartInterval->getUnit(),
                                             extendDataStartInterval->getCount(),
                                             extendDataStartInterval->getAlign());
    } else {
        extendDataStartInterval->setEnabled(false);
        data.hash("Extend").hash("DataStart").remove(false);
    }
    if (extendDataEnd->isChecked()) {
        extendDataEndInterval->setEnabled(true);
        Variant::Composite::fromTimeInterval(data.hash("Extend").hash("DataEnd"),
                                             extendDataEndInterval->getUnit(),
                                             extendDataEndInterval->getCount(),
                                             extendDataEndInterval->getAlign());
    } else {
        extendDataEndInterval->setEnabled(false);
        data.hash("Extend").hash("DataEnd").remove(false);
    }

    updated();
}

void EditDirectiveEditor::updated()
{
    emit changed();
}

void EditDirectiveEditor::updateSavedAuthor()
{
    QString authorText(author->text().trimmed());
    if (authorText.isEmpty())
        return;

    settings.setValue("user/initials", QVariant(authorText));
}

void EditDirectiveEditor::showEvent(QShowEvent *event)
{
    QWidget::showEvent(event);
    if (event->spontaneous())
        return;

    if (author->text().isEmpty())
        author->setText(settings.value("user/initials").toString());
}


void EditDirectiveEditor::setAvailableStations(const SequenceName::ComponentSet &set)
{
    action->setAvailableStations(set);
    trigger->setAvailableStations(set);
    averageContinuousSelection->setAvailableStations(set);
    averageFullSelection->setAvailableStations(set);
    extendDataSelection->setAvailableStations(set);
}

void EditDirectiveEditor::setAvailableArchives(const SequenceName::ComponentSet &set)
{
    action->setAvailableArchives(set);
    trigger->setAvailableArchives(set);
    averageContinuousSelection->setAvailableArchives(set);
    averageFullSelection->setAvailableArchives(set);
    extendDataSelection->setAvailableArchives(set);
}

void EditDirectiveEditor::setAvailableVariables(const SequenceName::ComponentSet &set)
{
    action->setAvailableVariables(set);
    trigger->setAvailableVariables(set);
    averageContinuousSelection->setAvailableVariables(set);
    averageFullSelection->setAvailableVariables(set);
    extendDataSelection->setAvailableVariables(set);
}

void EditDirectiveEditor::setAvailableFlavors(const SequenceName::ComponentSet &set)
{
    action->setAvailableFlavors(set);
    trigger->setAvailableFlavors(set);
    averageContinuousSelection->setAvailableFlavors(set);
    averageFullSelection->setAvailableFlavors(set);
    extendDataSelection->setAvailableFlavors(set);
}


}
}
}

