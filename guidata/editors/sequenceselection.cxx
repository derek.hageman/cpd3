/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QVBoxLayout>
#include <QDialog>
#include <QDialogButtonBox>
#include <QVBoxLayout>

#include "guidata/editors/sequenceselection.hxx"
#include "datacore/sequencematch.hxx"
#include "guidata/variableselect.hxx"
#include "core/util.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace GUI {
namespace Data {

/** @file guidata/editors/variableselection.hxx
 * Value editors for variable selections.
 */


bool SequenceSelectionEndpoint::matches(const Variant::Read &value, const Variant::Read &metadata)
{
    return Util::equal_insensitive(metadata.metadata("Editor").hash("Type").toString(),
                                   "SequenceSelection");
}

QString SequenceSelectionEndpoint::collapsedText(const Variant::Read &value,
                                                 const Variant::Read &metadata)
{
    SequenceMatch::OrderedLookup sel(value);
    {
        auto u = sel.reduce();
        if (u.isValid()) {
            return u.getVariableQString();
        }
    }

    {
        sel.registerExpected("nil", "raw");
        auto u = sel.knownInputs();
        if (u.size() == 1) {
            return u.cbegin()->getVariableQString();
        }
    }

    return ValueEndpointEditor::collapsedText(value, metadata);
}

bool SequenceSelectionEndpoint::edit(Variant::Write &value,
                                     const Variant::Read &metadata,
                                     QWidget *parent)
{
    QDialog dialog(parent);
    dialog.setWindowTitle(QObject::tr("Sequence Selection"));
    QVBoxLayout *layout = new QVBoxLayout(&dialog);
    dialog.setLayout(layout);

    VariableSelect *selector = new VariableSelect(&dialog);
    selector->configureFromSequenceMatch(value);
    selector->setDefaultAvailable();
    layout->addWidget(selector, 1);

    QDialogButtonBox *buttons =
            new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal,
                                 &dialog);
    layout->addWidget(buttons);
    QObject::connect(buttons, SIGNAL(accepted()), &dialog, SLOT(accept()));
    QObject::connect(buttons, SIGNAL(rejected()), &dialog, SLOT(reject()));

    if (dialog.exec() != QDialog::Accepted)
        return false;

    value.setEmpty();
    selector->writeSequenceMatch(value);
    return true;
}


}
}
}

