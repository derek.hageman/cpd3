/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUIDATAEDITDIRECTIVE_H
#define CPD3GUIDATAEDITDIRECTIVE_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QWidget>
#include <QTextEdit>
#include <QLineEdit>
#include <QPushButton>
#include <QSettings>
#include <QAction>
#include <QHash>
#include <QCheckBox>
#include <QGroupBox>
#include <QTableWidget>
#include <QVBoxLayout>
#include <QShowEvent>

#include "guidata/guidata.hxx"
#include "guidata/editors/edittrigger.hxx"
#include "guidata/editors/editaction.hxx"
#include "guidata/variableselect.hxx"
#include "guicore/timeboundselection.hxx"
#include "datacore/stream.hxx"
#include "datacore/stream.hxx"

namespace CPD3 {
namespace GUI {
namespace Data {

/**
 * A widget that provides an editor for edit directives.
 */
class CPD3GUIDATA_EXPORT EditDirectiveEditor : public QWidget {
Q_OBJECT

    QSettings settings;

    CPD3::Data::Variant::Write data;

    QTextEdit *comment;
    QLineEdit *author;
    QPushButton *ebasFlags;
    QHash<QString, QAction *> ebasFlagsOptions;
    QAction *ebasNoFlags;

    QCheckBox *priorityEnable;
    QSpinBox *priority;
    QCheckBox *systemInternal;

    QCheckBox *actionFragment;

    struct Fanout {
        QCheckBox *station;
        QCheckBox *archive;
        QCheckBox *variable;
        QCheckBox *flavors;
        QCheckBox *flattenStatistics;
        QCheckBox *flattenMetadata;

        static void fromValue(const CPD3::Data::Variant::Read &data, QCheckBox *box);

        static void toValue(CPD3::Data::Variant::Write &data, QCheckBox *box);

        static inline void toValue(CPD3::Data::Variant::Write &&data, QCheckBox *box)
        { return toValue(data, box); }

        QGroupBox *box;

        void initialize(QWidget *parent, QVBoxLayout *layout);

        void configure(const CPD3::Data::Variant::Read &data, QObject *target, const char *slot);

        void write(CPD3::Data::Variant::Write &data);

        inline void write(CPD3::Data::Variant::Write &&data)
        { return write(data); }
    };

    Fanout fanoutTrigger;
    Fanout fanoutAction;

    QGroupBox *averageContinuous;
    VariableSelect *averageContinuousSelection;
    VariableSelect::SequenceMatchDefaults averageContinuousSelectionDefaults;
    QGroupBox *averageFull;
    VariableSelect *averageFullSelection;
    VariableSelect::SequenceMatchDefaults averageFullSelectionDefaults;

    QGroupBox *extend;
    TimeBoundSelection *extendBounds;
    QGroupBox *extendData;
    QCheckBox *extendDataStart;
    TimeIntervalSelection *extendDataStartInterval;
    QCheckBox *extendDataEnd;
    TimeIntervalSelection *extendDataEndInterval;
    VariableSelect *extendDataSelection;
    VariableSelect::SequenceMatchDefaults extendDataSelectionDefaults;

    enum {
        HistoryColumn_Time, HistoryColumn_User, HistoryColumn_Description,
    };
    QTableWidget *historyDisplay;


    EditActionEditor *action;
    EditTriggerEditor *trigger;

    void setEBASMenu(QMenu *target, const QHash<int, QString> &flags);

public:
    EditDirectiveEditor(QWidget *parent = 0);

    virtual ~EditDirectiveEditor();

    const CPD3::Data::Variant::Write &getValue() const;

    void setAvailableStations(const CPD3::Data::SequenceName::ComponentSet &set);

    void setAvailableArchives(const CPD3::Data::SequenceName::ComponentSet &set);

    void setAvailableVariables(const CPD3::Data::SequenceName::ComponentSet &set);

    void setAvailableFlavors(const CPD3::Data::SequenceName::ComponentSet &set);

protected:
    virtual void showEvent(QShowEvent *event);

public slots:

    void configure(CPD3::Data::Variant::Write value);

signals:

    /**
     * Emitted whenever the trigger changes. 
     * 
     * @param value     the trigger content value
     */
    void changed();

    /**
     * Emitted whenever the EBAS flags have been altered in any way.
     */
    void ebasFlagsAltered();

private slots:

    void updated();

    void updateSavedAuthor();

    void authorUpdated(const QString &author);

    void commentUpdated();

    void ebasNoFlagsUpdated();

    void ebasFlagsUpdated();

    void priorityUpdated();

    void systemInternalUpdated();

    void actionFragmentUpdated();

    void fanoutActionUpdated();

    void fanoutTriggerUpdated();

    void averageContinuousUpdated();

    void averageFullUpdated();

    void extendUpdated();
};

}
}
}

#endif
