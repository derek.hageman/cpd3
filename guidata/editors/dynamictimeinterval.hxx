/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUIDATATIMEINTERVALSELECTION_H
#define CPD3GUIDATATIMEINTERVALSELECTION_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QWidget>

#include "guidata/guidata.hxx"
#include "guidata/valueeditor.hxx"
#include "guicore/dynamictimeinterval.hxx"
#include "datacore/dynamictimeinterval.hxx"

namespace CPD3 {
namespace GUI {
namespace Data {

/**
 * A widget to edit time interval selections.
 */
class CPD3GUIDATA_EXPORT DynamicTimeIntervalEditor : public QWidget {
Q_OBJECT

    QCheckBox *enabled;

    TimeIntervalSelection *interval;
    QLabel *undefinedText;
public:

    /**
     * Create the time interval selection editor.
     *
     * @param config    the current configuration
     * @param metadata  the metadata
     * @param defaultConfiguration the override for the default configuration
     * @param editor    the editor data
     * @param parent    the parent widget
     */
    DynamicTimeIntervalEditor(const CPD3::Data::Variant::Read &config,
                              const CPD3::Data::Variant::Read &metadata = CPD3::Data::Variant::Read::empty(),
                              const CPD3::Data::Variant::Read &defaultConfiguration = CPD3::Data::Variant::Read::empty(),
                              const CPD3::Data::Variant::Read &editor = CPD3::Data::Variant::Read::empty(),
                              QWidget *parent = 0);

    /**
     * Override the text displayed by the editor as the label of the enabled selection.
     *
     * @param text  the new text
     */
    void setEnabledText(const QString &text);

    /**
     * Get the result of editing.
     *
     * @return  the resulting smoother configuration
     */
    CPD3::Data::Variant::Root timeInterval() const;

    /**
     * Create a time interval from the contents of the editor.
     *
     * @return  a new time interval
     */
    std::unique_ptr<CPD3::Data::DynamicTimeInterval> create() const;

signals:

    void changed();

private slots:

    void updateEnabled();
};

/**
 * An editor endpoint for time intervals.
 */
class CPD3GUIDATA_EXPORT DynamicTimeIntervalEndpoint : public ValueEndpointEditor {
public:

    virtual bool matches(const CPD3::Data::Variant::Read &value,
                         const CPD3::Data::Variant::Read &metadata);

    virtual bool valid(const CPD3::Data::Variant::Read &value,
                       const CPD3::Data::Variant::Read &metadata);

    bool edit(CPD3::Data::Variant::Write &value,
              const CPD3::Data::Variant::Read &metadata,
              QWidget *parent = 0) override;

    virtual QString collapsedText(const CPD3::Data::Variant::Read &value,
                                  const CPD3::Data::Variant::Read &metadata);
};

}
}
}

#endif
