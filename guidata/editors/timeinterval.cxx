/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QVBoxLayout>
#include <QDialog>
#include <QDialogButtonBox>
#include <QVBoxLayout>

#include "guidata/editors/timeinterval.hxx"
#include "guicore/dynamictimeinterval.hxx"
#include "core/timeutils.hxx"
#include "core/util.hxx"
#include "datacore/variant/composite.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace GUI {
namespace Data {

/** @file guidata/editors/timeinterval.hxx
 * Value editors for time intervals.
 */


bool TimeIntervalEndpoint::matches(const Variant::Read &value, const Variant::Read &metadata)
{
    return Util::equal_insensitive(metadata.metadata("Editor").hash("Type").toString(),
                                   "TimeInterval");
}

bool TimeIntervalEndpoint::valid(const Variant::Read &value, const Variant::Read &metadata)
{
    auto editor = metadata.metadata("Editor");

    int count = 1;
    bool align = editor.hash("DefaultAligned").toBool();
    Variant::Composite::toTimeInterval(value, nullptr, &count, &align);

    if (align && editor.hash("DisableAlign").toBool())
        return false;

    if (count == 0) {
        if (!editor.hash("AllowZero").toBool())
            return false;
    } else if (count < 0) {
        if (!editor.hash("AllowNegative").toBool())
            return false;
    }

    return ValueEndpointEditor::valid(value, metadata);
}

QString TimeIntervalEndpoint::collapsedText(const Variant::Read &value,
                                            const Variant::Read &metadata)
{
    auto editor = metadata.metadata("Editor");

    Time::LogicalTimeUnit unit = Time::LogicalTimeUnit::Second;
    int count = 1;
    bool align = editor.hash("DefaultAligned").toBool();
    Variant::Composite::toTimeInterval(value, &unit, &count, &align);

    if (editor.hash("DisableCount").toBool()) {
        switch (unit) {
        case Time::Millisecond:
            if (align)
                return ValueEditor::tr("Milliseconds aligned");
            return ValueEditor::tr("Milliseconds");
        case Time::Second:
            if (align)
                return ValueEditor::tr("Seconds aligned");
            return ValueEditor::tr("Seconds");
        case Time::Minute:
            if (align)
                return ValueEditor::tr("Minutes aligned");
            return ValueEditor::tr("Minutes");
        case Time::Hour:
            if (align)
                return ValueEditor::tr("Hours aligned");
            return ValueEditor::tr("Hours");
        case Time::Day:
            if (align)
                return ValueEditor::tr("Days aligned");
            return ValueEditor::tr("Days");
        case Time::Week:
            if (align)
                return ValueEditor::tr("Weeks aligned");
            return ValueEditor::tr("Weeks");
        case Time::Month:
            if (align)
                return ValueEditor::tr("Months aligned");
            return ValueEditor::tr("Months");
        case Time::Quarter:
            if (align)
                return ValueEditor::tr("Quarters aligned");
            return ValueEditor::tr("Quarters");
        case Time::Year:
            if (align)
                return ValueEditor::tr("Years aligned");
            return ValueEditor::tr("Years");
        }
    }

    return Time::describeOffset(unit, count, align);
}

bool TimeIntervalEndpoint::edit(Variant::Write &value,
                                const Variant::Read &metadata,
                                QWidget *parent)
{
    auto editor = metadata.metadata("Editor");

    Time::LogicalTimeUnit unit = Time::LogicalTimeUnit::Second;
    int count = 1;
    bool align = editor.hash("DefaultAligned").toBool();
    Variant::Composite::toTimeInterval(value, &unit, &count, &align);

    QDialog dialog(parent);
    dialog.setWindowTitle(QObject::tr("Time Interval"));
    QVBoxLayout *layout = new QVBoxLayout(&dialog);
    dialog.setLayout(layout);

    TimeIntervalSelection *selector = new TimeIntervalSelection(unit, count, align, &dialog);
    selector->setAllowZero(editor.hash("AllowZero").toBool());
    selector->setAllowAlign(!editor.hash("DisableAlign").toBool());
    selector->setAllowNegative(editor.hash("AllowNegative").toBool());
    layout->addWidget(selector, 1);

    QDialogButtonBox *buttons =
            new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal,
                                 &dialog);
    layout->addWidget(buttons);
    QObject::connect(buttons, SIGNAL(accepted()), &dialog, SLOT(accept()));
    QObject::connect(buttons, SIGNAL(rejected()), &dialog, SLOT(reject()));

    if (dialog.exec() != QDialog::Accepted)
        return false;

    unit = selector->getUnit();
    align = selector->getAlign();
    count = selector->getCount();

    value.setEmpty();

    if (editor.hash("DisableAlign").toBool()) {
        if (editor.hash("DisableCount").toBool()) {
            Variant::Composite::fromTimeInterval(value, unit);
        } else {
            Variant::Composite::fromTimeInterval(value, unit, count);
        }
    } else {
        if (editor.hash("DisableCount").toBool()) {
            Variant::Composite::fromTimeInterval(value, unit);
            value.hash("Align").setBool(align);
        } else {
            Variant::Composite::fromTimeInterval(value, unit, count, align);
        }
    }

    return true;
}


}
}
}

