/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUIDATAFILTERINPUT_H
#define CPD3GUIDATAFILTERINPUT_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QTabWidget>
#include <QLineEdit>

#include "guidata/guidata.hxx"
#include "guidata/valueeditor.hxx"
#include "guidata/variableselect.hxx"
#include "guicore/calibrationedit.hxx"

namespace CPD3 {
namespace GUI {
namespace Data {

/**
 * An editor endpoint for time intervals.
 */
class CPD3GUIDATA_EXPORT DynamicInputEndpoint : public ValueEndpointEditor {
public:

    virtual bool matches(const CPD3::Data::Variant::Read &value,
                         const CPD3::Data::Variant::Read &metadata);

    bool edit(CPD3::Data::Variant::Write &value,
              const CPD3::Data::Variant::Read &metadata,
              QWidget *parent = 0) override;
};

/**
 * A dialog for simple dynamic inputs.
 */
class CPD3GUIDATA_EXPORT DynamicInputEditorDialog : public QDialog {
Q_OBJECT

    QTabWidget *typeSelect;

    QLineEdit *constantEditor;

    VariableSelect *dynamicSelection;
    CalibrationEdit *dynamicCalibration;
    QLineEdit *dynamicPath;
    QLineEdit *dynamicDefault;

public:
    /**
     * Create the baseline smoother editing dialog.
     *
     * @param config    the current configuration
     * @param metadata  the metadata for the smoother
     * @param parent    the parent widget
     */
    DynamicInputEditorDialog(const CPD3::Data::Variant::Read &config,
                             const CPD3::Data::Variant::Read &metadata = CPD3::Data::Variant::Read::empty(),
                             QWidget *parent = 0);


    /**
     * Get the result of editing.
     *
     * @return  the resulting smoother configuration
     */
    CPD3::Data::Variant::Root input() const;


private slots:

    void constantValueChanged();

};

}
}
}

#endif
