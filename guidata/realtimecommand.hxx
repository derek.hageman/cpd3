/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUIDATAREALTIMECOMMAND_H
#define CPD3GUIDATAREALTIMECOMMAND_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QDialog>
#include <QComboBox>

#include "guidata/guidata.hxx"
#include "datacore/variant/root.hxx"
#include "acquisition/acquisitionnetwork.hxx"

namespace CPD3 {
namespace GUI {
namespace Data {

/**
 * A dialog for entering a realtime message log entry.
 */
class CPD3GUIDATA_EXPORT RealtimeCommand : public QDialog {
Q_OBJECT

    bool showMainDialog;

    CPD3::Data::Variant::Root command;

    QList<QString> sortedTargets;
    QComboBox *targetSelection;
public:
    /**
     * Create a realtime command dialog.  The command is expected to provide
     * both the "Targets" and "Command" field so it can be correctly issued.
     * No signal is directly sent to the client (only the signal on this
     * dialog is emitted).
     * 
     * @param command       the command data
     * @param client        the client to look up data (if available)
     * @param parent        the parent
     */
    RealtimeCommand(const CPD3::Data::Variant::Read &command,
                    Acquisition::AcquisitionNetworkClient *client = nullptr,
                    QWidget *parent = 0);

    virtual ~RealtimeCommand();

signals:

    /**
     * Emitted when the command is accepted and completed.
     * 
     * @param target    the target interface
     * @param command   the command data
     */
    void issueCommandCompleted(QString target, CPD3::Data::Variant::Root command);

public slots:

    /**
     * Show the realtime command dialog (this may not actually show anything
     * if it requires no input).
     * 
     * @return  the dialog status code (Qt:Accepted)
     * @see QDialog::exec()
     */
    int exec();

private slots:

    void setHexValue(const QString &value);

    void setBooleanValue(bool value);

    void setEnumValue(int index);

    void setDecimalValue(double value);
};

}
}
}

#endif
