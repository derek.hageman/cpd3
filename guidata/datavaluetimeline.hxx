/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUIDATADATAVALUETIMELINE_H
#define CPD3GUIDATADATAVALUETIMELINE_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QWidget>
#include <QList>
#include <QGridLayout>
#include <QScrollBar>
#include <QSettings>

#include "guidata/guidata.hxx"
#include "graphing/axistickgenerator.hxx"
#include "datacore/stream.hxx"
#include "core/timeutils.hxx"

namespace CPD3 {
namespace GUI {
namespace Data {

namespace Internal {

class DataValueTimelineAxis;

class DataValueTimelineValues;

struct DataValueTimelineLayoutItem : public Time::Bounds {
    CPD3::Data::SequenceIdentity value;

    inline DataValueTimelineLayoutItem(const CPD3::Data::SequenceIdentity &v,
                                       double start,
                                       double end)
            : Time::Bounds(start, end), value(v)
    { }

    inline DataValueTimelineLayoutItem() : Time::Bounds(), value()
    { }
};

}

/**
 * Provides a timeline widget for SequenceValue(s).
 */
class CPD3GUIDATA_EXPORT DataValueTimeline : public QWidget {
Q_OBJECT

    /**
     * This property holds the current visible start time.  By default the
     * visible range is autoranging.  Setting this disables autoranging.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setVisibleStart(double)
     *  <li> double getVisibleStart() const
     * </ul>
     * @see autoranging, setAutoranging(bool)
     */
    Q_PROPERTY(double visibleStart
                       READ getVisibleStart
                       WRITE
                       setVisibleStart)

    /**
     * This property holds the current visible end time.  By default the
     * visible range is autoranging.  Setting this disables autoranging.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setVisibleEnd(double)
     *  <li> double getVisibleEnd() const
     * </ul>
     * @see autoranging, setAutoranging(bool)
     */
    Q_PROPERTY(double visibleEnd
                       READ getVisibleEnd
                       WRITE
                       setVisibleEnd)

    /**
     * This property holds if the timeline is autoranging.  If set then the
     * displayed bounds are determined by the current values, otherwise
     * they are determined by the visible range.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setAutorangingbool)
     *  <li> bool getAutoranging() const
     * </ul>
     * @see visibleStart, visibleEnd
     */
    Q_PROPERTY(bool autoranging
                       READ getAutoranging
                       WRITE
                       setAutoranging)

    CPD3::Data::SequenceIdentity::Transfer values;
    CPD3::Data::SequenceIdentity::Set highlight;

    bool autoranging;
    double visibleStart;
    double visibleEnd;
    double displayStart;
    double displayEnd;

    bool haveInfiniteStart;
    bool haveInfiniteEnd;

    QSettings settings;
    QScrollBar *scrollBar;
    Internal::DataValueTimelineAxis *axisDisplay;
    Internal::DataValueTimelineValues *valuesDisplay;

    CPD3::Graphing::AxisTickGeneratorTime *axisGenerator;

    friend class Internal::DataValueTimelineAxis;

    friend class Internal::DataValueTimelineValues;

    std::vector<CPD3::Graphing::AxisTickGenerator::Tick> majorTicks;
    std::vector<CPD3::Graphing::AxisTickGenerator::Tick> minorTicks;

    QList<CPD3::Data::SequenceIdentity::Transfer> defaultLayoutValues;
    QList<CPD3::Data::SequenceIdentity::Transfer> mainLayoutValues;

    void regenerateTicks();

    void updateRanges();

    void updateLayout();

    void clickValue(const CPD3::Data::SequenceIdentity &value, Qt::MouseButton button);

public:
    DataValueTimeline(QWidget *parent = 0);

    virtual ~DataValueTimeline();

    CPD3::Data::SequenceIdentity::Transfer getValues() const;

    bool getAutoranging() const;

    void setAutoranging(bool a);

    double getVisibleStart() const;

    double getVisibleEnd() const;

public slots:

    void setValues(const CPD3::Data::SequenceIdentity::Transfer &values);
    void setValues(const CPD3::Data::SequenceValue::Transfer &values);

    void ensureVisible(const CPD3::Data::SequenceIdentity &value);
    void ensureVisible(const CPD3::Data::SequenceValue &value);

    void highlightValue(const CPD3::Data::SequenceIdentity &value);
    void highlightValue(const CPD3::Data::SequenceValue &value);

    void highlightValues(const CPD3::Data::SequenceValue::Transfer &values);

    void highlightValues(const CPD3::Data::SequenceIdentity::Set &values);

    void setVisibleStart(double v);

    void setVisibleEnd(double v);

    void setVisible(double start, double end);

signals:

    void valueClicked(const CPD3::Data::SequenceIdentity &value);

    void valueRightClicked(const CPD3::Data::SequenceIdentity &value);
};

}
}
}

#endif
