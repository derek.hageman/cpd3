/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QString>
#include <QTest>
#include <QList>
#include <QBuffer>

#include "guidata/valueclipboard.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/externalsink.hxx"
#include "datacore/variant/root.hxx"

using namespace CPD3::GUI::Data;
using namespace CPD3::Data;

namespace CPD3 {
namespace Data {
namespace Variant {
bool operator==(const Root &a, const Root &b)
{ return a.read() == b.read(); }
}
}
}

class TestValueClipboard : public QObject {
Q_OBJECT
private slots:

    void encodeDecode()
    {
        std::vector<Variant::Root> input;
        input.emplace_back(Variant::Root(123.0));
        input.emplace_back(Variant::Root("abc"));

        QByteArray encoded(ValueClipboard::encode(input));
        QCOMPARE(ValueClipboard::decodeSingle(encoded).read().toDouble(), 123.0);
        QCOMPARE(ValueClipboard::decode(encoded), input);

        {
            SequenceValue::Transfer dvl
                    {SequenceValue(SequenceName("brw", "raw", "T_S11"), Variant::Root(123.0), 510.0,
                                   123454.0, 1),
                     SequenceValue(SequenceName("brw", "raw", "T_S11"), Variant::Root("abc"), 678.0,
                                   123454.0, 1)};
            encoded.clear();
            QBuffer outBuffer(&encoded);
            outBuffer.open(QIODevice::WriteOnly);
            StandardDataOutput out(&outBuffer, StandardDataOutput::OutputType::XML);
            out.start();
            out.incomingData(dvl);
            out.endData();
            CPD3::Threading::pollInEventLoop([&] { return !out.isFinished(); });
            out.wait();
            QCoreApplication::processEvents();
            QCOMPARE(ValueClipboard::decode(encoded), input);
        }

        Variant::Root v;
        v["/a/b/c"] = 1.0;
        v["/a/b/d"] = 2.0;
        v["/a/e/f"] = 3.0;
        encoded = ValueClipboard::encode(v["/a/b"]);
        Variant::Root v2 = ValueClipboard::decodeSingle(encoded);
        v["/a/e"].remove();
        QCOMPARE(v2.read(), v.read());
    }

    void mimedata()
    {
        Variant::Root v;
        v["/a/b/c"] = 1.0;
        v["/a/b/d"] = 2.0;
        v["/a/e/f"] = 3.0;
        std::vector<Variant::Root> vl;
        vl.emplace_back(v);

        ValueMimeData d(vl);
        QVERIFY(d.hasFormat("text/plain"));
        QVERIFY(d.hasText());
        QCOMPARE(ValueMimeData::fromMimeData(&d), vl);
    }
};

QTEST_MAIN(TestValueClipboard)

#include "valueclipboard.moc"
