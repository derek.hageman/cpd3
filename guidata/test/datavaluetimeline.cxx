/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <time.h>
#include <math.h>
#include <QtGlobal>
#include <QTest>
#include <QString>
#include <QStringList>

#include "guidata/datavaluetimeline.hxx"
#include "datacore/stream.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::GUI::Data;

namespace CPD3 {
namespace Data {
bool operator==(const SequenceValue &a, const SequenceValue &b)
{ return SequenceValue::ValueEqual()(a, b); }
}
}

class TestDataValueTimeline : public QObject {
Q_OBJECT
private slots:

    void initTestCase()
    {
        qRegisterMetaType<SequenceValue>("CPD3::Data::SequenceValue");
    }

    void properties()
    {
        DataValueTimeline tl;

        QVERIFY(!FP::defined(tl.getVisibleStart()));
        QVERIFY(!FP::defined(tl.getVisibleEnd()));
        tl.setVisibleStart(100.0);
        QCOMPARE(tl.getVisibleStart(), 100.0);
        tl.setVisibleEnd(110.0);
        QCOMPARE(tl.getVisibleEnd(), 110.0);
        tl.setVisible(110.0, 120.0);
        QCOMPARE(tl.getVisibleStart(), 110.0);
        QCOMPARE(tl.getVisibleEnd(), 120.0);
        tl.setVisibleStart(FP::undefined());
        QVERIFY(!FP::defined(tl.getVisibleStart()));
        tl.setVisibleEnd(FP::undefined());
        QVERIFY(!FP::defined(tl.getVisibleEnd()));

        QCOMPARE(tl.getAutoranging(), true);
        tl.setAutoranging(false);
        QCOMPARE(tl.getAutoranging(), false);
        tl.setAutoranging(true);
        QCOMPARE(tl.getAutoranging(), true);

        QVERIFY(tl.getValues().empty());
        SequenceIdentity::Transfer v
                {SequenceIdentity(SequenceName("brw", "raw", "BsG_S11"), 315532800, 946684800, 0),
                 SequenceIdentity(SequenceName("brw", "raw", "BsG_S11"), 631152000, 946684800, 1)};
        tl.setValues(v);
        QCOMPARE(tl.getValues(), v);
    }
};

QTEST_MAIN(TestDataValueTimeline)

#include "datavaluetimeline.moc"
