/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <time.h>
#include <math.h>
#include <QtGlobal>
#include <QTest>
#include <QString>
#include <QStringList>

#include "guidata/valueeditor.hxx"
#include "datacore/variant/root.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::GUI::Data;
using namespace CPD3::Data;

class TestValueEditor : public QObject {
Q_OBJECT
private slots:

    void properties()
    {
        ValueEditor ve;

        /*QCOMPARE(ve.getEditingMode(), ValueEditor::MetadataOnly);
        ve.setEditingMode(ValueEditor::ReadOnly);
        QCOMPARE(ve.getEditingMode(), ValueEditor::ReadOnly);
        ve.setEditingMode(ValueEditor::NoRestriction);
        QCOMPARE(ve.getEditingMode(), ValueEditor::NoRestriction);
        ve.setEditingMode(ValueEditor::MetadataOnly);
        QCOMPARE(ve.getEditingMode(), ValueEditor::MetadataOnly);

        QVERIFY(ve.getShowPossible());
        ve.setShowPossible(false);
        QVERIFY(!ve.getShowPossible());
        ve.setShowPossible(true);
        QVERIFY(ve.getShowPossible());*/
    }
};

QTEST_MAIN(TestValueEditor)

#include "valueeditor.moc"
