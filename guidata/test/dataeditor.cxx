/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <time.h>
#include <math.h>
#include <QtGlobal>
#include <QTest>
#include <QString>
#include <QStringList>

#include "guidata/dataeditor.hxx"
#include "datacore/stream.hxx"
#include "core/number.hxx"

using namespace CPD3;
using namespace CPD3::GUI::Data;
using namespace CPD3::Data;

class TestDataEditor : public QObject {
Q_OBJECT
private slots:

    void initTestCase()
    {
        qRegisterMetaType<SequenceValue>("CPD3::Data::SequenceValue");
    }

    void properties()
    {
        DataEditor editor;

        /* Set this first, so nothing below returns anything unexpected */
        QCOMPARE(editor.getSelectionFlags(), DataEditor::DefaultSelectionFlags);
        editor.setSelectionFlags(DataEditor::SelectEverything);
        QCOMPARE(editor.getSelectionFlags(), DataEditor::SelectEverything);

        QVERIFY(editor.getStation().empty());
        editor.setStation("brw");
        QCOMPARE(editor.getStation(), CPD3::Data::SequenceName::Component("brw"));
        QVERIFY(editor.getArchive().empty());
        editor.setArchive("raw");
        QCOMPARE(editor.getArchive(), CPD3::Data::SequenceName::Component("raw"));
        QVERIFY(editor.getVariable().empty());
        editor.setVariable("BsG_S11");
        QCOMPARE(editor.getVariable(), CPD3::Data::SequenceName::Component("BsG_S11"));
        QVERIFY(editor.getHasFlavors().empty());
        editor.setHasFlavors(CPD3::Data::SequenceName::Flavors{"pm10"});
        QCOMPARE(editor.getHasFlavors(), std::vector<CPD3::Data::SequenceName::Component>{"pm10"});
        QVERIFY(editor.getLacksFlavors().empty());
        editor.setLacksFlavors(CPD3::Data::SequenceName::Flavors{"stddev"});
        QCOMPARE(editor.getLacksFlavors(), std::vector<CPD3::Data::SequenceName::Component>{"stddev"});
        QVERIFY(!FP::defined(editor.getStart()));
        editor.setStart(1000.0);
        QCOMPARE(editor.getStart(), 1000.0);
        QVERIFY(!FP::defined(editor.getEnd()));
        editor.setEnd(1001.0);
        QCOMPARE(editor.getEnd(), 1001.0);
        editor.setBounds(FP::undefined(), FP::undefined());
        QVERIFY(!FP::defined(editor.getStart()));
        QVERIFY(!FP::defined(editor.getEnd()));

        editor.setAvailableStations({"brw"});
        editor.setAvailableArchives({"raw"});
        editor.setAvailableVariables({"BsG_S11"});
        editor.setAvailableFlavors({"pm10", "stddev"});

        /* Now change this to enforce constraints */
        editor.setSelectionFlags(DataEditor::SelectFull |
                                         DataEditor::SelectAllStations |
                                         DataEditor::SelectAllArchives |
                                         DataEditor::SelectAllVariables);
        QCOMPARE(editor.getSelectionFlags(), DataEditor::SelectFull |
                DataEditor::SelectAllStations |
                DataEditor::SelectAllArchives |
                DataEditor::SelectAllVariables);

        QCOMPARE(editor.getStation(), CPD3::Data::SequenceName::Component("brw"));
        QCOMPARE(editor.getArchive(), CPD3::Data::SequenceName::Component("raw"));
        QCOMPARE(editor.getVariable(), CPD3::Data::SequenceName::Component("BsG_S11"));
        QCOMPARE(editor.getHasFlavors(), std::vector<CPD3::Data::SequenceName::Component>{"pm10"});
        QCOMPARE(editor.getLacksFlavors(), std::vector<CPD3::Data::SequenceName::Component>{"stddev"});

        editor.setAvailableStations({"sgp"});
        QVERIFY(editor.getStation().empty());
        editor.setAvailableArchives({"clean"});
        QVERIFY(editor.getArchive().empty());
        editor.setAvailableVariables({"BsB_S11"});
        QVERIFY(editor.getVariable().empty());
        editor.setAvailableFlavors({"pm1"});
        QVERIFY(editor.getHasFlavors().empty());
        QVERIFY(editor.getLacksFlavors().empty());
    }
};

QTEST_MAIN(TestDataEditor)

#include "dataeditor.moc"
