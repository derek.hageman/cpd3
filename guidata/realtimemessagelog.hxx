/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUIDATAREALTIMEMESSAGELOG_H
#define CPD3GUIDATAREALTIMEMESSAGELOG_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QDialog>
#include <QSettings>
#include <QTextEdit>
#include <QLineEdit>
#include <QDialogButtonBox>

#include "guidata/guidata.hxx"
#include "datacore/variant/root.hxx"

namespace CPD3 {
namespace GUI {
namespace Data {

/**
 * A dialog for entering a realtime message log entry.
 */
class CPD3GUIDATA_EXPORT RealtimeMessageLog : public QDialog {
Q_OBJECT

    QSettings settings;

    QTextEdit *editor;
    QLineEdit *author;
    QDialogButtonBox *buttons;

public:
    RealtimeMessageLog(QWidget *parent = 0);

    virtual ~RealtimeMessageLog();

signals:

    void messageLogEvent(CPD3::Data::Variant::Root event);

private slots:

    void emitMessageLog();

    void updateAcceptable();

    void updateSavedAuthor();

protected:
    virtual void showEvent(QShowEvent *event);
};

}
}
}

#endif
