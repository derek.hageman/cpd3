/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <QFormLayout>
#include <QLabel>
#include <QDialogButtonBox>
#include <QPushButton>
#include <QValidator>
#include <QMessageBox>
#include <QLineEdit>
#include <QCheckBox>
#include <QComboBox>
#include <QDoubleSpinBox>

#include "guidata/realtimecommand.hxx"
#include "guicore/exponentedit.hxx"
#include "datacore/variant/composite.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace GUI {
namespace Data {

/** @file guidata/realtimecommand.hxx
 * A common interface to issuing commands to the realtime system.
 */

namespace {
struct ParameterSortData {
    QString name;
    qint64 sortPriority;
    QString label;

    bool operator<(const ParameterSortData &other) const
    {
        if (!INTEGER::defined(sortPriority)) {
            if (INTEGER::defined(other.sortPriority))
                return false;
        } else if (!INTEGER::defined(other.sortPriority)) {
            return true;
        } else if (sortPriority != other.sortPriority) {
            return sortPriority < other.sortPriority;
        }
        return label < other.label;
    }
};

class HexValidator : public QValidator {
    int digits;
public:
    HexValidator(int d = 4, QObject *p = 0) : QValidator(p), digits(d)
    { }

    virtual ~HexValidator()
    { }

    virtual void fixup(QString &input) const
    {
        if (input.startsWith("0x", Qt::CaseInsensitive))
            input.remove(0, 2);

        bool ok = false;
        qint64 value = input.toLongLong(&ok, 16);
        if (!ok || value < 0)
            return;
        if (!INTEGER::defined(value))
            return;
        input = QString::number(value, 16).rightJustified(digits, QChar('0'));
    }

    virtual QValidator::State validate(QString &input, int &pos) const
    {
        Q_UNUSED(pos);

        QString::const_iterator ch = input.constBegin();
        if (input.startsWith("0x", Qt::CaseInsensitive))
            ch += 2;

        for (QString::const_iterator endCH = input.constEnd(); ch != endCH; ++ch) {
            if (ch->isDigit())
                continue;
            QChar check(ch->toLower());
            if (check == 'a' ||
                    check == 'b' ||
                    check == 'c' ||
                    check == 'd' ||
                    check == 'e' ||
                    check == 'f')
                continue;
            return QValidator::Invalid;
        }
        return QValidator::Acceptable;
    }
};

};

RealtimeCommand::RealtimeCommand(const CPD3::Data::Variant::Read &cmd,
                                 Acquisition::AcquisitionNetworkClient *client,
                                 QWidget *parent) : QDialog(parent),
                                                    showMainDialog(false),
                                                    command(cmd),
                                                    sortedTargets(),
                                                    targetSelection(NULL)
{
    QList<QString> targets;
    switch (command["Targets"].getType()) {
    case Variant::Type::String:
        targets.append(command["Targets"].toQString());
        break;
    case Variant::Type::Array: {
        for (auto add : command["Targets"].toArray()) {
            QString s(add.toQString());
            if (s.isEmpty())
                continue;
            targets.append(s);
        }
        break;
    }
    default:
        break;
    }

    QSet<QString> uniqueTargets;
    if (client) {
        auto allInterfaces = client->getInterfaceInformation();
        for (QList<QString>::const_iterator check = targets.constBegin(),
                endCheck = targets.constEnd(); check != endCheck; ++check) {
            QRegExp reCheck(*check);
            for (const auto &interface : allInterfaces) {
                if (!reCheck.exactMatch(QString::fromStdString(interface.first)))
                    continue;
                uniqueTargets.insert(QString::fromStdString(interface.first));
            }
        }
    }


    QList<ParameterSortData> parameters;
    if (command["Parameters"].exists()) {
        for (auto param : command["Parameters"].toHash()) {
            if (param.first.empty())
                continue;
            ParameterSortData add;
            add.name = QString::fromStdString(param.first);
            add.sortPriority = param.second.hash("SortPriority").toInt64();
            add.label = param.second.hash("Name").toDisplayString();
            if (add.label.isEmpty())
                add.label = add.name;
            parameters.append(add);
        }

        std::sort(parameters.begin(), parameters.end());
    }

    QVBoxLayout *topLayout = new QVBoxLayout;
    setLayout(topLayout);

    QWidget *formWidget = new QWidget(this);
    topLayout->addWidget(formWidget);
    QFormLayout *layout = new QFormLayout;
    formWidget->setLayout(layout);

    if (uniqueTargets.size() > 1) {
        showMainDialog = true;

        std::vector<std::pair<QString, QString>> sortList;
        for (const auto &add : uniqueTargets) {
            QString name;
            if (client) {
                name = client->getInterfaceInformation(
                        name.toStdString())["MenuEntry"].toDisplayString();
            }
            if (name.isEmpty())
                name = add;

            sortList.emplace_back(add, name);
        }
        std::sort(sortList.begin(), sortList.end(),
                  [](const std::pair<QString, QString> &a, const std::pair<QString, QString> &b) {
                      return a.second < b.second;
                  });

        targetSelection = new QComboBox(formWidget);
        targetSelection->setToolTip(tr("Select the interface to send the command to."));
        targetSelection->setStatusTip(tr("Command targets"));
        targetSelection->setWhatsThis(
                tr("This allows you to select which interface the command targets.  Selecting \"All\" will send the command to all listed interfaces."));
        layout->addRow(tr("Send command to:"), targetSelection);

        targetSelection->addItem(tr("All", "all targets"));

        for (const auto &add : sortList) {
            sortedTargets.append(add.first);
            targetSelection->addItem(add.second);
        }
    } else if (uniqueTargets.size() == 1) {
        sortedTargets = uniqueTargets.values();
    } else if (targets.isEmpty()) {
        showMainDialog = false;
        return;
    } else {
        sortedTargets = targets;
    }

    if (!parameters.isEmpty()) {
        showMainDialog = true;

        auto data = command["Parameters"].toHash();

        for (const auto &add : parameters) {
            auto param = data[add.name];

            const auto &type = param.hash("Type").toString();
            if (Util::equal_insensitive(type, "hex", "hexadecimal")) {
                NumberFormat format(param.hash("Format").toDisplayString());
                format.setMode(NumberFormat::Hex);

                QLineEdit *editor = new QLineEdit(formWidget);
                editor->setValidator(new HexValidator(format.getIntegerDigits(), editor));
                editor->setToolTip(param.hash("ToolTip").toDisplayString());
                editor->setStatusTip(param.hash("StatusTip").toDisplayString());
                editor->setWhatsThis(param.hash("WhatsThis").toDisplayString());
                editor->setProperty("parameterName", add.name);
                editor->setText(format.apply(
                        static_cast<qint64>(Variant::Composite::toInteger(param.hash("Value")))));
                connect(editor, SIGNAL(textEdited(
                                               const QString &)), this, SLOT(setHexValue(
                                                                                     const QString &)));

                layout->addRow(tr("%1:", "parameter label").arg(add.label), editor);
            } else if (Util::equal_insensitive(type, "bool", "boolean")) {
                QCheckBox *editor = new QCheckBox(add.label, formWidget);
                editor->setToolTip(param.hash("ToolTip").toDisplayString());
                editor->setStatusTip(param.hash("StatusTip").toDisplayString());
                editor->setWhatsThis(param.hash("WhatsThis").toDisplayString());
                editor->setProperty("parameterName", add.name);
                editor->setChecked(param.hash("Value").toBool());
                connect(editor, SIGNAL(toggled(bool)), this, SLOT(setBooleanValue(bool)));

                layout->addRow(editor);
            } else if (Util::equal_insensitive(type, "enum", "enumeration")) {
                std::vector<std::pair<QString, QString>> possible;

                auto values = param.hash("Possible");
                switch (values.getType()) {
                case Variant::Type::String: {
                    for (const auto &v : values.toQString().split(' ')) {
                        possible.emplace_back(v, v);
                    }
                    break;
                }
                default: {
                    auto list = values.toChildren();
                    for (auto v = list.begin(), endV = list.end(); v != endV; ++v) {
                        QString key = QString::fromStdString(v.stringKey());
                        if (key.isEmpty())
                            key = v.value().toQString();
                        possible.emplace_back(std::move(key), v.value().toDisplayString());
                    }
                    break;
                }
                }

                if (possible.empty())
                    continue;

                std::stable_sort(possible.begin(), possible.end(),
                                 [](const std::pair<QString, QString> &a,
                                    const std::pair<QString, QString> &b) {
                                     return a.second < b.second;
                                 });

                QComboBox *editor = new QComboBox(formWidget);
                editor->setToolTip(param.hash("ToolTip").toDisplayString());
                editor->setStatusTip(param.hash("StatusTip").toDisplayString());
                editor->setWhatsThis(param.hash("WhatsThis").toDisplayString());
                editor->setProperty("parameterName", add.name);
                QString current(param.hash("Value").toQString().toLower());
                for (const auto &item : possible) {
                    editor->addItem(item.second, item.first);
                    if (current == item.first.toLower())
                        editor->setCurrentIndex(editor->count() - 1);
                }
                connect(editor, SIGNAL(currentIndexChanged(int)), this, SLOT(setEnumValue(int)));

                layout->addRow(tr("%1:", "parameter label").arg(add.label), editor);
            } else {
                NumberFormat format(param.hash("Format").toDisplayString());
                double minimum = Variant::Composite::toNumber(param.hash("Minimum"));
                double maximum = Variant::Composite::toNumber(param.hash("Maximum"));
                double value = Variant::Composite::toNumber(param.hash("Value"));

                QWidget *editor;
                if (format.getMode() == NumberFormat::Scientific ||
                        !FP::defined(minimum) ||
                        !FP::defined(maximum) ||
                        minimum >= maximum) {
                    ExponentEdit *ex = new ExponentEdit(formWidget);
                    editor = ex;
                    ex->setMinimum(minimum);
                    ex->setMaximum(maximum);
                    ex->setDecimals(format.getDecimalDigits());
                    if (FP::defined(value))
                        ex->setValue(value);

                    connect(ex, SIGNAL(valueChanged(double)), this, SLOT(setDecimalValue(double)));
                } else {
                    QDoubleSpinBox *spin = new QDoubleSpinBox(formWidget);
                    editor = spin;
                    spin->setRange(minimum, maximum);
                    spin->setDecimals(format.getDecimalDigits());
                    if (FP::defined(value))
                        spin->setValue(value);
                    {
                        int nDig = (int) floor(log10(maximum - minimum)) - 1;
                        nDig = qMax(nDig, -format.getDecimalDigits());
                        spin->setSingleStep(pow(10.0, nDig));
                    }

                    connect(spin, SIGNAL(valueChanged(double)), this,
                            SLOT(setDecimalValue(double)));
                }

                editor->setToolTip(param.hash("ToolTip").toDisplayString());
                editor->setStatusTip(param.hash("StatusTip").toDisplayString());
                editor->setWhatsThis(param.hash("WhatsThis").toDisplayString());
                editor->setProperty("parameterName", add.name);
                layout->addRow(tr("%1:", "parameter label").arg(add.label), editor);
            }
        }
    }

    QDialogButtonBox *buttons = new QDialogButtonBox(this);
    buttons->addButton(QDialogButtonBox::Ok);
    buttons->addButton(QDialogButtonBox::Cancel);
    connect(buttons, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttons, SIGNAL(rejected()), this, SLOT(reject()));
    topLayout->addWidget(buttons);
}

RealtimeCommand::~RealtimeCommand()
{ }

void RealtimeCommand::setHexValue(const QString &value)
{
    QString parameterName(sender()->property("parameterName").toString());
    if (parameterName.isEmpty())
        return;

    qint64 i;
    if (value.trimmed().isEmpty()) {
        i = INTEGER::undefined();
    } else {
        bool ok = false;
        i = value.toLongLong(&ok, 16);
        if (!ok)
            return;
    }

    command["Parameters"].hash(parameterName).hash("Value").setInt64(i);
}

void RealtimeCommand::setBooleanValue(bool value)
{
    QString parameterName(sender()->property("parameterName").toString());
    if (parameterName.isEmpty())
        return;
    command["Parameters"].hash(parameterName).hash("Value").setBool(value);
}

void RealtimeCommand::setEnumValue(int index)
{
    QString parameterName(sender()->property("parameterName").toString());
    if (parameterName.isEmpty())
        return;
    QComboBox *editor = qobject_cast<QComboBox *>(sender());
    if (!editor)
        return;
    if (index < 0 || index >= editor->count())
        return;
    command["Parameters"].hash(parameterName)
                         .hash("Value")
                         .setString(editor->itemData(index).toString());
}

void RealtimeCommand::setDecimalValue(double value)
{
    QString parameterName(sender()->property("parameterName").toString());
    if (parameterName.isEmpty())
        return;
    command["Parameters"].hash(parameterName).hash("Value").setDouble(value);
}

int RealtimeCommand::exec()
{
    if (sortedTargets.isEmpty())
        return QDialog::Rejected;

    if (showMainDialog) {
        int code = QDialog::exec();
        if (code != QDialog::Accepted)
            return code;
    }

    QStringList targets;
    if (targetSelection == NULL) {
        targets = sortedTargets;
    } else if (targetSelection->currentIndex() <= 0) {
        targets = sortedTargets;
    } else {
        targets.append(sortedTargets.at(targetSelection->currentIndex() - 1));
    }

    if (command["Confirm"].exists()) {
        QString confirmText(command["Confirm"].toDisplayString());
        if (confirmText.isEmpty())
            confirmText = tr("Continue?", "default confirm");

        QMessageBox box(QMessageBox::Question, tr("Realtime Command to %1").arg(targets.join(",")),
                        confirmText, QMessageBox::NoButton, qobject_cast<QWidget *>(parent()));

        QString yesText(command["ConfirmYes"].toDisplayString());
        if (!yesText.isEmpty()) {
            box.addButton(yesText, QMessageBox::YesRole);
        } else {
            box.addButton(QMessageBox::Yes);
        }

        QString noText(command["ConfirmNo"].toDisplayString());
        if (!noText.isEmpty()) {
            box.addButton(noText, QMessageBox::NoRole);
        } else {
            box.addButton(QMessageBox::No);
        }

        int code = box.exec();
        if (code != QMessageBox::Yes)
            return QDialog::Rejected;
    }

    Variant::Root data;
    data.write().hash(command.read().hash("Command").toString()).set(command);
    for (const auto &target : targets) {
        emit issueCommandCompleted(target, data);
    }

    return QDialog::Accepted;
}

}
}
}
