/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUIDATA_H
#define CPD3GUIDATA_H

#include <QtCore/QtGlobal>

#if defined(cpd3guidata_EXPORTS)
#   define CPD3GUIDATA_EXPORT Q_DECL_EXPORT
#else
#   define CPD3GUIDATA_EXPORT Q_DECL_IMPORT
#endif

#endif
