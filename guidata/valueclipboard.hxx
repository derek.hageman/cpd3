/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUIDATAVALUECLIPBOARD_H
#define CPD3GUIDATAVALUECLIPBOARD_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QMimeData>
#include <QVariant>
#include <QList>
#include <QClipboard>
#include <QMimeData>

#include "guidata/guidata.hxx"
#include "datacore/variant/root.hxx"

namespace CPD3 {
namespace GUI {
namespace Data {

/**
 * Provides a MIME storage interface for Values on the clipboard.
 */
class CPD3GUIDATA_EXPORT ValueMimeData : public QMimeData {
Q_OBJECT

    std::vector<CPD3::Data::Variant::Root> values;
public:
    ValueMimeData();

    ValueMimeData(const ValueMimeData &other);

    ValueMimeData &operator=(const ValueMimeData &other);

    ValueMimeData(ValueMimeData &&other);

    ValueMimeData &operator=(ValueMimeData &&other);

    ValueMimeData(const CPD3::Data::Variant::Root &value);

    ValueMimeData(CPD3::Data::Variant::Root &&value);

    ValueMimeData(const std::vector<CPD3::Data::Variant::Root> &setValues);

    ValueMimeData(std::vector<CPD3::Data::Variant::Root> &&setValues);

    virtual bool hasFormat(const QString &mimeType) const;

    virtual QStringList formats() const;

    static std::vector<CPD3::Data::Variant::Root> fromMimeData(const QMimeData *data);

protected:
    virtual QVariant retrieveData(const QString &mimeType, QVariant::Type type) const;
};

/**
 * Provides routines needed for copy-paste and duplication of values within
 * a tree of them.
 */
class CPD3GUIDATA_EXPORT ValueClipboard {
    ValueClipboard()
    { }

public:
    static QByteArray encode(const CPD3::Data::Variant::Read &value);

    static QByteArray encode(const std::vector<CPD3::Data::Variant::Read> &values);

    static QByteArray encode(const std::vector<CPD3::Data::Variant::Root> &values);

    static CPD3::Data::Variant::Root decodeSingle(const QByteArray &data);

    static std::vector<CPD3::Data::Variant::Root> decode(const QByteArray &data);

    static std::vector<CPD3::Data::Variant::Path> insert(CPD3::Data::Variant::Write &parent,
                                                         const std::vector<
                                                                 CPD3::Data::Variant::Read> &children,
                                                         const CPD3::Data::Variant::Read &metadata = CPD3::Data::Variant::Read::empty(),
                                                         bool strictChildren = false);

    static CPD3::Data::Variant::Path insert(CPD3::Data::Variant::Write &parent,
                                            const CPD3::Data::Variant::Read &child,
                                            const CPD3::Data::Variant::Read &metadata = CPD3::Data::Variant::Read::empty(),
                                            bool strictChildren = false);
};

}
}
}

#endif
