/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef SYNCSERVERGUI_H
#define SYNCSERVERGUI_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QSystemTrayIcon>
#include <QIcon>
#include <QTimer>
#include <QTime>
#include <QMenu>


class SyncServerGUI : public QObject {
Q_OBJECT

    QSystemTrayIcon icon;

    QMenu contextMenu;

public:
    SyncServerGUI();

    virtual ~SyncServerGUI();

signals:

    void exitRequested();

private slots:

    void iconActivated(QSystemTrayIcon::ActivationReason reason);

    void verifyExit();
};

#endif
