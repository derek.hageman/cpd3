/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTranslator>
#include <QLibraryInfo>
#include <QApplication>
#include <QMessageBox>


#include "core/abort.hxx"
#include "core/component.hxx"
#include "core/threadpool.hxx"
#include "sync/server.hxx"

#include "server_gui.hxx"

using namespace CPD3;
using namespace CPD3::Sync;

SyncServerGUI::SyncServerGUI() : icon(QIcon(":/icon.svg"), this)
{
    QAction *act;

    act = new QAction(tr("&Quit", "Context|Quit"), &contextMenu);
    act->setToolTip(tr("Exit the synchronization server"));
    act->setStatusTip(tr("Quit"));
    act->setWhatsThis(
            tr("This exits the synchronization server and closes any active connections."));
    act->setMenuRole(QAction::QuitRole);
    connect(act, SIGNAL(triggered()), this, SLOT(verifyExit()));
    contextMenu.addAction(act);

    icon.setContextMenu(&contextMenu);
    icon.setToolTip(tr("CPD3 synchronization server."));
    icon.show();
}

SyncServerGUI::~SyncServerGUI()
{
}

void SyncServerGUI::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
    case QSystemTrayIcon::Trigger:
        break;
    default:
        break;
    }
}

void SyncServerGUI::verifyExit()
{
    QMessageBox::StandardButton result = QMessageBox::question(0, tr("Confirm Quit"),
                                                               tr("Stop synchronization server?  This will abort any synchronizations in progress."),
                                                               QMessageBox::Yes | QMessageBox::No,
                                                               QMessageBox::No);
    if (result != QMessageBox::Yes)
        return;

    emit exitRequested();
}

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    Q_INIT_RESOURCE(server_gui);

    app.setQuitOnLastWindowClosed(false);
    app.setWindowIcon(QIcon(":/icon.svg"));

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

    QString translateLocation;
    QByteArray cpd3(qgetenv("CPD3"));
    if (cpd3.length() > 0) {
        translateLocation = QString(cpd3) + "/locale";
    }
    QTranslator cpd3Translator;
    cpd3Translator.load("cpd3_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cpd3Translator);
    QTranslator cliTranslator;
    cliTranslator.load("cli_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cliTranslator);
    QTranslator guiTranslator;
    guiTranslator.load("gui_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&guiTranslator);
    QTranslator acquisitionTranslator;
    acquisitionTranslator.load("acquisition_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&acquisitionTranslator);

    Server *server = new Server;
    if (!server->start()) {
        delete server;
        ThreadPool::system()->wait();
        return 1;
    }

    SyncServerGUI *gui = new SyncServerGUI;

    Abort::installAbortHandler(true);

    AbortPoller abortPoller;
    QObject::connect(&abortPoller, SIGNAL(aborted()), server, SLOT(signalTerminate()));
    QObject::connect(gui, SIGNAL(exitRequested()), server, SLOT(signalTerminate()));
    QObject::connect(server, SIGNAL(finished()), &app, SLOT(quit()));

    abortPoller.start();
    int rc = app.exec();
    abortPoller.disconnect();
    delete gui;
    delete server;
    ThreadPool::system()->wait();
    return rc;
}
