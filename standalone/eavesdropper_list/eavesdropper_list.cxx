/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <QCoreApplication>
#include <QTranslator>
#include <QLibraryInfo>
#include <QLocale>
#include <QLoggingCategory>
#include <QTimer>

#include "core/abort.hxx"
#include "core/qtcompat.hxx"
#include "core/threadpool.hxx"
#include "clicore/terminaloutput.hxx"
#include "datacore/variant/root.hxx"
#include "acquisition/acquisitionnetwork.hxx"
#include "acquisition/iotarget.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

static std::vector<Variant::Root> getAcquisitionInterfaces()
{
    std::unique_ptr<AcquisitionNetworkClient>
            client(new AcquisitionNetworkClient(Variant::Read::empty()));

    std::unordered_map<std::string, Variant::Root> result;
    std::mutex mutex;
    std::condition_variable cv;
    using Clock = std::chrono::steady_clock;
    enum class State {
        Startup, Connected, SeenInterface, Ended
    } state = State::Startup;
    Clock::time_point timeout = Clock::now() + std::chrono::seconds(30);

    AcquisitionNetworkClient *clientRef = client.get();
    client->interfaceInformationUpdated.connect([&](const std::string &interface) {
        auto info = clientRef->getInterfaceInformation(interface);
        if (!info["Interface"].exists())
            return;

        Util::insert_or_assign(result, interface, info);

        {
            std::lock_guard<std::mutex> lock(mutex);
            switch (state) {
            case State::Startup:
            case State::Connected:
                state = State::SeenInterface;
                timeout = Clock::now() + std::chrono::milliseconds(500);
                break;
            case State::SeenInterface:
            case State::Ended:
                return;
            }
        }
        cv.notify_all();
    });
    client->connectionState.connect([&](bool connected) {
        if (!connected)
            return;
        {
            std::lock_guard<std::mutex> lock(mutex);
            switch (state) {
            case State::Startup:
                state = State::Connected;
                timeout = Clock::now() + std::chrono::seconds(5);
                break;
            case State::Connected:
            case State::SeenInterface:
            case State::Ended:
                return;
            }
        }
        cv.notify_all();
    });
    client->connectionFailed.connect([&] {
        {
            std::lock_guard<std::mutex> lock(mutex);
            switch (state) {
            case State::Startup:
                state = State::Ended;
                break;
            case State::Connected:
            case State::SeenInterface:
            case State::Ended:
                break;
            }
        }
        cv.notify_all();
    });

    client->start();
    {
        std::unique_lock<std::mutex> lock(mutex);
        while (state != State::Ended && timeout > Clock::now()) {
            cv.wait_until(lock, timeout);
        }
    }
    client.reset();

    std::vector<Variant::Root> unique;
    for (auto &add : result) {
        unique.emplace_back(std::move(add.second));
    }
    return unique;
}

static void displayInterfaceList()
{
    struct ListEntry {
        std::unique_ptr<IOTarget> target;
        QString interfaceDescription;
        QString acquisitionDescription;
    };

    std::vector<ListEntry> displayList;

    for (auto &add : Acquisition::IOTarget::enumerateLocal()) {
        ListEntry entry;
        entry.target = std::move(add);
        entry.interfaceDescription = entry.target->description();
        displayList.emplace_back(std::move(entry));
    }

    for (auto &add : getAcquisitionInterfaces()) {
        auto interface = IOTarget::create(add["Interface"]);
        if (!interface)
            continue;
        if (!interface->isValid())
            continue;
        bool hit = false;
        for (auto &check : displayList) {
            if (!check.target->matchesDevice(*interface))
                continue;

            hit = true;

            check.acquisitionDescription = add["MenuEntry"].toDisplayString();
        }
        if (hit)
            continue;

        ListEntry entry;
        entry.target = std::move(interface);
        entry.interfaceDescription = entry.target->description();
        entry.acquisitionDescription = add["MenuEntry"].toDisplayString();
        displayList.emplace_back(std::move(entry));
    }

    if (displayList.empty()) {
        CLI::TerminalOutput::simpleWordWrap(QObject::tr("No interfaces available."), true);
        return;
    }

    std::sort(displayList.begin(), displayList.end(), [](const ListEntry &a, const ListEntry &b) {
        if (a.acquisitionDescription.isEmpty()) {
            if (!b.acquisitionDescription.isEmpty())
                return true;
        } else if (b.acquisitionDescription.isEmpty()) {
            return false;
        }

        if (a.interfaceDescription != b.interfaceDescription) {
            return a.interfaceDescription < b.interfaceDescription;
        }
        return a.acquisitionDescription < b.acquisitionDescription;
    });

    for (const auto &disp : displayList) {
        QString line = disp.interfaceDescription;

        if (!disp.acquisitionDescription.isEmpty()) {
            if (!line.isEmpty())
                line += " : ";
            line += disp.acquisitionDescription;
        }

        if (line.isEmpty())
            continue;

        line += "\n";

        CLI::TerminalOutput::output(line);
    }
}

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    Logging::suppressForConsole();

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

    QString translateLocation;
    QByteArray cpd3(qgetenv("CPD3"));
    if (cpd3.length() > 0) {
        translateLocation = QString(cpd3) + "/locale";
    }
    QTranslator cpd3Translator;
    cpd3Translator.load("cpd3_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cpd3Translator);
    QTranslator cliTranslator;
    cliTranslator.load("cli_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cliTranslator);
    QTranslator acquisitionTranslator;
    acquisitionTranslator.load("acquisition_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&acquisitionTranslator);
    QTranslator eavesdropperTranslator;
    eavesdropperTranslator.load("eavesdropper_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&eavesdropperTranslator);

    displayInterfaceList();

    ThreadPool::system()->wait();
    return 0;
}