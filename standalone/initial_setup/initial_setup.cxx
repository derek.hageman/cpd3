/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <string.h>
#include <QTranslator>
#include <QLibraryInfo>
#include <QApplication>
#include <QMessageBox>
#include <QIcon>
#include <QLocale>
#include <QFile>
#include <QSettings>
#include <QLoggingCategory>

#include "datacore/stream.hxx"
#include "core/waitutils.hxx"
#include "core/threadpool.hxx"
#include "clicore/argumentparser.hxx"
#include "clicore/terminaloutput.hxx"
#include "guidata/wizards/initialsetup.hxx"


Q_LOGGING_CATEGORY(log_initialsetup, "cpd3.initialsetup", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::CLI;
using namespace CPD3::GUI;
using namespace CPD3::GUI::Data;

class Parser : public ArgumentParser {
public:
    Parser()
    { }

    virtual ~Parser()
    { }

protected:
    virtual QString getProgramName()
    { return tr("cpd3.setup", "program name"); }

    virtual QString getDescription()
    {
        return tr("This program is the initial setup utility for CPD3.  It prompts the user for "
                          "various information needed to configure a CPD3 system.");
    }

    virtual QHash<QString, QString> getDocumentationTypeID()
    {
        QHash<QString, QString> result;
        result.insert("type", "initial_setup");
        return result;
    }
};

namespace {
enum DefaultDatabaseMode {
    Database_Clear, Database_SQLite, Database_MySQL, Database_PSQL,
};
}

template<typename OptionType>
void setDatabaseValue(QSettings &settings, const QString &name, const OptionType *option)
{
    if (!option->isSet()) {
        settings.remove(name);
        return;
    }
    settings.setValue(name, QVariant::fromValue(option->get()));
}

static void setDefaultDatabase(ComponentOptions &options)
{
    QString name;
    DefaultDatabaseMode mode;
    if (options.isSet("archive-name"))
        name = qobject_cast<ComponentOptionSingleString *>(options.get("archive-name"))->get();

    if (name.isEmpty()) {
        mode = Database_Clear;
    } else if (!options.isSet("archive-type")) {
        mode = Database_SQLite;
    } else {
        mode = (DefaultDatabaseMode) qobject_cast<ComponentOptionEnum *>(
                options.get("archive-type"))->get().getID();
    }

    QSettings settings("NOAA ESRL GMD", "CPD3Default");

    settings.beginGroup("db");
    switch (mode) {
    case Database_Clear:
        qCDebug(log_initialsetup) << "Clearing default database";
        settings.remove("");
        break;
    case Database_SQLite:
        qCDebug(log_initialsetup) << "Setting default database to SQLite file" << name;
        settings.setValue("type", "sqlite");
        settings.setValue("filename", name);
        break;
    case Database_MySQL:
        qCDebug(log_initialsetup) << "Setting default database to MySQL server" << name;
        settings.setValue("type", "mysql");
        settings.setValue("hostname", name);
        setDatabaseValue(settings, "port",
                         qobject_cast<ComponentOptionSingleInteger *>(options.get("archive-port")));
        setDatabaseValue(settings, "database", qobject_cast<ComponentOptionSingleString *>(
                options.get("archive-database")));
        setDatabaseValue(settings, "username", qobject_cast<ComponentOptionSingleString *>(
                options.get("archive-username")));
        setDatabaseValue(settings, "password", qobject_cast<ComponentOptionSingleString *>(
                options.get("archive-password")));
        break;
    case Database_PSQL:
        qCDebug(log_initialsetup) << "Setting default database to PostgreSQL server" << name;
        settings.setValue("type", "psql");
        settings.setValue("hostname", name);
        setDatabaseValue(settings, "port",
                         qobject_cast<ComponentOptionSingleInteger *>(options.get("archive-port")));
        setDatabaseValue(settings, "database", qobject_cast<ComponentOptionSingleString *>(
                options.get("archive-database")));
        setDatabaseValue(settings, "username", qobject_cast<ComponentOptionSingleString *>(
                options.get("archive-username")));
        setDatabaseValue(settings, "password", qobject_cast<ComponentOptionSingleString *>(
                options.get("archive-password")));
        break;
    }
    settings.endGroup();
}

static void setDefaultStation(const QString &station)
{
    QSettings settings("NOAA ESRL GMD", "CPD3Default");
    if (station.isEmpty()) {
        qCDebug(log_initialsetup) << "Clearing default station";
        settings.remove("station");
    } else {
        qCDebug(log_initialsetup) << "Seting default station to " << station;
        settings.setValue("station", station);
    }
}


int main(int argc, char **argv)
{
    bool initializeGUI = true;
    for (int i = 1; i < argc; ++i) {
        if (!strcmp(argv[i], "--silent"))
            initializeGUI = false;
    }

    QCoreApplication *app;
    if (!initializeGUI) {
#if defined(Q_OS_UNIX) && !defined(Q_OS_OSX)
        qputenv("QT_QPA_PLATFORM", "offscreen");
#endif
        app = new QCoreApplication(argc, argv);
    } else {
        app = new QApplication(argc, argv);
    }
    Q_ASSERT(app);

    Q_INIT_RESOURCE(initial_setup);

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app->installTranslator(&qtTranslator);

    QString translateLocation;
    QByteArray cpd3(qgetenv("CPD3"));
    if (cpd3.length() > 0) {
        translateLocation = QString(cpd3) + "/locale";
    }
    QTranslator cpd3Translator;
    cpd3Translator.load("cpd3_" + QLocale::system().name(), translateLocation);
    app->installTranslator(&cpd3Translator);
    QTranslator cliTranslator;
    cliTranslator.load("cli_" + QLocale::system().name(), translateLocation);
    app->installTranslator(&cliTranslator);
    QTranslator guiTranslator;
    guiTranslator.load("gui_" + QLocale::system().name(), translateLocation);
    app->installTranslator(&guiTranslator);

    InitialSetup::SetupMode setupMode = InitialSetup::Mode_Any;

    {
        ComponentOptions options;

        ComponentOptionEnum *e = new ComponentOptionEnum(InitialSetup::tr("mode", "name"),
                                                         InitialSetup::tr("Setup mode"),
                                                         InitialSetup::tr(
                                                                 "This is the setup mode to use.  The setup mode "
                                                                         "restricts what is shown as possible setup options."),
                                                         QString());
        e->add(InitialSetup::Mode_Any, "any", InitialSetup::tr("any", "mode name"),
               InitialSetup::tr("Allow any setup mode."));
        e->add(InitialSetup::Mode_Synchronize, "synchronize",
               InitialSetup::tr("synchronize", "mode name"),
               InitialSetup::tr("Only show synchronization client options."));
        e->add(InitialSetup::Mode_Acquisition, "acquisition",
               InitialSetup::tr("acquisition", "mode name"),
               InitialSetup::tr("Only show acquisition system options."));
        e->add(InitialSetup::Mode_SetStation, "station", InitialSetup::tr("station", "mode name"),
               InitialSetup::tr("Only show default station selection panel."));
        options.add("mode", e);

        options.add("firstrun", new ComponentOptionBoolean(InitialSetup::tr("firstrun", "name"),
                                                           InitialSetup::tr(
                                                                   "Enable first run mode"),
                                                           InitialSetup::tr(
                                                                   "When enabled, this will exit the setup if this is not the very first time it has been run."),
                                                           QString()));

        options.add("silent", new ComponentOptionBoolean(InitialSetup::tr("silent", "name"),
                                                         InitialSetup::tr(
                                                                 "Disable the GUI interface"),
                                                         InitialSetup::tr(
                                                                 "This disables the GUI interface and only runs the command line options as specified."),
                                                         QString()));

        options.add("station", new ComponentOptionSingleString(InitialSetup::tr("station", "name"),
                                                               InitialSetup::tr(
                                                                       "Set the default station"),
                                                               InitialSetup::tr(
                                                                       "When set, the default the default station is changed to "
                                                                               "one specified here.  An empty value clears the default."),
                                                               QString()));

        options.add("archive-name",
                    new ComponentOptionSingleString(InitialSetup::tr("archive", "name"),
                                                    InitialSetup::tr(
                                                            "The file or host name to set as the default database"),
                                                    InitialSetup::tr(
                                                            "When set, the default database is changed to the file "
                                                                    "name (SQLite) or host name (database servers) "
                                                                    "specified here."), QString()));
        options.add("archive-database",
                    new ComponentOptionSingleString(InitialSetup::tr("archive-database", "name"),
                                                    InitialSetup::tr(
                                                            "The database name to connect to"),
                                                    InitialSetup::tr(
                                                            "When changing the default database, this sets the "
                                                                    "name of the database to connect to."),
                                                    QString()));
        options.add("archive-username",
                    new ComponentOptionSingleString(InitialSetup::tr("archive-username", "name"),
                                                    InitialSetup::tr(
                                                            "The username to use for database connection"),
                                                    InitialSetup::tr(
                                                            "When changing the default database, this sets the "
                                                                    "username used during connection."),
                                                    QString()));
        options.add("archive-password",
                    new ComponentOptionSingleString(InitialSetup::tr("archive-password", "name"),
                                                    InitialSetup::tr(
                                                            "The password to use for database connection"),
                                                    InitialSetup::tr(
                                                            "When changing the default database, this sets the "
                                                                    "password used during connection."),
                                                    QString()));
        ComponentOptionSingleInteger *i =
                new ComponentOptionSingleInteger(InitialSetup::tr("archive-port", "name"),
                                                 InitialSetup::tr(
                                                         "The port to use for database connection"),
                                                 InitialSetup::tr(
                                                         "When changing the default database, this sets the "
                                                                 "port used during connection."),
                                                 QString());
        i->setMinimum(1);
        i->setMaximum(65535);
        options.add("archive-port", i);
        e = new ComponentOptionEnum(InitialSetup::tr("archive-type", "name"),
                                    InitialSetup::tr("Set the default database type"),
                                    InitialSetup::tr(
                                            "This changes the default database used to store data."),
                                    QString());
        e->add(Database_Clear, "clear", InitialSetup::tr("clear", "database type name"),
               InitialSetup::tr("Clear any default database set."));
        e->add(Database_SQLite, "sqlite", InitialSetup::tr("sqlite", "database type name"),
               InitialSetup::tr("Use a SQLite database file."));
        e->add(Database_MySQL, "mysql", InitialSetup::tr("mysql", "database type name"),
               InitialSetup::tr("Connect to a MySQL/MariaDB database server."));
        e->add(Database_PSQL, "psql", InitialSetup::tr("psql", "database type name"),
               InitialSetup::tr("Connect to a PostgreSQL database server."));
        options.add("archive-type", e);


        Parser parser;
        try {
            QStringList arguments(app->arguments());
            parser.parse(arguments, options);
        } catch (ArgumentParsingException ape) {
            QString text(ape.getText());
            if (ape.isError()) {
                if (!text.isEmpty())
                    TerminalOutput::simpleWordWrap(text, true);
                return 1;
            } else {
                if (!text.isEmpty())
                    TerminalOutput::simpleWordWrap(text);
                return 0;
            }
        }

        if (qobject_cast<ComponentOptionBoolean *>(options.get("firstrun"))->get()) {
            QSettings settings("NOAA ESRL GMD", "CPD3");
            settings.beginGroup("setup");
            if (settings.value("run").toBool()) {
                qCDebug(log_initialsetup) << "Exiting because first run flag is already set";
                return 0;
            }
            settings.setValue("run", QVariant::fromValue<bool>(true));
            settings.endGroup();
            settings.sync();
            qCDebug(log_initialsetup) << "Set first run flag";
        }

        if (options.isSet("mode")) {
            setupMode = (InitialSetup::SetupMode) qobject_cast<ComponentOptionEnum *>(
                    options.get("mode"))->get().getID();
        }

        if (options.isSet("station")) {
            setDefaultStation(
                    qobject_cast<ComponentOptionSingleString *>(options.get("station"))->get());
        }

        if (options.isSet("archive-name") || options.isSet("archive-type")) {
            setDefaultDatabase(options);
        }

        if (qobject_cast<ComponentOptionBoolean *>(options.get("silent"))->get())
            return 0;
    }

    if (!initializeGUI)
        return 0;

    static_cast<QApplication *>(app)->setQuitOnLastWindowClosed(true);
    static_cast<QApplication *>(app)->setWindowIcon(QIcon(":/icon.svg"));

    InitialSetup *setup = new InitialSetup(setupMode);
    setup->show();
    int rc = app->exec();
    delete setup;
    ThreadPool::system()->wait();
    delete app;
    return rc;
}
