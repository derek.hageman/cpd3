/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#ifdef Q_OS_UNIX

#include <unistd.h>

#endif

#include <QtEndian>
#include <QHostInfo>
#include <QLocale>
#include <QLibraryInfo>
#include <QTranslator>
#include <QLoggingCategory>

#include "core/abort.hxx"
#include "core/threadpool.hxx"
#include "sync/server.hxx"
#include "algorithms/cryptography.hxx"

#include "sync_serverhandler.hxx"


Q_LOGGING_CATEGORY(log_sync_handler, "cpd3.sync.handler", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Sync;
using namespace CPD3::Algorithms;

static void consumeBytes(QByteArray &data, int n)
{
    if (n >= data.size()) {
        data.clear();
        return;
    }

    int remaining = data.size() - n;
    memmove(data.data(), data.constData() + n, remaining);
    data.resize(remaining);
}

static bool readExactly(QIODevice *device, QByteArray &buffer, qint64 required)
{
    if (required <= 0)
        return false;
    qint64 n = device->bytesAvailable();
    if (n > 0) {
        if (buffer.isEmpty()) {
            buffer = device->read((int) qMin(required, n));
        } else {
            qint64 max = required - buffer.size();
            buffer.append(device->read((int) qMin(max, n)));
        }
        if (buffer.size() >= required)
            return false;
        if (device->bytesAvailable() > 0)
            return true;
    }
    return false;
}

Handler::Handler() : inputDevice(NULL),
                     outputDevice(NULL),
                     stdoutOpen(false),
                     socket(NULL),
                     compressor(NULL),
                     lock(),
                     peer(NULL),
                     serverSocketName(),
                     localAuthorization(),
                     remoteAuthorization(),
                     state(State_WaitForInitialize),
                     timeoutTimer(this),
                     readBuffer(),
                     writeBuffer(),
                     dataReadBuffer(),
                     dataWriteBuffer()
{
    inputDevice = new StdinDevice;
    Q_ASSERT(inputDevice);
    if (!inputDevice->open(QIODevice::ReadOnly)) {
        qFatal("Failed to open stdin: %s", inputDevice->errorString().toLatin1().constData());
        return;
    }

#ifdef Q_OS_UNIX
    outputDevice = new QFile;
    Q_ASSERT(outputDevice);
    if (!outputDevice->open(1, QIODevice::WriteOnly | QIODevice::Unbuffered)) {
        qFatal("Failed to open stdout: %s", outputDevice->errorString().toLatin1().constData());
        return;
    }
    stdoutOpen = true;
#endif


    connect(outputDevice, SIGNAL(bytesWritten(qint64)), this, SLOT(outputUpdated()),
            Qt::QueuedConnection);

    connect(inputDevice, SIGNAL(readyRead()), this, SLOT(inputUpdated()), Qt::QueuedConnection);
    connect(inputDevice, SIGNAL(readChannelFinished()), this, SLOT(inputUpdated()),
            Qt::QueuedConnection);

    timeoutTimer.setSingleShot(true);
    connect(&timeoutTimer, SIGNAL(timeout()), this, SLOT(connectionTimeout()),
            Qt::QueuedConnection);
    timeoutTimer.start(120 * 1000);
}

Handler::~Handler()
{
    if (peer != NULL)
        delete peer;
    if (compressor != NULL)
        delete compressor;
    if (lock) {
        lock->fail();
        lock.reset();
    }

    if (inputDevice != NULL)
        delete inputDevice;
    if (outputDevice != NULL)
        delete outputDevice;

#ifdef Q_OS_UNIX
    if (stdoutOpen) {
        ::close(1);
    }
#endif
}

void Handler::start()
{
    if (inputDevice == NULL || outputDevice == NULL) {
        emit completed();
        return;
    }

    qRegisterMetaType<QLocalSocket::LocalSocketState>("QLocalSocket::LocalSocketState");

    qCDebug(log_sync_handler) << "Handler starting up";

    QMetaObject::invokeMethod(this, "inputUpdated", Qt::QueuedConnection);
}


void Handler::fatalShutdown(bool local)
{
    if (inputDevice != NULL) {
        inputDevice->close();
        inputDevice->deleteLater();
        inputDevice = NULL;
    }
    if (compressor != NULL) {
        compressor->abort();
        compressor->close();
        compressor->deleteLater();
        compressor = NULL;
    }
    if (socket != NULL) {
        socket->close();
        socket->deleteLater();
        socket = NULL;
    }
    if (peer != NULL) {
        delete peer;
        peer = NULL;
    }
    if (lock) {
        lock->fail();
        lock.reset();
    }

    if (outputDevice != NULL) {
        if (local) {
            writeBuffer.append(
                    (char) CPD3::Sync::Internal::ServerConnectionDetached::External_FatalError);
            QMetaObject::invokeMethod(this, "outputUpdated", Qt::QueuedConnection);
        } else {
            outputDevice->close();
            outputDevice->deleteLater();
            outputDevice = NULL;
        }
    }

    state = State_Completed;
    checkCompleted();
}

void Handler::disconnectResume(bool local)
{
    if (peer == NULL) {
        qCWarning(log_sync_handler) << "Can't resume without a peer attached";
        fatalShutdown();
        return;
    }
    if (!peer->rollback()) {
        qCDebug(log_sync_handler) << "Rollback failed, synchronization aborted";
        fatalShutdown();
        return;
    }

    if (compressor != NULL) {
        compressor->abort();
        compressor->close();
        compressor->deleteLater();
        compressor = NULL;
    }
    if (socket != NULL) {
        socket->abort();
        if (socket->state() != QLocalSocket::UnconnectedState)
            socket->waitForDisconnected(5000);
        socket->close();
        socket->deleteLater();
        socket = NULL;
    }

    if (local) {
        writeBuffer.append(
                (char) CPD3::Sync::Internal::ServerConnectionDetached::External_ResumableError);
        QMetaObject::invokeMethod(this, "outputUpdated", Qt::QueuedConnection);
    }

    state = State_Resume;
    QMetaObject::invokeMethod(this, "inputUpdated", Qt::QueuedConnection);
}

void Handler::startDataChannel()
{
    if (peer == NULL) {
        qCWarning(log_sync_handler) << "Can't create a data channel without a peer attached";
        fatalShutdown();
        return;
    }
    socket = new QLocalSocket(this);
    Q_ASSERT(socket);
    socket->connectToServer(serverSocketName, QIODevice::ReadWrite | QIODevice::Unbuffered);
    if (!socket->waitForConnected(30000)) {
        qCDebug(log_sync_handler) << "Failed to connect to server:" << socket->errorString();
        delete socket;
        socket = NULL;
        fatalShutdown();
        return;
    }

    connect(socket, SIGNAL(readyRead()), this, SLOT(updateSocket()), Qt::QueuedConnection);
    connect(socket, SIGNAL(readChannelFinished()), this, SLOT(updateSocket()),
            Qt::QueuedConnection);
    connect(socket, SIGNAL(stateChanged(QLocalSocket::LocalSocketState)), this,
            SLOT(updateSocket()), Qt::QueuedConnection);
    connect(socket, SIGNAL(bytesWritten(qint64)), this, SLOT(dataWrite()), Qt::QueuedConnection);

    state = State_SendAuthorization;
    dataWriteBuffer.append(remoteAuthorization);
    dataReadBuffer.clear();

    QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
    QMetaObject::invokeMethod(this, "dataWrite", Qt::QueuedConnection);
}

static const quint8 rcSynchronizeStateVersion = 1;

void Handler::processIncoming()
{
    while (!readBuffer.isEmpty()) {
        switch (state) {
        case State_WaitForInitialize:
            if (readBuffer.size() < 20)
                return;

            {
                quint32 sz = qFromLittleEndian<quint32>((const uchar *) readBuffer.data() + 16);
                if (readBuffer.size() < (int) (20 + sz))
                    return;
                remoteAuthorization = readBuffer.mid(0, 16);

                SequenceName::Component station;
                QString authKey;
                {
                    QByteArray
                            parameterData(QByteArray::fromRawData(readBuffer.constData() + 20, sz));
                    QDataStream stream(&parameterData, QIODevice::ReadOnly);
                    stream.setVersion(QDataStream::Qt_4_5);
                    stream.setByteOrder(QDataStream::LittleEndian);

                    stream >> serverSocketName;
                    stream >> station;
                    stream >> authKey;

                    if (stream.status() != QDataStream::Ok ||
                            serverSocketName.isEmpty() ||
                            station.empty() ||
                            authKey.isEmpty()) {
                        qCWarning(log_sync_handler)
                            << "Invalid parameters received from the server";
                        fatalShutdown();
                        return;
                    }
                }

                consumeBytes(readBuffer, 20 + sz);

                ValueSegment::Transfer config;
                config = ValueSegment::Stream::read(
                        Archive::Selection(FP::undefined(), FP::undefined(), {station},
                                           {"configuration"}, {"synchronize"}));

                lock.reset(new Database::RunLock);

                bool ok = false;
                lockKey =
                        QString("synchronize %1 %2").arg(QString::fromStdString(station).toLower(),
                                                         authKey.toLower());
                double modified = lock->acquire(lockKey, 30.0, &ok);
                if (!ok) {
                    qCWarning(log_sync_handler) << "Failed to acquire lock for" << authKey << "-"
                                                << station;
                    fatalShutdown();
                    return;
                }

                config = ValueSegment::withPath(config, QString("Authorization/%1").arg(authKey));

                QByteArray remoteState;
                {
                    QByteArray data(lock->get(lockKey));
                    if (!data.isEmpty()) {
                        QDataStream stream(&data, QIODevice::ReadOnly);
                        quint8 version = 0;
                        stream >> version;

                        if (stream.status() != QDataStream::Ok ||
                                version != rcSynchronizeStateVersion) {
                            qCDebug(log_sync_handler) << "Invalid state data";
                        } else {
                            stream >> remoteState;
                        }
                    }
                }

                Q_ASSERT(peer == NULL);
                peer = new Peer(new BasicFilter(ValueSegment::withPath(config, "Local/Synchronize"),
                                                station), new TrackingFilter(
                        ValueSegment::withPath(config, "Remote/Synchronize"), station, remoteState),
                                Peer::Mode_Bidirectional, modified);
                Q_ASSERT(peer);
                connect(peer, SIGNAL(incomingStallCleared()), this, SLOT(updateSocket()),
                        Qt::QueuedConnection);
                connect(peer, SIGNAL(writeAvailable()), this, SLOT(peerWrite()),
                        Qt::QueuedConnection);
                connect(peer, SIGNAL(errorDetected()), this, SLOT(updateSocket()),
                        Qt::QueuedConnection);
                connect(peer, SIGNAL(finished()), this, SLOT(updateSocket()), Qt::QueuedConnection);
                connect(peer, SIGNAL(flushRequested()), this, SLOT(flushCompressor()),
                        Qt::QueuedConnection);

                peer->start();

                qCDebug(log_sync_handler) << "External handler finished initialization for"
                                          << authKey << "-"
                                          << QString::fromStdString(station).toUpper();
            }

            localAuthorization = Random::data(16);

            writeBuffer.append(localAuthorization);
            QMetaObject::invokeMethod(this, "outputUpdated", Qt::QueuedConnection);

            startDataChannel();
            break;
        case State_AuthorizeRemote:
        case State_Connected:
        case State_Resume: {
            CPD3::Sync::Internal::ServerConnectionDetached::InternalMessage code =
                    (CPD3::Sync::Internal::ServerConnectionDetached::InternalMessage) readBuffer.at(
                            0);
            consumeBytes(readBuffer, 1);
            switch (code) {
            case CPD3::Sync::Internal::ServerConnectionDetached::Internal_Reconnected:
                qCDebug(log_sync_handler) << "Reconnection notice received";
                switch (state) {
                case State_Connected:
                case State_AuthorizeRemote:
                case State_SendAuthorization:
                    disconnectResume(false);
                    /* Fall through */
                case State_Resume:
                    startDataChannel();
                    break;
                case State_WaitForInitialize:
                case State_Closing:
                case State_Completed:
                    break;
                }
                break;
            case CPD3::Sync::Internal::ServerConnectionDetached::Internal_Aborted:
                qCDebug(log_sync_handler) << "Abort received";
                fatalShutdown(false);
                return;
            }
            break;
        }
        case State_Closing:
        case State_Completed:
        case State_SendAuthorization:
            return;
        }
    }
}

void Handler::inputUpdated()
{
    if (inputDevice == NULL)
        return;

    qint64 n = inputDevice->bytesAvailable();
    if (n > 0) {
        {
            QByteArray bufferRead(inputDevice->read((int) qMin(Q_INT64_C(65536), n)));
            if (readBuffer.isEmpty())
                readBuffer = bufferRead;
            else
                readBuffer.append(bufferRead);
        }
        processIncoming();
        if (inputDevice->bytesAvailable() > 0) {
            QMetaObject::invokeMethod(this, "inputUpdated", Qt::QueuedConnection);
            return;
        }
    }

    if (!inputDevice->atEnd())
        return;

    qCDebug(log_sync_handler) << "Input closed";

    inputDevice->close();
    inputDevice->deleteLater();
    inputDevice = NULL;
    checkCompleted();
}

void Handler::outputUpdated()
{
    if (outputDevice == NULL)
        return;
    if (!writeBuffer.isEmpty()) {
        qint64 n = outputDevice->write(writeBuffer);
        if (n == -1) {
            qCDebug(log_sync_handler) << "Error writing to output:" << outputDevice->errorString();
            outputDevice->deleteLater();
            outputDevice = NULL;
            checkCompleted();
            return;
        }
        consumeBytes(writeBuffer, (int) n);
    }

    if (outputDevice->bytesToWrite() > 0)
        return;

    switch (state) {
    case State_SendAuthorization:
    case State_WaitForInitialize:
    case State_AuthorizeRemote:
    case State_Connected:
    case State_Resume:
        return;
    case State_Closing:
        if (!writeBuffer.isEmpty())
            return;
        break;
    case State_Completed:
        break;
    }

    qCDebug(log_sync_handler) << "Output completed, finishing shutdown";

    if (inputDevice != NULL) {
        inputDevice->close();
        inputDevice->deleteLater();
        inputDevice = NULL;
    }

    outputDevice->close();
    outputDevice->deleteLater();
    outputDevice = NULL;
    checkCompleted();
}

void Handler::updateSocket()
{
    if (socket == NULL)
        return;

    switch (state) {
    case State_SendAuthorization:
        if (readExactly(socket, dataReadBuffer, 16)) {
            QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
        }
        break;
    case State_AuthorizeRemote:
        if (readExactly(socket, dataReadBuffer, 16)) {
            QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
        }
        if (dataReadBuffer.size() < 16)
            break;
        if (dataReadBuffer != localAuthorization) {
            qCWarning(log_sync_handler) << "Received mismatched authorization";
            fatalShutdown();
            return;
        }
        dataReadBuffer.clear();

        Q_ASSERT(compressor == NULL);
        compressor = new CompressedStream(socket, this);
        Q_ASSERT(compressor);
        compressor->setMaximumBufferSize(8388608);

        connect(compressor, SIGNAL(readyRead()), this, SLOT(updateSocket()), Qt::QueuedConnection);
        connect(compressor, SIGNAL(readChannelFinished()), this, SLOT(updateSocket()),
                Qt::QueuedConnection);
        connect(compressor, SIGNAL(bytesWritten(qint64)), this, SLOT(peerWrite()),
                Qt::QueuedConnection);

        timeoutTimer.stop();

        qCDebug(log_sync_handler) << "Server connection accepted, synchronization proceeding";

        state = State_Connected;
        QMetaObject::invokeMethod(this, "peerWrite", Qt::QueuedConnection);
        /* Fall through */
    case State_Connected:
        Q_ASSERT(compressor != NULL);
        Q_ASSERT(socket != NULL);
        Q_ASSERT(peer != NULL);

        if (socket->state() == QLocalSocket::UnconnectedState) {
            compressor->drainBackendRead();
            while (compressor->bytesAvailable() > 0) {
                peer->incomingData(compressor->read(65536));
            }
        } else {
            if (!peer->shouldStallIncoming() && compressor->bytesAvailable() > 0) {
                peer->incomingData(compressor->read(65536));
                if (compressor->bytesAvailable() > 0) {
                    QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
                }
            }
        }

        switch (peer->errorState()) {
        case Peer::Error_None:
            break;
        case Peer::Error_Fatal:
            qCDebug(log_sync_handler) << "Fatal error in connection, aborting synchronization";
            fatalShutdown();
            return;
        case Peer::Error_Recoverable:
            disconnectResume();
            return;
        }

        if (peer->isFinished()) {
            compressor->flush(0);

            if (lock) {
                QByteArray data;
                {
                    QDataStream stream(&data, QIODevice::WriteOnly);
                    stream << rcSynchronizeStateVersion;
                    stream << peer->getRemoteFilterState();
                }
                lock->set(lockKey, data);

                lock->release();
                lock.reset();
            }

            peer->deleteLater();
            peer = NULL;
            state = State_Closing;
            QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);

            writeBuffer.append(
                    (char) CPD3::Sync::Internal::ServerConnectionDetached::External_Completed);
            QMetaObject::invokeMethod(this, "outputUpdated", Qt::QueuedConnection);

            qCDebug(log_sync_handler) << "Synchronization completed, shutting down connection";
            return;
        }

        /* Transfer completed, so don't check the network status */
        if (peer->transferCompleted())
            return;

        if (socket->state() == QLocalSocket::UnconnectedState) {
            qCDebug(log_sync_handler) << "Premature disconnection";
            disconnectResume();
            return;
        }
        break;
    case State_Closing:
        if (compressor == NULL)
            break;

        Q_ASSERT(socket != NULL);
        if (compressor->bytesToWrite() > 0) {
            compressor->flush(0);
            socket->flush();
        } else {
            if (socket->state() == QLocalSocket::ConnectedState) {
                socket->disconnectFromServer();
            }
        }
        if (socket->state() != QLocalSocket::UnconnectedState)
            return;

        qCDebug(log_sync_handler) << "Connection shutdown complete";
        compressor->close();
        compressor->deleteLater();
        compressor = NULL;

        socket->deleteLater();
        socket = NULL;

        if (outputDevice != NULL) {
            writeBuffer.append(
                    (char) CPD3::Sync::Internal::ServerConnectionDetached::External_Completed);
            QMetaObject::invokeMethod(this, "outputUpdated", Qt::QueuedConnection);
        } else {
            if (inputDevice != NULL) {
                inputDevice->close();
                inputDevice->deleteLater();
                inputDevice = NULL;
            }
        }

        state = State_Completed;
        checkCompleted();
        return;

    case State_WaitForInitialize:
    case State_Resume:
    case State_Completed:
        return;
    }
}

void Handler::dataWrite()
{
    if (socket == NULL)
        return;

    switch (state) {
    case State_SendAuthorization:
        break;
    case State_AuthorizeRemote:
    case State_Connected:
    case State_Resume:
    case State_Completed:
    case State_Closing:
    case State_WaitForInitialize:
        return;
    }

    if (!dataWriteBuffer.isEmpty()) {
        qint64 n = socket->write(dataWriteBuffer);
        if (n == -1) {
            qCDebug(log_sync_handler) << "Error writing to output:" << outputDevice->errorString();
            outputDevice->deleteLater();
            outputDevice = NULL;
            checkCompleted();
            return;
        }
        consumeBytes(dataWriteBuffer, (int) n);
        socket->flush();
    }
    if (!dataWriteBuffer.isEmpty())
        return;

    switch (state) {
    case State_SendAuthorization:
        state = State_AuthorizeRemote;
        socket->disconnect(this, SLOT(dataWrite()));
        QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
        break;
    case State_AuthorizeRemote:
    case State_Connected:
    case State_Resume:
    case State_Completed:
    case State_Closing:
    case State_WaitForInitialize:
        break;
    }
}

void Handler::peerWrite()
{
    if (peer == NULL || compressor == NULL)
        return;
    switch (peer->writePending(compressor)) {
    case Peer::WriteResult_Wait:
    case Peer::WriteResult_Continue:
        break;
    case Peer::WriteResult_Completed:
        compressor->disconnect(this, SLOT(peerWrite()));
        compressor->flush(0);
        QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
        break;
    case Peer::WriteResult_Error:
        qCDebug(log_sync_handler) << "Write error encountered on socket";
        QMetaObject::invokeMethod(this, "updateSocket", Qt::QueuedConnection);
        return;
    }

    switch (state) {
    case State_WaitForInitialize:
    case State_SendAuthorization:
    case State_AuthorizeRemote:
    case State_Connected:
    case State_Resume:
    case State_Completed:
        break;
    case State_Closing:
        updateSocket();
        break;
    }
}

void Handler::flushCompressor()
{
    if (compressor == NULL)
        return;
    compressor->flush(0);
}

void Handler::connectionTimeout()
{
    qCDebug(log_sync_handler) << "Handler timeout";
    emit completed();
}

void Handler::checkCompleted()
{
    if (inputDevice == NULL && outputDevice == NULL && socket == NULL) {
        state = State_Completed;
        emit completed();
        return;
    }

    switch (state) {
    case State_Completed:
        emit completed();
        return;
    case State_WaitForInitialize:
    case State_SendAuthorization:
    case State_AuthorizeRemote:
    case State_Connected:
    case State_Resume:
    case State_Closing:
        break;
    }
}

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

    QString translateLocation;
    QByteArray cpd3(qgetenv("CPD3"));
    if (cpd3.length() > 0) {
        translateLocation = QString(cpd3) + "/locale";
    }
    QTranslator cpd3Translator;
    cpd3Translator.load("cpd3_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cpd3Translator);
    QTranslator cliTranslator;
    cliTranslator.load("cli_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cliTranslator);
    QTranslator sync_serverhandlerTranslator;
    sync_serverhandlerTranslator.load("sync_serverhandler_" + QLocale::system().name(),
                                      translateLocation);
    app.installTranslator(&sync_serverhandlerTranslator);


    AbortPoller poller;
    QObject::connect(&poller, SIGNAL(aborted()), &app, SLOT(quit()));
    poller.start();

    Handler *handler = new Handler;
    QObject::connect(handler, SIGNAL(completed()), &app, SLOT(quit()));
    handler->start();

    int rc = app.exec();
    poller.disconnect();
    delete handler;
    ThreadPool::system()->wait();
    return rc;
}
