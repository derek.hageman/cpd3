/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef SYNCSERVERHANDLER_H
#define SYNCSERVERHANDLER_H

#include "core/first.hxx"

#include <memory>
#include <QtGlobal>
#include <QObject>
#include <QFile>
#include <QLocalSocket>
#include <QString>
#include <QByteArray>

#include "sync/peer.hxx"
#include "core/stdindevice.hxx"
#include "core/compression.hxx"
#include "database/runlock.hxx"

class Handler : public QObject {
Q_OBJECT

    CPD3::StdinDevice *inputDevice;
    QFile *outputDevice;
    bool stdoutOpen;

    QLocalSocket *socket;
    CPD3::CompressedStream *compressor;
    std::unique_ptr<CPD3::Database::RunLock> lock;
    QString lockKey;
    CPD3::Sync::Peer *peer;

    QString serverSocketName;
    QByteArray localAuthorization;
    QByteArray remoteAuthorization;

    enum {
        /* Initialization, waiting for data from the server */
                State_WaitForInitialize,

        /* Sending server authorization */
                State_SendAuthorization, /* Waiting for server authorization */
                State_AuthorizeRemote,

        /* Normal processing, peer handling all data */
                State_Connected, /* Connection interrupted, awaiting reattachment */
                State_Resume, /* Connection closing, flushing all remaining data */
                State_Closing, /* Connection completed */
                State_Completed,
    } state;
    QTimer timeoutTimer;
    QByteArray readBuffer;
    QByteArray writeBuffer;

    QByteArray dataReadBuffer;
    QByteArray dataWriteBuffer;

    void fatalShutdown(bool local = true);

    void disconnectResume(bool local = true);

    void startDataChannel();

    void processIncoming();

    void checkCompleted();

public:
    Handler();

    ~Handler();

    void start();

signals:

    void completed();

private slots:

    void inputUpdated();

    void outputUpdated();

    void updateSocket();

    void dataWrite();

    void peerWrite();

    void flushCompressor();

    void connectionTimeout();
};

#endif
