/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTranslator>
#include <QCoreApplication>
#include <QStringList>
#include <QLibraryInfo>

#include "core/abort.hxx"
#include "core/number.hxx"
#include "core/range.hxx"
#include "core/component.hxx"
#include "core/threadpool.hxx"
#include "datacore/variant/root.hxx"
#include "acquisition/ioserver.hxx"

#include "realtime_control.hxx"
#include "realtimeoperation.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

RealtimeControl::RealtimeControl(AcquisitionNetworkClient *net) : network(net),
                                                                  realtimeIngress(this),
                                                                  archiveReadIngress(this),
                                                                  serviceCounter(0),
                                                                  current(NULL),
                                                                  background(),
                                                                  pending(),
                                                                  pendingMutex(),
                                                                  pendingRealtime(),
                                                                  pendingArchive(),
                                                                  pendingArchiveEnd(false)
{
    network->setRealtimeEgress(&realtimeIngress);

    QMetaObject::invokeMethod(this, "serviceUpdate", Qt::QueuedConnection);
}

RealtimeControl::~RealtimeControl()
{
    if (current != NULL)
        delete current;
    qDeleteAll(background);
    qDeleteAll(pending);
}

RealtimeOperation *RealtimeControl::createOperation(const QString &data)
{
    QString type(data.section(':', 0, 0).toLower());
    QString subargs(data.section(':', 1));

    if (type == "debugrealtime")
        return new DebugRealtime(this, subargs);
    else if (type == "debugevents" || type == "debugevent")
        return new DebugEvents(this, subargs);
    else if (type == "stream" || type == "realtimestream")
        return new StreamRealtime(this);
    else if (type == "flush")
        return new Flush(this, subargs);
    else if (type == "dataflush")
        return new DataFlush(this);
    else if (type == "restart")
        return new Restart(this);
    else if (type == "bypass")
        return new Bypass(this, subargs);
    else if (type == "unbypass" || type == "clearbypass")
        return new UnBypass(this, subargs);
    else if (type == "bypassoverride" || type == "unbypassoverride")
        return new BypassOverride(this);
    else if (type == "flag")
        return new Flag(this, subargs);
    else if (type == "unflag" || type == "clearflag")
        return new UnFlag(this, subargs);
    else if (type == "flagoverride" || type == "unflagoverride")
        return new FlagOverride(this);
    else if (type == "waitconnected" || type == "connect" || type == "connected")
        return new WaitConnected(this);
    else if (type == "wait")
        return new OperationWait(this, subargs);
    else if (type == "delay" || type == "in")
        return new Delay(this, subargs.section(':', 0, 0),
                         createOperation(subargs.section(':', 1)));
    else if (type == "command")
        return new Command(this, subargs);
    else if (type == "message" || type == "messagelog" || type == "log")
        return new MessageLog(this, subargs);

    qWarning() << "Unrecognized type: " << type;
    return NULL;
}

void RealtimeControl::parseArguments(QStringList arguments)
{
    if (!arguments.isEmpty())
        arguments.removeFirst();

    for (QStringList::const_iterator arg = arguments.constBegin(), end = arguments.constEnd();
            arg != end;
            ++arg) {
        RealtimeOperation *op = createOperation(*arg);
        if (op == NULL)
            continue;
        pending.append(op);
    }
}

void RealtimeControl::completeOperation(RealtimeOperation *operation)
{
    if (operation == current)
        current = NULL;
    background.removeOne(operation);
    pending.removeOne(operation);
    scheduleServiceIfNeeded();
}

void RealtimeControl::enqueueOperation(RealtimeOperation *operation)
{
    operation->activate();
    background.append(operation);
    scheduleServiceIfNeeded();
}

void RealtimeControl::serviceUpdate()
{
    serviceCounter.fetchAndStoreOrdered(0);

    if (current == NULL) {
        while (!pending.isEmpty()) {
            RealtimeOperation *op = pending.takeFirst();
            if (op->activate()) {
                current = op;
                break;
            }
            background.append(op);
        }
    }

    {
        SequenceValue::Transfer dispatchRealtime;
        SequenceValue::Transfer dispatchArchive;
        bool dispatchArchiveEnd;

        {
            std::lock_guard<std::mutex> lock(pendingMutex);
            qSwap(dispatchRealtime, pendingRealtime);
            qSwap(dispatchArchive, pendingArchive);
            dispatchArchiveEnd = pendingArchiveEnd;
            pendingArchiveEnd = false;
        }

        if (!dispatchRealtime.empty()) {
            if (current != NULL)
                current->incomingRealtime(dispatchRealtime);
            for (QList<RealtimeOperation *>::const_iterator op = background.constBegin(),
                    end = background.constEnd(); op != end; ++op) {
                (*op)->incomingRealtime(dispatchRealtime);
            }
        }
        if (!dispatchArchive.empty()) {
            if (current != NULL)
                current->incomingArchive(dispatchArchive);
            for (QList<RealtimeOperation *>::const_iterator op = background.constBegin(),
                    end = background.constEnd(); op != end; ++op) {
                (*op)->incomingArchive(dispatchArchive);
            }
        }
        if (dispatchArchiveEnd) {
            if (current != NULL)
                current->endArchive();
            for (QList<RealtimeOperation *>::const_iterator op = background.constBegin(),
                    end = background.constEnd(); op != end; ++op) {
                (*op)->endArchive();
            }
        }
    }

    checkFinished();
}

void RealtimeControl::scheduleServiceIfNeeded()
{
    if (serviceCounter.fetchAndStoreAcquire(1) != 0)
        return;

    QMetaObject::invokeMethod(this, "serviceUpdate", Qt::QueuedConnection);
}

void RealtimeControl::checkFinished()
{
    if (current != NULL)
        return;
    if (!background.isEmpty())
        return;
    if (!pending.isEmpty())
        return;
    emit finished();
}

RealtimeControl::RealtimeIngress::RealtimeIngress(RealtimeControl *c) : control(c)
{ }

RealtimeControl::RealtimeIngress::~RealtimeIngress()
{ }

void RealtimeControl::RealtimeIngress::incomingData(const SequenceValue::Transfer &values)
{
    {
        std::lock_guard<std::mutex> lock(control->pendingMutex);
        Util::append(values, control->pendingRealtime);
    }
    control->scheduleServiceIfNeeded();
}

void RealtimeControl::RealtimeIngress::incomingData(SequenceValue::Transfer &&values)
{
    {
        std::lock_guard<std::mutex> lock(control->pendingMutex);
        Util::append(std::move(values), control->pendingRealtime);
    }
    control->scheduleServiceIfNeeded();
}

void RealtimeControl::RealtimeIngress::incomingData(const SequenceValue &value)
{
    {
        std::lock_guard<std::mutex> lock(control->pendingMutex);
        control->pendingRealtime.emplace_back(value);
    }
    control->scheduleServiceIfNeeded();
}

void RealtimeControl::RealtimeIngress::incomingData(SequenceValue &&value)
{
    {
        std::lock_guard<std::mutex> lock(control->pendingMutex);
        control->pendingRealtime.emplace_back(std::move(value));
    }
    control->scheduleServiceIfNeeded();
}

void RealtimeControl::RealtimeIngress::endData()
{ }

RealtimeControl::ArchiveReadIngress::ArchiveReadIngress(RealtimeControl *c) : control(c)
{ }

RealtimeControl::ArchiveReadIngress::~ArchiveReadIngress()
{ }

void RealtimeControl::ArchiveReadIngress::incomingData(const SequenceValue::Transfer &values)
{
    {
        std::lock_guard<std::mutex> lock(control->pendingMutex);
        Util::append(values, control->pendingArchive);
    }
    control->scheduleServiceIfNeeded();
}

void RealtimeControl::ArchiveReadIngress::incomingData(SequenceValue::Transfer &&values)
{
    {
        std::lock_guard<std::mutex> lock(control->pendingMutex);
        Util::append(std::move(values), control->pendingArchive);
    }
    control->scheduleServiceIfNeeded();
}

void RealtimeControl::ArchiveReadIngress::incomingData(const SequenceValue &value)
{
    {
        std::lock_guard<std::mutex> lock(control->pendingMutex);
        control->pendingArchive.emplace_back(value);
    }
    control->scheduleServiceIfNeeded();
}

void RealtimeControl::ArchiveReadIngress::incomingData(SequenceValue &&value)
{
    {
        std::lock_guard<std::mutex> lock(control->pendingMutex);
        control->pendingArchive.emplace_back(std::move(value));
    }
    control->scheduleServiceIfNeeded();
}

void RealtimeControl::ArchiveReadIngress::endData()
{
    {
        std::lock_guard<std::mutex> lock(control->pendingMutex);
        control->pendingArchiveEnd = true;
    }
    control->scheduleServiceIfNeeded();
}

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

    QString translateLocation;
    QByteArray cpd3(qgetenv("CPD3"));
    if (cpd3.length() > 0) {
        translateLocation = QString(cpd3) + "/locale";
    }
    QTranslator cpd3Translator;
    cpd3Translator.load("cpd3_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cpd3Translator);
    QTranslator cliTranslator;
    cliTranslator.load("cli_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cliTranslator);
    QTranslator realtimecontrolTranslator;
    realtimecontrolTranslator.load("realtimecontrol_" + QLocale::system().name(),
                                   translateLocation);
    app.installTranslator(&realtimecontrolTranslator);

    Abort::installAbortHandler(false);

    Variant::Root realtime;
    {
        QByteArray checkOverride(qgetenv("CPD3REALTIMEOVERRIDE"));
        if (!checkOverride.isEmpty())
            realtime.write().setString(QString(checkOverride));
    }
    std::unique_ptr<AcquisitionNetworkClient> network(new AcquisitionNetworkClient(realtime));
    RealtimeControl realtimeControl(network.get());
    realtimeControl.parseArguments(app.arguments());
    network->start();

    AbortPoller poller;
    QObject::connect(&poller, SIGNAL(aborted()), &app, SLOT(quit()));
    QObject::connect(&realtimeControl, SIGNAL(finished()), &app, SLOT(quit()));
    poller.start();
    int c = app.exec();
    network.reset();
    ThreadPool::system()->wait();
    return c;
}
