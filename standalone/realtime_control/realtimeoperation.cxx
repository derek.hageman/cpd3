/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLoggingCategory>
#include <QFileInfo>

#include "realtimeoperation.hxx"
#include "realtime_control.hxx"
#include "core/qtcompat.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/variant/parse.hxx"
#include "io/drivers/stdio.hxx"
#include "io/drivers/file.hxx"


Q_LOGGING_CATEGORY(log_realtime_data, "cpd3.realtime.data", QtWarningMsg)

Q_LOGGING_CATEGORY(log_realtime_event, "cpd3.realtime.event", QtWarningMsg)


using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;


RealtimeOperation::RealtimeOperation(RealtimeControl *c) : control(c)
{ }

RealtimeOperation::~RealtimeOperation()
{ }

void RealtimeOperation::complete()
{
    control->completeOperation(this);
    deleteLater();
}

bool RealtimeOperation::activate()
{ return true; }

void RealtimeOperation::incomingRealtime(const SequenceValue::Transfer &values)
{ Q_UNUSED(values); }

void RealtimeOperation::incomingArchive(const SequenceValue::Transfer &values)
{ Q_UNUSED(values); }

void RealtimeOperation::endArchive()
{ }


DebugRealtime::DebugRealtime(RealtimeControl *control, const QString &arguments)
        : RealtimeOperation(control), selection(SequenceMatch::Element::SpecialMatch::All)
{
    if (!arguments.isEmpty())
        selection = SequenceMatch::Composite(Variant::Root(arguments), {}, {}, {}, {}, {}, {});
}

DebugRealtime::~DebugRealtime()
{ }

bool DebugRealtime::activate()
{ return false; }

void DebugRealtime::incomingRealtime(const SequenceValue::Transfer &values)
{
    for (const auto &value : values) {
        if (!selection.matches(value.getUnit()))
            continue;
        qCDebug(log_realtime_data).nospace() << Logging::time(value.getStart()) << " ["
                                             << value.getStation() << ',' << value.getArchive()
                                             << ',' << value.getVariable() << ','
                                             << value.getFlavors() << "] = " << value.read();
    }
}

DebugEvents::DebugEvents(RealtimeControl *control, const QString &arguments) : RealtimeOperation(
        control)
{
    QStringList matchers(arguments.split(':', QString::SkipEmptyParts));
    if (matchers.isEmpty() ||
            matchers.contains("interfaceInformationUpdated", Qt::CaseInsensitive)) {
        control->getNetwork()
               ->interfaceInformationUpdated
               .connect(std::bind(&DebugEvents::interfaceInformationUpdated, this,
                                  std::placeholders::_1));
    }
    if (matchers.isEmpty() || matchers.contains("interfaceStateUpdated", Qt::CaseInsensitive)) {
        control->getNetwork()
               ->interfaceStateUpdated
               .connect(
                       std::bind(&DebugEvents::interfaceStateUpdated, this, std::placeholders::_1));
    }
    if (matchers.isEmpty() || matchers.contains("connectionState", Qt::CaseInsensitive)) {
        control->getNetwork()
               ->connectionState
               .connect(std::bind(&DebugEvents::connectionState, this, std::placeholders::_1));
    }
    if (matchers.isEmpty() || matchers.contains("connectionFailed", Qt::CaseInsensitive)) {
        control->getNetwork()
               ->connectionFailed
               .connect(std::bind(&DebugEvents::connectionFailed, this));
    }
    if (matchers.isEmpty() || matchers.contains("", Qt::CaseInsensitive)) {
        control->getNetwork()
               ->realtimeEvent
               .connect(std::bind(&DebugEvents::realtimeEvent, this, std::placeholders::_1));
    }
    if (matchers.isEmpty() || matchers.contains("autoprobeStateUpdated", Qt::CaseInsensitive)) {
        control->getNetwork()
               ->autoprobeStateUpdated
               .connect(std::bind(&DebugEvents::autoprobeStateUpdated, this));
    }
}

DebugEvents::~DebugEvents()
{ }

bool DebugEvents::activate()
{ return false; }

void DebugEvents::interfaceInformationUpdated(const std::string &name)
{
    qCDebug(log_realtime_event) << "interfaceInformationUpdated(" << name << ") = "
                                << control->getNetwork()->getInterfaceInformation(name);
}

void DebugEvents::interfaceStateUpdated(const std::string &name)
{
    qCDebug(log_realtime_event) << "interfaceStateUpdated(" << name << ") = "
                                << control->getNetwork()->getInterfaceState(name);
}

void DebugEvents::connectionState(bool connected)
{
    if (connected) {
        qCDebug(log_realtime_event) << "connectionEstablished";
    } else {
        qCDebug(log_realtime_event) << "connectionLost";
    }
}

void DebugEvents::connectionFailed()
{
    qCDebug(log_realtime_event) << "connectionFailed";
}

void DebugEvents::realtimeEvent(const CPD3::Data::Variant::Root &event)
{
    qCDebug(log_realtime_event) << "realtimeEvent = " << event.read();
}

void DebugEvents::autoprobeStateUpdated()
{
    qCDebug(log_realtime_event) << "autoprobeStateUpdated = "
                                << control->getNetwork()->getAutoprobeState();
}

StreamRealtime::StreamRealtime(RealtimeControl *control) : RealtimeOperation(control), chain(NULL)
{ }

StreamRealtime::~StreamRealtime()
{
    if (chain != NULL) {
        if (target != NULL)
            target->endData();
        chain->signalTerminate();
        chain->waitInEventLoop();
        delete chain;
    }
}

bool StreamRealtime::activate()
{
    chain = new StreamPipeline;
    target = chain->setInputExternal();
    chain->setOutputPipeline();
    chain->start();
    return false;
}

void StreamRealtime::incomingRealtime(const SequenceValue::Transfer &values)
{
    if (target == NULL)
        return;
    target->incomingData(values);
}


Flush::Flush(RealtimeControl *control, const QString &arguments) : RealtimeOperation(control),
                                                                   time(FP::undefined())
{
    if (!arguments.isEmpty()) {
        bool ok = false;
        double v = arguments.toDouble(&ok);
        if (ok && FP::defined(v) && v >= 0.0)
            time = v;
    }
}

Flush::~Flush()
{ }

bool Flush::activate()
{
    if (FP::defined(time))
        control->getNetwork()->flushRequest(time);
    else
        control->getNetwork()->flushRequest();
    QMetaObject::invokeMethod(this, "complete", Qt::QueuedConnection);
    return true;
}


DataFlush::DataFlush(RealtimeControl *control) : RealtimeOperation(control)
{ }

DataFlush::~DataFlush()
{ }

bool DataFlush::activate()
{
    control->getNetwork()->dataFlush();
    QMetaObject::invokeMethod(this, "complete", Qt::QueuedConnection);
    return true;
}

Restart::Restart(RealtimeControl *control) : RealtimeOperation(control)
{ }

Restart::~Restart()
{ }

bool Restart::activate()
{
    control->getNetwork()->restartRequested();
    QMetaObject::invokeMethod(this, "complete", Qt::QueuedConnection);
    return true;
}


Bypass::Bypass(RealtimeControl *control, const QString &arguments) : RealtimeOperation(control),
                                                                     flag(arguments.toStdString())
{ }

Bypass::~Bypass()
{ }

bool Bypass::activate()
{
    if (!flag.empty())
        control->getNetwork()->bypassFlagSet(flag);
    else
        control->getNetwork()->bypassFlagSet();
    QMetaObject::invokeMethod(this, "complete", Qt::QueuedConnection);
    return true;
}

UnBypass::UnBypass(RealtimeControl *control, const QString &arguments) : RealtimeOperation(control),
                                                                         flag(arguments.toStdString())
{ }

UnBypass::~UnBypass()
{ }

bool UnBypass::activate()
{
    if (!flag.empty())
        control->getNetwork()->bypassFlagClear(flag);
    else
        control->getNetwork()->bypassFlagClear();
    QMetaObject::invokeMethod(this, "complete", Qt::QueuedConnection);
    return true;
}

BypassOverride::BypassOverride(RealtimeControl *control) : RealtimeOperation(control)
{ }

BypassOverride::~BypassOverride()
{ }

bool BypassOverride::activate()
{
    control->getNetwork()->bypassFlagClearAll();
    QMetaObject::invokeMethod(this, "complete", Qt::QueuedConnection);
    return true;
}


Flag::Flag(RealtimeControl *control, const QString &arguments) : RealtimeOperation(control),
                                                                 flag(arguments.toStdString())
{ }

Flag::~Flag()
{ }

bool Flag::activate()
{
    if (!flag.empty())
        control->getNetwork()->systemFlagSet(flag);
    else
        control->getNetwork()->systemFlagSet();
    QMetaObject::invokeMethod(this, "complete", Qt::QueuedConnection);
    return true;
}

UnFlag::UnFlag(RealtimeControl *control, const QString &arguments) : RealtimeOperation(control),
                                                                     flag(arguments.toStdString())
{ }

UnFlag::~UnFlag()
{ }

bool UnFlag::activate()
{
    if (!flag.empty())
        control->getNetwork()->systemFlagClear(flag);
    else
        control->getNetwork()->systemFlagClear();
    QMetaObject::invokeMethod(this, "complete", Qt::QueuedConnection);
    return true;
}

FlagOverride::FlagOverride(RealtimeControl *control) : RealtimeOperation(control)
{ }

FlagOverride::~FlagOverride()
{ }

bool FlagOverride::activate()
{
    control->getNetwork()->systemFlagClearAll();
    QMetaObject::invokeMethod(this, "complete", Qt::QueuedConnection);
    return true;
}


WaitConnected::WaitConnected(RealtimeControl *control) : RealtimeOperation(control)
{
    control->getNetwork()->connectionState.connect(this, [this](bool connected) {
        if (!connected)
            return;
        this->complete();
    }, true);
}

WaitConnected::~WaitConnected()
{ }

bool WaitConnected::activate()
{ return false; }

OperationWait::OperationWait(RealtimeControl *control, const QString &arguments)
        : RealtimeOperation(control), msecs(1000)
{
    bool ok = false;
    double v = arguments.toDouble(&ok);
    if (ok && FP::defined(v) && v >= 0.0)
        msecs = floor(v * 1000.0 + 0.5);
}

OperationWait::~OperationWait()
{ }

bool OperationWait::activate()
{
    QTimer::singleShot(msecs, this, SLOT(complete()));
    return true;
}

Delay::Delay(RealtimeControl *control, const QString &delay, RealtimeOperation *op)
        : RealtimeOperation(control), msecs(1000), operation(op)
{
    bool ok = false;
    double v = delay.toDouble(&ok);
    if (ok && FP::defined(v) && v >= 0.0)
        msecs = floor(v * 1000.0 + 0.5);
}

Delay::~Delay()
{
    if (operation != NULL)
        delete operation;
}

bool Delay::activate()
{
    QTimer::singleShot(msecs, this, SLOT(process()));
    return false;
}

void Delay::process()
{
    if (operation != NULL) {
        control->enqueueOperation(operation);
        operation = NULL;
    }
    QMetaObject::invokeMethod(this, "complete", Qt::QueuedConnection);
}

static std::pair<Util::ByteView, Util::ByteView> splitLine(const Util::ByteView &line)
{
    std::size_t pathLength = 0;
    for (std::size_t max = line.size(); pathLength < max; ++pathLength) {
        if (line[pathLength] == '\\') {
            ++pathLength;
            if (pathLength >= max)
                break;
            ++pathLength;
        } else if (line[pathLength] == ',') {
            break;
        }
    }
    if (pathLength >= line.size())
        return {line, Util::ByteView()};
    return {line.mid(0, pathLength), line.mid(pathLength + 1)};
}

static Data::Variant::Root readSingleValue(const QString &dataSource)
{
    Util::ByteArray dataContent;
    if (dataSource.isEmpty() || dataSource == "-") {
        dataContent = IO::Access::stdio_in()->stream()->readAll();
    } else {
        if (QFileInfo(dataSource).exists()) {
            auto handle = IO::Access::file(dataSource, IO::File::Mode::readOnly());
            if (handle) {
                auto stream = handle->stream();
                if (stream) {
                    dataContent = stream->readAll();
                }
            }
        } else {
            dataContent = dataSource.toUtf8();
        }
    }

    if (dataContent.empty())
        return {};

    if (dataContent.string_start("/")) {
        Data::Variant::Root result;

        auto it = Util::ByteArray::LineIterator(dataContent);
        while (auto line = it.next()) {
            auto contents = splitLine(line);
            Data::Variant::Parse::fromInputString(result[contents.first.toString()],
                                                  QString::fromUtf8(
                                                          contents.second.toQByteArrayRef()));
        }
        return result;
    }

    class SegmentReader : public StreamSink {
        SequenceSegment::Stream reader;
    public:
        SequenceSegment::Transfer result;

        SegmentReader() = default;

        void incomingData(const SequenceValue::Transfer &values) override
        { Util::append(reader.add(values), result); }

        void incomingData(SequenceValue::Transfer &&values) override
        { Util::append(reader.add(std::move(values)), result); }

        void incomingData(const SequenceValue &value) override
        { Util::append(reader.add(value), result); }

        void incomingData(SequenceValue &&value) override
        { Util::append(reader.add(std::move(value)), result); }

        void endData() override
        { Util::append(reader.finish(), result); }
    };

    StandardDataInput dataInput;
    SegmentReader reader;
    dataInput.start();
    dataInput.setEgress(&reader);
    dataInput.incomingData(std::move(dataContent));
    dataInput.endData();

    auto result = SequenceSegment::reduce(reader.result);
    if (result.empty())
        return {};
    return std::move(result.front().root());
}

Command::Command(RealtimeControl *control, const QString &arguments) : RealtimeOperation(control)
{
    auto fields = arguments.split(':', QString::KeepEmptyParts);

    QString dataSource;
    if (!fields.empty())
        dataSource = fields.takeFirst();
    if (!fields.empty())
        target = fields.takeFirst().toStdString();

    command = readSingleValue(dataSource);
}

Command::~Command() = default;

bool Command::activate()
{
    control->getNetwork()->issueCommand(target, command);
    QMetaObject::invokeMethod(this, "complete", Qt::QueuedConnection);
    return true;
}

MessageLog::MessageLog(RealtimeControl *control, const QString &arguments) : RealtimeOperation(
        control)
{
    auto fields = arguments.split(':', QString::KeepEmptyParts);

    QString message;
    if (!fields.empty())
        message = fields.takeFirst();
    QString author;
    if (!fields.empty())
        author = fields.takeFirst();
    QString dataSource;
    if (!fields.empty())
        dataSource = fields.takeFirst();

    if (!dataSource.isEmpty())
        event = readSingleValue(dataSource);
    if (!message.isEmpty())
        event["Text"].setString(message);
    if (!author.isEmpty())
        event["Author"].setString(author);
}

MessageLog::~MessageLog() = default;

bool MessageLog::activate()
{
    control->getNetwork()->messageLogEvent(event);
    QMetaObject::invokeMethod(this, "complete", Qt::QueuedConnection);
    return true;
}
