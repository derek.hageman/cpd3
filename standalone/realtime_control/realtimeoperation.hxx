/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef REALTIMEOPERATION_H
#define REALTIMEOPERATION_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QList>
#include <QStringList>

#include "acquisition/acquisitionnetwork.hxx"
#include "datacore/stream.hxx"
#include "datacore/sequencematch.hxx"
#include "datacore/streampipeline.hxx"

class RealtimeControl;

class RealtimeOperation : public QObject {
Q_OBJECT

protected:
    RealtimeControl *control;
public:
    RealtimeOperation(RealtimeControl *control);

    virtual ~RealtimeOperation();

    virtual bool activate();

    virtual void incomingRealtime(const CPD3::Data::SequenceValue::Transfer &values);

    virtual void incomingArchive(const CPD3::Data::SequenceValue::Transfer &values);

    virtual void endArchive();

public slots:

    void complete();
};

class DebugRealtime : public RealtimeOperation {
    CPD3::Data::SequenceMatch::Composite selection;
public:
    DebugRealtime(RealtimeControl *control, const QString &arguments);

    virtual ~DebugRealtime();

    virtual bool activate();

    virtual void incomingRealtime(const CPD3::Data::SequenceValue::Transfer &values);
};

class DebugEvents : public RealtimeOperation {

    void interfaceInformationUpdated(const std::string &name);

    void interfaceStateUpdated(const std::string &name);

    void connectionState(bool connected);

    void connectionFailed();

    void realtimeEvent(const CPD3::Data::Variant::Root &event);

    void autoprobeStateUpdated();

public:
    DebugEvents(RealtimeControl *control, const QString &arguments);

    virtual ~DebugEvents();

    virtual bool activate();
};

class StreamRealtime : public RealtimeOperation {
    CPD3::Data::StreamPipeline *chain;
    CPD3::Data::StreamSink *target;
public:
    StreamRealtime(RealtimeControl *control);

    virtual ~StreamRealtime();

    virtual bool activate();

    virtual void incomingRealtime(const CPD3::Data::SequenceValue::Transfer &values);
};

class Flush : public RealtimeOperation {
    double time;
public:
    Flush(RealtimeControl *control, const QString &arguments);

    virtual ~Flush();

    virtual bool activate();
};

class DataFlush : public RealtimeOperation {
public:
    DataFlush(RealtimeControl *control);

    virtual ~DataFlush();

    virtual bool activate();
};

class Restart : public RealtimeOperation {
public:
    Restart(RealtimeControl *control);

    virtual ~Restart();

    virtual bool activate();
};

class Bypass : public RealtimeOperation {
    CPD3::Data::Variant::Flag flag;
public:
    Bypass(RealtimeControl *control, const QString &arguments);

    virtual ~Bypass();

    virtual bool activate();
};

class UnBypass : public RealtimeOperation {
    CPD3::Data::Variant::Flag flag;
public:
    UnBypass(RealtimeControl *control, const QString &arguments);

    virtual ~UnBypass();

    virtual bool activate();
};

class BypassOverride : public RealtimeOperation {
public:
    BypassOverride(RealtimeControl *control);

    virtual ~BypassOverride();

    virtual bool activate();
};

class Flag : public RealtimeOperation {
    CPD3::Data::Variant::Flag flag;
public:
    Flag(RealtimeControl *control, const QString &arguments);

    virtual ~Flag();

    virtual bool activate();
};

class UnFlag : public RealtimeOperation {
    CPD3::Data::Variant::Flag flag;
public:
    UnFlag(RealtimeControl *control, const QString &arguments);

    virtual ~UnFlag();

    virtual bool activate();
};

class FlagOverride : public RealtimeOperation {
public:
    FlagOverride(RealtimeControl *control);

    virtual ~FlagOverride();

    virtual bool activate();
};

class WaitConnected : public RealtimeOperation {
public:
    WaitConnected(RealtimeControl *control);

    virtual ~WaitConnected();

    virtual bool activate();
};

class OperationWait : public RealtimeOperation {
Q_OBJECT
    int msecs;
public:
    OperationWait(RealtimeControl *control, const QString &arguments);

    virtual ~OperationWait();

    virtual bool activate();
};

class Delay : public RealtimeOperation {
Q_OBJECT
    int msecs;
    RealtimeOperation *operation;
public:
    Delay(RealtimeControl *control, const QString &delay, RealtimeOperation *op);

    virtual ~Delay();

    virtual bool activate();

private slots:

    void process();
};

class Command : public RealtimeOperation {
    std::string target;
    CPD3::Data::Variant::Root command;
public:
    Command(RealtimeControl *control, const QString &arguments);

    virtual ~Command();

    virtual bool activate();
};

class MessageLog : public RealtimeOperation {
    CPD3::Data::Variant::Root event;
public:
    MessageLog(RealtimeControl *control, const QString &arguments);

    virtual ~MessageLog();

    virtual bool activate();
};

#endif
