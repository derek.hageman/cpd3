/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef REALTIMECONTROL_H
#define REALTIMECONTROL_H

#include "core/first.hxx"

#include <mutex>
#include <QtGlobal>
#include <QObject>
#include <QList>
#include <QStringList>
#include <QAtomicInt>

#include "acquisition/acquisitionnetwork.hxx"

class RealtimeOperation;

class RealtimeControl : public QObject {
Q_OBJECT

    CPD3::Acquisition::AcquisitionNetworkClient *network;

    class RealtimeIngress : public CPD3::Data::StreamSink {
        RealtimeControl *control;
    public:
        RealtimeIngress(RealtimeControl *control);

        virtual ~RealtimeIngress();

        void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

        void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

        void incomingData(const CPD3::Data::SequenceValue &value) override;

        void incomingData(CPD3::Data::SequenceValue &&value) override;

        void endData() override;
    };

    friend class RealtimeIngress;

    RealtimeIngress realtimeIngress;

    class ArchiveReadIngress : public CPD3::Data::StreamSink {
        RealtimeControl *control;
    public:
        ArchiveReadIngress(RealtimeControl *control);

        virtual ~ArchiveReadIngress();

        void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

        void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

        void incomingData(const CPD3::Data::SequenceValue &value) override;

        void incomingData(CPD3::Data::SequenceValue &&value) override;

        void endData() override;
    };

    friend class ArchiveReadIngress;

    ArchiveReadIngress archiveReadIngress;

    QAtomicInt serviceCounter;

    RealtimeOperation *current;
    QList<RealtimeOperation *> background;
    QList<RealtimeOperation *> pending;

    std::mutex pendingMutex;
    CPD3::Data::SequenceValue::Transfer pendingRealtime;
    CPD3::Data::SequenceValue::Transfer pendingArchive;
    bool pendingArchiveEnd;

    RealtimeOperation *createOperation(const QString &data);

public:
    RealtimeControl(CPD3::Acquisition::AcquisitionNetworkClient *network);

    ~RealtimeControl();

    void parseArguments(QStringList arguments);

    void completeOperation(RealtimeOperation *operation);

    void enqueueOperation(RealtimeOperation *operation);

    inline CPD3::Acquisition::AcquisitionNetworkClient *getNetwork()
    { return network; }

public slots:

    void serviceUpdate();

    void scheduleServiceIfNeeded();

    void checkFinished();

signals:

    void finished();
};

#endif
