/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QObject>
#include <QtDebug>
#include <QProcess>
#include <QString>
#include <QEventLoop>
#include <QTemporaryFile>
#include <QTimer>

#include "core/waitutils.hxx"

using namespace CPD3;

static const QString modelDefaultParameters("*\n"
                                                    "* ==[ PARAMETERS FOR DMT CCN COUNTER SIMULATOR - UNITS IN SI ]==\n"
                                                    "*\n"
                                                    "===[ INLET PARAMETRES ]=================================================\n"
                                                    "Parabolic inlet velocity profile  [UPARAB]\n"
                                                    ".TRUE.\n"
                                                    "Sheath, Aerosol inlet RH (0-1 scale) [RHINS, RHINA]\n"
                                                    "1.0, 0.95\n"
                                                    "===[ GRID PARAMETERS ]==================================================\n"
                                                    "Number of cells in X-direction, Y-direction  [NI, NJ]\n"
                                                    "50, 50\n"
                                                    "===[ DEPENDENT VARIABLES ]==============================================\n"
                                                    "Solve for U, V, P equations ?\n"
                                                    ".false.\n"
                                                    "Solve for T, C equations ?\n"
                                                    ".true., .true.\n"
                                                    "===[ TERMINATION CRITERIA FOR ITERATIONS ]==============================\n"
                                                    "Maximum number of iterations per time step  [MAXIT]\n"
                                                    "100\n"
                                                    "Maximum acceptable value of residuals  [SORMAX]\n"
                                                    "1.0E-6\n"
                                                    "===[ UNDER-RELAXATION ]=================================================\n"
                                                    "Under-relaxation factor for U, P equation  [URFU, URFP]\n"
                                                    "0.7, 0.05\n"
                                                    "Under-relaxation factor for T, C equation  [URFT, URFC]\n"
                                                    "1.0, 1.0\n"
                                                    "===[ EQUATION SOLVER SELECTION ]========================================\n"
                                                    "Solver selection for U, V, P equation  [ISLVU, ISLVP]\n"
                                                    "1, 1\n"
                                                    "Solver selection for T, C equation  [ISLVT, ISLVC]\n"
                                                    "2, 2\n"
                                                    "===[ INPUT-OUTPUT PARAMETERS ]==========================================\n"
                                                    "Exit S is average over last (%) of chamber (0=exit point only) [SOAVGP]\n"
                                                    "5\n");

class TestComponent : public QObject {
Q_OBJECT

private slots:

    void general()
    {
        QProcess p;

        QProcessEnvironment pe(p.processEnvironment());
        pe.insert("GFORTRAN_UNBUFFERED_ALL", "1");
        p.setProcessEnvironment(pe);

        p.start("cpd3_ccn_dmtmodel");
        QVERIFY(p.waitForStarted(30000));

        QTemporaryFile parametersFile;
        QVERIFY(parametersFile.open());
        {
            QByteArray prameterData(modelDefaultParameters.toUtf8());
            while (!prameterData.isEmpty()) {
                qint64 n = parametersFile.write(prameterData);
                if (n <= 0)
                    break;
                prameterData.remove(0, (int) n);
            }
        }
        parametersFile.flush();
        parametersFile.close();

        QByteArray toWrite(parametersFile.fileName().toUtf8());
        toWrite.append(
                "\n8.333333E-06, 9.000000E+00, 1.013250E+05, 2.931500E+02, 2.941500E+02, 2.961500E+02, 'nul'\n");
        while (!toWrite.isEmpty()) {
            qint64 n = p.write(toWrite);
            if (n <= 0)
                break;
            toWrite.remove(0, (int) n);
        }
        QVERIFY(toWrite.isEmpty());

        ElapsedTimer realTime;

        QByteArray readBuffer;
        realTime.start();
        while (realTime.elapsed() < 30000) {
            QEventLoop el;
            connect(&p, SIGNAL(readyRead()), &el, SLOT(quit()), Qt::QueuedConnection);
            QTimer::singleShot(1000, &el, SLOT(quit()));
            qint64 n = p.bytesAvailable();
            if (n <= 0) {
                el.exec();
                continue;
            }
            int oldSize = readBuffer.size();
            int maximumRead = qMin(65536, (int) n);
            readBuffer.resize(oldSize + maximumRead);
            n = p.read(readBuffer.data() + oldSize, maximumRead);
            QVERIFY(n >= 0);
            readBuffer.resize(oldSize + n);

            if (readBuffer.contains('\n') || readBuffer.contains('\r'))
                break;
        }
        p.close();

        static const double rounding = 10000.0;
        double value = floor(readBuffer.trimmed().toDouble() * rounding + 0.5) / rounding;
        double expected = floor(0.17279387 * rounding + 0.5) / rounding;

        QCOMPARE(value, expected);
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
