C=======================================================================
C
C *** INCLUDE FILE 'PARCEL.INC'
C *** THIS FILE CONTAINS THE DECLARATIONS OF THE GLOBAL CONSTANTS
C     AND VARIABLES FOR AEROSOL GROWTH.
C
C=======================================================================
C
      PARAMETER (NSMX = 3) ! # of sections
      DOUBLE PRECISION RTOL, ATOL
      REAL             MWA, MWW, KAPA, NPART, INSFR, INSDNS
      INTEGER          NSTOT
C
      COMMON /DISINP/ DPAVG, SIGV, ANTOT, PDNS, PMW, PVHF
C
     &       /AEROSL/ SLDMAS(NSMX),DPART(NSMX), DIFF(NSMX), KAPA(NSMX),
     &                SLDDNS(NSMX),SLDMW(NSMX), NPART(NSMX),DISCF(NSMX),
     &                DPMIN(NSMX), PARDNS(NSMX),INSFR(NSMX),SATRE(NSMX),
     &                INSDNS(NSMX),DVDT, NSEC, NSTOT
C
     &       /PARCEL/ TPAR, PRESS, SATR
C
     &       /PHYPRP/ DENAP, DENW, THCAP, SURFT, PSATP, DHVAP, MWA, MWW
C
     &       /LSDPAR/ RTOL, ATOL, ITOL, ITASK, ISTATE, IOPT, LRW1, 
     &                LIW1, MF
C
     &       /OTHER / RGAS, ZERO, GRAV
C
C *** END OF INCLUDE FILE **********************************************
C
