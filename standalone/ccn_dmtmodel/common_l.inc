C=======================================================================
C
C *** TEACHER CODE
C *** INCLUDE FILE 'COMMONL.INC'
C *** THIS FILE CONTAINS THE DECLARATIONS OF THE GLOBAL CONSTANTS
C     AND VARIABLES. THIS VERSION IS FOR LAMINAR FLOWS
C
C=======================================================================
C
      PARAMETER(II=120,JJ=120)
cc      IMPLICIT DOUBLE PRECISION (A-H,O-Z)
C
C *** GRID VARIABLES ***************************************************
C
      LOGICAL POLAR
      COMMON/CGRID/POLAR,NI,NJ,NIM1,NJM1,X(II),Y(JJ),XU(II),YV(JJ),
     &             R(JJ),RV(JJ),RCV(JJ),DXEP(II),DXPW(II),DXEPU(II),
     &             DXPWU(II),SEW(II),SEWU(II),DYNP(JJ),DYPS(JJ),
     &             DYNPV(JJ),DYPSV(JJ),SNS(JJ),SNSV(JJ)
C
C *** TIME VARIABLES ***************************************************
C
      LOGICAL STEADY
      COMMON/CTIME/STEADY,NT,IT,DT,TIME
C
C *** DEPENDENT VARIABLES **********************************************
C
      LOGICAL INCALU,INCALV,INCALP,INCALT,INCALC
      COMMON/CVAR/INCALU,INCALV,INCALP,INCALT,INCALC,
     &            U(II,JJ),V(II,JJ),P(II,JJ),PP(II,JJ),T(II,JJ),
     &            C(II,JJ),PSI(II,JJ),
     &            CO(II,JJ),UO(II,JJ),VO(II,JJ),TO(II,JJ)
C
C *** COEFFICIENTS OF EQUATIONS ****************************************
C
      COMMON/CCOEF/AP(II,JJ),AN(II,JJ),AS(II,JJ),AE(II,JJ),AW(II,JJ),
     &             SU(II,JJ),SP(II,JJ),DU(II,JJ),DV(II,JJ)
C
C *** THERMO-PHYSICAL PROPERTIES ***************************************
C
      LOGICAL INPRO
      COMMON/CPROP/INPRO,VISCOS,DENSIT,PRANDT,SCHMID,DEN(II,JJ),
     &             DENO(II,JJ), VIS(II,JJ),GAMH(II,JJ),GAMC(II,JJ)
C
C *** TERMINATION CRITERIA FOR ITERATIONS ******************************
C
      COMMON/CTERM/MAXIT,NITER,SORMAX,SNORM,SNORU,SNORV,SNORT,SNORC,
     &             RESORU,RESORV,RESORM,RESORT,RESORC
C
C *** UNDER-RELAXATION *************************************************
C
      COMMON/CRELX/URFU,URFV,URFP,URFT,URFC
C
C *** FIXED PRESSURE POINT *********************************************
C
      COMMON/CPRES/IPREF,JPREF
C
C *** EQUATION SOLVER SELECTION ****************************************
C
      COMMON/CSOLV/ISLVU,ISLVV,ISLVP,ISLVT,ISLVC,ALPHA
C
C *** TERMINATION CRITERIA FOR EQUATION SOLVER *************************
C
      COMMON/CTERS/LITERU,LITERV,LITERP,LITERT,LITERC,
     &             SORU,SORV,SORP,SORT,SORC
C
C *** INPUT-OUTPUT PARAMETERS ******************************************
C
      CHARACTER*12 OUTFIL,RSTFIL,SAVFIL
      CHARACTER*21 HEDU,HEDV,HEDP,HEDT,HEDC
      LOGICAL INREAD,INWRIT,INPLOT,INPRES,INTEMP,INCONC
      COMMON/CIO/IOINP,IOUSR,IOFIL,IOAUX,IOCON,OUTFIL,RSTFIL,SAVFIL,
     &           INREAD,INWRIT,INPLOT,INPRES,INTEMP,INCONC,
     &           IMON,JMON,NITPRI,NSTPRI,HEDU,HEDV,HEDP,HEDT,HEDC
C
C *** GENERAL VARIABLES ************************************************
C
      COMMON/CGEN/GREAT,TINY,PI
C
C *** END OF FILE 'COMMONL.INC' ****************************************
C
CC
