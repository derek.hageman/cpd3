#include <stdio.h>
// Only fancy thing here is the naming - to get around g77's name-mangler
int trvflush_(void) { 
    fflush(stdout); 
    return 0; 
}
#ifdef __GNUC__

static void dummy(void) { }
void MAIN__ () __attribute__ ((weak, alias ("dummy")));

int fakeMain(int argc, char **argv) {
    (void)argc;
    (void)argv;
    MAIN__();
    return 0;
}
int main(int argc, char **argv) __attribute__ ((weak, alias ("fakeMain")));

#else

void MAIN__(void);
int main(int argc, char **argv) {
    (void)argc;
    (void)argv;
    MAIN__();
    return 0;
}

#endif
