C=======================================================================
C
C *** TEACHER CODE
C *** INCLUDE FILE 'PROB.INC'
C *** THIS FILE CONTAINS DECLARATIONS OF PROBLEM-SPECIFIC VARIABLES
C
C=======================================================================
C
      LOGICAL   UPARAB,OUTPF
      INTEGER   SCT
     
      COMMON/CPROB/UPARAB,RADIUS,RADINJ,TUBLEN,UINS,UINA,TINS,TINA,
     &             RHINS,RHINA,TCOLD,THOT,FLOWIN,XMONIN,HEATIN,PRESF,
     &             VFL,RAT,NJAER,OUTPF,SOAVGP
     &      /CHGRD/FEXPX,FEXPY
C
      COMMON /CPAR/ DNCEL(II,JJ)
C
C *** END OF FILE 'PROB.INC' ******************************************
C
CC
