/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <unordered_set>
#include <QStringList>

#include "output_cpd2.hxx"
#include "clicore/terminaloutput.hxx"
#include "clicore/archiveparse.hxx"
#include "clicore/optionparse.hxx"
#include "clicore/argumentparser.hxx"
#include "core/timeparse.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::CLI;
using namespace CPD3::Data;

class Parser : public ArgumentParser {
    QString programName;
public:
    Parser(const QString &programNameIn) :
#ifndef NO_CONSOLE
            ArgumentParser(true),
#else
            ArgumentParser(false),
#endif
            programName(programNameIn)
    { }

    virtual ~Parser()
    { }

protected:

    virtual QList<BareWordHelp> getBareWordHelp()
    {
        return QList<BareWordHelp>() << BareWordHelp(tr("station", "station argument name"),
                                                     tr("The (default) station to read data from"),
                                                     tr("Inferred from the current directory"),
                                                     tr("The \"station\" bare word argument is used to specify the station(s) to "
                                                        "generate data for.  This is the three digit station identification "
                                                        "code.  For example \"brw\".  Multiple stations may be specified by separating "
                                                        "them with \":\", \";\" or \",\".  Regular expressions are accepted.  If "
                                                        "absent then a default station is attempted to be inferred from the working "
                                                        "directory.  This may also be specified as the special value \"allstations\""
                                                        "to generate data from all available stations.  Note that most CPD2 processing "
                                                        "programs do not support multiple stations, so using that mode of operation "
                                                        "may produce invalid data."))
                                     << BareWordHelp(tr("record", "records argument name"),
                                                     tr("The list of CPD2 records to generate"),
                                                     QString(),
                                                     tr("The \"record\" bare word argument sets the list of CPD2 records to "
                                                        "generate.  Multiple records may be specified by separating "
                                                        "them with \":\", \";\" or \",\".  For example \"S11a\".  Regular expressions "
                                                        "are accepted, but note that the stations specification is evaluated first, so "
                                                        "they must not match both if the station is omitted.  The special value "
                                                        "\"everything\" is accepted to generate data for all available records."))
                                     << BareWordHelp(tr("times", "times argument name"),
                                                     tr("The time range of data to read"),
                                                     QString(),
                                                     TimeParse::describeListBoundsUsage(false,
                                                                                        true))
                                     << BareWordHelp(tr("archive", "archive argument name"),
                                                     tr("The (default) archive to read data from"),
                                                     tr("The \"raw\" data archive"),
                                                     tr("The \"archive\" bare word argument is used to specify the archive used to "
                                                        "Look up variables that do not include an archive station as part of an "
                                                        "archive read specification.  This is the internal archive name, such as "
                                                        "\"raw\" or \"clean\".  Multiple archives may be specified by separating them "
                                                        "with \":\", \";\" or \",\".  Regular expressions are accepted."));
    }

    virtual QString getBareWordCommandLine()
    { return tr("[station] [record] times [archives]", "bare word command line"); }

    virtual QString getProgramName()
    { return programName; }

    virtual QString getExamples(const QList<ComponentExample> &examples)
    {
        return generateExamples(examples, OptionParse::Example_ActionTimes);
    }

    virtual QHash<QString, QString> getDocumentationTypeID()
    {
        QHash<QString, QString> result;
        result.insert("type", "output_cpd2");
        return result;
    }
};

namespace {
struct GenerateRecord {
    std::vector<SequenceName::Component> stations;
    std::vector<SequenceName::Component> archives;
    QString record;
    SequenceSegment::Transfer configuration;
};
static const SequenceName::Component configurationArchive = "configuration";
static const SequenceName::Component configurationVariable = "output_cpd2";

class ArchiveRecordTimeHandler : public TimeParseListHandler {
public:
    Archive::Access &access;
    Archive::Selection::Match checkStations;
    Archive::Selection::Match checkArchives;

    QList<GenerateRecord> selectedRecords;

    QString firstInvalidRecord;
    bool stopHandling;

    ArchiveRecordTimeHandler(Archive::Access &access) : access(access)
    { }

    static bool recordMatches(SequenceSegment::Transfer &configuration,
                              const std::vector<SequenceName::Component> &effectiveStations,
                              const QString &checkRecord)
    {
        for (auto &seg : configuration) {
            for (const auto &station : effectiveStations) {
                auto records = seg.getValue(station, configurationArchive, configurationVariable)
                                  .hash("Records");

                if (QRegExp::escape(checkRecord) == checkRecord) {
                    Q_ASSERT(!checkRecord.isEmpty());
                    if (records.hash(checkRecord).exists())
                        return true;
                } else {
                    QRegExp re(checkRecord);
                    for (auto check : records.toHash()) {
                        if (check.first.empty())
                            continue;
                        if (re.exactMatch(QString::fromStdString(check.first)))
                            return true;
                    }
                }
            }
        }
        return false;
    }

    virtual bool handleLeading(const QString &str,
                               int index,
                               const QStringList &list) noexcept(false)
    {
        Q_UNUSED(index);
        Q_UNUSED(list);

        if (stopHandling)
            return false;
        if (str == OutputCPD2::tr("everything", "everything variable name")) {
            selectedRecords.clear();
            stopHandling = true;
            return true;
        }

        QList<GenerateRecord> addRecords;

        auto effectiveStations = checkStations;
        auto effectiveArchives = checkArchives;

        QStringList allItems(str.split(',', QString::KeepEmptyParts));
        for (QList<QString>::const_iterator recordItem = allItems.constBegin(),
                endAI = allItems.constEnd(); recordItem != endAI; ++recordItem) {
            QStringList items(recordItem->split(':', QString::KeepEmptyParts));
            if (items.isEmpty()) {
                firstInvalidRecord = str;
                return false;
            }

            QString checkRecord;
            bool recalcArchives = false;
            bool recalcStations = false;
            if (items.length() == 1) {
                checkRecord = items.at(0);
            } else if (items.length() == 2) {
                checkRecord = items.at(1);
                if (items.at(0).length() > 0) {
                    effectiveArchives.clear();
                    effectiveArchives.push_back(items.at(0).toStdString());
                    recalcArchives = true;
                }
            } else {
                checkRecord = items.at(2);
                if (items.at(1).length() > 0) {
                    effectiveArchives.clear();
                    effectiveArchives.push_back(items.at(1).toStdString());
                    recalcArchives = true;
                }
                if (items.at(0).length() > 0) {
                    effectiveStations.clear();
                    effectiveStations.push_back(items.at(0).toStdString());
                    recalcStations = true;
                }
            }
            if (checkRecord.length() == 0) {
                firstInvalidRecord = str;
                return false;
            }

            if (recalcStations || recalcArchives) {
                auto check = access.availableSelected(
                        Archive::Selection(FP::undefined(), FP::undefined(), effectiveStations,
                                           effectiveArchives).withMetaArchive(false)
                                                             .withDefaultStation(false),
                        Archive::Access::IndexOnly);
                if (recalcStations) {
                    effectiveStations.clear();
                    std::copy(check.stations.begin(), check.stations.end(),
                              Util::back_emplacer(effectiveStations));
                }
                if (recalcArchives) {
                    effectiveArchives.clear();
                    std::copy(check.archives.begin(), check.archives.end(),
                              Util::back_emplacer(effectiveArchives));
                }
            }

            auto configuration = SequenceSegment::Stream::read(
                    Archive::Selection(FP::undefined(), FP::undefined(), effectiveStations,
                                       {configurationArchive}, {configurationVariable}), &access);
            if (!recordMatches(configuration, effectiveStations, checkRecord)) {
                firstInvalidRecord = str;
                continue;
            }

            GenerateRecord add;
            add.stations = effectiveStations;
            add.archives = effectiveArchives;
            add.record = checkRecord;
            add.configuration = configuration;
            addRecords.append(add);
        }

        if (addRecords.isEmpty())
            return false;

        selectedRecords.append(addRecords);
        return true;
    }

    virtual bool handleTrailing(const QString &str,
                                int index,
                                const QStringList &list) noexcept(false)
    {
        Q_UNUSED(str);
        Q_UNUSED(index);
        Q_UNUSED(list);
        return false;
    }
};

struct GeneratedTracker {
    SequenceName::Component station;
    SequenceName::Component archive;
    QString record;

    GeneratedTracker(const SequenceName::Component &s,
                     const SequenceName::Component &a,
                     const QString &r) : station(s), archive(a), record(r)
    { }

    bool operator==(const GeneratedTracker &other) const
    {
        return station == other.station && archive == other.archive && record == other.record;
    }

    struct Hash {
        std::size_t operator()(const GeneratedTracker &t) const {
            std::size_t i = std::hash<SequenceName::Component>()(t.station);
            i = INTEGER::mix(i, std::hash<SequenceName::Component>()(t.archive));
            i = INTEGER::mix(i, std::hash<QString>()(t.record));
            return i;
        }
    };
};

}

static void argumentParsingError(const QString &error)
{
#ifndef NO_CONSOLE
    TerminalOutput::simpleWordWrap(error, true);
#else
    QMessageBox::critical(0, tr("Invalid Usage"), error);
#endif
    QCoreApplication::exit(1);
}

int OutputCPD2::parseArguments(QStringList workingArguments)
{
    {
        QString programName("cpd3_output_cpd2");
        if (!workingArguments.isEmpty())
            programName = workingArguments.takeFirst();

        ComponentOptions options;
        QList<ComponentExample> examples;

        Parser parser(programName);
        try {
            parser.parse(workingArguments, options, examples);
        } catch (ArgumentParsingException ape) {
            if (ape.isError()) {
#ifndef NO_CONSOLE
                TerminalOutput::simpleWordWrap(ape.getText(), true);
#else
                QMessageBox::critical(0, tr("Invalid Usage"), error);
#endif
                QCoreApplication::exit(1);
                return -1;
            } else {
                if (!ape.getText().isEmpty()) {
#ifndef NO_CONSOLE
                    TerminalOutput::simpleWordWrap(ape.getText());
#else
                    QMessageBox::information(0, tr("Help"), ape.getText());
#endif
                }
                QCoreApplication::exit(0);
            }
            return 1;
        }
    }

    Archive::Access::ReadLock lock(access);

    Archive::Selection::Match checkStations;
    bool useAllStations = false;
    bool usedStation = false;
    if (!workingArguments.isEmpty()) {
        for (const auto &add : workingArguments.front()
                                               .split(QRegExp("[:;,]+"), QString::SkipEmptyParts)) {
            if (add.toLower() != tr("allstations", "all stations string")) {
                checkStations.emplace_back(add.toStdString());
                continue;
            }
            checkStations.clear();
            useAllStations = true;
            usedStation = true;
            workingArguments.removeFirst();
            break;
        }
    }
    if (!checkStations.empty()) {
        /* Special cases to handle the first argument being a full
         * specification like "brw:raw:S11a", which would otherwise
         * result in "brw" with the rest ignored.
         */
        if (checkStations.size() == 1 ||
                !access.availableStations({checkStations.front()}).empty()) {
            auto stations = access.availableStations(checkStations);
            checkStations.clear();
            std::move(stations.begin(), stations.end(), Util::back_emplacer(checkStations));
            if (!checkStations.empty()) {
                usedStation = true;
                workingArguments.removeFirst();
            }
        } else {
            checkStations.clear();
        }
    }
    if (checkStations.empty() && !useAllStations) {
        checkStations.clear();
        auto implied = SequenceName::impliedStations(&access);
        std::copy(implied.begin(), implied.end(), Util::back_emplacer(checkStations));
    }
    if (workingArguments.isEmpty()) {
        if (usedStation)
            argumentParsingError(
                    tr("No record specification detected.  The only arguments appear to be stations."));
        else
            argumentParsingError(tr("No record specification detected."));
        return -1;
    }

    Archive::Selection::Match checkArchives;
    bool usedArchive = false;
    bool useAllArchives = false;
    for (const auto &add : workingArguments.back()
                                           .split(QRegExp("[:;,]+"), QString::SkipEmptyParts)) {
        if (add.toLower() != tr("allarchives", "all archives string")) {
            checkArchives.emplace_back(add.toStdString());
            continue;
        }
        checkArchives.clear();
        workingArguments.removeLast();
        useAllArchives = true;
        usedArchive = true;
        break;
    }
    if (!checkArchives.empty()) {
        auto available = access.availableArchives(checkArchives);
        checkArchives.clear();
        std::copy(available.begin(), available.end(), Util::back_emplacer(checkArchives));
        if (!checkArchives.empty()) {
            workingArguments.removeLast();
            usedArchive = true;
        }
    }
    if (checkArchives.empty() && !useAllArchives)
        checkArchives.emplace_back("raw");
    if (workingArguments.isEmpty()) {
        if (usedArchive && usedStation)
            argumentParsingError(
                    tr("No record specification detected.  The only arguments appear to be stations and archives."));
        else if (usedArchive)
            argumentParsingError(
                    tr("No record specification detected.  The only arguments appear to be archives."));
        else
            argumentParsingError(tr("No record specification detected."));
        return -1;
    }

    ArchiveRecordTimeHandler handler(access);
    handler.checkStations = checkStations;
    handler.checkArchives = checkArchives;
    handler.stopHandling = false;
    Time::Bounds bounds;
    try {
        bounds = TimeParse::parseListBounds(workingArguments, &handler, false, true);
    } catch (TimeParsingException tpe) {
        argumentParsingError(tr("Error parsing time: %1\n"
                                "This may be the result of the record argument(s) being invalid and "
                                "being interpreted as a time argument.  Ensure that both the time specification "
                                "and the records listed are valid.  The first argument of the \"time\" was "
                                "\"%2\" and the last one was \"%3\".").arg(tpe.getDescription(),
                                                                           handler.firstInvalidRecord,
                                                                           workingArguments.last()));
        return -1;
    }

    if (handler.selectedRecords.isEmpty()) {
        auto configuration = SequenceSegment::Stream::read(
                Archive::Selection(bounds.getStart(), bounds.getEnd(), checkStations,
                                   {configurationArchive}, {configurationVariable}), &access);
        {
            auto stations = access.availableStations(checkStations);
            checkStations.clear();
            std::move(stations.begin(), stations.end(), Util::back_emplacer(checkStations));
        }

        QSet<QString> records;
        for (auto &seg : configuration) {
            for (const auto &station : checkStations) {
                for (const auto &add : seg.getValue(station, configurationArchive,
                                                    configurationVariable)
                                          .hash("Records")
                                          .toHash()
                                          .keys()) {
                    records.insert(QString::fromStdString(add));
                }
            }
        }
        records.remove(QString());
        if (records.isEmpty()) {
            argumentParsingError(tr("No record configuration defined."));
            return -1;
        }

        for (QSet<QString>::const_iterator record = records.constBegin(), end = records.constEnd();
                record != end;
                ++record) {
            GenerateRecord add;
            add.stations = checkStations;
            add.archives = checkArchives;
            add.record = *record;
            add.configuration = configuration;
            handler.selectedRecords.append(add);
        }
    }

    if (handler.selectedRecords.isEmpty()) {
        argumentParsingError(
                tr("No records available or all record arguments where mistakenly interpreted "
                   "as a time argument.  Please check the command line syntax."));
        return -1;
    }

    bool anyOutput = false;
    std::unordered_set<GeneratedTracker, GeneratedTracker::Hash> alreadySetup;
    for (QList<GenerateRecord>::iterator gen = handler.selectedRecords.begin(),
            endGen = handler.selectedRecords.end(); gen != endGen; ++gen) {
        if (gen->record.isEmpty())
            continue;

        auto stations = access.availableSelected(
                Archive::Selection(FP::undefined(), FP::undefined(), gen->stations,
                                   gen->archives).withDefaultStation(false)).stations;
        for (const auto &station : stations) {
            auto archives = access.availableSelected(
                    Archive::Selection(FP::undefined(), FP::undefined(), {station},
                                       gen->archives).withDefaultStation(false)).archives;
            for (const auto &archive : archives) {
                QSet<QString> records;
                for (auto seg = gen->configuration.begin(); seg != gen->configuration.end();) {
                    if (!Range::intersects(seg->getStart(), seg->getEnd(), bounds.getStart(),
                                           bounds.getEnd())) {
                        seg = gen->configuration.erase(seg);
                        continue;
                    }

                    auto recordData =
                            seg->getValue(station, configurationArchive, configurationVariable)
                               .hash("Records");

                    if (QRegExp::escape(gen->record) == gen->record) {
                        Q_ASSERT(!gen->record.isEmpty());
                        if (recordData.hash(gen->record).hash(archive).exists()) {
                            records |= gen->record;
                        }
                    } else {
                        QRegExp re(gen->record);
                        for (auto check : recordData.toHash()) {
                            if (check.first.empty())
                                continue;
                            if (!re.exactMatch(QString::fromStdString(check.first)))
                                continue;
                            if (!check.second.hash(archive).exists())
                                continue;
                            records.insert(QString::fromStdString(check.first));
                        }
                    }

                    ++seg;
                }
                records.remove(QString());
                if (records.isEmpty())
                    continue;

                for (QSet<QString>::const_iterator record = records.constBegin(),
                        endRecords = records.constEnd(); record != endRecords; ++record) {
                    ValueSegment::Transfer recordConfig;
                    for (auto &seg : gen->configuration) {
                        recordConfig.emplace_back(seg.getStart(), seg.getEnd(), Variant::Root(
                                seg.getValue(station, configurationArchive, configurationVariable)
                                   .hash("Records")
                                   .hash(*record)
                                   .hash(archive)));
                    }
                    if (recordConfig.empty())
                        continue;

                    GeneratedTracker track(station, archive, *record);
                    if (alreadySetup.count(track))
                        continue;
                    alreadySetup.insert(std::move(track));

                    prepareOutput(station, archive, *record, recordConfig);
                    anyOutput = true;
                }
            }
        }
    }

    if (!anyOutput) {
        argumentParsingError(
                tr("No configuration available within the selected time range for the selected "
                   "record(s).  Please ensure that the record(s) exist for the chosen times."));
        return -1;
    }

    this->start = bounds.start;
    this->end = bounds.end;

    mergeMetadata(this->start, this->end, checkStations, checkArchives);
    buildRecordState();
    openOutputDevice();
    startReading(checkStations, checkArchives);

    return 0;
}
