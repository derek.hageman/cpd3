/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QObject>
#include <QtDebug>
#include <QProcess>
#include <QString>
#include <QTemporaryFile>
#include <QSqlDatabase>

#include "datacore/archive/access.hxx"
#include "datacore/stream.hxx"
#include "core/number.hxx"
#include "core/qtcompat.hxx"
#include "core/csv.hxx"
#include "core/timeutils.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class TestComponent : public QObject {
Q_OBJECT

    static QHash<QString, QString> consumeHeaders(QString &data)
    {
        QHash<QString, QString> result;
        for (;;) {
            if (!data.startsWith('!'))
                break;
            int idxNewline = data.indexOf('\n');
            QString line;
            if (idxNewline < 0) {
                line = data;
                data = QString();
            } else {
                line = data.mid(0, idxNewline);
                data = data.mid(idxNewline + 1);
            }
            Q_ASSERT(line.startsWith('!'));
            line = line.mid(1).trimmed();
            QString key(line.section(',', 0, 0).trimmed().toLower());
            QString value(line.section(',', 1).trimmed());
            if (key.isEmpty() || value.isEmpty())
                continue;
            if (result.contains(key)) {
                qWarning() << "Headers contain a duplicate key" << key << ", prior value is"
                           << result.value(key) << "new value is" << value;
            }
            result.insert(key, value);
        }
        return result;
    }

    static bool checkHeader(QHash<QString, QString> &headers,
                            const QString &key,
                            const QString &value = QString())
    {
        QHash<QString, QString>::iterator it = headers.find(key.toLower());
        if (it == headers.end())
            return false;
        if (!value.isEmpty() && it.value() != value)
            return false;
        headers.erase(it);
        return true;
    }

    static bool checkLines(QString &data, double time, QHash<QString, QString> &expectedRecords)
    {
        for (;;) {
            if (expectedRecords.isEmpty())
                return true;

            if (data.isEmpty())
                return false;
            int idxNewline = data.indexOf('\n');
            QString line;
            if (idxNewline < 0) {
                line = data;
                data = QString();
            } else {
                line = data.mid(0, idxNewline);
                data = data.mid(idxNewline + 1);
            }

            QStringList fields(CSV::split(line));
            if (fields.size() < 5)
                return false;
            QString record(fields.takeFirst());
            QString station(fields.takeFirst());
            {
                bool ok = false;
                double t = fields.takeFirst().toDouble(&ok);
                if (t != time)
                    return false;
            }
            if (fields.takeFirst() != Time::toISO8601(time))
                return false;

            QHash<QString, QString>::iterator
                    expected = expectedRecords.find(station + ":" + record);
            if (expected == expectedRecords.end())
                return false;

            if (expected.value() != CSV::join(fields))
                return false;

            expectedRecords.erase(expected);
        }
    }

    QTemporaryFile databaseFile;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        QVERIFY(databaseFile.open());

        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void basic()
    {
        SequenceValue::Transfer toWrite;

        Variant::Root config;

        config["Records/S11a/raw/Headers/AuxHeader"] = "AuxValue";
        config["Records/S11a/raw/Variables/F1_S11/Parameters/Input/Variable"] = "F1_S11";
        config["Records/S11a/raw/Variables/F1_S11/Type"] = "SystemFlags";
        config["Records/S11a/raw/Variables/T_S11/Parameters/Input/Variable"] = "T_S11";
        config["Records/S11a/raw/Variables/U_S11/Parameters/Input/Variable"] = "U_S11";
        config["Records/S11a/raw/Variables/U_S11/Parameters/Calibration/#0"] = 0.5;
        config["Records/S11a/raw/Variables/U_S11/Parameters/Calibration/#1"] = 1.0;

        config["Records/A11a/raw/Variables/BaG_A11/Parameters/Input/Variable"] = "BaG_A11";

        toWrite.push_back(SequenceValue({"_", "configuration", "output_cpd2"}, config));

        SequenceName::Flavors pm1;
        pm1.insert("pm1");
        SequenceName::Flavors pm10;
        pm10.insert("pm10");

        Variant::Root meta;
        meta.write().metadataReal("Description") = "Desc";
        meta.write().metadataReal("Format") = "000.00";
        toWrite.push_back(SequenceValue({"bnd", "raw_meta", "T_S11", pm1}, meta));
        toWrite.push_back(SequenceValue({"bnd", "raw_meta", "U_S11", pm1}, meta));
        toWrite.push_back(SequenceValue({"bnd", "raw_meta", "T_S11", pm10}, meta));
        toWrite.push_back(SequenceValue({"bnd", "raw_meta", "U_S11", pm10}, meta));
        meta.write().metadataReal("Wavelength") = 550.0;
        toWrite.push_back(SequenceValue({"bnd", "raw_meta", "BaG_A11"}, meta));

        meta.write().setEmpty();
        meta.write().metadataFlags("Description") = "Some flags";
        meta.write().metadataSingleFlag("FlagA").hash("Bits").setInt64(0x0001);
        toWrite.push_back(SequenceValue({"bnd", "raw_meta", "F1_S11", pm1}, meta));
        toWrite.push_back(SequenceValue({"bnd", "raw_meta", "F1_S11", pm10}, meta));

        toWrite.push_back(
                SequenceValue({"bnd", "raw", "T_S11", pm10}, Variant::Root(1.0), 10.0, 20.0));
        toWrite.push_back(
                SequenceValue({"bnd", "raw", "U_S11", pm10}, Variant::Root(1.1), 10.0, 20.0));
        toWrite.push_back(
                SequenceValue({"bnd", "raw", "F1_S11", pm10}, Variant::Root(Variant::Flags()), 10.0,
                              20.0));
        toWrite.push_back(SequenceValue({"bnd", "raw", "BaG_A11"}, Variant::Root(1.2), 10.0, 20.0));

        toWrite.push_back(
                SequenceValue({"bnd", "raw", "T_S11", pm10}, Variant::Root(2.0), 20.0, 30.0));
        toWrite.push_back(
                SequenceValue({"bnd", "raw", "U_S11", pm10}, Variant::Root(2.1), 20.0, 30.0));
        toWrite.push_back(SequenceValue({"bnd", "raw", "F1_S11", pm10},
                                        Variant::Root(Variant::Flags{"FlagA"}), 20.0,
                                        30.0));

        toWrite.push_back(SequenceValue({"bnd", "raw", "BaG_A11"}, Variant::Root(3.2), 30.0, 40.0));

        toWrite.push_back(
                SequenceValue({"bnd", "raw", "T_S11", pm1}, Variant::Root(4.0), 40.0, 50.0));
        toWrite.push_back(
                SequenceValue({"bnd", "raw", "U_S11", pm1}, Variant::Root(4.1), 40.0, 50.0));
        toWrite.push_back(
                SequenceValue({"bnd", "raw", "F1_S11", pm1}, Variant::Root(Variant::Flags()), 40.0,
                              50.0));
        toWrite.push_back(SequenceValue({"bnd", "raw", "BaG_A11"}, Variant::Root(4.2), 40.0, 50.0));

        Archive::Access(databaseFile).writeSynchronous(toWrite);

        QString result;
        {
            QProcess p;
            p.start("cpd3_output_cpd2", QStringList() << "bnd" << "S11a,A11a" << "forever");
            QVERIFY(p.waitForFinished(30000));
            QCOMPARE(p.exitStatus(), QProcess::NormalExit);
            QCOMPARE(p.exitCode(), 0);
            result = QString::fromUtf8(p.readAllStandardOutput());
        }
        QVERIFY(!result.isEmpty());

        QHash<QString, QString> headers(consumeHeaders(result));
        QVERIFY(checkHeader(headers, "fil;ProcessedBy;S11a", "da.output.cpd2"));
        QVERIFY(checkHeader(headers, "fil;ProcessedBy;A11a", "da.output.cpd2"));
        QVERIFY(checkHeader(headers, "fil;cpd3;revision"));
        QVERIFY(checkHeader(headers, "fil;cpd3;environment"));
        QVERIFY(checkHeader(headers, "var;DateTime;FieldDesc",
                            "Date String (YYYY-MM-DDThh:mm:ssZ)"));
        QVERIFY(checkHeader(headers, "var;EPOCH;FieldDesc", "Epoch time: seconds from Jan 1 1970"));
        QVERIFY(checkHeader(headers, "var;STN;FieldDesc", "Station ID code"));

        QVERIFY(checkHeader(headers, "AuxHeader", "AuxValue"));

        QVERIFY(checkHeader(headers, "var;T_S11;FieldDesc", "Desc"));
        QVERIFY(checkHeader(headers, "var;U_S11;FieldDesc", "Desc"));
        QVERIFY(checkHeader(headers, "var;BaG_A11;FieldDesc", "Desc"));
        QVERIFY(checkHeader(headers, "var;BaG_A11;Wavelength;0", "550"));
        QVERIFY(checkHeader(headers, "var;F1_S11;FieldDesc", "Some flags"));

        QVERIFY(checkHeader(headers, "row;colhdr;A11a", "A11a;STN;EPOCH;DateTime;BaG_A11"));
        QVERIFY(checkHeader(headers, "row;colhdr;S11a",
                            "S11a;STN;EPOCH;DateTime;F1_S11;T_S11;U_S11"));

        QVERIFY(checkHeader(headers, "row;mvc;A11a", "A11a;ZZZ;0;9999-99-99T99:99:99Z;999.99"));
        QVERIFY(checkHeader(headers, "row;mvc;S11a",
                            "S11a;ZZZ;0;9999-99-99T99:99:99Z;FFFF;999.99;999.99"));

        QVERIFY(checkHeader(headers, "row;varfmt;A11a",
                            "A11a;%s;%u;%04d-%02d-%02dT%02d:%02d:%02dZ;*@03.2f"));
        QVERIFY(checkHeader(headers, "row;varfmt;S11a",
                            "S11a;%s;%u;%04d-%02d-%02dT%02d:%02d:%02dZ;%04X;*@03.2f;*@03.2f"));

        QCOMPARE(headers.size(), 0);

        QHash<QString, QString> lines;
        lines["BND:S11a"] = "0000,001.00,001.60";
        lines["BND:A11a"] = "001.20";
        QVERIFY(checkLines(result, 10.0, lines));

        lines.clear();
        lines["BND:S11a"] = "0001,002.00,002.60";
        QVERIFY(checkLines(result, 20.0, lines));

        lines.clear();
        lines["BND:A11a"] = "003.20";
        QVERIFY(checkLines(result, 30.0, lines));

        lines.clear();
        lines["BND:S11a"] = "0010,004.00,004.60";
        lines["BND:A11a"] = "004.20";
        QVERIFY(checkLines(result, 40.0, lines));

        QVERIFY(result.trimmed().isEmpty());
    }

    void timeDependence()
    {
        SequenceValue::Transfer toWrite;

        Variant::Root config;

        config["Records/A11a/raw/Variables/BaG_A11/Parameters/Input/Variable"] = "BaG_A11";
        toWrite.push_back(
                SequenceValue({"_", "configuration", "output_cpd2"}, config, 40, FP::undefined()));

        config["Records/A11a/raw/Variables/BaG_A11/Parameters/Wavelength"] = 530.0;
        config["Records/A11a/raw/Variables/BaG_A11/Parameters/Calibration/#0"] = 0.1;
        config["Records/A11a/raw/Variables/BaG_A11/Parameters/Calibration/#1"] = 1.0;
        toWrite.push_back(
                SequenceValue({"_", "configuration", "output_cpd2"}, config, FP::undefined(), 40));

        Variant::Root meta;
        meta.write().metadataReal("Format") = "0.00";
        toWrite.push_back(SequenceValue({"bnd", "raw_meta", "BaG_A11"}, meta, FP::undefined(), 30));

        meta.write().metadataReal("Wavelength") = 550.0;
        toWrite.push_back(SequenceValue({"bnd", "raw_meta", "BaG_A11"}, meta, 30, FP::undefined()));

        toWrite.push_back(SequenceValue({"bnd", "raw", "BaG_A11"}, Variant::Root(1.0), 10.0, 20.0));
        toWrite.push_back(SequenceValue({"bnd", "raw", "BaG_A11"}, Variant::Root(2.0), 20.0, 30.0));
        toWrite.push_back(SequenceValue({"bnd", "raw", "BaG_A11"}, Variant::Root(3.0), 30.0, 40.0));
        toWrite.push_back(SequenceValue({"bnd", "raw", "BaG_A11"}, Variant::Root(4.0), 40.0, 50.0));

        Archive::Access(databaseFile).writeSynchronous(toWrite);

        QString result;
        {
            QProcess p;
            p.start("cpd3_output_cpd2", QStringList() << "BND::A11a" << "forever");
            QVERIFY(p.waitForFinished(30000));
            QCOMPARE(p.exitStatus(), QProcess::NormalExit);
            QCOMPARE(p.exitCode(), 0);
            result = QString::fromUtf8(p.readAllStandardOutput());
        }
        QVERIFY(!result.isEmpty());

        QHash<QString, QString> headers(consumeHeaders(result));
        QVERIFY(checkHeader(headers, "fil;ProcessedBy;A11a", "da.output.cpd2"));
        QVERIFY(checkHeader(headers, "fil;cpd3;revision"));
        QVERIFY(checkHeader(headers, "fil;cpd3;environment"));
        QVERIFY(checkHeader(headers, "var;DateTime;FieldDesc",
                            "Date String (YYYY-MM-DDThh:mm:ssZ)"));
        QVERIFY(checkHeader(headers, "var;EPOCH;FieldDesc", "Epoch time: seconds from Jan 1 1970"));
        QVERIFY(checkHeader(headers, "var;STN;FieldDesc", "Station ID code"));

        QVERIFY(checkHeader(headers, "var;BaG_A11;Wavelength;0", "530"));
        QVERIFY(checkHeader(headers, "var;BaG_A11;Wavelength;1970-01-01T00:00:40Z", "550"));

        QVERIFY(checkHeader(headers, "row;colhdr;A11a", "A11a;STN;EPOCH;DateTime;BaG_A11"));

        QVERIFY(checkHeader(headers, "row;mvc;A11a", "A11a;ZZZ;0;9999-99-99T99:99:99Z;9.99"));

        QVERIFY(checkHeader(headers, "row;varfmt;A11a",
                            "A11a;%s;%u;%04d-%02d-%02dT%02d:%02d:%02dZ;*@01.2f"));

        QHash<QString, QString> lines;
        lines["BND:A11a"] = "1.10";
        QVERIFY(checkLines(result, 10.0, lines));

        lines.clear();
        lines["BND:A11a"] = "2.10";
        QVERIFY(checkLines(result, 20.0, lines));

        lines.clear();
        lines["BND:A11a"] = "3.10";
        QVERIFY(checkLines(result, 30.0, lines));

        lines.clear();
        lines["BND:A11a"] = "4.00";
        QVERIFY(checkLines(result, 40.0, lines));

        QVERIFY(result.trimmed().isEmpty());
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
