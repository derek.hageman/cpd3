/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QStringList>
#include <QLoggingCategory>

#include "core/waitutils.hxx"
#include "core/range.hxx"
#include "core/csv.hxx"
#include "core/threadpool.hxx"

#include "output_cpd2.hxx"

Q_DECLARE_LOGGING_CATEGORY(log_outputcpd3)

using namespace CPD3;
using namespace CPD3::Data;

void OutputCPD2::startReading(const Archive::Selection::Match &defaultStations,
                              const Archive::Selection::Match &defaultArchives)
{
    Archive::Selection::List selections;
    for (QHash<QString, RecordDefinition>::const_iterator record = recordContents.constBegin(),
            endRecord = recordContents.constEnd(); record != endRecord; ++record) {
        for (QHash<QString, VariableDefinition>::const_iterator
                variable = record.value().variables.constBegin(),
                endVariable = record.value().variables.constEnd();
                variable != endVariable;
                ++variable) {
            for (const auto &seg : variable.value().timeDependence) {
                Util::append(seg.inputs.toArchiveSelections(defaultStations, defaultArchives),
                             selections);
            }
        }
    }

    if (selections.empty()) {
        readState = Read_OutputCompleted;
        qCDebug(log_outputcpd3) << "No data selections to read so no output to generate";
        return;
    }

    for (auto &mod : selections) {
        mod.start = start;
        mod.end = end;
        mod.includeMetaArchive = false;
    }

    qCDebug(log_outputcpd3) << "Starting archive read with" << selections.size()
                            << " data selections";

    readState = Read_FirstValue;
    access.readStream(selections, &processingEgress)->detach();
}

void OutputCPD2::incomingReadData(const SequenceValue::Transfer &values)
{
    if (values.empty())
        return;
    {
        std::unique_lock<std::mutex> lock(readMutex);
        readStall(lock);
        Q_ASSERT(readState == Read_FirstValue || readState == Read_InProgress);
        Util::append(values, readValues);
    }
    scheduleProcessIfNeeded();
}

void OutputCPD2::incomingReadData(SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;
    {
        std::unique_lock<std::mutex> lock(readMutex);
        readStall(lock);
        Q_ASSERT(readState == Read_FirstValue || readState == Read_InProgress);
        Util::append(std::move(values), readValues);
    }
    scheduleProcessIfNeeded();
}

void OutputCPD2::incomingReadData(const SequenceValue &value)
{
    {
        std::unique_lock<std::mutex> lock(readMutex);
        readStall(lock);
        Q_ASSERT(readState == Read_FirstValue || readState == Read_InProgress);
        readValues.emplace_back(value);
    }
    scheduleProcessIfNeeded();
}

void OutputCPD2::incomingReadData(SequenceValue &&value)
{
    {
        std::unique_lock<std::mutex> lock(readMutex);
        readStall(lock);
        Q_ASSERT(readState == Read_FirstValue || readState == Read_InProgress);
        readValues.emplace_back(std::move(value));
    }
    scheduleProcessIfNeeded();
}

void OutputCPD2::incomingReadEnd()
{
    {
        std::lock_guard<std::mutex> lock(readMutex);
        Q_ASSERT(readState != Read_OutputCompleted);
        switch (readState) {
        case Read_NotStarted:
            Q_ASSERT(false);
            break;
        case Read_FirstValue:
            readState = Read_DataEnded_OnFirst;
            break;
        case Read_InProgress:
            readState = Read_DataEnded;
            break;
        case Read_DataEnded:
        case Read_DataEnded_OnFirst:
        case Read_OutputCompleted:
            Q_ASSERT(false);
            break;
        }
    }
    scheduleProcessIfNeeded();
}

void OutputCPD2::readStall(std::unique_lock<std::mutex> &lock)
{
    while (readValues.size() > StreamSink::stallThreshold) {
        if (readTerminated)
            return;
        readCond.wait(lock);
    }
}

OutputCPD2::ProcessingEgress::ProcessingEgress(OutputCPD2 *p) : parent(p)
{ }

OutputCPD2::ProcessingEgress::~ProcessingEgress()
{ }

void OutputCPD2::ProcessingEgress::incomingData(const SequenceValue::Transfer &values)
{ parent->incomingReadData(values); }

void OutputCPD2::ProcessingEgress::incomingData(SequenceValue::Transfer &&values)
{ parent->incomingReadData(std::move(values)); }

void OutputCPD2::ProcessingEgress::incomingData(const SequenceValue &value)
{ parent->incomingReadData(value); }

void OutputCPD2::ProcessingEgress::incomingData(SequenceValue &&value)
{ parent->incomingReadData(std::move(value)); }

void OutputCPD2::ProcessingEgress::endData()
{ parent->incomingReadEnd(); }

OutputCPD2::VariableFormatter::VariableFormatter()
{ }

OutputCPD2::VariableFormatter::~VariableFormatter()
{ }

OutputCPD2::DispatchHandler::DispatchHandler() : validStart(FP::undefined()),
                                                 validEnd(FP::undefined())
{ }

OutputCPD2::DispatchHandler::~DispatchHandler()
{ }

bool OutputCPD2::DispatchHandler::appliesTo(double start, double end) const
{ return Range::intersects(validStart, validEnd, start, end); }

OutputCPD2::DispatchFanout::DispatchFanout(const QList<DispatchHandler *> &hand)
{
    Q_ASSERT(!hand.isEmpty());
    validStart = hand.first()->validStart;
    validEnd = hand.first()->validEnd;
    for (QList<DispatchHandler *>::const_iterator h = hand.constBegin(), end = hand.constEnd();
            h != end;
            ++h) {
        handlers.push_back(*h);
        if (Range::compareStart((*h)->validStart, validStart) < 0)
            validStart = (*h)->validStart;
        if (Range::compareEnd((*h)->validEnd, validEnd) > 0)
            validEnd = (*h)->validEnd;
    }
}

OutputCPD2::DispatchFanout::~DispatchFanout()
{ }

void OutputCPD2::DispatchFanout::incoming(const SequenceValue &value)
{
    for (std::vector<DispatchHandler *>::const_iterator h = handlers.begin(), endH = handlers.end();
            h != endH;
            ++h) {
        if (!(*h)->appliesTo(value.getStart(), value.getEnd()))
            continue;
        (*h)->incoming(value);
    }
}

OutputCPD2::DispatchDiscard::DispatchDiscard()
{ }

OutputCPD2::DispatchDiscard::~DispatchDiscard()
{ }

void OutputCPD2::DispatchDiscard::incoming(const SequenceValue &value)
{ Q_UNUSED(value); }

OutputCPD2::DispatchPath::DispatchPath(DispatchHandler *t, const QString &p) : target(t), path(p)
{ }

OutputCPD2::DispatchPath::~DispatchPath()
{ }

void OutputCPD2::DispatchPath::incoming(const SequenceValue &value)
{
    target->incoming(SequenceValue(value.getUnit(), Variant::Root(value.read().getPath(path)),
                                   value.getStart(),
                                   value.getEnd(), value.getPriority()));
}

void OutputCPD2::VariableState::fromSequenceValue(const SequenceValue &value,
                                                  VariableFormatter *formatter)
{
    validUntil = value.getEnd();
    latestValue = value;
    latestFormatter = formatter;
    if (!FP::defined(record->start))
        record->start = value.getStart();
}

OutputCPD2::DispatchSimple::DispatchSimple(VariableState *t) : target(t)
{ }

OutputCPD2::DispatchSimple::~DispatchSimple()
{ }

void OutputCPD2::DispatchSimple::incoming(const SequenceValue &value)
{
    if (!value.read().exists())
        return;
    target->fromSequenceValue(value, this);
}


OutputCPD2::DispatchDecimal::DispatchDecimal(VariableState *target,
                                             const QString &f,
                                             const CPD3::Calibration &c) : DispatchSimple(target),
                                                                           formatter(f),
                                                                           calibration(c)
{ }

OutputCPD2::DispatchDecimal::~DispatchDecimal()
{ }

void OutputCPD2::DispatchDecimal::incoming(const SequenceValue &value)
{
    if (value.read().getType() != Variant::Type::Real)
        return;
    DispatchSimple::incoming(value);
}

QString OutputCPD2::DispatchDecimal::format(const SequenceValue &value) const
{ return formatter.apply(calibration.apply(value.read().toDouble())); }

OutputCPD2::DispatchInteger::DispatchInteger(VariableState *target, const QString &f)
        : DispatchSimple(target), formatter(f)
{ }

OutputCPD2::DispatchInteger::~DispatchInteger()
{ }

void OutputCPD2::DispatchInteger::incoming(const SequenceValue &value)
{
    if (value.read().getType() != Variant::Type::Integer)
        return;
    DispatchSimple::incoming(value);
}

QString OutputCPD2::DispatchInteger::format(const SequenceValue &value) const
{ return formatter.apply(value.read().toInt64()); }


OutputCPD2::DispatchString::DispatchString(VariableState *target) : DispatchSimple(target)
{ }

OutputCPD2::DispatchString::~DispatchString()
{ }

void OutputCPD2::DispatchString::incoming(const SequenceValue &value)
{
    if (value.read().getType() != Variant::Type::String)
        return;
    DispatchSimple::incoming(value);
}

QString OutputCPD2::DispatchString::format(const SequenceValue &value) const
{ return value.read().toQString(); }


OutputCPD2::DispatchFlags::DispatchFlags(VariableState *target,
                                         const QString &f,
                                         const std::unordered_map<Variant::Flag, qint64> &b)
        : DispatchSimple(target),
          formatter(f),
          bits(b)
{ }

OutputCPD2::DispatchFlags::~DispatchFlags()
{ }

QString OutputCPD2::DispatchFlags::format(const SequenceValue &value) const
{
    qint64 b = additionalBits(value);
    for (const auto &f : value.read().toFlags()) {
        auto check = bits.find(f);
        if (check == bits.end())
            continue;
        b |= check->second;
    }
    return formatter.apply(b);
}

qint64 OutputCPD2::DispatchFlags::additionalBits(const SequenceValue &value) const
{
    Q_UNUSED(value);
    return 0;
}

OutputCPD2::DispatchSystemFlags::DispatchSystemFlags(VariableState *target,
                                                     const QString &format,
                                                     const std::unordered_map<Variant::Flag,
                                                                              qint64> &bits)
        : DispatchFlags(target, format, bits)
{ }

OutputCPD2::DispatchSystemFlags::~DispatchSystemFlags()
{ }

qint64 OutputCPD2::DispatchSystemFlags::additionalBits(const SequenceValue &value) const
{
    if (value.getUnit().hasFlavor(SequenceName::flavor_pm10))
        return 0;
    if (value.getUnit().hasFlavor(SequenceName::flavor_pm1))
        return Q_INT64_C(0x0010);
    if (value.getUnit().hasFlavor(SequenceName::flavor_pm25))
        return Q_INT64_C(0x0010);
    return 0;
}


bool OutputCPD2::canExtend(QHash<QString, VariableDefinition>::const_iterator variable,
                           QList<VariableTimeDependence>::const_iterator prior,
                           QList<VariableTimeDependence>::const_iterator current)
{
    if (prior->lookupPath != current->lookupPath)
        return false;
    switch (variable.value().mode) {
    case VariableDefinition::Mode_Decimal:
        if (prior->calibration != current->calibration)
            return false;
        break;
    case VariableDefinition::Mode_Integer:
    case VariableDefinition::Mode_String:
        break;
    case VariableDefinition::Mode_Flags:
    case VariableDefinition::Mode_SystemFlags:
        if (prior->flagsBits != current->flagsBits)
            return false;
        break;
    }
    return true;
}

OutputCPD2::DispatchHandler *OutputCPD2::buildDispatch(const SequenceName &unit)
{
    QList<DispatchHandler *> result;
    for (QHash<QString, RecordState *>::const_iterator record = recordState.constBegin(),
            endRecord = recordState.constEnd(); record != endRecord; ++record) {
        QHash<QString, RecordDefinition>::const_iterator
                rDef = recordContents.constFind(record.key());
        Q_ASSERT(rDef != recordContents.constEnd());
        for (QList<VariableState *>::const_iterator
                variable = record.value()->variables.constBegin(),
                endVariable = record.value()->variables.constEnd();
                variable != endVariable;
                ++variable) {
            QHash<QString, VariableDefinition>::const_iterator
                    vDef = rDef.value().variables.constFind((*variable)->name);
            Q_ASSERT(vDef != rDef.value().variables.constEnd());

            DispatchHandler *priorHandler = NULL;
            QList<VariableTimeDependence>::const_iterator priorVTD;

            for (QList<VariableTimeDependence>::const_iterator
                    vtd = vDef.value().timeDependence.constBegin(),
                    endVtd = vDef.value().timeDependence.constEnd(); vtd != endVtd; ++vtd) {
                if (!vtd->inputs.matches(unit))
                    continue;
                if (unit.isMeta())
                    continue;

                if (priorHandler != NULL &&
                        Range::compareStartEnd(vtd->getStart(), priorHandler->validEnd) == 0) {
                    if (canExtend(vDef, priorVTD, vtd)) {
                        priorHandler->validEnd = vtd->getEnd();
                        priorVTD = vtd;
                        continue;
                    }
                }

                priorVTD = vtd;
                DispatchHandler *handler = NULL;

                switch (vDef.value().mode) {
                case VariableDefinition::Mode_Decimal:
                    handler = new DispatchDecimal(*variable, vDef.value().format, vtd->calibration);
                    break;
                case VariableDefinition::Mode_Integer:
                    handler = new DispatchInteger(*variable, vDef.value().format);
                    break;
                case VariableDefinition::Mode_String:
                    handler = new DispatchString(*variable);
                    break;
                case VariableDefinition::Mode_Flags:
                    handler = new DispatchFlags(*variable, vDef.value().format, vtd->flagsBits);
                    break;
                case VariableDefinition::Mode_SystemFlags:
                    handler =
                            new DispatchSystemFlags(*variable, vDef.value().format, vtd->flagsBits);
                    break;
                }
                Q_ASSERT(handler);
                uniqueTargets.append(handler);

                if (!vtd->lookupPath.isEmpty()) {
                    handler = new DispatchPath(handler, vtd->lookupPath);
                    Q_ASSERT(handler);
                    uniqueTargets.append(handler);
                }

                handler->validStart = vtd->getStart();
                handler->validEnd = vtd->getEnd();
                priorHandler = handler;
                result.append(handler);
            }
        }
    }

    if (result.isEmpty())
        return &discardTarget;
    if (result.size() == 1)
        return result.first();
    DispatchFanout *fanout = new DispatchFanout(result);
    Q_ASSERT(fanout);
    uniqueTargets.append(fanout);
    return fanout;
}

void OutputCPD2::dispatchValue(const SequenceValue &value)
{
    QHash<SequenceName, DispatchHandler *>::const_iterator
            target = targets.constFind(value.getUnit());
    if (target == targets.constEnd()) {
        target = targets.insert(value.getUnit(), buildDispatch(value.getUnit()));
    }
    if (!target.value()->appliesTo(value.getStart(), value.getEnd()))
        return;
    target.value()->incoming(value);
}

void OutputCPD2::flushPendingRecords()
{
    for (QHash<QString, RecordState *>::const_iterator record = recordState.constBegin(),
            endRecord = recordState.constEnd(); record != endRecord; ++record) {
        if (!FP::defined(record.value()->start))
            continue;

        QStringList fields;
        fields.append(record.key());
        fields.append(QString());
        fields.append(QString::number(qRound(record.value()->start)));
        fields.append(Time::toISO8601(record.value()->start));

        SequenceName::Component station;
        for (QList<VariableState *>::const_iterator
                variable = record.value()->variables.constBegin(),
                endVariable = record.value()->variables.constEnd();
                variable != endVariable;
                ++variable) {
            if ((*variable)->latestFormatter != NULL) {
                fields.append((*variable)->latestFormatter->format((*variable)->latestValue));
                if (station.empty())
                    station = (*variable)->latestValue.getStation();
            } else {
                fields.append((*variable)->mvc);
            }
            if (Range::compareStartEnd(lastReadStart, (*variable)->validUntil) >= 0) {
                (*variable)->validUntil = FP::undefined();
                (*variable)->latestFormatter = NULL;
            }
        }
        if (station.empty()) {
            fields[1] = "NIL";
        } else {
            fields[1] = QString::fromStdString(station).toUpper();
        }

        QString str(CSV::join(fields));
        str.append('\n');
        writeOutputData(str.toUtf8());

        record.value()->start = FP::undefined();
    }
}

bool OutputCPD2::processPending()
{
    SequenceValue::Transfer values;
    bool isFirstData = false;
    bool outputEnd = false;
    {
        std::lock_guard<std::mutex> lock(readMutex);
        switch (readState) {
        case Read_NotStarted:
            return true;
        case Read_FirstValue:
            if (readValues.empty())
                return true;
            isFirstData = true;
            readState = Read_InProgress;
            /* Fall through */
        case Read_InProgress:
            break;
        case Read_DataEnded_OnFirst:
            isFirstData = true;
            /* Fall through */
        case Read_DataEnded:
            outputEnd = true;
            break;
        case Read_OutputCompleted:
            return false;
        }
        values = std::move(readValues);
        readValues.clear();
    }

    if (values.size() > StreamSink::stallThreshold)
        readCond.notify_all();

    if (isFirstData && !values.empty()) {
        writeOutputData(buildHeaders());
    }

    for (const auto &v : values) {
        if (!FP::equal(lastReadStart, v.getStart())) {
            flushPendingRecords();
            lastReadStart = v.getStart();
        }
        dispatchValue(v);
    }

    if (!outputEnd)
        return true;

    flushPendingRecords();

    std::lock_guard<std::mutex> lock(readMutex);
    readState = Read_OutputCompleted;
    return false;
}
