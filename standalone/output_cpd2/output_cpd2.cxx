/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#ifdef Q_OS_UNIX

#include <unistd.h>

#endif

#include <QCoreApplication>
#include <QTranslator>
#include <QTimer>
#include <QLocale>
#include <QLibraryInfo>
#include <QLoggingCategory>

#include "core/abort.hxx"
#include "core/waitutils.hxx"
#include "core/threadpool.hxx"
#include "clicore/terminaloutput.hxx"

#include "output_cpd2.hxx"


Q_LOGGING_CATEGORY(log_outputcpd3, "cpd3.outputcpd2", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::CLI;

OutputCPD2::OutputCPD2() : processingEgress(this), access(),
                           start(FP::undefined()),
                           end(FP::undefined()),
#ifdef Q_OS_UNIX
                           stdoutOpen(false),
#endif
                           outputDevice(NULL),
                           pendingOutput(),
                           readMutex(), readCond(), readTerminated(false),
                           lastReadStart(FP::undefined()),
                           readValues(),
                           readState(Read_NotStarted),
                           processCounter(0)
{
    connect(this, SIGNAL(deferredProcess()), this, SLOT(process()), Qt::QueuedConnection);
}

OutputCPD2::~OutputCPD2()
{
    access.signalTerminate();
    access.waitForLocks();

    if (outputDevice != NULL)
        delete outputDevice;
#ifdef Q_OS_UNIX
    if (stdoutOpen)
        ::close(1);
#endif

    qDeleteAll(uniqueTargets);
    for (QHash<QString, RecordState *>::const_iterator rState = recordState.constBegin(),
            end = recordState.constEnd(); rState != end; ++rState) {
        qDeleteAll(rState.value()->variables);
        delete rState.value();
    }
}

void OutputCPD2::openOutputDevice()
{
    if (outputDevice != NULL)
        return;

#ifdef Q_OS_UNIX
    QFile *file = new QFile;
    Q_ASSERT(file);
    if (!file->open(1, QIODevice::WriteOnly | QIODevice::Unbuffered)) {
        TerminalOutput::simpleWordWrap(file->errorString(), true);
        QCoreApplication::exit(1);
        return;
    }
    stdoutOpen = true;
    outputDevice = file;
#endif

    if (outputDevice == NULL)
        return;

    connect(outputDevice, SIGNAL(bytesWritten(qint64)), this, SLOT(process()),
            Qt::QueuedConnection);
}

void OutputCPD2::writeOutputData(const QByteArray &data)
{
    if (outputDevice == NULL)
        return;
    if (data.isEmpty())
        return;

    if (pendingOutput.isEmpty())
        pendingOutput = data;
    else
        pendingOutput.append(data);
    QMetaObject::invokeMethod(this, "process", Qt::QueuedConnection);
}

bool OutputCPD2::handleOutput()
{
    if (outputDevice == NULL)
        return false;
    if (pendingOutput.isEmpty())
        return false;

    QByteArray toWrite;
    qSwap(toWrite, pendingOutput);
    qint64 n = outputDevice->write(toWrite);
    if (n == -1) {
        qCDebug(log_outputcpd3) << "Output device encountered error:" <<
                                outputDevice->errorString();
        outputDevice->close();
        outputDevice->deleteLater();
        outputDevice = NULL;
        return false;
    }
    if (n == 0)
        return true;

    if ((int) n == pendingOutput.size())
        return false;

    int remaining = toWrite.size() - (int) n;
    int original = pendingOutput.size();
    pendingOutput.resize(original + remaining);
    memcpy(pendingOutput.data() + original, toWrite.constData() + n, remaining);

    return true;
}

void OutputCPD2::scheduleProcessIfNeeded()
{
    if (processCounter.fetchAndStoreAcquire(1) != 0)
        return;

    emit deferredProcess();
}

void OutputCPD2::process()
{
    processCounter.fetchAndStoreOrdered(0);

    if (processPending()) {
        handleOutput();
        return;
    }

    if (handleOutput())
        return;

    if (outputDevice != NULL) {
        if (!pendingOutput.isEmpty())
            return;
        if (outputDevice->bytesToWrite() > 0) {
            /* File devices do not emit the bytesWritten signal, so we'll never check
             * again */
            if (QFileDevice *fileDevice = qobject_cast<QFileDevice *>(outputDevice)) {
                fileDevice->flush();
                QMetaObject::invokeMethod(this, "process", Qt::QueuedConnection);
            }
            return;
        }
        outputDevice->close();
        outputDevice->deleteLater();
        outputDevice = NULL;
    }

#ifdef Q_OS_UNIX
    if (stdoutOpen) {
        ::close(1);
        stdoutOpen = false;
    }
#endif

    access.waitForLocks();
    QCoreApplication::exit(0);
}


void OutputCPD2::shutdown()
{
    {
        std::lock_guard<std::mutex> lock(readMutex);
        readTerminated = true;
    }
    readCond.notify_all();

    access.signalTerminate();
    access.waitForLocks();

    for (; ;) {
        bool ended = false;
        {
            std::lock_guard<std::mutex> lock(readMutex);
            switch (readState) {
            case Read_FirstValue:
            case Read_InProgress:
                break;
            case Read_NotStarted:
            case Read_DataEnded_OnFirst:
            case Read_DataEnded:
            case Read_OutputCompleted:
                ended = true;
                break;
            }
            readValues.clear();
        }
        if (ended)
            break;
        QThread::yieldCurrentThread();
    }

    if (outputDevice != NULL) {
        outputDevice->close();
        outputDevice->deleteLater();
        outputDevice = NULL;
    }

#ifdef Q_OS_UNIX
    if (stdoutOpen) {
        ::close(1);
        stdoutOpen = false;
    }
#endif

    QCoreApplication::exit(1);
}


int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

    QString translateLocation;
    QByteArray cpd3(qgetenv("CPD3"));
    if (cpd3.length() > 0) {
        translateLocation = QString(cpd3) + "/locale";
    }
    QTranslator cpd3Translator;
    cpd3Translator.load("cpd3_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cpd3Translator);
    QTranslator cliTranslator;
    cliTranslator.load("cli_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cliTranslator);
    QTranslator output_cpd2Translator;
    output_cpd2Translator.load("output_cpd2_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&output_cpd2Translator);

    Abort::installAbortHandler(true);

    OutputCPD2 *output = new OutputCPD2;
    AbortPoller abortPoller;
    QObject::connect(&app, SIGNAL(aboutToQuit()), output, SLOT(shutdown()));
    QObject::connect(&abortPoller, SIGNAL(aborted()), &app, SLOT(quit()));

    if (int rc = output->parseArguments(app.arguments())) {
        delete output;
        ThreadPool::system()->wait();
        if (rc == -1)
            return 1;
        return 0;
    }
    abortPoller.start();
    QMetaObject::invokeMethod(output, "process", Qt::QueuedConnection);
    int rc = app.exec();
    abortPoller.disconnect();
    delete output;
    ThreadPool::system()->wait();
    return rc;
}
