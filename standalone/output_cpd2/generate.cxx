/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLoggingCategory>

#include "core/range.hxx"
#include "core/environment.hxx"
#include "datacore/externalsink.hxx"
#include "datacore/variant/composite.hxx"

#include "output_cpd2.hxx"

Q_DECLARE_LOGGING_CATEGORY(log_outputcpd3)

using namespace CPD3;
using namespace CPD3::Data;

OutputCPD2::VariableTimeDependence::VariableTimeDependence() : start(FP::undefined()),
                                                               end(FP::undefined()),
                                                               wavelength(FP::undefined()),
                                                               inputs(),
                                                               calibration(),
                                                               lookupPath(),
                                                               flagsBits(),
                                                               flagsAccept(),
                                                               flagBitShift(0),
                                                               metadataSuppliedWavelength(false),
                                                               metadataSuppliedFlags()
{ }

OutputCPD2::VariableDefinition::VariableDefinition() : mode(Mode_Decimal), sortPriority(0)
{
    timeDependence.append(VariableTimeDependence());
}

static void setIfValid(QString &target, const Variant::Read &source)
{
    if (source.getType() != Variant::Type::String)
        return;
    QString value(source.toDisplayString());
    if (value.isEmpty())
        return;
    target = value;
}

OutputCPD2::VariableTimeDependence::VariableTimeDependence(const VariableTimeDependence &other,
                                                           double s,
                                                           double e) : start(s),
                                                                       end(e),
                                                                       wavelength(other.wavelength),
                                                                       inputs(other.inputs),
                                                                       calibration(
                                                                               other.calibration),
                                                                       lookupPath(other.lookupPath),
                                                                       flagsBits(other.flagsBits),
                                                                       flagsAccept(
                                                                               other.flagsAccept),
                                                                       flagBitShift(
                                                                               other.flagBitShift),
                                                                       metadataSuppliedWavelength(
                                                                               other.metadataSuppliedWavelength),
                                                                       metadataSuppliedFlags(
                                                                               other.metadataSuppliedFlags)
{ }

OutputCPD2::VariableTimeDependence::VariableTimeDependence(const MergeConfig &other,
                                                           double s,
                                                           double e) : start(s),
                                                                       end(e),
                                                                       wavelength(FP::undefined()),
                                                                       inputs(),
                                                                       calibration(),
                                                                       lookupPath(),
                                                                       flagsBits(),
                                                                       flagsAccept(),
                                                                       flagBitShift(0),
                                                                       metadataSuppliedWavelength(
                                                                               false),
                                                                       metadataSuppliedFlags()
{
    configure(other.config, other.station, other.archive, other.variable);
}

OutputCPD2::VariableTimeDependence::VariableTimeDependence(const VariableTimeDependence &under,
                                                           const MergeConfig &over,
                                                           double s,
                                                           double e) : start(s),
                                                                       end(e),
                                                                       wavelength(under.wavelength),
                                                                       inputs(under.inputs),
                                                                       calibration(
                                                                               under.calibration),
                                                                       lookupPath(under.lookupPath),
                                                                       flagsBits(under.flagsBits),
                                                                       flagsAccept(
                                                                               under.flagsAccept),
                                                                       flagBitShift(
                                                                               under.flagBitShift),
                                                                       metadataSuppliedWavelength(
                                                                               under.metadataSuppliedWavelength),
                                                                       metadataSuppliedFlags(
                                                                               under.metadataSuppliedFlags)
{
    configure(over.config, over.station, over.archive, over.variable);
}

void OutputCPD2::VariableTimeDependence::configure(const Variant::Read &config,
                                                   const SequenceName::Component &station,
                                                   const SequenceName::Component &archive,
                                                   const SequenceName::Component &variable)
{
    if (FP::defined(config["Wavelength"].toDouble()))
        wavelength = config["Wavelength"].toDouble();

    /* Because the only way we'll overlap is from multiple stations so we 
     * just add them */
    inputs.append(SequenceMatch::Composite(config["Input"], SequenceMatch::Element::PatternList{
            QString::fromStdString(station)}, SequenceMatch::Element::PatternList{
            QString::fromStdString(archive)}, SequenceMatch::Element::PatternList{
            QString::fromStdString(variable)}));

    if (config["Calibration"].exists())
        calibration = Variant::Composite::toCalibration(config["Calibration"]);
    if (config["Path"].exists())
        lookupPath = config["Path"].toQString();

    if (config["Flags"].exists()) {
        for (auto add : config["Flags"].toHash()) {
            qint64 f = add.second.toInt64();
            if (!INTEGER::defined(f))
                f = 0;
            auto check = flagsBits.find(add.first);
            if (check == flagsBits.end())
                check = flagsBits.emplace(add.first, 0).first;
            check->second |= f;
        }
    }

    if (config["FlagsAccept"].exists())
        flagsAccept = config["FlagsAccept"].toFlags();

    if (INTEGER::defined(config["FlagsBitShift"].toInt64()))
        flagBitShift = config["FlagsBitShift"].toInt64();
}

void OutputCPD2::prepareOutput(const SequenceName::Component &station,
                               const SequenceName::Component &archive,
                               const QString &record,
                               const ValueSegment::Transfer &config)
{
    if (config.empty())
        return;

    qCDebug(log_outputcpd3) << "Merging configuration for" << station << ':' << archive << ':'
                            << record;

    QHash<QString, RecordDefinition>::iterator target = recordContents.find(record);
    if (target == recordContents.end())
        target = recordContents.insert(record, RecordDefinition());

    for (const auto &seg : config) {
        for (auto add : seg.getValue()["Headers"].toHash()) {
            if (add.first.empty())
                continue;
            QString v(add.second.toQString());
            if (v.isEmpty())
                continue;
            target.value().globalHeaders.insert(QString::fromStdString(add.first), v);
        }

        for (auto add : seg.getValue()["Variables"].toHash()) {
            if (add.first.empty())
                continue;
            auto data = target.value().variables.find(QString::fromStdString(add.first));
            if (data == target.value().variables.end()) {
                data = target.value()
                             .variables
                             .insert(QString::fromStdString(add.first), VariableDefinition());
            }

            setIfValid(data.value().format, add.second["Format"]);
            setIfValid(data.value().mvc, add.second["MVC"]);
            setIfValid(data.value().description, add.second["Description"]);

            const auto &type = add.second["Type"].toString();
            if (!type.empty()) {
                if (Util::equal_insensitive(type, "SystemFlags", "SysFlags", "System")) {
                    data.value().mode = VariableDefinition::Mode_SystemFlags;
                } else if (Util::equal_insensitive(type, "String", "Str")) {
                    data.value().mode = VariableDefinition::Mode_String;
                } else if (Util::equal_insensitive(type, "Integer", "Int")) {
                    data.value().mode = VariableDefinition::Mode_Integer;
                } else if (Util::equal_insensitive(type, "Flags", "Flag")) {
                    data.value().mode = VariableDefinition::Mode_Flags;
                } else {
                    data.value().mode = VariableDefinition::Mode_Decimal;
                }
            }

            if (INTEGER::defined(add.second["SortPriority"].toInt64())) {
                data.value().sortPriority = add.second["SortPriority"].toInt64();
            }

            for (auto h : add.second["Headers"].toHash()) {
                if (h.first.empty())
                    continue;
                QString v(h.second.toDisplayString());
                if (v.isEmpty())
                    continue;
                data.value().variableHeaders.insert(QString::fromStdString(h.first), v);
            }

            auto params = add.second["Parameters"];
            if (params.exists()) {
                Range::overlayFragmenting(data.value().timeDependence,
                                          VariableTimeDependence::MergeConfig(seg.getStart(),
                                                                              seg.getEnd(),
                                                                              add.second["Parameters"],
                                                                              station, archive,
                                                                              add.first));
            }
        }
    }
}


OutputCPD2::VariableTimeDependence::VariableTimeDependence(const MergeMetadata &other,
                                                           double s,
                                                           double e) : start(s),
                                                                       end(e),
                                                                       wavelength(FP::undefined()),
                                                                       inputs(),
                                                                       calibration(),
                                                                       lookupPath(),
                                                                       flagsBits(),
                                                                       flagsAccept(),
                                                                       flagBitShift(0),
                                                                       metadataSuppliedWavelength(
                                                                               false),
                                                                       metadataSuppliedFlags()
{
    applyMetadata(other.meta);
}

OutputCPD2::VariableTimeDependence::VariableTimeDependence(const VariableTimeDependence &under,
                                                           const MergeMetadata &over,
                                                           double s,
                                                           double e) : start(s),
                                                                       end(e),
                                                                       wavelength(under.wavelength),
                                                                       inputs(under.inputs),
                                                                       calibration(
                                                                               under.calibration),
                                                                       lookupPath(under.lookupPath),
                                                                       flagsBits(under.flagsBits),
                                                                       flagsAccept(
                                                                               under.flagsAccept),
                                                                       flagBitShift(
                                                                               under.flagBitShift),
                                                                       metadataSuppliedWavelength(
                                                                               under.metadataSuppliedWavelength),
                                                                       metadataSuppliedFlags(
                                                                               under.metadataSuppliedFlags)
{
    if (!inputs.matches(over.unit))
        return;
    applyMetadata(over.meta);
}

void OutputCPD2::VariableTimeDependence::applyMetadata(const Variant::Read &meta)
{
    double checkWL = meta.metadata("Wavelength").toDouble();
    if (!FP::defined(wavelength) || (FP::defined(checkWL) && metadataSuppliedWavelength)) {
        wavelength = checkWL;
        metadataSuppliedWavelength = true;
    }

    switch (meta.getType()) {
    case Variant::Type::MetadataFlags:
        for (auto add : meta.toMetadataSingleFlag()) {
            if (add.first.empty())
                continue;
            qint64 bits = add.second.hash("Bits").toInt64();
            if (!INTEGER::defined(bits))
                continue;

            bits >>= flagBitShift;
            if (bits == 0)
                continue;

            auto check = flagsBits.find(add.first);
            if (check != flagsBits.end()) {
                if (!metadataSuppliedFlags.count(add.first))
                    continue;
            }

            Util::insert_or_assign(flagsBits, add.first, bits);
            metadataSuppliedFlags.insert(add.first);
        }
        break;
    default:
        break;
    }
}

static void setIfUnset(QString &target, const Variant::Read &source)
{
    if (!target.isEmpty())
        return;
    if (source.getType() != Variant::Type::String)
        return;
    QString value(source.toDisplayString());
    if (value.isEmpty())
        return;
    target = value;
}

void OutputCPD2::mergeMetadata(double start,
                               double end,
                               const Archive::Selection::Match &defaultStations,
                               const Archive::Selection::Match &defaultArchives)
{
    Archive::Selection::List selections;
    for (QHash<QString, RecordDefinition>::const_iterator record = recordContents.constBegin(),
            endRecord = recordContents.constEnd(); record != endRecord; ++record) {
        for (QHash<QString, VariableDefinition>::const_iterator
                variable = record.value().variables.constBegin(),
                endVariable = record.value().variables.constEnd();
                variable != endVariable;
                ++variable) {
            for (const auto &seg : variable.value().timeDependence) {
                Util::append(seg.inputs.toArchiveSelections(defaultStations, defaultArchives),
                             selections);
            }
        }
    }
    for (auto mod = selections.begin(); mod != selections.end();) {
        mod->includeMetaArchive = true;
        mod->start = start;
        mod->end = end;

        bool allModified = true;
        for (auto &arc : mod->archives) {
            if (QRegExp::escape(QString::fromStdString(arc)).toStdString() != arc) {
                allModified = false;
                continue;
            }
            arc.append("_meta");
        }

        if (!allModified) {
            if (FP::defined(mod->end)) {
                mod->start = mod->end - 1.0;
            } else if (FP::defined(mod->start)) {
                mod->end = mod->start + 1.0;
            } else {
                mod = selections.erase(mod);
                continue;
            }
        }
        ++mod;
    }

    if (selections.empty())
        return;

    StreamSink::Iterator data;
    auto reader = access.readStream(selections, &data);
    SequenceValue::Transfer values;
    while (data.hasNext()) {
        auto value = data.next();
        if (!value.getUnit().isMeta())
            continue;
        auto name = value.getUnit();
        name.clearMeta();
        for (QHash<QString, RecordDefinition>::iterator record = recordContents.begin(),
                endRecord = recordContents.end(); record != endRecord; ++record) {
            for (QHash<QString, VariableDefinition>::iterator
                    variable = record.value().variables.begin(),
                    endVariable = record.value().variables.end();
                    variable != endVariable;
                    ++variable) {
                bool matched = false;
                for (QList<VariableTimeDependence>::const_iterator
                        seg = variable.value().timeDependence.constBegin(),
                        endSeg = variable.value().timeDependence.constEnd(); seg != endSeg; ++seg) {
                    if (seg->inputs.matches(name)) {
                        matched = true;
                        break;
                    }
                }
                if (!matched)
                    continue;

                Range::overlayFragmenting(variable.value().timeDependence,
                                          VariableTimeDependence::MergeMetadata(value.getStart(),
                                                                                value.getEnd(),
                                                                                value.read(),
                                                                                name));

                setIfUnset(variable.value().format, value.read().metadata("Format"));
                setIfUnset(variable.value().mvc, value.read().metadata("MVC"));

                if (variable.value().description.isEmpty()) {
                    QString check(value.read().metadata("Description").toDisplayString());
                    if (!check.isEmpty()) {
                        QString units(value.read().metadata("Units").toDisplayString());
                        if (!units.isEmpty()) {
                            check = tr("%1 (%2)", "description units").arg(check, units);
                        }
                        variable.value().description = check;
                    }
                }
            }
        }
    }
    reader->wait();
    reader.reset();
}

namespace {
struct VariableSorter {
    qint64 priority;
    QString name;

    static int getVariableColorOffset(const QStringList &cap, int base, int start)
    {
        if (cap.at(start) == "B")
            return base;
        if (cap.at(start) == "G")
            return base + 1;
        if (cap.at(start) == "R")
            return base + 2;
        return base + 3;
    }

    static int getBaseVariablePriority(const QString &variable)
    {
        /* Flags */
        {
            QRegExp check("^F\\d*_");
            if (check.indexIn(variable) != -1)
                return 0;
        }

        /* Extensives */
        {
            QRegExp check("^N\\d*_");
            if (check.indexIn(variable) != -1) {
                return 1;
            }
        }
        {
            QRegExp check("^Bs([0-9A-Z])_");
            if (check.indexIn(variable) != -1) {
                return getVariableColorOffset(check.capturedTexts(), 10, 1);
            }
        }
        {
            QRegExp check("^Bbs([0-9A-Z])_");
            if (check.indexIn(variable) != -1) {
                return getVariableColorOffset(check.capturedTexts(), 20, 1);
            }
        }
        {
            QRegExp check("^Ba([0-9A-Z])_");
            if (check.indexIn(variable) != -1) {
                return getVariableColorOffset(check.capturedTexts(), 30, 1);
            }
        }

        /* General instrument temperature pressure (usually neph) */
        {
            QRegExp check("^T(u?)_");
            if (check.indexIn(variable) != -1) {
                QStringList cap(check.capturedTexts());
                if (cap.at(1) == "u")
                    return 3000;
                return 3001;
            }
        }
        {
            QRegExp check("^U(u?)_");
            if (check.indexIn(variable) != -1) {
                QStringList cap(check.capturedTexts());
                if (cap.at(1) == "u")
                    return 3002;
                return 3003;
            }
        }
        {
            QRegExp check("^P_");
            if (check.indexIn(variable) != -1) {
                return 3004;
            }
        }

        /* Intensives */
        {
            QRegExp check("^Be([0-9A-Z])_");
            if (check.indexIn(variable) != -1) {
                return getVariableColorOffset(check.capturedTexts(), 900, 1);
            }
        }
        {
            QRegExp check("^ZSSA([0-9A-Z])_");
            if (check.indexIn(variable) != -1) {
                return getVariableColorOffset(check.capturedTexts(), 910, 1);
            }
        }
        {
            QRegExp check("^ZBfr([0-9A-Z])_");
            if (check.indexIn(variable) != -1) {
                return getVariableColorOffset(check.capturedTexts(), 920, 1);
            }
        }
        {
            QRegExp check("^ZAng(.?)([0-9A-Z])_");
            if (check.indexIn(variable) != -1) {
                QStringList cap(check.capturedTexts());
                if (cap.at(1) == "S")
                    return getVariableColorOffset(cap, 930, 2);
                return getVariableColorOffset(cap, 934, 2);
            }
        }
        {
            QRegExp check("^ZAsyPoly([0-9A-Z])_");
            if (check.indexIn(variable) != -1) {
                return getVariableColorOffset(check.capturedTexts(), 940, 1);
            }
        }
        {
            QRegExp check("^ZRFE([0-9A-Z])_");
            if (check.indexIn(variable) != -1) {
                return getVariableColorOffset(check.capturedTexts(), 950, 1);
            }
        }
        {
            QRegExp check("^ZfRH([0-9A-Z])_");
            if (check.indexIn(variable) != -1) {
                return getVariableColorOffset(check.capturedTexts(), 960, 1);
            }
        }
        {
            QRegExp check("^ZR([sa])p([0-9A-Z])_");
            if (check.indexIn(variable) != -1) {
                QStringList cap(check.capturedTexts());
                if (cap.at(1) == "s")
                    return getVariableColorOffset(cap, 1970, 2);
                return getVariableColorOffset(cap, 1974, 2);
            }
        }
        {
            QRegExp check("^ZCCN([Ck])_");
            if (check.indexIn(variable) != -1) {
                QStringList cap(check.capturedTexts());
                if (cap.at(1) == "C")
                    return 980;
                return 981;
            }
        }

        /* Aeth instance numbering */
        {
            QRegExp check("^Z?Ir[0-9A-Z]_");
            if (check.indexIn(variable) != -1) {
                return 99999;
            }
        }
        {
            QRegExp check("^I[fp]z?[0-9A-Z]_");
            if (check.indexIn(variable) != -1) {
                return 99999;
            }
        }
        {
            QRegExp check("^X[0-9A-Z]c_");
            if (check.indexIn(variable) != -1) {
                return 99999;
            }
        }

        return 99999;
    }

    bool operator<(const VariableSorter &other) const
    {
        if (priority != other.priority)
            return priority < other.priority;
        int a = getBaseVariablePriority(name);
        int b = getBaseVariablePriority(other.name);
        if (a != b)
            return a < b;
        return name < other.name;
    }
};
}

void OutputCPD2::buildRecordState()
{
    for (QHash<QString, RecordDefinition>::iterator record = recordContents.begin(),
            endRecord = recordContents.end(); record != endRecord; ++record) {
        if (record.value().variables.isEmpty())
            continue;
        Q_ASSERT(!recordState.contains(record.key()));
        RecordState *rState = new RecordState;
        Q_ASSERT(rState);
        recordState.insert(record.key(), rState);

        rState->start = FP::undefined();

        QList<VariableSorter> sorter;
        for (QHash<QString, VariableDefinition>::const_iterator
                variable = record.value().variables.constBegin(),
                endVariable = record.value().variables.constEnd();
                variable != endVariable;
                ++variable) {
            VariableSorter add;
            add.name = variable.key();
            add.priority = variable.value().sortPriority;
            sorter.append(add);
        }
        std::sort(sorter.begin(), sorter.end());

        for (QList<VariableSorter>::const_iterator add = sorter.constBegin(),
                endSort = sorter.constEnd(); add != endSort; ++add) {
            Q_ASSERT(record.value().variables.contains(add->name));

            VariableState *vState = new VariableState;
            Q_ASSERT(vState);
            rState->variables.append(vState);
            vState->record = rState;

            vState->validUntil = 0;
            vState->name = add->name;
            vState->latestFormatter = NULL;

            QHash<QString, VariableDefinition>::iterator
                    variable = record.value().variables.find(add->name);
            Q_ASSERT(variable != record.value().variables.end());

            for (QList<VariableTimeDependence>::iterator
                    vtd = variable.value().timeDependence.begin(),
                    endVTD = variable.value().timeDependence.end(); vtd != endVTD; ++vtd) {
                /* Filter out all non-accepted flags */
                if (!vtd->flagsAccept.empty()) {
                    for (auto check = vtd->flagsBits.begin(); check != vtd->flagsBits.end();) {
                        if (vtd->flagsAccept.count(check->first)) {
                            ++check;
                            continue;
                        }
                        check = vtd->flagsBits.erase(check);
                    }
                }
            }

            if (variable.value().format.isEmpty()) {
                QString formatString;
                if (!variable.value().mvc.isEmpty()) {
                    formatString = variable.value().mvc;
                } else {
                    switch (variable.value().mode) {
                    case VariableDefinition::Mode_Decimal:
                        formatString = "0000.000";
                        break;
                    case VariableDefinition::Mode_Integer:
                        formatString = "00000";
                        break;
                    case VariableDefinition::Mode_String:
                        formatString = QString();
                        break;
                    case VariableDefinition::Mode_Flags:
                    case VariableDefinition::Mode_SystemFlags:
                        formatString = "FFFF";
                        break;
                    }
                }
                variable.value().format = formatString;
            }

            if (variable.value().mvc.isEmpty()) {
                switch (variable.value().mode) {
                case VariableDefinition::Mode_Decimal:
                case VariableDefinition::Mode_Integer:
                    variable.value().mvc = NumberFormat(variable.value().format).mvc();
                    break;
                case VariableDefinition::Mode_String:
                    variable.value().mvc = "Z";
                    break;
                case VariableDefinition::Mode_Flags:
                case VariableDefinition::Mode_SystemFlags: {
                    NumberFormat fmt(variable.value().format);
                    fmt.setMode(NumberFormat::Hex);
                    variable.value().mvc = fmt.mvc();
                    break;
                }
                }
            }
            vState->mvc = variable.value().mvc.toUtf8();
        }
        Q_ASSERT(!rState->variables.isEmpty());

        qCDebug(log_outputcpd3) << "Record" << record.key() << "constructed with"
                                << rState->variables.size() << "variables";
    }
}

QByteArray OutputCPD2::buildHeaders() const
{
    QHash<QString, QString> headers;

    headers.insert("var;DateTime;FieldDesc", "Date String (YYYY-MM-DDThh:mm:ssZ)");
    headers.insert("var;EPOCH;FieldDesc", "Epoch time: seconds from Jan 1 1970");
    headers.insert("var;STN;FieldDesc", "Station ID code");
    headers.insert("fil;cpd3;revision", Environment::revision());
    headers.insert("fil;cpd3;environment", Environment::describe());

    for (QHash<QString, RecordState *>::const_iterator record = recordState.constBegin(),
            endRecord = recordState.constEnd(); record != endRecord; ++record) {
        headers.insert(QString("fil;ProcessedBy;%1").arg(record.key()), "da.output.cpd2");


        QHash<QString, RecordDefinition>::const_iterator
                rDef = recordContents.constFind(record.key());
        Q_ASSERT(rDef != recordContents.constEnd());
        for (QHash<QString, QString>::const_iterator
                merge = rDef.value().globalHeaders.constBegin(),
                endMerge = rDef.value().globalHeaders.constEnd(); merge != endMerge; ++merge) {
            headers.insert(merge.key(), merge.value());
        }

        QStringList recordMVCs;
        QStringList recordFormats;
        QStringList recordColumns;
        for (QList<VariableState *>::const_iterator
                variable = record.value()->variables.constBegin(),
                endVariable = record.value()->variables.constEnd();
                variable != endVariable;
                ++variable) {
            recordColumns.append((*variable)->name);

            QHash<QString, VariableDefinition>::const_iterator
                    vDef = rDef.value().variables.constFind((*variable)->name);
            Q_ASSERT(vDef != rDef.value().variables.constEnd());

            Q_ASSERT(QString::fromUtf8((*variable)->mvc) == vDef.value().mvc);
            recordMVCs.append(vDef.value().mvc);

            switch (vDef.value().mode) {
            case VariableDefinition::Mode_Decimal:
                recordFormats.append(NumberFormat(vDef.value().format).getFormatCPD2SFMT());
                break;
            case VariableDefinition::Mode_Integer:
                recordFormats.append(NumberFormat(vDef.value().format).getFormatPrintf("d"));
                break;
            case VariableDefinition::Mode_String:
                recordFormats.append("%s");
                break;
            case VariableDefinition::Mode_Flags:
            case VariableDefinition::Mode_SystemFlags: {
                NumberFormat fmt(vDef.value().format);
                fmt.setMode(NumberFormat::Hex);
                recordFormats.append(fmt.getFormatPrintf("X"));
                break;
            }
            }

            double priorWavelength = FP::undefined();
            for (QList<VariableTimeDependence>::const_iterator
                    vtd = vDef.value().timeDependence.constBegin(),
                    endVTD = vDef.value().timeDependence.constEnd(); vtd != endVTD; ++vtd) {
                if (!FP::defined(vtd->wavelength))
                    continue;
                if (FP::equal(priorWavelength, vtd->wavelength))
                    continue;

                QString timeStart;
                if (!FP::defined(vtd->start) || (FP::defined(start) && vtd->start < start)) {
                    timeStart = "0";
                } else {
                    timeStart = Time::toISO8601(vtd->start);
                }

                headers.insert(QString("var;%1;Wavelength;%2").arg((*variable)->name, timeStart),
                               QString::number(qRound(vtd->wavelength)));
                priorWavelength = vtd->wavelength;
            }

            if (!vDef.value().description.isEmpty()) {
                headers.insert(QString("var;%1;FieldDesc").arg((*variable)->name),
                               vDef.value().description);
            }

            for (QHash<QString, QString>::const_iterator
                    merge = vDef.value().variableHeaders.constBegin(),
                    endMerge = vDef.value().variableHeaders.constEnd();
                    merge != endMerge;
                    ++merge) {
                headers.insert(QString("var;%1;%2").arg((*variable)->name, merge.key()),
                               merge.value());
            }
        }

        headers.insert(QString("row;colhdr;%1").arg(record.key()),
                       QString("%1;STN;EPOCH;DateTime;%2").arg(record.key(),
                                                               recordColumns.join(";")));
        headers.insert(QString("row;varfmt;%1").arg(record.key()), record.key() +
                QString(";%s;%u;%04d-%02d-%02dT%02d:%02d:%02dZ;") +
                recordFormats.join(";"));
        headers.insert(QString("row;mvc;%1").arg(record.key()),
                       QString("%1;ZZZ;0;9999-99-99T99:99:99Z;%2").arg(record.key(),
                                                                       recordMVCs.join(";")));
    }

    QStringList sortedKeys(headers.keys());
    std::sort(sortedKeys.begin(), sortedKeys.end());

    QByteArray result;
    for (QList<QString>::const_iterator key = sortedKeys.constBegin(), end = sortedKeys.constEnd();
            key != end;
            ++key) {
        QString str(*key);
        ExternalSink::simplifyUnicode(str);
        result.append('!');
        result.append(str.toUtf8());
        result.append(',');
        str = headers.value(*key);
        ExternalSink::simplifyUnicode(str);
        result.append(str.toUtf8());
        result.append('\n');
    }
    return result;
}
