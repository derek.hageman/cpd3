/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef OUTPUTCPD2_H
#define OUTPUTCPD2_H

#include "core/first.hxx"

#include <vector>
#include <mutex>
#include <condition_variable>
#include <QtGlobal>
#include <QObject>
#include <QStringList>
#include <QList>
#include <QHash>
#include <QAtomicInt>

#include "datacore/archive/access.hxx"
#include "datacore/segment.hxx"
#include "datacore/sequencematch.hxx"
#include "datacore/stream.hxx"

class OutputCPD2 : public QObject {
Q_OBJECT

    class ProcessingEgress : public CPD3::Data::StreamSink {
        OutputCPD2 *parent;
    public:
        ProcessingEgress(OutputCPD2 *parent);

        virtual ~ProcessingEgress();

        void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

        void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

        void incomingData(const CPD3::Data::SequenceValue &value) override;

        void incomingData(CPD3::Data::SequenceValue &&value) override;

        void endData() override;
    };

    ProcessingEgress processingEgress;

    CPD3::Data::Archive::Access access;
    double start;
    double end;

#ifdef Q_OS_UNIX
    bool stdoutOpen;
#endif
    QIODevice *outputDevice;
    QByteArray pendingOutput;

    void writeOutputData(const QByteArray &data);

    friend class ProcessingEgress;

    std::mutex readMutex;
    std::condition_variable readCond;
    bool readTerminated;
    double lastReadStart;
    CPD3::Data::SequenceValue::Transfer readValues;
    enum {
        Read_NotStarted,
        Read_FirstValue,
        Read_InProgress,
        Read_DataEnded,
        Read_DataEnded_OnFirst,
        Read_OutputCompleted,
    } readState;
    QAtomicInt processCounter;

    void incomingReadData(const CPD3::Data::SequenceValue::Transfer &values);

    void incomingReadData(CPD3::Data::SequenceValue::Transfer &&values);

    void incomingReadData(const CPD3::Data::SequenceValue &value);

    void incomingReadData(CPD3::Data::SequenceValue &&value);

    void incomingReadEnd();

    void readStall(std::unique_lock<std::mutex> &lock);

    void openOutputDevice();

    struct VariableTimeDependence {
        double start;
        double end;

        double wavelength;

        CPD3::Data::SequenceMatch::Composite inputs;

        CPD3::Calibration calibration;
        QString lookupPath;
        std::unordered_map<CPD3::Data::Variant::Flag, qint64> flagsBits;
        CPD3::Data::Variant::Flags flagsAccept;
        qint64 flagBitShift;

        bool metadataSuppliedWavelength;
        CPD3::Data::Variant::Flags metadataSuppliedFlags;

        VariableTimeDependence();

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        VariableTimeDependence(const VariableTimeDependence &other, double start, double end);

        struct MergeConfig {
            double start;
            double end;
            CPD3::Data::Variant::Read config;
            CPD3::Data::SequenceName::Component station;
            CPD3::Data::SequenceName::Component archive;
            CPD3::Data::SequenceName::Component variable;

            inline MergeConfig(double s,
                               double e, const CPD3::Data::Variant::Read &c,
                               const CPD3::Data::SequenceName::Component &stn,
                               const CPD3::Data::SequenceName::Component &arc,
                               const CPD3::Data::SequenceName::Component &var) : start(s),
                                                     end(e),
                                                     config(c),
                                                     station(stn),
                                                     archive(arc),
                                                     variable(var)
            { }

            inline double getStart() const
            { return start; }

            inline double getEnd() const
            { return end; }
        };

        VariableTimeDependence(const MergeConfig &other, double start, double end);

        VariableTimeDependence(const VariableTimeDependence &under,
                               const MergeConfig &over,
                               double start,
                               double end);

        void configure(const CPD3::Data::Variant::Read &config,
                       const CPD3::Data::SequenceName::Component &station,
                       const CPD3::Data::SequenceName::Component &archive,
                       const CPD3::Data::SequenceName::Component &variable);

        struct MergeMetadata {
            double start;
            double end;
            const CPD3::Data::Variant::Read &meta;
            CPD3::Data::SequenceName unit;

            inline MergeMetadata(double s,
                                 double e,
                                 const CPD3::Data::Variant::Read &m,
                                 const CPD3::Data::SequenceName &u)
                    : start(s),
                      end(e),
                      meta(m),
                      unit(u)
            { }

            inline double getStart() const
            { return start; }

            inline double getEnd() const
            { return end; }
        };

        VariableTimeDependence(const MergeMetadata &other, double start, double end);

        VariableTimeDependence(const VariableTimeDependence &under,
                               const MergeMetadata &over,
                               double start,
                               double end);

        void applyMetadata(const CPD3::Data::Variant::Read &meta);
    };

    struct VariableDefinition {
        enum {
            Mode_Decimal, Mode_Integer, Mode_String, Mode_Flags, Mode_SystemFlags,
        } mode;
        qint64 sortPriority;

        QString format;
        QString mvc;
        QString description;

        QHash<QString, QString> variableHeaders;

        QList<VariableTimeDependence> timeDependence;

        VariableDefinition();
    };

    struct RecordDefinition {
        QHash<QString, VariableDefinition> variables;
        QHash<QString, QString> globalHeaders;
    };
    QHash<QString, RecordDefinition> recordContents;

    void prepareOutput(const CPD3::Data::SequenceName::Component &station,
                       const CPD3::Data::SequenceName::Component &archive,
                       const QString &record,
                       const CPD3::Data::ValueSegment::Transfer &config);

    void mergeMetadata(double start,
                       double end,
                       const CPD3::Data::Archive::Selection::Match &defaultStations,
                       const CPD3::Data::Archive::Selection::Match &defaultArchives);

    void buildRecordState();

    QByteArray buildHeaders() const;

    class VariableFormatter {
    public:
        VariableFormatter();

        virtual ~VariableFormatter();

        virtual QString format(const CPD3::Data::SequenceValue &value) const = 0;
    };

    struct RecordState;

    struct VariableState {
        RecordState *record;

        QString name;
        QByteArray mvc;

        double validUntil;
        CPD3::Data::SequenceValue latestValue;
        VariableFormatter *latestFormatter;

        void fromSequenceValue(const CPD3::Data::SequenceValue &value,
                               VariableFormatter *formatter);
    };

    struct RecordState {
        double start;
        QList<VariableState *> variables;
    };
    QHash<QString, RecordState *> recordState;

    class DispatchHandler {
    public:
        double validStart;
        double validEnd;

        DispatchHandler();

        virtual ~DispatchHandler();

        bool appliesTo(double start, double end) const;

        virtual void incoming(const CPD3::Data::SequenceValue &value) = 0;
    };

    class DispatchFanout : public DispatchHandler {
        std::vector<DispatchHandler *> handlers;
    public:
        DispatchFanout(const QList<DispatchHandler *> &handlers);

        virtual ~DispatchFanout();

        virtual void incoming(const CPD3::Data::SequenceValue &value);
    };

    class DispatchDiscard : public DispatchHandler {
    public:
        DispatchDiscard();

        virtual ~DispatchDiscard();

        virtual void incoming(const CPD3::Data::SequenceValue &value);
    };

    class DispatchPath : public DispatchHandler {
        DispatchHandler *target;
        QString path;
    public:
        DispatchPath(DispatchHandler *target, const QString &path);

        virtual ~DispatchPath();

        virtual void incoming(const CPD3::Data::SequenceValue &value);
    };

    class DispatchSimple : public DispatchHandler, public VariableFormatter {
        VariableState *target;
    public:
        DispatchSimple(VariableState *target);

        virtual ~DispatchSimple();

        virtual void incoming(const CPD3::Data::SequenceValue &value);
    };

    class DispatchDecimal : public DispatchSimple {
        VariableState *target;
        CPD3::NumberFormat formatter;
        CPD3::Calibration calibration;
    public:
        DispatchDecimal(VariableState *target, const QString &format, const CPD3::Calibration &cal);

        virtual ~DispatchDecimal();

        virtual void incoming(const CPD3::Data::SequenceValue &value);

        virtual QString format(const CPD3::Data::SequenceValue &value) const;
    };

    class DispatchInteger : public DispatchSimple {
        CPD3::NumberFormat formatter;
    public:
        DispatchInteger(VariableState *target, const QString &format);

        virtual ~DispatchInteger();

        virtual void incoming(const CPD3::Data::SequenceValue &value);

        virtual QString format(const CPD3::Data::SequenceValue &value) const;
    };

    class DispatchString : public DispatchSimple {
    public:
        DispatchString(VariableState *target);

        virtual ~DispatchString();

        virtual void incoming(const CPD3::Data::SequenceValue &value);

        virtual QString format(const CPD3::Data::SequenceValue &value) const;
    };

    class DispatchFlags : public DispatchSimple {
        CPD3::NumberFormat formatter;
        std::unordered_map<CPD3::Data::Variant::Flag, qint64> bits;
    public:
        DispatchFlags(VariableState *target,
                      const QString &format,
                      const std::unordered_map<CPD3::Data::Variant::Flag, qint64> &bits);

        virtual ~DispatchFlags();

        virtual QString format(const CPD3::Data::SequenceValue &value) const;

    protected:
        virtual qint64 additionalBits(const CPD3::Data::SequenceValue &value) const;
    };

    class DispatchSystemFlags : public DispatchFlags {
    public:
        DispatchSystemFlags(VariableState *target,
                            const QString &format,
                            const std::unordered_map<CPD3::Data::Variant::Flag, qint64> &bits);

        virtual ~DispatchSystemFlags();

    protected:
        virtual qint64 additionalBits(const CPD3::Data::SequenceValue &value) const;
    };

    QHash<CPD3::Data::SequenceName, DispatchHandler *> targets;
    QList<DispatchHandler *> uniqueTargets;
    DispatchDiscard discardTarget;

    void startReading(const CPD3::Data::Archive::Selection::Match &defaultStations,
                      const CPD3::Data::Archive::Selection::Match &defaultArchives);

    void dispatchValue(const CPD3::Data::SequenceValue &value);

    static bool canExtend(QHash<QString, VariableDefinition>::const_iterator variable,
                          QList<VariableTimeDependence>::const_iterator prior,
                          QList<VariableTimeDependence>::const_iterator current);

    DispatchHandler *buildDispatch(const CPD3::Data::SequenceName &unit);

    bool handleOutput();

    bool processPending();

    void flushPendingRecords();

public:
    OutputCPD2();

    virtual ~OutputCPD2();

    int parseArguments(QStringList arguments);

public slots:

    void shutdown();

private slots:

    void process();

    void scheduleProcessIfNeeded();

signals:

    void deferredProcess();
};

#endif
