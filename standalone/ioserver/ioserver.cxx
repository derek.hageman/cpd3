/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTranslator>
#include <QCoreApplication>
#include <QStringList>
#include <QLibraryInfo>

#include "core/abort.hxx"
#include "core/number.hxx"
#include "core/range.hxx"
#include "core/component.hxx"
#include "core/threadpool.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/segment.hxx"
#include "clicore/terminaloutput.hxx"
#include "clicore/optionparse.hxx"
#include "clicore/argumentparser.hxx"
#include "clicore/interfaceparse.hxx"
#include "acquisition/ioserver.hxx"

#ifdef Q_OS_UNIX

#include "io/drivers/stdio.hxx"

#endif

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;
using namespace CPD3::CLI;

class Parser : public ArgumentParser {
    bool &spawned;
public:
    Parser(bool &spawned) : spawned(spawned)
    { }

    virtual ~Parser()
    { }

protected:
    virtual ArgumentParser::RawOverrideResult rawOverride(QString &argument) noexcept(false)
    {
        /* Not localized because it's fixed in the IOInterface code. */
        if (argument == "--spawned") {
            spawned = true;
            return ArgumentParser::RawOverride_Terminate;
        }
        return ArgumentParser::rawOverride(argument);
    }

    virtual QList<BareWordHelp> getBareWordHelp()
    {
        return InterfaceParse::getBareWordHelp(false);
    }

    virtual QString getBareWordCommandLine()
    {
        return InterfaceParse::getBareWordCommandLine(false);
    }

    virtual QString getProgramName()
    { return tr("cpd3.ioserver", "program name"); }

    virtual QString getDescription()
    {
        return tr("This starts an I/O server instance.  This instance can be used by CPD3 "
                  "clients to connect to the target it was started with.");
    }

    virtual QHash<QString, QString> getDocumentationTypeID()
    {
        QHash<QString, QString> result;
        result.insert("type", "ioserver");
        return result;
    }
};

static int parseArguments(QStringList arguments,
                          Variant::Write &configuration,
                          bool &spawned,
                          QString &spawnToken)
{
    spawned = false;
    spawnToken.clear();
    if (!arguments.isEmpty())
        arguments.removeFirst();

    ComponentOptions options;

    options.add("ssl-cert", new ComponentOptionFile(QObject::tr("ssl-cert", "name"),
                                                    QObject::tr("The server SSL certificate file"),
                                                    QObject::tr(
                                                            "This sets the certificate used on the TCP server."),
                                                    QString()));
    options.add("ssl-key", new ComponentOptionFile(QObject::tr("ssl-key", "name"),
                                                   QObject::tr("The server SSL key file"),
                                                   QObject::tr(
                                                           "This sets the key used on the TCP server."),
                                                   QString()));

    ComponentOptionSingleInteger *optionInteger =
            new ComponentOptionSingleInteger(QObject::tr("external", "name"),
                                             QObject::tr("Set the external server TCP port"),
                                             QObject::tr(
                                                     "When set the server will listen for TCP connections on "
                                                     "the port specified."),
                                             QObject::tr("Disabled"));
    optionInteger->setAllowUndefined(false);
    optionInteger->setMinimum(1);
    optionInteger->setMaximum(65535);
    options.add("external", optionInteger);

    options.add("disable-local", new ComponentOptionBoolean(QObject::tr("disable-local", "name"),
                                                            QObject::tr(
                                                                    "Disable the local system interface"),
                                                            QObject::tr(
                                                                    "This disables the local system interface that is "
                                                                    "normally used to interface with the server.  When set the "
                                                                    "external option must be enabled or the server will "
                                                                    "not start.  Not recommended for general use."),
                                                            QString()));

    Parser parser(spawned);
    try {
        parser.parse(arguments, options);
    } catch (ArgumentParsingException ape) {
        QString text(ape.getText());
        if (ape.isError()) {
            if (!text.isEmpty())
                TerminalOutput::simpleWordWrap(text, true);
            QCoreApplication::exit(1);
            return -1;
        } else {
            if (!text.isEmpty())
                TerminalOutput::simpleWordWrap(text);
            QCoreApplication::exit(0);
        }
        return 1;
    }

#ifdef Q_OS_UNIX
    if (spawned)
        return 0;
#endif

    if (arguments.isEmpty()) {
        TerminalOutput::simpleWordWrap(QObject::tr("A target specification is required."), true);
        QCoreApplication::exit(1);
        return -1;
    }

    if (spawned) {
        spawnToken = arguments.takeFirst();
    } else {
        try {
            configuration = InterfaceParse::parse(arguments, false).write();
        } catch (InterfaceParsingException ipe) {
            QString text(ipe.getDescription());
            if (!text.isEmpty())
                TerminalOutput::simpleWordWrap(text, true);
            QCoreApplication::exit(1);
            return -1;
        }
    }

    if (qobject_cast<ComponentOptionBoolean *>(options.get("disable-local"))->get()) {
        configuration.hash("DisableLocal").setBool(true);
    }

    if (options.isSet("external")) {
        configuration.hash("External")
                     .hash("Port")
                     .setInt64(qobject_cast<ComponentOptionSingleInteger *>(options.get("external"))
                                       ->get());
        if (options.isSet("ssl-cert")) {
            configuration.hash("External")
                         .hash("SSL")
                         .hash("Certificate")
                         .setString(qobject_cast<ComponentOptionFile *>(
                                 options.get("ssl-cert"))->get());
        }
        if (options.isSet("ssl-key")) {
            configuration.hash("External")
                         .hash("SSL")
                         .hash("Key")
                         .setString(qobject_cast<ComponentOptionFile *>(
                                 options.get("ssl-key"))->get());
        }
    }

    return 0;
}

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

    QString translateLocation;
    QByteArray cpd3(qgetenv("CPD3"));
    if (cpd3.length() > 0) {
        translateLocation = QString(cpd3) + "/locale";
    }
    QTranslator cpd3Translator;
    cpd3Translator.load("cpd3_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cpd3Translator);
    QTranslator cliTranslator;
    cliTranslator.load("cli_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cliTranslator);
    QTranslator ioserverTranslator;
    ioserverTranslator.load("ioserver_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&ioserverTranslator);


    Variant::Write configuration = Variant::Write::empty();
    bool spawned;
    QString spawnToken;
    if (int rc = parseArguments(app.arguments(), configuration, spawned, spawnToken)) {
        ThreadPool::system()->wait();
        if (rc == -1)
            return 1;
        return 0;
    }

    Abort::installAbortHandler(false);

    std::unique_ptr<IOServer> server;
    if (spawned) {
#ifdef Q_OS_UNIX
        auto h = IO::Access::stdio_in();
        if (h) {
            auto stream = h->stream();
            if (stream) {
                auto data = stream->readAll();
                auto target = IOTarget::deserialize(data);
                if (target) {
                    server.reset(new IOServer(std::move(target), true));
                }
            }
        }
#else
        auto tokenRaw = QByteArray::fromBase64(spawnToken.toLatin1());
        auto target = IOTarget::deserialize(Util::ByteView(tokenRaw));
        if (target) {
            server.reset(new IOServer(std::move(target), true));
        }
#endif
    } else {
        auto target = IOTarget::create(configuration);
        if (target) {
            server.reset(new IOServer(std::move(target)));
        }
    }
    if (!server) {
        TerminalOutput::simpleWordWrap(QObject::tr("Invalid IO configuration."), true);
        QCoreApplication::exit(1);
        return 1;
    }

    AbortPoller poller;
    QObject::connect(&poller, &AbortPoller::aborted, &app, &QCoreApplication::quit);
    poller.start();

    server->finished.connect(&app, [&app] { app.quit(); }, true);
    server->start();
    int rc = app.exec();

    poller.disconnect();
    server.reset();
    ThreadPool::system()->wait();
    return rc;
}
