/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTranslator>
#include <QLibraryInfo>
#include <QApplication>
#include <QMessageBox>


#include "core/abort.hxx"
#include "core/component.hxx"
#include "core/threadpool.hxx"
#include "datacore/variant/root.hxx"
#include "guidata/realtimemessagelog.hxx"
#include "clicore/argumentparser.hxx"
#include "clicore/terminaloutput.hxx"
#include "acquisition/acquisitionnetwork.hxx"
#include "io/process.hxx"

#include "acquisition_tray.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::GUI::Data;
using namespace CPD3::CLI;
using namespace CPD3::Acquisition;

class Parser : public ArgumentParser {
public:
    Parser()
    { }

    virtual ~Parser()
    { }

protected:
    virtual QList<BareWordHelp> getBareWordHelp()
    {
        return QList<BareWordHelp>() << BareWordHelp(tr("target", "target argument name"),
                                                     tr("The target to launch this server on."),
                                                     QString(),
                                                     tr("The \"target\" bare word argument specifies the target to connect to.  This "
                                                        "interpreted as a host name with an optional port as the second argument."));
    }

    virtual QString getBareWordCommandLine()
    { return tr("[target ...]", "target command line"); }

    virtual QString getProgramName()
    { return tr("cpd3.acquisition.tray", "program name"); }

    virtual QString getDescription()
    {
        return tr("This is the system tray icon for an acquisition system.  It shows the basic "
                  "system state and allows for basic commands to be issued.  If no target is "
                  "given it all connect to the default system on the local machine.");
    }

    virtual QHash<QString, QString> getDocumentationTypeID()
    {
        QHash<QString, QString> result;
        result.insert("type", "acquisition_display");
        return result;
    }
};

static bool parseArguments(QStringList arguments, Variant::Write &configuration, bool &hideQuit)
{
    hideQuit = false;
    if (!arguments.isEmpty())
        arguments.removeFirst();

    ComponentOptions options;

    options.add("ssl-cert", new ComponentOptionFile(QObject::tr("ssl-cert", "name"),
                                                    QObject::tr("The server SSL certificate file"),
                                                    QObject::tr(
                                                            "This sets the certificate used on the TCP server."),
                                                    QString()));
    options.add("ssl-key", new ComponentOptionFile(QObject::tr("ssl-key", "name"),
                                                   QObject::tr("The server SSL key file"),
                                                   QObject::tr(
                                                           "This sets the key used on the TCP server."),
                                                   QString()));
    options.add("local", new ComponentOptionBoolean(QObject::tr("local", "name"), QObject::tr(
            "Interpret the target as a local socket name"), QObject::tr(
            "When enabled the target specification is interpreted "
            "as a local socket name instead of a remote TCP server."),
                                                    QObject::tr("Disabled", "local default")));

    options.add("hide-quit", new ComponentOptionBoolean(QObject::tr("hide-quit", "name"),
                                                        QObject::tr("Hide the quit menu option"),
                                                        QObject::tr(
                                                                "This disables the quit menu option.  Selecting the "
                                                                "quit menu option does not actually stop the acquisition system, "
                                                                "so it sometimes makes sense to hide it entirely."),
                                                        QObject::tr("Disabled", "local default")));

    Parser parser;
    try {
        parser.parse(arguments, options);
    } catch (ArgumentParsingException ape) {
        if (ape.isError()) {
            if (!ape.getText().isEmpty()) {
#ifndef NO_CONSOLE
                TerminalOutput::simpleWordWrap(ape.getText(), true);
#else
                QMessageBox::critical(0, QObject::tr("Invalid Usage"), ape.getText());
#endif
            }
            QCoreApplication::exit(1);
        } else {
            if (!ape.getText().isEmpty()) {
#ifndef NO_CONSOLE
                TerminalOutput::simpleWordWrap(ape.getText());
#else
                QMessageBox::information(0, QObject::tr("Help"), ape.getText());
#endif
            }
            QCoreApplication::exit(0);
        }
        return false;
    }

    if (options.isSet("hide-quit")) {
        hideQuit = qobject_cast<ComponentOptionBoolean *>(options.get("hide-quit"))->get();
    }

    if (arguments.isEmpty())
        return true;

    if (options.isSet("ssl-cert")) {
        configuration.hash("SSL")
                     .hash("Certificate")
                     .setString(
                             qobject_cast<ComponentOptionFile *>(options.get("ssl-cert"))->get());
    }
    if (options.isSet("ssl-key")) {
        configuration.hash("SSL")
                     .hash("Key")
                     .setString(qobject_cast<ComponentOptionFile *>(options.get("ssl-key"))->get());
    }

    if (options.isSet("local")) {
        if (qobject_cast<ComponentOptionBoolean *>(options.get("local"))->get()) {
            configuration.hash("Type").setString("Local");
            configuration.hash("Name").setString(arguments.takeFirst());
            return true;
        }
    }

    configuration.hash("Type").setString("Remote");
    configuration.hash("Server").setString(arguments.takeFirst());

    if (!arguments.isEmpty()) {
        bool ok = false;
        QString portStr(arguments.takeFirst());
        quint16 port = portStr.toUShort(&ok);
        if (ok && port > 1) {
            configuration.hash("ServerPort").setInt64(port);
        } else {
            TerminalOutput::simpleWordWrap(QObject::tr("Port \"%1\" is not valid.").arg(portStr));
            QCoreApplication::exit(1);
            return false;
        }
    }

    return true;
}

AcquisitionTray::AcquisitionTray(const QStringList &arguments) : normalIcon(":/icon.svg"),
                                                                 redIcon(":/icon_red.svg"),
                                                                 icon(redIcon, this),
                                                                 blinkTimer(this),
                                                                 haveConnection(false),
                                                                 blinkPolarity(false),
                                                                 spawnRealtimeDelay(),
                                                                 contextMenu(),
                                                                 messageLogDialog(NULL)
{
    blinkTimer.setInterval(500);
    blinkTimer.setSingleShot(false);
    connect(&blinkTimer, SIGNAL(timeout()), this, SLOT(blinkIcon()));

    Variant::Write config = Variant::Write::empty();
    bool hideQuit = false;
    if (!parseArguments(arguments, config, hideQuit)) {
        return;
    }
    connection.reset(new AcquisitionNetworkClient(config));

    QAction *act;

    act = new QAction(tr("&Message Log Entry", "Context|MessageLog"), &contextMenu);
    act->setToolTip(tr("Add a user message log entry."));
    act->setStatusTip(tr("Add a message log entry"));
    act->setWhatsThis(
            tr("When selected this will open a dialog box to enter a user message log event.  This log entry goes into the main message log for the system."));
    connect(act, SIGNAL(triggered()), this, SLOT(messageLog()));
    contextMenu.addAction(act);

    act = new QAction(tr("&Show Graphical Interface", "Context|ShowGUI"), &contextMenu);
    act->setToolTip(tr("Show the realtime graphical user interface."));
    act->setStatusTip(tr("Show the interface"));
    act->setWhatsThis(tr("This shows the graphical realtime user interface."));
    connect(act, SIGNAL(triggered()), this, SLOT(showRealtimeGUI()));
    contextMenu.addAction(act);

    act = new QAction(tr("&Restart Acquisition System", "Context|Restart"), &contextMenu);
    act->setToolTip(tr("Restart the acquisition system."));
    act->setStatusTip(tr("Restart the acquisition system"));
    act->setWhatsThis(
            tr("This issues a request to restart the acquisition system.  The result will be that it reloads all configuration data."));
    connect(act, SIGNAL(triggered()), this, SLOT(verifyRestart()));
    contextMenu.addAction(act);

    if (!hideQuit) {
        act = new QAction(tr("&Quit", "Context|Quit"), &contextMenu);
        act->setToolTip(tr("Exit the tray display.  This does not stop acquisition."));
        act->setStatusTip(tr("Exit the tray icon display"));
        act->setWhatsThis(
                tr("This exits the tray icon display.  The acquisition system continues running unaffected."));
        act->setMenuRole(QAction::QuitRole);
        connect(act, SIGNAL(triggered()), this, SIGNAL(exitRequested()));
        contextMenu.addAction(act);
    }

    icon.setContextMenu(&contextMenu);
    icon.show();

    connectionStateChanged(false);
    connection->interfaceInformationUpdated
              .connect(this, std::bind(&AcquisitionTray::interfacesUpdated, this), true);
    connection->interfaceStateUpdated
              .connect(this, std::bind(&AcquisitionTray::interfacesUpdated, this), true);
    connection->connectionState
              .connect(this, std::bind(&AcquisitionTray::connectionStateChanged, this,
                                       std::placeholders::_1), true);
    connection->realtimeEvent
              .connect(this,
                       std::bind(&AcquisitionTray::realtimeEvent, this, std::placeholders::_1),
                       true);
    connection->start();
}

AcquisitionTray::~AcquisitionTray()
{
    if (messageLogDialog != NULL)
        delete messageLogDialog;
}

void AcquisitionTray::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
    case QSystemTrayIcon::Trigger:
        showRealtimeGUI();
        break;
    default:
        break;
    }
}

void AcquisitionTray::connectionStateChanged(bool connected)
{
    if (connected) {
        blinkTimer.stop();
        icon.setIcon(normalIcon);
        icon.setToolTip(tr("Connected to CPD3 acquisition system."));
        interfacesUpdated();
    } else {
        blinkPolarity = false;
        blinkTimer.start();
        icon.setIcon(redIcon);
        icon.setToolTip(tr("No connection to CPD3 acquisition system."));
        if (haveConnection) {
            haveConnection = false;
            icon.showMessage(tr("Connection Lost"),
                             tr("The connection to the acquisition system has been lost.  It will automatically be restored when the system is available again."),
                             QSystemTrayIcon::Warning);
        }
    }
    haveConnection = connected;
}

void AcquisitionTray::realtimeEvent(const Variant::Root &event)
{
    QString text(event["Text"].toDisplayString());
    if (text.isEmpty())
        return;

    icon.showMessage(tr("Message - %1").arg(event["Source"].toDisplayString()), text,
                     QSystemTrayIcon::Information);
}

void AcquisitionTray::interfacesUpdated()
{
    if (!haveConnection)
        return;
    auto interfaceInformation = connection->getInterfaceInformation();
    if (interfaceInformation.empty()) {
        icon.setToolTip(tr("Connected to CPD3 acquisition system."));
        return;
    }
    auto interfaceState = connection->getInterfaceState();

    QString tip(tr("Connected to CPD3 acquisition system.  Active:<ul>"));
    QStringList sorted;
    for (const auto &add : interfaceInformation) {
        sorted.append(QString::fromStdString(add.first));
    }
    std::sort(sorted.begin(), sorted.end());
    for (const auto &name : sorted) {
        auto information = interfaceInformation[name.toStdString()].read();
        auto state = interfaceState[name.toStdString()].read();

        if (Util::equal_insensitive(state.hash("Status").toString(), "Normal")) {
            tip.append(tr("<li>%1 - %2</li>", "tooltip interface list entry").arg(name,
                                                                                  information.hash("MenuEntry")
                                                                                             .toDisplayString()));
        } else {
            tip.append(tr("<li><font color=\"#FF0000\">%1 - %2</font></li>",
                          "tooltip interface list entry abnormal").arg(name,
                                                                       information.hash("MenuEntry")
                                                                                  .toDisplayString()));
        }
    }
    tip.append(tr("</ul>", "tooltip end"));

    icon.setToolTip(tip);
}

void AcquisitionTray::blinkIcon()
{
    if (haveConnection)
        return;
    if (blinkPolarity) {
        icon.setIcon(redIcon);
        blinkPolarity = false;
    } else {
        icon.setIcon(normalIcon);
        blinkPolarity = true;
    }
}

void AcquisitionTray::showRealtimeGUI()
{
    if (AcquisitionTrayCommandHandler::issueCommand(
            AcquisitionTrayCommandHandler::Command::ShowRealtime))
        return;
    if (spawnRealtimeDelay.isValid() && spawnRealtimeDelay.elapsed() < 5000)
        return;
    spawnRealtimeDelay.start();

    IO::Process::Spawn("cpx3", {"--mode=realtime"}).detach();
}

void AcquisitionTray::verifyRestart()
{
    if (!haveConnection)
        return;
    QMessageBox::StandardButton result = QMessageBox::question(0, tr("Confirm Restart"),
                                                               tr("Restart acquisition system?  This will cause a momentary interruption in data collection while the system re-initializes."),
                                                               QMessageBox::Yes | QMessageBox::No,
                                                               QMessageBox::No);
    if (result != QMessageBox::Yes)
        return;
    connection->restartRequested();
}

void AcquisitionTray::messageLog()
{
    if (!haveConnection)
        return;
    if (messageLogDialog == NULL) {
        messageLogDialog = new RealtimeMessageLog;
        messageLogDialog->setModal(true);
        connect(messageLogDialog, &RealtimeMessageLog::messageLogEvent, this,
                [this](const Variant::Root &message) {
                    if (!connection)
                        return;
                    connection->messageLogEvent(message);
                });
    }

    messageLogDialog->show();
}


int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    Q_INIT_RESOURCE(acquisition_tray);

    app.setQuitOnLastWindowClosed(false);
    app.setWindowIcon(QIcon(":/icon.svg"));

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

    QString translateLocation;
    QByteArray cpd3(qgetenv("CPD3"));
    if (cpd3.length() > 0) {
        translateLocation = QString(cpd3) + "/locale";
    }
    QTranslator cpd3Translator;
    cpd3Translator.load("cpd3_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cpd3Translator);
    QTranslator cliTranslator;
    cliTranslator.load("cli_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cliTranslator);
    QTranslator guiTranslator;
    guiTranslator.load("gui_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&guiTranslator);
    QTranslator acquisitionTranslator;
    acquisitionTranslator.load("acquisition_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&acquisitionTranslator);

    int rc;
    {
        AcquisitionTray tray(app.arguments());
        if (!tray.validConnection()) {
            ThreadPool::system()->wait();
            return 0;
        }
        QObject::connect(&tray, SIGNAL(exitRequested()), &app, SLOT(quit()));

        Abort::installAbortHandler(true);

        AbortPoller abortPoller;
        QObject::connect(&abortPoller, SIGNAL(aborted()), &app, SLOT(quit()));
        abortPoller.start();
        rc = app.exec();
    }
    ThreadPool::system()->wait();
    return rc;
}
