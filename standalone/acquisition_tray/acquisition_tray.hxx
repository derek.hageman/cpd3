/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUISITIONTRAY_H
#define ACQUISITIONTRAY_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QSystemTrayIcon>
#include <QIcon>
#include <QTimer>
#include <QMenu>

#include "acquisition/acquisitionnetwork.hxx"
#include "guidata/realtimemessagelog.hxx"
#include "core/waitutils.hxx"

class AcquisitionTray : public QObject {
Q_OBJECT

    std::unique_ptr<CPD3::Acquisition::AcquisitionNetworkClient> connection;

    QIcon normalIcon;
    QIcon redIcon;
    QSystemTrayIcon icon;

    QTimer blinkTimer;
    bool haveConnection;
    bool blinkPolarity;

    CPD3::ElapsedTimer spawnRealtimeDelay;

    QMenu contextMenu;

    CPD3::GUI::Data::RealtimeMessageLog *messageLogDialog;

    void connectionStateChanged(bool connected);

public:
    AcquisitionTray(const QStringList &arguments);

    virtual ~AcquisitionTray();

    inline bool validConnection() const
    { return connection != NULL; }

signals:

    void exitRequested();

private slots:

    void iconActivated(QSystemTrayIcon::ActivationReason reason);

    void realtimeEvent(const CPD3::Data::Variant::Root &event);

    void blinkIcon();

    void showRealtimeGUI();

    void interfacesUpdated();

    void verifyRestart();

    void messageLog();
};

#endif
