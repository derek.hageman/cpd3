if(Qt5Widgets_FOUND)
    add_definitions(-DBUILD_GUI)
endif(Qt5Widgets_FOUND)
if(NOT UNIX)
    add_definitions(-DNO_CONSOLE)
endif(NOT UNIX)


macro(cpd3_standalone name)
    set(standalone_name "${name}")
    add_executable(${standalone_name} WIN32 ${ARGN})
    target_link_libraries(${standalone_name} cpd3core cpd3datacore Qt5::Core)
    if(Threads_FOUND)
        target_link_libraries(${standalone_name} Threads::Threads)
    endif(Threads_FOUND)
    install(TARGETS ${standalone_name}
            DESTINATION ${BIN_INSTALL_DIR})
    list(APPEND CPD3_EXECUTABLES ${standalone_name})
    set(CPD3_EXECUTABLES "${CPD3_EXECUTABLES}" PARENT_SCOPE)
endmacro(cpd3_standalone)

function(cpd3_standalonetest name source)
    set(test_name "test_${name}")
    add_executable(${test_name} ${source})
    target_link_libraries(${test_name} cpd3core cpd3datacore Qt5::Core Qt5::Test)
    if(Threads_FOUND)
        target_link_libraries(${test_name} Threads::Threads)
    endif(Threads_FOUND)
    target_link_libraries(${test_name} ${ARGN})
    include_directories(${CMAKE_CURRENT_BINARY_DIR})
    add_test(${name} ${test_name})
    set_property(TEST ${name} PROPERTY ENVIRONMENT
                 "CPD3COMPONENTS=${CMAKE_BINARY_DIR}"
                 "PATH=${CMAKE_CURRENT_BINARY_DIR}/../:$ENV{PATH}")
endfunction(cpd3_standalonetest)

add_subdirectory(detached_task)
add_subdirectory(eavesdropper_tap)
add_subdirectory(ioserver)
add_subdirectory(realtime_control)
add_subdirectory(sync_read)
add_subdirectory(sync_write)
add_subdirectory(sync_peer)

if(Qt5Widgets_FOUND)
    add_subdirectory(acquisition_gui)
    add_subdirectory(acquisition_tray)
    add_subdirectory(display_handler)
    add_subdirectory(editor)
    add_subdirectory(initial_setup)
    add_subdirectory(sync_servergui)

    add_subdirectory(widget_test EXCLUDE_FROM_ALL)
endif(Qt5Widgets_FOUND)

if(UNIX)
    add_subdirectory(acquisition_unix)
    add_subdirectory(config_text)
    add_subdirectory(data_progress)
    add_subdirectory(eavesdropper_list)
    add_subdirectory(forge_interface)
    add_subdirectory(multiplex)
    add_subdirectory(output_cpd2)
    add_subdirectory(sync_serverhandler)
    add_subdirectory(sync_serverunix)

    find_package(Curses)
    if(CURSES_FOUND)
        # Some verions of gcc won't link this on their own
        if(UNIX AND CMAKE_SYSTEM_NAME STREQUAL "Linux")
            set(CURSES_LIBRARIES ${CURSES_LIBRARIES} tinfo)
        endif(UNIX AND CMAKE_SYSTEM_NAME STREQUAL "Linux")

        add_subdirectory(acquisition_curses)
    endif(CURSES_FOUND)
endif(UNIX)

if(Fortran_FOUND)
    add_subdirectory(ccn_dmtmodel)
endif(Fortran_FOUND)

set(CPD3_EXECUTABLES "${CPD3_EXECUTABLES}" PARENT_SCOPE)
