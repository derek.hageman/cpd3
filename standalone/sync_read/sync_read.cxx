/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include <QTranslator>

#if !defined(NO_CONSOLE) || !defined(BUILD_GUI)

#include <QCoreApplication>

#else
#include <QApplication>
#endif

#ifdef NO_CONSOLE
#include <QMessageBox>
#endif

#include <QStringList>
#include <QLibraryInfo>
#include <QLocale>
#include <QLoggingCategory>

#include "core/abort.hxx"
#include "core/number.hxx"
#include "core/range.hxx"
#include "core/component.hxx"
#include "core/stdindevice.hxx"
#include "core/threadpool.hxx"
#include "clicore/terminaloutput.hxx"
#include "clicore/optionparse.hxx"
#include "clicore/argumentparser.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/segment.hxx"

#include "sync_read.hxx"


Q_LOGGING_CATEGORY(log_syncread, "cpd3.syncread", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::CLI;
using namespace CPD3::Sync;
using namespace CPD3::Data;

class Parser : public ArgumentParser {
public:
    Parser()
    { }

    virtual ~Parser()
    { }

protected:
    virtual QList<BareWordHelp> getBareWordHelp()
    {
        return QList<BareWordHelp>() << BareWordHelp(tr("station", "station argument name"),
                                                     tr("The station to accept data for"),
                                                     tr("Inferred from the current directory"),
                                                     tr("The \"station\" bare word argument defines the default station used during "
                                                        "data acceptance checking.  This sets both the profile loaded and the "
                                                        "station used for incoming data filtering."))
                                     << BareWordHelp(tr("file", "file argument name"),
#if defined(Q_OS_UNIX)
                                                     tr("The file to read data from or - for standard input"),
#else
                                             tr("The file to read data from"),
#endif
                                                     QString(),
                                                     tr("The \"file\" bare word argument is used to specify the the file to read "
                                                        "synchronization data from."
                                                        #if defined(Q_OS_UNIX)
                                                        "If it is present and exists then data is read from the given file name "
                                                        "instead of from standard input.  Alternatively \"-\" may be used to "
                                                        "explicitly specify standard input."
#endif
                                                     ));
    }

    virtual QString getBareWordCommandLine()
    {
#if defined(Q_OS_UNIX)
        return tr("[station] [file]", "target command line");
#else
        return tr("[station] file", "target command line");
#endif
    }

    virtual QString getProgramName()
    { return tr("cpd3.sync.read", "program name"); }

    virtual QString getDescription()
    {
        return tr("This reads synchronization data and writes the changes it defines to the "
                  "local archive.  It is used in conjunction with da.sync.write to perform "
                  "manual synchronization of data.");
    }

    virtual QHash<QString, QString> getDocumentationTypeID()
    {
        QHash<QString, QString> result;
        result.insert("type", "sync_read");
        return result;
    }
};

int SyncRead::parseArguments(const QStringList &rawArguments)
{

    ComponentOptions options;

    options.add("profile",
                new ComponentOptionSingleString(tr("profile", "name"), tr("Synchronize profile"),
                                                tr("This is the authorization profile to synchronize.  The profile "
                                                   "controls what data are accepted from the input.  "
                                                   "Consult the station configuration and authorization for available profiles."),
                                                tr("aerosol")));

#ifndef NO_CONSOLE
    options.add("quiet",
                new ComponentOptionBoolean(tr("quiet", "quiet option name"), tr("Suppress output"),
                                           tr("If set then no progress output is displayed."),
                                           tr("Disabled")));
#endif

    QStringList arguments(rawArguments);
    if (!arguments.isEmpty())
        arguments.removeFirst();
    Parser parser;
    try {
        parser.parse(arguments, options);
    } catch (ArgumentParsingException ape) {
        QString text(ape.getText());
        if (ape.isError()) {
            if (!text.isEmpty()) {
#ifndef NO_CONSOLE
                TerminalOutput::simpleWordWrap(text, true);
#else
                QMessageBox::critical(0, tr("Error"), text);
#endif
            }
            QCoreApplication::exit(1);
            return -1;
        } else {
            if (!text.isEmpty()) {
#ifndef NO_CONSOLE
                TerminalOutput::simpleWordWrap(text);
#else
                QMessageBox::information(0, tr("Help"), text);
#endif
            }
            QCoreApplication::exit(1);
        }
        return 1;
    }

    Archive::Access archive;
    Archive::Access::ReadLock lock(archive);

    SequenceName::Component station;
    if (!arguments.isEmpty()) {
        auto allStations = archive.availableStations({arguments.front().toStdString()});
        if (!allStations.empty()) {
            station = *allStations.begin();
            arguments.removeFirst();
        }
    }
    if (station.empty()) {
        station = SequenceName::impliedStation(&archive);
    }

    if (arguments.isEmpty()
#if defined(Q_OS_UNIX)
            || arguments.at(0) == tr("-", "stdin file")
#endif
            ) {
#if defined(Q_OS_UNIX)
        inputDevice = new StdinDevice;
        if (!inputDevice->open(QIODevice::ReadOnly)) {
#ifndef NO_CONSOLE
            TerminalOutput::output(
                    tr("Failed to open standard input: %1\n").arg(inputDevice->errorString()),
                    true);
#else
            QMessageBox::critical(0, tr("Error"), tr("Failed to open standard input: %1").arg(
                file->errorString()));
#endif
            QCoreApplication::exit(1);
            return -1;
        }
#else
#ifndef NO_CONSOLE
        TerminalOutput::output(tr("An input file is required.\n"), true);
#else
        QMessageBox::critical(0, tr("Error"), tr("An input file is required."));
#endif
        QCoreApplication::exit(1);
        return -1;
#endif
    } else if (arguments.size() != 1) {
#ifndef NO_CONSOLE
        TerminalOutput::output(tr("Multiple input files are not supported.\n"), true);
#else
        QMessageBox::critical(0, tr("Error"), tr("Multiple input files are not supported."));
#endif
        QCoreApplication::exit(1);
        return -1;
    } else {
        QFile *file = new QFile(arguments.takeFirst());
        Q_ASSERT(file);
        if (!file->open(QIODevice::ReadOnly)) {
#ifndef NO_CONSOLE
            TerminalOutput::output(tr("Failed to open input file: %1\n").arg(file->errorString()),
                                   true);
#else
            QMessageBox::critical(0, tr("Error"), tr("Failed to open input file: %1").arg(
                file->errorString()));
#endif
            QCoreApplication::exit(1);
            delete file;
            return -1;
        }
        inputDevice = file;
    }

    auto authorization = SequenceName::impliedProfile(&archive);
    if (options.isSet("profile")) {
        authorization = qobject_cast<ComponentOptionSingleString *>(options.get("profile"))->get()
                                                                                           .toLower()
                                                                                           .toStdString();
    }

    Archive::Selection::Match selectStations;
    if (!station.empty())
        selectStations.emplace_back(station);

    auto confList = SequenceSegment::Stream::read(
            Archive::Selection(FP::undefined(), FP::undefined(), selectStations, {"configuration"},
                               {"synchronize"}), &archive);

    ValueSegment::Transfer config;
    if (station.empty()) {
        auto allStations = archive.availableStations();
        for (auto seg = confList.rbegin(), endSeg = confList.rend(); seg != endSeg; ++seg) {
            for (const auto &check : allStations) {
                if (check.empty() || check == "_")
                    continue;
                SequenceName configUnit(check, "configuration", "synchronize");
                auto stationConfig = seg->getValue(configUnit);
                if (stationConfig.hash("Authorization").hash(authorization).exists()) {
                    station = check;
                    break;
                }
            }
        }
    }
    if (!station.empty()) {
        SequenceName configUnit(station, "configuration", "synchronize");
        for (auto &seg : confList) {
            config.emplace_back(seg.getStart(), seg.getEnd(), seg.takeValue(configUnit));
        }
    }

    if (station.empty()) {
#ifndef NO_CONSOLE
        TerminalOutput::output(tr("Unable to determine station.\n"), true);
#else
        QMessageBox::critical(0, tr("Error"), tr("Unable to determine station."));
#endif
        QCoreApplication::exit(1);
        return -1;
    }

    QString authPath(QString("Authorization/%1").arg(QString::fromStdString(authorization)));
    config = ValueSegment::withPath(config, authPath);

    peer = new Peer(new BasicFilter(ValueSegment::withPath(config, "Local/Synchronize"), station),
                    new BasicFilter(ValueSegment::withPath(config, "Remote/Synchronize"), station),
                    Peer::Mode_Read);
    Q_ASSERT(peer);

    Q_ASSERT(compressor == NULL);
    compressor = new CompressedStream(inputDevice, this);
    Q_ASSERT(compressor);

#ifndef NO_CONSOLE
    bool supressProgress = false;
    if (options.isSet("quiet")) {
        supressProgress = qobject_cast<ComponentOptionBoolean *>(options.get("quiet"))->get();
    }
    if (!supressProgress) {
        progress.attach(feedback);
        progress.start();
    }
#endif

    return 0;
}

SyncRead::SyncRead() : running(false),
                       inputThrottleTimer(this),
                       inputDevice(NULL),
                       compressor(NULL),
                       peer(NULL)
#ifndef NO_CONSOLE
        ,
                       progress()
#endif
{

    inputThrottleTimer.setSingleShot(true);
    connect(&inputThrottleTimer, SIGNAL(timeout()), this, SLOT(updateDevice()),
            Qt::DirectConnection);
}

SyncRead::~SyncRead()
{
    if (compressor != NULL) {
        compressor->close();
        delete compressor;
    }
    if (inputDevice != NULL) {
        inputDevice->close();
        delete inputDevice;
    }
    if (peer != NULL) {
        delete peer;
    }
}

void SyncRead::start()
{
    Q_ASSERT(peer != NULL);
    Q_ASSERT(inputDevice != NULL);

    connect(compressor, SIGNAL(readyRead()), this, SLOT(updateDevice()), Qt::QueuedConnection);
    connect(compressor, SIGNAL(readChannelFinished()), this, SLOT(updateDevice()),
            Qt::QueuedConnection);

    connect(peer, SIGNAL(errorDetected()), this, SLOT(updateDevice()), Qt::QueuedConnection);
    connect(peer, SIGNAL(finished()), this, SLOT(updateDevice()), Qt::QueuedConnection);
    peer->feedback.forward(feedback);

    peer->start();

    QMetaObject::invokeMethod(this, "updateDevice", Qt::QueuedConnection);

    running = true;
    qCDebug(log_syncread) << "Synchronization read starting";
}

void SyncRead::updateDevice()
{
    if (compressor == NULL || peer == NULL)
        return;

    switch (peer->errorState()) {
    case Peer::Error_None:
        break;
    case Peer::Error_Fatal:
    case Peer::Error_Recoverable:
        abort();
        return;
    }

    qint64 n = compressor->bytesAvailable();
    if (n > 0 && !peer->shouldStallIncoming()) {
        {
            QByteArray bufferRead(compressor->read((int) qMin(Q_INT64_C(65536), n)));
            peer->incomingData(bufferRead);
        }
        if (compressor->bytesAvailable() > 0) {
            QMetaObject::invokeMethod(this, "updateDevice", Qt::QueuedConnection);
            return;
        }
    }
    inputThrottleTimer.start(1000);

    if (!compressor->atEnd() || compressor->bytesAvailable() > 0)
        return;

    if (!peer->transferCompleted()) {
        qCWarning(log_syncread) << "Input ended before peer completion";
    }

    if (!peer->isFinished())
        return;

    delete peer;
    peer = NULL;

    compressor->close();
    compressor->deleteLater();
    compressor = NULL;

    inputDevice->close();
    inputDevice->deleteLater();
    inputDevice = NULL;

    qCDebug(log_syncread) << "Synchronization read completed";

#ifndef NO_CONSOLE
    progress.finished();
#endif

    running = false;
    emit finished();
}

void SyncRead::abort()
{
    if (!running)
        return;

    qCDebug(log_syncread) << "Aborting synchronization read";

#ifndef NO_CONSOLE
    progress.abort();
#endif
    peer->signalTerminate();
    QCoreApplication::exit(1);

    running = false;
    emit finished();
}

int main(int argc, char **argv)
{
#if !defined(NO_CONSOLE) || !defined(BUILD_GUI)
    QCoreApplication app(argc, argv);
#else
    QApplication app(argc, argv, false);
#endif

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

    QString translateLocation;
    QByteArray cpd3(qgetenv("CPD3"));
    if (cpd3.length() > 0) {
        translateLocation = QString(cpd3) + "/locale";
    }
    QTranslator cpd3Translator;
    cpd3Translator.load("cpd3_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cpd3Translator);
    QTranslator cliTranslator;
    cliTranslator.load("cli_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cliTranslator);
    QTranslator sync_readTranslator;
    sync_readTranslator.load("sync_read_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&sync_readTranslator);

    Abort::installAbortHandler(false);
    AbortPoller poller;
    QObject::connect(&poller, SIGNAL(aborted()), &app, SLOT(quit()));

    SyncRead *sync = new SyncRead;
    QObject::connect(sync, SIGNAL(finished()), &app, SLOT(quit()));
    if (int rc = sync->parseArguments(app.arguments())) {
        delete sync;
        ThreadPool::system()->wait();
        if (rc == -1)
            return 1;
        return 0;
    }
    sync->start();
    QObject::connect(&poller, SIGNAL(aborted()), sync, SLOT(abort()));

    poller.start();
    int rc = app.exec();
    poller.disconnect();
    delete sync;
    ThreadPool::system()->wait();
    return rc;
}
