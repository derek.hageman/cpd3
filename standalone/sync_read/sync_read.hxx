/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef SYNCREAD_H
#define SYNCREAD_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QIODevice>
#include <QTimer>
#include <QStringList>

#include "core/compression.hxx"
#include "core/actioncomponent.hxx"
#include "sync/peer.hxx"

#ifndef NO_CONSOLE

#include "clicore/terminaloutput.hxx"

#endif

class SyncRead : public QObject {
Q_OBJECT

    bool running;

    QTimer inputThrottleTimer;
    QIODevice *inputDevice;
    CPD3::CompressedStream *compressor;
    CPD3::Sync::Peer *peer;

#ifndef NO_CONSOLE
    CPD3::CLI::TerminalProgress progress;
#endif

public:
    SyncRead();

    ~SyncRead();

    int parseArguments(const QStringList &rawArguments);

    void start();

    CPD3::ActionFeedback::Source feedback;

public slots:

    void abort();

private slots:

    void updateDevice();

signals:

    void finished();
};

#endif
