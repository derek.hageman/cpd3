/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef DATAPROGRESS_H
#define DATAPROGRESS_H

#include "core/first.hxx"

#include <mutex>
#include <QtGlobal>
#include <QObject>
#include <QList>
#include <QTimer>

#include "datacore/streampipeline.hxx"

class DataProgress : public QObject, public CPD3::Data::StreamSink {
Q_OBJECT

    std::unique_ptr<CPD3::Data::StreamPipeline> pipeline;

    std::mutex mutex;
    double latestTime;
    qint64 count;

    double lastIncomingUpdate;

    double lastRateUpdate;
    qint64 rateCount;
    double rateTime;

    double lastRate;
    double lastSpeed;

    QTimer timer;

    int lastLineLength;

public:
    DataProgress();

    ~DataProgress();

    void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

    void endData() override;

public slots:

    void start();

    void serviceUpdate();

signals:

    void finished();
};

#endif
