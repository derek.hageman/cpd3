/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTranslator>
#include <QCoreApplication>
#include <QStringList>
#include <QLibraryInfo>
#include <QLocale>
#include <QLoggingCategory>

#include "core/abort.hxx"
#include "core/number.hxx"
#include "core/timeutils.hxx"
#include "core/threadpool.hxx"
#include "core/qtcompat.hxx"

#include "data_progress.hxx"


Q_LOGGING_CATEGORY(log_data_progress, "cpd3.progress", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;

DataProgress::DataProgress() : latestTime(FP::undefined()),
                               count(0),
                               lastIncomingUpdate(FP::undefined()),
                               lastRateUpdate(FP::undefined()),
                               rateCount(0),
                               rateTime(FP::undefined()),
                               lastRate(0),
                               lastSpeed(0),
                               timer(this),
                               lastLineLength(0)
{
    bool enablePipelineAcceleration = false;
    if (char *env = getenv("CPD3CLI_ACCELERATION")) {
        if (env[0] && atoi(env))
            enablePipelineAcceleration = true;
    }

    pipeline.reset(new StreamPipeline(enablePipelineAcceleration, false));

    if (!pipeline->setInputPipeline()) {
        qFatal("Unable to set input pipeline: %s",
               pipeline->getInputError().toLatin1().constData());
        return;
    }

    if (!pipeline->setOutputIngress(this)) {
        qFatal("Unable to set output egress: %s",
               pipeline->getOutputError().toLatin1().constData());
        return;
    }

    pipeline->finished.connect(this, std::bind(&DataProgress::finished, this));

    timer.setSingleShot(false);
    connect(&timer, SIGNAL(timeout()), this, SLOT(serviceUpdate()));
    timer.setInterval(250);
}

DataProgress::~DataProgress() = default;

void DataProgress::start()
{
    lastIncomingUpdate = lastRateUpdate = Time::time();

    timer.start();
    pipeline->start();
    QMetaObject::invokeMethod(this, "serviceUpdate", Qt::QueuedConnection);
}

void DataProgress::incomingData(const SequenceValue::Transfer &values)
{
    std::lock_guard<std::mutex> lock(mutex);
    lastIncomingUpdate = Time::time();
    if (values.empty())
        return;
    if (count == 0) {
        qCDebug(log_data_progress) << "First value received with a time of"
                                   << Logging::time(values.front().getStart());
    }
    latestTime = values.back().getStart();
    count += values.size();
}

void DataProgress::endData()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        lastIncomingUpdate = Time::time();
        qCDebug(log_data_progress) << "Ended on time" << Logging::time(latestTime)
                                   << "and a total of" << count << "value(s)";
    }
    QMetaObject::invokeMethod(this, "serviceUpdate", Qt::QueuedConnection);
}

static QString timeOrUnknown(double time)
{
    if (!FP::defined(time))
        return DataProgress::tr("UNKNOWN");
    return Time::toISO8601(time);
}

void DataProgress::serviceUpdate()
{
    if (!pipeline) {
        QString str;
        {
            std::lock_guard<std::mutex> lock(mutex);
            str = tr("\rCompleted at %2 with with %3 (%1) ").arg(count)
                                                            .arg(timeOrUnknown(lastIncomingUpdate),
                                                                 timeOrUnknown(latestTime));
        }

        int len = str.length();
        str = str.leftJustified(lastLineLength, ' ');
        lastLineLength = len;

        fprintf(stdout, "%s", str.toUtf8().constData());
        fflush(stdout);
        return;
    }
    if (pipeline->isFinished()) {
        pipeline.reset();
        QMetaObject::invokeMethod(this, "serviceUpdate", Qt::QueuedConnection);
        return;
    }

    QString str;
    {
        std::lock_guard<std::mutex> lock(mutex);
        double now = Time::time();

        double rateElapsed = now - lastRateUpdate;
        if (rateElapsed >= 1.0) {
            lastRate = (double) (count - rateCount) / rateElapsed;
            if (FP::defined(rateTime) && FP::defined(latestTime))
                lastSpeed = (latestTime - rateTime) / rateElapsed;
            rateCount = count;
            rateTime = latestTime;
            lastRateUpdate = now;
        }

        str = tr("\r%5 (%1) - %3m/s (%4/s) - %2 old ").arg(count)
                                                      .arg(QString::number(now - lastIncomingUpdate,
                                                                           'f', 2))
                                                      .arg(QString::number(lastSpeed / 60.0, 'f',
                                                                           0))
                                                      .arg(QString::number(lastRate, 'f', 0))
                                                      .arg(timeOrUnknown(latestTime));
    }

    int len = str.length();
    str = str.leftJustified(lastLineLength, ' ');
    lastLineLength = len;

    fprintf(stdout, "%s", str.toUtf8().constData());
    fflush(stdout);
}


int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

    QString translateLocation;
    QByteArray cpd3(qgetenv("CPD3"));
    if (cpd3.length() > 0) {
        translateLocation = QString(cpd3) + "/locale";
    }
    QTranslator cpd3Translator;
    cpd3Translator.load("cpd3_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cpd3Translator);
    QTranslator cliTranslator;
    cliTranslator.load("cli_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cliTranslator);
    QTranslator realtimecontrolTranslator;
    realtimecontrolTranslator.load("dataprogress_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&realtimecontrolTranslator);

    Abort::installAbortHandler(false);

    fprintf(stdout, "\n\n");
    fflush(stdout);

    DataProgress progress;
    QObject::connect(&progress, SIGNAL(finished()), &app, SLOT(quit()));
    progress.start();

    AbortPoller poller;
    QObject::connect(&poller, SIGNAL(aborted()), &app, SLOT(quit()));
    poller.start();
    progress.serviceUpdate();
    fprintf(stdout, "\n\n");
    fflush(stdout);
    int rc = app.exec();
    ThreadPool::system()->wait();
    return rc;
}
