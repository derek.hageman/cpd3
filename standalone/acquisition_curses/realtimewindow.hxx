/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef REALTIMEWINDOW_H
#define REALTIMEWINDOW_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QList>

#include "acquisition/realtimelayout.hxx"
#include "windowhandler.hxx"

/**
 * A base class for drawing the result of a realtime layout.
 */
class RealtimeWindow : public WindowHandler {
Q_OBJECT

    CPD3::Acquisition::RealtimeLayout *layout;
    int windowX;
    int windowY;
    int layoutPage;
    int scrollOffset;
    int scrollAmount;

    void adjustWindowPosition(int width, int height);

    void renderEmpty(bool top);

public:
    /**
     * Create a realtime layout drawing window.
     * 
     * @param l     the initial layout
     */
    RealtimeWindow(CPD3::Acquisition::RealtimeLayout *l = NULL);

    virtual ~RealtimeWindow();

    /**
     * Set the current realtime layout to draw.
     * 
     * @param l     the new layout to draw
     */
    inline void setLayout(CPD3::Acquisition::RealtimeLayout *l)
    { layout = l; }

    /**
     * Change the display page.
     */
    void nextDisplay();

    /**
     * Change to a specific page.
     * 
     * @param page  the page to select
     */
    void selectPage(int page);

    virtual void render(bool top);

    virtual bool keypress(int ch);

protected:
    /**
     * Get the base width of the window.  The window will never be thinner
     * than the result of this.  This can be used to reserve space for the
     * "NO COMMS" string, for example.
     * <br>
     * The default implementation returns 0.
     * 
     * @return the minimum width
     */
    virtual int getBaseWidth();

    /**
     * Get the base height of the window.  The window will never be shorter
     * than this.
     * <br>
     * The default implementation returns 0.
     * 
     * @return the minimum height
     */
    virtual int getBaseHeight();

    /**
     * Get the string to draw at the top of the window.
     * <br>
     * The default implementation returns an empty string.
     * 
     * @return  the title string
     */
    virtual QString getWindowTitle();

    /**
     * Called before the actual window draw.  This can be used to override
     * the entire window contents (for example to draw a "NO COMMS" message).
     * <br>
     * The default implementation does nothing and returns false.
     * 
     * @param top       true if the window is the topmost
     * @param width     the calculated total width (including borders)
     * @param height    the calculated total height (including borders)
     * @param windowX   the X position of the top-left corner
     * @param windowY   the Y position of the top-left corner
     * @return          true if the window was draw and nothing further should be done
     */
    virtual bool overrideContents(bool top, int width, int height, int windowX, int windowY);
};

#endif
