/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <unistd.h>
#include <algorithm>
#include <QTranslator>
#include <QLibraryInfo>
#include <QCoreApplication>
#include <QSettings>
#include <QLocale>

#include "acquisition/acquisitionnetwork.hxx"
#include "core/abort.hxx"
#include "core/range.hxx"
#include "core/threadpool.hxx"
#include "core/qtcompat.hxx"
#include "clicore/argumentparser.hxx"
#include "clicore/terminaloutput.hxx"
#include "datacore/segment.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/archive/access.hxx"

#include "acquisition_curses.hxx"
#include "interfacehandler.hxx"
#include "menuhandler.hxx"
#include "windowhandler.hxx"
#include "messagelog.hxx"
#include "eventdisplay.hxx"
#include "genericwindow.hxx"
#include "commandhandler.hxx"
#include "summarywindow.hxx"
#include "systemstatus.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;
using namespace CPD3::CLI;

Client::Client() : renderServiceCounter(0),
                   keyServiceTimer(this),
                   renderTimer(this),
                   connectionConfiguration(Variant::Write::empty()),
                   configuration(),
                   connected(false),
                   realtimeMutex(),
                   incomingRealtime(),
                   realtimeIngress(this),
                   commandManager(),
                   interfaces(),
                   latestRealtime(),
                   genericWindows(),
                   menuDisplay(),
                   commandHandler(NULL),
                   messageLog(NULL),
                   eventDisplay(new EventDisplay),
                   summaryWindow(NULL),
                   systemStatus(NULL),
                   windows()
{
    /* Short timeout since the actual service blocks for up to 50ms */
    keyServiceTimer.setSingleShot(false);
    keyServiceTimer.setInterval(1);
    connect(&keyServiceTimer, SIGNAL(timeout()), this, SLOT(keyUpdate()));
    keyServiceTimer.start();

    /* Relatively slow since this is auto-fired whenever a key is pressed */
    renderTimer.setSingleShot(false);
    renderTimer.setInterval(250);
    connect(&renderTimer, SIGNAL(timeout()), this, SLOT(renderAll()));
    renderTimer.start();

    connect(this, SIGNAL(serviceRealtime()), this, SLOT(processRealtime()), Qt::QueuedConnection);
}

Client::~Client()
{
    qDeleteAll(menuDisplay);
    if (commandHandler != NULL) {
        delete commandHandler;
    }
    if (messageLog != NULL) {
        delete messageLog;
    }
    if (systemStatus != NULL) {
        delete systemStatus;
    }
    if (eventDisplay != NULL) {
        delete eventDisplay;
    }
    connection.reset();
    for (QHash<QString, InterfaceHandler *>::iterator del = interfaces.begin(),
            endDel = interfaces.end(); del != endDel; ++del) {
        delete del.value();
    }
    qDeleteAll(genericWindows);
    qDeleteAll(windows);
}


class Parser : public ArgumentParser {
public:
    Parser()
    { }

    virtual ~Parser()
    { }

protected:
    virtual QList<BareWordHelp> getBareWordHelp()
    {
        return QList<BareWordHelp>() << BareWordHelp(tr("target", "target argument name"),
                                                     tr("The target to launch this server on."),
                                                     QString(),
                                                     tr("The \"target\" bare word argument specifies the target to connect to.  This "
                                                        "interpreted as a host name with an optional port as the second argument."));
    }

    virtual QString getBareWordCommandLine()
    { return tr("[target ...]", "target command line"); }

    virtual QString getProgramName()
    { return tr("cpd3.client", "program name"); }

    virtual QString getDescription()
    {
        return tr("This is the system tray icon for an acquisition system.  It shows the basic "
                  "system state and allows for basic commands to be issued.  If no target is "
                  "given it all connect to the default system on the local machine.");
    }

    virtual QHash<QString, QString> getDocumentationTypeID()
    {
        QHash<QString, QString> result;
        result.insert("type", "acquisition_display");
        return result;
    }
};

int Client::parseArguments(QStringList arguments)
{
    if (!arguments.isEmpty())
        arguments.removeFirst();

    ComponentOptions options;

    options.add("ssl-cert", new ComponentOptionFile(tr("ssl-cert", "name"),
                                                    tr("The server SSL certificate file"),
                                                    tr("This sets the certificate used on the TCP server."),
                                                    QString()));
    options.add("ssl-key",
                new ComponentOptionFile(tr("ssl-key", "name"), tr("The server SSL key file"),
                                        tr("This sets the key used on the TCP server."),
                                        QString()));
    options.add("local", new ComponentOptionBoolean(tr("local", "name"),
                                                    tr("Interpret the target as a local socket name"),
                                                    tr("When enabled the target specification is interpreted "
                                                       "as a local socket name instead of a remote TCP server."),
                                                    tr("Disabled", "local default")));

    options.add("configuration", new DynamicSequenceSelectionOption(tr("configuration", "name"),
                                                                    tr("The configuration for the client"),
                                                                    tr("The variable in the archive used to load the "
                                                                       "configuration of the client.  This defines additional displays "
                                                                       "that are not constructed from the dynamic realtime data. "),
                                                                    QString()));

    Parser parser;
    try {
        parser.parse(arguments, options);
    } catch (ArgumentParsingException ape) {
        QString text(ape.getText());
        if (ape.isError()) {
            if (!text.isEmpty())
                TerminalOutput::simpleWordWrap(text, true);
            QCoreApplication::exit(1);
            return -1;
        } else {
            if (!text.isEmpty())
                TerminalOutput::simpleWordWrap(text);
            QCoreApplication::exit(0);
            return 1;
        }
    }

    {
        Archive::Access archive;
        Archive::Access::ReadLock lock(archive);
        Archive::Selection::Match stations;
        Util::append(SequenceName::impliedStations(&archive), stations);

        std::unique_ptr<DynamicSequenceSelection> match;
        if (options.isSet("configuration")) {
            match.reset((qobject_cast<DynamicSequenceSelectionOption *>(
                    options.get("configuration")))->getOperator());
        } else {
            match.reset(new DynamicSequenceSelection::Match(std::vector<SequenceMatch::Element>{
                    SequenceMatch::Element(QStringList(), {"configuration"}, {"realtime"})}));
        }

        auto selections = match->toArchiveSelections(stations, {"configuration"}, {"realtime"});
        double now = Time::time();
        for (auto sel = selections.begin(); sel != selections.end();) {
            sel->includeMetaArchive = false;
            if (Range::compareStart(now, sel->getStart()) > 0) {
                sel->setStart(now);
                if (Range::compareStartEnd(sel->getStart(), sel->getEnd()) >= 0) {
                    sel = selections.erase(sel);
                    continue;
                }
            }
            ++sel;
        }

        if (!selections.empty()) {
            configuration.clear();

            StreamSink::Iterator incoming;
            archive.readStream(selections, &incoming)->detach();

            ValueSegment::Stream reader;
            while (incoming.hasNext()) {
                auto add = incoming.next();
                if (!match->matches(add, add.getName()))
                    continue;
                Util::append(reader.add(std::move(add)), configuration);
            }
            Util::append(reader.finish(), configuration);
        }
    }

    if (arguments.isEmpty())
        return 0;

    if (options.isSet("ssl-cert")) {
        connectionConfiguration.hash("SSL")
                               .hash("Certificate")
                               .setString(
                                       qobject_cast<ComponentOptionFile *>(options.get("ssl-cert"))
                                               ->get());
    }
    if (options.isSet("ssl-key")) {
        connectionConfiguration.hash("SSL")
                               .hash("Key")
                               .setString(qobject_cast<ComponentOptionFile *>(
                                       options.get("ssl-key"))->get());
    }

    if (options.isSet("local")) {
        if (qobject_cast<ComponentOptionBoolean *>(options.get("local"))->get()) {
            connectionConfiguration.hash("Type").setString("Local");
            connectionConfiguration.hash("Name").setString(arguments.takeFirst());
            return 0;
        }
    }

    connectionConfiguration.hash("Type").setString("Remote");
    connectionConfiguration.hash("Server").setString(arguments.takeFirst());

    if (!arguments.isEmpty()) {
        bool ok = false;
        QString portStr(arguments.takeFirst());
        quint16 port = portStr.toUShort(&ok);
        if (ok && port > 1) {
            connectionConfiguration.hash("ServerPort").setInt64(port);
        } else {
            TerminalOutput::simpleWordWrap(QObject::tr("Port \"%1\" is not valid.").arg(portStr));
            QCoreApplication::exit(1);
            return -1;
        }
    }

    return 0;
}

static bool shouldSpawnTTYCheck()
{
#ifdef Q_OS_UNIX
    int fd = ::fileno(stdin);
    if (fd >= 0)
        return ::isatty(fd);
    return false;
#else
    return false;
#endif
}

static bool isStillATTY()
{
#ifdef Q_OS_UNIX
    int fd = ::fileno(stdin);
    if (fd >= 0)
        return ::isatty(fd);
    return false;
#else
    return true;
#endif
}

void Client::runTTYCheck()
{
    if (isStillATTY())
        return;
    emit exitRequest();
}

void Client::startup()
{
    queueRenderService();

    Range::intersectShift(configuration, Time::time());
    if (!connectionConfiguration.exists()) {
        if (!configuration.empty()) {
            connectionConfiguration =
                    Variant::Root(configuration.front().value().hash("Connection")).write();
        }
    }

    connection.reset(new AcquisitionNetworkClient(connectionConfiguration));
    connection->setRealtimeEgress(&realtimeIngress);

    if (!configuration.empty()) {
        auto generics = configuration.front().value().hash("Windows").toChildren();
        for (auto add = generics.begin(), endAdd = generics.end(); add != endAdd; ++add) {
            QString key = QString::fromStdString(add.stringKey());
            if (key.isEmpty())
                key = QString::number(static_cast<int>(add.integerKey()));
            key.prepend("__GENERIC_");
            genericWindows.append(new GenericWindow(this, add.value(), key));
        }

        if (!configuration.front().value().hash("Summary").hash("Disable").toBool()) {
            summaryWindow = new SummaryWindow(configuration.front().value().hash("Summary"));
            windows.append(summaryWindow);
        }
    } else {
        summaryWindow = new SummaryWindow(Variant::Read::empty());
        windows.append(summaryWindow);
    }

    connection->interfaceInformationUpdated
              .connect(this,
                       std::bind(&Client::interfaceInformationUpdated, this, std::placeholders::_1),
                       true);
    connection->interfaceStateUpdated
              .connect(this, std::bind(&Client::interfaceStateUpdated, this, std::placeholders::_1),
                       true);
    connection->autoprobeStateUpdated
              .connect(this, std::bind(&Client::autoprobeStateUpdated, this), true);
    connection->connectionState
              .connect(this,
                       std::bind(&Client::connectionStateChanged, this, std::placeholders::_1),
                       true);
    connection->realtimeEvent
              .connect(this, std::bind(&Client::realtimeEvent, this, std::placeholders::_1), true);
    connection->realtimeEvent
              .connect(eventDisplay,
                       std::bind(&EventDisplay::realtimeEvent, eventDisplay, std::placeholders::_1),
                       true);

    connection->start();

    if (shouldSpawnTTYCheck()) {
        QTimer *ttyChecker = new QTimer(this);
        ttyChecker->setSingleShot(false);
        ttyChecker->setInterval(1000);
        connect(ttyChecker, SIGNAL(timeout()), this, SLOT(runTTYCheck()));
        ttyChecker->start();
    }
}

void Client::queueRenderService()
{
    if (renderServiceCounter.fetchAndStoreAcquire(1) != 0)
        return;
    QMetaObject::invokeMethod(this, "renderAll", Qt::QueuedConnection);
}

void Client::processRealtime()
{
    SequenceValue::Transfer update;
    {
        std::lock_guard<std::mutex> lock(realtimeMutex);
        if (incomingRealtime.empty())
            return;
        update = std::move(incomingRealtime);
        incomingRealtime.clear();
    }

    commandManager.incomingData(update);

    for (QHash<QString, InterfaceHandler *>::iterator i = interfaces.begin(),
            endI = interfaces.end(); i != endI; ++i) {
        i.value()->incomingRealtime(update);
    }
    for (QList<GenericWindow *>::iterator win = genericWindows.begin(),
            endWin = genericWindows.end(); win != endWin; ++win) {
        (*win)->incomingRealtime(update);
    }

    if (summaryWindow != NULL) {
        summaryWindow->incomingRealtime(update);
    }

    for (const auto &add : update) {
        latestRealtime.insert(add.getUnit(), add);
    }
}

void Client::realtimeUpdated()
{ emit serviceRealtime(); }

Client::RealtimeIngress::RealtimeIngress(Client *p) : parent(p)
{ }

Client::RealtimeIngress::~RealtimeIngress()
{ }

void Client::RealtimeIngress::incomingData(const SequenceValue::Transfer &values)
{
    bool doService = false;
    {
        std::lock_guard<std::mutex> lock(parent->realtimeMutex);
        doService = parent->incomingRealtime.empty();
        Util::append(values, parent->incomingRealtime);
    }
    if (doService)
        parent->realtimeUpdated();
}

void Client::RealtimeIngress::incomingData(SequenceValue::Transfer &&values)
{
    bool doService = false;
    {
        std::lock_guard<std::mutex> lock(parent->realtimeMutex);
        doService = parent->incomingRealtime.empty();
        Util::append(std::move(values), parent->incomingRealtime);
    }
    if (doService)
        parent->realtimeUpdated();
}

void Client::RealtimeIngress::incomingData(const SequenceValue &value)
{
    bool doService = false;
    {
        std::lock_guard<std::mutex> lock(parent->realtimeMutex);
        doService = parent->incomingRealtime.empty();
        parent->incomingRealtime.emplace_back(value);
    }
    if (doService)
        parent->realtimeUpdated();
}

void Client::RealtimeIngress::incomingData(SequenceValue &&value)
{
    bool doService = false;
    {
        std::lock_guard<std::mutex> lock(parent->realtimeMutex);
        doService = parent->incomingRealtime.empty();
        parent->incomingRealtime.emplace_back(std::move(value));
    }
    if (doService)
        parent->realtimeUpdated();
}

void Client::RealtimeIngress::endData()
{ }


InterfaceHandler *Client::createHandler(const Variant::Read &information, const QString &name)
{
    InterfaceHandler *handler = new InterfaceHandler(this, information, name);
    handler->stateUpdated(connection->getInterfaceState(name.toStdString()));
    SequenceValue::Transfer add;
    for (auto v = latestRealtime.constBegin(), endV = latestRealtime.constEnd(); v != endV; ++v) {
        add.emplace_back(v.value());
    }
    handler->incomingRealtime(std::move(add));
    return handler;
}

void Client::registerWindowHandler(WindowHandler *window)
{
    Q_ASSERT(!windows.contains(window));
    windows.append(window);
}

void Client::unregisterWindowHandler(WindowHandler *window)
{
    for (QList<WindowHandler *>::iterator check = windows.begin(), endCheck = windows.end();
            check != endCheck;
            ++check) {
        if (*check != window)
            continue;
        windows.erase(check);
        break;
    }
}

void Client::bringWindowToFront(WindowHandler *window)
{
    unregisterWindowHandler(window);
    registerWindowHandler(window);
}

void Client::interfaceInformationUpdated(const std::string &name)
{
    auto information = connection->getInterfaceInformation(name);
    if (!information.read().exists()) {
        for (QList<MenuHandler *>::const_iterator h = menuDisplay.constBegin(),
                end = menuDisplay.constEnd(); h != end; ++h) {
            (*h)->deleteLater();
        }
        menuDisplay.clear();

        commandManager.updateCommands(QString::fromStdString(name), Variant::Root());
        auto existing = interfaces.find(QString::fromStdString(name));
        if (existing != interfaces.end()) {
            delete existing.value();
            interfaces.erase(existing);
        }
        return;
    }

    commandManager.updateCommands(QString::fromStdString(name),
                                  information.read().hash("Commands"));

    auto existing = interfaces.find(QString::fromStdString(name));
    if (existing != interfaces.end()) {
        if (!existing.value()->informationUpdated(information)) {
            for (QList<MenuHandler *>::const_iterator h = menuDisplay.constBegin(),
                    end = menuDisplay.constEnd(); h != end; ++h) {
                (*h)->deleteLater();
            }
            menuDisplay.clear();
            if (commandHandler != NULL) {
                commandHandler->deleteLater();
                commandHandler = NULL;
            }
            delete existing.value();
            existing.value() = createHandler(information, QString::fromStdString(name));
        }
    } else {
        interfaces.insert(QString::fromStdString(name),
                          createHandler(information, QString::fromStdString(name)));
    }
}

void Client::interfaceStateUpdated(const std::string &name)
{
    auto target = interfaces.find(QString::fromStdString(name));
    if (target == interfaces.end())
        return;
    target.value()->stateUpdated(connection->getInterfaceState(name));
}

void Client::autoprobeStateUpdated()
{
    auto state = connection->getAutoprobeState();
    if (systemStatus)
        systemStatus->updateAutoprobeState(connection->getAutoprobeState());

    renderTimer.start();
    queueRenderService();
}

void Client::connectionStateChanged(bool conn)
{
    connected = conn;

    if (!connected) {
        commandManager.reset();

        for (QList<MenuHandler *>::const_iterator h = menuDisplay.constBegin(),
                end = menuDisplay.constEnd(); h != end; ++h) {
            (*h)->deleteLater();
        }
        menuDisplay.clear();
        if (commandHandler != NULL) {
            commandHandler->deleteLater();
            commandHandler = NULL;
        }

        for (QHash<QString, InterfaceHandler *>::iterator del = interfaces.begin(),
                endDel = interfaces.end(); del != endDel; ++del) {
            delete del.value();
        }
        interfaces.clear();
    }

    renderTimer.start();
    queueRenderService();
}

void Client::realtimeEvent(const CPD3::Data::Variant::Root &)
{
    renderTimer.start();
    queueRenderService();
}

void Client::menuDismissed()
{
    if (menuDisplay.isEmpty())
        return;
    menuDisplay.takeFirst()->deleteLater();
}

void Client::displayMenu(MenuHandler *menu)
{
    menuDisplay.append(menu);
    connect(menu, SIGNAL(dismissed()), this, SLOT(menuDismissed()));
}

void Client::displayCommandHandler(CommandHandler *handler)
{
    if (commandHandler != NULL) {
        commandHandler->deleteLater();
    }
    commandHandler = handler;
}

void Client::messageLogDismissed()
{
    if (messageLog == NULL)
        return;
    messageLog->deleteLater();
    messageLog = NULL;
}

void Client::messageLogEvent(Variant::Root event)
{
    if (!connection)
        return;
    connection->messageLogEvent(std::move(event));
}

void Client::showMessageLogEntry()
{
    if (messageLog != NULL)
        return;
    messageLog = new MessageLog(this);
    connect(messageLog, SIGNAL(dismissed()), this, SLOT(messageLogDismissed()));
}

void Client::systemStatusDismissed()
{
    if (systemStatus == NULL)
        return;
    systemStatus->deleteLater();
    systemStatus = NULL;
}

void Client::showSystemStatus()
{
    if (systemStatus != NULL)
        return;
    systemStatus = new SystemStatus(this);
    if (connection)
        systemStatus->updateAutoprobeState(connection->getAutoprobeState());
    connect(systemStatus, SIGNAL(dismissed()), this, SLOT(systemStatusDismissed()));
}

void Client::issueCommand(const QString &target, Variant::Root command)
{
    if (!connection)
        return;
    connection->issueCommand(target.toStdString(), std::move(command));
}

void initializeCurses();

void cleanupCurses();

int getCursesKey(bool *ok);

void cursesRenderStart();

void cursesRenderEnd();

void Client::keyUpdate()
{
    bool ok = false;
    int ch = getCursesKey(&ok);
    if (!ok)
        return;
    renderTimer.start();
    queueRenderService();

    for (int idx = menuDisplay.size() - 1; idx >= 0; --idx) {
        if (menuDisplay[idx]->keypress(ch))
            return;
    }

    if (commandHandler != NULL && commandHandler->keypress(ch))
        return;

    if (messageLog != NULL && messageLog->keypress(ch))
        return;

    if (systemStatus != NULL && systemStatus->keypress(ch))
        return;

    if (!windows.isEmpty() && windows.last()->keypress(ch))
        return;

    if (eventDisplay != NULL && eventDisplay->keypress(ch))
        return;

    if (backgroundKey(ch))
        return;
}

void Client::renderAll()
{
    renderServiceCounter.fetchAndStoreOrdered(0);

    cursesRenderStart();
    drawStatusLine();

    for (QList<WindowHandler *>::iterator win = windows.begin(), endWin = windows.end(),
            lastWin = windows.end() - 1; win != endWin; ++win) {
        (*win)->render(win == lastWin);
    }

    if (systemStatus != NULL)
        systemStatus->render();

    if (messageLog != NULL)
        messageLog->render();

    if (eventDisplay != NULL)
        eventDisplay->render();

    if (commandHandler != NULL)
        commandHandler->render();

    for (QList<MenuHandler *>::iterator menu = menuDisplay.begin(), endMenu = menuDisplay.end();
            menu != endMenu;
            ++menu) {
        (*menu)->render();
    }

    cursesRenderEnd();
}

bool Client::blinkState()
{
    return QTime::currentTime().msec() < 500;
}

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

    QString translateLocation;
    QByteArray cpd3(qgetenv("CPD3"));
    if (cpd3.length() > 0) {
        translateLocation = QString(cpd3) + "/locale";
    }
    QTranslator cpd3Translator;
    cpd3Translator.load("cpd3_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cpd3Translator);
    QTranslator cliTranslator;
    cliTranslator.load("cli_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cliTranslator);
    QTranslator acquisitionTranslator;
    acquisitionTranslator.load("acquisition_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&acquisitionTranslator);
    QTranslator eavesdropperTranslator;
    eavesdropperTranslator.load("acquisitionclient_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&eavesdropperTranslator);

    qRegisterMetaType<Variant::Read>("CPD3::Data::Variant::Read");
    qRegisterMetaType<Variant::Write>("CPD3::Data::Variant::Write");
    qRegisterMetaType<Variant::Root>("CPD3::Data::Variant::Root");

    int rc;
    {
        Client cl;
        if (int rc = cl.parseArguments(app.arguments())) {
            ThreadPool::system()->wait();
            if (rc == -1)
                rc = 1;
            else
                rc = 0;
            return rc;
        }

        Abort::installAbortHandler(true);

        AbortPoller abortPoller;
        QObject::connect(&abortPoller, SIGNAL(aborted()), &app, SLOT(quit()));
        QObject::connect(&cl, SIGNAL(exitRequest()), &app, SLOT(quit()));

        abortPoller.start();

        Logging::suppressForConsole();

        initializeCurses();
        cl.startup();
        rc = app.exec();
        ThreadPool::system()->wait();
    }
    cleanupCurses();
    return rc;
}


#ifdef Q_OS_LINUX

#include <sys/ioctl.h>

#endif

/* Last because it defines some weird things (like "timeout") */
#include <curses.h>

static void setColors()
{
    if (!has_colors())
        return;
    start_color();
    init_pair(Client::COLOR_WHITE_BLACK, COLOR_WHITE, COLOR_BLACK);
    init_pair(Client::COLOR_RED_BLACK, COLOR_RED, COLOR_BLACK);
    init_pair(Client::COLOR_YELLOW_BLACK, COLOR_YELLOW, COLOR_BLACK);
    init_pair(Client::COLOR_MAGENTA_BLACK, COLOR_MAGENTA, COLOR_BLACK);
    init_pair(Client::COLOR_GREEN_BLACK, COLOR_GREEN, COLOR_BLACK);

    init_pair(Client::COLOR_WHITE_BLUE, COLOR_WHITE, COLOR_BLUE);
    init_pair(Client::COLOR_RED_BLUE, COLOR_RED, COLOR_BLUE);
    init_pair(Client::COLOR_YELLOW_BLUE, COLOR_YELLOW, COLOR_BLUE);
    init_pair(Client::COLOR_RED_WHITE, COLOR_RED, COLOR_WHITE);
}

void initializeCurses()
{
    initscr();
    setColors();
    cbreak();
    keypad(stdscr, TRUE);
    noecho();
    curs_set(0);
    timeout(50);
}

void cleanupCurses()
{
    erase();
    clrtoeol();
    refresh();
    endwin();
}

void terminalResized()
{
    endwin();
    initscr();
    setColors();
    cbreak();
    keypad(stdscr, TRUE);
    noecho();
    curs_set(0);
    timeout(50);
}

int getCursesKey(bool *ok)
{
    timeout(50);
    int ch = getch();
    if (ch == KEY_RESIZE)
        terminalResized();
    *ok = (ch != ERR);
    return ch;
}

void cursesRenderStart()
{
    /* Because curses doesn't always seem to handle this correctly */
#ifdef Q_OS_LINUX
    struct winsize w;
    ioctl(0, TIOCGWINSZ, &w);
    if (w.ws_row != LINES || w.ws_col != COLS)
        terminalResized();
#endif
    erase();
}

void cursesRenderEnd()
{
    doupdate();
    refresh();
}

void Client::drawStatusLine()
{
    if (!connected) {
        wattrset(stdscr, COLOR_PAIR(COLOR_RED_BLACK));
        if (blinkState())
            wattron(stdscr, A_BOLD);
        QString text(tr("NO CONNECTION"));
        mvwaddstr(stdscr, LINES - 1, 0, text.toUtf8().constData());
    } else {
        QHash<QString, InterfaceHandler::StatusLineData> statusLine;
        for (QHash<QString, InterfaceHandler *>::const_iterator interface = interfaces.constBegin(),
                endInterfaces = interfaces.constEnd(); interface != endInterfaces; ++interface) {
            InterfaceHandler::StatusLineData data = interface.value()->getStatusLine();
            if (data.text.isEmpty())
                continue;
            statusLine.insert(data.text, data);
        }
        QList<QString> sorted(statusLine.keys());
        std::sort(sorted.begin(), sorted.end());
        int col = 0;
        for (QList<QString>::const_iterator add = sorted.constBegin(), endAdd = sorted.constEnd();
                add != endAdd;
                ++add, ++col) {
            if (col >= COLS - 1)
                break;
            InterfaceHandler::StatusLineData data(statusLine.value(*add));
            switch (data.status) {
            case AcquisitionInterface::GeneralStatus::Normal:
                if (data.bypassed) {
                    wattrset(stdscr, COLOR_PAIR(COLOR_MAGENTA_BLACK));
                } else if (data.contaminated) {
                    wattrset(stdscr, COLOR_PAIR(COLOR_GREEN_BLACK));
                } else {
                    wattrset(stdscr, A_BOLD | COLOR_PAIR(COLOR_YELLOW_BLACK));
                }
                break;
            case AcquisitionInterface::GeneralStatus::NoCommunications:
                wattrset(stdscr, COLOR_PAIR(COLOR_RED_BLACK));
                if (blinkState())
                    wattron(stdscr, A_BOLD);
                break;
            case AcquisitionInterface::GeneralStatus::Disabled:
                wattrset(stdscr, COLOR_PAIR(COLOR_WHITE_BLACK));
                break;
            }
            mvwaddstr(stdscr, LINES - 1, col, add->toUtf8().constData());
        }
    }


    wattrset(stdscr, COLOR_PAIR(COLOR_WHITE_BLACK));

    double tNow = Time::time();
    QString text(tr("UTC: %1 %2", "status time format").arg(Time::toISO8601(tNow),
                                                            Time::toYearDOY(tNow, QString(),
                                                                            Time::Second, QChar(),
                                                                            Time::DOYOnly)));
    mvwaddstr(stdscr, LINES - 1, COLS - text.length(), text.toUtf8().constData());
}

bool Client::backgroundKey(int key)
{
    switch (key) {
    case '\n':
    case '\r':
    case KEY_ENTER:
        if (menuDisplay.isEmpty()) {
            displayMenu(MenuHandler::createMainMenu(this));
            return true;
        }
        break;

    case '\t':
        if (windows.size() > 1) {
            WindowHandler *win = windows.takeFirst();
            windows.append(win);
            return true;
        }
        break;

    case KEY_BTAB:
        if (windows.size() > 1) {
            WindowHandler *win = windows.takeLast();
            windows.prepend(win);
            return true;
        }
        break;

    default:
        break;
    }
    return false;
}
