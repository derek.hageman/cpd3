/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "datacore/variant/root.hxx"

#include "acquisition_curses.hxx"
#include "menuhandler.hxx"
#include "interfacehandler.hxx"
#include "genericwindow.hxx"
#include "commandhandler.hxx"

/* Last because it defines some weird things (like "timeout") */
#include <curses.h>

using namespace CPD3;
using namespace CPD3::Data;

class MenuHandlerEntry {
public:
    MenuHandlerEntry()
    { }

    virtual ~MenuHandlerEntry()
    { }

    virtual int getSortPriority() const
    { return 0; }

    virtual QString getMenuHotkey() const
    { return QString(); }

    enum DisplayColor {
        ColorDefault, ColorRed,
    };

    virtual DisplayColor getDisplayColor() const
    { return ColorDefault; }

    virtual QString getMenuText() const = 0;

    virtual void selected() = 0;
};

class MenuHandlerMessageLog : public MenuHandlerEntry {
    Client *client;
public:
    MenuHandlerMessageLog(Client *c) : client(c)
    { }

    virtual ~MenuHandlerMessageLog()
    { }

    virtual QString getMenuText() const
    { return MenuHandler::tr("Online log entry"); }

    virtual void selected()
    { client->showMessageLogEntry(); }
};

class MenuHandlerSystemStatus : public MenuHandlerEntry {
    Client *client;
public:
    MenuHandlerSystemStatus(Client *c) : client(c)
    { }

    virtual ~MenuHandlerSystemStatus()
    { }

    virtual QString getMenuText() const
    { return MenuHandler::tr("System status"); }

    virtual void selected()
    { client->showSystemStatus(); }
};

class MenuHandlerInterface : public MenuHandlerEntry {
    InterfaceHandler *interface;
public:
    MenuHandlerInterface(InterfaceHandler *h) : interface(h)
    { }

    virtual QString getMenuHotkey() const
    { return interface->getMainMenuHotkey(); }

    virtual QString getMenuText() const
    { return interface->getMainMenuEntry(); }

    virtual void selected()
    { interface->mainMenuSelected(); }
};

class MenuHandlerInterfaceWindowHide : public MenuHandlerEntry {
    InterfaceHandler *win;
    QString hotkey;
public:
    MenuHandlerInterfaceWindowHide(InterfaceHandler *w, const QString &hk) : win(w), hotkey(hk)
    { }

    virtual QString getMenuHotkey() const
    { return hotkey; }

    virtual QString getMenuText() const
    { return MenuHandler::tr("Hide window"); }

    virtual void selected()
    { win->hideWindow(); }
};

class MenuHandlerInterfaceWindowNextDisplay : public MenuHandlerEntry {
    InterfaceHandler *win;
    QString hotkey;
public:
    MenuHandlerInterfaceWindowNextDisplay(InterfaceHandler *w, const QString &hk) : win(w),
                                                                                    hotkey(hk)
    { }

    virtual QString getMenuHotkey() const
    { return hotkey; }

    virtual QString getMenuText() const
    { return MenuHandler::tr("Next display"); }

    virtual void selected()
    { win->nextDisplay(); }
};

class MenuHandlerInterfaceWindowNextAveraging : public MenuHandlerEntry {
    InterfaceHandler *win;
    QString hotkey;
public:
    MenuHandlerInterfaceWindowNextAveraging(InterfaceHandler *w, const QString &hk = QString())
            : win(w), hotkey(hk)
    { }

    virtual QString getMenuHotkey() const
    { return hotkey; }

    virtual QString getMenuText() const
    { return MenuHandler::tr("Change displayed averaging"); }

    virtual void selected()
    { win->nextAveraging(); }
};

class MenuHandlerGenericWindow : public MenuHandlerEntry {
    GenericWindow *win;
public:
    MenuHandlerGenericWindow(GenericWindow *w) : win(w)
    { }

    virtual QString getMenuHotkey() const
    { return win->getMainMenuHotkey(); }

    virtual QString getMenuText() const
    { return win->getMainMenuEntry(); }

    virtual void selected()
    { win->mainMenuSelected(); }
};

class MenuHandlerGenericWindowHide : public MenuHandlerEntry {
    GenericWindow *win;
    QString hotkey;
public:
    MenuHandlerGenericWindowHide(GenericWindow *w, const QString &hk) : win(w), hotkey(hk)
    { }

    virtual QString getMenuHotkey() const
    { return hotkey; }

    virtual QString getMenuText() const
    { return MenuHandler::tr("Hide window"); }

    virtual void selected()
    { win->hideWindow(); }
};

class MenuHandlerGenericWindowNextDisplay : public MenuHandlerEntry {
    GenericWindow *win;
    QString hotkey;
public:
    MenuHandlerGenericWindowNextDisplay(GenericWindow *w, const QString &hk) : win(w), hotkey(hk)
    { }

    virtual QString getMenuHotkey() const
    { return hotkey; }

    virtual QString getMenuText() const
    { return MenuHandler::tr("Next display"); }

    virtual void selected()
    { win->nextDisplay(); }
};

class MenuHandlerGenericWindowNextAveraging : public MenuHandlerEntry {
    GenericWindow *win;
    QString hotkey;
public:
    MenuHandlerGenericWindowNextAveraging(GenericWindow *w, const QString &hk = QString()) : win(w),
                                                                                             hotkey(hk)
    { }

    virtual QString getMenuHotkey() const
    { return hotkey; }

    virtual QString getMenuText() const
    { return MenuHandler::tr("Change displayed averaging"); }

    virtual void selected()
    { win->nextAveraging(); }
};

class MenuHandlerCommand : public MenuHandlerEntry {
    Client *client;
    QString hotkey;
    QString menuText;
    Variant::Root command;
    QObject *pageChangeTarget;
    int sortPriority;
public:
    MenuHandlerCommand(Variant::Root &&cmd, Client *cl, const QString &hk, QObject *pct = NULL)
            : client(cl),
              hotkey(hk),
              menuText(cmd.read().hash("DisplayName").toDisplayString()),
              command(std::move(cmd)),
              pageChangeTarget(pct),
              sortPriority(0)
    {
        menuText.replace('&', QString());
        if (menuText.isEmpty())
            menuText = command.read().hash("Command").toQString();
        if (menuText.isEmpty())
            menuText = "INVALID COMMAND";

        qint64 i = command.read().hash("SortPriority").toInt64();
        if (INTEGER::defined(i))
            sortPriority = (int) i;
    }

    virtual QString getMenuHotkey() const
    { return hotkey; }

    virtual QString getMenuText() const
    { return menuText; }

    virtual int getSortPriority() const
    { return sortPriority; }

    virtual void selected()
    {
        CommandHandler *clh = new CommandHandler(command, client);
        if (pageChangeTarget != NULL) {
            QObject::connect(clh, SIGNAL(pageChangeRequest(int)), pageChangeTarget,
                             SLOT(pageChangeRequest(int)), Qt::QueuedConnection);
        }
        client->displayCommandHandler(clh);
    }
};

class MenuHandlerCommandInterfaceSelection : public MenuHandlerEntry {
    CommandHandler *command;
    QString menuText;
    QString hotkey;
    QList<QString> targets;
public:
    MenuHandlerCommandInterfaceSelection(const QString &text,
                                         const QString &hk,
                                         const QList<QString> &t,
                                         CommandHandler *cmd) : command(cmd),
                                                                menuText(text),
                                                                hotkey(hk),
                                                                targets(t)
    { }

    virtual QString getMenuHotkey() const
    { return hotkey; }

    virtual QString getMenuText() const
    { return menuText; }

    virtual void selected()
    { command->targetsSelected(targets); }
};

static bool menuHandlerSortCompare(const MenuHandlerEntry *a, const MenuHandlerEntry *b)
{
    int pa = a->getSortPriority();
    int pb = b->getSortPriority();
    if (pa != pb)
        return pa < pb;

    QString sa(a->getMenuHotkey());
    QString sb(b->getMenuHotkey());

    if (sa.isEmpty()) {
        return !sb.isEmpty();
    } else if (sb.isEmpty()) {
        return false;
    }

    return sa < sb;
}

static bool commandPreSortCompare(const Variant::Read &a, const Variant::Read &b)
{
    qint64 pa = a.hash("DisplayPriority").toInt64();
    qint64 pb = b.hash("DisplayPriority").toInt64();
    if (!INTEGER::defined(pa))
        pa = 0;
    if (!INTEGER::defined(pb))
        pb = 0;

    return pa < pb;
}

MenuHandler::MenuHandler() : entries(), selectedIndex(0)
{ }

MenuHandler *MenuHandler::createMainMenu(Client *parent)
{
    MenuHandler *result = new MenuHandler;

    QList<GenericWindow *> genericWindows(parent->getGenericWindows());
    for (QList<GenericWindow *>::const_iterator add = genericWindows.constBegin(),
            endAdd = genericWindows.constEnd(); add != endAdd; ++add) {
        if (!(*add)->hasMainMenuEntry())
            continue;
        result->entries.append(new MenuHandlerGenericWindow(*add));
    }
    QHash<QString, InterfaceHandler *> interfaces(parent->getInterfaces());
    for (QHash<QString, InterfaceHandler *>::const_iterator add = interfaces.constBegin(),
            endAdd = interfaces.constEnd(); add != endAdd; ++add) {
        if (!add.value()->hasMainMenuEntry())
            continue;
        result->entries.append(new MenuHandlerInterface(add.value()));
    }

    auto aggregateCommands = parent->getCommandManager().getAggregateCommands();
    std::stable_sort(aggregateCommands.begin(), aggregateCommands.end(), commandPreSortCompare);
    for (auto &cmd : aggregateCommands) {
        if (!cmd.read().hash("GlobalMenu").toBool())
            continue;
        result->entries.append(new MenuHandlerCommand(std::move(cmd), parent, QString()));
    }

    std::stable_sort(result->entries.begin(), result->entries.end(), menuHandlerSortCompare);
    result->entries.prepend(new MenuHandlerSystemStatus(parent));
    result->entries.prepend(new MenuHandlerMessageLog(parent));

    return result;
}

static const QString selectHotkey(std::unordered_set<QString> &taken,
                                  const Variant::Read &config = Variant::Read::empty())
{
    QString hotkey(config.hash("MenuCharacter").toDisplayString());
    if (!hotkey.isEmpty())
        return hotkey;
    QString selectOrder(MenuHandler::tr("1234567890ABCDEFGHIJKLMNOPQRTUVWXYZ", "hotkey order"));
    for (int idx = 0, max = selectOrder.size(); idx < max; ++idx) {
        hotkey = QString(selectOrder.at(idx));
        if (taken.count(hotkey))
            continue;
        taken.insert(hotkey);
        return hotkey;
    }
    return QString();
}

MenuHandler *MenuHandler::createGenericWindowMenu(GenericWindow *window, Client *client)
{
    MenuHandler *result = new MenuHandler;

    std::unordered_set<QString> takenHotkeys;
    std::vector<Variant::Root> commands;

    for (auto cmd : client->getCommandManager().getManual(window->getName()).read().toHash()) {
        if (cmd.first.empty())
            continue;
        takenHotkeys.insert(cmd.second.hash("MenuCharacter").toDisplayString());
        Variant::Root add(cmd.second);
        if (!add.read().hash("Command").exists())
            add.write().hash("Command").setString(cmd.first);
        commands.emplace_back(std::move(add));
    }

    if (window->showNextDisplay()) {
        takenHotkeys.insert(QString('D'));
        result->entries.append(new MenuHandlerGenericWindowNextDisplay(window, QString('D')));
    }

    if (window->showChangeAveraging()) {
        takenHotkeys.insert(QString('A'));
        result->entries.append(new MenuHandlerGenericWindowNextAveraging(window, QString('A')));
    }

    if (window->showHideWindow()) {
        result->entries
              .append(new MenuHandlerGenericWindowHide(window, selectHotkey(takenHotkeys)));
    }

    std::stable_sort(commands.begin(), commands.end(), commandPreSortCompare);
    for (auto &cmd : commands) {
        auto key = selectHotkey(takenHotkeys, cmd);
        result->entries.append(new MenuHandlerCommand(std::move(cmd), client, key, window));
    }

    std::stable_sort(result->entries.begin(), result->entries.end(), menuHandlerSortCompare);

    return result;
}

MenuHandler *MenuHandler::createInterfaceWindowMenu(InterfaceHandler *interface, Client *client)
{
    MenuHandler *result = new MenuHandler;

    std::unordered_set<QString> takenHotkeys;
    std::vector<Variant::Root> commands;

    for (auto cmd : client->getCommandManager().getCommands(interface->getName()).read().toHash()) {
        if (cmd.first.empty())
            continue;
        takenHotkeys.insert(cmd.second.hash("MenuCharacter").toDisplayString());
        Variant::Root add(cmd.second);
        if (!add.read().hash("Targets").exists())
            add.write().hash("Targets").toArray().after_back().setString(interface->getName());
        if (!add.read().hash("Command").exists())
            add.write().hash("Command").setString(cmd.first);
        commands.emplace_back(std::move(add));
    }

    for (auto &cmd : client->getCommandManager().getAggregateCommands()) {
        bool hit = false;
        for (auto check : cmd.read().hash("Targets").toArray()) {
            if (check.toQString() != interface->getName())
                continue;
            hit = true;
            break;
        }
        if (!hit)
            continue;

        takenHotkeys.insert(cmd.read().hash("MenuCharacter").toDisplayString());
        commands.emplace_back(std::move(cmd));
    }

    if (interface->showNextDisplay()) {
        takenHotkeys.insert(QString('D'));
        result->entries.append(new MenuHandlerInterfaceWindowNextDisplay(interface, QString('D')));
    }

    if (interface->showChangeAveraging()) {
        takenHotkeys.insert(QString('A'));
        result->entries
              .append(new MenuHandlerInterfaceWindowNextAveraging(interface, QString('A')));
    }

    if (interface->showHideWindow()) {
        result->entries
              .append(new MenuHandlerInterfaceWindowHide(interface, selectHotkey(takenHotkeys)));
    }

    std::stable_sort(commands.begin(), commands.end(), commandPreSortCompare);

    std::stable_sort(commands.begin(), commands.end(), commandPreSortCompare);
    for (auto &cmd : commands) {
        auto key = selectHotkey(takenHotkeys, cmd);
        result->entries.append(new MenuHandlerCommand(std::move(cmd), client, key, interface));
    }

    std::stable_sort(result->entries.begin(), result->entries.end(), menuHandlerSortCompare);

    return result;
}

MenuHandler *MenuHandler::createInterfaceSelectionMenu(const QList<QString> &all,
                                                       const QSet<QString> &unique,
                                                       CommandHandler *cmd,
                                                       Client *client)
{
    MenuHandler *result = new MenuHandler;

    std::unordered_set<QString> takenHotkeys;
    QHash<QString, InterfaceHandler *> interfaces(client->getInterfaces());
    for (QSet<QString>::const_iterator interfaceName = unique.constBegin(),
            endInterfaces = unique.constEnd(); interfaceName != endInterfaces; ++interfaceName) {
        QHash<QString, InterfaceHandler *>::const_iterator
                interface = interfaces.constFind(*interfaceName);
        if (interface == interfaces.constEnd()) {
            result->entries
                  .append(new MenuHandlerCommandInterfaceSelection(*interfaceName,
                                                                   selectHotkey(takenHotkeys),
                                                                   QList<QString>()
                                                                           << (*interfaceName),
                                                                   cmd));
            continue;
        }

        takenHotkeys.insert(interface.value()->getMainMenuHotkey());
        result->entries
              .append(new MenuHandlerCommandInterfaceSelection(
                      interface.value()->getMainMenuEntry(), interface.value()->getMainMenuHotkey(),
                      QList<QString>() << (*interfaceName), cmd));
    }

    std::stable_sort(result->entries.begin(), result->entries.end(), menuHandlerSortCompare);
    result->entries
          .prepend(new MenuHandlerCommandInterfaceSelection(tr("Everything", "all interfaces text"),
                                                            selectHotkey(takenHotkeys), all, cmd));

    return result;
}

MenuHandler::~MenuHandler()
{
    qDeleteAll(entries);
}

bool MenuHandler::keypress(int ch)
{
    switch (ch) {
    case KEY_HOME:
        selectedIndex = 0;
        break;

    case KEY_END:
        selectedIndex = entries.size() - 1;
        break;

    case KEY_UP:
        selectedIndex--;
        while (selectedIndex < 0) {
            selectedIndex += entries.size();
        }
        break;

    case KEY_DOWN:
        selectedIndex++;
        while (selectedIndex >= entries.size()) {
            selectedIndex -= entries.size();
        }
        break;

    case KEY_LEFT: {
        int rows;
        int cols;
        calculateDimensions(rows, cols);
        selectedIndex -= rows;
        while (selectedIndex < 0) {
            selectedIndex += rows * cols;
        }
        if (selectedIndex >= entries.size())
            selectedIndex = entries.size() - 1;
        break;
    }

    case KEY_RIGHT: {
        int rows;
        int cols;
        calculateDimensions(rows, cols);
        selectedIndex += rows;
        while (selectedIndex >= entries.size()) {
            selectedIndex -= rows * cols;
        }
        if (selectedIndex < 0)
            selectedIndex = 0;
        break;
    }

    case '\n':
    case '\r':
    case KEY_ENTER:
        entries[selectedIndex]->selected();
        emit dismissed();
        break;

    case 0x001B:
    case 0x0017:
        emit dismissed();
        break;

    default: {
        QChar testChar(ch);
        MenuHandlerEntry *sel = NULL;
        for (QList<MenuHandlerEntry *>::iterator check = entries.begin(), endCheck = entries.end();
                check != endCheck;
                ++check) {
            QString menuString((*check)->getMenuHotkey());
            if (menuString.startsWith(testChar, Qt::CaseInsensitive)) {
                if (menuString.length() == 1) {
                    (*check)->selected();
                    emit dismissed();
                    break;
                }
                if (sel == NULL) {
                    sel = *check;
                }
            }
        }
        if (sel != NULL) {
            sel->selected();
            emit dismissed();
        }
        break;
    }
    }

    return true;
}

void MenuHandler::calculateDimensions(int &rows, int &cols)
{
    int maxRows = qMax(1, LINES - 5);
    for (cols = 1, rows = entries.size(); rows > maxRows;) {
        ++cols;
        div_t result = div(entries.size(), cols);
        rows = result.quot;
        if (result.rem)
            ++rows;
    }
}

struct MenuHandlerColumnData {
    QString text;
    MenuHandlerEntry::DisplayColor color;
};

void MenuHandler::render()
{
    Q_ASSERT(!entries.isEmpty());

    int rows;
    int cols;
    calculateDimensions(rows, cols);

    QList<int> columnWidths;
    QList<QList<MenuHandlerColumnData> > columnContents;
    columnWidths.append(0);
    columnContents.append(QList<MenuHandlerColumnData>());
    int row = 0;
    for (QList<MenuHandlerEntry *>::const_iterator add = entries.constBegin(),
            endAdd = entries.constEnd(); add != endAdd; ++add, ++row) {
        if (row >= rows) {
            columnWidths.append(0);
            columnContents.append(QList<MenuHandlerColumnData>());
            row = 0;
        }

        QString text((*add)->getMenuHotkey());
        if (text.isEmpty())
            text = QChar(' ');
        text = tr("%1 %2", "menu entry text").arg(text, (*add)->getMenuText());
        columnWidths.last() = qMax(columnWidths.last(), text.length());
        columnContents.last().append(MenuHandlerColumnData());
        columnContents.last().last().text = text;
        columnContents.last().last().color = (*add)->getDisplayColor();
    }
    for (int i = 0, max = columnWidths.size() - 1; i < max; i++) {
        columnWidths[i] += 1;
    }

    int totalWidth = 0;
    for (QList<int>::const_iterator add = columnWidths.constBegin(),
            endAdd = columnWidths.constEnd(); add != endAdd; ++add) {
        totalWidth += *add;
    }

    int selectedColumn = selectedIndex / rows;

    int windowStartColumn;
    int windowWidth;
    int firstColumnOffset;
    if (totalWidth + 4 >= COLS) {
        windowStartColumn = 1;
        windowWidth = COLS - 2;

        firstColumnOffset = (COLS - columnWidths.at(selectedColumn)) / 2;
        int lastColumnEnd = firstColumnOffset + columnWidths.at(selectedColumn);
        for (int addCol = selectedColumn - 1; addCol >= 0; --addCol) {
            firstColumnOffset -= columnWidths.at(addCol);
        }
        for (int addCol = selectedColumn + 1, max = columnWidths.size(); addCol < max; ++addCol) {
            lastColumnEnd += columnWidths.at(addCol);
        }

        if (lastColumnEnd < COLS - 2)
            firstColumnOffset += (COLS - 2) - lastColumnEnd;
        if (firstColumnOffset > 2)
            firstColumnOffset = 2;
    } else {
        windowWidth = totalWidth + 2;
        windowStartColumn = (COLS - windowWidth) / 2;
        firstColumnOffset = windowStartColumn + 1;
    }

    WINDOW *win = subwin(stdscr, rows + 2, windowWidth, (LINES - rows) / 2, windowStartColumn);
    if (win == NULL)
        return;
    wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    werase(win);

    row = 0;
    for (int col = 0, index = 0, startColumn = firstColumnOffset;; ++row, ++index) {
        if (row >= rows) {
            row = 0;
            startColumn += columnWidths.at(col);
            ++col;
            if (col >= cols)
                break;
        }
        if (row >= columnContents.at(col).size()) {
            wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
            for (int effectiveCol = startColumn - windowStartColumn,
                    endCol = effectiveCol + columnWidths.at(col);
                    effectiveCol < endCol;
                    ++effectiveCol) {
                mvwaddch(win, row + 1, effectiveCol, ' ');
            }
            continue;
        }

        switch (columnContents.at(col).at(row).color) {
        case MenuHandlerEntry::ColorDefault:
            if (index == selectedIndex) {
                wattrset(win, A_REVERSE | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
            } else {
                wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
            }
            break;
        case MenuHandlerEntry::ColorRed:
            if (index == selectedIndex) {
                wattrset(win, COLOR_PAIR(Client::COLOR_RED_WHITE));
            } else {
                wattrset(win, COLOR_PAIR(Client::COLOR_RED_BLUE));
            }
            break;
        }


        int effectiveCol = startColumn - windowStartColumn;

        QString text;
        if (row < columnContents.at(col).size())
            text = columnContents.at(col).at(row).text;
        text = text.leftJustified(columnWidths.at(col));

        if (effectiveCol > 0 && effectiveCol + text.length() < windowWidth - 1) {
            mvwaddstr(win, row + 1, effectiveCol, text.toUtf8().constData());
            continue;
        }

        for (int textIndex = 0, max = text.length(); textIndex < max; ++textIndex, ++effectiveCol) {
            if (effectiveCol < 1)
                continue;
            if (effectiveCol >= windowWidth - 1)
                break;
            mvwaddch(win, row + 1, effectiveCol, text.at(textIndex).unicode());
        }
    }


    wattrset(win, A_BOLD | COLOR_PAIR(Client::COLOR_YELLOW_BLUE));
    box(win, 0, 0);
    wnoutrefresh(win);
    delwin(win);
}
