/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "datacore/variant/root.hxx"

#include "acquisition_curses.hxx"
#include "eventdisplay.hxx"

/* Last because it defines some weird things (like "timeout") */
#include <curses.h>

using namespace CPD3;
using namespace CPD3::Data;

EventDisplay::EventDisplay() : events(), hideTime(Time::time() - 1.0)
{ }

EventDisplay::~EventDisplay()
{ }

void EventDisplay::realtimeEvent(Variant::Root event)
{
    if (event.read().hash("Text").toDisplayString().isEmpty())
        return;
    events.emplace_back(std::move(event));

    int nMax = qMax(40, COLS * 2);
    if (static_cast<int>(events.size()) > nMax) {
        int nErase = static_cast<int>(events.size()) - nMax;
        events.erase(events.begin(), events.begin() + nErase);
    }

    if (FP::defined(hideTime))
        hideTime = Time::time() + 5.0;
}

bool EventDisplay::keypress(int ch)
{
    switch (ch) {
    case 0x05:  //CTRL-E
        if (FP::defined(hideTime)) {
            hideTime = FP::undefined();
        } else {
            hideTime = Time::time() - 1.0;
        }
        return true;

    default:
        break;
    }

    return false;
}

void EventDisplay::render()
{
    if (FP::defined(hideTime) && hideTime <= Time::time())
        return;

    int height = LINES / 3;
    if (height < 3)
        height = 3;

    WINDOW *win = subwin(stdscr, height, COLS, LINES - height - 1, 0);
    if (win == NULL)
        return;
    wbkgdset(win, ' ' | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    werase(win);

    for (int idx = static_cast<int>(events.size()) - 1, row = height - 2;
            idx >= 0 && row > 0;
            idx--, row--) {
        auto event = events[idx].read();
        double time = event.hash("Time").toDouble();
        if (!FP::defined(time))
            time = Time::time();

        QString text(tr("%1 %2: %3").arg(event.hash("Source").toDisplayString().leftJustified(3),
                                         Time::toISO8601(time),
                                         event.hash("Text").toDisplayString()));
        text.truncate(COLS - 2);

        mvwaddstr(win, row, 1, text.toUtf8().constData());
    }

    wattrset(win, A_BOLD | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    box(win, 0, 0);
    QString text(tr("Events"));
    mvwaddstr(win, 0, (COLS - text.length()) / 2, text.toUtf8().constData());
    wnoutrefresh(win);
    delwin(win);
}
