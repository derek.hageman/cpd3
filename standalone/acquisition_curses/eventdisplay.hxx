/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef EVENTDISPLAY_H
#define EVENTDISPLAY_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QList>

#include "datacore/stream.hxx"

/**
 * The display for realtime events.
 */
class EventDisplay : public QObject {
Q_OBJECT

    std::vector<CPD3::Data::Variant::Root> events;

    double hideTime;

public:
    EventDisplay();

    ~EventDisplay();

    /**
     * Handle a keypress.
     * 
     * @param ch    the key
     * @return true if the key was used
     */
    bool keypress(int ch);

    /**
     * Render the display.
     */
    void render();

public slots:

    /**
     * Handle an incoming realtime event.
     * 
     * @param event the event
     */
    void realtimeEvent(CPD3::Data::Variant::Root event);
};

#endif
