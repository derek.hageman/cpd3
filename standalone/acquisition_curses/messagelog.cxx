/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "datacore/variant/root.hxx"
#include "guicore/guicore.hxx"

#include "acquisition_curses.hxx"
#include "messagelog.hxx"

/* Last because it defines some weird things (like "timeout") */
#include <curses.h>

using namespace CPD3::Data;

MessageLog::MessageLog(Client *p) : QObject(p),
                                    parent(p),
                                    settings(CPD3GUI_ORGANIZATION, CPD3GUI_APPLICATION),
                                    text(),
                                    author(),
                                    selected(EditingText),
                                    editingCursor(0)
{
    author = settings.value("user/initials").toString();
}

MessageLog::~MessageLog()
{ }

static QList<QString> generateWordWrap(const QString &input, int lineLength)
{
    QStringList words(input.split(QRegExp("\\s")));
    QList<QString> result;
    if (words.isEmpty())
        return result;

    if (lineLength < 1)
        lineLength = 1;

    QString line;
    for (QStringList::const_iterator add = words.constBegin(), endAdd = words.constEnd() - 1;
            add != endAdd;
            ++add) {
        if (line.length() + 1 + add->length() <= lineLength) {
            line.append(*add);
            line.append(' ');
            continue;
        }

        if (add->length() + 1 <= lineLength) {
            result.append(line);
            line.clear();
            line.append(*add);
            line.append(' ');
            continue;
        }

        QString remaining(*add);
        remaining.append(' ');
        while (!remaining.isEmpty()) {
            int available = lineLength - line.length();
            line.append(remaining.mid(0, available));
            remaining = remaining.mid(available);

            result.append(line);
            line.clear();
        }
    }

    if (line.length() + words.last().length() <= lineLength) {
        line.append(words.last());
        result.append(line);
        return result;
    }

    if (words.last().length() <= lineLength) {
        if (!line.isEmpty())
            result.append(line);
        result.append(words.last());
        return result;
    }

    QString remaining(words.last());
    while (!remaining.isEmpty()) {
        int available = lineLength - line.length();
        line.append(remaining.mid(0, available));
        remaining = remaining.mid(available);

        result.append(line);
        line.clear();
    }

    if (!line.isEmpty())
        result.append(line);
    return result;
}

static int getWindowWidth()
{
    int width = (COLS * 2) / 3;
    if (width < 1)
        width = 1;
    if (width > COLS - 2)
        width = COLS - 2;
    return width;
}

bool MessageLog::keypress(int ch)
{
    switch (ch) {
    case KEY_UP:
        switch (selected) {
        case EditingText: {
            if (editingCursor == 0)
                break;
            QList<QString> textLines(generateWordWrap(text, getWindowWidth()));
            int textOffset = 0;
            int previousOffset = 0;
            for (QList<QString>::const_iterator add = textLines.constBegin(),
                    endAdd = textLines.constEnd(); add != endAdd; ++add) {
                int nextOffset = textOffset + add->length();
                if (nextOffset >= editingCursor) {
                    int startOffset = editingCursor - textOffset;
                    editingCursor = previousOffset;
                    editingCursor += startOffset;
                    editingCursor = qMin(editingCursor, textOffset - 1);
                    if (editingCursor < 0)
                        editingCursor = 0;
                    return true;
                }
                previousOffset = textOffset;
                textOffset = nextOffset;
            }
            editingCursor = 0;
            break;
        }
        case EditingAuthor:
            selected = EditingText;
            break;
        }
        return true;

    case KEY_DOWN:
        switch (selected) {
        case EditingText: {
            if (editingCursor == text.size())
                break;
            QList<QString> textLines(generateWordWrap(text, getWindowWidth()));
            int textOffset = 0;
            for (QList<QString>::const_iterator add = textLines.constBegin(),
                    endAdd = textLines.constEnd(); add != endAdd; ++add) {
                int nextOffset = textOffset + add->length();
                if (nextOffset >= editingCursor) {
                    int startOffset = editingCursor - textOffset;
                    editingCursor = nextOffset;
                    editingCursor += startOffset;
                    ++add;
                    if (add != endAdd) {
                        int lineEnd = nextOffset + add->length();
                        editingCursor = qMin(editingCursor, lineEnd - 1);
                    }
                    if (editingCursor > text.size())
                        editingCursor = text.size();
                    return true;
                }
                textOffset = nextOffset;
            }
            editingCursor = text.size();
            break;
        }
        case EditingAuthor:
            selected = EditingText;
            break;
        }
        return true;

    case KEY_END:
        switch (selected) {
        case EditingText:
            editingCursor = text.size();
            break;
        case EditingAuthor:
            editingCursor = author.size();
            break;
        }
        return true;

    case KEY_HOME:
        switch (selected) {
        case EditingText:
            editingCursor = 0;
            break;
        case EditingAuthor:
            editingCursor = 0;
            break;
        }
        return true;

    case KEY_LEFT:
        switch (selected) {
        case EditingText:
            editingCursor--;
            if (editingCursor < 0)
                editingCursor = 0;
            break;
        case EditingAuthor:
            editingCursor--;
            if (editingCursor < 0)
                editingCursor = 0;
            break;
        }
        return true;

    case KEY_RIGHT:
        switch (selected) {
        case EditingText:
            editingCursor++;
            if (editingCursor > text.size())
                editingCursor = text.size();
            break;
        case EditingAuthor:
            editingCursor++;
            if (editingCursor > author.size())
                editingCursor = author.size();
            break;
        }
        return true;

    case KEY_BACKSPACE:
        switch (selected) {
        case EditingText:
            if (editingCursor == 0 || text.isEmpty())
                break;
            text.remove(editingCursor - 1, 1);
            --editingCursor;
            break;
        case EditingAuthor:
            if (editingCursor == 0 || author.isEmpty())
                break;
            author.remove(editingCursor - 1, 1);
            --editingCursor;
            break;
        }
        return true;

    case KEY_DC:
        switch (selected) {
        case EditingText:
            if (text.isEmpty() || editingCursor == text.size())
                break;
            text.remove(editingCursor, 1);
            break;
        case EditingAuthor:
            if (author.isEmpty() || editingCursor == author.size())
                break;
            author.remove(editingCursor, 1);
            break;
        }
        return true;

    case '\t':
    case KEY_BTAB:
        switch (selected) {
        case EditingText:
            selected = EditingAuthor;
            editingCursor = author.size();
            break;
        case EditingAuthor:
            selected = EditingText;
            editingCursor = text.size();
            break;
        }
        return true;

    case 0x001B:
    case 0x0017:
        emit dismissed();
        return true;

    case '\n':
    case '\r':
    case KEY_ENTER: {
        if (author.isEmpty() || text.isEmpty())
            return true;
        settings.setValue("user/initials", QVariant(author));

        Variant::Root event;
        event["Author"].setString(author);
        event["Text"].setString(text);
        parent->messageLogEvent(std::move(event));

        emit dismissed();
        return true;
    }

    default: {
        if (!isprint(ch))
            return false;
        QChar add(ch);
        if (!add.isPrint())
            return false;
        switch (selected) {
        case EditingText:
            text.insert(editingCursor, add);
            ++editingCursor;
            return true;
        case EditingAuthor:
            author.insert(editingCursor, add);
            ++editingCursor;
            return true;
        }
        break;
    }
    }

    return false;
}

void MessageLog::render()
{
    int width = getWindowWidth();
    int height = (LINES * 1) / 3;
    if (height < 5)
        height = 5;
    if (height > LINES - 2)
        height = LINES - 2;

    WINDOW *win = subwin(stdscr, height + 2, width + 2, (LINES - (height + 2)) / 2,
                         (COLS - (width + 2)) / 2);
    if (win == NULL)
        return;
    wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    wbkgdset(win, ' ' | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    werase(win);

    QList<QString> textLines;
    if (editingCursor == text.length()) {
        textLines = generateWordWrap(text + ' ', width);
    } else {
        textLines = generateWordWrap(text, width);
    }
    if (selected != EditingText) {
        int row = 1;
        wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
        for (QList<QString>::const_iterator add = textLines.constBegin(),
                endAdd = textLines.constEnd(); add != endAdd; ++add, ++row) {
            if (row > height - 4)
                break;
            mvwaddstr(win, row, 1, add->toUtf8().constData());
        }
    } else {
        int cursorRow;
        if (editingCursor == text.length()) {
            cursorRow = textLines.size() - 1;
        } else {
            cursorRow = 0;
            int offset = 0;
            for (QList<QString>::const_iterator add = textLines.constBegin(),
                    endAdd = textLines.constEnd(); add != endAdd; ++add, ++cursorRow) {
                offset += add->length();
                if (offset >= editingCursor)
                    break;
            }
        }

        int rowOffset = (height - 4) - cursorRow;
        if (rowOffset > 0)
            rowOffset = 0;
        int textOffset = 0;
        for (int textRow = 0, maxRow = textLines.size();
                textRow < maxRow;
                textOffset += textLines.at(textRow).length(), ++textRow) {
            int row = rowOffset + textRow + 1;
            if (row < 1)
                continue;
            if (row > height - 3)
                break;

            QString line(textLines.at(textRow));
            if (editingCursor < textOffset || editingCursor > textOffset + line.length()) {
                wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
                mvwaddstr(win, row, 1, line.toUtf8().constData());
                continue;
            }

            for (int idx = 0, max = line.length(); idx < max; ++idx) {
                int textIndex = textOffset + idx;
                if (textIndex == editingCursor) {
                    wattrset(win, A_REVERSE | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
                } else {
                    wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
                }

                mvwaddch(win, row, idx + 1, line.at(idx).unicode());
            }
        }
    }

    QString text(tr("Author (initials): "));
    int textLength = text.length();
    if (selected != EditingAuthor) {
        wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
        mvwaddstr(win, height - 1, 1, text.toUtf8().constData());
        text = author;
        text.truncate(width - textLength);
        mvwaddstr(win, height - 1, textLength + 1, text.toUtf8().constData());
    } else {
        wattrset(win, A_BOLD | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
        mvwaddstr(win, height - 1, 1, text.toUtf8().constData());

        int textOffset = width - (textLength + editingCursor + 1);
        if (textOffset > 0)
            textOffset = 0;
        int nChr = author.length();
        for (int idx = 0; idx < nChr; ++idx, ++textOffset) {
            if (textOffset < 0)
                continue;
            int xPos = textLength + textOffset;
            if (xPos > width)
                break;

            if (idx == editingCursor)
                wattrset(win, A_REVERSE | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
            else
                wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));

            mvwaddch(win, height - 1, xPos + 1, author.at(idx).unicode());
        }
        if (nChr == editingCursor) {
            int xPos = textLength + textOffset + 1;
            if (xPos > 0 && xPos <= width) {
                wattrset(win, A_REVERSE | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
                mvwaddch(win, height - 1, xPos, ' ');
            }
        }
    }

    wattrset(win, A_REVERSE | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    text = tr("ENTER to accept, TAB to set author, ESC to abort");
    text = text.leftJustified((width + text.length()) / 2);
    text = text.rightJustified(width);
    mvwaddstr(win, height, 1, text.toUtf8().constData());

    wattrset(win, A_BOLD | COLOR_PAIR(Client::COLOR_YELLOW_BLUE));
    box(win, 0, 0);

    text = tr("Message Log Entry");
    mvwaddstr(win, 0, ((width + 2) - text.length()) / 2, text.toUtf8().constData());

    mvwhline(win, height - 2, 1, ACS_HLINE, width);
    mvwaddch(win, height - 2, 0, ACS_LTEE);
    mvwaddch(win, height - 2, width + 1, ACS_RTEE);
    wnoutrefresh(win);
    delwin(win);
}
