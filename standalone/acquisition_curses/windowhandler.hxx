/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef WINDOWHANDLER_H
#define WINDOWHANDLER_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

/**
 * A class for handling the various re-orderable windows in the main
 * display.
 */
class WindowHandler : public QObject {
Q_OBJECT

public:
    WindowHandler();

    virtual ~WindowHandler();

    /**
     * Called when the topmost window receives a key press.
     * 
     * @param ch    the key
     * @return      true if the key was consumed
     */
    virtual bool keypress(int ch);

    /**
     * Draw the window.
     * 
     * @param top   true if this window is the topmost
     */
    virtual void render(bool top);
};

#endif
