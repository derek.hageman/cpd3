/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QSettings>

#include "datacore/variant/root.hxx"

#include "genericwindow.hxx"
#include "windowhandler.hxx"
#include "realtimewindow.hxx"
#include "menuhandler.hxx"
#include "acquisition_curses.hxx"

/* Last because it defines some weird things (like "timeout") */

using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class GenericDisplayWindow : public RealtimeWindow {
    GenericWindow *parent;

    enum {
        Instant, Boxcar, Average
    } layout;
public:
    GenericDisplayWindow(GenericWindow *p) : parent(p), layout(Instant)
    {
        if (parent->layoutInstant.pages() != 0) {
            layout = Instant;
            setLayout(&parent->layoutInstant);
        } else if (parent->layoutBoxcar.pages() != 0) {
            layout = Boxcar;
            setLayout(&parent->layoutBoxcar);
        } else {
            layout = Average;
            setLayout(&parent->layoutAverage);
        }
    }

    virtual ~GenericDisplayWindow()
    { }

    void nextAveraging()
    {
        switch (layout) {
        case Instant:
            setLayout(&parent->layoutBoxcar);
            layout = Boxcar;
            if (parent->layoutBoxcar.pages() == 0) {
                setLayout(&parent->layoutAverage);
                layout = Average;
                if (parent->layoutAverage.pages() == 0) {
                    setLayout(&parent->layoutInstant);
                    layout = Instant;
                }
            }
            break;
        case Boxcar:
            setLayout(&parent->layoutAverage);
            layout = Average;
            if (parent->layoutAverage.pages() == 0) {
                setLayout(&parent->layoutInstant);
                layout = Instant;
                if (parent->layoutInstant.pages() == 0) {
                    setLayout(&parent->layoutBoxcar);
                    layout = Boxcar;
                }
            }
            break;
        case Average:
            setLayout(&parent->layoutInstant);
            layout = Instant;
            if (parent->layoutInstant.pages() == 0) {
                setLayout(&parent->layoutBoxcar);
                layout = Boxcar;
                if (parent->layoutBoxcar.pages() == 0) {
                    setLayout(&parent->layoutAverage);
                    layout = Average;
                }
            }
            break;
        }
    }

    virtual bool keypress(int ch)
    {
        if (RealtimeWindow::keypress(ch))
            return true;
        switch (ch) {
        case 'm':
        case 'M':
            parent->showCommandMenu();
            return true;

        case 'a':
        case 'A':
            nextAveraging();
            return true;

        case 0x001B:
        case 0x0017:
            if (!parent->showHideWindow())
                break;
            parent->hideWindow();
            return true;

        default:
            break;
        }
        return false;
    }

protected:
    virtual QString getWindowTitle()
    {
        QString title(parent->windowTitle);
        switch (layout) {
        case Instant:
            break;
        case Boxcar:
            title.append(GenericWindow::tr(" - Boxcar"));
            break;
        case Average:
            title.append(GenericWindow::tr(" - Average"));
            break;
        }
        return title;
    }
};

GenericWindow::InputSelection GenericWindow::generateInputSelection(const SequenceName::Component &defaultArchive,
                                                                    const SequenceName::Component &defaultVariable,
                                                                    const Variant::Read &config)
{
    InputSelection result;

    auto defaultStation = SequenceName::impliedUncheckedStation();

    result.selection =
            SequenceMatch::OrderedLookup(config, {}, {QString::fromStdString(defaultArchive)},
                                         {QString::fromStdString(defaultVariable)});
    result.selection.registerExpected(defaultStation, defaultArchive, defaultVariable);
    result.selection.registerExpected(defaultStation, defaultArchive);
    result.selection.registerExpected(defaultStation);
    result.selection.registerExpected();
    result.metadata = Variant::Root(config.hash("Metadata"));

    return result;
}

void GenericWindow::generateInputSelections(std::vector<InputSelection> &target,
                                            const SequenceName::Component &defaultArchive,
                                            const Variant::Read &config)
{
    auto children = config.toChildren();
    for (auto add = children.begin(), endAdd = children.end(); add != endAdd; ++add) {
        target.emplace_back(generateInputSelection(defaultArchive, add.stringKey(), add.value()));
    }
}

void GenericWindow::registerAllExisting(QSet<SequenceName> &seen,
                                        std::vector<InputSelection> &selections,
                                        RealtimeLayout &layout)
{
    for (const auto &check : selections) {
        for (const auto &add : check.selection.knownInputs()) {
            if (seen.contains(add))
                continue;
            seen.insert(add);
            if (add.isMeta())
                continue;

            layout.incomingData(SequenceValue(SequenceIdentity(add.toMeta()), check.metadata));
        }
    }
}

GenericWindow::GenericWindow(Client *c, const Variant::Read &config, const QString &n) : QObject(c),
                                                                                         parent(c),
                                                                                         name(n),
                                                                                         layoutInstant(
                                                                                                 true),
                                                                                         layoutBoxcar(
                                                                                                 true),
                                                                                         layoutAverage(
                                                                                                 true),
                                                                                         window(NULL),
                                                                                         menuTitle(
                                                                                                 config.hash("MenuEntry")
                                                                                                       .toDisplayString()),
                                                                                         menuHotkey(
                                                                                                 config.hash("MenuCharacter")
                                                                                                       .toDisplayString()),
                                                                                         windowTitle(
                                                                                                 config.hash("WindowTitle")
                                                                                                       .toDisplayString()),
                                                                                         inputSelectionsInstant(),
                                                                                         inputSelectionsBoxcar(),
                                                                                         inputSelectionsAverage(),
                                                                                         seenInstant(),
                                                                                         seenBoxcar(),
                                                                                         seenAverage(),
                                                                                         showOnUpdate(
                                                                                                 config.hash(
                                                                                                         "ShowOnUpdate")),
                                                                                         showOnUpdateRequire(
                                                                                                 config.hash("ShowOnUpdate")
                                                                                                       .hash("Require")),
                                                                                         showOnUpdateExclude(
                                                                                                 config.hash("ShowOnUpdate")
                                                                                                       .hash("Exclude")),
                                                                                         hideMenuEntry(
                                                                                                 config.hash("MenuHide")
                                                                                                       .toBool())
{
    if (windowTitle.isEmpty())
        windowTitle = menuTitle;

    generateInputSelections(inputSelectionsInstant, "rt_instant",
                            config.hash("Window").hash("Instant"));
    generateInputSelections(inputSelectionsBoxcar, "rt_boxcar",
                            config.hash("Window").hash("Boxcar"));
    generateInputSelections(inputSelectionsAverage, "raw", config.hash("Window").hash("Average"));

    registerAllExisting(seenInstant, inputSelectionsInstant, layoutInstant);
    registerAllExisting(seenBoxcar, inputSelectionsBoxcar, layoutBoxcar);
    registerAllExisting(seenAverage, inputSelectionsAverage, layoutAverage);

    parent->getCommandManager().updateManual(name, config.hash("Commands"));
}

GenericWindow::~GenericWindow()
{
    if (window != NULL) {
        parent->unregisterWindowHandler(window);
        window->deleteLater();
    }
}

QString GenericWindow::getMainMenuEntry() const
{ return menuTitle; }

QString GenericWindow::getMainMenuHotkey() const
{ return menuHotkey; }

bool GenericWindow::hasMainMenuEntry()
{
    if (hideMenuEntry)
        return false;
    layoutInstant.update();
    layoutBoxcar.update();
    layoutAverage.update();
    if (layoutInstant.pages() != 0 || layoutBoxcar.pages() != 0 || layoutAverage.pages() != 0)
        return true;

    if (window != NULL) {
        parent->unregisterWindowHandler(window);
        window->deleteLater();
        window = NULL;
    }

    if (!parent->getCommandManager().getManual(getName()).read().toHash().empty())
        return true;

    return false;
}

void GenericWindow::mainMenuSelected()
{
    layoutInstant.update();
    layoutBoxcar.update();
    layoutAverage.update();

    if (layoutInstant.pages() == 0 && layoutBoxcar.pages() == 0 && layoutAverage.pages() == 0) {
        if (window != NULL) {
            parent->unregisterWindowHandler(window);
            window->deleteLater();
            window = NULL;
        }
        showCommandMenu();
        return;
    }

    if (window != NULL) {
        parent->bringWindowToFront(window);
        return;
    }

    window = new GenericDisplayWindow(this);
    parent->registerWindowHandler(window);
}

void GenericWindow::pageChangeRequest(int page)
{
    if (window != NULL)
        window->selectPage(page);
}

void GenericWindow::showCommandMenu()
{
    parent->displayMenu(MenuHandler::createGenericWindowMenu(this, parent));
}

void GenericWindow::hideWindow()
{
    if (window == NULL)
        return;
    parent->unregisterWindowHandler(window);
    window->deleteLater();
    window = NULL;
}

void GenericWindow::nextDisplay()
{
    if (window == NULL)
        return;
    window->nextDisplay();
}

void GenericWindow::nextAveraging()
{
    if (window == NULL)
        return;
    window->nextAveraging();
}


void GenericWindow::handleSelectionUnit(const SequenceName &unit,
                                        QSet<SequenceName> &seen,
                                        std::vector<InputSelection> &selections,
                                        RealtimeLayout &layout)
{
    if (seen.contains(unit))
        return;
    seen.insert(unit);
    if (unit.isMeta())
        return;

    for (auto &check : selections) {
        if (!check.selection.registerInput(unit))
            continue;

        layout.incomingData(SequenceValue(SequenceIdentity(unit.toMeta()), check.metadata));
        break;
    }
}

static bool showValueCheck(const Variant::Read &input, const Variant::Read &check)
{ return Variant::Root::overlay(Variant::Root(input), Variant::Root(check)).read() == input; }

static bool shouldShow(const Variant::Read &input,
                       const Variant::Read &require,
                       const Variant::Read &exclude)
{
    if (require.exists() && !showValueCheck(input, require))
        return false;
    if (exclude.exists() && showValueCheck(input, exclude))
        return false;
    return true;
}

void GenericWindow::incomingRealtime(const SequenceValue::Transfer &data)
{
    for (const auto &value : data) {
        handleSelectionUnit(value.getUnit(), seenInstant, inputSelectionsInstant, layoutInstant);
        handleSelectionUnit(value.getUnit(), seenBoxcar, inputSelectionsBoxcar, layoutBoxcar);
        handleSelectionUnit(value.getUnit(), seenAverage, inputSelectionsAverage, layoutAverage);

        if (!value.getUnit().isMeta()) {
            layoutInstant.incomingData(value);
            layoutAverage.incomingData(value);
            layoutBoxcar.incomingData(value);
        }

        if (showOnUpdate.matches(value.getUnit()) &&
                shouldShow(value.read(), showOnUpdateRequire, showOnUpdateExclude)) {
            QMetaObject::invokeMethod(this, "mainMenuSelected", Qt::QueuedConnection);
        }
    }
}
