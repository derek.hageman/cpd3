/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "datacore/variant/root.hxx"
#include "datacore/variant/composite.hxx"
#include "core/util.hxx"

#include "interfacehandler.hxx"
#include "windowhandler.hxx"
#include "realtimewindow.hxx"
#include "menuhandler.hxx"
#include "acquisition_curses.hxx"

/* Last because it defines some weird things (like "timeout") */
#include <curses.h>

using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class InterfaceWindow : public RealtimeWindow {
    InterfaceHandler *interface;

    enum {
        Instant, Boxcar, Average
    } layout;
public:
    InterfaceWindow(InterfaceHandler *i) : interface(i), layout(Instant)
    {
        if (interface->layoutInstant.pages() != 0) {
            layout = Instant;
            setLayout(&interface->layoutInstant);
        } else if (interface->layoutBoxcar.pages() != 0) {
            layout = Boxcar;
            setLayout(&interface->layoutBoxcar);
        } else {
            layout = Average;
            setLayout(&interface->layoutAverage);
        }
    }

    virtual ~InterfaceWindow()
    { }

    void nextAveraging()
    {
        switch (layout) {
        case Instant:
            setLayout(&interface->layoutBoxcar);
            layout = Boxcar;
            if (interface->layoutBoxcar.pages() == 0) {
                setLayout(&interface->layoutAverage);
                layout = Average;
                if (interface->layoutAverage.pages() == 0) {
                    setLayout(&interface->layoutInstant);
                    layout = Instant;
                }
            }
            break;
        case Boxcar:
            setLayout(&interface->layoutAverage);
            layout = Average;
            if (interface->layoutAverage.pages() == 0) {
                setLayout(&interface->layoutInstant);
                layout = Instant;
                if (interface->layoutInstant.pages() == 0) {
                    setLayout(&interface->layoutBoxcar);
                    layout = Boxcar;
                }
            }
            break;
        case Average:
            setLayout(&interface->layoutInstant);
            layout = Instant;
            if (interface->layoutInstant.pages() == 0) {
                setLayout(&interface->layoutBoxcar);
                layout = Boxcar;
                if (interface->layoutBoxcar.pages() == 0) {
                    setLayout(&interface->layoutAverage);
                    layout = Average;
                }
            }
            break;
        }
    }

    virtual bool keypress(int ch)
    {
        if (RealtimeWindow::keypress(ch))
            return true;
        switch (ch) {
        case 'm':
        case 'M':
            interface->showInterfaceMenu();
            return true;

        case 'a':
        case 'A':
            nextAveraging();
            return true;

        case 0x001B:
        case 0x0017:
            if (!interface->showHideWindow())
                break;
            interface->hideWindow();
            return true;
        default:
            break;
        }
        return false;
    }

protected:
    virtual int getBaseWidth()
    {
        return InterfaceHandler::tr("NO COMMS").length();
    }

    virtual int getBaseHeight()
    {
        return 1;
    }

    virtual QString getWindowTitle()
    {
        QString title(interface->information.hash("WindowTitle").toDisplayString());
        switch (layout) {
        case Instant:
            break;
        case Boxcar:
            title.append(InterfaceHandler::tr(" - Boxcar"));
            break;
        case Average:
            title.append(InterfaceHandler::tr(" - Average"));
            break;
        }
        return title;
    }

    virtual bool overrideContents(bool top, int width, int height, int windowX, int windowY)
    {
        const auto &state = interface->state.hash("Status").toString();
        if (!CPD3::Util::equal_insensitive(state, "NoCommunications"))
            return false;

        WINDOW *win = subwin(stdscr, height, width, windowY, windowX);
        if (win == NULL)
            return true;
        wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
        wbkgdset(win, ' ' | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
        werase(win);

        if (Client::blinkState()) {
            wattrset(win, A_BOLD | COLOR_PAIR(Client::COLOR_RED_BLUE));
        } else {
            wattrset(win, COLOR_PAIR(Client::COLOR_RED_BLUE));
        }
        QString noComms(InterfaceHandler::tr("NO COMMS"));
        mvwaddstr(win, height / 2, (width - noComms.length()) / 2, noComms.toUtf8().constData());

        if (top)
            wattrset(win, A_BOLD | COLOR_PAIR(Client::COLOR_YELLOW_BLUE));
        else
            wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
        box(win, 0, 0);
        QString title(getWindowTitle());
        if (!title.isEmpty()) {
            mvwaddstr(win, 0, (width - title.length()) / 2, title.toUtf8().constData());
        }


        wnoutrefresh(win);
        delwin(win);
        return true;
    }
};

InterfaceHandler::InterfaceHandler(Client *c, const Variant::Read &info, const QString &n)
        : QObject(c),
          parent(c),
          information(
                                                                                             info),
          state(Variant::Read::empty()),
          name(n),
          layoutInstant(
                                                                                             true),
          layoutBoxcar(
                                                                                             true),
          layoutAverage(
                                                                                             true),
          window(NULL)
{
    information.detachFromRoot();
}

InterfaceHandler::~InterfaceHandler()
{
    if (window != NULL) {
        parent->unregisterWindowHandler(window);
        window->deleteLater();
    }
}

bool InterfaceHandler::informationUpdated(const Variant::Read &info)
{
    if (info.hash("Component") != information.hash("Component"))
        return false;

    information = info;
    information.detachFromRoot();
    return true;
}

void InterfaceHandler::stateUpdated(const Variant::Read &st)
{
    state = st;
    state.detachFromRoot();
}

bool InterfaceHandler::isBypassed() const
{
    return !state.hash("BypassFlags").toFlags().empty();
}

bool InterfaceHandler::isContaminated() const
{
    return Variant::Composite::isContaminationFlag(state.hash("SystemFlags").toFlags());
}

InterfaceHandler::StatusLineData::StatusLineData()
        : text(),
          status(AcquisitionInterface::GeneralStatus::NoCommunications),
          bypassed(false),
          contaminated(false)
{ }

InterfaceHandler::StatusLineData InterfaceHandler::getStatusLine() const
{
    StatusLineData data;
    data.text = information.hash("MenuCharacter").toDisplayString();
    if (data.text.isEmpty())
        return data;
    const auto &str = state.hash("Status").toString();
    if (CPD3::Util::equal_insensitive(str, "normal")) {
        data.status = AcquisitionInterface::GeneralStatus::Normal;
    } else if (CPD3::Util::equal_insensitive(str, "nocommunications")) {
        data.status = AcquisitionInterface::GeneralStatus::NoCommunications;
    } else if (CPD3::Util::equal_insensitive(str, "disabled")) {
        data.status = AcquisitionInterface::GeneralStatus::Disabled;
    } else {
        data.status = AcquisitionInterface::GeneralStatus::NoCommunications;
    }
    data.bypassed = isBypassed();
    data.contaminated = isContaminated();
    return data;
}

QString InterfaceHandler::getMainMenuEntry() const
{ return information.hash("MenuEntry").toDisplayString(); }

QString InterfaceHandler::getMainMenuHotkey() const
{ return information.hash("MenuCharacter").toDisplayString(); }


bool InterfaceHandler::hasMainMenuEntry()
{
    layoutInstant.update();
    layoutBoxcar.update();
    layoutAverage.update();
    if (information.hash("MenuHide").toBool())
        return false;
    if (layoutInstant.pages() != 0 || layoutBoxcar.pages() != 0 || layoutAverage.pages() != 0)
        return true;

    if (window != NULL) {
        parent->unregisterWindowHandler(window);
        window->deleteLater();
        window = NULL;
    }

    if (!parent->getCommandManager().getCommands(getName()).read().toHash().empty())
        return true;

    for (const auto &cmd : parent->getCommandManager().getAggregateCommands()) {
        for (auto check : cmd.read().hash("Targets").toArray()) {
            if (check.toQString() != getName())
                continue;
            return true;
        }
    }

    return false;
}

void InterfaceHandler::mainMenuSelected()
{
    layoutInstant.update();
    layoutBoxcar.update();
    layoutAverage.update();
    if (layoutInstant.pages() == 0 && layoutBoxcar.pages() == 0 && layoutAverage.pages() == 0) {
        if (window != NULL) {
            parent->unregisterWindowHandler(window);
            window->deleteLater();
            window = NULL;
        }
        showInterfaceMenu();
        return;
    }

    if (window != NULL) {
        parent->bringWindowToFront(window);
        return;
    }

    window = new InterfaceWindow(this);
    parent->registerWindowHandler(window);
}

void InterfaceHandler::pageChangeRequest(int page)
{
    if (window != NULL)
        window->selectPage(page);
}

void InterfaceHandler::showInterfaceMenu()
{
    parent->displayMenu(MenuHandler::createInterfaceWindowMenu(this, parent));
}

void InterfaceHandler::hideWindow()
{
    if (window == NULL)
        return;
    parent->unregisterWindowHandler(window);
    window->deleteLater();
    window = NULL;
}

void InterfaceHandler::nextDisplay()
{
    if (window == NULL)
        return;
    window->nextDisplay();
}

void InterfaceHandler::nextAveraging()
{
    if (window == NULL)
        return;
    window->nextAveraging();
}

static const SequenceName::Component archiveInstantMeta = "rt_instant_meta";
static const SequenceName::Component archiveBoxcarMeta = "rt_boxcar_meta";

void InterfaceHandler::incomingRealtime(const SequenceValue::Transfer &data)
{
    for (const auto &value : data) {
        if (value.getUnit().isMeta()) {
            if (value.read().metadata("Source").hash("Name").toQString() != name) {
                /* Explicitly discontinue these in case the variable source
                 * changes */
                layoutInstant.incomingData(
                        SequenceValue(value.getUnit(), Variant::Root(), value.getStart(),
                                      value.getEnd()));
                layoutAverage.incomingData(
                        SequenceValue(value.getUnit(), Variant::Root(), value.getStart(),
                                      value.getEnd()));
                layoutBoxcar.incomingData(
                        SequenceValue(value.getUnit(), Variant::Root(), value.getStart(),
                                      value.getEnd()));
                continue;
            }
            const auto &archive = value.getArchive();
            if (archive == archiveInstantMeta) {
                layoutInstant.incomingData(value);
                continue;
            } else if (archive == archiveBoxcarMeta) {
                layoutBoxcar.incomingData(value);
                continue;
            } else {
                layoutAverage.incomingData(value);
                continue;
            }
        }

        layoutInstant.incomingData(value);
        layoutAverage.incomingData(value);
        layoutBoxcar.incomingData(value);
    }
}
