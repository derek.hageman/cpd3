/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef SYSTEMSTATUS_H
#define SYSTEMSTATUS_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QStringList>

#include "core/timeutils.hxx"
#include "datacore/stream.hxx"

class Client;

/**
 * A window to display the system status information.
 */
class SystemStatus : public QObject {
Q_OBJECT

    Client *parent;

    enum {
        Averaging, AveragingEditing,

        ContaminateSet, ContaminateClear, ContaminateClearAll,

        BypassSet, BypassClear, BypassClearAll,

        Autoprobe,

        Restart,

        Close,

        RestartConfirmYes, RestartConfirmNo,

        ContaminateClearAllConfirmYes, ContaminateClearAllConfirmNo,

        BypassClearAllConfirmYes, BypassClearAllConfirmNo,

        AveragingChangeConfirmYes, AveragingChangeConfirmNo,

        AutoprobeDetails,
    } selection;

    QString averagingText;
    static CPD3::Time::LogicalTimeUnit averagingUnit;
    static int averagingCount;
    static bool averagingAlign;

    CPD3::Time::LogicalTimeUnit confirmAveragingUnit;
    int confirmAveragingCount;
    bool confirmAveragingAlign;

    int editingCursor;

    struct AutoprobeState {
        QString physical;

        QString activeName;
        int activeTry;
        int activeTotalTries;

        int otherCandidates;

        AutoprobeState();

        AutoprobeState(const CPD3::Data::Variant::Read &info);
    };

    std::vector<AutoprobeState> autoprobeState;

    static QString getAveragingText(CPD3::Time::LogicalTimeUnit unit, int count, bool align);

    void parseAveraging();

    void renderMainWindow();

    void renderGenericConfirm(const QStringList &confirmText,
                              const QString &yesText,
                              const QString &noText,
                              bool yesSelected);

    inline void renderGenericConfirm(const QString &confirmText,
                                     const QString &yesText,
                                     const QString &noText,
                                     bool yesSelected)
    {
        renderGenericConfirm(QStringList(confirmText), yesText, noText, yesSelected);
    }

    void renderAutoprobeDetails();

public:
    /**
     * Create the message log entry.
     */
    SystemStatus(Client *p);

    ~SystemStatus();

    /**
     * Handle a keypress when the dialog is shown.
     * 
     * @param ch    the key
     * @return      true if the key was used.
     */
    bool keypress(int ch);

    /**
     * Render the dialog.
     */
    void render();

    /**
     * Update the state of autoprobing.
     *
     * @param state the autoprobe state
     */
    void updateAutoprobeState(const CPD3::Data::Variant::Read &state);

signals:

    /**
     * Emitted when the dialog is dismissed, after any action has been taken.
     */
    void dismissed();
};

#endif
