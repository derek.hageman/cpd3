/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "acquisition_curses.hxx"
#include "windowhandler.hxx"

WindowHandler::WindowHandler()
{ }

WindowHandler::~WindowHandler()
{ }

bool WindowHandler::keypress(int ch)
{
    Q_UNUSED(ch);
    return false;
}

void WindowHandler::render(bool top)
{
    Q_UNUSED(top);
}
