/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "datacore/variant/root.hxx"
#include "acquisition/realtimelayout.hxx"

#include "acquisition_curses.hxx"
#include "realtimewindow.hxx"

/* Last because it defines some weird things (like "timeout") */
#include <curses.h>

using namespace CPD3::Acquisition;

RealtimeWindow::RealtimeWindow(RealtimeLayout *l) : layout(l),
                                                    windowX(-1),
                                                    windowY(-1),
                                                    layoutPage(0),
                                                    scrollOffset(0),
                                                    scrollAmount(0)
{ }

RealtimeWindow::~RealtimeWindow()
{ }

int RealtimeWindow::getBaseWidth()
{ return 0; }

int RealtimeWindow::getBaseHeight()
{ return 0; }

QString RealtimeWindow::getWindowTitle()
{ return QString(); }

bool RealtimeWindow::overrideContents(bool top, int width, int height, int windowX, int windowY)
{
    Q_UNUSED(top);
    Q_UNUSED(width);
    Q_UNUSED(height);
    Q_UNUSED(windowX);
    Q_UNUSED(windowY);
    return false;
}

void RealtimeWindow::nextDisplay()
{
    layoutPage++;
    scrollOffset = 0;
}

void RealtimeWindow::selectPage(int page)
{
    if (layoutPage != page) {
        layoutPage = page;
        scrollOffset = 0;
    }
}

bool RealtimeWindow::keypress(int ch)
{
    switch (ch) {
    case KEY_UP:
        if (windowY > 0)
            windowY--;
        return true;
    case KEY_DOWN:
        if (windowY != -1)
            windowY++;
        return true;
    case KEY_LEFT:
        if (windowX > 0)
            windowX--;
        return true;
    case KEY_RIGHT:
        if (windowX != -1)
            windowX++;
        return true;

    case 'd':
    case 'D':
        nextDisplay();
        return true;

    case KEY_NPAGE:
        if (scrollAmount > 0) {
            scrollOffset += scrollAmount;
            return true;
        }
        break;

    case KEY_PPAGE:
        if (scrollAmount > 0) {
            scrollOffset -= scrollAmount;
            return true;
        }
        break;
    }

    return false;
}

void RealtimeWindow::adjustWindowPosition(int width, int height)
{
    if (windowX < 0) {
        windowX = (COLS - width) / 2;
    } else if (windowX + width > COLS) {
        windowX = COLS - width;
    }
    if (windowY < 0) {
        windowY = (LINES - height) / 2;
    } else if (windowY + height >= LINES) {
        windowY = LINES - height - 1;
    }
}

void RealtimeWindow::renderEmpty(bool top)
{
    scrollAmount = 0;
    scrollOffset = 0;

    int width = getBaseWidth();
    int height = getBaseHeight();
    QString title(getWindowTitle());
    width = qMax(width, title.length());

    width += 2;
    height += 2;

    adjustWindowPosition(width, height);
    if (overrideContents(top, width, height, windowX, windowY))
        return;

    WINDOW *win = subwin(stdscr, height, width, windowY, windowX);
    if (win == NULL)
        return;
    wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    wbkgdset(win, ' ' | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    werase(win);
    if (top)
        wattrset(win, A_BOLD | COLOR_PAIR(Client::COLOR_YELLOW_BLUE));
    else
        wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    box(win, 0, 0);
    if (!title.isEmpty()) {
        mvwaddstr(win, 0, (width - title.length()) / 2, title.toUtf8().constData());
    }
    wnoutrefresh(win);
    delwin(win);
}

static void expandColumn(QList<int> &columns, int col, int width)
{
    while (columns.size() <= col) {
        columns.append(0);
    }
    if (columns.at(col) >= width)
        return;
    columns[col] = width;
}

static int longestLineLength(const QString &text)
{
    int length = 0;
    int offset = 0;
    while (offset < text.length()) {
        int idx = text.indexOf('\n', offset);
        int idx2 = text.indexOf('\r', offset);
        if (idx < 0 || (idx2 >= 0 && idx2 < idx))
            idx = idx2;
        if (idx < 0)
            break;

        length = qMax(length, idx - offset);
        offset = idx + 1;
    }
    length = qMax(length, text.length() - offset);
    return length;
}

void RealtimeWindow::render(bool top)
{
    if (layout == NULL) {
        renderEmpty(top);
        return;
    }
    layout->update();
    int totalLayoutPages = layout->pages();
    if (totalLayoutPages <= 0) {
        renderEmpty(top);
        return;
    }

    while (layoutPage >= totalLayoutPages) {
        layoutPage -= totalLayoutPages;
    }
    while (layoutPage < 0) {
        layoutPage += totalLayoutPages;
    }

    QString titleText(getWindowTitle());

    QList<int> columnWidths;
    int nGroupRows = 0;
    int windowHeight = getBaseHeight();
    int windowWidth = qMax(titleText.length(), getBaseWidth());
    int groupRowStart = 0;
    int multiLineOffset = 0;
    bool inGroup = false;
    for (RealtimeLayout::iterator add = layout->begin(layoutPage), endAdd = layout->end(layoutPage);
            add != endAdd;
            ++add) {
        switch (add.type()) {
        case RealtimeLayout::GroupBox:
        case RealtimeLayout::FinalGroupBox:
            if (inGroup) {
                groupRowStart += nGroupRows + 1;
                if (!add.text().isEmpty()) {
                    windowWidth = qMax(windowWidth, add.text().length() + 2);
                }
                windowHeight = qMax(windowHeight, groupRowStart);
            } else if (!add.text().isEmpty()) {
                windowWidth = qMax(windowWidth, add.text().length() + 2);
                groupRowStart++;
                windowHeight = qMax(windowHeight, groupRowStart);
            }
            multiLineOffset = 0;
            nGroupRows = 0;
            inGroup = true;
            break;
        case RealtimeLayout::LeftJustifiedLabel:
        case RealtimeLayout::CenteredLabel:
        case RealtimeLayout::RightJustifiedLabel:
        case RealtimeLayout::ValueText:
            windowHeight = qMax(windowHeight, groupRowStart + add.row() + 1 + multiLineOffset);
            nGroupRows = qMax(nGroupRows, add.row() + 1 + multiLineOffset);
            expandColumn(columnWidths, add.column(), add.text().length());
            break;
        case RealtimeLayout::ValueLine: {
            int nLineBreaks = add.text().count('\n') + add.text().count('\r');
            multiLineOffset += nLineBreaks;
            windowHeight = qMax(windowHeight, groupRowStart + add.row() + 1 + multiLineOffset);
            nGroupRows = qMax(nGroupRows, add.row() + 1 + nLineBreaks);
            windowWidth = qMax(windowWidth, longestLineLength(add.text()));
            break;
        }
        }
    }
    bool haveSeenNonEmptyColumn = false;
    int totalColumnsWidth = 0;
    for (int colIdx = columnWidths.size() - 1; colIdx >= 0; --colIdx) {
        if (columnWidths.at(colIdx) <= 0)
            continue;
        if (haveSeenNonEmptyColumn) {
            columnWidths[colIdx]++;
        }
        haveSeenNonEmptyColumn = true;
        totalColumnsWidth += columnWidths.at(colIdx);
    }
    windowWidth = qMax(totalColumnsWidth, windowWidth);

    int drawWidth = windowWidth + 2;
    int drawHeight = windowHeight + 2;

    bool drawScrollMessage = false;
    if (drawHeight >= LINES - 1) {
        int effectiveWindowHeight = LINES - 4;
        if (effectiveWindowHeight <= 0)
            effectiveWindowHeight = 1;
        drawHeight = effectiveWindowHeight + 3;
        scrollAmount = effectiveWindowHeight - 1;

        drawWidth = qMax(drawWidth, QObject::tr("PRESS PGUP/PGDN TO SCROLL").length() + 2);

        if (scrollOffset + effectiveWindowHeight > windowHeight)
            scrollOffset = windowHeight - effectiveWindowHeight;
        if (scrollOffset < 0)
            scrollOffset = 0;

        windowHeight = effectiveWindowHeight;
        drawScrollMessage = true;
    } else {
        scrollOffset = 0;
        scrollAmount = 0;
    }

    adjustWindowPosition(drawWidth, drawHeight);
    if (overrideContents(top, drawWidth, drawHeight, windowX, windowY))
        return;
    WINDOW *win = subwin(stdscr, drawHeight, drawWidth, windowY, windowX);
    if (win == NULL)
        return;
    wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    wbkgdset(win, ' ' | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    werase(win);

    QList<int> columnStarts;
    totalColumnsWidth = 1;
    for (QList<int>::const_iterator add = columnWidths.constBegin(),
            endAdd = columnWidths.constEnd(); add != endAdd; ++add) {
        columnStarts.append(totalColumnsWidth);
        totalColumnsWidth += *add;
    }

    QList<QPair<int, QString> > separatorLines;
    nGroupRows = 0;
    groupRowStart = 1;
    inGroup = false;
    int realRow = 0;
    multiLineOffset = 0;
    for (RealtimeLayout::iterator add = layout->begin(layoutPage), endAdd = layout->end(layoutPage);
            add != endAdd;
            ++add) {
        switch (add.type()) {
        case RealtimeLayout::GroupBox:
        case RealtimeLayout::FinalGroupBox:
            if (inGroup) {
                groupRowStart += nGroupRows;
                separatorLines.append(QPair<int, QString>(groupRowStart, add.text()));
                groupRowStart++;
            } else if (!add.text().isEmpty()) {
                separatorLines.append(QPair<int, QString>(groupRowStart, add.text()));
                groupRowStart++;
            }
            multiLineOffset = 0;
            nGroupRows = 0;
            inGroup = true;
            break;

        case RealtimeLayout::CenteredLabel:
            nGroupRows = qMax(nGroupRows, add.row() + 1);
            realRow = (groupRowStart + add.row()) - scrollOffset + multiLineOffset;
            if (realRow < 1 || realRow > windowHeight)
                break;
            mvwaddstr(win, realRow, columnStarts.at(add.column()) +
                    (columnWidths.at(add.column()) - add.text().length()) / 2,
                      add.text().toUtf8().constData());
            break;

        case RealtimeLayout::RightJustifiedLabel:
            nGroupRows = qMax(nGroupRows, add.row() + 1);
            realRow = (groupRowStart + add.row()) - scrollOffset + multiLineOffset;
            if (realRow < 1 || realRow > windowHeight)
                break;
            mvwaddstr(win, realRow, columnStarts.at(add.column()) +
                    (columnWidths.at(add.column()) - add.text().length()),
                      add.text().toUtf8().constData());
            break;

        case RealtimeLayout::LeftJustifiedLabel:
        case RealtimeLayout::ValueText:
            nGroupRows = qMax(nGroupRows, add.row() + 1);
            realRow = (groupRowStart + add.row()) - scrollOffset + multiLineOffset;
            if (realRow < 1 || realRow > windowHeight)
                break;
            mvwaddstr(win, realRow, columnStarts.at(add.column()), add.text().toUtf8().constData());
            break;

        case RealtimeLayout::ValueLine: {
            int nLineBreaks = add.text().count('\n') + add.text().count('\r');
            nGroupRows = qMax(nGroupRows, add.row() + 1 + nLineBreaks);
            realRow = (groupRowStart + add.row()) - scrollOffset + multiLineOffset;
            multiLineOffset += nLineBreaks;
            for (int offset = 0; ; ++realRow) {
                if (realRow > windowHeight)
                    break;

                int idx = add.text().indexOf('\n', offset);
                int idx2 = add.text().indexOf('\r', offset);
                if (idx < 0 || (idx2 >= 0 && idx2 < idx))
                    idx = idx2;
                QString text;
                if (idx < 0) {
                    text = add.text().mid(offset);
                } else {
                    text = add.text().mid(offset, idx - offset);
                }

                if (realRow > 0)
                    mvwaddstr(win, realRow, 1, text.toUtf8().constData());

                if (idx < 0)
                    break;
                offset = idx + 1;
                if (offset >= add.text().length())
                    break;
            }
            break;
        }
        }
    }

    if (drawScrollMessage) {
        QString text(QObject::tr("PRESS PGUP/PGDN TO SCROLL"));
        text = text.leftJustified((windowWidth + text.length()) / 2);
        text = text.rightJustified(windowWidth);
        wattrset(win, A_REVERSE | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
        mvwaddstr(win, windowHeight + 1, 1, text.toUtf8().constData());
    }

    if (top)
        wattrset(win, A_BOLD | COLOR_PAIR(Client::COLOR_YELLOW_BLUE));
    else
        wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    box(win, 0, 0);
    if (!titleText.isEmpty()) {
        mvwaddstr(win, 0, (drawWidth - titleText.length()) / 2, titleText.toUtf8().constData());
    }
    for (QList<QPair<int, QString> >::const_iterator add = separatorLines.constBegin(),
            endAdd = separatorLines.constEnd(); add != endAdd; ++add) {
        realRow = add->first - scrollOffset;
        if (realRow < 1 || realRow > windowHeight)
            continue;
        mvwhline(win, realRow, 1, ACS_HLINE, windowWidth);
        if (!add->second.isEmpty()) {
            mvwaddstr(win, realRow, (drawWidth - add->second.length()) / 2,
                      add->second.toUtf8().constData());
        }
        mvwaddch(win, realRow, 0, ACS_LTEE);
        mvwaddch(win, realRow, drawWidth - 1, ACS_RTEE);
    }
    wnoutrefresh(win);
    delwin(win);
}
