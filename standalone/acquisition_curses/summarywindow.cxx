/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QDateTime>

#include "datacore/variant/root.hxx"
#include "datacore/variant/composite.hxx"
#include "core/range.hxx"
#include "acquisition/realtimelayout.hxx"

#include "acquisition_curses.hxx"
#include "summarywindow.hxx"

/* Last because it defines some weird things (like "timeout") */
#include <curses.h>

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

SummaryWindow::ValueConfig::ValueConfig()
        : selection(), sortPriority(INTEGER::undefined()), config(Variant::Write::empty())
{ }

SummaryWindow::ValueConfig::ValueConfig(const Variant::Read &cfg) : selection(cfg),
                                                                    sortPriority(cfg.hash("SortPriority")
                                                                            .toInt64()),
                                                                    config(Variant::Root(
                                                                            cfg).write())
{ }

SummaryWindow::StateConfig::StateConfig()
        : selection(), sortPriority(INTEGER::undefined()), config(Variant::Write::empty())
{ }

SummaryWindow::StateConfig::StateConfig(const Variant::Read &cfg) : selection(cfg),
                                                                    sortPriority(cfg.hash("SortPriority")
                                                                            .toInt64()),
                                                                    config(Variant::Root(
                                                                            cfg).write())
{ }

SummaryWindow::SummaryWindow(const Variant::Read &config)
        : valueConfig(),
          stateConfig(),
          valueLines(),
          stateLines(),
          dispatch(),
          bufferUnit(Time::Hour),
          bufferCount(1),
          bufferAligned(false),
          titleString(),
          seenStations(),
          renderText(),
          lastWidth(-1),
          lastHeight(-1),
          renderOutdated(false)
{
    titleString = tr("NOAA/GMD aerosol monitoring station - ${STATION|UPPER}");

    if (!config.exists()) {
        ValueConfig *vc = new ValueConfig;
        vc->selection = SequenceMatch::Composite(SequenceMatch::Element("", "raw", "Bb?[sae][BGR0-9]_[ASE]11", {}, SequenceName::defaultLacksFlavors()));
        vc->config.hash("Format").setString("00000.0");
        vc->config.hash("Units").setString("Mm-1");
        valueConfig.append(vc);

        vc = new ValueConfig;
        vc->selection = SequenceMatch::Composite(SequenceMatch::Element("", "raw", "N_N[67][1-9]", {}, SequenceName::defaultLacksFlavors()));
        vc->config.hash("Format").setString("00000.0");
        valueConfig.append(vc);

        vc = new ValueConfig;
        vc->selection = SequenceMatch::Composite(SequenceMatch::Element("", "raw", "[TUP]_S11", {}, SequenceName::defaultLacksFlavors()));
        vc->config.hash("Format").setString("00000.0");
        valueConfig.append(vc);

        vc = new ValueConfig;
        vc->selection = SequenceMatch::Composite(SequenceMatch::Element("", "raw", "W[SD]_.*", {}, SequenceName::defaultLacksFlavors()));
        vc->config.hash("Format").setString("00000.0");
        valueConfig.append(vc);

        StateConfig *sc = new StateConfig;
        sc->selection = SequenceMatch::Composite(SequenceMatch::Element("", "rt_instant", "ZNEXT_IMPACTOR", {}, SequenceName::defaultLacksFlavors()));
        sc->config
          .hash("HashFormat")
          .setString(
                  "${FLAGS|Current/Flavors|,|uc} - SWITCHING AT ${USERTIME|Time} (${AHEAD|Time|COUNTDOWN})");
        stateConfig.append(sc);

        sc = new StateConfig;
        sc->selection = SequenceMatch::Composite(SequenceMatch::Element("", "rt_instant", "Pd_P01", {}, SequenceName::defaultLacksFlavors()));
        sc->config.hash("HashFormat").setString("Q stack ${PITOTFLOW||0000.0|INVALID|} (lpm)");
        stateConfig.append(sc);

        return;
    }

    if (config.hash("Title").exists())
        titleString = config.hash("Title").toDisplayString();

    if (config.hash("BufferTime").exists()) {
        bufferUnit = Variant::Composite::toTimeInterval(config.hash("BufferTime"), &bufferCount,
                                                        &bufferAligned);
    }

    for (auto child : config.hash("Lines").toChildren()) {
        valueConfig.append(new ValueConfig(child));
    }

    for (auto child : config.hash("State").toChildren()) {
        stateConfig.append(new StateConfig(child));
    }
}

SummaryWindow::~SummaryWindow()
{
    for (QHash<SequenceName, Dispatch *>::iterator del = dispatch.begin(), endDel = dispatch.end();
            del != endDel;
            ++del) {
        delete del.value();
    }
    qDeleteAll(stateLines);
    qDeleteAll(valueLines);

    qDeleteAll(valueConfig);
    qDeleteAll(stateConfig);
}


SummaryWindow::Dispatch::~Dispatch()
{ }

SummaryWindow::DispatchDiscard::DispatchDiscard()
{ }

SummaryWindow::DispatchDiscard::~DispatchDiscard()
{ }

void SummaryWindow::DispatchDiscard::handleValue(const SequenceValue &value)
{ Q_UNUSED(value); }


SummaryWindow::DispatchValueInstant::DispatchValueInstant(ValueData *t) : target(t)
{ }

SummaryWindow::DispatchValueInstant::~DispatchValueInstant()
{ }

void SummaryWindow::DispatchValueInstant::handleValue(const SequenceValue &value)
{ target->incomingInstant(value); }

SummaryWindow::DispatchValueBoxcar::DispatchValueBoxcar(ValueData *t) : target(t)
{ }

SummaryWindow::DispatchValueBoxcar::~DispatchValueBoxcar()
{ }

void SummaryWindow::DispatchValueBoxcar::handleValue(const SequenceValue &value)
{ target->incomingBoxcar(value); }

SummaryWindow::DispatchValueAverage::DispatchValueAverage(ValueData *t) : target(t)
{ }

SummaryWindow::DispatchValueAverage::~DispatchValueAverage()
{ }

void SummaryWindow::DispatchValueAverage::handleValue(const SequenceValue &value)
{ target->incomingAverage(value); }

SummaryWindow::DispatchValueMetadata::DispatchValueMetadata(ValueData *t) : target(t)
{ }

SummaryWindow::DispatchValueMetadata::~DispatchValueMetadata()
{ }

void SummaryWindow::DispatchValueMetadata::handleValue(const SequenceValue &value)
{ target->incomingMetadata(value); }

SummaryWindow::DispatchState::DispatchState(StateData *t) : target(t)
{ }

SummaryWindow::DispatchState::~DispatchState()
{ }

void SummaryWindow::DispatchState::handleValue(const SequenceValue &value)
{ target->incomingValue(value); }

SummaryWindow::DispatchStateMetadata::DispatchStateMetadata(StateData *t) : target(t)
{ }

SummaryWindow::DispatchStateMetadata::~DispatchStateMetadata()
{ }

void SummaryWindow::DispatchStateMetadata::handleValue(const SequenceValue &value)
{ target->incomingMetadata(value); }


SummaryWindow::ValueData::ValueData(const SequenceName &u,
                                    SummaryWindow::ValueConfig *c,
                                    SummaryWindow *p) : unit(u),
                                                        config(c),
                                                        parent(p),
                                                        sortPriority(c->sortPriority),
                                                        label(c->config
                                                               .hash("Name")
                                                               .toDisplayString()),
                                                        units(c->config
                                                               .hash("Units")
                                                               .toDisplayString()),
                                                        format(c->config
                                                                .hash("Format")
                                                                .toDisplayString()),
                                                        instant(),
                                                        boxcar(),
                                                        average(),
                                                        valueBuffer(),
                                                        bufferCacheValid(false)
{
    if (label.isEmpty() && !c->config.hash("Name").exists())
        label = unit.getVariableQString();
}

bool SummaryWindow::ValueData::sortCompare(const ValueData &other) const
{
    if (!INTEGER::defined(sortPriority)) {
        if (INTEGER::defined(other.sortPriority))
            return false;
    } else if (!INTEGER::defined(other.sortPriority)) {
        return true;
    } else if (sortPriority != other.sortPriority) {
        return sortPriority < other.sortPriority;
    }
    return SequenceName::OrderDisplay()(unit, other.unit);
}

void SummaryWindow::ValueData::incomingInstant(const SequenceValue &value)
{
    instant = value.root();
    parent->renderOutdated = true;
}

void SummaryWindow::ValueData::incomingBoxcar(const SequenceValue &value)
{
    boxcar = value.root();
    parent->renderOutdated = true;
}

void SummaryWindow::ValueData::incomingAverage(const SequenceValue &value)
{
    average = value.root();
    valueBuffer.emplace_back(value);
    bufferCacheValid = false;
    parent->pruneBuffer(valueBuffer);
    parent->renderOutdated = true;
}

void SummaryWindow::ValueData::incomingMetadata(const SequenceValue &value)
{
    if (!config->config.hash("Units").exists())
        units = value.read().metadata("Units").toDisplayString();
    if (!config->config.hash("Format").exists())
        format = NumberFormat(value.read().metadata("Format").toDisplayString());
    parent->renderOutdated = true;
}

QString SummaryWindow::ValueData::getInstant() const
{
    double v = instant.read().toDouble();
    if (!FP::defined(v))
        return QString();
    return format.apply(v, QChar(' '));
}

QString SummaryWindow::ValueData::getBoxcar() const
{
    double v = boxcar.read().toDouble();
    if (!FP::defined(v))
        return QString();
    return format.apply(v, QChar(' '));
}

QString SummaryWindow::ValueData::getAverage() const
{
    double v = average.read().toDouble();
    if (!FP::defined(v))
        return QString();
    return format.apply(v, QChar(' '));
}

void SummaryWindow::ValueData::getBuffer(QString &meanOut,
                                         QString &minOut,
                                         QString &maxOut,
                                         QString &minTimeOut,
                                         QString &maxTimeOut)
{
    if (bufferCacheValid) {
        meanOut = meanCached;
        minOut = minCached;
        maxOut = maxCached;
        minTimeOut = minTimeCached;
        maxTimeOut = maxTimeCached;
        return;
    }

    double min = FP::undefined();
    double max = FP::undefined();
    double minTime = FP::undefined();
    double maxTime = FP::undefined();
    double sum = 0.0;
    int count = 0;
    for (const auto &add : valueBuffer) {
        double v = add.read().toDouble();
        if (!FP::defined(v))
            continue;

        sum += v;
        ++count;

        if (!FP::defined(min) || v < min) {
            min = v;
            minTime = add.getStart();
        }
        if (!FP::defined(max) || v > max) {
            max = v;
            maxTime = add.getStart();
        }
    }

    if (count > 0) {
        meanCached = format.apply(sum / (double) count, QChar(' '));
    } else {
        meanCached = QString();
    }
    meanOut = meanCached;

    if (FP::defined(min)) {
        minCached = format.apply(min, QChar(' '));
    } else {
        minCached = QString();
    }
    minOut = minCached;

    if (FP::defined(max)) {
        maxCached = format.apply(max, QChar(' '));
    } else {
        maxCached = QString();
    }
    maxOut = maxCached;

    if (FP::defined(minTime)) {
        minTimeCached = Time::toDateTime(minTime).toString("hh:mm:ss");
    } else {
        minTimeCached = QString();
    }
    minTimeOut = minTimeCached;

    if (FP::defined(maxTime)) {
        maxTimeCached = Time::toDateTime(maxTime).toString("hh:mm:ss");
    } else {
        maxTimeCached = QString();
    }
    maxTimeOut = minTimeCached;

    bufferCacheValid = true;
}

SummaryWindow::StateData::StateData(const SequenceName &u,
                                    SummaryWindow::StateConfig *c,
                                    SummaryWindow *p) : unit(u),
                                                        config(c),
                                                        parent(p),
                                                        sortPriority(c->sortPriority),
                                                        format(c->config
                                                                .hash("HashFormat")
                                                                .toDisplayString()),
                                                        latest()
{ }

bool SummaryWindow::StateData::sortCompare(const StateData &other) const
{
    if (!INTEGER::defined(sortPriority)) {
        if (INTEGER::defined(other.sortPriority))
            return false;
    } else if (!INTEGER::defined(other.sortPriority)) {
        return true;
    } else if (sortPriority != other.sortPriority) {
        return sortPriority < other.sortPriority;
    }
    return SequenceName::OrderDisplay()(unit, other.unit);
}

void SummaryWindow::StateData::incomingValue(const SequenceValue &value)
{
    if (!value.read().exists())
        return;
    latest = value.root();
    parent->renderOutdated = true;
}

void SummaryWindow::StateData::incomingMetadata(const SequenceValue &value)
{
    if (!config->config.hash("HashFormat").exists()) {
        format = value.read().metadata("Realtime").hash("HashFormat").toDisplayString();
    }
    parent->renderOutdated = true;
}

QString SummaryWindow::StateData::getLine() const
{ return RealtimeLayout::hashFormat(format, latest); }

void SummaryWindow::pruneBuffer(SequenceValue::Transfer &buffer) const
{
    if (buffer.size() < 2)
        return;
    double pruneTime = buffer.back().getStart();
    if (!FP::defined(pruneTime))
        return;
    pruneTime = Time::logical(pruneTime, bufferUnit, -bufferCount, bufferAligned);
    if (!FP::defined(pruneTime))
        return;

    auto discardEnd = Range::lowerBound(buffer.begin(), buffer.end(), pruneTime);
    if (discardEnd == buffer.begin())
        return;
    buffer.erase(buffer.begin(), discardEnd);
}

template<class T>
static bool lineItemSortCompare(const T &a, const T &b)
{ return a->sortCompare(*b); }

static const SequenceName::Component archiveInstant = "rt_instant";
static const SequenceName::Component archiveInstantMeta = "rt_instant_meta";
static const SequenceName::Component archiveBoxcar = "rt_boxcar";
static const SequenceName::Component archiveAverage = "raw";

SummaryWindow::Dispatch *SummaryWindow::createDispatch(const SequenceName &unit)
{
    SequenceName normalizedUnit(unit);
    normalizedUnit.removeFlavor(SequenceName::flavor_pm1);
    normalizedUnit.removeFlavor(SequenceName::flavor_pm10);
    normalizedUnit.removeFlavor(SequenceName::flavor_pm25);
    normalizedUnit.clearMeta();

    if (normalizedUnit.getArchive() == archiveInstant) {
        for (QList<StateData *>::const_iterator state = stateLines.constBegin(),
                end = stateLines.constEnd(); state != end; ++state) {
            if (!(*state)->matches(normalizedUnit))
                continue;
            if (unit.isMeta()) {
                return new DispatchStateMetadata(*state);
            } else {
                return new DispatchState(*state);
            }
        }

        for (QList<StateConfig *>::const_iterator config = stateConfig.constBegin(),
                end = stateConfig.constEnd(); config != end; ++config) {
            if (!(*config)->selection.matches(normalizedUnit))
                continue;

            StateData *state = new StateData(normalizedUnit, *config, this);
            stateLines.append(state);
            std::sort(stateLines.begin(), stateLines.end(), lineItemSortCompare<StateData *>);

            if (unit.isMeta()) {
                return new DispatchStateMetadata(state);
            } else {
                return new DispatchState(state);
            }
        }
    }

    normalizedUnit.setArchive(archiveAverage);

    for (QList<ValueData *>::const_iterator value = valueLines.constBegin(),
            end = valueLines.constEnd(); value != end; ++value) {
        if (!(*value)->matches(normalizedUnit))
            continue;
        if (unit.isMeta()) {
            if (unit.getArchive() == archiveInstantMeta)
                return new DispatchValueMetadata(*value);
            continue;
        } else if (unit.getArchive() == archiveInstant) {
            return new DispatchValueInstant(*value);
        } else if (unit.getArchive() == archiveBoxcar) {
            return new DispatchValueBoxcar(*value);
        } else {
            return new DispatchValueAverage(*value);
        }
    }

    for (QList<ValueConfig *>::const_iterator config = valueConfig.constBegin(),
            end = valueConfig.constEnd(); config != end; ++config) {
        if (!(*config)->selection.matches(normalizedUnit))
            continue;
        if (unit.isMeta() && unit.getArchive() != archiveInstantMeta)
            continue;

        ValueData *value = new ValueData(normalizedUnit, *config, this);
        valueLines.append(value);
        std::sort(valueLines.begin(), valueLines.end(), lineItemSortCompare<ValueData *>);

        if (unit.isMeta()) {
            return new DispatchValueMetadata(value);
        } else if (unit.getArchive() == archiveInstant) {
            return new DispatchValueInstant(value);
        } else if (unit.getArchive() == archiveBoxcar) {
            return new DispatchValueBoxcar(value);
        } else {
            return new DispatchValueAverage(value);
        }
    }

    return new DispatchDiscard;
}

void SummaryWindow::incomingRealtime(const SequenceValue::Transfer &data)
{
    for (const auto &add : data) {
        auto target = dispatch.find(add.getUnit());
        if (target == dispatch.end()) {
            target = dispatch.insert(add.getUnit(), createDispatch(add.getUnit()));

            if (!add.getUnit().getStation().empty() && !add.getUnit().isDefaultStation()) {
                seenStations.insert(add.getUnit().getStationQString().toUpper());
            }
        }

        target.value()->handleValue(add);
    }
}

struct SummaryWindowValueRow {
    QString label;
    QString units;
    int decimalDigits;
    QString current;
    QString average;
    QString bufferMean;
    QString bufferMin;
    QString bufferMax;
    QString bufferMinTime;
    QString bufferMaxTime;
};

void SummaryWindow::executeRender(bool top) const
{
    WINDOW *win = subwin(stdscr, LINES - 1, COLS, 0, 0);
    if (win == NULL)
        return;
    wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    wbkgdset(win, ' ' | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    werase(win);

    for (QList<TextData>::const_iterator d = renderText.constBegin(), endD = renderText.constEnd();
            d != endD;
            ++d) {
        mvwaddstr(win, d->y, d->x, d->text.constData());
    }

    if (top) {
        wattrset(win, A_BOLD | COLOR_PAIR(Client::COLOR_YELLOW_BLUE));
    } else {
        wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    }
    box(win, 0, 0);

    QStringList sortedStations(seenStations.values());
    std::sort(sortedStations.begin(), sortedStations.end());
    if (sortedStations.isEmpty())
        sortedStations.append(tr("NO DATA", "no station filler"));
    CPD3::TextSubstitutionStack subs;
    subs.setString("station", sortedStations.front());
    subs.setString("stations", sortedStations.join(tr(",", "station join")));
    auto text = subs.apply(titleString);
    mvwaddstr(win, 0, ((COLS - 1) - text.length()) / 2, text.toUtf8().constData());

    wnoutrefresh(win);
    delwin(win);
}

void SummaryWindow::addTextData(int y, int x, const QByteArray &text)
{
    TextData add;
    add.x = x;
    add.y = y;
    add.text = text;
    renderText.append(add);
}

void SummaryWindow::render(bool top)
{
    {
        int width = COLS;
        int height = LINES - 1;

        if (!renderOutdated && width == lastWidth && height == lastHeight) {
            executeRender(top);
            return;
        }

        renderOutdated = false;
        lastWidth = width;
        lastHeight = height;
    }
    renderText.clear();

    QList<SummaryWindowValueRow> valueRows;
    QVector<int> columnWidths(9, 0);

    static const int minimumNumberWidth = 5;
    static const int minimumTimeWidth = 8;
    columnWidths[2] = qMax(tr("Current", "current header").length(), minimumNumberWidth);
    columnWidths[3] = qMax(tr("Average", "average header").length(), minimumNumberWidth);
    columnWidths[4] = qMax(tr("Mean", "buffer mean header").length(), minimumNumberWidth);
    columnWidths[5] = qMax(tr("Min", "buffer min header").length(), minimumNumberWidth);
    columnWidths[6] = qMax(tr("Max", "buffer max header").length(), minimumNumberWidth);
    columnWidths[7] = qMax(tr("Min", "buffer min time header").length(), minimumTimeWidth);
    columnWidths[8] = qMax(tr("Max", "buffer max time header").length(), minimumTimeWidth);

    int maximumDecimals = 0;

    int row = 2;
    for (QList<ValueData *>::const_iterator value = valueLines.constBegin(),
            end = valueLines.constEnd(); value != end; ++value) {
        if (row >= LINES - 2)
            break;
        ++row;

        SummaryWindowValueRow add;
        add.label = (*value)->getLabel();
        add.units = RealtimeLayout::convertConsoleUnits((*value)->getUnits());
        add.decimalDigits = (*value)->getDecimalDigits();
        add.current = (*value)->getInstant();
        add.average = (*value)->getBoxcar();
        (*value)->getBuffer(add.bufferMean, add.bufferMin, add.bufferMax, add.bufferMinTime,
                            add.bufferMaxTime);

        if (!add.units.isEmpty()) {
            add.units = tr("(%1)", "units format").arg(add.units);
        }

        valueRows.append(add);

        maximumDecimals = qMax(maximumDecimals, add.decimalDigits);

        columnWidths[0] = qMax(columnWidths.at(0), add.label.length());
        columnWidths[1] = qMax(columnWidths.at(1), add.units.length());
        columnWidths[7] = qMax(columnWidths.at(7), add.bufferMinTime.length());
        columnWidths[8] = qMax(columnWidths.at(8), add.bufferMaxTime.length());
    }
    for (QList<SummaryWindowValueRow>::iterator add = valueRows.begin(), endAdd = valueRows.end();
            add != endAdd;
            ++add) {
        if (add->decimalDigits < maximumDecimals) {
            QString padding(maximumDecimals - add->decimalDigits, QChar(' '));

            add->current.append(padding);
            add->average.append(padding);
            add->bufferMean.append(padding);
            add->bufferMin.append(padding);
            add->bufferMax.append(padding);
        }

        columnWidths[2] = qMax(columnWidths.at(2), add->current.length());
        columnWidths[3] = qMax(columnWidths.at(3), add->average.length());
        columnWidths[4] = qMax(columnWidths.at(4), add->bufferMean.length());
        columnWidths[5] = qMax(columnWidths.at(5), add->bufferMin.length());
        columnWidths[6] = qMax(columnWidths.at(6), add->bufferMax.length());
    }

    int totalWidth = 0;
    QVector<int> columnStarts;
    for (QVector<int>::const_iterator add = columnWidths.begin(), endAdd = columnWidths.end();
            add != endAdd;
            ++add) {
        columnStarts.append(totalWidth + 1);
        totalWidth += *add + 1;
    }
    /* Pad the label column up to a limit so on small terminals everything is
     * left justified.  On larger ones, make it right justified. */
    if (totalWidth < COLS - 1) {
        int totalAdd = (COLS - 1) - totalWidth;
        int labelAdd = totalAdd;
        if (labelAdd > 5)
            labelAdd = 5;
        columnWidths[0] += labelAdd;
        for (QVector<int>::iterator mod = columnStarts.begin() + 1, endMod = columnStarts.end();
                mod != endMod;
                ++mod) {
            *mod += labelAdd;
        }

        totalAdd -= labelAdd;
        columnWidths.last() += totalAdd;
    }

    QString text;
    row = 2;

    if (!valueRows.isEmpty()) {
        text = tr("Current", "current header");
        text = text.rightJustified(columnWidths.at(2));
        addTextData(1, columnStarts.at(2), text.toUtf8().constData());

        text = tr("Average", "average header");
        text = text.rightJustified(columnWidths.at(3));
        addTextData(1, columnStarts.at(3), text.toUtf8().constData());

        text = tr("Mean", "buffer mean header");
        text = text.rightJustified(columnWidths.at(4));
        addTextData(1, columnStarts.at(4), text.toUtf8().constData());

        text = tr("Min", "buffer min header");
        text = text.rightJustified(columnWidths.at(5));
        addTextData(1, columnStarts.at(5), text.toUtf8().constData());

        text = tr("Max", "buffer max header");
        text = text.rightJustified(columnWidths.at(6));
        addTextData(1, columnStarts.at(6), text.toUtf8().constData());

        text = tr("Min", "buffer min time header");
        addTextData(1, columnStarts.at(7), text.toUtf8().constData());

        text = tr("Max", "buffer max time header");
        addTextData(1, columnStarts.at(8), text.toUtf8().constData());

        for (QList<SummaryWindowValueRow>::const_iterator add = valueRows.constBegin(),
                endAdd = valueRows.constEnd(); add != endAdd && row < LINES - 2; ++add, ++row) {
            addTextData(row, columnStarts.at(0), add->label.toUtf8().constData());

            text = add->units.rightJustified(columnWidths.at(1));
            addTextData(row, columnStarts.at(1), text.toUtf8().constData());

            text = add->current.rightJustified(columnWidths.at(2));
            addTextData(row, columnStarts.at(2), text.toUtf8().constData());

            text = add->average.rightJustified(columnWidths.at(3));
            addTextData(row, columnStarts.at(3), text.toUtf8().constData());

            text = add->bufferMean.rightJustified(columnWidths.at(4));
            addTextData(row, columnStarts.at(4), text.toUtf8().constData());

            text = add->bufferMin.rightJustified(columnWidths.at(5));
            addTextData(row, columnStarts.at(5), text.toUtf8().constData());

            text = add->bufferMax.rightJustified(columnWidths.at(6));
            addTextData(row, columnStarts.at(6), text.toUtf8().constData());

            text = add->bufferMinTime.leftJustified(columnWidths.at(7));
            addTextData(row, columnStarts.at(7), text.toUtf8().constData());

            text = add->bufferMaxTime.leftJustified(columnWidths.at(8));
            addTextData(row, columnStarts.at(8), text.toUtf8().constData());
        }
    }

    for (QList<StateData *>::const_iterator state = stateLines.constBegin(),
            end = stateLines.constEnd(); state != end; ++state) {
        if (row >= LINES - 2)
            break;
        QString line((*state)->getLine());
        if (line.isEmpty())
            continue;

        addTextData(row, 1, line.toUtf8().constData());
        ++row;
    }

    executeRender(top);
}
