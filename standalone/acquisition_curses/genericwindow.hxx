/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef GENERICWINDOW_H
#define GENERICWINDOW_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "datacore/stream.hxx"
#include "datacore/sequencematch.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/realtimelayout.hxx"

class GenericDisplayWindow;

class Client;

class MenuHandler;

/**
 * The handler for a realtime interface.
 */
class GenericWindow : public QObject {
Q_OBJECT

    Client *parent;
    QString name;

    CPD3::Acquisition::RealtimeLayout layoutInstant;
    CPD3::Acquisition::RealtimeLayout layoutBoxcar;
    CPD3::Acquisition::RealtimeLayout layoutAverage;

    GenericDisplayWindow *window;

    friend class GenericDisplayWindow;

    QString menuTitle;
    QString menuHotkey;
    QString windowTitle;

    struct InputSelection {
        CPD3::Data::SequenceMatch::OrderedLookup selection;
        CPD3::Data::Variant::Root metadata;
    };
    std::vector<InputSelection> inputSelectionsInstant;
    std::vector<InputSelection> inputSelectionsBoxcar;
    std::vector<InputSelection> inputSelectionsAverage;

    QSet<CPD3::Data::SequenceName> seenInstant;
    QSet<CPD3::Data::SequenceName> seenBoxcar;
    QSet<CPD3::Data::SequenceName> seenAverage;

    CPD3::Data::SequenceMatch::Composite showOnUpdate;
    CPD3::Data::Variant::Root showOnUpdateRequire;
    CPD3::Data::Variant::Root showOnUpdateExclude;

    bool hideMenuEntry;

    InputSelection generateInputSelection(const CPD3::Data::SequenceName::Component &defaultArchive,
                                          const CPD3::Data::SequenceName::Component &defaultVariable,
                                          const CPD3::Data::Variant::Read &config);

    void generateInputSelections(std::vector<InputSelection> &target,
                                 const CPD3::Data::SequenceName::Component &defaultArchive,
                                 const CPD3::Data::Variant::Read &config);

    void registerAllExisting(QSet<CPD3::Data::SequenceName> &seen,
                             std::vector<InputSelection> &selections,
                             CPD3::Acquisition::RealtimeLayout &layout);

    void handleSelectionUnit(const CPD3::Data::SequenceName &unit,
                             QSet<CPD3::Data::SequenceName> &seen,
                             std::vector<InputSelection> &selections,
                             CPD3::Acquisition::RealtimeLayout &layout);

public:
    /**
     * Create the handler.
     * 
     * @param c         the main client
     * @param config    the the configuration data
     * @param name      the unique name for the window
     */
    GenericWindow(Client *c,
                  const CPD3::Data::Variant::Read &config,
                  const QString &name = QString());

    ~GenericWindow();

    /**
     * Test if the interface has an actual main menu entry.
     * 
     * @return true if the interface has a main menu entry
     */
    bool hasMainMenuEntry();

    /**
     * Get the main menu text line.
     * 
     * @return the text in the main menu
     */
    QString getMainMenuEntry() const;

    /**
     * Get the main menu hotkey text string.
     * 
     * @return the main menu hotkey
     */
    QString getMainMenuHotkey() const;

    /**
     * Show the command menu for the interface.
     */
    void showCommandMenu();

    /**
     * Hide the window.
     */
    void hideWindow();

    /**
     * Advance the window display mode.
     */
    void nextDisplay();

    /**
     * Advance the window averaging mode.
     */
    void nextAveraging();

    /**
     * Handling incoming realtime data.
     * 
     * @param data  the data
     */
    void incomingRealtime(const CPD3::Data::SequenceValue::Transfer &data);

    /**
     * Get the window unique name.
     * 
     * @return the unique name
     */
    inline QString getName() const
    { return name; }

    /**
     * Test if the next display option in the menu should be shown.
     * 
     * @return true to show the next display menu option
     */
    inline bool showNextDisplay() const
    {
        return window != NULL &&
                (layoutInstant.pages() > 1 ||
                        layoutBoxcar.pages() > 1 ||
                        layoutAverage.pages() > 1);
    }

    /**
     * Test if the advance averaging menu option should be displayed.
     * 
     * @return true to display the next averaging option
     */
    inline bool showChangeAveraging() const
    {
        return window != NULL &&
                ((layoutInstant.pages() > 0 ? 1 : 0) +
                        (layoutBoxcar.pages() > 0 ? 1 : 0) +
                        (layoutAverage.pages() > 0 ? 1 : 0)) > 1;
    }

    /**
     * Test if the hide window menu option should be displayed.
     * 
     * @return true to show the hide window menu option
     */
    inline bool showHideWindow() const
    { return window != NULL; }

public slots:

    /**
     * Called when this entry is selected in the main menu.
     */
    void mainMenuSelected();

private slots:

    void pageChangeRequest(int page);
};

#endif
