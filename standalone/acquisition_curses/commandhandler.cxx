/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "datacore/variant/root.hxx"
#include "datacore/externalsink.hxx"
#include "datacore/variant/composite.hxx"
#include "acquisition/realtimelayout.hxx"

#include "acquisition_curses.hxx"
#include "commandhandler.hxx"
#include "menuhandler.hxx"

/* Last because it defines some weird things (like "timeout") */
#include <curses.h>

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

bool CommandHandler::ParamaterData::operator<(const ParamaterData &other) const
{
    if (!INTEGER::defined(sortPriority)) {
        if (INTEGER::defined(other.sortPriority))
            return false;
    } else if (!INTEGER::defined(other.sortPriority)) {
        return true;
    } else if (sortPriority != other.sortPriority) {
        return sortPriority < other.sortPriority;
    }
    return label < other.label;
}

CommandHandler::CommandHandler(const Variant::Read &cmd, Client *cl) : client(cl),
                                                                       command(Variant::Root(
                                                                               cmd).write()),
                                                                       selectionState(
                                                                               SelectingTarget),
                                                                       targets(),
                                                                       parameters(),
                                                                       selectedParameterIndex(0),
                                                                       parameterEditingString(),
                                                                       parameterEditingCursor(0),
                                                                       confirmed(true)
{
    switch (command.hash("Targets").getType()) {
    case Variant::Type::String:
        targets.append(command.hash("Targets").toQString());
        break;
    case Variant::Type::Array:
        for (auto add : command.hash("Targets").toArray()) {
            QString s(add.toQString());
            if (s.isEmpty())
                continue;
            targets.append(s);
        }
        break;
    default:
        break;
    }

    QSet<QString> uniqueTargets;
    QList<QString> allInterfaces(client->getInterfaces().keys());
    for (QList<QString>::const_iterator check = targets.constBegin(), endCheck = targets.constEnd();
            check != endCheck;
            ++check) {
        QRegExp reCheck(*check);
        for (QList<QString>::const_iterator interfaceName = allInterfaces.constBegin(),
                endNames = allInterfaces.constEnd(); interfaceName != endNames; ++interfaceName) {
            if (!reCheck.exactMatch(*interfaceName))
                continue;
            uniqueTargets.insert(*interfaceName);
        }
    }

    if (command.hash("Parameters").exists()) {
        for (auto param : command.hash("Parameters").toHash()) {
            if (param.first.empty())
                continue;
            ParamaterData add;
            add.name = QString::fromStdString(param.first);
            add.sortPriority = param.second.hash("SortPriority").toInt64();
            add.label = param.second.hash("Name").toDisplayString();
            if (add.label.isEmpty())
                add.label = add.name;
            ExternalSink::simplifyUnicode(add.label);
            add.numberFormat = NumberFormat(param.second.hash("Format").toDisplayString());

            const auto &type = param.second.hash("Type").toString();
            if (Util::equal_insensitive(type, "hex", "hexadecimal")) {
                add.type = ParamaterData::Hex;
                add.value = QVariant(static_cast<qint64>(Variant::Composite::toInteger(
                        param.second.hash("Value"))));
                add.numberFormat.setMode(NumberFormat::Hex);
            } else if (Util::equal_insensitive(type, "bool", "boolean")) {
                add.type = ParamaterData::Boolean;
                add.value = QVariant(param.second.hash("Value").toBool());
            } else if (Util::equal_insensitive(type, "enum", "enumeration")) {
                add.type = ParamaterData::Enum;
                add.value = QVariant(param.second.hash("Value").toQString());

                auto values = param.second.hash("Possible");
                switch (values.getType()) {
                case Variant::Type::String: {
                    for (const auto &v : values.toQString().split(' ')) {
                        add.enumValues.emplace_back(v, v);
                    }
                    break;
                }
                default: {
                    auto list = values.toChildren();
                    for (auto v = list.begin(), endV = list.end(); v != endV; ++v) {
                        QString key = QString::fromStdString(v.stringKey());
                        if (key.isEmpty())
                            key = v.value().toQString();
                        add.enumValues.emplace_back(std::move(key), v.value().toDisplayString());
                    }
                    break;
                }
                }

                if (add.enumValues.empty())
                    continue;

                std::stable_sort(add.enumValues.begin(), add.enumValues.end(),
                                 [](const std::pair<QString, QString> &a,
                                    const std::pair<QString, QString> &b) {
                                     return a.second < b.second;
                                 });
            } else {
                add.type = ParamaterData::Decimal;
                add.value = QVariant(Variant::Composite::toNumber(param.second.hash("Value")));
                add.decimalMinimum = Variant::Composite::toNumber(param.second.hash("Minimum"));
                add.decimalMaximum = Variant::Composite::toNumber(param.second.hash("Maximum"));
            }

            parameters.append(add);
        }

        std::sort(parameters.begin(), parameters.end());
        selectedParameterIndex = parameters.size();
    }


    if (uniqueTargets.size() > 1) {
        selectionState = SelectingTarget;

        MenuHandler *add =
                MenuHandler::createInterfaceSelectionMenu(targets, uniqueTargets, this, client);
        connect(add, SIGNAL(dismissed()), this, SLOT(targetSelectionDismissed()));
        targets.clear();
        client->displayMenu(add);
        return;
    }
    if (targets.size() > 1) {
        targets = uniqueTargets.values();
    }
    if (targets.isEmpty()) {
        selectionState = SelectionCompleted;
        return;
    }

    if (!parameters.isEmpty()) {
        selectionState = SelectingParameters;
        return;
    }


    if (command.hash("Confirm").exists()) {
        selectionState = SelectingConfirm;
        confirmed = false;
        return;
    }


    selectionState = SelectionCompleted;
}

CommandHandler::~CommandHandler()
{ }

void CommandHandler::completeCommand()
{
    Variant::Root data;
    data.write().hash(command.hash("Command").toString()).set(command);
    for (QList<QString>::const_iterator target = targets.constBegin(),
            endTargets = targets.constEnd(); target != endTargets; ++target) {
        client->issueCommand(*target, data);
    }

    selectionState = SelectionCompleted;
    client->displayCommandHandler(NULL);

    qint64 page = command.hash("ChangePage").toInt64();
    if (INTEGER::defined(page) && page >= 0) {
        emit pageChangeRequest((int) page);
    }
}

void CommandHandler::targetSelectionDismissed()
{
    if (selectionState != SelectingTarget)
        return;
    /* If we're still selecting targets (menu aborted) then close the command
     * handler */
    client->displayCommandHandler(NULL);
}

void CommandHandler::targetsSelected(const QList<QString> &t)
{
    targets = t;

    if (!parameters.isEmpty()) {
        selectionState = SelectingParameters;
    } else if (command.hash("Confirm").exists()) {
        selectionState = SelectingConfirm;
        confirmed = false;
    } else {
        completeCommand();
    }
}

static std::vector<
        std::pair<QString, QString>>::const_iterator findEnumSelectedIndex(const QVariant &value,
                                                                           const std::vector<
                                                                                   std::pair<
                                                                                           QString,
                                                                                           QString>> &values)
{
    Q_ASSERT(!values.empty());
    QString sValue(value.toString().toLower());
    for (auto check = values.cbegin(), endCheck = values.cend(); check != endCheck; ++check) {
        if (sValue == check->first.toLower())
            return check;
    }
    return values.cbegin();
}

void CommandHandler::finishEditing()
{
    Q_ASSERT(selectedParameterIndex >= 0 && selectedParameterIndex < parameters.size());

    switch (parameters.at(selectedParameterIndex).type) {
    case ParamaterData::Decimal: {
        QString check(parameterEditingString.toLower());
        if (check == tr("undef") || check == tr("undefined") || check == tr("mvc")) {
            parameters[selectedParameterIndex].value = QVariant(FP::undefined());
            return;
        }
        if (parameterEditingString.isEmpty())
            return;
        bool ok = false;
        double v = parameterEditingString.toDouble(&ok);
        if (!ok)
            return;
        if (FP::defined(parameters.at(selectedParameterIndex).decimalMinimum) &&
                v < parameters.at(selectedParameterIndex).decimalMinimum)
            v = parameters.at(selectedParameterIndex).decimalMinimum;
        if (FP::defined(parameters.at(selectedParameterIndex).decimalMaximum) &&
                v > parameters.at(selectedParameterIndex).decimalMaximum)
            v = parameters.at(selectedParameterIndex).decimalMaximum;
        parameters[selectedParameterIndex].value = QVariant(v);
        break;
    }
    case ParamaterData::Hex: {
        QString check(parameterEditingString.toLower());
        if (check == tr("undef") || check == tr("undefined") || check == tr("mvc")) {
            parameters[selectedParameterIndex].value = QVariant(INTEGER::undefined());
            return;
        }
        if (parameterEditingString.isEmpty())
            return;
        bool ok = false;
        qint64 v = parameterEditingString.toLongLong(&ok, 16);
        if (!ok)
            return;
        parameters[selectedParameterIndex].value = QVariant(v);
        break;
    }
    case ParamaterData::Boolean:
        break;
    case ParamaterData::Enum:
        break;
    }
}

bool CommandHandler::keypress(int ch)
{
    if (selectionState == SelectingTarget)
        return false;
    if (selectionState == SelectionCompleted)
        return false;

    if (selectionState == SelectingParameters) {
        switch (ch) {
        case KEY_UP:
            selectedParameterIndex--;
            while (selectedParameterIndex < 0) {
                selectedParameterIndex += parameters.size() + 1;
            }
            return true;
        case KEY_DOWN:
            selectedParameterIndex++;
            while (selectedParameterIndex > parameters.size()) {
                selectedParameterIndex -= parameters.size() + 1;
            }
            return true;

        case KEY_PPAGE:
            selectedParameterIndex -= LINES - 6;
            if (selectedParameterIndex < 0)
                selectedParameterIndex = 0;
            return true;

        case KEY_NPAGE:
            selectedParameterIndex += LINES - 6;
            if (selectedParameterIndex > parameters.size())
                selectedParameterIndex = parameters.size();
            return true;

        case KEY_LEFT:
        case KEY_BTAB:
            if (selectedParameterIndex < parameters.size()) {
                if (parameters.at(selectedParameterIndex).type == ParamaterData::Enum) {
                    auto it = findEnumSelectedIndex(parameters.at(selectedParameterIndex).value,
                                                    parameters.at(selectedParameterIndex)
                                                              .enumValues);
                    if (it == parameters.at(selectedParameterIndex).enumValues.cbegin()) {
                        parameters[selectedParameterIndex].value = QVariant(
                                parameters.at(selectedParameterIndex).enumValues.back().first);
                    } else {
                        --it;
                        parameters[selectedParameterIndex].value = it->first;
                    }
                }
            }
            return true;

        case KEY_RIGHT:
        case '\t':
            if (selectedParameterIndex < parameters.size()) {
                if (parameters.at(selectedParameterIndex).type == ParamaterData::Enum) {
                    auto it = findEnumSelectedIndex(parameters.at(selectedParameterIndex).value,
                                                    parameters.at(selectedParameterIndex)
                                                              .enumValues);
                    ++it;
                    if (it == parameters.at(selectedParameterIndex).enumValues.cend()) {
                        parameters[selectedParameterIndex].value = QVariant(
                                parameters.at(selectedParameterIndex).enumValues.front().first);
                    } else {
                        parameters[selectedParameterIndex].value = it->first;
                    }
                }
            }
            return true;

        case KEY_END:
            if (selectedParameterIndex < parameters.size()) {
                if (parameters.at(selectedParameterIndex).type == ParamaterData::Enum) {
                    parameters[selectedParameterIndex].value =
                            QVariant(parameters.at(selectedParameterIndex).enumValues.back().first);
                }
            }
            return true;

        case KEY_HOME:
            if (selectedParameterIndex < parameters.size()) {
                if (parameters.at(selectedParameterIndex).type == ParamaterData::Enum) {
                    parameters[selectedParameterIndex].value = QVariant(
                            parameters.at(selectedParameterIndex).enumValues.front().first);
                }
            }
            return true;

        case ' ':
            if (selectedParameterIndex < parameters.size()) {
                if (parameters.at(selectedParameterIndex).type == ParamaterData::Enum) {
                    auto it = findEnumSelectedIndex(parameters.at(selectedParameterIndex).value,
                                                    parameters.at(selectedParameterIndex)
                                                              .enumValues);
                    ++it;
                    if (it == parameters.at(selectedParameterIndex).enumValues.cend()) {
                        parameters[selectedParameterIndex].value = QVariant(
                                parameters.at(selectedParameterIndex).enumValues.front().first);
                    } else {
                        parameters[selectedParameterIndex].value = it->first;
                    }
                } else if (parameters.at(selectedParameterIndex).type == ParamaterData::Boolean) {
                    parameters[selectedParameterIndex].value =
                            QVariant(!parameters.at(selectedParameterIndex).value.toBool());
                }
            }
            return true;

        case 0x001B:
        case 0x0017:
            selectionState = SelectionCompleted;
            targets.clear();
            client->displayCommandHandler(NULL);
            return true;

        case '\n':
        case '\r':
        case KEY_ENTER:
            if (selectedParameterIndex < parameters.size()) {
                if (parameters.at(selectedParameterIndex).type == ParamaterData::Enum) {
                    auto it =
                            findEnumSelectedIndex(parameters.at(selectedParameterIndex).value,
                                                  parameters.at(selectedParameterIndex).enumValues);
                    ++it;
                    if (it == parameters.at(selectedParameterIndex).enumValues.cend()) {
                        parameters[selectedParameterIndex].value = QVariant(
                                parameters.at(selectedParameterIndex).enumValues.front().first);
                    } else {
                        parameters[selectedParameterIndex].value = it->first;
                    }
                } else if (parameters.at(selectedParameterIndex).type == ParamaterData::Boolean) {
                    parameters[selectedParameterIndex].value =
                            QVariant(!parameters.at(selectedParameterIndex).value.toBool());
                } else if (parameters.at(selectedParameterIndex).type == ParamaterData::Hex) {
                    selectionState = SelectingParametersEditing;
                    parameterEditingString = parameters.at(selectedParameterIndex)
                                                       .numberFormat
                                                       .apply(parameters.at(selectedParameterIndex)
                                                                        .value
                                                                        .toLongLong());
                    parameterEditingCursor = parameterEditingString.length();
                } else {
                    selectionState = SelectingParametersEditing;
                    parameterEditingString = parameters.at(selectedParameterIndex)
                                                       .numberFormat
                                                       .apply(parameters.at(selectedParameterIndex)
                                                                        .value
                                                                        .toDouble(), QChar());
                    parameterEditingCursor = parameterEditingString.length();
                }
            } else {
                for (QList<ParamaterData>::const_iterator add = parameters.constBegin(),
                        endAdd = parameters.constEnd(); add != endAdd; ++add) {
                    auto target = command.hash("Parameters").hash(add->name).hash("Value");
                    switch (add->type) {
                    case ParamaterData::Decimal:
                        target.setDouble(add->value.toDouble());
                        break;
                    case ParamaterData::Hex:
                        target.setInt64(add->value.toLongLong());
                        break;
                    case ParamaterData::Boolean:
                        target.setBool(add->value.toBool());
                        break;
                    case ParamaterData::Enum:
                        target.setString(add->value.toString());
                        break;
                    }
                }

                if (command.hash("Confirm").exists()) {
                    selectionState = SelectingConfirm;
                    confirmed = false;
                } else {
                    completeCommand();
                }
            }
            return true;

        default:
            break;
        }
        return false;
    }

    if (selectionState == SelectingParametersEditing) {
        switch (ch) {
        case KEY_UP:
            finishEditing();
            selectionState = SelectingParameters;
            selectedParameterIndex--;
            while (selectedParameterIndex < 0) {
                selectedParameterIndex += parameters.size() + 1;
            }
            return true;
        case KEY_DOWN:
            finishEditing();
            selectionState = SelectingParameters;
            selectedParameterIndex++;
            while (selectedParameterIndex > parameters.size()) {
                selectedParameterIndex -= parameters.size() + 1;
            }
            return true;

        case KEY_PPAGE:
            finishEditing();
            selectionState = SelectingParameters;
            selectedParameterIndex -= LINES - 6;
            if (selectedParameterIndex < 0)
                selectedParameterIndex = 0;
            return true;
        case KEY_NPAGE:
            finishEditing();
            selectionState = SelectingParameters;
            selectedParameterIndex += LINES - 6;
            if (selectedParameterIndex > parameters.size())
                selectedParameterIndex = parameters.size();
            break;

        case KEY_LEFT:
            parameterEditingCursor--;
            if (parameterEditingCursor < 0)
                parameterEditingCursor = 0;
            return true;
        case KEY_RIGHT:
            parameterEditingCursor++;
            if (parameterEditingCursor > parameterEditingString.length())
                parameterEditingCursor = parameterEditingString.length();
            return true;

        case KEY_END:
            parameterEditingCursor = parameterEditingString.length();
            return true;
        case KEY_HOME:
            parameterEditingCursor = 0;
            return true;

        case KEY_BACKSPACE:
            if (parameterEditingCursor == 0 || parameterEditingString.isEmpty())
                break;
            parameterEditingString.remove(parameterEditingCursor - 1, 1);
            --parameterEditingCursor;
            return true;

        case KEY_DC:
            if (parameterEditingString.isEmpty() ||
                    parameterEditingCursor == parameterEditingString.size())
                break;
            parameterEditingString.remove(parameterEditingCursor, 1);
            return true;

        case 0x001B:
        case 0x0017:
            selectionState = SelectionCompleted;
            targets.clear();
            client->displayCommandHandler(NULL);
            return true;

        case '\n':
        case '\r':
        case KEY_ENTER:
            finishEditing();
            selectionState = SelectingParameters;
            return true;

        default: {
            if (!isprint(ch))
                return false;
            QChar add(ch);
            if (!add.isPrint())
                return false;
            parameterEditingString.insert(parameterEditingCursor, add);
            ++parameterEditingCursor;
            return true;
        }
        }
        return false;
    }

    if (selectionState == SelectingConfirm) {
        switch (ch) {
        case KEY_UP:
        case KEY_DOWN:
        case KEY_LEFT:
        case KEY_RIGHT:
        case '\t':
        case KEY_BTAB:
            confirmed = !confirmed;
            return true;

        case KEY_END:
            confirmed = false;
            return true;
        case KEY_HOME:
            confirmed = true;
            return true;

        case 0x001B:
        case 0x0017:
            selectionState = SelectionCompleted;
            targets.clear();
            client->displayCommandHandler(NULL);
            return true;

        case '\n':
        case '\r':
        case KEY_ENTER:
            if (!confirmed) {
                selectionState = SelectionCompleted;
                targets.clear();
                client->displayCommandHandler(NULL);
            } else {
                completeCommand();
            }
            return true;

        default:
            break;
        }
        return false;
    }

    return false;
}

void CommandHandler::render()
{
    if (selectionState == SelectingTarget)
        return;
    if (targets.isEmpty()) {
        client->displayCommandHandler(NULL);
        return;
    }

    if (selectionState == SelectionCompleted) {
        if (!targets.isEmpty()) {
            completeCommand();
        } else {
            client->displayCommandHandler(NULL);
        }
        return;
    }

    if (selectionState == SelectingParameters || selectionState == SelectingParametersEditing) {
        int width = 0;
        int height = 0;
        for (QList<ParamaterData>::const_iterator add = parameters.constBegin(),
                endAdd = parameters.constEnd(); add != endAdd; ++add, ++height) {
            switch (add->type) {
            case ParamaterData::Decimal:
                width = qMax(width, tr("%1: ", "basic label").arg(add->label).length() +
                        qMax(15, add->numberFormat.apply(add->value.toDouble(), QChar()).length()));
                break;
            case ParamaterData::Hex:
                width = qMax(width, tr("%1: ", "basic label").arg(add->label).length() +
                        qMax(15, add->numberFormat.apply(add->value.toLongLong()).length()));
                break;
            case ParamaterData::Boolean:
                width = qMax(width, tr("[ ] %1", "boolean false label").arg(add->label).length());
                width = qMax(width, tr("[X] %1", "boolean true label").arg(add->label).length());
                break;
            case ParamaterData::Enum: {
                for (const auto &value : add->enumValues) {
                    width = qMax(width,
                                 tr("%1:   %2  ", "enum unselected").arg(add->label, value.second)
                                                                    .length());
                    width = qMax(width,
                                 tr("%1: < %2 >", "enum selected").arg(add->label, value.second)
                                                                  .length());
                }
                break;
            }
            }
        }
        if (selectionState == SelectingParametersEditing &&
                selectedParameterIndex < parameters.size()) {
            width = qMax(width,
                         tr("%1: ", "basic label").arg(parameters.at(selectedParameterIndex).label)
                                                  .length() + parameterEditingString.length());
        }

        QString acceptString(command.hash("ParametersAccept").toDisplayString());
        if (acceptString.isEmpty())
            acceptString = tr("Accept", "parameters accept");
        width = qMax(width, acceptString.length() + 2);
        ++height;

        int scrollOffset;
        if (height > LINES - 6) {
            height = LINES - 6;
            scrollOffset = selectedParameterIndex - (height - 1);

            if (scrollOffset < 0)
                scrollOffset = 0;
        } else {
            scrollOffset = 0;
        }
        if (width > COLS - 4)
            width = COLS - 4;

        WINDOW *win = subwin(stdscr, height + 2, width + 2, (LINES - (height + 2)) / 2,
                             (COLS - (width + 2)) / 2);
        if (win == NULL)
            return;
        wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
        wbkgdset(win, ' ' | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
        werase(win);

        int index = scrollOffset;
        int row = 1;
        for (QList<ParamaterData>::const_iterator add = parameters.constBegin() + scrollOffset,
                endAdd = parameters.constEnd();
                add != endAdd && row <= height;
                ++add, ++index, ++row) {
            if (index != selectedParameterIndex) {
                wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
            } else if (selectionState == SelectingParametersEditing) {
                wattrset(win, A_BOLD | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
                QString prefix(tr("%1: ", "basic label").arg(add->label));
                mvwaddstr(win, row, 1, prefix.toUtf8().constData());

                int textLength = prefix.length();
                int textOffset = width - (textLength + parameterEditingCursor + 1);
                if (textOffset > 0)
                    textOffset = 0;
                int nChr = parameterEditingString.length();
                for (int idx = 0; idx < nChr; ++idx, ++textOffset) {
                    if (textOffset < 0)
                        continue;
                    int xPos = textLength + textOffset;
                    if (xPos > width)
                        break;

                    if (idx == parameterEditingCursor) {
                        wattrset(win, A_REVERSE | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
                    } else {
                        wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
                    }

                    mvwaddch(win, row, xPos + 1, parameterEditingString.at(idx).unicode());
                }
                if (nChr == parameterEditingCursor) {
                    int xPos = textLength + textOffset + 1;
                    if (xPos > 0 && xPos <= width) {
                        wattrset(win, A_REVERSE | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
                        mvwaddch(win, row, xPos, ' ');
                    }
                }
                continue;
            } else {
                wattrset(win, A_REVERSE | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
            }

            QString line;
            switch (add->type) {
            case ParamaterData::Decimal:
                line = tr("%1: ", "basic label").arg(add->label);
                line.append(add->numberFormat.apply(add->value.toDouble(), QChar()));
                break;
            case ParamaterData::Hex:
                line = tr("%1: ", "basic label").arg(add->label);
                line.append(add->numberFormat.apply(add->value.toLongLong()));
                break;
            case ParamaterData::Boolean:
                if (add->value.toBool()) {
                    line = tr("[X] %1", "boolean true label").arg(add->label);
                } else {
                    line = tr("[ ] %1", "boolean false label").arg(add->label);
                }
                break;
            case ParamaterData::Enum: {
                auto it = findEnumSelectedIndex(add->value, add->enumValues);
                if (index != selectedParameterIndex) {
                    line = tr("%1:   %2  ", "enum unselected").arg(add->label, it->second);
                } else {
                    line = tr("%1: < %2 >", "enum selected").arg(add->label, it->second);
                }
                break;
            }
            }

            line = line.leftJustified(width);
            mvwaddstr(win, row, 1, line.toUtf8().constData());
        }
        if (index >= parameters.size() && row <= height) {
            if (index != selectedParameterIndex) {
                wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
            } else {
                wattrset(win, A_REVERSE | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
            }

            acceptString = acceptString.leftJustified((width + acceptString.length()) / 2);
            acceptString = acceptString.rightJustified(width);
            mvwaddstr(win, row, 1, acceptString.toUtf8().constData());
        }

        wattrset(win, A_BOLD | COLOR_PAIR(Client::COLOR_YELLOW_BLUE));
        box(win, 0, 0);
        wnoutrefresh(win);
        delwin(win);
        return;
    }

    if (selectionState == SelectingConfirm) {
        QString confirmText(command.hash("Confirm").toDisplayString());
        if (confirmText.isEmpty())
            confirmText = tr("Continue?", "default confirm");
        QString yesText(command.hash("ConfirmYes").toDisplayString());
        if (yesText.isEmpty())
            yesText = tr("Yes", "confirm yes default");
        QString noText(command.hash("ConfirmNo").toDisplayString());
        if (noText.isEmpty())
            noText = tr("No", "confirm no default");

        int width = qMax(confirmText.length(), yesText.length() + noText.length() + 4);
        if (width > COLS - 4)
            width = COLS - 4;

        WINDOW *win = subwin(stdscr, 4, width + 2, (LINES - 4) / 2, (COLS - (width + 2)) / 2);
        if (win == NULL)
            return;
        wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
        wbkgdset(win, ' ' | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
        werase(win);

        mvwaddstr(win, 1, 1, confirmText.toUtf8().constData());

        int extraSpace = width - (yesText.length() + noText.length());
        int yesSpace = extraSpace / 2;
        int noSpace = extraSpace - yesSpace;

        yesSpace += yesText.length();
        yesText = yesText.leftJustified((yesSpace + yesText.length()) / 2);
        yesText = yesText.rightJustified(yesSpace);

        noSpace += noText.length();
        noText = noText.leftJustified((noSpace + noText.length()) / 2);
        noText = noText.rightJustified(noSpace);

        if (!confirmed) {
            wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
            mvwaddstr(win, 2, 1, yesText.toUtf8().constData());
            wattrset(win, A_REVERSE | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
            mvwaddstr(win, 2, 1 + yesSpace, noText.toUtf8().constData());
        } else {
            wattrset(win, A_REVERSE | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
            mvwaddstr(win, 2, 1, yesText.toUtf8().constData());
            wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
            mvwaddstr(win, 2, 1 + yesSpace, noText.toUtf8().constData());
        }

        wattrset(win, A_BOLD | COLOR_PAIR(Client::COLOR_YELLOW_BLUE));
        box(win, 0, 0);
        wnoutrefresh(win);
        delwin(win);
        return;
    }
}
