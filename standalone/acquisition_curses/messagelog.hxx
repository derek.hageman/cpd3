/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef MESSAGELOG_H
#define MESSAGELOG_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QList>
#include <QSettings>

class Client;

/**
 * The message log entry dialog.
 */
class MessageLog : public QObject {
Q_OBJECT

    Client *parent;
    QSettings settings;

    QString text;
    QString author;

    enum {
        EditingText, EditingAuthor
    } selected;
    int editingCursor;

public:
    /**
     * Create the message log entry.
     */
    MessageLog(Client *p);

    ~MessageLog();

    /**
     * Handle a keypress when the dialog is shown.
     * 
     * @param ch    the key
     * @return      true if the key was used.
     */
    bool keypress(int ch);

    /**
     * Render the dialog.
     */
    void render();

signals:

    /**
     * Emitted when the dialog is dismissed, after any action has been taken.
     */
    void dismissed();
};

#endif
