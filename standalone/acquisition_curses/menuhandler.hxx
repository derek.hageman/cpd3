/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef MENUHANDLER_H
#define MENUHANDLER_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QList>

#include "datacore/stream.hxx"
#include "datacore/variant/root.hxx"
#include "acquisition/realtimelayout.hxx"
#include "acquisition/commandmanager.hxx"

class Client;

class MenuHandlerEntry;

class GenericWindow;

class InterfaceHandler;

class CommandHandler;

/**
 * A class that can draw a menu to select on of various actions.
 */
class MenuHandler : public QObject {
Q_OBJECT

    QList<MenuHandlerEntry *> entries;
    int selectedIndex;

    void calculateDimensions(int &rows, int &cols);

    MenuHandler();

public:
    ~MenuHandler();

    /**
     * Create the main menu handler.
     * 
     * @param parent    the client
     * @return a menu handler
     */
    static MenuHandler *createMainMenu(Client *parent);

    /**
     * Create a command handler for a generic window's menu.
     * 
     * @param window    the window
     * @param client    the client
     * @return a menu handler
     */
    static MenuHandler *createGenericWindowMenu(GenericWindow *window, Client *client);

    /**
     * Create a command handler for an interface handler menu.
     * 
     * @param interface the interface
     * @param client    the client
     * @return a menu handler
     */
    static MenuHandler *createInterfaceWindowMenu(InterfaceHandler *interface, Client *client);

    /**
     * Create an interface selection menu.
     * 
     * @param all       the list of interfaces to use for the "all" option
     * @param unique    the list of unique interface names that can be selected
     * @param cmd       the command handler
     * @param client    the client
     * @return a menu handler
     */
    static MenuHandler *createInterfaceSelectionMenu(const QList<QString> &all,
                                                     const QSet<QString> &unique,
                                                     CommandHandler *cmd,
                                                     Client *client);

    /**
     * Process a key press.
     * 
     * @param ch    the key
     */
    bool keypress(int ch);

    /**
     * Draw the menu.
     */
    void render();

signals:

    /**
     * Emitted when the menu is dismissed, after the action has been taken.
     */
    void dismissed();
};

#endif
