/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/timeparse.hxx"

#include "acquisition_curses.hxx"
#include "systemstatus.hxx"
#include "interfacehandler.hxx"

/* Last because it defines some weird things (like "timeout") */
#include <curses.h>

using namespace CPD3;
using namespace CPD3::Data;

SystemStatus::SystemStatus(Client *p) : QObject(p),
                                        parent(p),
                                        selection(Close),
                                        averagingText(),
                                        confirmAveragingUnit(averagingUnit),
                                        confirmAveragingCount(averagingCount),
                                        confirmAveragingAlign(averagingAlign),
                                        editingCursor(0),
                                        autoprobeState()
{
    averagingText = getAveragingText(averagingUnit, averagingCount, averagingAlign);
}

SystemStatus::~SystemStatus() = default;

Time::LogicalTimeUnit SystemStatus::averagingUnit = Time::Minute;
int SystemStatus::averagingCount = 1;
bool SystemStatus::averagingAlign = true;

QString SystemStatus::getAveragingText(Time::LogicalTimeUnit unit, int count, bool align)
{
    if (count <= 0)
        return "None";

    QString text(QString::number(count));
    switch (unit) {
    case Time::Millisecond:
        if (averagingCount > 1)
            text.append(tr(" Milliseconds"));
        else
            text.append(tr(" Millisecond"));
        break;
    case Time::Second:
        if (averagingCount > 1)
            text.append(tr(" Seconds"));
        else
            text.append(tr(" Second"));
        break;
    case Time::Minute:
        if (averagingCount > 1)
            text.append(tr(" Minutes"));
        else
            text.append(tr(" Minute"));
        break;
    case Time::Hour:
        if (averagingCount > 1)
            text.append(tr(" Hours"));
        else
            text.append(tr(" Hour"));
        break;
    case Time::Day:
        if (averagingCount > 1)
            text.append(tr(" Days"));
        else
            text.append(tr(" Day"));
        break;
    case Time::Week:
        if (averagingCount > 1)
            text.append(tr(" Weeks"));
        else
            text.append(tr(" Week"));
        break;
    case Time::Month:
        if (averagingCount > 1)
            text.append(tr(" Months"));
        else
            text.append(tr(" Month"));
        break;
    case Time::Quarter:
        if (averagingCount > 1)
            text.append(tr(" Quarters"));
        else
            text.append(tr(" Quarter"));
        break;
    case Time::Year:
        if (averagingCount > 1)
            text.append(tr(" Years"));
        else
            text.append(tr(" Year"));
        break;
    }

    if (!align) {
        text.append(tr(" NoAlign"));
    }
    return text;
}

void SystemStatus::parseAveraging()
{
    try {
        confirmAveragingUnit = averagingUnit;
        confirmAveragingCount = averagingCount;
        confirmAveragingAlign = averagingAlign;
        TimeParse::parseOffset(averagingText, &confirmAveragingUnit, &confirmAveragingCount,
                               &confirmAveragingAlign, true, false);

        selection = AveragingChangeConfirmNo;
    } catch (TimeParsingException tpe) {
    }
    averagingText = getAveragingText(averagingUnit, averagingCount, averagingAlign);
}

SystemStatus::AutoprobeState::AutoprobeState() : activeTry(0),
                                                 activeTotalTries(0),
                                                 otherCandidates(0)
{ }

SystemStatus::AutoprobeState::AutoprobeState(const CPD3::Data::Variant::Read &info) : physical(
        info["Name"].toDisplayString()), activeName(info["Current/Instrument"].toDisplayString())
{
    if (activeName.isEmpty())
        activeName = info["Current/Component"].toQString();

    auto i = info["Current/Try"].toInteger();
    if (!INTEGER::defined(i))
        i = 0;
    activeTry = static_cast<int>(i);

    i = info["Current/MaximumTries"].toInteger();
    if (!INTEGER::defined(i))
        i = activeTry;
    activeTotalTries = static_cast<int>(i);

    otherCandidates = static_cast<int>(info["Pending"].toArray().size());
}

void SystemStatus::updateAutoprobeState(const CPD3::Data::Variant::Read &state)
{
    autoprobeState.clear();
    if (state["Active"].toBoolean()) {
        for (const auto &ap : state["Interfaces"].toArray()) {
            autoprobeState.emplace_back(ap);
        }
    }

    if (autoprobeState.empty()) {
        if (selection == AutoprobeDetails)
            selection = Autoprobe;
        return;
    }

    std::stable_sort(autoprobeState.begin(), autoprobeState.end(),
                     [](const AutoprobeState &a, const AutoprobeState &b) {
                         if (a.physical.isEmpty()) {
                             if (!b.physical.isEmpty())
                                 return false;
                         } else if (b.physical.isEmpty()) {
                             return true;
                         }
                         return a.physical < b.physical;
                     });
}

bool SystemStatus::keypress(int ch)
{
    switch (ch) {
    case KEY_UP:
        switch (selection) {
        case Averaging:
            selection = Close;
            return true;
        case AveragingEditing:
            selection = Close;
            parseAveraging();
            return true;

        case ContaminateSet:
        case ContaminateClear:
        case ContaminateClearAll:
            selection = Averaging;
            return true;

        case BypassSet:
            selection = ContaminateSet;
            return true;
        case BypassClear:
            selection = ContaminateClear;
            return true;
        case BypassClearAll:
            selection = ContaminateClearAll;
            return true;

        case Autoprobe:
            selection = BypassSet;
            return true;

        case Restart:
            selection = Autoprobe;
            return true;

        case Close:
            selection = Restart;
            return true;

        case RestartConfirmYes:
        case RestartConfirmNo:
        case ContaminateClearAllConfirmYes:
        case ContaminateClearAllConfirmNo:
        case BypassClearAllConfirmYes:
        case BypassClearAllConfirmNo:
        case AveragingChangeConfirmYes:
        case AveragingChangeConfirmNo:
        case AutoprobeDetails:
            return true;
        }
        break;

    case KEY_DOWN:
        switch (selection) {
        case Averaging:
            selection = ContaminateSet;
            return true;
        case AveragingEditing:
            selection = ContaminateSet;
            parseAveraging();
            return true;

        case ContaminateSet:
            selection = BypassSet;
            return true;
        case ContaminateClear:
            selection = BypassClear;
            return true;
        case ContaminateClearAll:
            selection = BypassClearAll;
            return true;

        case BypassSet:
        case BypassClear:
        case BypassClearAll:
            selection = Autoprobe;
            return true;

        case Autoprobe:
            selection = Restart;
            return true;

        case Restart:
            selection = Close;
            return true;

        case Close:
            selection = Averaging;
            return true;

        case RestartConfirmYes:
        case RestartConfirmNo:
        case ContaminateClearAllConfirmYes:
        case ContaminateClearAllConfirmNo:
        case BypassClearAllConfirmYes:
        case BypassClearAllConfirmNo:
        case AveragingChangeConfirmYes:
        case AveragingChangeConfirmNo:
        case AutoprobeDetails:
            return true;
        }
        return true;

    case KEY_LEFT:
        switch (selection) {
        case Averaging:
            break;
        case AveragingEditing:
            editingCursor--;
            if (editingCursor < 0)
                editingCursor = 0;
            return true;

        case ContaminateSet:
            selection = ContaminateClearAll;
            return true;
        case ContaminateClear:
            selection = ContaminateSet;
            return true;
        case ContaminateClearAll:
            selection = ContaminateClear;
            return true;

        case BypassSet:
            selection = BypassClearAll;
            return true;
        case BypassClear:
            selection = BypassSet;
            return true;
        case BypassClearAll:
            selection = BypassClear;
            return true;

        case Autoprobe:
        case Restart:
        case Close:
            return true;

        case RestartConfirmYes:
            selection = RestartConfirmNo;
            return true;
        case RestartConfirmNo:
            selection = RestartConfirmYes;
            return true;

        case ContaminateClearAllConfirmYes:
            selection = ContaminateClearAllConfirmNo;
            return true;
        case ContaminateClearAllConfirmNo:
            selection = ContaminateClearAllConfirmYes;
            return true;

        case BypassClearAllConfirmYes:
            selection = BypassClearAllConfirmNo;
            return true;
        case BypassClearAllConfirmNo:
            selection = BypassClearAllConfirmYes;
            return true;

        case AveragingChangeConfirmYes:
            selection = AveragingChangeConfirmNo;
            return true;
        case AveragingChangeConfirmNo:
            selection = AveragingChangeConfirmYes;
            return true;

        case AutoprobeDetails:
            return true;
        }
        return true;
    case KEY_RIGHT:
        switch (selection) {
        case Averaging:
            break;
        case AveragingEditing:
            editingCursor++;
            if (editingCursor > averagingText.length())
                editingCursor = averagingText.length();
            return true;

        case ContaminateSet:
            selection = ContaminateClear;
            return true;
        case ContaminateClear:
            selection = ContaminateClearAll;
            return true;
        case ContaminateClearAll:
            selection = ContaminateSet;
            return true;

        case BypassSet:
            selection = BypassClear;
            return true;
        case BypassClear:
            selection = BypassClearAll;
            return true;
        case BypassClearAll:
            selection = BypassSet;
            return true;

        case Autoprobe:
        case Restart:
        case Close:
            return true;

        case RestartConfirmYes:
            selection = RestartConfirmNo;
            return true;
        case RestartConfirmNo:
            selection = RestartConfirmYes;
            return true;

        case ContaminateClearAllConfirmYes:
            selection = ContaminateClearAllConfirmNo;
            return true;
        case ContaminateClearAllConfirmNo:
            selection = ContaminateClearAllConfirmYes;
            return true;

        case BypassClearAllConfirmYes:
            selection = BypassClearAllConfirmNo;
            return true;
        case BypassClearAllConfirmNo:
            selection = BypassClearAllConfirmYes;
            return true;

        case AveragingChangeConfirmYes:
            selection = AveragingChangeConfirmNo;
            return true;
        case AveragingChangeConfirmNo:
            selection = AveragingChangeConfirmYes;
            return true;

        case AutoprobeDetails:
            break;
        }
        return true;

    case KEY_END:
        switch (selection) {
        case Averaging:
            break;
        case AveragingEditing:
            editingCursor = averagingText.length();
            return true;

        case ContaminateSet:
        case ContaminateClear:
        case ContaminateClearAll:
            selection = ContaminateClearAll;
            return true;

        case BypassSet:
        case BypassClear:
        case BypassClearAll:
            selection = BypassClearAll;
            return true;

        case Autoprobe:
        case Restart:
        case Close:
            return true;

        case RestartConfirmYes:
        case RestartConfirmNo:
            selection = RestartConfirmNo;
            return true;

        case ContaminateClearAllConfirmYes:
        case ContaminateClearAllConfirmNo:
            selection = ContaminateClearAllConfirmNo;
            return true;

        case BypassClearAllConfirmYes:
        case BypassClearAllConfirmNo:
            selection = BypassClearAllConfirmNo;
            return true;

        case AveragingChangeConfirmYes:
        case AveragingChangeConfirmNo:
            selection = AveragingChangeConfirmNo;
            return true;

        case AutoprobeDetails:
            break;
        }
        return true;

    case KEY_HOME:
        switch (selection) {
        case Averaging:
            break;
        case AveragingEditing:
            editingCursor = 0;
            return true;

        case ContaminateSet:
        case ContaminateClear:
        case ContaminateClearAll:
            selection = ContaminateSet;
            return true;

        case BypassSet:
        case BypassClear:
        case BypassClearAll:
            selection = BypassSet;
            return true;

        case Autoprobe:
        case Restart:
        case Close:
            return true;

        case RestartConfirmYes:
        case RestartConfirmNo:
            selection = RestartConfirmYes;
            return true;

        case ContaminateClearAllConfirmYes:
        case ContaminateClearAllConfirmNo:
            selection = ContaminateClearAllConfirmYes;
            return true;

        case BypassClearAllConfirmYes:
        case BypassClearAllConfirmNo:
            selection = BypassClearAllConfirmYes;
            return true;

        case AveragingChangeConfirmYes:
        case AveragingChangeConfirmNo:
            selection = AveragingChangeConfirmYes;
            return true;

        case AutoprobeDetails:
            break;
        }
        return true;

    case KEY_BACKSPACE:
        switch (selection) {
        case AveragingEditing:
            if (editingCursor == 0 || averagingText.isEmpty())
                return true;
            averagingText.remove(editingCursor - 1, 1);
            --editingCursor;
            return true;
        case Averaging:
        case ContaminateSet:
        case ContaminateClear:
        case ContaminateClearAll:
        case BypassSet:
        case BypassClear:
        case BypassClearAll:
        case Autoprobe:
        case Restart:
        case Close:
        case RestartConfirmYes:
        case RestartConfirmNo:
        case ContaminateClearAllConfirmYes:
        case ContaminateClearAllConfirmNo:
        case BypassClearAllConfirmYes:
        case BypassClearAllConfirmNo:
        case AveragingChangeConfirmYes:
        case AveragingChangeConfirmNo:
        case AutoprobeDetails:
            return true;
        }
        return true;

    case KEY_DC:
        switch (selection) {
        case AveragingEditing:
            if (averagingText.isEmpty() || editingCursor == averagingText.size())
                return true;
            averagingText.remove(editingCursor, 1);
            return true;
        case Averaging:
        case ContaminateSet:
        case ContaminateClear:
        case ContaminateClearAll:
        case BypassSet:
        case BypassClear:
        case BypassClearAll:
        case Autoprobe:
        case Restart:
        case Close:
        case RestartConfirmYes:
        case RestartConfirmNo:
        case ContaminateClearAllConfirmYes:
        case ContaminateClearAllConfirmNo:
        case BypassClearAllConfirmYes:
        case BypassClearAllConfirmNo:
        case AveragingChangeConfirmYes:
        case AveragingChangeConfirmNo:
        case AutoprobeDetails:
            return true;
        }
        return true;

    case 0x001B:
    case 0x0017:
        emit dismissed();
        return true;

    case '\n':
    case '\r':
    case KEY_ENTER:
        switch (selection) {
        case Averaging:
            selection = AveragingEditing;
            editingCursor = averagingText.length();
            break;
        case AveragingEditing:
            selection = Averaging;
            parseAveraging();
            return true;

        case ContaminateSet:
            parent->getConnection()->systemFlagSet("Contaminated");
            return true;
        case ContaminateClear:
            parent->getConnection()->systemFlagClear("Contaminated");
            return true;
        case ContaminateClearAll:
            selection = ContaminateClearAllConfirmNo;
            return true;

        case BypassSet:
            parent->getConnection()->bypassFlagSet();
            return true;
        case BypassClear:
            parent->getConnection()->bypassFlagClear();
            return true;
        case BypassClearAll:
            selection = BypassClearAllConfirmNo;
            return true;

        case Autoprobe:
            if (!autoprobeState.empty())
                selection = AutoprobeDetails;
            return true;

        case Restart:
            selection = RestartConfirmNo;
            return true;

        case Close:
            emit dismissed();
            return true;

        case RestartConfirmYes:
            parent->getConnection()->restartRequested();
            selection = Restart;
            emit dismissed();
            return true;
        case RestartConfirmNo:
            selection = Restart;
            return true;

        case ContaminateClearAllConfirmYes:
            parent->getConnection()->systemFlagClearAll();
            selection = ContaminateClearAll;
            return true;
        case ContaminateClearAllConfirmNo:
            selection = ContaminateClearAll;
            return true;

        case BypassClearAllConfirmYes:
            parent->getConnection()->bypassFlagClearAll();
            selection = BypassClearAll;
            return true;
        case BypassClearAllConfirmNo:
            selection = BypassClearAll;
            return true;

        case AveragingChangeConfirmYes:
            averagingUnit = confirmAveragingUnit;
            averagingCount = confirmAveragingCount;
            averagingAlign = confirmAveragingAlign;
            averagingText = getAveragingText(confirmAveragingUnit, confirmAveragingCount,
                                             confirmAveragingAlign);
            parent->getConnection()
                  ->setAveragingTime(confirmAveragingUnit, confirmAveragingCount,
                                     confirmAveragingAlign);
            selection = Averaging;
            return true;
        case AveragingChangeConfirmNo:
            selection = Averaging;
            return true;

        case AutoprobeDetails:
            selection = Autoprobe;
            return true;
        }
        return true;

    default:
        if (selection == AveragingEditing) {
            if (!isprint(ch))
                break;
            QChar add(ch);
            if (!add.isPrint())
                break;
            averagingText.insert(editingCursor, add);
            ++editingCursor;
        }
        break;
    }

    return false;
}

static void centerText(QString &text, int length)
{
    text = text.leftJustified((length + text.length()) / 2);
    text = text.rightJustified(length);
}

void SystemStatus::renderMainWindow()
{
    QVector<int> columnWidths(4, 0);

    columnWidths[0] = qMax(columnWidths[0], tr("Averaging: ").length());
    columnWidths[0] = qMax(columnWidths[0], tr("Contaminated: ").length());
    columnWidths[0] = qMax(columnWidths[0], tr("Not Contaminated: ").length());
    columnWidths[0] = qMax(columnWidths[0], tr("Bypassed: ").length());
    columnWidths[0] = qMax(columnWidths[0], tr("Not Bypassed: ").length());

    columnWidths[1] = qMax(columnWidths[1], tr(" SET ").length());
    columnWidths[2] = qMax(columnWidths[2], tr(" CLEAR ").length());
    columnWidths[3] = qMax(columnWidths[3], tr(" OVERRIDE ").length());

    columnWidths[1] = qMax(columnWidths[1], tr(" BYPASS ").length());
    columnWidths[2] = qMax(columnWidths[2], tr(" UNBYPASS ").length());
    columnWidths[3] = qMax(columnWidths[3], tr(" OVERRIDE ").length());

    int requiredWidth = columnWidths[0] + 20;
    requiredWidth = qMax(requiredWidth, tr("CLOSE").length());
    requiredWidth = qMax(requiredWidth, tr("RESTART ACQUISITION SYSTEM").length());

    QString autoprobeText;
    if (autoprobeState.empty()) {
        autoprobeText = tr("AUTODETECTION COMPLETE");
    } else {
        autoprobeText =
                tr("AUTODETECTION IN PROGRESS (%1)").arg(static_cast<int>(autoprobeState.size()));
    }
    requiredWidth = qMax(requiredWidth, autoprobeText.length());

    int totalWidth = 0;
    for (QVector<int>::const_iterator add = columnWidths.constBegin(),
            end = columnWidths.constEnd(); add != end; ++add) {
        totalWidth += *add;
    }
    if (totalWidth < requiredWidth) {
        int nAdd = requiredWidth - totalWidth;
        int nFirst = nAdd / 3;
        int nSecond = nAdd / 3;
        int nThird = nAdd - (nFirst + nSecond);
        columnWidths[1] += nFirst;
        columnWidths[2] += nSecond;
        columnWidths[3] += nThird;
    }

    int width = 0;
    for (QVector<int>::const_iterator add = columnWidths.constBegin(),
            end = columnWidths.constEnd(); add != end; ++add) {
        width += *add;
    }
    width = qMax(width, requiredWidth);

    int height = 6;

    WINDOW *win = subwin(stdscr, height + 2, width + 2, (LINES - (height + 2)) / 2,
                         (COLS - (width + 2)) / 2);
    if (win == NULL)
        return;
    wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    wbkgdset(win, ' ' | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    werase(win);


    bool systemBypassed = false;
    bool systemContaminated = false;
    QHash<QString, InterfaceHandler *> interfaces(parent->getInterfaces());
    for (QHash<QString, InterfaceHandler *>::const_iterator add = interfaces.constBegin(),
            endAdd = interfaces.constEnd(); add != endAdd; ++add) {
        systemBypassed = systemBypassed | add.value()->isBypassed();
        systemContaminated = systemContaminated | add.value()->isContaminated();
    }

    int row = 1;


    QString text(tr("Averaging: "));
    text = text.rightJustified(columnWidths[0]);
    int textLength = text.length();
    if (selection == Averaging) {
        wattrset(win, A_REVERSE | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
        mvwaddstr(win, row, 1, text.toUtf8().constData());
        text = averagingText;
        text.truncate(width - textLength);
        mvwaddstr(win, row, 1 + textLength, text.toUtf8().constData());
    } else if (selection == AveragingEditing) {
        wattrset(win, A_BOLD | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
        mvwaddstr(win, row, 1, text.toUtf8().constData());

        int textOffset = width - (textLength + editingCursor + 1);
        if (textOffset > 0)
            textOffset = 0;
        int nChr = averagingText.length();
        for (int idx = 0; idx < nChr; ++idx, ++textOffset) {
            if (textOffset < 0)
                continue;
            int xPos = textLength + textOffset;
            if (xPos > width)
                break;

            if (idx == editingCursor)
                wattrset(win, A_REVERSE | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
            else
                wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));

            mvwaddch(win, row, xPos + 1, averagingText.at(idx).unicode());
        }
        if (nChr == editingCursor) {
            int xPos = textLength + textOffset + 1;
            if (xPos > 0 && xPos <= width) {
                wattrset(win, A_REVERSE | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
                mvwaddch(win, row, xPos, ' ');
            }
        }
    } else {
        wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
        mvwaddstr(win, row, 1, text.toUtf8().constData());
        text = averagingText;
        text.truncate(width - textLength);
        mvwaddstr(win, row, 1 + textLength, text.toUtf8().constData());
    }
    ++row;


    if (systemContaminated) {
        text = tr("Contaminated: ");
        wattrset(win, COLOR_PAIR(Client::COLOR_RED_BLUE));
    } else {
        text = tr("Not Contaminated: ");
        wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    }
    text = text.rightJustified(columnWidths[0]);
    mvwaddstr(win, row, 1, text.toUtf8().constData());
    textLength = text.length() + 1;

    text = tr(" SET ");
    centerText(text, columnWidths[1]);
    if (selection == ContaminateSet)
        wattrset(win, A_REVERSE | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    else
        wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    mvwaddstr(win, row, textLength, text.toUtf8().constData());
    textLength += text.length();

    text = tr(" CLEAR ");
    centerText(text, columnWidths[2]);
    if (selection == ContaminateClear)
        wattrset(win, A_REVERSE | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    else
        wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    mvwaddstr(win, row, textLength, text.toUtf8().constData());
    textLength += text.length();

    text = tr(" OVERRIDE ");
    centerText(text, columnWidths[3]);
    if (selection == ContaminateClearAll)
        wattrset(win, A_REVERSE | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    else
        wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    mvwaddstr(win, row, textLength, text.toUtf8().constData());
    ++row;


    if (systemBypassed) {
        text = tr("Bypassed: ");
        wattrset(win, COLOR_PAIR(Client::COLOR_RED_BLUE));
    } else {
        text = tr("Not Bypassed: ");
        wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    }
    text = text.rightJustified(columnWidths[0]);
    mvwaddstr(win, row, 1, text.toUtf8().constData());
    textLength = text.length() + 1;

    text = tr(" BYPASS ");
    centerText(text, columnWidths[1]);
    if (selection == BypassSet)
        wattrset(win, A_REVERSE | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    else
        wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    mvwaddstr(win, row, textLength, text.toUtf8().constData());
    textLength += text.length();

    text = tr(" UNBYPASS ");
    centerText(text, columnWidths[2]);
    if (selection == BypassClear)
        wattrset(win, A_REVERSE | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    else
        wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    mvwaddstr(win, row, textLength, text.toUtf8().constData());
    textLength += text.length();

    text = tr(" OVERRIDE ");
    centerText(text, columnWidths[3]);
    if (selection == BypassClearAll)
        wattrset(win, A_REVERSE | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    else
        wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    mvwaddstr(win, row, textLength, text.toUtf8().constData());
    ++row;


    text = autoprobeText;
    centerText(text, width);
    if (selection == Autoprobe)
        wattrset(win, A_REVERSE | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    else
        wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    mvwaddstr(win, row++, 1, text.toUtf8().constData());


    text = tr("RESTART ACQUISITION SYSTEM");
    centerText(text, width);
    if (selection == Restart)
        wattrset(win, A_REVERSE | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    else
        wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    mvwaddstr(win, row++, 1, text.toUtf8().constData());


    text = tr("CLOSE");
    centerText(text, width);
    if (selection == Close)
        wattrset(win, A_REVERSE | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    else
        wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    mvwaddstr(win, row++, 1, text.toUtf8().constData());


    wattrset(win, A_BOLD | COLOR_PAIR(Client::COLOR_YELLOW_BLUE));
    box(win, 0, 0);
    text = tr("System Status");
    mvwaddstr(win, 0, ((width + 2) - text.length()) / 2, text.toUtf8().constData());
    wnoutrefresh(win);
    delwin(win);
}

void SystemStatus::renderGenericConfirm(const QStringList &confirmText,
                                        const QString &yesText,
                                        const QString &noText,
                                        bool yesSelected)
{
    int width = 0;
    for (QList<QString>::const_iterator add = confirmText.constBegin(),
            end = confirmText.constEnd(); add != end; ++add) {
        width = qMax(width, add->length());
    }
    width = qMax(width, yesText.length() + noText.length() + 4);

    int height = confirmText.size() + 1;

    WINDOW *win = subwin(stdscr, height + 2, width + 2, (LINES - (height + 2)) / 2,
                         (COLS - (width + 2)) / 2);
    if (win == NULL)
        return;
    wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    wbkgdset(win, ' ' | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    werase(win);

    int row = 1;
    for (QList<QString>::const_iterator add = confirmText.constBegin(),
            end = confirmText.constEnd(); add != end; ++add) {
        mvwaddstr(win, row++, 1, add->toUtf8().constData());
    }

    int addSpace = width - (yesText.length() + noText.length());
    int addYesSpace = addSpace / 2;
    int addNoSpace = addSpace - addYesSpace;

    QString text(yesText);
    if (addYesSpace > 0) {
        int addBefore = addYesSpace / 2;
        int addAfter = addYesSpace - addBefore;
        text.prepend(QString(addBefore, ' '));
        text.append(QString(addAfter, ' '));
    }
    if (yesSelected) {
        wattrset(win, A_REVERSE | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    } else {
        wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    }
    mvwaddstr(win, row, 1, text.toUtf8().constData());

    int noStart = 1 + text.length();
    text = noText;
    if (addNoSpace > 0) {
        int addAfter = addYesSpace / 2;
        int addBefore = addNoSpace - addAfter;
        text.prepend(QString(addBefore, ' '));
        text.append(QString(addAfter, ' '));
    }
    if (!yesSelected) {
        wattrset(win, A_REVERSE | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    } else {
        wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    }
    mvwaddstr(win, row, noStart, text.toUtf8().constData());

    wattrset(win, A_BOLD | COLOR_PAIR(Client::COLOR_YELLOW_BLUE));
    box(win, 0, 0);
    wnoutrefresh(win);
    delwin(win);
}

void SystemStatus::renderAutoprobeDetails()
{
    std::vector<std::vector<QString>> lines;
    for (const auto &add : autoprobeState) {
        lines.emplace_back();
        auto &line = lines.back();

        if (!add.physical.isEmpty()) {
            line.emplace_back(tr("%1 ", "physical column").arg(add.physical));
        } else {
            line.emplace_back(tr("", "no physical name"));
        }

        if (!add.activeName.isEmpty()) {
            line.emplace_back(tr("%3 (%1/%2)", "active status").arg(add.activeTry)
                                                               .arg(add.activeTotalTries)
                                                               .arg(add.activeName));
        } else {
            line.emplace_back();
        }

        if (add.otherCandidates > 0) {
            line.emplace_back(tr("[%1 others]", "other remaining").arg(add.otherCandidates));
        }
    }
    std::vector<int> columnWidths;
    for (const auto &line : lines) {
        for (std::size_t i = 0, max = line.size(); i < max; i++) {
            const auto &field = line[i];
            if (field.isEmpty())
                continue;
            while (i >= columnWidths.size()) {
                columnWidths.emplace_back(0);
            }
            columnWidths[i] = std::max(columnWidths[i], field.length());
        }
    }

    int width = 0;
    std::vector<int> columnStarts;
    for (std::size_t i = 0, max = columnWidths.size(); i < max; i++) {
        auto cwidth = columnWidths[i];
        if (cwidth <= 0)
            continue;
        if (width > 0)
            width += 1;
        while (i >= columnStarts.size()) {
            columnStarts.push_back(width);
        }
        width += cwidth;
    }
    width = std::max(width, tr("CLOSE").length());
    width = std::max(width, tr("Autodetection Status").length());

    int height = static_cast<int>(lines.size()) + 1;

    WINDOW *win = subwin(stdscr, height + 2, width + 2, (LINES - (height + 2)) / 2,
                         (COLS - (width + 2)) / 2);
    if (!win)
        return;
    wattrset(win, COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    wbkgdset(win, ' ' | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    werase(win);


    int row = 1;
    for (const auto &line : lines) {
        for (std::size_t i = 0, max = line.size(); i < max; i++) {
            mvwaddstr(win, row, columnStarts[i] + 1, line[i].toUtf8().constData());
        }
        ++row;
    }

    QString text = tr("CLOSE");
    centerText(text, width);
    wattrset(win, A_REVERSE | COLOR_PAIR(Client::COLOR_WHITE_BLUE));
    mvwaddstr(win, row++, 1, text.toUtf8().constData());

    wattrset(win, A_BOLD | COLOR_PAIR(Client::COLOR_YELLOW_BLUE));
    box(win, 0, 0);
    text = tr("Autodetection Status");
    mvwaddstr(win, 0, ((width + 2) - text.length()) / 2, text.toUtf8().constData());
    wnoutrefresh(win);
    delwin(win);
}

void SystemStatus::render()
{
    renderMainWindow();
    if (selection == RestartConfirmYes || selection == RestartConfirmNo) {
        renderGenericConfirm(QStringList() << tr("System restart requested.")
                                           << tr("If you continue the acquisition system will restart.")
                                           << tr("This will cause a temporary interruption of data collection.")
                                           << tr("Any changes to the configuration will be loaded."),
                             tr("RESTART SYSTEM"), tr("ABORT"), selection == RestartConfirmYes);
    } else if (selection == ContaminateClearAllConfirmYes ||
            selection == ContaminateClearAllConfirmNo) {
        renderGenericConfirm(
                QStringList() << tr("Clear all system flags (including contamination)?")
                              << tr("This will remove all shared system flags."),
                tr("CONTINUE AND CLEAR FLAGS"), tr("ABORT"),
                selection == ContaminateClearAllConfirmYes);
    } else if (selection == BypassClearAllConfirmYes || selection == BypassClearAllConfirmNo) {
        renderGenericConfirm(QStringList() << tr("Clear all bypass locks?")
                                           << tr("This will remove all bypass locks.")
                                           << tr("This overrides internal bypass control."),
                             tr("CONTINUE AND CLEAR LOCKS"), tr("ABORT"),
                             selection == BypassClearAllConfirmYes);
    } else if (selection == AveragingChangeConfirmYes || selection == AveragingChangeConfirmNo) {
        renderGenericConfirm(tr("Set averaging time to %1?").arg(
                getAveragingText(confirmAveragingUnit, confirmAveragingCount,
                                 confirmAveragingAlign)), tr("CHANGE AVERAGING"), tr("ABORT"),
                             selection == AveragingChangeConfirmYes);
    } else if (selection == AutoprobeDetails) {
        renderAutoprobeDetails();
    }
}
