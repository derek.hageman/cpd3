/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef INTERFACEHANDLER_H
#define INTERFACEHANDLER_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "datacore/stream.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/realtimelayout.hxx"

class InterfaceWindow;

class Client;

class MenuHandler;

/**
 * The handler for a realtime interface.
 */
class InterfaceHandler : public QObject {
Q_OBJECT

    Client *parent;
    CPD3::Data::Variant::Read information;
    CPD3::Data::Variant::Read state;
    QString name;

    CPD3::Acquisition::RealtimeLayout layoutInstant;
    CPD3::Acquisition::RealtimeLayout layoutBoxcar;
    CPD3::Acquisition::RealtimeLayout layoutAverage;

    InterfaceWindow *window;

    friend class InterfaceWindow;

public:
    /**
     * Create the handler.
     * 
     * @param c     the main client
     * @param info  the interface information from the realtime link
     * @param n     the interface name
     */
    InterfaceHandler(Client *c, const CPD3::Data::Variant::Read &info, const QString &n);

    ~InterfaceHandler();

    /**
     * Update the interface information.  Returns false if this handler
     * is no longer valid (and should be destroyed so a new one can be
     * created).
     * 
     * @param info      the new interface information
     * @return          true if the handler is still valid
     */
    bool informationUpdated(const CPD3::Data::Variant::Read &info);

    /**
     * Update the interface state.
     * 
     * @param st        the new interface state
     */
    void stateUpdated(const CPD3::Data::Variant::Read &st);

    /**
     * Test if the interface is currently bypassed.
     * 
     * @return true if the interface is bypassed
     */
    bool isBypassed() const;

    /**
     * Test if the interface is currently contaminated
     * 
     * @return true if the interface is currently contaminated
     */
    bool isContaminated() const;

    /**
     * The information for the status line.
     */
    struct StatusLineData {
        /** The text, usually just a single character. */
        QString text;
        /** The general status, used to determine the color of the text. */
        CPD3::Acquisition::AcquisitionInterface::GeneralStatus status;
        /** Set if the interface is currently bypassed. */
        bool bypassed;
        /** Set if the interface is currently contaminated. */
        bool contaminated;

        StatusLineData();
    };

    /**
     * Get the status line information.
     * 
     * @return the current status information
     */
    StatusLineData getStatusLine() const;

    /**
     * Get the main menu text line.
     * 
     * @return the text in the main menu
     */
    QString getMainMenuEntry() const;

    /**
     * Get the main menu hotkey text string.
     * 
     * @return the main menu hotkey
     */
    QString getMainMenuHotkey() const;

    /**
     * Test if the interface has an actual main menu entry.
     * 
     * @return true if the interface has a main menu entry
     */
    bool hasMainMenuEntry();

    /**
     * Called when this entry is selected in the main menu.
     */
    void mainMenuSelected();

    /**
     * Show the command menu for the interface.
     */
    void showInterfaceMenu();

    /**
     * Hide the window.
     */
    void hideWindow();

    /**
     * Advance the window display mode.
     */
    void nextDisplay();

    /**
     * Advance the window averaging mode.
     */
    void nextAveraging();

    /**
     * Handling incoming realtime data.
     * 
     * @param data  the data
     */
    void incomingRealtime(const CPD3::Data::SequenceValue::Transfer &data);

    /**
     * Get the interface name.
     * 
     * @return the interface name
     */
    inline QString getName() const
    { return name; }

    /**
     * Test if the next display option in the menu should be shown.
     * 
     * @return true to show the next display menu option
     */
    inline bool showNextDisplay() const
    {
        return window != NULL &&
                (layoutInstant.pages() > 1 ||
                        layoutBoxcar.pages() > 1 ||
                        layoutAverage.pages() > 1);
    }

    /**
     * Test if the advance averaging menu option should be displayed.
     * 
     * @return true to display the next averaging option
     */
    inline bool showChangeAveraging() const
    {
        return window != NULL &&
                ((layoutInstant.pages() > 0 ? 1 : 0) +
                        (layoutBoxcar.pages() > 0 ? 1 : 0) +
                        (layoutAverage.pages() > 0 ? 1 : 0)) > 1;
    }

    /**
     * Test if the hide window menu option should be displayed.
     * 
     * @return true to show the hide window menu option
     */
    inline bool showHideWindow() const
    { return window != NULL; }

public slots:

    void pageChangeRequest(int page);
};

#endif
