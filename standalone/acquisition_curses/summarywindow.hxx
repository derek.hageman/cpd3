/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef SUMMARYWINDOW_H
#define SUMMARYWINDOW_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QList>

#include "datacore/stream.hxx"
#include "datacore/sequencematch.hxx"
#include "core/number.hxx"
#include "core/timeutils.hxx"

#include "windowhandler.hxx"

/**
 * The background summary window.
 */
class SummaryWindow : public WindowHandler {
Q_OBJECT

    struct ValueConfig {
        CPD3::Data::SequenceMatch::Composite selection;

        qint64 sortPriority;
        CPD3::Data::Variant::Write config;

        ValueConfig();

        ValueConfig(const CPD3::Data::Variant::Read &config);
    };

    QList<ValueConfig *> valueConfig;

    struct StateConfig {
        CPD3::Data::SequenceMatch::Composite selection;

        qint64 sortPriority;
        CPD3::Data::Variant::Write config;

        StateConfig();

        StateConfig(const CPD3::Data::Variant::Read &config);
    };

    QList<StateConfig *> stateConfig;

    friend class ValueData;

    class ValueData {
        CPD3::Data::SequenceName unit;
        ValueConfig *config;
        SummaryWindow *parent;

        qint64 sortPriority;
        QString label;
        QString units;
        CPD3::NumberFormat format;

        CPD3::Data::Variant::Root instant;
        CPD3::Data::Variant::Root boxcar;
        CPD3::Data::Variant::Root average;
        CPD3::Data::SequenceValue::Transfer valueBuffer;

        bool bufferCacheValid;
        QString meanCached;
        QString minCached;
        QString maxCached;
        QString minTimeCached;
        QString maxTimeCached;
    public:
        ValueData(const CPD3::Data::SequenceName &u, ValueConfig *c, SummaryWindow *p);

        inline bool matches(const CPD3::Data::SequenceName &u) const
        { return unit == u; }

        bool sortCompare(const ValueData &other) const;

        void incomingInstant(const CPD3::Data::SequenceValue &value);

        void incomingBoxcar(const CPD3::Data::SequenceValue &value);

        void incomingAverage(const CPD3::Data::SequenceValue &value);

        void incomingMetadata(const CPD3::Data::SequenceValue &value);

        inline QString getLabel() const
        { return label; }

        inline QString getUnits() const
        { return units; }

        inline int getDecimalDigits() const
        { return format.getDecimalDigits(); }

        QString getInstant() const;

        QString getBoxcar() const;

        QString getAverage() const;

        void getBuffer(QString &meanOut,
                       QString &minOut,
                       QString &maxOut,
                       QString &minTimeOut,
                       QString &maxTimeOut);
    };

    QList<ValueData *> valueLines;

    friend class StateData;

    class StateData {
        CPD3::Data::SequenceName unit;
        StateConfig *config;
        SummaryWindow *parent;

        qint64 sortPriority;
        QString format;

        CPD3::Data::Variant::Root latest;
    public:
        StateData(const CPD3::Data::SequenceName &u, StateConfig *c, SummaryWindow *p);

        inline bool matches(const CPD3::Data::SequenceName &u) const
        { return unit == u; }

        bool sortCompare(const StateData &other) const;

        void incomingValue(const CPD3::Data::SequenceValue &value);

        void incomingMetadata(const CPD3::Data::SequenceValue &value);

        QString getLine() const;
    };

    QList<StateData *> stateLines;

    class Dispatch {
    public:
        virtual ~Dispatch();

        virtual void handleValue(const CPD3::Data::SequenceValue &value) = 0;
    };

    QHash<CPD3::Data::SequenceName, Dispatch *> dispatch;

    Dispatch *createDispatch(const CPD3::Data::SequenceName &unit);

    CPD3::Time::LogicalTimeUnit bufferUnit;
    int bufferCount;
    bool bufferAligned;

    QString titleString;

    QSet<QString> seenStations;

    class DispatchDiscard : public Dispatch {
    public:
        DispatchDiscard();

        virtual ~DispatchDiscard();

        virtual void handleValue(const CPD3::Data::SequenceValue &value);
    };

    friend class DispatchValueInstant;

    class DispatchValueInstant : public Dispatch {
        ValueData *target;
    public:
        DispatchValueInstant(ValueData *t);

        virtual ~DispatchValueInstant();

        virtual void handleValue(const CPD3::Data::SequenceValue &value);
    };

    friend class DispatchValueBoxcar;

    class DispatchValueBoxcar : public Dispatch {
        ValueData *target;
    public:
        DispatchValueBoxcar(ValueData *t);

        virtual ~DispatchValueBoxcar();

        virtual void handleValue(const CPD3::Data::SequenceValue &value);
    };

    friend class DispatchValueAverage;

    class DispatchValueAverage : public Dispatch {
        ValueData *target;
    public:
        DispatchValueAverage(ValueData *t);

        virtual ~DispatchValueAverage();

        virtual void handleValue(const CPD3::Data::SequenceValue &value);
    };

    friend class DispatchValueMetadata;

    class DispatchValueMetadata : public Dispatch {
        ValueData *target;
    public:
        DispatchValueMetadata(ValueData *t);

        virtual ~DispatchValueMetadata();

        virtual void handleValue(const CPD3::Data::SequenceValue &value);
    };

    friend class DispatchState;

    class DispatchState : public Dispatch {
        StateData *target;
    public:
        DispatchState(StateData *t);

        virtual ~DispatchState();

        virtual void handleValue(const CPD3::Data::SequenceValue &value);
    };

    friend class DispatchStateMetadata;

    class DispatchStateMetadata : public Dispatch {
        StateData *target;
    public:
        DispatchStateMetadata(StateData *t);

        virtual ~DispatchStateMetadata();

        virtual void handleValue(const CPD3::Data::SequenceValue &value);
    };

    struct TextData {
        int x;
        int y;
        QByteArray text;
    };
    QList<TextData> renderText;
    int lastWidth;
    int lastHeight;
    bool renderOutdated;

    void executeRender(bool top) const;

    void addTextData(int y, int x, const QByteArray &text);

    void pruneBuffer(CPD3::Data::SequenceValue::Transfer &buffer) const;

public:
    /**
     * Create the summary window.
     * 
     * @param config    the summary window configuration
     */
    SummaryWindow(const CPD3::Data::Variant::Read &config);

    virtual ~SummaryWindow();

    virtual void render(bool top);

    /**
     * Handling incoming realtime data.
     * 
     * @param data  the data
     */
    void incomingRealtime(const CPD3::Data::SequenceValue::Transfer &data);
};

#endif
