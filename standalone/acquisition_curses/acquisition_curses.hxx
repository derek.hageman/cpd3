/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUISITIONCURSES_H
#define ACQUISITIONCURSES_H

#include "core/first.hxx"

#include <mutex>
#include <QtGlobal>
#include <QObject>
#include <QTimer>
#include <QAtomicInt>

#include "acquisition/acquisitionnetwork.hxx"
#include "acquisition/commandmanager.hxx"
#include "datacore/stream.hxx"
#include "datacore/segment.hxx"

class InterfaceHandler;

class MenuHandler;

class MessageLog;

class EventDisplay;

class WindowHandler;

class GenericWindow;

class CommandHandler;

class SummaryWindow;

class SystemStatus;

/**
 * The main client handler.
 */
class Client : public QObject {
Q_OBJECT

    QAtomicInt renderServiceCounter;
    QTimer keyServiceTimer;
    QTimer renderTimer;

    CPD3::Data::Variant::Write connectionConfiguration;
    std::deque<CPD3::Data::ValueSegment> configuration;
    std::unique_ptr<CPD3::Acquisition::AcquisitionNetworkClient> connection;
    bool connected;

    std::mutex realtimeMutex;
    CPD3::Data::SequenceValue::Transfer incomingRealtime;

    class RealtimeIngress : public CPD3::Data::StreamSink {
        Client *parent;
    public:
        RealtimeIngress(Client *p);

        virtual ~RealtimeIngress();

        void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

        void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

        void incomingData(const CPD3::Data::SequenceValue &value) override;

        void incomingData(CPD3::Data::SequenceValue &&value) override;

        void endData() override;
    };

    friend class RealtimeIngress;

    RealtimeIngress realtimeIngress;

    CPD3::Acquisition::CommandManager commandManager;

    QHash<QString, InterfaceHandler *> interfaces;
    QHash<CPD3::Data::SequenceName, CPD3::Data::SequenceValue> latestRealtime;

    InterfaceHandler *createHandler(const CPD3::Data::Variant::Read &information,
                                    const QString &name);

    QList<GenericWindow *> genericWindows;

    QList<MenuHandler *> menuDisplay;

    CommandHandler *commandHandler;
    MessageLog *messageLog;
    EventDisplay *eventDisplay;
    SummaryWindow *summaryWindow;
    SystemStatus *systemStatus;

    QList<WindowHandler *> windows;


    void realtimeUpdated();

    bool backgroundKey(int key);

    void drawStatusLine();

    void queueRenderService();

public:
    Client();

    ~Client();

    /**
     * Parse the argument list.
     * 
     * @param arguments the argument list
     * @return          0 on success, 1 on exit success, -1 on failure
     */
    int parseArguments(QStringList arguments);

    /**
     * Start the client.  The argument must already have been parsed.
     */
    void startup();

    /**
     * Returns true or false depending on the global blink status.
     * 
     * @return  true when blink text should be bolded
     */
    static bool blinkState();

    enum {
        /** The pair value for white text on a black background. */
                COLOR_WHITE_BLACK = 1, /** The pair value for red text on a black background. */
                COLOR_RED_BLACK, /** The pair value for yellow text on a black background. */
                COLOR_YELLOW_BLACK, /** The pair value for magenta text on a black background. */
                COLOR_MAGENTA_BLACK, /** The pair value for green text on a black background. */
                COLOR_GREEN_BLACK,

        /** The pair value for white text on a blue background. */
                COLOR_WHITE_BLUE, /** The pair value for red text on a blue background. */
                COLOR_RED_BLUE, /** The pair value for yellow text on a blue background. */
                COLOR_YELLOW_BLUE, /** The pair value for red text on a white background. */
                COLOR_RED_WHITE,
    };

    /**
     * Show the message log entry dialog.
     */
    void showMessageLogEntry();

    /**
     * Show the system status window.
     */
    void showSystemStatus();

    /**
     * Add a message log event to the system.
     * 
     * @param event the event data
     */
    void messageLogEvent(CPD3::Data::Variant::Root event);

    /**
     * Issue a command to the system.
     * 
     * @param target    the command target
     * @param command   the command data
     */
    void issueCommand(const QString &target, CPD3::Data::Variant::Root command);

    /**
     * Register a window handler for display.  The client takes ownership
     * of it unless it is unregistered.  The window appears at the top
     * of the display stack.
     * 
     * @param window    the window
     */
    void registerWindowHandler(WindowHandler *window);

    /**
     * Unregister a window handler.  The window is no longer drawn and
     * ownership is transferred to the caller.  That is, this will not
     * delete the window.
     * 
     * @param window    the window
     */
    void unregisterWindowHandler(WindowHandler *window);

    /**
     * Bring a window to the front of the display.
     * 
     * @param window    the window
     */
    void bringWindowToFront(WindowHandler *window);

    /**
     * Display a menu.  This will automatically connect the dismissed signal
     * so the menu will hide at the appropriate time.  This takes ownership
     * of the menu.
     * 
     * @param menu      the menu to display
     */
    void displayMenu(MenuHandler *menu);

    /**
     * Display a command handler.  If a previously existing one is still
     * show it is destroyed.  This takes ownership of the handler.  Setting
     * the handler to NULL hides it.
     * 
     * @param handler   the command handler to display
     */
    void displayCommandHandler(CommandHandler *handler);

    /**
     * Get the list of all generic windows
     * 
     * @return all generic windows
     */
    inline QList<GenericWindow *> getGenericWindows() const
    { return genericWindows; }

    /**
     * Get the mapping of all interface handlers.
     * 
     * @return all interface handlers
     */
    inline QHash<QString, InterfaceHandler *> getInterfaces() const
    { return interfaces; }

    /**
     * Get the command manager reference.
     * 
     * @return the command manager
     */
    inline CPD3::Acquisition::CommandManager &getCommandManager()
    { return commandManager; }

    /**
     * Get the connection.
     * 
     * @return the connection
     */
    inline CPD3::Acquisition::AcquisitionNetworkClient *getConnection()
    { return connection.get(); }

signals:

    void serviceRealtime();

    void exitRequest();

private slots:

    void processRealtime();

    void keyUpdate();

    void renderAll();

    void interfaceInformationUpdated(const std::string &name);

    void interfaceStateUpdated(const std::string &name);

    void autoprobeStateUpdated();

    void connectionStateChanged(bool connected);

    void realtimeEvent(const CPD3::Data::Variant::Root &event);

    void messageLogDismissed();

    void systemStatusDismissed();

    void menuDismissed();

    void runTTYCheck();
};

#endif
