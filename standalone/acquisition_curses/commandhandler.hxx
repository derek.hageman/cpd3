/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef COMMANDHANDLER_H
#define COMMANDHANDLER_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QList>
#include <QVariant>
#include <QString>
#include <QPair>

#include "datacore/stream.hxx"
#include "core/number.hxx"

class Client;

/**
 * The handler for dealing with commands issued to the system.
 */
class CommandHandler : public QObject {
Q_OBJECT

    Client *client;
    CPD3::Data::Variant::Write command;

    enum {
        SelectingTarget, SelectingParameters, SelectingParametersEditing, SelectingConfirm,

        SelectionCompleted,
    } selectionState;

    QList<QString> targets;

    struct ParamaterData {
        QString name;
        qint64 sortPriority;
        QString label;

        enum {
            Decimal, Hex, Boolean, Enum,
        } type;
        QVariant value;

        CPD3::NumberFormat numberFormat;
        double decimalMinimum;
        double decimalMaximum;
        std::vector<std::pair<QString, QString>> enumValues;

        bool operator<(const ParamaterData &other) const;
    };

    QList<ParamaterData> parameters;
    int selectedParameterIndex;
    QString parameterEditingString;
    int parameterEditingCursor;

    bool confirmed;

    void finishEditing();

    void completeCommand();

public:
    /**
     * Create a command handler.  The command data must have the "Targets"
     * and "Command" field set.
     * 
     * @param cmd   the command data
     * @param cl    the client
     */
    CommandHandler(const CPD3::Data::Variant::Read &cmd, Client *cl);

    virtual ~CommandHandler();

    /**
     * Called when the handler receives a keypress.
     * 
     * @param ch    the key
     * @return      true if the key was consumed
     */
    virtual bool keypress(int ch);

    /**
     * Draw the window.
     * 
     * @param top   true if this window is the topmost
     */
    virtual void render();

    /**
     * Select the targets.
     * 
     * @param t     the list of targets
     */
    void targetsSelected(const QList<QString> &t);

signals:

    /**
     * Emitted if the command is accepted and requests a page change.  This
     * can be used to switch the display to a specific page for the mode
     * the command switches to (e.x. a spancheck display).
     * 
     * @param page  the page to change to
     */
    void pageChangeRequest(int page);

private slots:

    void targetSelectionDismissed();
};

#endif
