/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#ifdef Q_OS_UNIX

#include <stdlib.h>

#endif

#include "clicore/terminaloutput.hxx"
#include "datacore/externalsource.hxx"
#include "core/process.hxx"
#include "io/process.hxx"

#include "input.hxx"
#include "multiplex.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::CLI;

Input::Input(StreamSink *target) : target(target)
{ }

Input::~Input()
{
    if (target)
        target->endData();
}

static void fatalError(const QString &message)
{
#ifndef NO_CONSOLE
    TerminalOutput::simpleWordWrap(message, true);
#else
    QMessageBox::critical(0, Multiplex::tr("Input error"), message);
#endif
    QCoreApplication::exit(1);
}

static bool enableAcceleration()
{
    bool enablePipelineAcceleration = false;
#ifdef Q_OS_UNIX
    if (char *env = getenv("CPD3CLI_ACCELERATION")) {
        if (env[0] && atoi(env))
            enablePipelineAcceleration = true;
    }
#endif
    return enablePipelineAcceleration;
}

static bool enableSerialization()
{
    bool enablePipelineSerialization = false;
#ifdef Q_OS_UNIX
    if (char *env = getenv("CPD3CLI_COMBINEPROCESSES")) {
        if (env[0] && atoi(env))
            enablePipelineSerialization = true;
    }
#endif
    return enablePipelineSerialization;
}

Input::Context::Context(Input &input) : pipeline(enableAcceleration(), enableSerialization())
{
    target = input.target;
    input.target = nullptr;
    pipeline.finished.connect([this] { this->updated(); });
}

Input::Context::~Context()
{
    if (target)
        target->endData();
}

void Input::Context::shutdown()
{ pipeline.signalTerminate(); }

bool Input::Context::start()
{
    Q_ASSERT(target);
    if (!pipeline.setOutputIngress(target)) {
        qWarning() << "Unable to set pipeline target:" << pipeline.getOutputError();
        return false;
    }
    if (!pipeline.start())
        return false;
    target = nullptr;
    return true;
}

bool Input::Context::isFinished()
{ return pipeline.isFinished(); }

InputFile::InputFile(const QString &name, StreamSink *target) : Input(target), fileName(name)
{ }

InputFile::~InputFile() = default;

std::unique_ptr<Input::Context> InputFile::start(Multiplex &)
{
    Q_ASSERT(!fileName.isEmpty());

    std::unique_ptr<Input::Context> result(new Context(*this));
    if (!result->pipeline.setInputFile(fileName)) {
        fatalError(Multiplex::tr("Error setting input file %1: %2").arg(fileName, result->pipeline
                                                                                        .getInputError()));
        return {};
    }
    fileName = QString();
    return std::move(result);
}

InputPipeline::InputPipeline(StreamSink *target) : Input(target)
{ }

InputPipeline::~InputPipeline() = default;

std::unique_ptr<Input::Context> InputPipeline::start(Multiplex &)
{
    std::unique_ptr<Input::Context> result(new Context(*this));
    if (!result->pipeline.setInputPipeline()) {
        fatalError(Multiplex::tr("Error setting input pipeline: %1").arg(
                result->pipeline.getInputError()));
        return {};
    }
    return std::move(result);
}


InputCommand::InputCommand(const QString &cmd, StreamSink *target) : Input(target), command(cmd)
{ }

InputCommand::~InputCommand() = default;

std::unique_ptr<Input::Context> InputCommand::start(Multiplex &mux)
{
    Q_ASSERT(!command.isEmpty());

    auto spawn = IO::Process::Spawn::shell(command);
    spawn.inputStream = IO::Process::Spawn::StreamMode::Discard;
    spawn.outputStream = IO::Process::Spawn::StreamMode::Capture;
    spawn.errorStream = IO::Process::Spawn::StreamMode::Forward;

    auto process = spawn.create();
    if (!process)
        return nullptr;
    auto stream = process->outputStream();
    if (!stream)
        return nullptr;

    class ProcessContext : public Context {
        std::shared_ptr<IO::Process::Instance> process;
    public:
        ProcessContext(Input &input, std::shared_ptr<IO::Process::Instance> &&p) : Context(input),
                                                                                   process(std::move(
                                                                                           p))
        {
            process->exited.connect([this](int, bool) { this->updated(); });
        }

        virtual ~ProcessContext()
        {
            process->terminate();
            process->wait(30.0);
            process->kill();
            process.reset();
        }

        void shutdown() override
        {
            process->terminate();
        }

        bool isFinished() override
        {
            if (process->isRunning())
                return false;
            return Context::isFinished();
        }

        bool start() override
        {
            if (!Context::start())
                return false;
            return process->start();
        }
    };

    std::unique_ptr<Input::Context> result(new ProcessContext(*this, std::move(process)));
    if (!result->pipeline
               .setInputGeneral(std::unique_ptr<ExternalConverter>(new StandardDataInput),
                                std::move(stream), true)) {
        fatalError(Multiplex::tr("Error setting input command %1: %2").arg(command, result->pipeline
                                                                                          .getInputError()));
        return {};
    }
    return std::move(result);
}

