/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef MULTIPLEX_H
#define MULTIPLEX_H

#include "core/first.hxx"

#include <vector>
#include <deque>
#include <QtGlobal>
#include <QObject>
#include <QStringList>

#include "datacore/streampipeline.hxx"
#include "datacore/sinkmultiplexer.hxx"

#include "input.hxx"

class Multiplex : public QObject {
Q_OBJECT

    CPD3::Data::SinkMultiplexer mux;
    std::unique_ptr<CPD3::Data::StreamPipeline> outputPipeline;
    std::deque<std::unique_ptr<Input>> unstartedInputs;
    std::vector<std::unique_ptr<Input::Context>> activeInputs;

    void outputUsage(int ec = 0, const QString &command = "da.multiplex");

public:

    Multiplex();

    virtual ~Multiplex();

    int parseArguments(QStringList arguments);

public slots:

    void updated();

    void shutdown();
};

#endif
