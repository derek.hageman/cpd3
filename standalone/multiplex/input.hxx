/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef INPUT_H
#define INPUT_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "core/threading.hxx"
#include "datacore/streampipeline.hxx"

class Multiplex;

class Input {
    CPD3::Data::StreamSink *target;
public:
    Input(CPD3::Data::StreamSink *target);

    virtual ~Input();

    class Context {
        CPD3::Data::StreamSink *target;
    public:
        explicit Context(Input &input);

        virtual ~Context();

        CPD3::Threading::Signal<> updated;

        CPD3::Data::StreamPipeline pipeline;

        virtual void shutdown();

        virtual bool isFinished();

        virtual bool start();
    };

    virtual std::unique_ptr<Context> start(Multiplex &mux) = 0;
};

class InputFile : public Input {
    QString fileName;
public:
    InputFile(const QString &name, CPD3::Data::StreamSink *target);

    virtual ~InputFile();

    std::unique_ptr<Context> start(Multiplex &mux) override;
};

class InputPipeline : public Input {
public:
    InputPipeline(CPD3::Data::StreamSink *target);

    virtual ~InputPipeline();

    std::unique_ptr<Context> start(Multiplex &mux) override;
};

class InputCommand : public Input {
    QString command;
public:
    InputCommand(const QString &cmd, CPD3::Data::StreamSink *target);

    virtual ~InputCommand();

    std::unique_ptr<Context> start(Multiplex &mux) override;
};

#endif
