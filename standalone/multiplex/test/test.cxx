/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QObject>
#include <QtDebug>
#include <QProcess>
#include <QString>
#include <QTemporaryFile>
#include <QSqlDatabase>

#include "datacore/externalsink.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/stream.hxx"
#include "core/number.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class TestComponent : public QObject {
Q_OBJECT

    static void writeData(QIODevice *target, const SequenceValue::Transfer &values)
    {
        ExternalSink *writer = new StandardDataOutput(target);
        writer->incomingData(values);
        writer->endData();
        writer->start();
        Threading::pollInEventLoop([&] { return !writer->isFinished(); });
        writer->wait();
        delete writer;
    }

    static bool compareData(const SequenceValue::Transfer &result,
                            const SequenceValue::Transfer &expected)
    {
        if (result.size() != expected.size())
            return false;
        for (int i = 0; i < (int) result.size(); i++) {
            SequenceValue r(result.at(i));
            SequenceValue e(expected.at(i));

            if (r.getUnit() != e.getUnit())
                return false;
            if (!FP::equal(r.getStart(), e.getStart()))
                return false;
            if (!FP::equal(r.getEnd(), e.getEnd()))
                return false;
            if (r.getValue() != e.getValue())
                return false;
        }
        return true;
    }

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();
    }

    void basic()
    {
        QTemporaryFile input1;
        QVERIFY(input1.open());
        writeData(&input1, SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "a"), Variant::Root(1.0), 100.0, 200.0),
                SequenceValue(SequenceName("bnd", "raw", "a"), Variant::Root(3.0), 300.0, 400.0)});
        input1.close();

        QTemporaryFile input2;
        QVERIFY(input2.open());
        writeData(&input2, SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "a"), Variant::Root(2.0), 200.0, 300.0),
                SequenceValue(SequenceName("bnd", "raw", "a"), Variant::Root(4.0), 400.0, 500.0)});
        input2.close();

        QTemporaryFile input3;
        QVERIFY(input3.open());
        writeData(&input3, SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "a"), Variant::Root(7.0), 700.0, 800.0)});
        input3.close();

        QProcess p;
        p.start("cpd3_multiplex",
                QStringList() << input1.fileName() << "-" << ("|cat " + input2.fileName()) << "--"
                              << input3.fileName());

        writeData(&p, SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "a"), Variant::Root(5.0), 500.0, 600.0),
                SequenceValue(SequenceName("bnd", "raw", "a"), Variant::Root(6.0), 600.0, 500.0),
                SequenceValue(SequenceName("bnd", "raw", "a"), Variant::Root(8.0), 800.0, 900.0)});
        p.closeWriteChannel();

        QVERIFY(p.waitForFinished(30000));
        QCOMPARE(p.exitStatus(), QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        ExternalConverter *reader = new StandardDataInput;
        StreamSink::Buffer buffer;
        reader->start();
        reader->setEgress(&buffer);
        reader->incomingData(p.readAllStandardOutput());
        reader->endData();
        QVERIFY(reader->wait());
        delete reader;

        QVERIFY(compareData(buffer.values(), SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "a"), Variant::Root(1.0), 100.0, 200.0),
                SequenceValue(SequenceName("bnd", "raw", "a"), Variant::Root(2.0), 200.0, 300.0),
                SequenceValue(SequenceName("bnd", "raw", "a"), Variant::Root(3.0), 300.0, 400.0),
                SequenceValue(SequenceName("bnd", "raw", "a"), Variant::Root(4.0), 400.0, 500.0),
                SequenceValue(SequenceName("bnd", "raw", "a"), Variant::Root(5.0), 500.0, 600.0),
                SequenceValue(SequenceName("bnd", "raw", "a"), Variant::Root(6.0), 600.0, 500.0),
                SequenceValue(SequenceName("bnd", "raw", "a"), Variant::Root(7.0), 700.0, 800.0),
                SequenceValue(SequenceName("bnd", "raw", "a"), Variant::Root(8.0), 800.0, 900.0)}));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
