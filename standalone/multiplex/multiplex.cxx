/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#ifdef Q_OS_UNIX

#include <stdlib.h>

#endif

#ifdef BUILD_GUI

#include <QGuiApplication>

#else
#include <QCoreApplication>
#endif

#include <QTranslator>
#include <QProcess>
#include <QLocale>
#include <QLibraryInfo>

#include "core/abort.hxx"
#include "core/waitutils.hxx"
#include "core/threadpool.hxx"
#include "clicore/terminaloutput.hxx"
#include "clicore/graphics.hxx"
#include "datacore/streampipeline.hxx"

#include "multiplex.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::CLI;

Multiplex::Multiplex()
{

    bool enablePipelineAcceleration = false;
    bool enablePipelineSerialization = false;
#ifdef Q_OS_UNIX
    if (char *env = getenv("CPD3CLI_ACCELERATION")) {
        if (env[0] && atoi(env))
            enablePipelineAcceleration = true;
    }
    if (char *env = getenv("CPD3CLI_COMBINEPROCESSES")) {
        if (env[0] && atoi(env))
            enablePipelineSerialization = true;
    }
#endif

    std::unique_ptr<StreamPipeline>
            pipeline(new StreamPipeline(enablePipelineAcceleration, enablePipelineSerialization));
    pipeline->finished.connect(this, std::bind(&Multiplex::updated, this), true);
    if (!pipeline->setOutputPipeline()) {
#ifndef NO_CONSOLE
        TerminalOutput::simpleWordWrap(
                tr("Error setting output pipeline: %1").arg(outputPipeline->getOutputError()),
                true);
#else
        QMessageBox::critical(0, tr("Error setting output"),
            tr("Error setting output pipeline: %1").
            arg(chain->getOutputError()));
#endif
        QCoreApplication::exit(1);
        return;
    }

    auto target = pipeline->setInputExternal();
    if (!target || !pipeline->start()) {
#ifndef NO_CONSOLE
        TerminalOutput::simpleWordWrap(
                tr("Error setting input multiplexer: %1").arg(outputPipeline->getInputError()),
                true);
#else
        QMessageBox::critical(0, tr("Error setting output"),
            tr("Error setting input multiplexer: %1").
            arg(chain->getInputError()));
#endif
        QCoreApplication::exit(1);
        return;
    }
    outputPipeline = std::move(pipeline);
    mux.setEgress(target);

    QMetaObject::invokeMethod(this, "updated", Qt::QueuedConnection);
}

Multiplex::~Multiplex()
{
    for (const auto &active : activeInputs) {
        active->shutdown();
    }
    activeInputs.clear();

    if (outputPipeline) {
        outputPipeline->signalTerminate();
        outputPipeline->waitInEventLoop();
    }
    outputPipeline.reset();

    mux.finished.disconnect();
    mux.signalTerminate();
    mux.wait();
}

void Multiplex::outputUsage(int ec, const QString &command)
{
    QString usage(tr("Usage: %1 <input> ...\n"
                     "Each input can be either a file or a command starting with '|'.  The "
                     "special argument '-' reads from standard input while '--' terminates "
                     "processing and interprets all further arguments as files.").arg(command));
#ifndef NO_CONSOLE
    TerminalOutput::simpleWordWrap(usage, ec != 0);
#else
    QMessageBox::critical(0, tr("Usage"), usage);
#endif
    QCoreApplication::exit(ec);
}

int Multiplex::parseArguments(QStringList arguments)
{
    QString programName("da.multiplex");
    if (!arguments.isEmpty())
        programName = arguments.takeFirst();
    if (arguments.isEmpty()) {
        outputUsage(1, programName);
        return 1;
    }
    bool haveOpenedStdin = false;
    while (!arguments.isEmpty()) {
        QString arg(arguments.takeFirst());
        if (arg == tr("--help", "help switch")) {
            outputUsage(0, programName);
            return 1;
        }
        if (arg == tr("--", "end argument")) {
            break;
        }

        if (arg == tr("-", "stdin argument")) {
            if (haveOpenedStdin) {
#ifndef NO_CONSOLE
                TerminalOutput::simpleWordWrap(tr("Standard input specified more than once."),
                                               true);
#else
                QMessageBox::critical(0, tr("Error in arguments"),
                    tr("Standard input specified more than once."));
#endif
                QCoreApplication::exit(1);
                return -1;
            }
            haveOpenedStdin = true;
            unstartedInputs.emplace_back(new InputPipeline(mux.createSink()));
            continue;
        }

        if (arg.startsWith(tr("|", "command start"))) {
            arg = arg.mid(tr("|", "command start").length()).trimmed();
            if (arg.isEmpty()) {
#ifndef NO_CONSOLE
                TerminalOutput::simpleWordWrap(tr("Cannot start empty commands."), true);
#else
                QMessageBox::critical(0, tr("Error in arguments"),
                    tr("Cannot start empty commands."));
#endif
                QCoreApplication::exit(1);
                return -1;
            }

            unstartedInputs.emplace_back(new InputCommand(arg, mux.createSink()));
            continue;
        }

        unstartedInputs.emplace_back(new InputFile(arg, mux.createSink()));
    }
    for (const auto &arg : arguments) {
        unstartedInputs.emplace_back(new InputFile(arg, mux.createSink()));
    }
    if (unstartedInputs.empty()) {
        outputUsage(1, programName);
        return 1;
    }

    mux.finished.connect(this, std::bind(&Multiplex::updated, this), true);
    mux.sinkCreationComplete();
    mux.start();

    QMetaObject::invokeMethod(this, "updated", Qt::QueuedConnection);

    return 0;
}

void Multiplex::shutdown()
{
    for (const auto &active : activeInputs) {
        active->shutdown();
    }
    activeInputs.clear();

    unstartedInputs.clear();
    mux.finished.disconnect();

    mux.signalTerminate();
    mux.wait();
}

void Multiplex::updated()
{
    for (auto it = activeInputs.begin(); it != activeInputs.end();) {
        if (!(*it)->isFinished()) {
            ++it;
            continue;
        }
        it = activeInputs.erase(it);
    }

    bool anyInputStarted = false;
    while (activeInputs.size() < 32) {
        if (unstartedInputs.empty())
            break;
        auto in = std::move(unstartedInputs.front());
        unstartedInputs.pop_front();
        auto context = in->start(*this);
        if (!context)
            continue;

        context->updated.connect(this, std::bind(&Multiplex::updated, this), true);
        if (!context->start()) {
            qWarning() << "Failed to start pipeline";
            continue;
        }

        activeInputs.emplace_back(std::move(context));
        anyInputStarted = true;
    }

    if (anyInputStarted) {
        Q_ASSERT(!activeInputs.empty());
        QMetaObject::invokeMethod(this, "updated", Qt::QueuedConnection);
        return;
    }

    if (!activeInputs.empty())
        return;

    if (outputPipeline) {
        if (!outputPipeline->isFinished())
            return;
        outputPipeline.reset();
    }

    if (!mux.isFinished())
        return;
    mux.wait();

    QCoreApplication::processEvents();
    QCoreApplication::exit(0);
}


int main(int argc, char **argv)
{
    std::unique_ptr<QCoreApplication> app(CLIGraphics::createApplication(argc, argv));

    qRegisterMetaType<QProcess::ProcessState>("QProcess::ProcessState");
    qRegisterMetaType<QProcess::ExitStatus>("QProcess::ExitStatus");

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app->installTranslator(&qtTranslator);

    QString translateLocation;
    QByteArray cpd3(qgetenv("CPD3"));
    if (cpd3.length() > 0) {
        translateLocation = QString(cpd3) + "/locale";
    }
    QTranslator cpd3Translator;
    cpd3Translator.load("cpd3_" + QLocale::system().name(), translateLocation);
    app->installTranslator(&cpd3Translator);
    QTranslator cliTranslator;
    cliTranslator.load("cli_" + QLocale::system().name(), translateLocation);
    app->installTranslator(&cliTranslator);
    QTranslator guiTranslator;
    guiTranslator.load("gui_" + QLocale::system().name(), translateLocation);
    app->installTranslator(&guiTranslator);
    QTranslator multiplexTranslator;
    multiplexTranslator.load("multiplex_" + QLocale::system().name(), translateLocation);
    app->installTranslator(&multiplexTranslator);

    Abort::installAbortHandler(true);

    std::unique_ptr<Multiplex> mux(new Multiplex);
    AbortPoller abortPoller;
    QObject::connect(app.get(), &QCoreApplication::aboutToQuit, mux.get(), &Multiplex::shutdown);
    QObject::connect(&abortPoller, &AbortPoller::aborted, app.get(), QCoreApplication::quit);

    if (int rc = mux->parseArguments(app->arguments())) {
        mux.reset();
        ThreadPool::system()->wait();
        app.reset();
        if (rc == -1)
            return 1;
        return 0;
    }
    abortPoller.start();
    int rc = app->exec();
    abortPoller.disconnect();
    mux.reset();
    ThreadPool::system()->wait();
    app.reset();

    return rc;
}
