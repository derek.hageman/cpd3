/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

#ifdef HAVE_SD_NOTIFY

#include <systemd/sd-daemon.h>

#endif

#ifdef HAVE_UDEV

#include <sys/select.h>

#endif

#include <QTranslator>
#include <QLibraryInfo>
#include <QCoreApplication>
#include <QSocketNotifier>
#include <QLocale>

#include "acquisition/acquisitioncontroller.hxx"
#include "core/abort.hxx"
#include "core/threadpool.hxx"
#include "clicore/terminaloutput.hxx"
#include "core/memory.hxx"

#include "acquisition.hxx"


Q_LOGGING_CATEGORY(log_acquisition_udev, "cpd3.acquisition.udev", QtWarningMsg)

Q_LOGGING_CATEGORY(log_acquisition, "cpd3.acquisition", QtWarningMsg)


using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;
using namespace CPD3::CLI;

static int startupDoneFD = -1;


::Acquisition::Acquisition() : persist(false)
{ }

::Acquisition::~Acquisition()
{
#ifdef HAVE_UDEV
    udevMonitor.disconnect();
#endif
    if (controller) {
        controller->initiateShutdown();
        controller->wait();
        controller.reset();
    }
    if (startupDoneFD != -1) {
        if (::write(startupDoneFD, "\x01", 1) < 0) {
            if (errno != EAGAIN && errno != EINTR && errno != EWOULDBLOCK) {
                qCDebug(log_acquisition) << "Failed to write to startup notification pipe: "
                                         << ::strerror(errno);
                ::close(startupDoneFD);
                startupDoneFD = -1;
            }
        }
    }
}

static void sendStartup(quint8 byte)
{
    if (startupDoneFD == -1)
        return;
    for (;;) {
        ssize_t n = write(startupDoneFD, &byte, 1);
        if (n == -1) {
            if (errno != EAGAIN && errno != EINTR && errno != EWOULDBLOCK) {
                qCDebug(log_acquisition) << "Failed to write to startup notification pipe: "
                                         << ::strerror(errno);
            }
            break;
        }
        if (n > 0)
            break;
    }
    ::close(startupDoneFD);
    startupDoneFD = -1;
}

void ::Acquisition::startController()
{
    qCInfo(log_acquisition) << "Starting acquisition controller";

    Q_ASSERT(controller.get() == nullptr);

    auto config = ValueSegment::Stream::read(
            Archive::Selection(Time::time(), FP::undefined(), {station}, {"configuration"},
                               {"acquisition"}));
    if (!path.isEmpty()) {
        config = ValueSegment::withPath(config, path);
    }

    controller.reset(new AcquisitionController(config, station, !persist));
    controller->restartRequested
              .connect(this, std::bind(&::Acquisition::restartController, this), true);
    controller->finished.connect(this, std::bind(&::Acquisition::readyToExit, this), true);
    controller->heartbeat.connect(this, std::bind(&::Acquisition::heartbeat, this), false);
    controller->controllerReady
              .connect(this, std::bind(&::Acquisition::sendReadyGlobal, this), true);
    if (startupDoneFD != -1) {
        controller->controllerReady
                  .connect(this, std::bind(&::Acquisition::sendReadyFD, this), true);
        controller->finished.connect(this, std::bind(&::Acquisition::sendReadyFD, this), true);
    }

#ifdef HAVE_UDEV
    connect(&udevMonitor, &AcquisitionUDEVMonitor::serialPortsChanged,
            std::bind(&AcquisitionController::autoprobeSystemChanged, controller.get()));
    udevMonitor.checkMonitor();
#endif

    controller->start();
}

void ::Acquisition::startShutdown()
{
#ifdef HAVE_SD_NOTIFY
    sd_notify(0, "STOPPING=1");
#endif

    if (controller) {
        controller->initiateShutdown();
    }
}

void ::Acquisition::restartController()
{
#ifdef HAVE_SD_NOTIFY
    sd_notify(0, "RELOADING=1");
#endif

#ifdef HAVE_UDEV
    udevMonitor.disconnect();
#endif
    if (controller) {
        controller->restartRequested.disconnectImmediate();
        controller->controllerReady.disconnectImmediate();
        controller->finished.disconnect();

        qCInfo(log_acquisition) << "Restarting acquisition system due to external request";

        do {
            qCInfo(log_acquisition) << "Sending controller shutdown for restart";
            controller->initiateShutdown();
        } while (!controller->wait(30));
        controller.reset();
    }
    startController();
}

void ::Acquisition::sendReadyFD()
{
    sendStartup(1);
}

void ::Acquisition::sendReadyGlobal()
{
#ifdef HAVE_SD_NOTIFY
    sd_notify(0, "READY=1");
#endif
}

void ::Acquisition::heartbeat()
{
#ifdef HAVE_SD_NOTIFY
    sd_notify(0, "WATCHDOG=1");
#endif
}

int ::Acquisition::parseArguments(QStringList arguments)
{
    station.clear();
    path.clear();

    for (QStringList::iterator arg = arguments.begin() + 1; arg != arguments.end();) {
        if (*arg == tr("--help")) {
            TerminalOutput::output(
                    tr("Usage: cpd3.acquisition [--station=<STN>] [--no-fork|--pid=/path/to/file] [SystemName]\n"));
            QCoreApplication::exit(0);
            return 1;
        }
        if (*arg == tr("--no-daemonize") || *arg == tr("--no-fork") || *arg == tr("-n")) {
            arg = arguments.erase(arg);
            continue;
        }
        if (*arg == tr("--")) {
            arguments.erase(arg);
            break;
        }

        if (arg->startsWith(tr("--station="))) {
            station = arg->mid(tr("--station=").length()).toStdString();
            arg = arguments.erase(arg);
            continue;
        }

        if (arg->startsWith(tr("--pid="))) {
            arg = arguments.erase(arg);
            continue;
        }

        if (*arg == tr("--persist")) {
            persist = true;
            arg = arguments.erase(arg);
            continue;
        }

        TerminalOutput::output(
                tr("Unrecognized argument \"%1\".  Use --help for usage.\n").arg(*arg));
        sendStartup(0);
        QCoreApplication::exit(0);
        return -1;
    }

    if (station.empty())
        station = Data::SequenceName::impliedUncheckedStation();
    if (station.empty()) {
        TerminalOutput::output(
                tr("No system station specified.  Please be sure this is an acquisition computer.\n"));
        sendStartup(0);
        QCoreApplication::exit(1);
        return -1;
    }

    if (arguments.size() > 1) {
        path = arguments.at(1);
    } else {
        path = QString::fromStdString(SequenceName::impliedProfile());
    }

    return 0;
}


#ifdef HAVE_UDEV

::AcquisitionUDEVMonitor::AcquisitionUDEVMonitor() : udev(0), mon(0), fd(-1)
{
    udev = ::udev_new();
    if (!udev) {
        qCDebug(log_acquisition_udev) << "Failed to create udev";
        return;
    }

    mon = ::udev_monitor_new_from_netlink(udev, "udev");
    if (mon) {
        ::udev_monitor_filter_add_match_subsystem_devtype(mon, "tty", NULL);
        ::udev_monitor_filter_update(mon);
        ::udev_monitor_enable_receiving(mon);

        fd = ::udev_monitor_get_fd(mon);
        QSocketNotifier *notifier = new QSocketNotifier(fd, QSocketNotifier::Read, this);
        connect(notifier, SIGNAL(activated(int)), this, SLOT(udevEvent()));

        qCDebug(log_acquisition_udev) << "Connected to udev monitor with socket" << fd;
    }
}

AcquisitionUDEVMonitor::~AcquisitionUDEVMonitor()
{
    if (mon)
        ::udev_monitor_unref(mon);
    if (udev)
        ::udev_unref(udev);
}

void AcquisitionUDEVMonitor::udevEvent()
{
    if (fd == -1)
        return;
    if (!mon)
        return;

    for (;;) {
        fd_set fds;
        FD_ZERO(&fds);
        FD_SET(fd, &fds);

        struct timeval tv;
        tv.tv_sec = 0;
        tv.tv_usec = 0;

        int ret = ::select(fd + 1, &fds, NULL, NULL, &tv);
        if (ret <= 0 || !FD_ISSET(fd, &fds))
            break;

        struct ::udev_device *dev = ::udev_monitor_receive_device(mon);
        if (!dev)
            break;
        QString action(::udev_device_get_action(dev));
        QString devpath(::udev_device_get_devpath(dev));
        ::udev_device_unref(dev);

        action = action.toLower();
        if (action == "add" || action == "change") {
            qCDebug(log_acquisition_udev) << "Reloading autoprobe due to udev event" << action
                                          << "on" << devpath;
            emit serialPortsChanged();
        }
    }
}

void AcquisitionUDEVMonitor::checkMonitor()
{
    QMetaObject::invokeMethod(this, "udevEvent", Qt::QueuedConnection);
}

#endif


static void doDaemonize(int argc, char **argv)
{
    char *pidFile = NULL;
    if (argc > 1) {
        for (char **arg = argv + 1, **endArg = argv + argc; arg != endArg; ++arg) {
            char *str = *arg;
            if (::Acquisition::tr("--no-daemonize") == str ||
                    ::Acquisition::tr("--no-fork") == str ||
                    ::Acquisition::tr("-n") == str ||
                    ::Acquisition::tr("--help") == str)
                return;
            QByteArray checkData(::Acquisition::tr("--pid=").toUtf8());
            if (!strncmp(checkData.constData(), str, checkData.size())) {
                pidFile = str + checkData.size();
                if (*pidFile == '\0')
                    pidFile = NULL;
            }
        }
    }

    int pipeFDs[2];
    if (::pipe(pipeFDs) == -1) {
        ::fprintf(stderr, "Can't create signal pipe: %s\n", ::strerror(errno));
        ::exit(1);
        return;
    }

    pid_t pid = ::fork();
    if (pid == -1) {
        ::fprintf(stderr, "Unable to fork: %s\n", ::strerror(errno));
        ::exit(2);
        return;
    }
    if (pid == 0) {
        ::close(pipeFDs[0]);
        startupDoneFD = pipeFDs[1];

        /* Child, so daemonize. */
        if (::setsid() == -1) {
            ::fprintf(stderr, "Can't create new process group: %s\n", ::strerror(errno));
            ::exit(3);
        }
        if (::chdir("/") == -1) {
            ::fprintf(stderr, "Can't change to root directory: %s\n", ::strerror(errno));
            ::exit(4);
        }

        int fd;
        if ((fd = ::open("/dev/null", O_RDONLY)) == -1) {
            ::fprintf(stderr, "Can't open /dev/null for stdin: %s\n", ::strerror(errno));
            ::exit(5);
        }
        if (::dup2(fd, 0) == -1) {
            ::fprintf(stderr, "Can't dup2 stdin: %s\n", ::strerror(errno));
            ::exit(6);
        }
        ::close(fd);
        if ((fd = ::open("/dev/null", O_WRONLY)) == -1) {
            ::fprintf(stderr, "Can't open /dev/null for stdout: %s\n", ::strerror(errno));
            ::exit(7);
        }
        if (::dup2(fd, 1) == -1) {
            ::fprintf(stderr, "Can't dup2 stdout: %s\n", ::strerror(errno));
            ::exit(9);
        }
        ::close(fd);
        if ((fd = ::open("/dev/null", O_WRONLY)) == -1) {
            ::fprintf(stderr, "Can't open /dev/null for stderr: %s\n", ::strerror(errno));
            ::exit(10);
        }
        if (::dup2(fd, 2) == -1) {
            ::fprintf(stderr, "Can't dup2 stderr: %s\n", ::strerror(errno));
            ::exit(11);
        }
        ::close(fd);

        return;
    }

    ::close(pipeFDs[1]);
    int waitFD = pipeFDs[0];
    for (;;) {
        char bfr[1];
        ssize_t n = ::read(waitFD, bfr, sizeof(bfr));
        if (n > 0) {
            ::close(waitFD);

            if (bfr[0] == 0) {
                ::fprintf(stderr, "%s\n",
                          ::Acquisition::tr("Acquisition start up failed.").toLatin1().data());
                ::exit(1);
                return;
            }

            if (pidFile != NULL) {
                FILE *outFile = ::fopen(pidFile, "w");
                if (outFile != NULL) {
                    ::fprintf(outFile, "%u", (unsigned int) pid);
                    ::fclose(outFile);
                } else {
                    char *er = ::strerror(errno);
                    ::fprintf(stderr, "%s\n", ::Acquisition::tr(
                            "Unable to open PID file \"%1\" for writing: %2").arg(pidFile)
                                                                             .arg(er)
                                                                             .toLatin1()
                                                                             .data());
                }
            }

            ::exit(0);
            return;
        }
        if (n == -1) {
            ::fprintf(stderr, "Read on signal pipe failed: %s\n", ::strerror(errno));
            ::close(waitFD);
            ::exit(12);
            return;
        }
    }
}

#if defined(ALLOCATOR_TYPE) && ALLOCATOR_TYPE == 1
#include <tbb/scalable_allocator.h>
static void allocatorRelease()
{
    ::free(nullptr);
#ifdef HAVE_TBB_COMMAND
    scalable_allocation_command(TBBMALLOC_CLEAN_ALL_BUFFERS, nullptr);
#endif
}
static void installAllocator()
{ CPD3::Memory::install_release_function(allocatorRelease); }
#elif defined(ALLOCATOR_TYPE) && ALLOCATOR_TYPE == 2
#include <gperftools/malloc_extension.h>
static void allocatorRelease()
{ MallocExtension::instance()->ReleaseFreeMemory(); }
static void installAllocator()
{ CPD3::Memory::install_release_function(allocatorRelease); }
#else

static void installAllocator()
{ }

#endif

int main(int argc, char **argv)
{
    installAllocator();
    doDaemonize(argc, argv);

    QCoreApplication app(argc, argv);

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

    QString translateLocation;
    QByteArray cpd3(qgetenv("CPD3"));
    if (cpd3.length() > 0) {
        translateLocation = QString(cpd3) + "/locale";
    }
    QTranslator cpd3Translator;
    cpd3Translator.load("cpd3_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cpd3Translator);
    QTranslator cliTranslator;
    cliTranslator.load("cli_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cliTranslator);
    QTranslator acquisitionTranslator;
    acquisitionTranslator.load("acquisition_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&acquisitionTranslator);

    ::Acquisition *acq = new ::Acquisition;
    if (int rc = acq->parseArguments(app.arguments())) {
        delete acq;
        ThreadPool::system()->wait();
        if (rc == -1)
            return 1;
        return 0;
    }

    Abort::installAbortHandler(true);

    AbortPoller abortPoller;
    QObject::connect(&abortPoller, SIGNAL(aborted()), &app, SLOT(quit()));

    QObject::connect(&abortPoller, SIGNAL(aborted()), acq, SLOT(startShutdown()));
    QObject::connect(acq, SIGNAL(readyToExit()), &app, SLOT(quit()));
    acq->startController();

    abortPoller.start();
    int rc = app.exec();
    abortPoller.disconnect();
    delete acq;
    ThreadPool::system()->wait();
    return rc;
}
