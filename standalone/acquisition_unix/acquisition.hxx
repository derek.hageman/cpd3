/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUISITIONUNIX_H
#define ACQUISITIONUNIX_H

#include "core/first.hxx"

#ifdef HAVE_UDEV

#include <libudev.h>

#endif

#include <QtGlobal>
#include <QObject>

#include "acquisition/acquisitioncontroller.hxx"


#ifdef HAVE_UDEV

class AcquisitionUDEVMonitor : public QObject {
Q_OBJECT
    struct udev *udev;
    struct udev_monitor *mon;
    int fd;
public:
    AcquisitionUDEVMonitor();

    ~AcquisitionUDEVMonitor();

    void checkMonitor();

private slots:

    void udevEvent();

signals:

    void serialPortsChanged();
};

#endif


class Acquisition : public QObject {
Q_OBJECT

    std::unique_ptr<CPD3::Acquisition::AcquisitionController> controller;
    CPD3::Data::SequenceName::Component station;
    QString path;
    bool persist;

#ifdef HAVE_UDEV
    AcquisitionUDEVMonitor udevMonitor;
#endif

public:
    Acquisition();

    virtual ~Acquisition();

    int parseArguments(QStringList arguments);

public slots:

    void startShutdown();

    void startController();

    void restartController();

    void sendReadyFD();

    void sendReadyGlobal();

    void heartbeat();

signals:

    void readyToExit();
};

#endif
