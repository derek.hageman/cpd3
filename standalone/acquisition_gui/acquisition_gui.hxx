/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUISITIONTGUI_H
#define ACQUISITIONTGUI_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QSystemTrayIcon>
#include <QIcon>
#include <QTimer>
#include <QTime>
#include <QMenu>

#include "acquisition/acquisitionnetwork.hxx"
#include "acquisition/acquisitioncontroller.hxx"
#include "guidata/realtimemessagelog.hxx"

class AcquisitionGUI : public QObject {
Q_OBJECT

    std::unique_ptr<CPD3::Acquisition::AcquisitionController> controller;
    std::unique_ptr<CPD3::Acquisition::AcquisitionNetworkClient> connection;

    QSystemTrayIcon *icon;

    bool haveConnection;
    QElapsedTimer spawnRealtimeDelay;

    QMenu *contextMenu;

    CPD3::GUI::Data::RealtimeMessageLog *messageLogDialog;

    CPD3::Data::SequenceName::Component station;
    QString path;

public:
    AcquisitionGUI();

    virtual ~AcquisitionGUI();

    int parseArguments(QStringList arguments);

    void start();

signals:

    void exitRequested();

private slots:

    void startController();

    void restartController();

    void iconActivated(QSystemTrayIcon::ActivationReason reason);

    void connectionStateChanged(bool connected);

    void initiateShutdown();

    void realtimeEvent(const CPD3::Data::Variant::Root &event);

    void showRealtimeGUI();

    void interfacesUpdated();

    void verifyRestart();

    void verifyExit();

    void messageLog();
};

#endif
