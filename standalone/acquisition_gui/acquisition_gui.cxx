/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTranslator>
#include <QLibraryInfo>
#include <QApplication>
#include <QMessageBox>


#include "core/abort.hxx"
#include "core/component.hxx"
#include "core/threadpool.hxx"
#include "core/memory.hxx"
#include "datacore/variant/root.hxx"
#include "clicore/argumentparser.hxx"
#include "clicore/terminaloutput.hxx"
#include "acquisition/acquisitionnetwork.hxx"
#include "guidata/realtimemessagelog.hxx"
#include "io/process.hxx"

#include "acquisition_gui.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::GUI::Data;
using namespace CPD3::CLI;
using namespace CPD3::Acquisition;

AcquisitionGUI::AcquisitionGUI() : icon(NULL),
                                   haveConnection(false),
                                   spawnRealtimeDelay(),
                                   contextMenu(NULL),
                                   messageLogDialog(NULL)
{ }

AcquisitionGUI::~AcquisitionGUI()
{
    if (messageLogDialog != NULL)
        delete messageLogDialog;
    connection.reset();
    if (controller) {
        controller->signalTerminate();
        controller->wait();
        controller.reset();
    }

    if (icon != NULL)
        delete icon;
    if (contextMenu != NULL)
        delete contextMenu;
}

void AcquisitionGUI::start()
{
    icon = new QSystemTrayIcon(QIcon(":/icon.svg"), this);

    contextMenu = new QMenu;

    QAction *act;

    act = new QAction(tr("&Message Log Entry", "Context|MessageLog"), contextMenu);
    act->setToolTip(tr("Add a user message log entry."));
    act->setStatusTip(tr("Add a message log entry"));
    act->setWhatsThis(
            tr("When selected this will open a dialog box to enter a user message log event.  This log entry goes into the main message log for the system."));
    connect(act, SIGNAL(triggered()), this, SLOT(messageLog()));
    contextMenu->addAction(act);

    act = new QAction(tr("&Show Graphical Interface", "Context|ShowGUI"), contextMenu);
    act->setToolTip(tr("Show the realtime graphical user interface."));
    act->setStatusTip(tr("Show the interface"));
    act->setWhatsThis(tr("This shows the graphical realtime user interface."));
    connect(act, SIGNAL(triggered()), this, SLOT(showRealtimeGUI()));
    contextMenu->addAction(act);

    act = new QAction(tr("&Restart Acquisition System", "Context|Restart"), contextMenu);
    act->setToolTip(tr("Restart the acquisition system."));
    act->setStatusTip(tr("Restart the acquisition system"));
    act->setWhatsThis(
            tr("This issues a request to restart the acquisition system.  The result will be that it reloads all configuration data."));
    connect(act, SIGNAL(triggered()), this, SLOT(verifyRestart()));
    contextMenu->addAction(act);

    act = new QAction(tr("&Quit", "Context|Quit"), contextMenu);
    act->setToolTip(tr("Exit the acquisition system.  This stops data acquisition."));
    act->setStatusTip(tr("Quit"));
    act->setWhatsThis(tr("This stops the acquisition system."));
    act->setMenuRole(QAction::QuitRole);
    connect(act, SIGNAL(triggered()), this, SLOT(verifyExit()));
    contextMenu->addAction(act);

    icon->setContextMenu(contextMenu);
    icon->setToolTip(tr("CPD3 acquisition system."));
    icon->show();
}

class Parser : public ArgumentParser {
public:
    Parser()
    { }

    virtual ~Parser()
    { }

protected:
    virtual QList<BareWordHelp> getBareWordHelp()
    {
        return QList<BareWordHelp>() << BareWordHelp(tr("system", "system argument name"),
                                                     tr("The global system configuration name."),
                                                     tr("\"aerosol\"", "system argument default"),
                                                     tr("The \"system\" bare word argument specifies global system name that is "
                                                        "started.  This determines all the configuration used.  If absent then the "
                                                        "\"aerosol\" configuration is started."));
    }

    virtual QString getBareWordCommandLine()
    { return tr("[system]", "system command line"); }

    virtual QString getProgramName()
    {
#ifndef Q_OS_WIN32
        return tr("cpd3.acquisition.gui", "program name");
#else
        return tr("cpd3_acquisition", "program name");
#endif
    }

    virtual QString getDescription()
    {
        return tr("This is a graphical version of the CPD3 acquisition system.  It is capable "
                  "of running the full acquisition system and providing a minimal front end "
                  "to interface with it (in the form of a tray icon).");
    }

    virtual QHash<QString, QString> getDocumentationTypeID()
    {
        QHash<QString, QString> result;
        result.insert("type", "acquisition_controller");
        return result;
    }
};

int AcquisitionGUI::parseArguments(QStringList arguments)
{
    if (!arguments.isEmpty())
        arguments.removeFirst();

    ComponentOptions options;

    options.add("station", new ComponentOptionSingleString(QObject::tr("station", "name"),
                                                           QObject::tr("System station code"),
                                                           QObject::tr(
                                                                   "This is the primary system station code.  This determines "
                                                                   "the configuration loaded as well as the station data is logged "
                                                                   "as."),
                                                           QObject::tr("System default",
                                                                       "station default")));

    Parser parser;
    try {
        parser.parse(arguments, options);
    } catch (ArgumentParsingException ape) {
        if (ape.isError()) {
            if (!ape.getText().isEmpty()) {
#ifndef NO_CONSOLE
                TerminalOutput::simpleWordWrap(ape.getText(), true);
#else
                QMessageBox::critical(0, tr("Invalid Usage"), ape.getText());
#endif
            }
            QCoreApplication::exit(1);
            return -1;
        } else {
            if (!ape.getText().isEmpty()) {
#ifndef NO_CONSOLE
                TerminalOutput::simpleWordWrap(ape.getText());
#else
                QMessageBox::information(0, tr("Help"), ape.getText());
#endif
            }
            QCoreApplication::exit(0);
        }
        return 1;
    }

    if (options.isSet("station")) {
        station = qobject_cast<ComponentOptionSingleString *>(options.get("station"))->get()
                                                                                     .toStdString();
    }

    if (station.empty())
        station = Data::SequenceName::impliedUncheckedStation();
    if (station.empty()) {
#ifndef NO_CONSOLE
        TerminalOutput::output(
                tr("No system station specified.  Please be sure this is an acquisition computer.\n"));
#else
        QMessageBox::critical(0, tr("No Station Specified"), tr("No system station specified.  Please be sure this is an acquisition computer."));
#endif
        QCoreApplication::exit(1);
        return -1;
    }

    if (arguments.size() > 1) {
        path = arguments.at(1);
    } else {
        path = QString::fromStdString(SequenceName::impliedProfile());
    }

    startController();

    connection.reset(new AcquisitionNetworkClient(Variant::Read::empty()));
    connection->interfaceInformationUpdated
              .connect(this, std::bind(&AcquisitionGUI::interfacesUpdated, this), true);
    connection->interfaceStateUpdated
              .connect(this, std::bind(&AcquisitionGUI::interfacesUpdated, this), true);
    connection->connectionState
              .connect(this, std::bind(&AcquisitionGUI::connectionStateChanged, this,
                                       std::placeholders::_1), true);
    connection->realtimeEvent
              .connect(this, std::bind(&AcquisitionGUI::realtimeEvent, this, std::placeholders::_1),
                       true);
    connection->start();

    return 0;
}

void AcquisitionGUI::startController()
{
    Q_ASSERT(controller.get() == nullptr);

    auto config = ValueSegment::Stream::read(
            Archive::Selection(Time::time(), FP::undefined(), {station}, {"configuration"},
                               {"acquisition"}));
    if (!path.isEmpty()) {
        config = ValueSegment::withPath(config, path);
    }

    controller.reset(new AcquisitionController(config, station, false));
    controller->restartRequested
              .connect(this, std::bind(&AcquisitionGUI::restartController, this), true);
    controller->start();
}

void AcquisitionGUI::restartController()
{
    if (controller) {
        controller->initiateShutdown();
        controller->wait();
        controller.reset();
    }
    startController();
}

void AcquisitionGUI::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
    case QSystemTrayIcon::Trigger:
        showRealtimeGUI();
        break;
    default:
        break;
    }
}

void AcquisitionGUI::connectionStateChanged(bool connected)
{
    haveConnection = connected;
    if (haveConnection) {
        interfacesUpdated();
    } else {
        if (icon)
            icon->setToolTip(tr("No connection to acquisition system."));
    }
}

void AcquisitionGUI::initiateShutdown()
{
    connection.reset();
    controller->initiateShutdown();
}

void AcquisitionGUI::realtimeEvent(const Variant::Root &event)
{
    QString text(event["Text"].toDisplayString());
    if (text.isEmpty())
        return;

    if (icon != NULL) {
        icon->showMessage(tr("Message - %1").arg(event["Source"].toDisplayString()), text,
                          QSystemTrayIcon::Information);
    }
}

void AcquisitionGUI::interfacesUpdated()
{
    if (!haveConnection)
        return;
    auto interfaceInformation = connection->getInterfaceInformation();
    if (interfaceInformation.empty()) {
        if (icon != NULL)
            icon->setToolTip(tr("Connected to CPD3 acquisition system."));
        return;
    }
    auto interfaceState = connection->getInterfaceState();

    QString tip(tr("Connected to CPD3 acquisition system.  Active:<ul>"));
    QStringList sorted;
    for (const auto &add : interfaceInformation) {
        sorted.append(QString::fromStdString(add.first));
    }
    std::sort(sorted.begin(), sorted.end());
    for (const auto &name : sorted) {
        auto information = interfaceInformation[name.toStdString()].read();
        auto state = interfaceState[name.toStdString()].read();

        if (Util::equal_insensitive(state.hash("Status").toString(), "Normal")) {
            tip.append(tr("<li>%1 - %2</li>", "tooltip interface list entry").arg(name,
                                                                                  information.hash("MenuEntry")
                                                                                             .toDisplayString()));
        } else {
            tip.append(tr("<li><font color=\"#FF0000\">%1 - %2</font></li>",
                          "tooltip interface list entry abnormal").arg(name,
                                                                       information.hash("MenuEntry")
                                                                                  .toDisplayString()));
        }
    }
    tip.append(tr("</ul>", "tooltip end"));

    if (icon != NULL)
        icon->setToolTip(tip);
}

void AcquisitionGUI::showRealtimeGUI()
{
    if (AcquisitionTrayCommandHandler::issueCommand(
            AcquisitionTrayCommandHandler::Command::ShowRealtime))
        return;
    if (spawnRealtimeDelay.isValid() && spawnRealtimeDelay.elapsed() < 5000)
        return;
    spawnRealtimeDelay.start();

    IO::Process::Spawn("cpx3", {"--mode=realtime"}).detach();
}

void AcquisitionGUI::verifyRestart()
{
    QMessageBox::StandardButton result = QMessageBox::question(0, tr("Confirm Restart"),
                                                               tr("Restart acquisition system?  This will cause a momentary interruption in data collection while the system re-initializes."),
                                                               QMessageBox::Yes | QMessageBox::No,
                                                               QMessageBox::No);
    if (result != QMessageBox::Yes)
        return;
    restartController();
}

void AcquisitionGUI::verifyExit()
{
    QMessageBox::StandardButton result = QMessageBox::question(0, tr("Confirm Quit"),
                                                               tr("Stop acquisition system.  This will cause all data collection to be halted."),
                                                               QMessageBox::Yes | QMessageBox::No,
                                                               QMessageBox::No);
    if (result != QMessageBox::Yes)
        return;
    connection.reset();

    controller->initiateShutdown();
    controller->wait();
    emit exitRequested();
}

void AcquisitionGUI::messageLog()
{
    if (!haveConnection)
        return;
    if (messageLogDialog == NULL) {
        messageLogDialog = new RealtimeMessageLog;
        messageLogDialog->setModal(true);
        connect(messageLogDialog, &RealtimeMessageLog::messageLogEvent, this,
                [this](const Variant::Root &message) {
                    if (!connection)
                        return;
                    connection->messageLogEvent(message);
                });
    }

    messageLogDialog->show();
}


#if defined(ALLOCATOR_TYPE) && ALLOCATOR_TYPE == 1

#include <tbb/scalable_allocator.h>

static void allocatorRelease()
{
    ::free(nullptr);
#ifdef HAVE_TBB_COMMAND
    scalable_allocation_command(TBBMALLOC_CLEAN_ALL_BUFFERS, nullptr);
#endif
}

static void installAllocator()
{ CPD3::Memory::install_release_function(allocatorRelease); }

#elif defined(ALLOCATOR_TYPE) && ALLOCATOR_TYPE == 2
#include <gperftools/malloc_extension.h>
static void allocatorRelease()
{ MallocExtension::instance()->ReleaseFreeMemory(); }
static void installAllocator()
{ CPD3::Memory::install_release_function(allocatorRelease); }
#else
static void installAllocator()
{ }
#endif

int main(int argc, char **argv)
{
    installAllocator();

    QApplication app(argc, argv);
    Q_INIT_RESOURCE(acquisition_gui);

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

    QString translateLocation;
    QByteArray cpd3(qgetenv("CPD3"));
    if (cpd3.length() > 0) {
        translateLocation = QString(cpd3) + "/locale";
    }
    QTranslator cpd3Translator;
    cpd3Translator.load("cpd3_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cpd3Translator);
    QTranslator cliTranslator;
    cliTranslator.load("cli_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cliTranslator);
    QTranslator guiTranslator;
    guiTranslator.load("gui_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&guiTranslator);
    QTranslator acquisitionTranslator;
    acquisitionTranslator.load("acquisition_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&acquisitionTranslator);

    AcquisitionGUI *gui = new AcquisitionGUI;
    if (int rc = gui->parseArguments(app.arguments())) {
        delete gui;
        ThreadPool::system()->wait();
        if (rc == -1)
            return 1;
        return 0;
    }

    gui->start();

    app.setQuitOnLastWindowClosed(false);
    app.setWindowIcon(QIcon(":/icon.svg"));

    Abort::installAbortHandler(true);

    AbortPoller abortPoller;
    QObject::connect(&abortPoller, SIGNAL(aborted()), &app, SLOT(quit()));
    QObject::connect(&abortPoller, SIGNAL(aborted()), gui, SLOT(initiateShutdown()));
    QObject::connect(gui, SIGNAL(exitRequested()), &app, SLOT(quit()));
    abortPoller.start();
    int rc = app.exec();
    abortPoller.disconnect();
    delete gui;

    ThreadPool::system()->wait();

    return rc;
}
