/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "datacore/stream.hxx"
#include "datacore/variant/parse.hxx"
#include "core/range.hxx"
#include "core/timeparse.hxx"
#include "clicore/terminaloutput.hxx"
#include "datacore/segment.hxx"

#include "config_text.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::CLI;

class ParseListKeyOverlay {
    double start;
    double end;
    std::shared_ptr<Variant::Parse::InputNodeString> node;
public:
    ParseListKeyOverlay(double s, double e, const QString &path, const QString &value) : start(s),
                                                                                         end(e),
                                                                                         node(std::make_shared<
                                                                                                 Variant::Parse::InputNodeString>(
                                                                                                 Variant::PathElement::parse(
                                                                                                         path.toStdString()),
                                                                                                 value))
    { }

    inline double getStart() const
    { return start; }

    inline double getEnd() const
    { return end; }

    inline const std::shared_ptr<Variant::Parse::InputNodeString> &getNode() const
    { return node; }
};

class ParseListData {
    double start;
    double end;
    Variant::Read metadata;
    Variant::Read existing;
    std::vector<std::shared_ptr<Variant::Parse::InputNodeString>> contents;

public:
    inline double getStart() const
    { return start; }

    inline double getEnd() const
    { return end; }

    inline void setStart(double s)
    { start = s; }

    inline void setEnd(double e)
    { end = e; }

    ParseListData() : start(FP::undefined()),
                      end(FP::undefined()),
                      metadata(Variant::Read::empty()),
                      existing(Variant::Read::empty()), contents()
    { }

    ParseListData(const ParseListData &other) = default;

    ParseListData &operator=(const ParseListData &other) = default;

    ParseListData(ParseListData &&other) = default;

    ParseListData &operator=(ParseListData &&other) = default;

    ParseListData(const SequenceName &unit, SequenceSegment &segment) : start(segment.getStart()),
                                                                        end(segment.getEnd()),
                                                                        metadata(segment.getValue(
                                                                                unit.toMeta())),
                                                                        existing(segment.getValue(
                                                                                unit)), contents()
    { }

    ParseListData(const ParseListData &other, double s, double e) : start(s),
                                                                    end(e),
                                                                    metadata(other.metadata),
                                                                    existing(other.existing),
                                                                    contents(other.contents)
    { }

    ParseListData(const ParseListKeyOverlay &add, double s, double e) : start(s),
                                                                        end(e),
                                                                        metadata(
                                                                                Variant::Read::empty()),
                                                                        existing(
                                                                                Variant::Read::empty()),
                                                                        contents()
    {
        contents.emplace_back(add.getNode());
    }

    ParseListData(const ParseListData &under, const ParseListKeyOverlay &over, double s, double e)
            : start(s),
              end(e),
              metadata(under.metadata),
              existing(under.existing), contents(under.contents)
    {
        contents.emplace_back(over.getNode());
    }

    ParseListData(const ParseListData &under, const ParseListData &over, double s, double e)
            : start(s),
              end(e),
              metadata(over.metadata),
              existing(over.existing), contents(over.contents)
    {
        Util::append(over.contents, contents);
    }

    Variant::Root finalize()
    {
        auto result =
                Variant::Parse::InputNode::fromPointers(contents.begin(), contents.end(), metadata,
                                                        existing);
        contents.clear();
        return result;
    }
};

OutputBreakdown parseFlatFile(InputBreakdown existing, const QString &input)
{
    QStringList lines(input.split(QRegExp("[\n\r]"), QString::SkipEmptyParts));

    SequenceName::Component defaultStation;
    SequenceName::Component defaultArchive;
    SequenceName::Component defaultVariable;
    SequenceName::Flavors defaultFlavors;
    {
        SequenceName::ComponentSet availableStations;
        SequenceName::ComponentSet availableArchives;
        SequenceName::ComponentSet availableVariables;
        SequenceName::FlavorsSet availableFlavors;

        SequenceName::ComponentSet availableStationsValid;
        SequenceName::ComponentSet availableArchivesValid;
        SequenceName::ComponentSet availableVariablesValid;
        SequenceName::FlavorsSet availableFlavorsValid;
        for (const auto &add : existing) {
            availableStations.insert(add.first.getStation());
            availableArchives.insert(add.first.getArchive());
            availableVariables.insert(add.first.getVariable());
            availableFlavors.insert(add.first.getFlavors());

            bool haveValid = false;
            for (auto &check : add.second) {
                if (check.getValue(add.first).exists()) {
                    haveValid = true;
                    break;
                }
            }

            if (haveValid) {
                availableStationsValid.insert(add.first.getStation());
                availableArchivesValid.insert(add.first.getArchive());
                availableVariablesValid.insert(add.first.getVariable());
                availableFlavorsValid.insert(add.first.getFlavors());
            }
        }

        if (availableStationsValid.size() == 1)
            defaultStation = *(availableStationsValid.begin());
        else if (availableStations.size() == 1)
            defaultStation = *(availableStations.begin());

        if (availableArchivesValid.size() == 1)
            defaultArchive = *(availableArchivesValid.begin());
        else if (availableArchives.size() == 1)
            defaultArchive = *(availableArchives.begin());

        if (availableVariablesValid.size() == 1)
            defaultVariable = *(availableVariablesValid.begin());
        else if (availableVariables.size() == 1)
            defaultVariable = *(availableVariables.begin());

        if (availableFlavorsValid.size() == 1)
            defaultFlavors = *(availableFlavorsValid.begin());
        else if (availableFlavors.size() == 1)
            defaultFlavors = *(availableFlavors.begin());
    }

    SequenceName::Map<std::deque<ParseListData>> overlayData;

    int lineNumber = 0;
    for (const auto &line : lines) {
        ++lineNumber;

        SequenceName::Component station;
        SequenceName::Component archive;
        SequenceName::Component variable;
        SequenceName::Flavors flavors = defaultFlavors;
        Time::Bounds bounds;
        QString path;
        QString value;

        bool inPath = false;
        for (int index = 0, max = line.length(); index < max; ++index) {
            QChar add(line.at(index));

            if (add == '<' && !inPath) {
                int end = line.indexOf('>', index + 1);
                if (end < 0)
                    end = line.indexOf(',', index + 1);
                if (end > index) {
                    ++index;
                    QString base(line.mid(index, end - index));
                    QStringList components(base.split(':', QString::KeepEmptyParts));
                    if (!components.isEmpty())
                        station = components.takeFirst().toStdString();
                    if (!components.isEmpty())
                        archive = components.takeFirst().toStdString();
                    if (!components.isEmpty())
                        variable = components.takeFirst().toStdString();
                    if (!components.isEmpty()) {
                        flavors = Util::set_from_qstring(components);
                        flavors.erase(SequenceName::Component());
                    }
                    index = end;
                    continue;
                }
            } else if (add == '(' && !inPath) {
                int end = line.indexOf(')', index + 1);
                if (end < 0)
                    end = line.indexOf(',', index + 1);
                if (end > index) {
                    ++index;
                    QString base(line.mid(index, end - index));
                    try {
                        bounds = TimeParse::parseListBounds(QStringList() << base, true, true);
                        index = end;
                        continue;
                    } catch (TimeParsingException tpe) {
                        TerminalOutput::simpleWordWrap(
                                QObject::tr("Error parsing time on line %1: %2").arg(lineNumber)
                                                                                .arg(tpe.getDescription()),
                                true);
                        QCoreApplication::exit(1);
                        throw std::exception();
                        return {};
                    }
                }
            } else if (add == '\\') {
                inPath = true;
                path.append(add);
                ++index;
                if (index < max) {
                    path.append(add);
                }
                continue;
            } else if (add == ',') {
                value = line.mid(index + 1);
                break;
            } else if (add == '/') {
                inPath = true;
            }

            path.append(add);
        }

        if (station.empty())
            station = defaultStation;
        if (archive.empty())
            archive = defaultArchive;
        if (variable.empty())
            variable = defaultVariable;

        if (station.empty()) {
            TerminalOutput::simpleWordWrap(QObject::tr(
                    "No valid station on line %1 and no default station available.").arg(
                    lineNumber), true);
            QCoreApplication::exit(1);
            throw std::exception();
            return {};
        }
        if (archive.empty()) {
            TerminalOutput::simpleWordWrap(QObject::tr(
                    "No valid archive on line %1 and no default station available.").arg(
                    lineNumber), true);
            QCoreApplication::exit(1);
            throw std::exception();
            return {};
        }
        if (variable.empty()) {
            TerminalOutput::simpleWordWrap(QObject::tr(
                    "No valid variable on line %1 and no default station available.").arg(
                    lineNumber), true);
            QCoreApplication::exit(1);
            throw std::exception();
            return {};
        }
        SequenceName unit(station, archive, variable, flavors);
        unit.clearMeta();

        auto target = overlayData.find(unit);
        if (target == overlayData.end()) {
            target = overlayData.emplace(unit, std::deque<ParseListData>()).first;

            auto source = existing.find(unit);
            if (source != existing.end()) {
                for (auto &add : source->second) {
                    Range::overlayFragmenting(target->second, ParseListData(unit, add));
                }
            }
        }

        Range::overlayFragmenting(target->second,
                                  ParseListKeyOverlay(bounds.getStart(), bounds.getEnd(), path,
                                                      value));

    }

    OutputBreakdown result;
    for (auto &add : overlayData) {
        OutputList combined;
        for (auto &seg : add.second) {
            auto data = seg.finalize();
            if (!data.read().exists())
                continue;

            combined.emplace_back(seg.getStart(), seg.getEnd(), std::move(data));
        }
        result.emplace(add.first, std::move(combined));
    }

    return result;
}
