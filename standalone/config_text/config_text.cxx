/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#ifdef Q_OS_UNIX

#include <stdlib.h>

#endif

#include <QTranslator>
#include <QCoreApplication>
#include <QStringList>
#include <QLibraryInfo>
#include <QDir>
#include <QLocale>
#include <QTemporaryFile>

#include "core/number.hxx"
#include "core/range.hxx"
#include "core/component.hxx"
#include "core/timeparse.hxx"
#include "core/threadpool.hxx"
#include "clicore/terminaloutput.hxx"
#include "clicore/optionparse.hxx"
#include "clicore/argumentparser.hxx"
#include "clicore/archiveparse.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/segment.hxx"
#include "datacore/streampipeline.hxx"
#include "io/process.hxx"

#include "config_text.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::CLI;

class Parser : public ArgumentParser {
public:
    Parser()
    { }

    virtual ~Parser()
    { }

protected:

    virtual QList<BareWordHelp> getBareWordHelp()
    {
        return QList<BareWordHelp>() << BareWordHelp(tr("station", "station argument name"),
                                                     tr("The (default) station to read data from"),
                                                     tr("Inferred from the current directory"),
                                                     tr("The \"station\" bare word argument is used to specify the station(s) used to "
                                                                "look up variables that do not include an explicit station as part of an "
                                                                "archive read specification.  This is the three digit station identification "
                                                                "code.  For example \"brw\".  If a variable does not specify a station, then "
                                                                "this is the station it will be looked up from.  Multiple stations may be "
                                                                "specified by separating them with \":\", \";\" or \",\".  Regular expressions "
                                                                "are accepted."))
                                     << BareWordHelp(tr("variables", "variables argument name"),
                                                     tr("The list of variable specifies to read from the archive"),
                                                     QString(),
                                                     tr("The \"variables\" bare word argument sets the list of variables to read "
                                                                "from the archive.  This can be either a direct list of variables (such as "
                                                                "\"T_S11\" or a alias for multiple variables, such as \"S11a\".  In the single "
                                                                "variable form regular expressions may be used, as long as they match the "
                                                                "entire variable name.  This list is delimited by commas.  If a specification "
                                                                "includes a colon then the part before the colon is treated as an override to "
                                                                "the default archive.  If it includes two colons then the first field is "
                                                                "treated as the station, the second as the archive, and the third as the "
                                                                "actual variable specification.  If it includes four or more colons then "
                                                                "the specification is treated as with three except that the trailing "
                                                                "components specify the flavor (e.x. PM1 or PM10) restriction.  This "
                                                                "restriction consists of an optional prefix and the flavor name.  If there is "
                                                                "no prefix then the flavor is required.  If the prefix is \"!\" then the flavor "
                                                                "is excluded, that is the results will never include it.  If the prefix is"
                                                                "\"=\" then the flavor is added to the list of flavors that any match must "
                                                                "have exactly."
                                                                "\nFor example:"
                                                                "\"T_S11\" specifies the variable in the \"default\" station (set "
                                                                "either explicitly on the command line or inferred from the current directory) "
                                                                "and the \"default\" archive (either the \"raw\" archive or the one set on "
                                                                "the command line.\n"
                                                                "\"raw:T_S1[12]\" specifies the variables T_S11 and T_S12 from the raw archive\n"
                                                                "on the \"default\" station.\n"
                                                                "\"brw:raw:S11a\" specifies the \"S11a\" alias record for the station \"brw\"\n"
                                                                "and the \"raw\" archive.\n"
                                                                "\":avgh:T_S11:pm1:!stats\" specifies T_S11 from the \"default\" station "
                                                                "hourly averages restricted to only PM1 data, but not any calculated statistics.\n\n"
                                                                "The string \"everything\" can also be used to retrieve all available "
                                                                "variables."))
                                     << BareWordHelp(tr("times", "times argument name"),
                                                     tr("The time range of data to read"),
                                                     QString(),
                                                     TimeParse::describeListBoundsUsage(false,
                                                                                        true))
                                     << BareWordHelp(tr("archive", "archive argument name"),
                                                     tr("The (default) archive to read data from"),
                                                     tr("The \"raw\" data archive"),
                                                     tr("The \"archive\" bare word argument is used to specify the archive used to "
                                                                "Look up variables that do not include an archive station as part of an "
                                                                "archive read specification.  This is the internal archive name, such as "
                                                                "\"raw\" or \"clean\".  Multiple archives may be specified by separating them "
                                                                "with \":\", \";\" or \",\".  Regular expressions are accepted."));
    }

    virtual QString getBareWordCommandLine()
    { return tr("[station] variables [times] [archive]", "bare word command line"); }

    virtual QString getProgramName()
    { return tr("da.config.text", "program name"); }

    virtual QString getDescription()
    {
        return tr("This utility allows for generating and editing flat text files representing "
                          "system configuration data.  These files are generated from the archive "
                          "and metadata.");
    }

    virtual QHash<QString, QString> getDocumentationTypeID()
    {
        QHash<QString, QString> result;
        result.insert("type", "config_text");
        return result;
    }
};

enum {
    Mode_ReadArchive = 0,
    Mode_WriteArchive,
    Mode_Edit,
    Mode_EditXML,
    Mode_ParseToOutput,
    Mode_GenerateFromInput,
};

enum ReadMode {
    Read_DefaultMetadataOnly, Read_NoDefault, Read_Everything,
};

static bool launchEditor(QString command, const QString &file)
{
    command.append(' ');
    command.append('\"');
    command.append(file);
    command.append('\"');

    auto run = IO::Process::Spawn(command).forward().create();
    if (!run)
        return false;
    return run->start() && run->wait();
}

static InputBreakdown breakdownData(const SequenceValue::Transfer &input, ReadMode mode)
{
    InputBreakdown result;
    SequenceName::Map<SequenceSegment::Stream> readers;
    SequenceValue::Transfer existingDefault;

    for (const auto &add : input) {
        SequenceName baseUnit = add.getUnit();
        baseUnit.clearMeta();

        if (baseUnit.isDefaultStation()) {
            if (mode == Read_NoDefault)
                continue;
            if (mode == Read_DefaultMetadataOnly) {
                if (!add.getUnit().isMeta())
                    continue;
            }

            existingDefault.emplace_back(add);
            for (auto &rd : readers) {
                Util::append(rd.second.add(add), result[rd.first]);
            }

            continue;
        }

        auto target = readers.find(baseUnit);
        if (target == readers.end()) {
            target = readers.emplace(baseUnit, SequenceSegment::Stream()).first;

            for (const auto &defaultValue : existingDefault) {
                Util::append(target->second.add(defaultValue), result[baseUnit]);
            }
        }

        Util::append(target->second.add(add), result[baseUnit]);
    }
    for (auto &rd : readers) {
        Util::append(rd.second.finish(), result[rd.first]);
    }
    return result;
}

static bool checkIdentical(InputBreakdown &original, OutputBreakdown &modified)
{
    SequenceName::Set keys;
    for (const auto &add : original) {
        keys.insert(add.first);
    }
    for (const auto &add : modified) {
        keys.insert(add.first);
    }

    for (const auto &key : keys) {
        auto &originalSegments = original[key];
        auto &modifiedSegments = modified[key];

        if (originalSegments.size() != modifiedSegments.size()) {
            if (modifiedSegments.empty()) {
                bool anyOriginalDefined = false;
                for (auto &check : originalSegments) {
                    if (!check.value(key).exists())
                        continue;
                    anyOriginalDefined = true;
                    break;
                }
                if (!anyOriginalDefined)
                    continue;
            }
            return false;
        }

        auto mSeg = modifiedSegments.begin();
        for (auto oSeg = originalSegments.begin(), endO = originalSegments.end();
                oSeg != endO;
                ++oSeg, ++mSeg) {
            if (!FP::equal(oSeg->getStart(), mSeg->getStart()))
                return false;
            if (!FP::equal(oSeg->getEnd(), mSeg->getEnd()))
                return false;
            if (oSeg->value(key) != mSeg->value())
                return false;
        }
    }

    return true;
}

static StreamPipeline *createChain()
{
    bool enablePipelineAcceleration = false;
    bool enablePipelineSerialization = false;
    if (char *env = getenv("CPD3CLI_ACCELERATION")) {
        if (env[0] && atoi(env))
            enablePipelineAcceleration = true;
    }
    if (char *env = getenv("CPD3CLI_COMBINEPROCESSES")) {
        if (env[0] && atoi(env))
            enablePipelineSerialization = true;
    }

    StreamPipeline
            *chain = new StreamPipeline(enablePipelineAcceleration, enablePipelineSerialization);
    Q_ASSERT(chain);
    return chain;
}

static bool executeEditor(ComponentOptions &options, const QString &fileName)
{
    if (options.isSet("editor")) {
        if (!launchEditor(
                (qobject_cast<ComponentOptionSingleString *>(options.get("editor")))->get(),
                fileName)) {
            TerminalOutput::simpleWordWrap(QObject::tr("Unable to start editor command."), true);
            return false;
        }
        return true;
    } else {
        QByteArray check(qgetenv("EDITOR"));
        if (!check.isEmpty() && launchEditor(QString(check), fileName))
            return true;

#ifdef Q_OS_WIN32
        if (launchEditor("notepad.exe", fileName))
            return true;
#else
#ifdef Q_OS_UNIX
        check = qgetenv("DISPLAY");
        if (!check.isEmpty() && launchEditor("mousepad", fileName))
            return true;
        if (!check.isEmpty() && launchEditor("nedit -nowrap -noautosave --", fileName))
            return true;
        if (!check.isEmpty() && launchEditor("gedit", fileName))
            return true;
        if (!check.isEmpty() && launchEditor("gvim", fileName))
            return true;
#endif
        if (launchEditor("sensible-editor", fileName))
            return true;
        if (launchEditor("nano", fileName))
            return true;
        if (launchEditor("pico", fileName))
            return true;
        if (launchEditor("vim", fileName))
            return true;
        if (launchEditor("vi", fileName))
            return true;
        if (launchEditor("emacs", fileName))
            return true;
#endif

        TerminalOutput::simpleWordWrap(QObject::tr("Unable to find an available text editor."),
                                       true);
        return false;
    }
    return false;
}

static int editXML(ComponentOptions &options,
                   const Archive::Selection::List &selection,
                   Archive::Access &access,
                   ReadMode mode)
{
    QTemporaryFile modifyFile(QDir(QDir::tempPath()).filePath("cpd3_edit_XXXXXX.xml"));
    if (!modifyFile.open()) {
        TerminalOutput::simpleWordWrap(
                QObject::tr("Unable to open temporary file: %1").arg(modifyFile.errorString()),
                true);
        QCoreApplication::exit(1);
        return -1;
    }

    auto originalValues = access.readSynchronous(selection);
    for (auto v = originalValues.begin(); v != originalValues.end();) {
        if (v->getUnit().isDefaultStation()) {
            if (mode == Read_NoDefault) {
                v = originalValues.erase(v);
                continue;
            }
            if (mode == Read_DefaultMetadataOnly) {
                if (!v->getUnit().isMeta()) {
                    v = originalValues.erase(v);
                    continue;
                }
            }
        }

        ++v;
    }

    {
        QFile outputFlatFile(modifyFile.fileName());
        if (!outputFlatFile.open(QIODevice::WriteOnly)) {
            TerminalOutput::simpleWordWrap(
                    QObject::tr("Unable to open temporary file for writing: %1").arg(
                            outputFlatFile.errorString()), true);
            QCoreApplication::exit(1);
            return -1;
        }

        StandardDataOutput dataWrite(&outputFlatFile, StandardDataOutput::OutputType::XML);
        dataWrite.start();
        dataWrite.incomingData(originalValues);
        dataWrite.endData();
        Threading::pollInEventLoop([&] { return !dataWrite.isFinished(); });
        dataWrite.wait();
        outputFlatFile.close();
    }

    if (!executeEditor(options, modifyFile.fileName())) {
        QCoreApplication::exit(1);
        return -1;
    }

    SequenceValue::Transfer modifiedValues;
    {
        QFile inputFlatFile(modifyFile.fileName());
        if (!inputFlatFile.open(QIODevice::ReadOnly)) {
            TerminalOutput::simpleWordWrap(
                    QObject::tr("Unable to open temporary file for reading: %1").arg(
                            inputFlatFile.errorString()), true);
            QCoreApplication::exit(1);
            return -1;
        }

        StreamSink::Buffer ingress;
        StandardDataInput dataRead;
        dataRead.start();
        dataRead.setEgress(&ingress);
        dataRead.incomingData(inputFlatFile.readAll());
        dataRead.endData();
        dataRead.wait();
        modifiedValues = ingress.take();
        inputFlatFile.close();
    }

    for (auto mod = modifiedValues.begin(); mod != modifiedValues.end();) {
        bool hit = false;
        for (auto check = originalValues.begin(), endCheck = originalValues.end();
                check != endCheck;
                ++check) {
            if (!FP::equal(mod->getStart(), check->getStart()))
                continue;
            if (!FP::equal(mod->getEnd(), check->getEnd()))
                continue;
            if (mod->getPriority() != check->getPriority())
                continue;
            if (mod->getUnit() != check->getUnit())
                continue;

            if (mod->read() == check->read()) {
                /* No change, so do nothing with it */
                mod = modifiedValues.erase(mod);
                hit = true;
            }

            /* Have a match (modification or no change), so don't delete it */
            originalValues.erase(check);
            break;
        }
        if (!hit) {
            /* Changed or added, so keep it */
            ++mod;
        }
    }

    /* Anything that wasn't matched above should be removed */
    SequenceIdentity::Transfer deletedValues;
    for (auto &del : originalValues) {
        deletedValues.emplace_back(std::move(del));
    }

    Archive::Access().writeSynchronous(modifiedValues, deletedValues);

    QCoreApplication::exit(0);
    return 0;
}

static bool dataValueTimeSort(const SequenceValue &a, const SequenceValue &b)
{ return Range::compareStart(a.getStart(), b.getStart()) < 0; }

static void roundTime(double &time, bool isStart = true)
{
    if (!FP::defined(time))
        return;
    if (isStart)
        time = ::floor(time);
    else
        time = ::ceil(time);
}

static double roundTimeOutput(double time, bool isStart = true)
{
    if (!FP::defined(time))
        return time;
    if (isStart)
        time = ::floor(time);
    else
        time = ::ceil(time);
    return time;
}

static int process(QStringList arguments)
{
    if (!arguments.isEmpty())
        arguments.removeFirst();

    ComponentOptions options;

    ComponentOptionEnum *modeOption =
            new ComponentOptionEnum(QObject::tr("mode", "name"), QObject::tr("Editing mode"),
                                    QObject::tr("This sets the mode the flat file generator "
                                                        "works in."),
                                    QObject::tr("Read data from the archive", "mode default"));
    modeOption->add(Mode_ReadArchive, "read", QObject::tr("read", "mode name"),
                    QObject::tr("Read data from the archive and generate a flat "
                                        "file as output."));
    modeOption->add(Mode_WriteArchive, "write", QObject::tr("write", "mode name"),
                    QObject::tr("Write data to the archive from an input flag "
                                        "file."));
    modeOption->add(Mode_Edit, "edit", QObject::tr("edit", "mode name"),
                    QObject::tr("Read data from the archive to a temporary file "
                                        "then open an editor on that file.  Once the editor is closed "
                                        "write the changes back to the archive."));
    modeOption->add(Mode_EditXML, "xml", QObject::tr("xml", "mode name"),
                    QObject::tr("Read data from the archive to a temporary file in XML "
                                        "then open an editor on that file.  Once the editor is closed "
                                        "write the changes back to the archive."));
#ifndef NO_CONSOLE
    modeOption->add(Mode_ParseToOutput, "dataoutput", QObject::tr("dataoutput", "mode name"),
                    QObject::tr("Read and parse data from the file and generate data "
                                        "on standard output."));
    modeOption->add(Mode_GenerateFromInput, "datainput", QObject::tr("datainput", "mode name"),
                    QObject::tr("Read data from the archive or standard input and "
                                        "generate the flat file as output."));
#endif
    modeOption->alias(Mode_Edit, QObject::tr("modify", "mode name"));
    options.add("mode", modeOption);

    ComponentOptionEnum *readOption =
            new ComponentOptionEnum(QObject::tr("read", "name"), QObject::tr("Read mode"),
                                    QObject::tr("This sets the mode data are read with.  This "
                                                        "is used to control what data are displayed for editing."),
                                    QObject::tr("Accept default metadata", "read default"));
    readOption->add(Read_DefaultMetadataOnly, "metadata", QObject::tr("metadata", "name name"),
                    QObject::tr("Accept only metadata from the default archive.  All read "
                                        "default data are ignored."));
    readOption->add(Read_Everything, "everything", QObject::tr("everything", "read name"),
                    QObject::tr("Accept all input data."));
    readOption->add(Read_NoDefault, "nodefault", QObject::tr("nodefault", "read name"),
                    QObject::tr("Do not accept any default station data or metadata."));
    readOption->alias(Read_Everything, QObject::tr("all", "read name"));
    options.add("read", readOption);

    options.add("editor", new ComponentOptionSingleString(QObject::tr("editor", "name"),
                                                          QObject::tr(
                                                                  "The editor command to invoke"),
                                                          QObject::tr(
                                                                  "This is the editor command that will be invoked when "
                                                                          "in read->modify->write editing mode.  If not set then the editor "
                                                                          "will be selected by what is set in the $EDITOR environment "
                                                                          "variable.  If that is not set then an editor will be selected "
                                                                          "based on the presence of a GUI environment."),
                                                          QObject::tr("Automatically selected")));

    ComponentOptionFile *fileOption =
            new ComponentOptionFile(QObject::tr("file", "file option name"),
                                    QObject::tr("The input or output file name"), QObject::tr(
                            "This sets the file name to read or write to.  By default "
                                    "standard input or output is used."),
                                    QObject::tr("Disabled, used standard input/output",
                                                "default file"));
    fileOption->setMode(ComponentOptionFile::ReadWrite);
    fileOption->setTypeDescription(QObject::tr("Text files"));
    fileOption->setExtensions(QSet<QString>() << "txt" << "conf");
    options.add("file", fileOption);

    options.exclude("file", "editor");
    options.exclude("editor", "file");

    options.add("output-units",
                new ComponentOptionBoolean(QObject::tr("output-identifiers", "name"),
                                           QObject::tr("Always output data unit identifiers"),
                                           QObject::tr(
                                                   "When set, this causes the data identifiers (station, "
                                                           "archive, variable, and flavors) to always be output instead of "
                                                           "only output when needed."), QString()));

    options.add("output-bounds", new ComponentOptionBoolean(QObject::tr("output-bounds", "name"),
                                                            QObject::tr(
                                                                    "Always output data bounds"),
                                                            QObject::tr(
                                                                    "When set, this causes the data bounds to always be output "
                                                                            "instead of only output when needed."),
                                                            QString()));

    options.add("flat", new ComponentOptionBoolean(QObject::tr("flat", "name"),
                                                   QObject::tr("Output a completely flat display"),
                                                   QObject::tr(
                                                           "When set, this outputs on a single data indentifer and time bound "
                                                                   "to be output, instead of the full specification."),
                                                   QString()));

    options.exclude("output-units", "flat");
    options.exclude("output-bounds", "flat");
    options.exclude("flat", "output-units");
    options.exclude("flat", "output-bounds");

    Parser parser;
    try {
        parser.parse(arguments, options);
    } catch (ArgumentParsingException ape) {
        QString text(ape.getText());
        if (ape.isError()) {
            if (!text.isEmpty())
                TerminalOutput::simpleWordWrap(text, true);
            QCoreApplication::exit(1);
            return -1;
        } else {
            if (!text.isEmpty())
                TerminalOutput::simpleWordWrap(text);
            QCoreApplication::exit(0);
        }
        return 1;
    }

    int mode = Mode_ReadArchive;
    if (options.isSet("mode")) {
        mode = (qobject_cast<ComponentOptionEnum *>(options.get("mode")))->get().getID();
    }

    if (mode != Mode_ParseToOutput && mode != Mode_GenerateFromInput && arguments.isEmpty()) {
        TerminalOutput::simpleWordWrap(QObject::tr("A variable specification is required."), true);
        QCoreApplication::exit(1);
        return -1;
    }

    ReadMode readMode = Read_DefaultMetadataOnly;
    if (options.isSet("read")) {
        readMode = (ReadMode) (qobject_cast<ComponentOptionEnum *>(options.get("read")))->get()
                                                                                        .getID();
    }

    InputBreakdown inputData;
    Time::Bounds clip;
    Archive::Selection::List selection;

    if (!arguments.isEmpty()) {
        Archive::Access access;
        try {
            selection = ArchiveParse::parse(arguments, &clip, &access, true, {}, "configuration");
        } catch (AcrhiveParsingException ape) {
            TerminalOutput::simpleWordWrap(ape.getDescription(), true);
            QCoreApplication::exit(1);
            return -1;
        }

        roundTime(clip.start, true);
        roundTime(clip.end, false);
        for (auto &m : selection) {
            roundTime(m.start, true);
            roundTime(m.end, false);
        }

        if (mode == Mode_EditXML) {
            return editXML(options, selection, access, readMode);
        }
        inputData = breakdownData(access.readSynchronous(selection), readMode);
    } else if (mode == Mode_GenerateFromInput) {
        StreamPipeline *chain = createChain();

        if (!chain->setInputPipeline()) {
            TerminalOutput::simpleWordWrap(
                    QObject::tr("Error setting input pipeline: %1").arg(chain->getInputError()),
                    true);
            delete chain;
            QCoreApplication::exit(1);
            return -1;
        }

        StreamSink::Buffer target;
        if (!chain->setOutputIngress(&target)) {
            TerminalOutput::simpleWordWrap(
                    QObject::tr("Error setting output target: %1").arg(chain->getOutputError()),
                    true);
            delete chain;
            QCoreApplication::exit(1);
            return -1;
        }

        chain->start();
        chain->waitInEventLoop();
        delete chain;

        inputData = breakdownData(target.take(), readMode);
    }

    bool forceUnits = false;
    if (options.isSet("output-units")) {
        forceUnits = (qobject_cast<ComponentOptionBoolean *>(options.get("output-units")))->get();
    }
    bool forceBounds = false;
    if (options.isSet("output-bounds")) {
        forceBounds = (qobject_cast<ComponentOptionBoolean *>(options.get("output-bounds")))->get();
    }

    QTemporaryFile *modifyFlatFile = NULL;
    QFile *outputFlatFile = NULL;
    if (mode == Mode_ReadArchive || mode == Mode_GenerateFromInput) {
        if (options.isSet("file")) {
            outputFlatFile =
                    new QFile((qobject_cast<ComponentOptionFile *>(options.get("file")))->get());
            if (!outputFlatFile->open(QIODevice::WriteOnly)) {
                TerminalOutput::simpleWordWrap(
                        QObject::tr("Unable to open output file for writing: %1").arg(
                                outputFlatFile->errorString()), true);
                QCoreApplication::exit(1);
                return -1;
            }
        } else {
#ifndef NO_CONSOLE
            outputFlatFile = new QFile();
            if (!outputFlatFile->open(1, QIODevice::WriteOnly)) {
                TerminalOutput::simpleWordWrap(
                        QObject::tr("Unable to open stdout for writing: %1").arg(
                                outputFlatFile->errorString()), true);
                QCoreApplication::exit(1);
                return -1;
            }
#else
            TerminalOutput::simpleWordWrap(QObject::tr("An output file is required"), true);
            QCoreApplication::exit(1);
            return -1;
#endif
        }
    } else if (mode == Mode_Edit) {
        modifyFlatFile =
                new QTemporaryFile(QDir(QDir::tempPath()).filePath("cpd3_edit_XXXXXX.conf"));
        if (!modifyFlatFile->open()) {
            TerminalOutput::simpleWordWrap(QObject::tr("Unable to open temporary file: %1").arg(
                    modifyFlatFile->errorString()), true);
            QCoreApplication::exit(1);
            return -1;
        }
        outputFlatFile = new QFile(modifyFlatFile->fileName());
        if (!outputFlatFile->open(QIODevice::WriteOnly)) {
            TerminalOutput::simpleWordWrap(
                    QObject::tr("Unable to open temporary file for writing: %1").arg(
                            outputFlatFile->errorString()), true);
            QCoreApplication::exit(1);
            return -1;
        }
    }

    for (auto unitData = inputData.begin(); unitData != inputData.end();) {
        if (unitData->second.empty()) {
            unitData = inputData.erase(unitData);
            continue;
        }
        unitData->second
                .front().setStart(roundTimeOutput(unitData->second.front().getStart(), true));
        unitData->second.front().setEnd(roundTimeOutput(unitData->second.front().getEnd(), false));
        for (InputList::iterator s = unitData->second.begin() + 1; s != unitData->second.end();) {
            Q_ASSERT(FP::defined(s->getStart()));
            Q_ASSERT(FP::defined((s - 1)->getEnd()));
            if (s->getStart() <= (s - 1)->getEnd())
                s->setStart((s - 1)->getEnd());
            else
                s->setStart(roundTimeOutput(s->getStart(), true));
            s->setEnd(roundTimeOutput(s->getEnd(), false));
            if (Range::compareStartEnd(s->getStart(), s->getEnd()) >= 0) {
                s = unitData->second.erase(s);
                continue;
            }

            ++s;
        }

        while (!unitData->second.empty() &&
                Range::compareStartEnd(unitData->second.back().getStart(), clip.getEnd()) >= 0) {
            unitData->second.pop_back();
        }
        if (unitData->second.empty()) {
            unitData = inputData.erase(unitData);
            continue;
        }

        if (Range::compareStart(unitData->second.front().getStart(), clip.getStart()) < 0) {
            unitData->second.front().setStart(clip.getStart());
        }
        if (Range::compareEnd(unitData->second.back().getEnd(), clip.getEnd()) > 0) {
            unitData->second.back().setEnd(clip.getEnd());
        }

        ++unitData;
    }

    if (options.isSet("flat") &&
            (qobject_cast<ComponentOptionBoolean *>(options.get("flat")))->get()) {
        if (inputData.size() > 1) {
            std::vector<SequenceName> keys;
            for (const auto &add : inputData) {
                keys.emplace_back(add.first);
            }
            std::sort(keys.begin(), keys.end(), SequenceName::OrderLogical());
            auto &selected = keys.front();
            auto reduced = std::move(inputData[selected]);
            inputData.clear();
            inputData.emplace(std::move(selected), std::move(reduced));
        }
        for (auto &unitData : inputData) {
            if (unitData.second.size() <= 1)
                continue;
            auto first = unitData.second.begin();
            auto last = unitData.second.end();
            --last;
            unitData.second.erase(first, last);
        }
    }


    if (outputFlatFile != NULL) {
        QByteArray output(generateFlatFile(inputData, forceUnits, forceBounds).toUtf8());
        const char *data = output.constData();
        qint64 remaining = output.size();
        while (remaining > 0) {
            qint64 n = outputFlatFile->write(data, remaining);
            if (n < 0) {
                TerminalOutput::simpleWordWrap(
                        QObject::tr("Error writing output: %1").arg(outputFlatFile->errorString()),
                        true);
                QCoreApplication::exit(1);
                return -1;
            }
            remaining -= n;
            data += n;
        }

        outputFlatFile->close();
        delete outputFlatFile;
        outputFlatFile = NULL;
    }


    if (modifyFlatFile != NULL) {
        if (!executeEditor(options, modifyFlatFile->fileName())) {
            modifyFlatFile->close();
            delete modifyFlatFile;
            QCoreApplication::exit(1);
            return -1;
        }
    }


    QFile *inputFlatFile = NULL;
    if (mode == Mode_WriteArchive || mode == Mode_ParseToOutput) {
        if (options.isSet("file")) {
            inputFlatFile =
                    new QFile((qobject_cast<ComponentOptionFile *>(options.get("file")))->get());
            if (!inputFlatFile->open(QIODevice::ReadOnly)) {
                TerminalOutput::simpleWordWrap(
                        QObject::tr("Unable to open input file for reading: %1").arg(
                                inputFlatFile->errorString()), true);
                QCoreApplication::exit(1);
                return -1;
            }
        } else {
#ifndef NO_CONSOLE
            inputFlatFile = new QFile();
            if (!inputFlatFile->open(0, QIODevice::ReadOnly)) {
                TerminalOutput::simpleWordWrap(
                        QObject::tr("Unable to open stdin for reading: %1").arg(
                                inputFlatFile->errorString()), true);
                QCoreApplication::exit(1);
                return -1;
            }
#else
            TerminalOutput::simpleWordWrap(QObject::tr("An input file is required"), true);
            QCoreApplication::exit(1);
            return -1;
#endif
        }
    } else if (mode == Mode_Edit) {
        Q_ASSERT(modifyFlatFile != NULL);
        inputFlatFile = new QFile(modifyFlatFile->fileName());
        if (!inputFlatFile->open(QIODevice::ReadOnly)) {
            TerminalOutput::simpleWordWrap(
                    QObject::tr("Unable to open temporary file for reading: %1").arg(
                            inputFlatFile->errorString()), true);
            QCoreApplication::exit(1);
            return -1;
        }
    }

    if (inputFlatFile != NULL) {
        OutputBreakdown outputData;
        try {
            outputData = parseFlatFile(inputData, QString::fromUtf8(inputFlatFile->readAll()));
        } catch (std::exception &) {
            return -1;
        }

        inputFlatFile->close();
        delete inputFlatFile;
        inputFlatFile = NULL;

        for (auto unitData = outputData.begin(); unitData != outputData.end();) {
            while (!unitData->second.empty() &&
                    Range::compareStartEnd(unitData->second.back().getStart(), clip.getEnd()) >=
                            0) {
                unitData->second.pop_back();
            }
            if (unitData->second.empty()) {
                unitData = outputData.erase(unitData);
                continue;
            }

            if (Range::compareStart(unitData->second.front().getStart(), clip.getStart()) < 0) {
                /* If this truncates it down to zero length then just
                 * remove it */
                if (Range::compareStartEnd(unitData->second.front().getStart(),
                                           unitData->second.front().getEnd()) < 0 &&
                        Range::compareStartEnd(clip.getStart(),
                                               unitData->second.front().getEnd()) >= 0) {
                    unitData->second.pop_front();
                    if (unitData->second.empty()) {
                        unitData = outputData.erase(unitData);
                        continue;
                    }
                } else {
                    unitData->second.front().setStart(clip.getStart());
                }
            }
            if (Range::compareEnd(unitData->second.back().getEnd(), clip.getEnd()) > 0) {
                /* If this truncates it down to zero length then just
                 * remove it */
                if (Range::compareStartEnd(unitData->second.front().getStart(),
                                           unitData->second.front().getEnd()) < 0 &&
                        Range::compareStartEnd(unitData->second.front().getStart(),
                                               clip.getEnd()) >= 0) {
                    unitData->second.pop_back();
                    if (unitData->second.empty()) {
                        unitData = outputData.erase(unitData);
                        continue;
                    }
                } else {
                    unitData->second.back().setEnd(clip.getEnd());
                }
            }

            ++unitData;
        }

        if (mode == Mode_Edit) {
            if (checkIdentical(inputData, outputData)) {
                if (modifyFlatFile != NULL) {
                    modifyFlatFile->close();
                    delete modifyFlatFile;
                }

                TerminalOutput::simpleWordWrap(
                        QObject::tr("No changes detected, so the archive will not be modified."));
                QCoreApplication::exit(0);
                return 1;
            }
        }

        SequenceValue::Transfer sortedOutput;
        for (auto &unitData : outputData) {
            for (auto &seg : unitData.second) {
                sortedOutput.emplace_back(unitData.first, std::move(seg.root()), seg.getStart(),
                                          seg.getEnd());
            }
        }
        outputData.clear();
        std::sort(sortedOutput.begin(), sortedOutput.end(), dataValueTimeSort);

        if (mode == Mode_ParseToOutput) {
            StreamPipeline *chain = createChain();

            if (!chain->setOutputPipeline()) {
                TerminalOutput::simpleWordWrap(QObject::tr("Error setting output pipeline: %1").arg(
                        chain->getOutputError()), true);
                delete chain;
                QCoreApplication::exit(1);
                return -1;
            }

            StreamSink *target = chain->setInputExternal();
            if (target == NULL) {
                TerminalOutput::simpleWordWrap(
                        QObject::tr("Error setting input: %1").arg(chain->getInputError()), true);
                delete chain;
                QCoreApplication::exit(1);
                return -1;
            }

            chain->start();

            target->incomingData(std::move(sortedOutput));
            target->endData();

            chain->waitInEventLoop();
            delete chain;
        } else {
            Archive::Access access;

            access.writeSynchronous([&] {
                auto existing = access.readSynchronous(selection);
                SequenceIdentity::Transfer remove;
                SequenceValue::Transfer modify;

                for (const auto &value : existing) {
                    if (value.getUnit().isMeta())
                        continue;
                    if (value.getUnit().isDefaultStation())
                        continue;

                    remove.emplace_back(value);

                    if (Range::compareStart(value.getStart(), clip.getStart()) < 0) {
                        Q_ASSERT(FP::defined(clip.getStart()));
                        SequenceValue mod = value;
                        mod.setEnd(clip.getStart());
                        modify.emplace_back(std::move(mod));
                    }
                    if (Range::compareEnd(value.getEnd(), clip.getEnd()) > 0) {
                        Q_ASSERT(FP::defined(clip.getEnd()));
                        SequenceValue mod = value;
                        mod.setStart(clip.getEnd());
                        modify.emplace_back(std::move(mod));
                    }
                }

                access.writeSynchronous(modify, remove);
                access.writeSynchronous(sortedOutput, true);

                return true;
            });
        }
    }

    if (modifyFlatFile != NULL) {
        modifyFlatFile->close();
        delete modifyFlatFile;
    }

    QCoreApplication::exit(0);
    return 0;
}

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

    QString translateLocation;
    QByteArray cpd3(qgetenv("CPD3"));
    if (cpd3.length() > 0) {
        translateLocation = QString(cpd3) + "/locale";
    }
    QTranslator cpd3Translator;
    cpd3Translator.load("cpd3_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cpd3Translator);
    QTranslator cliTranslator;
    cliTranslator.load("cli_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cliTranslator);
    QTranslator ioserverTranslator;
    ioserverTranslator.load("configtext_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&ioserverTranslator);

    if (int rc = process(app.arguments())) {
        ThreadPool::system()->wait();
        if (rc == -1)
            return 1;
        return 0;
    }
    ThreadPool::system()->wait();
    return 0;
}
