/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QSqlDatabase>
#include <QTemporaryFile>
#include <QProcess>

#include "datacore/archive/access.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/externalsink.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class TestComponent : public QObject {
Q_OBJECT

    static bool verifyContents(QTemporaryFile &databaseFile, SequenceValue::Transfer expected)
    {
        auto result = Archive::Access(databaseFile).readSynchronous(Archive::Selection());

        for (auto rv = result.begin(); rv != result.end();) {
            auto check =
                    std::find_if(expected.begin(), expected.end(), [=](const SequenceValue &e) {
                        if (!FP::equal(e.getStart(), rv->getStart()))
                            return false;
                        if (!FP::equal(e.getEnd(), rv->getEnd()))
                            return false;
                        if (e.getUnit() != rv->getUnit())
                            return false;
                        if (e.getPriority() != rv->getPriority())
                            return false;
                        return e.getValue() == rv->getValue();
                    });
            if (check == expected.end()) {
                ++rv;
                continue;
            }

            rv = result.erase(rv);
            expected.erase(check);
        }

        if (!result.empty()) {
            qDebug() << "Unmatched result values: " << result;
            return false;
        }
        if (!expected.empty()) {
            qDebug() << "Unmatched expected values: " << expected;
            return false;
        }
        return true;
    }

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();
    }

    void cliHelp()
    {
        QVERIFY(qputenv("CPD3ARCHIVE", QByteArray("sqlite:_")));

        QProcess p;
        p.start("cpd3_config_text", QStringList() << "--help");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        QVERIFY(output.contains("Usage:"));
    }

    void process()
    {
        QFETCH(SequenceValue::Transfer, values);
        QFETCH(QString, formatted);

        QTemporaryFile temp;
        QVERIFY(temp.open());


        Variant::Root metaA;
        metaA.write().metadataHashChild("K1").metadataReal("Format").setString("0.0");
        metaA.write().metadataHashChild("K2")
             .metadataArray("Children")
             .metadataInteger("Format")
             .setString("FF");
        metaA.write().metadataHashChild("K3").metadataBoolean("Stuff").setString("A comment");
        metaA.write().metadataHashChild("K4")
             .metadataKeyframe("Children").metadataReal("MVC")
             .setString("XXX");

        Variant::Root metaB;
        metaB.write().metadataReal("Format").setString("00.00");

        Variant::Root metaC;
        metaC.write().metadataMatrix("Blarg").setString("Foobar");

        {
            SequenceValue::Transfer mod = values;
            mod.push_back(SequenceValue({"bnd", "configuration_meta", "a"}, metaA));
            mod.push_back(SequenceValue({"bnd", "configuration_meta", "b"}, metaB));
            mod.push_back(SequenceValue({"bnd", "configuration_meta", "c"}, metaC));
            mod.push_back(SequenceValue({"_", "configuration_meta", "a"}, metaA));
            Archive::Access(temp).writeSynchronous(mod);
        }


        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(temp.fileName())).toLatin1().data()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"), (QString("sqlite:%1").arg(temp.fileName())).toLatin1());


        QProcess p;
        p.start("cpd3_config_text",
                QStringList() << "--mode=read" << "allstations" << "everything" << "none"
                              << "none");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        QCOMPARE(output.trimmed(), formatted.trimmed());


        //p.setProcessChannelMode(QProcess::ForwardedChannels);
        p.start("cpd3_config_text",
                QStringList() << "--mode=write" << "allstations" << "everything" << "none"
                              << "none");
        p.write(formatted.toUtf8());
        p.closeWriteChannel();
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        auto checkValues = Archive::Access(temp).readSynchronous(Archive::Selection());
        auto checkExpected = values;
        for (const auto &verifyValue : checkValues) {
            if (verifyValue.getUnit().isMeta()) {
                if (verifyValue.getVariable() == "a") {
                    QCOMPARE(verifyValue.getValue(), metaA.read());
                    continue;
                }
                if (verifyValue.getVariable() == "b") {
                    QCOMPARE(verifyValue.getValue(), metaB.read());
                    continue;
                }
                if (verifyValue.getVariable() == "c") {
                    QCOMPARE(verifyValue.getValue(), metaC.read());
                    continue;
                }
                QFAIL("Unknown metadata variable");
                continue;
            }

            bool hit = false;
            for (auto v = checkExpected.begin(), endV = checkExpected.end(); v != endV; ++v) {
                if (v->getUnit() != verifyValue.getUnit())
                    continue;
                if (!FP::equal(v->getStart(), verifyValue.getStart()))
                    continue;
                if (!FP::equal(v->getEnd(), verifyValue.getEnd()))
                    continue;

                QCOMPARE(verifyValue.getValue(), v->getValue());
                hit = true;
                checkExpected.erase(v);
                break;
            }
            QVERIFY(hit);
        }
        QVERIFY(checkExpected.empty());


        p.start("cpd3_config_text",
                QStringList() << "--mode=edit" << "--editor=true" << "allstations" << "everything"
                              << "none" << "none");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
        output = QString::fromUtf8(p.readAllStandardOutput());
        QVERIFY(output.contains("No changes detected"));
    }

    void process_data()
    {
        QTest::addColumn<SequenceValue::Transfer>("values");
        QTest::addColumn<QString>("formatted");

        QTest::newRow("Empty") << SequenceValue::Transfer() << QString();

        QTest::newRow("Single double") << (SequenceValue::Transfer{
                SequenceValue({"bnd", "configuration", "a"}, Variant::Root(1.0))})
                                       << QString("/,1.0");
        QTest::newRow("Single integer") << (SequenceValue::Transfer{
                SequenceValue({"bnd", "configuration", "a"}, Variant::Root(2))}) << QString("/,2");
        QTest::newRow("Double times") << (SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "configuration", "a"), Variant::Root(1.0),
                              FP::undefined(), 1262304000),
                SequenceValue(SequenceName("bnd", "configuration", "a"), Variant::Root(2.0),
                              1262304000, FP::undefined())})
                                      << QString("(;2010-01-01T00:00:00Z)/,1.0\n"
                                                         "(2010-01-01T00:00:00Z;)/,2.0");
        QTest::newRow("Double units") << (SequenceValue::Transfer{
                SequenceValue({"bnd", "configuration", "a"}, Variant::Root(1.0)),
                SequenceValue({"bnd", "configuration", "b"}, Variant::Root(2.0))})
                                      << QString("<BND:CONFIGURATION:a>/,1.0\n"
                                                         "<BND:CONFIGURATION:b>/,2.00\n");

        Variant::Root v1;

        v1.write().setEmpty();
        v1.write().hash("K1").setDouble(3.1);
        v1.write().hash("K2").array(0).setInt64(0x1);
        v1.write().hash("K2").array(1).setInt64(0xA);
        v1.write().hash("K2").array(3).setInt64(0x2);
        v1.write().hash("K3").setBool(true);
        v1.write().hash("K4").keyframe(1.5).setDouble(FP::undefined());
        QTest::newRow("Basic path")
                << (SequenceValue::Transfer{SequenceValue({"bnd", "configuration", "a"}, v1)})
                << QString("/K1,3.1\n"
                                   "/K2/#0,1\n"
                                   "/K2/#1,A\n"
                                   "/K2/#2,_\n"
                                   "/K2/#3,2\n"
                                   "/K3,TRUE\n"
                                   "/K4/@1.5,XXX\n");

        v1.write().setEmpty();
        v1.write().hash("A").setDouble(1.5);
        v1.write().hash("B").setDouble(1.6);
        QTest::newRow("No meta path")
                << (SequenceValue::Transfer{SequenceValue({"bnd", "configuration", "d"}, v1)})
                << QString("/A,1.5\n"
                                   "/B,1.6\n");

        v1.write().setEmpty();
        v1.write().matrix({0, 0}).setDouble(1.1);
        v1.write().matrix({1, 0}).setDouble(2.1);
        v1.write().matrix({2, 0}).setDouble(3.1);
        v1.write().matrix({3, 0}).setDouble(4.1);
        v1.write().matrix({0, 1}).setDouble(1.2);
        v1.write().matrix({1, 1}).setDouble(2.2);
        v1.write().matrix({2, 1}).setDouble(3.2);
        v1.write().matrix({3, 1}).setDouble(4.2);
        QTest::newRow("Matrix path")
                << (SequenceValue::Transfer{SequenceValue({"bnd", "configuration", "c"}, v1)})
                << QString("/[0:0],1.1\n"
                                   "/[0:1],1.2\n"
                                   "/[1:0],2.1\n"
                                   "/[1:1],2.2\n"
                                   "/[2:0],3.1\n"
                                   "/[2:1],3.2\n"
                                   "/[3:0],4.1\n"
                                   "/[3:1],4.2\n");

        v1.write().setEmpty();
        v1.write().metadataReal("MP1").setDouble(2.0);
        v1.write().metadataReal("MP2").setString("Foobar");
        QTest::newRow("Metadata path")
                << (SequenceValue::Transfer{SequenceValue({"bnd", "configuration", "d"}, v1)})
                << QString("/*rMP1,2.0\n"
                                   "/*rMP2,\"Foobar\"\n");

        v1.write().setEmpty();
        v1.write().hash("Simple").hash("Basic").hash("\\Odd\"").setString("");
        QTest::newRow("Strange element")
                << (SequenceValue::Transfer{SequenceValue({"bnd", "configuration", "d"}, v1)})
                << QString("/Simple/Basic/\\\\Odd\",\"\"\n");
    }

    void timeClipping()
    {
        QTemporaryFile temp;
        QVERIFY(temp.open());

        Variant::Root v1;
        v1.write().hash("Q").setDouble(1.0);
        Variant::Root v2;
        v2.write().hash("Q").setDouble(2.0);

        Archive::Access(temp).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"bnd", "configuration", "a"}, v1, FP::undefined(), 1414368000),
                SequenceValue({"bnd", "configuration", "a"}, v2, 1414368000, FP::undefined())});

        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(temp.fileName())).toLatin1().data()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"), (QString("sqlite:%1").arg(temp.fileName())).toLatin1());

        QProcess p;
        p.start("cpd3_config_text",
                QStringList() << "--mode=read" << "allstations" << "everything" << "2014-10-28"
                              << "none");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        QCOMPARE(output.trimmed(), QString("/Q,2.0"));

        p.start("cpd3_config_text",
                QStringList() << "--mode=write" << "allstations" << "everything" << "2014-10-28"
                              << "none");
        p.write(QString("/Q,3.0").toUtf8());
        p.closeWriteChannel();
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        Variant::Root v3;
        v3.write().hash("Q").setDouble(3.0);

        QVERIFY(verifyContents(temp, SequenceValue::Transfer{
                SequenceValue({"bnd", "configuration", "a"}, v1, FP::undefined(), 1414368000),
                SequenceValue({"bnd", "configuration", "a"}, v2, 1414368000, 1414454400),
                SequenceValue({"bnd", "configuration", "a"}, v3, 1414454400, FP::undefined())}));
    }

    void flatOutput()
    {
        QTemporaryFile temp;
        QVERIFY(temp.open());

        Variant::Root v1;
        v1.write().hash("Q").setDouble(1.0);
        Variant::Root v2;
        v2.write().hash("Q").setDouble(2.0);
        Variant::Root v3;
        v3.write().hash("R").setDouble(3.0);

        Archive::Access(temp).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"bnd", "configuration", "b"}, v3, 1414368000, 1414368001),
                SequenceValue({"bnd", "configuration", "a"}, v1, FP::undefined(), 1414368000.010),
                SequenceValue({"bnd", "configuration", "a"}, v2, 1414368000.005, FP::undefined())});

        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(temp.fileName())).toLatin1().data()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"), (QString("sqlite:%1").arg(temp.fileName())).toLatin1());

        QProcess p;
        p.start("cpd3_config_text",
                QStringList() << "--mode=read" << "--flat" << "allstations" << "everything"
                              << "forever");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        QCOMPARE(output.trimmed(), QString("/Q,2.0"));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"