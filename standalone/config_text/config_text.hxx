/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CONFIGTEXT_H
#define CONFIGTEXT_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QHash>
#include <QList>

#include "datacore/stream.hxx"
#include "datacore/segment.hxx"

typedef CPD3::Data::SequenceSegment::Transfer InputList;
typedef CPD3::Data::SequenceName::Map<InputList> InputBreakdown;

typedef std::deque<CPD3::Data::ValueSegment> OutputList;
typedef CPD3::Data::SequenceName::Map<OutputList> OutputBreakdown;

QString generateFlatFile(const InputBreakdown &values, bool forceUnits, bool forceBounds);

OutputBreakdown parseFlatFile(InputBreakdown existing, const QString &input);

#endif
