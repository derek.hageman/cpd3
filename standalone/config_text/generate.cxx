/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <unordered_map>

#include "datacore/stream.hxx"
#include "datacore/variant/parse.hxx"
#include "core/range.hxx"

#include "config_text.hxx"

using namespace CPD3;
using namespace CPD3::Data;

struct GeneratePathBounds {
    double start;
    double end;

    inline double getStart() const
    { return start; }

    inline double getEnd() const
    { return end; }

    bool operator<(const GeneratePathBounds &other) const
    { return Range::compareStart(start, other.start) < 0; }

    explicit GeneratePathBounds(const SequenceSegment &seg) : start(seg.getStart()),
                                                              end(seg.getEnd())
    { }

    bool operator==(const GeneratePathBounds &other) const
    {
        return FP::equal(start, other.start) && FP::equal(end, other.end);
    }
};

namespace std {

template<>
struct hash<GeneratePathBounds> {
    std::size_t operator()(const GeneratePathBounds &s) const
    {
        std::size_t result = 0;
        if (FP::defined(s.start))
            result = std::hash<double>()(s.start);
        if (FP::defined(s.end))
            result = INTEGER::mix(result, std::hash<double>()(s.end));
        return result;
    }
};

}

static Variant::Read childMetadata(const Variant::Read &child, const Variant::Read &parentMetadata)
{
    return Variant::Parse::evaluateMetadata(
            parentMetadata.getPath(Variant::PathElement::toMetadata(child.currentPath().back())),
            child);
}

typedef std::unordered_map<Variant::Path, QString> ValueBreakdown;

static void buildBreakdownPath(const Variant::Read &value,
                               const Variant::Read &metadata,
                               ValueBreakdown &result)
{
    switch (value.getType()) {
    case Variant::Type::Empty:
    case Variant::Type::Real:
    case Variant::Type::Integer:
    case Variant::Type::Boolean:
    case Variant::Type::String:
    case Variant::Type::Bytes:
    case Variant::Type::Flags:
    case Variant::Type::Overlay:
        break;

    case Variant::Type::Array: {
        bool hasChildren = false;
        for (auto child : value.toArray()) {
            hasChildren = true;
            buildBreakdownPath(child, childMetadata(child, metadata), result);
        }
        if (hasChildren)
            return;
        break;
    }
    case Variant::Type::Hash: {
        bool hasChildren = false;
        for (auto child : value.toHash()) {
            hasChildren = true;
            buildBreakdownPath(child.second, childMetadata(child.second, metadata), result);
        }
        if (hasChildren)
            return;
        break;
    }
    case Variant::Type::Keyframe: {
        bool hasChildren = false;
        for (auto child : value.toKeyframe()) {
            hasChildren = true;
            buildBreakdownPath(child.second, childMetadata(child.second, metadata), result);
        }
        if (hasChildren)
            return;
        break;
    }

    case Variant::Type::Matrix: {
        bool hasChildren = false;
        for (auto child : value.toMatrix()) {
            hasChildren = true;
            buildBreakdownPath(child.second, childMetadata(child.second, metadata), result);
        }
        if (hasChildren)
            return;
        break;
    }

    case Variant::Type::MetadataReal:
    case Variant::Type::MetadataInteger:
    case Variant::Type::MetadataBoolean:
    case Variant::Type::MetadataString:
    case Variant::Type::MetadataBytes:
    case Variant::Type::MetadataArray:
    case Variant::Type::MetadataMatrix:
    case Variant::Type::MetadataKeyframe: {
        bool hasChildren = false;
        for (auto child : value.toMetadataSpecialized()) {
            hasChildren = true;
            buildBreakdownPath(child.second, childMetadata(child.second, metadata), result);
        }
        if (hasChildren)
            return;
        break;
    }

    case Variant::Type::MetadataFlags: {
        bool hasChildren = false;
        for (auto child : value.toMetadataFlags()) {
            hasChildren = true;
            buildBreakdownPath(child.second, childMetadata(child.second, metadata), result);
        }
        for (auto child : value.toMetadataSingleFlag()) {
            hasChildren = true;
            buildBreakdownPath(child.second, childMetadata(child.second, metadata), result);
        }
        if (hasChildren)
            return;
        break;
    }

    case Variant::Type::MetadataHash: {
        bool hasChildren = false;
        for (auto child : value.toMetadataHash()) {
            hasChildren = true;
            buildBreakdownPath(child.second, childMetadata(child.second, metadata), result);
        }
        for (auto child : value.toMetadataHashChild()) {
            hasChildren = true;
            buildBreakdownPath(child.second, childMetadata(child.second, metadata), result);
        }
        if (hasChildren)
            return;
        break;
    }
    }

    result.emplace(value.currentPath(), Variant::Parse::toInputString(value, metadata));
}

static ValueBreakdown buildPathBreakdownRoot(const Variant::Read &value,
                                             const Variant::Read &metadata)
{
    ValueBreakdown result;
    buildBreakdownPath(value, Variant::Parse::evaluateMetadata(metadata, value), result);
    return result;
}

static QString toPath(const Variant::Path &input)
{
    QString path;
    for (const auto &add : input) {
        path += '/';
        std::string pathString = add.toString();
        Util::replace_all(pathString, ",", "\\,");
        path += QString::fromStdString(pathString);
    }
    if (path.isEmpty())
        path = "/";
    return path;
}

QString generateFlatFile(const InputBreakdown &values, bool forceUnits, bool forceBounds)
{
    SequenceName::ComponentSet seenStations;
    SequenceName::ComponentSet seenArchives;
    SequenceName::ComponentSet seenVariables;
    SequenceName::FlavorsSet seenFlavors;
    std::unordered_set<GeneratePathBounds> seenBounds;
    std::unordered_set<Variant::Path> seenPaths;

    SequenceName::Map<std::unordered_map<GeneratePathBounds, ValueBreakdown>> breakdownData;
    for (const auto &data : values) {
        bool anyValidData = false;
        for (const auto &segment : data.second) {
            auto valueData = segment.getValue(data.first);
            if (!valueData.exists())
                continue;

            GeneratePathBounds bounds(segment);
            auto paths = buildPathBreakdownRoot(valueData, segment.getValue(data.first.toMeta()));
            for (const auto &add : paths) {
                seenPaths.insert(add.first);
            }
            seenBounds.insert(bounds);

            breakdownData[data.first].emplace(std::move(bounds), std::move(paths));

            anyValidData = true;
        }

        if (!anyValidData)
            continue;

        seenStations.insert(data.first.getStation());
        seenArchives.insert(data.first.getArchive());
        seenVariables.insert(data.first.getVariable());
        seenFlavors.insert(data.first.getFlavors());
    }

    bool includeName = forceUnits ||
            (seenStations.size() > 1 ||
                    seenArchives.size() > 1 ||
                    seenVariables.size() > 1 ||
                    seenFlavors.size() > 1);
    bool includeBounds = forceBounds || (seenBounds.size() > 1);

    std::vector<Variant::Path> sortedPaths(seenPaths.begin(), seenPaths.end());
    std::sort(sortedPaths.begin(), sortedPaths.end(),
              [](const Variant::Path &a, const Variant::Path &b) {
                  std::size_t max = std::min(a.size(), b.size());
                  std::size_t i = 0;
                  for (auto ca = a.begin(), cb = b.begin(); i < max; ++i, ++ca, ++cb) {
                      if (*ca == *cb)
                          continue;
                      return Variant::PathElement::OrderDisplay()(*ca, *cb);
                  }

                  if (i >= a.size()) {
                      return i < b.size();
                  }

                  return false;
              });

    std::vector<SequenceName> sortedNames;
    for (const auto &add : breakdownData) {
        sortedNames.emplace_back(add.first);
    }
    std::sort(sortedNames.begin(), sortedNames.end(), SequenceName::OrderDisplayCaching());

    QString outputString;
    for (const auto &path : sortedPaths) {
        QString pathText = toPath(path);

        for (const auto &name : sortedNames) {
            auto nameData = breakdownData.find(name);
            if (nameData == breakdownData.end())
                continue;

            QString namePrefix;
            if (includeName) {
                namePrefix.append('<');
                namePrefix.append(name.getStationQString().toUpper());
                namePrefix.append(':');
                namePrefix.append(name.getArchiveQString().toUpper());
                namePrefix.append(':');
                namePrefix.append(name.getVariableQString());
                QStringList sorted;
                for (const auto &add : name.getFlavors()) {
                    sorted.push_back(QString::fromStdString(add));
                }
                if (!sorted.isEmpty()) {
                    namePrefix.append(':');
                    std::sort(sorted.begin(), sorted.end());
                    namePrefix.append(sorted.join(":"));
                }
                namePrefix.append('>');
            }

            std::vector<GeneratePathBounds> sortedBounds;
            for (const auto &add : nameData->second) {
                sortedBounds.emplace_back(add.first);
            }
            std::sort(sortedBounds.begin(), sortedBounds.end());

            for (const auto &bounds : sortedBounds) {
                auto boundsData = nameData->second.find(bounds);
                if (boundsData == nameData->second.end())
                    continue;

                auto leafData = boundsData->second.find(path);
                if (leafData == boundsData->second.end())
                    continue;

                QString boundPrefix = namePrefix;
                if (includeBounds) {
                    boundPrefix.append('(');
                    if (FP::defined(bounds.getStart()))
                        boundPrefix.append(Time::toISO8601(bounds.getStart()));
                    boundPrefix.append(';');
                    if (FP::defined(bounds.getEnd()))
                        boundPrefix.append(Time::toISO8601(bounds.getEnd()));
                    boundPrefix.append(')');
                }

                outputString.append(boundPrefix);
                outputString.append(pathText);
                outputString.append(',');
                outputString.append(leafData->second);
                outputString.append('\n');
            }
        }
    }

    return outputString;
}
