/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTranslator>
#include <QCoreApplication>
#include <QStringList>
#include <QLibraryInfo>
#include <QLocale>
#include <QLoggingCategory>
#include <QDataStream>
#include <QBuffer>

#include "core/abort.hxx"
#include "core/threadpool.hxx"
#include "core/qtcompat.hxx"
#include "core/component.hxx"
#include "core/memory.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/stream.hxx"
#include "datacore/valueoptionparse.hxx"
#include "clicore/terminaloutput.hxx"
#include "clicore/graphics.hxx"
#include "io/drivers/stdio.hxx"


Q_LOGGING_CATEGORY(log_detached_task, "cpd3.component.tasks.detached", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::CLI;

#if defined(ALLOCATOR_TYPE) && ALLOCATOR_TYPE == 1

#include <tbb/scalable_allocator.h>

static void allocatorRelease()
{
    ::free(nullptr);
#ifdef HAVE_TBB_COMMAND
    scalable_allocation_command(TBBMALLOC_CLEAN_ALL_BUFFERS, nullptr);
#endif
}

static void installAllocator()
{ CPD3::Memory::install_release_function(allocatorRelease); }

#elif defined(ALLOCATOR_TYPE) && ALLOCATOR_TYPE == 2
#include <gperftools/malloc_extension.h>
static void allocatorRelease()
{ MallocExtension::instance()->ReleaseFreeMemory(); }
static void installAllocator()
{ CPD3::Memory::install_release_function(allocatorRelease); }
#else
static void installAllocator()
{ }
#endif

int main(int argc, char **argv)
{
    installAllocator();

    std::unique_ptr<QCoreApplication> app(CLIGraphics::createApplication(argc, argv));

    qRegisterMetaType<SequenceValue>("CPD3::Data::SequenceValue");
    qRegisterMetaType<SequenceValue::Transfer>("CPD3::Data::SequenceValue::Transfer");

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app->installTranslator(&qtTranslator);

    QString translateLocation;
    QByteArray cpd3(qgetenv("CPD3"));
    if (cpd3.length() > 0) {
        translateLocation = QString(cpd3) + "/locale";
    }
    QTranslator cpd3Translator;
    cpd3Translator.load("cpd3_" + QLocale::system().name(), translateLocation);
    app->installTranslator(&cpd3Translator);
    QTranslator cliTranslator;
    cliTranslator.load("cli_" + QLocale::system().name(), translateLocation);
    app->installTranslator(&cliTranslator);

    Abort::installAbortHandler(true);

    Variant::Root config;
    {
#ifdef Q_OS_UNIX
        auto input = IO::Access::stdio_in();
        if (!input) {
            qFatal("Standard input open error");
            return 1;
        }
        auto stream = input->stream();
        if (!stream) {
            qFatal("Standard input stream error");
            return 2;
        }
        auto data = stream->readAll();
        if (!data) {
            qFatal("No configuration data");
            return 3;
        }
        Util::ByteArray::Device device(data);
#else
        auto arguments = QCoreApplication::arguments();
        if (arguments.size() < 2) {
            qFatal("No arguments provided");
            return 1;
        }
        auto data = QByteArray::fromBase64(arguments[1].toLatin1());
        QBuffer device(&data);
#endif
        if (!device.open(QIODevice::ReadOnly)) {
            qFatal("Device open error");
            return 4;
        }
        QDataStream ds(&device);
        config = Variant::Root::deserialize(ds);
        if (ds.status() != QDataStream::Ok) {
            qFatal("Configuration deserialization error");
            return 5;
        }
    }

    QObject *component = ComponentLoader::create(config["Name"].toQString());
    if (!component) {
        qFatal("Component load failure");
        return 6;
    }

    std::vector<SequenceName::Component> stations;
    for (auto add : config["Stations"].toArray()) {
        stations.emplace_back(add.toString());
    }

    std::unique_ptr<AbortPoller> abortPoller(new AbortPoller);
    QObject::connect(abortPoller.get(), &AbortPoller::aborted, app.get(), &QCoreApplication::quit);

    std::unique_ptr<CPD3Action> action;
    if (ActionComponent *actionComponent = qobject_cast<ActionComponent *>(component)) {
        ComponentOptions o = actionComponent->getOptions();
        ValueOptionParse::parse(o, config["Options"]);
        action.reset(actionComponent->createAction(o, stations));
    }
    if (!action) {
        if (ActionComponentTime
                *actionComponentTime = qobject_cast<ActionComponentTime *>(component)) {
            ComponentOptions o = actionComponentTime->getOptions();
            ValueOptionParse::parse(o, config["Options"]);

            if (config["Start"].exists() || config["End"].exists()) {
                action.reset(actionComponentTime->createTimeAction(o, config["Start"].toReal(),
                                                                   config["End"].toReal(),
                                                                   stations));
            } else {
                action.reset(actionComponentTime->createTimeAction(o, stations));
            }
        }
    }
    if (!action) {
        qFatal("Error creating component");
        return 7;
    }

    qCDebug(log_detached_task) << "Started detached task for" << config["Name"].toString() << "with"
                               << config["Stations"].toArray().size() << "station(s)";

    QObject::connect(abortPoller.get(), &AbortPoller::aborted, action.get(),
                     &CPD3Action::signalTerminate);
    QObject::connect(action.get(), &QThread::finished, app.get(), &QCoreApplication::quit);

    action->start();
    abortPoller->start();
    int rc = app->exec();

    abortPoller.reset();
    action->wait();
    action.reset();

    ThreadPool::system()->wait();
    app.reset();

    return rc;
}
