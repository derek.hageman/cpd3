/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#ifdef Q_OS_UNIX

#include <unistd.h>

#endif

#include <QTranslator>

#if !defined(NO_CONSOLE) || !defined(BUILD_GUI)

#include <QCoreApplication>

#else
#include <QApplication>
#endif

#ifdef NO_CONSOLE
#include <QMessageBox>
#endif

#include <QStringList>
#include <QLibraryInfo>
#include <QLoggingCategory>

#include "core/abort.hxx"
#include "core/number.hxx"
#include "core/range.hxx"
#include "core/component.hxx"
#include "core/stdindevice.hxx"
#include "core/process.hxx"
#include "core/threadpool.hxx"
#include "clicore/terminaloutput.hxx"
#include "clicore/optionparse.hxx"
#include "clicore/argumentparser.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/segment.hxx"

#include "sync_peer.hxx"


Q_LOGGING_CATEGORY(log_syncpeer, "cpd3.syncpeer", QtWarningMsg)

#define FORWARD_OUPUT

using namespace CPD3;
using namespace CPD3::CLI;
using namespace CPD3::Sync;
using namespace CPD3::Data;

class SyncTCPSocket : public QTcpSocket {
public:
    SyncTCPSocket(QObject *parent = 0) : QTcpSocket(parent)
    { }

    virtual ~SyncTCPSocket()
    { }

    virtual bool atEnd() const
    {
        QAbstractSocket::SocketState s = state();
        return s == QAbstractSocket::UnconnectedState || s == QAbstractSocket::ClosingState;
    }
};

class SyncTCPServer : public QTcpServer {
    QList<QTcpSocket *> pending;
public:
    SyncTCPServer(QObject *parent = 0) : QTcpServer(parent)
    { }

    virtual ~SyncTCPServer()
    { }

    virtual bool hasPendingConnections() const
    { return !pending.isEmpty(); }

    virtual QTcpSocket *nextPendingConnection()
    {
        if (pending.isEmpty())
            return 0;
        return pending.takeFirst();
    }

protected:
    virtual void incomingConnection(qintptr socketDescriptor)
    {
        QTcpSocket *socket = new SyncTCPSocket(this);
        if (!socket->setSocketDescriptor(socketDescriptor, QAbstractSocket::ConnectedState,
                                         QIODevice::ReadWrite)) {
            socket->close();
            delete socket;
            return;
        }
        socket->setReadBufferSize(8388608);

        pending.append(socket);
        emit newConnection();
    }
};

#ifndef NO_CONSOLE

class StdioForward : public QIODevice {
    StdinDevice *input;
    QFile *output;
public:
    StdioForward(QObject *parent = 0) : QIODevice(parent), input(NULL), output(NULL)
    { }

    virtual ~StdioForward()
    {
        if (input != NULL)
            delete input;
        if (output != NULL)
            delete output;
    }

    virtual bool atEnd() const
    {
        if (input == NULL)
            return true;
        return input->atEnd();
    }

    virtual qint64 bytesAvailable() const
    {
        if (input == NULL)
            return 0;
        return input->bytesAvailable();
    }

    virtual qint64 bytesToWrite() const
    {
        if (output == NULL)
            return 0;
        return output->bytesToWrite();
    }

    virtual void close()
    {
        if (input != NULL) {
            input->close();
            delete input;
            input = NULL;
        }
        if (output != NULL) {
            output->close();
            delete output;
            output = NULL;
        }
    }

    virtual bool isSequential() const
    { return true; }

    virtual bool open(QIODevice::OpenMode mode)
    {
        close();
        if (mode & QIODevice::ReadOnly) {
            input = new StdinDevice(this);
            connect(input, SIGNAL(readChannelFinished()), this, SIGNAL(readChannelFinished()),
                    Qt::DirectConnection);
            connect(input, SIGNAL(readyRead()), this, SIGNAL(readyRead()), Qt::DirectConnection);
            if (!input->open(mode & ~(QIODevice::WriteOnly))) {
                setErrorString(input->errorString());
                return false;
            }
        }
        if (mode & QIODevice::WriteOnly) {
            output = new QFile(this);
            Q_ASSERT(output);
            connect(output, SIGNAL(bytesWritten(qint64)), this, SIGNAL(bytesWritten(qint64)),
                    Qt::DirectConnection);
            if (!output->open(1, mode & ~(QIODevice::ReadOnly))) {
                setErrorString(output->errorString());
                return false;
            }
        }
        return QIODevice::open(mode);
    }

    virtual bool waitForBytesWritten(int msecs)
    {
        if (output == NULL)
            return false;
        return output->waitForBytesWritten(msecs);
    }

    virtual bool waitForReadyRead(int msecs)
    {
        if (input == NULL)
            return false;
        return input->waitForReadyRead(msecs);
    }

protected:
    virtual qint64 readData(char *data, qint64 maxSize)
    {
        if (input == NULL)
            return -1;
        qint64 n = input->read(data, maxSize);
        if (n < 0) {
            setErrorString(input->errorString());
            input->close();
            delete input;
            input = NULL;
            emit readChannelFinished();
            return -1;
        }
        return n;
    }

    virtual qint64 writeData(const char *data, qint64 maxSize)
    {
        if (output == NULL)
            return -1;
        qint64 remaining = maxSize;
        while (remaining > 0) {
            qint64 n = output->write(data, maxSize);
            if (n < 0) {
                setErrorString(output->errorString());
                output->close();
                delete output;
                output = NULL;
                return -1;
            }
            remaining -= n;
            data += n;
            output->waitForBytesWritten(1000);
        }
        return maxSize;
    }
};

#endif

class Parser : public ArgumentParser {
public:
    Parser()
    { }

    virtual ~Parser()
    { }

protected:
    virtual QList<BareWordHelp> getBareWordHelp()
    {
        return QList<BareWordHelp>() << BareWordHelp(tr("station", "station argument name"),
                                                     tr("The station to accept data for"),
                                                     tr("Inferred from the current directory"),
                                                     tr("The \"station\" bare word argument defines the default station used during "
                                                        "data acceptance checking.  This sets both the profile loaded and the "
                                                        "station used for incoming data filtering."));
    }

    virtual QString getBareWordCommandLine()
    {
        return tr("[station]", "command line");
    }

    virtual QString getProgramName()
    { return tr("cpd3.sync.peer", "program name"); }

    virtual QString getDescription()
    {
        return tr("This performs bi-directional synchronization of data with another peer "
                  "on the remote end.  The connection between peers is usually tunneled or "
                  "otherwise set up externally.");
    }

    virtual QHash<QString, QString> getDocumentationTypeID()
    {
        QHash<QString, QString> result;
        result.insert("type", "sync_peer");
        return result;
    }
};

static const quint8 rcSynchronizeStateVersion = 1;

int SyncPeer::parseArguments(const QStringList &rawArguments)
{

    ComponentOptions options;

    options.add("profile",
                new ComponentOptionSingleString(tr("profile", "name"), tr("Synchronize profile"),
                                                tr("This is the authorization profile to synchronize.  The profile "
                                                   "controls what data are accepted from the input.  "
                                                   "Consult the station configuration and authorization for available profiles."),
                                                tr("aerosol")));

    options.add("disable-lock",
                new ComponentOptionBoolean(tr("disable-lock", "disable lock option name"),
                                           tr("Do not lock output"),
                                           tr("If set then a lock is not acquired before synchronization "
                                              "output.  This also means that data from all time is output, not just "
                                              "the data modified since the last synchronize."),
                                           QString()));

    options.add("disable-compression", new ComponentOptionBoolean(
            tr("disable-compression", "disable compression option name"),
            tr("Do not compress output"),
            tr("If set this is set then the output is not compressed before "
               "writing.  This is normally used when the transport stream already "
               "has compression applied (e.x. SSH tunnels)."), QString()));

    options.add("connect",
                new ComponentOptionSingleString(tr("connect", "name"), tr("Host to connect to"),
                                                tr("This is the hostname of that connection will be attempted to."
                                                   #if defined(Q_OS_UNIX)
                                                   "  When neither this nor the listen option are set the the exchange "
                                                   "is through standard output and standard input."
#endif
                                                ),
#if defined(Q_OS_UNIX)
                                                QString()
#else
                        tr("any", "default listen host")
#endif
                ));

    options.add("listen",
                new ComponentOptionSingleString(tr("listen", "name"), tr("Host to listen on"),
                                                tr("This is the hostname that connections will be listened for on.  An "
                                                   "empty setting listens on all available hosts."
                                                   #if defined(Q_OS_UNIX)
                                                   "  When neither this nor the listen option are set the the exchange "
                                                   "is through standard output and standard input."
#endif
                                                ), QString()));

    options.add("rsh",
                new ComponentOptionSingleString(tr("rsh", "name"), tr("Remote shell command"),
                                                tr("When set, this causes the program to invoke the given command "
                                                   "then attempt to communicate with its input and output.  This can "
                                                   "be used to tunnel a synchronize over a remote shell."),
                                                QString()));

    options.exclude("connect", "listen");
    options.exclude("connect", "rsh");
    options.exclude("listen", "connect");
    options.exclude("listen", "rsh");
    options.exclude("rsh", "listen");
    options.exclude("rsh", "connect");
    options.exclude("rsh", "port");
    options.exclude("port", "rsh");

    {
        ComponentOptionSingleInteger *i =
                new ComponentOptionSingleInteger(tr("port", "name"), tr("The port to use"),
                                                 tr("This is the port that connection and listening are performed "
                                                    "on."), tr("14231"));
        i->setAllowUndefined(false);
        i->setMinimum(1);
        i->setMaximum(65535);
        options.add("port", i);
    }

#ifndef NO_CONSOLE
    options.add("quiet",
                new ComponentOptionBoolean(tr("quiet", "quiet option name"), tr("Suppress output"),
                                           tr("If set then no progress output is displayed."),
                                           tr("Disabled")));
#endif

    QStringList arguments(rawArguments);
    if (!arguments.isEmpty())
        arguments.removeFirst();
    Parser parser;
    try {
        parser.parse(arguments, options);
    } catch (ArgumentParsingException ape) {
        QString text(ape.getText());
        if (ape.isError()) {
            if (!text.isEmpty()) {
#ifndef NO_CONSOLE
                TerminalOutput::simpleWordWrap(text, true);
#else
                QMessageBox::critical(0, tr("Error"), text);
#endif
            }
            QCoreApplication::exit(1);
            return -1;
        } else {
            if (!text.isEmpty()) {
#ifndef NO_CONSOLE
                TerminalOutput::simpleWordWrap(text);
#else
                QMessageBox::information(0, tr("Help"), text);
#endif
            }
            QCoreApplication::exit(1);
        }
        return 1;
    }

    Archive::Access archive;
    Archive::Access::ReadLock archiveLock(archive);

    SequenceName::Component station;
    if (!arguments.isEmpty()) {
        auto allStations = archive.availableStations({arguments.front().toStdString()});
        if (!allStations.empty()) {
            station = *allStations.begin();
            arguments.removeFirst();
        }
    }
    if (station.empty()) {
        station = SequenceName::impliedStation(&archive);
    }

    quint16 port = 14231;
    if (options.isSet("port")) {
        port = (quint16) qobject_cast<ComponentOptionSingleInteger *>(options.get("port"))->get();
    }

    if (options.isSet("rsh")) {
        QString command(qobject_cast<ComponentOptionSingleString *>(options.get("rsh"))->get());

        QProcess *process = new QProcess(this);
        Process::forwardError(process);

        connect(process, SIGNAL(stateChanged(QProcess::ProcessState)), this, SLOT(updateDevice()),
                Qt::QueuedConnection);
        process->start(command, QIODevice::ReadWrite | QIODevice::Unbuffered);

        device = process;

        qCDebug(log_syncpeer) << "Started command handler:" << command;
    } else if (options.isSet("listen")) {
        QString host(qobject_cast<ComponentOptionSingleString *>(options.get("listen"))->get());
        server = new SyncTCPServer(this);
        QHostAddress addr(QHostAddress::Any);
        if (!host.isEmpty())
            addr = QHostAddress(host);
        connect(server, SIGNAL(newConnection()), this, SLOT(incomingConnection()),
                Qt::QueuedConnection);
        if (!server->listen(addr, port)) {
#ifndef NO_CONSOLE
            TerminalOutput::output(tr("Failed to listen to port: %1.\n").arg(server->errorString()),
                                   true);
#else
            QMessageBox::critical(0, tr("Error"), tr("Failed to listen to port: %1.").
                arg(server->errorString()));
#endif
            QCoreApplication::exit(1);
            return -1;
        }
        qCDebug(log_syncpeer) << "Listening to" << addr.toString() << "on port" << port;
    } else if (options.isSet("connect")) {
        QString host(qobject_cast<ComponentOptionSingleString *>(options.get("connect"))->get());
        if (host.isEmpty()) {
#ifndef QT_NO_IPV4
            host = "127.0.0.1";
#else
            host = "::1";
#endif
        }
        QTcpSocket *socket = new SyncTCPSocket(this);
        socket->setReadBufferSize(8388608);
        connect(socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this,
                SLOT(updateDevice()), Qt::QueuedConnection);
        socket->connectToHost(host, port, QIODevice::ReadWrite | QIODevice::Unbuffered);
        device = socket;

        qCDebug(log_syncpeer) << "Connecting to" << host << "on port" << port;
    } else {
#ifndef NO_CONSOLE
        device = new StdioForward(this);
        if (!device->open(QIODevice::ReadWrite | QIODevice::Unbuffered)) {
            QCoreApplication::exit(1);
            return -1;
        }
        stdoutOpen = true;
        qCDebug(log_syncpeer) << "Starting STDIO exchange";


#else
        QTcpSocket *socket = new SyncTCPSocket(this);
        socket->setReadBufferSize(8388608);
        connect(socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)),
            this, SLOT(updateDevice()), Qt::QueuedConnection);
        QString host;
#ifndef QT_NO_IPV4
        host = "127.0.0.1";
#else
        host = "::1";
#endif
        socket->connectToHost(host, port,
            QIODevice::ReadWrite | QIODevice::Unbuffered);
        device = socket;
        qCDebug(log_syncpeer) << "Connecting to" << host << "on port" << port;
#endif
    }

    auto authorization = SequenceName::impliedProfile(&archive);
    if (options.isSet("profile")) {
        authorization = qobject_cast<ComponentOptionSingleString *>(options.get("profile"))->get()
                                                                                           .toLower()
                                                                                           .toStdString();
    }

    Archive::Selection::Match selectStations;
    if (!station.empty())
        selectStations.emplace_back(station);

    auto confList = SequenceSegment::Stream::read(
            Archive::Selection(FP::undefined(), FP::undefined(), selectStations, {"configuration"},
                               {"synchronize"}), &archive);

    ValueSegment::Transfer config;
    if (station.empty()) {
        auto allStations = archive.availableStations();
        for (auto seg = confList.rbegin(), endSeg = confList.rend(); seg != endSeg; ++seg) {
            for (const auto &check : allStations) {
                if (check.empty() || check == "_")
                    continue;
                SequenceName configUnit(check, "configuration", "synchronize");
                auto stationConfig = seg->getValue(configUnit);
                if (stationConfig.hash("Authorization").hash(authorization).exists()) {
                    station = check;
                    break;
                }
            }
        }
    }
    if (!station.empty()) {
        SequenceName configUnit(station, "configuration", "synchronize");
        for (auto &seg : confList) {
            config.emplace_back(seg.getStart(), seg.getEnd(), seg.takeValue(configUnit));
        }
    }

    if (station.empty()) {
#ifndef NO_CONSOLE
        TerminalOutput::output(tr("Unable to determine station.\n"), true);
#else
        QMessageBox::critical(0, tr("Error"), tr("Unable to determine station."));
#endif
        QCoreApplication::exit(1);
        return -1;
    }

    QString authPath(QString("Authorization/%1").arg(QString::fromStdString(authorization)));
    config = ValueSegment::withPath(config, authPath);

    double modified = FP::undefined();
    bool disableLock = true;
    if (options.isSet("disable-lock")) {
        disableLock = qobject_cast<ComponentOptionBoolean *>(options.get("disable-lock"))->get();
    }

    QByteArray remoteState;
    if (!disableLock) {
        double lockTimeout = 30.0;
        if (options.isSet("timeout")) {
            lockTimeout =
                    qobject_cast<ComponentOptionSingleDouble *>(options.get("timeout"))->get();
        }
        lock.reset(new Database::RunLock);
        bool ok = false;
        lockKey = QString("synchronize %1 %2").arg(QString::fromStdString(station).toLower(),
                                                   QString::fromStdString(authorization));
        modified = lock->acquire(lockKey, lockTimeout, &ok);
        if (!ok) {
            lock.reset();

            qCDebug(log_syncpeer) << "Lock failed for" << station;
#ifndef NO_CONSOLE
            TerminalOutput::output(tr("Unable to acquire lock for %1.\n").arg(
                    QString::fromStdString(station).toUpper()), true);
#else
            QMessageBox::critical(0, tr("Error"), tr("Unable to acquire lock for %1.").
                arg(QString::fromStdString(station).toUpper()));
#endif
            QCoreApplication::exit(1);
            return -1;
        }

        {
            QByteArray data(lock->get(lockKey));
            if (!data.isEmpty()) {
                QDataStream stream(&data, QIODevice::ReadOnly);
                quint8 version = 0;
                stream >> version;

                if (stream.status() != QDataStream::Ok || version != rcSynchronizeStateVersion) {
                    qCDebug(log_syncpeer) << "Invalid state data";
                } else {
                    stream >> remoteState;
                }
            }
        }
    }

    disableCompression = false;
    if (options.isSet("disable-compression")) {
        disableCompression =
                qobject_cast<ComponentOptionBoolean *>(options.get("disable-compression"))->get();
    }


    peer = new Peer(new BasicFilter(ValueSegment::withPath(config, "Local/Synchronize"), station),
                    new TrackingFilter(ValueSegment::withPath(config, "Remote/Synchronize"),
                                       station, remoteState), Peer::Mode_Bidirectional, modified);
    Q_ASSERT(peer);


    connect(peer, SIGNAL(errorDetected()), this, SLOT(updateDevice()), Qt::QueuedConnection);
    connect(peer, SIGNAL(finished()), this, SLOT(updateDevice()), Qt::QueuedConnection);
    connect(peer, SIGNAL(incomingStallCleared()), this, SLOT(updateDevice()), Qt::QueuedConnection);
    connect(peer, SIGNAL(writeAvailable()), this, SLOT(writeToDevice()), Qt::QueuedConnection);
    connect(peer, SIGNAL(flushRequested()), this, SLOT(flushCompressor()), Qt::QueuedConnection);
    peer->feedback.forward(feedback);

#ifndef NO_CONSOLE
    bool supressProgress = false;
    if (options.isSet("quiet")) {
        supressProgress = qobject_cast<ComponentOptionBoolean *>(options.get("quiet"))->get();
    }
    if (!stdoutOpen && !supressProgress) {
        progress.attach(feedback);
        progress.start();
    }
#endif

    return 0;
}

SyncPeer::SyncPeer() : running(false),
                       inputThrottleTimer(this),
                       lock(),
                       device(NULL),
                       compressor(NULL),
                       peer(NULL),
                       disableCompression(false),
                       server(NULL)
#ifndef NO_CONSOLE
        ,
                       stdoutOpen(false),
                       progress()
#endif
{

    inputThrottleTimer.setSingleShot(true);
    connect(&inputThrottleTimer, SIGNAL(timeout()), this, SLOT(updateDevice()),
            Qt::DirectConnection);
}

SyncPeer::~SyncPeer()
{
    if (lock) {
        lock->fail();
        lock.reset();
    }
    if (compressor != NULL) {
        compressor->close();
        delete compressor;
    }
    if (device != NULL) {
        device->close();
        delete device;
    }
    if (peer != NULL) {
        delete peer;
    }
    if (server != NULL) {
        server->close();
        delete server;
    }

#ifndef NO_CONSOLE
    if (stdoutOpen) {
        ::close(1);
    }
#endif
}

void SyncPeer::start()
{
    running = true;

    if (server != NULL) {
        feedback.emitStage(tr("Waiting for connection"),
                           tr("The peer is waiting for an incoming connection before proceeding."),
                           false);

        QMetaObject::invokeMethod(this, "incomingConnection", Qt::QueuedConnection);
        return;
    } else {
        feedback.emitStage(tr("Connecting"),
                           tr("The peer is establishing the connection before proceeding."), false);
    }

    QMetaObject::invokeMethod(this, "updateDevice", Qt::QueuedConnection);
}

void SyncPeer::incomingConnection()
{
    if (server == NULL)
        return;
    if (device != NULL)
        return;
    for (;;) {
        QTcpSocket *socket = server->nextPendingConnection();
        if (socket == 0)
            break;

        qCDebug(log_syncpeer) << "Starting connection with" << socket->peerAddress().toString();

        connect(socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this,
                SLOT(updateDevice()), Qt::QueuedConnection);

        device = socket;
        QMetaObject::invokeMethod(this, "updateDevice", Qt::QueuedConnection);
        break;
    }
}

void SyncPeer::flushCompressor()
{
    if (compressor == NULL)
        return;
    compressor->flush(0);
}

void SyncPeer::updateDevice()
{
    if (compressor == NULL) {
        if (device == NULL)
            return;

        QTcpSocket *socket = qobject_cast<QTcpSocket *>(device);
        if (socket) {
            switch (socket->state()) {
            case QAbstractSocket::UnconnectedState:
                qCDebug(log_syncpeer) << "Connection closed:" << socket->errorString();
                device->deleteLater();
                device = NULL;
                if (server != NULL) {
                    QMetaObject::invokeMethod(this, "incomingConnection", Qt::QueuedConnection);
                    return;
                }
                abort();
                return;
            case QAbstractSocket::ConnectedState:
                break;
            default:
                return;
            }

            qCDebug(log_syncpeer) << "Connection established to"
                                  << socket->peerAddress().toString();
        }

        QProcess *process = qobject_cast<QProcess *>(device);
        if (process) {
            switch (process->state()) {
            case QProcess::NotRunning:
                qCDebug(log_syncpeer) << "Process stopped:" << process->errorString();
                device->deleteLater();
                device = NULL;
                abort();
                return;
            case QProcess::Running:
                break;
            default:
                return;
            }

            qCDebug(log_syncpeer) << "Invoked command completed started on PID"
                                  << process->processId();
        }

        compressor = new CompressedStream(device, this);
        Q_ASSERT(compressor);

        compressor->setMaximumBufferSize(8388608);
        if (disableCompression) {
            compressor->setPreferredCompression(CompressedStream::None);
        }

        connect(compressor, SIGNAL(readyRead()), this, SLOT(updateDevice()), Qt::QueuedConnection);
        connect(compressor, SIGNAL(readChannelFinished()), this, SLOT(updateDevice()),
                Qt::QueuedConnection);
        connect(compressor, SIGNAL(bytesWritten(qint64)), this, SLOT(writeToDevice()),
                Qt::QueuedConnection);

        qCDebug(log_syncpeer) << "Starting synchronization";

        peer->start();

        QMetaObject::invokeMethod(this, "writeToDevice", Qt::QueuedConnection);
    }

    switch (peer->errorState()) {
    case Peer::Error_None:
        break;
    case Peer::Error_Fatal:
    case Peer::Error_Recoverable:
        abort();
        return;
    }

    qint64 n = compressor->bytesAvailable();
    /* Do not stall the incoming, since as a peer the flow control should take care of that
     * and this would also stall that (and the heartbeats, etc) */
    if (n > 0) {
        {
            QByteArray bufferRead(compressor->read((int) qMin(Q_INT64_C(65536), n)));
            peer->incomingData(bufferRead);
        }
        if (compressor->bytesAvailable() > 0) {
            QMetaObject::invokeMethod(this, "updateDevice", Qt::QueuedConnection);
            return;
        }
    }
    inputThrottleTimer.start(1000);

    if (!peer->transferCompleted())
        return;

    compressor->flush();

    QProcess *process = qobject_cast<QProcess *>(device);
    if (process) {
        process->closeReadChannel(QProcess::StandardOutput);
        process->closeWriteChannel();

        switch (process->state()) {
        case QProcess::NotRunning:
            break;
        default:
            return;
        }
    }

    compressor->close();

    if (!peer->isFinished())
        return;

    if (lock) {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << rcSynchronizeStateVersion;
            stream << peer->getRemoteFilterState();
        }
        lock->set(lockKey, data);

        lock->release();
        lock.reset();
    }

    delete peer;
    peer = NULL;

    compressor->deleteLater();
    compressor = NULL;

    device->close();
    device->deleteLater();
    device = NULL;

#ifndef NO_CONSOLE
    if (stdoutOpen) {
        ::close(1);
        stdoutOpen = false;
    }
#endif

    if (server != NULL) {
        server->close();
        server->deleteLater();
        server = NULL;
    }

    qCDebug(log_syncpeer) << "Synchronization completed";

#ifndef NO_CONSOLE
    progress.finished();
#endif

    running = false;
    emit finished();
}

void SyncPeer::writeToDevice()
{
    if (peer == NULL || compressor == NULL)
        return;
    switch (peer->writePending(compressor)) {
    case Peer::WriteResult_Wait:
    case Peer::WriteResult_Continue:
        break;
    case Peer::WriteResult_Completed:
        compressor->disconnect(this, SLOT(writeToDevice()));
        compressor->flush(0);
        QMetaObject::invokeMethod(this, "updateDevice", Qt::QueuedConnection);
        break;
    case Peer::WriteResult_Error:
        qCDebug(log_syncpeer) << "Write error encountered";
        QMetaObject::invokeMethod(this, "updateDevice", Qt::QueuedConnection);
        return;
    }
}

void SyncPeer::abort()
{
    if (!running)
        return;

    qCDebug(log_syncpeer) << "Aborting synchronization";

#ifndef NO_CONSOLE
    progress.abort();
#endif
    peer->signalTerminate();
    QCoreApplication::exit(1);

    running = false;
    emit finished();
}

int main(int argc, char **argv)
{
#if !defined(NO_CONSOLE) || !defined(BUILD_GUI)
    QCoreApplication app(argc, argv);
#else
    QApplication app(argc, argv, false);
#endif

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

    QString translateLocation;
    QByteArray cpd3(qgetenv("CPD3"));
    if (cpd3.length() > 0) {
        translateLocation = QString(cpd3) + "/locale";
    }
    QTranslator cpd3Translator;
    cpd3Translator.load("cpd3_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cpd3Translator);
    QTranslator cliTranslator;
    cliTranslator.load("cli_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cliTranslator);
    QTranslator sync_peerTranslator;
    sync_peerTranslator.load("sync_peer_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&sync_peerTranslator);

    Abort::installAbortHandler(false);
    AbortPoller poller;
    QObject::connect(&poller, SIGNAL(aborted()), &app, SLOT(quit()));

    qRegisterMetaType<QAbstractSocket::SocketState>("QAbstractSocket::SocketState");
    qRegisterMetaType<QProcess::ProcessState>("QProcess::ProcessState");

    SyncPeer *sync = new SyncPeer;
    QObject::connect(sync, SIGNAL(finished()), &app, SLOT(quit()));
    if (int rc = sync->parseArguments(app.arguments())) {
        delete sync;
        ThreadPool::system()->wait();
        if (rc == -1)
            return 1;
        return 0;
    }
    sync->start();
    QObject::connect(&poller, SIGNAL(aborted()), sync, SLOT(abort()));

    poller.start();
    int rc = app.exec();
    poller.disconnect();
    delete sync;
    ThreadPool::system()->wait();
    return rc;
}
