/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef SYNCREAD_H
#define SYNCREAD_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QIODevice>
#include <QTimer>
#include <QStringList>
#include <QTcpServer>
#include <QTcpSocket>

#include "core/compression.hxx"
#include "database/runlock.hxx"
#include "sync/peer.hxx"
#include "core/actioncomponent.hxx"

#ifndef NO_CONSOLE

#include "clicore/terminaloutput.hxx"
#include "core/stdindevice.hxx"

#endif

class SyncPeer : public QObject {
Q_OBJECT

    bool running;

    QTimer inputThrottleTimer;
    std::unique_ptr<CPD3::Database::RunLock> lock;
    QString lockKey;
    QIODevice *device;
    CPD3::CompressedStream *compressor;
    CPD3::Sync::Peer *peer;

    bool disableCompression;

    QTcpServer *server;

#ifndef NO_CONSOLE
    bool stdoutOpen;
    CPD3::CLI::TerminalProgress progress;
#endif

public:
    SyncPeer();

    ~SyncPeer();

    int parseArguments(const QStringList &rawArguments);

    void start();

    CPD3::ActionFeedback::Source feedback;

public slots:

    void abort();

private slots:

    void incomingConnection();

    void updateDevice();

    void flushCompressor();

    void writeToDevice();

signals:

    void finished();
};

#endif
