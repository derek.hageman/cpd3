/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QObject>
#include <QtDebug>
#include <QProcess>
#include <QString>
#include <QTcpServer>
#include <QTemporaryFile>

#include "datacore/archive/access.hxx"
#include "datacore/stream.hxx"
#include "core/qtcompat.hxx"
#include "core/process.hxx"
#include "core/waitutils.hxx"
#include "database/runlock.hxx"
#include "database/storage.hxx"

//#define FORWARD_OUPUT

using namespace CPD3;
using namespace CPD3::Data;

class TestComponent : public QObject {
Q_OBJECT

    QString listenHost;
    quint16 listenPort;

    static bool archiveContains(const QFile &db, const ArchiveValue &lookingFor)
    {
        Archive::Access access(db);
        ArchiveSink::Iterator data;
        access.readArchive(Archive::Selection(lookingFor.getUnit(), lookingFor.getStart(),
                                              lookingFor.getEnd()), &data)->detach();

        ArchiveValue check;
        while ((check = data.next()).isValid()) {
            if (!ArchiveValue::ExactlyEqual()(check, lookingFor))
                continue;
            data.abort();
            data.wait();
            return true;
        }
        return false;
    }

    static bool archiveContains(const QFile &db, const ArchiveErasure &lookingFor)
    {
        Archive::Access access(db);
        ErasureSink::Iterator data;
        access.readErasure(Archive::Selection(lookingFor.getUnit(), lookingFor.getStart(),
                                              lookingFor.getEnd()), &data)->detach();

        ArchiveErasure check;
        while ((check = data.erasureNext()).isValid()) {
            if (!ArchiveErasure::ExactlyEqual()(check, lookingFor))
                continue;
            data.abort();
            data.wait();
            return true;
        }
        return false;
    }

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

#ifndef QT_NO_IPV4
        listenHost = "127.0.0.1";
#else
        listenHost = "::1";
#endif
        {
            QTcpServer server;
            QVERIFY(server.listen(QHostAddress(listenHost)));
            listenPort = server.serverPort();
            server.close();
        }
        QVERIFY(listenPort != 0);
    }

    void cliHelp()
    {
        QVERIFY(qputenv("CPD3ARCHIVE", QByteArray("sqlite:_")));

        QProcess p;
        p.start("cpd3_sync_peer", QStringList() << "--help");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        QVERIFY(output.contains("Usage:"));
    }

    void socketExchange()
    {
        QTemporaryFile db1;
        QVERIFY(db1.open());

        QTemporaryFile db2;
        QVERIFY(db2.open());

        SequenceName u1("s1", "a1", "u1");
        SequenceName u2("s1", "a1", "u2");
        SequenceName u3("s1", "a1", "u3");
        SequenceName u4("s1", "a1", "u4");

        Archive::Access(db1).writeSynchronous(ArchiveValue::Transfer{
                ArchiveValue(u1, Variant::Root(0.0), 50.0, 60.0, 0, 2000.0, true),
                ArchiveValue(u3, Variant::Root(0.0), 50.0, 60.0, 0, 1000.0, true),});
        Archive::Access(db1).writeSynchronous(ArchiveValue::Transfer {
                                                      ArchiveValue(u1, Variant::Root(1.1), 10.0, 20.0, 0, 1000.0, false),
                                                      ArchiveValue(u1, Variant::Root(2.1), 20.0, 30.0, 0, 1000.0, false),
                                                      ArchiveValue(u2, Variant::Root(3.1), 30.0, 40.0, 0, 2000.0, false),
                                                      ArchiveValue(u3, Variant::Root(0.1), 40.0, 50.0, 1, 1000.0, false),
                                                      ArchiveValue(u1, Variant::Root(4.1), 40.0, 50.0, 0, 2000.0, false),},
                                              ArchiveErasure::Transfer {ArchiveErasure(u1, 50.0, 60.0, 0, 2000.0),
                                          ArchiveErasure(u3, 50.0, 60.0, 0, 1000.0),});

        Archive::Access(db2).writeSynchronous(ArchiveValue::Transfer{
                ArchiveValue(u4, Variant::Root(0.0), 50.0, 60.0, 0, 2000.0, true),
                ArchiveValue(u1, Variant::Root(0.0), 50.0, 60.0, 0, 1000.0, true),});
        Archive::Access(db2).writeSynchronous(ArchiveValue::Transfer {
                                                      ArchiveValue(u3, Variant::Root(1.2), 10.0, 20.0, 0, 1000.0, false),
                                                      ArchiveValue(u3, Variant::Root(2.2), 20.0, 30.0, 0, 1000.0, false),
                                                      ArchiveValue(u4, Variant::Root(3.2), 30.0, 40.0, 0, 2000.0, false),
                                                      ArchiveValue(u3, Variant::Root(4.2), 40.0, 50.0, 0, 2000.0, false),
                                                      ArchiveValue(u1, Variant::Root(0.2), 40.0, 50.0, 0, 2001.0, false),},
                                              ArchiveErasure::Transfer {ArchiveErasure(u4, 50.0, 60.0, 0, 2000.0),
                                          ArchiveErasure(u1, 50.0, 60.0, 0, 1000.0),});


        {
            Variant::Root config1;
            auto auth = config1["Authorization"].hash("aerosol");
            auth["Local/Synchronize/#0/Variable"] = "u1";
            auth["Local/Synchronize/#0/Archive"] = "a1";
            auth["Local/Synchronize/#0/Accept"] = true;
            auth["Local/Synchronize/#1/Variable"] = "u2";
            auth["Local/Synchronize/#1/Archive"] = "a1";
            auth["Local/Synchronize/#1/Accept"] = true;
            auth["Remote/Synchronize/#0/Variable"] = "u3";
            auth["Remote/Synchronize/#0/Archive"] = "a1";
            auth["Remote/Synchronize/#0/Accept"] = true;
            auth["Remote/Synchronize/#1/Variable"] = "u4";
            auth["Remote/Synchronize/#1/Archive"] = "a1";
            auth["Remote/Synchronize/#1/Accept"] = true;
            Archive::Access(db1).writeSynchronous(SequenceValue::Transfer{
                    SequenceValue({"s1", "configuration", "synchronize"}, config1)});

            Database::RunLock lock(Database::Storage::sqlite(db1.fileName().toStdString()));
            lock.acquire("synchronize s1 aerosol");
            lock.seenTime(1001.0);
            lock.release();
        }
        {
            Variant::Root config2;
            auto auth = config2["Authorization"].hash("aerosol");
            auth["Local/Synchronize/#0/Variable"] = "u3";
            auth["Local/Synchronize/#0/Archive"] = "a1";
            auth["Local/Synchronize/#0/Accept"] = true;
            auth["Local/Synchronize/#1/Variable"] = "u4";
            auth["Local/Synchronize/#1/Archive"] = "a1";
            auth["Local/Synchronize/#1/Accept"] = true;
            auth["Remote/Synchronize/#0/Variable"] = "u1";
            auth["Remote/Synchronize/#0/Archive"] = "a1";
            auth["Remote/Synchronize/#0/Accept"] = true;
            auth["Remote/Synchronize/#1/Variable"] = "u2";
            auth["Remote/Synchronize/#1/Archive"] = "a1";
            auth["Remote/Synchronize/#1/Accept"] = true;
            Archive::Access(db2).writeSynchronous(SequenceValue::Transfer{
                    SequenceValue({"s1", "configuration", "synchronize"}, config2)});

            Database::RunLock lock(Database::Storage::sqlite(db2.fileName().toStdString()));
            lock.acquire("synchronize s1 aerosol");
            lock.seenTime(1002.0);
            lock.release();
        }

        QProcess p1;
        QProcess p2;
#ifdef FORWARD_OUPUT
        Process::forwardError(&p1);
        Process::forwardError(&p2);
#else
        p1.setProcessChannelMode(QProcess::SeparateChannels);
        p2.setProcessChannelMode(QProcess::SeparateChannels);
        p1.setReadChannel(QProcess::StandardOutput);
        p2.setReadChannel(QProcess::StandardOutput);
        p1.closeReadChannel(QProcess::StandardError);
        p2.closeReadChannel(QProcess::StandardError);
#endif

        {
            QProcessEnvironment pe(p1.processEnvironment());
            pe.insert("CPD3ARCHIVE", QString("sqlite:%1").arg(db1.fileName()));
            p1.setProcessEnvironment(pe);
        }
        {
            QProcessEnvironment pe(p2.processEnvironment());
            pe.insert("CPD3ARCHIVE", QString("sqlite:%1").arg(db2.fileName()));
            p2.setProcessEnvironment(pe);
        }

        p1.start("cpd3_sync_peer", QStringList() << QString("--listen=%1").arg(listenHost)
                                                 << QString("--port=%1").arg(listenPort)
                                                 << "--disable-lock=off" << "s1");
        QVERIFY(p1.waitForStarted());
        QTest::qSleep(2000);

        p2.start("cpd3_sync_peer", QStringList() << QString("--connect=%1").arg(listenHost)
                                                 << QString("--port=%1").arg(listenPort)
                                                 << "--disable-lock=off" << "s1");

        QVERIFY(p2.waitForStarted());

        QVERIFY(p1.waitForFinished());
        QVERIFY(p2.waitForFinished());

        QVERIFY(p1.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p1.exitCode(), 0);

        QVERIFY(p2.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p2.exitCode(), 0);


        QVERIFY(archiveContains(db1, ArchiveValue(u1, Variant::Root(1.1), 10.0, 20.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db1, ArchiveValue(u1, Variant::Root(2.1), 20.0, 30.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u2, Variant::Root(3.1), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u1, Variant::Root(4.1), 40.0, 50.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u4, Variant::Root(3.2), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u3, Variant::Root(4.2), 40.0, 50.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1, ArchiveErasure(u1, 50.0, 60.0, 0, 2000.0)));
        QVERIFY(archiveContains(db1, ArchiveErasure(u3, 50.0, 60.0, 0, 1000.0)));
        QVERIFY(archiveContains(db1, ArchiveErasure(u4, 50.0, 60.0, 0, 2000.0)));

        QVERIFY(archiveContains(db2, ArchiveValue(u3, Variant::Root(1.2), 10.0, 20.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db2, ArchiveValue(u3, Variant::Root(2.2), 20.0, 30.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u4, Variant::Root(3.2), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u3, Variant::Root(4.2), 40.0, 50.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u1, Variant::Root(0.2), 40.0, 50.0, 0, 2001.0, true)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u2, Variant::Root(3.1), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db2, ArchiveErasure(u4, 50.0, 60.0, 0, 2000.0)));
        QVERIFY(archiveContains(db2, ArchiveErasure(u1, 50.0, 60.0, 0, 2000.0)));
    }

#ifdef Q_OS_UNIX

    void stdioExchange()
    {
        QTemporaryFile db1;
        QVERIFY(db1.open());

        QTemporaryFile db2;
        QVERIFY(db2.open());

        SequenceName u1("s1", "a1", "u1");
        SequenceName u2("s1", "a1", "u2");
        SequenceName u3("s1", "a1", "u3");
        SequenceName u4("s1", "a1", "u4");

        Archive::Access(db1).writeSynchronous(ArchiveValue::Transfer{
                ArchiveValue(u1, Variant::Root(0.0), 50.0, 60.0, 0, 2000.0, true),
                ArchiveValue(u3, Variant::Root(0.0), 50.0, 60.0, 0, 1000.0, true),});
        Archive::Access(db1).writeSynchronous(ArchiveValue::Transfer {
                                                      ArchiveValue(u1, Variant::Root(1.1), 10.0, 20.0, 0, 1000.0, false),
                                                      ArchiveValue(u1, Variant::Root(2.1), 20.0, 30.0, 0, 1000.0, false),
                                                      ArchiveValue(u2, Variant::Root(3.1), 30.0, 40.0, 0, 2000.0, false),
                                                      ArchiveValue(u3, Variant::Root(0.1), 40.0, 50.0, 1, 1000.0, false),
                                                      ArchiveValue(u1, Variant::Root(4.1), 40.0, 50.0, 0, 2000.0, false),},
                                              ArchiveErasure::Transfer {ArchiveErasure(u1, 50.0, 60.0, 0, 2000.0),
                                          ArchiveErasure(u3, 50.0, 60.0, 0, 1000.0),});

        Archive::Access(db2).writeSynchronous(ArchiveValue::Transfer{
                ArchiveValue(u4, Variant::Root(0.0), 50.0, 60.0, 0, 2000.0, true),
                ArchiveValue(u1, Variant::Root(0.0), 50.0, 60.0, 0, 1000.0, true),});
        Archive::Access(db2).writeSynchronous(ArchiveValue::Transfer {
                                                      ArchiveValue(u3, Variant::Root(1.2), 10.0, 20.0, 0, 1000.0, false),
                                                      ArchiveValue(u3, Variant::Root(2.2), 20.0, 30.0, 0, 1000.0, false),
                                                      ArchiveValue(u4, Variant::Root(3.2), 30.0, 40.0, 0, 2000.0, false),
                                                      ArchiveValue(u3, Variant::Root(4.2), 40.0, 50.0, 0, 2000.0, false),
                                                      ArchiveValue(u1, Variant::Root(0.2), 40.0, 50.0, 0, 2001.0, false),},
                                              ArchiveErasure::Transfer {ArchiveErasure(u4, 50.0, 60.0, 0, 2000.0),
                                          ArchiveErasure(u1, 50.0, 60.0, 0, 1000.0),});


        {
            Variant::Root config1;
            auto auth = config1["Authorization"].hash("aerosol");
            auth["Local/Synchronize/#0/Variable"] = "u1";
            auth["Local/Synchronize/#0/Archive"] = "a1";
            auth["Local/Synchronize/#0/Accept"] = true;
            auth["Local/Synchronize/#1/Variable"] = "u2";
            auth["Local/Synchronize/#1/Archive"] = "a1";
            auth["Local/Synchronize/#1/Accept"] = true;
            auth["Remote/Synchronize/#0/Variable"] = "u3";
            auth["Remote/Synchronize/#0/Archive"] = "a1";
            auth["Remote/Synchronize/#0/Accept"] = true;
            auth["Remote/Synchronize/#1/Variable"] = "u4";
            auth["Remote/Synchronize/#1/Archive"] = "a1";
            auth["Remote/Synchronize/#1/Accept"] = true;
            Archive::Access(db1).writeSynchronous(SequenceValue::Transfer{
                    SequenceValue({"s1", "configuration", "synchronize"}, config1)});

            Database::RunLock lock(Database::Storage::sqlite(db1.fileName().toStdString()));
            lock.acquire("synchronize s1 aerosol");
            lock.seenTime(1001.0);
            lock.release();
        }
        {
            Variant::Root config2;
            auto auth = config2["Authorization"].hash("aerosol");
            auth["Local/Synchronize/#0/Variable"] = "u3";
            auth["Local/Synchronize/#0/Archive"] = "a1";
            auth["Local/Synchronize/#0/Accept"] = true;
            auth["Local/Synchronize/#1/Variable"] = "u4";
            auth["Local/Synchronize/#1/Archive"] = "a1";
            auth["Local/Synchronize/#1/Accept"] = true;
            auth["Remote/Synchronize/#0/Variable"] = "u1";
            auth["Remote/Synchronize/#0/Archive"] = "a1";
            auth["Remote/Synchronize/#0/Accept"] = true;
            auth["Remote/Synchronize/#1/Variable"] = "u2";
            auth["Remote/Synchronize/#1/Archive"] = "a1";
            auth["Remote/Synchronize/#1/Accept"] = true;
            Archive::Access(db2).writeSynchronous(SequenceValue::Transfer{
                    SequenceValue({"s1", "configuration", "synchronize"}, config2)});

            Database::RunLock lock(Database::Storage::sqlite(db2.fileName().toStdString()));
            lock.acquire("synchronize s1 aerosol");
            lock.seenTime(1002.0);
            lock.release();
        }

        QProcess p1;
        QProcess p2;
#ifdef FORWARD_OUPUT
        Process::forwardError(&p1);
        Process::forwardError(&p2);
#else
        p1.setProcessChannelMode(QProcess::SeparateChannels);
        p2.setProcessChannelMode(QProcess::SeparateChannels);
        p1.setReadChannel(QProcess::StandardOutput);
        p2.setReadChannel(QProcess::StandardOutput);
        p1.closeReadChannel(QProcess::StandardError);
        p2.closeReadChannel(QProcess::StandardError);
#endif

        {
            QProcessEnvironment pe(p1.processEnvironment());
            pe.insert("CPD3ARCHIVE", QString("sqlite:%1").arg(db1.fileName()));
            p1.setProcessEnvironment(pe);
        }
        {
            QProcessEnvironment pe(p2.processEnvironment());
            pe.insert("CPD3ARCHIVE", QString("sqlite:%1").arg(db2.fileName()));
            p2.setProcessEnvironment(pe);
        }

        p1.start("cpd3_sync_peer", QStringList() << "--disable-lock=off" << "s1",
                 QIODevice::ReadWrite | QIODevice::Unbuffered);
        p2.start("cpd3_sync_peer", QStringList() << "--disable-lock=off" << "s1",
                 QIODevice::ReadWrite | QIODevice::Unbuffered);

        QVERIFY(p1.waitForStarted());
        QVERIFY(p2.waitForStarted());

        QVERIFY(p1.state() == QProcess::Running);
        QVERIFY(p2.state() == QProcess::Running);

        QByteArray write1;
        QByteArray write2;
        ElapsedTimer to;
        to.start();
        for (;;) {
            QEventLoop el;

            connect(&p1, SIGNAL(readyRead()), &el, SLOT(quit()));
            connect(&p1, SIGNAL(readChannelFinished()), &el, SLOT(quit()));
            connect(&p1, SIGNAL(bytesWritten(qint64)), &el, SLOT(quit()));
            connect(&p1, SIGNAL(stateChanged(QProcess::ProcessState)), &el, SLOT(quit()));

            connect(&p2, SIGNAL(readyRead()), &el, SLOT(quit()));
            connect(&p2, SIGNAL(readChannelFinished()), &el, SLOT(quit()));
            connect(&p2, SIGNAL(bytesWritten(qint64)), &el, SLOT(quit()));
            connect(&p2, SIGNAL(stateChanged(QProcess::ProcessState)), &el, SLOT(quit()));

            bool active1 = (p1.state() == QProcess::Running);
            bool active2 = (p2.state() == QProcess::Running);

            if (!active1 && !active2)
                break;


            write2.append(p1.readAll());
            write1.append(p2.readAll());

            if (!write1.isEmpty()) {
                if (active1) {
                    qint64 n = p1.write(write1);
                    if (n < 0)
                        break;
                    write1 = write1.mid((int) n);
                } else {
                    write1.clear();
                }
            } else if (!active2) {
                p1.closeWriteChannel();
            }
            if (!write2.isEmpty()) {
                if (active2) {
                    qint64 n = p2.write(write2);
                    if (n < 0)
                        break;
                    write2 = write2.mid((int) n);
                } else {
                    write2.clear();
                }
            } else if (!active1) {
                p2.closeWriteChannel();
            }

            int remaining = 30000 - to.elapsed();
            if (remaining <= 0)
                break;
            QTimer::singleShot(remaining, &el, SLOT(quit()));

            el.exec();
        }

        QVERIFY(p1.state() == QProcess::NotRunning);
        QVERIFY(p1.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p1.exitCode(), 0);

        QVERIFY(p2.state() == QProcess::NotRunning);
        QVERIFY(p2.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p2.exitCode(), 0);


        QVERIFY(archiveContains(db1, ArchiveValue(u1, Variant::Root(1.1), 10.0, 20.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db1, ArchiveValue(u1, Variant::Root(2.1), 20.0, 30.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u2, Variant::Root(3.1), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u1, Variant::Root(4.1), 40.0, 50.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u4, Variant::Root(3.2), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u3, Variant::Root(4.2), 40.0, 50.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1, ArchiveErasure(u1, 50.0, 60.0, 0, 2000.0)));
        QVERIFY(archiveContains(db1, ArchiveErasure(u3, 50.0, 60.0, 0, 1000.0)));
        QVERIFY(archiveContains(db1, ArchiveErasure(u4, 50.0, 60.0, 0, 2000.0)));

        QVERIFY(archiveContains(db2, ArchiveValue(u3, Variant::Root(1.2), 10.0, 20.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db2, ArchiveValue(u3, Variant::Root(2.2), 20.0, 30.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u4, Variant::Root(3.2), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u3, Variant::Root(4.2), 40.0, 50.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u1, Variant::Root(0.2), 40.0, 50.0, 0, 2001.0, true)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u2, Variant::Root(3.1), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db2, ArchiveErasure(u4, 50.0, 60.0, 0, 2000.0)));
        QVERIFY(archiveContains(db2, ArchiveErasure(u1, 50.0, 60.0, 0, 2000.0)));
    }

    void rshExchange()
    {
        QTemporaryFile db1;
        QVERIFY(db1.open());

        QTemporaryFile db2;
        QVERIFY(db2.open());

        SequenceName u1("s1", "a1", "u1");
        SequenceName u2("s1", "a1", "u2");
        SequenceName u3("s1", "a1", "u3");
        SequenceName u4("s1", "a1", "u4");

        Archive::Access(db1).writeSynchronous(ArchiveValue::Transfer{
                ArchiveValue(u1, Variant::Root(0.0), 50.0, 60.0, 0, 2000.0, true),
                ArchiveValue(u3, Variant::Root(0.0), 50.0, 60.0, 0, 1000.0, true),});
        Archive::Access(db1).writeSynchronous(ArchiveValue::Transfer {
                                                      ArchiveValue(u1, Variant::Root(1.1), 10.0, 20.0, 0, 1000.0, false),
                                                      ArchiveValue(u1, Variant::Root(2.1), 20.0, 30.0, 0, 1000.0, false),
                                                      ArchiveValue(u2, Variant::Root(3.1), 30.0, 40.0, 0, 2000.0, false),
                                                      ArchiveValue(u3, Variant::Root(0.1), 40.0, 50.0, 1, 1000.0, false),
                                                      ArchiveValue(u1, Variant::Root(4.1), 40.0, 50.0, 0, 2000.0, false),},
                                              ArchiveErasure::Transfer {ArchiveErasure(u1, 50.0, 60.0, 0, 2000.0),
                                          ArchiveErasure(u3, 50.0, 60.0, 0, 1000.0),});

        Archive::Access(db2).writeSynchronous(ArchiveValue::Transfer{
                ArchiveValue(u4, Variant::Root(0.0), 50.0, 60.0, 0, 2000.0, true),
                ArchiveValue(u1, Variant::Root(0.0), 50.0, 60.0, 0, 1000.0, true),});
        Archive::Access(db2).writeSynchronous(ArchiveValue::Transfer {
                                                      ArchiveValue(u3, Variant::Root(1.2), 10.0, 20.0, 0, 1000.0, false),
                                                      ArchiveValue(u3, Variant::Root(2.2), 20.0, 30.0, 0, 1000.0, false),
                                                      ArchiveValue(u4, Variant::Root(3.2), 30.0, 40.0, 0, 2000.0, false),
                                                      ArchiveValue(u3, Variant::Root(4.2), 40.0, 50.0, 0, 2000.0, false),
                                                      ArchiveValue(u1, Variant::Root(0.2), 40.0, 50.0, 0, 2001.0, false),},
                                              ArchiveErasure::Transfer {ArchiveErasure(u4, 50.0, 60.0, 0, 2000.0),
                                          ArchiveErasure(u1, 50.0, 60.0, 0, 1000.0),});


        {
            Variant::Root config1;
            auto auth = config1["Authorization"].hash("aerosol");
            auth["Local/Synchronize/#0/Variable"] = "u1";
            auth["Local/Synchronize/#0/Archive"] = "a1";
            auth["Local/Synchronize/#0/Accept"] = true;
            auth["Local/Synchronize/#1/Variable"] = "u2";
            auth["Local/Synchronize/#1/Archive"] = "a1";
            auth["Local/Synchronize/#1/Accept"] = true;
            auth["Remote/Synchronize/#0/Variable"] = "u3";
            auth["Remote/Synchronize/#0/Archive"] = "a1";
            auth["Remote/Synchronize/#0/Accept"] = true;
            auth["Remote/Synchronize/#1/Variable"] = "u4";
            auth["Remote/Synchronize/#1/Archive"] = "a1";
            auth["Remote/Synchronize/#1/Accept"] = true;
            Archive::Access(db1).writeSynchronous(SequenceValue::Transfer{
                    SequenceValue({"s1", "configuration", "synchronize"}, config1)});

            Database::RunLock lock(Database::Storage::sqlite(db1.fileName().toStdString()));
            lock.acquire("synchronize s1 aerosol");
            lock.seenTime(1001.0);
            lock.release();
        }
        {
            Variant::Root config2;
            auto auth = config2["Authorization"].hash("aerosol");
            auth["Local/Synchronize/#0/Variable"] = "u3";
            auth["Local/Synchronize/#0/Archive"] = "a1";
            auth["Local/Synchronize/#0/Accept"] = true;
            auth["Local/Synchronize/#1/Variable"] = "u4";
            auth["Local/Synchronize/#1/Archive"] = "a1";
            auth["Local/Synchronize/#1/Accept"] = true;
            auth["Remote/Synchronize/#0/Variable"] = "u1";
            auth["Remote/Synchronize/#0/Archive"] = "a1";
            auth["Remote/Synchronize/#0/Accept"] = true;
            auth["Remote/Synchronize/#1/Variable"] = "u2";
            auth["Remote/Synchronize/#1/Archive"] = "a1";
            auth["Remote/Synchronize/#1/Accept"] = true;
            Archive::Access(db2).writeSynchronous(SequenceValue::Transfer{
                    SequenceValue({"s1", "configuration", "synchronize"}, config2)});

            Database::RunLock lock(Database::Storage::sqlite(db2.fileName().toStdString()));
            lock.acquire("synchronize s1 aerosol");
            lock.seenTime(1002.0);
            lock.release();
        }

        QProcess p;
        p.setProcessChannelMode(QProcess::SeparateChannels);

        {
            QProcessEnvironment pe(p.processEnvironment());
            pe.insert("CPD3ARCHIVE", QString("sqlite:%1").arg(db1.fileName()));
            p.setProcessEnvironment(pe);
        }

        QString command;
        {
            QProcess which;
            which.start("which", QStringList("cpd3_sync_peer"));
            which.waitForFinished();
            QVERIFY(which.exitStatus() == QProcess::NormalExit);
            QCOMPARE(which.exitCode(), 0);
            command = QString::fromUtf8(which.readAll());
            command = command.trimmed();
            command.append(" --disable-lock=off");
            QVERIFY(!command.isEmpty());
        }

        p.start("cpd3_sync_peer", QStringList() << "--disable-lock=off" << QString(
                "--rsh=/bin/sh -c \"CPD3ARCHIVE=sqlite:%1 %2\"").arg(db2.fileName(), command));

        QVERIFY(p.waitForStarted());
        QVERIFY(p.waitForFinished());

        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);


        QVERIFY(archiveContains(db1, ArchiveValue(u1, Variant::Root(1.1), 10.0, 20.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db1, ArchiveValue(u1, Variant::Root(2.1), 20.0, 30.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u2, Variant::Root(3.1), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u1, Variant::Root(4.1), 40.0, 50.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u4, Variant::Root(3.2), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u3, Variant::Root(4.2), 40.0, 50.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1, ArchiveErasure(u1, 50.0, 60.0, 0, 2000.0)));
        QVERIFY(archiveContains(db1, ArchiveErasure(u3, 50.0, 60.0, 0, 1000.0)));
        QVERIFY(archiveContains(db1, ArchiveErasure(u4, 50.0, 60.0, 0, 2000.0)));

        QVERIFY(archiveContains(db2, ArchiveValue(u3, Variant::Root(1.2), 10.0, 20.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db2, ArchiveValue(u3, Variant::Root(2.2), 20.0, 30.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u4, Variant::Root(3.2), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u3, Variant::Root(4.2), 40.0, 50.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u1, Variant::Root(0.2), 40.0, 50.0, 0, 2001.0, true)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u2, Variant::Root(3.1), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db2, ArchiveErasure(u4, 50.0, 60.0, 0, 2000.0)));
        QVERIFY(archiveContains(db2, ArchiveErasure(u1, 50.0, 60.0, 0, 2000.0)));
    }

#endif
};

QTEST_MAIN(TestComponent)

#include "test.moc"
