/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#ifdef Q_OS_UNIX

#include <unistd.h>

#endif

#include <QTranslator>

#if !defined(NO_CONSOLE) || !defined(BUILD_GUI)

#include <QCoreApplication>

#else
#include <QApplication>
#endif

#ifdef NO_CONSOLE
#include <QMessageBox>
#endif

#include <QStringList>
#include <QLibraryInfo>
#include <QLocale>
#include <QLoggingCategory>

#include "core/abort.hxx"
#include "core/number.hxx"
#include "core/range.hxx"
#include "core/component.hxx"
#include "core/stdindevice.hxx"
#include "core/threadpool.hxx"
#include "clicore/terminaloutput.hxx"
#include "clicore/optionparse.hxx"
#include "clicore/argumentparser.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/segment.hxx"

#include "sync_write.hxx"


Q_LOGGING_CATEGORY(log_syncwrite, "cpd3.syncwrite", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::CLI;
using namespace CPD3::Sync;
using namespace CPD3::Data;

class Parser : public ArgumentParser {
public:
    Parser()
    { }

    virtual ~Parser()
    { }

protected:
    virtual QList<BareWordHelp> getBareWordHelp()
    {
        return QList<BareWordHelp>() << BareWordHelp(tr("station", "station argument name"),
                                                     tr("The station to accept data for"),
                                                     tr("Inferred from the current directory"),
                                                     tr("The \"station\" bare word argument defines the station used to determine "
                                                        "output data as well as the time lock acquired.  This sets both the profile "
                                                        "loaded and the modification time for data output."))
                                     << BareWordHelp(tr("file", "file argument name"),
#if defined(Q_OS_UNIX)
                                                     tr("The file to write data to or - for standard input"),
#else
                                             tr("The file to write data to"),
#endif
                                                     QString(),
                                                     tr("The \"file\" bare word argument is used to specify the the file to write "
                                                        "synchronization data to."
                                                        #if defined(Q_OS_UNIX)
                                                        "If it is present then the file is truncated to zero size and data are "
                                                        "written to it.  Alternatively \"-\" may be used to explicitly specify "
                                                        "standard output."
#endif
                                                     ));
    }

    virtual QString getBareWordCommandLine()
    {
#if defined(Q_OS_UNIX)
        return tr("[station] [file]", "target command line");
#else
        return tr("[station] file", "target command line");
#endif
    }

    virtual QString getProgramName()
    { return tr("cpd3.sync.write", "program name"); }

    virtual QString getDescription()
    {
        return tr("This writes synchronization data and writes the changes it defines to the "
                  "local archive.  It is used in conjunction with da.sync.write to perform "
                  "manual synchronization of data.");
    }

    virtual QHash<QString, QString> getDocumentationTypeID()
    {
        QHash<QString, QString> result;
        result.insert("type", "sync_write");
        return result;
    }
};

static const quint8 rcSynchronizeStateVersion = 1;

int SyncWrite::parseArguments(const QStringList &rawArguments)
{

    ComponentOptions options;

    options.add("profile",
                new ComponentOptionSingleString(tr("profile", "name"), tr("Synchronize profile"),
                                                tr("This is the authorization profile to synchronize.  The profile "
                                                   "controls what data are written.  "
                                                   "Consult the station configuration and authorization for available profiles."),
                                                tr("aerosol")));

    options.add("disable-lock",
                new ComponentOptionBoolean(tr("disable-lock", "disable lock option name"),
                                           tr("Do not lock output"),
                                           tr("If set then a lock is not acquired before synchronization "
                                              "output.  This also means that data from all time is output, not just "
                                              "the data modified since the last synchronize."),
                                           QString()));

    options.add("disable-compression", new ComponentOptionBoolean(
            tr("disable-compression", "disable compression option name"),
            tr("Do not compress output"),
            tr("If set this is set then the output is not compressed before "
               "writing.  This can be used if the resulting file will be compressed "
               "with an external program before transfer that may result in a "
               "smaller overall size."), QString()));

    {
        ComponentOptionSingleDouble *timeout =
                new ComponentOptionSingleDouble(tr("wait", "name"), tr("Maximum wait time"),
                                                tr("This is the maximum time to wait for another process to "
                                                   "complete, in seconds.  If there are two concurrent processes "
                                                   "for the same station and profile, the new one will wait at "
                                                   "most this many seconds before giving up without outputting "
                                                   "any synchronization data."), tr("30 seconds"));
        timeout->setMinimum(0.0);
        timeout->setAllowUndefined(true);
        options.add("timeout", timeout);
    }

    QStringList arguments(rawArguments);
    if (!arguments.isEmpty())
        arguments.removeFirst();
    Parser parser;
    try {
        parser.parse(arguments, options);
    } catch (ArgumentParsingException ape) {
        QString text(ape.getText());
        if (ape.isError()) {
            if (!text.isEmpty()) {
#ifndef NO_CONSOLE
                TerminalOutput::simpleWordWrap(text, true);
#else
                QMessageBox::critical(0, tr("Error"), text);
#endif
            }
            QCoreApplication::exit(1);
            return -1;
        } else {
            if (!text.isEmpty()) {
#ifndef NO_CONSOLE
                TerminalOutput::simpleWordWrap(text);
#else
                QMessageBox::information(0, tr("Help"), text);
#endif
            }
            QCoreApplication::exit(0);
        }
        return 1;
    }

    Archive::Access archive;
    Archive::Access::ReadLock archiveLock(archive);

    SequenceName::Component station;
    if (!arguments.isEmpty()) {
        auto allStations = archive.availableStations({arguments.front().toStdString()});
        if (!allStations.empty()) {
            station = *allStations.begin();
            arguments.removeFirst();
        }
    }
    if (station.empty()) {
        station = SequenceName::impliedStation(&archive);
    }

    if (arguments.isEmpty()
#if defined(Q_OS_UNIX)
            || arguments.at(0) == tr("-", "stdout file")
#endif
            ) {
#if defined(Q_OS_UNIX)
        QFile *file = new QFile;
        Q_ASSERT(file);
        if (!file->open(1, QIODevice::WriteOnly | QIODevice::Unbuffered)) {
#ifndef NO_CONSOLE
            TerminalOutput::output(
                    tr("Failed to open standard output: %1\n").arg(outputDevice->errorString()),
                    true);
#else
            QMessageBox::critical(0, tr("Error"), tr("Failed to open standard input: %1").arg(
                outputDevice->errorString()));
#endif
            QCoreApplication::exit(1);
            return -1;
        }
        stdoutOpen = true;
        outputDevice = file;
#else
#ifndef NO_CONSOLE
        TerminalOutput::output(tr("An input file is required.\n"), true);
#else
        QMessageBox::critical(0, tr("Error"), tr("An input file is required."));
#endif
        QCoreApplication::exit(1);
        return -1;
#endif
    } else if (arguments.size() != 1) {
#ifndef NO_CONSOLE
        TerminalOutput::output(tr("Multiple input files are not supported.\n"), true);
#else
        QMessageBox::critical(0, tr("Error"), tr("Multiple input files are not supported."));
#endif
        QCoreApplication::exit(1);
        return -1;
    } else {
        QFile *file = new QFile(arguments.takeFirst());
        Q_ASSERT(file);
        if (!file->open(QIODevice::WriteOnly | QIODevice::Truncate)) {
#ifndef NO_CONSOLE
            TerminalOutput::output(tr("Failed to open input file: %1\n").arg(file->errorString()),
                                   true);
#else
            QMessageBox::critical(0, tr("Error"), tr("Failed to open input file: %1").arg(
                file->errorString()));
#endif
            QCoreApplication::exit(1);
            delete file;
            return -1;
        }
        outputDevice = file;
    }

    auto authorization = SequenceName::impliedProfile(&archive);
    if (options.isSet("profile")) {
        authorization = qobject_cast<ComponentOptionSingleString *>(options.get("profile"))->get()
                                                                                           .toLower()
                                                                                           .toStdString();
    }

    Archive::Selection::Match selectStations;
    if (!station.empty())
        selectStations.emplace_back(station);

    auto confList = SequenceSegment::Stream::read(
            Archive::Selection(FP::undefined(), FP::undefined(), selectStations, {"configuration"},
                               {"synchronize"}), &archive);

    ValueSegment::Transfer config;
    if (station.empty()) {
        auto allStations = archive.availableStations();
        for (auto seg = confList.rbegin(), endSeg = confList.rend(); seg != endSeg; ++seg) {
            for (const auto &check : allStations) {
                if (check.empty() || check == "_")
                    continue;
                SequenceName configUnit(check, "configuration", "synchronize");
                auto stationConfig = seg->getValue(configUnit);
                if (stationConfig.hash("Authorization").hash(authorization).exists()) {
                    station = check;
                    break;
                }
            }
        }
    }
    if (!station.empty()) {
        SequenceName configUnit(station, "configuration", "synchronize");
        for (auto &seg : confList) {
            config.emplace_back(seg.getStart(), seg.getEnd(), seg.takeValue(configUnit));
        }
    }

    if (station.empty()) {
#ifndef NO_CONSOLE
        TerminalOutput::output(tr("Unable to determine station.\n"), true);
#else
        QMessageBox::critical(0, tr("Error"), tr("Unable to determine station."));
#endif
        QCoreApplication::exit(1);
        return -1;
    }

    QString authPath(QString("Authorization/%1").arg(QString::fromStdString(authorization)));
    config = ValueSegment::withPath(config, authPath);

    double modified = FP::undefined();
    bool disableLock = true;
    if (options.isSet("disable-lock")) {
        disableLock = qobject_cast<ComponentOptionBoolean *>(options.get("disable-lock"))->get();
    }

    QByteArray remoteState;
    if (!disableLock) {
        double lockTimeout = 30.0;
        if (options.isSet("timeout")) {
            lockTimeout =
                    qobject_cast<ComponentOptionSingleDouble *>(options.get("timeout"))->get();
        }
        lock.reset(new Database::RunLock);
        bool ok = false;
        lockKey = QString("synchronize %1 %2").arg(QString::fromStdString(station).toLower(),
                                                   QString::fromStdString(authorization));
        modified = lock->acquire(lockKey, lockTimeout, &ok);
        if (!ok) {
            lock.reset();

            qCDebug(log_syncwrite) << "Lock failed for " << station;
#ifndef NO_CONSOLE
            TerminalOutput::output(tr("Unable to acquire lock for %1.\n").arg(
                    QString::fromStdString(station).toUpper()), true);
#else
            QMessageBox::critical(0, tr("Error"), tr("Unable to acquire lock for %1.").
                arg(QString::fromStdString(station).toUpper()));
#endif
            QCoreApplication::exit(1);
            return -1;
        }

        {
            QByteArray data(lock->get(lockKey));
            if (!data.isEmpty()) {
                QDataStream stream(&data, QIODevice::ReadOnly);
                quint8 version = 0;
                stream >> version;

                if (stream.status() != QDataStream::Ok || version != rcSynchronizeStateVersion) {
                    qCDebug(log_syncwrite) << "Invalid state data";
                } else {
                    stream >> remoteState;
                }
            }
        }
    }

    peer = new Peer(new BasicFilter(ValueSegment::withPath(config, "Local/Synchronize"), station),
                    new TrackingFilter(ValueSegment::withPath(config, "Remote/Synchronize"),
                                       station, remoteState), Peer::Mode_Write, modified);
    Q_ASSERT(peer);

    compressor = new CompressedStream(outputDevice, this);
    Q_ASSERT(compressor);

    if (options.isSet("disable-compression") &&
            qobject_cast<ComponentOptionBoolean *>(options.get("disable-compression"))->get()) {
        compressor->setPreferredCompression(CompressedStream::None);
    } else {
        compressor->setPreferredCompression(CompressedStream::LZ4HC);
    }

    return 0;
}

SyncWrite::SyncWrite() : running(false), outputDevice(NULL), compressor(NULL), lock(), peer(NULL)
#ifdef Q_OS_UNIX
        , stdoutOpen(false)
#endif
{
}

SyncWrite::~SyncWrite()
{
    if (compressor != NULL) {
        compressor->close();
        delete compressor;
    }
    if (outputDevice != NULL) {
        outputDevice->close();
        delete outputDevice;
    }
    if (peer != NULL) {
        delete peer;
    }
    if (lock) {
        lock->fail();
        lock.reset();
    }

#ifdef Q_OS_UNIX
    if (stdoutOpen) {
        ::close(1);
    }
#endif
}

void SyncWrite::start()
{
    Q_ASSERT(peer != NULL);
    Q_ASSERT(outputDevice != NULL);

    connect(compressor, SIGNAL(bytesWritten(qint64)), this, SLOT(writeToDevice()),
            Qt::QueuedConnection);

    connect(peer, SIGNAL(writeAvailable()), this, SLOT(writeToDevice()), Qt::QueuedConnection);
    connect(peer, SIGNAL(errorDetected()), this, SLOT(updateDevice()), Qt::QueuedConnection);
    connect(peer, SIGNAL(finished()), this, SLOT(updateDevice()), Qt::QueuedConnection);

    peer->start();

    QMetaObject::invokeMethod(this, "writeToDevice", Qt::QueuedConnection);
    QMetaObject::invokeMethod(this, "updateDevice", Qt::QueuedConnection);

    running = true;
    qCDebug(log_syncwrite) << "Synchronization write starting";
}

void SyncWrite::writeToDevice()
{
    if (peer == NULL || compressor == NULL)
        return;
    switch (peer->writePending(compressor)) {
    case Peer::WriteResult_Wait:
    case Peer::WriteResult_Continue:
        break;
    case Peer::WriteResult_Completed:
        compressor->disconnect(this, SLOT(writeToDevice()));
        compressor->flush(0);
        QMetaObject::invokeMethod(this, "updateDevice", Qt::QueuedConnection);
        break;
    case Peer::WriteResult_Error:
        qCDebug(log_syncwrite) << "Write error encountered";
        QMetaObject::invokeMethod(this, "updateDevice", Qt::QueuedConnection);
        return;
    }
}

void SyncWrite::updateDevice()
{
    if (compressor == NULL || peer == NULL)
        return;

    switch (peer->errorState()) {
    case Peer::Error_None:
        break;
    case Peer::Error_Fatal:
    case Peer::Error_Recoverable:
        abort();
        return;
    }

    if (!peer->isFinished())
        return;

    if (lock) {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << rcSynchronizeStateVersion;
            stream << peer->getRemoteFilterState();
        }
        lock->set(lockKey, data);

        lock->release();
        lock.reset();
    }

    delete peer;
    peer = NULL;

    compressor->close();
    compressor->deleteLater();
    compressor = NULL;

    outputDevice->close();
    outputDevice->deleteLater();
    outputDevice = NULL;


#ifdef Q_OS_UNIX
    if (stdoutOpen) {
        ::close(1);
    }
#endif

    qCDebug(log_syncwrite) << "Synchronization write completed";

    running = false;
    emit finished();
}

void SyncWrite::abort()
{
    if (!running)
        return;

    qCDebug(log_syncwrite) << "Aborting synchronization write";

    peer->signalTerminate();
    QCoreApplication::exit(1);

    running = false;
    emit finished();
}

int main(int argc, char **argv)
{
#if !defined(NO_CONSOLE) || !defined(BUILD_GUI)
    QCoreApplication app(argc, argv);
#else
    QApplication app(argc, argv, false);
#endif

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

    QString translateLocation;
    QByteArray cpd3(qgetenv("CPD3"));
    if (cpd3.length() > 0) {
        translateLocation = QString(cpd3) + "/locale";
    }
    QTranslator cpd3Translator;
    cpd3Translator.load("cpd3_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cpd3Translator);
    QTranslator cliTranslator;
    cliTranslator.load("cli_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cliTranslator);
    QTranslator sync_writeTranslator;
    sync_writeTranslator.load("sync_write_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&sync_writeTranslator);

    Abort::installAbortHandler(false);
    AbortPoller poller;
    QObject::connect(&poller, SIGNAL(aborted()), &app, SLOT(quit()));

    SyncWrite *sync = new SyncWrite;
    QObject::connect(sync, SIGNAL(finished()), &app, SLOT(quit()));
    if (int rc = sync->parseArguments(app.arguments())) {
        delete sync;
        ThreadPool::system()->wait();
        if (rc == -1)
            return 1;
        return 0;
    }
    sync->start();
    QObject::connect(&poller, SIGNAL(aborted()), sync, SLOT(abort()));

    poller.start();
    int rc = app.exec();
    poller.disconnect();
    delete sync;
    ThreadPool::system()->wait();
    return rc;
}
