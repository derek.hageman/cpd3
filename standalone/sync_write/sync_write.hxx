/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef SYNCWRITE_H
#define SYNCWRITE_H

#include "core/first.hxx"

#include <memory>
#include <QtGlobal>
#include <QObject>
#include <QIODevice>
#include <QTimer>
#include <QStringList>

#include "core/compression.hxx"
#include "sync/peer.hxx"
#include "database/runlock.hxx"

class SyncWrite : public QObject {
Q_OBJECT

    bool running;

    QIODevice *outputDevice;
    CPD3::CompressedStream *compressor;
    std::unique_ptr<CPD3::Database::RunLock> lock;
    QString lockKey;
    CPD3::Sync::Peer *peer;

#ifdef Q_OS_UNIX
    bool stdoutOpen;
#endif

public:
    SyncWrite();

    ~SyncWrite();

    int parseArguments(const QStringList &rawArguments);

    void start();

signals:

    void finished();

public slots:

    void abort();

private slots:

    void updateDevice();

    void writeToDevice();
};

#endif
