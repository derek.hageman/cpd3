/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QObject>
#include <QtDebug>
#include <QProcess>
#include <QString>
#include <QTemporaryFile>

#include "datacore/archive/access.hxx"
#include "datacore/stream.hxx"
#include "core/qtcompat.hxx"
#include "database/runlock.hxx"
#include "database/storage.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class TestComponent : public QObject {
Q_OBJECT

    static bool archiveContains(const QFile &db, const ArchiveValue &lookingFor)
    {
        Archive::Access access(db);
        ArchiveSink::Iterator data;
        access.readArchive(Archive::Selection(lookingFor.getUnit(), lookingFor.getStart(),
                                              lookingFor.getEnd()), &data)->detach();

        ArchiveValue check;
        while ((check = data.next()).isValid()) {
            if (!ArchiveValue::ExactlyEqual()(check, lookingFor))
                continue;
            data.abort();
            data.wait();
            return true;
        }
        return false;
    }

    static bool archiveContains(const QFile &db, const ArchiveErasure &lookingFor)
    {
        Archive::Access access(db);
        ErasureSink::Iterator data;
        access.readErasure(Archive::Selection(lookingFor.getUnit(), lookingFor.getStart(),
                                              lookingFor.getEnd()), &data)->detach();

        ArchiveErasure check;
        while ((check = data.erasureNext()).isValid()) {
            if (!ArchiveErasure::ExactlyEqual()(check, lookingFor))
                continue;
            data.abort();
            data.wait();
            return true;
        }
        return false;
    }

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();
    }

    void cliHelp()
    {
        QVERIFY(qputenv("CPD3ARCHIVE", QByteArray("sqlite:_")));

        QProcess p;
        p.start("cpd3_sync_write", QStringList() << "--help");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        QVERIFY(output.contains("Usage:"));
    }

#ifdef Q_OS_UNIX

    void fileExchange()
    {
        QTemporaryFile db1;
        QVERIFY(db1.open());

        QTemporaryFile db2;
        QVERIFY(db2.open());

        SequenceName u1("s1", "a1", "u1");
        SequenceName u2("s1", "a1", "u2");
        SequenceName u3("s1", "a1", "u3");
        SequenceName u4("s1", "a1", "u4");

        Archive::Access(db1).writeSynchronous(ArchiveValue::Transfer{
                ArchiveValue(u1, Variant::Root(0.0), 50.0, 60.0, 0, 2000.0, true),
                ArchiveValue(u3, Variant::Root(0.0), 50.0, 60.0, 0, 1000.0, true),});
        Archive::Access(db1).writeSynchronous(ArchiveValue::Transfer {
                                                      ArchiveValue(u1, Variant::Root(1.1), 10.0, 20.0, 0, 1000.0, false),
                                                      ArchiveValue(u1, Variant::Root(2.1), 20.0, 30.0, 0, 1000.0, false),
                                                      ArchiveValue(u2, Variant::Root(3.1), 30.0, 40.0, 0, 2000.0, false),
                                                      ArchiveValue(u3, Variant::Root(0.1), 40.0, 50.0, 1, 1000.0, false),
                                                      ArchiveValue(u1, Variant::Root(4.1), 40.0, 50.0, 0, 2000.0, false),},
                                              ArchiveErasure::Transfer {ArchiveErasure(u1, 50.0, 60.0, 0, 2000.0),
                                          ArchiveErasure(u3, 50.0, 60.0, 0, 1000.0),});

        Archive::Access(db2).writeSynchronous(ArchiveValue::Transfer{
                ArchiveValue(u4, Variant::Root(0.0), 50.0, 60.0, 0, 2000.0, true),
                ArchiveValue(u1, Variant::Root(0.0), 50.0, 60.0, 0, 1000.0, true),});
        Archive::Access(db2).writeSynchronous(ArchiveValue::Transfer {
                                                      ArchiveValue(u3, Variant::Root(1.2), 10.0, 20.0, 0, 1000.0, false),
                                                      ArchiveValue(u3, Variant::Root(2.2), 20.0, 30.0, 0, 1000.0, false),
                                                      ArchiveValue(u4, Variant::Root(3.2), 30.0, 40.0, 0, 2000.0, false),
                                                      ArchiveValue(u3, Variant::Root(4.2), 40.0, 50.0, 0, 2000.0, false),
                                                      ArchiveValue(u1, Variant::Root(0.2), 40.0, 50.0, 0, 2001.0, false),},
                                              ArchiveErasure::Transfer {ArchiveErasure(u4, 50.0, 60.0, 0, 2000.0),
                                          ArchiveErasure(u1, 50.0, 60.0, 0, 1000.0),});


        {
            Variant::Root config1;
            auto auth = config1["Authorization"].hash("aerosol");
            auth["Local/Synchronize/#0/Variable"] = "u1";
            auth["Local/Synchronize/#0/Archive"] = "a1";
            auth["Local/Synchronize/#0/Accept"] = true;
            auth["Local/Synchronize/#1/Variable"] = "u2";
            auth["Local/Synchronize/#1/Archive"] = "a1";
            auth["Local/Synchronize/#1/Accept"] = true;
            auth["Remote/Synchronize/#0/Variable"] = "u3";
            auth["Remote/Synchronize/#0/Archive"] = "a1";
            auth["Remote/Synchronize/#0/Accept"] = true;
            auth["Remote/Synchronize/#1/Variable"] = "u4";
            auth["Remote/Synchronize/#1/Archive"] = "a1";
            auth["Remote/Synchronize/#1/Accept"] = true;
            Archive::Access(db1).writeSynchronous(SequenceValue::Transfer{
                    SequenceValue({"s1", "configuration", "synchronize"}, config1)});

            Database::RunLock lock(Database::Storage::sqlite(db1.fileName().toStdString()));
            lock.acquire("synchronize s1 aerosol");
            lock.seenTime(1001.0);
            lock.release();
        }
        {
            Variant::Root config2;
            auto auth = config2["Authorization"].hash("aerosol");
            auth["Local/Synchronize/#0/Variable"] = "u3";
            auth["Local/Synchronize/#0/Archive"] = "a1";
            auth["Local/Synchronize/#0/Accept"] = true;
            auth["Local/Synchronize/#1/Variable"] = "u4";
            auth["Local/Synchronize/#1/Archive"] = "a1";
            auth["Local/Synchronize/#1/Accept"] = true;
            auth["Remote/Synchronize/#0/Variable"] = "u1";
            auth["Remote/Synchronize/#0/Archive"] = "a1";
            auth["Remote/Synchronize/#0/Accept"] = true;
            auth["Remote/Synchronize/#1/Variable"] = "u2";
            auth["Remote/Synchronize/#1/Archive"] = "a1";
            auth["Remote/Synchronize/#1/Accept"] = true;
            Archive::Access(db2).writeSynchronous(SequenceValue::Transfer{
                    SequenceValue({"s1", "configuration", "synchronize"}, config2)});

            Database::RunLock lock(Database::Storage::sqlite(db2.fileName().toStdString()));
            lock.acquire("synchronize s1 aerosol");
            lock.seenTime(1002.0);
            lock.release();
        }


        QTemporaryFile file;
        QVERIFY(file.open());

        {
            QVERIFY(qputenv("CPD3ARCHIVE", (QString("sqlite:%1").arg(db1.fileName())).toLatin1()));
            QCOMPARE(qgetenv("CPD3ARCHIVE"), (QString("sqlite:%1").arg(db1.fileName())).toLatin1());

            QProcess p;
            p.start("cpd3_sync_write",
                    QStringList() << "--disable-lock=off" << "s1" << file.fileName());
            QVERIFY(p.waitForFinished());
            QVERIFY(p.exitStatus() == QProcess::NormalExit);
            QCOMPARE(p.exitCode(), 0);
        }
        QVERIFY(file.size() > 0);

        {
            QVERIFY(qputenv("CPD3ARCHIVE", (QString("sqlite:%1").arg(db2.fileName())).toLatin1()));
            QCOMPARE(qgetenv("CPD3ARCHIVE"), (QString("sqlite:%1").arg(db2.fileName())).toLatin1());

            QProcess p;
            p.start("cpd3_sync_read", QStringList() << "s1" << file.fileName());
            QVERIFY(p.waitForFinished());
            QVERIFY(p.exitStatus() == QProcess::NormalExit);
            QCOMPARE(p.exitCode(), 0);
        }

        QVERIFY(archiveContains(db1, ArchiveValue(u1, Variant::Root(1.1), 10.0, 20.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db1, ArchiveValue(u1, Variant::Root(2.1), 20.0, 30.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u2, Variant::Root(3.1), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u1, Variant::Root(4.1), 40.0, 50.0, 0, 2000.0, true)));

        QVERIFY(archiveContains(db2, ArchiveValue(u3, Variant::Root(1.2), 10.0, 20.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db2, ArchiveValue(u3, Variant::Root(2.2), 20.0, 30.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db2, ArchiveValue(u4, Variant::Root(3.2), 30.0, 40.0, 0, 2000.0,
                                                  false)));
        QVERIFY(archiveContains(db2, ArchiveValue(u3, Variant::Root(4.2), 40.0, 50.0, 0, 2000.0,
                                                  false)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u1, Variant::Root(0.2), 40.0, 50.0, 0, 2001.0, true)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u2, Variant::Root(3.1), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db2, ArchiveErasure(u4, 50.0, 60.0, 0, 2000.0)));
        QVERIFY(archiveContains(db2, ArchiveErasure(u1, 50.0, 60.0, 0, 2000.0)));


        QByteArray data;

        {
            QVERIFY(qputenv("CPD3ARCHIVE", (QString("sqlite:%1").arg(db2.fileName())).toLatin1()));
            QCOMPARE(qgetenv("CPD3ARCHIVE"), (QString("sqlite:%1").arg(db2.fileName())).toLatin1());

            QProcess p;
            p.start("cpd3_sync_write", QStringList() << "--disable-lock=off");
            QVERIFY(p.waitForFinished());
            QVERIFY(p.exitStatus() == QProcess::NormalExit);
            QCOMPARE(p.exitCode(), 0);
            data = p.readAllStandardOutput();
        }
        QVERIFY(data.size() > 0);

        {
            QVERIFY(qputenv("CPD3ARCHIVE", (QString("sqlite:%1").arg(db1.fileName())).toLatin1()));
            QCOMPARE(qgetenv("CPD3ARCHIVE"), (QString("sqlite:%1").arg(db1.fileName())).toLatin1());

            QProcess p;
            p.start("cpd3_sync_read");
            QVERIFY(p.waitForStarted());

            int remaining = data.size();
            const char *ptr = data.constData();
            while (remaining > 0) {
                qint64 n = p.write(ptr, remaining);
                QVERIFY(n >= 0);
                ptr += n;
                remaining -= n;
            }
            p.closeWriteChannel();

            QVERIFY(p.waitForFinished());
            QVERIFY(p.exitStatus() == QProcess::NormalExit);
            QCOMPARE(p.exitCode(), 0);
        }

        QVERIFY(archiveContains(db2,
                                ArchiveValue(u4, Variant::Root(3.2), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db2,
                                ArchiveValue(u3, Variant::Root(4.2), 40.0, 50.0, 0, 2000.0, true)));

        QVERIFY(archiveContains(db1, ArchiveValue(u1, Variant::Root(1.1), 10.0, 20.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db1, ArchiveValue(u1, Variant::Root(2.1), 20.0, 30.0, 0, 1000.0,
                                                  false)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u2, Variant::Root(3.1), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u1, Variant::Root(4.1), 40.0, 50.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u4, Variant::Root(3.2), 30.0, 40.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1,
                                ArchiveValue(u3, Variant::Root(4.2), 40.0, 50.0, 0, 2000.0, true)));
        QVERIFY(archiveContains(db1, ArchiveErasure(u1, 50.0, 60.0, 0, 2000.0)));
        QVERIFY(archiveContains(db1, ArchiveErasure(u3, 50.0, 60.0, 0, 1000.0)));
        QVERIFY(archiveContains(db1, ArchiveErasure(u4, 50.0, 60.0, 0, 2000.0)));
    }

#endif
};

QTEST_MAIN(TestComponent)

#include "test.moc"
