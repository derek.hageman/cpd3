/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QProgressDialog>
#include <QCloseEvent>

#include "editor.hxx"
#include "guicore/guiformat.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::GUI;

class ProgressOverride : public QProgressDialog {
public:
    ProgressOverride(QWidget *parent) : QProgressDialog(parent)
    { }

    ~ProgressOverride()
    { }

protected:
    virtual void closeEvent(QCloseEvent *event)
    {
        event->ignore();
    }

    virtual void showEvent(QShowEvent *event)
    { QProgressDialog::showEvent(event); }
};

void Editor::createProgress()
{
    if (progress == NULL) {
        progress = new ProgressOverride(this);
        progress->setWindowModality(Qt::WindowModal);
        progress->setMinimumDuration(500);
        progress->setMinimum(0);
        progress->setMaximum(0);
        progress->setValue(1);
        progress->setAutoClose(false);
        progress->setAutoReset(false);
        progress->setSizeGripEnabled(false);
    }
    progress->reset();
    disconnect(progress, SIGNAL(canceled()), this, SLOT(shutdown()));
    disconnect(progress, SIGNAL(canceled()), this, SLOT(maybeCancelWrite()));
}

void Editor::hideProgress()
{
    if (progress == NULL)
        return;
    disconnect(progress, SIGNAL(canceled()), this, SLOT(shutdown()));
    disconnect(progress, SIGNAL(canceled()), this, SLOT(maybeCancelWrite()));
    progress->setMinimum(0);
    progress->setMaximum(0);
    progress->setValue(1);
    progress->reset();
    progress->hide();
}

void Editor::showLoadProgress()
{
    incomingProgress.updated.disconnect();
    createProgress();
    progress->findChildren<QPushButton *>().at(0)->setEnabled(true);
    connect(progress, SIGNAL(canceled()), this, SLOT(shutdown()));
    progress->setMinimum(0);
    progress->setMaximum(0);
    progress->setValue(1);
    progress->reset();
    incomingProgress.updated.connect(this, std::bind(&Editor::updateFileWriteProgress, this), true);
    updateLoadProgress();
}

void Editor::showArchiveSaveProgress()
{
    incomingProgress.updated.disconnect();
    createProgress();
    progress->findChildren<QPushButton *>().at(0)->setEnabled(false);
    progress->setMinimum(0);
    progress->setMaximum(0);
    progress->setValue(1);
    progress->setWhatsThis(QString());
    progress->setLabelText(tr("Writing archive..."));
    progress->reset();
}

void Editor::showFileWriteProgress()
{
    incomingProgress.updated.disconnect();
    createProgress();
    progress->findChildren<QPushButton *>().at(0)->setEnabled(true);
    connect(progress, SIGNAL(canceled()), this, SLOT(maybeCancelWrite()));
    progress->setMinimum(0);
    progress->setMaximum(0);
    progress->setValue(1);
    progress->setLabelText(tr("Writing data: Sorting..."));
    progress->setWhatsThis(QString());
    progress->reset();
}

void Editor::updateFileWriteProgress()
{
    ActionFeedback::Serializer::Stage stage = incomingProgress.process().back();

    double fraction = stage.toFraction(outputStartTime, outputEndTime);
    if (stage.state() == ActionFeedback::Serializer::Stage::Spin && FP::defined(fraction)) {
        progress->setMinimum(0);
        progress->setMaximum(0);
        progress->setValue(1);
    } else {
        progress->setMinimum(0);
        progress->setMaximum(1000);
        progress->setValue((int) ::floor(fraction * 1000.0));
    }

    auto title = stage.title();
    if (!title.isEmpty()) {
        double time = stage.time();
        if (FP::defined(time)) {
            if (!stage.stageStatus().isEmpty()) {
                progress->setLabelText(
                        tr("Writing data: %1 -- %2 (%3)", "stage state label time").arg(title,
                                                                                        GUITime::formatTime(
                                                                                                time,
                                                                                                &settings),
                                                                                        stage.stageStatus()));
            } else {
                progress->setLabelText(tr("Writing data: %1 -- %2", "stage label time").arg(title,
                                                                                            GUITime::formatTime(
                                                                                                    time,
                                                                                                    &settings)));
            }
        } else {
            if (!stage.stageStatus().isEmpty()) {
                progress->setLabelText(tr("Writing data: %1 (%2)", "stage state label").arg(title,
                                                                                            stage.stageStatus()));
            } else {
                progress->setLabelText(title);
            }
        }
    }

    progress->setWhatsThis(stage.description());
}

void Editor::updateLoadProgress()
{
    if (FP::defined(expectedInputEnd) &&
            FP::defined(latestInputTime) &&
            FP::defined(firstInputTime) &&
            expectedInputEnd != firstInputTime) {
        progress->setMinimum(0);
        progress->setMaximum(1000);
        progress->setValue((int) floor(
                1000.0 * (latestInputTime - firstInputTime) / (expectedInputEnd - firstInputTime)));
    } else if (expectedInputSize > 0 && lastestInputSize > 0) {
        progress->setMinimum(0);
        progress->setMaximum(1000);
        progress->setValue(
                (int) floor(1000.0 * (double) lastestInputSize / (double) expectedInputSize));
    } else if (progress->minimum() != 0 || progress->maximum() != 0) {
        progress->setMinimum(0);
        progress->setMaximum(0);
        progress->setValue(1);
    }

    if (FP::defined(latestInputTime) && lastestInputSize > 0) {
        if (lastestInputSize > 1024 * 1024 * 1024) {
            progress->setLabelText(
                    tr("Loading data at %1 (%2 GiB read)...").arg(GUITime::formatTime(
                                                                     latestInputTime, &settings))
                                                             .arg((double) lastestInputSize /
                                                                          (double) (1024 *
                                                                                  1024 *
                                                                                  1024), 0, 'f',
                                                                  2));
        } else if (lastestInputSize > 1024 * 1024) {
            progress->setLabelText(
                    tr("Loading data at %1 (%2 MiB read)...").arg(GUITime::formatTime(
                                                                     latestInputTime, &settings))
                                                             .arg((double) lastestInputSize /
                                                                          (double) (1024 * 1024), 0,
                                                                  'f', 2));
        } else if (lastestInputSize > 1024) {
            progress->setLabelText(
                    tr("Loading data at %1 (%2 KiB read)...").arg(GUITime::formatTime(
                                                                     latestInputTime, &settings))
                                                             .arg((double) lastestInputSize /
                                                                          (double) (1024), 0, 'f',
                                                                  2));
        } else {
            progress->setLabelText(
                    tr("Loading data at %1 (%n bytes read)...", "", (int) lastestInputSize).arg(
                            GUITime::formatTime(latestInputTime, &settings)));
        }
    } else if (FP::defined(latestInputTime)) {
        progress->setLabelText(
                tr("Loading data at %1...").arg(GUITime::formatTime(latestInputTime, &settings)));
    } else if (lastestInputSize > 0) {
        if (lastestInputSize > 1024 * 1024 * 1024) {
            progress->setLabelText(tr("Loading data (%1 GiB read)...").arg(
                    (double) lastestInputSize / (double) (1024 * 1024 * 1024), 0, 'f', 2));
        } else if (lastestInputSize > 1024 * 1024) {
            progress->setLabelText(tr("Loading data (%1 MiB read)...").arg(
                    (double) lastestInputSize / (double) (1024 * 1024), 0, 'f', 2));
        } else if (lastestInputSize > 1024) {
            progress->setLabelText(tr("Loading data (%1 KiB read)...").arg(
                    (double) lastestInputSize / (double) (1024), 0, 'f', 2));
        } else {
            progress->setLabelText(
                    tr("Loading data (%n bytes read)...", "", (int) lastestInputSize));
        }
    } else {
        progress->setLabelText(tr("Loading data...", "initial loading text"));
    }
}
