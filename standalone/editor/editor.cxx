/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#ifndef NO_CONSOLE

#endif

#include <QTranslator>
#include <QLibraryInfo>
#include <QApplication>
#include <QMessageBox>
#include <QFile>
#include <QCloseEvent>
#include <QDialogButtonBox>

#include "core/abort.hxx"
#include "core/number.hxx"
#include "core/range.hxx"
#include "core/threadpool.hxx"
#include "guidata/dataeditor.hxx"
#include "io/drivers/stdio.hxx"

#include "editor.hxx"

using namespace CPD3;
using namespace CPD3::GUI::Data;
using namespace CPD3::Data;

Editor::Editor() : QWidget(), settings(CPD3GUI_ORGANIZATION, CPD3GUI_APPLICATION)
{
    filterReloadable = false;
    filterInitialized = false;
    dataModified = false;

    inputDevice = NULL;
    inputHandler = NULL;
    input = NULL;

    lastestInputSize = 0;
    latestInputTime = FP::undefined();
    firstInputTime = FP::undefined();
    expectedInputSize = 0;
    expectedInputEnd = FP::undefined();
    outputStartTime = FP::undefined();
    outputEndTime = FP::undefined();

    progress = NULL;

    QGridLayout *layout = new QGridLayout;
    setLayout(layout);

    e = new DataEditor(this);
    layout->addWidget(e, 0, 0, 1, -1);
    layout->setRowStretch(0, 1);

    dialogButtons =
            new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel, Qt::Horizontal,
                                 this);
    connect(dialogButtons, SIGNAL(accepted()), this, SLOT(okButtonPressed()));
    connect(dialogButtons, SIGNAL(rejected()), this, SLOT(cancelButtonPressed()));
    layout->addWidget(dialogButtons, 1, 0, 1, -1);

    handoffDataCompleted = false;
}

Editor::~Editor()
{
    if (reader) {
        reader->signalTerminate();
        reader->wait();
        reader.reset();
    }
    if (inputHandler != NULL)
        delete inputHandler;
    if (inputDevice != NULL)
        delete inputDevice;
}

void Editor::shutdown()
{
    if (inputHandler != NULL)
        inputHandler->signalTerminate();
    if (reader)
        reader->signalTerminate();
    if (dataOutput)
        dataOutput->signalTerminate();

    if (inputHandler != NULL)
        inputHandler->wait();
    if (reader)
        reader->wait();

    if (inputDevice != NULL)
        inputDevice->close();

    hideProgress();

    QCoreApplication::exit(0);
}

void Editor::checkCleanup()
{
    if (inputHandler != NULL && inputHandler->isFinished()) {
        inputHandler->wait();
        delete inputHandler;
        inputHandler = NULL;
    }
    if (reader && reader->wait(0.0)) {
        reader.reset();
    }
}

void Editor::readInputDevice()
{
    if (inputDevice == NULL || inputHandler == NULL)
        return;
    qint64 n = inputDevice->bytesAvailable();
    if (n > 0) {
        QByteArray data(inputDevice->read((int) qMin(Q_INT64_C(65536), n)));
        lastestInputSize += data.size();
        inputHandler->incomingData(std::move(data));

        updateLoadProgress();

        if (inputDevice->bytesAvailable() > 0) {
            QMetaObject::invokeMethod(this, "readInputDevice", Qt::QueuedConnection);
            return;
        }
    }

    if (!inputDevice->atEnd())
        return;

    inputDevice->disconnect();
    inputDevice->close();
    inputDevice->deleteLater();
    inputDevice = NULL;

    inputHandler->endData();
}

void Editor::incomingData(const SequenceValue::Transfer &values)
{
    bool doUpdate = false;
    {
        std::lock_guard<std::mutex> lock(handoffMutex);
        doUpdate = handoffBuffer.empty();
        Util::append(values, handoffBuffer);
    }
    if (doUpdate) {
        emit dataHandoffUpdated();
    }
}

void Editor::incomingData(SequenceValue::Transfer &&values)
{
    bool doUpdate = false;
    {
        std::lock_guard<std::mutex> lock(handoffMutex);
        doUpdate = handoffBuffer.empty();
        Util::append(std::move(values), handoffBuffer);
    }
    if (doUpdate) {
        emit dataHandoffUpdated();
    }
}

void Editor::incomingData(const SequenceValue &value)
{
    {
        std::lock_guard<std::mutex> lock(handoffMutex);
        handoffBuffer.emplace_back(value);
        if (handoffBuffer.size() != 1) {
            /* Don't need to re-emit the signal if it's pending processing */
            return;
        }
    }
    emit dataHandoffUpdated();
}

void Editor::incomingData(SequenceValue &&value)
{
    {
        std::lock_guard<std::mutex> lock(handoffMutex);
        handoffBuffer.emplace_back(std::move(value));
        if (handoffBuffer.size() != 1) {
            /* Don't need to re-emit the signal if it's pending processing */
            return;
        }
    }
    emit dataHandoffUpdated();
}

void Editor::endData()
{
    {
        std::lock_guard<std::mutex> lock(handoffMutex);
        handoffDataCompleted = true;
    }
    emit dataHandoffUpdated();
}

void Editor::processHandoff()
{
    bool completed;
    {
        SequenceValue::Transfer values;
        {
            std::lock_guard<std::mutex> lock(handoffMutex);
            if (!handoffDataCompleted && handoffBuffer.empty())
                return;
            completed = handoffDataCompleted;
            values = std::move(handoffBuffer);
            handoffBuffer.clear();
            handoffDataCompleted = false;
        }

        if (!values.empty()) {
            if (!FP::defined(firstInputTime)) {
                if (FP::defined(values.front().getStart()))
                    firstInputTime = values.front().getStart();
                else if (FP::defined(values.back().getStart()))
                    firstInputTime = values.back().getStart();
            }
            latestInputTime = values.back().getStart();
            for (auto &v : values) {
                data[v.getIdentity()] = std::move(v.root());
            }
            if (!completed)
                updateLoadProgress();
        }
    }

    if (!completed)
        return;
    disconnect(SIGNAL(dataHandoffUpdated()));

    QCoreApplication::processEvents();

    hideProgress();
    checkCleanup();

    e->disconnect();
    dataModified = false;

    if (!filterInitialized) {
        filterInitialized = true;

        SequenceName::ComponentSet stations;
        SequenceName::ComponentSet archives;
        SequenceName::ComponentSet variables;
        SequenceName::ComponentSet flavors;
        for (const auto &v : data) {
            stations.emplace(v.first.getStation());
            archives.emplace(v.first.getArchive());
            variables.emplace(v.first.getVariable());
            auto add = v.first.getFlavors();
            flavors.insert(add.begin(), add.end());
        }

        if (filterReloadable) {
            /* Clean out the "underlay" values if there's a possibility of
             * reducing it to a single one to set the default filter. */
            if (stations.size() == 2) {
                auto i = stations.find("_");
                if (i != stations.end())
                    stations.erase(i);
            }
            if (archives.size() == 2) {
                auto a = archives.begin();
                auto b = a;
                ++b;
                bool am = SequenceName::isMeta(*a);
                bool bm = SequenceName::isMeta(*b);
                if (am && !bm) {
                    archives.erase(a);
                } else if (!am && bm) {
                    archives.erase(b);
                }
            }

            if (stations.size() == 1)
                e->setStation(*stations.begin());
            if (archives.size() == 1)
                e->setArchive(*archives.begin());
            if (variables.size() == 1)
                e->setVariable(*variables.begin());
        } else {
            e->setAvailableStations(stations);
            e->setAvailableArchives(archives);
            e->setAvailableVariables(variables);
            e->setAvailableFlavors(flavors);
        }
    }

    SequenceIdentity select;
    for (const auto &v : data) {
        if (v.first.getName().isMeta())
            continue;
        if (v.first.getName().isDefaultStation())
            continue;
        if (select.isValid()) {
            select = SequenceIdentity();
            break;
        }
        select = v.first;
    }

    e->setData(data);
    if (select.isValid())
        e->selectValue(select);

    connect(e, SIGNAL(selectionChanged()), this, SLOT(updateFilter()));
    if (!filterReloadable) {
        QMetaObject::invokeMethod(this, "updateFilter", Qt::QueuedConnection);
    }


    switch (output) {
    case Archive:
        if (dataModified || !e->isPristine()) {
            dialogButtons->button(QDialogButtonBox::Ok)->setEnabled(true);
        } else {
            connect(e, SIGNAL(dataModified()), this, SLOT(enableOkButton()));
            dialogButtons->button(QDialogButtonBox::Ok)->setEnabled(false);
        }
        break;
    case File:
    case Stdout:
        break;
    }
}

void Editor::archiveReload()
{
    e->disconnect();
    disconnect(SIGNAL(dataHandoffUpdated()));

    data.clear();
    lastestInputSize = 0;
    latestInputTime = FP::undefined();
    firstInputTime = FP::undefined();
    expectedInputSize = 0;
    expectedInputEnd = FP::undefined();

    if (reader) {
        reader->signalTerminate();
        reader->wait();
        reader.reset();
    }

    handoffBuffer.clear();
    handoffDataCompleted = false;
    connect(this, SIGNAL(dataHandoffUpdated()), this, SLOT(processHandoff()), Qt::QueuedConnection);

    showLoadProgress();
    reader = access.readStream(
            Archive::Selection(e->getStart(), e->getEnd(), {e->getStation()}, {e->getArchive()},
                               {e->getVariable()}, e->getHasFlavors(), e->getLacksFlavors()), this);
    reader->complete.connect(this, "checkCleanup");
    e->setData(data);
}

void Editor::updateFilter()
{
    if (filterReloadable) {
        if (!e->isPristine()) {
            switch (QMessageBox::question(this, tr("Archive Modified"),
                                          tr("Changes have been made to the visible data.  Close the existing and discard changes?"),
                                          QMessageBox::Discard |
                                                  QMessageBox::Save |
                                                  QMessageBox::Cancel)) {
            case QMessageBox::Save:
                e->disconnect();
                disconnect(SIGNAL(writeCompleted()));
                connect(this, SIGNAL(writeCompleted()), this, SLOT(archiveReload()),
                        Qt::QueuedConnection);
                archiveWrite();
                return;
            case QMessageBox::Discard:
                break;
            default:
                return;
            }
        }
        archiveReload();
    } else {
        if (!e->isPristine()) {
            dataModified = true;

            auto modified = e->getNewSet();
            auto removed = e->getRemovedSet();
            for (auto &v : removed) {
                data.erase(v.first);
                deleted.emplace(std::move(v));
            }
            for (auto &v : modified) {
                data[std::move(v.first)] = std::move(v.second);
            }
        }

        QRegExp station(QString::fromStdString(e->getStation()), Qt::CaseInsensitive);
        QRegExp archive(QString::fromStdString(e->getArchive()), Qt::CaseInsensitive);
        QRegExp variable(QString::fromStdString(e->getVariable()), Qt::CaseSensitive);
        QList<QRegExp> hasFlavors;
        for (const auto &str : Util::to_qstringlist(e->getHasFlavors())) {
            hasFlavors.append(QRegExp(str, Qt::CaseInsensitive));
        }
        QList<QRegExp> lacksFlavors;
        for (const auto &str : Util::to_qstringlist(e->getLacksFlavors())) {
            lacksFlavors.append(QRegExp(str, Qt::CaseInsensitive));
        }
        double start = e->getStart();
        double end = e->getEnd();

        DataEditor::DataSet passed;
        passed.reserve(data.size());
        for (const auto &v : data) {
            if (!Range::intersects(v.first.getStart(), v.first.getEnd(), start, end))
                continue;
            const auto &u = v.first.getName();
            if (!e->getStation().empty() && !u.isDefaultStation()) {
                if (!station.exactMatch(u.getStationQString()))
                    continue;
            }
            if (!e->getArchive().empty()) {
                if (!archive.exactMatch(u.getArchiveQString())) {
                    if (!u.isMeta() || !archive.exactMatch(u.fromMeta().getArchiveQString()))
                        continue;
                }
            }
            if (!e->getVariable().empty()) {
                if (!variable.exactMatch(u.getVariableQString()))
                    continue;
            }
            if (!hasFlavors.isEmpty()) {
                bool missed = false;
                for (QList<QRegExp>::const_iterator check = hasFlavors.constBegin(),
                        endHF = hasFlavors.constEnd(); check != endHF; ++check) {
                    bool hit = false;
                    for (const auto &flavor : u.getFlavors()) {
                        if (check->exactMatch(QString::fromStdString(flavor))) {
                            hit = true;
                            break;
                        }
                    }
                    if (!hit) {
                        missed = true;
                        break;
                    }
                }
                if (missed)
                    continue;
            }
            if (!lacksFlavors.isEmpty()) {
                bool missed = true;
                for (QList<QRegExp>::const_iterator check = hasFlavors.constBegin(),
                        endHF = hasFlavors.constEnd(); check != endHF; ++check) {
                    bool hit = false;
                    for (const auto &flavor : u.getFlavors()) {
                        if (check->exactMatch(QString::fromStdString(flavor))) {
                            hit = true;
                            break;
                        }
                    }
                    if (hit) {
                        missed = false;
                        break;
                    }
                }
                if (!missed)
                    continue;
            }

            passed.insert(v);
        }

        e->setData(passed);
    }
}

void Editor::archiveWrite()
{
    auto update = e->getNew();
    auto removeValues = e->getRemoved();
    if (update.empty() && removeValues.empty()) {
        emit writeCompleted();
        return;
    }
    Q_ASSERT(!update.empty() || !removeValues.empty());
    showArchiveSaveProgress();

    QProgressDialog progress(tr("Writing data."), tr("Cancel"), 0, 0, this, 0);
    progress.setWindowModality(Qt::ApplicationModal);
    progress.setValue(0);

    Threading::Signal<> complete;
    complete.connect(&progress, "reset");

    std::mutex mutex;
    bool aborted = false;
    auto thread = std::thread([&] {
        for (;;) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                if (aborted)
                    break;
            }

            Archive::Access::WriteLock lock(access);
            access.writeSynchronous(update, removeValues);
            if (lock.commit())
                break;
        }
        complete();
    });

    /* Safe because the complete signal cannot be delivered before we re-enter the
     * event loop, so even if the thread has already exited, the reset() is still
     * pending */
    progress.exec();

    if (progress.wasCanceled()) {
        std::lock_guard<std::mutex> lock(mutex);
        aborted = true;
    }
    thread.join();

    checkCleanup();
    hideProgress();
    emit writeCompleted();
}


void Editor::maybeCancelWrite()
{
    if (output == Stdout) {
        shutdown();
        return;
    }
    switch (QMessageBox::question(this, tr("Abort Write?"),
                                  tr("Abort the write operation (this will mean not all data is written to the file)?"),
                                  QMessageBox::Yes | QMessageBox::Cancel)) {
    case QMessageBox::Yes:
        shutdown();
        return;
    default:
        return;
    }
}

void Editor::fileWrite(std::unique_ptr<IO::Generic::Stream> &&stream)
{
    outputStartTime = FP::undefined();
    outputEndTime = FP::undefined();
    showFileWriteProgress();

    dataOutput.reset(new StandardDataOutput(std::move(stream)));
    dataOutput->finished.connect(this, [this] {
        checkCleanup();
        hideProgress();
        dataOutput.reset();
        emit writeCompleted();
    }, true);
    incomingProgress.attach(dataOutput->feedback);
    dataOutput->start();
    disconnect(SIGNAL(writeCompleted()));
    connect(this, SIGNAL(writeCompleted()), this, SLOT(shutdown()), Qt::QueuedConnection);

    SequenceValue::Transfer sorted;
    for (const auto &add : data) {
        sorted.emplace_back(add.first, add.second);
    }
    std::sort(sorted.begin(), sorted.end(), SequenceIdentity::OrderTime());
    if (FP::defined(sorted.back().getStart())) {
        outputEndTime = sorted.back().getStart();
        for (const auto &v : sorted) {
            if (FP::defined(v.getStart())) {
                outputStartTime = v.getStart();
                break;
            }
        }
    }
    dataOutput->incomingData(std::move(sorted));
    dataOutput->endData();
}

void Editor::enableOkButton()
{
    dialogButtons->button(QDialogButtonBox::Ok)->setEnabled(dataModified || !e->isPristine());
}

void Editor::okButtonPressed()
{
    if (!e->isPristine() && output != Archive) {
        auto modified = e->getNewSet();
        auto removed = e->getRemovedSet();
        for (auto &v : removed) {
            data.erase(v.first);
            deleted.emplace(std::move(v));
        }
        for (auto &v : modified) {
            data[std::move(v.first)] = std::move(v.second);
        }
    }

    e->disconnect();
    disconnect(SIGNAL(writeCompleted()));
    connect(this, SIGNAL(writeCompleted()), this, SLOT(shutdown()), Qt::QueuedConnection);

    switch (output) {
    case Archive:
        archiveWrite();
        return;
    case File: {
        auto backing = IO::Access::file(outputFileName,
                                        IO::File::Mode::writeOnly().textMode().bufferedMode());
        if (!backing) {
            QMessageBox::critical(this, tr("Error opening output file"),
                                  tr("Unable to open \"%1\" for writing.").arg(outputFileName));
            shutdown();
            return;
        }
        auto stream = backing->stream();
        if (!stream) {
            QMessageBox::critical(this, tr("Error opening output file"),
                                  tr("Unable to open \"%1\" for writing.").arg(outputFileName));
            shutdown();
            return;
        }
        if (data.empty()) {
            shutdown();
            return;
        }
        fileWrite(std::move(stream));
        break;
    }
    case Stdout: {
        if (data.empty()) {
            shutdown();
            return;
        }
        auto stream = IO::Access::stdio_out()->stream();
        if (!stream) {
            QMessageBox::critical(this, tr("Error opening output file"),
                                  tr("Unable to open standard output for writing."));
            shutdown();
            return;
        }
        fileWrite(std::move(stream));
        break;
    }
    }
}

void Editor::cancelButtonPressed()
{
    switch (output) {
    case Archive:
        if (dataModified || !e->isPristine()) {
            switch (QMessageBox::question(this, tr("Archive Modified"),
                                          tr("Changes have been made to the archive, continue?"),
                                          QMessageBox::Discard |
                                                  QMessageBox::Save |
                                                  QMessageBox::Cancel)) {
            case QMessageBox::Save:
                okButtonPressed();
                return;
            case QMessageBox::Discard:
                break;
            default:
                return;
            }
        }
        break;
    case File:
        if (dataModified || !e->isPristine()) {
            switch (QMessageBox::question(this, tr("Data Modified"),
                                          tr("Changes have been made to the data, continue?"),
                                          QMessageBox::Discard |
                                                  QMessageBox::Save |
                                                  QMessageBox::Cancel)) {
            case QMessageBox::Save:
                okButtonPressed();
                return;
            case QMessageBox::Discard:
                break;
            default:
                return;
            }
        }
        break;
    case Stdout:
        break;
    }
    shutdown();
}

void Editor::closeEvent(QCloseEvent *event)
{
    if (dataOutput) {
        if (output == Stdout) {
            event->accept();
            return;
        }
        switch (QMessageBox::question(this, tr("Abort Write?"),
                                      tr("Abort the write operation (this will mean not all data is written to the file)?"),
                                      QMessageBox::Yes | QMessageBox::Cancel)) {
        case QMessageBox::Yes:
            event->accept();
            return;
        default:
            event->ignore();
            return;
        }
        QWidget::closeEvent(event);
        return;
    }
    switch (output) {
    case Archive:
        if (dataModified || !e->isPristine()) {
            switch (QMessageBox::question(this, tr("Archive Modified"),
                                          tr("Changes have been made to the archive, continue?"),
                                          QMessageBox::Discard |
                                                  QMessageBox::Save |
                                                  QMessageBox::Cancel)) {
            case QMessageBox::Save:
                okButtonPressed();
                event->ignore();
                return;
            case QMessageBox::Discard:
                break;
            default:
                event->ignore();
                return;
            }
        }
        break;
    case File:
        if (dataModified || !e->isPristine()) {
            switch (QMessageBox::question(this, tr("Data Modified"),
                                          tr("Changes have been made to the data, continue?"),
                                          QMessageBox::Discard |
                                                  QMessageBox::Save |
                                                  QMessageBox::Cancel)) {
            case QMessageBox::Save:
                okButtonPressed();
                event->ignore();
                return;
            case QMessageBox::Discard:
                break;
            default:
                event->ignore();
                return;
            }
        }
        break;
    case Stdout:
        break;
    }

    QWidget::closeEvent(event);
}

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    Q_INIT_RESOURCE(editor);

    app.setWindowIcon(QIcon(":/icon.svg"));

    qRegisterMetaType<SequenceValue>("CPD3::Data::SequenceValue");
    qRegisterMetaType<SequenceValue::Transfer>("CPD3::Data::SequenceValue::Transfer");

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

    QString translateLocation;
    QByteArray cpd3(qgetenv("CPD3"));
    if (cpd3.length() > 0) {
        translateLocation = QString(cpd3) + "/locale";
    }
    QTranslator cpd3Translator;
    cpd3Translator.load("cpd3_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cpd3Translator);
    QTranslator cliTranslator;
    cliTranslator.load("cli_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cliTranslator);
    QTranslator guiTranslator;
    guiTranslator.load("gui_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&guiTranslator);
    QTranslator editorTranslator;
    editorTranslator.load("editor_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&editorTranslator);

    Abort::installAbortHandler(true);

    QStringList arguments;
    for (int i = 1; i < argc; i++) {
        arguments.append(argv[i]);
    }

    Editor *editor = new Editor;
    if (int rc = editor->parseArguments(arguments)) {
        delete editor;
        ThreadPool::system()->wait();
        if (rc == -1)
            return 1;
        return 0;
    }

    AbortPoller abortPoller;
    QObject::connect(&app, SIGNAL(aboutToQuit()), editor, SLOT(shutdown()));
    QObject::connect(&abortPoller, SIGNAL(aborted()), &app, SLOT(quit()));

    editor->show();
    abortPoller.start();
    int rc = app.exec();
    abortPoller.disconnect();
    delete editor;
    ThreadPool::system()->wait();
    return rc;
}
