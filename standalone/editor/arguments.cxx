/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLibraryInfo>
#include <QApplication>
#include <QStringList>
#include <QThread>

#ifdef NO_CONSOLE
#include <QMessageBox>
#endif

#include "editor.hxx"
#include "core/stdindevice.hxx"
#include "clicore/terminaloutput.hxx"
#include "clicore/archiveparse.hxx"
#include "clicore/optionparse.hxx"

using namespace CPD3;
using namespace CPD3::CLI;
using namespace CPD3::Data;
using namespace CPD3::GUI::Data;

int Editor::parseArguments(QStringList arguments)
{
    QString switchPrefix(tr("--", "switch prefix"));

    bool forceStdout = false;
    for (QStringList::iterator arg = arguments.begin(); arg != arguments.end();) {
        if (*arg == switchPrefix) {
            arguments.erase(arg);
            break;
        } else if (!arg->startsWith(switchPrefix)) {
            ++arg;
            continue;
        }

        QString rawName(*arg);
        QString name(rawName.mid(switchPrefix.length()));
        arg = arguments.erase(arg);

        if (name == tr("help", "help switch")) {
            QString help
                    (tr("Usage: da.editor [switches] [[station] variables times [archive]]|[file]\n\n"
                                "Data Editor - This tool provides an interactive GUI to edit data.\n"));
#ifndef NO_CONSOLE
            help.append(
                    tr("If used without any data specification (variables, start, etc) then input will "
                               "be read from standard input and the output after the dialog has been accepted "
                               "will be written to standard output.  "));
#endif
            help.append(
                    tr("If an existing, readable file is given input will be read from that file and "
                               "the final output written back to it.  Otherwise the data given on the "
                               "command line is read and accepting the dialog writes back to the main archive.  "
                               "Switches can be given at any point in the command line, however a simple "
                               "\"--\" terminates switch processing and all further arguments are treated "
                               "as bare words.\n\n"));

#ifndef NO_CONSOLE
            TerminalOutput::output(help);

            int argumentWidth = tr("variables", "variables argument name").length();
            if (argumentWidth < tr("station", "station argument name").length())
                argumentWidth = tr("station", "station argument name").length();
            if (argumentWidth < tr("times", "times argument name").length())
                argumentWidth = tr("times", "times argument name").length();
            if (argumentWidth < tr("archive", "archive argument name").length())
                argumentWidth = tr("archive", "archive argument name").length();
            if (argumentWidth < tr("file", "file argument name").length())
                argumentWidth = tr("file", "file argument name").length();
            if (argumentWidth < tr("disable-filter", "disable filter argument name").length())
                argumentWidth = tr("disable-filter", "disable filter argument name").length();
            if (argumentWidth < tr("disable-advanced", "disable advance argument name").length())
                argumentWidth = tr("disable-advanced", "disable advance argument name").length();
            if (argumentWidth < tr("path", "path argument name").length())
                argumentWidth = tr("path", "path argument name").length();
            if (argumentWidth < tr("read-only", "read only argument name").length())
                argumentWidth = tr("read-only", "read only argument name").length();
            if (argumentWidth < tr("reload-on-filter", "reload on filter argument name").length())
                argumentWidth = tr("reload-on-filter", "reload on filter argument name").length();
            if (argumentWidth < tr("show-default", "show default argument name").length())
                argumentWidth = tr("show-default", "show default argument name").length();
            if (argumentWidth < tr("select-default", "show default argument name").length())
                argumentWidth = tr("select-default", "select default argument name").length();
            if (argumentWidth < tr("stdout", "stdout argument name").length())
                argumentWidth = tr("stdout", "stdout argument name").length();

            TerminalOutput::output(tr("\nArguments:\n"));
            OptionParse::optionSummaryOutput(argumentWidth, tr("station", "station argument name"),
                                             tr("The (default) station to read data from"),
                                             tr("Inferred from the current directory"));
            OptionParse::optionSummaryOutput(argumentWidth,
                                             tr("variables", "variables argument name"),
                                             tr("The list of variable specifies to read from the archive"));
            OptionParse::optionSummaryOutput(argumentWidth, tr("times", "times argument name"),
                                             tr("The time range of data to read"));
            OptionParse::optionSummaryOutput(argumentWidth, tr("archive", "archive argument name"),
                                             tr("The (default) archive to read data from"),
                                             tr("The \"raw\" data archive"));
            OptionParse::optionSummaryOutput(argumentWidth, tr("file", "file argument name"),
                                             tr("The file to read data from or - for standard input"));
            OptionParse::optionSummaryOutput(argumentWidth, switchPrefix +
                                                     tr("disable-filter", "disable filter argument name"),
                                             tr("Disable filtering of data from the dialog"));
            OptionParse::optionSummaryOutput(argumentWidth, switchPrefix +
                                                     tr("reload-on-filter", "reload on filter argument name"),
                                             tr("Reload the archive on filter selection"));
            OptionParse::optionSummaryOutput(argumentWidth,
                                             switchPrefix + tr("stdout", "stdout argument name"),
                                             tr("Force output to be written to standard output"));


#else
            QMessageBox::information(this, tr("Help"), help);
#endif

            QCoreApplication::exit(0);
            return 1;
        }

        if (name == tr("disable-filter", "disable filter argument name")) {
            e->setSelectionFlags(e->getSelectionFlags() & ~DataEditor::SelectFull);
            continue;
        } else if (name == tr("reload-on-filter", "reload on filter argument name")) {
            filterReloadable = true;
            continue;
        }
#ifndef NO_CONSOLE
        else if (name == tr("stdout", "stdout argument name")) {
            forceStdout = true;
            continue;
        }
#endif

        QString value;
        int idxEqual = name.indexOf(tr("=", "argument value separator"));
        if (idxEqual != -1) {
            value = name.mid(idxEqual + 1);
            name = name.mid(0, idxEqual);
        }

#ifndef NO_CONSOLE
        TerminalOutput::output(
                tr("%1 is not a valid switch.\nUse --help to list all arguments.").arg(rawName),
                true);
#else
        QMessageBox::critical(this, tr("Invalid argument"),
                tr("%1 is not a valid switch").arg(rawName));
#endif
        QCoreApplication::exit(1);
        return -1;
    }

    handoffBuffer.clear();
    handoffDataCompleted = false;
    connect(this, SIGNAL(dataHandoffUpdated()), this, SLOT(processHandoff()), Qt::QueuedConnection);

#ifndef NO_CONSOLE
    if (arguments.isEmpty() ||
            (arguments.size() == 1 && arguments.at(0) == tr("-", "stdin argument name"))) {
        output = Stdout;

        if (filterReloadable) {
            TerminalOutput::output(
                    tr("Warning: --reload-on-filter has no effect unless reading from the archive."),
                    true);
            filterReloadable = false;
        }

        inputHandler = new StandardDataInput;
        inputHandler->finished.connect(this, std::bind(&Editor::checkCleanup, this), true);
        inputHandler->start();

        inputDevice = new StdinDevice;
        connect(inputDevice, SIGNAL(readyRead()), this, SLOT(readInputDevice()),
                Qt::QueuedConnection);
        connect(inputDevice, SIGNAL(readChannelFinished()), this, SLOT(readInputDevice()),
                Qt::QueuedConnection);
        inputDevice->open(QIODevice::ReadOnly);
        inputHandler->setEgress(this);

        showLoadProgress();
        readInputDevice();
    } else
#endif
    if (arguments.size() == 1 && QFile::exists(arguments.at(0))) {
        if (!forceStdout) {
            output = File;
            outputFileName = arguments.at(0);
        } else {
            output = Stdout;
        }

        QFile *readQFile = new QFile(arguments.at(0));
        if (!readQFile->open(QIODevice::ReadOnly)) {
#ifndef NO_CONSOLE
            TerminalOutput::output(
                    tr("Can't open file %1: %2\n").arg(arguments.at(0)).arg(readQFile->error()),
                    true);
#else
            QMessageBox::critical(this, tr("Error opening file"),
                tr("Can't open file %1: %2\n").
                arg(arguments.at(0)).
                arg(readQFile->error()));
#endif
            QCoreApplication::exit(1);
            return -1;
        }
        expectedInputSize = readQFile->size();

        if (filterReloadable) {
#ifndef NO_CONSOLE
            TerminalOutput::output(
                    tr("Warning: --reload-on-filter has no effect unless reading from the archive."),
                    true);
#else
            QMessageBox::warning(this, tr("Arguments warning"),
                tr("Warning: --reload-on-filter has no effect unless reading from the archive."));
#endif
            filterReloadable = false;
        }


        inputHandler = new StandardDataInput;
        inputHandler->finished.connect(this, std::bind(&Editor::checkCleanup, this), true);
        inputHandler->start();

        inputDevice = readQFile;
        connect(inputDevice, SIGNAL(readyRead()), this, SLOT(readInputDevice()),
                Qt::QueuedConnection);
        connect(inputDevice, SIGNAL(readChannelFinished()), this, SLOT(readInputDevice()),
                Qt::QueuedConnection);
        inputHandler->setEgress(this);

        showLoadProgress();
        readInputDevice();
    } else {
        if (!forceStdout) {
            output = Archive;
        } else {
            output = Stdout;
        }

        Time::Bounds clip;
        try {
            selections = ArchiveParse::parse(arguments, &clip, &access, true, {}, "configuration");
        } catch (AcrhiveParsingException ape) {
#ifndef NO_CONSOLE
            TerminalOutput::simpleWordWrap(ape.getDescription(), true);
#else
            QMessageBox::critical(this, tr("Error in command line time"),
                ape.getDescription());
#endif
            QCoreApplication::exit(1);
            return -1;
        }

        e->setBounds(clip.start, clip.end);
        expectedInputEnd = clip.end;

        showLoadProgress();
        reader = access.readStream(selections, this);
        reader->complete.connect(this, "checkCleanup");

        if (filterReloadable) {
            e->setAvailableStations(access.availableStations());
            e->setAvailableArchives(access.availableArchives());
            e->setAvailableVariables(access.availableVariables());
            e->setAvailableFlavors(access.availableFlavors());
        }
    }
    return 0;
}
