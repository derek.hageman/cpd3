/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef EDITOR_H
#define EDITOR_H

#include "core/first.hxx"

#include <mutex>
#include <QtGlobal>
#include <QObject>
#include <QWidget>
#include <QStringList>
#include <QSet>
#include <QList>
#include <QProgressDialog>
#include <QFile>
#include <QDialogButtonBox>

#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"
#include "guidata/dataeditor.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/externalsink.hxx"
#include "io/drivers/file.hxx"

class Editor : public QWidget, public CPD3::Data::StreamSink {
Q_OBJECT

    QSettings settings;

    CPD3::GUI::Data::DataEditor *e;

    qint64 expectedInputSize;
    double expectedInputEnd;
    double firstInputTime;
    qint64 lastestInputSize;
    double latestInputTime;

    bool filterReloadable;
    bool filterInitialized;
    QIODevice *inputDevice;
    CPD3::Data::Archive::Access access;
    CPD3::Data::Archive::Access::StreamHandle reader;
    CPD3::Data::ExternalConverter *inputHandler;
    CPD3::Data::StreamSource *input;
    CPD3::Data::Archive::Selection::List selections;

    bool dataModified;
    CPD3::GUI::Data::DataEditor::DataSet data;
    CPD3::GUI::Data::DataEditor::DataSet deleted;

    enum {
        Archive, File, Stdout,
    } output;
    QString outputFileName;
    std::unique_ptr<CPD3::Data::ExternalSink> dataOutput;
    double outputStartTime;
    double outputEndTime;

    std::mutex handoffMutex;
    CPD3::Data::SequenceValue::Transfer handoffBuffer;
    bool handoffDataCompleted;

    QDialogButtonBox *dialogButtons;
    QProgressDialog *progress;
    CPD3::ActionFeedback::Serializer incomingProgress;

    void createProgress();

    void hideProgress();

    void showLoadProgress();

    void showArchiveSaveProgress();

    void showFileWriteProgress();

    void archiveWrite();

    void fileWrite(std::unique_ptr<CPD3::IO::Generic::Stream> &&stream);

    void updateFileWriteProgress();

public:
    Editor();

    virtual ~Editor();

    int parseArguments(QStringList arguments);

    void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

    void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

    void incomingData(const CPD3::Data::SequenceValue &value) override;

    void incomingData(CPD3::Data::SequenceValue &&value) override;

    void endData() override;

protected:
    virtual void closeEvent(QCloseEvent *event);

signals:

    void dataHandoffUpdated();

    void writeCompleted();

private slots:

    void shutdown();

    void readInputDevice();

    void checkCleanup();

    void updateLoadProgress();

    void processHandoff();

    void updateFilter();

    void archiveReload();

    void enableOkButton();

    void okButtonPressed();

    void cancelButtonPressed();

    void maybeCancelWrite();
};

#endif
