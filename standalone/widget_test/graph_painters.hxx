/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef WIDGETTESTGRAPHPAINTERS_H
#define WIDGETTESTGRAPHPAINTERS_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QWidget>

#include "datacore/variant/root.hxx"

class GraphPaintersWidget : public QWidget {
Q_OBJECT

    int type;
    int subtype;
public:
    GraphPaintersWidget(QWidget *parent = 0);

    virtual QSize sizeHint() const;

protected:
    virtual void paintEvent(QPaintEvent *event);

    virtual void mousePressEvent(QMouseEvent *event);
};

#endif
