/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "graphing/graphpainters2d.hxx"
#include "graphing/axis2d.hxx"
#include "graph_painters.hxx"

using namespace CPD3;
using namespace CPD3::Graphing;
using namespace CPD3::Algorithms;
using namespace CPD3::Data;

GraphPaintersWidget::GraphPaintersWidget(QWidget *parent) : QWidget(parent)
{
    type = 0;
    subtype = 0;
    setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));
}

QSize GraphPaintersWidget::sizeHint() const
{ return QSize(100, 100); }

static QVector<TracePoint<3> > make2DPoints()
{
    QVector<TracePoint<3> > result;
    for (double angle = 0.0; angle < 2.5; angle += 0.01) {
        TracePoint<3> add;
        add.start = FP::undefined();
        add.end = FP::undefined();
        add.d[0] = sin(angle) * (0.5 + sin(angle * 30.0) * 0.1);
        add.d[1] = cos(angle) * (0.5 + sin(angle * 30.0) * 0.1);
        add.d[2] = angle / 2.5;
        result.push_back(add);
    }

    TracePoint<3> add;

    add.start = 9.0;
    add.end = 10.0;
    add.d[0] = FP::undefined();
    add.d[1] = FP::undefined();
    add.d[2] = FP::undefined();
    result.push_back(add);

    add.start = 10.0;
    add.end = 11.0;
    add.d[0] = 1.0;
    add.d[1] = 1.0;
    add.d[2] = 1.0;
    result.push_back(add);

    add.start = 11.0;
    add.end = 12.0;
    add.d[0] = 1.25;
    add.d[1] = 1.25;
    add.d[2] = 0.75;
    result.push_back(add);

    add.start = 12.0;
    add.end = 13.0;
    add.d[0] = 1.5;
    add.d[1] = 1.5;
    add.d[2] = 0.5;
    result.push_back(add);

    add.start = 13.0;
    add.end = 14.0;
    add.d[0] = FP::undefined();
    add.d[1] = FP::undefined();
    add.d[2] = FP::undefined();
    result.push_back(add);

    add.start = 14.0;
    add.end = 15.0;
    add.d[0] = 2.0;
    add.d[1] = 1.0;
    add.d[2] = 1.0;
    result.push_back(add);

    add.start = 15.0;
    add.end = 16.0;
    add.d[0] = FP::undefined();
    add.d[1] = FP::undefined();
    add.d[2] = FP::undefined();
    result.push_back(add);

    add.start = 16.0;
    add.end = 17.0;
    add.d[0] = 3.0;
    add.d[1] = 1.0;
    add.d[2] = 1.0;
    result.push_back(add);

    add.start = 20.0;
    add.end = 21.0;
    add.d[0] = 4.0;
    add.d[1] = 1.0;
    add.d[2] = 1.0;
    result.push_back(add);

    add.start = 21.0;
    add.end = 22.0;
    add.d[0] = 4.25;
    add.d[1] = 1.25;
    add.d[2] = 0.45;
    result.push_back(add);

    return result;
}

static QVector<IndicatorPoint<1> > makeIndicatorPoints()
{
    QVector<IndicatorPoint<1> > result;
    IndicatorPoint<1> add;

    add.start = 9.0;
    add.end = 10.0;
    add.d[0] = FP::undefined();
    result.push_back(add);

    add.start = 10.0;
    add.end = 11.0;
    add.d[0] = 1.0;
    result.push_back(add);

    add.start = 11.0;
    add.end = 12.0;
    add.d[0] = 1.25;
    result.push_back(add);

    add.start = 12.0;
    add.end = 13.0;
    add.d[0] = 1.5;
    result.push_back(add);

    add.start = 13.0;
    add.end = 14.0;
    add.d[0] = FP::undefined();
    result.push_back(add);

    add.start = 14.0;
    add.end = 15.0;
    add.d[0] = 2.0;
    result.push_back(add);

    add.start = 15.0;
    add.end = 16.0;
    add.d[0] = FP::undefined();
    result.push_back(add);

    add.start = 16.0;
    add.end = 17.0;
    add.d[0] = 3.0;
    result.push_back(add);

    add.start = 20.0;
    add.end = 21.0;
    add.d[0] = 4.0;
    result.push_back(add);

    add.start = 21.0;
    add.end = 22.0;
    add.d[0] = 4.25;
    result.push_back(add);

    return result;
}

static QVector<Bin2DPaintPoint> makeBinPoints()
{
    QVector<Bin2DPaintPoint> result;

    BinPoint<1, 1> add;
    add.center[0] = 1.0;
    add.width[0] = 0.8;
    add.lowest[0] = 0.1;
    add.lower[0] = 0.25;
    add.middle[0] = 0.5;
    add.upper[0] = 0.75;
    add.uppermost[0] = 0.9;
    result.append(add);

    add.center[0] = 2.0;
    add.width[0] = 0.5;
    add.lowest[0] = 0.3;
    add.lower[0] = 0.4;
    add.middle[0] = 0.5;
    add.upper[0] = 0.75;
    add.uppermost[0] = 0.9;
    result.append(add);

    add.center[0] = 3.0;
    add.width[0] = -20.0;
    add.lowest[0] = 0.1;
    add.lower[0] = 0.55;
    add.middle[0] = 0.6;
    add.upper[0] = 0.7;
    add.uppermost[0] = 0.8;
    result.append(add);

    return result;
}

template<typename PointType>
static void setRealBounds(const QVector<PointType> &points, int idx, AxisTransformer &t)
{
    double min = FP::undefined();
    double max = 0;
    for (typename QVector<PointType>::const_iterator it = points.constBegin();
            it != points.constEnd();
            ++it) {
        double v = it->d[idx];
        if (!FP::defined(min)) {
            min = v;
            max = v;
            continue;
        }
        if (!FP::defined(v))
            continue;
        if (min > v) min = v;
        if (max < v) max = v;
    }
    t.setReal(min, max);
}

class SimpleTestModel : public Model {
public:
    double evaluate(double value) const
    {
        return sin(value) * 0.9 + sin(value * 20.0) * 0.1;
    }

    virtual QVector<double> applyConst(const QVector<double> &inputs) const
    {
        QVector<double> result;
        for (int i = 0, max = inputs.size(); i < max; i++) {
            result.push_back(evaluate(inputs.at(i) + (double) i));
        }
        return result;
    }

    virtual Model *clone() const
    { return new SimpleTestModel; }

protected:
    virtual void serialize(QDataStream &stream) const
    {
        Q_UNUSED(stream);
    }
};

class SimpleTestScanModel : public Model {
public:
    virtual QVector<double> applyConst(const QVector<double> &inputs) const
    {
        double result = 0.0;
        for (int i = 0, max = inputs.size(); i < max; i++) {
            result += sin(inputs.at(i) + (double) i);
        }
        return QVector<double>() << (result / (double) inputs.size());
    }

    virtual double twoToOneConst(double x, double y)
    {
        return (sin(x) + sin(y + 1)) * 0.5;
    }

    virtual Model *clone() const
    { return new SimpleTestScanModel; }

protected:
    virtual void serialize(QDataStream &stream) const
    {
        Q_UNUSED(stream);
    }
};

static bool drawSort(const Graph2DDrawPrimitive *a, const Graph2DDrawPrimitive *b)
{
    return a->drawLessThan(b);
}

void GraphPaintersWidget::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);
    QPainter painter(this);

    painter.setPen(QPen(QColor(0, 0, 0), 0));
    painter.setBrush(QBrush(QColor(255, 255, 255)));
    painter.drawRect(1, 1, width() - 2, height() - 2);

    switch (type) {
    case 0:
    case 1:
    case 2: {
        QVector<TracePoint<3> > points(make2DPoints());
        AxisTransformer x;
        setRealBounds(points, 0, x);
        x.setScreen(1, width() - 2);
        AxisTransformer y;
        setRealBounds(points, 1, y);
        y.setScreen(height() - 2, 1);
        if (type == 0) {
            TraceSymbol symbol;
            symbol.setType(TraceSymbol::CircleCrossX);
            GraphPainter2DLines p(points, x, y, 0.0, Qt::SolidLine, QColor(255, 0, 0), symbol, 1.0,
                                  !!(subtype % 2));
            p.paint(&painter);
        } else if (type == 1) {
            Axis2DSide side = Axis2DBottom;
            switch (subtype % 4) {
            case 0:
                side = Axis2DBottom;
                break;
            case 1:
                side = Axis2DTop;
                break;
            case 2:
                side = Axis2DLeft;
                break;
            case 3:
                side = Axis2DRight;
                break;
            default:
                break;
            }
            GraphPainter2DFillShade
                    p(points, x, y, side, QColor(255, 0, 0), 1.0, (subtype % 8) > 3);
            p.paint(&painter);
        } else {
            TraceSymbol symbol;
            symbol.setType(TraceSymbol::CircleFilled);
            GraphPainter2DPoints p(points, x, y, symbol, QColor(255, 0, 0));
            p.paint(&painter);
        }
        break;
    }

    case 3:
    case 4:
    case 5: {
        QVector<TracePoint<3> > points(make2DPoints());
        AxisTransformer x;
        setRealBounds(points, 0, x);
        x.setScreen(1, width() - 2);
        AxisTransformer y;
        setRealBounds(points, 1, y);
        y.setScreen(height() - 2, 1);
        AxisTransformer z;
        z.setMinExtendAbsolute(0);
        z.setMaxExtendAbsolute(0);
        setRealBounds(points, 2, z);
        z.setScreen(0, 1);
        if (type == 3) {
            TraceSymbol symbol;
            symbol.setType(TraceSymbol::CircleCrossX);
            GraphPainter2DLinesGradient p
                    (points, x, y, z, 0.0, Qt::SolidLine, TraceGradient(), symbol, 1.0,
                     !!(subtype % 2));
            p.paint(&painter);
        } else if (type == 4) {
            Axis2DSide side = Axis2DBottom;
            switch (subtype % 4) {
            case 0:
                side = Axis2DBottom;
                break;
            case 1:
                side = Axis2DTop;
                break;
            case 2:
                side = Axis2DLeft;
                break;
            case 3:
                side = Axis2DRight;
                break;
            default:
                break;
            }
            GraphPainter2DFillShadeGradient
                    p(points, x, y, z, side, TraceGradient(), 1.0, (subtype % 8) > 3);
            p.paint(&painter);
        } else {
            TraceSymbol symbol;
            symbol.setType(TraceSymbol::CircleFilled);
            GraphPainter2DPointsGradient p(points, x, y, z, symbol, TraceGradient());
            p.paint(&painter);
        }
        break;
    }

    case 6:
    case 7:
    case 8:
    case 9: {
        std::shared_ptr<Model> model(new SimpleTestModel);
        AxisTransformer x;
        x.setReal(0, 6.2832);
        x.setScreen(1, width() - 2);
        AxisTransformer y;
        y.setReal(-1, 1);
        y.setScreen(height() - 2, 1);
        if (type == 6) {
            GraphPainter2DFitX p(model, x, y, 0.0, Qt::SolidLine, QColor(255, 0, 0), 1.0);
            p.paint(&painter);
        } else if (type == 7) {
            Axis2DSide side = Axis2DBottom;
            switch (subtype % 4) {
            case 0:
                side = Axis2DBottom;
                break;
            case 1:
                side = Axis2DTop;
                break;
            case 2:
                side = Axis2DLeft;
                break;
            case 3:
                side = Axis2DRight;
                break;
            default:
                break;
            }
            GraphPainter2DShadeFitX p(model, x, y, side, QColor(255, 0, 0), 1.0);
            p.paint(&painter);
        } else if (type == 8) {
            AxisTransformer z;
            z.setMinExtendAbsolute(0);
            z.setMaxExtendAbsolute(0);
            z.setReal(-1, 1);
            z.setScreen(0, 1);
            GraphPainter2DFitXGradient p(model, x, y, z, 0.0, Qt::SolidLine, TraceGradient(), 1.0);
            p.paint(&painter);
        } else {
            AxisTransformer z;
            z.setMinExtendAbsolute(0);
            z.setMaxExtendAbsolute(0);
            z.setReal(-1, 1);
            z.setScreen(0, 1);
            Axis2DSide side = Axis2DBottom;
            switch (subtype % 4) {
            case 0:
                side = Axis2DBottom;
                break;
            case 1:
                side = Axis2DTop;
                break;
            case 2:
                side = Axis2DLeft;
                break;
            case 3:
                side = Axis2DRight;
                break;
            default:
                break;
            }
            GraphPainter2DShadeFitXGradient p(model, x, y, z, side, TraceGradient(), 1.0);
            p.paint(&painter);
        }
        break;
    }

    case 10:
    case 11:
    case 12:
    case 13: {
        std::shared_ptr<Model> model(new SimpleTestModel);
        AxisTransformer x;
        x.setReal(-1, 1);
        x.setScreen(1, width() - 2);
        AxisTransformer y;
        y.setReal(0, 6.2832);
        y.setScreen(height() - 2, 1);
        if (type == 10) {
            GraphPainter2DFitY p(model, x, y, 0.0, Qt::SolidLine, QColor(255, 0, 0), 1.0);
            p.paint(&painter);
        } else if (type == 11) {
            Axis2DSide side = Axis2DBottom;
            switch (subtype % 4) {
            case 0:
                side = Axis2DBottom;
                break;
            case 1:
                side = Axis2DTop;
                break;
            case 2:
                side = Axis2DLeft;
                break;
            case 3:
                side = Axis2DRight;
                break;
            default:
                break;
            }
            GraphPainter2DShadeFitY p(model, x, y, side, QColor(255, 0, 0), 1.0);
            p.paint(&painter);
        } else if (type == 12) {
            AxisTransformer z;
            z.setMinExtendAbsolute(0);
            z.setMaxExtendAbsolute(0);
            z.setReal(-1, 1);
            z.setScreen(0, 1);
            GraphPainter2DFitYGradient p(model, x, y, z, 0.0, Qt::SolidLine, TraceGradient(), 1.0);
            p.paint(&painter);
        } else {
            AxisTransformer z;
            z.setMinExtendAbsolute(0);
            z.setMaxExtendAbsolute(0);
            z.setReal(-1, 1);
            z.setScreen(0, 1);
            Axis2DSide side = Axis2DBottom;
            switch (subtype % 4) {
            case 0:
                side = Axis2DBottom;
                break;
            case 1:
                side = Axis2DTop;
                break;
            case 2:
                side = Axis2DLeft;
                break;
            case 3:
                side = Axis2DRight;
                break;
            default:
                break;
            }
            GraphPainter2DShadeFitYGradient p(model, x, y, z, side, TraceGradient(), 1.0);
            p.paint(&painter);
        }
        break;
    }

    case 14:
    case 15:
    case 16:
    case 17: {
        std::shared_ptr<Model> model(new SimpleTestModel);
        AxisTransformer x;
        x.setReal(-1, 1);
        x.setScreen(1, width() - 2);
        AxisTransformer y;
        y.setReal(-1, 1);
        y.setScreen(height() - 2, 1);
        if (type == 14) {
            GraphPainter2DFitI p(model, x, y, 0.0, Qt::SolidLine, QColor(255, 0, 0), 0, 6.1);
            p.paint(&painter);
        } else if (type == 15) {
            Axis2DSide side = Axis2DBottom;
            switch (subtype % 4) {
            case 0:
                side = Axis2DBottom;
                break;
            case 1:
                side = Axis2DTop;
                break;
            case 2:
                side = Axis2DLeft;
                break;
            case 3:
                side = Axis2DRight;
                break;
            default:
                break;
            }
            GraphPainter2DShadeFitI p(model, x, y, side, QColor(255, 0, 0), 0, 6.1);
            p.paint(&painter);
        } else if (type == 16) {
            AxisTransformer z;
            z.setMinExtendAbsolute(0);
            z.setMaxExtendAbsolute(0);
            z.setReal(-1, 1);
            z.setScreen(0, 1);
            GraphPainter2DFitIGradient
                    p(model, x, y, z, 0.0, Qt::SolidLine, TraceGradient(), 0, 6.1);
            p.paint(&painter);
        } else {
            AxisTransformer z;
            z.setMinExtendAbsolute(0);
            z.setMaxExtendAbsolute(0);
            z.setReal(-1, 1);
            z.setScreen(0, 1);
            Axis2DSide side = Axis2DBottom;
            switch (subtype % 4) {
            case 0:
                side = Axis2DBottom;
                break;
            case 1:
                side = Axis2DTop;
                break;
            case 2:
                side = Axis2DLeft;
                break;
            case 3:
                side = Axis2DRight;
                break;
            default:
                break;
            }
            GraphPainter2DShadeFitIGradient p(model, x, y, z, side, TraceGradient(), 0, 6.1);
            p.paint(&painter);
        }
        break;
    }

    case 18:
    case 19: {
        QVector<Bin2DPaintPoint> points(makeBinPoints());
        AxisTransformer x;
        AxisTransformer y;
        if (type == 18) {
            x.setReal(0, 4.0);
            x.setScreen(1, width() - 2);
            y.setReal(0, 1);
            y.setScreen(height() - 2, 1);
            GraphPainter2DBin p
                    (points, x, y, QColor(255, 127, 127), 0, Qt::DashLine, QColor(0, 0, 255), 0, 0,
                     Qt::SolidLine, QColor(0, 0, 0), 5, Qt::DashDotLine, QColor(0, 127, 0), true);
            p.paint(&painter);
        } else {
            x.setReal(0, 1);
            x.setScreen(1, width() - 2);
            y.setReal(0, 4.0);
            y.setScreen(height() - 2, 1);
            GraphPainter2DBin p
                    (points, x, y, QColor(255, 127, 127), 0, Qt::DashLine, QColor(0, 0, 255), 0.8,
                     0, Qt::SolidLine, QColor(0, 0, 0), 0, Qt::DashDotLine, QColor(0, 127, 0),
                     false);
            p.paint(&painter);
        }
        break;
    }

    case 20:
    case 21: {
        std::shared_ptr<Model> model(new SimpleTestScanModel);
        AxisTransformer x;
        x.setReal(0, 12.2);
        x.setScreen(1, width() - 2);
        AxisTransformer y;
        y.setReal(0, 12.2);
        y.setScreen(height() - 2, 1);
        if (type == 20) {
            GraphPainter2DFillModelScan p(model, x, y, TraceGradient());
            p.paint(&painter);
        } else {
            AxisTransformer z;
            z.setReal(-1, 1);
            z.setScreen(0, 1);
            GraphPainter2DFillModelScan p(model, x, y, z, TraceGradient());
            p.paint(&painter);
        }
        break;
    }

    case 22: {
        QVector<IndicatorPoint<1> > points(makeIndicatorPoints());
        AxisTransformer x;
        x.setScreen(1, width() - 2);
        AxisTransformer y;
        setRealBounds(points, 0, y);
        y.setScreen(height() - 2, 1);

        GraphPainter2DIndicator p1(points, x, 1, width() - 2,
                                   AxisIndicator2D(Axis2DLeft, AxisIndicator2D::AreaFilled),
                                   QColor(200, 200, 200), 1.0);
        GraphPainter2DIndicator p2(points, x, width() - 1, width() - 2,
                                   AxisIndicator2D(Axis2DRight, AxisIndicator2D::RectangleFilled),
                                   QColor(255, 0, 0), 1.0);
        setRealBounds(points, 0, x);
        x.setScreen(1, width() - 2);
        GraphPainter2DIndicator p3(points, y, 1, height() - 2,
                                   AxisIndicator2D(Axis2DTop, AxisIndicator2D::SpanningLine),
                                   QColor(0, 0, 255), 1.0);
        GraphPainter2DIndicator p4(points, y, height() - 1, height() - 2,
                                   AxisIndicator2D(Axis2DBottom, AxisIndicator2D::TriangleAway),
                                   QColor(0, 127, 0), 1.0);
        p1.paint(&painter);
        p2.paint(&painter);
        p3.paint(&painter);
        p4.paint(&painter);
        break;
    }

    case 23:
    case 24:
    case 25:
    case 26:
    case 27: {
        QRectF outer(1, 1, width() - 2, height() - 2);
        AxisDimensionSet2D *set;
        Variant::Root config;
        if (subtype % 2) {
            set = new AxisDimensionSet2DSide(type == 23 || type == 24 || type == 27);
            if (type == 23 || type == 25)
                config["Defaults/Side"].setString("primary");
            else if (type == 24 || type == 26)
                config["Defaults/Side"].setString("secondary");
            config["Defaults/GridEnable"].setBool(true);
        } else {
            set = new AxisDimensionSet2DColor;
            switch (type) {
            case 23:
                config["Defaults/Side"].setString("left");
                break;
            case 24:
                config["Defaults/Side"].setString("right");
                break;
            case 25:
                config["Defaults/Side"].setString("bottom");
                break;
            case 26:
                config["Defaults/Side"].setString("top");
                break;
            default:
                break;
            }
        }

        set->initialize(
                ValueSegment::Transfer{ValueSegment(FP::undefined(), FP::undefined(), config)});
        set->beginUpdate();
        set->get(QSet<QString>() << "hPa", QSet<QString>(), "Axis1")->registerDataLimits(0.5, 0.6);
        set->get(QSet<QString>() << "Mm-1", QSet<QString>(), "Axis2")
           ->registerDataLimits(-2.0, 1.1);
        set->finishUpdate();
        set->prepareDraw(painter.device());

        QRectF inner(outer);
        if (subtype % 2) {
            AxisDimensionSet2DSide *side = static_cast<AxisDimensionSet2DSide *>(set);
            if (type == 23 || type == 24 || type == 27) {
                inner.setLeft(inner.left() + side->getPredictedDepth(true));
                inner.setRight(inner.right() - side->getPredictedDepth(false));
            } else {
                inner.setBottom(inner.bottom() - side->getPredictedDepth(true));
                inner.setTop(inner.top() + side->getPredictedDepth(false));
            }
        } else {
            AxisDimensionSet2DColor *color = static_cast<AxisDimensionSet2DColor *>(set);
            inner.setLeft(inner.left() + color->getPredictedDepth(Axis2DLeft));
            inner.setRight(inner.right() - color->getPredictedDepth(Axis2DRight));
            inner.setTop(inner.top() + color->getPredictedDepth(Axis2DTop));
            inner.setBottom(inner.bottom() - color->getPredictedDepth(Axis2DBottom));
        }
        set->setArea(inner, inner);

        QVector<Graph2DDrawPrimitive *> draw(set->createDraw());
        std::stable_sort(draw.begin(), draw.end(), drawSort);
        for (QVector<Graph2DDrawPrimitive *>::iterator d = draw.begin(); d != draw.end(); ++d) {
            (*d)->paint(&painter);
        }

        qDeleteAll(draw);
        delete set;
        break;
    }

    case 28: {
        AxisTransformer x;
        x.setReal(1.0, 9.0);
        x.setScreen(1, width() - 2);
        AxisTransformer y;
        y.setReal(1.0, 9.0);
        y.setScreen(height() - 2, 1);
        AxisTransformer z;
        z.setReal(-1, 1);
        z.setScreen(0, 1);

        QVector<TracePoint<3> > points;
        {
            TracePoint<3> p;
            p.start = FP::undefined();
            p.end = FP::undefined();

            p.d[0] = 1.0;
            p.d[1] = 1.0;
            p.d[2] = 0.11;
            points.append(p);
            p.d[0] = 2.0;
            p.d[1] = 1.0;
            p.d[2] = -0.21;
            points.append(p);
            p.d[0] = 4.0;
            p.d[1] = 1.0;
            p.d[2] = -0.41;
            points.append(p);
            p.d[0] = 6.0;
            p.d[1] = 1.0;
            p.d[2] = -0.61;
            points.append(p);
            p.d[0] = 7.0;
            p.d[1] = 1.0;
            p.d[2] = 0.71;
            points.append(p);
            p.d[0] = 9.0;
            p.d[1] = 1.0;
            p.d[2] = 0.91;
            points.append(p);

            p.d[0] = 1.0;
            p.d[1] = 3.0;
            p.d[2] = 0.13;
            points.append(p);
            p.d[0] = 2.0;
            p.d[1] = 3.0;
            p.d[2] = 0.23;
            points.append(p);
            p.d[0] = 4.0;
            p.d[1] = 3.0;
            p.d[2] = 0.43;
            points.append(p);
            p.d[0] = 6.0;
            p.d[1] = 3.0;
            p.d[2] = -0.63;
            points.append(p);
            p.d[0] = 7.0;
            p.d[1] = 3.0;
            p.d[2] = -0.73;
            points.append(p);
            p.d[0] = 9.0;
            p.d[1] = 3.0;
            p.d[2] = -0.93;
            points.append(p);

            p.d[0] = 1.0;
            p.d[1] = 4.0;
            p.d[2] = -0.14;
            points.append(p);
            p.d[0] = 2.0;
            p.d[1] = 4.0;
            p.d[2] = 0.24;
            points.append(p);
            p.d[0] = 4.0;
            p.d[1] = 4.0;
            p.d[2] = 0.44;
            points.append(p);
            p.d[0] = 6.0;
            p.d[1] = 4.0;
            p.d[2] = -0.64;
            points.append(p);
            p.d[0] = 7.0;
            p.d[1] = 4.0;
            p.d[2] = 0.74;
            points.append(p);
            p.d[0] = 9.0;
            p.d[1] = 4.0;
            p.d[2] = -0.94;
            points.append(p);

            p.d[0] = 1.0;
            p.d[1] = 7.0;
            p.d[2] = 0.17;
            points.append(p);
            p.d[0] = 2.0;
            p.d[1] = 7.0;
            p.d[2] = 0.27;
            points.append(p);
            p.d[0] = 4.0;
            p.d[1] = 7.0;
            p.d[2] = -0.47;
            points.append(p);
            p.d[0] = 6.0;
            p.d[1] = 7.0;
            p.d[2] = -0.67;
            points.append(p);
            p.d[0] = 7.0;
            p.d[1] = 7.0;
            p.d[2] = -0.77;
            points.append(p);
            p.d[0] = 9.0;
            p.d[1] = 7.0;
            p.d[2] = 0.97;
            points.append(p);

            p.d[0] = 1.0;
            p.d[1] = 9.0;
            p.d[2] = -0.19;
            points.append(p);
            p.d[0] = 2.0;
            p.d[1] = 9.0;
            p.d[2] = -0.29;
            points.append(p);
            p.d[0] = 4.0;
            p.d[1] = 9.0;
            p.d[2] = -0.49;
            points.append(p);
            p.d[0] = 6.0;
            p.d[1] = 9.0;
            p.d[2] = 0.69;
            points.append(p);
            p.d[0] = 7.0;
            p.d[1] = 9.0;
            p.d[2] = 0.79;
            points.append(p);
            p.d[0] = 9.0;
            p.d[1] = 9.0;
            p.d[2] = 0.99;
            points.append(p);
        }
        GraphPainter2DFillBilinearGrid p(points, x, y, z, TraceGradient());
        p.paint(&painter);
        break;
    }

    default:
        break;
    }
}

void GraphPaintersWidget::mousePressEvent(QMouseEvent *event)
{
    Q_UNUSED(event);

    if (event->button() & Qt::MidButton) {
        subtype++;
    } else {
        type = (type + 1) % 29;
    }
    repaint();
}
