/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTranslator>
#include <QDialog>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLibraryInfo>
#include <QApplication>

#include "core/threadpool.hxx"
#include "guicore/timeboundselection.hxx"
#include "guicore/dynamictimeinterval.hxx"
#include "guicore/timesingleselection.hxx"
#include "guidata/datavaluetimeline.hxx"
#include "guidata/valueeditor.hxx"
#include "guicore/exponentedit.hxx"
#include "guidata/dataeditor.hxx"
#include "guidata/optionseditor.hxx"
#include "guidata/wizards/iointerfaceselect.hxx"
#include "guidata/editors/edittrigger.hxx"
#include "guidata/editors/editaction.hxx"
#include "guidata/editors/editdirective.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicinput.hxx"
#include "widget_test.hxx"
#include "graph_painters.hxx"

using namespace CPD3;
using namespace CPD3::GUI;
using namespace CPD3::Data;
using namespace CPD3::GUI::Data;


void DiagnosticStyle::drawControl(ControlElement element,
                                  const QStyleOption *option,
                                  QPainter *painter,
                                  const QWidget *widget) const
{
    BaseStyle::drawControl(element, option, painter, widget);
    if (widget && painter) {
        // draw a border around the widget
        painter->setPen(QColor("red"));
        painter->drawRect(widget->rect());

        // show the classname of the widget
        QBrush translucentBrush(QColor(255, 246, 240, 100));
        painter->fillRect(widget->rect(), translucentBrush);
        painter->setPen(QColor("darkblue"));
        painter->drawText(widget->rect(), Qt::AlignLeft | Qt::AlignVCenter,
                          widget->metaObject()->className());
    }
}


CPD3WidgetTest::CPD3WidgetTest()
{
    dialog = NULL;

    QCheckBox *checkBox;
    QPushButton *button;
    QMenu *menu;
    QVBoxLayout *layout = new QVBoxLayout;

    checkBox = new QCheckBox(tr("Enable Diagnostic Style"), this);
    checkBox->connect(checkBox, SIGNAL(stateChanged(int)), this, SLOT(toggleDiagnosticStyle(int)));
    layout->addWidget(checkBox);


    button = new QPushButton(tr("Basic Widgets"), this);
    layout->addWidget(button);
    menu = new QMenu(button);
    button->setMenu(menu);
    menu->addAction(tr("Time Selection"), this, SLOT(showTimeSelection()));
    menu->addAction(tr("Scientific Value Editor"), this, SLOT(showExponentEdit()));
    menu->addAction(tr("I/O Interface Selection"), this, SLOT(showInterfaceSelection()));
    menu->addAction(tr("Options Editor"), this, SLOT(showOptionsEditor()));


    button = new QPushButton(tr("Data Widgets"), this);
    layout->addWidget(button);
    menu = new QMenu(button);
    button->setMenu(menu);
    menu->addAction(tr("Data Value Timeline"), this, SLOT(showDataValueTimeline()));
    menu->addAction(tr("Value Editor"), this, SLOT(showValueEditor()));
    menu->addAction(tr("Data Editor"), this, SLOT(showDataEditor()));
    menu->addAction(tr("Variable Selection"), this, SLOT(showVariableSelect()));


    button = new QPushButton(tr("Editor Widgets"), this);
    layout->addWidget(button);
    menu = new QMenu(button);
    button->setMenu(menu);
    menu->addAction(tr("Trigger"), this, SLOT(showEditTrigger()));
    menu->addAction(tr("Action"), this, SLOT(showEditAction()));
    menu->addAction(tr("Edit Directive"), this, SLOT(showEditDirective()));

    button = new QPushButton(tr("Show Graph Painters Test"), this);
    button->connect(button, SIGNAL(clicked()), this, SLOT(showGraphPainters()));
    layout->addWidget(button);


    setLayout(layout);
}

void CPD3WidgetTest::toggleDiagnosticStyle(int state)
{
    if (state == Qt::Checked) {
        QApplication::setStyle(new DiagnosticStyle);
    } else {
        QApplication::setStyle(new QCommonStyle);
    }
}

void CPD3WidgetTest::showTimeSelection()
{
    if (dialog != NULL) {
        dialog->hide();
        dialog->deleteLater();
    }

    dialog = new QDialog(this);
    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(new TimeBoundSelection(dialog));
    layout->addWidget(new TimeIntervalSelection(dialog));
    layout->addWidget(new TimeSingleSelection(dialog));
    dialog->setLayout(layout);
    dialog->show();
    dialog->raise();
    dialog->activateWindow();
}

void CPD3WidgetTest::showDataValueTimeline()
{
    if (dialog != NULL) {
        dialog->hide();
        dialog->deleteLater();
    }

    dialog = new QDialog(this);
    QVBoxLayout *layout = new QVBoxLayout;
    DataValueTimeline *tl;

    tl = new DataValueTimeline(dialog);
    tl->setValues(SequenceValue::Transfer{
            SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(1.0), 315532800, 946684800, 0),
            SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(2.0), 631152000, 946684800, 1),
            SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(3.0), 788918400, 946684800, -1),
            SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(4.0), 946684800, 1262304000, 1),
            SequenceValue(SequenceName("_", "raw", "BsG_S11"), Variant::Root(5.0), 315532800,
                          1262304000,
                          5)});
    connect(tl, SIGNAL(valueClicked(
                               const CPD3::Data::SequenceIdentity &)), tl, SLOT(highlightValue(
                                                                                     const CPD3::Data::SequenceIdentity &)));
    layout->addWidget(tl);

    tl = new DataValueTimeline(dialog);
    tl->setValues(SequenceValue::Transfer{
            SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(1.0), FP::undefined(), 946684800,
                          0),
            SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(2.0), FP::undefined(), 946684800,
                          1),
            SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(3.0), FP::undefined(), 946684800,
                          -1),
            SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(4.0), 946684800, FP::undefined(),
                          1),
            SequenceValue(SequenceName("_", "raw", "BsG_S11"), Variant::Root(5.0), FP::undefined(),
                          FP::undefined(), 5)});
    connect(tl, SIGNAL(valueClicked(
                               const CPD3::Data::SequenceIdentity &)), tl, SLOT(highlightValue(
                                                                                     const CPD3::Data::SequenceIdentity &)));
    layout->addWidget(tl);

    tl = new DataValueTimeline(dialog);
    tl->setValues(SequenceValue::Transfer{
            SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(4.0), FP::undefined(),
                          FP::undefined(),
                          1),
            SequenceValue(SequenceName("_", "raw", "BsG_S11"), Variant::Root(5.0), FP::undefined(),
                          FP::undefined(), 5)});
    connect(tl, SIGNAL(valueClicked(
                               const CPD3::Data::SequenceIdentity &)), tl, SLOT(highlightValue(
                                                                                     const CPD3::Data::SequenceIdentity &)));
    layout->addWidget(tl);

    tl = new DataValueTimeline(dialog);
    tl->setValues(SequenceValue::Transfer{
            SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(4.0), FP::undefined(),
                          1262304000, 0),
            SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(5.0), 1262304000,
                          FP::undefined(), 0),
            SequenceValue(SequenceName("_", "raw", "BsG_S11"), Variant::Root(7.0), 1262304000,
                          1293840000,
                          1)});
    connect(tl, SIGNAL(valueClicked(
                               const CPD3::Data::SequenceIdentity &)), tl, SLOT(highlightValue(
                                                                                     const CPD3::Data::SequenceIdentity &)));
    layout->addWidget(tl);

    tl = new DataValueTimeline(dialog);
    tl->setValues(SequenceValue::Transfer{
            SequenceValue({"_", "raw_meta", "BsG_S11"}, Variant::Root(0.5), 1356998400, 1357084800),
            SequenceValue({"_", "raw_meta", "BsG_S11"}, Variant::Root(0.5), 1357084800, 1357257600),
            SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(1.0), 1356998400, 1357084800),
            SequenceValue({"brw", "raw", "BsR_S11"}, Variant::Root(1.1), 1356998400, 1357084800),
            SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(2.0), 1357084800, 1357171200),
            SequenceValue({"brw", "raw", "BsR_S11"}, Variant::Root(2.1), 1357084800, 1357171200),
            SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(3.0), 1357171200, 1357257600),
            SequenceValue({"brw", "raw", "BsR_S11"}, Variant::Root(3.1), 1357171200, 1357257600)});
    connect(tl, SIGNAL(valueClicked(
                               const CPD3::Data::SequenceIdentity &)), tl, SLOT(highlightValue(
                                                                                     const CPD3::Data::SequenceIdentity &)));
    layout->addWidget(tl);

    dialog->setLayout(layout);
    dialog->show();
    dialog->raise();
    dialog->activateWindow();
}

void CPD3WidgetTest::showValueEditor()
{
    if (dialog != NULL) {
        dialog->hide();
        dialog->deleteLater();
    }

    Variant::Write meta = Variant::Write::empty();
    meta.metadataHash("Description").setString("A description");
    meta.metadataHashChild("Key1").metadataReal("DefaultCollapsed").setBool(true);
    meta.metadataHashChild("Key2").metadataReal("Editor")["Minimum"] = 1.0;
    meta.metadataHashChild("Key3").metadataReal("Editor")["Maximum"] = 4.0;
    meta.metadataHashChild("Key3").metadataReal("Description").setString("A child");

    Variant::Write value = Variant::Write::empty();
    value.hash("Key1").setDouble(1.5);
    value.hash("Key3").setDouble(2.5);

    dialog = new QDialog(this);
    QVBoxLayout *layout = new QVBoxLayout;
    ValueEditor *ve = new ValueEditor(dialog);
    ve->setValue(value, meta);
    layout->addWidget(ve);
    dialog->setLayout(layout);
    dialog->show();
    dialog->raise();
    dialog->activateWindow();
}

void CPD3WidgetTest::showExponentEdit()
{
    if (dialog != NULL) {
        dialog->hide();
        dialog->deleteLater();
    }

    dialog = new QDialog(this);
    QVBoxLayout *layout = new QVBoxLayout;
    layout->addWidget(new ExponentEdit(dialog));
    layout->addWidget(new ExponentEdit(dialog));
    dialog->setLayout(layout);
    dialog->show();
    dialog->raise();
    dialog->activateWindow();
}

void CPD3WidgetTest::showInterfaceSelection()
{
    if (dialog != NULL) {
        dialog->hide();
        dialog->deleteLater();
    }

    dialog = new IOInterfaceSelect(this);
    dialog->show();
    dialog->raise();
    dialog->activateWindow();
}

void CPD3WidgetTest::showDataEditor()
{
    if (dialog != NULL) {
        dialog->hide();
        dialog->deleteLater();
    }

    dialog = new QDialog(this);
    QVBoxLayout *layout = new QVBoxLayout;
    DataEditor *editor = new DataEditor(dialog);
    editor->setSelectionFlags(DataEditor::SelectEverything);
    editor->setAvailableStations({"alt", "brw", "bnd"});
    editor->setAvailableArchives({"raw", "clean", "config"});
    editor->setAvailableVariables({"BsG_S11", "GUI"});
    editor->setAvailableFlavors({"pm1", "pm10", "stddev"});

    SequenceValue::Transfer data;
    Variant::Root value;

    value.write().metadataHash("GenericMeta").setDouble(1.0);
    value.write().metadataHashChild("Key1").metadataReal("Default").setDouble(1.0);
    value.write().metadataHashChild("Key2").metadataReal("Default").setDouble(2.0);
    value.write().metadataHashChild("Key3").metadataReal("Default").setDouble(3.0);
    data.emplace_back(SequenceName("brw", "config_meta", "GUI"), value, FP::undefined(),
                          FP::undefined());
    value.write().setEmpty();

    value.write().metadataHashChild("").metadataReal("Minimum").setDouble(-10.0);
    value.write().metadataHashChild("").metadataReal("Maximum").setDouble(10.0);
    data.emplace_back(SequenceName("brw", "config_meta", "GUI"), value, FP::undefined(),
                          FP::undefined(), 1);
    value.write().setEmpty();

    value.write().hash("Key1").setDouble(1.5);
    value.write().hash("Key3").setDouble(2.5);
    data.emplace_back(SequenceName("brw", "config", "GUI"), value, FP::undefined(), 1262304000,
                          0);
    value.write().setEmpty();

    value.write().hash("Key1").setDouble(0.5);
    data.emplace_back(SequenceName("_", "config", "GUI"), value, FP::undefined(), 1293840000,
                          1);
    value.write().setEmpty();

    value.write().hash("Key3").setDouble(2.75);
    data.emplace_back(SequenceName("brw", "config", "GUI"), value, 1262304000, FP::undefined(),
                          0);
    value.write().setEmpty();

    editor->setData(data);

    layout->addWidget(editor);
    dialog->setLayout(layout);
    dialog->show();
    dialog->raise();
    dialog->activateWindow();
}

void CPD3WidgetTest::showOptionsEditor()
{
    if (dialog != NULL) {
        dialog->hide();
        dialog->deleteLater();
    }

    dialog = new QDialog(this);
    QVBoxLayout *layout = new QVBoxLayout;
    OptionEditor *editor = new OptionEditor(dialog);

    ComponentOptions options;
    options.add("option1",
                new ComponentOptionSingleString(tr("option1", "name"), tr("String option"),
                                                tr("This option does some stuff to some things.  It also does other "
                                                           "more interesting things."),
                                                tr("A default value")));
    options.add("option2", new ComponentOptionFile(tr("option2", "name"), tr("File option"),
                                                   tr("This option does some stuff to some different things.  It also "
                                                              "does other less interesting things."),
                                                   QString()));
    options.add("option3",
                new ComponentOptionDirectory(tr("option3", "name"), tr("Directory option"),
                                             tr("A description goes here."), QString()));
    options.add("option4", new ComponentOptionScript(tr("option4", "name"), tr("Script option"),
                                                     tr("A description goes here."), QString()));
    options.add("option5",
                new ComponentOptionSingleDouble(tr("option5", "name"), tr("Double option"),
                                                tr("A description goes here."), QString()));
    options.add("option6",
                new ComponentOptionSingleInteger(tr("option6", "name"), tr("Integer option"),
                                                 tr("A description goes here."), QString()));
    options.add("option7",
                new ComponentOptionDoubleSet(tr("option7", "name"), tr("Double set option"),
                                             tr("A description goes here."), QString()));
    options.add("option8",
                new ComponentOptionDoubleList(tr("option8", "name"), tr("Double list option"),
                                              tr("A description goes here."), QString()));
    options.add("option9",
                new ComponentOptionStringSet(tr("option9", "name"), tr("String set option"),
                                             tr("A description goes here."), QString()));
    options.add("option10", new ComponentOptionBoolean(tr("option10", "name"), tr("Boolean option"),
                                                       tr("A description goes here."), QString()));
    options.add("option11", new ComponentOptionInstrumentSuffixSet(tr("option11", "name"),
                                                                   tr("Instrument option"),
                                                                   tr("A description goes here."),
                                                                   QString()));
    options.add("option12",
                new ComponentOptionTimeOffset(tr("option12", "name"), tr("Offset option"),
                                              tr("A description goes here."), QString()));
    options.add("option13", new ComponentOptionSingleTime(tr("option13", "name"), tr("Time option"),
                                                          tr("A description goes here."),
                                                          QString()));
    options.add("option14", new ComponentOptionSingleCalibration(tr("option14", "name"),
                                                                 tr("Calibration option"),
                                                                 tr("A description goes here."),
                                                                 QString()));
    options.add("option15",
                new DynamicStringOption(tr("option15", "name"), tr("Time-String option"),
                                        tr("A description goes here."), QString()));
    options.add("option16", new DynamicBoolOption(tr("option16", "name"), tr("Time-Boolean option"),
                                                  tr("A description goes here."), QString()));
    options.add("option17",
                new DynamicDoubleOption(tr("option17", "name"), tr("Time-Double option"),
                                        tr("A description goes here."), QString()));
    options.add("option18",
                new DynamicIntegerOption(tr("option18", "name"), tr("Time-Integer option"),
                                         tr("A description goes here."), QString()));
    options.add("option19",
                new DynamicCalibrationOption(tr("option19", "name"), tr("Time-Calibration option"),
                                             tr("A description goes here."), QString()));
    options.add("option20", new DynamicSequenceSelectionOption(tr("option20", "name"),
                                                               tr("Time-Operate option"),
                                                               tr("A description goes here."),
                                                               QString()));
    options.add("option21", new DynamicInputOption(tr("option21", "name"), tr("Time-Input option"),
                                                   tr("A description goes here."), QString()));

    options.exclude("option2", "option1");

    editor->configureAvailable();
    editor->setOptions(options);

    layout->addWidget(editor);
    dialog->setLayout(layout);
    dialog->show();
    dialog->raise();
    dialog->activateWindow();
}

void CPD3WidgetTest::showGraphPainters()
{
    if (dialog != NULL) {
        dialog->hide();
        dialog->deleteLater();
    }

    dialog = new QDialog(this);
    QVBoxLayout *layout = new QVBoxLayout;
    GraphPaintersWidget *painters = new GraphPaintersWidget(dialog);
    layout->addWidget(painters);
    dialog->setLayout(layout);
    dialog->show();
    dialog->raise();
    dialog->activateWindow();
}

void CPD3WidgetTest::showVariableSelect()
{
    if (dialog != NULL) {
        dialog->hide();
        dialog->deleteLater();
    }

    dialog = new QDialog(this);
    QVBoxLayout *layout = new QVBoxLayout;
    VariableSelect *vs = new VariableSelect(dialog);

    vs->setAvailableArchives({"raw", "clean"});
    vs->setAvailableStations({"sgp", "bnd"});
    vs->setAvailableVariables({"BsB_S11", "BsG_S11", "BsR_S11"});
    vs->setAvailableFlavors({"pm1", "pm10", "end"});

    Variant::Root config;
    config["#0/Variable"].setString("BsB_S11");
    config["#1/Variable"].setString("BsG_S11");
    vs->configureFromSequenceMatch(config);

    layout->addWidget(vs);
    dialog->setLayout(layout);
    dialog->show();
    dialog->raise();
    dialog->activateWindow();
}

void CPD3WidgetTest::showEditTrigger()
{
    if (dialog != NULL) {
        dialog->hide();
        dialog->deleteLater();
    }

    dialog = new QDialog(this);
    QVBoxLayout *layout = new QVBoxLayout;
    EditTriggerEditor *et = new EditTriggerEditor(dialog);

    et->setAvailableVariables({"BsB_S11", "BsG_S11", "BsR_S11"});
    et->setAvailableFlavors({"pm1", "pm10"});

    layout->addWidget(et);
    dialog->setLayout(layout);
    dialog->show();
    dialog->raise();
    dialog->activateWindow();
}

void CPD3WidgetTest::showEditAction()
{
    if (dialog != NULL) {
        dialog->hide();
        dialog->deleteLater();
    }

    dialog = new QDialog(this);
    QVBoxLayout *layout = new QVBoxLayout;
    EditActionEditor *et = new EditActionEditor(dialog);

    et->setAvailableArchives({"raw"});
    et->setAvailableStations({"bnd"});
    et->setAvailableVariables({"BsB_S11", "BsG_S11", "BsR_S11"});
    et->setAvailableFlavors({"pm1", "pm10"});

    layout->addWidget(et);
    dialog->setLayout(layout);
    dialog->show();
    dialog->raise();
    dialog->activateWindow();
}

void CPD3WidgetTest::showEditDirective()
{
    if (dialog != NULL) {
        dialog->hide();
        dialog->deleteLater();
    }

    dialog = new QDialog(this);
    QVBoxLayout *layout = new QVBoxLayout;
    EditDirectiveEditor *et = new EditDirectiveEditor(dialog);

    et->setAvailableArchives({"raw"});
    et->setAvailableStations({"bnd"});
    et->setAvailableVariables({"BsB_S11", "BsG_S11", "BsR_S11"});
    et->setAvailableFlavors({"pm1", "pm10"});

    Variant::Root data;
    data["History/#0/Type"].setString("Created");
    data["History/#0/At"].setDouble(1409064471);
    data["History/#0/User"].setString("hageman");

    data["History/#1/Type"].setString("BoundsChanged");
    data["History/#1/At"].setDouble(1409064511);
    data["History/#1/User"].setString("aerosol");
    data["History/#1/OriginalBounds/Start"].setDouble(1293840000);
    data["History/#1/OriginalBounds/End"].setDouble(1325376000);
    data["History/#1/RevisedBounds/Start"].setDouble(1293840000);
    data["History/#1/RevisedBounds/End"].setDouble(1356998400);

    et->configure(data.write());

    layout->addWidget(et);
    dialog->setLayout(layout);
    dialog->show();
    dialog->raise();
    dialog->activateWindow();
}

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

    QString translateLocation;
    {
        QByteArray env(qgetenv("CPD3"));
        if (!env.isEmpty()) {
            translateLocation = QString::fromUtf8(env) + "/locale";
        }
    }
    QTranslator cpd3Translator;
    cpd3Translator.load("cpd3_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cpd3Translator);
    QTranslator guiTranslator;
    guiTranslator.load("gui_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&guiTranslator);

    CPD3WidgetTest widget_test;
    widget_test.show();
    int rc = app.exec();
    ThreadPool::system()->wait();
    return rc;
}
