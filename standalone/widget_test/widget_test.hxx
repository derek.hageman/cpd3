/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef WIDGETTEST_H
#define WIDGETTEST_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QWidget>
#include <QDialog>
#include <QPainter>

#include <QCommonStyle>

#include "datacore/variant/root.hxx"

class CPD3WidgetTest : public QWidget {
Q_OBJECT

    QDialog *dialog;
public:
    CPD3WidgetTest();

private slots:

    void toggleDiagnosticStyle(int state);

    void showTimeSelection();

    void showInterfaceSelection();

    void showDataValueTimeline();

    void showValueEditor();

    void showExponentEdit();

    void showDataEditor();

    void showOptionsEditor();

    void showGraphPainters();

    void showVariableSelect();

    void showEditTrigger();

    void showEditAction();

    void showEditDirective();
};

class DiagnosticStyle : public QCommonStyle {
    typedef QCommonStyle BaseStyle;
Q_OBJECT
public:
    void drawControl(ControlElement element,
                     const QStyleOption *option,
                     QPainter *painter,
                     const QWidget *widget) const;
};

#endif
