/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef DISPLAYWINDOW_H
#define DISPLAYWINDOW_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QWidget>
#include <QMainWindow>
#include <QLabel>
#include <QProgressBar>
#include <QSettings>

#include "graphing/displaywidget.hxx"
#include "graphing/display.hxx"
#include "core/actioncomponent.hxx"
#include "datacore/variant/root.hxx"
#include "display_handler.hxx"

class DisplayWindow : public QMainWindow {
Q_OBJECT

    DisplayHandler *handler;
    CPD3::Graphing::DisplayWidget *displayWidget;
    QTimer *repaintTimer;
    QProgressBar *progressBar;
    QLabel *statusLabel;
    QString statusText;
    QString statusState;
    CPD3::ActionFeedback::Serializer incoming;

    void feedbackUpdate();
public:
    DisplayWindow(DisplayHandler *setHandler);

    virtual ~DisplayWindow();

    virtual QSize sizeHint() const;

public slots:

    void start();

    void finished();

    void abort();

private slots:

    void chainReload();

    void saveChanges(CPD3::Data::ValueSegment::Transfer overlay);
};

#endif
