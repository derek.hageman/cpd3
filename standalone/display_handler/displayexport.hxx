/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef DISPLAYEXPORT_H
#define DISPLAYEXPORT_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QWidget>
#include <QFile>

#include <QGuiApplication>

#include "graphing/displaywidget.hxx"
#include "graphing/display.hxx"
#include "clicore/terminaloutput.hxx"
#include "display_handler.hxx"

class DisplayExport : public QObject {
Q_OBJECT

    DisplayHandler *handler;
    QGuiApplication *application;
    std::unique_ptr<CPD3::Graphing::Display> display;
    CPD3::CLI::TerminalProgress progress;
public:
    DisplayExport(DisplayHandler *setHandler, QGuiApplication *setApplication);

    virtual ~DisplayExport();

private slots:

    void writeOutput();
};

#endif
