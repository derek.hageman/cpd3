/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QMainWindow>
#include <QStatusBar>

#include "displaywindow.hxx"
#include "guicore/guiformat.hxx"

using namespace CPD3;
using namespace CPD3::Graphing;
using namespace CPD3::GUI;
using namespace CPD3::Data;

namespace {
class TimerBackoffDisplayWidget : public CPD3::Graphing::DisplayWidget {
    QTimer *repaintTimer;
    int baseInterval;
    int thresholdInterval;
public:
    TimerBackoffDisplayWidget(std::unique_ptr<CPD3::Graphing::Display> &&setDisplay,
                              QWidget *parent,
                              QTimer *timer) : DisplayWidget(std::move(setDisplay), parent),
                                               repaintTimer(timer),
                                               baseInterval(timer->interval()),
                                               thresholdInterval((int) (timer->interval() * 0.1))
    { }

protected:
    virtual void paintEvent(QPaintEvent *event)
    {
        ElapsedTimer timeTaken;
        timeTaken.start();
        DisplayWidget::paintEvent(event);
        int elapsed = (int) timeTaken.elapsed();
        if (elapsed > thresholdInterval) {
            int adjustedInterval = baseInterval + elapsed;
            if (abs(repaintTimer->interval() - adjustedInterval) > 10) {
                repaintTimer->setInterval(adjustedInterval);
            }
        } else {
            if (repaintTimer->interval() != baseInterval) {
                repaintTimer->setInterval(baseInterval);
            }
        }
    }
};
}

DisplayWindow::DisplayWindow(DisplayHandler *setHandler) : QMainWindow(), handler(setHandler)
{
    setWindowTitle(tr("CPD3 Display"));

    repaintTimer = new QTimer(this);
    if (handler->getSettings() != NULL) {
        repaintTimer->setInterval(
                handler->getSettings()->value("display/repaintinterval", 1000).toInt());
    } else {
        repaintTimer->setInterval(1000);
    }
    repaintTimer->setSingleShot(false);

    displayWidget = new TimerBackoffDisplayWidget(handler->takeDisplay(), this, repaintTimer);
    displayWidget->setContext(handler->getDisplayContext());
    displayWidget->setResetSelectsAll(true);
    connect(displayWidget, SIGNAL(internalZoom(CPD3::Graphing::DisplayZoomCommand)), displayWidget,
            SLOT(applyZoom(CPD3::Graphing::DisplayZoomCommand)));
    connect(displayWidget, SIGNAL(visibleZoom(CPD3::Graphing::DisplayZoomCommand)), displayWidget,
            SLOT(applyZoom(CPD3::Graphing::DisplayZoomCommand)));
    connect(displayWidget, SIGNAL(globalZoom(CPD3::Graphing::DisplayZoomCommand)), displayWidget,
            SLOT(applyZoom(CPD3::Graphing::DisplayZoomCommand)));
    connect(displayWidget, SIGNAL(timeRangeSelected(double, double)), displayWidget,
            SLOT(setVisibleTimeRange(double, double)), Qt::QueuedConnection);
    setCentralWidget(displayWidget);
    connect(repaintTimer, SIGNAL(timeout()), displayWidget, SLOT(update()));

    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

    progressBar = new QProgressBar(statusBar());
    progressBar->setMaximum(0);
    progressBar->setMinimum(0);
    progressBar->reset();
    progressBar->setValue(1);
    statusBar()->addPermanentWidget(progressBar);
    statusLabel = new QLabel(tr("Initializing"));
    statusBar()->addWidget(statusLabel);

    connect(handler, SIGNAL(start()), this, SLOT(start()));
    connect(handler, SIGNAL(finished()), this, SLOT(finished()));
    connect(handler, SIGNAL(abort()), this, SLOT(abort()));
    incoming.updated.connect(this, std::bind(&DisplayWindow::feedbackUpdate, this), true);
    incoming.attach(handler->feedback);
    connect(displayWidget, SIGNAL(requestChainReload()), this, SLOT(chainReload()));
    connect(displayWidget, SIGNAL(saveChanges(CPD3::Data::ValueSegment::Transfer)), this,
            SLOT(saveChanges(CPD3::Data::ValueSegment::Transfer)));

    chainReload();
}

DisplayWindow::~DisplayWindow()
{ }

void DisplayWindow::chainReload()
{
    DisplayWidgetTapChain *chain = new DisplayWidgetTapChain;
    handler->setupChain(chain);
    displayWidget->registerChain(chain);
    repaintTimer->start();
    handler->startChain();
}

void DisplayWindow::saveChanges(ValueSegment::Transfer overlay)
{
    DisplayWidget::saveConfiguration(handler->displayBaselineConfiguration(), overlay, this);
}

QSize DisplayWindow::sizeHint() const
{ return QSize(801, 600); }

void DisplayWindow::start()
{
    statusText = QString();
    statusState = QString();

    progressBar->setTextVisible(false);
    progressBar->setMinimum(0);
    progressBar->setMaximum(0);
    progressBar->setValue(1);
    statusLabel->setText("");
    progressBar->show();
    statusLabel->show();
}

void DisplayWindow::finished()
{
    progressBar->setMinimum(0);
    progressBar->setMaximum(0);
    progressBar->setValue(1);
    statusLabel->setText("");
    progressBar->hide();
    statusLabel->hide();
    repaintTimer->stop();

    displayWidget->update();
}

void DisplayWindow::abort()
{ finished(); }

void DisplayWindow::feedbackUpdate()
{
    ActionFeedback::Serializer::Stage stage = incoming.process().back();

    double fraction = stage.toFraction(handler->getBounds().start, handler->getBounds().end);
    if (stage.state() == ActionFeedback::Serializer::Stage::Spin && FP::defined(fraction)) {
        progressBar->setTextVisible(false);
        progressBar->setMinimum(0);
        progressBar->setMaximum(0);
        progressBar->setValue(1);
    } else {
        progressBar->setTextVisible(true);
        progressBar->setMinimum(0);
        progressBar->setMaximum(1000);
        progressBar->setValue((int) ::floor(fraction * 1000.0));
    }

    QString firstLine;

    QString title = stage.title();
    if (!title.isEmpty()) {
        double time = stage.time();
        if (FP::defined(time)) {
            if (!stage.stageStatus().isEmpty()) {
                statusLabel->setText(tr("%1 -- %2 (%3)", "stage state label time").arg(title,
                                                                                       GUITime::formatTime(
                                                                                               time,
                                                                                               handler->getSettings()),
                                                                                       stage.stageStatus()));
            } else {
                statusLabel->setText(tr("%1 -- %2", "stage label time").arg(title,
                                                                            GUITime::formatTime(
                                                                                    time,
                                                                                    handler->getSettings())));
            }
        } else {
            if (!stage.stageStatus().isEmpty()) {
                statusLabel->setText(
                        tr("%1 (%2)", "stage state label").arg(title, stage.stageStatus()));
            } else {
                statusLabel->setText(title);
            }
        }
    }

    progressBar->setToolTip(stage.description());
}