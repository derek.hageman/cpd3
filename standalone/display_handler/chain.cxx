/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#ifdef NO_CONSOLE
#include <QMessageBox>
#endif

#include "display_handler.hxx"
#include "core/stdindevice.hxx"
#include "clicore/terminaloutput.hxx"
#include "guicore/guiformat.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Graphing;
using namespace CPD3::CLI;
using namespace CPD3::GUI;

void DisplayHandler::setupChain(ProcessingTapChain *newChain, bool retainOutput)
{
    if (haveStartedPipeline) {
        pipeline->waitInEventLoop();
    }

    if (chain != NULL) {
        chain->signalTerminate();
        chain->wait();
        chain->deleteLater();
        chain = NULL;
    }

    chain = newChain;
    connect(chain, SIGNAL(finished()), this, SLOT(checkCleanup()), Qt::QueuedConnection);
    feedback.stage = chain->feedback.stage;
    //feedback.state = chain->feedback.state;
    feedback.failure = chain->feedback.failure;
    feedback.progressTime = chain->feedback.progressTime;
    feedback.progressFraction = chain->feedback.progressFraction;

    feedback.progressTime.connect(std::bind(&DisplayHandler::generateChainState, this));
    feedback.progressFraction.connect(std::bind(&DisplayHandler::generateChainState, this));

    chain->setDefaultSelection(chainBounds.start, chainBounds.end, chainStations, chainArchives,
                               chainVariables);

    if (!pipeline->setOutputFilterChain(chain, retainOutput)) {
#ifndef NO_CONSOLE
        TerminalOutput::simpleWordWrap(
                tr("Error setting output chain: %1").arg(pipeline->getOutputError()), true);
#else
        QMessageBox::critical(0, tr("Error setting output"),
            tr("Error setting output filter chain: %1").
            arg(pipeline->getOutputError()));
#endif
        QCoreApplication::exit(1);
    }

    if (requestPipelineInput) {
        requestPipelineInput = false;
        if (!pipeline->setInputArchiveRequested(chainBounds, true)) {
#ifndef NO_CONSOLE
            TerminalOutput::simpleWordWrap(
                    tr("Error setting requested input: %1").arg(pipeline->getInputError()), true);
#else
            QMessageBox::critical(0, tr("Error in command line time"),
                    tr("Error setting requested input: %1").
                    arg(pipeline->getInputError()));
#endif
            QCoreApplication::exit(1);
        }
    }

    emit start();
}

void DisplayHandler::generateChainState()
{
    quint64 expectedInputSize = pipeline->getExpectedInputSize();
    quint64 latestInputSize = pipeline->getTotalInputRead();

    if (latestInputSize > 0 && (expectedInputSize <= 0 || latestInputSize < expectedInputSize)) {
        if (latestInputSize > 1024 * 1024 * 1024) {
            feedback.emitState(
                    tr("%1 GiB read").arg((double) latestInputSize / (double) (1024 * 1024 * 1024),
                                          0, 'f', 2));
        } else if (latestInputSize > 1024 * 1024) {
            feedback.emitState(
                    tr("%1 MiB read").arg((double) latestInputSize / (double) (1024 * 1024), 0, 'f',
                                          2));
        } else if (latestInputSize > 1024) {
            feedback.emitState(
                    tr("%1 KiB read").arg((double) latestInputSize / (double) (1024), 0, 'f', 2));
        } else {
            feedback.emitState(tr("%n bytes read", "", (int) latestInputSize));
        }
    } else {
        feedback.emitState(QString());
    }
}
