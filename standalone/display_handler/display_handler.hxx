/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef DISPLAYHANDLER_H
#define DISPLAYHANDLER_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QWidget>
#include <QMainWindow>
#include <QList>
#include <QFile>
#include <QLabel>
#include <QProgressBar>

#include "datacore/stream.hxx"
#include "datacore/archive/selection.hxx"
#include "guidata/dataeditor.hxx"
#include "datacore/streampipeline.hxx"
#include "graphing/displaywidget.hxx"
#include "graphing/display.hxx"
#include "core/actioncomponent.hxx"

class ArchiveReader;

class DisplayHandler : public QObject {
Q_OBJECT

    QSettings *settings;

    std::unique_ptr<CPD3::Data::StreamPipeline> pipeline;
    CPD3::Data::ProcessingTapChain *chain;
    bool haveStartedPipeline;
    bool requestPipelineInput;

    QString outputFile;
    int outputWidth;
    int outputHeight;
    bool fillOpaque;

    CPD3::Data::Archive::Selection::List chainSelections;
    CPD3::Time::Bounds chainBounds;
    CPD3::Data::Archive::Selection::Match chainStations;
    CPD3::Data::Archive::Selection::Match chainArchives;
    CPD3::Data::Archive::Selection::Match chainVariables;

    std::unique_ptr<CPD3::Graphing::Display> display;
    CPD3::Data::Variant::Read baselineConfiguration;

    CPD3::Graphing::DisplayDynamicContext displayContext;

    int parseOptions(const QString &programName,
                     const QString &componentName,
                     const QString &componentDisplay,
                     const QString &componentDescription,
                     QStringList &arguments,
                     CPD3::ComponentOptions &options,
                     const QList<CPD3::ComponentExample> &examples);

    void generateChainState();

public:
    DisplayHandler();

    virtual ~DisplayHandler();

    int parseArguments(QStringList arguments);

    void setupChain(CPD3::Data::ProcessingTapChain *chain, bool retainOutput = true);

    inline const CPD3::Time::Bounds &getBounds() const
    { return chainBounds; }

    void setSettings(QSettings *set);

    inline QSettings *getSettings()
    { return settings; }

    const CPD3::Graphing::DisplayDynamicContext &getDisplayContext() const
    { return displayContext; }

    inline std::unique_ptr<CPD3::Graphing::Display> takeDisplay()
    { return std::move(display); }

    inline const CPD3::Data::Variant::Read &displayBaselineConfiguration()
    { return baselineConfiguration; }

    inline bool isGUI() const
    { return outputFile.isEmpty(); }

    inline QString getOutputFile() const
    { return outputFile; }

    inline QSize getOutputSize() const
    { return QSize(outputWidth, outputHeight); }

    inline bool getOutputOpaque() const
    { return fillOpaque; }

    inline void startChain()
    {
        pipeline->start();
        haveStartedPipeline = true;
    }

    void waitCompleted();

    CPD3::ActionFeedback::Source feedback;

signals:

    void start();

    void finished();

    void abort();

public slots:

    void shutdown();

private slots:

    void checkCleanup();
};

#endif
