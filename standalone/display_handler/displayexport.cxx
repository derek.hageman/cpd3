/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QSvgGenerator>
#include <QImage>

#include "displayexport.hxx"

using namespace CPD3;
using namespace CPD3::Graphing;
using namespace CPD3::GUI;
using namespace CPD3::Data;

DisplayExport::DisplayExport(DisplayHandler *setHandler, QGuiApplication *setApplication)
        : QObject(), handler(setHandler), application(setApplication)
{
    display = handler->takeDisplay();
    connect(display.get(), &CPD3::Graphing::Display::readyForFinalPaint, this,
            &DisplayExport::writeOutput, Qt::QueuedConnection);

    connect(handler, SIGNAL(start()), &progress, SLOT(start()));
    connect(handler, SIGNAL(abort()), &progress, SLOT(abort()));
    progress.attach(handler->feedback);

    ProcessingTapChain *chain = new ProcessingTapChain;
    handler->setupChain(chain, false);
    display->registerChain(chain);
    handler->startChain();
}

DisplayExport::~DisplayExport() = default;

void DisplayExport::writeOutput()
{
    QString fileName(handler->getOutputFile());
    DisplayDynamicContext context(handler->getDisplayContext());
    context.setInteractive(false);

    ActionFeedback::Source feedback;
    progress.attach(feedback);

    if (fileName.endsWith(".svg", Qt::CaseInsensitive)) {
        feedback.emitStage(tr("Writing %1").arg(fileName), tr("Writing to final output file."));

        QSize size(handler->getOutputSize());
        QSvgGenerator svg;
        svg.setFileName(fileName);
        svg.setSize(size);
        svg.setViewBox(QRect(0, 0, size.width(), size.height()));
        QPainter painter;
        painter.begin(&svg);
        display->paint(&painter, QRectF(0, 0, size.width(), size.height()), context);
        painter.end();
    } else {
        feedback.emitStage(tr("Drawing output"), tr("The display is being drawn."));
        QSize size(handler->getOutputSize());
        QImage img(size, QImage::Format_ARGB32);
        if (handler->getOutputOpaque())
            img.fill(0xFFFFFFFF);
        else
            img.fill(0x00FFFFFF);
        QPainter painter;
        painter.begin(&img);
        display->paint(&painter, QRectF(0, 0, size.width(), size.height()), context);
        painter.end();

        feedback.emitStage(tr("Writing image to %1").arg(fileName),
                           tr("Writing to final output file."));
        img.save(fileName);
    }

    handler->waitCompleted();

    progress.finished();
    application->quit();
}
