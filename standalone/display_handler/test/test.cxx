/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QObject>
#include <QtDebug>
#include <QProcess>
#include <QString>

class TestComponent : public QObject {
Q_OBJECT

private slots:

    void initTestCase()
    {
        QVERIFY(qputenv("CPD3ARCHIVE", QByteArray("sqlite:_")));
    }

    void cliHelp()
    {
        QProcess p;
        p.start("da.show.scatter", QStringList() << "--help");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        QVERIFY(output.contains("Usage:"));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
