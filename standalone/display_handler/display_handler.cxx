/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#ifdef Q_OS_UNIX

#include <unistd.h>

#endif

#include <QGuiApplication>
#include <QApplication>

#include <QTranslator>
#include <QLibraryInfo>
#include <QWidget>
#include <QSettings>

#include "core/abort.hxx"
#include "core/number.hxx"
#include "core/range.hxx"
#include "core/threadpool.hxx"
#include "core/memory.hxx"
#include "graphing/display.hxx"
#include "graphing/graph2d.hxx"
#include "clicore/terminaloutput.hxx"
#include "clicore/graphics.hxx"

#include "display_handler.hxx"
#include "displaywindow.hxx"
#include "displayexport.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Graphing;
using namespace CPD3::CLI;

DisplayHandler::DisplayHandler()
{
    settings = new QSettings(CPD3GUI_ORGANIZATION, CPD3GUI_APPLICATION, this);

    haveStartedPipeline = false;
    requestPipelineInput = false;

    outputWidth = 1024;
    outputHeight = 768;
    fillOpaque = false;

    bool enablePipelineAcceleration = false;
    bool enablePipelineSerialization = false;
#ifndef NO_CONSOLE
    if (char *env = getenv("CPD3CLI_ACCELERATION")) {
        if (env[0] && atoi(env))
            enablePipelineAcceleration = true;
    }
    if (char *env = getenv("CPD3CLI_COMBINEPROCESSES")) {
        if (env[0] && atoi(env))
            enablePipelineSerialization = true;
    }
#endif

    chain = NULL;
    pipeline.reset(new StreamPipeline(enablePipelineAcceleration, enablePipelineSerialization));
    pipeline->finished.connect(this, std::bind(&DisplayHandler::checkCleanup, this), true);
}

DisplayHandler::~DisplayHandler()
{
    pipeline.reset();

    if (chain != NULL)
        delete chain;

    if (settings != NULL)
        delete settings;
}

void DisplayHandler::shutdown()
{
    emit abort();

    pipeline->signalTerminate();
    pipeline->waitInEventLoop();

    QCoreApplication::exit(0);
}

void DisplayHandler::checkCleanup()
{
    if (!pipeline->isFinished())
        return;

    if (chain != NULL) {
        chain->deleteLater();
        chain = NULL;

        emit finished();
    }
}

void DisplayHandler::waitCompleted()
{
    pipeline->waitInEventLoop();
}

void DisplayHandler::setSettings(QSettings *set)
{
    this->settings = set;
    displayContext.setSettings(set);
}

static bool canRunHeadless(const char *argument)
{
#if defined(Q_OS_UNIX)
    {
        char *env = ::getenv("CPD3_DISABLEGRAPHICS");
        if (env != NULL && env[0] && atoi(env))
            return true;
    }
#endif

    if (!argument)
        return false;
    QString arg(argument);
    if (arg.contains("da.export", Qt::CaseInsensitive))
        return true;
    if (arg.contains("da_export", Qt::CaseInsensitive))
        return true;
    if (arg.contains("da.plot", Qt::CaseInsensitive))
        return true;
    if (arg.contains("da_plot", Qt::CaseInsensitive))
        return true;
    return false;
}


#if defined(ALLOCATOR_TYPE) && ALLOCATOR_TYPE == 1
#include <tbb/scalable_allocator.h>
static void allocatorRelease()
{
    ::free(nullptr);
#ifdef HAVE_TBB_COMMAND
    scalable_allocation_command(TBBMALLOC_CLEAN_ALL_BUFFERS, nullptr);
#endif
}
static void installAllocator()
{ CPD3::Memory::install_release_function(allocatorRelease); }
#elif defined(ALLOCATOR_TYPE) && ALLOCATOR_TYPE == 2
#include <gperftools/malloc_extension.h>
static void allocatorRelease()
{ MallocExtension::instance()->ReleaseFreeMemory(); }
static void installAllocator()
{ CPD3::Memory::install_release_function(allocatorRelease); }
#else
static void installAllocator()
{ }
#endif

int main(int argc, char **argv)
{
    installAllocator();

    bool initializeGUI = argc <= 0 || !canRunHeadless(argv[0]);

    QGuiApplication *app;
    if (!initializeGUI) {
        app = static_cast<QGuiApplication *>(CLIGraphics::createApplication(argc, argv));
    } else {
        app = new QApplication(argc, argv);
    }
    Q_ASSERT(app);

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app->installTranslator(&qtTranslator);

    QString translateLocation;
    QByteArray cpd3(qgetenv("CPD3"));
    if (cpd3.length() > 0) {
        translateLocation = QString(cpd3) + "/locale";
    }
    QTranslator cpd3Translator;
    cpd3Translator.load("cpd3_" + QLocale::system().name(), translateLocation);
    app->installTranslator(&cpd3Translator);
    QTranslator cliTranslator;
    cliTranslator.load("cli_" + QLocale::system().name(), translateLocation);
    app->installTranslator(&cliTranslator);
    QTranslator guiTranslator;
    guiTranslator.load("gui_" + QLocale::system().name(), translateLocation);
    app->installTranslator(&guiTranslator);
    QTranslator displayHandlerTranslator;
    displayHandlerTranslator.load("displayhandler_" + QLocale::system().name(), translateLocation);
    app->installTranslator(&displayHandlerTranslator);

    DisplayHandler *handler = new DisplayHandler;
    QObject::connect(app, SIGNAL(aboutToQuit()), handler, SLOT(shutdown()));


    if (initializeGUI) {
        handler->setSettings(new QSettings(CPD3GUI_ORGANIZATION, CPD3GUI_APPLICATION));
    }
    if (int rc = handler->parseArguments(app->arguments())) {
        delete handler;
        ThreadPool::system()->wait();
        delete app;
        if (rc == -1)
            return 1;
        return 0;
    }
    int rc;
    if (handler->isGUI() && initializeGUI) {
        DisplayWindow window(handler);
        window.show();
        rc = app->exec();
    } else {
        /* Only use the abort poller in console mode, since a forced
         * exit from the GUI is probably desirable */
        Abort::installAbortHandler(true);
        AbortPoller abortPoller;
        QObject::connect(&abortPoller, SIGNAL(aborted()), app, SLOT(quit()));

#if defined(Q_OS_UNIX) && !defined(NO_CONSOLE)
        if (!initializeGUI) {
            if (!isatty(1) && isatty(2)) {
                TerminalOutput::simpleWordWrap(DisplayHandler::tr(
                        "WARNING: Output is not a terminal; if you are trying to redirect the output to make a plot, use the --file switch instead."),
                                               true);
            }
        }
#endif

        DisplayExport de(handler, app);
        abortPoller.start();
        rc = app->exec();
    }
    delete handler;
    ThreadPool::system()->wait();
    delete app;
    return rc;
}
