/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLibraryInfo>
#include <QApplication>
#include <QStringList>
#include <QThread>
#include <QImageWriter>
#include <QRegularExpression>

#ifdef NO_CONSOLE
#include <QMessageBox>
#endif

#include "display_handler.hxx"
#include "core/stdindevice.hxx"
#include "core/timeparse.hxx"
#include "clicore/terminaloutput.hxx"
#include "clicore/archiveparse.hxx"
#include "clicore/optionparse.hxx"
#include "clicore/argumentparser.hxx"
#include "graphing/displaycomponent.hxx"

using namespace CPD3;
using namespace CPD3::CLI;
using namespace CPD3::Data;
using namespace CPD3::Graphing;

class Parser : public ArgumentParser {
    QString programName;
    QString componentName;
    QString componentDisplay;
    QString componentDescription;
public:
    Parser(const QString &programNameIn,
           const QString &componentNameIn,
           const QString &componentDisplayIn,
           const QString &componentDescriptionIn) :
#ifndef NO_CONSOLE
            ArgumentParser(true),
#else
            ArgumentParser(false),
#endif
            programName(programNameIn),
            componentName(componentNameIn),
            componentDisplay(componentDisplayIn),
            componentDescription(componentDescriptionIn)
    { }

    virtual ~Parser()
    { }

protected:

    virtual QList<BareWordHelp> getBareWordHelp()
    {
        return QList<BareWordHelp>() << BareWordHelp(tr("station", "station argument name"),
                                                     tr("The (default) station to read data from"),
                                                     tr("Inferred from the current directory"),
                                                     tr("The \"station\" bare word argument is used to specify the station(s) used to "
                                                        "look up variables that do not include an explicit station as part of an "
                                                        "archive read specification.  This is the three digit station identification "
                                                        "code.  For example \"brw\".  If a variable does not specify a station, then "
                                                        "this is the station it will be looked up from.  Multiple stations may be "
                                                        "specified by separating them with \":\", \";\" or \",\".  Regular expressions "
                                                        "are accepted."))
                                     << BareWordHelp(tr("variables", "variables argument name"),
                                                     tr("The list of variable specifies to read from the archive"),
                                                     QString(),
                                                     tr("The \"variables\" bare word argument sets the list of variables to read "
                                                        "from the archive.  This can be either a direct list of variables (such as "
                                                        "\"T_S11\" or a alias for multiple variables, such as \"S11a\".  In the single "
                                                        "variable form regular expressions may be used, as long as they match the "
                                                        "entire variable name.  This list is delimited by commas.  If a specification "
                                                        "includes a colon then the part before the colon is treated as an override to "
                                                        "the default archive.  If it includes two colons then the first field is "
                                                        "treated as the station, the second as the archive, and the third as the "
                                                        "actual variable specification.  If it includes four or more colons then "
                                                        "the specification is treated as with three except that the trailing "
                                                        "components specify the flavor (e.x. PM1 or PM10) restriction.  This "
                                                        "restriction consists of an optional prefix and the flavor name.  If there is "
                                                        "no prefix then the flavor is required.  If the prefix is \"!\" then the flavor "
                                                        "is excluded, that is the results will never include it.  If the prefix is"
                                                        "\"=\" then the flavor is added to the list of flavors that any match must "
                                                        "have exactly."
                                                        "\nFor example:"
                                                        "\"T_S11\" specifies the variable in the \"default\" station (set "
                                                        "either explicitly on the command line or inferred from the current directory) "
                                                        "and the \"default\" archive (either the \"raw\" archive or the one set on "
                                                        "the command line.\n"
                                                        "\"raw:T_S1[12]\" specifies the variables T_S11 and T_S12 from the raw archive\n"
                                                        "on the \"default\" station.\n"
                                                        "\"brw:raw:S11a\" specifies the \"S11a\" alias record for the station \"brw\"\n"
                                                        "and the \"raw\" archive.\n"
                                                        "\":avgh:T_S11:pm1:!stats\" specifies T_S11 from the \"default\" station "
                                                        "hourly averages restricted to only PM1 data, but not any calculated statistics.\n\n"
                                                        "The string \"everything\" can also be used to retrieve all available "
                                                        "variables."))
                                     << BareWordHelp(tr("times", "times argument name"),
                                                     tr("The time range of data to read"),
                                                     QString(),
                                                     TimeParse::describeListBoundsUsage(false,
                                                                                        true))
                                     << BareWordHelp(tr("archive", "archive argument name"),
                                                     tr("The (default) archive to read data from"),
                                                     tr("The \"raw\" data archive"),
                                                     tr("The \"archive\" bare word argument is used to specify the archive used to "
                                                        "Look up variables that do not include an archive station as part of an "
                                                        "archive read specification.  This is the internal archive name, such as "
                                                        "\"raw\" or \"clean\".  Multiple archives may be specified by separating them "
                                                        "with \":\", \";\" or \",\".  Regular expressions are accepted."))
                                     << BareWordHelp(tr("inputFile", "file argument name"),
                                                     tr("The file to read data from or - for standard input"),
                                                     QString(),
                                                     tr("The \"inputFile\" bare word argument is used to specify the the file to read "
                                                        "data from.  If it is present and exists then data is read from the given file "
                                                        "name instead of from standard input.  Alternatively \"-\" may be used to "
                                                        "explicitly specify standard input."));
    }

    virtual QString getBareWordCommandLine()
    { return tr("[[station] [variables] times [archive]]|[inputFile]", "bare word command line"); }

    virtual QString getProgramName()
    { return programName; }

    virtual QString getDescription()
    {
        QString text
                (tr("%1 - %2\n\n", "description base").arg(componentDisplay, componentDescription));
#ifndef NO_CONSOLE
        text.append(
                tr("If used without any data specification (variables, start, etc) then input will "
                   "be read from standard input.  "));
#endif
        text.append(tr("If an existing, readable file is given input will be read from that file.  "
                       "Otherwise the data given on the command line is read, with the variables "
                       "determined by the graph's switch(es) if not explicitly specified."));
        return text;
    }

    virtual QString getExamples(const QList<ComponentExample> &examples)
    {
        return generateExamples(examples, OptionParse::Example_DataFilterSilent);
    }

    virtual QHash<QString, QString> getDocumentationTypeID()
    {
        QHash<QString, QString> result;
        result.insert("type", "display");

        if (!componentName.isEmpty())
            result.insert("output", componentName);
        if (!componentDisplay.isEmpty())
            result.insert("display", componentDisplay);
        if (!componentDescription.isEmpty())
            result.insert("description", componentDescription);

        return result;
    }
};

int DisplayHandler::parseOptions(const QString &programName,
                                 const QString &componentName,
                                 const QString &componentDisplay,
                                 const QString &componentDescription,
                                 QStringList &arguments,
                                 ComponentOptions &options,
                                 const QList<ComponentExample> &examples)
{
    Parser parser(programName, componentName, componentDisplay, componentDescription);
    try {
        parser.parse(arguments, options, examples);
    } catch (ArgumentParsingException ape) {
        if (ape.isError()) {
            if (!ape.getText().isEmpty()) {
#ifndef NO_CONSOLE
                TerminalOutput::simpleWordWrap(ape.getText(), true);
#else
                QMessageBox::critical(0, tr("Invalid Usage"), ape.getText());
#endif
            }
            QCoreApplication::exit(1);
            return -1;
        } else {
            if (!ape.getText().isEmpty()) {
#ifndef NO_CONSOLE
                TerminalOutput::simpleWordWrap(ape.getText());
#else
                QMessageBox::information(0, tr("Help"), ape.getText());
#endif
            }
            QCoreApplication::exit(0);
        }
        return 1;
    }

    return 0;
}

static QString getInvokedName(const QString &argument)
{
    QFileInfo file(argument);
    QString result(file.fileName());
    if (result.endsWith(".exe", Qt::CaseInsensitive))
        result.chop(4);
    if (!result.isEmpty())
        return result;
    return argument;
}

int DisplayHandler::parseArguments(QStringList arguments)
{
    QString typeName;
    QString programName(getInvokedName(arguments.first()));
    if (programName.compare("cpd3_display_handler", Qt::CaseInsensitive) == 0) {
        if (arguments.length() > 1) {
            if (arguments.at(1).startsWith(tr("--type=", "type switch"))) {
                programName = arguments.takeFirst();
                typeName = arguments.takeFirst().mid(tr("--type=", "type switch").length());

                if (typeName.startsWith("plot_", Qt::CaseInsensitive)) {
                    outputFile = QString(tr("output.png", "default output name"));
                    typeName = typeName.mid(5);
                } else if (typeName.startsWith("export_", Qt::CaseInsensitive)) {
                    outputFile = QString(tr("output.png", "default output name"));
                    typeName = typeName.mid(7);
                } else if (typeName.startsWith("show_", Qt::CaseInsensitive)) {
                    typeName = typeName.mid(5);
                }
            } else if (arguments.at(1) == tr("--help", "help switch") ||
                    arguments.at(1) == tr("help", "help switch")) {
                QString help
                        (tr("Usage: da.show.<type> [arguments]:\n   Or: da.plot.<type> [arguments]\n   Or: display_handler --type=<type> [arguments]\n   Or: display_handler <type> [arguments]\nUse da.show.<type> --help for type specific help.\n"));
#ifndef NO_CONSOLE
                TerminalOutput::output(help);
#else
                QMessageBox::information(0, tr("Help"), help);
#endif

                QCoreApplication::exit(0);
                return 1;
            } else {
                programName = arguments.takeFirst();
                typeName = arguments.takeFirst();
            }
        } else {
            QString help
                    (tr("Invalid usage: no type specified.\nAvailable types: scatter density timeseries cycle cdf pdf layout\n"));
#ifndef NO_CONSOLE
            TerminalOutput::output(help);
#else
            QMessageBox::critical(0, tr("Invalid Usage"), help);
#endif

            QCoreApplication::exit(1);
            return -1;
        }
    } else {
        arguments.removeFirst();

        typeName = programName.section('.', 2);
        if (typeName == programName)
            typeName = programName.section('_', 2);

        QString mode(programName.section('.', 1, 1));
        if (mode == programName)
            mode = programName.section('_', 1, 1);
        mode = mode.toLower();
        if (mode == tr("plot", "plot base mode name") ||
                mode == tr("export", "export base mode name"))
            outputFile = QString(tr("output.png", "default output name"));
    }
    typeName.replace(QString('.'), QString('_'));
    typeName = typeName.toLower();

    if (programName.isEmpty())
        programName = tr("da.show.%1").arg(typeName);

    DisplayComponent::DisplayType type = DisplayComponent::Display_Scatter2D;
    QString name;
    QString description;
    if (typeName == "scatter") {
        type = DisplayComponent::Display_Scatter2D;
        name = tr("Scatter plot");
        description = tr("A two dimensional scatter plot of values.");
    } else if (typeName == "density") {
        type = DisplayComponent::Display_Density2D;
        name = tr("Density plot");
        description = tr("A two dimensional scatter plot of point density.");
    } else if (typeName == "timeseries") {
        type = DisplayComponent::Display_TimeSeries2D;
        name = tr("Time series plot");
        description = tr("A two dimensional time series of values.");
    } else if (typeName == "cycle") {
        type = DisplayComponent::Display_Cycle2D;
        name = tr("Cycle plot");
        description = tr("A box-whisker plot of periodic cycles.");
    } else if (typeName == "cdf") {
        type = DisplayComponent::Display_CDF;
        name = tr("Cumulative Distribution");
        description = tr("A cumulative distribution plot.");
    } else if (typeName == "pdf") {
        type = DisplayComponent::Display_PDF;
        name = tr("Probability Distribution");
        description = tr("A probability distribution plot.");
    } else if (typeName == "allan") {
        type = DisplayComponent::Display_Allan;
        name = tr("Allan Plot");
        description = tr("A plot of standard deviation versus averaging time.");
    } else if (typeName == "layout") {
        type = DisplayComponent::Display_Layout;
        name = tr("Pre-defined layout");
        description = tr("An already defined layout of other plots.");
    } else {
        QString help
                (tr("Invalid type \"%1\" specified.\nAvailable types: scatter density timeseries cycle cdf pdf layout\n")
                         .arg(typeName));
#ifndef NO_CONSOLE
        TerminalOutput::output(help, true);
#else
        QMessageBox::critical(0, tr("Invalid Type"), help);
#endif

        QCoreApplication::exit(1);
        return -1;
    }

    ComponentOptions options(DisplayComponent::options(type));
    if (!outputFile.isEmpty()) {
        QStringList extensions;
        QList<QByteArray> rawExtensions(QImageWriter::supportedImageFormats());
        for (QList<QByteArray>::const_iterator add = rawExtensions.constBegin(),
                end = rawExtensions.constEnd(); add != end; ++add) {
            QString str(*add);
            if (str.isEmpty() || str.contains('%'))
                continue;
            extensions.append(str.toLower());
        }
        extensions.append("svg");
        std::sort(extensions.begin(), extensions.end());

        ComponentOptionFile *fileOption =
                new ComponentOptionFile(tr("file", "file option name"), tr("Output file name"),
                                        tr("This is the output image file to write to.  Supported "
                                           "formats: %1.").arg(
                                                extensions.join(tr(", ", "extension join"))),
                                        tr("output.png", "default output name"));
        fileOption->setMode(ComponentOptionFile::Write);
        fileOption->setTypeDescription(tr("Image files"));
        fileOption->setExtensions(QSet<QString>::fromList(extensions));
        options.add("file", fileOption);

        ComponentOptionSingleInteger *size =
                new ComponentOptionSingleInteger(tr("width", "width option name"),
                                                 tr("Output file width"),
                                                 tr("This is the width of the output file."),
                                                 tr("1024", "default output width"));
        size->setMinimum(1);
        size->setAllowUndefined(false);
        options.add("width", size);

        size = new ComponentOptionSingleInteger(tr("height", "height option name"),
                                                tr("Output file height"),
                                                tr("This is the height of the output file."),
                                                tr("768", "default output height"));
        size->setMinimum(1);
        size->setAllowUndefined(false);
        options.add("height", size);

        options.add("opaque", new ComponentOptionBoolean(tr("opaque", "opaque option name"),
                                                         tr("Opaque image output"),
                                                         tr("When enabled on an output format that supports transparency, "
                                                            "this causes the output image to have an opaque white "
                                                            "background instead of the normal transparent one."),
                                                         QString()));
    }

    if (int rc = parseOptions(programName, typeName, name, description, arguments, options,
                              DisplayComponent::examples(type))) {
        return rc;
    }

    if (!outputFile.isEmpty()) {
        if (options.isSet("file")) {
            ComponentOptionFile
                    *fileOption = qobject_cast<ComponentOptionFile *>(options.get("file"));
            if (fileOption) {
                outputFile = fileOption->get();
            }
        }
        if (options.isSet("width")) {
            ComponentOptionSingleInteger
                    *size = qobject_cast<ComponentOptionSingleInteger *>(options.get("width"));
            if (size) {
                outputWidth = (int) size->get();
            }
        }
        if (options.isSet("height")) {
            ComponentOptionSingleInteger
                    *size = qobject_cast<ComponentOptionSingleInteger *>(options.get("height"));
            if (size) {
                outputHeight = (int) size->get();
            }
        }
        if (options.isSet("opaque")) {
            ComponentOptionBoolean
                    *opaqueOption = qobject_cast<ComponentOptionBoolean *>(options.get("opaque"));
            if (opaqueOption) {
                fillOpaque = opaqueOption->get();
            }
        }
    }

    DisplayDefaults defaults;

#ifndef NO_CONSOLE
    if (arguments.isEmpty() ||
            (arguments.size() == 1 && arguments.at(0) == tr("-", "standard input file name"))) {
        if (!pipeline->setInputPipeline()) {
            TerminalOutput::simpleWordWrap(
                    tr("Error setting pipeline input: %1").arg(pipeline->getInputError()), true);
            QCoreApplication::exit(1);
            return -1;
        }
    } else
#endif
    if (arguments.size() == 1 && QFile::exists(arguments.at(0))) {
        if (!pipeline->setInputFile(arguments.at(0))) {
#ifndef NO_CONSOLE
            TerminalOutput::simpleWordWrap(
                    tr("Error setting input file: %1").arg(pipeline->getInputError()), true);
#else
            QMessageBox::critical(0, tr("Error in command line file"),
                tr("Error setting input file: %1").
                arg(pipeline->getInputError()));
#endif
            QCoreApplication::exit(1);
            return -1;
        }
    } else {
        Time::Bounds clip;
        Archive::Selection::List selections;
        try {
            selections = ArchiveParse::parse(arguments, &clip, nullptr, true);
        } catch (AcrhiveParsingException ape) {
#ifndef NO_CONSOLE
            TerminalOutput::simpleWordWrap(ape.getDescription(), true);
#else
            QMessageBox::critical(0, tr("Error in command line time"),
                ape.getDescription());
#endif
            QCoreApplication::exit(1);
            return -1;
        }
        chainBounds = clip;

        if (!selections.empty()) {
            if (selections.size() == 1) {
                chainStations = selections.front().stations;
                chainArchives = selections.front().archives;
                chainVariables = selections.front().variables;
            }
            if (selections.size() != 1 || !selections.front().variables.empty()) {
                chainSelections = selections;
            }
        }

        if (chainStations.size() == 1 &&
                QRegularExpression::escape(QString::fromStdString(chainStations.front())) ==
                        QString::fromStdString(chainStations.front())) {
            displayContext.setStation(chainStations.front());
        }

        if (chainArchives.size() == 1 &&
                QRegularExpression::escape(QString::fromStdString(chainArchives.front())) ==
                        QString::fromStdString(chainArchives.front())) {
            defaults.setArchive(chainArchives.front());
        } else {
            defaults.setArchive("raw");
        }

        if (!chainSelections.empty()) {
            if (!pipeline->setInputArchive(chainSelections, clip)) {
#ifndef NO_CONSOLE
                TerminalOutput::simpleWordWrap(
                        tr("Error setting input archive: %1").arg(pipeline->getInputError()), true);
#else
                QMessageBox::critical(0, tr("Error in command line time"),
                    tr("Error setting input archive: %1").
                    arg(pipeline->getInputError()));
#endif
                QCoreApplication::exit(1);
                return -1;
            }
        } else {
            requestPipelineInput = true;
        }
    }

    defaults.setSettings(settings);
    display.reset(DisplayComponent::create(type, options, defaults));
    baselineConfiguration = DisplayComponent::baselineConfiguration(type, options).read();

    return 0;
}
