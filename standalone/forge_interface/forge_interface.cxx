/*
 * Copyright (c) 2021 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTranslator>
#include <QCoreApplication>
#include <QStringList>
#include <QDataStream>
#include <QBuffer>

#include "core/number.hxx"
#include "datacore/archive/selection.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/externalsink.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "datacore/sequencematch.hxx"
#include "io/drivers/stdio.hxx"
#include "editing/editingengine.hxx"

using namespace CPD3;
using namespace CPD3::Data;

static double toTime(const QString &raw)
{
    if (raw.isEmpty()) {
        return FP::undefined();
    }
    return raw.toDouble();
}

static Archive::Selection toSelection(const QString &raw)
{
    Archive::Selection result;
    result.includeDefaultStation = false;
    result.includeMetaArchive = false;

    auto parts = raw.split(':');
    if (parts.size() < 3) {
        qFatal("Invalid data selection");
        return {};
    }
    result.stations = {parts.takeFirst().toStdString()};
    result.archives = {parts.takeFirst().toStdString()};

    auto variables = parts.takeFirst();
    if (!variables.isEmpty()) {
        result.variables = {variables.toStdString()};
    }

    for (const auto &flavor: parts) {
        if (flavor.startsWith('=')) {
            result.exactFlavors.emplace_back(flavor.mid(1).toStdString());
        } else if (flavor.startsWith('-')) {
            result.lacksFlavors.emplace_back(flavor.mid(1).toStdString());
        } else if (flavor.startsWith('+')) {
            result.hasFlavors.emplace_back(flavor.mid(1).toStdString());
        } else if (!flavor.isEmpty()) {
            result.hasFlavors.emplace_back(flavor.toStdString());
        }
    }

    return result;
}

static SequenceMatch::Element toSequenceMatch(const QString &raw)
{
    auto parts = raw.split(':');
    if (parts.size() < 3) {
        qFatal("Invalid data selection");
        return {};
    }

    auto station = parts.takeFirst();
    auto archive = parts.takeFirst();
    auto variable = parts.takeFirst();

    SequenceMatch::Element::PatternList hasFlavors;
    SequenceMatch::Element::PatternList lacksFlavors;
    SequenceName::Flavors exactFlavors;
    bool exactEmptyFlavors = false;
    for (const auto &flavor: parts) {
        if (flavor == "=") {
            exactEmptyFlavors = true;
            break;
        } else if (flavor.startsWith('=')) {
            exactFlavors.insert(flavor.mid(1).toStdString());
        } else if (flavor.startsWith('-')) {
            lacksFlavors.push_back(flavor.mid(1));
        } else if (flavor.startsWith('+')) {
            hasFlavors.push_back(flavor.mid(1));
        } else if (!flavor.isEmpty()) {
            hasFlavors.push_back(flavor);
        }
    }

    if (exactEmptyFlavors) {
        return SequenceMatch::Element(station, archive, variable, {});
    } else if (!exactFlavors.empty()) {
        return SequenceMatch::Element(station, archive, variable, exactFlavors);
    } else {
        return SequenceMatch::Element(station, archive, variable, hasFlavors, lacksFlavors);
    }
}

static void allocatePriority(SequenceIdentity &value, Archive::Access &access)
{
    bool hit = false;
    std::int_fast32_t maxHit = 0;
    std::int_fast32_t minHit = 0;
    {
        double loadStart = value.getStart();
        double loadEnd;
        if (!FP::defined(loadStart))
            loadEnd = 1.0;
        else
            loadEnd = loadStart + 1.0;

        StreamSink::Iterator data;
        access.readStream(Archive::Selection(value.getName(), loadStart, loadEnd), &data)->detach();
        while (data.hasNext()) {
            auto check = data.next();

            if (!FP::equal(check.getStart(), value.getStart()) ||
                    !FP::equal(check.getEnd(), value.getEnd()))
                continue;
            hit = true;

            maxHit = std::max<std::int_fast32_t>(maxHit, check.getPriority());
            minHit = std::min<std::int_fast32_t>(minHit, check.getPriority());
        }
    }

    if (!hit)
        return;

    if (maxHit < static_cast<std::int_fast32_t>(2147483647L)) {
        value.setPriority(maxHit + 1);
        return;
    }
    if (minHit > static_cast<std::int_fast32_t>(-2147483647L)) {
        value.setPriority(minHit - 1);
        return;
    }

    qWarning() << "Exhausted edit priorities trying to write" << value;

    double loadStart = value.getStart();
    double loadEnd;
    if (!FP::defined(loadStart))
        loadEnd = 1.0;
    else
        loadEnd = loadStart + 1.0;

    /* Stupid, but this should never happen either */
    for (qint32 p = 1; p != 0; p++) {
        hit = false;

        StreamSink::Iterator data;
        auto reader =
                access.readStream(Archive::Selection(value.getName(), loadStart, loadEnd), &data);
        while (data.hasNext()) {
            auto check = data.next();

            if (!FP::equal(check.getStart(), value.getStart()) ||
                    !FP::equal(check.getEnd(), value.getEnd()))
                continue;
            if (check.getPriority() != p)
                continue;
            hit = true;
            break;
        }
        reader->signalTerminate();
        reader->wait();
        if (hit)
            continue;

        value.setPriority(p);
        return;
    }

    qFatal("No possible priority for directive");
}


int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    auto args = app.arguments();
    if (args.size() < 2) {
        qWarning("No operation provided");
        return 1;
    }
    args.removeFirst();

    auto op = args.takeFirst().toLower();
    if (op == "archive_read") {
        if (args.size() < 3) {
            qWarning("Start, end, and at least one selection required");
            return 2;
        }
        auto start = toTime(args.takeFirst());
        auto end = toTime(args.takeFirst());
        Archive::Selection::List selections;
        for (const auto &arg: args) {
            selections.emplace_back(toSelection(arg));
            selections.back().start = start;
            selections.back().end = end;
        }

        Archive::Access access;
        auto of = IO::Access::stdio_out();
        StandardDataOutput output(of->stream(), StandardDataOutput::OutputType::Raw);
        auto reader = access.readStream(selections, &output);
        output.start();
        output.wait();
        of.reset();
        reader->wait();
        reader.reset();
        return 0;
    } else if (op == "directive_read") {
        if (args.size() < 3) {
            qWarning("Start, end, and station required");
            return 2;
        }
        auto start = toTime(args.takeFirst());
        auto end = toTime(args.takeFirst());
        auto station = args.takeFirst().toStdString();

        Archive::Selection selection(start, end, {station}, {"edits"});
        selection.includeMetaArchive = false;

        Archive::Access access;
        auto of = IO::Access::stdio_out();
        StandardDataOutput output(of->stream(), StandardDataOutput::OutputType::Raw);
        auto reader = access.readStream(selection, &output);
        output.start();
        output.wait();
        of.reset();
        reader->wait();
        reader.reset();
        return 0;
    } else if (op == "directive_create") {
        SequenceIdentity identity;
        Variant::Root contents;
        {
            auto in = IO::Access::stdio_in()->qioStream();
            in->open(QIODevice::ReadOnly);
            QDataStream stream(in.get());
            stream.setByteOrder(QDataStream::LittleEndian);
            stream.setVersion(QDataStream::Qt_4_5);
            stream >> identity;
            stream >> contents;
        }

        if (identity.getArchive() != "edits" || contents.read().getType() != Variant::Type::Hash) {
            qWarning("Invalid directive");
            return 2;
        }

        {
            Archive::Access access;
            for (;;) {
                Archive::Access::WriteLock lock(access);

                identity.setPriority(0);
                allocatePriority(identity, access);
                access.writeSynchronous({SequenceValue(identity, contents)});

                if (lock.commit())
                    break;
            }
        }

        {
            auto out = IO::Access::stdio_out()->qioStream();
            out->open(QIODevice::WriteOnly);
            QDataStream stream(out.get());
            stream.setByteOrder(QDataStream::LittleEndian);
            stream.setVersion(QDataStream::Qt_4_5);
            stream << identity;
        }
        return 0;
    } else if (op == "directive_rmw") {
        Archive::Access access;
        Archive::Access::WriteLock lock(access);

        auto in = IO::Access::stdio_in()->qioStream();
        in->open(QIODevice::ReadOnly);
        QDataStream input(in.get());
        input.setByteOrder(QDataStream::LittleEndian);
        input.setVersion(QDataStream::Qt_4_5);

        SequenceIdentity original;
        input >> original;

        auto out = IO::Access::stdio_out()->qioStream();
        out->open(QIODevice::WriteOnly);
        QDataStream output(out.get());
        output.setByteOrder(QDataStream::LittleEndian);
        output.setVersion(QDataStream::Qt_4_5);

        {
            double loadStart = original.getStart();
            double loadEnd;
            if (!FP::defined(loadStart))
                loadEnd = 1.0;
            else
                loadEnd = loadStart + 1.0;

            bool hit = false;

            StreamSink::Iterator data;
            auto reader =
                    access.readStream(Archive::Selection(original.getName(), loadStart, loadEnd),
                                      &data);
            while (data.hasNext()) {
                auto check = data.next();
                if (check.getIdentity() != original)
                    continue;

                hit = true;

                QByteArray outData;
                {
                    QBuffer outBuffer(&outData);
                    outBuffer.open(QIODevice::WriteOnly);
                    QDataStream outStream(&outBuffer);
                    outStream.setByteOrder(QDataStream::LittleEndian);
                    outStream.setVersion(QDataStream::Qt_4_5);
                    outStream << check.root();
                }

                output << outData;
                break;
            }
            reader->signalTerminate();
            reader->wait();

            if (!hit) {
                qWarning("No edit directive found to modify");
                return 4;
            }
        }

        SequenceIdentity modified;
        Variant::Root contents;
        input >> modified;
        input >> contents;

        ArchiveErasure::Transfer toRemove;
        if (modified != original) {
            toRemove.emplace_back(original);
            modified.setPriority(0);
            allocatePriority(modified, access);
        }

        access.writeSynchronous({SequenceValue(modified, contents)}, toRemove);

        if (!lock.commit())
            return 100;

        output << modified;

        return 0;
    } else if (op == "edited_read") {
        if (args.size() < 4) {
            qWarning("Start, end, station, and profile required");
            return 5;
        }
        auto start = toTime(args.takeFirst());
        auto end = toTime(args.takeFirst());
        auto station = args.takeFirst().toStdString();
        auto profile = args.takeFirst();

        Archive::Access access;
        Archive::Access::ReadLock lock(access);

        Editing::EditingEngine engine(start, end, station, profile, &access);
        engine.start();

        auto of = IO::Access::stdio_out();
        StandardDataOutput output(of->stream(), StandardDataOutput::OutputType::Raw);
        output.start();
        engine.setEgress(&output);
        output.wait();
        of.reset();
        engine.wait();
        return 0;
    } else if (op == "edited_available") {
        if (args.size() < 4) {
            qWarning("Start, end, station, and profile required");
            return 5;
        }
        auto start = toTime(args.takeFirst());
        auto end = toTime(args.takeFirst());
        auto station = args.takeFirst().toStdString();
        auto profile = args.takeFirst();

        SequenceName::ComponentSet variables;
        {
            Archive::Access access;
            Archive::Access::ReadLock lock(access);

            auto config = ValueSegment::Stream::read(
                    Archive::Selection(start, end, {station}, {"configuration"}, {"editing"}),
                    &access);

            Archive::Selection::List inputSelections;
            SequenceName::ComponentSet additionalVariables;
            for (const auto &segment: config) {
                auto cfg = segment.getValue().hash("Profiles").hash(profile);
                Util::append(
                        SequenceMatch::Composite(cfg["Output/Data"]).toArchiveSelections({station},
                                                                                         {"raw"}),
                        inputSelections);
                Util::append(
                        SequenceMatch::Composite(cfg["Input/Data"]).toArchiveSelections({station},
                                                                                        {"raw"}),
                        inputSelections);
                Util::append(
                        SequenceMatch::Composite(cfg["Display/Data"]).toArchiveSelections({station},
                                                                                          {"raw"}),
                        inputSelections);
                for (const auto &add: cfg["Display/AdditionalVariables"].toFlags()) {
                    additionalVariables.insert(add);
                }
            }

            for (auto &sel: inputSelections) {
                sel.start = start;
                sel.end = end;
            }

            if (inputSelections.empty()) {
                variables = access.availableVariables();
            } else {
                auto selected = access.availableSelected(inputSelections);
                variables = std::move(selected.variables);
                if (variables.empty()) {
                    variables = access.availableVariables();
                }
            }
            Util::merge(std::move(additionalVariables), variables);
        }

        auto of = IO::Access::stdio_out();
        auto output = of->stream();
        for (const auto &var: variables) {
            output->write(var + "\n");
        }

        output.reset();
        of.reset();
        return 0;
    } else if (op == "pass_data") {
        if (args.size() < 4) {
            qWarning("Start, end, station, and profile required");
            return 6;
        }
        auto start = toTime(args.takeFirst());
        auto end = toTime(args.takeFirst());
        auto station = args.takeFirst().toStdString();
        auto profile = args.takeFirst();

        auto in = IO::Access::stdio_in()->qioStream();
        in->open(QIODevice::ReadOnly);
        QDataStream input(in.get());
        input.setByteOrder(QDataStream::LittleEndian);
        input.setVersion(QDataStream::Qt_4_5);

        std::string comment;
        input >> comment;

        auto component = qobject_cast<ActionComponentTime *>(ComponentLoader::create("pass"));
        Q_ASSERT(component != nullptr);
        auto options = component->getOptions();

        qobject_cast<ComponentOptionBoolean *>(options.get("noupdate"))->set(true);
        qobject_cast<ComponentOptionSingleString *>(options.get("profile"))->set(profile);
        if (!comment.empty()) {
            qobject_cast<ComponentOptionSingleString *>(options.get("comment"))->set(
                    QString::fromStdString(comment));
        }

        auto action = component->createTimeAction(options, start, end, {station});
        Q_ASSERT(action != nullptr);

        action->start();
        action->wait();
        return 0;
    } else if (op == "pass_update") {
        if (args.size() < 2) {
            qWarning("Station and profile required");
            return 7;
        }
        auto station = args.takeFirst().toStdString();
        auto profile = args.takeFirst();

        auto component =
                qobject_cast<ActionComponentTime *>(ComponentLoader::create("update_passed"));
        Q_ASSERT(component != nullptr);
        auto options = component->getOptions();

        qobject_cast<ComponentOptionBoolean *>(options.get("detached"))->set(true);
        qobject_cast<ComponentOptionStringSet *>(options.get("profile"))->add(profile);

        auto action = component->createTimeAction(options, {station});
        Q_ASSERT(action != nullptr);

        action->start();
        action->wait();
        return 0;
    } else if (op == "filter") {
        if (args.size() < 1) {
            qWarning("At least one selection required");
            return 2;
        }
        SequenceMatch::Composite matcher;
        for (const auto &arg: args) {
            matcher.append(toSequenceMatch(arg));
        }

        class Filter final : public StreamSink {
            SequenceMatch::Composite matcher;
            StreamSink &next;
            SequenceName::Map<bool> accept;

            bool acceptName(const SequenceName &name)
            {
                auto check = accept.find(name);
                if (check != accept.end())
                    return check->second;

                bool result = matcher.matches(name);
                accept.emplace(name, result);
                return result;
            }

        public:
            Filter(SequenceMatch::Composite &&matcher, StreamSink &next) : matcher(
                    std::move(matcher)), next(next)
            { }

            virtual ~Filter() = default;

            void incomingData(const SequenceValue::Transfer &values) override
            {
                SequenceValue::Transfer passed;
                for (const auto &check: values) {
                    if (!acceptName(check.getName()))
                        continue;
                    passed.emplace_back(check);
                }
                if (passed.empty())
                    return;
                next.incomingData(std::move(passed));
            }

            void incomingData(SequenceValue::Transfer &&values) override
            {
                SequenceValue::Transfer passed;
                for (auto &check: values) {
                    if (!acceptName(check.getName()))
                        continue;
                    passed.emplace_back(std::move(check));
                }
                if (passed.empty())
                    return;
                next.incomingData(std::move(passed));
            }

            void incomingData(const SequenceValue &value) override
            {
                if (!acceptName(value.getName()))
                    return;
                next.incomingData(value);
            }

            void incomingData(SequenceValue &&value) override
            {
                if (!acceptName(value.getName()))
                    return;
                next.incomingData(std::move(value));
            }

            void endData() override
            { next.endData(); }
        };

        Archive::Access access;
        auto out = IO::Access::stdio_out();

        StandardDataOutput output(out->stream(), StandardDataOutput::OutputType::Raw);
        Filter filter(std::move(matcher), output);
        StandardDataInput input;
        input.start();
        output.start();
        input.setEgress(&filter);

        auto in = IO::Access::stdio_in()->stream();
        in->read.connect([&](const Util::ByteArray &data) {
            input.incomingData(data);
        });
        in->ended.connect([&]() {
            input.endData();
        });
        in->start();

        input.wait();
        in.reset();

        output.wait();
        out.reset();
        return 0;
    } else if (op == "export") {
        if (args.size() < 4) {
            qWarning("Mode, start, end, and at least one selection required");
            return 2;
        }
        auto mode = args.takeFirst().toLower();
        auto start = toTime(args.takeFirst());
        auto end = toTime(args.takeFirst());
        Archive::Selection::List selections;
        for (const auto &arg: args) {
            selections.emplace_back(toSelection(arg));
            selections.back().start = start;
            selections.back().end = end;
            selections.back().includeDefaultStation = true;
            selections.back().includeMetaArchive = true;
        }

        auto component = qobject_cast<ExternalSinkComponent *>(ComponentLoader::create("export"));
        Q_ASSERT(component != nullptr);
        auto options = component->getOptions();

        qobject_cast<ComponentOptionBoolean *>(options.get("stddev"))->set(false);
        qobject_cast<ComponentOptionBoolean *>(options.get("count"))->set(false);
        qobject_cast<ComponentOptionBoolean *>(options.get("header-description"))->set(true);
        qobject_cast<TimeIntervalSelectionOption *>(options.get("squash"))->set(Time::LogicalTimeUnit::Minute, 1, true);
        if (mode == "basic") {
            qobject_cast<ComponentOptionBoolean *>(options.get("cut-split"))->set(false);
            qobject_cast<ComponentOptionBoolean *>(options.get("cut-string"))->set(true);
        } else if (mode == "average") {
            qobject_cast<ComponentOptionBoolean *>(options.get("cut-split"))->set(true);
            qobject_cast<TimeIntervalSelectionOption *>(options.get("squash"))->set(Time::LogicalTimeUnit::Hour, 1, true);
            qobject_cast<TimeIntervalSelectionOption *>(options.get("fill"))->set(Time::LogicalTimeUnit::Hour, 1, true);
        } else if (mode == "unsplit") {
            qobject_cast<ComponentOptionBoolean *>(options.get("cut-split"))->set(false);
        }

        Archive::Access access;
        auto of = IO::Access::stdio_out();
        std::unique_ptr<ExternalSink> output(component->createDataSink(of->stream(), options));
        auto reader = access.readStream(selections, output.get());
        output->start();
        output->wait();
        of.reset();
        reader->wait();
        reader.reset();
        return 0;
    } else if (op == "lossless_read") {
        Archive::Selection::List selections;
        {
            auto in = IO::Access::stdio_in()->qioStream();
            in->open(QIODevice::ReadOnly);
            QDataStream stream(in.get());
            stream.setByteOrder(QDataStream::LittleEndian);
            stream.setVersion(QDataStream::Qt_4_5);
            for (;;) {
                Archive::Selection sel;
                stream >> sel;
                if (stream.status() != QDataStream::Ok) {
                    break;
                }
                selections.emplace_back(std::move(sel));
            }
        }

        Archive::Access access;
        ArchiveSink::Iterator data;

        auto reader = access.readArchive(selections, &data);
        {
            auto out = IO::Access::stdio_out()->qioStream();
            out->open(QIODevice::WriteOnly);

            QDataStream stream(out.get());
            stream.setByteOrder(QDataStream::LittleEndian);
            stream.setVersion(QDataStream::Qt_4_5);
            while (data.hasNext()) {
                auto value = data.next();
                stream << value;
            }
        }

        reader->wait();
        reader.reset();
        return 0;
    }

    qWarning("Unknown operation");
    return 99;
}

