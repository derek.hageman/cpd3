/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#ifdef HAVE_SD_NOTIFY
#include <systemd/sd-daemon.h>
#endif

#include <QTranslator>
#include <QLibraryInfo>
#include <QCoreApplication>
#include <QTimer>
#include <QLoggingCategory>

#include "sync/server.hxx"
#include "core/abort.hxx"
#include "core/threadpool.hxx"

#include "server.hxx"


Q_LOGGING_CATEGORY(log_serverunix, "cpd3.syncserver", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Sync;

static int startupDoneFD = -1;

static void sendStartup(quint8 byte)
{
    if (startupDoneFD == -1)
        return;
    for (; ;) {
        ssize_t n = ::write(startupDoneFD, &byte, 1);
        if (n == -1) {
            if (errno != EAGAIN && errno != EINTR && errno != EWOULDBLOCK) {
                qCDebug(log_serverunix) << "Failed to write to startup notification pipe:" <<
                                        ::strerror(errno);
            }
            break;
        }
        if (n > 0)
            break;
    }
    ::close(startupDoneFD);
    startupDoneFD = -1;
}

static void doDaemonize(int argc, char **argv)
{
    char *pidFile = NULL;
    if (argc > 1) {
        for (char **arg = argv + 1, **endArg = argv + argc; arg != endArg; ++arg) {
            char *str = *arg;
            if (::QObject::tr("--no-daemonize") == str ||
                    ::QObject::tr("--no-fork") == str ||
                    ::QObject::tr("-n") == str)
                return;
            QByteArray checkData(::QObject::tr("--pid=").toUtf8());
            if (!strncmp(checkData.constData(), str, checkData.size())) {
                pidFile = str + checkData.size();
                if (*pidFile == '\0')
                    pidFile = NULL;
            }
        }
    }

    int pipeFDs[2];
    if (::pipe(pipeFDs) == -1) {
        ::fprintf(stderr, "Can't create signal pipe: %s\n", ::strerror(errno));
        ::exit(1);
        return;
    }

    pid_t pid = ::fork();
    if (pid == -1) {
        ::fprintf(stderr, "Unable to fork: %s\n", ::strerror(errno));
        ::exit(2);
        return;
    }
    if (pid == 0) {
        ::close(pipeFDs[0]);
        startupDoneFD = pipeFDs[1];

        /* Child, so daemonize. */
        if (::setsid() == -1) {
            ::fprintf(stderr, "Can't create new process group: %s\n", ::strerror(errno));
            ::exit(3);
        }
        if (::chdir("/") == -1) {
            ::fprintf(stderr, "Can't change to root directory: %s\n", ::strerror(errno));
            ::exit(4);
        }

        int fd;
        if ((fd = ::open("/dev/null", O_RDONLY)) == -1) {
            ::fprintf(stderr, "Can't open /dev/null for stdin: %s\n", ::strerror(errno));
            ::exit(5);
        }
        if (::dup2(fd, 0) == -1) {
            ::fprintf(stderr, "Can't dup2 stdin: %s\n", ::strerror(errno));
            ::exit(6);
        }
        ::close(fd);
        if ((fd = ::open("/dev/null", O_WRONLY)) == -1) {
            ::fprintf(stderr, "Can't open /dev/null for stdout: %s\n", ::strerror(errno));
            ::exit(7);
        }
        if (::dup2(fd, 1) == -1) {
            ::fprintf(stderr, "Can't dup2 stdout: %s\n", ::strerror(errno));
            ::exit(9);
        }
        ::close(fd);
        if ((fd = ::open("/dev/null", O_WRONLY)) == -1) {
            ::fprintf(stderr, "Can't open /dev/null for stderr: %s\n", ::strerror(errno));
            ::exit(10);
        }
        if (::dup2(fd, 2) == -1) {
            ::fprintf(stderr, "Can't dup2 stderr: %s\n", ::strerror(errno));
            ::exit(11);
        }
        ::close(fd);

        return;
    }

    ::close(pipeFDs[1]);
    int waitFD = pipeFDs[0];
    for (; ;) {
        char bfr[1];
        ssize_t n = ::read(waitFD, bfr, sizeof(bfr));
        if (n > 0) {
            ::close(waitFD);

            if (bfr[0] == 0) {
                ::fprintf(stderr, "%s\n",
                          ::QObject::tr("Server start up failed.").toLatin1().data());
                ::exit(1);
                return;
            }

            if (pidFile != NULL) {
                FILE *outFile = ::fopen(pidFile, "w");
                if (outFile != NULL) {
                    fprintf(outFile, "%u", (unsigned int) pid);
                    ::fclose(outFile);
                } else {
                    char *er = ::strerror(errno);
                    ::fprintf(stderr, "%s\n",
                              ::QObject::tr("Unable to open PID file \"%1\" for writing: %2").arg(
                                      pidFile).arg(er).toLatin1().data());
                }
            }

            ::exit(0);
            return;
        }
        if (n == -1) {
            ::fprintf(stderr, "Read on signal pipe failed: %s\n", ::strerror(errno));
            ::close(waitFD);
            ::exit(12);
            return;
        }
    }
}

UnixServerHandler::UnixServerHandler()
{ }

void UnixServerHandler::heartbeat()
{
#ifdef HAVE_SD_NOTIFY
    sd_notify(0, "WATCHDOG=1");
#endif
}

int main(int argc, char **argv)
{
    doDaemonize(argc, argv);

    QCoreApplication app(argc, argv);

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

    QString translateLocation;
    QByteArray cpd3(qgetenv("CPD3"));
    if (cpd3.length() > 0) {
        translateLocation = QString(cpd3) + "/locale";
    }
    QTranslator cpd3Translator;
    cpd3Translator.load("cpd3_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cpd3Translator);
    QTranslator cliTranslator;
    cliTranslator.load("cli_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cliTranslator);
    QTranslator acquisitionTranslator;
    acquisitionTranslator.load("sync_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&acquisitionTranslator);

    Abort::installAbortHandler(true);

    Server *server = new Server;
    AbortPoller abortPoller;

    QObject::connect(&abortPoller, SIGNAL(aborted()), server, SLOT(signalTerminate()));
    QObject::connect(server, SIGNAL(finished()), &app, SLOT(quit()));

    abortPoller.start();
    if (!server->start()) {
        delete server;
        sendStartup(0);
        return 1;
    }

#ifdef HAVE_SD_NOTIFY
    sd_notify(0, "READY=1");
#endif

    UnixServerHandler handler;
    QTimer *heartbeatTimer = new QTimer(server);
    QObject::connect(heartbeatTimer, SIGNAL(timeout()), &handler, SLOT(heartbeat()));
    heartbeatTimer->setSingleShot(false);
    heartbeatTimer->start(5000);

    sendStartup(1);
    int rc = app.exec();

#ifdef HAVE_SD_NOTIFY
    sd_notify(0, "STOPPING=1");
#endif

    heartbeatTimer->disconnect();
    abortPoller.disconnect();
    delete server;
    ThreadPool::system()->wait();
    return rc;
}
