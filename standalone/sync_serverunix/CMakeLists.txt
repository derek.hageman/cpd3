cpd3_standalone(cpd3_syncserver server.cxx server.hxx)

target_link_libraries(cpd3_syncserver cpd3clicore cpd3sync)

if(HAVE_SD_NOTIFY)
    target_link_libraries(cpd3_syncserver ${SYSTEMD_LIBRARY})
endif(HAVE_SD_NOTIFY)

cpd3_symlink(cpd3_syncserver cpd3.sync.server) 

if(UNIX)
    add_subdirectory(test)
endif(UNIX)
