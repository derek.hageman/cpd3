/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3_SERVER_HXX
#define CPD3_SERVER_HXX

#include "core/first.hxx"

#include <QObject>

class UnixServerHandler : public QObject {
Q_OBJECT

public:
    UnixServerHandler();

public slots:

    void heartbeat();
};

#endif //CPD3_SERVER_HXX
