/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#ifdef Q_OS_UNIX

#include <unistd.h>

#endif

#include "core/timeutils.hxx"
#include "io.hxx"

using namespace CPD3;

TCP::Control::Control() : port(0)
{ }

InputSource::Control::Control() : linefeedTranslation(LinefeedTranslation::LF)
{ }

InputSource::FileControl::FileControl() : textMode(true)
{ }

InputSource::StandardInputControl::StandardInputControl()
{
    linefeedTranslation = LinefeedTranslation::CR;
}

Pty::Control::Input::Input()
{
    linefeedTranslation = LinefeedTranslation::CR;
}

TCP::Control::Input::Input()
{
    linefeedTranslation = LinefeedTranslation::CR;
}

OutputTarget::Control::Control() : condenseNewlines(false),
                                   enableOriginIndicator(false),
                                   displayEcho(false),
                                   hideUnprintable(false)
{ }

OutputTarget::FileControl::FileControl() = default;

OutputTarget::InteractiveControl::InteractiveControl() : enableANSI(true)
{
    condenseNewlines = true;
    displayEcho = true;
    hideUnprintable = true;
}

OutputTarget::StandardOutputControl::StandardOutputControl()
{
    displayEcho = false;
#ifdef Q_OS_UNIX
    if (!::isatty(1)) {
        enableANSI = false;
    }
#endif
}

Pty::Control::Output::Output() = default;

TCP::Control::Output::Output()
{
    enableANSI = false;
}

InputSource::InputSource() = default;

InputSource::~InputSource() = default;

OutputTarget::OutputTarget() = default;

OutputTarget::~OutputTarget() = default;

void OutputTarget::incomingPeriodic()
{ }


OutputTarget::FormatFilter::FormatFilter(const OutputTarget::Control &control,
                                         bool enableANSI,
                                         const QByteArray &newlineSequence)
        : packetStart(Time::time()),
          packetOrigin(PacketOrigin::None), lastNewlineOrigin(PacketOrigin::None),
          bypass(true),
          enableANSI(enableANSI), newlineSequence(newlineSequence),
          control(control)
{
    if (control.hideUnprintable ||
            control.enableOriginIndicator ||
            control.condenseNewlines ||
            enableANSI) {
        bypass = false;
    }
}

OutputTarget::FormatFilter::~FormatFilter() = default;

static QByteArray formatANSIEscape(const char *parameters)
{
    QByteArray result;
    result += static_cast<char>(0x1B);
    result += '[';
    result += parameters;
    return result;
}

QByteArray OutputTarget::FormatFilter::incomingData(const QByteArray &contents)
{
    if (bypass)
        return contents;
    auto result = maybeFlush();
    if (contents.isEmpty())
        return result;
    if (packetOrigin != PacketOrigin::Data) {
        result += flushExisting();
        packetStart = Time::time();
        packetOrigin = PacketOrigin::Data;
    }

    result += addContents(contents, "m", '>');
    return result;
}

QByteArray OutputTarget::FormatFilter::incomingControl(const QByteArray &contents)
{
    if (bypass)
        return contents;
    auto result = maybeFlush();
    if (contents.isEmpty())
        return result;
    if (packetOrigin != PacketOrigin::Control) {
        result += flushExisting();
        packetStart = Time::time();
        packetOrigin = PacketOrigin::Control;
    }

    result += addContents(contents, "1m", '<');
    return result;
}

QByteArray OutputTarget::FormatFilter::incomingLocal(const QByteArray &contents)
{
    if (!control.displayEcho)
        return {};
    if (bypass)
        return contents;
    auto result = maybeFlush();
    if (contents.isEmpty())
        return result;
    if (packetOrigin != PacketOrigin::Local) {
        result += flushExisting();
        packetStart = Time::time();
        packetOrigin = PacketOrigin::Local;
    }

    result += addContents(contents, "4m", '=');
    return result;
}

QByteArray OutputTarget::FormatFilter::addContents(const QByteArray &contents,
                                                   const char *displaySequence,
                                                   char directionCode)
{
    QByteArray result;

    bool emitStart = control.enableOriginIndicator && packetContents.isEmpty();

    if (enableANSI) {
        packetContents += formatANSIEscape(displaySequence);
    }

    for (auto add : contents) {
        if (emitStart) {
            emitStart = false;
            result += startPacket(directionCode);
            lastNewlineOrigin = PacketOrigin::None;
        }
        if (isNewline(add)) {
            bool wasNewline = (lastNewlineOrigin == packetOrigin);
            lastNewlineOrigin = packetOrigin;
            if (control.condenseNewlines) {
                if (!wasNewline) {
                    packetContents += newlineSequence;
                }
            } else {
                packetContents += add;
                emitStart = true;
            }
            continue;
        } else {
            bool wasNewline = (lastNewlineOrigin == packetOrigin);
            lastNewlineOrigin = PacketOrigin::None;
            if (control.condenseNewlines && wasNewline) {
                emitStart = false;
                result += startPacket(directionCode);
            }
        }

        if (control.hideUnprintable && isUnprintable(add)) {
            if (enableANSI) {
                packetContents += formatANSIEscape("7m");
            }
            packetContents += '.';
            if (enableANSI) {
                packetContents += formatANSIEscape("27m");
            }
            continue;
        }

        packetContents += add;
    }

    if (!control.enableOriginIndicator) {
        result += flushExisting();
    }

    return result;
}

QByteArray OutputTarget::FormatFilter::incomingPeriodic()
{ return maybeFlush(); }

QByteArray OutputTarget::FormatFilter::finish()
{ return flushExisting(); }

QByteArray OutputTarget::FormatFilter::maybeFlush()
{
    if (packetOrigin == PacketOrigin::None)
        return {};
    if (packetContents.isEmpty())
        return {};

    if ((Time::time() - packetStart) > 0.1) {
        return flushExisting();
    }

    return {};
}

QByteArray OutputTarget::FormatFilter::flushExisting()
{
    auto result = std::move(packetContents);
    if (!result.isEmpty()) {
        if (control.enableOriginIndicator && !isNewline(result[result.size() - 1])) {
            result += newlineSequence;
        }
        if (enableANSI) {
            result += formatANSIEscape("m");
        }
    }

    packetContents.clear();
    packetOrigin = PacketOrigin::None;
    return result;
}

QByteArray OutputTarget::FormatFilter::startPacket(char directionCode)
{
    if (!control.enableOriginIndicator)
        return {};

    packetStart = Time::time();
    auto result = std::move(packetContents);
    packetContents = Time::toISO8601(packetStart, true).toLatin1();
    packetContents += directionCode;
    packetContents += ' ';
    return result;
}

bool OutputTarget::FormatFilter::isNewline(char test)
{
    switch (test) {
    case '\r':
    case '\n':
        return true;
    default:
        break;
    }
    return false;
}

bool OutputTarget::FormatFilter::isUnprintable(char test)
{
    return false;
}
