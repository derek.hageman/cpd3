/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <memory>
#include <QTcpServer>
#include <QTcpSocket>
#include <QLoggingCategory>

#include "clicore/terminaloutput.hxx"
#include "io.hxx"

Q_DECLARE_LOGGING_CATEGORY(log_eavesdropper)

using namespace CPD3;

namespace TCP {

namespace {

class Server {
    std::unique_ptr<QTcpServer> backend;
    Threading::Signal<> pruneConnections;

    class Connection {
        std::unique_ptr<QTcpSocket> socket;
        Threading::Signal<> pruneConnections;

        void readChunk()
        {
            if (!socket)
                return;

            qint64 n = socket->bytesAvailable();
            if (n > 0) {
                dataReady(Util::ByteArray(socket->read(std::min(static_cast<qint64>(65536), n))));
                if (socket->bytesAvailable() > 0) {
                    Threading::runQueuedFunctor(socket.get(),
                                                std::bind(&Connection::readChunk, this));
                    return;
                }
            }
            if (socket->state() != QAbstractSocket::UnconnectedState)
                return;

            qCDebug(log_eavesdropper) << "Socket closed";

            socket->disconnect();
            socket->close();
            socket.reset();
            return pruneConnections();
        }

    public:
        Connection(Server &server, std::unique_ptr<QTcpSocket> &&incoming) : socket(
                std::move(incoming)), pruneConnections(server.pruneConnections)
        {
            QObject::connect(socket.get(), &QIODevice::readyRead, socket.get(),
                             std::bind(&Connection::readChunk, this), Qt::QueuedConnection);
            QObject::connect(socket.get(), &QIODevice::readChannelFinished, socket.get(),
                             std::bind(&Connection::readChunk, this), Qt::QueuedConnection);
            QObject::connect(socket.get(), &QAbstractSocket::stateChanged, socket.get(),
                             std::bind(&Connection::readChunk, this), Qt::QueuedConnection);

            qCDebug(log_eavesdropper) << "Accepted connection from" << socket->peerName();

            readChunk();
        }

        ~Connection()
        {
            if (socket) {
                socket->disconnect();
                socket->close();
                socket.reset();
            }
        }

        void write(const QByteArray &incoming)
        {
            if (!socket)
                return;
            if (incoming.isEmpty())
                return;
            const char *data = static_cast<const char *>(incoming.data());
            qint64 size = incoming.size();
            while (size > 0) {
                qint64 n = socket->write(data, size);
                if (n < 0) {
                    qCDebug(log_eavesdropper) << "Error writing to socket:"
                                              << socket->errorString();
                    socket->disconnect();
                    socket.reset();
                    pruneConnections();
                    return;
                }

                Q_ASSERT(n <= size);
                data += n;
                size -= n;
            }

            socket->flush();
        }

        bool doPrune()
        {
            if (!socket)
                return true;
            return socket->state() == QAbstractSocket::UnconnectedState;
        }

        Threading::Signal<Util::ByteArray> dataReady;
    };

    friend class Connection;

    std::vector<std::unique_ptr<Connection>> connections;

    void addConnection(QTcpSocket *add)
    {
        std::unique_ptr<Connection>
                connection(new Connection(*this, std::unique_ptr<QTcpSocket>(add)));
        connection->dataReady.connect([this](Util::ByteArray data) { dataReady(std::move(data)); });
        if (connection->doPrune())
            return;
        connections.emplace_back(std::move(connection));
    }

    void acceptConnections()
    {
        while (auto add = backend->nextPendingConnection()) {
            addConnection(add);
        }
    }

public:
    explicit Server(std::unique_ptr<QTcpServer> &&server) : backend(std::move(server))
    {
        QObject::connect(backend.get(), &QTcpServer::newConnection, backend.get(),
                         std::bind(&Server::acceptConnections, this), Qt::QueuedConnection);

        qCDebug(log_eavesdropper) << "Listening for connections on port" << backend->serverPort();

        pruneConnections.connect(backend.get(), [this]() {
            for (auto check = connections.begin(); check != connections.end();) {
                if (!(*check)->doPrune()) {
                    ++check;
                    continue;
                }
                check = connections.erase(check);
            }
        }, true);

        acceptConnections();
    }

    ~Server()
    {
        connections.clear();
        backend->close();
        backend.reset();
    }

    Threading::Signal<Util::ByteArray> dataReady;

    void write(const QByteArray &data)
    {
        /* Only called from the main event loop, so lock free is safe */
        for (const auto &c : connections) {
            c->write(data);
        }
    }
};

class OutputWriter : public OutputTarget {
    std::shared_ptr<Server> server;
    FormatFilter filter;
public:
    OutputWriter(std::shared_ptr<Server> server, const TCP::Control::Output &control) : server(
            std::move(server)), filter(control, control.enableANSI)
    { }

    virtual ~OutputWriter() = default;

    void incomingData(const QByteArray &contents) override
    { server->write(filter.incomingData(contents)); }

    void incomingControl(const QByteArray &contents) override
    { server->write(filter.incomingControl(contents)); }

    void incomingLocal(const QByteArray &contents) override
    { server->write(filter.incomingLocal(contents)); }

    void incomingPeriodic() override
    { server->write(filter.incomingPeriodic()); }
};

class InputReader : public InputSource {
    std::shared_ptr<Server> server;
    Threading::Receiver receiver;
    Control::LinefeedTranslation linefeedTranslation;

    void processDataFromServer(Util::ByteArray contents)
    {
        if (linefeedTranslation == Control::LinefeedTranslation::LF) {
            dataReady(std::move(contents));
            return;
        }
        Util::ByteArray data;
        for (auto add : contents) {
            if (add != '\n') {
                data.push_back(static_cast<std::uint8_t>(add));
                continue;
            }

            switch (linefeedTranslation) {
            case Control::LinefeedTranslation::LF:
                data.push_back(static_cast<std::uint8_t>('\n'));
                break;
            case Control::LinefeedTranslation::CR:
                data.push_back(static_cast<std::uint8_t>('\r'));
                break;
            case Control::LinefeedTranslation::CRLF:
                data.push_back(static_cast<std::uint8_t>('\r'));
                data.push_back(static_cast<std::uint8_t>('\n'));
                break;
            case Control::LinefeedTranslation::LFCR:
                data.push_back(static_cast<std::uint8_t>('\n'));
                data.push_back(static_cast<std::uint8_t>('\r'));
                break;
            }
        }
        dataReady(std::move(data));
    }

public:
    InputReader(std::shared_ptr<Server> server, const TCP::Control::Input &control) : server(
            std::move(server)), linefeedTranslation(control.linefeedTranslation)
    { }

    virtual ~InputReader()
    {
        receiver.disconnect();
    }

    void startReading() override
    {
        server->dataReady
              .connect(receiver,
                       std::bind(&InputReader::processDataFromServer, this, std::placeholders::_1));
    }
};

}

IOPair create(const Control &control)
{
    std::unique_ptr<QTcpServer> backend(new QTcpServer);
    if (!backend->listen(QHostAddress::Any, control.port)) {
        qCWarning(log_eavesdropper) << "Error starting TCP listen:" << backend->errorString();
        return {};
    }
    if (control.port == 0) {
        CPD3::CLI::TerminalOutput::output(
                QString("TCP server listening, port = %1\n").arg(backend->serverPort()));
    }

    auto server = std::make_shared<Server>(std::move(backend));
    return {std::unique_ptr<OutputTarget>(new OutputWriter(server, control.output)),
            std::unique_ptr<InputSource>(new InputReader(server, control.input))};
}

}