/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <memory>
#include <QTest>
#include <QObject>
#include <QtDebug>
#include <QProcess>
#include <QString>
#include <QLocalSocket>
#include <QLocalServer>
#include <QTemporaryFile>
#include <QTimer>
#include <QFileInfo>

#include "core/number.hxx"
#include "core/waitutils.hxx"

using namespace CPD3;

class TestComponent : public QObject {
Q_OBJECT

    QString uid;

    static std::unique_ptr<QLocalSocket> acceptConnection(QLocalServer &server)
    {
        ElapsedTimer st;
        st.start();
        while (st.elapsed() < 30000) {
            QEventLoop loop;
            QObject::connect(&server, &QLocalServer::newConnection, &loop, &QEventLoop::quit);
            QTimer::singleShot(1000, &loop, &QEventLoop::quit);
            if (server.waitForNewConnection(500)) {
                auto socket = server.nextPendingConnection();
                if (socket) {
                    socket->open(QIODevice::ReadWrite | QIODevice::Unbuffered);
                    return std::unique_ptr<QLocalSocket>(socket);
                }
            }
            loop.exec();
        }
        return {};
    }

    static QByteArray readBytes(QIODevice *source, int expected)
    {
        QByteArray output;

        ElapsedTimer st;
        st.start();
        while (st.elapsed() < 30000) {
            auto n = source->bytesAvailable();
            if (n > 0) {
                output += source->readAll();
                if (output.size() >= expected)
                    return output;
            } else {
                QEventLoop loop;
                QObject::connect(source, &QIODevice::readyRead, &loop, &QEventLoop::quit);
                QTimer::singleShot(1000, &loop, &QEventLoop::quit);
                if (source->waitForReadyRead(100))
                    continue;
                loop.exec();
            }
        }
        return {};
    }

private slots:

    void initTestCase()
    {
        uid = Random::string();

        QVERIFY(qputenv("CPD3ARCHIVE", QByteArray("sqlite:_")));
    }

    void cliHelp()
    {
        QProcess p;
        p.start("cpd3_eavesdropper_tap", QStringList() << "--help");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        QVERIFY(output.contains("Usage:"));
    }

    void fileRead()
    {
        QLocalServer localSocketServer;
        QString socketName = QString("CPD3EVSTest-%1").arg(uid);
        QLocalServer::removeServer(socketName);
        QVERIFY(localSocketServer.listen(socketName));

        QTemporaryFile inputFile;
        QVERIFY(inputFile.open());

        QByteArray inputData = "Data to read";
        inputFile.write(inputData);
        inputFile.flush();
        inputFile.close();

        QProcess p;
        p.start("cpd3_eavesdropper_tap",
                QStringList{"qtlocal", "--input=" + inputFile.fileName(), "--mode=file",
                            socketName});
        QVERIFY(p.waitForStarted());

        auto socket = acceptConnection(localSocketServer);
        QVERIFY(socket.get() != nullptr);

        QCOMPARE(readBytes(socket.get(), inputData.size()), inputData);

        QVERIFY(p.waitForFinished());

        socket->close();
    }

    void fileWrite()
    {
        QLocalServer localSocketServer;
        QString socketName = QString("CPD3EVSTest-%1").arg(uid);
        QLocalServer::removeServer(socketName);
        QVERIFY(localSocketServer.listen(socketName));

        QTemporaryFile outputFile;
        QVERIFY(outputFile.open());

        QProcess p;
        p.start("cpd3_eavesdropper_tap",
                QStringList{"qtlocal", "--output=" + outputFile.fileName(), "--mode=file",
                            socketName});
        QVERIFY(p.waitForStarted());

        auto socket = acceptConnection(localSocketServer);
        QVERIFY(socket.get() != nullptr);

        QByteArray outputData = "Data to write";

        {
            ElapsedTimer st;
            st.start();
            while (st.elapsed() < 30000) {
                if (QFileInfo(outputFile.fileName()).size() >= outputData.size())
                    break;
                QTest::qSleep(500);

                socket->write(outputData);
                socket->flush();
            }
        }

        p.terminate();
        QVERIFY(p.waitForFinished());

        socket->close();

        QVERIFY(outputFile.readAll().contains(outputData));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
