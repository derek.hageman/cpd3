/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <QLoggingCategory>
#include <QFile>

#include "core/stdindevice.hxx"
#include "core/threading.hxx"
#include "io.hxx"

Q_DECLARE_LOGGING_CATEGORY(log_eavesdropper)

using namespace CPD3;

namespace {
class InputReader : public InputSource {
    std::unique_ptr<StdinDevice> input;
    Control::LinefeedTranslation linefeedTranslation;

    void emitData(QByteArray &&contents)
    {
        if (contents.isEmpty())
            return;
        if (linefeedTranslation == Control::LinefeedTranslation::LF) {
            dataReady(Util::ByteArray(std::move(contents)));
            return;
        }
        Util::ByteArray data;
        for (auto add : contents) {
            if (add != '\n') {
                data.push_back(static_cast<std::uint8_t>(add));
                continue;
            }

            switch (linefeedTranslation) {
            case Control::LinefeedTranslation::LF:
                data.push_back(static_cast<std::uint8_t>('\n'));
                break;
            case Control::LinefeedTranslation::CR:
                data.push_back(static_cast<std::uint8_t>('\r'));
                break;
            case Control::LinefeedTranslation::CRLF:
                data.push_back(static_cast<std::uint8_t>('\r'));
                data.push_back(static_cast<std::uint8_t>('\n'));
                break;
            case Control::LinefeedTranslation::LFCR:
                data.push_back(static_cast<std::uint8_t>('\n'));
                data.push_back(static_cast<std::uint8_t>('\r'));
                break;
            }
        }
        dataReady(std::move(data));
    }

    void readChunk()
    {
        if (!input)
            return;

        qint64 n = input->bytesAvailable();
        if (n > 0) {
            emitData(input->read(std::min(static_cast<qint64>(65536), n)));
            if (input->bytesAvailable() > 0) {
                Threading::runQueuedFunctor(input.get(), std::bind(&InputReader::readChunk, this));
                return;
            }
        }
        if (!input->atEnd())
            return;

        input->close();
        input.reset();
        completed();
    }

public:
    InputReader(std::unique_ptr<StdinDevice> &&input, const StandardInputControl &control) : input(
            std::move(input)), linefeedTranslation(control.linefeedTranslation)
    { }

    virtual ~InputReader()
    {
        if (input) {
            input->close();
        }
    }

    void startReading() override
    {
        QObject::connect(input.get(), &QIODevice::readyRead, input.get(),
                         std::bind(&InputReader::readChunk, this), Qt::QueuedConnection);
        QObject::connect(input.get(), &QIODevice::readChannelFinished, input.get(),
                         std::bind(&InputReader::readChunk, this), Qt::QueuedConnection);
        readChunk();
    }
};
}

std::unique_ptr<InputSource> InputSource::standardInput(const StandardInputControl &control)
{
    std::unique_ptr<StdinDevice> target(new StdinDevice);
    if (!target->open(QIODevice::ReadOnly)) {
        qCWarning(log_eavesdropper) << "Unable to open input standard input:"
                                    << target->errorString();
        return {};
    }
    return std::unique_ptr<InputSource>(new InputReader(std::move(target), control));
}

namespace {

class OutputWriter : public OutputTarget {
    std::unique_ptr<QFile> file;
    FormatFilter filter;

    void write(const QByteArray &incoming)
    {
        if (!file)
            return;
        if (incoming.isEmpty())
            return;
        const char *data = static_cast<const char *>(incoming.data());
        qint64 size = incoming.size();
        while (size > 0) {
            qint64 n = file->write(data, size);
            if (n < 0) {
                qCDebug(log_eavesdropper) << "Error writing to output:" << file->errorString();
                file.reset();
                return;
            }

            Q_ASSERT(n <= size);
            data += n;
            size -= n;
        }

        file->flush();
    }

public:
    OutputWriter(std::unique_ptr<QFile> &&file, const StandardOutputControl &control) : file(
            std::move(file)), filter(control, control.enableANSI)
    {
        qCDebug(log_eavesdropper) << "Writing output to file" << this->file->fileName();
    }

    virtual ~OutputWriter()
    {
        write(filter.finish());
        if (file) {
            file->close();
            file.reset();
        }
    }

    void incomingData(const QByteArray &contents) override
    { write(filter.incomingData(contents)); }

    void incomingControl(const QByteArray &contents) override
    { write(filter.incomingControl(contents)); }

    void incomingLocal(const QByteArray &contents) override
    { write(filter.incomingLocal(contents)); }

    void incomingPeriodic() override
    { write(filter.incomingPeriodic()); }
};

}

std::unique_ptr<OutputTarget> OutputTarget::standardOutput(const StandardOutputControl &control)
{
#ifdef Q_OS_UNIX
    std::unique_ptr<QFile> target(new QFile);
    if (!target->open(1, QIODevice::WriteOnly | QIODevice::Unbuffered)) {
        qCWarning(log_eavesdropper) << "Unable to open standard output:" << target->errorString();
        return {};
    }
    return std::unique_ptr<OutputTarget>(new OutputWriter(std::move(target), control));
#else
    return {};
#endif
}

