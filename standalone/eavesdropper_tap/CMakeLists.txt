include(CheckSymbolExists)
set(CMAKE_REQUIRED_LIBRARIES util)
check_symbol_exists(openpty "pty.h"
                    OPENPTY_PTYH)
set(CMAKE_REQUIRED_LIBRARIES "")
if(OPENPTY_PTYH)
    add_definitions(-DHAVE_PTY)
    set(OPENPTY_LIB "util")
else(OPENPTY_PTYH)
    check_symbol_exists(openpty "util.h"
                        OPENPTY_UTILH)
    if(OPENPTY_UTILH)
        add_definitions(-DHAVE_PTY)
    endif(OPENPTY_UTILH)
endif(OPENPTY_PTYH)

cpd3_standalone(cpd3_eavesdropper_tap
                eavesdropper_tap.cxx eavesdropper_tap.hxx
                io.cxx io.hxx
                io_file.cxx io_stdio.cxx io_pty.cxx io_tcp.cxx)


target_link_libraries(cpd3_eavesdropper_tap cpd3clicore cpd3acquisition cpd3io
                      Qt5::Network ${OPENPTY_LIB})

cpd3_symlink(cpd3_eavesdropper_tap cpd3.eavesdropper cpd3.evs)

add_subdirectory(test)
