/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include "eavesdropper_tap.hxx"
#include "io.hxx"

#ifdef HAVE_PTY

#include <thread>
#include <mutex>
#include <unistd.h>
#include <string.h>
#include <poll.h>
#include <QLoggingCategory>

#include "clicore/terminaloutput.hxx"
#include "io/notifywake.hxx"

#ifdef Q_OS_LINUX

#include <pty.h>
#include <termios.h>
#include <sys/ioctl.h>

#else
#include <util.h>
#endif

Q_DECLARE_LOGGING_CATEGORY(log_eavesdropper)

using namespace CPD3;

namespace Pty {

namespace {
class PtyThread {
    int fdMaster;
    int fdSlave;
    IO::NotifyWake wake;
    std::thread thread;

    std::mutex mutex;
    bool terminated;
    Util::ByteArray outgoingBuffer;


    void processSlaveRead(const Util::ByteArray &data)
    {
        if (data.empty())
            return;
#if defined(Q_OS_LINUX) && defined(TIOCPKT_IOCTL)
        auto flags = static_cast<std::uint_fast8_t>(data.front());
        if (flags == 0) {
            dataFromSlave(Util::ByteArray(data.mid(1)));
            return;
        }
        if (flags & TIOCPKT_IOCTL) {
            struct termios ops;
            ::memset(&ops, 0, sizeof(ops));
            if (::tcgetattr(fdMaster, &ops) == 0) {
                Data::Variant::Root config;

                switch (::cfgetospeed(&ops)) {
                case B50:
                    config["Baud"] = 50;
                    break;
                case B75:
                    config["Baud"] = 75;
                    break;
                case B110:
                    config["Baud"] = 110;
                    break;
                case B134:
                    config["Baud"] = 134;
                    break;
                case B150:
                    config["Baud"] = 150;
                    break;
                case B200:
                    config["Baud"] = 200;
                    break;
                case B300:
                    config["Baud"] = 300;
                    break;
                case B600:
                    config["Baud"] = 600;
                    break;
                case B1200:
                    config["Baud"] = 1200;
                    break;
                case B1800:
                    config["Baud"] = 1800;
                    break;
                case B2400:
                    config["Baud"] = 2400;
                    break;
                case B4800:
                    config["Baud"] = 4800;
                    break;
                case B9600:
                    config["Baud"] = 9600;
                    break;
                case B19200:
                    config["Baud"] = 19200;
                    break;
                case B38400:
                    config["Baud"] = 38400;
                    break;
                case B115200:
                    config["Baud"] = 115200;
                    break;
                case B230400:
                    config["Baud"] = 230400;
                    break;
                default:
                    break;
                }

                if (ops.c_cflag & PARENB) {
                    if (ops.c_cflag & PARODD) {
                        config["Parity"] = "Odd";
                    } else {
                        config["Parity"] = "Even";
                    }
                } else {
                    config["Parity"] = "None";
                }

                switch (ops.c_cflag & CSIZE) {
                case CS5:
                    config["DataBits"] = 5;
                    break;
                case CS6:
                    config["DataBits"] = 6;
                    break;
                case CS7:
                    config["DataBits"] = 7;
                    break;
                case CS8:
                    config["DataBits"] = 8;
                    break;
                default:
                    break;
                }

                if (ops.c_cflag & CSTOPB) {
                    config["StopBits"] = 2;
                } else {
                    config["StopBits"] = 1;
                }

                if ((ops.c_iflag & CRTSCTS) && !(ops.c_cflag & CLOCAL)) {
                    config["HardwareFlowControl"] = "Standard";
                } else {
                    config["HardwareFlowControl"] = "None";
                }

                if (ops.c_iflag & IXON) {
                    if (ops.c_iflag & IXOFF) {
                        config["SoftwareFlowControl"] = "Both";
                    } else {
                        config["SoftwareFlowControl"] = "XON";
                    }
                } else if (ops.c_iflag & IXOFF) {
                    config["SoftwareFlowControl"] = "XOFF";
                } else {
                    config["SoftwareFlowControl"] = "None";
                }
                if (ops.c_iflag & IXANY) {
                    config["SoftwareFlowControlResume"] = true;
                }

                qCDebug(log_eavesdropper) << "Received reconfiguration to" << config.read();

                connectionUpdate(std::move(config));
            } else {
                qCWarning(log_eavesdropper) << "tcgetattr failed on master:" << ::strerror(errno);
            }
        }
#else
        dataFromSlave(data);
#endif
    }

    void run()
    {
        Util::ByteArray readBuffer;

        struct pollfd fds[2];
        ::memset(fds, 0, sizeof(fds));

        wake.install(fds[0]);

        fds[1].fd = fdMaster;

        for (;;) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                if (terminated)
                    return;
                if (outgoingBuffer.empty()) {
                    fds[1].events = POLLIN;
                } else {
                    fds[1].events = POLLIN | POLLOUT;
                }
            }

#if defined(Q_OS_LINUX) && defined(TIOCPKT_IOCTL)
            fds[1].events |= POLLPRI;
#endif

            int rc = ::poll(fds, 2, -1);
            if (rc < 0) {
                if (rc == EINTR)
                    continue;
                if (rc == EAGAIN)
                    continue;
                qCWarning(log_eavesdropper) << "poll failed: " << ::strerror(errno);
                return;
            }
            if (rc == 0)
                continue;

            if (fds[0].revents & POLLIN) {
                wake.clear();
            }

            if (fds[1].revents & (POLLIN | POLLPRI)) {
                readBuffer.resize(65536);
                auto nrd = ::read(fdMaster, readBuffer.data(), readBuffer.size());
                if (nrd < 0) {
                    qCDebug(log_eavesdropper) << "Read failure on master:" << ::strerror(errno);
                } else {
                    readBuffer.resize(nrd);
                    processSlaveRead(readBuffer);
                }
            }

            if (fds[1].revents & POLLOUT) {
                Util::ByteArray toWrite;
                {
                    std::lock_guard<std::mutex> lock(mutex);
                    toWrite = std::move(outgoingBuffer);
                    outgoingBuffer.clear();
                }
                if (!toWrite.empty()) {
                    auto nwr = ::write(fdMaster, toWrite.data(), toWrite.size());
                    if (nwr < 0) {
                        qCDebug(log_eavesdropper) << "Write failure on master:"
                                                  << ::strerror(errno);
                        nwr = 0;
                    }
                    if (static_cast<std::size_t>(nwr) < toWrite.size()) {
                        using std::swap;
                        std::lock_guard<std::mutex> lock(mutex);
                        swap(outgoingBuffer, toWrite);
                        outgoingBuffer.resize(nwr);
                        outgoingBuffer += std::move(toWrite);
                    }
                }
            }
        }
    }

public:
    PtyThread(int fdMaster, int fdSlave) : fdMaster(fdMaster),
                                           fdSlave(fdSlave),
                                           terminated(false)
    {
#if defined(Q_OS_LINUX) && defined(TIOCPKT_IOCTL)
        {
            int enable = 1;
            if (::ioctl(fdMaster, TIOCPKT, &enable)) {
                qCCritical(log_eavesdropper) << "Error setting packet mode: " << ::strerror(errno);
                return;
            }
            {
                struct termios ops;
                ::memset(&ops, 0, sizeof(ops));
                if (::tcgetattr(fdSlave, &ops) == 0) {
                    ops.c_lflag |= EXTPROC;
                    ::tcsetattr(fdSlave, TCSANOW, &ops);
                }
            }
        }
#endif

        thread = std::thread(&PtyThread::run, this);
    }

    ~PtyThread()
    {
        {
            std::lock_guard<std::mutex> lock(mutex);
            terminated = true;
        }
        wake();
        if (thread.joinable()) {
            thread.join();
        }

        if (fdSlave != -1) {
            ::close(fdSlave);
        }
        if (fdMaster != -1) {
            ::close(fdMaster);
        }
    }

    void writeToSlave(const Util::ByteArray &data)
    {
        if (data.empty())
            return;
        {
            std::lock_guard<std::mutex> lock(mutex);
            outgoingBuffer += data;
        }
        wake();
    }

    void writeToSlave(Util::ByteArray &&data)
    {
        if (data.empty())
            return;
        {
            std::lock_guard<std::mutex> lock(mutex);
            outgoingBuffer += std::move(data);
        }
        wake();
    }

    void writeToSlave(const QByteArray &data)
    {
        if (data.isEmpty())
            return;
        {
            std::lock_guard<std::mutex> lock(mutex);
            outgoingBuffer += data;
        }
        wake();
    }

    CPD3::Threading::Signal<Util::ByteArray> dataFromSlave;
    CPD3::Threading::Signal<Data::Variant::Root> connectionUpdate;
};


class OutputWriter : public OutputTarget {
    std::shared_ptr<PtyThread> thread;
    FormatFilter filter;
public:
    OutputWriter(std::shared_ptr<PtyThread> thread, const Pty::Control::Output &control) : thread(
            std::move(thread)), filter(control, control.enableANSI, "\r\n")
    { }

    virtual ~OutputWriter() = default;

    void incomingData(const QByteArray &contents) override
    { thread->writeToSlave(filter.incomingData(contents)); }

    void incomingControl(const QByteArray &contents) override
    { thread->writeToSlave(filter.incomingControl(contents)); }

    void incomingLocal(const QByteArray &contents) override
    { thread->writeToSlave(filter.incomingLocal(contents)); }

    void incomingPeriodic() override
    { thread->writeToSlave(filter.incomingPeriodic()); }
};

class InputReader : public InputSource {
    std::shared_ptr<PtyThread> thread;
    Threading::Receiver receiver;
    Control::LinefeedTranslation linefeedTranslation;

    void processDataFromServer(Util::ByteArray contents)
    {
        if (linefeedTranslation == Control::LinefeedTranslation::LF) {
            dataReady(std::move(contents));
            return;
        }
        Util::ByteArray data;
        for (auto add : contents) {
            if (add != '\n') {
                data.push_back(static_cast<std::uint8_t>(add));
                continue;
            }

            switch (linefeedTranslation) {
            case Control::LinefeedTranslation::LF:
                data.push_back(static_cast<std::uint8_t>('\n'));
                break;
            case Control::LinefeedTranslation::CR:
                data.push_back(static_cast<std::uint8_t>('\r'));
                break;
            case Control::LinefeedTranslation::CRLF:
                data.push_back(static_cast<std::uint8_t>('\r'));
                data.push_back(static_cast<std::uint8_t>('\n'));
                break;
            case Control::LinefeedTranslation::LFCR:
                data.push_back(static_cast<std::uint8_t>('\n'));
                data.push_back(static_cast<std::uint8_t>('\r'));
                break;
            }
        }
        dataReady(std::move(data));
    }

public:
    InputReader(std::shared_ptr<PtyThread> thread, const Pty::Control::Input &control) : thread(
            std::move(thread)), linefeedTranslation(control.linefeedTranslation)
    { }

    virtual ~InputReader()
    {
        receiver.disconnect();
    }

    void startReading() override
    {
        thread->dataFromSlave
              .connect(receiver,
                       std::bind(&InputReader::processDataFromServer, this, std::placeholders::_1));
    }
};

}

Connection create(const Control &control)
{
    int fdMaster = -1;
    int fdSlave = -1;
    QString ptyName;

    {
        char rawName[PATH_MAX + 1];
        ::memset(rawName, 0, sizeof(rawName));
        if (::openpty(&fdMaster, &fdSlave, rawName, nullptr, nullptr)) {
            qCWarning(log_eavesdropper) << "Failed to allocate pty: " << ::strerror(errno);
            return {};
        }
        rawName[PATH_MAX] = '\0';
        ptyName = rawName;
    }

    auto thread = std::make_shared<PtyThread>(fdMaster, fdSlave);
    CPD3::CLI::TerminalOutput::output(QString("PTY open = %1\n").arg(ptyName));

    Connection result;
    result.output = std::unique_ptr<OutputTarget>(new OutputWriter(thread, control.output));
    result.input = std::unique_ptr<InputSource>(new InputReader(thread, control.input));
    result.connectionUpdate = thread->connectionUpdate;
    return std::move(result);
}

}

#else

namespace Pty {

Connection create(const Control &control)
{ return {}; }

}

#endif