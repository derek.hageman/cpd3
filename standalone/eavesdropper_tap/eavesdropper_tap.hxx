/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef EAVESDROPPER_TAP_HXX
#define EAVESDROPPER_TAP_HXX

#include "core/first.hxx"

#include <memory>
#include <deque>
#include <vector>
#include <QIODevice>

#include "acquisition/iointerface.hxx"

#if defined(Q_OS_UNIX)
#define ENABLE_STDIO
#endif

class InputSource;

class OutputTarget;

class Eavesdropper : public QObject {
Q_OBJECT

    std::unique_ptr<CPD3::Acquisition::IOInterface> interface;

    std::vector<std::unique_ptr<OutputTarget>> outputs;
    std::deque<std::unique_ptr<InputSource>> inputs;

    double timePerByte;
    double sendDoneTime;

    void writeInputData(const CPD3::Util::ByteArray &data);

    void inputComplete();

    void startInput();

public:
    Eavesdropper();

    virtual ~Eavesdropper();

    int parseArguments(QStringList arguments);


};

#endif //EAVESDROPPER_TAP_HXX
