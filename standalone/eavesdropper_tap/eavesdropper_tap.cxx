/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <QCoreApplication>
#include <QTranslator>
#include <QLibraryInfo>
#include <QLocale>
#include <QLoggingCategory>
#include <QTimer>

#include "core/abort.hxx"
#include "core/qtcompat.hxx"
#include "core/threadpool.hxx"
#include "clicore/argumentparser.hxx"
#include "clicore/interfaceparse.hxx"
#include "clicore/terminaloutput.hxx"
#include "datacore/variant/root.hxx"
#include "acquisition/acquisitionnetwork.hxx"
#include "acquisition/iotarget.hxx"
#include "eavesdropper_tap.hxx"
#include "io.hxx"

Q_LOGGING_CATEGORY(log_eavesdropper, "cpd3.eavesdropper", QtWarningMsg)

using namespace CPD3;

namespace {

class Parser : public CPD3::CLI::ArgumentParser {
public:
    Parser() = default;

    virtual ~Parser() = default;

protected:
    QList<BareWordHelp> getBareWordHelp() override
    {
        return CPD3::CLI::InterfaceParse::getBareWordHelp();
    }

    QString getBareWordCommandLine() override
    {
        return CPD3::CLI::InterfaceParse::getBareWordCommandLine();
    }

    QString getProgramName() override
    { return tr("cpd3.eavesdropper", "program name"); }

    QString getDescription() override
    {
        return tr("This is the CPD3 eavesdropper.  It allows observation of the traffic to and "
                  "from a physical interface that the acquisition system is operating on "
                  "without disturbing that operation.  It also allows direct communication with "
                  "the interface as well as temporarily locking out the acquisition system.");
    }

    QHash<QString, QString> getDocumentationTypeID() override
    {
        QHash<QString, QString> result;
        result.insert("type", "eavesdropper");
        return result;
    }
};

}

Eavesdropper::Eavesdropper() : timePerByte(0), sendDoneTime(FP::undefined())
{ }

Eavesdropper::~Eavesdropper()
{
    inputs.clear();
    outputs.clear();
    interface.reset();
}

void Eavesdropper::writeInputData(const CPD3::Util::ByteArray &data)
{
    if (!interface)
        return;
    auto ba = data.toQByteArray();
    interface->write(data);
    for (const auto &target : outputs) {
        target->incomingLocal(ba);
    }

    double now = Time::time();
    if (!FP::defined(sendDoneTime) || sendDoneTime <= now) {
        sendDoneTime = now;
    }
    sendDoneTime += data.size() * timePerByte;
}

void Eavesdropper::inputComplete()
{
    if (inputs.empty())
        return;
    inputs.pop_front();

    if (inputs.empty()) {
        qCDebug(log_eavesdropper) << "All inputs completed";

        if (outputs.empty()) {
            qCDebug(log_eavesdropper) << "No output available, shutting down";
            /* No reliable way to make sure data are flushed, so just wait a bit */
            int wait = 250;
            double now = Time::time();
            if (FP::defined(sendDoneTime) && sendDoneTime > now) {
                wait += std::ceil((sendDoneTime - now) * 1000.0);
            }
            QTimer::singleShot(wait, this, []() {
                QCoreApplication::exit(0);
            });
        }
        return;
    }

    qCDebug(log_eavesdropper) << "Input complete, advancing to next";
    startInput();
}

void Eavesdropper::startInput()
{
    if (inputs.empty())
        return;
    const auto &next = inputs.front();
    next->dataReady
        .connect(this, std::bind(&Eavesdropper::writeInputData, this, std::placeholders::_1), true);
    next->completed.connect(this, std::bind(&Eavesdropper::inputComplete, this), true);
    next->startReading();
}

static CPD3::Data::Variant::Root lookupAcquisitionInterface(const QString &name)
{
    using namespace CPD3::Data;
    using namespace CPD3::Acquisition;

    if (name.isEmpty())
        return {};

    std::unique_ptr<AcquisitionNetworkClient>
            client(new AcquisitionNetworkClient(Variant::Read::empty()));

    CPD3::Data::Variant::Root result;

    qCDebug(log_eavesdropper) << "Looking for interface for" << name;

    std::mutex mutex;
    std::condition_variable cv;
    using Clock = std::chrono::steady_clock;
    enum class State {
        Startup, Connected, Ended
    } state = State::Startup;
    Clock::time_point timeout = Clock::now() + std::chrono::seconds(30);

    AcquisitionNetworkClient *clientRef = client.get();
    client->interfaceInformationUpdated.connect([&](const std::string &interface) {
        auto info = clientRef->getInterfaceInformation(interface);
        if (!info.read().exists())
            return;
        if (info.read().hash("Source").hash("Name").toQString() != name)
            return;

        qCDebug(log_eavesdropper) << "Found interface for" << name;

        result.write().set(info["Interface"]);

        {
            std::lock_guard<std::mutex> lock(mutex);
            switch (state) {
            case State::Startup:
            case State::Connected:
                state = State::Ended;
                break;
            case State::Ended:
                return;
            }
        }
        cv.notify_all();
    });
    client->connectionState.connect([&](bool connected) {
        if (!connected)
            return;
        {
            std::lock_guard<std::mutex> lock(mutex);
            switch (state) {
            case State::Startup:
                state = State::Connected;
                timeout = Clock::now() + std::chrono::seconds(5);
                break;
            case State::Connected:
            case State::Ended:
                return;
            }
        }
        cv.notify_all();
    });
    client->connectionFailed.connect([&] {
        {
            std::lock_guard<std::mutex> lock(mutex);
            switch (state) {
            case State::Startup:
                state = State::Ended;
                break;
            case State::Connected:
            case State::Ended:
                break;
            }
        }
        cv.notify_all();
    });

    client->start();
    {
        std::unique_lock<std::mutex> lock(mutex);
        while (state != State::Ended && timeout > Clock::now()) {
            cv.wait_until(lock, timeout);
        }
    }
    client.reset();

    return result;
}

static void configureInput(const ComponentOptions &options, InputSource::Control &control)
{
    if (options.isSet("linefeed")) {
        control.linefeedTranslation =
                static_cast<InputSource::Control::LinefeedTranslation>(qobject_cast<
                        ComponentOptionEnum *>(options.get("linefeed"))->get().getID());
    }
}

static void configureOutput(const ComponentOptions &options, OutputTarget::Control &control)
{
    if (options.isSet("origin")) {
        control.enableOriginIndicator =
                qobject_cast<ComponentOptionBoolean *>(options.get("origin"))->get();
        if (control.enableOriginIndicator) {
            control.condenseNewlines = true;
            control.hideUnprintable = true;
        }
    }
    if (options.isSet("condense-newlines")) {
        control.condenseNewlines =
                qobject_cast<ComponentOptionBoolean *>(options.get("condense-newlines"))->get();
    }
    if (options.isSet("echo")) {
        control.displayEcho = qobject_cast<ComponentOptionBoolean *>(options.get("echo"))->get();
    }
    if (options.isSet("hide-unprintable")) {
        control.hideUnprintable =
                qobject_cast<ComponentOptionBoolean *>(options.get("hide-unprintable"))->get();
    }
}

int Eavesdropper::parseArguments(QStringList arguments)
{
    if (!arguments.isEmpty())
        arguments.removeFirst();

    ComponentOptions options;

    options.add("exclusive",
                new ComponentOptionBoolean(tr("exclusive", "name"), tr("Start in exclusive mode"),
                                           tr("If set the eavesdropper will start in exclusive mode.  "
                                              "Exclusive mode prevents the acquisition system from "
                                              "communicating with the interface while it is in effect.  The "
                                              "acquisition system will experience timeouts during the effect."),
                                           {}));
    options.add("instrument", new ComponentOptionSingleString(tr("instrument", "name"),
                                                              tr("Eavesdropped instrument"),
                                                              tr("When set, this selects the interface to the one used by "
                                                                 "the instrument with the selected identifier."),
                                                              {}));

    options.add("ansi-format", new ComponentOptionBoolean(tr("ansi-format", "name"),
                                                          tr("Enable ANSI terminal format codes"),
                                                          tr("Enable the usage of ANSI terminal format codes to display "
                                                             "the direction or source of data.  This has no effect on data "
                                                             "written to files."), {}));
    options.add("condense-newlines", new ComponentOptionBoolean(tr("condense-newlines", "name"),
                                                                tr("Condense all newline sequences into a single one"),
                                                                tr("When enabled, this option translates all line advance "
                                                                   "sequences (CR and LF) into a single output linefeed."),
                                                                tr("Enabled for interactive outputs")));
    options.add("hide-unprintable", new ComponentOptionBoolean(tr("hide-unprintable", "name"),
                                                               tr("Hide all unprintable characters"),
                                                               tr("Convert all unprintable ASCII characters (including "
                                                                  "unicode) into placholders."),
                                                               tr("Enabled for interactive outputs")));
    options.add("origin", new ComponentOptionBoolean(tr("origin", "name"),
                                                     tr("Add an indicating the origin of data"),
                                                     tr("When enabled, an indicator is added to all incoming data "
                                                        "blocks that shows the origin of the data and the time "
                                                        "of arrival."), {}));
    options.add("echo", new ComponentOptionBoolean(tr("echo", "name"), tr("Display local echo"),
                                                   tr("When enabled, locally read data are also echoed to the output."),
                                                   tr("Enabled for interactive outputs")));
    options.add("raw", new ComponentOptionBoolean(tr("raw", "name"), tr("Raw file mode"),
                                                  tr("When set, this disables the automatic translation of OS "
                                                     "specific line advances to line feeds for input and output files."),
                                                  {}));

    {
        auto i = new ComponentOptionSingleInteger(tr("port", "name"), tr("The port to listen on"),
                                                  tr("This is the port the TCP server will listen for connections on."),
                                                  tr("Automatically assigned"));
        i->setAllowUndefined(false);
        i->setMinimum(1);
        i->setMaximum(65535);
        options.add("port", i);
    }

    {
        auto file = new ComponentOptionFile(tr("output", "name"), tr("The file to write to"),
                                            tr("This is the output file to write to.  All data read "
                                               "from the interface are written to this file."),
                                            tr("eavesdropper.log in file mode only",
                                               "default output"));
        file->setMode(ComponentOptionFile::Write);
        options.add("output", file);
    }

    {
        auto file = new ComponentOptionFile(tr("input", "name"), tr("The file to write to"),
                                            tr("This is the input file to read from.  The contents of "
                                               "the file are written out to the interface."), {});
        file->setMode(ComponentOptionFile::Read);
        options.add("input", file);
    }

    {
        auto linefeedOption =
                new ComponentOptionEnum(tr("linefeed", "name"), tr("Input linefeed translation"),
                                        tr("This selects how linefeeds are translated in data input."),
                                        tr("LF for files and CR for interactive",
                                           "linefeed default"));
        linefeedOption->add(static_cast<int>(InputSource::Control::LinefeedTranslation::LF), "lf",
                            tr("LF", "mode name"), tr("Keep linefeeds as a linefeed alone."));
        linefeedOption->add(static_cast<int>(InputSource::Control::LinefeedTranslation::CR), "cr",
                            tr("CR", "mode name"), tr("Translate linefeeds to carriage returns."));
        linefeedOption->add(static_cast<int>(InputSource::Control::LinefeedTranslation::CRLF),
                            "crlf", tr("CRLF", "mode name"),
                            tr("Translate linefeeds to a CRLF sequence."));
        linefeedOption->add(static_cast<int>(InputSource::Control::LinefeedTranslation::LFCR),
                            "lfcr", tr("LFCR", "mode name"),
                            tr("Translate linefeeds to a LFCR sequence."));
        options.add("linefeed", linefeedOption);
    }

    enum class Mode : int {
        File, TCP, StandardIO, Pty,
    };

    {
        auto modeOption = new ComponentOptionEnum(tr("mode", "name"), tr("Communications mode"),
                                                  tr("This selects the communications mode the eavesdropper uses."),
#if defined(ENABLE_STDIO)
                                                  tr("Standard I/O", "mode default"),
#elif defined(HAVE_PTY)
        tr("Pseudo terminal", "mode default"),
#else
        tr("File input and output", "mode default"),
#endif
                                                  0);
        modeOption->add(static_cast<int>(Mode::File), "file", tr("file", "mode name"),
                        tr("Read and write data to or from files."));
        modeOption->add(static_cast<int>(Mode::TCP), "tcp", tr("tcp", "mode name"),
                        tr("Listen for TCP connections and forward the data send or received."));
#if defined(ENABLE_STDIO)
        modeOption->add(static_cast<int>(Mode::StandardIO), "stdio", tr("stdio", "mode name"),
                        tr("Forward standard I/O."));
#endif
#if defined(HAVE_PTY)
        modeOption->add(static_cast<int>(Mode::Pty), "pty", tr("pty", "mode name"),
                        tr("Create a pseudo terminal and use it for input and output."));
#endif

        options.add("mode", modeOption);
    }

    Parser parser;
    try {
        parser.parse(arguments, options);
    } catch (const CPD3::CLI::ArgumentParsingException &ape) {
        QString text(ape.getText());
        if (ape.isError()) {
            if (!text.isEmpty())
                CPD3::CLI::TerminalOutput::simpleWordWrap(text, true);
            QCoreApplication::exit(1);
            return -1;
        } else {
            if (!text.isEmpty())
                CPD3::CLI::TerminalOutput::simpleWordWrap(text);
            QCoreApplication::exit(0);
        }
        return 1;
    }

    QString selectedInstrument;
    if (options.isSet("instrument")) {
        selectedInstrument =
                qobject_cast<ComponentOptionSingleString *>(options.get("instrument"))->get();
    }

    if (arguments.isEmpty() && selectedInstrument.isEmpty()) {
        CPD3::CLI::TerminalOutput::simpleWordWrap(tr("No interface specification provided."), true);
        return -1;
    }

    CPD3::Data::Variant::Root configuration;
    if (!arguments.isEmpty()) {
        try {
            configuration = CPD3::CLI::InterfaceParse::parse(arguments, true);
        } catch (const CPD3::CLI::InterfaceParsingException &ipe) {
            QString text(ipe.getDescription());
            if (!text.isEmpty())
                CPD3::CLI::TerminalOutput::simpleWordWrap(text, true);
            QCoreApplication::exit(1);
            return -1;
        }
    }
    if (!selectedInstrument.isEmpty()) {
        auto over = lookupAcquisitionInterface(selectedInstrument);
        if (over.read().exists()) {
            configuration = CPD3::Data::Variant::Root::overlay(configuration, over);
        }
    }
    if (!configuration.read().exists()) {
        CPD3::CLI::TerminalOutput::simpleWordWrap(
                tr("Unable to determine target interface (check the instrument code)."), true);
        return -1;
    }

    Mode mode;
#if defined(ENABLE_STDIO)
    mode = Mode::StandardIO;
#elif defined(HAVE_PTY)
    if (!options.isSet("output") && !options.isSet("input")) {
        mode = Mode::Pty;
    } else {
        mode = Mode::File;
    }
#else
    mode = Mode::File;
#endif

    if (options.isSet("mode")) {
        mode = static_cast<Mode>(qobject_cast<ComponentOptionEnum *>(options.get("mode"))->get()
                                                                                         .getID());
    }
    if (mode == Mode::StandardIO) {
        Logging::suppressForConsole();
    }

    if (options.isSet("input")) {
        InputSource::FileControl control;
        configureInput(options, control);
        if (options.isSet("raw")) {
            control.textMode = !qobject_cast<ComponentOptionBoolean *>(options.get("raw"))->get();
        }
        auto filename =
                qobject_cast<ComponentOptionFile *>(options.get("input"))->get().toStdString();
        if (!filename.empty()) {
            auto add = InputSource::file(filename, control);
            if (add) {
                inputs.emplace_back(std::move(add));
            }
        }
    }

    switch (mode) {
    case Mode::File:
        if (!options.isSet("output") && !options.isSet("input")) {
            OutputTarget::FileControl control;
            configureOutput(options, control);
            if (options.isSet("raw")) {
                control.textMode =
                        !qobject_cast<ComponentOptionBoolean *>(options.get("raw"))->get();
            }
            auto add = OutputTarget::file("eavesdropper.log", control);
            if (add) {
                outputs.emplace_back(std::move(add));
            }
        }
        break;
    case Mode::TCP: {
        TCP::Control control;
        if (options.isSet("port")) {
            control.port = static_cast<std::uint16_t>(qobject_cast<ComponentOptionSingleInteger *>(
                    options.get("port"))->get());
        }
        configureInput(options, control.input);
        configureOutput(options, control.output);
        auto add = TCP::create(control);
        if (add.first) {
            outputs.emplace_back(std::move(add.first));
        }
        if (add.second) {
            inputs.emplace_back(std::move(add.second));
        }
        break;
    }
    case Mode::StandardIO: {
        {
            InputSource::StandardInputControl control;
            configureInput(options, control);
            auto add = InputSource::standardInput(control);
            if (add) {
                inputs.emplace_back(std::move(add));
            }
        }
        {
            OutputTarget::StandardOutputControl control;
            configureOutput(options, control);
            auto add = OutputTarget::standardOutput(control);
            if (add) {
                outputs.emplace_back(std::move(add));
            }
        }
        break;
    }
    case Mode::Pty: {
        Pty::Control control;
        configureInput(options, control.input);
        configureOutput(options, control.output);
        auto add = Pty::create(control);
        if (add.output) {
            outputs.emplace_back(std::move(add.output));
        }
        if (add.input) {
            inputs.emplace_back(std::move(add.input));
        }
        add.connectionUpdate.connect(this, [this](const Data::Variant::Root &over) {
            if (!interface)
                return;
            auto target = CPD3::Acquisition::IOTarget::create(over);
            if (!target)
                return;
            interface->merge(*target);
        }, true);
        break;
    }
    }

    if (options.isSet("output")) {
        OutputTarget::FileControl control;
        configureOutput(options, control);
        if (options.isSet("raw")) {
            control.textMode = !qobject_cast<ComponentOptionBoolean *>(options.get("raw"))->get();
        }
        auto filename =
                qobject_cast<ComponentOptionFile *>(options.get("output"))->get().toStdString();
        if (!filename.empty()) {
            auto add = OutputTarget::file(filename, control);
            if (add) {
                outputs.emplace_back(std::move(add));
            }
        }
    }

    if (inputs.empty() && outputs.empty()) {
        CPD3::CLI::TerminalOutput::simpleWordWrap(tr("No input or output available."), true);
        QCoreApplication::exit(1);
        return -1;
    }

    using namespace CPD3::Acquisition;

    auto target = IOTarget::create(configuration);
    if (!target || !target->isValid()) {
        CPD3::CLI::TerminalOutput::simpleWordWrap(tr("Unable to create io interface."), true);
        QCoreApplication::exit(1);
        return -1;
    }
    interface.reset(new IOInterface(std::move(target)));

    interface->read.connect([this](const Util::ByteArray &data) {
        for (const auto &target : outputs) {
            target->incomingData(data.toQByteArray());
        }
    });
    interface->otherControl.connect([this](const Util::ByteArray &data) {
        for (const auto &target : outputs) {
            target->incomingControl(data.toQByteArray());
        }
    });

    qCDebug(log_eavesdropper) << "Attaching to interface" << interface->description();

    {
        auto baud = configuration["Baud"].toInteger();
        if (INTEGER::defined(baud) && baud > 0) {
            /* Just assume 8N1 as an approximation */
            timePerByte = 10.0 / static_cast<double>(baud);
        }
    }

    interface->start();
    if (options.isSet("exclusive")) {
        interface->setExclusive(
                qobject_cast<ComponentOptionBoolean *>(options.get("exclusive"))->get());
    }

    {
        auto timer = new QTimer(this);
        timer->setInterval(50);
        timer->setSingleShot(false);
        QObject::connect(timer, &QTimer::timeout, this, [this]() {
            for (const auto &output : outputs) {
                output->incomingPeriodic();
            }
        });
        timer->start();
    }

    startInput();

    return 0;
}

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app.installTranslator(&qtTranslator);

    QString translateLocation;
    QByteArray cpd3(qgetenv("CPD3"));
    if (cpd3.length() > 0) {
        translateLocation = QString(cpd3) + "/locale";
    }
    QTranslator cpd3Translator;
    cpd3Translator.load("cpd3_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cpd3Translator);
    QTranslator cliTranslator;
    cliTranslator.load("cli_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&cliTranslator);
    QTranslator acquisitionTranslator;
    acquisitionTranslator.load("acquisition_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&acquisitionTranslator);
    QTranslator eavesdropperTranslator;
    eavesdropperTranslator.load("eavesdropper_" + QLocale::system().name(), translateLocation);
    app.installTranslator(&eavesdropperTranslator);

    int rc;
    {
        Eavesdropper evs;
        if (int rc = evs.parseArguments(app.arguments())) {
            ThreadPool::system()->wait();
            if (rc == -1)
                return 1;
            return 0;
        }

        Abort::installAbortHandler(true);

        AbortPoller abortPoller;
        QObject::connect(&abortPoller, SIGNAL(aborted()), &app, SLOT(quit()));

        abortPoller.start();

        rc = app.exec();
    }
    ThreadPool::system()->wait();
    return rc;
}