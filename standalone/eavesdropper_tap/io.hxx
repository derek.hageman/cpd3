/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef IO_HXX
#define IO_HXX

#include "core/first.hxx"

#include "core/threading.hxx"
#include "core/util.hxx"
#include "datacore/variant/root.hxx"

class InputSource {
public:
    InputSource();

    virtual ~InputSource();

    virtual void startReading() = 0;

    CPD3::Threading::Signal<CPD3::Util::ByteArray> dataReady;
    CPD3::Threading::Signal<> completed;

    struct Control {
        enum class LinefeedTranslation : int {
            LF, CR, CRLF, LFCR
        } linefeedTranslation;

        Control();
    };

    struct FileControl : public Control {
        bool textMode;

        FileControl();
    };

    static std::unique_ptr<InputSource> file(const std::string &filename,
                                             const FileControl &control = {});

    struct StandardInputControl : public Control {
        StandardInputControl();
    };

    static std::unique_ptr<InputSource> standardInput(const StandardInputControl &control = {});
};

class OutputTarget {
public:
    OutputTarget();

    virtual ~OutputTarget();

    virtual void incomingData(const QByteArray &contents) = 0;

    virtual void incomingControl(const QByteArray &contents) = 0;

    virtual void incomingLocal(const QByteArray &contents) = 0;

    virtual void incomingPeriodic();

    struct Control {
        bool condenseNewlines;
        bool enableOriginIndicator;
        bool displayEcho;
        bool hideUnprintable;

        Control();
    };

    class FormatFilter final {
        double packetStart;
        enum class PacketOrigin {
            None, Data, Control, Local
        } packetOrigin;
        QByteArray packetContents;
        PacketOrigin lastNewlineOrigin;

        bool bypass;
        bool enableANSI;
        QByteArray newlineSequence;
        Control control;

        static bool isNewline(char test);

        static bool isUnprintable(char test);

        QByteArray startPacket(char directionCode);

        QByteArray addContents(const QByteArray &contents,
                               const char *displaySequence,
                               char directionCode);

        QByteArray maybeFlush();

        QByteArray flushExisting();

    public:
        explicit FormatFilter(const Control &control,
                              bool enableANSI = false,
                              const QByteArray &newlineSequence = "\n");

        ~FormatFilter();

        QByteArray incomingData(const QByteArray &contents);

        QByteArray incomingControl(const QByteArray &contents);

        QByteArray incomingLocal(const QByteArray &contents);

        QByteArray incomingPeriodic();

        QByteArray finish();
    };

    struct FileControl : public Control {
        bool textMode;

        FileControl();
    };

    static std::unique_ptr<OutputTarget> file(const std::string &filename,
                                              const FileControl &control = {});

    struct InteractiveControl : public Control {
        bool enableANSI;

        InteractiveControl();
    };

    struct StandardOutputControl : public InteractiveControl {
        StandardOutputControl();
    };

    static std::unique_ptr<OutputTarget> standardOutput(const StandardOutputControl &control = {});
};

namespace Pty {

struct Control {
    struct Input : public InputSource::Control {
        Input();
    };

    Input input;

    struct Output : public OutputTarget::InteractiveControl {
        Output();
    };

    Output output;
};

struct Connection {
    std::unique_ptr<OutputTarget> output;
    std::unique_ptr<InputSource> input;
    CPD3::Threading::Signal<CPD3::Data::Variant::Root> connectionUpdate;
};

Connection create(const Control &control = {});

}

namespace TCP {

struct Control {
    struct Input : public InputSource::Control {
        Input();
    };

    Input input;

    struct Output : public OutputTarget::InteractiveControl {
        Output();
    };

    Output output;

    std::uint16_t port;

    Control();
};

using IOPair = std::pair<std::unique_ptr<OutputTarget>, std::unique_ptr<InputSource>>;

IOPair create(const Control &control = {});

}

#endif //IO_HXX
