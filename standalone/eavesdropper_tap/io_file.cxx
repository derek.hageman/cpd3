/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <QFile>
#include <QLoggingCategory>

#include "io.hxx"
#include "core/qtcompat.hxx"
#include "core/threading.hxx"

Q_DECLARE_LOGGING_CATEGORY(log_eavesdropper)

using namespace CPD3;

namespace {
class InputReader : public InputSource {
    std::unique_ptr<QFile> file;
    Control::LinefeedTranslation linefeedTranslation;

    void emitData(QByteArray &&contents)
    {
        if (contents.isEmpty())
            return;
        if (linefeedTranslation == Control::LinefeedTranslation::LF) {
            dataReady(Util::ByteArray(std::move(contents)));
            return;
        }
        Util::ByteArray data;
        for (auto add : contents) {
            if (add != '\n') {
                data.push_back(static_cast<std::uint8_t>(add));
                continue;
            }

            switch (linefeedTranslation) {
            case Control::LinefeedTranslation::LF:
                data.push_back(static_cast<std::uint8_t>('\n'));
                break;
            case Control::LinefeedTranslation::CR:
                data.push_back(static_cast<std::uint8_t>('\r'));
                break;
            case Control::LinefeedTranslation::CRLF:
                data.push_back(static_cast<std::uint8_t>('\r'));
                data.push_back(static_cast<std::uint8_t>('\n'));
                break;
            case Control::LinefeedTranslation::LFCR:
                data.push_back(static_cast<std::uint8_t>('\n'));
                data.push_back(static_cast<std::uint8_t>('\r'));
                break;
            }
        }
        dataReady(std::move(data));
    }

    void readChunk()
    {
        if (!file)
            return;

        qint64 n = file->bytesAvailable();
        if (n > 0) {
            emitData(file->read(std::min(static_cast<qint64>(65536), n)));
            if (file->bytesAvailable() > 0) {
                Threading::runQueuedFunctor(file.get(), std::bind(&InputReader::readChunk, this));
                return;
            }
        }
        if (!file->atEnd())
            return;

        file->close();
        file.reset();
        completed();
    }

public:
    InputReader(std::unique_ptr<QFile> &&file, const FileControl &control) : file(std::move(file)),
                                                                             linefeedTranslation(
                                                                                     control.linefeedTranslation)
    { }

    virtual ~InputReader() = default;

    void startReading() override
    {
        QObject::connect(file.get(), &QIODevice::readyRead, file.get(),
                         std::bind(&InputReader::readChunk, this), Qt::QueuedConnection);
        QObject::connect(file.get(), &QIODevice::readChannelFinished, file.get(),
                         std::bind(&InputReader::readChunk, this), Qt::QueuedConnection);
        readChunk();
    }
};
}

std::unique_ptr<InputSource> InputSource::file(const std::string &filename,
                                               const FileControl &control)
{
    if (filename.empty())
        return {};
    std::unique_ptr<QFile> target(new QFile(QString::fromStdString(filename)));
    QIODevice::OpenMode mode = QIODevice::ReadOnly;
    if (control.textMode)
        mode |= QIODevice::Text;
    if (!target->open(mode)) {
        qCWarning(log_eavesdropper) << "Unable to open input file" << filename << ":"
                                    << target->errorString();
        return {};
    }
    return std::unique_ptr<InputSource>(new InputReader(std::move(target), control));
}

namespace {

class OutputWriter : public OutputTarget {
    std::unique_ptr<QFile> file;
    FormatFilter filter;

    void write(const QByteArray &incoming)
    {
        if (!file)
            return;
        if (incoming.isEmpty())
            return;
        const char *data = static_cast<const char *>(incoming.data());
        qint64 size = incoming.size();
        while (size > 0) {
            qint64 n = file->write(data, size);
            if (n < 0) {
                qCDebug(log_eavesdropper) << "Error writing to output:" << file->errorString();
                file.reset();
                return;
            }

            Q_ASSERT(n <= size);
            data += n;
            size -= n;
        }

        file->flush();
    }

public:
    OutputWriter(std::unique_ptr<QFile> &&file, const FileControl &control) : file(std::move(file)),
                                                                              filter(control)
    {
        qCDebug(log_eavesdropper) << "Writing output to file" << this->file->fileName();
    }

    virtual ~OutputWriter()
    {
        write(filter.finish());
        if (file) {
            file->close();
            file.reset();
        }
    }

    void incomingData(const QByteArray &contents) override
    { write(filter.incomingData(contents)); }

    void incomingControl(const QByteArray &contents) override
    { write(filter.incomingControl(contents)); }

    void incomingLocal(const QByteArray &contents) override
    { write(filter.incomingLocal(contents)); }

    void incomingPeriodic() override
    { write(filter.incomingPeriodic()); }
};

}

std::unique_ptr<OutputTarget> OutputTarget::file(const std::string &filename,
                                                 const FileControl &control)
{
    if (filename.empty())
        return {};
    std::unique_ptr<QFile> target(new QFile(QString::fromStdString(filename)));
    QIODevice::OpenMode mode = QIODevice::WriteOnly | QIODevice::Append;
    if (control.textMode)
        mode |= QIODevice::Text;
    if (!target->open(mode)) {
        qCWarning(log_eavesdropper) << "Unable to open output file" << filename << ":"
                                    << target->errorString();
        return {};
    }
    return std::unique_ptr<OutputTarget>(new OutputWriter(std::move(target), control));
}

