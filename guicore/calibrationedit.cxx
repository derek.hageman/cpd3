/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QGridLayout>
#include <QScrollArea>
#include <QScrollBar>
#include <QDoubleValidator>

#include "guicore/calibrationedit.hxx"

namespace CPD3 {
namespace GUI {

/** @file guicore/calibrationedit.hxx
 * Provides an editing widget for CPD3 calibration objects.
 */


static const char *selectionListStyle = "min-width: 40px;";

namespace {
class CalibrationScrollArea : public QScrollArea {
public:
    CalibrationScrollArea(QWidget *parent = 0) : QScrollArea(parent), minHeight(0)
    { }

    virtual ~CalibrationScrollArea()
    { }

    int minHeight;

    virtual QSize minimumSizeHint() const
    {
        QSize sz(QScrollArea::minimumSizeHint());
        sz.setHeight(qMax(minHeight, sz.height()));
        return sz;
    }
};
}

CalibrationEdit::CalibrationEdit(QWidget *parent) : QWidget(parent)
{
    QGridLayout *mainLayout = new QGridLayout;
    mainLayout->setContentsMargins(0, 0, 0, 0);
    setLayout(mainLayout);

    mainLayout->setColumnStretch(0, 1);

    CalibrationScrollArea *scrollArea = new CalibrationScrollArea(this);
    mainLayout->addWidget(scrollArea, 0, 0, 1, -1);
    listWidget = new QWidget(scrollArea);
    scrollArea->setWidget(listWidget);
    scrollArea->setWidgetResizable(true);
    scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    scrollArea->setFrameShape(QFrame::NoFrame);
    scrollArea->verticalScrollBar()->hide();
    scrollArea->verticalScrollBar()->resize(0, 0);

    listLayout = new QGridLayout;
    listLayout->setContentsMargins(0, 0, 0, 0);
    listWidget->setLayout(listLayout);

    listGroup = new QButtonGroup(listWidget);
    listGroup->setExclusive(true);

    addButton = new QPushButton(tr("Add", "add text"), listWidget);
    addButton->setToolTip(tr("Add a coefficient to the polynomial."));
    addButton->setStatusTip(tr("Add coefficient"));
    addButton->setWhatsThis(
            tr("This adds a new coefficient to the polynomial in the next highest order slot.  That is, it increases the order of the polynomial."));
    scrollArea->minHeight = addButton->minimumSizeHint().height();
    listLayout->addWidget(addButton, 0, 0);
    connect(addButton, SIGNAL(clicked(bool)), this, SLOT(addPressed()));

    valueEditor = new QLineEdit(this);
    valueEditor->setValidator(new QDoubleValidator(valueEditor));
    connect(valueEditor, SIGNAL(textEdited(
                                        const QString &)), this, SLOT(coefficientChanged()));
    mainLayout->addWidget(valueEditor, 1, 0, 1, 1);

    removeButton = new QPushButton(tr("Remove", "remove text"), this);
    removeButton->setToolTip(tr("Remove a coefficient to the polynomial."));
    removeButton->setStatusTip(tr("Remove coefficient"));
    removeButton->setWhatsThis(
            tr("This removes the highest order coefficient of the polynomial.  That is, it decreases the order of the polynomial."));
    mainLayout->addWidget(removeButton, 1, 1, 1, 1);
    connect(removeButton, SIGNAL(clicked(bool)), this, SLOT(removePressed()));

    updateCalibration();
}

Calibration CalibrationEdit::getCalibration() const
{ return calibration; }

void CalibrationEdit::setCalibration(const Calibration &cal)
{
    calibration = cal;
    updateCalibration();
}

void CalibrationEdit::setButtonText(int i)
{
    QPushButton *button = listButtons.at(i);

    double v = calibration.get(i);
    if (i == 0) {
        button->setText(tr("%1", "selection zero button text").arg(v));
    } else if (i == 1) {
        if (v < 0) {
            button->setText(tr("- %1 x", "selection first button text").arg(-v));
        } else {
            button->setText(tr("+ %1 x", "selection first button text").arg(v));
        }
    } else {
        NumberFormat exponentFormat(1, 0);

        if (v < 0) {
            button->setText(
                    tr("- %2 x%1", "selection button text").arg(exponentFormat.superscript(i))
                                                           .arg(-v));
        } else {
            button->setText(
                    tr("+ %2 x%1", "selection button text").arg(exponentFormat.superscript(i))
                                                           .arg(v));
        }
    }
}

void CalibrationEdit::updateCalibration()
{
    int selected = listGroup->checkedId();
    for (int i = 0; i <= listButtons.size(); i++) {
        listLayout->setColumnStretch(i, 0);
    }
    for (QList<QPushButton *>::const_iterator button = listButtons.constBegin(),
            end = listButtons.constEnd(); button != end; ++button) {
        listGroup->removeButton(*button);
        listLayout->removeWidget(*button);
        (*button)->hide();
        (*button)->deleteLater();
    }
    listButtons.clear();
    listLayout->removeWidget(addButton);

    for (int i = 0; i < calibration.size(); i++) {
        QPushButton *button = new QPushButton(this);
        button->setStyleSheet(selectionListStyle);
        listButtons.append(button);
        listLayout->addWidget(button, 0, i);
        button->setCheckable(true);
        listGroup->addButton(button, i);
        setButtonText(i);
        listLayout->setColumnStretch(i, 0);

        connect(button, SIGNAL(toggled(bool)), this, SLOT(coefficientSelected()));
    }

    listLayout->addWidget(addButton, 0, calibration.size());
    listLayout->setColumnStretch(calibration.size(), 1);

    if (selected != -1 && selected < listButtons.size()) {
        valueEditor->setEnabled(true);
        listButtons.at(selected)->setChecked(true);
    } else {
        valueEditor->setEnabled(false);
    }

    removeButton->setEnabled(calibration.size() > 0);
}

void CalibrationEdit::addPressed()
{
    calibration.append(0);
    updateCalibration();

    removeButton->setEnabled(true);
    valueEditor->setEnabled(true);
    listButtons.last()->setChecked(true);

    emit calibrationChanged(calibration);
}

void CalibrationEdit::removePressed()
{
    calibration.removeLast();
    updateCalibration();
    emit calibrationChanged(calibration);
}

void CalibrationEdit::coefficientSelected()
{
    int selected = listGroup->checkedId();
    if (selected < 0 || selected >= calibration.size()) {
        valueEditor->setEnabled(false);
        return;
    }

    valueEditor->setEnabled(true);
    valueEditor->setText(QString::number(calibration.get(selected)));
}

void CalibrationEdit::coefficientChanged()
{
    int selected = listGroup->checkedId();
    if (selected < 0 || selected >= listButtons.size())
        return;

    bool ok = false;
    double v = valueEditor->text().toDouble(&ok);
    if (!ok || !FP::defined(v))
        return;

    calibration.set(selected, v);
    setButtonText(selected);

    emit calibrationChanged(calibration);
}

}
}
