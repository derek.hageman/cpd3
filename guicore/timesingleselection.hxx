/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUICORETIMESINGLESELECTION_H
#define CPD3GUICORETIMESINGLESELECTION_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QWidget>
#include <QLineEdit>
#include <QSettings>

#include "guicore/guicore.hxx"

#include "core/timeutils.hxx"

namespace CPD3 {
namespace GUI {

namespace Internal {
class SingleTimeValidator;

class SingleTimeEntry;
}

/**
 * Provides a widget to select a single time.
 */
class CPD3GUICORE_EXPORT TimeSingleSelection : public QWidget {
Q_OBJECT

    /**
     * This property holds if the time can be undefined.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setAllowUndefined(bool)
     *  <li> bool allowsUndefined() const
     * </ul>
     */
    Q_PROPERTY(bool allowUndefined
                       READ getAllowUndefined
                       WRITE
                       setAllowUndefined)

    /**
     * This property holds the minimum time the selection can have, inclusive.
     * This is undefined (infinite) by default.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setMinimum(double)
     *  <li> double getMinimum() const
     * </ul>
     */
    Q_PROPERTY(double minimum
                       READ getMinimum
                       WRITE
                       setMinimum)

    /**
     * This property holds the maximum time the selection can have, inclusive.
     * This is undefined (infinite) by default.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setMaximum(double)
     *  <li> double getMaximum() const
     * </ul>
     */
    Q_PROPERTY(double maximum
                       READ getMaximum
                       WRITE
                       setMaximum)

    /**
     * This property holds the the default unit used when parsing times.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setUnit(Time::LogicalTimeUnit)
     *  <li> Time::LogicalTimeUnit getUnit() const
     * </ul>
     */
    Q_PROPERTY(CPD3::Time::LogicalTimeUnit unit
                       READ getUnit
                       WRITE
                       setUnit)

    /**
     * This property holds the current time.  This is undefined by
     * default.  This is an inconstant state, unless the default of not allowing
     * undefined times is changed, so it must be set to something valid.
     * Changing the value will cause the corresponding signals to be emitted.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setTime(double)
     *  <li> double getTime() const
     * </ul>
     * Notifier signal:
     * <ul>
     *   <li> void changed( double )
     * </ul>
     */
    Q_PROPERTY(double time
                       READ getTime
                       WRITE setTime
                       NOTIFY changed
                       DESIGNABLE
                       false
                       USER
                       true)

    bool allowUndefined;

    double minimum;
    double maximum;
    CPD3::Time::LogicalTimeUnit unit;

    double time;

    QSettings settings;
    QLineEdit *entry;

    friend class Internal::SingleTimeValidator;

    friend class Internal::SingleTimeEntry;

    QString reformatTime(double time) const;

    void updateDisplay();

public:
    /**
     * Construct a time selection.
     * 
     * @param parent    the parent widget
     */
    TimeSingleSelection(QWidget *parent = 0);

    /**
     * Construct a time selection with the specified initial time.
     * 
     * @param time      the initial time
     * @param parent    the parent widget
     */
    TimeSingleSelection(double time, QWidget *parent = 0);

    virtual ~TimeSingleSelection();

    /**
     * Test if the selection allows undefined times.
     * 
     * @return true if undefined times are permitted
     */
    bool getAllowUndefined() const;

    /**
     * Get the minimum allowed time.
     * 
     * @return  the minimum accepted time
     */
    double getMinimum() const;

    /**
     * Get the maximum allowed time.
     * 
     * @return  the maximum accepted time
     */
    double getMaximum() const;

    /**
     * Get the default unit of the select.
     * 
     * @return the current default unit
     */
    Time::LogicalTimeUnit getUnit() const;

    /**
     * Get the current time.
     * 
     * @return  the currently selected time
     */
    double getTime() const;

    virtual QSize sizeHint() const;

    virtual QSize minimumSizeHint() const;

protected:
    virtual void keyReleaseEvent(QKeyEvent *event);

    virtual void keyPressEvent(QKeyEvent *event);

public slots:

    /**
     * Set if undefined times are permitted.
     * 
     * @param allow true to allow undefined times
     */
    void setAllowUndefined(bool allow);

    /**
     * Set the minimum accepted time.
     * 
     * @param min   the minimum time
     */
    void setMinimum(double min);

    /**
     * Set the maximum accepted time.
     * 
     * @param max   the maximum time
     */
    void setMaximum(double min);

    /**
     * Set the current time.
     * 
     * @param time  the new time
     */
    void setTime(double time);

    /**
     * Set the unit of the selection.
     * 
     * @param set   the new unit
     */
    void setUnit(Time::LogicalTimeUnit set);

signals:

    /**
     * Emitted when the time changes.
     * 
     * @param time  the new time
     */
    void changed(double time);

private slots:

    void broadcastUpdate();
};

}
}

#endif
