/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QVBoxLayout>
#include <QInputDialog>
#include <QStringList>

#include "guicore/setselect.hxx"

/** @file guicore/setselect.hxx
 * Provides an editing widget for selecting sets of items.
 */

namespace CPD3 {
namespace GUI {


SetSelector::SetSelector(QWidget *parent) : QWidget(parent),
                                            allowAdd(true),
                                            emptyText(tr("...", "empty text"))
{
    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->setContentsMargins(0, 0, 0, 0);
    setLayout(layout);

    button = new QPushButton(emptyText, this);
    layout->addWidget(button);

    /* So that the virtual function is called after the sub-class initialization */
    QMetaObject::invokeMethod(this, "updateButtonText", Qt::QueuedConnection);

    menu = new QMenu(button);
    connect(menu, SIGNAL(triggered(QAction * )), this, SLOT(menuActionTriggered(QAction * )));

    separatorAction = menu->addSeparator();
    addAction = menu->addAction(tr("Add"));
    connect(addAction, SIGNAL(triggered(bool)), this, SLOT(addItemSelected()));

    actionGroup = new QActionGroup(menu);
    actionGroup->setExclusive(false);

    updateButtonState();
}

SetSelector::~SetSelector()
{ }

void SetSelector::updateButtonState()
{
    button->disconnect(this);
    button->setEnabled(true);

    if (items.isEmpty()) {
        button->setMenu(0);
        if (!allowAdd) {
            button->setEnabled(false);
            return;
        }

        connect(button, SIGNAL(clicked(bool)), this, SLOT(addItemSelected()));
        return;
    }

    button->setMenu(menu);

    separatorAction->setVisible(allowAdd);
    addAction->setVisible(allowAdd);
}

void SetSelector::updateButtonText()
{
    button->setText(getButtonText());
}

void SetSelector::addItemSelected()
{
    QString data(QInputDialog::getText(this, tr("Add"), tr("Value:")));
    data = data.trimmed();
    if (data.isEmpty())
        return;
    addSelected(data);
}

void SetSelector::menuActionTriggered(QAction *action)
{
    QString name(action->property("set-name").toString());
    if (name.isEmpty())
        return;

    updateButtonText();

    if (action->isChecked()) {
        emit itemSelected(name);
    } else {
        emit itemUnselected(name);
    }
    emit selectionChanged();
}

QString SetSelector::getButtonText() const
{
    QStringList selectedText;
    QList<QAction *> actions(menu->actions());
    for (QList<QAction *>::const_iterator a = actions.constBegin(), endA = actions.constEnd();
            a != endA;
            ++a) {
        if (!(*a)->isChecked())
            continue;
        QString name((*a)->property("set-name").toString());
        if (name.isEmpty())
            continue;
        selectedText.append((*a)->text());
    }
    if (selectedText.isEmpty())
        return emptyText;
    return selectedText.join(tr(",", "selected join"));
}

void SetSelector::setAllowAdd(bool enable)
{
    allowAdd = enable;
    updateButtonState();
}

bool SetSelector::getAllowAdd() const
{ return allowAdd; }

void SetSelector::setEmptyText(const QString &text)
{
    emptyText = text;
    updateButtonText();
}

QString SetSelector::getEmptyText() const
{ return emptyText; }

void SetSelector::setExclusive(bool enable)
{ actionGroup->setExclusive(enable); }

bool SetSelector::getExclusive() const
{ return actionGroup->isExclusive(); }

QVariant SetSelector::getItemData(const QString &name) const
{
    QAction *action = items.value(name, NULL);
    if (action == NULL)
        return QVariant();
    return action->property("set-data");
}

void SetSelector::clear()
{
    for (QHash<QString, QAction *>::const_iterator i = items.constBegin(), endI = items.constEnd();
            i != endI;
            ++i) {
        menu->removeAction(i.value());
        actionGroup->removeAction(i.value());
        delete i.value();
    }
    items.clear();

    updateButtonText();
    updateButtonState();

    emit selectionChanged();
}

void SetSelector::clearSelection()
{
    for (QHash<QString, QAction *>::const_iterator i = items.constBegin(), endI = items.constEnd();
            i != endI;
            ++i) {
        i.value()->setChecked(false);
    }
    updateButtonText();
    updateButtonState();

    emit selectionChanged();
}

QSet<QString> SetSelector::getSelected() const
{
    QSet<QString> selected;
    for (QHash<QString, QAction *>::const_iterator i = items.constBegin(), endI = items.constEnd();
            i != endI;
            ++i) {
        if (!i.value()->isChecked())
            continue;
        selected.insert(i.key());
    }
    return selected;
}

QHash<QString, QVariant> SetSelector::getSelectedData() const
{
    QHash<QString, QVariant> selected;
    for (QHash<QString, QAction *>::const_iterator i = items.constBegin(), endI = items.constEnd();
            i != endI;
            ++i) {
        if (!i.value()->isChecked())
            continue;
        selected.insert(i.key(), i.value()->property("set-data"));
    }
    return selected;
}

void SetSelector::setSelected(const QSet<QString> &items)
{
    QStringList sorted(items.toList());
    for (QStringList::const_iterator i = sorted.constBegin(), endI = sorted.constEnd();
            i != endI;
            ++i) {
        addSelected(*i);
    }
}

void SetSelector::addSelected(const QString &name, const QVariant &data)
{ addItem(name, name, data, true); }

void SetSelector::setItemToolTip(const QString &name, const QString &toolTip)
{
    QAction *action = items.value(name, NULL);
    if (action == NULL)
        return;
    action->setToolTip(toolTip);
}

void SetSelector::setItemStatusTip(const QString &name, const QString &statusTip)
{
    QAction *action = items.value(name, NULL);
    if (action == NULL)
        return;
    action->setStatusTip(statusTip);
}

void SetSelector::setItemWhatsThis(const QString &name, const QString &whatsThis)
{
    QAction *action = items.value(name, NULL);
    if (action == NULL)
        return;
    action->setWhatsThis(whatsThis);
}

void SetSelector::addItem(const QString &name,
                          const QString &text,
                          const QVariant &data,
                          bool selected)
{
    QAction *action = items.value(name, NULL);
    if (action == NULL) {
        action = new QAction(text, menu);
        action->setProperty("set-name", name);
        action->setProperty("set-data", data);
        action->setCheckable(true);
        actionGroup->addAction(action);
        action->setChecked(selected);
        menu->insertAction(separatorAction, action);
        items.insert(name, action);
    } else {
        action->setProperty("set-data", data);
        action->setChecked(selected);
    }

    updateButtonText();
    updateButtonState();

    if (action->isChecked()) {
        emit itemSelected(name);
    } else {
        emit itemUnselected(name);
    }
    emit selectionChanged();
}


}
}
