/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUICOREACTIONPROGRESS_H
#define CPD3GUICOREACTIONPROGRESS_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QProgressDialog>
#include <QSettings>

#include "guicore/guicore.hxx"
#include "core/number.hxx"
#include "core/timeutils.hxx"
#include "core/actioncomponent.hxx"

namespace CPD3 {
namespace GUI {


/**
 * A dialog presenting a progress indicator for a system action.
 */
class CPD3GUICORE_EXPORT ActionProgressDialog : public QProgressDialog {
Q_OBJECT

    double start;
    double end;

    ActionFeedback::Serializer incoming;

    QSettings *settings;
    QLabel *label;

    void feedbackUpdate();

public:
    /**
     * Create the progress dialog.
     * 
     * @param parent        the parent widget
     * @param f             the window flags
     * @param settings      the settings object if available
     */
    ActionProgressDialog(QWidget *parent = 0, Qt::WindowFlags f = 0, QSettings *settings = 0);

    /**
     * Attach the progress to a given source.
     *
     * @param source    the feedback source
     */
    void attach(ActionFeedback::Source &source);

    /**
     * Set the time bounds that the dialog is used for.  These are used to
     * generate the progress range
     * 
     * @param bounds    the time bounds
     */
    inline void setTimeBounds(const Time::Bounds &bounds)
    { setTimeBounds(bounds.getStart(), bounds.getEnd()); }

public slots:

    /**
     * Set the time bounds that the dialog is used for.  These are used to
     * generate the progress range
     * 
     * @param start     the start time
     * @param end       the end time
     */
    void setTimeBounds(double start, double end);

    /**
     * Set the end time bound.  This is used to generate the progress range.
     * 
     * @param start     the start time
     */
    void setStart(double start);

    /**
     * Set the end time bound.  This is used to generate the progress range.
     * 
     * @param start     the start time
     */
    void setEnd(double end);
};

/**
 * A connection component that handles action progress within a status bar
 * indicator.
 */
class CPD3GUICORE_EXPORT ActionProgressStatusBar : public QObject {
Q_OBJECT

    double start;
    double end;

    ActionFeedback::Serializer incoming;

    QSettings *settings;
    QProgressBar *bar;
    QLabel *label;

    void feedbackUpdate();

public:
    /**
     * Create the status bar connection.
     * 
     * @param bar           the progress bar if any
     * @param label         the label if any
     * @param parent        the parent object
     * @param settings      the settings object if available
     */
    ActionProgressStatusBar
            (QProgressBar *bar, QLabel *label, QObject *parent = 0, QSettings *settings = 0);

    /**
     * Attach the progress to a given source.
     *
     * @param source    the feedback source
     */
    void attach(ActionFeedback::Source &source);

    /**
     * Set the time bounds that the dialog is used for.  These are used to
     * generate the progress range
     * 
     * @param bounds    the time bounds
     */
    inline void setTimeBounds(const Time::Bounds &bounds)
    { setTimeBounds(bounds.getStart(), bounds.getEnd()); }

public slots:

    /**
     * Set the time bounds that the dialog is used for.  These are used to
     * generate the progress range
     * 
     * @param start     the start time
     * @param end       the end time
     */
    void setTimeBounds(double start, double end);

    /**
     * Set the end time bound.  This is used to generate the progress range.
     * 
     * @param start     the start time
     */
    void setStart(double start);

    /**
     * Set the end time bound.  This is used to generate the progress range.
     * 
     * @param start     the start time
     */
    void setEnd(double end);

    /**
     * Initialize the progress indicator.
     */
    void initialize();

    /**
     * Finish and hide the progress indicator.
     */
    void finished();
};

}
}

#endif
