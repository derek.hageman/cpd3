/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUICORESCRIPTEDIT_H
#define CPD3GUICORESCRIPTEDIT_H

#include "core/first.hxx"

#include <vector>

#include <QtGlobal>
#include <QObject>
#include <QWidget>
#include <QTextEdit>
#include <QSyntaxHighlighter>
#include <QRegularExpression>
#include <QTextCharFormat>

#include "guicore/guicore.hxx"

namespace CPD3 {
namespace GUI {


/**
 * A widget that defines an editor for CPD3 script code.
 */
class CPD3GUICORE_EXPORT ScriptEdit : public QWidget {
Q_OBJECT

    /**
     * This property holds the current script code.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setCode( const QString & )
     *  <li> QString getCode() const
     * </ul>
     * Notifier signal:
     * <ul>
     *   <li> void scriptChanged( const QString & )
     * </ul>
     */
    Q_PROPERTY(QString code
                       READ getCode
                       WRITE setCode
                       NOTIFY scriptChanged
                       DESIGNABLE
                       false
                       USER
                       true)
    QString code;

    QTextEdit *editor;

    class Highlighter : public QSyntaxHighlighter {
        struct Rule {
            QRegularExpression pattern;
            QTextCharFormat format;
        };
        std::vector<Rule> highlightingRules;

        QTextCharFormat literalStringFormat;
        QRegularExpression literalStringBegin;
        QString literalStringEndPattern;

        QTextCharFormat blockCommentFormat;
        QRegularExpression blockCommentBegin;
        QString blockCommentEndPattern;

        Rule &installRule(const QString &pattern);

    public:
        Highlighter(QTextDocument *parent = 0);

        virtual ~Highlighter();

    protected:
        virtual void highlightBlock(const QString &text);
    };

public:
    ScriptEdit(QWidget *parent = 0);

    QString getCode() const;

public slots:

    void setCode(const QString &code);

signals:

    /**
     * Emitted whenever the script code changes.
     * 
     * @param code  the updated code
     */
    void scriptChanged(const QString &code);

private slots:

    void editorChanged();
};

}
}

#endif
