/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QValidator>
#include <QLineEdit>
#include <QSettings>
#include <QHBoxLayout>
#include <QKeyEvent>

#include "core/number.hxx"
#include "core/range.hxx"
#include "core/timeutils.hxx"
#include "core/timeparse.hxx"
#include "guicore/timeboundselection.hxx"
#include "guicore/guiformat.hxx"

namespace CPD3 {
namespace GUI {

/** @file guicore/timeboundselection.hxx
 * Provides a time bound selection widget.
 */

namespace Internal {
class BoundValidator : public QValidator {
    TimeBoundSelection *parent;

    QStringList doSplit(QString input) const
    {
        QString rep(TimeBoundSelection::tr("Start:", "time bound start"));
        input.replace(rep, QString(' ').repeated(rep.length()));
        rep = TimeBoundSelection::tr("End:", "time bound end");
        input.replace(rep, QString(' ').repeated(rep.length()));
        return input.split(' ', QString::SkipEmptyParts);
    }

public:
    BoundValidator(TimeBoundSelection *setParent) : QValidator(setParent), parent(setParent)
    { }

    virtual ~BoundValidator()
    { }

    virtual QValidator::State validate(QString &input, int &pos) const
    {
        Q_UNUSED(pos);

        QStringList list(doSplit(input));
        TimeParseNOOPListHandler handler;
        Time::Bounds bounds;
        try {
            bounds = TimeParse::parseListBounds(list, &handler, false,
                                                parent->allowsUndefinedStart() ||
                                                        parent->allowsUndefinedEnd());
        } catch (TimeParsingException tpe) {
            return QValidator::Intermediate;
        }
        if (!parent->allowsUndefinedStart() && !FP::defined(bounds.start))
            return QValidator::Intermediate;
        if (!parent->allowsUndefinedEnd() && !FP::defined(bounds.end))
            return QValidator::Intermediate;
        if (Range::notWithinStart(bounds.start, parent->getMinimumStart(),
                                  parent->getMaximumStart(), true, true))
            return QValidator::Intermediate;
        if (Range::notWithinEnd(bounds.end, parent->getMinimumEnd(), parent->getMaximumEnd(), true,
                                true))
            return QValidator::Intermediate;
        return QValidator::Acceptable;
    }

    virtual void fixup(QString &input) const
    {
        QStringList list(doSplit(input));
        TimeParseNOOPListHandler handler;
        Time::Bounds bounds;
        try {
            bounds = TimeParse::parseListBounds(list, &handler, false,
                                                parent->allowsUndefinedStart() ||
                                                        parent->allowsUndefinedEnd());
        } catch (TimeParsingException tpe) {
            return;
        }

        if (!parent->allowsUndefinedStart() && !FP::defined(bounds.start)) {
            if (FP::defined(parent->getMinimumStart()))
                bounds.start = parent->getMinimumStart();
            else
                return;
        }
        if (!parent->allowsUndefinedEnd() && !FP::defined(bounds.end)) {
            if (FP::defined(parent->getMaximumEnd()))
                bounds.end = parent->getMaximumEnd();
            else
                return;
        }
        int cr = Range::notWithinStart(bounds.start, parent->getMinimumStart(),
                                       parent->getMaximumStart(), true, true);
        if (cr < 0) bounds.start = parent->getMinimumStart(); else if (cr > 0)
            bounds.start = parent->getMaximumStart();
        cr = Range::notWithinEnd(bounds.end, parent->getMinimumEnd(), parent->getMaximumEnd(), true,
                                 true);
        if (cr < 0) bounds.end = parent->getMinimumEnd(); else if (cr > 0)
            bounds.end = parent->getMaximumEnd();

        input = parent->reformatBounds(bounds.start, bounds.end, false);
    }
};

class LabeledEntry : public QLineEdit {
    TimeBoundSelection *parent;
public:
    LabeledEntry(TimeBoundSelection *setParent) : QLineEdit(setParent), parent(setParent)
    { }

    virtual ~LabeledEntry()
    { }

protected:
    virtual void focusInEvent(QFocusEvent *e)
    {
        if (parent->getDisableWindowFocus()) {
            switch (e->reason()) {
            case Qt::ActiveWindowFocusReason:
            case Qt::PopupFocusReason:
                clearFocus();
                return;
            default:
                break;
            }
        }

        QString input(text());
        QString rep(TimeBoundSelection::tr("Start:", "time bound start"));
        input.replace(rep, QString(' ').repeated(rep.length()));
        rep = TimeBoundSelection::tr("End:", "time bound end");
        input.replace(rep, QString(' ').repeated(rep.length()));
        setText(input);

        QLineEdit::focusInEvent(e);
    }

    virtual void focusOutEvent(QFocusEvent *e)
    {
        QLineEdit::focusOutEvent(e);
        setText(parent->reformatBounds(parent->getStart(), parent->getEnd(), true));
    }

    void resizeEvent(QResizeEvent *e)
    {
        QLineEdit::resizeEvent(e);
        if (!hasFocus()) {
            setText(parent->reformatBounds(parent->getStart(), parent->getEnd(), true));
        }
    }
};
}

QString TimeBoundSelection::reformatBounds(double start, double end, bool includeLabels) const
{
    QString display;
    if (!includeLabels) {
        display.append(QString(' ').repeated(tr("Start: ", "time bound start label").length()));
    } else {
        display.append(tr("Start: ", "time bound start label"));
    }
    display.append(GUITime::formatTime(start, &settings, true));

    QFontMetrics fm(entry->font());
    int charWidth = fm.width(' ');
    if (charWidth < 1) charWidth = 1;
    int nCenterChar = (entry->width() / 2) / charWidth;
    int nAdd = nCenterChar - display.length();
    if (nAdd < 1) nAdd = 1;
    display.append(QString(' ').repeated(nAdd));

    if (!includeLabels) {
        display.append(QString(' ').repeated(tr("End: ", "time bound end label").length()));
    } else {
        display.append(tr("End: ", "time bound end label"));
    }
    display.append(GUITime::formatTime(end, &settings, true));
    return display;
}

/**
 * Creates a bound selection widget with the initial start and end times
 * as infinity.  Note that this is an invalid state (since the default does
 * not allow those as valid bounds).
 * 
 * @param parent    the parent object
 */
TimeBoundSelection::TimeBoundSelection(QWidget *parent) : QWidget(parent),
                                                          allowUndefinedStart(false),
                                                          allowUndefinedEnd(false),
                                                          allowZeroSize(false),
                                                          disableWindowFocus(false),
                                                          minimumStart(FP::undefined()),
                                                          maximumStart(FP::undefined()),
                                                          minimumEnd(FP::undefined()),
                                                          maximumEnd(FP::undefined()),
                                                          start(FP::undefined()),
                                                          end(FP::undefined()),
                                                          settings(CPD3GUI_ORGANIZATION,
                                                                   CPD3GUI_APPLICATION)
{
    entry = new Internal::LabeledEntry(this);
    QFont f(entry->font());
    f.setFamily("Monospace");
    f.setStyleHint(QFont::TypeWriter);
    f.setBold(true);
    entry->setFont(f);
    entry->setText(reformatBounds(start, end, true));
    entry->setValidator(new Internal::BoundValidator(this));
    entry->setSizePolicy(
            QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Ignored, QSizePolicy::LineEdit));

    QHBoxLayout *layout = new QHBoxLayout;
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(entry);
    layout->setSpacing(0);
    layout->setMargin(0);
    setLayout(layout);

    connect(entry, SIGNAL(editingFinished()), this, SLOT(broadcastUpdate()));
}

/**
 * Creates a bound selection widget with the initial start and end times
 * as specified.
 * 
 * @param setStart  the initial start time
 * @param setEnd    the initial end time
 * @param parent    the parent object
 */
TimeBoundSelection::TimeBoundSelection(double setStart, double setEnd, QWidget *parent) : QWidget(
        parent),
                                                                                          allowUndefinedStart(
                                                                                                  false),
                                                                                          allowUndefinedEnd(
                                                                                                  false),
                                                                                          allowZeroSize(
                                                                                                  false),
                                                                                          minimumStart(
                                                                                                  FP::undefined()),
                                                                                          maximumStart(
                                                                                                  FP::undefined()),
                                                                                          minimumEnd(
                                                                                                  FP::undefined()),
                                                                                          maximumEnd(
                                                                                                  FP::undefined()),
                                                                                          start(setStart),
                                                                                          end(setEnd),
                                                                                          settings(
                                                                                                  CPD3GUI_ORGANIZATION,
                                                                                                  CPD3GUI_APPLICATION)
{
    entry = new Internal::LabeledEntry(this);
    QFont f(entry->font());
    f.setFamily("Monospace");
    f.setStyleHint(QFont::TypeWriter);
    f.setBold(true);
    entry->setFont(f);
    entry->setText(reformatBounds(start, end, true));
    entry->setValidator(new Internal::BoundValidator(this));
    entry->setSizePolicy(
            QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Ignored, QSizePolicy::LineEdit));

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(entry);
    layout->setSpacing(0);
    layout->setMargin(0);
    setLayout(layout);

    connect(entry, SIGNAL(editingFinished()), this, SLOT(broadcastUpdate()));
}

TimeBoundSelection::~TimeBoundSelection()
{
}

void TimeBoundSelection::setAllowUndefinedStart(bool allow)
{ allowUndefinedStart = allow; }

void TimeBoundSelection::setAllowUndefinedEnd(bool allow)
{ allowUndefinedEnd = allow; }

bool TimeBoundSelection::allowsUndefinedStart() const
{ return allowUndefinedStart; }

bool TimeBoundSelection::allowsUndefinedEnd() const
{ return allowUndefinedEnd; }

void TimeBoundSelection::setAllowZeroSize(bool allow)
{ allowZeroSize = allow; }

bool TimeBoundSelection::allowsZeroSize() const
{ return allowZeroSize; }

void TimeBoundSelection::setDisableWindowFocus(bool disable)
{ disableWindowFocus = disable; }

bool TimeBoundSelection::getDisableWindowFocus() const
{ return disableWindowFocus; }

void TimeBoundSelection::setMinimumStart(double m)
{
    minimumStart = m;
    if (Range::compareStart(start, m) < 0) {
        entry->clearFocus();
        start = m;
        entry->setText(GUITime::formatRange(start, end, &settings));
        emit startChanged(start);
        emit boundsChanged(start, end);
    }
}

void TimeBoundSelection::setMaximumStart(double m)
{
    maximumStart = m;
    if (Range::compareStartEnd(start, m) > 0) {
        entry->clearFocus();
        start = m;
        entry->setText(reformatBounds(start, end, true));
        emit startChanged(start);
        emit boundsChanged(start, end);
    }
}

void TimeBoundSelection::setMinimumEnd(double m)
{
    minimumEnd = m;
    if (Range::compareStartEnd(m, end) < 0) {
        entry->clearFocus();
        end = m;
        entry->setText(reformatBounds(start, end, true));
        emit endChanged(end);
        emit boundsChanged(start, end);
    }
}

void TimeBoundSelection::setMaximumEnd(double m)
{
    maximumEnd = m;
    if (Range::compareEnd(end, m) > 0) {
        entry->clearFocus();
        end = m;
        entry->setText(reformatBounds(start, end, true));
        emit endChanged(end);
        emit boundsChanged(start, end);
    }
}

double TimeBoundSelection::getMinimumStart() const
{ return minimumStart; }

double TimeBoundSelection::getMaximumStart() const
{ return maximumStart; }

double TimeBoundSelection::getMinimumEnd() const
{ return minimumEnd; }

double TimeBoundSelection::getMaximumEnd() const
{ return maximumEnd; }

double TimeBoundSelection::getStart() const
{ return start; }

double TimeBoundSelection::getEnd() const
{ return end; }

void TimeBoundSelection::setStart(double v)
{
    entry->clearFocus();
    start = v;
    entry->setText(reformatBounds(start, end, true));

    emit startChanged(start);
    emit boundsChanged(start, end);
}

void TimeBoundSelection::setEnd(double v)
{
    entry->clearFocus();
    end = v;
    entry->setText(reformatBounds(start, end, true));

    emit endChanged(end);
    emit boundsChanged(start, end);
}

void TimeBoundSelection::setBounds(double s, double e)
{
    entry->clearFocus();
    start = s;
    end = e;
    entry->setText(reformatBounds(start, end, true));

    emit startChanged(start);
    emit endChanged(end);
    emit boundsChanged(start, end);
}

QSize TimeBoundSelection::sizeHint() const
{
    /* Probably need a better way of setting the width */
    return entry->sizeHint().expandedTo(QSize(440, 0));
}

QSize TimeBoundSelection::minimumSizeHint() const
{ return sizeHint(); }

void TimeBoundSelection::broadcastUpdate()
{
    if (!interpretText())
        return;
    entry->setText(reformatBounds(start, end, true));
}

/**
 * Interpret the text entered.  If the text is valid and the start or
 * end have changed then signals will be emitted.
 * 
 * @return true if the interpretation is valid
 */
bool TimeBoundSelection::interpretText()
{
    double oldStart = start;
    double oldEnd = end;

    QString input(entry->text());
    QString rep(tr("Start:", "time bound start"));
    input.replace(rep, QString(' ').repeated(rep.length()));
    rep = tr("End:", "time bound end");
    input.replace(rep, QString(' ').repeated(rep.length()));
    QStringList list(input.split(' ', QString::SkipEmptyParts));

    TimeParseNOOPListHandler handler;
    try {
        Time::Bounds bounds = TimeParse::parseListBounds(list, &handler, false,
                                                         allowsUndefinedStart() ||
                                                                 allowsUndefinedEnd());
        start = bounds.start;
        end = bounds.end;
    } catch (TimeParsingException tpe) {
        return false;
    }

    if (!FP::equal(start, oldStart)) {
        emit startChanged(start);
        emit startEdited(start);
    }
    if (!FP::equal(end, oldEnd)) {
        emit endChanged(end);
        emit endEdited(end);
    }
    if (!FP::equal(start, oldStart) || !FP::equal(end, oldEnd)) {
        emit boundsChanged(start, end);
        emit boundsEdited(start, end);
    }

    return true;
}

void TimeBoundSelection::keyReleaseEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter)
        return;
    QWidget::keyReleaseEvent(event);
}

void TimeBoundSelection::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter)
        return;
    QWidget::keyPressEvent(event);
}

}
}
