/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QValidator>
#include <QHBoxLayout>
#include <QKeyEvent>

#include "core/number.hxx"
#include "core/timeutils.hxx"
#include "core/timeparse.hxx"
#include "guicore/dynamictimeinterval.hxx"

namespace CPD3 {
namespace GUI {

/** @file guicore/timeintervalselection.hxx
 * Provides a time interval selection widget.
 */

namespace Internal {
class IntervalValidator : public QValidator {
    TimeIntervalSelection *parent;

public:
    IntervalValidator(TimeIntervalSelection *setParent) : QValidator(setParent), parent(setParent)
    { }

    virtual ~IntervalValidator()
    { }

    virtual QValidator::State validate(QString &input, int &pos) const
    {
        Q_UNUSED(pos);

        Time::LogicalTimeUnit unit = parent->getUnit();
        int count = parent->getCount();
        bool align = parent->getAlign();
        try {
            TimeParse::parseOffset(input, &unit, &count, &align, parent->getAllowZero(),
                                   parent->getAllowNegative());
        } catch (TimeParsingException tpe) {
            return QValidator::Intermediate;
        }

        return QValidator::Acceptable;
    }

    virtual void fixup(QString &input) const
    {
        Time::LogicalTimeUnit unit = parent->getUnit();
        int count = parent->getCount();
        bool align = parent->getAlign();
        try {
            TimeParse::parseOffset(input, &unit, &count, &align, parent->getAllowZero(),
                                   parent->getAllowNegative());
        } catch (TimeParsingException tpe) {
            return;
        }

        if (!parent->getAllowAlign())
            align = false;

        input = parent->reformatInterval(unit, count, align);
    }
};

class IntervalEntry : public QLineEdit {
    TimeIntervalSelection *parent;
public:
    IntervalEntry(TimeIntervalSelection *setParent) : QLineEdit(setParent), parent(setParent)
    { }

    virtual ~IntervalEntry()
    { }

protected:
    virtual void focusInEvent(QFocusEvent *e)
    {
        if (parent->getDisableWindowFocus()) {
            switch (e->reason()) {
            case Qt::ActiveWindowFocusReason:
            case Qt::PopupFocusReason:
                clearFocus();
                return;
            default:
                break;
            }
        }
        QLineEdit::focusInEvent(e);
    }

    virtual void focusOutEvent(QFocusEvent *e)
    {
        QLineEdit::focusOutEvent(e);
        setText(parent->reformatInterval(parent->getUnit(), parent->getCount(),
                                         parent->getAlign()));
    }
};
}

QString TimeIntervalSelection::reformatInterval(Time::LogicalTimeUnit unit,
                                                int count,
                                                bool align) const
{
    return Time::describeOffset(unit, count, align);
}

TimeIntervalSelection::TimeIntervalSelection(QWidget *parent) : QWidget(parent),
                                                                allowZero(false),
                                                                allowNegative(false),
                                                                allowAlign(true),
                                                                unit(Time::Minute),
                                                                count(1),
                                                                align(false)
{
    entry = new Internal::IntervalEntry(this);
    QFont f(entry->font());
    f.setFamily("Courier");
    f.setStyleHint(QFont::TypeWriter);
    f.setBold(true);
    entry->setFont(f);
    entry->setText(reformatInterval(unit, count, align));
    entry->setValidator(new Internal::IntervalValidator(this));
    entry->setSizePolicy(
            QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Ignored, QSizePolicy::LineEdit));

    QHBoxLayout *layout = new QHBoxLayout;
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(entry);
    layout->setSpacing(0);
    layout->setMargin(0);
    setLayout(layout);

    connect(entry, SIGNAL(editingFinished()), this, SLOT(broadcastUpdate()));
}

TimeIntervalSelection::TimeIntervalSelection(Time::LogicalTimeUnit setUnit,
                                             int setCount,
                                             bool setAligned,
                                             QWidget *parent) : QWidget(parent),
                                                                allowZero(false),
                                                                allowNegative(false),
                                                                allowAlign(true),
                                                                disableWindowFocus(false),
                                                                unit(setUnit),
                                                                count(setCount),
                                                                align(setAligned)
{
    entry = new Internal::IntervalEntry(this);
    QFont f(entry->font());
    f.setFamily("Monospace");
    f.setStyleHint(QFont::TypeWriter);
    f.setBold(true);
    entry->setFont(f);
    entry->setText(reformatInterval(unit, count, align));
    entry->setValidator(new Internal::IntervalValidator(this));
    entry->setSizePolicy(
            QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Ignored, QSizePolicy::LineEdit));

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(entry);
    layout->setSpacing(0);
    layout->setMargin(0);
    setLayout(layout);

    connect(entry, SIGNAL(editingFinished()), this, SLOT(broadcastUpdate()));
}

TimeIntervalSelection::~TimeIntervalSelection()
{
}

void TimeIntervalSelection::updateDisplay()
{
    entry->setText(reformatInterval(unit, count, align));
}

void TimeIntervalSelection::broadcastUpdate()
{
    bool unitUpdated = false;
    bool countUpdated = false;
    bool alignUpdated = false;

    try {
        Time::LogicalTimeUnit nextUnit = unit;
        int nextCount = count;
        bool nextAlign = align;
        TimeParse::parseOffset(entry->text(), &nextUnit, &nextCount, &nextAlign, allowZero,
                               allowNegative);
        if (!allowAlign)
            nextAlign = false;

        unitUpdated = (nextUnit != unit);
        countUpdated = (nextCount != count);
        alignUpdated = (nextAlign != align);

        unit = nextUnit;
        count = nextCount;
        align = nextAlign;
    } catch (TimeParsingException tpe) {
    }

    if (unitUpdated)
            emit unitChanged(unit);
    if (countUpdated)
            emit countChanged(count);
    if (alignUpdated)
            emit alignChanged(align);
    if (unitUpdated || countUpdated || alignUpdated)
            emit intervalChanged(unit, count, align);

    entry->setText(reformatInterval(unit, count, align));
}

QSize TimeIntervalSelection::sizeHint() const
{
    return entry->sizeHint().expandedTo(QSize(150, 0));
}

QSize TimeIntervalSelection::minimumSizeHint() const
{ return sizeHint(); }

void TimeIntervalSelection::keyReleaseEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter)
        return;
    QWidget::keyReleaseEvent(event);
}

void TimeIntervalSelection::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter)
        return;
    QWidget::keyPressEvent(event);
}

bool TimeIntervalSelection::getAllowZero() const
{ return allowZero; }

void TimeIntervalSelection::setAllowZero(bool allow)
{
    allowZero = allow;
    if (!allowZero && count == 0) {
        count = 1;
        updateDisplay();
        emit countChanged(count);
        emit intervalChanged(unit, count, align);
    }
}

bool TimeIntervalSelection::getAllowNegative() const
{ return allowNegative; }

void TimeIntervalSelection::setAllowNegative(bool allow)
{
    allowNegative = allow;
    if (!allowNegative && count < 0) {
        if (allowZero)
            count = 0;
        else
            count = 1;
        updateDisplay();
        emit countChanged(count);
        emit intervalChanged(unit, count, align);
    }
}

bool TimeIntervalSelection::getAllowAlign() const
{ return allowAlign; }

void TimeIntervalSelection::setAllowAlign(bool allow)
{
    allowAlign = allow;
    if (!allowAlign && align) {
        align = false;
        updateDisplay();
        emit alignChanged(align);
        emit intervalChanged(unit, count, align);
    }
}

void TimeIntervalSelection::setDisableWindowFocus(bool disable)
{ disableWindowFocus = disable; }

bool TimeIntervalSelection::getDisableWindowFocus() const
{ return disableWindowFocus; }

Time::LogicalTimeUnit TimeIntervalSelection::getUnit() const
{ return unit; }

int TimeIntervalSelection::getCount() const
{ return count; }

bool TimeIntervalSelection::getAlign() const
{ return align; }

Time::LogicalTimeUnit TimeIntervalSelection::getInterval(int *outputCount, bool *outputAlign) const
{
    if (outputCount != NULL) *outputCount = count;
    if (outputAlign != NULL) *outputAlign = align;
    return unit;
}

Time::LogicalTimeUnit TimeIntervalSelection::getInterval(int &outputCount, bool &outputAlign) const
{
    outputCount = count;
    outputAlign = align;
    return unit;
}

void TimeIntervalSelection::setUnit(Time::LogicalTimeUnit set)
{
    if (unit == set)
        return;

    unit = set;
    updateDisplay();
    emit unitChanged(unit);
    emit intervalChanged(unit, count, align);
}

void TimeIntervalSelection::setCount(int set)
{
    if ((set < 0 && !allowNegative) || (set == 0 && !allowZero))
        return;
    if (set == count)
        return;

    count = set;
    updateDisplay();
    emit countChanged(count);
    emit intervalChanged(unit, count, align);
}

void TimeIntervalSelection::setAlign(bool set)
{
    if (!allowAlign)
        return;
    if (set == align)
        return;

    align = set;
    updateDisplay();
    emit alignChanged(align);
    emit intervalChanged(unit, count, align);
}

void TimeIntervalSelection::setInterval(Time::LogicalTimeUnit setUnit, int setCount, bool setAlign)
{
    if ((setCount < 0 && !allowNegative) || (setCount == 0 && !allowZero))
        setCount = count;
    if (setAlign && !allowAlign)
        setAlign = false;

    bool unitUpdated = (setUnit != unit);
    bool countUpdated = (setCount != count);
    bool alignUpdated = (setAlign != align);
    if (!unitUpdated && !countUpdated && !alignUpdated)
        return;

    unit = setUnit;
    count = setCount;
    align = setAlign;

    updateDisplay();

    if (unitUpdated)
            emit unitChanged(unit);
    if (countUpdated)
            emit countChanged(count);
    if (alignUpdated)
            emit alignChanged(align);
    emit intervalChanged(unit, count, align);
}

void TimeIntervalSelection::setFrame(bool frame)
{ entry->setFrame(frame); }

void TimeIntervalSelection::interpretText()
{ broadcastUpdate(); }

}
}
