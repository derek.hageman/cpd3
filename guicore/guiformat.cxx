/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QSettings>

#include "core/timeutils.hxx"
#include "core/number.hxx"
#include "guicore/guiformat.hxx"

namespace CPD3 {
namespace GUI {

/** @file guicore/guiformat.hxx
 * Provides utilities to deal with GUI specific formatting.
 */

/**
 * Returns a single time formatted to the common format (read from the settings,
 * if available).
 * 
 * @param time              the time to format
 * @param settings          the settings to use or NULL
 * @param allowSpaces       true if spaces are allowed in the format
 * @param dropDOYAsNeeded   in year and DOY mode, allow for dropping the DOY as needed
 * @return  the formatted time
 */
QString GUITime::formatTime(double time,
                            const QSettings *settings,
                            bool allowSpaces,
                            bool dropDOYAsNeeded)
{
    if (!FP::defined(time))
        return tr("None", "time format none");

    QString format;
    if (settings != NULL)
        format = settings->value("time/displayformat").toString().toLower();
    if (format == "datetime") {
        return Time::toISO8601(time);
    } else {
        if (allowSpaces) {
            return Time::toYearDOY(time, QString(' '), Time::Year, QChar(),
                                   dropDOYAsNeeded ? Time::DOYAsNeeded : Time::YearAndDOY);
        } else {
            return Time::toYearDOY(time, QString(':'), Time::Year, QChar(),
                                   dropDOYAsNeeded ? Time::DOYAsNeeded : Time::YearAndDOY);
        }
    }
}

/**
 * Returns a single time formatted to the common format (read from the settings,
 * if available).
 * 
 * @param time              the time to format
 * @param settings          the settings to use or NULL
 * @param format            the default format
 * @param allowSpaces       true if spaces are allowed in the format
 * @param dropDOYAsNeeded   in year and DOY mode, allow for dropping the DOY as needed
 * @return  the formatted time
 */
QString GUITime::formatTime(double time,
                            const QSettings *settings,
                            const QString &format,
                            bool allowSpaces,
                            bool dropDOYAsNeeded)
{
    if (!FP::defined(time))
        return tr("None", "time format none");

    QString fmt(format.toLower());
    if (fmt.isEmpty() && settings != NULL)
        fmt = settings->value("time/displayformat").toString().toLower();
    if (fmt == "datetime") {
        return Time::toISO8601(time);
    } else {
        if (allowSpaces) {
            return Time::toYearDOY(time, QString(' '), Time::Year, QChar(),
                                   dropDOYAsNeeded ? Time::DOYAsNeeded : Time::YearAndDOY);
        } else {
            return Time::toYearDOY(time, QString(':'), Time::Year, QChar(),
                                   dropDOYAsNeeded ? Time::DOYAsNeeded : Time::YearAndDOY);
        }
    }
}

/**
 * Returns the formatted time range in the common format (read from the 
 * settings, if available).
 * 
 * @param start             the start of the range
 * @param end               the end of the range
 * @param settings          the settings to use or NULL
 * @param allowTimeSpaces   true if the times can have spaces (the bounds always do)
 * @param dropDOYAsNeeded   in year and DOY mode, allow for dropping the DOY as needed
 * @return the formatted time range
 */
QString GUITime::formatRange(double start,
                             double end,
                             const QSettings *settings,
                             bool allowTimeSpaces,
                             bool dropDOYAsNeeded)
{
    QString result(formatTime(start, settings, allowTimeSpaces, dropDOYAsNeeded));
    result.append(' ');
    result.append(formatTime(end, settings, allowTimeSpaces, dropDOYAsNeeded));
    return result;
}

/**
 * Returns the formatted time range in the common format (read from the 
 * settings, if available).
 * 
 * @param start             the start of the range
 * @param end               the end of the range
 * @param settings          the settings to use or NULL
 * @param format            the default format
 * @param allowTimeSpaces   true if the times can have spaces (the bounds always do)
 * @param dropDOYAsNeeded   in year and DOY mode, allow for dropping the DOY as needed
 * @return the formatted time range
 */
QString GUITime::formatRange(double start,
                             double end,
                             const QSettings *settings,
                             const QString &format,
                             bool allowTimeSpaces,
                             bool dropDOYAsNeeded)
{
    QString result(formatTime(start, settings, format, allowTimeSpaces, dropDOYAsNeeded));
    result.append(' ');
    result.append(formatTime(end, settings, format, allowTimeSpaces, dropDOYAsNeeded));
    return result;
}

/**
 * Returns the maximum dimensions of all the characters in the given string.
 * This is needed since the various QFontMetrics functions do not base
 * their output on the actual characters but use the standard for the font
 * (which is often much to large in the context of labels).
 * 
 * @param str       the string to use
 * @param fm        the font metrics to use
 * @return          the maximum dimensions of any character in the string
 */
QRect GUIStringLayout::maximumCharacterDimensions(const QString &str, const QFontMetrics &fm)
{
    QRect dims(0, 0, 0, 0);
    for (const auto &c : str) {
        dims = dims.united(fm.boundingRect(c));
    }
    return dims;
}

/**
 * Perform alignment on a text rectangle such that the bottom of the text is exactly aligned
 * with the origin point.  This is needed because the bounding rectangle functions all calculate
 * based on the font height, which does not consider the actual character contents.
 *
 * @param painter   the painter to use
 * @param origin    the origin of the text
 * @param flags     the text flags
 * @param str       the string to format
 */
QRectF GUIStringLayout::alignTextBottom(QPainter *painter,
                                        const QRectF &origin,
                                        int flags,
                                        const QString &str)
{
    auto fm = painter->fontMetrics();
    auto dims = maximumCharacterDimensions(str, fm);
    auto realHeightAboveBaseline = -dims.y();
    auto fontHeightAboveBaseline = fm.ascent();
    auto effectiveAdditionalHeight = fontHeightAboveBaseline - realHeightAboveBaseline;
    auto rect = painter->boundingRect(origin, flags, str);
    rect.translate(0, -(dims.height() + effectiveAdditionalHeight));
    return rect;
}

/**
 * Perform alignment on a text rectangle such that the top of the text is exactly aligned
 * with the origin point.  This is needed because the bounding rectangle functions all calculate
 * based on the font height, which does not consider the actual character contents.
 *
 * @param painter   the painter to use
 * @param origin    the origin of the text
 * @param flags     the text flags
 * @param str       the string to format
 */
QRectF GUIStringLayout::alignTextTop(QPainter *painter,
                                     const QRectF &origin,
                                     int flags,
                                     const QString &str)
{
    auto fm = painter->fontMetrics();
    auto dims = maximumCharacterDimensions(str, fm);
    auto realHeightAboveBaseline = -dims.y();
    auto fontHeightAboveBaseline = fm.ascent();
    auto effectiveAdditionalHeight = fontHeightAboveBaseline - realHeightAboveBaseline;
    auto rect = painter->boundingRect(origin, flags, str);
    rect.translate(0, -effectiveAdditionalHeight);
    return rect;
}

}
}
