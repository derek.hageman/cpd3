/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QObject>
#include <QString>
#include <QTest>

#include "guicore/guiformat.hxx"

using namespace CPD3::GUI;

#define TEST_SETTINGS_APP   CPD3GUI_APPLICATION "Testing"

class TestGUIFormat : public QObject {
Q_OBJECT
private slots:

    void initTestCase()
    {
        QSettings settings(CPD3GUI_ORGANIZATION, TEST_SETTINGS_APP);
        settings.clear();
    }

    void cleanupTestCase()
    {
        QSettings settings(CPD3GUI_ORGANIZATION, TEST_SETTINGS_APP);
        settings.clear();
    }


    void formatTime()
    {
        QSettings settings(CPD3GUI_ORGANIZATION, TEST_SETTINGS_APP);

        settings.setValue("time/displayformat", "datetime");
        QCOMPARE(GUITime::formatTime(1292431645.0, &settings), QString("2010-12-15T16:47:25Z"));

        settings.setValue("time/displayformat", "yeardoy");
        QCOMPARE(GUITime::formatTime(1292431645.0, &settings), QString("2010 349.69959"));
        QCOMPARE(GUITime::formatTime(1292431645.0, &settings, false), QString("2010:349.69959"));
    }

    void formatRange()
    {
        QSettings settings(CPD3GUI_ORGANIZATION, TEST_SETTINGS_APP);

        settings.setValue("time/displayformat", "datetime");
        QCOMPARE(GUITime::formatRange(1292431645.0, 1292431805.0, &settings),
                 QString("2010-12-15T16:47:25Z 2010-12-15T16:50:05Z"));

        settings.setValue("time/displayformat", "yeardoy");
        QCOMPARE(GUITime::formatRange(1292431645.0, 1292431805.0, &settings),
                 QString("2010 349.69959 2010 349.70145"));
        QCOMPARE(GUITime::formatRange(1292431645.0, 1292431805.0, &settings, false),
                 QString("2010:349.69959 2010:349.70145"));
    }

    void stringCharacterDimensions()
    {
        QRect r(GUIStringLayout::maximumCharacterDimensions("abc",
                                                            QFontMetrics(QApplication::font())));
        QVERIFY(r.width() > 0);
        QVERIFY(r.height() > 0);
    }
};

QTEST_MAIN(TestGUIFormat)

#include "guiformat.moc"
