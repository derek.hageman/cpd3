/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QTest>
#include <QString>
#include <QSignalSpy>

#include "guicore/timeboundselection.hxx"

using namespace CPD3::GUI;


class TestTimeBoundSelection : public QObject {
Q_OBJECT
private slots:

    void parseValid()
    {
        TimeBoundSelection selection;
        QSignalSpy checkStartChanged(&selection, SIGNAL(startChanged(double)));
        QVERIFY(checkStartChanged.isValid());
        QSignalSpy checkStartEdited(&selection, SIGNAL(startEdited(double)));
        QVERIFY(checkStartEdited.isValid());
        QSignalSpy checkEndChanged(&selection, SIGNAL(endChanged(double)));
        QVERIFY(checkEndChanged.isValid());
        QSignalSpy checkEndEdited(&selection, SIGNAL(endEdited(double)));
        QVERIFY(checkEndEdited.isValid());
        QSignalSpy checkBoundsChanged(&selection, SIGNAL(boundsChanged(double, double)));
        QVERIFY(checkBoundsChanged.isValid());
        QSignalSpy checkBoundsEdited(&selection, SIGNAL(boundsEdited(double, double)));
        QVERIFY(checkBoundsEdited.isValid());

        QObjectList children = selection.children();
        for (QList<QObject *>::const_iterator child = children.constBegin(),
                end = children.constEnd(); child != end; ++child) {
            QLineEdit *lineEdit = qobject_cast<QLineEdit *>(*child);
            if (lineEdit != NULL) {
                lineEdit->setFocus(Qt::TabFocusReason);
                QTest::keyClick(lineEdit, Qt::Key_A, Qt::ControlModifier);
                QTest::keyClick(lineEdit, Qt::Key_Backspace, Qt::NoModifier);
                QTest::keyClicks(lineEdit, "2010:1 2010:2");
                QTest::keyClick(lineEdit, Qt::Key_Return, Qt::NoModifier);
                break;
            }
        }

        QCOMPARE(checkStartChanged.count(), 1);
        QCOMPARE(checkStartChanged.at(0).value(0).toDouble(), 1262304000.0);
        QCOMPARE(checkStartEdited.count(), 1);
        QCOMPARE(checkStartEdited.at(0).value(0).toDouble(), 1262304000.0);
        QCOMPARE(checkEndChanged.count(), 1);
        QCOMPARE(checkEndChanged.at(0).value(0).toDouble(), 1262390400.0);
        QCOMPARE(checkEndEdited.count(), 1);
        QCOMPARE(checkEndEdited.at(0).value(0).toDouble(), 1262390400.0);
        QCOMPARE(checkBoundsChanged.count(), 1);
        QCOMPARE(checkBoundsChanged.at(0).value(0).toDouble(), 1262304000.0);
        QCOMPARE(checkBoundsChanged.at(0).value(1).toDouble(), 1262390400.0);
        QCOMPARE(checkBoundsEdited.count(), 1);
        QCOMPARE(checkBoundsEdited.at(0).value(0).toDouble(), 1262304000.0);
        QCOMPARE(checkBoundsEdited.at(0).value(1).toDouble(), 1262390400.0);
    }

    void rejectInvalid()
    {
        TimeBoundSelection selection;
        QSignalSpy checkStartChanged(&selection, SIGNAL(startChanged(double)));
        QVERIFY(checkStartChanged.isValid());
        QSignalSpy checkStartEdited(&selection, SIGNAL(startEdited(double)));
        QVERIFY(checkStartEdited.isValid());
        QSignalSpy checkEndChanged(&selection, SIGNAL(endChanged(double)));
        QVERIFY(checkEndChanged.isValid());
        QSignalSpy checkEndEdited(&selection, SIGNAL(endEdited(double)));
        QVERIFY(checkEndEdited.isValid());
        QSignalSpy checkBoundsChanged(&selection, SIGNAL(boundsChanged(double, double)));
        QVERIFY(checkBoundsChanged.isValid());
        QSignalSpy checkBoundsEdited(&selection, SIGNAL(boundsEdited(double, double)));
        QVERIFY(checkBoundsEdited.isValid());

        QObjectList children = selection.children();
        for (QList<QObject *>::const_iterator child = children.constBegin(),
                end = children.constEnd(); child != end; ++child) {
            QLineEdit *lineEdit = qobject_cast<QLineEdit *>(*child);
            if (lineEdit != NULL) {
                lineEdit->setFocus(Qt::TabFocusReason);
                QTest::keyClick(lineEdit, Qt::Key_A, Qt::ControlModifier);
                QTest::keyClick(lineEdit, Qt::Key_Backspace, Qt::NoModifier);
                QTest::keyClicks(lineEdit, "2010:1 blarg");
                QTest::keyClick(lineEdit, Qt::Key_Return, Qt::NoModifier);
                break;
            }
        }


        QVERIFY(checkStartChanged.isEmpty());
        QVERIFY(checkStartEdited.isEmpty());
        QVERIFY(checkEndChanged.isEmpty());
        QVERIFY(checkEndEdited.isEmpty());
        QVERIFY(checkBoundsChanged.isEmpty());
        QVERIFY(checkBoundsEdited.isEmpty());
    }
};

QTEST_MAIN(TestTimeBoundSelection)

#include "timeboundselection.moc"
