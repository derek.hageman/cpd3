/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3GUICORESETSELECT_HXX
#define CPD3GUICORESETSELECT_HXX

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QWidget>
#include <QVariant>
#include <QPushButton>
#include <QMenu>
#include <QAction>
#include <QActionGroup>
#include <QSet>
#include <QHash>

#include "guicore/guicore.hxx"

namespace CPD3 {
namespace GUI {

/**
 * Provides functions to handle a selection of items from within a set.
 */
class CPD3GUICORE_EXPORT SetSelector : public QWidget {
Q_OBJECT

    /**
     * This property holds the currently selected elements of the set.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setSelected(const QSet<QString> &)
     *  <li> QSet<QString> getSelected() const
     * </ul>
     */
    Q_PROPERTY(QSet<QString> selected
                       READ getSelected
                       WRITE
                       setSelected)


    /**
     * This property sets if adding new items is allowed.  Enabled by default.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setAllowAdd(bool enable)
     *  <li> bool getAllowAdd() const
     * </ul>
     */
    Q_PROPERTY(bool allowAdd
                       READ getAllowAdd
                       WRITE
                       setAllowAdd)
    bool allowAdd;

    /**
     * This property sets if item selection is exclsuive.  Disabled by default.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setExclusive(bool enable)
     *  <li> bool getExclusive() const
     * </ul>
     */
    Q_PROPERTY(bool exclusive
                       READ getExclusive
                       WRITE
                       setExclusive)

    /**
     * This property sets the button text when no items are selected.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setEmptyText(const QString &)
     *  <li> QString getEmptyText() const
     * </ul>
     */
    Q_PROPERTY(QString emptyText
                       READ getEmptyText
                       WRITE
                       setEmptyText)
    QString emptyText;


    QPushButton *button;
    QMenu *menu;
    QAction *separatorAction;
    QAction *addAction;
    QActionGroup *actionGroup;
    QHash<QString, QAction *> items;

    void updateButtonState();

public:
    SetSelector(QWidget *parent = 0);

    virtual ~SetSelector();

    void setSelected(const QSet<QString> &items);

    QSet<QString> getSelected() const;

    void setAllowAdd(bool enable);

    bool getAllowAdd() const;

    void setExclusive(bool enable);

    bool getExclusive() const;

    void setEmptyText(const QString &text);

    QString getEmptyText() const;

    /**
     * Get a hash of the selected items and their associated data.
     *
     * @return  the selected items and data
     */
    QHash<QString, QVariant> getSelectedData() const;

    /**
     * Get the data associated with a given item.
     *
     * @param name  the item data
     * @return      the item data
     */
    QVariant getItemData(const QString &name) const;

    /**
     * Add a selected item.
     *
     * @param name  the name of the item
     * @param data  the data associated with the item
     */
    void addSelected(const QString &name, const QVariant &data = QVariant());

    /**
     * Add an item, optionally selecting it.
     *
     * @param name  the name of the item to add
     * @param text  the text of the entry to display
     * @param data  the data associated with the item
     * @param selected select the item
     */
    void addItem(const QString &name,
                 const QString &text = QString(),
                 const QVariant &data = QVariant(),
                 bool selected = false);

    /**
     * Set the tool tip for a given item.
     *
     * @param name      the item name
     * @param toolTip   the item tool tip
     */
    void setItemToolTip(const QString &name, const QString &toolTip);

    /**
     * Set the status tip for a given item.
     * @param name      the item name
     * @param statusTip the status tip
     */
    void setItemStatusTip(const QString &name, const QString &statusTip);

    /**
     * Set the what's this data for a given item.
     *
     * @param name      the item name
     * @param whatsThis the what's this text
     */
    void setItemWhatsThis(const QString &name, const QString &whatsThis);

    /**
     * Remove all items from the group selection.
     */
    void clear();

    /**
     * Clear all selected elements.
     */
    void clearSelection();

signals:

    /**
     * Emitted whenever any part of the selection has changed.
     */
    void selectionChanged();

    /**
     * Emitted when a given item is selected.
     *
     * @param name  the name of the item being selected
     */
    void itemSelected(const QString &name);

    /**
     * Emitted when a given item is unselected.
     *
     * @param name  the name of the item being unselected
     */
    void itemUnselected(const QString &name);

protected:

    /**
     * Generate the text of the button.
     * <br>
     * By default this returns the empty text if nothing is selected or
     * the selected items joined by commas if any are.
     *
     * @return  the text
     */
    virtual QString getButtonText() const;

protected slots:

    /**
     * Called when an add item operation is requested.
     * <br>
     * The default implementation shows a text input box and adds the item if it is accepted
     */
    virtual void addItemSelected();

private slots:

    void updateButtonText();

    void menuActionTriggered(QAction *action);
};

}
}

#endif //CPD3GUICORESETSELECT_HXX
