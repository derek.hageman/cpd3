/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QVBoxLayout>
#include <QSet>
#include <QRegularExpression>
#include <QDebug>

#include "guicore/scriptedit.hxx"

namespace CPD3 {
namespace GUI {

/** @file guicore/scriptedit.hxx
 * Provides an editing widget for CPD3 script code.
 */

ScriptEdit::ScriptEdit(QWidget *parent) : QWidget(parent)
{
    QVBoxLayout *layout = new QVBoxLayout;
    setLayout(layout);
    layout->setContentsMargins(0, 0, 0, 0);

    editor = new QTextEdit(this);
    editor->setTabStopWidth(40);
    layout->addWidget(editor);

    QFont font;
    font.setFamily("Courier");
    font.setFixedPitch(true);
    font.setPointSize(10);
    editor->setFont(font);

    new Highlighter(editor->document());

    connect(editor, SIGNAL(textChanged()), this, SLOT(editorChanged()));
}

QString ScriptEdit::getCode() const
{ return code; }

void ScriptEdit::setCode(const QString &code)
{
    this->code = code;
    editor->setPlainText(code);
    emit scriptChanged(code);
}

void ScriptEdit::editorChanged()
{
    code = editor->toPlainText();
    emit scriptChanged(code);
}


ScriptEdit::Highlighter::Rule &ScriptEdit::Highlighter::installRule(const QString &pattern)
{
    highlightingRules.emplace_back();
    auto &target = highlightingRules.back();
    target.pattern = QRegularExpression("\\b" + QRegularExpression::escape(pattern) + "\\b");
    return target;
}

ScriptEdit::Highlighter::Highlighter(QTextDocument *parent) : QSyntaxHighlighter(parent)
{
    {
        static const QSet<QString> keywords{"and", "function", "in", "local", "not", "or"};
        for (const auto &add : keywords) {
            auto &target = installRule(add);
            target.format.setForeground(QColor(200, 100, 0));
        }
    }
    {
        static const QSet<QString> control
                {"break", "do", "else", "elseif", "end", "for", "if", "repeat", "return", "then",
                 "until", "while"};
        for (const auto &add : control) {
            auto &target = installRule(add);
            target.format.setForeground(QColor(200, 100, 0));
            target.format.setFontWeight(QFont::Bold);
        }
    }
    {
        static const QSet<QString> constant{"nil", "false", "true", "_G", "_VERSION"};
        for (const auto &add : constant) {
            auto &target = installRule(add);
            target.format.setForeground(Qt::darkYellow);
            target.format.setFontWeight(QFont::Bold);
        }
    }
    {
        static const QSet<QString> builtin
                {"assert", "collectgarbage", "dofile", "error", "getfenv", "getmetatable", "ipairs",
                 "load", "loadfile", "loadstring", "module", "next", "pairs", "pcall", "print",
                 "rawequal", "rawget", "rawset", "require", "select", "setfenv", "setmetatable",
                 "tonumber", "tostring", "type", "unpack", "xpcall", "coroutine",
                 "coroutine.create", "coroutine.resume", "coroutine.running", "coroutine.status",
                 "coroutine.wrap", "coroutine.yield", "debug", "debug.debug", "debug.getfenv",
                 "debug.gethook", "debug.getinfo", "debug.getlocal", "debug.getmetatable",
                 "debug.getregistry", "debug.getupvalue", "debug.setfenv", "debug.sethook",
                 "debug.setlocal", "debug.setmetatable", "debug.setupvalue", "debug.traceback",
                 "io", "io.close", "io.flush", "io.input", "io.lines", "io.open", "io.output",
                 "io.popen", "io.read", "io.stderr", "io.stdin", "io.stdout", "io.tmpfile",
                 "io.type", "io.write", "math", "math.abs", "math.acos", "math.asin", "math.atan",
                 "math.atan2", "math.ceil", "math.cos", "math.cosh", "math.deg", "math.exp",
                 "math.floor", "math.fmod", "math.frexp", "math.huge", "math.ldexp", "math.log",
                 "math.log10", "math.max", "math.min", "math.modf", "math.pi", "math.pow",
                 "math.rad", "math.random", "math.randomseed", "math.sin", "math.sinh", "math.sqrt",
                 "math.tan", "math.tanh", "os", "os.clock", "os.date", "os.difftime", "os.execute",
                 "os.exit", "os.getenv", "os.remove", "os.rename", "os.setlocale", "os.time",
                 "os.tmpname", "package", "package.cpath", "package.loaded", "package.loaders",
                 "package.loadlib", "package.path", "package.preload", "package.seeall", "string",
                 "string.byte", "string.char", "string.dump", "string.find", "string.format",
                 "string.gmatch", "string.gsub", "string.len", "string.lower", "string.match",
                 "string.rep", "string.reverse", "string.sub", "string.upper", "table",
                 "table.concat", "table.insert", "table.maxn", "table.remove", "table.sort",
                 "data"};
        for (const auto &add : builtin) {
            auto &target = installRule(add);
            target.format.setForeground(Qt::magenta);
        }
    }
    {
        highlightingRules.emplace_back();
        auto &target = highlightingRules.back();
        target.pattern = QRegularExpression("\\bCPD3(\\.[\\.A-Za-z0-9:]*)?\\b");
        target.format.setForeground(Qt::magenta);
    }

    {
        highlightingRules.emplace_back();
        auto &target = highlightingRules.back();
        target.pattern = QRegularExpression("\\b[-+]?[0-9]+(\\.[0-9]*)?([eE][-+]?[0-9]+)?\\b");
        target.format.setForeground(Qt::blue);
    }
    {
        highlightingRules.emplace_back();
        auto &target = highlightingRules.back();
        target.pattern = QRegularExpression("\\b0[xX][0-9a-fA-F]+\\b");
        target.format.setForeground(Qt::blue);
    }
    {
        highlightingRules.emplace_back();
        auto &target = highlightingRules.back();
        target.pattern = QRegularExpression("\".*\"");
        target.format.setForeground(Qt::darkGreen);
    }
    {
        highlightingRules.emplace_back();
        auto &target = highlightingRules.back();
        target.pattern = QRegularExpression("\'.*\'");
        target.format.setForeground(Qt::darkGreen);
    }
    {
        highlightingRules.emplace_back();
        auto &target = highlightingRules.back();
        target.pattern = QRegularExpression("--[^\n\r]*");
        target.format.setForeground(Qt::gray);
    }

    literalStringFormat.setForeground(Qt::darkGreen);
    literalStringBegin = QRegularExpression("\\[(=*)\\[");
    literalStringEndPattern = "\\]%1\\]";

    blockCommentFormat.setForeground(Qt::gray);
    blockCommentBegin = QRegularExpression("--\\[(=*)\\[");
    blockCommentEndPattern = "\\]%1\\]";
}

ScriptEdit::Highlighter::~Highlighter() = default;

void ScriptEdit::Highlighter::highlightBlock(const QString &text)
{
    for (const auto &rule : highlightingRules) {
        auto matchIterator = rule.pattern.globalMatch(text);
        while (matchIterator.hasNext()) {
            auto match = matchIterator.next();
            setFormat(match.capturedStart(), match.capturedLength(), rule.format);
        }
    }
    setCurrentBlockState(0);

    int existingLiteralStringRepeat = -1;
    int existingBlockCommentRepeat = -1;
    if (previousBlockState() != -1) {
        if (previousBlockState() & 0x1) {
            existingLiteralStringRepeat = (previousBlockState() >> 8) & 0xFF;
        }
        if (previousBlockState() & 0x2) {
            existingBlockCommentRepeat = (previousBlockState() >> 16) & 0xFF;
        }
    }

    {
        int startIndex = 0;
        QRegularExpression endPattern;
        if (existingLiteralStringRepeat == -1) {
            auto match = literalStringBegin.match(text);
            if (match.hasMatch()) {
                startIndex = match.capturedStart();
                existingLiteralStringRepeat = match.captured(1).length();
                endPattern = QRegularExpression(literalStringEndPattern.arg(match.captured(1)));
            } else {
                startIndex = -1;
            }
        } else {
            endPattern = QRegularExpression(literalStringEndPattern.arg(
                    QString("=").repeated(existingLiteralStringRepeat)));
        }

        while (startIndex >= 0) {
            auto match = endPattern.match(text, startIndex);
            int endIndex = match.capturedStart();
            int length = 0;
            if (endIndex == -1) {
                setCurrentBlockState(
                        currentBlockState() | 0x1 | ((existingLiteralStringRepeat & 0xFF) << 8));
                length = text.length() - startIndex;
                setFormat(startIndex, length, literalStringFormat);
                break;
            } else {
                length = endIndex - startIndex + match.capturedLength();
                setFormat(startIndex, length, literalStringFormat);
            }

            match = literalStringBegin.match(text, startIndex + length);
            if (!match.hasMatch())
                break;
            startIndex = match.capturedStart();
            existingLiteralStringRepeat = match.captured(1).length();
            endPattern = QRegularExpression(literalStringEndPattern.arg(match.captured(1)));
        }
    }

    {
        int startIndex = 0;
        QRegularExpression endPattern;
        if (existingBlockCommentRepeat == -1) {
            auto match = blockCommentBegin.match(text);
            if (match.hasMatch()) {
                startIndex = match.capturedStart();
                existingBlockCommentRepeat = match.captured(1).length();
                endPattern = QRegularExpression(blockCommentEndPattern.arg(match.captured(1)));
            } else {
                startIndex = -1;
            }
        } else {
            endPattern = QRegularExpression(
                    blockCommentEndPattern.arg(QString("=").repeated(existingBlockCommentRepeat)));
        }

        while (startIndex >= 0) {
            auto match = endPattern.match(text, startIndex);
            int endIndex = match.capturedStart();
            int length = 0;
            if (endIndex == -1) {
                setCurrentBlockState(
                        currentBlockState() | 0x2 | ((existingBlockCommentRepeat & 0xFF) << 16));
                length = text.length() - startIndex;
                setFormat(startIndex, length, blockCommentFormat);
                break;
            } else {
                length = endIndex - startIndex + match.capturedLength();
                setFormat(startIndex, length, blockCommentFormat);
            }

            match = blockCommentBegin.match(text, startIndex + length);
            if (!match.hasMatch())
                break;
            startIndex = match.capturedStart();
            existingBlockCommentRepeat = match.captured(1).length();
            endPattern = QRegularExpression(blockCommentEndPattern.arg(match.captured(1)));
        }
    }
}


}
}
