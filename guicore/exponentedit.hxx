/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUICOREEXPONTENTEDIT_H
#define CPD3GUICOREEXPONTENTEDIT_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QWidget>
#include <QSpinBox>

#include "guicore/guicore.hxx"

namespace CPD3 {
namespace GUI {

namespace Internal {
class FractionalSpinBox;
}

/**
 * A widget that allows for editing and input of exponentially formatted
 * real numbers.
 */
class CPD3GUICORE_EXPORT ExponentEdit : public QWidget {
Q_OBJECT

    /**
     * This property hold the absolute minimum of the editor.  Inclusive.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setMinimum(double)
     *  <li> double getMinimum() const
     * </ul>
     */
    Q_PROPERTY(double min
                       READ getMinimum
                       WRITE
                       setMinimum)
    double min;

    /**
     * This property hold the absolute maximum of the editor.  Inclusive.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setMaximum(double)
     *  <li> double getMaximum() const
     * </ul>
     */
    Q_PROPERTY(double max
                       READ getMaximum
                       WRITE
                       setMaximum)
    double max;

    /**
     * This property hold the number of digits after the decimal place.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setDecimals(int)
     *  <li> int getDecimals() const
     * </ul>
     */
    Q_PROPERTY(int dec
                       READ getDecimals
                       WRITE
                       setDecimals)
    int decimals;

    friend class Internal::FractionalSpinBox;

    Internal::FractionalSpinBox *fractional;
    QSpinBox *exponent;

    void rangeChanged();

    void fractionalExponent(int e) const;

public:
    ExponentEdit(QWidget *parent = 0);

    void setMinimum(double v);

    double getMinimum() const;

    void setMaximum(double v);

    double getMaximum() const;

    void setDecimals(int d);

    int getDecimals() const;

    double getValue() const;

public slots:

    void setValue(double val);

private slots:

    void spinnersChanged();

signals:

    /**
     * Emitted whenever the value changes, including during editing.
     * 
     * @param d the new value
     */
    void valueChanged(double d);
};

}
}

#endif
