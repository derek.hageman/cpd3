/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QValidator>
#include <QHBoxLayout>
#include <QKeyEvent>

#include "core/number.hxx"
#include "core/timeutils.hxx"
#include "core/timeparse.hxx"
#include "guicore/timesingleselection.hxx"
#include "guicore/guiformat.hxx"

namespace CPD3 {
namespace GUI {

/** @file guicore/timesingleselection.hxx
 * Provides a single time selection widget.
 */

namespace Internal {
class SingleTimeValidator : public QValidator {
    TimeSingleSelection *parent;

public:
    SingleTimeValidator(TimeSingleSelection *setParent) : QValidator(setParent), parent(setParent)
    { }

    virtual ~SingleTimeValidator()
    { }

    virtual QValidator::State validate(QString &input, int &pos) const
    {
        Q_UNUSED(pos);

        try {
            Time::LogicalTimeUnit unit = parent->getUnit();
            double time = TimeParse::parseTime(input, &unit);
            if (!FP::defined(time)) {
                if (!parent->getAllowUndefined())
                    return QValidator::Intermediate;
            } else {
                if (FP::defined(parent->getMinimum()) && time < parent->getMinimum())
                    return QValidator::Intermediate;
                if (FP::defined(parent->getMaximum()) && time > parent->getMaximum())
                    return QValidator::Intermediate;
            }
        } catch (TimeParsingException tpe) {
            return QValidator::Intermediate;
        }

        return QValidator::Acceptable;
    }

    virtual void fixup(QString &input) const
    {
        double time = parent->getTime();
        try {
            Time::LogicalTimeUnit unit = parent->getUnit();
            time = TimeParse::parseTime(input, &unit);
        } catch (TimeParsingException tpe) {
            return;
        }

        if (FP::defined(time)) {
            if (FP::defined(parent->getMinimum()) && time < parent->getMinimum())
                time = parent->getMinimum();
            if (FP::defined(parent->getMaximum()) && time > parent->getMaximum())
                time = parent->getMaximum();
        } else {
            if (!parent->getAllowUndefined())
                time = parent->getTime();
        }

        input = parent->reformatTime(time);
    }
};

class SingleTimeEntry : public QLineEdit {
    TimeSingleSelection *parent;
public:
    SingleTimeEntry(TimeSingleSelection *setParent) : QLineEdit(setParent), parent(setParent)
    { }

    virtual ~SingleTimeEntry()
    { }

protected:
    virtual void focusOutEvent(QFocusEvent *e)
    {
        QLineEdit::focusOutEvent(e);
        setText(parent->reformatTime(parent->getTime()));
    }
};
}

QString TimeSingleSelection::reformatTime(double time) const
{
    return GUITime::formatTime(time, &settings);
}

TimeSingleSelection::TimeSingleSelection(QWidget *parent) : QWidget(parent),
                                                            allowUndefined(false),
                                                            minimum(FP::undefined()),
                                                            maximum(FP::undefined()),
                                                            unit(Time::Day),
                                                            time(FP::undefined()),
                                                            settings(CPD3GUI_ORGANIZATION,
                                                                     CPD3GUI_APPLICATION)
{
    entry = new Internal::SingleTimeEntry(this);
    QFont f(entry->font());
    f.setFamily("Courier");
    f.setStyleHint(QFont::TypeWriter);
    f.setBold(true);
    entry->setFont(f);
    entry->setText(reformatTime(time));
    entry->setValidator(new Internal::SingleTimeValidator(this));
    entry->setSizePolicy(
            QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Ignored, QSizePolicy::LineEdit));

    QHBoxLayout *layout = new QHBoxLayout;
    layout->setContentsMargins(0, 0, 0, 0);
    layout->addWidget(entry);
    layout->setSpacing(0);
    layout->setMargin(0);
    setLayout(layout);

    connect(entry, SIGNAL(editingFinished()), this, SLOT(broadcastUpdate()));
}

TimeSingleSelection::TimeSingleSelection(double t, QWidget *parent) : QWidget(parent),
                                                                      allowUndefined(false),
                                                                      minimum(FP::undefined()),
                                                                      maximum(FP::undefined()),
                                                                      time(t),
                                                                      settings(CPD3GUI_ORGANIZATION,
                                                                               CPD3GUI_APPLICATION)
{
    entry = new Internal::SingleTimeEntry(this);
    QFont f(entry->font());
    f.setFamily("Monospace");
    f.setStyleHint(QFont::TypeWriter);
    f.setBold(true);
    entry->setFont(f);
    entry->setText(reformatTime(time));
    entry->setValidator(new Internal::SingleTimeValidator(this));
    entry->setSizePolicy(
            QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Ignored, QSizePolicy::LineEdit));

    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(entry);
    layout->setSpacing(0);
    layout->setMargin(0);
    setLayout(layout);

    connect(entry, SIGNAL(editingFinished()), this, SLOT(broadcastUpdate()));
}

TimeSingleSelection::~TimeSingleSelection()
{
}

void TimeSingleSelection::updateDisplay()
{
    entry->setText(reformatTime(time));
}

void TimeSingleSelection::broadcastUpdate()
{
    bool timeUpdated = false;

    try {
        Time::LogicalTimeUnit unit = getUnit();
        double t = TimeParse::parseTime(entry->text(), &unit);
        if (!FP::defined(t) && !allowUndefined) {
            t = time;
        } else if (!FP::equal(t, time)) {
            time = t;
            timeUpdated = true;
        }
    } catch (TimeParsingException tpe) {
    }

    if (timeUpdated)
            emit changed(time);

    entry->setText(reformatTime(time));
}

QSize TimeSingleSelection::sizeHint() const
{
    return entry->sizeHint().expandedTo(QSize(200, 0));
}

QSize TimeSingleSelection::minimumSizeHint() const
{ return sizeHint(); }

void TimeSingleSelection::keyReleaseEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter)
        return;
    QWidget::keyReleaseEvent(event);
}

void TimeSingleSelection::keyPressEvent(QKeyEvent *event)
{
    if (event->key() == Qt::Key_Return || event->key() == Qt::Key_Enter)
        return;
    QWidget::keyPressEvent(event);
}

double TimeSingleSelection::getTime() const
{ return time; }

bool TimeSingleSelection::getAllowUndefined() const
{ return allowUndefined; }

void TimeSingleSelection::setAllowUndefined(bool allow)
{
    if (allow == allowUndefined)
        return;

    allowUndefined = allow;
}

double TimeSingleSelection::getMinimum() const
{ return minimum; }

void TimeSingleSelection::setMinimum(double min)
{
    if (FP::equal(min, minimum))
        return;

    minimum = min;
    if (FP::defined(minimum) && FP::defined(time) && time < minimum) {
        time = minimum;
        updateDisplay();
        emit changed(time);
    }
}

double TimeSingleSelection::getMaximum() const
{ return maximum; }

void TimeSingleSelection::setMaximum(double max)
{
    if (FP::equal(max, maximum))
        return;

    maximum = max;
    if (FP::defined(maximum) && FP::defined(time) && time > maximum) {
        time = maximum;
        updateDisplay();
        emit changed(time);
    }
}

Time::LogicalTimeUnit TimeSingleSelection::getUnit() const
{ return unit; }

void TimeSingleSelection::setUnit(Time::LogicalTimeUnit unit)
{
    if (unit == this->unit)
        return;
    this->unit = unit;
}

void TimeSingleSelection::setTime(double time)
{
    if (FP::equal(time, this->time))
        return;

    this->time = time;
    updateDisplay();
    emit changed(time);
}

}
}
