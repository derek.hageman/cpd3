/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUICORECALIBRATIONEDIT_H
#define CPD3GUICORECALIBRATIONEDIT_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QWidget>
#include <QGridLayout>
#include <QList>
#include <QPushButton>
#include <QButtonGroup>
#include <QLineEdit>

#include "guicore/guicore.hxx"
#include "core/number.hxx"

namespace CPD3 {
namespace GUI {


/**
 * A widget that defines an editor for calibration polynomials.
 */
class CPD3GUICORE_EXPORT CalibrationEdit : public QWidget {
Q_OBJECT

    /* Can't declare this as a property because it upsets moc */
    Calibration calibration;

    QWidget *listWidget;
    QGridLayout *listLayout;
    QList<QPushButton *> listButtons;
    QButtonGroup *listGroup;
    QPushButton *addButton;
    QPushButton *removeButton;
    QLineEdit *valueEditor;

    void updateCalibration();

    void setButtonText(int i);

public:
    CalibrationEdit(QWidget *parent = 0);

    /**
     * Get the current calibration.
     * 
     * @return  the calibration the widget reflects
     */
    Calibration getCalibration() const;

public slots:

    /**
     * Set the calibration the editor is displaying.
     * 
     * @param cal       the calibration
     */
    void setCalibration(const CPD3::Calibration &cal);

signals:

    /**
     * Emitted whenever the calibration changes.
     * 
     * @param cal   the updated calibration
     */
    void calibrationChanged(const CPD3::Calibration &cal);

private slots:

    void addPressed();

    void removePressed();

    void coefficientSelected();

    void coefficientChanged();
};

}
}

#endif
