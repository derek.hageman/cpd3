/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QProgressBar>
#include <QLabel>
#include <QMessageBox>

#include "guicore/actionprogress.hxx"
#include "guicore/guiformat.hxx"

namespace CPD3 {
namespace GUI {

/** @file guicore/actionprogress.hxx
 * Provides interface to display standard action progress.
 */

ActionProgressDialog::ActionProgressDialog(QWidget *parent, Qt::WindowFlags f, QSettings *set)
        : QProgressDialog(parent, f),
          start(FP::undefined()),
          end(FP::undefined()), incoming(),
          settings(set)
{
    if (!settings) {
        settings = new QSettings(CPD3GUI_ORGANIZATION, CPD3GUI_APPLICATION, this);
    }

    label = new QLabel;
    label->setWordWrap(true);
    setLabel(label);

    setAutoClose(false);

    incoming.updated.connect(this, std::bind(&ActionProgressDialog::feedbackUpdate, this), true);
}

void ActionProgressDialog::attach(ActionFeedback::Source &source)
{
    incoming.attach(source);
}

void ActionProgressDialog::setTimeBounds(double start, double end)
{
    this->start = start;
    this->end = end;
}

void ActionProgressDialog::setStart(double start)
{
    this->start = start;
}

void ActionProgressDialog::setEnd(double end)
{
    this->end = end;
}

void ActionProgressDialog::feedbackUpdate()
{
    auto all = incoming.process();
    for (const auto &check : all) {
        if (check.state() != ActionFeedback::Serializer::Stage::Failure)
            continue;

        QString message;
        auto reason = check.failureReason();
        if (!reason.isEmpty()) {
            message = tr("FAILURE: %1: %2", "failure").arg(check.title(), reason);
        } else {
            message = tr("FAILURE: %1", "failure").arg(check.title());
        }

        QMessageBox::warning(this, tr("Failure Detected"), message);
    }

    ActionFeedback::Serializer::Stage stage = all.back();

    double fraction = stage.toFraction(start, end);
    if (stage.state() == ActionFeedback::Serializer::Stage::Spin && FP::defined(fraction)) {
        setMinimum(0);
        setMaximum(0);
        setValue(1);
    } else {
        setMinimum(0);
        setMaximum(1000);
        setValue((int) ::floor(fraction * 1000.0));
    }

    QString firstLine;

    QString title = stage.title();
    if (title.isEmpty())
        title = tr("Initializing");

    double time = stage.time();
    if (FP::defined(time)) {
        if (!stage.stageStatus().isEmpty()) {
            firstLine = tr("%1 -- %2 (%3)", "stage state label time").arg(title,
                                                                          GUITime::formatTime(time,
                                                                                              settings),
                                                                          stage.stageStatus());
        } else {
            firstLine = tr("%1 -- %2", "stage label time").arg(title,
                                                               GUITime::formatTime(time, settings));
        }
    } else {
        if (!stage.stageStatus().isEmpty()) {
            firstLine = tr("%1 (%2)", "stage state label").arg(title, stage.stageStatus());
        } else {
            firstLine = title;
        }
    }

    QString labelText;
    if (stage.description().isEmpty()) {
        labelText = firstLine;
    } else if (firstLine.isEmpty()) {
        labelText = stage.description();
    } else {
        labelText = tr("<dl><dt>%1</dt><dd>%2</dd></dl>", "stage description").arg(firstLine,
                                                                                   stage.description());
    }

    if (labelText != label->text()) {
        label->setText(labelText);
        adjustSize();
    }
}


ActionProgressStatusBar::ActionProgressStatusBar(QProgressBar *b,
                                                 QLabel *l,
                                                 QObject *parent,
                                                 QSettings *set) : QObject(parent),
                                                                   start(FP::undefined()),
                                                                   end(FP::undefined()),
                                                                   settings(set),
                                                                   bar(b),
                                                                   label(l)
{
    if (!settings) {
        settings = new QSettings(CPD3GUI_ORGANIZATION, CPD3GUI_APPLICATION, this);
    }

    incoming.updated.connect(this, std::bind(&ActionProgressStatusBar::feedbackUpdate, this), true);
}

void ActionProgressStatusBar::attach(ActionFeedback::Source &source)
{
    incoming.attach(source);
}

void ActionProgressStatusBar::setTimeBounds(double start, double end)
{
    this->start = start;
    this->end = end;
}

void ActionProgressStatusBar::setStart(double start)
{
    this->start = start;
}

void ActionProgressStatusBar::setEnd(double end)
{
    this->end = end;
}

void ActionProgressStatusBar::feedbackUpdate()
{
    ActionFeedback::Serializer::Stage stage = incoming.process().back();

    double fraction = stage.toFraction(start, end);
    if (stage.state() == ActionFeedback::Serializer::Stage::Spin && FP::defined(fraction)) {
        bar->setTextVisible(false);
        bar->setMinimum(0);
        bar->setMaximum(0);
        bar->setValue(1);
    } else {
        bar->setTextVisible(true);
        bar->setMinimum(0);
        bar->setMaximum(1000);
        bar->setValue((int) ::floor(fraction * 1000.0));
    }

    auto title = stage.title();
    if (!title.isEmpty()) {
        double time = stage.time();
        if (FP::defined(time)) {
            if (!stage.stageStatus().isEmpty()) {
                label->setText(tr("%1 -- %2 (%3)", "stage state label time").arg(title,
                                                                                 GUITime::formatTime(
                                                                                         time,
                                                                                         settings),
                                                                                 stage.stageStatus()));
            } else {
                label->setText(tr("%1 -- %2", "stage label time").arg(title,
                                                                      GUITime::formatTime(time,
                                                                                          settings)));
            }
        } else {
            if (!stage.stageStatus().isEmpty()) {
                label->setText(tr("%1 (%2)", "stage state label").arg(title, stage.stageStatus()));
            } else {
                label->setText(title);
            }
        }
    }

    bar->setToolTip(stage.description());
}

void ActionProgressStatusBar::initialize()
{
    incoming.reset();

    if (bar != NULL) {
        bar->setTextVisible(false);
        bar->setMinimum(0);
        bar->setMaximum(0);
        bar->setValue(1);
        bar->setToolTip(QString());
        bar->show();
    }
    if (label != NULL) {
        label->setText(QString());
    }
}

void ActionProgressStatusBar::finished()
{
    incoming.reset();

    if (bar != NULL) {
        bar->setMinimum(0);
        bar->setMaximum(0);
        bar->setValue(1);
        bar->setToolTip(QString());
        bar->hide();
    }
    if (label != NULL) {
        label->setText(QString());
    }
}


}
}
