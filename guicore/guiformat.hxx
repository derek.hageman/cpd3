/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUICOREGUIFORMAT_H
#define CPD3GUICOREGUIFORMAT_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QSettings>
#include <QString>
#include <QFontMetrics>
#include <QPainter>

#include "guicore/guicore.hxx"

namespace CPD3 {
namespace GUI {

/**
 * Provides functions to handle time in the GUI.
 */
class CPD3GUICORE_EXPORT GUITime : public QObject {
Q_OBJECT
public:
    static QString formatTime(double time,
                              const QSettings *settings = NULL,
                              bool allowSpaces = true,
                              bool dropDOYAsNeeded = false);

    static QString formatTime(double time,
                              const QSettings *settings,
                              const QString &format,
                              bool allowSpaces = true,
                              bool dropDOYAsNeeded = false);

    static QString formatRange(double start,
                               double end,
                               const QSettings *settings = NULL,
                               bool allowTimeSpaces = true,
                               bool dropDOYAsNeeded = false);

    static QString formatRange(double start,
                               double end,
                               const QSettings *settings,
                               const QString &format,
                               bool allowTimeSpaces = true,
                               bool dropDOYAsNeeded = false);
};

/**
 * Provides routines to handle string layout.
 */
class CPD3GUICORE_EXPORT GUIStringLayout : public QObject {
Q_OBJECT
public:
    static QRect maximumCharacterDimensions(const QString &str, const QFontMetrics &fm);

    static QRectF alignTextBottom(QPainter *painter,
                                  const QRectF &origin,
                                  int flags,
                                  const QString &str);

    static QRectF alignTextTop(QPainter *painter,
                               const QRectF &origin,
                               int flags,
                               const QString &str);
};

}
}

#endif
