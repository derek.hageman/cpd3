/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUICORETIMEBOUNDSELECTION_H
#define CPD3GUICORETIMEBOUNDSELECTION_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QWidget>
#include <QValidator>
#include <QLineEdit>
#include <QSettings>

#include "guicore/guicore.hxx"

namespace CPD3 {
namespace GUI {

namespace Internal {
class BoundValidator;

class LabeledEntry;
}

/**
 * Provides a widget to select time bounds.
 */
class CPD3GUICORE_EXPORT TimeBoundSelection : public QWidget {
Q_OBJECT

    /**
     * This property holds if the range can have an undefined/infinite start.
     * This is false by default.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setAllowUndefinedStart(bool)
     *  <li> bool allowsUndefinedStart() const
     * </ul>
     */
    Q_PROPERTY(bool allowUndefinedStart
                       READ allowsUndefinedStart
                       WRITE
                       setAllowUndefinedStart)

    /**
     * This property holds if the range can have an undefined/infinite end.
     * This is false by default.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setAllowUndefinedEnd(bool)
     *  <li> bool allowsUndefinedEnd() const
     * </ul>
     */
    Q_PROPERTY(bool allowUndefinedEnd
                       READ allowsUndefinedEnd
                       WRITE
                       setAllowUndefinedEnd)

    /**
     * This property holds if the range can have a zero length interval.
     * This is false by default.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setAllowZeroSize(bool)
     *  <li> bool allowsZeroSize() const
     * </ul>
     */
    Q_PROPERTY(bool allowZeroSize
                       READ allowsZeroSize
                       WRITE
                       setAllowZeroSize)

    /**
     * This property holds the minimum time the start can have, inclusive.
     * This is undefined (infinite) by default.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setMinimumStart(double)
     *  <li> double getMinimumStart() const
     * </ul>
     */
    Q_PROPERTY(double minimumStart
                       READ getMinimumStart
                       WRITE
                       setMinimumStart)

    /**
     * This property holds the maximum time the start can have, inclusive.
     * This is undefined (infinite) by default.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setMaximumStart(double)
     *  <li> double getMaximumStart() const
     * </ul>
     */
    Q_PROPERTY(double maximumStart
                       READ getMaximumStart
                       WRITE
                       setMaximumStart)

    /**
     * This property holds the minimum time the end can have, inclusive.
     * This is undefined (infinite) by default.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setMinimumEnd(double)
     *  <li> double getMinimumEnd() const
     * </ul>
     */
    Q_PROPERTY(double minimumEnd
                       READ getMinimumEnd
                       WRITE
                       setMinimumEnd)

    /**
     * This property holds the maximum time the end can have, inclusive.
     * This is undefined (infinite) by default.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setMaximumEnd(double)
     *  <li> double getMaximumEnd() const
     * </ul>
     */
    Q_PROPERTY(double maximumEnd
                       READ getMaximumEnd
                       WRITE
                       setMaximumEnd)

    /**
     * This property holds the current start time.  This is undefined by
     * default.  This is an inconstant state, unless the default of not allowing
     * undefined starts is changed, so it must be set to something valid.
     * Changing the value will cause the corresponding signals to be emitted.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setStart(double)
     *  <li> double getStart() const
     * </ul>
     * Notifier signal:
     * <ul>
     *   <li> void startChanged( double )
     *   <li> void boundsChanged( double, double );
     * </ul>
     */
    Q_PROPERTY(double start
                       READ getStart
                       WRITE setStart
                       NOTIFY startChanged
                       DESIGNABLE
                       false
                       USER
                       true)

    /**
     * This property holds the current end time.  This is undefined by
     * default.  This is an inconstant state, unless the default of not allowing
     * undefined starts is changed, so it must be set to something valid.
     * Changing the value will cause the corresponding signals to be emitted.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setEnd(double)
     *  <li> double getEnd() const
     * </ul>
     * Notifier signal:
     * <ul>
     *  <li> void endChanged( double )
     *  <li> void boundsChanged( double, double )
     * </ul>
     */
    Q_PROPERTY(double end
                       READ getEnd
                       WRITE setEnd
                       NOTIFY endChanged
                       DESIGNABLE
                       false
                       USER
                       true)


    /**
     * This property holds if the range rejects focusing on window 
     * (re-)activation.  This is used for the main GUI window to stop
     * the editor from getting the focus all the time (since it's the only
     * thing that accepts keyboard focus).  The default is false (accept
     * window activation focus).
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setDisableWindowFocus(bool)
     *  <li> bool getDisableWindowFocus() const
     * </ul>
     */
    Q_PROPERTY(bool disableWindowFocus
                       READ getDisableWindowFocus
                       WRITE
                       setDisableWindowFocus)

    bool allowUndefinedStart;
    bool allowUndefinedEnd;
    bool allowZeroSize;
    bool disableWindowFocus;

    double minimumStart;
    double maximumStart;
    double minimumEnd;
    double maximumEnd;

    double start;
    double end;

    QSettings settings;
    QLineEdit *entry;

    friend class Internal::BoundValidator;

    friend class Internal::LabeledEntry;

    QString reformatBounds(double start, double end, bool includeLabels) const;

public:
    TimeBoundSelection(QWidget *parent = 0);

    TimeBoundSelection(double setStart, double setEnd, QWidget *parent = 0);

    virtual ~TimeBoundSelection();

    void setAllowUndefinedStart(bool allow);

    void setAllowUndefinedEnd(bool allow);

    bool allowsUndefinedStart() const;

    bool allowsUndefinedEnd() const;

    void setAllowZeroSize(bool allow);

    bool allowsZeroSize() const;

    void setDisableWindowFocus(bool disable);

    bool getDisableWindowFocus() const;

    void setMinimumStart(double m);

    void setMaximumStart(double m);

    void setMinimumEnd(double m);

    void setMaximumEnd(double m);

    double getMinimumStart() const;

    double getMaximumStart() const;

    double getMinimumEnd() const;

    double getMaximumEnd() const;

    double getStart() const;

    double getEnd() const;

    virtual QSize sizeHint() const;

    virtual QSize minimumSizeHint() const;

protected:
    virtual void keyReleaseEvent(QKeyEvent *event);

    virtual void keyPressEvent(QKeyEvent *event);

public slots:

    void setStart(double v);

    void setEnd(double v);

    void setBounds(double s, double e);

    bool interpretText();

signals:

    /**
     * Emitted whenever the start time changes.
     * 
     * @param start     the current start time
     */
    void startChanged(double start);

    /**
     * Emitted whenever the end time changes.
     * 
     * @param start     the current end time
     */
    void endChanged(double end);

    /**
     * Emitted whenever either or both of the start and/or end changes.
     * 
     * @param start     the current start time
     * @param end       the current end time
     */
    void boundsChanged(double start, double end);

    /**
     * Emitted whenever the start time was changed by user action only.
     * 
     * @param start     the current start time
     */
    void startEdited(double start);

    /**
     * Emitted whenever the end time was changed by user action only.
     * 
     * @param start     the current end time
     */
    void endEdited(double end);

    /**
     * Emitted whenever either or both of the start and/or was changed by user 
     * action only.
     * 
     * @param start     the current start time
     * @param end       the current end time
     */
    void boundsEdited(double start, double end);

private slots:

    void broadcastUpdate();
};

}
}

#endif
