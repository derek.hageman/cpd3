/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUICORETIMEINTERVALSELECTION_H
#define CPD3GUICORETIMEINTERVALSELECTION_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QWidget>
#include <QLineEdit>

#include "guicore/guicore.hxx"

#include "core/timeutils.hxx"

namespace CPD3 {
namespace GUI {

namespace Internal {
class IntervalValidator;

class IntervalEntry;
}

/**
 * Provides a widget to time intervals.
 */
class CPD3GUICORE_EXPORT TimeIntervalSelection : public QWidget {
Q_OBJECT

    /**
     * This property is set if the interval allows for zero time length.  The
     * default is to not allow for zero.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setAllowZero(bool)
     *  <li> bool getAllowZero() const
     * </ul>
     */
    Q_PROPERTY(bool allowZero
                       READ getAllowZero
                       WRITE
                       setAllowZero)

    /**
     * This property is set if the interval allows for negative time length.
     * The default is to not allow for negatives.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setAllowNegative(bool)
     *  <li> bool getAllowNegative() const
     * </ul>
     */
    Q_PROPERTY(bool allowNegative
                       READ getAllowNegative
                       WRITE
                       setAllowNegative)

    /**
     * This property is set if the interval allows for alignment.  The default
     * is to allow for alignment.  If this is disabled then attempting to 
     * specify an alignment will just result in it being discarded.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setAllowAlign(bool)
     *  <li> bool getAllowAlign() const
     * </ul>
     */
    Q_PROPERTY(bool allowAlign
                       READ getAllowAlign
                       WRITE
                       setAllowAlign)

    /**
     * This property holds the current time unit of the interval.  Changing
     * the value will cause the signals to be emitted.  The default is minutes.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setUnit(Time::LogicalTimeUnit)
     *  <li> void setInterval( Time::LogicalTimeUnit, int, bool )
     *  <li> Time::LogicalTimeUnit getUnit() const
     *  <li> Time::LogicalTimeUnit getInterval( int *, bool * ) const
     *  <li> Time::LogicalTimeUnit getInterval( int &, bool & ) const
     * </ul>
     * Notifier signal:
     * <ul>
     *   <li> void unitChanged(Time::LogicalTimeUnit)
     *   <li> void intervalChanged(Time::LogicalTimeUnit, int, bool)
     * </ul>
     */
    Q_PROPERTY(CPD3::Time::LogicalTimeUnit unit
                       READ getUnit
                       WRITE setUnit
                       NOTIFY unitChanged
                       DESIGNABLE
                       false
                       USER
                       true)

    /**
     * This property holds the current number of units in the interval.  
     * Changing the value will cause the signals to be emitted.  The default is
     * one.
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setCount(int)
     *  <li> void setInterval( Time::LogicalTimeUnit, int, bool )
     *  <li> int getCount() const
     *  <li> Time::LogicalTimeUnit getInterval( int *, bool * ) const
     *  <li> Time::LogicalTimeUnit getInterval( int &, bool & ) const
     * </ul>
     * Notifier signal:
     * <ul>
     *   <li> void countChanged(int)
     *   <li> void intervalChanged(Time::LogicalTimeUnit, int, bool)
     * </ul>
     */
    Q_PROPERTY(int count
                       READ getCount
                       WRITE setCount
                       NOTIFY countChanged
                       DESIGNABLE
                       false
                       USER
                       true)

    /**
     * This property holds the current alignment state of the interval.
     * Changing the value will cause the signals to be emitted.  The default is
     * false (no alignment).
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setAlign(int)
     *  <li> void setInterval( Time::LogicalTimeUnit, int, bool )
     *  <li> bool getAlign() const
     *  <li> Time::LogicalTimeUnit getInterval( int *, bool * ) const
     *  <li> Time::LogicalTimeUnit getInterval( int &, bool & ) const
     * </ul>
     * Notifier signal:
     * <ul>
     *   <li> void alignChanged(bool)
     *   <li> void intervalChanged(Time::LogicalTimeUnit, int, bool)
     * </ul>
     */
    Q_PROPERTY(bool align
                       READ getAlign
                       WRITE setAlign
                       NOTIFY alignChanged
                       DESIGNABLE
                       false
                       USER
                       true)

    /**
     * This property holds if the range rejects focusing on window 
     * (re-)activation.  This is used for the main GUI window to stop
     * the editor from getting the focus all the time (since it's the only
     * thing that accepts keyboard focus).  The default is false (accept
     * window activation focus).
     * <br><br>
     * Access functions:
     * <ul>
     *  <li> void setDisableWindowFocus(bool)
     *  <li> bool getDisableWindowFocus() const
     * </ul>
     */
    Q_PROPERTY(bool disableWindowFocus
                       READ getDisableWindowFocus
                       WRITE
                       setDisableWindowFocus)

    bool allowZero;
    bool allowNegative;
    bool allowAlign;
    bool disableWindowFocus;

    Time::LogicalTimeUnit unit;
    int count;
    bool align;

    friend class Internal::IntervalValidator;

    friend class Internal::IntervalEntry;

    QLineEdit *entry;

    void updateDisplay();

    QString reformatInterval(Time::LogicalTimeUnit unit, int count, bool align) const;

public:
    /**
     * Construct a time interval selection defaulting to one minute.
     * 
     * @param parent    the parent widget
     */
    TimeIntervalSelection(QWidget *parent = 0);

    /**
     * Construct a time interval selection with the specified initial
     * interval.
     * 
     * @param unit      the initial time unit
     * @param count     the initial number of units
     * @param align     the initial alignment state
     * @param parent    the parent widget
     */
    TimeIntervalSelection
            (Time::LogicalTimeUnit unit, int count = 1, bool align = false, QWidget *parent = 0);

    virtual ~TimeIntervalSelection();

    /**
     * Test if the interval allows for zero length.
     * 
     * @return true if zero length intervals are permitted
     */
    bool getAllowZero() const;

    /**
     * Set if the interval allows for zero length.
     * 
     * @param allow if true then zero length interval are accepted
     */
    void setAllowZero(bool allow);

    /**
     * Test if the interval allows for negative length intervals.
     * 
     * @return true if negative length intervals are permitted
     */
    bool getAllowNegative() const;

    /**
     * Set if the interval allows for negative length intervals
     * 
     * @param allow if true then negative length interval are accepted
     */
    void setAllowNegative(bool allow);

    /**
     * Test if the interval allows for alignment.
     * 
     * @return true if alignment is permitted
     */
    bool getAllowAlign() const;

    /**
     * Set if the interval allows for alignment.  If it is not then specifying
     * one will simply discard it.
     * 
     * @param allow if true then alignment is permitted.
     */
    void setAllowAlign(bool allow);

    /**
     * Get the current unit of the interval.
     * 
     * @return the current interval unit
     */
    Time::LogicalTimeUnit getUnit() const;

    /**
     * Get the number of units in the interval.
     * 
     * @return the current count
     */
    int getCount() const;

    /**
     * Get the alignment state of the interval.
     * 
     * @return the current alignment state
     */
    bool getAlign() const;

    /**
     * Get the full specification of the interval.
     * 
     * @param count the target count if not NULL
     * @param align the target alignment if not NULL
     * @return      the interval unit
     */
    Time::LogicalTimeUnit getInterval(int *count = NULL, bool *align = NULL) const;

    /**
     * Get the full specification of the interval.
     * 
     * @param count the target count if
     * @param align the target alignment
     * @return      the interval unit
     */
    Time::LogicalTimeUnit getInterval(int &count, bool &align) const;

    void setDisableWindowFocus(bool disable);

    bool getDisableWindowFocus() const;

    virtual QSize sizeHint() const;

    virtual QSize minimumSizeHint() const;

    /** @see QLineEdit::setFrame(bool) */
    void setFrame(bool frame);

    /**
     * Interpret the text currently set.
     */
    void interpretText();

protected:
    virtual void keyReleaseEvent(QKeyEvent *event);

    virtual void keyPressEvent(QKeyEvent *event);

public slots:

    /**
     * Set the unit of the interval.
     * 
     * @param set   the new unit
     */
    void setUnit(Time::LogicalTimeUnit set);

    /**
     * Set the number of units in the interval.
     * 
     * @param set   the new count
     */
    void setCount(int set);

    /**
     * Set the alignment state of the interval.
     * 
     * @param set   the new alignment state
     */
    void setAlign(bool set);

    /**
     * Set the full interval specification.
     * 
     * @param unit  the new interval unit
     * @param count the new number of units
     * @param align the new alignment state
     */
    void setInterval(CPD3::Time::LogicalTimeUnit unit, int count = 1, bool align = false);

signals:

    /**
     * Emitted when the interval unit changes.
     * 
     * @param unit  the new unit
     */
    void unitChanged(Time::LogicalTimeUnit unit);

    /**
     * Emitted when the number of units in the interval changes.
     * 
     * @param count the new number of units
     */
    void countChanged(int count);

    /**
     * Emitted when the alignment state of the interval changes.
     * 
     * @param align the new alignment state
     */
    void alignChanged(bool align);

    /**
     * Emitted when any part of the interval changes.
     * 
     * @param unit  the new of the interval
     * @param count the new number of units in the interval
     * @param align the new alignment state of the interval
     */
    void intervalChanged(CPD3::Time::LogicalTimeUnit unit, int count, bool align);

private slots:

    void broadcastUpdate();
};

}
}

#endif
