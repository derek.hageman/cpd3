/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3GUICORE_H
#define CPD3GUICORE_H

#include <QtCore/QtGlobal>

#if defined(cpd3guicore_EXPORTS)
#   define CPD3GUICORE_EXPORT Q_DECL_EXPORT
#else
#   define CPD3GUICORE_EXPORT Q_DECL_IMPORT
#endif

/* Defaults for QSettings */
#define CPD3GUI_ORGANIZATION    "NOAA ESRL GMD"
#define CPD3GUI_APPLICATION     "CPD3"

#endif
