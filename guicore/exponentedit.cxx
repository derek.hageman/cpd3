/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QApplication>
#include <QWidget>
#include <QSpinBox>
#include <QHBoxLayout>
#include <QLabel>

#include "guicore/exponentedit.hxx"
#include "core/number.hxx"

namespace CPD3 {
namespace GUI {

/** @file guicore/exponentedit.hxx
 * Provides an editing widget for exponential formatted number 
 */

namespace Internal {
class FractionalSpinBox : public QSpinBox {
    ExponentEdit *parent;
    QDoubleValidator validator;
    NumberFormat format;
public:
    FractionalSpinBox(ExponentEdit *setParent) : QSpinBox(setParent),
                                                 parent(setParent),
                                                 validator(this),
                                                 format()
    {
        format.setIntegerDigits(1);
        format.setDecimalDigits(3);
    }

    void setFractionalRange(double mn, double mx)
    {
        double scalar = pow(10.0, parent->decimals);
        setRange(qRound(mn * scalar), qRound(mx * scalar));
    }

    double getValue() const
    {
        double scalar = pow(10.0, -parent->decimals);
        return ((double) value()) * scalar;
    }

    void setFractionalValue(double v)
    {
        double scalar = pow(10.0, parent->decimals);
        setValue(qRound(v * scalar));
    }

    virtual QValidator::State validate(QString &input, int &pos) const
    {
        return validator.validate(input, pos);
    }

    double clipValue(int e, double v) const
    {
        if (FP::defined(parent->min) && FP::defined(parent->max) && parent->min < parent->max) {
            v *= pow(10.0, e);
            if (v < parent->min)
                v = parent->min;
            else if (v > parent->max)
                v = parent->max;
            v /= pow(10.0, -e);
        } else if (!FP::defined(parent->min) && FP::defined(parent->max)) {
            v *= pow(10.0, e);
            if (v > parent->max)
                v = parent->max;
            v /= pow(10.0, -e);
        } else if (!FP::defined(parent->max) && FP::defined(parent->min)) {
            v *= pow(10.0, e);
            if (v < parent->min)
                v = parent->min;
            v /= pow(10.0, -e);
        }
        return v;
    }

    virtual void fixup(QString &input) const
    {
        bool ok;
        double v = input.toDouble(&ok);
        if (!ok)
            v = 0.0;

        int e = parent->exponent->value();
        int deltaE = 0;

        if (v == 0.0) {
            v = clipValue(0, 0.0);
        } else if (input.contains('e', Qt::CaseInsensitive) || fabs(v) < 1.0 || fabs(v) >= 10.0) {
            int eNew = (int) floor(log10(fabs(v)));
            if (eNew != e) {
                deltaE = eNew - e;
                e = eNew;
            }
            v *= pow(10.0, -e);
            v = clipValue(e, v);
        } else {
            v = clipValue(e, v);
        }

        if (v == 0.0) {
            parent->fractionalExponent(0);
            input = format.apply(v);
        } else {
            if (deltaE != 0)
                parent->fractionalExponent(e);
            input = format.apply(v);
        }
    }

    virtual QString textFromValue(int value) const
    {
        double scalar = pow(10.0, -parent->decimals);
        return format.apply((double) value * scalar);
    }

    virtual int valueFromText(const QString &text) const
    {
        QString fixed(text);
        fixup(fixed);
        double v = fixed.toDouble();
        double scalar = pow(10.0, parent->decimals);
        int rounded = qRound(v * scalar);
        scalar = pow(10.0, parent->decimals + 1);
        if (rounded <= -scalar)
            rounded = -((int) scalar - 1);
        else if (rounded >= scalar)
            rounded = (int) scalar - 1;
        return rounded;
    }
};
}

ExponentEdit::ExponentEdit(QWidget *parent) : QWidget(parent)
{
    setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed, QSizePolicy::SpinBox));

    QHBoxLayout *layout = new QHBoxLayout;
    layout->setContentsMargins(0, 0, 0, 0);

    min = FP::undefined();
    max = FP::undefined();
    decimals = 3;

    fractional = new Internal::FractionalSpinBox(this);
    fractional->setAlignment(Qt::AlignRight);
    layout->addWidget(fractional);
    connect(fractional, SIGNAL(valueChanged(int)), this, SLOT(spinnersChanged()));

    layout->addWidget(new QLabel(tr(" E ", "exponent label"), this));

    exponent = new QSpinBox(this);
    exponent->setAlignment(Qt::AlignLeft);
    layout->addWidget(exponent);
    connect(exponent, SIGNAL(valueChanged(int)), this, SLOT(spinnersChanged()));

    setLayout(layout);

    rangeChanged();
    fractional->setValue(0);
    exponent->setValue(0);
}

void ExponentEdit::rangeChanged()
{
    double eps = pow(10.0, -decimals);
    double mf = pow(10.0, decimals) - eps;

    disconnect(exponent, 0, this, 0);
    disconnect(fractional, 0, this, 0);

    if (FP::defined(min) && FP::defined(max) && min < max) {
        double absmin = fabs(min);
        double absmax = fabs(max);
        if (absmin < absmax) {
            if (absmin > 0.0) {
                int abslog = (int) floor(log10(absmin));
                if (abslog < -308)
                    abslog = -308;
                exponent->setMinimum(abslog);
            } else {
                exponent->setMinimum(-308);
            }
            int abslog = (int) ceil(log10(absmax));
            if (abslog > 308)
                abslog = 308;
            exponent->setMaximum(abslog);
        } else {
            if (absmax > 0.0) {
                int abslog = (int) floor(log10(absmax));
                if (abslog < -308)
                    abslog = -308;
                exponent->setMinimum(abslog);
            } else {
                exponent->setMinimum(-308);
            }
            int abslog = (int) ceil(log10(absmin));
            if (abslog > 308)
                abslog = 308;
            exponent->setMaximum(abslog);
        }

        if (min > 0.0 && max > 0.0) {
            fractional->setFractionalRange(eps, mf);
        } else if (min < 0.0 && max < 0.0) {
            fractional->setFractionalRange(-mf, -eps);
        } else if (min == 0.0) {
            fractional->setFractionalRange(0.0, mf);
        } else if (max == 0.0) {
            fractional->setFractionalRange(-mf, 0.0);
        } else {
            fractional->setFractionalRange(-mf, mf);
        }
    } else if (!FP::defined(max) && FP::defined(min)) {
        if (min > 0.0) {
            int abslog = (int) floor(log10(min));
            if (abslog < -308)
                abslog = -308;
            exponent->setMinimum(abslog);
            exponent->setMaximum(308);
            fractional->setFractionalRange(eps, mf);
        } else if (min < 0.0) {
            exponent->setMinimum(-308);
            exponent->setMaximum(308);
            fractional->setFractionalRange(-mf, mf);
        } else {
            exponent->setMinimum(-308);
            exponent->setMaximum(308);
            fractional->setFractionalRange(eps, mf);
        }
    } else if (!FP::defined(min) && FP::defined(max)) {
        if (max > 0.0) {
            exponent->setMinimum(-308);
            exponent->setMaximum(308);
            fractional->setFractionalRange(-mf, mf);
        } else if (max < 0.0) {
            int abslog = (int) floor(log10(-max));
            if (abslog < -308)
                abslog = -308;
            exponent->setMinimum(abslog);
            exponent->setMaximum(308);
            fractional->setFractionalRange(-mf, -eps);
        } else {
            exponent->setMinimum(-308);
            exponent->setMaximum(308);
            fractional->setFractionalRange(eps, mf);
        }
    } else {
        exponent->setMinimum(-308);
        exponent->setMaximum(308);
        fractional->setFractionalRange(-mf, mf);
    }

    connect(exponent, SIGNAL(valueChanged(int)), this, SLOT(spinnersChanged()));
    connect(fractional, SIGNAL(valueChanged(int)), this, SLOT(spinnersChanged()));
}

void ExponentEdit::setMinimum(double v)
{
    min = v;
    rangeChanged();
}

double ExponentEdit::getMinimum() const
{ return min; }

void ExponentEdit::setMaximum(double v)
{
    max = v;
    rangeChanged();
}

double ExponentEdit::getMaximum() const
{ return max; }

void ExponentEdit::setDecimals(int d)
{
    decimals = d;
    rangeChanged();
}

int ExponentEdit::getDecimals() const
{ return decimals; }

/**
 * Get the current value of the editor.
 * 
 * @return  a double value
 */
double ExponentEdit::getValue() const
{
    double f = fractional->getValue();
    int e = exponent->value();

    f *= pow(10.0, e);
    if (FP::defined(min) && FP::defined(max) && min < max) {
        if (f < min)
            f = min;
        else if (f > max)
            f = max;
    } else if (!FP::defined(max)) {
        if (f < min)
            f = min;
    } else if (!FP::defined(min)) {
        if (f > max)
            f = max;
    }
    return f;
}

/**
 * Set the current value of the editor.  Does not trigger a 
 * valueChanged( double ) signal.
 * 
 * @param val   the new value
 */
void ExponentEdit::setValue(double val)
{
    if (!FP::defined(val))
        return;
    if (FP::defined(min) && FP::defined(max) && min < max) {
        if (val < min)
            val = min;
        else if (val > max)
            val = max;
    } else if (!FP::defined(max)) {
        if (val < min)
            val = min;
    } else if (!FP::defined(min)) {
        if (val > max)
            val = max;
    }

    disconnect(exponent, 0, this, 0);
    disconnect(fractional, 0, this, 0);
    if (val == 0.0) {
        exponent->setValue(0);
        fractional->setValue(0);
    } else {
        int e = (int) floor(log10(fabs(val)));
        exponent->setValue(e);
        fractional->setFractionalValue(val / pow(10.0, e));
    }
    connect(exponent, SIGNAL(valueChanged(int)), this, SLOT(spinnersChanged()));
    connect(fractional, SIGNAL(valueChanged(int)), this, SLOT(spinnersChanged()));
}

void ExponentEdit::fractionalExponent(int e) const
{
    const_cast<QSpinBox *>(exponent)->setValue(e);
}

void ExponentEdit::spinnersChanged()
{
    emit valueChanged(getValue());
}


}
}
