/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QString>

#include "clicore/terminaloutput.hxx"

namespace CPD3 {
namespace CLI {

int TerminalOutput::terminalWidth()
{ return 80; }

int TerminalOutput::terminalWidth(QIODevice *device)
{
    Q_UNUSED(device);
    return 80;
}

int TerminalOutput::terminalHeight()
{ return 24; }

int TerminalOutput::terminalHeight(QIODevice *device)
{
    Q_UNUSED(device);
    return 24;
}

void TerminalOutput::output(const QString &string, bool toStderr)
{
    Q_UNUSED(string);
    Q_UNUSED(toStderr);
}

void TerminalOutput::applyWordWrap(const QString &text,
                                   bool toStderr,
                                   int leadSpace,
                                   int columnWidth)
{
    Q_UNUSED(text);
    Q_UNUSED(toStderr);
    Q_UNUSED(leadSpace);
    Q_UNUSED(columnWidth);
}

void TerminalOutput::simpleWordWrap(const QString &text, bool toStderr)
{
    Q_UNUSED(text);
    Q_UNUSED(toStderr);
}

TerminalProgress::TerminalProgress()
{ }

TerminalProgress::~TerminalProgress()
{ }

void TerminalProgress::setTimeBounds(const Time::Bounds &bounds)
{
    Q_UNUSED(bounds);
}

void TerminalProgress::setTimeBounds(double start, double end)
{
    Q_UNUSED(start);
    Q_UNUSED(end);
}

void TerminalProgress::start(bool enableAutoUpdate)
{
    Q_UNUSED(enableAutoUpdate);
}

void TerminalProgress::finished()
{
}

void TerminalProgress::abort()
{
}

void TerminalProgress::attach(ActionFeedback::Source &)
{ }

void TerminalProgress::outputProgressLine()
{ }

}
}
