/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3CLICOREOPTIONPARSE_H
#define CPD3CLICOREOPTIONPARSE_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QString>

#include "clicore/clicore.hxx"
#include "core/component.hxx"

namespace CPD3 {
namespace CLI {

/**
 * An exception representing an error parsing the command line option
 * specification.
 */
class CPD3CLICORE_EXPORT OptionParsingException {
private:
    QString description;
public:
    OptionParsingException(const QString &d);

    /**
     * Get the localized description of what occurred.
     */
    QString getDescription() const;
};

class CPD3CLICORE_EXPORT OptionParse : public QObject {
Q_OBJECT

    static QString assembleArchiveCommand(const ComponentExample &example);

    static QString assembleTimes(const ComponentExample &example);

    static QString formatExampleOption(ComponentOptionBase *input, const ComponentExample &example);

public:
    static void optionSummaryOutput(int maximumOptionWidth,
                                    const QString &optionName,
                                    const QString &optionHelp,
                                    const QString &defaultBehavior = QString());

    enum ExampleMode {
        Example_DataFilter,
        Example_DataFilterSilent,
        Example_DataInputFile,
        Example_DataInputExternal,
        Example_Action,
        Example_ActionTimes,
        Example_ActionTimesOptional,
        Example_ExternalTimes,
        Example_ExternalTimesOptional,
    };

    static void optionExampleOuput(const QString &program,
                                   const QList<ComponentExample> &examples,
                                   ExampleMode mode,
                                   int allowStations = -1,
                                   int requireStations = 0);

    static void optionHelpOutput(ComponentOptionBase *help);

    static void parse(ComponentOptionBase *output,
                      const QString &value,
                      const QString &displayName = QString())
    noexcept(false);
};

}
}

#endif
