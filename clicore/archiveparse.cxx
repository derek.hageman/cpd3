/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QStringList>
#include <QDir>
#include <QSettings>

#include "clicore/archiveparse.hxx"
#include "core/timeparse.hxx"
#include "core/range.hxx"
#include "datacore/segment.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace CPD3 {
namespace CLI {

/** @file clicore/archiveparse.hxx
 * Provides utilities to parse archive specifications from the command line.
 */

AcrhiveParsingException::AcrhiveParsingException(const QString &d)
{
    description = d;
}

QString AcrhiveParsingException::getDescription() const
{
    return description;
}


namespace {
class ArchiveVariableTimeHandler : public TimeParseListHandler {
public:
    Archive::Access *access;
    Archive::Selection::Match checkStations;
    Archive::Selection::Match checkArchives;

    Archive::Selection::List selectedVariables;

    QString firstInvalidVariable;

    bool stopHandling;

    bool handleLeading(const QString &str, int, const QStringList &) noexcept(false) override
    {
        if (stopHandling)
            return false;
        if (str == ArchiveParse::tr("everything", "everything variable name")) {
            selectedVariables.clear();
            selectedVariables.emplace_back(FP::undefined(), FP::undefined(), checkStations,
                                           checkArchives);
            stopHandling = true;
            return true;
        }
        Archive::Selection::Match effectiveStations = checkStations;
        Archive::Selection::Match effectiveArchives = checkArchives;

        Archive::Selection::List addSelections;

        auto allItems = str.split(',', QString::KeepEmptyParts);
        for (const auto &variableItem : allItems) {
            auto items = variableItem.split(':', QString::KeepEmptyParts);
            if (items.empty()) {
                firstInvalidVariable = str;
                return false;
            }

            SequenceName::Component checkVariable;
            Archive::Selection::Match hasFlavors;
            Archive::Selection::Match lacksFlavors;
            Archive::Selection::Match exactFlavors;
            bool recalcArchives = false;
            bool recalcStations = false;
            if (items.length() == 1) {
                checkVariable = items.at(0).toStdString();
            } else if (items.length() == 2) {
                checkVariable = items.at(1).toStdString();
                if (items.at(0).length() > 0) {
                    effectiveArchives.clear();
                    effectiveArchives.emplace_back(items.at(0).toStdString());
                    recalcArchives = true;
                }
            } else {
                checkVariable = items.at(2).toStdString();
                if (items.at(1).length() > 0) {
                    effectiveArchives.clear();
                    effectiveArchives.emplace_back(items.at(1).toStdString());
                    recalcArchives = true;
                }
                if (items.at(0).length() > 0) {
                    effectiveStations.clear();
                    effectiveStations.emplace_back(items.at(0).toStdString());
                    recalcStations = true;
                }
                if (items.length() > 3) {
                    for (int i = 3, max = items.length(); i < max; i++) {
                        QString specifier(items.at(i));
                        if (specifier.length() == 0)
                            continue;
                        if (specifier.startsWith('=')) {
                            exactFlavors.emplace_back(specifier.mid(1).toStdString());
                        } else if (specifier.startsWith('!') || specifier.startsWith('-')) {
                            lacksFlavors.emplace_back(specifier.mid(1).toStdString());
                        } else if (specifier.startsWith('+')) {
                            hasFlavors.emplace_back(specifier.mid(1).toStdString());
                        } else {
                            hasFlavors.emplace_back(specifier.toStdString());
                        }
                    }
                }
            }
            if (checkVariable.length() == 0) {
                firstInvalidVariable = str;
                return false;
            }

            if (access->availableExists(
                    Archive::Selection(FP::undefined(), FP::undefined(), effectiveStations,
                                       effectiveArchives, {checkVariable}),
                    Archive::Access::IndexOnly)) {
                addSelections.emplace_back(
                        Archive::Selection(FP::undefined(), FP::undefined(), effectiveStations,
                                           effectiveArchives, {checkVariable}, hasFlavors,
                                           lacksFlavors, exactFlavors));
                continue;
            }


            Archive::Selection aliasSelection
                    (FP::undefined(), FP::undefined(), effectiveStations, effectiveArchives,
                     {"alias"});
            aliasSelection.includeMetaArchive = false;
            auto result = SequenceSegment::Stream::read(aliasSelection, access);

            if (recalcStations || recalcArchives) {
                auto check = access->availableSelected(
                        Archive::Selection(FP::undefined(), FP::undefined(), effectiveStations,
                                           effectiveArchives).withMetaArchive(false),
                        Archive::Access::IndexOnly);
                if (recalcStations) {
                    effectiveStations.clear();
                    std::copy(check.stations.begin(), check.stations.end(),
                              Util::back_emplacer(effectiveStations));
                }
                if (recalcArchives) {
                    effectiveArchives.clear();
                    std::copy(check.archives.begin(), check.archives.end(),
                              Util::back_emplacer(effectiveArchives));
                }
            }

            bool hitAny = false;

            QRegExp reAlias(QString::fromStdString(checkVariable));
            for (const auto &station : effectiveStations) {
                for (const auto &archive : effectiveArchives) {
                    SequenceName aliasUnit(station, archive, "alias");
                    for (auto &seg : result) {
                        for (auto i : seg.getValue(aliasUnit).toHash()) {
                            if ((reAlias.isValid() &&
                                    reAlias.exactMatch(QString::fromStdString(i.first))) ||
                                    (!reAlias.isValid() && i.first == checkVariable)) {
                                hitAny = true;

                                Archive::Selection::Match selectVariables;
                                for (auto v : i.second.toArray()) {
                                    const auto &addName = v.toString();
                                    if (!addName.empty())
                                        selectVariables.emplace_back(addName);
                                }

                                addSelections.emplace_back(
                                        Archive::Selection(seg.getStart(), seg.getEnd(), {station},
                                                           {archive}, selectVariables, hasFlavors,
                                                           lacksFlavors, exactFlavors));
                            }
                        }
                    }
                }
            }

            if (!hitAny) {
                firstInvalidVariable = str;
                return false;
            }
        }

        std::move(addSelections.begin(), addSelections.end(),
                  Util::back_emplacer(selectedVariables));

        return true;
    }

    bool handleTrailing(const QString &, int, const QStringList &) noexcept(false) override
    { return false; }
};
}

/**
 * Parse an argument list into a list of archive selections.  Throws an 
 * exception if there is an error in parsing the arguments.
 * 
 * @param arguments the argument list to parse
 * @param clip      the output clipping range or NULL
 * @param read      the archive reader to query or NULL to use the default
 * @param allowEmptySelection if true then "empty" selections are allowed and converted to maximally expansive ones
 * @param defaultStation the station to default to if no other one is specified, or empty for none
 * @param defaultArchive the archive to default to if no other one is specified, or empty for none
 * @return          a list of archive selections parsed from the arguments
 */
Archive::Selection::List ArchiveParse::parse(const QStringList &arguments,
                                             Time::Bounds *clip,
                                             Archive::Access *archive,
                                             bool allowEmptySelection,
                                             const SequenceName::Component &defaultStation,
                                             const SequenceName::Component &defaultArchive) noexcept(false)
{
    std::unique_ptr<Archive::Access> localAccess;
    if (!archive) {
        localAccess.reset(new Archive::Access);
        archive = localAccess.get();
    }

    Archive::Access::ReadLock lock(*archive);
    QStringList workingArguments(arguments);

    Archive::Selection::Match checkStations;
    bool useAllStations = false;
    bool usedStation = false;
    if (!workingArguments.isEmpty()) {
        for (const auto &add : workingArguments.front()
                                               .split(QRegExp("[:;,]+"), QString::SkipEmptyParts)) {
            if (add.toLower() != tr("allstations", "all stations string")) {
                checkStations.emplace_back(add.toStdString());
                continue;
            }
            checkStations.clear();
            useAllStations = true;
            usedStation = true;
            workingArguments.removeFirst();
            break;
        }
    }
    if (!checkStations.empty()) {
        /* Special cases to handle the first argument being a full
         * specification like "brw:raw:BsG_S11", which would otherwise
         * result in "brw" with the rest ignored.
         */
        if (checkStations.size() == 1 || !archive->availableStations({checkStations[1]}).empty()) {
            auto stations = archive->availableStations(checkStations);
            checkStations.clear();
            std::move(stations.begin(), stations.end(), Util::back_emplacer(checkStations));
            if (!checkStations.empty()) {
                usedStation = true;
                workingArguments.removeFirst();
            }
        } else {
            checkStations.clear();
        }
    }
    if (checkStations.empty() && !useAllStations) {
        checkStations.clear();
        auto implied = SequenceName::impliedStations(archive);
        std::copy(implied.begin(), implied.end(), Util::back_emplacer(checkStations));
    }
    if (checkStations.empty() && defaultStation.length() > 0 && !useAllStations)
        checkStations.emplace_back(defaultStation);
    if (workingArguments.isEmpty()) {
        if (allowEmptySelection) {
            Archive::Selection::List result;
            Archive::Selection::Match checkArchives;
            if (defaultArchive.length() > 0)
                checkArchives.emplace_back(defaultArchive);
            result.emplace_back(FP::undefined(), FP::undefined(), std::move(checkStations),
                                std::move(checkArchives));
            return result;
        }

        if (usedStation)
            throw AcrhiveParsingException(
                    tr("No variable specification detected.  The only arguments appear to be stations."));
        throw AcrhiveParsingException(tr("No data specified."));
    }

    Archive::Selection::Match checkArchives;
    bool usedArchive = false;
    bool useAllArchives = false;
    for (const auto &add : workingArguments.back()
                                           .split(QRegExp("[:;,]+"), QString::SkipEmptyParts)) {
        if (add.toLower() != tr("allarchives", "all archives string")) {
            checkArchives.emplace_back(add.toStdString());
            continue;
        }
        checkArchives.clear();
        workingArguments.removeLast();
        useAllArchives = true;
        usedArchive = true;
        break;
    }
    if (!checkArchives.empty()) {
        auto available = archive->availableArchives(checkArchives);
        checkArchives.clear();
        std::copy(available.begin(), available.end(), Util::back_emplacer(checkArchives));
        if (!checkArchives.empty()) {
            workingArguments.removeLast();
            usedArchive = true;
        }
    }
    if (checkArchives.empty() && defaultArchive.length() > 0 && !useAllArchives)
        checkArchives.emplace_back(defaultArchive);
    if (workingArguments.isEmpty()) {
        if (allowEmptySelection) {
            Archive::Selection::List result;
            result.emplace_back(FP::undefined(), FP::undefined(), std::move(checkStations),
                                std::move(checkArchives));
            return result;
        }

        if (usedArchive && usedStation)
            throw AcrhiveParsingException(
                    tr("No variable specification detected.  The only arguments appear to be stations and archives."));
        else if (usedArchive)
            throw AcrhiveParsingException(
                    tr("No variable specification detected.  The only arguments appear to be archives."));
        throw AcrhiveParsingException(tr("No data specified.\n"));
    }

    ArchiveVariableTimeHandler handler;
    handler.access = archive;
    handler.checkStations = checkStations;
    handler.checkArchives = checkArchives;
    handler.stopHandling = false;
    Time::Bounds bounds;
    try {
        bounds = TimeParse::parseListBounds(workingArguments, &handler, allowEmptySelection, true);
    } catch (TimeParsingException tpe) {
        throw AcrhiveParsingException(tr("Error parsing time: %1\n"
                                         "This may be the result of the variables argument(s) being invalid and "
                                         "being interpreted as a time argument.  Ensure that both the time specification "
                                         "and the variables listed are valid.  The first argument of the \"time\" was "
                                         "\"%2\" and the last one was \"%3\".").arg(
                tpe.getDescription(), handler.firstInvalidVariable, workingArguments.last()));
    }

    if (clip)
        *clip = bounds;

    if (handler.selectedVariables.empty()) {
        if (allowEmptySelection) {
            Archive::Selection::List result;
            result.emplace_back(bounds.start, bounds.end, std::move(checkStations),
                                std::move(checkArchives));
            return result;
        }

        throw AcrhiveParsingException(tr("No data specified.\n"));
    }

    Archive::Selection::List result;
    for (auto &sel : handler.selectedVariables) {
        if (Range::compareStartEnd(bounds.start, sel.end) >= 0)
            continue;
        if (Range::compareStartEnd(sel.start, bounds.end) >= 0)
            continue;
        if (Range::compareStart(sel.start, bounds.start) < 0)
            sel.start = bounds.start;
        if (Range::compareEnd(sel.end, bounds.end) > 0)
            sel.end = bounds.end;
        if (Range::compareStartEnd(sel.start, sel.end) >= 0)
            continue;
        result.emplace_back(std::move(sel));
    }

    if (result.empty()) {
        throw AcrhiveParsingException(tr("No data available in the given time range.\n"));
    }

    return result;
}


}
}
