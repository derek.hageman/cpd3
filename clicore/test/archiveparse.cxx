/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include <time.h>
#include <math.h>
#include <QtGlobal>
#include <QTest>
#include <QString>
#include <QTemporaryFile>

#include "clicore/archiveparse.hxx"
#include "core/number.hxx"
#include "core/range.hxx"
#include "core/timeutils.hxx"
#include "datacore/archive/access.hxx"

using namespace CPD3::CLI;
using namespace CPD3::Data;
using namespace CPD3;

Q_DECLARE_METATYPE(QSet<CPD3::Data::Archive::Selection>);

bool sortSelectionLessThan(const Archive::Selection &a, const Archive::Selection &b)
{
    int c = Range::compareStart(a.start, b.start);
    if (c != 0)
        return c < 0;

    if (a.stations.size() < b.stations.size())
        return true;
    else if (a.stations.size() > b.stations.size())
        return false;
    for (int i = 0, max = a.stations.size(); i < max; i++) {
        c = a.stations.at(i).compare(b.stations.at(i));
        if (c != 0)
            return c < 0;
    }

    if (a.archives.size() < b.archives.size())
        return true;
    else if (a.archives.size() > b.archives.size())
        return false;
    for (int i = 0, max = a.archives.size(); i < max; i++) {
        c = a.archives.at(i).compare(b.archives.at(i));
        if (c != 0)
            return c < 0;
    }

    if (a.variables.size() < b.variables.size())
        return true;
    else if (a.variables.size() > b.variables.size())
        return false;
    for (int i = 0, max = a.variables.size(); i < max; i++) {
        c = a.variables.at(i).compare(b.variables.at(i));
        if (c != 0)
            return c < 0;
    }

    return false;
}

namespace CPD3 {
namespace Data {
namespace Archive {
bool operator==(const Selection &a, const Selection &b)
{
    if (!FP::equal(a.start, b.start))
        return false;
    if (!FP::equal(a.end, b.end))
        return false;
    if (!FP::equal(a.modifiedAfter, b.modifiedAfter))
        return false;
    if (a.includeDefaultStation != b.includeDefaultStation)
        return false;
    if (a.includeMetaArchive != b.includeMetaArchive)
        return false;

    auto al = a.stations;
    std::sort(al.begin(), al.end());
    auto bl = b.stations;
    std::sort(bl.begin(), bl.end());
    if (al != bl)
        return false;

    al = a.archives;
    std::sort(al.begin(), al.end());
    bl = b.archives;
    std::sort(bl.begin(), bl.end());
    if (al != bl)
        return false;

    al = a.variables;
    std::sort(al.begin(), al.end());
    bl = b.variables;
    std::sort(bl.begin(), bl.end());
    if (al != bl)
        return false;

    al = a.hasFlavors;
    std::sort(al.begin(), al.end());
    bl = b.hasFlavors;
    std::sort(bl.begin(), bl.end());
    if (al != bl)
        return false;

    al = a.lacksFlavors;
    std::sort(al.begin(), al.end());
    bl = b.lacksFlavors;
    std::sort(bl.begin(), bl.end());
    if (al != bl)
        return false;

    al = a.exactFlavors;
    std::sort(al.begin(), al.end());
    bl = b.exactFlavors;
    std::sort(bl.begin(), bl.end());
    if (al != bl)
        return false;

    return true;
}

uint qHash(const Selection &key)
{
    return ::qHash(key.start) ^ ::qHash(key.end) ^ ::qHash(key.modifiedAfter);
}
}
}
}

namespace QTest {
template<>
char *toString(const QSet<CPD3::Data::Archive::Selection> &sels)
{
    QByteArray ba = "SelectionSet(";
    QList<CPD3::Data::Archive::Selection>
            printList(QList<CPD3::Data::Archive::Selection>::fromSet(sels));
    std::sort(printList.begin(), printList.end(), sortSelectionLessThan);
    for (int i = 0, max = printList.size(); i < max; i++) {
        CPD3::Data::Archive::Selection s(printList.at(i));
        if (i != 0) ba += ", ";
        ba += "[" +
                (CPD3::FP::defined(s.start) ? QString::number(s.start, 'f', 0) : "-INF") +
                ", " +
                (CPD3::FP::defined(s.end) ? QString::number(s.end, 'f', 0) : "INF") +
                ", S(" + Util::to_qstringlist(s.stations).join(",") +
                "), A(" + Util::to_qstringlist(s.archives).join(",") +
                "), V(" + Util::to_qstringlist(s.variables).join(",") +
                "), HF(" + Util::to_qstringlist(s.hasFlavors).join(",") +
                "), LF(" + Util::to_qstringlist(s.lacksFlavors).join(",") +
                "), " +
                (CPD3::FP::defined(s.modifiedAfter) ? QString::number(s.modifiedAfter) : "ANY") +
                "]";
    }
    ba += ")";
    return qstrdup(ba.data());
}
}

class TestArchiveParse : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

private slots:

    void initTestCase()
    {
        QVERIFY(databaseFile.open());

        SequenceValue::Transfer input;
        {
            Variant::Root v;
            v["S11a/#0"] = "BsG_S11";
            v["S11a/#1"] = "BsB_S11";
            input.emplace_back(SequenceName("brw", "raw", "alias"), v, FP::undefined(), 1262304000);
        }
        {
            Variant::Root v;
            v["S11a/#0"] = "BsG_S11";
            v["S11a/#1"] = "BsR_S11";
            input.emplace_back(SequenceName("brw", "raw", "alias"), v, 1262304000, FP::undefined());
        }

        input.emplace_back(SequenceName("brw", "raw", "BsG_S11", {"pm1"}), Variant::Root(1.0),
                           FP::undefined(), 1293840000);
        input.emplace_back(SequenceName("brw", "raw", "BsG_S11", {"pm10"}), Variant::Root(2.0),
                           1293840000, FP::undefined());
        input.emplace_back(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(3.0),
                           FP::undefined(), FP::undefined());
        input.emplace_back(SequenceName("brw", "raw", "BsR_S11"), Variant::Root(4.0),
                           FP::undefined(), FP::undefined());
        input.emplace_back(SequenceName("brw", "clean", "BsG_S11"), Variant::Root(5.0),
                           FP::undefined(), FP::undefined());
        input.emplace_back(SequenceName("alt", "raw", "BsG_S11"), Variant::Root(6.0),
                           FP::undefined(), FP::undefined());

        Archive::Access(databaseFile).writeSynchronous(input);
    }


    void parse()
    {
        QFETCH(QStringList, arguments);
        QFETCH(bool, allowEmptySelection);
        QFETCH(QString, defaultStation);
        QFETCH(QString, defaultArchive);
        QFETCH(bool, expectFailure);
        QFETCH(Time::Bounds, expectedClip);
        QFETCH(QSet<Archive::Selection>, expectedSelections);

        Archive::Access reader(databaseFile);

        try {
            Time::Bounds clip(FP::undefined(), FP::undefined());
            QSet<Archive::Selection> result;
            for (const auto &sel : ArchiveParse::parse(arguments, &clip, &reader,
                                                       allowEmptySelection,
                                                       defaultStation.toStdString(),
                                                       defaultArchive.toStdString())) {
                result.insert(sel);
            }
            if (expectFailure)
                QFAIL("Expected an exception to be thrown but none was.");

            QCOMPARE(result, expectedSelections);
            QCOMPARE(clip, expectedClip);
        } catch (AcrhiveParsingException ape) {
            if (!expectFailure) {
                QFAIL(qPrintable(ape.getDescription()));
            }
        }
    }

    void parse_data()
    {
        QTest::addColumn<QStringList>("arguments");
        QTest::addColumn<bool>("allowEmptySelection");
        QTest::addColumn<QString>("defaultStation");
        QTest::addColumn<QString>("defaultArchive");
        QTest::addColumn<bool>("expectFailure");
        QTest::addColumn<Time::Bounds>("expectedClip");
        QTest::addColumn<QSet<Archive::Selection> >("expectedSelections");


        QTest::newRow("Empty failure") << QStringList() << false << QString() << QString() << true
                                       << Time::Bounds(FP::undefined(), FP::undefined())
                                       << QSet<Archive::Selection>();
        QTest::newRow("Empty defaults") << QStringList() << true << QString("brw") << QString("raw")
                                        << false << Time::Bounds(FP::undefined(), FP::undefined())
                                        << (QSet<Archive::Selection>()
                                                << Archive::Selection(FP::undefined(),
                                                                      FP::undefined(), {"brw"},
                                                                      {"raw"}));

        QTest::newRow("Time delimited") << (QStringList("2010:1") << "1d") << true << QString()
                                        << QString("raw") << false
                                        << Time::Bounds(1262304000.0, 1262390400.0)
                                        << (QSet<Archive::Selection>()
                                                << Archive::Selection(1262304000.0, 1262390400.0,
                                                                      {}, {"raw"}));

        QTest::newRow("Station only failure") << QStringList("brw") << false << QString("brw")
                                              << QString("raw") << true
                                              << Time::Bounds(FP::undefined(), FP::undefined())
                                              << QSet<Archive::Selection>();
        QTest::newRow("Station only") << QStringList("brw") << true << QString("alt")
                                      << QString("raw") << false
                                      << Time::Bounds(FP::undefined(), FP::undefined())
                                      << (QSet<Archive::Selection>()
                                              << Archive::Selection(FP::undefined(),
                                                                    FP::undefined(), {"brw"},
                                                                    {"raw"}));

        QTest::newRow("Station and archive failure") << (QStringList() << "brw" << "raw") << false
                                                     << QString("brw") << QString("raw") << true
                                                     << Time::Bounds(FP::undefined(),
                                                                     FP::undefined())
                                                     << QSet<Archive::Selection>();
        QTest::newRow("Station and archive") << (QStringList() << "brw" << "raw") << true
                                             << QString("alt") << QString("clean") << false
                                             << Time::Bounds(FP::undefined(), FP::undefined())
                                             << (QSet<Archive::Selection>()
                                                     << Archive::Selection(FP::undefined(),
                                                                           FP::undefined(), {"brw"},
                                                                           {"raw"}));

        QTest::newRow("Station, archive, and variable failure")
                << (QStringList() << "brw" << "BsG_S11" << "raw") << false << QString("brw")
                << QString("raw") << true << Time::Bounds(FP::undefined(), FP::undefined())
                << QSet<Archive::Selection>();
        QTest::newRow("Station, archive, and variable")
                << (QStringList() << "brw" << "BsG_S11" << "raw") << true << QString("alt")
                << QString("clean") << false << Time::Bounds(FP::undefined(), FP::undefined())
                << (QSet<Archive::Selection>()
                        << Archive::Selection(FP::undefined(), FP::undefined(), {"brw"}, {"raw"},
                                              {"BsG_S11"}));

        QTest::newRow("Station and variable failure") << (QStringList() << "brw" << "BsG_S11")
                                                      << false << QString("brw") << QString("raw")
                                                      << true << Time::Bounds(FP::undefined(),
                                                                              FP::undefined())
                                                      << QSet<Archive::Selection>();
        QTest::newRow("Station and variable") << (QStringList() << "brw" << "BsG_S11") << true
                                              << QString("alt") << QString("raw") << false
                                              << Time::Bounds(FP::undefined(), FP::undefined())
                                              << (QSet<Archive::Selection>()
                                                      << Archive::Selection(FP::undefined(),
                                                                            FP::undefined(),
                                                                            {"brw"}, {"raw"},
                                                                            {"BsG_S11"}));

        QTest::newRow("Variable failure") << (QStringList() << "BsG_S11") << false << QString("brw")
                                          << QString("raw") << true
                                          << Time::Bounds(FP::undefined(), FP::undefined())
                                          << QSet<Archive::Selection>();
        QTest::newRow("Variable") << (QStringList() << "BsG_S11") << true << QString("alt")
                                  << QString("raw") << false
                                  << Time::Bounds(FP::undefined(), FP::undefined())
                                  << (QSet<Archive::Selection>()
                                          << Archive::Selection(FP::undefined(), FP::undefined(),
                                                                {"alt"}, {"raw"}, {"BsG_S11"}));

        QTest::newRow("Full specification failure")
                << (QStringList() << "brw" << "BsG_S11" << "garbage" << "raw") << false
                << QString("brw") << QString("raw") << true
                << Time::Bounds(FP::undefined(), FP::undefined()) << QSet<Archive::Selection>();
        QTest::newRow("Full specification")
                << (QStringList() << "brw" << "BsG_S11" << "2010" << "2011" << "raw") << false
                << QString("alt") << QString("raw") << false
                << Time::Bounds(1262304000.0, 1293840000.0) << (QSet<Archive::Selection>()
                << Archive::Selection(1262304000.0, 1293840000.0, {"brw"}, {"raw"}, {"BsG_S11"}));
        QTest::newRow("Full specification archive override")
                << (QStringList() << "brw" << "clean:BsG_S11" << "2010" << "2011") << false
                << QString("alt") << QString("raw") << false
                << Time::Bounds(1262304000.0, 1293840000.0) << (QSet<Archive::Selection>()
                << Archive::Selection(1262304000.0, 1293840000.0, {"brw"}, {"clean"}, {"BsG_S11"}));
        QTest::newRow("Full specification station override")
                << (QStringList() << "brw" << "alt::BsG_S11" << "2010" << "2011") << false
                << QString("alt") << QString("raw") << false
                << Time::Bounds(1262304000.0, 1293840000.0) << (QSet<Archive::Selection>()
                << Archive::Selection(1262304000.0, 1293840000.0, {"alt"}, {"raw"}, {"BsG_S11"}));
        QTest::newRow("Full specification both override")
                << (QStringList() << "brw" << "alt:raw:BsG_S11" << "2010" << "2011") << false
                << QString("brw") << QString("clean") << false
                << Time::Bounds(1262304000.0, 1293840000.0) << (QSet<Archive::Selection>()
                << Archive::Selection(1262304000.0, 1293840000.0, {"alt"}, {"raw"}, {"BsG_S11"}));
        QTest::newRow("Times no station")
                << (QStringList() << "alt:raw:BsG_S11" << "2010" << "2011") << false << QString()
                << QString("clean") << false << Time::Bounds(1262304000.0, 1293840000.0)
                << (QSet<Archive::Selection>()
                        << Archive::Selection(1262304000.0, 1293840000.0, {"alt"}, {"raw"},
                                              {"BsG_S11"}));

        QTest::newRow("Flavors")
                << (QStringList() << "brw" << "::BsG_S11:pm1:!pm10" << "2010" << "2011") << false
                << QString("alt") << QString("raw") << false
                << Time::Bounds(1262304000.0, 1293840000.0) << (QSet<Archive::Selection>()
                << Archive::Selection(1262304000.0, 1293840000.0, {"brw"}, {"raw"}, {"BsG_S11"},
                                      {"pm1"}, {"pm10"}));
        QTest::newRow("Flavors exact")
                << (QStringList() << "brw" << "::BsG_S11:=pm1" << "2010" << "2011") << false
                << QString("alt") << QString("raw") << false
                << Time::Bounds(1262304000.0, 1293840000.0) << (QSet<Archive::Selection>()
                << Archive::Selection(1262304000.0, 1293840000.0, {"brw"}, {"raw"}, {"BsG_S11"}, {},
                                      {}, {"pm1"}));
        QTest::newRow("Flavors exact none")
                << (QStringList() << "brw" << "::BsB_S11:=" << "2010" << "2011") << false
                << QString("alt") << QString("raw") << false
                << Time::Bounds(1262304000.0, 1293840000.0) << (QSet<Archive::Selection>()
                << Archive::Selection(1262304000.0, 1293840000.0, {"brw"}, {"raw"}, {"BsB_S11"}, {},
                                      {}, {""}));

        QTest::newRow("Alias simple") << (QStringList() << "brw" << "S11a" << "2008" << "2009")
                                      << false << QString() << QString("raw") << false
                                      << Time::Bounds(1199145600.0, 1230768000.0)
                                      << (QSet<Archive::Selection>()
                                              << Archive::Selection(1199145600.0, 1230768000.0,
                                                                    {"brw"}, {"raw"},
                                                                    {"BsG_S11", "BsB_S11"}));
        QTest::newRow("Alias changed") << (QStringList() << "brw" << "S11a" << "2009" << "2011")
                                       << false << QString() << QString("raw") << false
                                       << Time::Bounds(1230768000.0, 1293840000.0)
                                       << (QSet<Archive::Selection>()
                                               << Archive::Selection(1230768000.0, 1262304000.0,
                                                                     {"brw"}, {"raw"},
                                                                     {"BsG_S11", "BsB_S11"})
                                               << Archive::Selection(1262304000.0, 1293840000.0,
                                                                     {"brw"}, {"raw"},
                                                                     {"BsG_S11", "BsR_S11"}));
    }
};

QTEST_APPLESS_MAIN(TestArchiveParse)

#include "archiveparse.moc"
