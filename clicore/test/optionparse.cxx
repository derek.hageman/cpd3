/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <time.h>
#include <math.h>
#include <QtGlobal>
#include <QTest>
#include <QString>

#include "clicore/optionparse.hxx"
#include "core/component.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "datacore/dynamictimeinterval.hxx"

using namespace CPD3;
using namespace CPD3::CLI;
using namespace CPD3::Data;

typedef QMap<double, double> DoubleToDoubleMap;

Q_DECLARE_METATYPE(DoubleToDoubleMap);
typedef QMap<double, SequenceName::Set> DoubleToUnitSetMap;

Q_DECLARE_METATYPE(DoubleToUnitSetMap);
typedef QMap<double, QString> DoubleToStringMap;

Q_DECLARE_METATYPE(DoubleToStringMap);
typedef QMap<double, bool> DoubleToBoolMap;

Q_DECLARE_METATYPE(DoubleToBoolMap);
typedef QMap<double, qint64> DoubleToIntegerMap;

Q_DECLARE_METATYPE(DoubleToIntegerMap);

class TestOptionParse : public QObject {
Q_OBJECT
private slots:

    void testBoolean()
    {
        ComponentOptionBoolean option("", "", "", "");

        option.set(false);
        OptionParse::parse(&option, "true");
        QCOMPARE(option.get(), true);
        OptionParse::parse(&option, "false");
        QCOMPARE(option.get(), false);
        OptionParse::parse(&option, "on");
        QCOMPARE(option.get(), true);
        OptionParse::parse(&option, "off");
        QCOMPARE(option.get(), false);
        OptionParse::parse(&option, "yes");
        QCOMPARE(option.get(), true);
        OptionParse::parse(&option, "no");
        QCOMPARE(option.get(), false);

        try {
            OptionParse::parse(&option, "garbage");
            QFAIL("Parsing didn't catch a bad value.");
        } catch (OptionParsingException ope) { }
    }

    void testString()
    {
        ComponentOptionSingleString option("", "", "", "");
        OptionParse::parse(&option, "value");
        QCOMPARE(option.get(), QString("value"));
        OptionParse::parse(&option, "a");
        QCOMPARE(option.get(), QString("a"));
    }

    void testDouble()
    {
        ComponentOptionSingleDouble option("", "", "", "");
        option.setAllowUndefined(true);

        option.set(1.0);
        OptionParse::parse(&option, "");
        QVERIFY(!FP::defined(option.get()));
        OptionParse::parse(&option, "mvc");
        QVERIFY(!FP::defined(option.get()));
        OptionParse::parse(&option, "undef");
        QVERIFY(!FP::defined(option.get()));
        OptionParse::parse(&option, "undefined");
        QVERIFY(!FP::defined(option.get()));
        OptionParse::parse(&option, "1.0");
        QCOMPARE(option.get(), 1.0);

        try {
            OptionParse::parse(&option, "blarg");
            QFAIL("Parsing didn't catch a bad value.");
        } catch (OptionParsingException ope) { }

        option.setAllowUndefined(false);
        try {
            OptionParse::parse(&option, "");
            QFAIL("Parsing didn't catch an undefined value.");
        } catch (OptionParsingException ope) { }

        option.setMaximum(10.0, true);
        OptionParse::parse(&option, "10.0");
        QCOMPARE(option.get(), 10.0);
        try {
            OptionParse::parse(&option, "11.0");
            QFAIL("Parsing didn't catch a maximum exclusive value.");
        } catch (OptionParsingException ope) { }
        option.setMaximum(10.0, false);
        OptionParse::parse(&option, "9.0");
        QCOMPARE(option.get(), 9.0);
        try {
            OptionParse::parse(&option, "10.0");
            QFAIL("Parsing didn't catch a maximum inclusive value.");
        } catch (OptionParsingException ope) { }

        option.setMinimum(1.0, true);
        OptionParse::parse(&option, "1.0");
        QCOMPARE(option.get(), 1.0);
        try {
            OptionParse::parse(&option, "0.0");
            QFAIL("Parsing didn't catch a minimum exclusive value.");
        } catch (OptionParsingException ope) { }
        option.setMinimum(1.0, false);
        OptionParse::parse(&option, "2.0");
        QCOMPARE(option.get(), 2.0);
        try {
            OptionParse::parse(&option, "0.0");
            QFAIL("Parsing didn't catch a minimum inclusive value.");
        } catch (OptionParsingException ope) { }
    }

    void testInteger()
    {
        ComponentOptionSingleInteger option("", "", "", "");
        option.setAllowUndefined(true);

        option.set(1);
        OptionParse::parse(&option, "");
        QVERIFY(!INTEGER::defined(option.get()));
        OptionParse::parse(&option, "mvc");
        QVERIFY(!INTEGER::defined(option.get()));
        OptionParse::parse(&option, "undef");
        QVERIFY(!INTEGER::defined(option.get()));
        OptionParse::parse(&option, "undefined");
        QVERIFY(!INTEGER::defined(option.get()));
        OptionParse::parse(&option, "1");
        QCOMPARE(option.get(), Q_INT64_C(1));

        try {
            OptionParse::parse(&option, "blarg");
            QFAIL("Parsing didn't catch a bad value.");
        } catch (OptionParsingException ope) { }

        option.setAllowUndefined(false);
        try {
            OptionParse::parse(&option, "");
            QFAIL("Parsing didn't catch an undefined value.");
        } catch (OptionParsingException ope) { }

        option.setMaximum(10);
        OptionParse::parse(&option, "10");
        QCOMPARE(option.get(), Q_INT64_C(10));
        try {
            OptionParse::parse(&option, "11");
            QFAIL("Parsing didn't catch a maximum value.");
        } catch (OptionParsingException ope) { }

        option.setMinimum(1);
        OptionParse::parse(&option, "1");
        QCOMPARE(option.get(), Q_INT64_C(1));
        try {
            OptionParse::parse(&option, "-1");
            QFAIL("Parsing didn't catch a minimum value.");
        } catch (OptionParsingException ope) { }
    }

    void testStringSet()
    {
        ComponentOptionStringSet option("", "", "", "");
        OptionParse::parse(&option, "value");
        QCOMPARE(option.get(), (QSet<QString>() << "value"));
        OptionParse::parse(&option, "ab,cd;ef,,gh,ab");
        QCOMPARE(option.get(), (QSet<QString>() << "ab" << "cd" << "ef" << "gh"));
    }

    void testInstrumentSuffixSet()
    {
        ComponentOptionInstrumentSuffixSet option("", "", "", "");
        OptionParse::parse(&option, "S11");
        QCOMPARE(option.get(), (QSet<QString>() << "S11"));
        OptionParse::parse(&option, "S11,S12,S12");
        QCOMPARE(option.get(), (QSet<QString>() << "S11" << "S12"));
    }

    void testEnum()
    {
        ComponentOptionEnum option("", "", "", "");
        option.add(0, "i0", "e0", "");
        option.add(1, "i1", "e1", "");
        option.add(2, "i2", "e2", "");
        option.alias(0, "a0a");
        option.alias(0, "a0b");
        option.alias(1, "a1");

        option.set("i2");
        OptionParse::parse(&option, "e0");
        QCOMPARE(option.get().getID(), 0);
        OptionParse::parse(&option, "e2");
        QCOMPARE(option.get().getID(), 2);
        OptionParse::parse(&option, "a0a");
        QCOMPARE(option.get().getID(), 0);
        OptionParse::parse(&option, "a1");
        QCOMPARE(option.get().getID(), 1);

        try {
            OptionParse::parse(&option, "garbage");
            QFAIL("Parsing didn't catch a bad value.");
        } catch (OptionParsingException ope) { }
    }

    void testTimeOffset()
    {
        ComponentOptionTimeOffset option("", "", "", "");
        OptionParse::parse(&option, "2week");
        QCOMPARE(option.getUnit(), Time::Week);
        QCOMPARE(option.getCount(), 2);
        QCOMPARE(option.getAlign(), false);
    }

    void testTime()
    {
        ComponentOptionSingleTime option("", "", "", "");
        OptionParse::parse(&option, "2010-02-01T00:00:01Z");
        QCOMPARE(option.get(), 1264982401.0);
    }

    void testCalibration()
    {
        ComponentOptionSingleCalibration option("", "", "", "");
        option.setAllowInvalid(true);

        option.set(Calibration());
        OptionParse::parse(&option, "");
        QCOMPARE(option.get().size(), 0);
        OptionParse::parse(&option, "undef");
        QCOMPARE(option.get().size(), 0);
        OptionParse::parse(&option, "undefined");
        QCOMPARE(option.get().size(), 0);
        OptionParse::parse(&option, "invalid");
        QCOMPARE(option.get().size(), 0);
        OptionParse::parse(&option, "1.0");
        QCOMPARE(option.get().size(), 1);
        QCOMPARE(option.get().get(0), 1.0);
        OptionParse::parse(&option, "0.0,1.0,2.0");
        QCOMPARE(option.get().size(), 3);
        QCOMPARE(option.get().get(0), 0.0);
        QCOMPARE(option.get().get(1), 1.0);
        QCOMPARE(option.get().get(2), 2.0);

        try {
            OptionParse::parse(&option, "blarg");
            QFAIL("Parsing didn't catch a bad value.");
        } catch (OptionParsingException ope) { }

        option.setAllowInvalid(false);
        try {
            OptionParse::parse(&option, "");
            QFAIL("Parsing didn't catch an invalid value.");
        } catch (OptionParsingException ope) { }
        try {
            OptionParse::parse(&option, "invalid");
            QFAIL("Parsing didn't catch an invalid value.");
        } catch (OptionParsingException ope) { }

        option.setAllowConstant(false);
        try {
            OptionParse::parse(&option, "2.0");
            QFAIL("Parsing didn't catch a constant value.");
        } catch (OptionParsingException ope) { }
    }

    void testFilterInput()
    {
        QFETCH(QString, input);
        QFETCH(DoubleToDoubleMap, expected);

        DynamicInputOption option("", "", "", "");

        try {
            OptionParse::parse(&option, input);
        } catch (OptionParsingException ope) {
            QFAIL(qPrintable(ope.getDescription()));
        }

        DynamicInput *check = option.getInput();

        SequenceSegment seg;
        seg.setValue(SequenceName("brw", "raw", "v1"), Variant::Root(1.0));
        seg.setValue(SequenceName("brw", "raw", "v2"), Variant::Root(2.0));
        seg.setValue(SequenceName("brw", "raw", "vu"), Variant::Root());
        check->registerInput(SequenceName("brw", "raw", "v1"));
        check->registerInput(SequenceName("brw", "raw", "v2"));
        check->registerInput(SequenceName("brw", "raw", "vu"));

        for (QMap<double, double>::const_iterator it = expected.constBegin();
                it != expected.constEnd();
                ++it) {
            seg.setStart(it.key());
            seg.setEnd(it.key() + 1.0);

            QCOMPARE(check->getConst(seg), it.value());
        }

        delete check;
    }

    void testFilterInput_data()
    {
        QTest::addColumn<QString>("input");
        QTest::addColumn<DoubleToDoubleMap>("expected");

        QMap<double, double> expected;

        expected.clear();
        expected.insert(1262304000.0, 42.0);
        QTest::newRow("Constant") << QString("42.0") << expected;

        expected.clear();
        expected.insert(1262304000.0, 1.0);
        QTest::newRow("Always value") << QString("v1") << expected;
        QTest::newRow("Always value archive") << QString("raw:v1") << expected;
        QTest::newRow("Always value full") << QString("brw:raw:v1:!pm1") << expected;
        QTest::newRow("Always value flavors") << QString("::v1:!pm10") << expected;

        expected.clear();
        expected.insert(1230768000.0, 42.0);
        expected.insert(1293840000.0, 43.0);
        QTest::newRow("Constant time range") << QString("42.0,,2010/43,2010,") << expected;

        expected.clear();
        expected.insert(1230768000.0, 1.0);
        expected.insert(1293840000.0, 2.0);
        QTest::newRow("Variable time range") << QString("v1,,,,2010/v2,,,2010,") << expected;

        expected.clear();
        expected.insert(1167609600.0, 5.0);
        expected.insert(1214956800.0, 1.0);
        expected.insert(1246536000.0, 0.5);
        expected.insert(1293840000.0, 6.0);
        QTest::newRow("Complex")
                << QString("5.0,,2008/v1,,,2008,2009/v1,0;0.5,,2009,2010/vu,,6,2010,") << expected;
    }

    void testFilterInputFailure()
    {
        DynamicInputOption option("", "", "", "");

        try {
            OptionParse::parse(&option, "");
            QFAIL("Parsing didn't catch an empty value.");
        } catch (OptionParsingException ope) { }

        try {
            OptionParse::parse(&option, "/v1,,,blarg,blarg");
            QFAIL("Parsing didn't catch a bad time range.");
        } catch (OptionParsingException ope) { }

        try {
            OptionParse::parse(&option, "/v1,,fab,,");
            QFAIL("Parsing didn't catch a bad default value.");
        } catch (OptionParsingException ope) { }
    }


    void testFilterOperate()
    {
        QFETCH(QString, input);
        QFETCH(DoubleToUnitSetMap, expected);

        DynamicSequenceSelectionOption option("", "", "", "");

        try {
            OptionParse::parse(&option, input);
        } catch (OptionParsingException ope) {
            QFAIL(qPrintable(ope.getDescription()));
        }

        DynamicSequenceSelection *check = option.getOperator();

        check->registerInput(SequenceName("brw", "raw", "v1"));
        check->registerInput(SequenceName("brw", "raw", "v2"));

        for (QMap<double, SequenceName::Set>::const_iterator it = expected.constBegin();
                it != expected.constEnd();
                ++it) {
            QCOMPARE(check->getConst(it.key()), it.value());
        }

        delete check;
    }

    void testFilterOperate_data()
    {
        QTest::addColumn<QString>("input");
        QTest::addColumn<DoubleToUnitSetMap>("expected");

        QMap<double, SequenceName::Set> expected;

        expected.clear();
        expected.insert(1262304000.0, SequenceName::Set{SequenceName("brw", "raw", "v1")});
        QTest::newRow("Always") << QString("v1") << expected;
        QTest::newRow("Always archive") << QString("raw:v1") << expected;
        QTest::newRow("Always station") << QString("brw:raw:v1") << expected;
        QTest::newRow("Always flavors") << QString("brw:raw:v1:!pm1") << expected;

        expected.clear();
        expected.insert(1262304000.0, SequenceName::Set{SequenceName("brw", "raw", "v1"),
                                                        SequenceName("brw", "raw", "v2")});
        QTest::newRow("Always multiple") << QString("v.") << expected;

        expected.clear();
        expected.insert(1230768000.0, SequenceName::Set{SequenceName("brw", "raw", "v1")});
        expected.insert(1293840000.0, SequenceName::Set{SequenceName("brw", "raw", "v2")});
        QTest::newRow("Transition") << QString("v1,,1262304000/v2,1262304000,") << expected;

    }

    void testFilterOperateFailure()
    {
        DynamicSequenceSelectionOption option("", "", "", "");

        try {
            OptionParse::parse(&option, "");
            QFAIL("Parsing didn't catch an empty value.");
        } catch (OptionParsingException ope) { }

        try {
            OptionParse::parse(&option, "//");
            QFAIL("Parsing didn't catch an empty complex value.");
        } catch (OptionParsingException ope) { }

        try {
            OptionParse::parse(&option, "/v1,blarg,blarg");
            QFAIL("Parsing didn't catch a bad time range.");
        } catch (OptionParsingException ope) { }
    }


    void testTimeInputString()
    {
        QFETCH(QString, input);
        QFETCH(DoubleToStringMap, expected);

        DynamicStringOption option("", "", "", "");

        try {
            OptionParse::parse(&option, input);
        } catch (OptionParsingException ope) {
            QFAIL(qPrintable(ope.getDescription()));
        }

        DynamicString *check = option.getInput();

        for (DoubleToStringMap::const_iterator it = expected.constBegin();
                it != expected.constEnd();
                ++it) {
            QCOMPARE(check->getConst(it.key()), it.value());
        }

        delete check;
    }

    void testTimeInputString_data()
    {
        QTest::addColumn<QString>("input");
        QTest::addColumn<DoubleToStringMap>("expected");

        DoubleToStringMap expected;

        expected.clear();
        expected.insert(1262304000.0, QString("asd"));
        expected.insert(1262305000.0, QString("asd"));
        QTest::newRow("Always single") << QString("asd") << expected;

        QTest::newRow("Always single times") << QString("asd,,/") << expected;

        expected.clear();
        expected.insert(1262304000.0, QString("asd/"));
        expected.insert(1262305000.0, QString("asd/"));
        QTest::newRow("Always single escaped") << QString("asd\\/") << expected;

        expected.clear();
        expected.insert(1230768000.0, "AAA");
        expected.insert(1293840000.0, "BBB");
        QTest::newRow("Transition") << QString("AAA,,1262304000/BBB,1262304000,") << expected;


        expected.clear();
        expected.insert(1230768000.0, "AA/A");
        expected.insert(1293840000.0, "BBB");
        QTest::newRow("Transition escaped") << QString("AA\\/A,,1262304000/BBB,1262304000,")
                                            << expected;

    }

    void testTimeInputStringFailure()
    {
        DynamicStringOption option("", "", "", "");

        try {
            OptionParse::parse(&option, "//");
            QFAIL("Parsing didn't catch an empty complex value.");
        } catch (OptionParsingException ope) { }

        try {
            OptionParse::parse(&option, "/v1,blarg,blarg");
            QFAIL("Parsing didn't catch a bad time range.");
        } catch (OptionParsingException ope) { }
    }


    void testTimeInputBool()
    {
        QFETCH(QString, input);
        QFETCH(DoubleToBoolMap, expected);

        DynamicBoolOption option("", "", "", "");

        try {
            OptionParse::parse(&option, input);
        } catch (OptionParsingException ope) {
            QFAIL(qPrintable(ope.getDescription()));
        }

        DynamicBool *check = option.getInput();

        for (DoubleToBoolMap::const_iterator it = expected.constBegin();
                it != expected.constEnd();
                ++it) {
            QCOMPARE(check->getConst(it.key()), it.value());
        }

        delete check;
    }

    void testTimeInputBool_data()
    {
        QTest::addColumn<QString>("input");
        QTest::addColumn<DoubleToBoolMap>("expected");

        DoubleToBoolMap expected;

        expected.clear();
        expected.insert(1262304000.0, true);
        expected.insert(1262305000.0, true);
        QTest::newRow("Always single") << QString("true") << expected;
        QTest::newRow("Always single number") << QString("1") << expected;
        QTest::newRow("Always single times") << QString("on,,/") << expected;

        expected.clear();
        expected.insert(1262304000.0, false);
        expected.insert(1262305000.0, false);
        QTest::newRow("Always off") << QString("false") << expected;

        expected.clear();
        expected.insert(1230768000.0, true);
        expected.insert(1293840000.0, false);
        QTest::newRow("Transition") << QString("on,,1262304000/off,1262304000,") << expected;

    }

    void testTimeInputBoolFailure()
    {
        DynamicBoolOption option("", "", "", "");

        try {
            OptionParse::parse(&option, "//");
            QFAIL("Parsing didn't catch an empty complex value.");
        } catch (OptionParsingException ope) { }

        try {
            OptionParse::parse(&option, "/v1,blarg,blarg");
            QFAIL("Parsing didn't catch a bad time range.");
        } catch (OptionParsingException ope) { }
    }


    void testTimeInputDouble()
    {
        QFETCH(QString, input);
        QFETCH(DoubleToDoubleMap, expected);

        DynamicDoubleOption option("", "", "", "");

        try {
            OptionParse::parse(&option, input);
        } catch (OptionParsingException ope) {
            QFAIL(qPrintable(ope.getDescription()));
        }

        DynamicDouble *check = option.getInput();

        for (DoubleToDoubleMap::const_iterator it = expected.constBegin();
                it != expected.constEnd();
                ++it) {
            QCOMPARE(check->getConst(it.key()), it.value());
        }

        delete check;
    }

    void testTimeInputDouble_data()
    {
        QTest::addColumn<QString>("input");
        QTest::addColumn<DoubleToDoubleMap>("expected");

        DoubleToDoubleMap expected;

        expected.clear();
        expected.insert(1262304000.0, 1.0);
        expected.insert(1262305000.0, 1.0);
        QTest::newRow("Always single") << QString("1.0") << expected;

        expected.clear();
        expected.insert(1230768000.0, 1.0);
        expected.insert(1293840000.0, 2.0);
        QTest::newRow("Transition") << QString("1.0,,1262304000/2.0,1262304000,") << expected;

    }

    void testTimeInputDoubleFailure()
    {
        DynamicDoubleOption option("", "", "", "");

        try {
            OptionParse::parse(&option, "");
            QFAIL("Parsing didn't catch an empty value.");
        } catch (OptionParsingException ope) { }

        try {
            OptionParse::parse(&option, "//");
            QFAIL("Parsing didn't catch an empty complex value.");
        } catch (OptionParsingException ope) { }

        try {
            OptionParse::parse(&option, "/v1,blarg,blarg");
            QFAIL("Parsing didn't catch a bad time range.");
        } catch (OptionParsingException ope) { }
    }


    void testTimeInputInteger()
    {
        QFETCH(QString, input);
        QFETCH(DoubleToIntegerMap, expected);

        DynamicIntegerOption option("", "", "", "");

        try {
            OptionParse::parse(&option, input);
        } catch (OptionParsingException ope) {
            QFAIL(qPrintable(ope.getDescription()));
        }

        DynamicInteger *check = option.getInput();

        for (DoubleToIntegerMap::const_iterator it = expected.constBegin();
                it != expected.constEnd();
                ++it) {
            QCOMPARE(check->getConst(it.key()), it.value());
        }

        delete check;
    }

    void testTimeInputInteger_data()
    {
        QTest::addColumn<QString>("input");
        QTest::addColumn<DoubleToIntegerMap>("expected");

        DoubleToIntegerMap expected;

        expected.clear();
        expected.insert(1262304000.0, 1);
        expected.insert(1262305000.0, 1);
        QTest::newRow("Always single") << QString("1") << expected;

        expected.clear();
        expected.insert(1230768000.0, 1);
        expected.insert(1293840000.0, 2);
        QTest::newRow("Transition") << QString("1,,1262304000/2,1262304000,") << expected;

    }

    void testTimeInputIntegerFailure()
    {
        DynamicIntegerOption option("", "", "", "");

        try {
            OptionParse::parse(&option, "");
            QFAIL("Parsing didn't catch an empty value.");
        } catch (OptionParsingException ope) { }

        try {
            OptionParse::parse(&option, "//");
            QFAIL("Parsing didn't catch an empty complex value.");
        } catch (OptionParsingException ope) { }

        try {
            OptionParse::parse(&option, "/v1,blarg,blarg");
            QFAIL("Parsing didn't catch a bad time range.");
        } catch (OptionParsingException ope) { }
    }
};

QTEST_APPLESS_MAIN(TestOptionParse)

#include "optionparse.moc"
