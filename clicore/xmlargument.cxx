/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QFile>
#include <QXmlStreamWriter>

#include "clicore/argumentparser.hxx"
#include "clicore/terminaloutput.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "clicore/terminaloutput.hxx"
#include "smoothing/baseline.hxx"
#include "algorithms/cryptography.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Smoothing;
using namespace CPD3::Algorithms;

namespace CPD3 {
namespace CLI {

static void outputTimeOffset(QXmlStreamWriter *xml,
                             Time::LogicalTimeUnit unit,
                             int count,
                             bool aligned)
{
    switch (unit) {
    case Time::Millisecond:
        xml->writeAttribute("unit", "millisecond");
        break;
    case Time::Second:
        xml->writeAttribute("unit", "second");
        break;
    case Time::Minute:
        xml->writeAttribute("unit", "minute");
        break;
    case Time::Hour:
        xml->writeAttribute("unit", "hour");
        break;
    case Time::Day:
        xml->writeAttribute("unit", "day");
        break;
    case Time::Week:
        xml->writeAttribute("unit", "week");
        break;
    case Time::Month:
        xml->writeAttribute("unit", "month");
        break;
    case Time::Quarter:
        xml->writeAttribute("unit", "quarter");
        break;
    case Time::Year:
        xml->writeAttribute("unit", "year");
        break;
    }
    xml->writeAttribute("aligned", aligned ? "true" : "false");
    xml->writeCharacters(QString::number(count));
}

static void outputStringList(QXmlStreamWriter *xml,
                             const QString &indent,
                             const QString &parent,
                             const QString &child,
                             const QStringList &list)
{
    if (list.isEmpty())
        return;

    xml->writeCharacters(indent);
    xml->writeStartElement(parent);
    xml->writeCharacters(QString('\n'));

    for (QStringList::const_iterator add = list.constBegin(), endAdd = list.constEnd();
            add != endAdd;
            ++add) {
        xml->writeCharacters(indent + "    ");
        xml->writeStartElement(child);
        xml->writeCharacters(*add);
        xml->writeEndElement();
        xml->writeCharacters(QString('\n'));
    }

    xml->writeCharacters(indent);
    xml->writeEndElement();
    xml->writeCharacters(QString('\n'));
}

static void outputUnit(QXmlStreamWriter *xml, const QString &indent, const SequenceName &unit,
                       bool flavors = false)
{
    if (!unit.getStation().empty()) {
        xml->writeCharacters(indent);
        xml->writeStartElement("station");
        xml->writeCharacters(unit.getStationQString());
        xml->writeEndElement();
        xml->writeCharacters(QString('\n'));
    }

    if (!unit.getArchive().empty()) {
        xml->writeCharacters(indent);
        xml->writeStartElement("archive");
        xml->writeCharacters(unit.getArchiveQString());
        xml->writeEndElement();
        xml->writeCharacters(QString('\n'));
    }

    if (!unit.getVariable().empty()) {
        xml->writeCharacters(indent);
        xml->writeStartElement("variable");
        xml->writeCharacters(unit.getVariableQString());
        xml->writeEndElement();
        xml->writeCharacters(QString('\n'));
    }

    if (flavors) {
        QStringList flavors = Util::to_qstringlist(unit.getFlavors());
        std::sort(flavors.begin(), flavors.end());
        outputStringList(xml, indent, "flavors", "flavor", flavors);
    }
}

static void outputOptionType(QXmlStreamWriter *xml,
                             const QString &indent,
                             const QString id,
                             ComponentOptionBase *input)
{
    {
        if (qobject_cast<ComponentOptionBoolean *>(input) != NULL) {
            xml->writeStartElement("boolean");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        ComponentOptionFile *option;
        if ((option = qobject_cast<ComponentOptionFile *>(input)) != NULL) {
            xml->writeStartElement("file");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            QStringList extensions(option->getExtensions().values());
            std::sort(extensions.begin(), extensions.end());
            outputStringList(xml, indent, "extensions", "extension", extensions);

            switch (option->getMode()) {
            case ComponentOptionFile::Read:
                xml->writeCharacters(indent);
                xml->writeStartElement("read");
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
                break;
            case ComponentOptionFile::Write:
                xml->writeCharacters(indent);
                xml->writeStartElement("write");
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
                break;
            case ComponentOptionFile::ReadWrite:
                xml->writeCharacters(indent);
                xml->writeStartElement("read");
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));

                xml->writeCharacters(indent);
                xml->writeStartElement("write");
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
                break;
            }
            return;
        }
    }

    {
        if (qobject_cast<ComponentOptionDirectory *>(input) != NULL) {
            xml->writeStartElement("directory");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));
            return;
        }
    }


    {
        if (qobject_cast<ComponentOptionScript *>(input) != NULL) {
            xml->writeStartElement("script");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        if (qobject_cast<ComponentOptionSequenceMatch *>(input) != NULL) {
            xml->writeStartElement("variable");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        if (qobject_cast<ComponentOptionSingleString *>(input) != NULL) {
            xml->writeStartElement("string");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        ComponentOptionSingleDouble *option;
        if ((option = qobject_cast<ComponentOptionSingleDouble *>(input)) != NULL) {
            xml->writeStartElement("double");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            xml->writeCharacters(indent);
            xml->writeStartElement("limits");
            if (option->getAllowUndefined()) {
                xml->writeAttribute("undefined", "true");
            }
            xml->writeCharacters(QString('\n'));


            double max = option->getMaximum();
            if (FP::defined(max)) {
                xml->writeCharacters(indent + "    ");
                xml->writeStartElement("maximum");
                if (!option->getMaximumInclusive())
                    xml->writeAttribute("exclusive", "true");
                xml->writeCharacters(QString::number(max));
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }

            double min = option->getMinimum();
            if (FP::defined(min)) {
                xml->writeCharacters(indent + "    ");
                xml->writeStartElement("minimum");
                if (!option->getMinimumInclusive())
                    xml->writeAttribute("exclusive", "true");
                xml->writeCharacters(QString::number(min));
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }

            if (FP::defined(min) || FP::defined(max))
                xml->writeCharacters(indent);
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        ComponentOptionDoubleSet *option;
        if ((option = qobject_cast<ComponentOptionDoubleSet *>(input)) != NULL) {
            xml->writeStartElement("doubleSet");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            xml->writeCharacters(indent);
            xml->writeStartElement("limits");
            xml->writeCharacters(QString('\n'));

            double max = option->getMaximum();
            if (FP::defined(max)) {
                xml->writeCharacters(indent + "    ");
                xml->writeStartElement("maximum");
                if (!option->getMaximumInclusive())
                    xml->writeAttribute("exclusive", "true");
                xml->writeCharacters(QString::number(max));
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }

            double min = option->getMinimum();
            if (FP::defined(min)) {
                xml->writeCharacters(indent + "    ");
                xml->writeStartElement("minimum");
                if (!option->getMinimumInclusive())
                    xml->writeAttribute("exclusive", "true");
                xml->writeCharacters(QString::number(min));
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }

            if (FP::defined(min) || FP::defined(max))
                xml->writeCharacters(indent);
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));

            return;
        }
    }

    {
        ComponentOptionDoubleList *option;
        if ((option = qobject_cast<ComponentOptionDoubleList *>(input)) != NULL) {
            xml->writeStartElement("doubleList");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            int minimumComponents = option->getMinimumComponents();
            if (minimumComponents > 0) {
                xml->writeCharacters(indent);
                xml->writeStartElement("components");
                xml->writeCharacters(QString::number(minimumComponents));
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }
            return;
        }
    }

    {
        ComponentOptionSingleInteger *option;
        if ((option = qobject_cast<ComponentOptionSingleInteger *>(input)) != NULL) {
            xml->writeStartElement("integer");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            xml->writeCharacters(indent);
            xml->writeStartElement("limits");
            if (option->getAllowUndefined()) {
                xml->writeAttribute("undefined", "true");
            }


            qint64 max = option->getMaximum();
            if (INTEGER::defined(max)) {
                xml->writeCharacters(indent + "    ");
                xml->writeStartElement("maximum");
                xml->writeCharacters(QString::number(max));
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }

            qint64 min = option->getMinimum();
            if (INTEGER::defined(min)) {
                xml->writeCharacters(indent + "    ");
                xml->writeStartElement("minimum");
                xml->writeCharacters(QString::number(min));
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }

            if (FP::defined(min) || FP::defined(max))
                xml->writeCharacters(indent);
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        if (qobject_cast<ComponentOptionInstrumentSuffixSet *>(input) != NULL) {
            xml->writeStartElement("instrument");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        if (qobject_cast<ComponentOptionStringSet *>(input) != NULL) {
            xml->writeStartElement("stringSet");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        ComponentOptionSingleTime *option;
        if ((option = qobject_cast<ComponentOptionSingleTime *>(input)) != NULL) {
            xml->writeStartElement("time");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            if (option->getAllowUndefined()) {
                xml->writeCharacters(indent);
                xml->writeStartElement("undefined");
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }
            return;
        }
    }

    {
        ComponentOptionSingleCalibration *option;
        if ((option = qobject_cast<ComponentOptionSingleCalibration *>(input)) != NULL) {
            xml->writeStartElement("calibration");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            if (option->getAllowConstant()) {
                xml->writeCharacters(indent);
                xml->writeStartElement("constant");
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }

            if (option->getAllowInvalid()) {
                xml->writeCharacters(indent);
                xml->writeStartElement("invalid");
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }
            return;
        }
    }

    {
        if (qobject_cast<DynamicInputOption *>(input) != NULL) {
            xml->writeStartElement("input");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        if (qobject_cast<DynamicSequenceSelectionOption *>(input) != NULL) {
            xml->writeStartElement("operate");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        ComponentOptionEnum *option;
        if ((option = qobject_cast<ComponentOptionEnum *>(input)) != NULL) {
            xml->writeStartElement("enum");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            xml->writeCharacters(indent);
            xml->writeStartElement("values");
            xml->writeCharacters(QString('\n'));

            QList<ComponentOptionEnum::EnumValue> values(option->getAll());
            for (QList<ComponentOptionEnum::EnumValue>::const_iterator v = values.constBegin(),
                    end = values.constEnd(); v != end; ++v) {

                xml->writeCharacters(indent + "    ");
                xml->writeStartElement("value");
                xml->writeAttribute("id", v->getInternalName());
                xml->writeCharacters(QString('\n'));

                xml->writeCharacters(indent + "        ");
                xml->writeStartElement("name");
                xml->writeCharacters(v->getName());
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));

                xml->writeCharacters(indent + "        ");
                xml->writeStartElement("description");
                xml->writeCharacters(v->getDescription());
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));

                xml->writeCharacters(indent + "    ");
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }

            xml->writeCharacters(indent);
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));

            QHash<QString, ComponentOptionEnum::EnumValue> aliases(option->getAliases());
            if (!aliases.isEmpty()) {
                xml->writeCharacters(indent);
                xml->writeStartElement("aliases");
                xml->writeCharacters(QString('\n'));

                for (QHash<QString, ComponentOptionEnum::EnumValue>::const_iterator
                        i = aliases.constBegin(); i != aliases.constEnd(); ++i) {
                    xml->writeCharacters(indent + "    ");
                    xml->writeStartElement("alias");
                    xml->writeCharacters(QString('\n'));

                    xml->writeCharacters(indent + "        ");
                    xml->writeStartElement("name");
                    xml->writeCharacters(i.key());
                    xml->writeEndElement();
                    xml->writeCharacters(QString('\n'));

                    xml->writeCharacters(indent + "        ");
                    xml->writeStartElement("origin");
                    xml->writeAttribute("id", i.value().getInternalName());
                    xml->writeCharacters(i.value().getName());
                    xml->writeEndElement();
                    xml->writeCharacters(QString('\n'));

                    xml->writeCharacters(indent + "        ");
                    xml->writeStartElement("description");
                    xml->writeCharacters(i.value().getDescription());
                    xml->writeEndElement();
                    xml->writeCharacters(QString('\n'));

                    xml->writeCharacters(indent + "    ");
                    xml->writeEndElement();
                    xml->writeCharacters(QString('\n'));
                }

                xml->writeCharacters(indent);
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }

            return;
        }
    }

    {
        TimeIntervalSelectionOption *option;
        if ((option = qobject_cast<TimeIntervalSelectionOption *>(input)) != NULL) {
            xml->writeStartElement("interval");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            if (option->getAllowZero()) {
                xml->writeCharacters(indent);
                xml->writeStartElement("zero");
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }

            if (option->getAllowNegative()) {
                xml->writeCharacters(indent);
                xml->writeStartElement("negative");
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }

            if (option->getAllowUndefined()) {
                xml->writeCharacters(indent);
                xml->writeStartElement("undefined");
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }

            if (option->getDefaultAligned()) {
                xml->writeCharacters(indent);
                xml->writeStartElement("fallback");
                xml->writeAttribute("aligned", "true");
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }
            return;
        }
    }

    {
        ComponentOptionTimeBlock *option;
        if ((option = qobject_cast<ComponentOptionTimeBlock *>(input)) != NULL) {
            xml->writeStartElement("intervalBlock");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            xml->writeCharacters(indent);
            xml->writeStartElement("fallback");
            outputTimeOffset(xml, option->getDefaultUnit(), option->getDefaultCount(),
                             option->getDefaultAlign());
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        ComponentOptionTimeOffset *option;
        if ((option = qobject_cast<ComponentOptionTimeOffset *>(input)) != NULL) {
            xml->writeStartElement("timeOffset");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            if (option->getAllowZero()) {
                xml->writeCharacters(indent);
                xml->writeStartElement("zero");
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }

            if (option->getAllowNegative()) {
                xml->writeCharacters(indent);
                xml->writeStartElement("negative");
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }

            xml->writeCharacters(indent);
            xml->writeStartElement("fallback");
            outputTimeOffset(xml, option->getDefaultUnit(), option->getDefaultCount(),
                             option->getDefaultAlign());
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        if (qobject_cast<DynamicStringOption *>(input) != NULL) {
            xml->writeStartElement("dynamicString");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        if (qobject_cast<DynamicBoolOption *>(input) != NULL) {
            xml->writeStartElement("dynamicBoolean");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        DynamicDoubleOption *option;
        if ((option = qobject_cast<DynamicDoubleOption *>(input)) != NULL) {
            xml->writeStartElement("dynamicDouble");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            xml->writeCharacters(indent);
            xml->writeStartElement("limits");
            if (option->getAllowUndefined()) {
                xml->writeAttribute("undefined", "true");
            }
            xml->writeCharacters(QString('\n'));


            double max = option->getMaximum();
            if (FP::defined(max)) {
                xml->writeCharacters(indent + "    ");
                xml->writeStartElement("maximum");
                if (!option->getMaximumInclusive())
                    xml->writeAttribute("exclusive", "true");
                xml->writeCharacters(QString::number(max));
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }

            double min = option->getMinimum();
            if (FP::defined(min)) {
                xml->writeCharacters(indent + "    ");
                xml->writeStartElement("minimum");
                if (!option->getMinimumInclusive())
                    xml->writeAttribute("exclusive", "true");
                xml->writeCharacters(QString::number(min));
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }

            if (FP::defined(min) || FP::defined(max))
                xml->writeCharacters(indent);
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        DynamicIntegerOption *option;
        if ((option = qobject_cast<DynamicIntegerOption *>(input)) != NULL) {
            xml->writeStartElement("dynamicInteger");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            xml->writeCharacters(indent);
            xml->writeStartElement("limits");
            if (option->getAllowUndefined()) {
                xml->writeAttribute("undefined", "true");
            }


            qint64 max = option->getMaximum();
            if (INTEGER::defined(max)) {
                xml->writeCharacters(indent + "    ");
                xml->writeStartElement("maximum");
                xml->writeCharacters(QString::number(max));
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }

            qint64 min = option->getMinimum();
            if (INTEGER::defined(min)) {
                xml->writeCharacters(indent + "    ");
                xml->writeStartElement("minimum");
                xml->writeCharacters(QString::number(min));
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }

            if (FP::defined(min) || FP::defined(max))
                xml->writeCharacters(indent);
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        if (qobject_cast<DynamicCalibrationOption *>(input) != NULL) {
            xml->writeStartElement("dynamicCalibration");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        BaselineSmootherOption *option;
        if ((option = qobject_cast<BaselineSmootherOption *>(input)) != NULL) {
            xml->writeStartElement("smoother");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            if (option->getStabilityDetection()) {
                xml->writeCharacters(indent);
                xml->writeStartElement("stability");
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }

            if (option->getSpikeDetection()) {
                xml->writeCharacters(indent);
                xml->writeStartElement("spike");
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }
            return;
        }
    }

    {
        if (qobject_cast<SSLAuthenticationOption *>(input) != NULL) {
            xml->writeStartElement("ssl");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    Q_ASSERT(false);
}

static void outputOptionGeneric(QXmlStreamWriter *xml,
                                const QString &indent,
                                ComponentOptionBase *input)
{
    xml->writeCharacters(indent);
    xml->writeStartElement("name");
    xml->writeCharacters(input->getArgumentName());
    xml->writeEndElement();
    xml->writeCharacters(QString('\n'));

    xml->writeCharacters(indent);
    xml->writeStartElement("display");
    xml->writeCharacters(input->getDisplayName());
    xml->writeEndElement();
    xml->writeCharacters(QString('\n'));

    xml->writeCharacters(indent);
    xml->writeStartElement("description");
    xml->writeCharacters(input->getDescription());
    xml->writeEndElement();
    xml->writeCharacters(QString('\n'));

    if (!input->getDefaultBehavior().isEmpty()) {
        xml->writeCharacters(indent);
        xml->writeStartElement("default");
        xml->writeCharacters(input->getDefaultBehavior());
        xml->writeEndElement();
        xml->writeCharacters(QString('\n'));
    }


}

static void outputExampleOption(QXmlStreamWriter *xml,
                                const QString &indent,
                                const QString &id,
                                const ComponentExample &example,
                                ComponentOptionBase *input)
{
    {
        ComponentOptionBoolean *option;
        if ((option = qobject_cast<ComponentOptionBoolean *>(input)) != NULL) {
            xml->writeStartElement("boolean");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            xml->writeCharacters(indent);
            xml->writeStartElement("value");
            xml->writeCharacters(option->get() ? "true" : "false");
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        ComponentOptionFile *option;
        if ((option = qobject_cast<ComponentOptionFile *>(input)) != NULL) {
            xml->writeStartElement("file");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            xml->writeCharacters(indent);
            xml->writeStartElement("value");
            xml->writeCharacters(option->get());
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        ComponentOptionDirectory *option;
        if ((option = qobject_cast<ComponentOptionDirectory *>(input)) != NULL) {
            xml->writeStartElement("directory");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            xml->writeCharacters(indent);
            xml->writeStartElement("value");
            xml->writeCharacters(option->get());
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        ComponentOptionScript *option;
        if ((option = qobject_cast<ComponentOptionScript *>(input)) != NULL) {
            xml->writeStartElement("script");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            xml->writeCharacters(indent);
            xml->writeStartElement("value");
            xml->writeCharacters(option->get());
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        ComponentOptionSequenceMatch *option;
        if ((option = qobject_cast<ComponentOptionSequenceMatch *>(input)) != NULL) {
            xml->writeStartElement("variable");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            xml->writeCharacters(indent);
            xml->writeStartElement("value");
            xml->writeCharacters(option->get());
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        ComponentOptionSingleString *option;
        if ((option = qobject_cast<ComponentOptionSingleString *>(input)) != NULL) {
            xml->writeStartElement("string");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            xml->writeCharacters(indent);
            xml->writeStartElement("value");
            xml->writeCharacters(option->get());
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        ComponentOptionSingleDouble *option;
        if ((option = qobject_cast<ComponentOptionSingleDouble *>(input)) != NULL) {
            xml->writeStartElement("double");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            double v = option->get();
            if (FP::defined(v)) {
                xml->writeCharacters(indent);
                xml->writeStartElement("value");
                xml->writeCharacters(QString::number(v));
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }
            return;
        }
    }

    {
        ComponentOptionDoubleSet *option;
        if ((option = qobject_cast<ComponentOptionDoubleSet *>(input)) != NULL) {
            xml->writeStartElement("doubleSet");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            xml->writeCharacters(indent);
            xml->writeStartElement("values");
            xml->writeCharacters(QString('\n'));

            QList<double> values(option->get().values());
            std::sort(values.begin(), values.end());
            for (QList<double>::const_iterator add = values.constBegin(), end = values.constEnd();
                    add != end;
                    ++add) {
                xml->writeCharacters(indent + "    ");
                xml->writeStartElement("value");
                xml->writeCharacters(QString::number(*add));
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }

            xml->writeCharacters(indent);
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        ComponentOptionDoubleList *option;
        if ((option = qobject_cast<ComponentOptionDoubleList *>(input)) != NULL) {
            xml->writeStartElement("doubleList");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            xml->writeCharacters(indent);
            xml->writeStartElement("values");
            xml->writeCharacters(QString('\n'));

            QList<double> values(option->get());
            std::sort(values.begin(), values.end());
            for (QList<double>::const_iterator add = values.constBegin(), end = values.constEnd();
                    add != end;
                    ++add) {
                xml->writeCharacters(indent + "    ");
                xml->writeStartElement("value");
                xml->writeCharacters(QString::number(*add));
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }

            xml->writeCharacters(indent);
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        ComponentOptionSingleInteger *option;
        if ((option = qobject_cast<ComponentOptionSingleInteger *>(input)) != NULL) {
            xml->writeStartElement("integer");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            qint64 v = option->get();
            if (INTEGER::defined(v)) {
                xml->writeCharacters(indent);
                xml->writeStartElement("value");
                xml->writeCharacters(QString::number(v));
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }
            return;
        }
    }

    {
        ComponentOptionInstrumentSuffixSet *option;
        if ((option = qobject_cast<ComponentOptionInstrumentSuffixSet *>(input)) != NULL) {
            xml->writeStartElement("instrument");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            QStringList values(option->get().values());
            std::sort(values.begin(), values.end());
            outputStringList(xml, indent, "values", "value", values);
            return;
        }
    }

    {
        ComponentOptionStringSet *option;
        if ((option = qobject_cast<ComponentOptionStringSet *>(input)) != NULL) {
            xml->writeStartElement("stringset");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            QStringList values(option->get().values());
            std::sort(values.begin(), values.end());
            outputStringList(xml, indent, "values", "value", values);
            return;
        }
    }

    {
        ComponentOptionSingleTime *option;
        if ((option = qobject_cast<ComponentOptionSingleTime *>(input)) != NULL) {
            xml->writeStartElement("time");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            double v = option->get();
            if (FP::defined(v)) {
                xml->writeCharacters(indent);
                xml->writeStartElement("value");
                xml->writeCharacters(Time::toISO8601(v));
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }
            return;
        }
    }

    {
        ComponentOptionSingleCalibration *option;
        if ((option = qobject_cast<ComponentOptionSingleCalibration *>(input)) != NULL) {
            xml->writeStartElement("calibration");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            xml->writeCharacters(indent);
            xml->writeStartElement("coefficients");
            xml->writeCharacters(QString('\n'));

            Calibration cal(option->get());
            for (int i = 0, max = cal.size(); i < max; i++) {
                xml->writeCharacters(indent + "    ");
                xml->writeStartElement("coefficient");
                xml->writeCharacters(QString::number(cal.get(i)));
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }

            xml->writeCharacters(indent);
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        DynamicInputOption *option;
        if ((option = qobject_cast<DynamicInputOption *>(input)) != NULL) {
            xml->writeStartElement("input");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            DynamicInput *v = option->getInput();
            double c = v->constant(FP::undefined(), FP::undefined());
            if (FP::defined(c)) {

                xml->writeCharacters(indent);
                xml->writeStartElement("value");
                xml->writeCharacters(QString::number(c));
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            } else {
                v->registerExpected(example.getInputStation().toStdString(),
                                    example.getInputArchive().toStdString());
                std::vector<SequenceName> sorted;
                for (const auto &add : v->getUsedInputs()) {
                    sorted.emplace_back(add);
                }
                if (!sorted.empty()) {
                    xml->writeCharacters(indent);
                    xml->writeStartElement("selected");
                    xml->writeCharacters(QString('\n'));

                    std::sort(sorted.begin(), sorted.end(), SequenceName::OrderLogical());
                    outputUnit(xml, indent + "    ", sorted.front());

                    xml->writeCharacters(indent);
                    xml->writeEndElement();
                    xml->writeCharacters(QString('\n'));
                }
            }
            delete v;
            return;
        }
    }

    {
        DynamicSequenceSelectionOption *option;
        if ((option = qobject_cast<DynamicSequenceSelectionOption *>(input)) != NULL) {
            xml->writeStartElement("operate");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            DynamicSequenceSelection *v = option->getOperator();
            v->registerExpected(example.getInputStation().toStdString(),
                                example.getInputArchive().toStdString());
            std::vector<SequenceName> sorted;
            for (const auto &add : v->getAllUnits()) {
                sorted.emplace_back(add);
            }
            delete v;
            if (!sorted.empty()) {
                xml->writeCharacters(indent);
                xml->writeStartElement("selected");
                xml->writeCharacters(QString('\n'));

                std::sort(sorted.begin(), sorted.end(), SequenceName::OrderLogical());
                outputUnit(xml, indent + "    ", sorted.front());

                xml->writeCharacters(indent);
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }
            return;
        }
    }

    {
        ComponentOptionEnum *option;
        if ((option = qobject_cast<ComponentOptionEnum *>(input)) != NULL) {
            xml->writeStartElement("enum");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            xml->writeCharacters(indent);
            xml->writeStartElement("value");
            xml->writeAttribute("id", option->get().getInternalName());
            xml->writeCharacters(option->get().getName());
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        TimeIntervalSelectionOption *option;
        if ((option = qobject_cast<TimeIntervalSelectionOption *>(input)) != NULL) {
            xml->writeStartElement("interval");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            DynamicTimeInterval *v = option->getInterval();
            Time::LogicalTimeUnit unit = Time::Second;
            int count = 0;
            bool aligned = false;
            if (v->constantOffsetDescription(&unit, &count, &aligned)) {
                xml->writeCharacters(indent);
                xml->writeStartElement("value");
                outputTimeOffset(xml, unit, count, aligned);
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }
            delete v;
            return;
        }
    }

    {
        ComponentOptionTimeBlock *option;
        if ((option = qobject_cast<ComponentOptionTimeBlock *>(input)) != NULL) {
            xml->writeStartElement("intervalbBock");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            xml->writeCharacters(indent);
            xml->writeStartElement("value");
            outputTimeOffset(xml, option->getUnit(), option->getCount(), option->getAlign());
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        ComponentOptionTimeOffset *option;
        if ((option = qobject_cast<ComponentOptionTimeOffset *>(input)) != NULL) {
            xml->writeStartElement("timeOffset");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            xml->writeCharacters(indent);
            xml->writeStartElement("value");
            outputTimeOffset(xml, option->getUnit(), option->getCount(), option->getAlign());
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        DynamicStringOption *option;
        if ((option = qobject_cast<DynamicStringOption *>(input)) != NULL) {
            xml->writeStartElement("dynamicString");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            xml->writeCharacters(indent);
            xml->writeStartElement("value");
            DynamicString *ti = option->getInput();
            xml->writeCharacters(ti->get(FP::undefined()));
            delete ti;
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        DynamicBoolOption *option;
        if ((option = qobject_cast<DynamicBoolOption *>(input)) != NULL) {
            xml->writeStartElement("dynamicBoolean");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            xml->writeCharacters(indent);
            xml->writeStartElement("value");
            DynamicBool *ti = option->getInput();
            xml->writeCharacters(ti->get(FP::undefined()) ? "true" : "false");
            delete ti;
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        DynamicDoubleOption *option;
        if ((option = qobject_cast<DynamicDoubleOption *>(input)) != NULL) {
            xml->writeStartElement("dynamicDouble");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            DynamicDouble *ti = option->getInput();
            double v = ti->get(FP::undefined());
            delete ti;
            if (FP::defined(v)) {
                xml->writeCharacters(indent);
                xml->writeStartElement("value");
                xml->writeCharacters(QString::number(v));
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }
            return;
        }
    }

    {
        DynamicIntegerOption *option;
        if ((option = qobject_cast<DynamicIntegerOption *>(input)) != NULL) {
            xml->writeStartElement("dynamicInteger");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            DynamicInteger *ti = option->getInput();
            qint64 v = ti->get(FP::undefined());
            delete ti;
            if (INTEGER::defined(v)) {
                xml->writeCharacters(indent);
                xml->writeStartElement("value");
                xml->writeCharacters(QString::number(v));
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }
            return;
        }
    }

    {
        DynamicCalibrationOption *option;
        if ((option = qobject_cast<DynamicCalibrationOption *>(input)) != NULL) {
            xml->writeStartElement("dynamicCalibration");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            xml->writeCharacters(indent);
            xml->writeStartElement("coefficients");
            xml->writeCharacters(QString('\n'));

            DynamicCalibration *ti = option->getInput();
            Calibration cal(ti->get(FP::undefined()));
            delete ti;
            for (int i = 0, max = cal.size(); i < max; i++) {
                xml->writeCharacters(indent + "    ");
                xml->writeStartElement("coefficient");
                xml->writeCharacters(QString::number(cal.get(i)));
                xml->writeEndElement();
                xml->writeCharacters(QString('\n'));
            }

            xml->writeCharacters(indent);
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    {
        BaselineSmootherOption *option;
        if ((option = qobject_cast<BaselineSmootherOption *>(input)) != NULL) {
            xml->writeStartElement("smoother");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            return;
        }
    }

    {
        SSLAuthenticationOption *option;
        if ((option = qobject_cast<SSLAuthenticationOption *>(input)) != NULL) {
            xml->writeStartElement("ssl");
            xml->writeAttribute("id", id);
            xml->writeAttribute("sort", QString::number(input->getSortPriority()));
            xml->writeCharacters(QString('\n'));

            auto config = option->getLocalConfiguration();
            xml->writeCharacters(indent);
            xml->writeStartElement("certificate");
            xml->writeCharacters(config["Certificate"].toQString());
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));

            xml->writeCharacters(indent);
            xml->writeStartElement("key");
            xml->writeCharacters(config["Key"].toQString());
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
            return;
        }
    }

    Q_ASSERT(false);
}

static void outputExample(QXmlStreamWriter *xml,
                          const QString &indent,
                          const ComponentExample &example)
{
    xml->writeCharacters(indent);
    xml->writeStartElement("title");
    xml->writeCharacters(example.getTitle());
    xml->writeEndElement();
    xml->writeCharacters(QString('\n'));

    if (!example.getDescription().isEmpty()) {
        xml->writeCharacters(indent);
        xml->writeStartElement("description");
        xml->writeCharacters(example.getDescription());
        xml->writeEndElement();
        xml->writeCharacters(QString('\n'));
    }

    xml->writeCharacters(indent);
    xml->writeStartElement("input");
    xml->writeCharacters(QString('\n'));
    {
        xml->writeCharacters(indent + "    ");
        xml->writeStartElement("station");
        xml->writeCharacters(example.getInputStation());
        xml->writeEndElement();
        xml->writeCharacters(QString('\n'));

        xml->writeCharacters(indent + "    ");
        xml->writeStartElement("archive");
        xml->writeCharacters(example.getInputArchive());
        xml->writeEndElement();
        xml->writeCharacters(QString('\n'));

        if (FP::defined(example.getInputRange().getStart())) {
            xml->writeCharacters(indent + "    ");
            xml->writeStartElement("start");
            xml->writeCharacters(Time::toISO8601(example.getInputRange().getStart()));
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
        }

        if (FP::defined(example.getInputRange().getEnd())) {
            xml->writeCharacters(indent + "    ");
            xml->writeStartElement("end");
            xml->writeCharacters(Time::toISO8601(example.getInputRange().getEnd()));
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
        }

        if (example.getInputTypes() & ComponentExample::Input_Scattering) {
            xml->writeCharacters(indent + "    ");
            xml->writeStartElement("scattering");
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
        }

        if (example.getInputTypes() & ComponentExample::Input_Absorption) {
            xml->writeCharacters(indent + "    ");
            xml->writeStartElement("absorption");
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
        }

        if (example.getInputTypes() & ComponentExample::Input_Counts) {
            xml->writeCharacters(indent + "    ");
            xml->writeStartElement("counts");
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
        }

        if (example.getInputTypes() & ComponentExample::Input_FileOnly) {
            xml->writeCharacters(indent + "    ");
            xml->writeStartElement("file");
            xml->writeEndElement();
            xml->writeCharacters(QString('\n'));
        }

        outputStringList(xml, indent + "    ", "auxiliary", "value", example.getInputAuxiliary());
    }
    xml->writeCharacters(indent);
    xml->writeEndElement();
    xml->writeCharacters(QString('\n'));

    bool anyOptions = false;
    QHash<QString, ComponentOptionBase *> allOptions = example.getOptions().getAll();
    for (QHash<QString, ComponentOptionBase *>::const_iterator i = allOptions.constBegin();
            i != allOptions.constEnd();
            ++i) {
        if (!i.value()->isSet())
            continue;

        if (!anyOptions) {
            anyOptions = true;

            xml->writeCharacters(indent);
            xml->writeStartElement("options");
            xml->writeCharacters(QString('\n'));
        }

        xml->writeCharacters(indent + "    ");
        outputExampleOption(xml, indent + "        ", i.key(), example, i.value());

        xml->writeCharacters(indent + "    ");
        xml->writeEndElement();
        xml->writeCharacters(QString('\n'));
    }
    if (anyOptions) {
        xml->writeCharacters(indent);
        xml->writeEndElement();
        xml->writeCharacters(QString('\n'));
    }
}


void ArgumentParser::outputXMLDescription(ComponentOptions &options,
                                          const QList<ComponentExample> &examples)
{
    QFile device;
    if (!device.open(1, QIODevice::WriteOnly | QIODevice::Unbuffered))
        return;
    QXmlStreamWriter xml(&device);

    xml.writeStartDocument();
    xml.writeCharacters(QString('\n'));
    xml.writeDTD("<!DOCTYPE cpd3arguments>");
    xml.writeCharacters(QString('\n'));
    xml.writeStartElement("cpd3arguments");
    xml.writeAttribute("version", "1.0");
    xml.writeCharacters(QString('\n'));

    xml.writeCharacters("    ");
    xml.writeStartElement("identifier");
    {
        QHash<QString, QString> id(getDocumentationTypeID());
        if (!id.isEmpty()) {
            xml.writeCharacters(QString('\n'));
            for (QHash<QString, QString>::const_iterator v = id.constBegin(), endV = id.constEnd();
                    v != endV;
                    ++v) {
                if (v.key().isEmpty())
                    continue;
                xml.writeCharacters("        ");
                xml.writeStartElement(v.key());
                if (!v.value().isEmpty())
                    xml.writeCharacters(v.value());
                xml.writeEndElement();
                xml.writeCharacters(QString('\n'));
            }
            xml.writeCharacters("    ");
        }
    }
    xml.writeEndElement();
    xml.writeCharacters(QString('\n'));


    xml.writeCharacters("    ");
    xml.writeStartElement("command");
    xml.writeCharacters(QString('\n'));
    {
        xml.writeCharacters("        ");
        xml.writeStartElement("program");
        xml.writeCharacters(getProgramName());
        xml.writeEndElement();
        xml.writeCharacters(QString('\n'));

        xml.writeCharacters("        ");
        xml.writeStartElement("description");
        xml.writeCharacters(getDescription());
        xml.writeEndElement();
        xml.writeCharacters(QString('\n'));

        xml.writeCharacters("        ");
        xml.writeStartElement("barewords");
        xml.writeCharacters(getBareWordCommandLine());
        xml.writeEndElement();
        xml.writeCharacters(QString('\n'));

        xml.writeCharacters("    ");
    }
    xml.writeEndElement();
    xml.writeCharacters(QString('\n'));


    xml.writeCharacters("    ");
    xml.writeStartElement("barewords");
    {
        QList<BareWordHelp> allBarewords(getBareWordHelp());
        if (!allBarewords.isEmpty()) {
            xml.writeCharacters(QString('\n'));

            for (QList<BareWordHelp>::const_iterator add = allBarewords.constBegin(),
                    endAdd = allBarewords.constEnd(); add != endAdd; ++add) {
                xml.writeCharacters("        ");
                xml.writeStartElement("bareword");
                xml.writeCharacters(QString('\n'));

                xml.writeCharacters("            ");
                xml.writeStartElement("name");
                xml.writeCharacters(add->name);
                xml.writeEndElement();
                xml.writeCharacters(QString('\n'));

                xml.writeCharacters("            ");
                xml.writeStartElement("description");
                xml.writeCharacters(add->description);
                xml.writeEndElement();
                xml.writeCharacters(QString('\n'));

                if (!add->defaultBehavior.isEmpty()) {
                    xml.writeCharacters("            ");
                    xml.writeStartElement("default");
                    xml.writeCharacters(add->defaultBehavior);
                    xml.writeEndElement();
                    xml.writeCharacters(QString('\n'));
                }

                xml.writeCharacters("            ");
                xml.writeStartElement("details");
                xml.writeCharacters(add->detailedHelp);
                xml.writeEndElement();
                xml.writeCharacters(QString('\n'));

                xml.writeCharacters("        ");
                xml.writeEndElement();
                xml.writeCharacters(QString('\n'));
            }

            xml.writeCharacters("    ");
        }
    }
    xml.writeEndElement();
    xml.writeCharacters(QString('\n'));


    xml.writeCharacters("    ");
    xml.writeStartElement("options");
    {
        QHash<QString, ComponentOptionBase *> allOptions(options.getAll());
        if (!allOptions.isEmpty()) {
            xml.writeCharacters(QString('\n'));

            for (QHash<QString, ComponentOptionBase *>::const_iterator o = allOptions.constBegin(),
                    endO = allOptions.constEnd(); o != endO; ++o) {
                xml.writeCharacters("        ");

                outputOptionType(&xml, "            ", o.key(), o.value());
                outputOptionGeneric(&xml, "            ", o.value());

                xml.writeCharacters("        ");
                xml.writeEndElement();
                xml.writeCharacters(QString('\n'));
            }

            xml.writeCharacters("    ");
        }
    }
    xml.writeEndElement();
    xml.writeCharacters(QString('\n'));

    xml.writeCharacters("    ");
    xml.writeStartElement("examples");
    if (!examples.isEmpty()) {
        xml.writeCharacters(QString('\n'));

        for (QList<ComponentExample>::const_iterator e = examples.constBegin(),
                endE = examples.constEnd(); e != endE; ++e) {
            xml.writeCharacters("        ");
            xml.writeStartElement("example");
            xml.writeCharacters(QString('\n'));

            outputExample(&xml, "            ", *e);

            xml.writeCharacters("        ");
            xml.writeEndElement();
            xml.writeCharacters(QString('\n'));
        }

        xml.writeCharacters("    ");
    }
    xml.writeEndElement();
    xml.writeCharacters(QString('\n'));

    xml.writeEndDocument();
}

}
}
