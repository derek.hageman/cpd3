/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QFile>
#include <QThread>

#include "clicore/interfaceparse.hxx"
#include "datacore/segment.hxx"
#include "datacore/externalsource.hxx"
#include "io/drivers/file.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace CPD3 {
namespace CLI {

/** @file clicore/archiveparse.hxx
 * Provides utilities to parse archive specifications from the command line.
 */

InterfaceParsingException::InterfaceParsingException(const QString &d)
{
    description = d;
}

QString InterfaceParsingException::getDescription() const
{
    return description;
}


namespace {
class ConfigSegmentReader : public StreamSink {
    ValueSegment::Stream reader;
public:
    ValueSegment::Transfer result;

    ConfigSegmentReader() = default;

    void incomingData(const SequenceValue::Transfer &values) override
    {
        Util::append(reader.add(values), result);
    }

    void endData() override
    {
        Util::append(reader.finish(), result);
    }
};
}

static void handleSSL(QStringList &arguments, Variant::Write &result)
{
    if (arguments.isEmpty())
        return;
    result.hash("SSL").hash("Certificate").setString(arguments.takeFirst());

    if (arguments.isEmpty())
        return;
    result.hash("SSL").hash("Key").setString(arguments.takeFirst());

    if (arguments.isEmpty())
        return;
    result.hash("SSL").hash("Password").setString(arguments.takeFirst());
}

Variant::Root InterfaceParse::parse(QStringList arguments, bool allowRemoteCPD3) noexcept(false)
{
    QString checkType(arguments.first().toLower());
    if (checkType == tr("config", "config name") ||
            checkType == tr("configuration", "config name")) {
        arguments.removeFirst();
        if (arguments.isEmpty()) {
            throw InterfaceParsingException(tr("A file is required"));
            return {};
        }

        auto fileName = arguments.takeFirst();
        auto file = IO::Access::file(fileName, IO::File::Mode::readOnly())->stream();
        if (!file) {
            throw InterfaceParsingException(tr("Unable to open configuration file"));
            return {};
        }
        StandardDataInput dataInput;
        ConfigSegmentReader reader;
        dataInput.start();
        dataInput.setEgress(&reader);

        IO::Generic::Stream::Iterator it(*file);
        while (auto contents = it.next()) {
            dataInput.incomingData(contents);
        }
        dataInput.endData();
        dataInput.wait();

        if (reader.result.empty()) {
            throw InterfaceParsingException(tr("No configuration found in file %1").arg(fileName));
            return {};
        }

        return reader.result.back().root();
    } else if (checkType == tr("remote", "remote server name")) {
        Variant::Root output;
        auto result = output.write();

        result.hash("Type").setString("remoteserver");

        arguments.removeFirst();
        if (arguments.isEmpty()) {
            throw InterfaceParsingException(tr("A remote server specification is required"));
            return Variant::Root();
        }
        result.hash("Server").setString(arguments.takeFirst());
        if (arguments.isEmpty())
            return output;

        bool ok = false;
        uint p = arguments.takeFirst().toUInt(&ok);
        if (ok && p > 0 && p < 65536) {
            result.hash("ServerPort").setInt64(p);
        } else {
            throw InterfaceParsingException(tr("Invalid port"));
            return Variant::Root();
        }

        handleSSL(arguments, result);
        return output;
    } else if (checkType == tr("tcplisten", "remote server name")) {
        Variant::Root output;
        auto result = output.write();
        result.hash("Type").setString("tcplisten");

        arguments.removeFirst();
        if (arguments.isEmpty()) {
            throw InterfaceParsingException(tr("A port is required"));
            return Variant::Root();
        }

        bool ok = false;
        uint p = arguments.takeFirst().toUInt(&ok);
        if (ok && p > 0 && p < 65536) {
            result.hash("LocalPort").setInt64(p);
        } else {
            throw InterfaceParsingException(tr("Invalid port"));
            return Variant::Root();
        }

        if (arguments.isEmpty())
            return output;
        result.hash("ListenAddress").setString(arguments.takeFirst());

        handleSSL(arguments, result);

        return output;
    } else if (checkType == tr("udp", "remote server name")) {
        Variant::Root output;
        auto result = output.write();
        result.hash("Type").setString("udp");

        arguments.removeFirst();
        if (arguments.isEmpty()) {
            throw InterfaceParsingException(tr("A port is or server is required"));
            return Variant::Root();
        }

        bool ok = false;
        uint p = arguments.first().toUInt(&ok);
        if (ok && p > 0 && p < 65536) {
            result.hash("LocalPort").setInt64(p);
        } else {
            QString server(arguments.takeFirst());

            if (arguments.isEmpty()) {
                throw InterfaceParsingException(tr("A destination port is required"));
                return Variant::Root();
            }

            ok = false;
            p = arguments.takeFirst().toUInt(&ok);
            if (ok && p > 0 && p < 65536) {
                result.hash("ServerPort").setInt64(p);
            } else {
                throw InterfaceParsingException(tr("Invalid port"));
                return Variant::Root();
            }

            result.hash("ServerAddress").setString(server);

            if (arguments.isEmpty())
                return output;

            ok = false;
            p = arguments.takeFirst().toUInt(&ok);
            if (ok && p > 0 && p < 65536) {
                result.hash("LocalPort").setInt64(p);
            } else {
                throw InterfaceParsingException(tr("Invalid local port"));
                return Variant::Root();
            }
        }

        if (arguments.isEmpty())
            return output;
        result.hash("ListenAddress").setString(arguments.takeFirst());

        return output;
    } else if (checkType == tr("qtlocal", "qt local name") ||
            checkType == tr("qtlocalsocket", "qt local name")) {
        Variant::Root output;
        auto result = output.write();
        result.hash("Type").setString("qtlocalsocket");

        arguments.removeFirst();
        if (arguments.isEmpty()) {
            throw InterfaceParsingException(tr("A socket specification is required"));
            return Variant::Root();
        }
        result.hash("Name").setString(arguments.takeFirst());
        return output;
    } else if (checkType == tr("qtlisten", "qt local name") ||
            checkType == tr("qtlocallisten", "qt local name")) {
        Variant::Root output;
        auto result = output.write();
        result.hash("Type").setString("qtlocallisten");

        arguments.removeFirst();
        if (arguments.isEmpty()) {
            throw InterfaceParsingException(tr("A socket specification is required"));
            return Variant::Root();
        }
        result.hash("Name").setString(arguments.takeFirst());
        return output;
    } else if (checkType == tr("command", "command name")) {
        Variant::Root output;
        auto result = output.write();
        result.hash("Type").setString("command");

        arguments.removeFirst();
        if (arguments.isEmpty()) {
            throw InterfaceParsingException(tr("A program name is required"));
            return Variant::Root();
        }

        result.hash("Command").setString(arguments.takeFirst());
        for (const auto &add : arguments) {
            result.hash("Arguments").toArray().after_back().setString(add);
        }
        return output;
    } else if (allowRemoteCPD3 &&
            (checkType == tr("cpd3server", "command name") ||
                    checkType == tr("cpd3", "command name"))) {
        Variant::Root output;
        auto result = output.write();
        result.hash("Type").setString("cpd3server");

        arguments.removeFirst();
        if (arguments.isEmpty()) {
            throw InterfaceParsingException(tr("A remote server specification is required"));
            return Variant::Root();
        }
        result.hash("Server").setString(arguments.takeFirst());
        if (arguments.isEmpty())
            return output;

        bool ok = false;
        uint p = arguments.takeFirst().toUInt(&ok);
        if (ok && p > 0 && p < 65536) {
            result.hash("ServerPort").setInt64(p);
        } else {
            throw InterfaceParsingException(tr("Invalid port"));
            return Variant::Root();
        }

        handleSSL(arguments, result);
        return output;
    }
#ifdef Q_OS_UNIX
    else if (checkType == tr("localsocket", "local socket name")) {
        Variant::Root output;
        auto result = output.write();
        result.hash("Type").setString("localsocket");

        arguments.removeFirst();
        if (arguments.isEmpty()) {
            throw InterfaceParsingException(tr("A socket specification is required"));
            return Variant::Root();
        }
        result.hash("Name").setString(arguments.takeFirst());
        return output;
    } else if (checkType == tr("locallisten", "local socket name")) {
        Variant::Root output;
        auto result = output.write();
        result.hash("Type").setString("locallisten");

        arguments.removeFirst();
        if (arguments.isEmpty()) {
            throw InterfaceParsingException(tr("A socket specification is required"));
            return Variant::Root();
        }
        result.hash("Name").setString(arguments.takeFirst());
        return output;
    } else if (checkType == tr("pipe", "pipe name")) {
        Variant::Root output;
        auto result = output.write();
        result.hash("Type").setString("pipe");

        arguments.removeFirst();
        if (arguments.isEmpty()) {
            throw InterfaceParsingException(tr("A pipe specification is required"));
            return Variant::Root();
        }

        QString check(arguments.takeFirst());
        if (!check.isEmpty())
            result.hash("Input").setString(check);
        if (!arguments.isEmpty())
            return output;
        check = arguments.takeFirst();
        if (!check.isEmpty())
            result.hash("Output").setString(check);
        if (arguments.isEmpty())
            return output;
        check = arguments.takeFirst();
        if (!check.isEmpty())
            result.hash("Echo").setString(check);
        return output;
    } else if (checkType == tr("cpd2", "cpd2 name")) {
        Variant::Root output;
        auto result = output.write();
        result.hash("Type").setString("cpd2serial");

        arguments.removeFirst();
        if (arguments.isEmpty()) {
            throw InterfaceParsingException(tr("A port specification is required"));
            return Variant::Root();
        }

        result.hash("Port").setString(arguments.takeFirst());
        if (arguments.isEmpty())
            return output;

        QString check(arguments.takeFirst());
        if (!check.isEmpty()) {
            bool ok = false;
            uint b = arguments.first().toUInt(&ok);
            if (ok && b > 0) {
                result.hash("Baud").setInt64(b);
                result.hash("EnableWrite").setBool(true);
            } else {
                throw InterfaceParsingException(tr("Invalid baud rate"));
                return Variant::Root();
            }
        }

        if (arguments.isEmpty())
            return output;
        check = arguments.takeFirst();
        if (check.startsWith(tr("E", "even parity identifier"), Qt::CaseInsensitive)) {
            result.hash("Parity").setString("even");
            result.hash("EnableWrite").setBool(true);
        } else if (check.startsWith(tr("O", "odd parity identifier"), Qt::CaseInsensitive)) {
            result.hash("Parity").setString("odd");
            result.hash("EnableWrite").setBool(true);
        } else if (!check.isEmpty()) {
            result.hash("Parity").setString("none");
        }

        if (arguments.isEmpty())
            return output;
        check = arguments.takeFirst();
        if (!check.isEmpty()) {
            bool ok = false;
            uint b = check.toUInt(&ok);
            if (ok && b >= 5 && b <= 8) {
                result.hash("DataBits").setInt64(b);
                result.hash("EnableWrite").setBool(true);
            } else {
                throw InterfaceParsingException(tr("Invalid number of data bits"));
                return Variant::Root();
            }
        }

        if (arguments.isEmpty())
            return output;
        check = arguments.takeFirst();
        if (!check.isEmpty()) {
            bool ok = false;
            uint b = check.toUInt(&ok);
            if (ok && b >= 1 && b <= 2) {
                result.hash("StopBits").setInt64(b);
                result.hash("EnableWrite").setBool(true);
            } else {
                throw InterfaceParsingException(tr("Invalid number of stop bits"));
                return Variant::Root();
            }
        }

        return output;
    }
#endif
    else {
        Variant::Root output;
        auto result = output.write();
        result.hash("Type").setString("serialport");
        if (checkType == "serial") {
            arguments.removeFirst();
        }

        if (arguments.isEmpty()) {
            throw InterfaceParsingException(tr("A port specification is required"));
            return Variant::Root();
        }

        result.hash("Port").setString(arguments.takeFirst());
        if (arguments.isEmpty())
            return output;

        QString check(arguments.takeFirst());
        if (!check.isEmpty()) {
            bool ok = false;
            uint b = check.toUInt(&ok);
            if (ok && b > 0) {
                result.hash("Baud").setInt64(b);
            } else {
                throw InterfaceParsingException(tr("Invalid baud rate"));
                return Variant::Root();
            }
        }

        if (arguments.isEmpty())
            return output;
        check = arguments.takeFirst();
        if (check.startsWith(tr("E", "even parity identifier"), Qt::CaseInsensitive)) {
            result.hash("Parity").setString("even");
        } else if (check.startsWith(tr("O", "odd parity identifier"), Qt::CaseInsensitive)) {
            result.hash("Parity").setString("odd");
        } else if (check.startsWith(tr("S", "space parity identifier"), Qt::CaseInsensitive)) {
            result.hash("Parity").setString("space");
        } else if (check.startsWith(tr("M", "mark parity identifier"), Qt::CaseInsensitive)) {
            result.hash("Parity").setString("mark");
        } else if (!check.isEmpty()) {
            result.hash("Parity").setString("none");
        }

        if (arguments.isEmpty())
            return output;
        check = arguments.takeFirst();
        if (!check.isEmpty()) {
            bool ok = false;
            uint b = check.toUInt(&ok);
            if (ok && b >= 5 && b <= 8) {
                result.hash("DataBits").setInt64(b);
            } else {
                throw InterfaceParsingException(tr("Invalid number of data bits"));
                return Variant::Root();
            }
        }

        if (arguments.isEmpty())
            return output;
        check = arguments.takeFirst();
        if (!check.isEmpty()) {
            bool ok = false;
            uint b = check.toUInt(&ok);
            if (ok && b >= 1 && b <= 3) {
                result.hash("StopBits").setInt64(b);
            } else {
                throw InterfaceParsingException(tr("Invalid number of stop bits"));
                return Variant::Root();
            }
        }

        return output;
    }
}

QList<ArgumentParser::BareWordHelp> InterfaceParse::getBareWordHelp(bool allowRemoteCPD3)
{
    QList<ArgumentParser::BareWordHelp> result;

    result << ArgumentParser::BareWordHelp(tr("device", "argument name"),
                                           tr("The serial port device to launch the server on"),
                                           QString(),
                                           tr("This can either be the full specification (such as "
                                              "\"/dev/ttyUSB0N81\") or just the device name with the "
                                              "parameters in subsequent arguments"));
    result << ArgumentParser::BareWordHelp(tr("baud", "argument name"),
                                           tr("The serial port baud rate"), QString(),
                                           tr("The numeric serial port baud rate to set."));
    result << ArgumentParser::BareWordHelp(tr("parity", "argument name"),
                                           tr("The serial parity bit setting"), QString(),
                                           tr("The parity setting for the serial port.  One of "
                                              "\"N\", \"E\", or \"O\" for one, even, or odd, "
                                              "respectively"));
    result << ArgumentParser::BareWordHelp(tr("data", "argument name"), tr("The serial data bits."),
                                           QString(), tr("The number of data bits in a frame."));
    result << ArgumentParser::BareWordHelp(tr("stop", "argument name"), tr("The serial stop bits."),
                                           QString(), tr("The number of stop bits in a frame."));

    result << ArgumentParser::BareWordHelp(tr("config", "argument name"),
                                           tr("Set the interface based on the contents of a file"),
                                           QString(),
                                           tr("When the first argument is the word \"config\" "
                                              "the interface specification is loaded from the "
                                              "specified configuration file."));
    result << ArgumentParser::BareWordHelp(tr("file", "argument name"),
                                           tr("The configuration file to load"), QString(),
                                           tr("When loading the configuration from a file, this is "
                                              "the file name that is used."));


    result << ArgumentParser::BareWordHelp(tr("remote", "argument name"),
                                           tr("Set the interface for connection to a remote server"),
                                           QString(),
                                           tr("When the first argument is the word \"remote\" "
                                              "the specification is treated as a connection "
                                              "to a transparent remote server."));
    result << ArgumentParser::BareWordHelp(tr("host", "argument name"),
                                           tr("The host name to connect to"), QString(),
                                           tr("This is the name of the remote host to establish a "
                                              "connection to."));
    result << ArgumentParser::BareWordHelp(tr("port", "argument name"),
                                           tr("The remote port to connect to"), QString(),
                                           tr("This is the port number on the remote host to "
                                              "establish a connection on."));
    result << ArgumentParser::BareWordHelp(tr("certificate", "argument name"),
                                           tr("The SSL certificate to use on the connection"),
                                           QString(),
                                           tr("This argument specifies the SSL certificate used in "
                                              "the connection.  This can either be the "
                                              "direct PEM encoded contents, or a file name "
                                              "to load the certificate from."));
    result << ArgumentParser::BareWordHelp(tr("key", "argument name"),
                                           tr("The SSL key to use on the connection"), QString(),
                                           tr("This argument specifies the SSL key used in "
                                              "the connection.  This can either be the "
                                              "direct PEM encoded contents, or a file name "
                                              "to load the key from."));
    result << ArgumentParser::BareWordHelp(tr("password", "argument name"),
                                           tr("The the password to unlock the SSL key"), QString(),
                                           tr("This argument specifies the password that decrypts "
                                              "the SSL key.  It should be omitted if the "
                                              "key it not protected."));

    result << ArgumentParser::BareWordHelp(tr("tcplisten", "argument name"),
                                           tr("Create a server waiting for connections"), QString(),
                                           tr("When the first argument is the word \"tcplisten\" "
                                              "the specification defines a TCP/IP server "
                                              "that will wait for incoming connections."));
    result << ArgumentParser::BareWordHelp(tr("listenaddress", "argument name"),
                                           tr("The local address to listen on"), QString(),
                                           tr("This is the local address that will be listened "
                                              "on.  This allows restricition of what "
                                              "network interface is used."));
    result << ArgumentParser::BareWordHelp(tr("listenport", "argument name"),
                                           tr("The local port to listen on"), QString(),
                                           tr("This is the port number that server will listen on."));
    result << ArgumentParser::BareWordHelp(tr("udp", "argument name"),
                                           tr("Create a server handling connectionless UDP datagrams."),
                                           QString(),
                                           tr("When the first argument is the word \"udp\" "
                                              "the specification defines a UDP transport.  "
                                              "This requires either or both of a port to "
                                              "listen for incoming data on or a server to "
                                              "send data to."));

    result << ArgumentParser::BareWordHelp(tr("qtlocal", "argument name"),
                                           tr("Connect to a Qt local server"), QString(),
                                           tr("When the first argument is the word \"qtlocal\" "
                                              "the interface will connect to a Qt style "
                                              "local server.  The specific naming of the "
                                              "server interface is platform dependant and "
                                              "will likely only be accessible through "
                                              "another Qt based program."));
    result << ArgumentParser::BareWordHelp(tr("qtlisten", "argument name"),
                                           tr("Listen for connections using a Qt local server"),
                                           QString(),
                                           tr("When the first argument is the word \"qtlisten\" "
                                              "the interface will create a Qt style "
                                              "local server.  The specific naming of the "
                                              "server interface is platform dependant and "
                                              "will likely only be accessible through "
                                              "another Qt based program."));
    result << ArgumentParser::BareWordHelp(tr("qtsocket", "argument name"),
                                           tr("The base name for the Qt socket"), QString(),
                                           tr("This is the name of the local connection passed to "
                                              "Qt's local socket naming.  This should be "
                                              "the same on both ends of the connection."));

    if (allowRemoteCPD3) {
        result << ArgumentParser::BareWordHelp(tr("cpd3", "argument name"),
                                               tr("Connect to a CPD3 interface server"), QString(),
                                               tr("When the first argument is the word \"cpd3\" "
                                                  "the interface will connect to a CPD3 "
                                                  "external server.  This server must "
                                                  "be exposed using the external server "
                                                  "options of its interface."));
    }

#ifdef Q_OS_UNIX
    result << ArgumentParser::BareWordHelp(tr("localsocket", "argument name"),
                                           tr("Connect to a Unix local socket"), QString(),
                                           tr("When the first argument is the word \"localsocket\" "
                                              "the interface will connect to a local Unix "
                                              "domain socket."));
    result << ArgumentParser::BareWordHelp(tr("locallisten", "argument name"),
                                           tr("Create to a Unix local server"), QString(),
                                           tr("When the first argument is the word \"locallisten\" "
                                              "the interface will create a local Unix "
                                              "domain socket server and wait for incoming "
                                              "connections on it."));
    result << ArgumentParser::BareWordHelp(tr("socket", "argument name"),
                                           tr("The Unix domain socket name"), QString(),
                                           tr("This is the name of the Unix domain local socket "
                                              "used by the connection."));

    result << ArgumentParser::BareWordHelp(tr("pipe", "argument name"),
                                           tr("Read and write to Unix named FIFOs"), QString(),
                                           tr("When the first argument is the word \"pipe\" "
                                              "the interface will read and write data "
                                              "to named FIFOs."));
    result << ArgumentParser::BareWordHelp(tr("input", "argument name"),
                                           tr("The FIFO to read data from"), QString(),
                                           tr("The filename of the FIFO that input data are "
                                              "read from.  This may be empty to disable "
                                              "input."));
    result << ArgumentParser::BareWordHelp(tr("output", "argument name"),
                                           tr("The FIFO to write data to"), QString(),
                                           tr("The filename of the FIFO that output data are "
                                              "written to.  This may be empty to disable "
                                              "output."));
    result << ArgumentParser::BareWordHelp(tr("echo", "argument name"),
                                           tr("The FIFO to read echo from"), QString(),
                                           tr("The filename of the FIFO that echo data are "
                                              "read from.  This may be empty to disable "
                                              "echo reading."));
#endif

    return result;
}

QString InterfaceParse::getBareWordCommandLine(bool allowRemoteCPD3)
{
    QString result(tr("<device [baud [parity [data [stop]]]]>|"
                      "<config file>|"
                      "<remote host [port [certificate [key [password]]]>|"
                      "<tcplisten listenport [listenaddress]>|"
                      "<udp <listenport>|<host port [listenport]>>|"
                      "<qtlocal qtsocket>|"
                      "<qtlisten qtsocket>", "command line"));
    if (allowRemoteCPD3) {
        result.append(tr("|<cpd3 host [port [certificate [key [password]]]>",
                         "cpd3 remote command additional"));
    }
#ifdef Q_OS_UNIX
    result.append(tr("|<localsocket socket>|"
                     "<locallisten socket>|"
                     "<pipe input [output [echo]]>|"
                     "<cpd2 device [baud [parity [data [stop]]]]>", "unix command additional"));
#endif
    return result;
}
}
}