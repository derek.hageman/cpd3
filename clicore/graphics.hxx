/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3CLICOREGRAPHICS_HXX
#define CPD3CLICOREGRAPHICS_HXX

#include "core/first.hxx"

#include <QCoreApplication>

#include "clicore/clicore.hxx"

namespace CPD3 {
namespace CLI {

/**
 * Tools for handling graphics from the command line.
 */
class CPD3CLICORE_EXPORT CLIGraphics {
public:

    /**
     * Create a graphics application using the best available solution that doesn't initialize
     * the main GUI system.
     *
     * @param argc  the arguments from the command line
     * @param argv  the arguments from the command line
     * @return      the application created
     */
    static QCoreApplication *createApplication(int &argc, char **argv);
};

}
}

#endif
