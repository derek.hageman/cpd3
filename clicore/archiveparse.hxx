/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3CLICOREARCHIVEPARSE_H
#define CPD3CLICOREARCHIVEPARSE_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QList>
#include <QString>
#include <QStringList>

#include "clicore/clicore.hxx"
#include "core/timeutils.hxx"
#include "datacore/archive/selection.hxx"
#include "datacore/archive/access.hxx"

namespace CPD3 {
namespace CLI {

/**
 * An exception representing an error parsing the command line archive
 * specification.
 */
class CPD3CLICORE_EXPORT AcrhiveParsingException {
private:
    QString description;
public:
    AcrhiveParsingException(const QString &d);

    /**
     * Get the localized description of what occurred.
     */
    QString getDescription() const;
};

/**
 * Provides utilities to parse archive specifications from the command line.
 */
class CPD3CLICORE_EXPORT ArchiveParse : public QObject {
Q_OBJECT
public:
    static Data::Archive::Selection::List parse(const QStringList &arguments,
                                                Time::Bounds *clip = nullptr,
                                                Data::Archive::Access *archive = nullptr,
                                                bool allowEmptySelection = false,
                                                const Data::SequenceName::Component &defaultStation = {},
                                                const Data::SequenceName::Component &defaultArchive = "raw")
    noexcept(false);
};

}
}

#endif
