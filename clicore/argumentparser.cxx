/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QFile>

#include "clicore/argumentparser.hxx"
#include "clicore/terminaloutput.hxx"

using namespace CPD3;

namespace CPD3 {
namespace CLI {

/** @file clicore/argumentparser.hxx
 * This provides a framework for parsing command line arguments.
 */

ArgumentParsingException::ArgumentParsingException(const QString &d, bool e) : text(d),
                                                                               error(e)
{ }

QString ArgumentParsingException::getText() const
{ return text; }

bool ArgumentParsingException::isError() const
{ return error; }

ArgumentParser::ArgumentParser(bool toTerminal) : directTerminal(toTerminal)
{ }

ArgumentParser::~ArgumentParser()
{ }

ArgumentParser::RawOverrideResult ArgumentParser::rawOverride(QString &argument) noexcept(false)
{
    Q_UNUSED(argument);
    return RawOverride_PassThrough;
}

QList<ArgumentParser::BareWordHelp> ArgumentParser::getBareWordHelp()
{ return QList<BareWordHelp>(); }

QString ArgumentParser::getBareWordCommandLine()
{ return QString(); }

QString ArgumentParser::getDescription()
{ return QString(); }

QHash<QString, QString> ArgumentParser::getDocumentationTypeID()
{ return QHash<QString, QString>(); }

QString ArgumentParser::getPostProcessingDescription(int nBareWords, int nArguments)
{
    if (nBareWords == 0 && nArguments == 0)
        return tr("No arguments are accepted.");
    if (nArguments == 0)
        return tr("All arguments are treated as bare words.  However, a simple \"--\" "
                          "will be ignored before the bare word processing.");
    if (nBareWords == 0)
        return tr("All arguments are switches.  Any remaining bare words will cause an error.  "
                          "A simple simple \"--\" will be ignored, but if it is not the last argument "
                          "it will cause an error.");
    return tr("Switches can be given at any point in the command line, however a simple "
                      "\"--\" terminates switch processing and all further arguments are treated "
                      "as bare words.");
}

QString ArgumentParser::generateExamples(const QList<ComponentExample> &examples,
                                         OptionParse::ExampleMode mode)
{
    if (examples.isEmpty())
        return QString();
    TerminalOutput::output(tr("\nExamples:\n", "", examples.size()));
    OptionParse::optionExampleOuput(getProgramName(), examples, mode);
    return QString();
}

QString ArgumentParser::getExamples(const QList<ComponentExample> &examples)
{
    if (directTerminal)
        return generateExamples(examples, OptionParse::Example_DataFilter);
    return QString();
}


static bool sortOptionsCompare(const ComponentOptionBase *a, const ComponentOptionBase *b)
{
    int p0 = a->getSortPriority();
    int p1 = b->getSortPriority();
    if (p0 < p1) return true;
    if (p0 > p1) return false;
    return a->getArgumentName() < b->getArgumentName();
}

static ComponentOptionBase *getOptionByArgument(const ComponentOptions &options,
                                                const QString &argument)
{
    QHash<QString, ComponentOptionBase *> h(options.getAll());
    for (QHash<QString, ComponentOptionBase *>::const_iterator i = h.constBegin();
            i != h.constEnd();
            ++i) {
        if (i.value()->getArgumentName() == argument)
            return i.value();
    }
    return NULL;
}

void ArgumentParser::parse(QStringList &arguments,
                           ComponentOptions &options,
                           const QList<ComponentExample> &examples)
noexcept(false)
{
    QString switchPrefix(tr("--", "switch prefix"));
    QHash<QString, QSet<QString> > excluded(options.excluded());

    QSet<QString> usedOptions;
    for (QStringList::iterator arg = arguments.begin(); arg != arguments.end();) {
        {
            RawOverrideResult result = rawOverride(*arg);
            if (result == RawOverride_Consumed) {
                arg = arguments.erase(arg);
                continue;
            }
            if (result == RawOverride_Terminate) {
                arguments.erase(arg);
                return;
            }
            if (result == RawOverride_Skip) {
                ++arg;
                continue;
            }
        }

        if (*arg == switchPrefix) {
            arguments.erase(arg);
            return;
        } else if (!arg->startsWith(switchPrefix)) {
            ++arg;
            continue;
        }
        QString rawName(*arg);
        QString name(rawName.mid(switchPrefix.length()));
        arg = arguments.erase(arg);

        if (name == tr("help", "help switch")) {
            QList<ComponentOptionBase *> sorted(options.getAll().values());
            std::sort(sorted.begin(), sorted.end(), sortOptionsCompare);

            int argumentWidth = 0;
            QList<BareWordHelp> bareWords(getBareWordHelp());
            for (QList<BareWordHelp>::const_iterator word = bareWords.constBegin(),
                    endWord = bareWords.constEnd(); word != endWord; ++word) {
                argumentWidth = qMax(argumentWidth, word->name.length());
            }

            for (QList<ComponentOptionBase *>::const_iterator option = sorted.constBegin(),
                    endOption = sorted.constEnd(); option != endOption; ++option) {
                argumentWidth = qMax(argumentWidth,
                                     (*option)->getArgumentName().length() + switchPrefix.length());
            }

            QString text;
            QString bareWordCLI(getBareWordCommandLine());
            if (!directTerminal) {
                if (sorted.isEmpty() && bareWordCLI.isEmpty()) {
                    text = tr("Usage: %1\n\n", "usage no arguments").arg(getProgramName());
                } else if (sorted.isEmpty()) {
                    text = tr("Usage: %1 %2\n\n", "usage only barewords").arg(getProgramName(),
                                                                              bareWordCLI);
                } else if (bareWordCLI.isEmpty()) {
                    text = tr("Usage: %1 [switches]\n\n", "usage only switches").arg(
                            getProgramName());
                } else {
                    text = tr("Usage: %1 [switches] %2\n\n", "usage with both arguments").arg(
                            getProgramName(), bareWordCLI);
                }
            } else {
                QString usageText;
                QString usageBase;
                if (sorted.isEmpty() && bareWordCLI.isEmpty()) {
                    usageText = tr("", "arguments empty");
                    usageBase = tr("Usage: %1", "usage base no arguments").arg(getProgramName());
                } else if (sorted.isEmpty()) {
                    usageText = tr("%1", "arguments only barewords").arg(bareWordCLI);
                    usageBase = tr("Usage: %1 ", "usage base").arg(getProgramName());
                } else if (bareWordCLI.isEmpty()) {
                    usageText = tr("[switches]", "arguments only switches");
                    usageBase = tr("Usage: %1 ", "usage base").arg(getProgramName());
                } else {
                    usageText = tr("[switches] %1", "arguments both").arg(bareWordCLI);
                    usageBase = tr("Usage: %1 ", "usage base").arg(getProgramName());
                }

                TerminalOutput::output(usageBase);
                TerminalOutput::applyWordWrap(usageText, false, -usageBase.length());
                TerminalOutput::output("\n");
            }
            QString description(getDescription());
            QString postDescription(getPostProcessingDescription(bareWords.size(), sorted.size()));
            if (!description.isEmpty() && !postDescription.isEmpty()) {
                text.append(description);
                text.append("\n\n");
                text.append(postDescription);
            } else if (!description.isEmpty()) {
                text.append(description);
            } else if (!postDescription.isEmpty()) {
                text.append(postDescription);
            }

            if (!directTerminal) {
                if (!sorted.isEmpty() || !bareWords.isEmpty()) {
                    text.append(tr("\n\nArguments:\n", "", sorted.size() + bareWords.size()));
                    for (QList<BareWordHelp>::const_iterator word = bareWords.constBegin(),
                            endWord = bareWords.constEnd(); word != endWord; ++word) {
                        if (word->defaultBehavior.isEmpty()) {
                            text.append(tr(" %1 - %2", "bareword basic output").arg(word->name,
                                                                                    word->description));
                        } else {
                            text.append(
                                    tr(" %1 - %2\n  Default: %3", "bareword default output").arg(
                                            word->name, word->description, word->defaultBehavior));
                        }
                    }
                    for (QList<ComponentOptionBase *>::const_iterator option = sorted.constBegin(),
                            endOption = sorted.constEnd(); option != endOption; ++option) {
                        QString def((*option)->getDefaultBehavior());
                        if (def.isEmpty()) {
                            text.append(tr(" %1%2 - %3", "argument basic output").arg(switchPrefix,
                                                                                      (*option)->getArgumentName(),
                                                                                      (*option)->getDisplayName()));
                        } else {
                            text.append(
                                    tr(" %1%2 - %3\n  Default: %4", "argument default output").arg(
                                            switchPrefix, (*option)->getArgumentName(),
                                            (*option)->getDisplayName(), def));
                        }
                    }
                }
                text.append(getExamples(examples));
                throw ArgumentParsingException(text, false);
            }

            TerminalOutput::simpleWordWrap(text);

            if (!sorted.isEmpty() || !bareWords.isEmpty()) {
                TerminalOutput::output(tr("\nArguments:\n", "", sorted.size() + bareWords.size()));

                for (QList<BareWordHelp>::const_iterator word = bareWords.constBegin(),
                        endWord = bareWords.constEnd(); word != endWord; ++word) {
                    OptionParse::optionSummaryOutput(argumentWidth, word->name, word->description,
                                                     word->defaultBehavior);
                }

                for (QList<ComponentOptionBase *>::const_iterator option = sorted.constBegin(),
                        endOption = sorted.constEnd(); option != endOption; ++option) {
                    OptionParse::optionSummaryOutput(argumentWidth,
                                                     switchPrefix + (*option)->getArgumentName(),
                                                     (*option)->getDisplayName(),
                                                     (*option)->getDefaultBehavior());
                }
            }

            text = getExamples(examples);
            if (!text.isEmpty()) {
                TerminalOutput::simpleWordWrap(text);
            }

            throw ArgumentParsingException(QString(), false);

        } else if (name.startsWith(tr("help=", "argument help switch"))) {
            name = name.mid(tr("help=", "argument help switch").length());
#ifdef BUILD_XML
            if (name == "__xml__") {
                outputXMLDescription(options, examples);
                throw ArgumentParsingException(QString(), false);
                return;
            }
#endif
            if (name.startsWith(switchPrefix)) {
                name = name.mid(switchPrefix.length());
            }
            if (name.length() == 0)
                throw ArgumentParsingException(
                        tr("Need to specify an switch to get help about.\nFor example: %1 --help=times\nUse --help with no '=' to list all arguments.")
                                .arg(getProgramName()));

            ComponentOptionBase *option = getOptionByArgument(options, name);
            if (option != NULL) {
                if (!directTerminal) {
                    throw ArgumentParsingException(option->getDescription(), false);
                }

                TerminalOutput::simpleWordWrap(option->getDescription());
                TerminalOutput::output(QString("\n\n"));
                OptionParse::optionHelpOutput(option);
                throw ArgumentParsingException(QString(), false);
            }

            QList<BareWordHelp> bareWords(getBareWordHelp());
            for (QList<BareWordHelp>::const_iterator word = bareWords.constBegin(),
                    endWord = bareWords.constEnd(); word != endWord; ++word) {
                if (name.toLower() == word->name.toLower())
                    throw ArgumentParsingException(word->detailedHelp, false);
            }

            throw ArgumentParsingException(
                    tr("%1 is not a valid switch to %2.\nUse --help with no '=' to list all arguments.")
                            .arg(name, getProgramName()), true);
        }

        QString value;
        int idxEqual = name.indexOf(tr("=", "argument value separator"));
        if (idxEqual != -1) {
            value = name.mid(idxEqual + 1);
            name = name.mid(0, idxEqual);
        }

        ComponentOptionBase *option = NULL;
        QString optionID;

        QHash<QString, ComponentOptionBase *> h(options.getAll());
        for (QHash<QString, ComponentOptionBase *>::const_iterator i = h.constBegin(),
                endI = h.constEnd(); i != endI; ++i) {
            if (i.value()->getArgumentName() == name) {
                option = i.value();
                optionID = i.key();
                break;
            }
        }
        if (option == NULL) {
            throw ArgumentParsingException(
                    tr("%1 is not a valid switch to %2.\nUse --help with no '=' to list all arguments.")
                            .arg(rawName, getProgramName()), true);
        }

        QHash<QString, QSet<QString> >::const_iterator check = excluded.constFind(optionID);
        if (check != excluded.constEnd()) {
            QSet<QString> excluding(check.value());
            excluding.intersect(usedOptions);
            if (!excluding.isEmpty()) {
                QStringList sorted;
                for (QSet<QString>::const_iterator id = excluding.constBegin(),
                        end = excluding.constEnd(); id != end; ++id) {
                    sorted << h[*id]->getArgumentName();
                }
                std::sort(sorted.begin(), sorted.end());
                int n = sorted.size();
                throw ArgumentParsingException(
                        tr("Can't use argument %1 with argument(s) %2\nUse --help to list all possible arguments.\nFor example: %3 --help\n",
                           "", n).arg(rawName, sorted.join(tr(", ", "argument join")),
                                      getProgramName()), true);
            }
        }
        usedOptions |= optionID;

        try {
            OptionParse::parse(option, value, rawName);
        } catch (OptionParsingException ope) {
            QString text(ope.getDescription());
            text.append('\n');
            text.append(tr("Use --help=%1 for more information.").arg(name));
            throw ArgumentParsingException(text, true);
        }
    }
}

}
}
