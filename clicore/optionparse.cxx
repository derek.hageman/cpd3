/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "clicore/optionparse.hxx"
#include "core/timeparse.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "datacore/sequencematch.hxx"
#include "clicore/terminaloutput.hxx"
#include "smoothing/baseline.hxx"
#include "algorithms/cryptography.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Smoothing;
using namespace CPD3::Algorithms;

namespace CPD3 {
namespace CLI {

/** @file clicore/optionparse.hxx
 * Provides utilities to parse command line option specifications.
 */


OptionParsingException::OptionParsingException(const QString &d)
{
    description = d;
}

QString OptionParsingException::getDescription() const
{
    return description;
}

/**
 * Output the help for an option in the summary format to standard output.
 * 
 * @param maximumOptionWidth the width of the longest option available
 * @param optionName    the name of the option
 * @param optionHelp    the help description of the option
 * @param defaultBehavior the default behavior string (if any)
 */
void OptionParse::optionSummaryOutput(int maximumOptionWidth,
                                      const QString &optionName,
                                      const QString &optionHelp,
                                      const QString &defaultBehavior)
{
    QString optionLeader;
    if (maximumOptionWidth > optionName.length())
        optionLeader = QString(' ').repeated(maximumOptionWidth - optionName.length());
    optionLeader.append(optionName);
    optionLeader.append(' ');

    int terminalWidth = TerminalOutput::terminalWidth() - 1;
    if (terminalWidth <= 0)
        terminalWidth = 79;

    int columnWidth = terminalWidth - optionLeader.length();
    if (columnWidth <= 0) columnWidth = 40;
    TerminalOutput::output(optionLeader);
    TerminalOutput::applyWordWrap(optionHelp, false, optionLeader.length(), columnWidth);
    if (defaultBehavior.length() > 0) {
        if (optionLeader.length() > 0)
            TerminalOutput::output(QString(' ').repeated(optionLeader.length()));
        TerminalOutput::applyWordWrap(
                tr("  Default: ", "default option desc prefix") + defaultBehavior, false,
                optionLeader.length() + tr("  Default: ", "default option desc prefix").length(),
                columnWidth);
    }
}


static bool caseInsensitiveLessThan(const QString &s1, const QString &s2)
{
    return s1.toLower() < s2.toLower();
}

/**
 * Output the detailed help description for an option to standard output.
 * 
 * @param help    the option to output
 */
void OptionParse::optionHelpOutput(ComponentOptionBase *help)
{
    {
        if (qobject_cast<ComponentOptionBoolean *>(help) != NULL) {
            TerminalOutput::simpleWordWrap(
                    tr("This option is a boolean toggle, if present with no value or set to "
                       "\"true\", \"on\", \"yes\" or a non-zero value then it is enabled.  For "
                       "example \"--%1=TRUE\" or just \"--%1\" enable it, while \"--%1=off\" "
                       "disables it.").arg(help->getArgumentName()));
            return;
        }
    }

    {
        ComponentOptionFile *option;
        if ((option = qobject_cast<ComponentOptionFile *>(help)) != NULL) {
            QList<QString> extensions(option->getExtensions().toList());
            std::sort(extensions.begin(), extensions.end());
            QString preferredExtension(tr("dat", "default extension"));
            if (!extensions.isEmpty())
                preferredExtension = extensions.first();

            switch (option->getMode()) {
            case ComponentOptionFile::Read:
                TerminalOutput::simpleWordWrap(
                        tr("This option takes a file name to read from.  For example \"--%1=input.%2\"")
                                .arg(help->getArgumentName(), preferredExtension));
                break;
            case ComponentOptionFile::Write:
                TerminalOutput::simpleWordWrap(
                        tr("This option takes a file name to write to.  For example \"--%1=input.%2\"")
                                .arg(help->getArgumentName(), preferredExtension));
                break;
            case ComponentOptionFile::ReadWrite:
                TerminalOutput::simpleWordWrap(
                        tr("This option takes a file name to read from and write to.  "
                           "For example \"--%1=input.%2\"").arg(help->getArgumentName(),
                                                                preferredExtension));
                break;
            }
            return;
        }
    }

    {
        if (qobject_cast<ComponentOptionDirectory *>(help) != NULL) {
            TerminalOutput::simpleWordWrap(
                    tr("This option takes a directory as a value.  For example \"--%1=/tmp\"").arg(
                            help->getArgumentName()));
            return;
        }
    }

    {
        if (qobject_cast<ComponentOptionScript *>(help) != NULL) {
            TerminalOutput::simpleWordWrap(
                    tr("This option takes CPD3 ECMA 262 script as a value.  For example \"--%1=print('Test')\"")
                            .arg(help->getArgumentName()));
            return;
        }
    }

    {
        if (qobject_cast<ComponentOptionSequenceMatch *>(help) != NULL) {
            TerminalOutput::simpleWordWrap(
                    tr("This option specified selection of input variables.  In the simplest form, "
                       "this can be just a variable name.  In more advanced forms it can consist of "
                       "multiple specifications separated by \";\" or \",\".  Each "
                       "variable may have a station and archive prefix, separated by \":\" and a "
                       "flavors (e.x. PM1 or PM10) restricting suffix if both the station and archive "
                       "are given.  The flavor restricting suffix is also separated into components "
                       "with \":\" and consists of terms starting with \"=\" to specify that the "
                       "variable must have exactly all the \"=\" terms, \"!\" or \"-\" to exclude "
                       "specific flavors or no prefix to require it to have the given flavor.  "
                       "For example \"::BsG_S11:pm1:!cover\" specifies BsG_S11 sub-1um data that is "
                       "not coverage data and \"::BsG_S11:=pm1\" specifies only \"real\" values.  "
                       "For example \"--%1=sgp::BsG_S11,BsR_S11,Bs[BGR]_S11\"").arg(
                            help->getArgumentName()));
            return;
        }
    }

    {
        if (qobject_cast<ComponentOptionSingleString *>(help) != NULL) {
            TerminalOutput::simpleWordWrap(
                    tr("This option takes a single string as a value.  For example \"--%1=VALUE\"").arg(
                            help->getArgumentName()));
            return;
        }
    }

    {
        ComponentOptionSingleDouble *option;
        if ((option = qobject_cast<ComponentOptionSingleDouble *>(help)) != NULL) {
            double max = option->getMaximum();
            double min = option->getMinimum();
            bool maxI = option->getMaximumInclusive();
            bool minI = option->getMinimumInclusive();
            QString ais;
            if (option->getAllowUndefined()) {
                ais = tr("  The value may also be empty, \"undef\", \"undefined\", or \"MVC\" to "
                         "indicate that it is set, but undefined.");
            }

            if (FP::defined(max) && FP::defined(min)) {
                if (maxI && minI) {
                    TerminalOutput::simpleWordWrap(
                            tr("This option takes a single number as a value.  The value must be greater "
                               "than or equal to %3 and less than or equal to %4.%5  For example \"--%1=%2\"")
                                    .arg(help->getArgumentName())
                                    .arg(min)
                                    .arg(min)
                                    .arg(max)
                                    .arg(ais));
                } else if (maxI && !minI) {
                    TerminalOutput::simpleWordWrap(
                            tr("This option takes a single number as a value.  The value must be greater "
                               "than or equal to %3 and less than %4.%5  For example \"--%1=%2\"").arg(help->getArgumentName())
                                                                                                  .arg(max)
                                                                                                  .arg(min)
                                                                                                  .arg(max)
                                                                                                  .arg(ais));
                } else if (minI) {
                    TerminalOutput::simpleWordWrap(
                            tr("This option takes a single number as a value.  The value must be greater "
                               "than %3 and less than or equal to %4.%5  For example \"--%1=%2\"").arg(help->getArgumentName())
                                                                                                  .arg(min)
                                                                                                  .arg(min)
                                                                                                  .arg(max)
                                                                                                  .arg(ais));
                } else {
                    TerminalOutput::simpleWordWrap(
                            tr("This option takes a single number as a value.  The value must be greater "
                               "than %3 and less than %4.%5  For example \"--%1=%2\"").arg(help->getArgumentName())
                                                                                      .arg((min +
                                                                                              max) *
                                                                                                   0.5)
                                                                                      .arg(min)
                                                                                      .arg(max)
                                                                                      .arg(ais));
                }
            } else if (FP::defined(max)) {
                if (maxI) {
                    TerminalOutput::simpleWordWrap(
                            tr("This option takes a single number as a value.  The value must be less "
                               "than or equal to %3.%4  For example \"--%1=%2\"").arg(help->getArgumentName())
                                                                                 .arg(max)
                                                                                 .arg(max)
                                                                                 .arg(ais));
                } else {
                    TerminalOutput::simpleWordWrap(
                            tr("This option takes a single number as a value.  The value must be less "
                               "than %3.%4  For example \"--%1=%2\"").arg(help->getArgumentName())
                                                                     .arg(max - 1.0)
                                                                     .arg(max)
                                                                     .arg(ais));
                }
            } else if (FP::defined(min)) {
                if (maxI) {
                    TerminalOutput::simpleWordWrap(
                            tr("This option takes a single number as a value.  The value must be greater "
                               "than or equal to %3.%4  For example \"--%1=%2\"").arg(help->getArgumentName())
                                                                                 .arg(min)
                                                                                 .arg(min)
                                                                                 .arg(ais));
                } else {
                    TerminalOutput::simpleWordWrap(
                            tr("This option takes a single number as a value.  The value must be greater "
                               "than %3.  For example \"--%1=%2\"").arg(help->getArgumentName())
                                                                   .arg(min + 1.0)
                                                                   .arg(min)
                                                                   .arg(ais));
                }
            } else {
                TerminalOutput::simpleWordWrap(
                        tr("This option takes a single number as a value.%5  For example \"--%1=0.0\"")
                                .arg(help->getArgumentName())
                                .arg(ais));
            }
            return;
        }
    }

    {
        ComponentOptionDoubleSet *option;
        if ((option = qobject_cast<ComponentOptionDoubleSet *>(help)) != NULL) {
            double max = option->getMaximum();
            double min = option->getMinimum();
            bool maxI = option->getMaximumInclusive();
            bool minI = option->getMinimumInclusive();

            if (FP::defined(max) && FP::defined(min)) {
                if (maxI && minI) {
                    TerminalOutput::simpleWordWrap(
                            tr("This option takes a set of numbers separated by \",\", \";\", or \":\" as a "
                               "value.  All values must be greater than or equal to %3 and less than or "
                               "equal to %4.  For example \"--%1=%2\"").arg(help->getArgumentName())
                                                                       .arg(min)
                                                                       .arg(min)
                                                                       .arg(max));
                } else if (maxI && !minI) {
                    TerminalOutput::simpleWordWrap(
                            tr("This option takes a set of numbers separated by \",\", \";\", or \":\" as a "
                               "value.  All values must be greater than or equal to %3 and less than %4.  "
                               "For example \"--%1=%2\"").arg(help->getArgumentName())
                                                         .arg(max)
                                                         .arg(min)
                                                         .arg(max));
                } else if (minI) {
                    TerminalOutput::simpleWordWrap(
                            tr("This option takes a set of numbers separated by \",\", \";\", or \":\" as a "
                               "value.  All values must be greater than %3 and less than or equal to %4.  "
                               "For example \"--%1=%2\"").arg(help->getArgumentName())
                                                         .arg(min)
                                                         .arg(min)
                                                         .arg(max));
                } else {
                    TerminalOutput::simpleWordWrap(
                            tr("This option takes a set of numbers separated by \",\", \";\", or \":\" as a "
                               "value.  All values must be greater than %3 and less than %4.  "
                               "For example \"--%1=%2\"").arg(help->getArgumentName())
                                                         .arg((min + max) * 0.5)
                                                         .arg(min)
                                                         .arg(max));
                }
            } else if (FP::defined(max)) {
                if (maxI) {
                    TerminalOutput::simpleWordWrap(
                            tr("This option takes a set of numbers separated by \",\", \";\", or \":\" as a "
                               "value.  All values must be less than or equal to %3.%4  For example "
                               "\"--%1=%2\"").arg(help->getArgumentName()).arg(min).arg(min));
                } else {
                    TerminalOutput::simpleWordWrap(
                            tr("This option takes a set of numbers separated by \",\", \";\", or \":\" as a "
                               "value.  All values must be less than %3.%4  For example \"--%1=%2\"")
                                    .arg(help->getArgumentName())
                                    .arg(min + 1.0)
                                    .arg(min));
                }
            } else if (FP::defined(min)) {
                if (maxI) {
                    TerminalOutput::simpleWordWrap(
                            tr("This option takes a set of numbers separated by \",\", \";\", or \":\" as a "
                               "value.  All values must be greater than or equal to %3.  For example "
                               "\"--%1=%2\"").arg(help->getArgumentName()).arg(min).arg(min));
                } else {
                    TerminalOutput::simpleWordWrap(
                            tr("This option takes a set of numbers separated by \",\", \";\", or \":\" as a "
                               "value.  All values must be greater than %3.  For example \"--%1=%2\"")
                                    .arg(help->getArgumentName())
                                    .arg(min + 1.0)
                                    .arg(min));
                }
            } else {
                TerminalOutput::simpleWordWrap(
                        tr("This option takes a set of numbers separated by \",\", \";\", or \":\" as a "
                           "value.  For example \"--%1=0.0\"").arg(help->getArgumentName()));
            }
            return;
        }
    }

    {
        ComponentOptionDoubleList *option;
        if ((option = qobject_cast<ComponentOptionDoubleList *>(help)) != NULL) {
            int minimumComponents = option->getMinimumComponents();
            if (minimumComponents > 0) {
                QStringList parts;
                for (int i = 1; i <= minimumComponents; i++) {
                    parts.append(QString::number((double) i, 'f', 1));
                }
                TerminalOutput::simpleWordWrap(
                        tr("This option takes a list of at least %n number(s) separated by \",\", \";\", "
                           "or \":\" as a value.  For example \"--%1=%2\".",
                           "multiple number list description",
                           option->getMinimumComponents()).arg(help->getArgumentName())
                                                          .arg(parts.join(",")));
            } else {
                TerminalOutput::simpleWordWrap(
                        tr("This option takes a list of numbers separated by \",\", \";\", or \":\" as a "
                           "value.  For example \"--%1=0.0\".  It may also be empty.").arg(
                                help->getArgumentName()));
            }
            return;
        }
    }

    {
        ComponentOptionSingleInteger *option;
        if ((option = qobject_cast<ComponentOptionSingleInteger *>(help)) != NULL) {
            qint64 max = option->getMaximum();
            qint64 min = option->getMinimum();
            QString ais;
            if (option->getAllowUndefined()) {
                ais = tr("  The value may also be empty, \"undef\", \"undefined\", or \"MVC\" to "
                         "indicate that it is set, but undefined.");
            }
            if (INTEGER::defined(max) && INTEGER::defined(min)) {
                TerminalOutput::simpleWordWrap(
                        tr("This option takes a single integer as a value.  The value must be greater "
                           "than or equal to %3 and less than or equal to %4.%5  For example \"--%1=%2\"")
                                .arg(help->getArgumentName())
                                .arg(min)
                                .arg(min)
                                .arg(max)
                                .arg(ais));
            } else if (INTEGER::defined(max)) {
                TerminalOutput::simpleWordWrap(
                        tr("This option takes a single integer as a value.  The value must be less "
                           "than or equal to %3.%4  For example \"--%1=%2\"").arg(help->getArgumentName())
                                                                             .arg(max)
                                                                             .arg(max)
                                                                             .arg(ais));
            } else if (INTEGER::defined(min)) {
                TerminalOutput::simpleWordWrap(
                        tr("This option takes a single integer as a value.  The value must be greater "
                           "than or equal to %3.%4  For example \"--%1=%2\"").arg(help->getArgumentName())
                                                                             .arg(min)
                                                                             .arg(min)
                                                                             .arg(ais));
            } else {
                TerminalOutput::simpleWordWrap(
                        tr("This option takes a single integer as a value.%2  For example \"--%1=0\"")
                                .arg(help->getArgumentName(), ais));
            }
            return;
        }
    }

    {
        if (qobject_cast<ComponentOptionInstrumentSuffixSet *>(help) != NULL) {
            TerminalOutput::simpleWordWrap(
                    tr("This option takes a set of instrument suffixes, separated by \":\", \";\", or "
                       "\",\".  For example \"--%1=S11,A11\"").arg(help->getArgumentName()));
            return;
        }
    }

    {
        if (qobject_cast<ComponentOptionStringSet *>(help) != NULL) {
            TerminalOutput::simpleWordWrap(
                    tr("This option takes a set of strings as a value, separated by \":\", \";\", or "
                       "\",\".  To specify an empty string, use two separators in a row.  "
                       "For example \"--%1=A,B,C\"").arg(help->getArgumentName()));
            return;
        }
    }

    {
        ComponentOptionSingleTime *option;
        if ((option = qobject_cast<ComponentOptionSingleTime *>(help)) != NULL) {
            TerminalOutput::simpleWordWrap(
                    tr("This option takes a single time specified completely as argument to the "
                       "option.\n"));
            TerminalOutput::simpleWordWrap(
                    TimeParse::describeSingleTime(option->getAllowUndefined()));
            return;
        }
    }

    {
        ComponentOptionSingleCalibration *option;
        if ((option = qobject_cast<ComponentOptionSingleCalibration *>(help)) != NULL) {
            TerminalOutput::simpleWordWrap(
                    tr("This option takes a calibration specified as a set of polynomial coefficients, "
                           "separated by \":\", \";\", or \",\".  For example \"--%1=0.5,2.0,-.1\".  "
                           "Coefficients are listed in ascending power order, starting from the constant "
                           "offset.  The preceding example specifies a calibration polynomial of "
                           "\"-0.1x\xC2\xB2 + 2.0x + 0.5\".").arg(help->getArgumentName()));
            if (!option->getAllowConstant()) {
                TerminalOutput::simpleWordWrap(
                        tr("\nThe calibration may consist of only a constant offset."));
            }
            if (option->getAllowInvalid()) {
                TerminalOutput::simpleWordWrap(
                        tr("\nAlternatively the value may be empty or set to \"undef\", \"undefined\", "
                           "or \"invalid\" to explicitly specify a calibration that is always invalid."));
            }
            return;
        }
    }

    {
        if (qobject_cast<DynamicInputOption *>(help) != NULL) {
            TerminalOutput::simpleWordWrap(
                    tr("This option specifies an input data source.  In the simplest form, this can "
                       "be a simple specification of a number or a variable name.  If a recognizable "
                       "number is given, then that is the value used for all time.  Otherwise if it "
                       "does not contain with a \"/\" then it is treated as a variable whose value is "
                       "used for all time.  No calibration or default is available in this case.  "
                       "For example \"--%1=12.0\" or \"--%1=BsG_S11\""
                       "\n\nIf the specification contains a \"/\" then it is treated as an advanced "
                       "input.  Advanced inputs allow for both time dependency and alteration of "
                       "input value.  This consists of a series of specifications separated by \"/\" "
                       "that define the effective input between the times they list.  The general "
                       "format is \"<Variables>,[<Calibration>,[<DefaultConstant>[,<Times>]]]\" or "
                       "\"<Constant>[,<Times>]\" in the former specification, the default constant is "
                       "only used if no value is obtained from the variables.  The variables "
                       "specification can consist of one or more variables combined with \";\".  Each "
                       "variable may have a station and archive prefix, separated by \":\" and a "
                       "flavors (e.x. PM1 or PM10) restricting suffix if both the station and archive "
                       "are given.  The flavor restricting suffix is also separated into components "
                       "with \":\" and consists of terms starting with \"=\" to specify that the "
                       "variable must have exactly all the \"=\" terms, \"!\" or \"-\" to exclude "
                       "specific flavors or no prefix to require it to have the given flavor.  "
                       "For example \"::BsG_S11:pm1:!cover\" specifies BsG_S11 sub-1um data that is "
                       "not coverage data and \"::BsG_S11:=pm1\" specifies only \"real\" values.  The "
                       "calibration is a set of polynomial coefficients separated by \";\" or \":\" in "
                       "ascending order.  In both cases the times specification is any recognized time "
                       "range specification.  In all cases a station, archive, or variable can be "
                       "specified as a regular expression.  For example "
                       "\"--%1=sgp::BsG_S11;BsR_S11,0;1.1,,,2010:1/Bs[BGR]_S11,,12.123,2010:1,2010:10\"")
                            .arg(help->getArgumentName()));
            return;
        }
    }

    {
        if (qobject_cast<DynamicSequenceSelectionOption *>(help) != NULL) {
            TerminalOutput::simpleWordWrap(
                    tr("This option specifies a set of variables to operate on.  In the simplest "
                       "form this can just be a list of variable specifications.  For example: "
                       "\"--%1=BsG_S11\".  If the specification contains a \"/\" then it is treated as "
                       "a set of variable lists and time ranges of the form \"<Variables>[,<Times>]\" "
                       "The time list is a standard time range specification.  In all cases the "
                       "variable list is a series of specifications separated by \";\".  Each "
                       "variable may have a station and archive prefix, separated by \":\" and a "
                       "flavors (e.x. PM1 or PM10) restricting suffix if both the station and archive "
                       "are given.  The flavor restricting suffix is also separated into components "
                       "with \":\" and consists of terms starting with \"=\" to specify that the "
                       "variable must have exactly all the \"=\" terms, \"!\" or \"-\" to exclude "
                       "specific flavors or no prefix to require it to have the given flavor.  For "
                       "example \"::BsG_S11:pm1:!cover\" specifies BsG_S11 sub-1um data that is not "
                       "coverage data and \"::BsG_S11:=pm1\" specifies only \"real\" values.  In "
                       "all variable components, regular expressions are accepted.  For example: "
                       "\"--%1=sgp::BsG_S11\", \"--%1=raw:Bs[BGR]_S11,2010:1,2010:10/BsG_S11,2010:10,,\"")
                            .arg(help->getArgumentName()));
            return;
        }
    }

    {
        ComponentOptionEnum *option;
        if ((option = qobject_cast<ComponentOptionEnum *>(help)) != NULL) {
            TerminalOutput::simpleWordWrap(
                    tr("This option takes a single value from a list of possible options.  For "
                       "example \"--%1=%2\".  The value is case insensitive.").arg(
                            help->getArgumentName(), option->getAll().at(0).getName()));
            TerminalOutput::output(tr("\nValid Values:\n"));

            QHash<QString, QString> enumValues;
            QList<ComponentOptionEnum::EnumValue> all(option->getAll());
            for (QList<ComponentOptionEnum::EnumValue>::const_iterator v = all.constBegin(),
                    end = all.constEnd(); v != end; ++v) {
                enumValues.insert(v->getName(), v->getDescription());
            }
            QHash<QString, ComponentOptionEnum::EnumValue> aliases(option->getAliases());
            for (QHash<QString, ComponentOptionEnum::EnumValue>::const_iterator
                    i = aliases.constBegin(); i != aliases.constEnd(); ++i) {
                enumValues.insert(i.key(), tr("Same as %1", "enum alias").arg(i.value().getName()));
            }

            QList<QString> sorted(enumValues.keys());
            std::sort(sorted.begin(), sorted.end(), caseInsensitiveLessThan);

            int argumentWidth = 0;
            for (int i = 0, max = sorted.size(); i < max; i++) {
                if (argumentWidth < sorted.at(i).length())
                    argumentWidth = sorted.at(i).length();
            }
            argumentWidth++;

            for (int i = 0, max = sorted.size(); i < max; i++) {
                OptionParse::optionSummaryOutput(argumentWidth, sorted.at(i),
                                                 enumValues.value(sorted.at(i)));
            }

            return;
        }
    }

    {
        TimeIntervalSelectionOption *option;
        if ((option = qobject_cast<TimeIntervalSelectionOption *>(help)) != NULL) {
            TerminalOutput::simpleWordWrap(
                    TimeParse::describeOffset(option->getAllowZero(), option->getAllowNegative()));
            TerminalOutput::simpleWordWrap(
                    tr("\n\nIf this option does does not contain with a \"/\" then it is treated as "
                       "a constant offset as described above.  For example \"--%1=1w\""
                       "\n\nIf the specification contains a \"/\" then it is treated as an advanced "
                       "interval selection.  This consists of a series of specifications separated "
                       "by \"/\" that define the effective interval between the times they list.  "
                       "The general format is \"<Specification>,[,<Times>]\" The times specification "
                       "is any recognized time range specification.  For example "
                       "\"--%1=1d,,2010:1/2d,2010:1,2010:10\"").arg(help->getArgumentName()));

            if (option->getAllowUndefined()) {
                TerminalOutput::simpleWordWrap(
                        tr("\n\nThe option may also be set (at any time) to \"none\", \"undef\" or "
                           "\"undefined\" to specify an explicitly undefined result of the offset."));
            }
            return;
        }
    }

    {
        if (qobject_cast<ComponentOptionTimeBlock *>(help) != NULL) {
            TerminalOutput::simpleWordWrap(TimeParse::describeOffset(false, false));
            TerminalOutput::simpleWordWrap(
                    tr("\n\nThe time interval specified must be strictly positive in all cases.  "
                       "That is, it must always result in a non-zero, so zero length aligned "
                       "specifications are not accepted."));
            return;
        }
    }

    {
        ComponentOptionTimeOffset *option;
        if ((option = qobject_cast<ComponentOptionTimeOffset *>(help)) != NULL) {
            TerminalOutput::simpleWordWrap(
                    TimeParse::describeOffset(option->getAllowZero(), option->getAllowNegative()));
            return;
        }
    }

    {
        if (qobject_cast<DynamicStringOption *>(help) != NULL) {
            TerminalOutput::simpleWordWrap(
                    tr("This option specifies a set of strings used as input.  In the simplest form "
                       "this can be just a single string applied for all time.  For example "
                       "\"--%1=STRING\".  If the specification contains a \"/\" then is is treated as "
                       "a set of strings and time ranges of the form \"<VALUE>[,<Times>]\".  "
                       "The time list is a standard time range specification.   For example "
                       "\"--%1=Value1,,2010:1/Value2,2010:1,2010:10\"").arg(
                            help->getArgumentName()));
            return;
        }
    }

    {
        if (qobject_cast<DynamicBoolOption *>(help) != NULL) {
            TerminalOutput::simpleWordWrap(
                    tr("This option specifies a set of booleans used as input.  In the simplest form "
                       "this can be just a single truth value applied for all time.  For example "
                       "\"--%1=true\" or just \"--%1\".  If the specification contains a \"/\" then "
                       "is is treated as a set of booleans and time ranges of the form "
                       "\"<VALUE>[,<Times>]\".  The time list is a standard time range "
                       "specification.   For example \"--%1=true,,2010:1/false,2010:1,2010:10\"").arg(
                            help->getArgumentName()));
            return;
        }
    }

    {
        DynamicDoubleOption *option;
        if ((option = qobject_cast<DynamicDoubleOption *>(help)) != NULL) {
            double max = option->getMaximum();
            double min = option->getMinimum();
            bool maxI = option->getMaximumInclusive();
            bool minI = option->getMinimumInclusive();
            QString ais;
            if (option->getAllowUndefined()) {
                ais = tr("  The value may also be empty, \"undef\", \"undefined\", or \"MVC\" to "
                         "indicate that it is set, but undefined.");
            }

            double exampleValue1 = 2.0;
            double exampleValue2 = 3.0;
            if (FP::defined(max) && FP::defined(min)) {
                if (maxI && minI) {
                    exampleValue1 = min;
                    exampleValue2 = max;
                    TerminalOutput::simpleWordWrap(
                            tr("This option takes a set of numbers as input.  The value must be greater "
                               "than or equal to %3 and less than or equal to %4.%5  For example \"--%1=%2\"")
                                    .arg(help->getArgumentName())
                                    .arg(exampleValue1)
                                    .arg(min)
                                    .arg(max)
                                    .arg(ais));
                } else if (maxI && !minI) {
                    exampleValue1 = max;
                    exampleValue2 = (max + min) / 2.0;
                    TerminalOutput::simpleWordWrap(
                            tr("This option takes a set of numbers as input.  The value must be greater "
                               "than or equal to %3 and less than %4.%5  For example \"--%1=%2\"").arg(help->getArgumentName())
                                                                                                  .arg(exampleValue1)
                                                                                                  .arg(min)
                                                                                                  .arg(max)
                                                                                                  .arg(ais));
                } else if (minI) {
                    exampleValue1 = min;
                    exampleValue2 = (max + min) / 2.0;
                    TerminalOutput::simpleWordWrap(
                            tr("This option takes a set of numbers as input.  The value must be greater "
                               "than %3 and less than or equal to %4.%5  For example \"--%1=%2\"").arg(help->getArgumentName())
                                                                                                  .arg(exampleValue1)
                                                                                                  .arg(min)
                                                                                                  .arg(max)
                                                                                                  .arg(ais));
                } else {
                    exampleValue1 = (max + min) / 2.0;
                    exampleValue2 = (max + min) * 0.75;
                    TerminalOutput::simpleWordWrap(
                            tr("This option takes a set of numbers as input.  The value must be greater "
                               "than %3 and less than %4.%5  For example \"--%1=%2\"").arg(help->getArgumentName())
                                                                                      .arg(exampleValue1)
                                                                                      .arg(min)
                                                                                      .arg(max)
                                                                                      .arg(ais));
                }
            } else if (FP::defined(max)) {
                if (maxI) {
                    exampleValue1 = max;
                    exampleValue2 = max * 0.5;
                    TerminalOutput::simpleWordWrap(
                            tr("This option takes a set of numbers as input.  The value must be less "
                               "than or equal to %3.%4  For example \"--%1=%2\"").arg(help->getArgumentName())
                                                                                 .arg(exampleValue1)
                                                                                 .arg(min)
                                                                                 .arg(ais));
                } else {
                    exampleValue1 = max * 0.5;
                    exampleValue2 = max * 0.75;
                    TerminalOutput::simpleWordWrap(
                            tr("This option takes a set of numbers as input.  The value must be less "
                               "than %3.%4  For example \"--%1=%2\"").arg(help->getArgumentName())
                                                                     .arg(exampleValue1)
                                                                     .arg(min)
                                                                     .arg(ais));
                }
            } else if (FP::defined(min)) {
                if (minI) {
                    exampleValue1 = min;
                    exampleValue2 = min * 0.5;
                    TerminalOutput::simpleWordWrap(
                            tr("This option takes a set of numbers as input.  The value must be greater "
                               "than or equal to %3.%4  For example \"--%1=%2\"").arg(help->getArgumentName())
                                                                                 .arg(exampleValue1)
                                                                                 .arg(max)
                                                                                 .arg(ais));
                } else {
                    exampleValue1 = min * 0.5;
                    exampleValue2 = min * 0.75;
                    TerminalOutput::simpleWordWrap(
                            tr("This option takes a set of numbers as input.  The value must be greater "
                               "than %3.  For example \"--%1=%2\"").arg(help->getArgumentName())
                                                                   .arg(exampleValue1)
                                                                   .arg(max)
                                                                   .arg(ais));
                }
            } else {
                exampleValue1 = 1.0;
                exampleValue2 = 2.0;
                TerminalOutput::simpleWordWrap(
                        tr("This option takes a set of numbers as input.%5  For example \"--%1=0.0\"")
                                .arg(help->getArgumentName())
                                .arg(ais));
            }
            TerminalOutput::simpleWordWrap(
                    tr("If the input contains a \"/\" then it is treated as being a specification of "
                       "multiple numbers at different times of the form \"<VALUE>[,<Times>]\".  "
                       "The time list is a standard time range specification.   For example "
                       "\"--%1=%2,,2010:1/%3,2010:1,2010:10\"").arg(help->getArgumentName())
                                                               .arg(exampleValue1)
                                                               .arg(exampleValue2));
            return;
        }
    }

    {
        DynamicIntegerOption *option;
        if ((option = qobject_cast<DynamicIntegerOption *>(help)) != NULL) {
            qint64 max = option->getMaximum();
            qint64 min = option->getMinimum();
            QString ais;
            if (option->getAllowUndefined()) {
                ais = tr("  The value may also be empty, \"undef\", \"undefined\", or \"MVC\" to "
                         "indicate that it is set, but undefined.");
            }
            qint64 exampleValue1 = 1;
            qint64 exampleValue2 = 2;
            if (INTEGER::defined(max) && INTEGER::defined(min)) {
                exampleValue1 = min;
                exampleValue2 = max;
                TerminalOutput::simpleWordWrap(
                        tr("This option takes option takes a set of integers as input.  The values "
                           "must be greater than or equal to %3 and less than or equal to %4.%5  "
                           "For example \"--%1=%2\"").arg(help->getArgumentName())
                                                     .arg(exampleValue1)
                                                     .arg(min)
                                                     .arg(max)
                                                     .arg(ais));
            } else if (INTEGER::defined(max)) {
                exampleValue1 = max;
                exampleValue2 = exampleValue1 - 1;
                TerminalOutput::simpleWordWrap(
                        tr("This option takes a set of integers as input.  The value must be less "
                           "than or equal to %3.%4  For example \"--%1=%2\"").arg(
                                help->getArgumentName()).arg(exampleValue1).arg(max).arg(ais));
            } else if (INTEGER::defined(min)) {
                exampleValue1 = min;
                exampleValue2 = exampleValue1 + 1;
                TerminalOutput::simpleWordWrap(
                        tr("This option takes a set of integers as input.  The value must be greater "
                           "than or equal to %3.%4  For example \"--%1=%2\"").arg(
                                help->getArgumentName()).arg(exampleValue1).arg(min).arg(ais));
            } else {
                TerminalOutput::simpleWordWrap(
                        tr("This option takes a set of integers as input.%2  For example \"--%1=0\"")
                                .arg(help->getArgumentName(), ais));
            }
            TerminalOutput::simpleWordWrap(
                    tr("If the input contains a \"/\" then it is treated as being a specification of "
                       "multiple numbers at different times of the form \"<VALUE>[,<Times>]\".  "
                       "The time list is a standard time range specification.   For example "
                       "\"--%1=%2,,2010:1/%3,2010:1,2010:10\"").arg(help->getArgumentName())
                                                               .arg(exampleValue1)
                                                               .arg(exampleValue2));
            return;
        }
    }

    {
        if (qobject_cast<DynamicCalibrationOption *>(help) != NULL) {
            TerminalOutput::simpleWordWrap(
                    tr("This option specifies a set of calibrations to use.  Each calibration is a "
                       "series of polynomial constants separated by \",\", \";\", or \":\".  The "
                       "constants are in ascending order of significance.  That is, the first number "
                       "is the constant offset, the second is the linear factor, etc.  For example "
                       "\"--%1=0.5,2.0\".  If the specification contains a \"/\" then is is treated "
                       "as a set of calibrations and time ranges of the form \"<VALUE>[,<Times>]\".  "
                       "The time list is a standard time range specification.  When this form is used "
                       "\",\" cannot be used to separate the constants.   For example "
                       "\"--%1=0.1:2.0,,2010:1/0.5:2.5,2010:1,2010:10\"").arg(
                            help->getArgumentName()));
            return;
        }
    }

    {
        BaselineSmootherOption *option;
        if ((option = qobject_cast<BaselineSmootherOption *>(help)) != NULL) {
            if (option->getStabilityDetection() && option->getSpikeDetection()) {
                TerminalOutput::simpleWordWrap(
                        tr("This option is used to determine a smoothed baseline value from the raw "
                           "input.  This baseline value is also used both to determine when the input is "
                           "stable and if the last inserted value was a spike.  The simplest form "
                           "consists of a minimum duration, maximum duration, initial wait time, stable "
                           "relative standard deviation, and spike band.  For example "
                           "\"--%1=30,60,0.01,2.0\".  If the specification contains a \"/\" is is treated "
                           "as variable time specification.  In this mode a time range can be added to "
                           "the end of the list to define when it is effective for.  For example "
                           "\"--%1=60,60,0,0.01,2.0/30,60,0,0.01,2.0,2010:1,2010:2\" which would specify a "
                           "minimum time of 60 seconds by default, but override it to 30 seconds on 2010 "
                           "day one.  "
                           "A spike band is the fraction minus one relative to zero.  So a band of 2.0 "
                           "considers any data below 1/3 or above 3 times the mean to be a spike.  "
                           "If the first field of the specification is \"1p\" or \"4p\" then the smoother "
                           "is defined as a one or four pole low pass digital filter.  The first field "
                           "after the type is a time interval (e.x. \"3m\") that specifies the time "
                           "constant of the filter; defaulting to three minutes.  The second field "
                           "specifies the gap detection threshold (e.x. \"1m\") defaulting to unlimited.  "
                           "If the third field evaluates to true then any missing value causes a filter "
                           "reset.  "
                           "The fourth field specifies the maximum band (as described above) of the input "
                           "value to the smoothed value to consider the smoother stable in.  "
                           "The fifth field specifies the minimum band (as described above) of the input "
                           "value to the smoothed value to report a spike.  "
                           "Additionally the specifications \"disable\", \"last\", \"anylast\" are "
                           "accepted to represent disabling the baseline, using the last value alone, "
                           "or using the last, even if invalid, value, respectively.  None of these will "
                           "ever be a spike and are stable if they have any valid value.").arg(
                                help->getArgumentName()));
            } else if (option->getStabilityDetection()) {
                TerminalOutput::simpleWordWrap(
                        tr("This option is used to determine a smoothed baseline value from the raw "
                           "input.  This baseline value is also used to determine when the input is "
                           "stable.  The simplest form consists of a minimum duration, maximum duration, "
                           "initial wait time, and stable relative standard deviation.  For example "
                           "\"--%1=30,60,0,0.010\".  If the specification contains a \"/\" is is treated "
                           "as variable time specification.  In this mode a time range can be added to "
                           "the end of the list to define when it is effective for.  For example "
                           "\"--%1=60,60,0,0.01/30,60,0,0.01,2010:1,2010:2\" which would specify a "
                           "minimum time of 60 seconds by default, but override it to 30 seconds on 2010 "
                           "day one.  "
                           "If the first field of the specification is \"1p\" or \"4p\" then the smoother "
                           "is defined as a one or four pole low pass digital filter.  The first field "
                           "after the type is a time interval (e.x. \"3m\") that specifies the time "
                           "constant of the filter; defaulting to three minutes.  The second field "
                           "specifies the gap detection threshold (e.x. \"1m\") defaulting to unlimited.  "
                           "If the third field evaluates to true then any missing value causes a filter "
                           "reset.  "
                           "The fourth field specifies the maximum band (as described above) of the input "
                           "value to the smoothed value to consider the smoother stable in.  "
                           "Additionally the specifications \"disable\", \"last\", \"anylast\" are "
                           "accepted to represent disabling the baseline, using the last value alone, "
                           "or using the last, even if invalid, value, respectively.  If there is any "
                           "value these options are stable.").arg(help->getArgumentName()));
            } else if (option->getSpikeDetection()) {
                TerminalOutput::simpleWordWrap(
                        tr("This option is used to determine a smoothed baseline value from the raw "
                           "input.  This baseline value is also if the last inserted value was a spike.  "
                           "The simplest form consists of a minimum duration, maximum duration, initial "
                           "wait time, stable relative standard deviation, and spike band.  For example "
                           "\"--%1=30,0,60,2.0\".  If the specification contains a \"/\" is is treated "
                           "as variable time specification.  In this mode a time range can be added to "
                           "the end of the list to define when it is effective for.  For example "
                           "\"--%1=60,60,0,2.0/30,60,0,2.0,2010:1,2010:2\" which would specify a "
                           "minimum time of 60 seconds by default, but override it to 30 seconds on 2010 "
                           "day one.  "
                           "A spike band is the fraction minus one relative to zero.  So a band of 2.0 "
                           "considers any data below 1/3 or above 3 times the mean to be a spike.  "
                           "If the first field of the specification is \"1p\" or \"4p\" then the smoother "
                           "is defined as a one or four pole low pass digital filter.  The first field "
                           "after the type is a time interval (e.x. \"3m\") that specifies the time "
                           "constant of the filter; defaulting to three minutes.  The second field "
                           "specifies the gap detection threshold (e.x. \"1m\") defaulting to unlimited.  "
                           "If the third field evaluates to true then any missing value causes a filter "
                           "reset.  "
                           "The fourth field specifies the minimum band (as described above) of the input "
                           "value to the smoothed value to report a spike.  "
                           "Additionally the specifications \"disable\", \"last\", \"anylast\" are "
                           "accepted to represent disabling the baseline, using the last value alone, "
                           "or using the last, even if invalid, value, respectively.  None of these will "
                           "ever be a spike.").arg(help->getArgumentName()));
            } else {
                TerminalOutput::simpleWordWrap(
                        tr("This option is used to determine a smoothed baseline value from the raw "
                           "input.  The simplest form consists of a minimum duration, maximum "
                           "duration, and initial wait time.  For example \"--%1=30,60\".  The maximum "
                           "duration can also be omitted and it will be set to the minimum.  "
                           "If the specification contains a \"/\" is is treated as variable time "
                           "specification.  In this mode a time range can be added to the end of the list "
                           "to define when it is effective for.  For example "
                           "\"--%1=60,600/30,60,2010:1,2010:2\" which would specify a "
                           "minimum time of 60 seconds by default, but override it to 30 seconds on 2010 "
                           "day one.  "
                           "If the first field of the specification is \"1p\" or \"4p\" then the smoother "
                           "is defined as a one or four pole low pass digital filter.  The first field "
                           "after the type is a time interval (e.x. \"3m\") that specifies the time "
                           "constant of the filter; defaulting to three minutes.  The second field "
                           "specifies the gap detection threshold (e.x. \"1m\") defaulting to unlimited.  "
                           "If the third field evaluates to true then any missing value causes a filter "
                           "reset.  "
                           "Additionally the specifications \"disable\", \"last\", \"anylast\" are "
                           "accepted to represent disabling the baseline, using the last value alone, "
                           "or using the last, even if invalid, value, respectively.").arg(
                                help->getArgumentName()));
            }
            return;
        }
    }

    {
        if (qobject_cast<SSLAuthenticationOption *>(help) != NULL) {
            TerminalOutput::simpleWordWrap(
                    tr("This option defines a key and certificate pair used with an SSL socket.  "
                       "That is, it sets the private key and corresponding certificate presented "
                       "to the other host.  The key and certificate are listed in that order, "
                       "separated by a \",\", \";\", \":\", or \"|\".  Normally both are given "
                       "as files (both PEM and DER encodings are accepted), however they may also be "
                       "given as inline PEM data (not recommended).  For example: "
                       "\"--%1=/path/to/key.pem,/path/to/cert.pem\"").arg(help->getArgumentName()));
            return;
        }
    }

    Q_ASSERT(false);
}


static QString describeTimeOffset(Time::LogicalTimeUnit unit,
                                  int count,
                                  bool aligned,
                                  bool defaultAligned)
{
    switch (unit) {
    case Time::Millisecond:
        if (aligned && !defaultAligned)
            return OptionParse::tr("%1mseca").arg(count);
        if (!aligned && defaultAligned)
            return OptionParse::tr("%1msecna").arg(count);
        return OptionParse::tr("%1msec").arg(count);
    case Time::Second:
        if (aligned && !defaultAligned)
            return OptionParse::tr("%1sa").arg(count);
        if (!aligned && defaultAligned)
            return OptionParse::tr("%1sna").arg(count);
        return OptionParse::tr("%1s").arg(count);
    case Time::Minute:
        if (aligned && !defaultAligned)
            return OptionParse::tr("%1ma").arg(count);
        if (!aligned && defaultAligned)
            return OptionParse::tr("%1mnoalign").arg(count);
        return OptionParse::tr("%1m").arg(count);
    case Time::Hour:
        if (aligned && !defaultAligned)
            return OptionParse::tr("%1ha").arg(count);
        if (!aligned && defaultAligned)
            return OptionParse::tr("%1hna").arg(count);
        return OptionParse::tr("%1h").arg(count);
    case Time::Day:
        if (aligned && !defaultAligned)
            return OptionParse::tr("%1da").arg(count);
        if (!aligned && defaultAligned)
            return OptionParse::tr("%1dna").arg(count);
        return OptionParse::tr("%1d").arg(count);
    case Time::Week:
        if (aligned && !defaultAligned)
            return OptionParse::tr("%1wa").arg(count);
        if (!aligned && defaultAligned)
            return OptionParse::tr("%1wna").arg(count);
        return OptionParse::tr("%1w").arg(count);
    case Time::Month:
        if (aligned && !defaultAligned)
            return OptionParse::tr("%1moa").arg(count);
        if (!aligned && defaultAligned)
            return OptionParse::tr("%1monnoalign").arg(count);
        return OptionParse::tr("%1mo").arg(count);
    case Time::Quarter:
        if (aligned && !defaultAligned)
            return OptionParse::tr("%1qa").arg(count);
        if (!aligned && defaultAligned)
            return OptionParse::tr("%1qna").arg(count);
        return OptionParse::tr("%1q").arg(count);
    case Time::Year:
        if (aligned && !defaultAligned)
            return OptionParse::tr("%1ya").arg(count);
        if (!aligned && defaultAligned)
            return OptionParse::tr("%1yna").arg(count);
        return OptionParse::tr("%1y").arg(count);
    }
    return OptionParse::tr("undef");
}

QString OptionParse::formatExampleOption(ComponentOptionBase *input,
                                         const ComponentExample &example)
{
    QString station(example.getInputStation());
    QString archive(example.getInputArchive());

    {
        ComponentOptionBoolean *option = qobject_cast<ComponentOptionBoolean *>(input);
        if (option != NULL) {
            return option->get() ? tr("true", "boolean true") : tr("false", "boolean false");
        }
    }

    {
        ComponentOptionSingleString *option;
        if ((option = qobject_cast<ComponentOptionSingleString *>(input)) != NULL) {
            QString str(option->get());
            if (str.contains(' ') ||
                    str.contains('"') ||
                    str.contains('&') ||
                    str.contains('>') ||
                    str.contains('<')) {
                str.prepend('\'');
                str.append('\'');
            }
            return str;
        }
    }

    {
        ComponentOptionSingleDouble *option;
        if ((option = qobject_cast<ComponentOptionSingleDouble *>(input)) != NULL) {
            return QString::number(option->get());
        }
    }

    {
        ComponentOptionSingleInteger *option;
        if ((option = qobject_cast<ComponentOptionSingleInteger *>(input)) != NULL) {
            return QString::number(option->get());
        }
    }

    {
        ComponentOptionDoubleSet *option;
        if ((option = qobject_cast<ComponentOptionDoubleSet *>(input)) != NULL) {
            QList<double> sorted(option->get().toList());
            std::sort(sorted.begin(), sorted.end());
            QStringList combined;
            for (QList<double>::const_iterator it = sorted.constBegin(), end = sorted.constEnd();
                    it != end;
                    ++it) {
                combined.append(QString::number(*it));
            }
            return combined.join(",");
        }
    }

    {
        ComponentOptionDoubleList *option;
        if ((option = qobject_cast<ComponentOptionDoubleList *>(input)) != NULL) {
            QList<double> l(option->get());
            QStringList combined;
            for (QList<double>::const_iterator it = l.constBegin(), end = l.constEnd();
                    it != end;
                    ++it) {
                combined.append(QString::number(*it));
            }
            return combined.join(",");
        }
    }

    {
        ComponentOptionStringSet *option;
        if ((option = qobject_cast<ComponentOptionStringSet *>(input)) != NULL) {
            QStringList l(option->get().toList());
            std::sort(l.begin(), l.end());
            if (option->get().contains(QString()) && l.size() <= 1)
                l.append(QString());
            return l.join(tr(",", "string list join"));
        }
    }

    {
        ComponentOptionEnum *option;
        if ((option = qobject_cast<ComponentOptionEnum *>(input)) != NULL) {
            return option->get().getName();
        }
    }

    {
        ComponentOptionSingleTime *option;
        if ((option = qobject_cast<ComponentOptionSingleTime *>(input)) != NULL) {
            return Time::toISO8601(option->get());
        }
    }

    {
        ComponentOptionSingleCalibration *option;
        if ((option = qobject_cast<ComponentOptionSingleCalibration *>(input)) != NULL) {
            Calibration cal(option->get());
            if (cal.size() == 0) {
                return tr("invalid", "calibration invalid");
            }
            QString str;
            for (int i = 0, max = cal.size(); i < max; i++) {
                if (str.isEmpty())
                    str.append(',');
                str.append(QString::number(cal.get(i)));
            }
            return str;
        }
    }

    {
        DynamicInputOption *option;
        if ((option = qobject_cast<DynamicInputOption *>(input)) != NULL) {
            DynamicInput *v = option->getInput();
            v->registerExpected(station.toStdString(), archive.toStdString());
            auto vs = v->getUsedInputs();
            QString result(v->describe(FP::undefined(), FP::undefined()));
            delete v;
            if (vs.empty())
                return result;
            SequenceName u(*vs.begin());
            if (u.getStationQString() == station &&
                    u.getArchiveQString() == archive &&
                    u.getFlavors().empty())
                return u.getVariableQString();
            return tr("%1:%2:%3", "filter operate format").arg(u.getStationQString(),
                                                               u.getArchiveQString(),
                                                               u.getVariableQString());
        }
    }

    {
        DynamicSequenceSelectionOption *option;
        if ((option = qobject_cast<DynamicSequenceSelectionOption *>(input)) != NULL) {
            DynamicSequenceSelection *v = option->getOperator();
            v->registerExpected(station.toStdString(), archive.toStdString());
            auto vs = v->getAllUnits();
            delete v;
            if (vs.empty())
                return QString();
            SequenceName u(*vs.begin());
            if (u.getStationQString() == station && u.getArchiveQString() == archive)
                return u.getVariableQString();
            return tr("%1:%2:%3", "filter operate format").arg(u.getStationQString(),
                                                               u.getArchiveQString(),
                                                               u.getVariableQString());
        }
    }

    {
        TimeIntervalSelectionOption *option;
        if ((option = qobject_cast<TimeIntervalSelectionOption *>(input)) != NULL) {
            DynamicTimeInterval *v = option->getInterval();
            Time::LogicalTimeUnit unit = Time::Second;
            int count = 0;
            bool aligned = false;
            if (!v->constantOffsetDescription(&unit, &count, &aligned)) {
                delete v;
                return tr("undef");
            }
            QString result(describeTimeOffset(unit, count, aligned, option->getDefaultAligned()));
            delete v;
            return result;
        }
    }

    {
        ComponentOptionTimeOffset *option;
        if ((option = qobject_cast<ComponentOptionTimeOffset *>(input)) != NULL) {
            return describeTimeOffset(option->getUnit(), option->getCount(), option->getAlign(),
                                      option->getDefaultAlign());
        }
    }

    {
        DynamicStringOption *option;
        if ((option = qobject_cast<DynamicStringOption *>(input)) != NULL) {
            DynamicString *v = option->getInput();
            QString result(v->get(FP::undefined()));
            delete v;
            return result;
        }
    }

    {
        DynamicBoolOption *option;
        if ((option = qobject_cast<DynamicBoolOption *>(input)) != NULL) {
            DynamicBool *v = option->getInput();
            QString result(v->get(FP::undefined()) ? tr("true", "boolean true") : tr("false",
                                                                                     "boolean false"));
            delete v;
            return result;
        }
    }

    {
        DynamicDoubleOption *option;
        if ((option = qobject_cast<DynamicDoubleOption *>(input)) != NULL) {
            DynamicDouble *v = option->getInput();
            QString result(QString::number(v->get(FP::undefined())));
            delete v;
            return result;
        }
    }

    {
        DynamicIntegerOption *option;
        if ((option = qobject_cast<DynamicIntegerOption *>(input)) != NULL) {
            DynamicInteger *v = option->getInput();
            QString result(QString::number(v->get(FP::undefined())));
            delete v;
            return result;
        }
    }

    {
        DynamicCalibrationOption *option;
        if ((option = qobject_cast<DynamicCalibrationOption *>(input)) != NULL) {
            DynamicCalibration *v = option->getInput();
            Calibration cal(v->get(FP::undefined()));
            delete v;
            QString result;
            for (int i = 0, max = cal.size(); i < max; i++) {
                if (!result.isEmpty())
                    result.append(",");
                result.append(QString::number(cal.get(i)));
            }
            return result;
        }
    }

    {
        SSLAuthenticationOption *option;
        if ((option = qobject_cast<SSLAuthenticationOption *>(input)) != NULL) {
            auto config = option->getLocalConfiguration();
            return tr("%1,%2", "ssl option format").arg(config["Key"].toQString(),
                                                        config["Certificate"].toQString());
        }
    }

    Q_ASSERT(false);
    return QString();
}

static bool sortExampleOptions(const ComponentOptionBase *a, const ComponentOptionBase *b)
{
    int p0 = a->getSortPriority();
    int p1 = b->getSortPriority();
    if (p0 < p1) return true;
    if (p0 > p1) return false;
    return a->getArgumentName() < b->getArgumentName();
}

QString OptionParse::assembleTimes(const ComponentExample &example)
{
    QString result;

    if (!FP::defined(example.getInputRange().getStart())) {
        if (!FP::defined(example.getInputRange().getEnd())) {
            result.append(tr("forever", "access all data"));
        } else {
            result.append(tr("none", "undefined bound"));
            result.append(' ');
            result.append(
                    Time::toYearDOY(example.getInputRange().getEnd(), QString(':'), Time::Year,
                                    QChar()));
        }
    } else {
        result.append(Time::toYearDOY(example.getInputRange().getStart(), QString(':'), Time::Year,
                                      QChar()));
        result.append(' ');
        if (!FP::defined(example.getInputRange().getEnd())) {
            result.append(tr("none", "undefined bound"));
        } else {
            result.append(
                    Time::toYearDOY(example.getInputRange().getEnd(), QString(':'), Time::Year,
                                    QChar()));
        }
    }

    return result;
}

QString OptionParse::assembleArchiveCommand(const ComponentExample &example)
{
    QString result(example.getInputStation());
    if (result.isEmpty())
        result.append(tr("allstations", "access all stations"));
    result.append(' ');

    QStringList displayInputs;
    if (example.getInputTypes() & ComponentExample::Input_Scattering)
        displayInputs.append(tr("S11a", "scattering input"));
    if (example.getInputTypes() & ComponentExample::Input_Absorption)
        displayInputs.append(tr("A11a", "absorption input"));
    if (example.getInputTypes() & ComponentExample::Input_Counts)
        displayInputs.append(tr("N61a", "absorption input"));
    displayInputs.append(example.getInputAuxiliary());

    if (displayInputs.isEmpty())
        result.append(tr("everything", "access all inputs"));
    else
        result.append(displayInputs.join(","));
    result.append(' ');

    result.append(assembleTimes(example));

    if (example.getInputArchive().isEmpty()) {
        result.append(' ');
        result.append(tr("allarchives", "access all archives"));
    } else if (example.getInputArchive() != "raw") {
        result.append(' ');
        result.append(example.getInputArchive());
    }

    return result;
}

/**
 * Output the list of formatted examples to standard output.
 * 
 * @param examples the list of examples
 */
void OptionParse::optionExampleOuput(const QString &program,
                                     const QList<ComponentExample> &examples,
                                     ExampleMode mode,
                                     int allowStations,
                                     int requireStations)
{
    QString switchPrefix(tr("--", "switch prefix"));

    bool first = true;
    bool haveShownAlternate = false;
    for (QList<ComponentExample>::const_iterator example = examples.constBegin(),
            endExamples = examples.constEnd(); example != endExamples; ++example) {
        if (!first) {
            TerminalOutput::simpleWordWrap(
                    tr("\n%1:\n", "later example heading").arg(example->getTitle()));
        } else {
            TerminalOutput::simpleWordWrap(
                    tr("%1:\n", "first example heading").arg(example->getTitle()));
        }
        first = false;

        QList<ComponentOptionBase *> sorted;
        QHash<QString, ComponentOptionBase *> allOptions = example->getOptions().getAll();
        for (QHash<QString, ComponentOptionBase *>::const_iterator i = allOptions.constBegin();
                i != allOptions.constEnd();
                ++i) {
            if (!i.value()->isSet())
                continue;
            sorted.append(i.value());
        }
        std::sort(sorted.begin(), sorted.end(), sortExampleOptions);

        QString arguments;
        for (QList<ComponentOptionBase *>::const_iterator option = sorted.constBegin(),
                endSorted = sorted.constEnd(); option != endSorted; ++option) {
            if (!arguments.isEmpty()) arguments.append(' ');

            QString arg(switchPrefix);
            arg.append((*option)->getArgumentName());
            QString add(formatExampleOption(*option, *example));
            if (!add.isEmpty()) {
                arg.append(tr("=", "argument value separator"));
                arg.append(add);
            }

            arguments.append(arg);
        }
        if (!arguments.isEmpty())
            arguments.prepend(' ');

        QString baseInvoke(tr("    %1", "base invoke").arg(program));

        bool showArchiveExamples = !(example->getInputTypes() & ComponentExample::Input_FileOnly);

        switch (mode) {
        case Example_DataFilter:
            if (showArchiveExamples) {
                TerminalOutput::applyWordWrap(
                        tr("%1%2 %3 > file.c3d", "filter example 1").arg(baseInvoke, arguments,
                                                                         assembleArchiveCommand(
                                                                                 *example)), false,
                        baseInvoke.length() + 1);
                if (!haveShownAlternate) {
                    TerminalOutput::output(tr("Or:\n", "example alternate"));
                    TerminalOutput::applyWordWrap(
                            tr("%1%2 file.c3d", "filter example 2").arg(baseInvoke, arguments),
                            false, baseInvoke.length() + 1);
                    TerminalOutput::output(tr("Or:\n", "example alternate"));
                    TerminalOutput::applyWordWrap(
                            tr("    da.get %3 | %1%2", "filter example 3").arg(program, arguments,
                                                                               assembleArchiveCommand(
                                                                                       *example)),
                            false, baseInvoke.length() + 1);

                    haveShownAlternate = true;
                }
            } else {
                TerminalOutput::applyWordWrap(
                        tr("%1%2 file.c3d", "filter example 2").arg(baseInvoke, arguments), false,
                        baseInvoke.length() + 1);
            }
            break;
        case Example_DataFilterSilent:
            if (showArchiveExamples) {
                TerminalOutput::applyWordWrap(
                        tr("%1%2 %3", "filter silent example 1").arg(baseInvoke, arguments,
                                                                     assembleArchiveCommand(
                                                                             *example)), false,
                        baseInvoke.length() + 1);
                if (!haveShownAlternate) {
                    TerminalOutput::output(tr("Or:\n", "example alternate"));
                    TerminalOutput::applyWordWrap(
                            tr("%1%2 file.c3d", "filter example 2").arg(baseInvoke, arguments),
                            false, baseInvoke.length() + 1);
                    TerminalOutput::output(tr("Or:\n", "example alternate"));
                    TerminalOutput::applyWordWrap(
                            tr("    da.get %3 | %1%2", "filter example 3").arg(program, arguments,
                                                                               assembleArchiveCommand(
                                                                                       *example)),
                            false, baseInvoke.length() + 1);

                    haveShownAlternate = true;
                }
            } else {
                TerminalOutput::applyWordWrap(
                        tr("%1%2 file.c3d", "filter example 2").arg(baseInvoke, arguments), false,
                        baseInvoke.length() + 1);
            }
            break;

        case Example_DataInputFile:
            TerminalOutput::applyWordWrap(
                    tr("%1%2 file.raw", "input file example 1").arg(baseInvoke, arguments), false,
                    baseInvoke.length() + 1);
            if (!haveShownAlternate) {
                TerminalOutput::output(tr("Or:\n", "example alternate"));
                TerminalOutput::applyWordWrap(
                        tr("    cat file.raw | %1%2", "filter example 2").arg(program, arguments),
                        false, baseInvoke.length() + 1);
                haveShownAlternate = false;
            }
            break;

        case Example_Action:
        case Example_DataInputExternal:
            if (requireStations == 1 && allowStations <= requireStations) {
                TerminalOutput::applyWordWrap(
                        tr("%1%2 %3", "single station example 1").arg(baseInvoke, arguments,
                                                                      example->getInputStation()),
                        false, baseInvoke.length() + 1);
                if (!haveShownAlternate) {
                    TerminalOutput::output(tr("Or:\n", "example alternate"));
                    TerminalOutput::applyWordWrap(
                            tr("%1%2", "station empty example 1").arg(baseInvoke, arguments), false,
                            baseInvoke.length() + 1);
                    haveShownAlternate = true;
                }
            } else if (requireStations > 0) {
                QString s;
                for (int i = 0; i < requireStations; i++) {
                    if (i != 0) s.append(',');
                    s.append(tr("stn%1", "station N").arg(i + 1));
                }
                TerminalOutput::applyWordWrap(
                        tr("%1%2 %3", "multiple station example 1").arg(baseInvoke, arguments, s),
                        false, baseInvoke.length() + 1);
            } else if (allowStations > 0) {
                TerminalOutput::applyWordWrap(
                        tr("%1%2 %3", "single station example 1").arg(baseInvoke, arguments,
                                                                      example->getInputStation()),
                        false, baseInvoke.length() + 1);
                if (!haveShownAlternate) {
                    if (allowStations > 1) {
                        TerminalOutput::output(tr("Or:\n", "example alternate"));
                        TerminalOutput::applyWordWrap(
                                tr("%1%2 bnd,smo", "multiple station example 1").arg(baseInvoke,
                                                                                     arguments),
                                false, baseInvoke.length() + 1);
                        haveShownAlternate = true;
                    }
                    TerminalOutput::output(tr("Or:\n", "example alternate"));
                    TerminalOutput::applyWordWrap(
                            tr("%1%2", "station empty example 1").arg(baseInvoke, arguments), false,
                            baseInvoke.length() + 1);
                }
            } else {
                TerminalOutput::applyWordWrap(
                        tr("%1%2", "station empty example 1").arg(baseInvoke, arguments), false,
                        baseInvoke.length() + 1);
            }
            break;

        case Example_ActionTimes:
        case Example_ActionTimesOptional:
        case Example_ExternalTimes:
        case Example_ExternalTimesOptional:
            if (requireStations == 1 && allowStations <= requireStations) {
                TerminalOutput::applyWordWrap(
                        tr("%1%2 %3 %4", "times single station example 1").arg(baseInvoke,
                                                                               arguments,
                                                                               example->getInputStation(),
                                                                               assembleTimes(
                                                                                       *example)),
                        false, baseInvoke.length() + 1);
                if (!haveShownAlternate) {
                    TerminalOutput::output(tr("Or:\n", "example alternate"));
                    TerminalOutput::applyWordWrap(
                            tr("%1%2 %3", "times no station example 1").arg(baseInvoke, arguments,
                                                                            assembleTimes(
                                                                                    *example)),
                            false, baseInvoke.length() + 1);
                }
                if (!haveShownAlternate &&
                        (mode == Example_ActionTimesOptional ||
                                mode == Example_ExternalTimesOptional)) {
                    TerminalOutput::output(tr("Or:\n", "example alternate"));
                    TerminalOutput::applyWordWrap(
                            tr("%1%2 %3", "single station example 1").arg(baseInvoke, arguments,
                                                                          example->getInputStation()),
                            false, baseInvoke.length() + 1);
                    TerminalOutput::output(tr("Or:\n", "example alternate"));
                    TerminalOutput::applyWordWrap(
                            tr("%1%2", "station empty example 1").arg(baseInvoke, arguments), false,
                            baseInvoke.length() + 1);
                }
                haveShownAlternate = true;
            } else if (requireStations > 0) {
                QString s;
                for (int i = 0; i < requireStations; i++) {
                    if (i != 0) s.append(',');
                    s.append(tr("stn%1", "station N").arg(i + 1));
                }
                TerminalOutput::applyWordWrap(
                        tr("%1%2 %3 %4", "times multiple station example 1").arg(baseInvoke,
                                                                                 arguments, s,
                                                                                 assembleTimes(
                                                                                         *example)),
                        false, baseInvoke.length() + 1);
                if (!haveShownAlternate &&
                        (mode == Example_ActionTimesOptional ||
                                mode == Example_ExternalTimesOptional)) {
                    TerminalOutput::output(tr("Or:\n", "example alternate"));
                    TerminalOutput::applyWordWrap(
                            tr("%1%2 %3", "multiple station example 1").arg(baseInvoke, arguments,
                                                                            s), false,
                            baseInvoke.length() + 1);
                }
                haveShownAlternate = true;
            } else if (allowStations > 0) {
                TerminalOutput::applyWordWrap(
                        tr("%1%2 %3 %4", "times single station example 1").arg(baseInvoke,
                                                                               arguments,
                                                                               example->getInputStation(),
                                                                               assembleTimes(
                                                                                       *example)),
                        false, baseInvoke.length() + 1);
                if (!haveShownAlternate) {
                    if (allowStations > 1) {
                        TerminalOutput::output(tr("Or:\n", "example alternate"));
                        TerminalOutput::applyWordWrap(
                                tr("%1%2 bnd,smo %3", "times multiple station example 1").arg(
                                        baseInvoke, arguments, assembleTimes(*example)), false,
                                baseInvoke.length() + 1);
                    }
                    TerminalOutput::output(tr("Or:\n", "example alternate"));
                    TerminalOutput::applyWordWrap(
                            tr("%1%2 %3", "times station empty example 1").arg(baseInvoke,
                                                                               arguments,
                                                                               assembleTimes(
                                                                                       *example)),
                            false, baseInvoke.length() + 1);
                }
                if (!haveShownAlternate &&
                        (mode == Example_ActionTimesOptional ||
                                mode == Example_ExternalTimesOptional)) {
                    if (allowStations > 1) {
                        TerminalOutput::output(tr("Or:\n", "example alternate"));
                        TerminalOutput::applyWordWrap(
                                tr("%1%2 bnd,smo", "multiple station example 1").arg(baseInvoke,
                                                                                     arguments),
                                false, baseInvoke.length() + 1);
                    }
                    TerminalOutput::output(tr("Or:\n", "example alternate"));
                    TerminalOutput::applyWordWrap(
                            tr("%1%2", "station empty example 1").arg(baseInvoke, arguments), false,
                            baseInvoke.length() + 1);
                }
                haveShownAlternate = true;
            } else {
                TerminalOutput::applyWordWrap(
                        tr("%1%2 %3", "times station empty example 1").arg(baseInvoke, arguments,
                                                                           assembleTimes(*example)),
                        false, baseInvoke.length() + 1);
                if (first &&
                        (mode == Example_ActionTimesOptional ||
                                mode == Example_ExternalTimesOptional)) {
                    TerminalOutput::output(tr("Or:\n", "example alternate"));
                    TerminalOutput::applyWordWrap(
                            tr("%1%2", "station empty example 1").arg(baseInvoke, arguments), false,
                            baseInvoke.length() + 1);
                }
            }
            break;

        default:
            break;
        }

        if (!example->getDescription().isEmpty()) {
            TerminalOutput::output("\n");
            TerminalOutput::applyWordWrap(example->getDescription());
        }
    }
}

static QList<SequenceMatch::Element> parseOperateMatcher(const QString &value)
{
    QStringList parts(value.split(QRegExp("[;,]+"), QString::SkipEmptyParts));
    QList<SequenceMatch::Element> result;
    for (const auto &str : parts) {
        result.append(SequenceMatch::Element(Variant::Root(str), {}, {}, {}, {}, {}, {}));
    }
    return result;
}

static Calibration parseCalibration(const QString &value) noexcept(false)
{
    QStringList parts(value.split(QRegExp("[:;,]+"), QString::KeepEmptyParts));
    QList<double> values;
    for (QList<QString>::const_iterator str = parts.constBegin(), end = parts.constEnd();
            str != end;
            ++str) {
        QString v(str->trimmed());
        if (v.isEmpty()) {
            values.append(0);
            continue;
        }
        bool ok;
        double dv = v.toDouble(&ok);
        if (!ok) {
            throw OptionParsingException(
                    OptionParse::tr("Invalid calibration \"%1\"\n").arg(value));
        }
        values.append(dv);
    }
    return Calibration(values);
}

template<class DigitalFilterOutputType>
static DigitalFilterOutputType *parseDigitalFilterTC(QStringList &parts) noexcept(false)
{
    DynamicTimeInterval *tc = NULL;
    if (!parts.isEmpty()) {
        Time::LogicalTimeUnit unit = Time::Minute;
        int count = 3;
        bool align = false;
        try {
            TimeParse::parseOffset(parts.takeFirst(), &unit, &count, &align, false, false);
        } catch (TimeParsingException tpe) {
            throw OptionParsingException(
                    OptionParse::tr("Error parsing time constant: %1").arg(tpe.getDescription()));
        }
        tc = new DynamicTimeInterval::Constant(unit, count, align);
    } else {
        tc = new DynamicTimeInterval::Constant(Time::Minute, 3, false);
    }
    DynamicTimeInterval *gap = NULL;
    if (!parts.isEmpty()) {
        QString v(parts.takeFirst());
        if (!v.isEmpty()) {
            Time::LogicalTimeUnit unit = Time::Minute;
            int count = 31;
            bool align = false;
            try {
                TimeParse::parseOffset(v, &unit, &count, &align, false, false);
            } catch (TimeParsingException tpe) {
                throw OptionParsingException(OptionParse::tr("Error parsing gap interval: %1").arg(
                        tpe.getDescription()));
            }
            gap = new DynamicTimeInterval::Constant(unit, count, align);
        }
    }
    bool resetUndefined = true;
    if (!parts.isEmpty()) {
        QString lc(parts.takeFirst());
        if (lc.isEmpty()) {
            resetUndefined = true;
        } else if (lc == OptionParse::tr("on", "boolean on") ||
                lc == OptionParse::tr("yes", "boolean yes") ||
                lc == OptionParse::tr("true", "boolean true")) {
            resetUndefined = true;
        } else if (lc == OptionParse::tr("off", "boolean off") ||
                lc == OptionParse::tr("no", "boolean no") ||
                lc == OptionParse::tr("false", "boolean false") ||
                lc == OptionParse::tr("0", "boolean zero")) {
            resetUndefined = false;
        } else {
            throw OptionParsingException(
                    OptionParse::tr("Invalid boolean truth value in argument."));
        }
    }
    return new DigitalFilterOutputType(tc, gap, resetUndefined);
}

static BaselineDigitalFilter *parseDigitalFilterBasline(DigitalFilter *filter,
                                                        QStringList &parts,
                                                        BaselineSmootherOption *baseline) noexcept(false)
{
    double stableBand = FP::undefined();
    if (baseline->getStabilityDetection() && !parts.isEmpty()) {
        if (!parts.first().isEmpty()) {
            bool ok = false;
            stableBand = parts.takeFirst().toDouble(&ok);
            if (!ok || !FP::defined(stableBand) || stableBand < 0.0) {
                throw OptionParsingException(
                        OptionParse::tr("Invalid stability band specification."));
            }
        }
    }

    double spikeBand = FP::undefined();
    if (baseline->getSpikeDetection() && !parts.isEmpty()) {
        if (!parts.first().isEmpty()) {
            bool ok = false;
            spikeBand = parts.takeFirst().toDouble(&ok);
            if (!ok || !FP::defined(spikeBand) || spikeBand < 0.0) {
                throw OptionParsingException(OptionParse::tr("Invalid spike band specification."));
            }
        }
    }

    return new BaselineDigitalFilter(filter, stableBand, spikeBand);
}

static double parseDouble(const QString &name,
                          const QString &value,
                          double min,
                          double max,
                          bool allowUndefined,
                          bool minimumInclusive, bool maximumInclusive) noexcept(false)
{
    double v;
    if (value.length() == 0) {
        v = FP::undefined();
    } else if (allowUndefined &&
            (value.toLower() == OptionParse::tr("undef", "number undefined") ||
                    value.toLower() == OptionParse::tr("undefined", "number undefined") ||
                    value.toLower() == OptionParse::tr("inf", "number undefined") ||
                    value.toLower() == OptionParse::tr("infinity", "number undefined") ||
                    value.toLower() == OptionParse::tr("mvc", "number undefined"))) {
        v = FP::undefined();
    } else {
        bool ok;
        v = value.toDouble(&ok);
        if (!ok) {
            throw OptionParsingException(
                    OptionParse::tr("Argument %1 does not specify a valid real number.").arg(name));
        }
    }

    if (!FP::defined(v)) {
        if (!allowUndefined) {
            throw OptionParsingException(
                    OptionParse::tr("Argument %1 must have a valid value.").arg(name));
        }
    } else {
        if (FP::defined(max)) {
            if (maximumInclusive) {
                if (v > max) {
                    throw OptionParsingException(
                            OptionParse::tr("Argument %1 is out of range.").arg(name));
                }
            } else {
                if (v >= max) {
                    throw OptionParsingException(
                            OptionParse::tr("Argument %1 is out of range.").arg(name));
                }
            }
        }

        if (FP::defined(min)) {
            if (minimumInclusive) {
                if (v < min) {
                    throw OptionParsingException(
                            OptionParse::tr("Argument %1 is out of range.").arg(name));
                }
            } else {
                if (v <= min) {
                    throw OptionParsingException(
                            OptionParse::tr("Argument %1 is out of range.").arg(name));
                }
            }
        }
    }
    return v;
}

static qint64 parseInteger(const QString &name,
                           const QString &value,
                           qint64 min,
                           qint64 max, bool allowUndefined) noexcept(false)
{
    qint64 v;
    if (value.length() == 0) {
        v = INTEGER::undefined();
    } else if (allowUndefined &&
            (value.toLower() == OptionParse::tr("undef", "number undefined") ||
                    value.toLower() == OptionParse::tr("undefined", "number undefined") ||
                    value.toLower() == OptionParse::tr("inf", "number undefined") ||
                    value.toLower() == OptionParse::tr("infinity", "number undefined") ||
                    value.toLower() == OptionParse::tr("mvc", "number undefined"))) {
        v = INTEGER::undefined();
    } else {
        bool ok;
        v = value.toLongLong(&ok);
        if (!ok) {
            throw OptionParsingException(
                    OptionParse::tr("Argument %1 does not specify a valid integer.").arg(name));
        }
    }

    if (!INTEGER::defined(v)) {
        if (!allowUndefined) {
            throw OptionParsingException(
                    OptionParse::tr("Argument %1 must have a valid value.").arg(name));
        }
    } else {
        if (INTEGER::defined(max)) {
            if (v > max) {
                throw OptionParsingException(
                        OptionParse::tr("Argument %1 is out of range.").arg(name));
            }
        }
        if (INTEGER::defined(min)) {
            if (v < min) {
                throw OptionParsingException(
                        OptionParse::tr("Argument %1 is out of range.").arg(name));
            }
        }
    }
    return v;
}


static QString parsePrimitive(const QString &value,
                              const QString &name,
                              DynamicStringOption *option) noexcept(false)
{
    Q_UNUSED(name);
    Q_UNUSED(option);
    return value;
}

static bool parsePrimitive(const QString &value,
                           const QString &name,
                           DynamicBoolOption *option) noexcept(false)
{
    Q_UNUSED(name);
    Q_UNUSED(option);
    if (value.length() == 0) {
        return true;
    } else {
        bool ok;
        if (value.toInt(&ok) != 0 && ok) {
            return true;
        } else {
            QString lc(value.toLower());
            if (lc == OptionParse::tr("on", "boolean on") ||
                    lc == OptionParse::tr("yes", "boolean yes") ||
                    lc == OptionParse::tr("true", "boolean true")) {
                return true;
            } else if (lc == OptionParse::tr("off", "boolean off") ||
                    lc == OptionParse::tr("no", "boolean no") ||
                    lc == OptionParse::tr("false", "boolean false") ||
                    lc == OptionParse::tr("0", "boolean zero")) {
                return false;
            } else {
                throw OptionParsingException(
                        OptionParse::tr("Invalid boolean truth value in argument %1.").arg(name));
            }
        }
    }
    return false;
}

static double parsePrimitive(const QString &value,
                             const QString &name,
                             DynamicDoubleOption *option) noexcept(false)
{
    return parseDouble(name, value, option->getMinimum(), option->getMaximum(),
                       option->getAllowUndefined(), option->getMinimumInclusive(),
                       option->getMaximumInclusive());
}

static qint64 parsePrimitive(const QString &value,
                             const QString &name,
                             DynamicIntegerOption *option) noexcept(false)
{
    return parseInteger(name, value, option->getMinimum(), option->getMaximum(),
                        option->getAllowUndefined());
}

static Calibration parsePrimitive(const QString &value,
                                  const QString &name,
                                  DynamicCalibrationOption *option) noexcept(false)
{
    Q_UNUSED(name);
    Q_UNUSED(option);
    return parseCalibration(value);
}

static bool isSplitByTime(const QString &value)
{
    static const QChar split('/');
    static const QChar escape('\\');
    for (int i = 0, max = value.size(); i < max; i++) {
        if (value.at(i) == split)
            return true;
        if (value.at(i) == escape)
            return true;
    }
    return false;
}

static QStringList splitIntoTimeRanges(const QString &value)
{
    static const QChar split('/');
    static const QChar escape('\\');
    QStringList result;
    QString current;
    bool inEscape = false;
    for (int i = 0, max = value.size(); i < max; i++) {
        if (inEscape) {
            Q_ASSERT(i > 0);
            inEscape = false;
            current.append(value.at(i));
            continue;
        }
        if (value.at(i) == split) {
            if (!current.isEmpty())
                result.append(current);
            current.clear();
            continue;
        }
        if (value.at(i) == escape) {
            inEscape = true;
            continue;
        }

        current.append(value.at(i));
    }
    if (!current.isEmpty())
        result.append(current);
    return result;
}

template<typename OptionType>
static void parsePrimitiveOption(const QString &name,
                                 const QString &value,
                                 OptionType option) noexcept(false)
{
    if (!isSplitByTime(value)) {
        option->set(parsePrimitive(value, name, option));
    } else {
        QStringList ranges(splitIntoTimeRanges(value));
        if (ranges.isEmpty()) {
            throw OptionParsingException(
                    OptionParse::tr("Complex time in argument %1  is empty").arg(name));
        }
        for (QList<QString>::const_iterator rangeStr = ranges.constBegin(), end = ranges.constEnd();
                rangeStr != end;
                ++rangeStr) {
            QStringList parts(rangeStr->split(',', QString::KeepEmptyParts));
            if (parts.isEmpty())
                continue;
            QString first(parts.takeFirst());
            if (parts.isEmpty()) {
                option->set(parsePrimitive(first, name, option));
            } else {
                Time::Bounds bounds;
                try {
                    bounds = TimeParse::parseListBounds(parts, true, true);
                } catch (TimeParsingException tpe) {
                    throw OptionParsingException(
                            OptionParse::tr("Error parsing time in argument %1 : %2").arg(name,
                                                                                          tpe.getDescription()));
                }
                option->set(parsePrimitive(first, name, option), bounds.start, bounds.end);
            }
        }
    }
}

namespace {
class SingleTimeReference : public TimeReference {
private:
    bool allowUndefined;
    double ref;
public:
    SingleTimeReference(const bool allowUndefined)
    {
        this->allowUndefined = allowUndefined;
        ref = Time::time();
    }

    SingleTimeReference(const SingleTimeReference &c) : TimeReference()
    {
        allowUndefined = c.allowUndefined;
        ref = c.ref;
    }

    virtual double getReference() noexcept(false)
    {
        return ref;
    }

    virtual bool isLeading() noexcept(false)
    {
        return true;
    }

    virtual double getUndefinedValue() noexcept(false)
    {
        if (allowUndefined)
            return FP::undefined();
        throw TimeParsingException(TimeParse::tr("Undefined times are not allowed."));
    }
};
};

/**
 * Parse the value string for a given option.  Throws an exception if
 * the option cannot be parsed.
 * 
 * @param output    the target option
 * @param value     the value to parse
 * @param displayName the full name of the option to display (used when an exception is thrown)
 */
void OptionParse::parse(ComponentOptionBase *output,
                        const QString &value,
                        const QString &displayName) noexcept(false)
{
    QString name(displayName);
    if (name.length() == 0)
        name = output->getArgumentName();

    {
        ComponentOptionBoolean *option;
        if ((option = qobject_cast<ComponentOptionBoolean *>(output)) != NULL) {
            if (value.length() == 0) {
                option->set(true);
            } else {
                bool ok;
                if (value.toInt(&ok) != 0 && ok) {
                    option->set(true);
                } else {
                    QString lc(value.toLower());
                    if (lc == tr("on", "boolean on") ||
                            lc == tr("yes", "boolean yes") ||
                            lc == tr("true", "boolean true")) {
                        option->set(true);
                    } else if (lc == tr("off", "boolean off") ||
                            lc == tr("no", "boolean no") ||
                            lc == tr("false", "boolean false") ||
                            lc == tr("0", "boolean zero")) {
                        option->set(false);
                    } else {
                        throw OptionParsingException(
                                tr("Invalid boolean truth value in argument %1.").arg(name));
                    }
                }
            }
            return;
        }
    }

    {
        ComponentOptionSingleString *option;
        if ((option = qobject_cast<ComponentOptionSingleString *>(output)) != NULL) {
            option->set(value);
            return;
        }
    }

    {
        ComponentOptionSingleDouble *option;
        if ((option = qobject_cast<ComponentOptionSingleDouble *>(output)) != NULL) {
            double v = parseDouble(name, value, option->getMinimum(), option->getMaximum(),
                                   option->getAllowUndefined(), option->getMinimumInclusive(),
                                   option->getMaximumInclusive());
            if (!option->isValid(v)) {
                throw OptionParsingException(
                        OptionParse::tr("Argument %1 is not valid.").arg(name));
            }
            option->set(v);
            return;
        }
    }

    {
        ComponentOptionSingleInteger *option;
        if ((option = qobject_cast<ComponentOptionSingleInteger *>(output)) != NULL) {
            qint64 v = parseInteger(name, value, option->getMinimum(), option->getMaximum(),
                                    option->getAllowUndefined());
            if (!option->isValid(v)) {
                throw OptionParsingException(
                        OptionParse::tr("Argument %1 is not valid.").arg(name));
            }
            option->set(v);
            return;
        }
    }

    {
        ComponentOptionDoubleSet *option;
        if ((option = qobject_cast<ComponentOptionDoubleSet *>(output)) != NULL) {
            if (value.length() == 0) {
                option->set(QSet<double>());
                return;
            }

            QStringList parts(value.split(QRegExp("[:;,]+"), QString::SkipEmptyParts));
            option->clear();
            for (QList<QString>::const_iterator str = parts.constBegin(), end = parts.constEnd();
                    str != end;
                    ++str) {
                bool ok = false;
                double v = str->toDouble(&ok);
                if (!ok) {
                    throw OptionParsingException(
                            tr("Argument %1 (%2) does not specify a valid real number.").arg(name,
                                                                                             *str));
                }
                if (!FP::defined(v)) {
                    throw OptionParsingException(
                            tr("Argument %1 (%2) can only contain real numbers.").arg(name, *str));
                }

                if (FP::defined(option->getMaximum())) {
                    if (option->getMaximumInclusive()) {
                        if (v > option->getMaximum()) {
                            throw OptionParsingException(
                                    tr("Argument %1 (%2) is out of range.").arg(name, *str));
                        }
                    } else {
                        if (v >= option->getMaximum()) {
                            throw OptionParsingException(
                                    tr("Argument %1 (%2) is out of range.").arg(name, *str));
                        }
                    }
                }

                if (FP::defined(option->getMinimum())) {
                    if (option->getMinimumInclusive()) {
                        if (v < option->getMinimum()) {
                            throw OptionParsingException(
                                    tr("Argument %1 (%2) is out of range.").arg(name, *str));
                        }
                    } else {
                        if (v <= option->getMinimum()) {
                            throw OptionParsingException(
                                    tr("Argument %1 (%2) is out of range.").arg(name, *str));
                        }
                    }
                }

                if (!option->isValid(v)) {
                    throw OptionParsingException(
                            OptionParse::tr("Argument %1 is not valid.").arg(name));
                }

                option->add(v);
            }
            return;
        }
    }

    {
        ComponentOptionDoubleList *option;
        if ((option = qobject_cast<ComponentOptionDoubleList *>(output)) != NULL) {
            int requireN = option->getMinimumComponents();
            if (value.length() == 0 && requireN <= 0) {
                option->set(QList<double>());
                return;
            }

            QStringList parts(value.split(QRegExp("[:;,]+"), QString::SkipEmptyParts));
            option->clear();
            int n = 0;
            for (QList<QString>::const_iterator str = parts.constBegin(), end = parts.constEnd();
                    str != end;
                    ++str) {
                bool ok;
                double v = str->toDouble(&ok);
                if (!ok) {
                    throw OptionParsingException(
                            tr("Argument %1 (%2) does not specify a valid real number.").arg(name,
                                                                                             *str));
                }
                if (!FP::defined(v)) {
                    throw OptionParsingException(
                            tr("Argument %1 (%2) can only contain real numbers.").arg(name, *str));
                }

                option->append(v);
                n++;
            }
            if (requireN > 0 && n < requireN) {
                throw OptionParsingException(tr("Argument %1 must contain at least %n number(s).",
                                                "double list too short", requireN).arg(name));
            }
            return;
        }
    }

    {
        ComponentOptionStringSet *option;
        if ((option = qobject_cast<ComponentOptionStringSet *>(output)) != NULL) {
            QStringList parts(value.split(QRegExp("[:;,]+"), QString::KeepEmptyParts));
            option->clear();
            for (QList<QString>::const_iterator str = parts.constBegin(), end = parts.constEnd();
                    str != end;
                    ++str) {
                if (str->isEmpty() && str == parts.constBegin())
                    continue;
                option->add(*str);
            }
            return;
        }
    }

    {
        ComponentOptionSingleTime *option;
        if ((option = qobject_cast<ComponentOptionSingleTime *>(output)) != NULL) {
            try {
                SingleTimeReference ref(option->getAllowUndefined());
                option->set(TimeParse::parseTime(value, &ref));
            } catch (TimeParsingException tpe) {
                throw OptionParsingException(tr("Error parsing time in argument %1 : %2").arg(name,
                                                                                              tpe.getDescription()));
            }
            return;
        }
    }

    {
        ComponentOptionSingleCalibration *option;
        if ((option = qobject_cast<ComponentOptionSingleCalibration *>(output)) != NULL) {
            if (option->getAllowInvalid() &&
                    (value.isEmpty() ||
                            value.toLower() == tr("undef", "calibration invalid") ||
                            value.toLower() == tr("undefined", "calibration invalid") ||
                            value.toLower() == tr("invalid", "calibration invalid"))) {
                option->set(Calibration(QVector<double>()));
            } else {
                QStringList parts(value.split(QRegExp("[:;,]+"), QString::SkipEmptyParts));
                if (parts.isEmpty()) {
                    if (option->getAllowInvalid()) {
                        option->set(Calibration(QVector<double>()));
                    } else {
                        throw OptionParsingException(
                                tr("Argument %1 must have a valid calibration.").arg(name));
                    }
                    return;
                }
                Calibration cal;
                cal.clear();
                for (QList<QString>::const_iterator str = parts.constBegin(),
                        end = parts.constEnd(); str != end; ++str) {
                    bool ok;
                    double v = str->toDouble(&ok);
                    if (!ok || !FP::defined(v)) {
                        throw OptionParsingException(
                                tr("Argument %1 contains a coefficient that is not a valid real number.")
                                        .arg(name));
                    }
                    cal.append(v);
                }
                if (cal.size() == 0) {
                    if (!option->getAllowInvalid()) {
                        throw OptionParsingException(
                                tr("Argument %1 must have a valid calibration.").arg(name));
                    }
                } else if (cal.size() == 1) {
                    if (!option->getAllowConstant()) {
                        throw OptionParsingException(
                                tr("Argument %1 must specify a calibration that is not a single constant value.")
                                        .arg(name));
                    }
                }
                option->set(cal);
            }
            return;
        }
    }

    {
        DynamicInputOption *option;
        if ((option = qobject_cast<DynamicInputOption *>(output)) != NULL) {
            if (!isSplitByTime(value)) {
                bool ok;
                double check = value.toDouble(&ok);
                if (ok) {
                    option->set(check);
                } else {
                    SequenceMatch::OrderedLookup sel;
                    sel = SequenceMatch::OrderedLookup(Variant::Root(value));
                    if (!sel.valid()) {
                        throw OptionParsingException(
                                tr("Argument %1 does not contain any valid input specifications.").arg(
                                        name));
                    }
                    option->set(std::move(sel), Calibration());
                }
            } else {
                QStringList ranges(splitIntoTimeRanges(value));
                for (QList<QString>::const_iterator rangeStr = ranges.constBegin(),
                        end = ranges.constEnd(); rangeStr != end; ++rangeStr) {
                    QStringList parts(rangeStr->split(',', QString::KeepEmptyParts));
                    if (parts.isEmpty())
                        continue;
                    bool ok;
                    QString first(parts.takeFirst());
                    double check = first.toDouble(&ok);
                    if (ok) {
                        Time::Bounds bounds;
                        try {
                            bounds = TimeParse::parseListBounds(parts, true, true);
                        } catch (TimeParsingException tpe) {
                            throw OptionParsingException(
                                    tr("Error parsing time in argument %1 : %2").arg(name,
                                                                                     tpe.getDescription()));
                        }
                        option->set(bounds.start, bounds.end, check);
                    } else {
                        SequenceMatch::OrderedLookup sel;
                        sel = SequenceMatch::OrderedLookup(Variant::Root(first));
                        if (!sel.valid()) {
                            throw OptionParsingException(
                                    tr("Argument %1 contains an advanced specification with no input variables.")
                                            .arg(name));
                        }
                        Calibration cal;

                        if (!parts.isEmpty()) {
                            QString rawCal(parts.takeFirst());
                            if (!rawCal.isEmpty())
                                cal = parseCalibration(rawCal);
                        }
                        QString rawDefault;
                        if (!parts.isEmpty())
                            rawDefault = parts.takeFirst().trimmed();


                        Time::Bounds bounds;
                        try {
                            bounds = TimeParse::parseListBounds(parts, true, true);
                        } catch (TimeParsingException tpe) {
                            throw OptionParsingException(
                                    tr("Error parsing time in argument %1 : %2").arg(name,
                                                                                     tpe.getDescription()));
                        }

                        option->set(bounds.start, bounds.end, std::move(sel), cal);

                        if (!rawDefault.isEmpty()) {
                            check = rawDefault.toDouble(&ok);
                            if (!ok) {
                                throw OptionParsingException(
                                        tr("Argument %1 contains an invalid default value specification \"%2\".")
                                                .arg(name, rawDefault));
                            }
                            option->set(bounds.start, bounds.end, check, true);
                        }
                    }
                }
            }
            return;
        }
    }

    {
        DynamicSequenceSelectionOption *option;
        if ((option = qobject_cast<DynamicSequenceSelectionOption *>(output)) != NULL) {
            QStringList ranges(splitIntoTimeRanges(value));
            if (ranges.isEmpty()) {
                throw OptionParsingException(
                        tr("Argument %1 does not contain any valid variable specifications.").arg(
                                name));
            }
            for (QList<QString>::const_iterator rangeStr = ranges.constBegin(),
                    end = ranges.constEnd(); rangeStr != end; ++rangeStr) {
                QStringList parts(rangeStr->split(',', QString::KeepEmptyParts));
                if (parts.isEmpty()) {
                    throw OptionParsingException(
                            tr("Argument %1 contains an invalid variable specification.").arg(
                                    name));
                }
                QString variablesRaw(parts.takeFirst());
                variablesRaw = variablesRaw.trimmed();
                QList<SequenceMatch::Element> variables;
                if (!variablesRaw.isEmpty())
                    variables = parseOperateMatcher(variablesRaw);

                if (!parts.isEmpty()) {
                    Time::Bounds bounds;
                    try {
                        bounds = TimeParse::parseListBounds(parts, true, true);
                    } catch (TimeParsingException tpe) {
                        throw OptionParsingException(
                                tr("Error parsing time in argument %1 : %2").arg(name,
                                                                                 tpe.getDescription()));
                    }
                    option->set(bounds.start, bounds.end, variables);
                } else {
                    option->set(variables);
                }
            }
            return;
        }
    }

    {
        ComponentOptionEnum *option;
        if ((option = qobject_cast<ComponentOptionEnum *>(output)) != NULL) {
            QString lc(value.toLower());
            QList<ComponentOptionEnum::EnumValue> all(option->getAll());
            for (QList<ComponentOptionEnum::EnumValue>::const_iterator v = all.constBegin(),
                    end = all.constEnd(); v != end; ++v) {
                if (lc == v->getName().toLower()) {
                    option->set(*v);
                    return;
                }
            }

            QHash<QString, ComponentOptionEnum::EnumValue> aliases(option->getAliases());
            for (QHash<QString, ComponentOptionEnum::EnumValue>::const_iterator
                    i = aliases.constBegin(); i != aliases.constEnd(); ++i) {
                if (lc == i.key().toLower()) {
                    option->set(i.value());
                    return;
                }
            }

            throw OptionParsingException(
                    tr("Argument %1 contains an invalid value \"%2\".").arg(name, value));
        }
    }

    {
        TimeIntervalSelectionOption *option;
        if ((option = qobject_cast<TimeIntervalSelectionOption *>(output)) != NULL) {
            QStringList ranges(splitIntoTimeRanges(value));
            if (ranges.isEmpty()) {
                throw OptionParsingException(
                        tr("Argument %1 does not contain any valid interval specifications.").arg(
                                name));
            }
            for (QList<QString>::const_iterator rangeStr = ranges.constBegin(),
                    end = ranges.constEnd(); rangeStr != end; ++rangeStr) {
                QStringList parts(rangeStr->split(',', QString::KeepEmptyParts));
                if (parts.isEmpty()) {
                    throw OptionParsingException(
                            tr("Argument %1 contains an invalid interval specification.").arg(
                                    name));
                }
                QString intervalRaw(parts.takeFirst());
                intervalRaw = intervalRaw.trimmed();
                QString intervalLC(intervalRaw.toLower());

                if (option->getAllowUndefined()) {
                    if (intervalLC == tr("undef") ||
                            intervalLC == tr("undefined") ||
                            intervalLC == tr("none")) {
                        if (!parts.isEmpty()) {
                            Time::Bounds bounds;
                            try {
                                bounds = TimeParse::parseListBounds(parts, true, true);
                            } catch (TimeParsingException tpe) {
                                throw OptionParsingException(
                                        tr("Error parsing time in argument %1 : %2").arg(name,
                                                                                         tpe.getDescription()));
                            }
                            option->setUndefined(bounds.start, bounds.end);
                        } else {
                            option->setUndefined();
                        }
                        continue;
                    }
                }

                Time::LogicalTimeUnit unit = Time::Second;
                int count = 0;
                bool aligned = option->getDefaultAligned();
                try {
                    TimeParse::parseOffset(intervalRaw, &unit, &count, &aligned,
                                           option->getAllowZero(), option->getAllowNegative());
                } catch (TimeParsingException tpe) {
                    throw OptionParsingException(
                            tr("Error parsing time in argument %1 : %2").arg(name,
                                                                             tpe.getDescription()));
                }

                if (!parts.isEmpty()) {
                    Time::Bounds bounds;
                    try {
                        bounds = TimeParse::parseListBounds(parts, true, true);
                    } catch (TimeParsingException tpe) {
                        throw OptionParsingException(
                                tr("Error parsing time in argument %1 : %2").arg(name,
                                                                                 tpe.getDescription()));
                    }
                    option->set(bounds.start, bounds.end, unit, count, aligned);
                } else {
                    option->set(unit, count, aligned);
                }
            }
            return;
        }
    }

    {
        ComponentOptionTimeOffset *option;
        if ((option = qobject_cast<ComponentOptionTimeOffset *>(output)) != NULL) {
            Time::LogicalTimeUnit unit = option->getDefaultUnit();
            int count = option->getDefaultCount();
            bool align = option->getDefaultAlign();
            try {
                TimeParse::parseOffset(value, &unit, &count, &align, option->getAllowZero(),
                                       option->getAllowNegative());
            } catch (TimeParsingException tpe) {
                throw OptionParsingException(tr("Error parsing time in argument %1 : %2").arg(name,
                                                                                              tpe.getDescription()));
            }
            if (qobject_cast<ComponentOptionTimeBlock *>(output) != NULL) {
                if (count < 1) {
                    throw OptionParsingException(
                            tr("The time interval specified in argument %1 must be greater than zero.")
                                    .arg(name));
                }
            }
            option->set(unit, count, align);
            return;
        }
    }

    {
        DynamicStringOption *option;
        if ((option = qobject_cast<DynamicStringOption *>(output)) != NULL) {
            parsePrimitiveOption(name, value, option);
            return;
        }
    }

    {
        DynamicBoolOption *option;
        if ((option = qobject_cast<DynamicBoolOption *>(output)) != NULL) {
            if (value.isEmpty()) {
                option->set(true);
                return;
            }
            parsePrimitiveOption(name, value, option);
            return;
        }
    }

    {
        DynamicDoubleOption *option;
        if ((option = qobject_cast<DynamicDoubleOption *>(output)) != NULL) {
            parsePrimitiveOption(name, value, option);
            return;
        }
    }

    {
        DynamicIntegerOption *option;
        if ((option = qobject_cast<DynamicIntegerOption *>(output)) != NULL) {
            parsePrimitiveOption(name, value, option);
            return;
        }
    }

    {
        DynamicCalibrationOption *option;
        if ((option = qobject_cast<DynamicCalibrationOption *>(output)) != NULL) {
            parsePrimitiveOption(name, value, option);
            return;
        }
    }

    {
        BaselineSmootherOption *option;
        if ((option = qobject_cast<BaselineSmootherOption *>(output)) != NULL) {
            QStringList ranges(splitIntoTimeRanges(value));
            if (ranges.isEmpty()) {
                throw OptionParsingException(
                        tr("Argument %1 does not contain any valid baseline smoother specifications.")
                                .arg(name));
            }
            for (QList<QString>::const_iterator rangeStr = ranges.constBegin(),
                    end = ranges.constEnd(); rangeStr != end; ++rangeStr) {
                QStringList parts(rangeStr->split(',', QString::KeepEmptyParts));

                if (parts.isEmpty()) {
                    throw OptionParsingException(
                            tr("Argument %1 contains an invalid smoother specification.").arg(
                                    name));
                }

                QString checkFirst(parts.at(0).toLower());
                BaselineSmoother *smoother;
                if (checkFirst == tr("disable")) {
                    smoother = new BaselineInvalid;
                    parts.removeFirst();
                } else if (checkFirst == tr("last")) {
                    smoother = new BaselineSinglePoint;
                    parts.removeFirst();
                } else if (checkFirst == tr("anylast")) {
                    smoother = new BaselineLatest;
                    parts.removeFirst();
                } else if (checkFirst == tr("1p") ||
                        checkFirst == tr("lowpass") ||
                        checkFirst == tr("1plp") ||
                        checkFirst == tr("singlepole") ||
                        checkFirst == tr("singlepolelowpass")) {
                    parts.removeFirst();
                    DigitalFilter
                            *filter = parseDigitalFilterTC<DigitalFilterSinglePoleLowPass>(parts);
                    smoother = parseDigitalFilterBasline(filter, parts, option);
                } else if (checkFirst == tr("4p") ||
                        checkFirst == tr("4plp") ||
                        checkFirst == tr("fourpole") ||
                        checkFirst == tr("fourpolelowpass")) {
                    parts.removeFirst();
                    DigitalFilter
                            *filter = parseDigitalFilterTC<DigitalFilterFourPoleLowPass>(parts);
                    smoother = parseDigitalFilterBasline(filter, parts, option);
                } else {
                    bool ok;
                    double requireTime = parts.takeFirst().toDouble(&ok);
                    if (!ok || !FP::defined(requireTime) || requireTime < 0.0) {
                        throw OptionParsingException(
                                tr("Argument %1 contains an invalid required time specification.").arg(
                                        name));
                    }
                    double maximumTime = requireTime;
                    if (!parts.isEmpty()) {
                        if (!parts.first().isEmpty()) {
                            maximumTime = parts.takeFirst().toDouble(&ok);
                            if (!ok || !FP::defined(maximumTime) || maximumTime < requireTime) {
                                throw OptionParsingException(
                                        tr("Argument %1 contains an invalid maximum time specification.")
                                                .arg(name));
                            }
                        }
                    }
                    double waitTime = 0.0;
                    if (!parts.isEmpty()) {
                        if (!parts.first().isEmpty()) {
                            waitTime = parts.takeFirst().toDouble(&ok);
                            if (!ok || !FP::defined(maximumTime) || waitTime < 0.0) {
                                throw OptionParsingException(
                                        tr("Argument %1 contains an invalid wait time specification.")
                                                .arg(name));
                            }
                        }
                    }

                    double stableRSD = FP::undefined();
                    if (option->getStabilityDetection() && !parts.isEmpty()) {
                        if (!parts.first().isEmpty()) {
                            stableRSD = parts.takeFirst().toDouble(&ok);
                            if (!ok || !FP::defined(stableRSD) || stableRSD < 0.0) {
                                throw OptionParsingException(
                                        tr("Argument %1 contains an invalid stability RSD specification.")
                                                .arg(name));
                            }
                        }
                    }
                    double spikeBand = FP::undefined();
                    if (option->getSpikeDetection() && !parts.isEmpty()) {
                        if (!parts.first().isEmpty()) {
                            spikeBand = parts.takeFirst().toDouble(&ok);
                            if (!ok || !FP::defined(spikeBand) || spikeBand < 0.0) {
                                throw OptionParsingException(
                                        tr("Argument %1 contains an invalid spike band specification.")
                                                .arg(name));
                            }
                        }
                    }

                    smoother = new BaselineFixedTime(requireTime, maximumTime, stableRSD, spikeBand,
                                                     waitTime);
                }


                if (!parts.isEmpty()) {
                    Time::Bounds bounds;
                    try {
                        bounds = TimeParse::parseListBounds(parts, true, true);
                    } catch (TimeParsingException tpe) {
                        throw OptionParsingException(
                                tr("Error parsing time in argument %1 : %2").arg(name,
                                                                                 tpe.getDescription()));
                    }
                    option->overlay(smoother, bounds.start, bounds.end);
                } else {
                    option->overlay(smoother, FP::undefined(), FP::undefined());
                }
            }
            return;
        }
    }

    {
        SSLAuthenticationOption *option;
        if ((option = qobject_cast<SSLAuthenticationOption *>(output)) != NULL) {
            QStringList parts(value.split(QRegExp("[:;,\\|]+"), QString::SkipEmptyParts));
            if (!parts.isEmpty())
                option->setKey(parts.takeFirst());
            if (!parts.isEmpty())
                option->setCertificate(parts.takeFirst());
            return;
        }
    }

    Q_ASSERT(false);
}

}
}
