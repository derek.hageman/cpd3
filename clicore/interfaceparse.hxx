/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3_INTERFACEPARSE_HXX
#define CPD3_INTERFACEPARSE_HXX

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QList>
#include <QString>
#include <QStringList>

#include "clicore/clicore.hxx"
#include "clicore/optionparse.hxx"
#include "clicore/argumentparser.hxx"
#include "datacore/variant/root.hxx"

namespace CPD3 {
namespace CLI {

/**
 * An exception representing an error parsing the command line I/o interface
 * specification.
 */
class CPD3CLICORE_EXPORT InterfaceParsingException {
private:
    QString description;
public:
    InterfaceParsingException(const QString &d);

    /**
     * Get the localized description of what occurred.
     */
    QString getDescription() const;
};

/**
 * Provides utilities to parse I/O interface specifications from the command line.
 */
class CPD3CLICORE_EXPORT InterfaceParse : public QObject {
Q_OBJECT
public:
    /**
     * Parse bare word arguments into an I/O interface specification configuration value.
     *
     * @param arguments the bare words to parse
     * @param allowRemoteCPD3 enable connection to remote CPD3 servers
     * @return          the I/O interface configuration value
     */
    static Data::Variant::Root parse(QStringList arguments,
                                     bool allowRemoteCPD3 = true) noexcept(false);

    /**
     * Get the bare word help for the interface parsing.
     *
     * @param allowRemoteCPD3 enable connection to remote CPD3 servers
     * @return      the bare word help information
     */
    static QList<ArgumentParser::BareWordHelp> getBareWordHelp(bool allowRemoteCPD3 = true);

    /**
     * Get the bare word command line used for interface parsing.
     *
     * @param allowRemoteCPD3 enable connection to remote CPD3 servers
     * @return      the command line string
     */
    static QString getBareWordCommandLine(bool allowRemoteCPD3 = true);
};

}
}

#endif //CPD3_INTERFACEPARSE_HXX
