/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3CLICOREARGUMENTPARSER_H
#define CPD3CLICOREARGUMENTPARSER_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QStringList>
#include <QList>

#include "clicore/clicore.hxx"
#include "core/component.hxx"
#include "clicore/optionparse.hxx"

namespace CPD3 {
namespace CLI {

/**
 * An exception representing a halting condition in parsing the command
 * line arguments.
 */
class CPD3CLICORE_EXPORT ArgumentParsingException {
private:
    QString text;
    bool error;
public:
    ArgumentParsingException(const QString &d, bool e = false);

    /**
     * Get the localized text to output.
     */
    QString getText() const;

    /**
     * Test if the exception is an error or just a information message
     * (e.x. a help message).
     */
    bool isError() const;
};

class CPD3CLICORE_EXPORT ArgumentParser : public QObject {
Q_OBJECT

    bool directTerminal;
public:
    /**
     * Create a new argument parser.
     * 
     * @param toTerminal    if set then output is directed straight to the terminal without using the exception, except to end the processing
     */
    ArgumentParser(bool toTerminal = true);

    virtual ~ArgumentParser();

    void parse(QStringList &arguments,
               ComponentOptions &options,
               const QList<ComponentExample> &examples = QList<ComponentExample>())
    noexcept(false);

    /**
     * A container for help about bare word arguments.
     */
    struct CPD3CLICORE_EXPORT BareWordHelp {
        /** The command line name of the argument. */
        QString name;
        /** A short description of it. */
        QString description;
        /** The default behavior (or empty). */
        QString defaultBehavior;
        /** The detailed help about the argument. */
        QString detailedHelp;

        inline BareWordHelp() : name(), description(), defaultBehavior(), detailedHelp()
        { }

        inline BareWordHelp(const BareWordHelp &other) : name(other.name),
                                                         description(other.description),
                                                         defaultBehavior(other.defaultBehavior),
                                                         detailedHelp(other.detailedHelp)
        { }

        inline BareWordHelp &operator=(const BareWordHelp &other)
        {
            if (&other == this) return *this;
            name = other.name;
            description = other.description;
            defaultBehavior = other.defaultBehavior;
            detailedHelp = other.detailedHelp;
            return *this;
        }

        /**
         * Initialize the help with the given parameters.
         * 
         * @param setName           the name
         * @param setDescription    the description
         * @param setDefaultBehavior the default behavior
         * @param setDetailedHelp   the detailed help
         */
        inline BareWordHelp(const QString &setName,
                            const QString &setDescription,
                            const QString &setDefaultBehavior,
                            const QString &setDetailedHelp) : name(setName),
                                                              description(setDescription),
                                                              defaultBehavior(setDefaultBehavior),
                                                              detailedHelp(setDetailedHelp)
        { }
    };

protected:
    /** The result of a raw override. */
    enum RawOverrideResult {
        /** The argument is passed through to normal handling. */
                RawOverride_PassThrough,
        /** The argument is skipped, but not removed from the list */
                RawOverride_Skip,
        /** The argument is removed from the processing list */
                RawOverride_Consumed,
        /** The argument is removed and processing stops. */
                RawOverride_Terminate,
    };

    /**
     * Called before any more advanced handling of a raw argument. 
     * <br>
     * The default implementation always returns pass through
     * 
     * @param argument  the argument, may be altered as needed
     * @return          the result of the override
     */
    virtual RawOverrideResult rawOverride(QString &argument)
    noexcept(false);

    /**
     * Get the help information about all the bare words, in their
     * output order.
     * <br>
     * The default implementation returns an empty list.
     * 
     * @return the bare word help
     */
    virtual QList<BareWordHelp> getBareWordHelp();

    /**
     * Get the bare words command line trailer.  This should include a
     * space if there are any bare words.
     * <br>
     * The default implementation returns nothing.
     * 
     * @return the bare word command line output
     */
    virtual QString getBareWordCommandLine();

    /**
     * Get the program name used in the help strings.
     * 
     * @return the program name
     */
    virtual QString getProgramName() = 0;

    /**
     * Get the general program description.
     * <br>
     * The default implementation returns nothing.
     * 
     * @return  the general program description
     */
    virtual QString getDescription();

    /**
     * Get an identifier code for the type of documentation.  This is used
     * for automatic documentation generation.
     * <br>
     * The default implementation returns nothing.
     * @return  the documentation type identifier
     */
    virtual QHash<QString, QString> getDocumentationTypeID();

    /**
     * Get the text output after the description.  This should generally
     * state something about "--" terminating argument processing and
     * how bare words are handled.
     * <br>
     * The default implementation returns a short description that states
     * all remaining arguments are treated as bare words.
     * 
     * @param nBareWords    the number of possible bare words
     * @param nArguments    the number of possible arguments
     * @return              the post processing description.
     */
    virtual QString getPostProcessingDescription(int nBareWords, int nArguments);

    /**
     * Output the examples to the terminal with the given mode.
     * 
     * @param examples  the examples
     * @param mode      the mode
     */
    QString generateExamples
            (const QList<ComponentExample> &examples, OptionParse::ExampleMode mode);

    /**
     * Get the text for example output.
     * <br>
     * The default implementation calls outputExamples(
     * const QList<CPD3Core::ComponentExample> &, OptionParse::ExampleMode)
     * with OptionParse::Example_DataFilter and returns an empty string.
     * 
     * @param examples  the examples passed in
     * @return          the example text
     */
    virtual QString getExamples(const QList<ComponentExample> &examples);

private:
#ifdef BUILD_XML
    void outputXMLDescription(ComponentOptions &options, const QList<ComponentExample> &examples);

#endif
};

}
}

#endif
