/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#ifdef Q_OS_UNIX

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <signal.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>

#include <vector>

#endif


#ifdef BUILD_GUI
#include <QGuiApplication>
#include <QApplication>
#else
#include <QCoreApplication>
#endif

#include <QTemporaryFile>
#include <QFile>
#include <QByteArray>
#include <QString>
#include <QStringList>

#include "clicore/graphics.hxx"
#include "core/number.hxx"

using namespace CPD3;

namespace CPD3 {
namespace CLI {

/** @file clicore/graphics.hxx
 * This provides a tools for dealing with graphics interactions from the command line.
 */


#if defined(Q_OS_UNIX) && !defined(Q_OS_OSX)
namespace {

class CleanupXVFB {
public:
    pid_t pid;
    QTemporaryFile *authfile;

    CleanupXVFB() : pid(-1), authfile(NULL)
    { }

    ~CleanupXVFB()
    {
        if (pid != -1) {
            ::kill(pid, SIGTERM);
            pid = -1;
        }
        if (authfile != NULL) {
            delete authfile;
            authfile = NULL;
        }
    }
};

static CleanupXVFB xvfbCleanup;

static int readDisplay(int fd)
{
    QFile file;
    if (!file.open(fd, QIODevice::ReadOnly | QIODevice::Unbuffered))
        return -1;
    QByteArray data(file.readLine(32));
    if (data.isEmpty())
        return -1;
    bool ok = false;
    int n = data.trimmed().toInt(&ok);
    if (!ok || n < 0)
        return -1;
    return n;
}

static bool startAndWrite(const QStringList &args, const QByteArray &data = QByteArray())
{
    int fds[2];
    if (::pipe(fds) != 0) {
        ::perror("pipe");
        return false;
    }

    pid_t child = ::fork();
    if (child < 0) {
        ::perror("fork");
        ::close(fds[0]);
        ::close(fds[1]);
        return false;
    } else if (child == 0) {
        ::close(fds[0]);
        if (::dup2(fds[1], 0) < 0) {
            ::perror("dup2");
            ::_exit(1);
            return false;
        }
        ::close(fds[1]);

        {
            int flags = ::fcntl(0, F_GETFD, 0);
            if (flags < 0) {
                ::perror("fcntl");
                ::_exit(1);
                return false;
            }
            flags &= ~FD_CLOEXEC;
            if (::fcntl(0, F_SETFD, flags) < 0) {
                ::perror("fcntl");
                ::_exit(1);
                return false;
            }
        }

        char **argv = (char **) ::calloc((size_t) args.size() + 1, sizeof(char *));
        for (int i = 0, max = args.size(); i < max; i++) {
            argv[i] = ::strdup(args.at(i).toUtf8());
        }
        argv[args.size()] = NULL;

        ::execvp(argv[0], argv);
        ::_exit(1);
        return false;
    }

    ::close(fds[1]);

    const char *ptr = data.constData();
    size_t remaining = (size_t) data.size();

    while (remaining > 0) {
        ssize_t n = ::write(fds[0], ptr, remaining);
        if (n < 0) {
            ::perror("write");
            break;
        }
        ptr += n;
        remaining -= n;
    }
    ::close(fds[0]);

    ::waitpid(child, NULL, 0);
    return true;
}

static bool startupXVFB()
{
    {
        char *env = ::getenv("CPD3_DISABLEXVFB");
        if (env != NULL && env[0] && atoi(env))
            return false;
    }

    QTemporaryFile *authfile = new QTemporaryFile;
    if (!authfile->open()) {
        delete authfile;
        return false;
    }
    xvfbCleanup.authfile = authfile;

    int fds[2];
    if (::pipe(fds) != 0) {
        ::perror("pipe");
        return false;
    }

    ::unsetenv("DISPLAY");

    pid_t child = ::fork();
    if (child < 0) {
        ::perror("fork");
        ::close(fds[0]);
        ::close(fds[1]);
        return false;
    } else if (child == 0) {
        ::close(fds[0]);
        {
            int flags = ::fcntl(fds[1], F_GETFD, 0);
            if (flags < 0) {
                ::perror("fcntl");
                ::_exit(1);
                return false;
            }
            flags &= ~FD_CLOEXEC;
            if (::fcntl(fds[1], F_SETFD, flags) < 0) {
                ::perror("fcntl");
                ::_exit(1);
                return false;
            }
        }

        int devnull = ::open("/dev/null", O_RDWR);
        if (devnull >= 0) {
            ::dup2(devnull, 0);
            ::dup2(devnull, 1);
            ::dup2(devnull, 2);
            ::close(devnull);
        }

        QStringList args;
        args << "Xvfb";
        args << "-nolisten";
        args << "tcp";
        args << "-auth";
        args << authfile->fileName();
        args << "-displayfd";
        args << QString::number(fds[1]);

        char **argv = (char **) ::calloc((size_t) args.size() + 1, sizeof(char *));
        for (int i = 0, max = args.size(); i < max; i++) {
            argv[i] = ::strdup(args.at(i).toUtf8());
        }
        argv[args.size()] = NULL;

        ::execvp(argv[0], argv);
        ::_exit(1);
        return false;
    }

    ::close(fds[1]);
    xvfbCleanup.pid = child;
    //::setenv("QT_QPA_PLATFORM", "offscreen", 1);

    int display = readDisplay(fds[0]);
    if (display < 0) {
        ::close(fds[0]);
        return false;
    }

    ::setenv("XAUTHORITY", authfile->fileName().toUtf8().constData(), 1);

    QByteArray cookie(Random::data(16).toHex());

    {
        QByteArray command("add :");
        command.append(QByteArray::number(display));
        command.append(". ");
        command.append(cookie);
        command.append('\n');
        startAndWrite(QStringList("xauth") << "source" << "-", command);
    }

    QByteArray disp(":");
    disp.append(QByteArray::number(display));
    ::setenv("DISPLAY", disp.constData(), 1);

    ::close(fds[0]);
    return true;
}

}

#endif


QCoreApplication *CLIGraphics::createApplication(int &argc, char **argv)
{
#if defined(Q_OS_UNIX)
    {
        char *env = ::getenv("CPD3_DISABLEGRAPHICS");
        if (env != NULL && env[0] && atoi(env))
            return new QCoreApplication(argc, argv);
    }
#endif

#ifndef BUILD_GUI
    return new QCoreApplication(argc, argv);
#else

#if defined(Q_OS_UNIX) && !defined(Q_OS_OSX)
    {
        char *env = ::getenv("DISPLAY");
        if (env == NULL || !env[0]) {
            if (!startupXVFB()) {
                return new QCoreApplication(argc, argv);
            }
        }
    }
#endif

    return new QGuiApplication(argc, argv);
#endif
}

}
}