/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <unistd.h>
#include <stdio.h>

#ifdef Q_OS_LINUX

#include <sys/ioctl.h>

#endif

#include <QRegExp>
#include <QFile>

#include "clicore/terminaloutput.hxx"
#include "datacore/externalsink.hxx"

using namespace CPD3;

namespace CPD3 {
namespace CLI {

int TerminalOutput::terminalWidth()
{
#ifdef Q_OS_LINUX
    {
        struct winsize w;
        if (ioctl(0, TIOCGWINSZ, &w) == 0) {
            return w.ws_col;
        }
    }
#endif
    {
        QByteArray colEnv(qgetenv("COLUMNS"));
        if (colEnv.length() > 0) {
            int n = atoi(colEnv.data());
            if (n > 0)
                return n;
        }
    }
    return 80;
}

int TerminalOutput::terminalWidth(QIODevice *device)
{
    QFile *file = qobject_cast<QFile *>(device);
    if (file) {
        int fd = file->handle();
        if (fd != -1) {
#ifdef Q_OS_LINUX
            {
                struct winsize w;
                if (ioctl(fd, TIOCGWINSZ, &w) == 0) {
                    return w.ws_col;
                }
            }
#endif
            if (::isatty(fd)) {
                QByteArray colEnv(qgetenv("COLUMNS"));
                if (colEnv.length() > 0) {
                    int n = atoi(colEnv.data());
                    if (n > 0)
                        return n;
                }
            }
        }
    }
    return 80;
}

int TerminalOutput::terminalHeight()
{
#ifdef Q_OS_LINUX
    {
        struct winsize w;
        if (ioctl(0, TIOCGWINSZ, &w) == 0) {
            return w.ws_row;
        }
    }
#endif
    {
        QByteArray rowEnv(qgetenv("ROWS"));
        if (rowEnv.length() > 0) {
            int n = atoi(rowEnv.data());
            if (n > 0)
                return n;
        }
    }
    return 24;
}

int TerminalOutput::terminalHeight(QIODevice *device)
{
    QFile *file = qobject_cast<QFile *>(device);
    if (file) {
        int fd = file->handle();
        if (fd != -1) {
#ifdef Q_OS_LINUX
            {
                struct winsize w;
                if (ioctl(fd, TIOCGWINSZ, &w) == 0) {
                    return w.ws_row;
                }
            }
#endif
            if (::isatty(fd)) {
                QByteArray rowEnv(qgetenv("ROWS"));
                if (rowEnv.length() > 0) {
                    int n = atoi(rowEnv.data());
                    if (n > 0)
                        return n;
                }
            }
        }
    }
    return 24;
}

void TerminalOutput::output(const QString &string, bool toStderr)
{
    QByteArray data(string.toLocal8Bit());
    FILE *target;
    if (toStderr)
        target = stderr;
    else
        target = stdout;
    size_t total = strlen(data.constData());
    for (size_t n = 0; n < total;) {
        size_t add = fwrite(data.constData() + n, 1, total - n, target);
        n += add;
    }
}


static QString sanitizeOutput(const QString &text)
{
#ifdef Q_OS_LINUX
    /* Bit of a hack here, but we want nice printable strings for
     * the GUI and to make it sane for the terminal */
    QByteArray termLocale(qgetenv("LANG"));
    if (termLocale.isEmpty() || termLocale == "C") {
        QString result(text);
        Data::ExternalSink::simplifyUnicode(result);
        return result;
    }
#endif
    return text;
}


void TerminalOutput::applyWordWrap(const QString &text,
                                   bool toStderr,
                                   int leadSpace,
                                   int columnWidth)
{
    QString working(sanitizeOutput(text));

    if (columnWidth <= 0) {
        columnWidth = terminalWidth() - 1;
        if (columnWidth <= 0)
            columnWidth = 79;
    }

    bool first = true;
    for (;;) {
        if (!first)
            working = working.trimmed();
        if (working.length() == 0) {
            if (first)
                output(QString('\n'), toStderr);
            return;
        }
        int consumedColumns = 0;
        if (first) {
            first = false;
            if (leadSpace < 0) {
                leadSpace = -leadSpace;
                consumedColumns = leadSpace;
            }
        } else if (leadSpace > 0) {
            output(QString(' ').repeated(leadSpace), toStderr);
            consumedColumns = leadSpace;
        }
        if (working.length() <= columnWidth - consumedColumns || columnWidth <= consumedColumns) {
            output(working, toStderr);
            output(QString('\n'), toStderr);
            return;
        }
        QString fitText(working.mid(0, columnWidth - consumedColumns));
        int lastSpace = fitText.lastIndexOf(QRegExp("\\s+"));
        if (lastSpace != -1) {
            fitText = fitText.mid(0, lastSpace + 1);
        }
        working = working.mid(fitText.length());

        output(fitText, toStderr);
        output(QString('\n'), toStderr);
    }
}

void TerminalOutput::simpleWordWrap(const QString &text, bool toStderr)
{
    QString working(sanitizeOutput(text));

    for (;;) {
        int newline = working.indexOf('\n');
        if (newline == -1) {
            applyWordWrap(working, toStderr);
            return;
        } else if (newline == 0) {
            output(QString('\n'), toStderr);
            working = working.mid(1);
            continue;
        }
        applyWordWrap(working.mid(0, newline), toStderr);
        working = working.mid(newline + 1);
    }
}

TerminalProgress::TerminalProgress() : isATerminal(true),
                                       progressShown(false), incoming(),
                                       maximumProgressLineLength(0),
                                       progressSpinModulus(0),
                                       throbber(NULL)
{
    int fd = fileno(stdout);
    if (fd >= 0)
        isATerminal = ::isatty(fd);

    expectedBounds.start = FP::undefined();
    expectedBounds.end = FP::undefined();
}

TerminalProgress::~TerminalProgress()
{
    incoming.updated.disconnect();
}

void TerminalProgress::setTimeBounds(const Time::Bounds &bounds)
{
    this->expectedBounds = bounds;
}

void TerminalProgress::setTimeBounds(double start, double end)
{
    this->expectedBounds.setStart(start);
    this->expectedBounds.setEnd(end);
}

void TerminalProgress::attach(ActionFeedback::Source &source)
{ incoming.attach(source); }

static void notATerminalOutput(const QString &str)
{
    QString outputLine(TerminalProgress::tr("%1: %2\n", "non terminal format").arg(
            Time::toISO8601(Time::time()), str));
    TerminalOutput::output(sanitizeOutput(outputLine));
    fflush(stdout);
}

void TerminalProgress::start(bool enableAutoUpdate)
{
    incoming.advanced.disconnect();

    progressShown = true;

    if (!isATerminal) {
        writeLine();
        incoming.advanced.connect(this, "outputProgressLine");
        maximumProgressLineLength = 0;
        return;
    }

    TerminalOutput::output(QString("\n\n"));
    if (enableAutoUpdate) {
        throbber = new QTimer(this);
        connect(throbber, SIGNAL(timeout()), this, SLOT(outputProgressLine()));
        throbber->start(100);
    }

    writeLine();
    maximumProgressLineLength = 0;
}

void TerminalProgress::finished()
{
    if (throbber != NULL) {
        throbber->stop();
        throbber->deleteLater();
        throbber = NULL;
    }
    incoming.updated.disconnect();

    if (!progressShown)
        return;
    progressShown = false;
    writeLine(Complete);
}

void TerminalProgress::abort()
{
    if (throbber != NULL) {
        throbber->stop();
        throbber->deleteLater();
        throbber = NULL;
    }
    incoming.updated.disconnect();

    if (!progressShown)
        return;
    progressShown = false;
    writeLine(Abort);
}

void TerminalProgress::writeLine(LineFinish finish)
{
    for (const auto &s : incoming.process()) {
        auto state = s.state();

        if (!isATerminal) {
            QString title = s.title();
            if (title.isEmpty())
                title = tr("Initialize");

            switch (s.state()) {
            case ActionFeedback::Serializer::Stage::Spin:
            case ActionFeedback::Serializer::Stage::Progress:
                switch (finish) {
                case Active:
                    notATerminalOutput(tr("START: %1", "non terminal start").arg(title));
                    break;
                case Complete:
                    notATerminalOutput(tr("COMPLETE: %1", "non terminal complete").arg(title));
                    break;
                case Abort:
                    notATerminalOutput(tr("ABORT: %1", "non terminal abort").arg(title));
                    break;
                }
                break;
            case ActionFeedback::Serializer::Stage::Complete:
                notATerminalOutput(tr("COMPLETE: %1", "non terminal complete").arg(title));
                break;
            case ActionFeedback::Serializer::Stage::Failure: {
                auto reason = s.failureReason();
                QString outputLine;
                if (!reason.isEmpty()) {
                    outputLine =
                            tr("FAILED: %1 [%2]", "non terminal failure").arg(s.title(), reason);
                } else {
                    outputLine = tr("FAILED: %1", "non terminal failure").arg(s.title());
                }
                notATerminalOutput(outputLine);
                break;
            }
            }
            continue;
        }

        if (state == ActionFeedback::Serializer::Stage::Failure) {
            QString outputLine;
            auto reason = s.failureReason();
            if (!reason.isEmpty()) {
                outputLine = tr("   FAILED: %1 [%2]", "failure base").arg(s.title(), reason);
            } else {
                outputLine = tr("   FAILED: %1", "failure base").arg(s.title());
            }
            outputLine = outputLine.leftJustified(maximumProgressLineLength, ' ');
            outputLine.prepend('\r');
            outputLine.append('\n');
            TerminalOutput::output(sanitizeOutput(outputLine));
            ::fflush(stdout);
            continue;
        }

        if (state == ActionFeedback::Serializer::Stage::Complete || finish != Active) {
            QString outputLine;
            if (finish == Abort) {
                outputLine = tr("  ABORTED: %1", "aborted base").arg(s.title());
            } else {
                outputLine = tr("COMPLETED: %1", "completed base").arg(s.title());
            }
            outputLine = outputLine.leftJustified(maximumProgressLineLength, ' ');
            outputLine.prepend('\r');
            outputLine.append('\n');
            TerminalOutput::output(sanitizeOutput(outputLine));
            ::fflush(stdout);
            continue;
        }

        QString spinnerCharacters(tr("-\\|/", "spinner"));
        QChar spin(spinnerCharacters.at(progressSpinModulus % spinnerCharacters.length()));

        int terminalWidth = TerminalOutput::terminalWidth() - 1;
        if (terminalWidth <= 0)
            terminalWidth = 79;


        QString title = s.title();
        if (title.isEmpty())
            title = tr("Initializing");

        QString line = tr("   STATUS: %1", "status base").arg(title);

        double fraction = s.toFraction(expectedBounds.start, expectedBounds.end);
        if (!FP::defined(fraction)) {
            auto status = s.stageStatus();
            if (status.length() > 0) {
                QString add = tr(" (%1)", "status text").arg(status);
                if (line.length() + add.length() <= terminalWidth) {
                    line.append(add);
                }
            }

            double time = s.time();

            if (FP::defined(time)) {
                QString add = tr(" ... %1 %2", "time suffix").arg(Time::toISO8601(time), spin);
                if (line.length() + add.length() <= terminalWidth) {
                    line.append(add);
                }
            } else {
                QString add = tr(" ... %1", "throb suffix").arg(spin);
                if (line.length() + add.length() <= terminalWidth) {
                    line.append(add);
                }
            }
        } else {
            QString pct = QString::number(fraction * 100.0, 'f', 2);
            pct = pct.rightJustified(6, ' ');
            QString suffix = tr("%1% %2", "progress percent suffix").arg(pct, spin);

            int totalBars = terminalWidth - (line.length() + suffix.length() + 4);
            if (totalBars <= 0) {
                line.append(' ');
                line.append(suffix);

                auto status = s.stageStatus();
                if (status.length() > 0) {
                    QString add = tr(" (%1)", "status text").arg(status);
                    if (line.length() + add.length() <= terminalWidth) {
                        line.append(add);
                    }
                }
            } else {
                auto status = s.stageStatus();
                if (status.length() > 0) {
                    QString add = tr(" (%1)", "status text").arg(status);

                    if (totalBars - add.length() > 1) {
                        line.append(add);
                        totalBars -= add.length();
                    }
                }

                line.append(" [");

                int filledBars = (int) ::floor((double) totalBars * fraction);
                line.append(QString('=').repeated(filledBars));
                if (fraction < 1.0 && qRound((double) totalBars * fraction) > filledBars) {
                    line.append('-');
                    totalBars--;
                }
                totalBars -= filledBars;
                if (totalBars > 0)
                    line.append(QString(' ').repeated(totalBars));

                line.append("] ");
                line.append(suffix);
            }
        }

        if (line.length() > terminalWidth)
            line.truncate(terminalWidth);

        int len = line.length();
        if (len < maximumProgressLineLength) {
            line = line.leftJustified(maximumProgressLineLength, ' ');
        } else {
            maximumProgressLineLength = len;
        }
        line.prepend('\r');
        TerminalOutput::output(sanitizeOutput(line));
        ::fflush(stdout);
    }
}

void TerminalProgress::outputProgressLine()
{
    if (!progressShown)
        return;

    progressSpinModulus++;
    writeLine();
}

}
}
