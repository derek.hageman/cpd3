/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3CLICORETERMINALOUTPUT_H
#define CPD3CLICORETERMINALOUTPUT_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QTimer>
#include <QIODevice>

#include "clicore/clicore.hxx"
#include "core/timeutils.hxx"
#include "core/number.hxx"
#include "core/actioncomponent.hxx"

namespace CPD3 {
namespace CLI {

/**
 * Provides utilities to parse archive specifications from the command line.
 */
class CPD3CLICORE_EXPORT TerminalOutput : public QObject {
Q_OBJECT
public:
    /**
     * Get the width of the terminal.
     * 
     * @return the number of columns in the terminal
     */
    static int terminalWidth();

    /**
     * Get the width of the terminal as an output device.
     * 
     * @param device    the device that might be a terminal
     * @return          the number of columns in the terminal
     */
    static int terminalWidth(QIODevice *device);

    /**
     * Get the height of the terminal.
     * 
     * @return the number of rows in the terminal
     */
    static int terminalHeight();

    /**
     * Get the height of the terminal as an output device.
     * 
     * @param device    the device that might be a terminal
     * @return          the number of rows in the terminal
     */
    static int terminalHeight(QIODevice *device);

    /**
     * Output a string to the terminal. 
     * 
     * @param string    the string to output
     * @param toStderr  if true then output to standard error
     */
    static void output(const QString &string, bool toStderr = false);

    /**
     * Outputs a string to the terminal with word wrapping.
     * 
     * @param text      the text to output
     * @param toStderr  if true then output to standard error
     * @param leadSpace the amount of spaces prepended before any output after the first line when less than zero this is also considered for the first line
     * @param columnWidth the length of the column before wrapping (including leading spaces), if negative then use the terminal width
     */
    static void applyWordWrap
            (const QString &text, bool toStderr = false, int leadSpace = 0, int columnWidth = -1);

    /**
     * Apply simple worp wrapping to the given text.  This includes "smart"
     * handling of newlines such that they can be used to force text
     * block spacing.
     * 
     * @param text      the text to output
     * @param toStderr  if true then output to standard error
     */
    static void simpleWordWrap(const QString &text, bool toStderr = false);
};

/**
 * A class that handles progress displays on a terminal.
 */
class CPD3CLICORE_EXPORT TerminalProgress : public QObject {
Q_OBJECT

    bool isATerminal;
    bool progressShown;
    ActionFeedback::Serializer incoming;
    int maximumProgressLineLength;
    int progressSpinModulus;
    QTimer *throbber;
    Time::Bounds expectedBounds;

    enum LineFinish {
        Active, Complete, Abort,
    };

    void writeLine(LineFinish finish = Active);

public:
    TerminalProgress();

    virtual ~TerminalProgress();

    /**
     * Set the time bounds to try interpret times with.
     * 
     * @param bounds    the time bounds
     */
    void setTimeBounds(const Time::Bounds &bounds);

    /**
     * Attach the progress to a given source.
     *
     * @param source    the feedback source
     */
    void attach(ActionFeedback::Source &source);

public slots:

    /**
     * Set the time bounds to try interpret times with.
     * 
     * @param start     the start time
     * @param end       the end time
     */
    void setTimeBounds(double start, double end);

    /**
     * Begin the progress output.  This is used to begin the progress stage
     * display.
     * 
     * @param enableAutoUpdate if set then outputProgressLine() is called automatically
     */
    void start(bool enableAutoUpdate = true);

    /**
     * Output the finished state of the progress.  This is used when the
     * progress operation has completed successfully.
     */
    void finished();

    /**
     * Output the aborted message of the progress.  This is used when the
     * progress operation was aborted before completion.
     */
    void abort();

    /**
     * Output the progress line.
     */
    void outputProgressLine();
};

}
}

#endif
