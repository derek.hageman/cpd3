/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATABASE_CONTEXT_HXX
#define CPD3DATABASE_CONTEXT_HXX

#include "core/first.hxx"

#include <memory>
#include <queue>
#include <mutex>
#include <cstdint>
#include <condition_variable>
#include <QString>

#include "database.hxx"
#include "util.hxx"
#include "core/threading.hxx"

namespace CPD3 {
namespace Database {

class ContextInterface;

class Statement;

/**
 * An executing statement context.
 */
class CPD3DATABASE_EXPORT Context final {
    std::shared_ptr<ContextInterface> interface;

    friend class Statement;

    explicit Context(std::shared_ptr<ContextInterface> &&interface);

public:
    Context();

    ~Context();

    Context(Context &&other);

    Context &operator=(Context &&other);

    Context(const Context &other);

    Context &operator=(const Context &other);

    /**
     * The signal generated from the database thread when a result is read.
     */
    Threading::Signal<const Types::Results &> result;

    /**
     * The signal generated from the database thread when the execution of the
     * context finishes, either because of an end request, exhausting results
     * or from an error.
     */
    Threading::Signal<> complete;


    /**
     * Start the context execution.  Signals should be connected before this is called.
     *
     * @param read      the number of results to try and read initially
     */
    void begin(std::uint_fast32_t read = 1);

    /**
     * Advance the context and produce the next result (or end).
     */
    void next();

    /**
     * Complete the context.
     */
    void finish();

    /**
     * Wait for context completion.
     */
    void wait();

    /**
     * Fetch all results from an un-started context.
     *
     * @return  all results produced by the context
     */
    std::vector<Types::Results> all();

    class Iterator;

    class Guard;
};

/**
 * A simple iterator over all results produced by the context.  The context
 * must not have been started yet.
 */
class CPD3DATABASE_EXPORT Context::Iterator {
    bool isFinished;
    Context context;
    Threading::Receiver receiver;
    std::mutex mutex;
    std::condition_variable cv;
    std::queue<Types::Results> results;

    static constexpr std::size_t prefetch = 4;

    void begin();

    void contextResult(const Types::Results &data);

    void contextComplete();

public:
    Iterator();

    /**
     * Construct the iterator and start iteration.
     *
     * @param context   the context
     */
    explicit Iterator(Context &&context);

    /**
     * Construct the iterator and start iteration.
     *
     * @param context   the context
     */
    explicit Iterator(Context &context);

    ~Iterator();

    Iterator(const Iterator &) = delete;

    Iterator &operator=(const Iterator &) = delete;

    /**
     * Test if the iterator has next values, waiting if required.
     *
     * @return  true if there is a next result
     */
    bool hasNext();

    /**
     * Get the next results from the iterator, waiting if required.
     *
     * @return  get the next results
     */
    Types::Results next();
};

/**
 * A simple guard that starts the context and ensures it is complete before
 * allowing itself to be destroyed.  This starts the guard then immediately
 * finishes it.
 */
class CPD3DATABASE_EXPORT Context::Guard {
    Context context;
public:
    Guard();

    Guard(const Guard &other) = delete;

    Guard(Guard &&other);

    Guard &operator=(const Guard &other) = delete;

    Guard &operator=(Guard &&other);

    /**
    * Construct the guard and start the context.
    *
    * @param context   the context
    */
    explicit Guard(Context &&context);

    /**
     * Construct the guard and start the context.
     *
     * @param context   the context
     */
    explicit Guard(Context &context);

    ~Guard();

    /**
     * For for context completion immediately, instead of when the guard is destroyed.
     */
    void wait();
};

}
}

#endif //CPD3DATABASE_CONTEXT_HXX
