/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <chrono>

#include "connection.hxx"
#include "driver.hxx"
#include "interface.hxx"
#include "tabledefinition.hxx"
#include "core/timeutils.hxx"
#include "core/waitutils.hxx"


namespace CPD3 {
namespace Database {

Connection::Connection(const Storage &d) : db(d), interface(std::make_shared<ConnectionInterface>())
{ }

Connection::Connection(Storage &&d) : db(std::move(d)),
                                      interface(std::make_shared<ConnectionInterface>())
{ }

Connection::~Connection()
{
    signalTerminate();
    wait();
}

bool Connection::start()
{
    {
        std::lock_guard<std::mutex> lock(interface->mutex);
        if (interface->threadState != ConnectionInterface::Thread_Initialize)
            return false;
    }
    if (!db.open(std::bind(&ConnectionInterface::run, interface, std::placeholders::_1))) {
        std::vector<std::unique_ptr<BaseRelease>> toRelease;
        {
            std::lock_guard<std::mutex> lock(interface->mutex);
            interface->threadState = ConnectionInterface::Thread_Complete;
            toRelease = std::move(interface->releaseQueue);
            interface->releaseQueue.clear();
        }
        toRelease.clear();
        return false;
    }

    std::unique_lock<std::mutex> lock(interface->mutex);
    interface->response.wait(lock, [this] {
        return interface->threadState != ConnectionInterface::Thread_Initialize;
    });
    return true;
}

bool Connection::isFinished()
{ return interface->isFinished(); }

bool Connection::wait(double timeout)
{ return interface->wait(timeout); }

void Connection::shutdown(bool waitForTransaction)
{ return interface->shutdown(waitForTransaction); }

void Connection::signalTerminate()
{ return shutdown(false); }

static bool transactionRetryWait(int tc, double timeOfEnd)
{
    if (!FP::defined(timeOfEnd)) {
        Wait::backoffWait(tc);
        return true;
    }
    double now = Time::time();
    double remaining = timeOfEnd - now;
    if (remaining <= 0.0) {
        return false;
    }
    Wait::backoffWait(tc, now, remaining);
    return true;
}

bool Connection::synchronousTransaction(const std::function<SynchronousResult()> &exec,
                                        int flags,
                                        double timeout)
{
    double timeOfEnd = FP::undefined();
    if (FP::defined(timeout))
        timeOfEnd = Time::time() + timeout;

    for (int tc = 0;; ++tc) {
        double remaining = FP::undefined();
        if (FP::defined(timeOfEnd))
            remaining = timeOfEnd - Time::time();

        Transaction tx(*this, flags, remaining);
        if (!tx.isValid())
            return false;

        switch (exec()) {
        case Synchronous_Ok:
            break;
        case Synchronous_Failed:
            tx.abort();
            break;
        case Synchronous_Abort:
            tx.abort();
            return false;
        }

        if (tx.end())
            return true;
        if (!transactionRetryWait(tc, timeOfEnd))
            return false;
        std::lock_guard<std::mutex> lock(interface->mutex);
        if (!interface->canOperate())
            return false;
    }

    return false;
}

void Connection::requiredTransaction(const std::function<RequiredResult()> &exec, int flags)
{
    for (int tc = 0;; ++tc) {
        Transaction tx(*this);
        if (!tx.isValid()) {
            qFatal("Cannot proceed in required transaction");
            return;
        }
        switch (exec()) {
        case Required_Ok:
            break;
        case Required_Failed:
            tx.abort();
            break;
        }
        if (tx.end())
            break;
        Wait::backoffWait(tc);
    }
}

void Connection::createTable(const TableDefinition &table)
{
    interface->runOnTransaction([this, &table](Driver *driver) {
        return driver->createTable(table);
    });
}

void Connection::removeTable(const std::string &table)
{
    interface->runOnTransaction([this, &table](Driver *driver) {
        return driver->removeTable(table);
    });
}

Statement Connection::general(const std::string &statement, std::size_t outputs)
{
    return interface->createStatement([this, &statement, outputs](Driver *driver) {
        return new StatementInterface(interface, driver->general(statement, outputs));
    });
}

Statement Connection::select(const Types::TableSet &from,
                             const Types::Outputs &select,
                             const std::string &where,
                             const std::string &order)
{
    return interface->createStatement([this, &from, &select, &where, &order](Driver *driver) {
        return new StatementInterface(interface, driver->select(from, select, where, order));
    });
}

Statement Connection::insert(const std::string &table, const Types::ColumnSet &columns)
{
    return interface->createStatement([this, &table, &columns](Driver *driver) {
        return new StatementInterface(interface, driver->insert(table, columns));
    });
}

Statement Connection::insertSelect(const std::string &table,
                                   const Types::Outputs &columns,
                                   const Types::TableSet &from,
                                   const Types::Outputs &select,
                                   const std::string &where,
                                   const std::string &order)
{
    return interface->createStatement(
            [this, &table, &columns, &from, &select, &where, &order](Driver *driver) {
                return new StatementInterface(interface,
                                              driver->insertSelect(table, columns, from, select,
                                                                   where, order));
            });
}

Statement Connection::merge(const std::string &table,
                            const Types::ColumnSet &keys,
                            const Types::ColumnSet &values)
{
    return interface->createStatement([this, &table, &keys, &values](Driver *driver) {
        return new StatementInterface(interface, driver->merge(table, keys, values));
    });
}

Statement Connection::merge(const std::string &table, const Types::ColumnSet &columns)
{
    return interface->createStatement([this, &table, &columns](Driver *driver) {
        return new StatementInterface(interface, driver->merge(table, columns));
    });
}

Statement Connection::update(const std::string &table,
                             const Types::ColumnSet &columns,
                             const std::string &where)
{
    return interface->createStatement([this, &table, &columns, &where](Driver *driver) {
        return new StatementInterface(interface, driver->update(table, columns, where));
    });
}


Statement Connection::del(const std::string &table, const std::string &where)
{
    return interface->createStatement([this, &table, &where](Driver *driver) {
        return new StatementInterface(interface, driver->del(table, where));
    });
}

std::unordered_set<std::string> Connection::tables()
{
    std::unordered_set<std::string> t;
    interface->runOnTransaction([&t](Driver *driver) {
        t = driver->loadTables();
        return true;
    });
    return t;
}


Connection::Transaction::Transaction() = default;

Connection::Transaction::Transaction(Connection::Transaction &&other) = default;

Connection::Transaction &Connection::Transaction::operator=(Connection::Transaction &&other) = default;

Connection::Transaction::Transaction(Connection &connection, int flags, double timeout) : active(
        TransactionInterface::acquire(*connection.interface, flags, timeout))
{ }

Connection::Transaction::~Transaction()
{
    auto activeTransaction = active.lock();
    if (!activeTransaction)
        return;
    activeTransaction->release();
}

bool Connection::Transaction::isValid() const
{ return !active.expired(); }

bool Connection::Transaction::inError() const
{
    auto activeTransaction = active.lock();
    if (!activeTransaction)
        return true;
    return activeTransaction->inError();
}

void Connection::Transaction::abort()
{
    auto activeTransaction = active.lock();
    if (!activeTransaction)
        return;
    active.reset();
    activeTransaction->abort();
}

bool Connection::Transaction::end()
{
    auto activeTransaction = active.lock();
    if (!activeTransaction)
        return false;
    active.reset();
    return activeTransaction->endTransaction();
}

}
}
