/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATABASE_STORAGE_HXX
#define CPD3DATABASE_STORAGE_HXX

#include "core/first.hxx"

#include <cstdint>
#include <memory>
#include <functional>
#include <thread>
#include <string>

#include "database/database.hxx"

class QSqlDatabase;

namespace CPD3 {
namespace Database {

class Driver;

/**
 * A definition of database backend storage.
 */
class CPD3DATABASE_EXPORT Storage final {

    class Type;

    std::shared_ptr<Type> type;

    explicit Storage(std::shared_ptr<Type> &&type);
public:
    Storage(const Storage &other);

    Storage &operator=(const Storage &other);

    Storage(Storage &&other);

    Storage &operator=(Storage &&other);

    Storage() = delete;

    /**
     * Create a storage interface for the given Qt database.
     *
     * @param qt    the Qt database definition
     */
    explicit Storage(const QSqlDatabase &qt);

    /**
     * Create a storage interface for a SQLite database.
     *
     * @param filename  the database file
     * @return          the storage interface
     */
    static Storage sqlite(const std::string &filename);

    /**
     * Create a storage definition for a SQLite database in memory.
     *
     * @return          the storage interface
     */
    static Storage sqlite();

    /**
     * Create storage definition for a MySQL/MariaDB database.
     *
     * @param server    the server name
     * @param db        the database name
     * @param username  the username
     * @param password  the password
     * @param port      the server port
     * @return          the storage definition
     */
    static Storage mysql(const std::string &server,
                         const std::string &db,
                         const std::string &username = std::string(),
                         const std::string &password = std::string(),
                         std::uint16_t port = 0);

    /**
     * Create storage definition for a PostgreSQL database.
     *
     * @param server    the server name
     * @param db        the database name
     * @param username  the username
     * @param password  the password
     * @param port      the server port
     * @return          the storage definition
     */
    static Storage postgresql(const std::string &server,
                              const std::string &db,
                              const std::string &username = std::string(),
                              const std::string &password = std::string(),
                              std::uint16_t port = 0);

    /**
     * Create a storage interface for the given Qt database.
     *
     * @param db        the Qt database definition
     * @return          the storage definition
     */
    static Storage qt(const QSqlDatabase &db);

    /**
     * Open the storage interface.  This operates by starting a thread
     * and a backend driver on that thread, then calling the specified
     * function to actually perform database operations on the newly
     * created thread.  The thread exits once the function returns.
     * <br>
     * An implementation of the thread can and should release the
     * database driver before exiting, so it doesn't race against
     * library unload.
     *
     * @param thread    the function run on the database thread
     * @return          true on success
     */
    bool open(const std::function<void(std::unique_ptr<Driver> &)> &thread);
};

}
}


#endif //CPD3DATABASE_STORAGE_HXX
