/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <cstdlib>
#include <QUuid>
#include <QSettings>
#include <QDir>
#include <QLoggingCategory>
#include <QStandardPaths>

#include "util.hxx"
#include "core/qtcompat.hxx"


Q_LOGGING_CATEGORY(log_database_util, "cpd3.database.util", QtWarningMsg)


namespace CPD3 {
namespace Database {


Storage Util::getDefaultDatabase()
{
    QByteArray p(qgetenv("CPD3ARCHIVE"));
    if (p.isEmpty())
        p = qgetenv("CPD3_ARCHIVE");

    if (!p.isEmpty()) {
        QString dbname(p);
        if (dbname.startsWith("mysql:", Qt::CaseInsensitive)) {
            QString host = dbname.mid(6);
            if (host.length() > 0) {
                std::string server(host.toLatin1().constData());
                std::string db;
                std::string username;
                std::string password;
                std::uint16_t port = 0;

                p = qgetenv("CPD3ARCHIVEPORT");
                if (p.isEmpty())
                    p = qgetenv("CPD3_ARCHIVEPORT");
                if (!p.isEmpty())
                    port = (std::uint16_t) std::atoi(p.constData());

                p = qgetenv("CPD3ARCHIVEDBNAME");
                if (p.isEmpty())
                    p = qgetenv("CPD3_ARCHIVEDBNAME");
                if (!p.isEmpty())
                    db = p.constData();

                p = qgetenv("CPD3ARCHIVEUSERNAME");
                if (p.isEmpty())
                    p = qgetenv("CPD3_ARCHIVEUSERNAME");
                if (!p.isEmpty())
                    username = p.constData();

                p = qgetenv("CPD3ARCHIVEPASSWORD");
                if (p.isEmpty())
                    p = qgetenv("CPD3_ARCHIVEPASSWORD");
                if (!p.isEmpty())
                    password = p.constData();

                qCDebug(log_database_util) << "Selected MySQL database:" << server << ":" << db
                                           << ":"
                                           << username;
                return Storage::mysql(server, db, username, password, port);
            }
        } else if (dbname.startsWith("psql:", Qt::CaseInsensitive)) {
            QString host = dbname.mid(5);
            if (host.length() > 0) {
                std::string server(host.toStdString());
                std::string db;
                std::string username;
                std::string password;
                std::uint16_t port = 0;

                p = qgetenv("CPD3ARCHIVEPORT");
                if (p.isEmpty())
                    p = qgetenv("CPD3_ARCHIVEPORT");
                if (!p.isEmpty())
                    port = (std::uint16_t) std::atoi(p.constData());

                p = qgetenv("CPD3ARCHIVEDBNAME");
                if (p.isEmpty())
                    p = qgetenv("CPD3_ARCHIVEDBNAME");
                if (!p.isEmpty())
                    db = p.constData();

                p = qgetenv("CPD3ARCHIVEUSERNAME");
                if (p.isEmpty())
                    p = qgetenv("CPD3_ARCHIVEUSERNAME");
                if (!p.isEmpty())
                    username = p.constData();

                p = qgetenv("CPD3ARCHIVEPASSWORD");
                if (p.isEmpty())
                    p = qgetenv("CPD3_ARCHIVEPASSWORD");
                if (!p.isEmpty())
                    password = p.constData();

                qCDebug(log_database_util) << "Selected PostgreSQL database:" << server << ":" << db
                                           << ":" << username;
                return Storage::postgresql(server, db, username, password, port);
            }
        } else if (dbname.startsWith("sqlite:", Qt::CaseInsensitive)) {
            QString filename = dbname.mid(7);
            if (filename == "_") {
                return Storage::sqlite();
            }
            if (filename.length() > 0) {
                qCDebug(log_database_util) << "Selected SQLite database" << filename;
                return Storage::sqlite(filename.toStdString());
            }
        }
    }


    {
        QSettings settings("NOAA ESRL GMD", "CPD3Default");
        QString type(settings.value("db/type").toString().toLower());
        if (type == "mysql") {
            std::string server(settings.value("db/hostname", "localhost").toString().toStdString());
            std::string db(settings.value("db/database").toString().toStdString());
            std::string username(settings.value("db/username").toString().toStdString());
            std::string password(settings.value("db/password").toString().toStdString());
            std::uint16_t port = 0;
            if (settings.contains("db/port")) {
                bool ok = false;
                port = static_cast<std::uint16_t>(settings.value("db/port").toUInt(&ok));
                if (!ok)
                    port = 0;
            }
            qCDebug(log_database_util) << "Selected MySQL database:" << server << ":" << db << ":"
                                       << username;
            return Storage::mysql(server, db, username, password, port);
        } else if (type == "psql") {
            std::string server(settings.value("db/hostname", "localhost").toString().toStdString());
            std::string db(settings.value("db/database").toString().toStdString());
            std::string username(settings.value("db/username").toString().toStdString());
            std::string password(settings.value("db/password").toString().toStdString());
            std::uint16_t port = 0;
            if (settings.contains("db/port")) {
                bool ok = false;
                port = static_cast<std::uint16_t>(settings.value("db/port").toUInt(&ok));
                if (!ok)
                    port = 0;
            }
            qCDebug(log_database_util) << "Selected PostgreSQL database: " << server << ":" << db
                                       << ":"
                                       << username;
            return Storage::postgresql(server, db, username, password, port);
        } else {
            QString filename(settings.value("db/filename").toString());
            if (!filename.isEmpty()) {
                qCDebug(log_database_util) << "Selected SQLite database " << filename;
                return Storage::sqlite(filename.toStdString());
            }
        }
    }

    p = qgetenv("CPD3");
    if (!p.isEmpty()) {
        QDir path(p);
        if (path.exists()) {
            auto filename = path.filePath("cpd3.db").toStdString();
            qCDebug(log_database_util) << "Selected default SQLite database" << filename;
            return Storage::sqlite(filename);
        }
    }

    auto dir = QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation);
    if (!dir.isEmpty()) {
        QDir path(dir);
        if (path.exists()) {
            auto filename = path.filePath("cpd3.db").toStdString();
            qCDebug(log_database_util) << "Selected default SQLite database" << filename;
            return Storage::sqlite(filename);
        }
    }

    std::string filename = "cpd3.db";
    qCDebug(log_database_util) << "Selected fallback SQLite database" << filename;
    return Storage::sqlite(filename);
}


namespace Types {

Variant::Variant() : storedType(Type::Empty)
{}

Variant::~Variant()
{
    switch (storedType) {
    case Type::Empty:
    case Type::Null:
    case Type::Integer:
    case Type::Real:
        break;
    case Type::String:
        storedData.destruct<std::string>();
        break;
    case Type::Bytes:
        storedData.destruct<Bytes>();
        break;
    }
}

Variant::Variant(const Variant &other) : storedType(other.storedType)
{
    switch (storedType) {
    case Type::Empty:
    case Type::Null:
        break;
    case Type::Integer:
        storedData.construct<std::int_fast64_t>(other.storedData.ref<std::int_fast64_t>());
        break;
    case Type::Real:
        storedData.construct<double>(other.storedData.ref<double>());
        break;
    case Type::String:
        storedData.construct<std::string>(other.storedData.ref<std::string>());
        break;
    case Type::Bytes:
        storedData.construct<Bytes>(other.storedData.ref<Bytes>());
        break;
    }
}

Variant &Variant::operator=(const Variant &other)
{
    if (storedType == other.storedType) {
        switch (storedType) {
        case Type::Empty:
        case Type::Null:
            break;
        case Type::Integer:
            storedData.ref<std::int_fast64_t>() = other.storedData.ref<std::int_fast64_t>();
            break;
        case Type::Real:
            storedData.ref<double>() = other.storedData.ref<double>();
            break;
        case Type::String:
            storedData.ref<std::string>() = other.storedData.ref<std::string>();
            break;
        case Type::Bytes:
            storedData.ref<Bytes>() = other.storedData.ref<Bytes>();
            break;
        }
        return *this;
    }

    Q_ASSERT(&other != this);

    switch (storedType) {
    case Type::Empty:
    case Type::Null:
    case Type::Integer:
    case Type::Real:
        break;
    case Type::String:
        storedData.destruct<std::string>();
        break;
    case Type::Bytes:
        storedData.destruct<Bytes>();
        break;
    }
    storedType = other.storedType;

    switch (storedType) {
    case Type::Empty:
    case Type::Null:
        break;
    case Type::Integer:
        storedData.construct<std::int_fast64_t>(other.storedData.ref<std::int_fast64_t>());
        break;
    case Type::Real:
        storedData.construct<double>(other.storedData.ref<double>());
        break;
    case Type::String:
        storedData.construct<std::string>(other.storedData.ref<std::string>());
        break;
    case Type::Bytes:
        storedData.construct<Bytes>(other.storedData.ref<Bytes>());
        break;
    }
    return *this;
}

Variant::Variant(Variant &&other) noexcept : storedType(other.storedType)
{
    switch (storedType) {
    case Type::Empty:
    case Type::Null:
        break;
    case Type::Integer:
        storedData.construct<std::int_fast64_t>(std::move(other.storedData.ref<std::int_fast64_t>()));
        break;
    case Type::Real:
        storedData.construct<double>(std::move(other.storedData.ref<double>()));
        break;
    case Type::String:
        storedData.construct<std::string>(std::move(other.storedData.ref<std::string>()));
        break;
    case Type::Bytes:
        storedData.construct<Bytes>(std::move(other.storedData.ref<Bytes>()));
        break;
    }
}

Variant &Variant::operator=(Variant &&other) noexcept
{
    if (other.storedType == storedType) {
        switch (storedType) {
        case Type::Empty:
        case Type::Null:
            break;
        case Type::Integer:
            storedData.ref<std::int_fast64_t>() = std::move(other.storedData.ref<std::int_fast64_t>());
            break;
        case Type::Real:
            storedData.ref<double>() = std::move(other.storedData.ref<double>());
            break;
        case Type::String:
            storedData.ref<std::string>() = std::move(other.storedData.ref<std::string>());
            break;
        case Type::Bytes:
            storedData.ref<Bytes>() = std::move(other.storedData.ref<Bytes>());
            break;
        }
        return *this;
    }

    Q_ASSERT(&other != this);

    switch (storedType) {
    case Type::Empty:
    case Type::Null:
    case Type::Integer:
    case Type::Real:
        break;
    case Type::String:
        storedData.destruct<std::string>();
        break;
    case Type::Bytes:
        storedData.destruct<Bytes>();
        break;
    }
    storedType = other.storedType;

    switch (storedType) {
    case Type::Empty:
    case Type::Null:
        break;
    case Type::Integer:
        storedData.construct<std::int_fast64_t>(std::move(other.storedData.ref<std::int_fast64_t>()));
        break;
    case Type::Real:
        storedData.construct<double>(std::move(other.storedData.ref<double>()));
        break;
    case Type::String:
        storedData.construct<std::string>(std::move(other.storedData.ref<std::string>()));
        break;
    case Type::Bytes:
        storedData.construct<Bytes>(std::move(other.storedData.ref<Bytes>()));
        break;
    }
    return *this;
}

Variant::Variant(Variant::Type type) : storedType(type)
{
    switch (storedType) {
    case Type::Empty:
    case Type::Null:
        break;
    case Type::Integer:
        storedData.construct<std::int_fast64_t>(INTEGER::undefined());
        break;
    case Type::Real:
        storedData.construct<double>(FP::undefined());
        break;
    case Type::String:
        storedData.construct<std::string>();
        break;
    case Type::Bytes:
        storedData.construct<Bytes>();
        break;
    }
}

Variant::Variant(double value) : storedType(Type::Real)
{ storedData.construct<double>(value); }

Variant::Variant(std::int_fast64_t value) : storedType(Type::Integer)
{ storedData.construct<std::int_fast64_t>(value); }

Variant::Variant(const std::string &value) : storedType(Type::String)
{ storedData.construct<std::string>(value); }

Variant::Variant(std::string &&value) : storedType(Type::String)
{ storedData.construct<std::string>(std::move(value)); }

Variant::Variant(const char *value) : storedType(Type::String)
{ storedData.construct<std::string>(value); }

Variant::Variant(const QString &value) : storedType(Type::String)
{ storedData.construct<std::string>(value.toStdString()); }

Variant::Variant(const Bytes &value) : storedType(Type::Bytes)
{ storedData.construct<Bytes>(value); }

Variant::Variant(Bytes &&value) : storedType(Type::Bytes)
{ storedData.construct<Bytes>(std::move(value)); }

Variant::Variant(const QByteArray &value) : storedType(Type::Bytes)
{ storedData.construct<Bytes>(value); }

std::int_fast64_t Variant::toInteger() const
{
    switch (type()) {
    case Type::Integer:
        return storedData.ref<std::int_fast64_t>();
    case Type::Real: {
        auto r = storedData.ref<double>();
        if (!FP::defined(r))
            break;
        return static_cast<std::int_fast64_t>(r);
    }
    default:
        break;
    }
    return INTEGER::undefined();
}

double Variant::toReal() const
{
    switch (type()) {
    case Type::Real:
        return storedData.ref<double>();
    case Type::Integer: {
        auto i = storedData.ref<std::int_fast64_t>();
        if (!INTEGER::defined(i))
            break;
        return static_cast<double>(i);
    }
    default:
        break;
    }
    return FP::undefined();
}

static const std::string emptyString;

const std::string &Variant::toString() const
{
    switch (type()) {
    case Type::String:
        return storedData.ref<std::string>();
    default:
        break;
    }
    return emptyString;
}

std::string Variant::takeString()
{
    switch (type()) {
    case Type::String:
        return std::move(storedData.ref<std::string>());
    default:
        break;
    }
    return {};
}

static const Bytes emptyBytes;

const Bytes &Variant::toBytes() const
{
    switch (type()) {
    case Type::Bytes:
        return storedData.ref<Bytes>();
    case Type::String: {
        const auto &cnv = storedData.ref<std::string>();
        static thread_local Bytes result;
        result = CPD3::Util::ByteArray(cnv.data(), cnv.size());
        return result;
    }
    default:
        break;
    }
    return emptyBytes;
}

Bytes Variant::takeBytes()
{
    switch (type()) {
    case Type::Bytes:
        return std::move(storedData.ref<Bytes>());
    case Type::String: {
        auto &cnv = storedData.ref<std::string>();
        return CPD3::Util::ByteArray(cnv.data(), cnv.size());
    }
    default:
        break;
    }
    return {};
}

namespace {
enum VariantSerializationType {
    Type_Empty = 0,
    Type_Null,
    Type_Integer,
    Type_Real,
    Type_String,
    Type_Bytes,
};
}

QDataStream &operator<<(QDataStream &stream, const Variant &a)
{
    switch (a.type()) {
    case Variant::Type::Empty:
        stream << static_cast<quint8>(Type_Empty);
        break;
    case Variant::Type::Null:
        stream << static_cast<quint8>(Type_Null);
        break;
    case Variant::Type::Integer:
        stream << static_cast<quint8>(Type_Integer) << static_cast<qint64>(a.toInteger());
        break;
    case Variant::Type::Real:
        stream << static_cast<quint8>(Type_Real) << a.toReal();
        break;
    case Variant::Type::String:
        stream << static_cast<quint8>(Type_String) << a.toString();
        break;
    case Variant::Type::Bytes:
        stream << static_cast<quint8>(Type_Bytes) << a.toBytes();
        break;
    }
    return stream;
}

QDataStream &operator>>(QDataStream &stream, Variant &a)
{
    quint8 type = 0;
    stream >> type;

    switch (static_cast<VariantSerializationType>(type)) {
    case Type_Empty:
        a = Variant();
        break;
    case Type_Null:
        a = Variant(Variant::Type::Null);
        break;
    case Type_Integer: {
        qint64 v = 0;
        stream >> v;
        a = Variant(v);
        break;
    }
    case Type_Real: {
        double v = 0;
        stream >> v;
        a = Variant(v);
        break;
    }
    case Type_String: {
        std::string v;
        stream >> v;
        a = Variant(std::move(v));
        break;
    }
    case Type_Bytes: {
        CPD3::Util::ByteArray v;
        stream >> v;
        a = Variant(std::move(v));
        break;
    }
    default:
        qCWarning(log_database_util) << "Invalid variant type" << type;
        a = Variant();
        break;
    }

    return stream;
}

QDebug operator<<(QDebug stream, const Variant &a)
{
    QDebugStateSaver saver(stream);
    stream.nospace();

    switch (a.type()) {
    case Variant::Type::Empty:
        stream << "Variant()";
        break;
    case Variant::Type::Null:
        stream << "Variant(null)";
        break;
    case Variant::Type::Integer:
        stream << "Variant(" << a.toInteger() << ")";
        break;
    case Variant::Type::Real:
        stream << "Variant(" << a.toReal() << ")";
        break;
    case Variant::Type::String:
        stream << "Variant(" << a.toString() << ")";
        break;
    case Variant::Type::Bytes:
        stream << "Variant(" << a.toBytes() << ")";
        break;
    }

    return stream;
}

bool operator==(const Variant &a, const Variant &b)
{
    if (a.type() != b.type())
        return false;
    switch (a.type()) {
    case Variant::Type::Empty:
    case Variant::Type::Null:
        return true;
    case Variant::Type::Integer:
        return a.toInteger() == b.toInteger();
    case Variant::Type::Real:
        return FP::equal(a.toReal(), b.toReal());
    case Variant::Type::String:
        return a.toString() == b.toString();
    case Variant::Type::Bytes:
        return CPD3::Util::ByteView(a.toBytes()) == CPD3::Util::ByteView(b.toBytes());
    }
    Q_ASSERT(false);
    return false;
}

bool operator!=(const Variant &a, const Variant &b)
{
    if (a.type() != b.type())
        return true;
    switch (a.type()) {
    case Variant::Type::Empty:
    case Variant::Type::Null:
        return false;
    case Variant::Type::Integer:
        return a.toInteger() != b.toInteger();
    case Variant::Type::Real:
        return !FP::equal(a.toReal(), b.toReal());
    case Variant::Type::String:
        return a.toString() != b.toString();
    case Variant::Type::Bytes:
        return CPD3::Util::ByteView(a.toBytes()) != CPD3::Util::ByteView(b.toBytes());
    }
    Q_ASSERT(false);
    return false;
}


}

}
}
