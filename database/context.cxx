/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include "context.hxx"
#include "interface.hxx"


namespace CPD3 {
namespace Database {

Context::Context(std::shared_ptr<ContextInterface> &&i) : interface(std::move(i)),
                                                          result(interface->result),
                                                          complete(interface->complete)
{
    Q_ASSERT(interface);
}

Context::Context() : interface(std::make_shared<ContextInterface>()),
                     result(interface->result),
                     complete(interface->complete)
{ }

Context::~Context() = default;

Context::Context(Context &&other) : interface(std::move(other.interface)),
                                    result(std::move(other.result)),
                                    complete(std::move(other.complete))
{ }

Context &Context::operator=(Context &&other)
{
    interface = std::move(other.interface);
    result = std::move(other.result);
    complete = std::move(other.complete);
    return *this;
}

Context::Context(const Context &other) : interface(other.interface),
                                         result(other.result),
                                         complete(other.complete)
{ }

Context &Context::operator=(const Context &other)
{
    interface = other.interface;
    result = other.result;
    complete = other.complete;
    return *this;
}

void Context::begin(std::uint_fast32_t read)
{ return interface->begin(read); }

void Context::next()
{ return interface->next(); }

void Context::finish()
{ return interface->finish(); }

void Context::wait()
{ return interface->wait(); }

std::vector<Types::Results> Context::all()
{
    std::vector<Types::Results> r;
    Iterator it(*this);
    while (it.hasNext()) {
        r.emplace_back(it.next());
    }
    return r;
}

Context::Iterator::Iterator() = default;

Context::Iterator::Iterator(Context &&ctx) : isFinished(false), context(std::move(ctx))
{
    begin();
}

Context::Iterator::Iterator(Context &ctx) : isFinished(false), context(ctx)
{
    begin();
}

Context::Iterator::~Iterator()
{
    receiver.disconnect();

    {
        std::lock_guard<std::mutex> lock(mutex);
        if (isFinished)
            return;
    }
    context.finish();
    context.wait();
}

void Context::Iterator::begin()
{
    Q_ASSERT(!isFinished);
    context.result
           .connect(receiver,
                    std::bind(&Context::Iterator::contextResult, this, std::placeholders::_1));
    context.complete.connect(receiver, std::bind(&Context::Iterator::contextComplete, this));
    context.begin();
}

void Context::Iterator::contextResult(const Types::Results &data)
{
    std::unique_lock<std::mutex> lock(mutex);
    Q_ASSERT(!isFinished);
    results.emplace(data);
    if (results.size() >= prefetch) {
        lock.unlock();
        cv.notify_all();
        return;
    }
    lock.unlock();
    cv.notify_all();
    context.next();
}

void Context::Iterator::contextComplete()
{
    /* Hold the lock, so destruction can't happen */
    std::lock_guard<std::mutex> lock(mutex);
    Q_ASSERT(!isFinished);
    isFinished = true;
    cv.notify_all();
}

bool Context::Iterator::hasNext()
{
    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        if (!results.empty())
            return true;
        if (isFinished)
            return false;
        cv.wait(lock);
    }
}

Types::Results Context::Iterator::next()
{
    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        if (!results.empty()) {
            bool full = (results.size() >= prefetch);
            Types::Results r = std::move(results.front());
            results.pop();
            if (!isFinished && full) {
                lock.unlock();
                context.next();
            }
            return r;
        }
        if (isFinished)
            return Types::Results();
        cv.wait(lock);
    }
}


Context::Guard::Guard() = default;

Context::Guard::Guard(Context::Guard &&other) : context(std::move(other.context))
{
    other.context = Context();
}

Context::Guard &Context::Guard::operator=(Context::Guard &&other)
{
    context = std::move(other.context);
    other.context = Context();
    return *this;
}

Context::Guard::Guard(Context &&ctx) : context(std::move(ctx))
{
    context.begin(0);
    context.finish();
}

Context::Guard::Guard(Context &ctx) : context(ctx)
{
    context.begin(0);
    context.finish();
}

Context::Guard::~Guard()
{ context.wait(); }

void Context::Guard::wait()
{ context.wait(); }

}
}