if(HAVE_SQLITE3)
    set(SQLITE3_SRC drivers/sqlite3.cxx drivers/sqlite3.hxx)
    add_definitions(-DHAVE_SQLITE3)
    include_directories(${SQLITE3_INCLUDE_DIR})
    if(HAVE_SQLITE3_PREPARE3)
        add_definitions(-DHAVE_SQLITE3_PREPARE3)
    endif(HAVE_SQLITE3_PREPARE3)
    if(HAVE_SQLITE3_TEXT64)
        add_definitions(-DHAVE_SQLITE3_TEXT64)
    endif(HAVE_SQLITE3_TEXT64)
    if(HAVE_SQLITE3_BLOB64)
        add_definitions(-DHAVE_SQLITE3_BLOB64)
    endif(HAVE_SQLITE3_BLOB64)
    if(HAVE_SQLITE3_ERRSTR)
        add_definitions(-DHAVE_SQLITE3_ERRSTR)
    endif(HAVE_SQLITE3_ERRSTR)
endif(HAVE_SQLITE3)

if(PostgreSQL_FOUND)
    set(POSTGRESQL_SRC drivers/postgresql.cxx drivers/postgresql.hxx)
    add_definitions(-DHAVE_POSTGRESQL)
    include_directories(${PostgreSQL_INCLUDE_DIRS})
    if(PostgreSQL_TYPE_INCLUDE_DIR)
        find_path(PostgreSQL_TYPE_DEFINITIONS
                  NAMES catalog/pg_type_d.h
                  ${PostgreSQL_TYPE_INCLUDE_DIR})
        if(PostgreSQL_TYPE_DEFINITIONS)
            add_definitions(-DHAVE_POSTGRESQL_TYPES_D)
            include_directories(${PostgreSQL_TYPE_INCLUDE_DIR})
        endif(PostgreSQL_TYPE_DEFINITIONS)
    endif(PostgreSQL_TYPE_INCLUDE_DIR)
    if(HAVE_POSTGRESQL_PQCONNECTDBPARAMS)
        add_definitions(-DHAVE_POSTGRESQL_PQCONNECTDBPARAMS)
    endif(HAVE_POSTGRESQL_PQCONNECTDBPARAMS)
endif(PostgreSQL_FOUND)

add_library(cpd3database SHARED
            database.hxx
            util.cxx util.hxx
            storage.cxx storage.hxx
            tabledefinition.cxx tabledefinition.hxx
            driver.cxx driver.hxx
            connection.cxx connection.hxx
            statement.cxx statement.hxx
            context.cxx context.hxx
            interface.cxx interface.hxx
            runlock.cxx runlock.hxx
            drivers/sqlitecommon.cxx drivers/sqlitecommon.hxx
            drivers/postgresqlcommon.cxx drivers/postgresqlcommon.hxx
            drivers/qtdatabase.cxx drivers/qtdatabase.hxx
            drivers/qtsqlite.cxx drivers/qtsqlite.hxx
            drivers/qtmysql.cxx drivers/qtmysql.hxx
            drivers/qtpostgresql.cxx drivers/qtpostgresql.hxx
            ${SQLITE3_SRC} ${POSTGRESQL_SRC})
target_link_libraries(cpd3database cpd3core Qt5::Core Qt5::Sql)
if(Threads_FOUND)
    target_link_libraries(cpd3database Threads::Threads)
endif(Threads_FOUND)
if(HAVE_SQLITE3)
    target_link_libraries(cpd3database ${SQLITE3_LIBRARY})
endif(HAVE_SQLITE3)
if(PostgreSQL_FOUND)
    target_link_libraries(cpd3database ${PostgreSQL_LIBRARIES})
endif(PostgreSQL_FOUND)

install(TARGETS cpd3database
        LIBRARY DESTINATION ${LIB_INSTALL_DIR}
        ARCHIVE DESTINATION ${LIB_INSTALL_DIR}
        RUNTIME DESTINATION ${BIN_INSTALL_DIR})


add_subdirectory(test)
add_subdirectory(drivers/test)
