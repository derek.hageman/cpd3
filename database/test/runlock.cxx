/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <cmath>
#include <QTest>
#include <QtSql>
#include <QString>
#include <QTemporaryFile>

#include "database/runlock.hxx"
#include "database/storage.hxx"
#include "database/connection.hxx"
#include "core/number.hxx"
#include "core/timeutils.hxx"

using namespace CPD3;
using namespace CPD3::Database;

//#define USE_QT_BACKEND
//#define USE_MYSQL_DB
//#define USE_PSQL_DB

#if defined(USE_PSQL_DB) && !defined(USE_QT_BACKEND)
#include "database/drivers/postgresql.hxx"
#endif

class TestRunLock : public QObject {
Q_OBJECT

    QString diskDB;

    QSqlDatabase getQtDatabase()
    {
#if defined(USE_MYSQL_DB)
        QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL",
            QUuid::createUuid().toString());
        db.setHostName("localhost");
        db.setUserName("root");
        db.setPassword("aer");
        db.setDatabaseName("cpd3test");
        return db;
#elif defined(USE_PSQL_DB)
        QSqlDatabase db = QSqlDatabase::addDatabase("QPSQL",
            QUuid::createUuid().toString());
        db.setHostName("localhost");
        db.setUserName("aer");
        db.setDatabaseName("test");
        return db;
#else
        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", QUuid::createUuid().toString());
        db.setDatabaseName(diskDB);
        return db;
#endif
    }

    Storage getDatabaseStorage()
    {
#if defined(USE_PSQL_DB) && !defined(USE_QT_BACKEND)
        return Storage::postgresql("localhost", "test", "aer");
#elif !defined(USE_QT_BACKEND)
        return Storage::sqlite(diskDB.toStdString());
#else
        return Storage::qt(getQtDatabase());
#endif
    }

    static void releaseDatabase(QSqlDatabase &db)
    {
        QString name = db.connectionName();
        if (db.isOpen())
            db.close();
        db = QSqlDatabase();
        QSqlDatabase::removeDatabase(name);
    }

private slots:

    void initTestCase()
    {
#if !defined(USE_MYSQL_DB) && !defined(USE_PSQL_DB)
        QTemporaryFile temp;
        temp.setAutoRemove(false);
        temp.open();
        diskDB = temp.fileName();
#endif
    }

    void cleanup()
    {
        QSqlDatabase db = getQtDatabase();
        QVERIFY(db.open());
        {
            QSqlQuery("DROP TABLE CPD3Run", db).finish();
            QSqlQuery("DROP TABLE CPD3DBVersion", db).finish();
        }
        releaseDatabase(db);
    }

    void cleanupTestCase()
    {
#if !defined(USE_MYSQL_DB) && !defined(USE_PSQL_DB)
        QFile(diskDB).remove();
#endif
    }

    void initializeEmptyDB()
    {
        QSqlDatabase db = getQtDatabase();
        QVERIFY(db.open());

        {
            RunLock rc(getDatabaseStorage());

            {
                Connection c(Storage::qt(getQtDatabase()));
                c.start();
                Connection::Transaction tx(c, Connection::Transaction_Serializable);
                QVERIFY(c.hasTable("CPD3DBVersion"));
                QVERIFY(c.hasTable("CPD3Run"));
            }

            QSqlQuery q(db);
            QSqlRecord r;

            QVERIFY(q.prepare("SELECT version FROM CPD3DBVersion WHERE system = 3"));
            QVERIFY(q.exec());
            QVERIFY(q.first());
            r = q.record();
            QVERIFY(r.count() > 0);
            QVERIFY(r.value(0).toInt() != 0);
            q.finish();

            QVERIFY(q.prepare("SELECT * FROM CPD3Run"));
            QVERIFY(q.exec());
            q.finish();
        }

        releaseDatabase(db);
    }

    void serial()
    {
        QSqlDatabase db = getQtDatabase();
        QVERIFY(db.open());

        {
            RunLock rc(getDatabaseStorage());
            double t1 = Time::time();
            QTest::qSleep(1);
            QVERIFY(!FP::defined(rc.acquire("key1")));
            QTest::qSleep(1);
            double t2 = Time::time();
            rc.release();
            double rt = rc.acquire("key1");
            QTest::qSleep(1);
            double t3 = Time::time();
            QVERIFY(FP::defined(rt));
            QVERIFY(rt >= std::floor(t1 / 1000.0) * 1000.0);
            QVERIFY(rt <= t2);
            rc.release();
            QTest::qSleep(1);
            double to1s = Time::time();
            QTest::qSleep(1);
            rt = rc.acquire("key1");
            QTest::qSleep(1);
            double to1e = Time::time();
            QVERIFY(FP::defined(rt));
            QVERIFY(rt >= std::floor(t2 / 1000.0) * 1000.0);
            QVERIFY(rt <= t3);
            rc.release();

            QTest::qSleep(1);
            t1 = Time::time();
            QTest::qSleep(1);
            QVERIFY(!FP::defined(rc.acquire("key2")));
            QTest::qSleep(1);
            t2 = Time::time();
            rc.release();
            rt = rc.acquire("key2");
            QTest::qSleep(1);
            t3 = Time::time();
            QVERIFY(FP::defined(rt));
            QVERIFY(rt >= std::floor(t1 / 1000.0) * 1000.0);
            QVERIFY(rt <= t2);
            rc.release();
            QTest::qSleep(1);
            double t4 = Time::time();
            QTest::qSleep(1);
            rt = rc.acquire("key2");
            QTest::qSleep(1);
            double t5 = Time::time();
            QTest::qSleep(1);
            QVERIFY(FP::defined(rt));
            QVERIFY(rt >= std::floor(t2 / 1000.0) * 1000.0);
            QVERIFY(rt <= t3);
            rt = rc.acquire("key1");
            QVERIFY(FP::defined(rt));
            QVERIFY(rt >= std::floor(to1s / 1000.0) * 1000.0);
            QVERIFY(rt <= to1e);
            rc.release();

            rt = rc.acquire("key2");
            QVERIFY(FP::defined(rt));
            QVERIFY(rt >= std::floor(t4 / 1000.0) * 1000.0);
            QVERIFY(rt <= t5);
            rc.release();
            rt = rc.acquire("key1");
            QVERIFY(FP::defined(rt));
            QVERIFY(rt >= std::floor(t4 / 1000.0) * 1000.0);
            QVERIFY(rt <= t5);
            rc.fail();

            rt = rc.acquire("key1");
            QVERIFY(rt >= std::floor(t4 / 1000.0) * 1000.0);
            QVERIFY(rt <= t5);
        }

        RunLock::cleanupStale(getDatabaseStorage());

        releaseDatabase(db);
    }

    void dataStorage()
    {
        QSqlDatabase db = getQtDatabase();
        QVERIFY(db.open());

        {
            RunLock rc(getDatabaseStorage());

            rc.acquire("key1");
            QVERIFY(rc.get("key1").isEmpty());
            QVERIFY(rc.get("key1").isNull());
            rc.set("key1", QByteArray("abc"));
            QCOMPARE(rc.get("key1"), QByteArray("abc"));
            rc.release();

            QCOMPARE(rc.blindGet("key1"), QByteArray("abc"));

            rc.acquire("key1");
            QCOMPARE(rc.get("key1"), QByteArray("abc"));
            rc.set("key1", QByteArray("def"));
            QCOMPARE(rc.get("key1"), QByteArray("def"));
            rc.fail();
            rc.acquire("key1");
            QCOMPARE(rc.get("key1"), QByteArray("abc"));
            rc.set("key1", QByteArray());
            rc.release();

            rc.acquire("key1");
            QVERIFY(rc.get("key1").isEmpty());
            QVERIFY(rc.get("key1").isNull());
            rc.release();

            rc.blindSet("key2", QByteArray("cde"));
            QCOMPARE(rc.blindGet("key2"), QByteArray("cde"));
        }

        releaseDatabase(db);
    }

    void blocking()
    {
        QSqlDatabase db = getQtDatabase();
        QVERIFY(db.open());

        {
            RunLock rc1(getDatabaseStorage());
            RunLock rc2(getDatabaseStorage());
            bool ok;

            ok = false;
            rc1.acquire("key1", 0, &ok);
            QVERIFY(ok);
            ok = false;
            rc1.acquire("key1", 0, &ok);
            QVERIFY(ok);

            ok = true;
            rc2.acquire("key1", 0, &ok);
            QVERIFY(!ok);
            ok = true;
            rc2.acquire("key1", 0.5, &ok);
            QVERIFY(!ok);
            ok = false;
            rc2.acquire("key2", 0, &ok);
            QVERIFY(ok);
            rc1.release();
            ok = false;
            rc2.acquire("key1", 0, &ok);
            QVERIFY(ok);
            rc2.release();

            ok = false;
            rc1.acquire("key1", 0.1, &ok);
            QVERIFY(ok);
            rc1.release();
        }

        releaseDatabase(db);
    }

    void lockedPing()
    {
        QSqlDatabase db = getQtDatabase();
        QVERIFY(db.open());
        {
            RunLock rc(getDatabaseStorage());
            QSqlQuery q(db);

            bool ok = false;
            rc.acquire("key", 0, &ok);
            QVERIFY(ok);
            qint64 tStart = (qint64) std::floor(Time::time());
            QTest::qSleep(6000);
            QVERIFY(q.exec("SELECT ping FROM CPD3Run"));
            QVERIFY(q.first());
            QVERIFY(q.record().value("ping").toLongLong() > tStart);
            q.finish();
            rc.release();
        }
        releaseDatabase(db);
    }
};

QTEST_MAIN(TestRunLock)

#include "runlock.moc"
