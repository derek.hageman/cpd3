/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <thread>
#include <memory>
#include <cstdint>
#include <QTest>
#include <QtSql>
#include <QString>
#include <QTemporaryFile>

#include "database/connection.hxx"
#include "database/tabledefinition.hxx"

using namespace CPD3;
using namespace CPD3::Database;


class TestStatement : public QObject {
Q_OBJECT

    static void releaseDatabase(QSqlDatabase &db)
    {
        QString name = db.connectionName();
        if (db.isOpen())
            db.close();
        db = QSqlDatabase();
        QSqlDatabase::removeDatabase(name);
    }

private slots:

    void types()
    {
        QTemporaryFile file;
        QVERIFY(file.open());

        {
            QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", QUuid::createUuid().toString());
            db.setDatabaseName(file.fileName());
            QVERIFY(db.open());

            db.exec("CREATE TABLE ti ( i INTEGER NOT NULL PRIMARY KEY, j INTEGER NULL )");
            QVERIFY(!db.lastError().isValid());

            releaseDatabase(db);
        }

        {
            Connection c(Storage::sqlite(file.fileName().toStdString()));
            QVERIFY(c.start());

            auto s = c.general("CREATE TABLE tt ( i INTEGER NOT NULL PRIMARY KEY )");
            QVERIFY(s);

            s = c.select("ti", {"i"});
            QVERIFY(s);

            s = c.insert("ti", {"i"});
            QVERIFY(s);

            s = c.insertSelect("ti", {"i"}, "ti", {"i"});
            QVERIFY(s);

            s = c.merge("ti", {"i"}, {"j"});
            QVERIFY(s);

            s = c.merge("ti", {"i", "j"});
            QVERIFY(s);

            s = c.update("ti", {"j"}, "i = :x");
            QVERIFY(s);

            s = c.del("ti", "i = :x");
            QVERIFY(s);
        }

        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", QUuid::createUuid().toString());
        db.setDatabaseName(file.fileName());
        QVERIFY(db.open());
        releaseDatabase(db);
    }

    void synchronous()
    {
        QTemporaryFile file;
        QVERIFY(file.open());

        {
            Connection c(Storage::sqlite(file.fileName().toStdString()));
            QVERIFY(c.start());

            auto s = c.general("CREATE TABLE ti ( i INTEGER NOT NULL PRIMARY KEY )");
            QVERIFY(s);

            c.synchronousTransaction([&]() -> Connection::SynchronousResult {
                s.synchronous();
                return Connection::Synchronous_Ok;
            });
        }

        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", QUuid::createUuid().toString());
        db.setDatabaseName(file.fileName());
        QVERIFY(db.open());
        QStringList check(db.tables());
        std::sort(check.begin(), check.end());
        QCOMPARE(check, QStringList("ti"));
        releaseDatabase(db);
    }

    void batch()
    {
        QTemporaryFile file;
        QVERIFY(file.open());

        {
            QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", QUuid::createUuid().toString());
            db.setDatabaseName(file.fileName());
            QVERIFY(db.open());

            db.exec("CREATE TABLE ti ( i INTEGER NOT NULL PRIMARY KEY )");
            QVERIFY(!db.lastError().isValid());

            releaseDatabase(db);
        }

        {
            Connection c(Storage::sqlite(file.fileName().toStdString()));
            QVERIFY(c.start());

            auto s = c.insert("ti", {"i"});
            QVERIFY(s);

            bool ok = c.synchronousTransaction([&]() -> Connection::SynchronousResult {
                s.batch(Types::BatchBinds{{"i", Types::BatchVariants{1, 2}}});
                return Connection::Synchronous_Ok;
            });
            QVERIFY(ok);
        }

        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", QUuid::createUuid().toString());
        db.setDatabaseName(file.fileName());
        QVERIFY(db.open());
        {
            auto q = db.exec("SELECT i FROM ti ORDER BY i ASC");
            QVERIFY(q.exec());
            QVERIFY(q.first());
            QCOMPARE(q.value(0).toInt(), 1);
            QVERIFY(q.next());
            QCOMPARE(q.value(0).toInt(), 2);
            QVERIFY(!q.next());
            q.finish();
        }
        releaseDatabase(db);
    }

    void extract()
    {
        QTemporaryFile file;
        QVERIFY(file.open());

        {
            QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", QUuid::createUuid().toString());
            db.setDatabaseName(file.fileName());
            QVERIFY(db.open());

            db.exec("CREATE TABLE ti ( i INTEGER NOT NULL PRIMARY KEY, j INTEGER NOT NULL )");
            QVERIFY(!db.lastError().isValid());

            db.exec("INSERT INTO ti(i, j) VALUES(1, 10)");
            QVERIFY(!db.lastError().isValid());
            db.exec("INSERT INTO ti(i, j) VALUES(2, 20)");
            QVERIFY(!db.lastError().isValid());

            releaseDatabase(db);
        }

        {
            Connection c(Storage::sqlite(file.fileName().toStdString()));
            QVERIFY(c.start());

            auto s = c.select("ti", {"j"}, "i = :x");
            QVERIFY(s);

            Types::Results out;
            c.synchronousTransaction([&]() -> Connection::SynchronousResult {
                out = s.extract({{"x", 1}}, true);
                return Connection::Synchronous_Ok;
            });

            QCOMPARE((int) out.size(), 1);
            QCOMPARE((int)out[0].toInteger(), 10);

            Types::Variant v;
            c.synchronousTransaction([&]() -> Connection::SynchronousResult {
                v = s.single({{"x", 2}}, true);
                return Connection::Synchronous_Ok;
            });

            QVERIFY(v.isSet());
            QVERIFY(!v.isNull());
            QCOMPARE((int)v.toInteger(), 20);
        }
    }
};

QTEST_MAIN(TestStatement)

#include "statement.moc"
