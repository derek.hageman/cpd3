/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <thread>
#include <mutex>
#include <condition_variable>
#include <QTest>
#include <QtSql>
#include <QString>
#include <QTemporaryFile>

#include "database/connection.hxx"
#include "database/tabledefinition.hxx"

using namespace CPD3;
using namespace CPD3::Database;


class TestContext : public QObject {
Q_OBJECT

    static void releaseDatabase(QSqlDatabase &db)
    {
        QString name = db.connectionName();
        if (db.isOpen())
            db.close();
        db = QSqlDatabase();
        QSqlDatabase::removeDatabase(name);
    }

private slots:

    void basic()
    {
        QTemporaryFile file;
        QVERIFY(file.open());

        {
            QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", QUuid::createUuid().toString());
            db.setDatabaseName(file.fileName());
            QVERIFY(db.open());

            db.exec("CREATE TABLE ti ( i INTEGER NOT NULL PRIMARY KEY, j INTEGER NOT NULL )");
            QVERIFY(!db.lastError().isValid());

            db.exec("INSERT INTO ti(i, j) VALUES(1, 10)");
            QVERIFY(!db.lastError().isValid());
            db.exec("INSERT INTO ti(i, j) VALUES(2, 20)");
            QVERIFY(!db.lastError().isValid());

            releaseDatabase(db);
        }

        {
            Connection c(Storage::sqlite(file.fileName().toStdString()));
            QVERIFY(c.start());

            auto s = c.select("ti", {"j"}, std::string(), "j ASC");
            QVERIFY(s);

            std::vector<std::int_fast64_t> result;
            bool ok = c.synchronousTransaction([&]() -> Connection::SynchronousResult {
                result.clear();

                auto ctx = s.execute();

                bool done = false;
                std::mutex mutex;
                std::condition_variable cv;

                ctx.result.connect([&](const Types::Results &v) {
                    Q_ASSERT(!v.empty());
                    result.push_back(v[0].toInteger());
                    ctx.next();
                });
                ctx.complete.connect([&]() {
                    std::lock_guard<std::mutex> lock(mutex);
                    done = true;
                    cv.notify_all();
                });

                ctx.begin();

                std::unique_lock<std::mutex> lock(mutex);
                cv.wait(lock, [&] { return done; });

                return Connection::Synchronous_Ok;
            });
            QVERIFY(ok);

            QCOMPARE((int) result.size(), 2);
            QCOMPARE((int)result[0], 10);
            QCOMPARE((int)result[1], 20);
        }
    }

    void firstComplete()
    {
        QTemporaryFile file;
        QVERIFY(file.open());

        {
            QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", QUuid::createUuid().toString());
            db.setDatabaseName(file.fileName());
            QVERIFY(db.open());

            db.exec("CREATE TABLE ti ( i INTEGER NOT NULL PRIMARY KEY, j INTEGER NOT NULL )");
            QVERIFY(!db.lastError().isValid());

            releaseDatabase(db);
        }

        {
            Connection c(Storage::sqlite(file.fileName().toStdString()));
            QVERIFY(c.start());

            auto s = c.select("ti", {"j"}, std::string(), "j ASC");
            QVERIFY(s);

            std::vector<std::int_fast64_t> result;
            bool ok = c.synchronousTransaction([&]() -> Connection::SynchronousResult {
                result.clear();

                auto ctx = s.execute();

                bool done = false;
                std::mutex mutex;
                std::condition_variable cv;

                ctx.result.connect([&](const Types::Results &v) {
                    Q_ASSERT(!v.empty());
                    result.push_back(v[0].toInteger());
                    ctx.next();
                });
                ctx.complete.connect([&]() {
                    std::lock_guard<std::mutex> lock(mutex);
                    done = true;
                    cv.notify_all();
                });

                ctx.begin();

                std::unique_lock<std::mutex> lock(mutex);
                cv.wait(lock, [&] { return done; });

                return Connection::Synchronous_Ok;
            });
            QVERIFY(ok);

            QCOMPARE((int) result.size(), 0);
        }
    }

    void iterator()
    {
        QTemporaryFile file;
        QVERIFY(file.open());

        {
            QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", QUuid::createUuid().toString());
            db.setDatabaseName(file.fileName());
            QVERIFY(db.open());

            db.exec("CREATE TABLE ti ( i INTEGER NOT NULL PRIMARY KEY, j INTEGER NOT NULL )");
            QVERIFY(!db.lastError().isValid());

            db.exec("INSERT INTO ti(i, j) VALUES(1, 10)");
            QVERIFY(!db.lastError().isValid());
            db.exec("INSERT INTO ti(i, j) VALUES(2, 20)");
            QVERIFY(!db.lastError().isValid());

            releaseDatabase(db);
        }

        {
            Connection c(Storage::sqlite(file.fileName().toStdString()));
            QVERIFY(c.start());

            auto s = c.select("ti", {"j"}, std::string(), "j ASC");
            QVERIFY(s);

            std::vector<std::int_fast64_t> result;
            bool ok = c.synchronousTransaction([&]() -> Connection::SynchronousResult {
                result.clear();

                Context::Iterator it(s.execute());
                while (it.hasNext()) {
                    result.push_back(it.next()[0].toInteger());
                }

                return Connection::Synchronous_Ok;
            });
            QVERIFY(ok);

            QCOMPARE((int) result.size(), 2);
            QCOMPARE((int)result[0], 10);
            QCOMPARE((int)result[1], 20);
        }
    }

    void guard()
    {
        QTemporaryFile file;
        QVERIFY(file.open());

        {
            QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", QUuid::createUuid().toString());
            db.setDatabaseName(file.fileName());
            QVERIFY(db.open());

            db.exec("CREATE TABLE ti ( i INTEGER NOT NULL PRIMARY KEY, j INTEGER NOT NULL )");
            QVERIFY(!db.lastError().isValid());

            db.exec("INSERT INTO ti(i, j) VALUES(1, 10)");
            QVERIFY(!db.lastError().isValid());
            db.exec("INSERT INTO ti(i, j) VALUES(2, 20)");
            QVERIFY(!db.lastError().isValid());

            releaseDatabase(db);
        }

        {
            Connection c(Storage::sqlite(file.fileName().toStdString()));
            QVERIFY(c.start());

            auto s = c.del("ti");
            QVERIFY(s);

            bool ok = c.synchronousTransaction([&]() -> Connection::SynchronousResult {
                Context::Guard g(s.execute());
                return Connection::Synchronous_Ok;
            });
            QVERIFY(ok);
        }

        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", QUuid::createUuid().toString());
        db.setDatabaseName(file.fileName());
        QVERIFY(db.open());
        {
            auto q = db.exec("SELECT i FROM ti ORDER BY i ASC");
            QVERIFY(q.exec());
            QVERIFY(!q.first());
            q.finish();
        }

        releaseDatabase(db);
    }

    void shutdownActive()
    {
        QTemporaryFile file;
        QVERIFY(file.open());

        Connection c(Storage::sqlite(file.fileName().toStdString()));
        QVERIFY(c.start());

        bool ok = c.synchronousTransaction([&]() -> Connection::SynchronousResult {
            TableDefinition td("tt");
            td.integer<std::uint16_t>("pk", false);
            td.primaryKey("pk");
            c.createTable(td);

            c.insert("tt", {"pk"}).synchronous({{"pk", static_cast<qint64>(1)}});
            c.insert("tt", {"pk"}).synchronous({{"pk", static_cast<qint64>(2)}});

            return Connection::Synchronous_Ok;
        });
        QVERIFY(ok);

        auto tx = c.transaction();
        auto s = c.select("tt", {"pk"});
        auto a = s.execute();

        bool completed = false;

        std::mutex mutex;
        std::condition_variable cv;
        bool gate = false;
        bool blocked = false;

        a.result.connect([&](const Types::Results &) {
            std::unique_lock<std::mutex> lock(mutex);
            blocked = true;
            cv.notify_all();
            cv.wait(lock, [&]() { return gate; });
            gate = false;
            blocked = false;
        });
        a.complete.connect([&]() {
            completed = true;
        });

        a.begin();

        {
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait_for(lock, std::chrono::seconds(5), [&]() { return blocked; });
            QVERIFY(blocked);
        }

        c.signalTerminate();
        {
            std::lock_guard<std::mutex> lock(mutex);
            gate = true;
        }
        cv.notify_all();

        QVERIFY(c.wait(5.0));
        QVERIFY(completed);
        QVERIFY(!blocked);

        a = s.execute();
        completed = false;
        a.complete.connect([&]() {
            completed = true;
        });
        a.begin();
        QVERIFY(completed);
    }
};

QTEST_MAIN(TestContext)

#include "context.moc"
