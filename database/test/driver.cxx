/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <thread>
#include <memory>
#include <condition_variable>
#include <QTest>
#include <QtSql>
#include <QString>
#include <QTemporaryFile>

#include "database/connection.hxx"
#include "database/tabledefinition.hxx"
#include "database/driver.hxx"

using namespace CPD3;
using namespace CPD3::Database;


class TestDriver : public QObject {
Q_OBJECT

    static void releaseDatabase(QSqlDatabase &db)
    {
        QString name = db.connectionName();
        if (db.isOpen())
            db.close();
        db = QSqlDatabase();
        QSqlDatabase::removeDatabase(name);
    }

private slots:

    void qtsqlite()
    {
        QTemporaryFile file;
        QVERIFY(file.open());

        {
            QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", QUuid::createUuid().toString());
            db.setDatabaseName(file.fileName());
            Storage s = Storage::qt(db);
            db = QSqlDatabase();

            bool ok = false;
            std::mutex mutex;
            std::condition_variable cv;
            bool done = false;
            ok = s.open([&](std::unique_ptr<Driver> &d) {
                Q_ASSERT(d.get());
                d.reset();

                std::lock_guard<std::mutex> lock(mutex);
                done = true;
                cv.notify_all();
            });

            QVERIFY(ok);
            std::unique_lock<std::mutex> lock(mutex);
            cv.wait(lock, [&] { return done; });
        }

        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", QUuid::createUuid().toString());
        db.setDatabaseName(file.fileName());
        QVERIFY(db.open());
        releaseDatabase(db);
    }
};

QTEST_MAIN(TestDriver)

#include "driver.moc"
