/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <thread>
#include <memory>
#include <cstdint>
#include <QTest>
#include <QtSql>
#include <QString>
#include <QTemporaryFile>

#include "database/connection.hxx"
#include "database/tabledefinition.hxx"

using namespace CPD3;
using namespace CPD3::Database;


class TestConnection : public QObject {
Q_OBJECT

    static void releaseDatabase(QSqlDatabase &db)
    {
        QString name = db.connectionName();
        if (db.isOpen())
            db.close();
        db = QSqlDatabase();
        QSqlDatabase::removeDatabase(name);
    }

private slots:

    void basic()
    {
        QTemporaryFile file;
        QVERIFY(file.open());

        {
            Connection c(Storage::sqlite(file.fileName().toStdString()));
            QVERIFY(c.start());

            c.signalTerminate();
            c.wait();
        }

        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", QUuid::createUuid().toString());
        db.setDatabaseName(file.fileName());
        QVERIFY(db.open());

        releaseDatabase(db);
    }

    void transaction()
    {
        QTemporaryFile file;
        QVERIFY(file.open());

        {
            Connection c(Storage::sqlite(file.fileName().toStdString()));
            QVERIFY(c.start());

            {
                auto tx = c.transaction();

                tx.end();
            }
        }

        {
            Connection c(Storage::sqlite(file.fileName().toStdString()));
            QVERIFY(c.start());

            {
                auto tx = c.transaction();

                tx.abort();
            }

            c.shutdown(false);
            c.wait();
        }

        {
            Connection c(Storage::sqlite(file.fileName().toStdString()));
            QVERIFY(c.start());

            {
                auto tx = std::make_shared<Connection::Transaction>(c);

                std::thread([=] {
                    std::this_thread::sleep_for(std::chrono::milliseconds(250));
                    tx->end();
                }).detach();
            }

            c.shutdown(true);
            c.wait();
        }

        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", QUuid::createUuid().toString());
        db.setDatabaseName(file.fileName());
        QVERIFY(db.open());
        releaseDatabase(db);
    }

    void synchronousTransaction()
    {
        QTemporaryFile file;
        QVERIFY(file.open());

        {
            Connection c(Storage::sqlite(file.fileName().toStdString()));
            QVERIFY(c.start());

            c.synchronousTransaction([]() -> Connection::SynchronousResult {
                return Connection::Synchronous_Ok;
            });

            c.shutdown(true);
            c.wait();
        }

        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", QUuid::createUuid().toString());
        db.setDatabaseName(file.fileName());
        QVERIFY(db.open());
        releaseDatabase(db);
    }

    void tables()
    {
        QTemporaryFile file;
        QVERIFY(file.open());

        {
            QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", QUuid::createUuid().toString());
            db.setDatabaseName(file.fileName());
            QVERIFY(db.open());

            db.exec("CREATE TABLE ti ( i INTEGER NOT NULL PRIMARY KEY )");
            QVERIFY(!db.lastError().isValid());

            releaseDatabase(db);
        }

        {
            Connection c(Storage::sqlite(file.fileName().toStdString()));
            QVERIFY(c.start());

            for (;;) {
                auto tx = c.transaction();

                TableDefinition td("tt");
                td.integer<std::uint16_t>("pk", false);
                td.primaryKey("pk");
                c.createTable(td);

                QCOMPARE(c.tables(), (std::unordered_set<std::string>{"tt", "ti"}));

                if (!tx.end())
                    continue;
                break;
            }

            c.shutdown(true);
            c.wait();
        }

        {
            QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", QUuid::createUuid().toString());
            db.setDatabaseName(file.fileName());
            QVERIFY(db.open());
            QStringList check(db.tables());
            std::sort(check.begin(), check.end());
            QCOMPARE(check, QStringList("ti") << "tt");
            releaseDatabase(db);
        }

        {
            Connection c(Storage::sqlite(file.fileName().toStdString()));
            QVERIFY(c.start());

            for (;;) {
                auto tx = c.transaction();

                c.removeTable("ti");

                QCOMPARE(c.tables(), (std::unordered_set<std::string>{"tt"}));

                if (!tx.end())
                    continue;
                break;
            }

            c.shutdown(true);
            c.wait();
        }

        {
            QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", QUuid::createUuid().toString());
            db.setDatabaseName(file.fileName());
            QVERIFY(db.open());
            QStringList check(db.tables());
            std::sort(check.begin(), check.end());
            QCOMPARE(check, QStringList("tt"));
            releaseDatabase(db);
        }
    }

    void tablesMemory()
    {
        Connection c(Storage::sqlite());
        QVERIFY(c.start());

        for (;;) {
            auto tx = c.transaction();

            TableDefinition td("tt");
            td.integer<std::uint16_t>("pk", false);
            td.primaryKey("pk");
            c.createTable(td);

            QCOMPARE(c.tables(), (std::unordered_set<std::string>{"tt"}));

            if (!tx.end())
                continue;
            break;
        }
        {
            auto tx = c.transaction(Connection::Transaction_ReadOnly);
            QCOMPARE(c.tables(), (std::unordered_set<std::string>{"tt"}));
        }

        c.shutdown(true);
        c.wait();
    }
};

QTEST_MAIN(TestConnection)

#include "connection.moc"
