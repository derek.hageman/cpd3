/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATABASE_DRIVER_HXX
#define CPD3DATABASE_DRIVER_HXX

#include "core/first.hxx"

#include <vector>
#include <unordered_set>
#include <string>

#include "database.hxx"
#include "statement.hxx"
#include "context.hxx"
#include "util.hxx"
#include "core/number.hxx"

namespace CPD3 {
namespace Database {

class TableDefinition;

/**
 * The base interface for database drivers.  This is only created on the thread
 * that will execute database operations.
 */
class CPD3DATABASE_EXPORT Driver {
protected:
    /**
     * Assemble a standard SQL select statement.
     *
     * @param select        the pairs of parameters and names to select
     * @param from          the paris of tables and names to fetch from
     * @param where         the where clause
     * @param order         the order clause
     * @return              the select statement
     */
    static std::string assembleSelect(const Types::TableSet &from,
                                      const Types::Outputs &select,
                                      const std::string &where,
                                      const std::string &order);

    /**
     * Assemble a standard SQL insert statement.
     *
     * @param table         the table
     * @param columns       the columns to insert
     * @return              the insert statement
     */
    static std::string assembleInsert(const std::string &table, const Types::ColumnSet &columns);

    /**
     * Assemble a standard SQL insert statement that gets its values from a select.
     *
     * @param table         the table
     * @param columns       the columns to insert
     * @param select        the pairs of parameters and names to select
     * @param from          the paris of tables and names to fetch from
     * @param where         the where clause
     * @param order         the order clause
     * @return              the insert statement
     */
    static std::string assembleInsertSelect(const std::string &table,
                                            const Types::Outputs &columns,
                                            const Types::TableSet &from,
                                            const Types::Outputs &select,
                                            const std::string &where,
                                            const std::string &order);

    /**
     * Assemble a standard SQL merge (upsert) statement.
     *
     * @param table         the table to insert into
     * @param keys          the key columns
     * @param values        the value columns (if any)
     * @return              the merge statement
     */
    static std::string assembleMerge(const std::string &table,
                                     const Types::ColumnSet &keys,
                                     const Types::ColumnSet &values);


    /**
     * Assemble a standard SQL update statement.
     *
     * @param table         the table to update
     * @param columns       the columns to set
     * @param where         there where clause
     * @return              the update statement
     */
    static std::string assembleUpdate(const std::string &table,
                                       const Types::ColumnSet &columns,
                                       const std::string &where);

    /**
     * Assemble a standard SQL delete statement.
     *
     * @param table         the table to delete from
     * @param where         the where clause
     * @return              the delete statement
     */
    static std::string assembleDel(const std::string &table, const std::string &where);

public:
    Driver();

    virtual ~Driver();

    /**
     * Open the database backend.
     *
     * @return  true on success
     */
    virtual bool open() = 0;

    enum {
        /**
         * Flag the transaction as read only.
         */
                Transaction_ReadOnly = 0x01,

        /**
         * Flag the transaction as serializable, enforcing in order
         * appearance of changes.
         */
                Transaction_Serializable = 0x02,

        /**
         * The default transaction flags.
         */
                Transaction_Default = 0x0,
    };

    /**
     * Begin a trasnaction.  Nesting is not supported.
     *
     * @param flags     the transaction flags
     * @param timeout   the maximum time to wait for the transaction to start
     * @return          true on success
     */
    virtual bool beginTransaction(int flags = Transaction_Default,
                                  double timeout = FP::undefined()) = 0;

    /**
     * Commit the current transaction.
     *
     * @return          true on success
     */
    virtual bool commitTransaction() = 0;

    /**
     * Roll back the current transaction.
     */
    virtual void rollbackTransaction() = 0;

    /**
     * Load the set of tables defined in the database.
     *
     * @return          all defined tables
     */
    virtual std::unordered_set<std::string> loadTables() = 0;

    /**
     * Create a table in the database.
     *
     * @param table     the table definition
     * @return          true on success
     */
    virtual bool createTable(const TableDefinition &table) = 0;

    /**
     * Remove a table from the database.
     *
     * @param table     the table name
     * @return          true on success
     */
    virtual bool removeTable(const std::string &table) = 0;

    /**
     * Perform a database optimization.  This is an infrequent operation that is
     * expected to block access to the database while it is in progress.
     * <br>
     * This should generally not be executed in the context of a transaction.
     */
    virtual void optimizeDatabase();


    /**
     * A handle for an active query in the database.
     */
    class CPD3DATABASE_EXPORT Query {
    public:
        Query();

        virtual ~Query();

        /**
         * Start the query execution.
         */
        virtual void begin() = 0;

        /**
         * Test if the query has experienced an error.
         *
         * @return      true if there is no error
         */
        virtual bool isOk() = 0;

        /**
         * Advance the query to the next result row.
         *
         * @return      true if there is a next result
         */
        virtual bool advance() = 0;

        /**
         * Retrieve the values from the query.
         *
         * @return      the query values
         */
        virtual Types::Results values() = 0;

        /**
         * Finish the query.
         */
        virtual void end() = 0;
    };

    /**
     * An operation on the database.
     */
    class CPD3DATABASE_EXPORT Operation {
    public:
        Operation();

        virtual ~Operation();

        /**
         * Issue the operation to the database.
         *
         * @param binds         the binds in the operation
         * @param buffer        request that the results be buffered entirely instead of row by row
         * @return              a new query that can be started or null if there is an error
         */
        virtual Query *issue(const Types::Binds &binds = Types::Binds(), bool buffer = false) = 0;

        /**
         * Execute a batch operation.
         *
         * @param binds         the binds of the operation
         * @return              true on succcess
         */
        virtual bool batch(const Types::BatchBinds &binds) = 0;
    };

    /**
     * An operation that is always invalid, used for returning a failure in construction
     */
    class CPD3DATABASE_EXPORT InvalidOperation : public Operation {
    public:
        InvalidOperation();

        virtual ~InvalidOperation();

        Query *issue(const Types::Binds &binds = Types::Binds(), bool buffer = false) override;

        bool batch(const Types::BatchBinds &binds) override;
    };

    /**
     * Create a general purpose operation from a SQL statement.
     *
     * @param statement         the SQL statement
     * @param outputs           the expected number of outputs
     * @return                  the operation
     */
    virtual Operation *general(const std::string &statement, std::size_t outputs = 0) = 0;

    /**
     * Create a select operation.
     *
     * @param select        the pairs of parameters and names to select
     * @param from          the paris of tables and names to fetch from
     * @param where         the where clause
     * @param order         the order clause
     * @return              a new select operation
     */
    virtual Operation *select(const Types::TableSet &from,
                              const Types::Outputs &select,
                              const std::string &where = std::string(),
                              const std::string &order = std::string());

    /**
     * Create an insert operation.
     *
     * @param table         the table
     * @param columns       the columns to insert
     * @return              a new insertion operation
     */
    virtual Operation *insert(const std::string &table, const Types::ColumnSet &columns);

    /**
     * Create an insert operation that gets its values from a select.
     *
     * @param table         the table
     * @param columns       the columns to insert
     * @param select        the pairs of parameters and names to select
     * @param from          the paris of tables and names to fetch from
     * @param where         the where clause
     * @param order         the order clause
     * @return              a new insertion operation
     */
    virtual Operation *insertSelect(const std::string &table,
                                    const Types::Outputs &columns,
                                    const Types::TableSet &from,
                                    const Types::Outputs &select,
                                    const std::string &where = std::string(),
                                    const std::string &order = std::string());

    /**
     * Create a merge (upsert) operation.
     *
     * @param table         the table to insert into
     * @param keys          the key columns
     * @param values        the value columns (if any)
     * @return              a new merge operation
     */
    virtual Operation *merge(const std::string &table,
                             const Types::ColumnSet &keys,
                             const Types::ColumnSet &values);

    /**
     * Create a merge (upsert) operation, without specific key columns.
     *
     * @param table         the table to insert into
     * @param columns       the columns to insert
     * @return              a new merge operation
     */
    virtual Operation *merge(const std::string &table, const Types::ColumnSet &columns);

    /**
     * Create a update operation.
     *
     * @param table         the table to update
     * @param columns       the columns to set
     * @param where         there where clause
     * @return              a new update operation
     */
    virtual Operation *update(const std::string &table,
                              const Types::ColumnSet &columns,
                              const std::string &where = std::string());

    /**
     * Create a delete operation.
     *
     * @param table         the table to delete from
     * @param where         the where clause
     * @return              a new delete operation
     */
    virtual Operation *del(const std::string &table, const std::string &where = std::string());
};

}
}

#endif //CPD3DATABASE_DRIVER_HXX
