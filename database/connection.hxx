/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATABASE_CONNECTION_HXX
#define CPD3DATABASE_CONNECTION_HXX

#include "core/first.hxx"

#include <algorithm>
#include <utility>
#include <memory>
#include <unordered_set>
#include <string>
#include <limits>
#include <cctype>

#include "database.hxx"
#include "util.hxx"
#include "statement.hxx"
#include "context.hxx"

#include "core/number.hxx"
#include "core/util.hxx"

namespace CPD3 {
namespace Database {

class Driver;

class TableDefinition;

class ConnectionInterface;

class TransactionInterface;

/**
 * A connection to a database.
 */
class CPD3DATABASE_EXPORT Connection {

    Storage db;

    std::shared_ptr<ConnectionInterface> interface;

public:
    /**
     * Create the connection.
     *
     * @param db    the storage database
     */
    Connection(Storage &&db);

    /** @see Connection(Storage &&) */
    Connection(const Storage &db = Util::getDefaultDatabase());

    ~Connection();

    Connection(const Connection &) = delete;

    Connection &operator=(const Connection &) = delete;


    enum {
        /**
         * Flag the transaction as read only.
         */
                Transaction_ReadOnly = 0x01,

        /**
         * Flag the transaction as serializable, enforcing in order
         * appearance of changes.
         */
                Transaction_Serializable = 0x02,

        /**
         * The default transaction flags.
         */
                Transaction_Default = 0x0,
    };

    /**
     * A transaction context, only one context is permitted to exist at a time,
     * so as long as an initially valid one exists, further ones are blocked.
     */
    class CPD3DATABASE_EXPORT Transaction final {
        std::weak_ptr<TransactionInterface> active;
    public:
        /**
         * Create the transaction.  This waits for any existing transaction to end.  The
         * new transaction must immediately be checked for validity, before assuming
         * that operations can proceed.  After that check, operations will automatically
         * fail if the transaction is aborted, until it is destroyed.
         *
         * @param connection    the connection to create on
         * @param flags         the transaction flags
         * @param timeout       the maximum time to attempt to start the transaction
         */
        Transaction(Connection &connection,
                    int flags = Transaction_Default,
                    double timeout = FP::undefined());

        Transaction();

        ~Transaction();

        Transaction(Transaction &&other);

        Transaction &operator=(Transaction &&other);

        Transaction(const Transaction &) = delete;

        Transaction &operator=(const Transaction &) = delete;

        /**
         * Check if the transaction is valid, meaning that it excludes and encloses
         * all operations until is is destroyed or ended.
         *
         * @return  true if the transaction is valid
         */
        bool isValid() const;

        /**
         * Test if the transaction is in an error state.
         *
         * @return  true if the transaction is in an error state, preventing operations from succeeding
         */
        bool inError() const;

        /**
         * Abort the transaction, causing further operations to fail.
         */
        void abort();

        /**
         * End the transaction, after which is is no longer valid.
         *
         * @return  true if the transaction was successful
         */
        bool end();
    };

    friend class Transaction;

    /**
     * Start a transaction.  This waits for any existing transaction to end.  The
     * new transaction must immediately be checked for validity, before assuming
     * that operations can proceed.  After that check, operations will automatically
     * fail if the transaction is aborted, until it is destroyed.
     *
     * @param flags     the transaction flags
     * @param timeout   the maximum time to attempt to start the transaction
     */
    inline Transaction transaction(int flags = Transaction_Default,
                                   double timeout = FP::undefined())
    { return Transaction(*this, flags, timeout); }

    /**
     * The possible results from a synchronous transaction.
     */
    enum SynchronousResult {
        /**
         * The synchronous transaction completed successfully, try to commit it.
         */
                Synchronous_Ok,

        /**
         * The transaction has failed, roll is back and try again.
         */
                Synchronous_Failed,

        /**
         * The transaction has failed, roll it back and stop all retry attempts.
         */
                Synchronous_Abort,
    };

    /**
     * Execute a transaction block that can be retried, synchronously.
     *
     * @param transaction   the transaction block
     * @param flags         transaction flags
     * @param timeout       the maximum time to continue retrying
     * @return              the status
     */
    bool synchronousTransaction(const std::function<SynchronousResult()> &transaction,
                                int flags = Transaction_Default,
                                double timeout = FP::undefined());


    /**
     * The possible results from a required transaction.
     */
    enum RequiredResult {
        /**
         * The synchronous transaction completed successfully, try to commit it.
         */
                Required_Ok,

        /**
         * The transaction has failed, roll is back and try again.
         */
                Required_Failed,
    };

    /**
     * Execute a transaction block that can be retried, synchronously.  The transaction
     * is attempted until it succeeds, with any failure preventing a retry being fatal.
     *
     * @param transaction   the transaction block
     * @param flags         transaction flags
     */
    void requiredTransaction(const std::function<RequiredResult()> &transaction,
                             int flags = Transaction_Default | Transaction_Serializable);

    /**
     * Create a general purpose operation from a SQL statement.
     *
     * @param statement         the SQL statement
     * @param outputs           the expected number of outputs
     * @return                  the operation
     */
    Statement general(const std::string &statement, std::size_t outputs = 0);

    /**
     * Create a select operation.
     *
     * @param select        the pairs of parameters and names to select
     * @param from          the paris of tables and names to fetch from
     * @param where         the where clause
     * @param order         the order clause
     * @return              a new select operation
     */
    Statement select(const Types::TableSet &from,
                     const Types::Outputs &select,
                     const std::string &where = std::string(),
                     const std::string &order = std::string());

    /**
     * Create a select operation.
     *
     * @param select        the pairs of parameters and names to select
     * @param from          the table to select from
     * @param where         the where clause
     * @param order         the order clause
     * @return              a new select operation
     */
    inline Statement select(const std::string &from,
                            const Types::Outputs &select,
                            const std::string &where = std::string(),
                            const std::string &order = std::string())
    {
        Types::TableSet f;
        f.emplace(std::string(), from);
        return this->select(f, select, where, order);
    }

    /**
     * Create an insert operation.
     *
     * @param table         the table
     * @param columns       the columns to insert
     * @return              a new insertion operation
     */
    Statement insert(const std::string &table, const Types::ColumnSet &columns);

    /**
     * Create an insert operation that gets its values from a select.
     *
     * @param table         the table
     * @param columns       the columns to insert
     * @param select        the pairs of parameters and names to select
     * @param from          the paris of tables and names to fetch from
     * @param where         the where clause
     * @param order         the order clause
     * @return              a new insertion operation
     */
    Statement insertSelect(const std::string &table,
                           const Types::Outputs &columns,
                           const Types::TableSet &from,
                           const Types::Outputs &select,
                           const std::string &where = std::string(),
                           const std::string &order = std::string());

    /**
     * Create an insert operation that gets its values from a select.
     *
     * @param table         the table
     * @param columns       the columns to insert
     * @param select        the pairs of parameters and names to select
     * @param from          the table to select from
     * @param where         the where clause
     * @param order         the order clause
     * @return              a new insertion operation
     */
    inline Statement insertSelect(const std::string &table,
                                  const Types::Outputs &columns,
                                  const std::string &from,
                                  const Types::Outputs &select,
                                  const std::string &where = std::string(),
                                  const std::string &order = std::string())
    {
        Types::TableSet f;
        f.emplace(std::string(), from);
        return this->insertSelect(table, columns, f, select, where, order);
    }

    /**
     * Create a merge (upsert) operation.
     *
     * @param table         the table to insert into
     * @param keys          the key columns
     * @param values        the value columns (if any)
     * @return              a new merge operation
     */
    Statement merge(const std::string &table,
                    const Types::ColumnSet &keys,
                    const Types::ColumnSet &values);

    /**
     * Create a merge (upsert) operation, without specific key columns.
     *
     * @param table         the table to insert into
     * @param columns       the columns to insert
     * @return              a new merge operation
     */
    Statement merge(const std::string &table, const Types::ColumnSet &columns);

    /**
     * Create a update operation.
     *
     * @param table         the table to update
     * @param columns       the columns to set
     * @param where         there where clause
     * @return              a new update operation
     */
    Statement update(const std::string &table,
                     const Types::ColumnSet &columns,
                     const std::string &where = std::string());

    /**
     * Create a delete operation.
     *
     * @param table         the table to delete from
     * @param where         the where clause
     * @return              a new delete operation
     */
    Statement del(const std::string &table, const std::string &where = std::string());


    /**
     * Create a table in the database.
     *
     * @param table     the table definition
     * @return          true on success
     */
    void createTable(const TableDefinition &table);

    /**
     * Remove a table from the database.
     *
     * @param table     the table definition
     * @return          true on success
     */
    void removeTable(const std::string &table);

    /**
     * Get the set of defined tables.
     *
     * @return          the tables
     */
    std::unordered_set<std::string> tables();

    /**
     * Get the filtered set of tables.
     *
     * @tparam Functor  the filter type
     * @param filter    the filter functor
     * @return          the filtered tables
     */
    template<typename Functor>
    inline std::unordered_set<std::string> tables(Functor filter)
    {
        std::unordered_set<std::string> result;
        std::unordered_set<std::string> input(tables());
        std::copy_if(input.begin(), input.end(), std::inserter(result, result.begin()), filter);
        return result;
    }

    /**
     * Test if a specific table is defined.
     *
     * @param table     the table name
     * @return
     */
    inline bool hasTable(const std::string &table)
    {
        return !(tables([&table](const std::string &t) -> bool {
            return CPD3::Util::equal_insensitive(table, t);
        })).empty();
    }


    /**
     * Start the connection.
     *
     * @return  true on success
     */
    bool start();

    /**
     * Test if the connection has completed.
     *
     * @return  true if the connection has completed
     */
    bool isFinished();

    /**
     * Wait for the connection to finish.
     *
     * @param timeout   the maximum time to wait
     * @return          true if the connection has completed
     */
    bool wait(double timeout = FP::undefined());

    /**
     * Shutdown the connection.
     *
     * @param waitForTransaction    wait for the current transaction to end instead of aborting it
     */
    void shutdown(bool waitForTransaction = true);

    /**
     * Immediately terminate the connection, aborting the transaction if required.
     */
    void signalTerminate();
};

}
}

#endif //CPD3DATABASE_CONNECTION_HXX
