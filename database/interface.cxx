/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QLoggingCategory>

#include "interface.hxx"
#include "connection.hxx"
#include "core/timeutils.hxx"


Q_LOGGING_CATEGORY(log_database_interface, "cpd3.database.interface", QtWarningMsg)

namespace CPD3 {
namespace Database {

ConnectionInterface::ConnectionInterface() : threadState(Thread_Initialize)
{ }

bool ConnectionInterface::isFinished()
{
    std::lock_guard<std::mutex> lock(mutex);
    return threadState == Thread_Complete;
}

bool ConnectionInterface::wait(double timeout)
{
    return Threading::waitForTimeout(timeout, mutex, response,
                                     [this] { return threadState == Thread_Complete; });
}

void ConnectionInterface::shutdown(bool waitForTransaction)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        switch (threadState) {
        case Thread_Initialize:
            response.wait(lock, [this] { return threadState != Thread_Initialize; });
            switch (threadState) {
            case Thread_Initialize:
                Q_ASSERT(false);
                break;
            case Thread_Active:
                threadState = waitForTransaction ? Thread_TerminateWaitForTransaction
                                                 : Thread_TerminateAbort;
                break;
            case Thread_TerminateWaitForTransaction:
                if (waitForTransaction)
                    return;
                threadState = Thread_TerminateAbort;
                break;
            case Thread_TerminateAbort:
            case Thread_Complete:
                return;
            }
            return;
        case Thread_Active:
            threadState =
                    waitForTransaction ? Thread_TerminateWaitForTransaction : Thread_TerminateAbort;
            break;
        case Thread_TerminateWaitForTransaction:
            if (waitForTransaction)
                return;
            threadState = Thread_TerminateAbort;
            break;
        case Thread_TerminateAbort:
        case Thread_Complete:
            return;
        }
    }
    command.notify_all();
}

void ConnectionInterface::run(const std::shared_ptr<ConnectionInterface> &self,
                              std::unique_ptr<Driver> &driver)
{
    std::unique_lock<std::mutex> lock(self->mutex);
    for (;;) {
        switch (self->threadState) {
        case Thread_Initialize:
            self->threadState = Thread_Active;
            self->response.notify_all();
            /* Fall through */
        case Thread_Active:
            break;
        case Thread_TerminateWaitForTransaction:
            if (!self->activeTransaction) {
                self->threadFinish(driver.get(), lock);

                /* Do this again here because so that any pending operations that need to access
                 * the connection to be destroyed are run before it's released */
                lock.lock();
                self->releaseQueue.clear();
                lock.unlock();

                driver.reset();

                lock.lock();
                self->threadState = Thread_Complete;
                self->response.notify_all();

                std::vector<std::unique_ptr<BaseRelease>> toRelease = std::move(self->releaseQueue);
                self->releaseQueue.clear();
                lock.unlock();
                toRelease.clear();
                return;
            }
            break;
        case Thread_TerminateAbort: {
            bool doRollback = false;
            if (self->activeTransaction) {
                if (self->activeTransaction->terminate())
                    doRollback = true;
            }
            self->threadFinish(driver.get(), lock);

            /* Do this again here because so that any pending operations that need to access
             * the connection to be destroyed are run before it's released */
            lock.lock();
            self->releaseQueue.clear();
            lock.unlock();

            if (doRollback)
                driver->rollbackTransaction();
            driver.reset();

            lock.lock();
            /* Don't reset it until after the thread finish, so any pending operation
             * still sees one active */
            self->activeTransaction.reset();

            self->threadState = Thread_Complete;
            self->response.notify_all();

            std::vector<std::unique_ptr<BaseRelease>> toRelease = std::move(self->releaseQueue);
            self->releaseQueue.clear();
            lock.unlock();
            toRelease.clear();
            return;
        }
        case Thread_Complete:
            Q_ASSERT(false);
            return;
        }

        if (!self->processingInterfaces.empty()) {
            std::vector<std::weak_ptr<BaseInterface>>
                    interfaces = std::move(self->processingInterfaces);
            self->processingInterfaces.clear();

            lock.unlock();
            for (const auto &i : interfaces) {
                auto ref = i.lock();
                if (!ref)
                    continue;
                ref->process(driver.get());
            }
            interfaces.clear();
            lock.lock();
            continue;
        }

        if (self->synchronousOperation) {
            if (self->synchronousOperation(driver.get(), lock)) {
                if (!lock)
                    lock.lock();
                self->synchronousOperation = Operation();
                self->response.notify_all();
                continue;
            }
            Q_ASSERT(lock);
            self->synchronousOperation = Operation();
            self->response.notify_all();
        }

        if (!self->releaseQueue.empty()) {
            std::vector<std::unique_ptr<BaseRelease>> toRelease = std::move(self->releaseQueue);
            self->releaseQueue.clear();
            lock.unlock();
            toRelease.clear();
            lock.lock();
            continue;
        }

        for (auto i = self->threadedInterfaces.begin(), endI = self->threadedInterfaces.end();
                i != endI;) {
            if (i->expired()) {
                i = self->threadedInterfaces.erase(i);
            } else {
                ++i;
            }
        }

        self->command.wait(lock);
    }
}

void ConnectionInterface::transactionEnd(std::unique_lock<std::mutex> &lock)
{

    std::vector<std::shared_ptr<BaseInterface>> toEnd;
    for (const auto &i : threadedInterfaces) {
        auto l = i.lock();
        if (!l)
            continue;
        toEnd.emplace_back(std::move(l));
    }

    if (toEnd.empty())
        return;

    lock.unlock();

    for (const auto &ctx : toEnd) {
        ctx->transactionEnd();
    }
    toEnd.clear();

    lock.lock();
}

void ConnectionInterface::threadFinish(Driver *driver, std::unique_lock<std::mutex> &lock)
{
    Q_ASSERT(threadState != Thread_Complete);

    std::list<std::weak_ptr<BaseInterface>> tInterfaces = std::move(threadedInterfaces);
    threadedInterfaces.clear();

    Operation releaseOp = synchronousOperation;
    /* Do not clear the synchronous op, so nothing else tries to acquire it */

    if (releaseOp)
        releaseOp(driver, lock);

    if (lock)
        lock.unlock();

    for (const auto &i : tInterfaces) {
        auto ref = i.lock();
        if (!ref)
            continue;
        ref->threadFinish();
    }
    tInterfaces.clear();
}

bool ConnectionInterface::canOperate() const
{
    switch (threadState) {
    case Thread_Initialize:
    case Thread_Active:
        break;
    case Thread_TerminateWaitForTransaction:
        if (!activeTransaction)
            return false;
        break;
    case Thread_TerminateAbort:
    case Thread_Complete:
        return false;
    }

    return true;
}

bool ConnectionInterface::runOnTransaction(const std::function<bool(Driver *)> &operation)
{
    std::unique_lock<std::mutex> lock(mutex);
    enum {
        Waiting, InProgress, Complete, Failed,
    } state = Waiting;
    for (;;) {
        if (!canOperate())
            return false;

        Q_ASSERT(activeTransaction);

        switch (state) {
        case Waiting:
            if (!synchronousOperation) {
                state = InProgress;
                synchronousOperation =
                        [&](Driver *driver, std::unique_lock<std::mutex> &lock) -> bool {
                            Q_ASSERT(state == InProgress);
                            Q_ASSERT(activeTransaction);

                            if (!activeTransaction->isValid()) {
                                state = Failed;
                                response.notify_all();
                                return false;
                            }

                            lock.unlock();
                            if (operation(driver)) {
                                lock.lock();
                                state = Complete;
                                return true;
                            }

                            lock.lock();
                            activeTransaction->terminate();
                            transactionEnd(lock);
                            lock.unlock();

                            driver->rollbackTransaction();

                            lock.lock();
                            state = Failed;
                            return true;
                        };
                command.notify_all();
                break;
            }
            break;
        case InProgress:
            break;
        case Complete:
            return true;
        case Failed:
            return false;
        }
        response.wait(lock);
    }
}

void ConnectionInterface::synchronizeTransactionEnd(std::unique_lock<std::mutex> &lock)
{
    enum {
        Waiting, InProgress, Complete,
    } state = Waiting;
    for (;;) {
        switch (threadState) {
        case Thread_Initialize:
        case Thread_Active:
        case Thread_TerminateWaitForTransaction:
            break;
        case Thread_Complete:
        case Thread_TerminateAbort:
            /* We can't finish it on this this thread, but it should have
             * received the thread end signal from the actual database
             * thread */
            return;
        }

        Q_ASSERT(activeTransaction);

        switch (state) {
        case Waiting:
            if (!synchronousOperation) {
                state = InProgress;
                synchronousOperation = [&](Driver *, std::unique_lock<std::mutex> &lock) -> bool {
                    Q_ASSERT(state == InProgress);
                    transactionEnd(lock);
                    state = Complete;
                    return true;
                };
                command.notify_all();
                break;
            }
            break;
        case InProgress:
            break;
        case Complete:
            return;
        }
        response.wait(lock);
    }
}

void ConnectionInterface::runFromTransaction(Driver *driver,
                                             const std::function<bool()> &operation,
                                             const std::function<void(bool)> &complete)
{
    std::unique_lock<std::mutex> lock(mutex);
    if (!activeTransaction || !activeTransaction->isValid()) {
        lock.unlock();
        complete(false);
        return;
    }

    lock.unlock();
    bool ok = operation();

    if (!ok) {
        lock.lock();
        Q_ASSERT(activeTransaction);
        activeTransaction->terminate();
        transactionEnd(lock);
        lock.unlock();

        driver->rollbackTransaction();

        response.notify_all();
    }

    complete(ok);
}

Statement ConnectionInterface::createStatement(const std::function<
        StatementInterface *(Driver *)> &create)
{
    /* Have to construct before the lock, so a destroy on out of scope
     * doesn't have the lock held (the destructor may lock) */
    typedef std::shared_ptr<StatementInterface> Ptr;
    Ptr ptr;

    std::unique_lock<std::mutex> lock(mutex);
    ptr = resultSynchronousUnlocked<Ptr>(lock, [this, &create](Driver *driver) -> Ptr {
        StatementInterface *res = create(driver);
        if (!res)
            return Ptr();
        return Ptr(res);
    }, [this](const Ptr &ptr) {
        if (!ptr)
            return;
        threadedInterfaces.emplace_back(ptr);
    });

    if (!ptr)
        return Statement();
    return Statement(std::move(ptr));
}

std::shared_ptr<
        ContextInterface> ConnectionInterface::executeStatement(const StatementInterface *statement,
                                                                const Types::Binds &binds,
                                                                bool buffer)
{
    /* Have to construct before the lock, so a destroy on out of scope
     * doesn't have the lock held (the destructor may lock) */
    std::shared_ptr<ContextInterface> result;

    std::unique_lock<std::mutex> lock(mutex);
    enum {
        Waiting, InProgress, Complete,
    } state = Waiting;
    for (;;) {
        if (!canOperate())
            return std::shared_ptr<ContextInterface>();

        Q_ASSERT(activeTransaction);

        switch (state) {
        case Waiting:
            if (!synchronousOperation) {
                state = InProgress;
                synchronousOperation =
                        [&](Driver *driver, std::unique_lock<std::mutex> &lock) -> bool {
                            Q_ASSERT(state == InProgress);
                            Q_ASSERT(activeTransaction);

                            if (!activeTransaction->isValid()) {
                                state = Complete;
                                response.notify_all();
                                return false;
                            }

                            auto op = statement->operation.get();
                            if (!op) {
                                state = Complete;
                                response.notify_all();
                                return false;
                            }

                            lock.unlock();

                            /* Release it now if required, while the lock in unowned */
                            result.reset();

                            auto q = op->issue(binds, buffer);
                            if (q) {
                                lock.lock();
                                result = std::shared_ptr<ContextInterface>(
                                        new ContextInterface(shared_from_this(), q));
                                threadedInterfaces.emplace_back(result);
                                state = Complete;
                                return true;
                            }

                            lock.lock();
                            activeTransaction->terminate();
                            transactionEnd(lock);
                            lock.unlock();

                            driver->rollbackTransaction();

                            lock.lock();
                            state = Complete;
                            return true;
                        };
                command.notify_all();
                break;
            }
            break;
        case InProgress:
            break;
        case Complete:
            return result;
        }
        response.wait(lock);
    }
}

void ConnectionInterface::queueForRelease(BaseRelease *release)
{
    std::unique_lock<std::mutex> lock(mutex);
    switch (threadState) {
    case Thread_Initialize:
    case Thread_Active:
    case Thread_TerminateWaitForTransaction:
    case Thread_TerminateAbort:
        break;
    case Thread_Complete:
        /* Deletion may call into a database backend and lock, so we can't
         * hold the lock while doing it */
        lock.unlock();
        delete release;
        return;
    }
    releaseQueue.emplace_back(release);
    command.notify_all();
}


TransactionInterface::TransactionInterface(ConnectionInterface &c) : connection(c),
                                                                     activeState(Initialize)
{ }

bool TransactionInterface::initialize(std::unique_lock<std::mutex> &lock, int flags, double timeout)
{
    int driverFlags = 0;
    if (flags & Connection::Transaction_ReadOnly)
        driverFlags |= Driver::Transaction_ReadOnly;
    if (flags & Connection::Transaction_Serializable)
        driverFlags |= Driver::Transaction_Serializable;

    Q_ASSERT(connection.activeTransaction.get() == this);

    return connection.resultSynchronousUnlocked<bool>(lock,
                                                      [this, driverFlags, timeout](Driver *driver) -> bool {
                                                          return driver->beginTransaction(
                                                                  driverFlags, timeout);
                                                      }, [this](bool ok) {
                activeState = ok ? Valid : Invalid;
            }, false);
}

std::weak_ptr<TransactionInterface> TransactionInterface::acquire(ConnectionInterface &connection,
                                                                  int flags,
                                                                  double timeout)
{
    double timeOfEnd = FP::undefined();
    if (FP::defined(timeout))
        timeOfEnd = Time::time() + timeout;

    std::unique_lock<std::mutex> lock(connection.mutex);
    for (;;) {
        if (!connection.canOperate())
            return std::weak_ptr<TransactionInterface>();

        if (connection.activeTransaction) {
            if (FP::defined(timeOfEnd)) {
                double remaining = timeOfEnd - Time::time();
                if (remaining <= 0.0)
                    return std::weak_ptr<TransactionInterface>();
                connection.response
                          .wait_for(lock, std::chrono::duration<double>(remaining),
                                    [&] { return !connection.activeTransaction; });
            } else {
                connection.response.wait(lock, [&] { return !connection.activeTransaction; });
            }
            continue;
        }

        connection.activeTransaction = std::make_shared<TransactionInterface>(connection);

        double remaining = FP::undefined();
        if (FP::defined(timeOfEnd))
            remaining = timeOfEnd - Time::time();
        if (connection.activeTransaction->initialize(lock, flags, remaining)) {
            return std::weak_ptr<TransactionInterface>(connection.activeTransaction);
        } else {
            if (!lock.owns_lock())
                lock.lock();
            connection.activeTransaction.reset();
            connection.response.notify_all();
            connection.command.notify_all();
            if (FP::defined(remaining) && remaining <= 0.0)
                return std::weak_ptr<TransactionInterface>();
        }
    }
}

bool TransactionInterface::terminate()
{
    if (activeState != Valid)
        return false;
    activeState = Invalid;
    return true;
}

void TransactionInterface::release()
{
    std::unique_lock<std::mutex> lock(connection.mutex);
    Q_ASSERT(connection.activeTransaction.get() == this);

    connection.synchronizeTransactionEnd(lock);

    if (activeState == Valid) {
        connection.resultSynchronousUnlocked<bool>(lock, [](Driver *driver) -> bool {
            driver->rollbackTransaction();
            return true;
        }, [this](bool) {
            activeState = Invalid;
        });
    }
    connection.activeTransaction.reset();
    connection.command.notify_all();
    connection.response.notify_all();
}

bool TransactionInterface::inError()
{
    std::lock_guard<std::mutex> lock(connection.mutex);
    return activeState != Valid;
}

bool TransactionInterface::isValid() const
{
    return activeState == Valid;
}

void TransactionInterface::abort()
{
    release();
}

bool TransactionInterface::endTransaction()
{
    std::unique_lock<std::mutex> lock(connection.mutex);
    Q_ASSERT(connection.activeTransaction.get() == this);

    connection.synchronizeTransactionEnd(lock);

    if (activeState != Valid) {
        connection.activeTransaction.reset();
        connection.response.notify_all();
        connection.command.notify_all();
        return false;
    }

    bool ok = connection.resultSynchronousUnlocked<bool>(lock, [this](Driver *driver) -> bool {
        if (driver->commitTransaction())
            return true;
        driver->rollbackTransaction();
        return false;
    }, [this](bool) {
        activeState = Invalid;
    }, false);

    connection.activeTransaction.reset();
    connection.response.notify_all();
    connection.command.notify_all();
    return ok;
}


BaseRelease::BaseRelease() = default;

BaseRelease::~BaseRelease() = default;


BaseInterface::BaseInterface(const std::shared_ptr<ConnectionInterface> &c) : connection(c)
{ }

BaseInterface::BaseInterface() = default;

BaseInterface::~BaseInterface() = default;

void BaseInterface::threadFinish()
{ }

void BaseInterface::transactionEnd()
{ }

void BaseInterface::process(Driver *)
{ }

bool BaseInterface::enqueueForProcessing()
{
    auto c = connection.lock();
    if (!c)
        return false;
    std::lock_guard<std::mutex> lock(c->mutex);
    if (!c->canOperate())
        return false;
#if CPD3_CXX >= 201700L
    c->processingInterfaces.emplace_back(weak_from_this());
#else
    c->processingInterfaces.emplace_back(shared_from_this());
#endif
    /* Have to hold the lock, since it might process us and cause deletion
     * via the connection thread between the lock release and the notify */
    c->command.notify_all();
    return true;
}


StatementInterface::StatementInterface(const std::shared_ptr<ConnectionInterface> &connection,
                                       Driver::Operation *o) : BaseInterface(connection),
                                                               operation(o)
{ }

namespace {
template<typename T>
class UniqueRelease : public BaseRelease {
    std::unique_ptr<T> release;
public:
    UniqueRelease(std::unique_ptr<T> &&r) : release(std::move(r))
    { }

    virtual ~UniqueRelease() = default;
};
}

StatementInterface::~StatementInterface()
{
    if (!operation)
        return;
    auto c = connection.lock();
    if (!c)
        return;
    c->queueForRelease(new UniqueRelease<Driver::Operation>(std::move(operation)));
}

void StatementInterface::threadFinish()
{ operation.reset(); }

void StatementInterface::batch(const Types::BatchBinds &binds)
{
    auto c = connection.lock();
    if (!c)
        return;
    c->runOnTransaction([this, &binds](Driver *) -> bool {
        if (!operation)
            return false;
        return operation->batch(binds);
    });
}

std::shared_ptr<ContextInterface> StatementInterface::execute(const Types::Binds &binds) const
{
    auto c = connection.lock();
    if (!c)
        return std::shared_ptr<ContextInterface>();
    return c->executeStatement(this, binds);
}

void StatementInterface::synchronous(const Types::Binds &binds)
{
    auto c = connection.lock();
    if (!c)
        return;
    c->runOnTransaction([this, &binds](Driver *) -> bool {
        if (!operation)
            return false;
        std::unique_ptr<Driver::Query> q(operation->issue(binds, true));
        if (!q)
            return false;
        q->begin();
        q->end();
        return q->isOk();
    });
}

Types::Results StatementInterface::extract(const Types::Binds &binds, bool failOnEmpty)
{
    auto c = connection.lock();
    if (!c)
        return Types::Results();
    Types::Results result;
    c->runOnTransaction([&](Driver *) -> bool {
        if (!operation)
            return false;
        std::unique_ptr<Driver::Query> q(operation->issue(binds, true));
        if (!q)
            return false;
        q->begin();
        if (!q->advance()) {
            if (failOnEmpty)
                return false;
            /* No results is fine */
            return q->isOk();
        }
        result = q->values();
        q->end();
        return q->isOk();
    });
    return result;
}


ContextInterface::ContextInterface(const std::shared_ptr<ConnectionInterface> &connection,
                                   Driver::Query *q) : BaseInterface(connection),
                                                       query(q),
                                                       state(Initialize),
                                                       readyForNext(0)
{
    Q_ASSERT(query);
}

ContextInterface::ContextInterface() : BaseInterface(), query(), state(Fault), readyForNext(0)
{ }

ContextInterface::~ContextInterface()
{
    if (!query)
        return;
    auto c = connection.lock();
    if (!c)
        return;
    c->queueForRelease(new UniqueRelease<Driver::Query>(std::move(query)));
}

void ContextInterface::threadFinish()
{
    transactionEnd();
    stateChange.notify_all();
}

void ContextInterface::process(Driver *driver)
{
    if (!query) {
        Q_ASSERT(state == QueryComplete || state == Fault);
        return;
    }
    auto c = connection.lock();
    Q_ASSERT(c);
    std::unique_lock<std::mutex> lock(mutex);
    switch (state) {
    case Initialize:
    case Fault:
        return;
    case StartQuery: {
        state = QueryRunning;
        std::uint_fast32_t doNext = readyForNext;
        readyForNext = 0;
        lock.unlock();
        c->runFromTransaction(driver, [this, doNext]() -> bool {
            query->begin();
            if (!query->isOk())
                return false;
            return resultLoop(doNext);
        }, [this](bool ok) {
            if (ok)
                return;
            {
                std::lock_guard<std::mutex> lock(mutex);
                if (state == QueryComplete)
                    return;
                state = QueryComplete;
            }
            complete();
            stateChange.notify_all();
        });
        break;
    }
    case StartAndFinish: {
        state = QueryFinishing;
        std::uint_fast32_t doNext = readyForNext;
        readyForNext = 0;
        lock.unlock();
        c->runFromTransaction(driver, [this, doNext]() -> bool {
            query->begin();
            if (!query->isOk())
                return false;
            return resultLoop(doNext);
        }, [this](bool ok) {
            if (ok)
                return;
            {
                std::lock_guard<std::mutex> lock(mutex);
                if (state == QueryComplete)
                    return;
                state = QueryComplete;
            }
            complete();
            stateChange.notify_all();
        });

        lock.lock();
        if (state != QueryFinishing)
            return;
        /* Fall through */
    }
    case QueryFinishing: {
        std::uint_fast32_t doNext = readyForNext;
        readyForNext = 0;
        lock.unlock();
        c->runFromTransaction(driver, [this, doNext]() -> bool {
            if (!resultLoop(doNext))
                return false;
            query->end();
            return query->isOk();
        }, [this](bool) {
            {
                std::lock_guard<std::mutex> lock(mutex);
                if (state == QueryComplete)
                    return;
                state = QueryComplete;
            }
            complete();
            stateChange.notify_all();
        });
        break;
    }
    case QueryRunning: {
        std::uint_fast32_t doNext = readyForNext;
        readyForNext = 0;
        if (!doNext)
            return;
        lock.unlock();
        c->runFromTransaction(driver, std::bind(&ContextInterface::resultLoop, this, doNext),
                              [this](bool ok) {
                                  if (ok)
                                      return;
                                  {
                                      std::lock_guard<std::mutex> lock(mutex);
                                      if (state == QueryComplete)
                                          return;
                                      state = QueryComplete;
                                  }
                                  complete();
                                  stateChange.notify_all();
                              });
        break;
    }
    case QueryComplete:
        break;
    }
}

void ContextInterface::transactionEnd()
{
    query.reset();

    std::unique_lock<std::mutex> lock(mutex);
    switch (state) {
    case Initialize:
        state = Fault;
        stateChange.notify_all();
        break;
    case Fault:
        break;
    case StartQuery:
    case StartAndFinish:
    case QueryRunning:
    case QueryFinishing:
        state = QueryComplete;
        lock.unlock();
        stateChange.notify_all();
        complete();
        return;
    case QueryComplete:
        break;
    }
}

bool ContextInterface::resultLoop(std::uint_fast32_t n)
{
    for (; n; --n) {
        if (query->advance()) {
            result(query->values());
            continue;
        }
        if (!query->isOk())
            return false;

        if (n != 1) {
            qCDebug(log_database_interface) << "Query advanced past end, with" << (n - 1)
                                            << "remaining value(s)";
            return false;
        }

        {
            std::lock_guard<std::mutex> lock(mutex);
            /* The result may have caused a query finish */
            if (state == QueryComplete)
                return true;
            state = QueryComplete;
        }
        stateChange.notify_all();
        complete();
        return true;
    }

    return true;
}

void ContextInterface::begin(std::uint_fast32_t read)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        switch (state) {
        case Initialize:
            state = StartQuery;
            readyForNext = read;
            break;
        case StartQuery:
        case StartAndFinish:
        case QueryRunning:
        case QueryFinishing:
            Q_ASSERT(false);
            return;
        case Fault:
            state = QueryComplete;
            lock.unlock();
            stateChange.notify_all();
            complete.defer();
            return;
        case QueryComplete:
            return;
        }
    }
    if (!enqueueForProcessing()) {
        {
            std::lock_guard<std::mutex> lock(mutex);
            if (state == QueryComplete)
                return;
            state = QueryComplete;
        }
        stateChange.notify_all();
        complete.defer();
    }
}

void ContextInterface::next()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        switch (state) {
        case StartQuery:
        case QueryRunning:
            if (readyForNext++)
                return;
            break;
        case Initialize:
        case Fault:
        case StartAndFinish:
        case QueryFinishing:
            //Q_ASSERT(false);
            return;
        case QueryComplete:
            return;
        }
    }
    if (!enqueueForProcessing()) {
        {
            std::lock_guard<std::mutex> lock(mutex);
            if (state == QueryComplete)
                return;
            state = QueryComplete;
        }
        stateChange.notify_all();
        complete.defer();
    }
}

void ContextInterface::finish()
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        switch (state) {
        case StartQuery:
            state = StartAndFinish;
            break;
        case QueryRunning:
            state = QueryFinishing;
            break;
        case Initialize:
        case Fault:
            state = QueryComplete;
            lock.unlock();
            stateChange.notify_all();
            complete.defer();
            return;
        case StartAndFinish:
        case QueryFinishing:
            //Q_ASSERT(false);
            return;
        case QueryComplete:
            return;
        }
    }
    if (!enqueueForProcessing()) {
        {
            std::lock_guard<std::mutex> lock(mutex);
            if (state == QueryComplete)
                return;
            state = QueryComplete;
        }
        stateChange.notify_all();
        complete.defer();
    }
}

void ContextInterface::wait()
{
    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        switch (state) {
        case QueryComplete:
        case Fault:
            return;
        case Initialize:
        case StartQuery:
        case StartAndFinish:
        case QueryRunning:
        case QueryFinishing:
            break;
        }
        if (connection.expired())
            return;
        stateChange.wait(lock);
    }
}

}
}