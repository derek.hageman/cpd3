/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3DATABASE_HXX
#define CPD3DATABASE_HXX

#include <QtCore/QtGlobal>

#if defined(cpd3database_EXPORTS)
#   define CPD3DATABASE_EXPORT Q_DECL_EXPORT
#else
#   define CPD3DATABASE_EXPORT Q_DECL_IMPORT
#endif

#endif
