/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QLoggingCategory>

#include "tabledefinition.hxx"
#include "core/util.hxx"
#include "core/qtcompat.hxx"


Q_LOGGING_CATEGORY(log_database_tabledefinition, "cpd3.database.tabledefinition", QtWarningMsg)


namespace CPD3 {
namespace Database {

TableDefinition::TableDefinition(const std::string &n) : name(n)
{ }

void TableDefinition::column(const std::string &name,
                             CPD3::Database::TableDefinition::Type type,
                             bool allowNull,
                             int width)
{ columns.emplace_back(name, type, allowNull, width); }

void TableDefinition::primaryKey(const std::vector<std::string> &columns)
{ primary = KeyDefinition(columns, this->columns); }

void TableDefinition::index(const std::string &name, const std::vector<std::string> &columns)
{ indices.emplace(name, KeyDefinition(columns, this->columns)); }

TableDefinition::ColumnDefinition::ColumnDefinition(const std::string &n,
                                                    TableDefinition::Type t,
                                                    bool a,
                                                    int w) : name(n),
                                                             type(t),
                                                             allowNull(a),
                                                             width(w)
{ }

TableDefinition::ColumnDefinition::ColumnDefinition() = default;


TableDefinition::KeyDefinition::KeyDefinition() = default;

TableDefinition::KeyDefinition::KeyDefinition(const std::vector<std::size_t> &i) : indices(i)
{ }

TableDefinition::KeyDefinition::KeyDefinition(const std::vector<std::string> &names,
                                              const std::vector<
                                                      TableDefinition::ColumnDefinition> &columns)
        : indices()
{
    for (const auto &name : names) {
        std::size_t index = static_cast<std::size_t>(-1);
        for (std::size_t i = 0, max = columns.size(); i < max; ++i) {
            if (!Util::equal_insensitive(name, columns[i].name))
                continue;
            index = i;
            break;
        }
        if (index == static_cast<std::size_t>(-1)) {
            qCWarning(log_database_tabledefinition) << "Column" << name
                                                    << "not found in definition";
            continue;
        }

        if (columns[index].allowNull) {
            qCWarning(log_database_tabledefinition) << "Null column (" << name
                                                    << ") in key definition will likely result in an error";
        }
        indices.emplace_back(index);
    }
}

}
}