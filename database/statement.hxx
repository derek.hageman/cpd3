/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATABASE_STATEMENT_HXX
#define CPD3DATABASE_STATEMENT_HXX

#include "core/first.hxx"

#include <unordered_set>
#include <memory>
#include <string>

#include "database.hxx"
#include "context.hxx"
#include "util.hxx"

namespace CPD3 {
namespace Database {

class ConnectionInterface;

class StatementInterface;

/**
 * A statement for execution on a database connection.
 */
class CPD3DATABASE_EXPORT Statement final {
    std::shared_ptr<StatementInterface> interface;

    friend class ConnectionInterface;

    explicit Statement(std::shared_ptr<StatementInterface> &&interface);

public:
    Statement(const Statement &other);

    Statement(Statement &&other);

    Statement &operator=(const Statement &other);

    Statement &operator=(Statement &&other);

    /**
     * Test if the statement is valid.
     *
     * @return  true if the statement is valid
     */
    operator bool() const;

    Statement();

    ~Statement();

    /**
     * Initiate statement execution, this will not produce any results until
     * the context is started.
     *
     * @param binds     the bindings
     * @param buffer    buffer the entire result, if possible
     * @return          the execution context
     */
    Context execute(const Types::Binds &binds = Types::Binds(), bool buffer = false) const;

    /**
     * Execute the statement synchronously, returning when it has completed.
     *
     * @param binds     the bindings
     */
    void synchronous(const Types::Binds &binds = Types::Binds()) const;

    /**
     * Execute the statement in batch mode.
     *
     * @param binds     the row bindings
     */
    void batch(const Types::BatchBinds &binds) const;

    /**
     * Execute the statement synchronously, returning the first result when it has completed.
     *
     * @param binds         the bindings
     * @param failOnEmpty   if set, then fail the transaction if the result is empty
     * @return              the resulting data
     */
    Types::Results extract(const Types::Binds &binds = Types::Binds(),
                           bool failOnEmpty = false) const;

    /**
     * Execute the statement synchronously, returning the first result when it has completed.
     *
     * @param binds         the bindings
     * @param failOnEmpty   if set, then fail the transaction if the result is empty
     * @return              the first column of the first result or default constructed if none
     */
    Types::Results::value_type single(const Types::Binds &binds = Types::Binds(),
                                      bool failOnEmpty = false) const;
};

}
}

#endif //CPD3_STATEMENT_HXX
