/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include "statement.hxx"
#include "interface.hxx"

namespace CPD3 {
namespace Database {


Statement::Statement(std::shared_ptr<StatementInterface> &&in) : interface(std::move(in))
{ }

Statement::~Statement()
{ }

Statement::Statement() : interface()
{ }

Statement::Statement(const Statement &other) : interface(other.interface)
{ }

Statement &Statement::operator=(const Statement &other)
{
    interface = other.interface;
    return *this;
}

Statement::Statement(Statement &&other) : interface(std::move(other.interface))
{ }

Statement &Statement::operator=(Statement &&other)
{
    interface = std::move(other.interface);
    return *this;
}

Statement::operator bool() const
{ return !!interface; }

void Statement::batch(const Types::BatchBinds &binds) const
{
    if (!interface)
        return;
    return interface->batch(binds);
}

Context Statement::execute(const Types::Binds &binds, bool buffer) const
{
    if (!interface)
        return Context();
    auto ptr = interface->execute(binds);
    if (!ptr)
        return Context();
    return Context(std::move(ptr));
}

void Statement::synchronous(const Types::Binds &binds) const
{
    if (!interface)
        return;
    return interface->synchronous(binds);
}

Types::Results Statement::extract(const Types::Binds &binds, bool failOnEmpty) const
{
    if (!interface)
        return Types::Results();
    return interface->extract(binds, failOnEmpty);
}

Types::Results::value_type Statement::single(const Types::Binds &binds, bool failOnEmpty) const
{
    if (!interface)
        return Types::Results::value_type();
    auto r = interface->extract(binds, failOnEmpty);
    if (r.empty())
        return Types::Results::value_type();
    return r.front();
}

}
}
