/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATABASE_RUNLOCK_HXX
#define CPD3DATABASE_RUNLOCK_HXX

#include "core/first.hxx"

#include <unordered_set>
#include <unordered_map>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <QString>
#include <QByteArray>

#include "database.hxx"
#include "core/number.hxx"

namespace CPD3 {
namespace Database {

class Connection;

class Storage;

/**
 * A manager for exclusive locks that record their last acquire time
 * and optional data.
 */
class CPD3DATABASE_EXPORT RunLock {
    Connection *connection;

    double firstAcquireTime;
    std::int_fast64_t lockIdentifer;
    std::unordered_map<QString, QByteArray> updateData;

    std::mutex mutex;
    std::condition_variable cv;
    std::unordered_set<QString> heldKeys;
    enum {
        Ping_Inactive, Ping_Active, Ping_Terminate,
    } pingState;
    std::thread pingThread;

    void shutdownPing();

    void startupPing(const QString &key);

    void pingExecute();

public:
    /**
     * Create the manager on the default database.
     */
    RunLock();

    /**
     * Create the manager on the specified database.
     *
     * @param db    the database
     */
    RunLock(Storage &&db);

    /** @see RunLock(Storage &&) */
    RunLock(const Storage &db);

    RunLock(const RunLock &) = delete;

    RunLock &operator=(const RunLock &) = delete;

    ~RunLock();

    /**
     * Acquire a lock.
     *
     * @param key       the key to lock
     * @param timeout   the timeout in seconds
     * @param ok        if present then set to true on success
     * @return          the last lock acquire time or undefined if this is the first
     */
    double acquire(const QString &key, double timeout = FP::undefined(), bool *ok = nullptr);

    /**
     * Fail the lock, and discard any pending changes.
     */
    void fail();

    /**
     * Release the lock and commit changes.
     */
    void release();

    /**
     * Get the time the lock was acquired.
     *
     * @return  the lock time
     */
    inline double startTime() const
    {
        Q_ASSERT(FP::defined(firstAcquireTime));
        return firstAcquireTime;
    }

    /**
     * Set a seen time of the lock.  This can move the acquire time backwards if the
     * earlest seen time is before the acquire time.
     * @param time  the time seen
     */
    void seenTime(double time);

    /**
     * Fetch the data associated with a key.  The key must be locked beforehand.
     *
     * @param key   the key
     * @return      the associated data
     */
    QByteArray get(const QString &key) const;

    /**
     * Set the data associated with a key.  The changes are not changed until the
     * lock is released.
     *
     * @param key   the key
     * @param data  the data to associated
     */
    void set(const QString &key, const QByteArray &data);

    /**
     * Fetch the data associated with a key without locking it.  No concurrency
     * guarantees are made if there are concurrant writers other than non-partial
     * data will be seen.
     *
     * @param key   the key
     * @return      the associated data
     */
    QByteArray blindGet(const QString &key) const;

    /**
     * Perform a blind immediate set of data.  This does not require the key to
     * be locked and immediately writes the data out.
     *
     * @param key   the key
     * @param data  the data to associated
     */
    void blindSet(const QString &key, const QByteArray &data) const;

    /**
     * Cleanup all stale locks in default database.
     */
    static void cleanupStale();

    /**
     * Cleanup all stale locks in the specified database.
     *
     * @param db    the database to access
     */
    static void cleanupStale(Storage &&db);

    /** @see cleanupStale(Storage &&) */
    static void cleanupStale(const Storage &db);
};

}
}

#endif //CPD3DATABASE_RUNLOCK_HXX
