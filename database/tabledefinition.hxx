/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATABASE_TABLEDEFINITION_HXX
#define CPD3DATABASE_TABLEDEFINITION_HXX

#include "core/first.hxx"

#include <vector>
#include <unordered_map>
#include <string>
#include <cstdint>
#include <type_traits>

#include "database/database.hxx"

namespace CPD3 {
namespace Database {

namespace Drivers {
class QtDatabase;

class QtMySQL;

class PostgreSQLCommon;

class SQLiteCommon;
}


/**
 * A definition of a table for creation.
 */
class CPD3DATABASE_EXPORT TableDefinition {
public:
    TableDefinition() = delete;

    /**
     * Create a table definition.
     *
     * @param name  the table name
     */
    TableDefinition(const std::string &name);

    /**
     * The data types supported for columns.
     */
    enum Type {
        /**
         * A fixed width integer, of at least the specified width bytes.
         */
                Integer,

        /**
         * A fixed width unsigned integer, of at least the specified width bytes.
         */
                UnsignedInteger,

        /**
         * A real number, of at least double precision.
         */
                Real,

        /**
         * A string of text, of arbitrary length.
         */
                String,

        /**
         * A string of text, of the given width maximum in unicode characters.
         */
                FiniteString,

        /**
         * Binary byte storage.
         */
                Bytes
    };

    /**
     * Define a column in the table.
     *
     * @param name          the column name
     * @param type          the data type
     * @param allowNull     true if the column can be null
     * @param width         the width
     */
    void column(const std::string &name, Type type, bool allowNull = true, int width = -1);

    /**
     * Define a string column.
     *
     * @param name          the column name
     * @param allowNull     true if the column can be null
     */
    inline void string(const std::string &name, bool allowNull = true)
    { return column(name, String, allowNull); }

    /**
     * Define a real number column.
     *
     * @param name          the column name
     * @param allowNull     true if the column can be null
     */
    void real(const std::string &name, bool allowNull = true)
    { return column(name, Real, allowNull); }

    /**
     * Define a binary data column.
     *
     * @param name          the column name
     * @param allowNull     true if the column can be null
     */
    void bytes(const std::string &name, bool allowNull = true)
    { return column(name, Bytes, allowNull); }

    /**
     * Define an integer column
     * @tparam T            the integer type to support
     * @param name          the column name
     * @param allowNull     true if the column can be null
     */
    template<typename T>
    inline void integer(const std::string &name, bool allowNull = true)
    {
        return column(name, (std::is_unsigned<T>::value ? UnsignedInteger : Integer), allowNull,
                      (int) sizeof(T));
    }

    /**
     * Set the primary key of the table.
     *
     * @param columns       the column in the primary key
     */
    void primaryKey(const std::vector<std::string> &columns);

    /**
     * Set the primary key of the table.
     *
     * @param column        the column primary key
     */
    inline void primaryKey(const std::string &column)
    {
        std::vector<std::string> columns;
        columns.emplace_back(column);
        return primaryKey(std::move(columns));
    }

    /**
     * Add a unique index to the table.
     *
     * @param name          the index name
     * @param columns       the column in the index
     */
    void index(const std::string &name, const std::vector<std::string> &columns);

    /**
     * Add a unique index to the table.
     *
     * @param name          the index name
     * @param column        the column to index on
     */
    void index(const std::string &name, const std::string &column)
    {
        std::vector<std::string> columns;
        columns.emplace_back(column);
        return index(name, std::move(columns));
    }

private:
    std::string name;

    struct ColumnDefinition {
        std::string name;
        Type type;
        bool allowNull;
        int width;

        ColumnDefinition();

        ColumnDefinition(const std::string &name, Type type, bool allowNull, int width);
    };

    std::vector<ColumnDefinition> columns;

    struct KeyDefinition {
        std::vector<std::size_t> indices;

        KeyDefinition();

        KeyDefinition(const std::vector<std::size_t> &indices);

        KeyDefinition(const std::vector<std::string> &names,
                      const std::vector<ColumnDefinition> &columns);
    };

    KeyDefinition primary;
    std::unordered_map<std::string, KeyDefinition> indices;

    friend class Drivers::QtDatabase;

    friend class Drivers::QtMySQL;

    friend class Drivers::PostgreSQLCommon;

    friend class Drivers::SQLiteCommon;
};

}
}

#endif //CPD3DATABASE_TABLEDEFINITION_HXX
