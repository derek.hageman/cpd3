/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <stdlib.h>
#include <mutex>
#include <thread>
#include <atomic>
#include <condition_variable>
#include <functional>
#include <unordered_set>
#include <stdlib.h>
#include <QSqlDatabase>
#include <QUuid>
#include <QThread>
#include <QThreadPool>
#include <QDebug>

#include "storage.hxx"
#include "driver.hxx"

#include "drivers/qtdatabase.hxx"
#include "drivers/qtsqlite.hxx"
#include "drivers/qtmysql.hxx"
#include "drivers/qtpostgresql.hxx"

#ifdef HAVE_SQLITE3
#include "drivers/sqlite3.hxx"
#endif

#ifdef HAVE_POSTGRESQL
#include "drivers/postgresql.hxx"
#endif


namespace CPD3 {
namespace Database {

class Storage::Type {
public:
    virtual bool run(const std::function<void(std::unique_ptr<Driver> &)> &thread) = 0;

    virtual ~Type() = default;

    class QtThreaded;

    class QtDatabase;

    class StdThreaded;
};

namespace {
class StartupConfirmation {
    std::mutex mutex;
    std::condition_variable cv;
    enum {
        Starting, Ok, Failed,
    } state;
public:

    StartupConfirmation() : mutex(), cv(), state(Starting)
    { }

    bool startup()
    {
        std::unique_lock<std::mutex> lock(mutex);
        for (;;) {
            switch (state) {
            case Starting:
                break;
            case Ok:
                return true;
            case Failed:
                return false;
            }
            cv.wait(lock);
        }
        Q_ASSERT(false);
        return false;
    }

    static void exec(const std::function<Driver *()> &createDriver,
                     StartupConfirmation *startup,
                     const std::function<void(std::unique_ptr<Driver> &)> &thread)
    {
        std::unique_ptr<Driver> d(createDriver());
        if (!d->open()) {
            d.reset();
            std::lock_guard<std::mutex> lock(startup->mutex);
            startup->state = StartupConfirmation::Failed;
            startup->cv.notify_all();
            startup = nullptr;
            return;
        } else {
            std::lock_guard<std::mutex> lock(startup->mutex);
            startup->state = StartupConfirmation::Ok;
            startup->cv.notify_all();
            startup = nullptr;
        }
        thread(d);
        d.reset();
    }
};
}

class Storage::Type::QtThreaded : public Storage::Type {
protected:
    virtual Driver *createDriver() = 0;

public:
    QtThreaded()
    { }

    virtual ~QtThreaded() = default;

    bool run(const std::function<void(std::unique_ptr<Driver> &)> &thread) override
    {
        StartupConfirmation startup;
        /* Intermediate variable to make std::bind type conversion happy */
        std::function<Driver *()> driver = std::bind(&QtThreaded::createDriver, this);
        Threading::detachQThread(
                std::bind(&StartupConfirmation::exec, std::move(driver), &startup, thread));
        return startup.startup();
    }
};

class Storage::Type::QtDatabase : public Storage::Type::QtThreaded {
    QSqlDatabase db;
public:
    QtDatabase(const QSqlDatabase &d) : db(d)
    { }

    virtual ~QtDatabase()
    {
        QString name = db.connectionName();
        if (db.isOpen())
            db.close();
        db = QSqlDatabase();
        QSqlDatabase::removeDatabase(name);
    }

protected:
    Driver *createDriver() override
    {
        QSqlDatabase db = QSqlDatabase::cloneDatabase(this->db, QUuid::createUuid().toString());
        this->db = QSqlDatabase();

        if (db.driverName() == "QSQLITE") {
            return new Drivers::QtSqlite(db);
        } else if (db.driverName() == "QMYSQL") {
            return new Drivers::QtMySQL(db);
        } else if (db.driverName() == "QPSQL") {
            return new Drivers::QtPostgreSQL(db);
        } else {
            return new Drivers::QtDatabase(db);
        }
    }
};

class Storage::Type::StdThreaded : public Storage::Type {
protected:
    virtual Driver *createDriver() = 0;

public:
    StdThreaded()
    { }

    virtual ~StdThreaded() = default;

    bool run(const std::function<void(std::unique_ptr<Driver> &)> &thread) override
    {
        StartupConfirmation startup;
        /* Intermediate variable to make std::bind type conversion happy */
        std::function<Driver *()> driver = std::bind(&StdThreaded::createDriver, this);
        std::thread(std::bind(&StartupConfirmation::exec, std::move(driver), &startup,
                              thread)).detach();
        return startup.startup();
    }
};


Storage::Storage(const Storage &other) = default;

Storage &Storage::operator=(const Storage &other) = default;

Storage::Storage(Storage &&other) : type(std::move(other.type))
{ }

Storage &Storage::operator=(Storage &&other)
{
    type = std::move(other.type);
    return *this;
}

Storage::Storage(std::shared_ptr<Type> &&type) : type(std::move(type))
{ }

Storage::Storage(const QSqlDatabase &qt) : type(std::make_shared<Type::QtDatabase>(qt))
{ }

Storage Storage::sqlite(const std::string &filename)
{
#ifdef HAVE_SQLITE3
    class SQLite3Type : public Type::StdThreaded {
        std::string filename;
    public:
        explicit SQLite3Type(std::string filename) : filename(std::move(filename))
        { }

        virtual ~SQLite3Type() = default;
    protected:
        Driver *createDriver() override
        { return new Drivers::SQLite3(filename); }
    };
    return Storage(std::make_shared<SQLite3Type>(filename));
#else
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", QUuid::createUuid().toString());
    db.setDatabaseName(QString::fromStdString(filename));
    return Storage(db);
#endif
}

Storage Storage::sqlite()
{
#ifdef HAVE_SQLITE3
    return sqlite(":memory:");
#else
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", QUuid::createUuid().toString());
    db.setDatabaseName(":memory:");
    return Storage(db);
#endif
}

Storage Storage::mysql(const std::string &server,
                       const std::string &db,
                       const std::string &username,
                       const std::string &password,
                       uint16_t port)
{
    QSqlDatabase r = QSqlDatabase::addDatabase("QMYSQL", QUuid::createUuid().toString());
    if (!server.empty())
        r.setHostName(QString::fromStdString(server));
    if (!db.empty())
        r.setDatabaseName(QString::fromStdString(db));
    if (!username.empty())
        r.setUserName(QString::fromStdString(username));
    if (!password.empty())
        r.setPassword(QString::fromStdString(password));
    if (port != 0)
        r.setPort(port);
    return Storage(r);
}

Storage Storage::postgresql(const std::string &server,
                            const std::string &db,
                            const std::string &username,
                            const std::string &password,
                            uint16_t port)
{
#ifdef HAVE_POSTGRESQL
    class PostgreSQLType : public Type::StdThreaded {
        Drivers::PostgreSQL::Connection connection;
    public:
        explicit PostgreSQLType(Drivers::PostgreSQL::Connection connection) : connection(std::move(connection))
        { }

        virtual ~PostgreSQLType() = default;
    protected:
        Driver *createDriver() override
        { return new Drivers::PostgreSQL(connection); }
    };
    Drivers::PostgreSQL::Connection connection;
    connection.host = server;
    connection.port = port;
    connection.db = db;
    connection.username = username;
    connection.password = password;
    return Storage(std::make_shared<PostgreSQLType>(std::move(connection)));
#else
    QSqlDatabase r = QSqlDatabase::addDatabase("QPSQL", QUuid::createUuid().toString());
    if (!server.empty())
        r.setHostName(QString::fromStdString(server));
    if (!db.empty())
        r.setDatabaseName(QString::fromStdString(db));
    if (!username.empty())
        r.setUserName(QString::fromStdString(username));
    if (!password.empty())
        r.setPassword(QString::fromStdString(password));
    if (port != 0)
        r.setPort(port);
    return Storage(r);
#endif
}

Storage Storage::qt(const QSqlDatabase &db)
{
    return Storage(db);
}

bool Storage::open(const std::function<void(std::unique_ptr<Driver> &)> &thread)
{
    Q_ASSERT(type);
    return type->run(thread);
}

}
}