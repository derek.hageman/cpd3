/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATABASE_INTERFACE_HXX
#define CPD3DATABASE_INTERFACE_HXX

#include "core/first.hxx"

#include <mutex>
#include <atomic>
#include <thread>
#include <cstdint>

#include "database.hxx"
#include "driver.hxx"
#include "util.hxx"

namespace CPD3 {
namespace Database {

class ContextInterface;

class Context;

class Connection;

class TransactionInterface;

class BaseRelease;

class BaseInterface;

/**
 * The shared interface to an active database connection.
 */
class ConnectionInterface : public std::enable_shared_from_this<ConnectionInterface> {
    friend class Connection;

    friend class BaseInterface;

    enum {
        Thread_Initialize,
        Thread_Active,
        Thread_TerminateWaitForTransaction,
        Thread_TerminateAbort,
        Thread_Complete,
    } threadState;

    std::mutex mutex;
    std::condition_variable command;
    std::condition_variable response;
    std::list<std::weak_ptr<BaseInterface>> threadedInterfaces;
    std::vector<std::weak_ptr<BaseInterface>> processingInterfaces;
    std::vector<std::unique_ptr<BaseRelease>> releaseQueue;

    typedef std::function<bool(Driver *, std::unique_lock<std::mutex> &)> Operation;
    Operation synchronousOperation;

    friend class TransactionInterface;

    std::shared_ptr<TransactionInterface> activeTransaction;

    void threadFinish(Driver *driver, std::unique_lock<std::mutex> &lock);

    void transactionEnd(std::unique_lock<std::mutex> &lock);

    static void run(const std::shared_ptr<ConnectionInterface> &self,
                    std::unique_ptr<Driver> &driver);

    bool canOperate() const;

    template<typename Result>
    Result resultSynchronousUnlocked(std::unique_lock<std::mutex> &lock,
                                     const std::function<Result(Driver *)> &operation,
                                     const std::function<
                                             void(Result output)> &finalize = std::function<
                                             void(Result output)>(),
                                     Result output = Result())
    {
        enum {
            Waiting, InProgress, Complete,
        } state = Waiting;
        for (;;) {
            if (!canOperate())
                return output;

            switch (state) {
            case Waiting:
                if (!synchronousOperation) {
                    state = InProgress;
                    synchronousOperation =
                            [&](Driver *driver, std::unique_lock<std::mutex> &lock) -> bool {
                                Q_ASSERT(state == InProgress);

                                lock.unlock();
                                output = operation(driver);
                                lock.lock();
                                if (finalize)
                                    finalize(output);
                                state = Complete;
                                return true;
                            };
                    command.notify_all();
                    break;
                }
                break;
            case InProgress:
                break;
            case Complete:
                return output;
            }
            response.wait(lock);
        }
    }

    Statement createStatement(const std::function<StatementInterface *(Driver *)> &create);

    void synchronizeTransactionEnd(std::unique_lock<std::mutex> &lock);

public:
    ConnectionInterface();

    /**
     * Test if the connection has completed.
     *
     * @return  true if the connection has completed
     */
    bool isFinished();

    /**
     * Wait for the connection to finish.
     *
     * @param timeout   the maximum time to wait
     * @return          true if the connection has completed
     */
    bool wait(double timeout = FP::undefined());

    /**
     * Shutdown the connection.
     *
     * @param waitForTransaction    wait for the current transaction to end instead of aborting it
     */
    void shutdown(bool waitForTransaction = true);

    /**
     * Run the given operation on the active transaction.
     * <br>
     * Only valid from outside the database thread.  That is, this may not be
     * called during an interface's processing method.
     *
     * @param operation     the operation to run, returning false aborts the transaction
     * @return              true if the operation was run
     */
    bool runOnTransaction(const std::function<bool(Driver *)> &operation);

    /**
     * Execute a statement.
     *
     * @param statement     the statement interface to create the execution context for
     * @param binds         the binds to use
     * @param buffer        request that the result be bufffered entirely
     * @return              the execution context
     */
    std::shared_ptr<ContextInterface> executeStatement(const StatementInterface *statement,
                                                       const Types::Binds &binds,
                                                       bool buffer = false);

    /**
     * Queue a release operation to run on the database thread.
     *
     * @param release       the operation to be deleted from the database thread
     */
    void queueForRelease(BaseRelease *release);

    /**
     * Run an operation on the current transaction from the database thread.
     *
     * @param driver        the database driver
     * @param operation     the operation to run, returning false aborts the transaction
     * @param complete      the completion function, called with false if the operation was not run or failed
     */
    void runFromTransaction(Driver *driver,
                            const std::function<bool()> &operation,
                            const std::function<void(bool)> &complete);
};

/**
 * The shared interface for a transaction on a connection.
 */
class TransactionInterface {
    ConnectionInterface &connection;
    enum {
        Initialize, Valid, Invalid,
    } activeState;

    friend class ConnectionInterface;

    bool initialize(std::unique_lock<std::mutex> &lock, int flags, double timeout);

public:
    /**
     * Create the interface.
     *
     * @param connection    the attached connection
     */
    TransactionInterface(ConnectionInterface &connection);

    /**
     * Acquire an active transaction.
     *
     * @param connection    the connection to create on
     * @param flags         the transaction flags
     * @param timeout       the maximum time to attempt to start the transaction
     * @return              the transaction interface
     */
    static std::weak_ptr<TransactionInterface> acquire(ConnectionInterface &connection,
                                                       int flags,
                                                       double timeout);

    /**
     * Abort the active transaction.  A rollback should be done if required.
     *
     * @return  true if the transaction was valid and a rollback is required
     */
    bool terminate();

    /**
     * Release the transaction, rolling it back if it is still valid.
     */
    void release();

    /**
     * Test if the transaction is in an error state.
     *
     * @return  true if the transaction is in an error state
     */
    bool inError();

    /**
     * Test if the transaction is valid, only available with the connection mutex held.
     *
     * @return  true if the transaction is valid
     */
    bool isValid() const;

    /**
     * Abort the transaction, rolling it back if required.
     */
    void abort();

    /**
     * End the transaction and commit the results.
     *
     * @return  true if the commit succeeded
     */
    bool endTransaction();
};

/**
 * The base class used for releasing resources that are managed by the
 * connection thread.
 */
class BaseRelease {
public:
    BaseRelease();

    virtual ~BaseRelease();
};

/**
 * The base interface used between handles and the connection.
 */
class BaseInterface : public std::enable_shared_from_this<BaseInterface> {
protected:
    /**
     * The connection the interface is attached to
     */
    std::weak_ptr<ConnectionInterface> connection;

    /**
     * Create the interface.
     *
     * @param connection    the attached connection
     */
    BaseInterface(const std::shared_ptr<ConnectionInterface> &connection);

    BaseInterface();

    /**
     * Queue the interface for processing.
     *
     * @return  true if the interface was queued up
     */
    bool enqueueForProcessing();

public:

    virtual ~BaseInterface();

    /**
     * The method called when the database connection thread is completing.
     */
    virtual void threadFinish();

    /**
     * The method called when the database connection is ending a transaction.
     */
    virtual void transactionEnd();

    /**
     * The method called during processing on the database connection thread.
     *
     * @param driver    the database driver
     */
    virtual void process(Driver *driver);
};

/**
 * The interface between statement instances and the connection thread.
 */
class StatementInterface final : public BaseInterface {
    std::unique_ptr<Driver::Operation> operation;

    friend class Connection;

    friend class ConnectionInterface;

    friend class ContextInterface;

    StatementInterface(const std::shared_ptr<ConnectionInterface> &connection,
                       Driver::Operation *operation);

public:

    ~StatementInterface();

    virtual void threadFinish();

    /**
     * Execute the statement in batch mode.
     *
     * @param binds     the row bindings
     */
    void batch(const Types::BatchBinds &binds);

    /**
     * Start statement execution.
     *
     * @param binds     the bindings
     * @return          the execution context
     */
    std::shared_ptr<ContextInterface> execute(const Types::Binds &binds = Types::Binds()) const;

    /**
     * Execute the statement synchronously, returning when it has completed.
     *
     * @param binds     the bindings
     */
    void synchronous(const Types::Binds &binds = Types::Binds());

    /**
     * Execute the statement synchronously, returning the first result when it has completed.
     *
     * @param binds         the bindings
     * @param failOnEmpty   if set, then fail the transaction if the result is empty
     * @return              the resulting data
     */
    Types::Results extract(const Types::Binds &binds = Types::Binds(), bool failOnEmpty = false);
};

/**
 * The interface between execution contexts and the connection thread.
 */
class ContextInterface final : public BaseInterface {
    std::unique_ptr<Driver::Query> query;

    std::mutex mutex;
    std::condition_variable stateChange;
    enum {
        Initialize, Fault, StartQuery, StartAndFinish, QueryRunning, QueryFinishing, QueryComplete
    } state;
    std::uint_fast32_t readyForNext;

    Threading::Signal<const Types::Results &> result;
    Threading::Signal<> complete;

    friend class StatementInterface;

    ContextInterface(const std::shared_ptr<ConnectionInterface> &connection, Driver::Query *query);

    bool resultLoop(std::uint_fast32_t n);

    friend class Context;

    friend class ConnectionInterface;

public:
    ContextInterface();

    ~ContextInterface();

    virtual void threadFinish();

    virtual void transactionEnd();

    virtual void process(Driver *driver);

    /**
     * Start context execution, returning the first result or complete.
     *
     * @param read  the number of results to try to read initially
     */
    void begin(std::uint_fast32_t read = 1);

    /**
     * Advance the context and return the next result when ready.
     */
    void next();

    /**
     * Complete context execution.
     */
    void finish();

    /**
     * Wait for the context to complete.
     */
    void wait();
};

}
}

#endif //CPD3DATABASE_INTERFACE_HXX
