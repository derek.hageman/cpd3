/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATABASEDRIVERS_QTDATABASE_HXX
#define CPD3DATABASEDRIVERS_QTDATABASE_HXX

#include "core/first.hxx"

#include <QSqlDatabase>
#include <QSqlQuery>

#include "database/database.hxx"
#include "database/driver.hxx"

namespace CPD3 {
namespace Database {
namespace Drivers {

/**
 * A general backend driver for dealing with Qt SQL managed databases.
 */
class CPD3DATABASE_EXPORT QtDatabase : public Driver {
protected:
    QSqlDatabase db;

    class QtQuery;

    class QtOperation : public Driver::Operation {
        friend class QtQuery;

    protected:
        QSqlDatabase &db;
        QString query;
        std::size_t extract;

    public:
        QtOperation(QtDatabase *d, const QString &query, std::size_t outputs = 0);

        virtual ~QtOperation();

        Query *issue(const Types::Binds &binds = Types::Binds(), bool buffer = false) override;

        bool batch(const Types::BatchBinds &binds) override;
    };

    friend class QtOperation;

    class QtQuery : public Driver::Query {
        friend class QtOperation;

    protected:
        QSqlQuery q;
        std::size_t extract;
        enum {
            Initialize, First, Valid, Complete, Error,
        } state;
    public:
        QtQuery(QtOperation *op, std::size_t extract);

        virtual ~QtQuery();

        void begin() override;

        bool isOk() override;

        bool advance() override;

        Types::Results values() override;

        void end() override;
    };

public:
    QtDatabase(const QSqlDatabase &db);

    virtual ~QtDatabase();

    bool open() override;

    bool beginTransaction(int flags, double timeout) override;

    bool commitTransaction() override;

    void rollbackTransaction() override;

    std::unordered_set<std::string> loadTables() override;

    bool createTable(const TableDefinition &table) override;

    bool removeTable(const std::string &table) override;

    Driver::Operation *general(const std::string &statement, size_t outputs = 0) override;
};

}
}
}

#endif //CPD3DATABASEDRIVERS_QTDATABASE_HXX
