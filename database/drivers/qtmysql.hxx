/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATABASRDRIVERS_QTMYSQL_HXX
#define CPD3DATABASRDRIVERS_QTMYSQL_HXX

#include <database/tabledefinition.hxx>
#include "core/first.hxx"

#include "database/database.hxx"
#include "qtdatabase.hxx"

namespace CPD3 {
namespace Database {
namespace Drivers {

/**
 * The backend driver for Qt managed MySQL/MariaDB databases.
 */
class CPD3DATABASE_EXPORT QtMySQL : public QtDatabase {
public:
    QtMySQL(const QSqlDatabase &db);

    virtual ~QtMySQL();

    bool beginTransaction(int flags, double timeout) override;

    std::unordered_set<std::string> loadTables() override;

    bool createTable(const TableDefinition &table) override;

    Operation *merge(const std::string &table,
                     const Types::ColumnSet &keys,
                     const Types::ColumnSet &values) override;

    Operation *merge(const std::string &table, const Types::ColumnSet &columns) override;

    void optimizeDatabase() override;
};

}
}
}

#endif //CPD3DATABASRDRIVERS_QTMYSQL_HXX
