/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QSqlError>
#include <QSqlDriver>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QLoggingCategory>

#include "qtdatabase.hxx"
#include "core/waitutils.hxx"
#include "core/timeutils.hxx"
#include "database/tabledefinition.hxx"


Q_LOGGING_CATEGORY(log_database_drivers_qtdatabase, "cpd3.database.drivers.qtdatabase",
                   QtWarningMsg)

namespace CPD3 {
namespace Database {
namespace Drivers {

QtDatabase::QtDatabase(const QSqlDatabase &d) : db(d)
{ }

QtDatabase::~QtDatabase()
{
    if (!db.isValid())
        return;
    QString name = db.connectionName();
    if (db.isOpen())
        db.close();
    db = QSqlDatabase();
    QSqlDatabase::removeDatabase(name);
}

bool QtDatabase::open()
{
    if (!db.open()) {
        qCWarning(log_database_drivers_qtdatabase) << "Failed to open database:"
                                                   << db.lastError().text();
        return false;
    }
    return true;
}

bool QtDatabase::beginTransaction(int, double timeout)
{
    double end = FP::undefined();
    if (FP::defined(timeout))
        end = Time::time() + timeout;
    for (int tc = 0;; tc++) {
        if (!db.isOpen())
            break;
        if (db.transaction())
            return true;

        std::this_thread::yield();
        if (timeout <= 0.0 || tc > 10000 || (FP::defined(end) && Time::time() > end)) {
            qCDebug(log_database_drivers_qtdatabase) << "Timeout starting transaction:"
                                                     << db.lastError().text();
            return false;
        }

        if (!db.isOpen() || (tc != 0 && (tc % 10) == 0)) {
            db.close();
            std::this_thread::yield();
            if (!this->open())
                return false;
        }

        Wait::backoffWait(tc);
    }
    if (db.lastError().isValid()) {
        qCDebug(log_database_drivers_qtdatabase) << "Transaction start failed:"
                                                 << db.lastError().text();
    }
    return false;
}

bool QtDatabase::commitTransaction()
{
    if (!db.commit()) {
        qCDebug(log_database_drivers_qtdatabase) << "Transaction commit failed: "
                                                 << db.lastError().text();
        db.rollback();
        return false;
    }
    return true;
}

void QtDatabase::rollbackTransaction()
{
    db.rollback();
}

std::unordered_set<std::string> QtDatabase::loadTables()
{
    auto tables = db.tables();
    std::unordered_set<std::string> result;
    for (const auto &t : tables) {
        result.emplace(t.toStdString());
    }
    return result;
}

bool QtDatabase::createTable(const TableDefinition &table)
{
    Q_ASSERT(!table.columns.empty());

    QStringList columnDefinitions;
    for (const auto &column : table.columns) {
        QString def(QString::fromStdString(column.name));

        QString type;
        switch (column.type) {
        case TableDefinition::Integer:
            switch (column.width) {
            case 1:
                type = "TINYINT";
                break;
            case 2:
                type = "SMALLINT";
                break;
            case 3:
                type = "MEDIUMINT";
                break;
            case 4:
                type = "INTEGER";
                break;
            default:
                type = "BIGINT";
                break;
            }
            break;
        case TableDefinition::UnsignedInteger:
            switch (column.width) {
            case 1:
                type = "TINYINT UNSIGNED";
                break;
            case 2:
                type = "SMALLINT UNSIGNED";
                break;
            case 3:
                type = "MEDIUMINT UNSIGNED";
                break;
            case 4:
                type = "INTEGER UNSIGNED";
                break;
            default:
                type = "BIGINT UNSIGNED";
                break;
            }
            break;
        case TableDefinition::Real:
            type = "DOUBLE";
            break;
        case TableDefinition::String:
            type = "TEXT";
            break;
        case TableDefinition::FiniteString:
            type = QString("VARCHAR(%1)").arg(column.width);
            break;
        case TableDefinition::Bytes:
            type = "LONGBLOB";
            break;
        }

        def.append(' ');
        def.append(type);

        if (column.allowNull) {
            def.append(" NULL");
        } else {
            def.append(" NOT NULL");
        }

        columnDefinitions.append(def);
    }
    Q_ASSERT(!columnDefinitions.empty());

    QString statement("CREATE TABLE ");
    statement.append(QString::fromStdString(table.name));
    statement.append('(');
    statement.append(columnDefinitions.join(','));

    if (!table.primary.indices.empty()) {
        QStringList keys;
        for (auto ki : table.primary.indices) {
            keys.append(QString::fromStdString(table.columns[ki].name));
        }
        Q_ASSERT(!keys.empty());

        statement.append(", PRIMARY KEY(");
        statement.append(keys.join(','));
        statement.append(')');
    }

    statement.append(')');

    db.exec(statement);
    if (db.lastError().isValid()) {
        qCDebug(log_database_drivers_qtdatabase) << "Failed to create table:" << statement << "-"
                                                 << db.lastError().text();
        return false;
    }

    for (const auto &idef : table.indices) {
        Q_ASSERT(!idef.first.empty());
        Q_ASSERT(!idef.second.indices.empty());

        QStringList keys;
        for (auto ki : idef.second.indices) {
            keys.append(QString::fromStdString(table.columns[ki].name));
        }
        Q_ASSERT(!keys.empty());

        statement = "CREATE UNIQUE INDEX ";
        statement.append(QString::fromStdString(table.name));
        statement.append("Index");
        statement.append(QString::fromStdString(idef.first));
        statement.append(" ON ");
        statement.append(QString::fromStdString(table.name));
        statement.append(" (");
        statement.append(keys.join(','));
        statement.append(')');

        db.exec(statement);
        if (db.lastError().isValid()) {
            qCDebug(log_database_drivers_qtdatabase) << "Failed to create index:" << statement
                                                     << "-" << db.lastError().text();
            return false;
        }
    }

    return true;
}

bool QtDatabase::removeTable(const std::string &table)
{
    QString statement("DROP TABLE ");
    statement.append(QString::fromStdString(table));

    db.exec(statement);
    if (db.lastError().isValid()) {
        qCDebug(log_database_drivers_qtdatabase) << "Failed to remove table:" << statement << "-"
                                                 << db.lastError().text();
        return false;
    }

    return true;
}

Driver::Operation *QtDatabase::general(const std::string &statement, size_t outputs)
{ return new QtOperation(this, QString::fromStdString(statement), outputs); }


QtDatabase::QtOperation::QtOperation(QtDatabase *d, const QString &s, std::size_t o) : db(d->db),
                                                                                       query(s),
                                                                                       extract(o)
{ }

QtDatabase::QtOperation::~QtOperation() = default;

Driver::Query *QtDatabase::QtOperation::issue(const Types::Binds &binds, bool)
{
    std::unique_ptr<QtQuery> q(new QtQuery(this, extract));
    if (!q->q.prepare(query)) {
        qCWarning(log_database_drivers_qtdatabase) << "Error preparing query" << query << ":"
                                                   << q->q.lastError().text();
        return nullptr;
    }

    for (const auto &b : binds) {
        QString bindName(QString::fromStdString(b.first));
        bindName.prepend(':');
        switch (b.second.type()) {
        case Types::Variant::Type::Empty:
        case Types::Variant::Type::Null:
            q->q.bindValue(bindName, QVariant(), QSql::In);
            break;
        case Types::Variant::Type::Integer:
            if (!INTEGER::defined(b.second.toInteger())) {
                q->q.bindValue(bindName, QVariant(), QSql::In);
            } else {
                q->q.bindValue(bindName, static_cast<qlonglong>(b.second.toInteger()), QSql::In);
            }
            break;
        case Types::Variant::Type::Real:
            if (!FP::defined(b.second.toReal())) {
                q->q.bindValue(bindName, QVariant(), QSql::In);
            } else {
                q->q.bindValue(bindName, b.second.toReal(), QSql::In);
            }
            break;
        case Types::Variant::Type::String:
            q->q.bindValue(bindName, b.second.toQString(), QSql::In);
            break;
        case Types::Variant::Type::Bytes:
            q->q.bindValue(bindName, b.second.toQByteArray(), QSql::In | QSql::Binary);
            break;
        }
    }

    return q.release();
}

bool QtDatabase::QtOperation::batch(const Types::BatchBinds &binds)
{
    QSqlQuery e(db);
    if (!e.prepare(query)) {
        qCWarning(log_database_drivers_qtdatabase) << "Error preparing query" << query << ":"
                                                   << e.lastError().text();
        return false;
    }
    for (const auto &b : binds) {
        QString bindName(QString::fromStdString(b.first));
        bindName.prepend(':');

        QVariantList applied;
        QSql::ParamType pt = QSql::In;
        for (const auto &row : b.second) {
            switch (row.type()) {
            case Types::Variant::Type::Empty:
            case Types::Variant::Type::Null:
                applied.append(QVariant());
                break;
            case Types::Variant::Type::Integer:
                applied.append(static_cast<qlonglong>(row.toInteger()));
                break;
            case Types::Variant::Type::Real:
                applied.append(row.toReal());
                break;
            case Types::Variant::Type::String:
                applied.append(row.toQString());
                break;
            case Types::Variant::Type::Bytes:
                applied.append(row.toQByteArray());
                pt |= QSql::Binary;
                break;
            }
        }
        e.bindValue(bindName, std::move(applied), pt);
    }
    if (!e.execBatch()) {
        qCDebug(log_database_drivers_qtdatabase) << "Batch execution failed in" << e.executedQuery()
                                                 << ":" << e.lastError().text();
        return false;
    }
    e.finish();
    return true;
}


QtDatabase::QtQuery::QtQuery(QtDatabase::QtOperation *op, std::size_t ex) : q(op->db),
                                                                            extract(ex),
                                                                            state(Initialize)
{ }

QtDatabase::QtQuery::~QtQuery() = default;

void QtDatabase::QtQuery::begin()
{
    if (state != Initialize)
        return;
    if (!q.exec()) {
        qCDebug(log_database_drivers_qtdatabase) << "Error executing query" << q.executedQuery()
                                                 << ":" << q.lastError().text();
        q.clear();
        state = Error;
        return;
    }

    state = First;
}

bool QtDatabase::QtQuery::isOk()
{ return state != Error; }

bool QtDatabase::QtQuery::advance()
{
    switch (state) {
    case Initialize:
        return false;
    case First:
        if (!q.first()) {
            state = Complete;
            q.finish();
            q.clear();
            return false;
        }
        state = Valid;
        return true;
    case Valid:
        if (!q.next()) {
            state = Complete;
            q.finish();
            q.clear();
            return false;
        }
        return true;
    case Complete:
        return false;
    case Error:
        return false;
    }
    Q_ASSERT(false);
    return false;
}

Types::Results QtDatabase::QtQuery::values()
{
    Types::Results result;
    QSqlRecord rec = q.record();
    for (std::size_t i = 0; i < extract; ++i) {
        auto rv = rec.value(static_cast<int>(i));
        if (!rv.isValid()) {
            result.emplace_back();
            continue;
        }
        if (rv.isNull()) {
            result.emplace_back(Database::Types::Variant::Type::Null);
            continue;
        }

        switch (static_cast<QMetaType::Type>(rv.type())) {
        case QMetaType::Char:
        case QMetaType::SChar:
        case QMetaType::UChar:
        case QMetaType::Short:
        case QMetaType::UShort:
        case QMetaType::Int:
        case QMetaType::UInt:
        case QMetaType::Long:
        case QMetaType::ULong:
        case QMetaType::LongLong:
        case QMetaType::ULongLong: {
            Q_ASSERT(rv.canConvert<qlonglong>());
            bool ok = false;
            auto v = rv.toLongLong(&ok);
            if (!ok)
                result.emplace_back(INTEGER::undefined());
            else
                result.emplace_back(static_cast<std::int_fast64_t>(v));
            break;
        }
        case QMetaType::Float:
        case QMetaType::Double: {
            Q_ASSERT(rv.canConvert<double>());
            bool ok = false;
            auto v = rv.toDouble(&ok);
            if (!ok)
                result.emplace_back(FP::undefined());
            else
                result.emplace_back(v);
            break;
        }
        case QMetaType::QString: {
            Q_ASSERT(rv.canConvert<QString>());
            result.emplace_back(rv.toString());
            break;
        }
        case QMetaType::QByteArray: {
            Q_ASSERT(rv.canConvert<QByteArray>());
            result.emplace_back(rv.toByteArray());
            break;
        }
        case QMetaType::Bool: {
            Q_ASSERT(rv.canConvert<bool>());
            bool v = rv.toBool();
            result.emplace_back(v ? 1 : 0);
            break;
        }
        case QMetaType::Void:
            result.emplace_back();
            break;
        default: {
            if (rv.canConvert<QString>()) {
                result.emplace_back(rv.toString());
                break;
            }
            if (rv.canConvert<QByteArray>()) {
                result.emplace_back(rv.toByteArray());
                break;
            }
            if (rv.canConvert<double>()) {
                result.emplace_back(rv.toDouble());
                break;
            }
            if (rv.canConvert<qlonglong>()) {
                result.emplace_back(static_cast<std::int_fast64_t>(rv.toLongLong()));
                break;
            }

            result.emplace_back();
            break;
        }
        }
    }
    return result;
}

void QtDatabase::QtQuery::end()
{
    switch (state) {
    case Error:
    case Complete:
        return;
    case Initialize:
    case First:
    case Valid:
        break;
    }

    state = Complete;
    q.finish();
    q.clear();
}

}
}
}