/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <sqlite3.h>

#include <memory>

#include <QLoggingCategory>

#include "sqlite3.hxx"
#include "core/waitutils.hxx"
#include "core/timeutils.hxx"
#include "core/qtcompat.hxx"


Q_LOGGING_CATEGORY(log_database_drivers_sqlite3, "cpd3.database.drivers.sqlite3",
                   QtWarningMsg)


namespace CPD3 {
namespace Database {
namespace Drivers {

SQLite3::SQLite3(std::string filename) : filename(std::move(filename)), db(nullptr)
{}

SQLite3::~SQLite3()
{
    if (db) {
        int rc = ::sqlite3_close(static_cast<::sqlite3 *>(db));
        if (rc != SQLITE_OK) {
            qCWarning(log_database_drivers_sqlite3) << "Error closing database" << filename << ": "
#ifdef HAVE_SQLITE3_ERRSTR
                                                    << ::sqlite3_errstr(rc);
#else
                                                    << ::sqlite3_errmsg(static_cast<::sqlite3 *>(db));
#endif
        }
    }
}

Driver::Operation *SQLite3::common_general(const std::string &statement, size_t outputs)
{ return general(statement, outputs); }

bool SQLite3::open()
{
    if (db) {
        int rc = ::sqlite3_close(static_cast<::sqlite3 *>(db));
        if (rc != SQLITE_OK) {
            qCWarning(log_database_drivers_sqlite3) << "Error closing database" << filename << ": "
#ifdef HAVE_SQLITE3_ERRSTR
                                                    << ::sqlite3_errstr(rc);
#else
                                                    << ::sqlite3_errmsg(static_cast<::sqlite3 *>(db));
#endif
        }
        db = nullptr;
    }

    int rc = ::sqlite3_open_v2(filename.c_str(), reinterpret_cast<::sqlite3 **>(&db),
                               SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE | SQLITE_OPEN_NOMUTEX, nullptr);
    if (rc != SQLITE_OK) {
        if (db) {
            qCWarning(log_database_drivers_sqlite3) << "Failed to open database" <<
                                                    filename << ":" << ::sqlite3_errmsg(static_cast<::sqlite3 *>(db));
            rc = ::sqlite3_close(static_cast<::sqlite3 *>(db));
            if (rc != SQLITE_OK) {
                qCWarning(log_database_drivers_sqlite3) << "Error closing database" << filename << ": "
#ifdef HAVE_SQLITE3_ERRSTR
                                                        << ::sqlite3_errstr(rc);
#else
                                                        << ::sqlite3_errmsg(static_cast<::sqlite3 *>(db));
#endif
            }
            db = nullptr;
        } else {
            qCWarning(log_database_drivers_sqlite3) << "Failed to open database" << filename << ":"
#ifdef HAVE_SQLITE3_ERRSTR
                                                    << ::sqlite3_errstr(rc);
#else
                                                    << ::sqlite3_errmsg(static_cast<::sqlite3 *>(db));
#endif
        }
        return false;
    }

    rc = ::sqlite3_busy_timeout(static_cast<::sqlite3 *>(db), 500);
    if (rc != SQLITE_OK) {
        qCWarning(log_database_drivers_sqlite3) << "Failed to set database busy handler" <<
                                                ::sqlite3_errmsg(static_cast<::sqlite3 *>(db));
    }


    return common_open();
}

bool SQLite3::beginTransaction(int flags, double timeout)
{
    double end = FP::undefined();
    if (FP::defined(timeout))
        end = Time::time() + timeout;
    for (int tc = 0;; tc++) {
        if (!db)
            return false;

        char *err;
        int rc;
        if (flags & Driver::Transaction_ReadOnly) {
            if (flags & Driver::Transaction_Serializable) {
                rc = ::sqlite3_exec(static_cast<::sqlite3 *>(db), "BEGIN IMMEDIATE TRANSACTION",
                                    nullptr, nullptr, &err);
            } else {
                rc = ::sqlite3_exec(static_cast<::sqlite3 *>(db), "BEGIN DEFERRED TRANSACTION",
                                    nullptr, nullptr, &err);
            }
        } else {
            rc = ::sqlite3_exec(static_cast<::sqlite3 *>(db), "BEGIN IMMEDIATE TRANSACTION",
                                nullptr, nullptr, &err);
        }
        if (rc == SQLITE_OK)
            return true;

        std::this_thread::yield();
        if (timeout <= 0.0 || tc > 10000 || (FP::defined(end) && Time::time() > end)) {
            qCDebug(log_database_drivers_sqlite3) << "Timeout starting transaction, last error:" <<
                                                  ::sqlite3_errmsg(static_cast<::sqlite3 *>(db));
            return false;
        }

        if (tc != 0 && (tc % 10) == 0) {
            rc = ::sqlite3_close(static_cast<::sqlite3 *>(db));
            if (rc != SQLITE_OK) {
                qCWarning(log_database_drivers_sqlite3) << "Error closing database" << filename << ": "
#ifdef HAVE_SQLITE3_ERRSTR
                                                        << ::sqlite3_errstr(rc);
#else
                                                        << ::sqlite3_errmsg(static_cast<::sqlite3 *>(db));
#endif
            }
            db = nullptr;

            std::this_thread::yield();
            if (!this->open()) {
                qCDebug(log_database_drivers_sqlite3) << "Database reopen failed for" << filename;
                return false;
            }
        }

        Wait::backoffWait(tc);
    }
    return false;
}

bool SQLite3::commitTransaction()
{
    if (!db)
        return false;
    char *err;
    int rc;
    if ((rc = ::sqlite3_exec(static_cast<::sqlite3 *>(db), "COMMIT TRANSACTION",
                             nullptr, nullptr, &err)) != SQLITE_OK) {
        if (rc != SQLITE_BUSY) {
            qCDebug(log_database_drivers_sqlite3) << "Error committing transaction:" << err;
        }
        ::sqlite3_free(err);

        ::sqlite3_exec(static_cast<::sqlite3 *>(db), "ROLLBACK TRANSACTION",
                       nullptr, nullptr, nullptr);
        return false;
    }
    return true;
}

void SQLite3::rollbackTransaction()
{
    if (!db)
        return;
    ::sqlite3_exec(static_cast<::sqlite3 *>(db), "ROLLBACK TRANSACTION", nullptr, nullptr, nullptr);
}

std::unordered_set<std::string> SQLite3::loadTables()
{
    struct Data {
        std::unordered_set<std::string> result;

        static int callback(void *param, int nCol, char **values, char **names)
        {
            Data *data = reinterpret_cast<Data *>(param);

            if (nCol < 1)
                return 0;
            if (!values[0])
                return 0;
            if (!values[0][0])
                return 0;

            data->result.emplace(values[0]);

            return 0;
        }
    };

    char *err;
    Data data;
    if (::sqlite3_exec(static_cast<::sqlite3 *>(db), R"(SELECT name FROM sqlite_master WHERE type = "table" )",
                       Data::callback, &data, &err) != SQLITE_OK) {
        qCDebug(log_database_drivers_sqlite3) << "Error reading tables:" << err;
        ::sqlite3_free(err);
        return {};
    }
    return std::move(data.result);
}

bool SQLite3::createTable(const TableDefinition &table)
{ return common_createTable(table); }

bool SQLite3::removeTable(const std::string &table)
{
    if (!db)
        return false;

    std::string query = "DROP TABLE " + table;

    char *err;
    if (::sqlite3_exec(static_cast<::sqlite3 *>(db), query.c_str(), nullptr, nullptr, &err) != SQLITE_OK) {
        qCDebug(log_database_drivers_sqlite3) << "Error dropping table" << table << ":" << err;
        ::sqlite3_free(err);
        return false;
    }
    return true;
}

Driver::Operation *SQLite3::merge(const std::string &table,
                                  const Types::ColumnSet &keys,
                                  const Types::ColumnSet &values)
{
    Types::ColumnSet combined(keys);
    for (const auto &add : values) {
        combined.insert(add);
    }
    return merge(table, combined);
}

Driver::Operation *SQLite3::merge(const std::string &table, const Types::ColumnSet &columns)
{ return general(common_merge(table, columns)); }

void SQLite3::optimizeDatabase()
{ return common_optimizeDatabase(); }


static bool bindVariant(sqlite3_stmt *st, int index, const Types::Variant &variant)
{
    switch (variant.type()) {
    case Types::Variant::Type::Empty:
    case Types::Variant::Type::Null:
        return ::sqlite3_bind_null(st, index) == SQLITE_OK;
    case Types::Variant::Type::Integer:
        if (!INTEGER::defined(variant.toInteger()))
            return ::sqlite3_bind_null(st, index) == SQLITE_OK;
        return ::sqlite3_bind_int64(st, index, static_cast<::sqlite3_int64>(variant.toInteger())) == SQLITE_OK;
    case Types::Variant::Type::Real:
        if (!FP::defined(variant.toReal()))
            return ::sqlite3_bind_null(st, index) == SQLITE_OK;
        return ::sqlite3_bind_double(st, index, variant.toReal()) == SQLITE_OK;
    case Types::Variant::Type::String: {
        const auto &v = variant.toString();
#ifdef DHAVE_SQLITE3_TEXT64
        return ::sqlite3_bind_text64(st, index, v.c_str(), static_cast<::sqlite3_uint64>(v.size()),
                                     SQLITE_TRANSIENT, SQLITE_UTF8) == SQLITE_OK;
#else
        return ::sqlite3_bind_text(st, index, v.c_str(), static_cast<int>(v.size()),
                                   SQLITE_TRANSIENT) == SQLITE_OK;
#endif
    }
    case Types::Variant::Type::Bytes: {
        const auto &v = variant.toBytes();
#ifdef DHAVE_SQLITE3_BLOB64
        return ::sqlite3_bind_blob64(st, index, v.data(), static_cast<::sqlite3_uint64>(v.size()),
                                     SQLITE_TRANSIENT) == SQLITE_OK;
#else
        return ::sqlite3_bind_blob(st, index, v.data(), static_cast<int>(v.size()),
                                   SQLITE_TRANSIENT) == SQLITE_OK;
#endif
    }
    }
    Q_ASSERT(false);
    return false;
}

namespace {
using Statement = std::unique_ptr<::sqlite3_stmt, decltype(&(::sqlite3_finalize))>;
}

class SQLite3::Sqlite3Query : public Driver::Query {
    Statement st;
    std::weak_ptr<Statement> recycle;
    std::size_t extract;
    enum class State {
        Initialize, First, Valid, Complete, Error,
    } state;
    int available;

    void releaseStatement()
    {
        Q_ASSERT(st.get() != nullptr);

        auto rel = std::move(st);

        if (auto target = recycle.lock()) {
            if ((*target))
                return;

            int rc = ::sqlite3_reset(rel.get());
            if (rc != SQLITE_OK) {
#ifdef HAVE_SQLITE3_ERRSTR
                qCDebug(log_database_drivers_sqlite3) << "Error resetting query:"
                                                      << ::sqlite3_errstr(rc);
#else
                qCDebug(log_database_drivers_sqlite3) << "Error resetting query";
#endif
                return;
            }

            rc = ::sqlite3_clear_bindings(rel.get());
            if (rc != SQLITE_OK) {
#ifdef HAVE_SQLITE3_ERRSTR
                qCDebug(log_database_drivers_sqlite3) << "Error clearing query:"
                                                      << ::sqlite3_errstr(rc);
#else
                qCDebug(log_database_drivers_sqlite3) << "Error clearing query";
#endif
                return;
            }

            *target = std::move(rel);
        }
    }

public:
    Sqlite3Query(Statement &&st, const std::shared_ptr<Statement> &recycle, std::size_t extract) :
            st(std::move(st)), recycle(recycle), extract(extract), state(State::Initialize), available(0)
    {}

    virtual ~Sqlite3Query() = default;

    void begin() override
    {
        if (state != State::Initialize)
            return;
        Q_ASSERT(st.get() != nullptr);

        auto rc = ::sqlite3_step(st.get());
        switch (rc) {
        case SQLITE_DONE:
            state = State::Complete;
            releaseStatement();
            break;
        case SQLITE_ROW: {
            state = State::First;
            int n = ::sqlite3_column_count(st.get());
            if (n <= 0) {
                available = 0;
            } else if (static_cast<std::size_t>(n) < extract) {
                available = n;
            } else {
                available = static_cast<int>(extract);
            }
            Q_ASSERT(available <= n);
            break;
        }
        default:
#ifdef HAVE_SQLITE3_ERRSTR
            qCDebug(log_database_drivers_sqlite3) << "Error starting query:"
                                                  << ::sqlite3_errstr(rc);
#else
            qCDebug(log_database_drivers_sqlite3) << "Error starting query";
#endif
            state = State::Error;
            st.reset();
            break;
        }
    }

    bool isOk() override
    { return state != State::Error; }

    bool advance() override
    {
        switch (state) {
        case State::Initialize:
            return false;
        case State::First:
            state = State::Valid;
            return true;
        case State::Valid: {
            Q_ASSERT(st.get() != nullptr);

            auto rc = ::sqlite3_step(st.get());
            switch (rc) {
            case SQLITE_DONE:
                state = State::Complete;
                releaseStatement();
                return false;
            case SQLITE_ROW:
                return true;
            default:
#ifdef HAVE_SQLITE3_ERRSTR
                qCDebug(log_database_drivers_sqlite3) << "Error advancing query:"
                                                      << ::sqlite3_errstr(rc);
#else
                qCDebug(log_database_drivers_sqlite3) << "Error advancing query";
#endif
                state = State::Error;
                st.reset();
                return false;
            }
            Q_ASSERT(false);
            return false;
        }
        case State::Complete:
            return false;
        case State::Error:
            return false;
        }
        Q_ASSERT(false);
        return false;
    }

    Types::Results values() override
    {
        Q_ASSERT(state == State::Valid);
        Q_ASSERT(st.get() != nullptr);

        Types::Results result;
        for (int i = 0; i < available; i++) {
            auto tc = ::sqlite3_column_type(st.get(), i);
            switch (tc) {
            case SQLITE_INTEGER:
                result.emplace_back(static_cast<std::int_fast64_t>(::sqlite3_column_int64(st.get(), i)));
                break;
            case SQLITE_FLOAT:
                result.emplace_back(::sqlite3_column_double(st.get(), i));
                break;
            case SQLITE3_TEXT: {
                auto n = static_cast<std::size_t>(::sqlite3_column_bytes(st.get(), i));
                result.emplace_back(std::string(reinterpret_cast<const char *>(::sqlite3_column_text(st.get(), i)), n));
                break;
            }
            case SQLITE_BLOB: {
                auto n = static_cast<std::size_t>(::sqlite3_column_bytes(st.get(), i));
                if (!n) {
                    result.emplace_back(CPD3::Util::ByteArray());
                } else {
                    result.emplace_back(CPD3::Util::ByteArray(::sqlite3_column_blob(st.get(), i), n));
                }
                break;
            }
            case SQLITE_NULL:
                result.emplace_back(Types::Variant::Type::Null);
                break;
            default:
                qCWarning(log_database_drivers_sqlite3) << "Recognized type code" << tc << "at index" << i;
                result.emplace_back();
                break;
            }
        }

        result.resize(extract);
        return result;
    }

    void end() override
    {
        switch (state) {
        case State::Initialize:
        case State::First:
        case State::Valid:
            break;
        case State::Complete:
        case State::Error:
            return;
        }

        releaseStatement();
        state = State::Complete;
    }
};

class SQLite3::Sqlite3Operation : public Driver::Operation {
    SQLite3 &driver;
    std::string query;
    std::size_t extract;
    std::shared_ptr<Statement> recycle;
public:
    Sqlite3Operation(SQLite3 &driver, std::string query, std::size_t outputs = 0) : driver(driver),
                                                                                    query(std::move(query)),
                                                                                    extract(outputs),
                                                                                    recycle(std::make_shared<Statement>(
                                                                                            nullptr,
                                                                                            &(::sqlite3_finalize)))
    {}

    virtual ~Sqlite3Operation() = default;

    Query *issue(const Types::Binds &binds = Types::Binds(), bool = false) override
    {
        Statement st(nullptr, &(::sqlite3_finalize));
        if (!recycle->get()) {
            sqlite3_stmt *result = nullptr;
            if (::sqlite3_prepare_v2(static_cast<::sqlite3 *>(driver.db),
                                     query.c_str(), static_cast<int>(query.size()),
                                     &result, nullptr) != SQLITE_OK) {
                qCWarning(log_database_drivers_sqlite3) << "Error preparing query" << query << ":"
                                                        << ::sqlite3_errmsg(static_cast<::sqlite3 *>(driver.db));
                return nullptr;
            }
            st.reset(result);
        } else {
            st = std::move(*recycle);
        }

        for (int i = 1, max = ::sqlite3_bind_parameter_count(st.get()); i <= max; i++) {
            auto name = ::sqlite3_bind_parameter_name(st.get(), i);
            if (!name || !name[0] || !name[1]) {
                qCDebug(log_database_drivers_sqlite3) << "Query" << query << "has a nameless parameter at index" << i;
                continue;
            }

            auto source = binds.find(name + 1);
            if (source == binds.end()) {
                qCDebug(log_database_drivers_sqlite3) << "No bound values for parameter" << name << "in query" << query;
                continue;
            }

            if (!bindVariant(st.get(), i, source->second)) {
                qCDebug(log_database_drivers_sqlite3) << "Error binding parameter" << name << "in query" << query
                                                      << ":" << ::sqlite3_errmsg(static_cast<::sqlite3 *>(driver.db));
                return nullptr;
            }
        }

        return new Sqlite3Query(std::move(st), recycle, extract);
    }

    bool batch(const Types::BatchBinds &binds) override
    {
        if (!driver.db)
            return false;
        std::size_t rows = 0;
        for (const auto &add : binds) {
            if (add.second.size() > rows)
                rows = add.second.size();
        }
        if (!rows)
            return true;

        sqlite3_stmt *result = nullptr;
#ifdef DHAVE_SQLITE3_PREPARE3
        if (::sqlite3_prepare_v3(static_cast<::sqlite3 *>(driver.db),
                                 query.c_str(), static_cast<int>(query.size()),
                                 SQLITE_PREPARE_PERSISTENT, &result, nullptr) != SQLITE_OK) {
#else
        if (::sqlite3_prepare_v2(static_cast<::sqlite3 *>(driver.db),
                                 query.c_str(), static_cast<int>(query.size()),
                                 &result, nullptr) != SQLITE_OK) {
#endif
            qCWarning(log_database_drivers_sqlite3) << "Error preparing query" << query << ":"
                                                    << ::sqlite3_errmsg(static_cast<::sqlite3 *>(driver.db));
            return false;
        }
        Statement st(result, &(::sqlite3_finalize));

        std::vector<const Types::BatchVariants *> lookup(
                static_cast<std::size_t>(::sqlite3_bind_parameter_count(st.get())),
                nullptr);
        for (std::size_t i = 0, max = lookup.size(); i < max; i++) {
            auto name = ::sqlite3_bind_parameter_name(st.get(), static_cast<int>(i + 1));
            if (!name || !name[0] || !name[1]) {
                qCDebug(log_database_drivers_sqlite3) << "Query" << query << "has a nameless parameter at index" << i;
                continue;
            }

            auto source = binds.find(name + 1);
            if (source == binds.end()) {
                qCDebug(log_database_drivers_sqlite3) << "No bound values for parameter" << name << "in query" << query;
                continue;
            }

            lookup[i] = &(source->second);
        }

        for (std::size_t row = 0; row < rows; ++row) {
            if (::sqlite3_reset(st.get()) != SQLITE_OK) {
                qCDebug(log_database_drivers_sqlite3) << "Error resetting query" << query << ":"
                                                      << ::sqlite3_errmsg(static_cast<::sqlite3 *>(driver.db));
                return false;
            }
            if (::sqlite3_clear_bindings(st.get()) != SQLITE_OK) {
                qCDebug(log_database_drivers_sqlite3) << "Error clearing query" << query << ":"
                                                      << ::sqlite3_errmsg(static_cast<::sqlite3 *>(driver.db));
                return false;
            }

            for (std::size_t i = 0, max = lookup.size(); i < max; i++) {
                auto source = lookup[i];
                if (!source) {
                    if (!bindVariant(st.get(), static_cast<int>(i + 1), {})) {
                        qCDebug(log_database_drivers_sqlite3) << "Error binding parameter" << i << "on row" << row
                                                              << "in query" << query
                                                              << ":"
                                                              << ::sqlite3_errmsg(static_cast<::sqlite3 *>(driver.db));
                        return false;
                    }
                    continue;
                }
                if (row >= source->size()) {
                    if (!bindVariant(st.get(), static_cast<int>(i + 1), {})) {
                        qCDebug(log_database_drivers_sqlite3) << "Error binding parameter" << i << "on row" << row
                                                              << "in query" << query
                                                              << ":"
                                                              << ::sqlite3_errmsg(static_cast<::sqlite3 *>(driver.db));
                        return false;
                    }
                    continue;
                }

                if (!bindVariant(st.get(), static_cast<int>(i + 1), (*source)[row])) {
                    qCDebug(log_database_drivers_sqlite3) << "Error binding parameter" << i << "on row" << row
                                                          << "in query" << query
                                                          << ":"
                                                          << ::sqlite3_errmsg(static_cast<::sqlite3 *>(driver.db));
                    return false;
                }
            }

            switch (::sqlite3_step(st.get())) {
            case SQLITE_ROW:
            case SQLITE_DONE:
                break;
            default:
                qCDebug(log_database_drivers_sqlite3) << "Error executing query" << query << "row" << row << ":"
                                                      << ::sqlite3_errmsg(static_cast<::sqlite3 *>(driver.db));
                return false;
            }
        }

        return true;
    }
};

Driver::Operation *SQLite3::general(const std::string &statement, size_t outputs)
{ return new Sqlite3Operation(*this, statement, outputs); }

}
}
}