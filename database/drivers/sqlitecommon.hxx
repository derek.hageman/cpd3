/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATABASEDRIVERS_SQLITECOMMON_HXX
#define CPD3DATABASEDRIVERS_SQLITECOMMON_HXX

#include "core/first.hxx"

#include <string>
#include <cstdint>

#include "database/database.hxx"
#include "database/tabledefinition.hxx"
#include "database/util.hxx"
#include "database/driver.hxx"

namespace CPD3 {
namespace Database {
namespace Drivers {

/**
 * A shared base class for interfacing with SQLite backends.
 */
class CPD3DATABASE_EXPORT SQLiteCommon {
    bool supports_OptionalRowID;

    bool issueVoid(const std::string &statement);

    std::uint_fast32_t getVersion();
public:
    SQLiteCommon();

    virtual ~SQLiteCommon();

protected:

    /**
     * Perform common operations to set up the database after the database is opened.
     *
     * @return  true on success
     */

    bool common_open();

    /**
     * Create a table in the database.
     *
     * @param table     the table definition
     * @return          true on success
     */
    bool common_createTable(const TableDefinition &table);

    /**
     * Perform database optimization.
     *
     * @param tables    the tables in the database
     */
    void common_optimizeDatabase();

    /**
      * Create a merge (upsert) operation.
      *
      * @param table         the table to insert into
      * @param columns       the columns to insert
      * @return              a new merge operation
      */
    static std::string common_merge(const std::string &table, const Types::ColumnSet &columns);

    /**
     * A handler used to create a general operation in the specific driver.
     *
     * @param statement     the statement to execute
     * @param outputs       the number of outputs
     * @return              true on success
     */
    virtual Driver::Operation *common_general(const std::string &statement, size_t outputs = 0) = 0;
};

}
}
}

#endif //CPD3DATABASEDRIVERS_SQLITECOMMON_HXX
