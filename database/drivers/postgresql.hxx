/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATABASEDRIVERS_POSTGRESQL_HXX
#define CPD3DATABASEDRIVERS_POSTGRESQL_HXX

#include <cstdint>
#include <memory>

#include "core/first.hxx"

#include "database/database.hxx"
#include "database/driver.hxx"
#include "postgresqlcommon.hxx"

namespace CPD3 {
namespace Database {
namespace Drivers {

/**
 * A backend driver for PostgreSQL that uses libpq directly.
 */
class CPD3DATABASE_EXPORT PostgreSQL : public Driver, private PostgreSQLCommon {
public:
    struct Connection {
        std::string host;
        std::string db;
        std::string username;
        std::string password;
        std::uint_fast16_t port;

        Connection();
    };
private:
    Connection connection;
    void *db;
    std::uint_fast64_t sessionUID;
    std::uint_fast64_t uniqueCounter;

    std::string makePreparedStatementName();

    std::string makeCursorName();

    class PostgreSQLQueryExtract;

    friend class PostgreSQLQueryExtract;

    class PostgreSQLQueryCursor;

    friend class PostgreSQLQueryCursor;

    class PostgreSQLQueryBuffer;

    class PostgreSQLQuerySimple;


    class PostgreSQLOperationBase;

    friend class PostgreSQLOperationBase;

    class PostgreSQLOperationExtract;

    friend class PostgreSQLOperationExtract;

    class PostgreSQLOperationSimple;

public:
    PostgreSQL(Connection connection);

    virtual ~PostgreSQL();

    bool open() override;

    bool beginTransaction(int flags, double timeout) override;

    bool commitTransaction() override;

    void rollbackTransaction() override;

    std::unordered_set<std::string> loadTables() override;

    bool createTable(const TableDefinition &table) override;

    bool removeTable(const std::string &table) override;

    Driver::Operation *general(const std::string &statement, std::size_t outputs = 0) override;

    Operation *select(const Types::TableSet &from,
                      const Types::Outputs &select,
                      const std::string &where = std::string(),
                      const std::string &order = std::string()) override;

    Operation *insert(const std::string &table, const Types::ColumnSet &columns) override;

    Operation *insertSelect(const std::string &table,
                            const Types::Outputs &columns,
                            const Types::TableSet &from,
                            const Types::Outputs &select,
                            const std::string &where = std::string(),
                            const std::string &order = std::string()) override;


    Operation *merge(const std::string &table,
                     const Types::ColumnSet &keys,
                     const Types::ColumnSet &values) override;

    Operation *merge(const std::string &table, const Types::ColumnSet &columns) override;

    Operation *update(const std::string &table,
                      const Types::ColumnSet &columns,
                      const std::string &where = std::string()) override;

    Operation *del(const std::string &table, const std::string &where = std::string()) override;

    void optimizeDatabase() override;

protected:
    Operation *common_general(const std::string &statement, std::size_t outputs = 0,
                              GeneralType type = GeneralType::Unknown,
                              const std::string &pairedTable = {}) override;

    Operation *common_insert(const std::string &table, const Types::ColumnSet &columns) override;

    bool common_typeless() const override;
};

}
}
}

#endif //CPD3DATABASEDRIVERS_POSTGRESQL_HXX
