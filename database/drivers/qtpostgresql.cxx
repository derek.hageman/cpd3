/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <vector>
#include <algorithm>
#include <cstdint>
#include <cctype>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlRecord>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QLoggingCategory>

#include "qtpostgresql.hxx"
#include "core/timeutils.hxx"
#include "core/waitutils.hxx"
#include "core/util.hxx"
#include "core/qtcompat.hxx"


Q_LOGGING_CATEGORY(log_database_drivers_qtpostgresql, "cpd3.database.drivers.qtpostgresql",
                   QtWarningMsg)

namespace CPD3 {
namespace Database {
namespace Drivers {

QtPostgreSQL::QtPostgreSQL(const QSqlDatabase &d) : QtDatabase(d)
{ }

QtPostgreSQL::~QtPostgreSQL() = default;

Driver::Operation *QtPostgreSQL::common_general(const std::string &statement, size_t outputs, GeneralType type,
                                                const std::string &pairedTable)
{
    switch (type) {
    case GeneralType::Unknown:
        if (outputs)
            return general(statement, outputs);
        break;
    case GeneralType::Select:
    case GeneralType::Insert:
        return general(statement, outputs);
    }

    /* Qt's backend always uses prepared statements for QSqlQuery, but PostgreSQL only supports those
     * for limited types of statements.  So we have to have a simple one that just does the
     * QSqlDatabase::exec which does not do a PREPARE. */

    class DirectOperation : public Operation {
        QSqlDatabase &db;
        QString query;
    public:
        explicit DirectOperation(QSqlDatabase &db, QString query) : db(db), query(std::move(query))
        { }

        virtual ~DirectOperation() = default;

        Query *issue(const Types::Binds &binds, bool) override
        {
            Q_ASSERT(binds.empty());

            class DirectQuery : public Query {
                bool ok;
            public:
                explicit DirectQuery(bool ok) : ok(ok)
                { }

                virtual ~DirectQuery() = default;

                void begin() override
                { }

                bool isOk() override
                { return ok; }

                bool advance() override
                { return false; }

                Types::Results values() override
                { return {}; }

                void end() override
                { }
            };

            db.exec(query);
            if (db.lastError().isValid()) {
                qCDebug(log_database_drivers_qtpostgresql) << "Failed to execute:" << query << "-"
                                                           << db.lastError().text();
                return new DirectQuery(false);
            }
            return new DirectQuery(true);
        }

        bool batch(const Types::BatchBinds &) override
        {
            Q_ASSERT(false);
            return false;
        }
    };
    return new DirectOperation(db, QString::fromStdString(statement));
}

Driver::Operation *QtPostgreSQL::common_insert(const std::string &table, const Types::ColumnSet &columns)
{ return insert(table, columns); }

bool QtPostgreSQL::open()
{
    /* Set this here, since Qt5's connections sequence tries a warning generating query
     * before we can actually explicitly set the suppression below. */
    qputenv("PGOPTIONS", "-c client_min_messages=ERROR");

    QString baseOptions(db.connectOptions());
    QString options(baseOptions);
    if (!options.isEmpty())
        options.append(';');
    /* Set some sane defaults for keepalives, since it looks like if
     * the connections gets broken Qt's SQL driver just blocks
     * indefinitely the next time it tries to execute a query. */
    options.append("keepalives=1;keepalives_idle=15;keepalives_interval=10;keepalives_count=8");
    db.setConnectOptions(options);
    if (!db.open()) {
        /* As above, not sure a better way to detect this */
        /*if (!db.isOpenError() ||
                db.lastError().type() != QSqlError::ConnectionError ||
                !db.lastError().text().contains("keepalives"))
            return false;*/
        db.close();
        /* PSQL < 9.0 doesn't support keepalives, so try again without
         * them */
        db.setConnectOptions(baseOptions);
        if (!db.open()) {
            qCWarning(log_database_drivers_qtpostgresql) << "Failed to open database:"
                                                         << db.lastError().text();
            return false;
        }
    }

    /* "Should" relax this, but the Qt PSQL module causes constant
     * warning spam about non-standard binary escapes */
    db.exec("SET client_min_messages TO ERROR");
    if (db.lastError().isValid()) {
        qCWarning(log_database_drivers_qtpostgresql) << "Failed to set error message level:"
                                                     << db.lastError().text();
        if (!db.rollback()) {
            qCDebug(log_database_drivers_qtpostgresql) << "Rollback failed: "
                                                       << db.lastError().text();
        }
        return false;
    }

    return common_open();
}

bool QtPostgreSQL::beginTransaction(int flags, double timeout)
{
    QString isolationCommand = "SET TRANSACTION ";
    isolationCommand += QString::fromStdString(common_getTransactionIsolation(flags));

    double end = FP::undefined();
    if (FP::defined(timeout))
        end = Time::time() + timeout;
    for (int tc = 0;; tc++) {
        if (!db.isOpen())
            break;
        if (db.transaction()) {
            db.exec(isolationCommand);
            if (!db.lastError().isValid())
                return true;
            qCDebug(log_database_drivers_qtpostgresql)
                << "Failed to set transaction isolation level:" << db.lastError().text();
        }

        std::this_thread::yield();
        if (timeout <= 0.0 || tc > 10000 || (FP::defined(end) && Time::time() > end)) {
            qCDebug(log_database_drivers_qtpostgresql) << "Timeout starting transaction:"
                                                       << db.lastError().text();
            return false;
        }

        if (!db.isOpen() || (tc != 0 && (tc % 10) == 0)) {
            db.close();
            std::this_thread::yield();
            if (!this->open())
                return false;
        }

        Wait::backoffWait(tc);
    }
    if (db.lastError().isValid()) {
        qCDebug(log_database_drivers_qtpostgresql) << "Transaction start failed:"
                                                   << db.lastError().text();
    }
    return false;
}

bool QtPostgreSQL::createTable(const TableDefinition &table)
{ return common_createTable(table); }

Driver::Operation *QtPostgreSQL::merge(const std::string &table,
                                       const Types::ColumnSet &keys,
                                       const Types::ColumnSet &values)
{ return common_merge(table, keys, values); }

Driver::Operation *QtPostgreSQL::merge(const std::string &table, const Types::ColumnSet &columns)
{ return common_merge(table, columns); }

void QtPostgreSQL::optimizeDatabase()
{ return common_optimizeDatabase(loadTables()); }

bool QtPostgreSQL::common_typeless() const
{ return true; }

}
}
}