/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATABASEDRIVERS_POSTGRESQLCOMMON_HXX
#define CPD3DATABASEDRIVERS_POSTGRESQLCOMMON_HXX

#include "core/first.hxx"

#include <string>
#include <cstdint>
#include <functional>

#include "database/database.hxx"
#include "database/tabledefinition.hxx"
#include "database/util.hxx"
#include "database/driver.hxx"

namespace CPD3 {
namespace Database {
namespace Drivers {

/**
 * A shared base class for interfacing with PostgreSQL backends.
 */
class CPD3DATABASE_EXPORT PostgreSQLCommon {
    bool supports_InsertConflictClause;
    bool supports_DeferrableTransaction;

    bool issueVoid(const std::string &statement);

    std::string getVerionString();

    std::uint_fast32_t getVersion();

    bool prepareLanguage();

public:
    PostgreSQLCommon();

    virtual ~PostgreSQLCommon();

protected:
    /**
     * Perform common operations to set up the database after the connection is opened.
     *
     * @return  true on success
     */
    bool common_open();

    /**
     * Get the transaction isolation command.
     *
     * @param flags     the transaction flags
     * @return          true on success
     */
    std::string common_getTransactionIsolation(int flags);

    /**
     * Create a table in the database.
     *
     * @param table     the table definition
     * @return          true on success
     */
    bool common_createTable(const TableDefinition &table);

    /**
     * Perform database optimization.
     *
     * @param tables    the tables in the database
     */
    void common_optimizeDatabase(const std::unordered_set<std::string> &tables);

    /**
     * Create a merge (upsert) operation.
     *
     * @param table         the table to insert into
     * @param keys          the key columns
     * @param values        the value columns (if any)
     * @return              a new merge operation
     */
    Driver::Operation *common_merge(const std::string &table,
                                    const Types::ColumnSet &keys,
                                    const Types::ColumnSet &values);

    /**
     * Create a merge (upsert) operation, without specific key columns.
     *
     * @param table         the table to insert into
     * @param columns       the columns to insert
     * @return              a new merge operation
     */
    Driver::Operation *common_merge(const std::string &table, const Types::ColumnSet &columns);

    enum class GeneralType {
        Unknown,
        Select,
        Insert,
    };

    /**
     * A handler used to create a general operation in the specific driver.
     *
     * @param statement     the statement to execute
     * @param outputs       the number of outputs
     * @param type          the type of operation
     * @param pairedTable   the table the operation is paired with, for parameter type lookup
     * @return              the operation
     */
    virtual Driver::Operation *common_general(const std::string &statement, size_t outputs = 0,
                                              GeneralType type = GeneralType::Unknown,
                                              const std::string &pairedTable = {}) = 0;

    /**
     * A handler used to create an insert operation in the specific driver
     * @param table         the table to insert into
     * @param columns       the columns to write
     * @return              the operation
     */
    virtual Driver::Operation *common_insert(const std::string &table, const Types::ColumnSet &columns) = 0;


    /**
     * Test if the backend is typeless (i.e. does type conversion on the server).
     *
     * @return true         if the backend relies on server type conversion
     */
    virtual bool common_typeless() const = 0;
};

}
}
}

#endif //CPD3DATABASEDRIVERS_POSTGRESQLCOMMON_HXX
