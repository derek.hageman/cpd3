/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATABASRDRIVERS_QTPOSTGRESQL_HXX
#define CPD3DATABASRDRIVERS_QTPOSTGRESQL_HXX

#include "core/first.hxx"

#include "database/database.hxx"
#include "qtdatabase.hxx"
#include "postgresqlcommon.hxx"

namespace CPD3 {
namespace Database {
namespace Drivers {

/**
 * The backend driver for dealing with Qt managed PostgreSQL databases.
 */
class CPD3DATABASE_EXPORT QtPostgreSQL : public QtDatabase, private PostgreSQLCommon {
public:
    QtPostgreSQL(const QSqlDatabase &db);

    virtual ~QtPostgreSQL();

    virtual bool open();

    virtual bool beginTransaction(int flags, double timeout);

    virtual bool createTable(const TableDefinition &table);

    virtual Operation *merge(const std::string &table,
                             const Types::ColumnSet &keys,
                             const Types::ColumnSet &values);

    virtual Operation *merge(const std::string &table, const Types::ColumnSet &columns);

    virtual void optimizeDatabase();

protected:
    Operation *common_general(const std::string &statement, size_t outputs = 0,
            GeneralType type = GeneralType::Unknown,
            const std::string &pairedTable = {}) override;

    Operation *common_insert(const std::string &table, const Types::ColumnSet &columns) override;

    bool common_typeless() const override;
};

}
}
}

#endif //CPD3DATABASRDRIVERS_QTPOSTGRESQL_HXX
