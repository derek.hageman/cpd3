/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATABASRDRIVERS_QTSQLITE_HXX
#define CPD3DATABASRDRIVERS_QTSQLITE_HXX

#include "core/first.hxx"

#include "database/database.hxx"
#include "qtdatabase.hxx"
#include "sqlitecommon.hxx"

namespace CPD3 {
namespace Database {
namespace Drivers {

/**
 * The backend driver for Qt SQLite databases.
 */
class CPD3DATABASE_EXPORT QtSqlite : public QtDatabase, private SQLiteCommon {
    bool supports_OptionalRowID;
public:
    QtSqlite(const QSqlDatabase &db);

    virtual ~QtSqlite();

    bool open() override;

    bool beginTransaction(int flags, double timeout) override;

    bool commitTransaction() override;

    void rollbackTransaction() override;

    bool createTable(const TableDefinition &table) override;

    Operation *merge(const std::string &table,
                             const Types::ColumnSet &keys,
                             const Types::ColumnSet &values) override;

    Operation *merge(const std::string &table, const Types::ColumnSet &columns) override;

    void optimizeDatabase() override;

protected:
    Operation *common_general(const std::string &statement, size_t outputs = 0) override;
};

}
}
}

#endif //CPD3DATABASRDRIVERS_QTSQLITE_HXX
