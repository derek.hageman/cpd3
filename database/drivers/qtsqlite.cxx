/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QSqlError>
#include <QSqlQuery>
#include <QVariant>
#include <QFile>
#include <QUuid>
#include <QLoggingCategory>

#include "qtsqlite.hxx"
#include "database/tabledefinition.hxx"
#include "core/waitutils.hxx"
#include "core/timeutils.hxx"
#include "core/qtcompat.hxx"


Q_LOGGING_CATEGORY(log_database_drivers_qtsqlite, "cpd3.database.drivers.qtsqlite", QtWarningMsg)

namespace CPD3 {
namespace Database {
namespace Drivers {

QtSqlite::QtSqlite(const QSqlDatabase &d) : QtDatabase(d)
{ }

QtSqlite::~QtSqlite() = default;

Driver::Operation *QtSqlite::common_general(const std::string &statement, size_t outputs)
{ return general(statement, outputs); }

bool QtSqlite::open()
{
    ElapsedTimer to;
    to.start();
    int t = 0;
    QString options(db.connectOptions());
    if (!options.isEmpty())
        options.append(';');
    options.append("QSQLITE_BUSY_TIMEOUT=500");
    for (;;) {
        db.setConnectOptions(options);
        if (db.open())
            break;
        /* Crude, but I don't see a better way of doing it */
        if (!db.lastError().text().contains("database is locked", Qt::CaseInsensitive)) {
            qCWarning(log_database_drivers_qtsqlite) << "Failed to open database:"
                                                     << db.lastError().text();
            return false;
        }

        int remaining = 30 * 60 * 1000 - to.elapsed();
        if (remaining < 0 || t > 100000) {
            qCWarning(log_database_drivers_qtsqlite) << "Database open timeout:"
                                                     << db.lastError().text();
            return false;
        }

        db.close();
        Wait::backoffWait(t, FP::undefined(), remaining / 1000.0);
        ++t;
    }

    return common_open();
}

bool QtSqlite::beginTransaction(int flags, double timeout)
{
    double end = FP::undefined();
    if (FP::defined(timeout))
        end = Time::time() + timeout;
    for (int tc = 0;; tc++) {
        if (!db.isOpen())
            break;
        if (flags & Driver::Transaction_ReadOnly) {
            if (flags & Driver::Transaction_Serializable) {
                db.exec("BEGIN IMMEDIATE TRANSACTION");
            } else {
                db.exec("BEGIN DEFERRED TRANSACTION");
            }
        } else {
            db.exec("BEGIN IMMEDIATE TRANSACTION");
        }
        if (!db.lastError().isValid())
            return true;

        std::this_thread::yield();
        if (timeout <= 0.0 || tc > 10000 || (FP::defined(end) && Time::time() > end)) {
            qCDebug(log_database_drivers_qtsqlite) << "Timeout starting transaction:"
                                                   << db.lastError().text();
            return false;
        }

        if (!db.isOpen() || (tc != 0 && (tc % 10) == 0)) {
            db.close();
            std::this_thread::yield();
            if (!this->open())
                return false;
        }

        Wait::backoffWait(tc);
    }
    if (db.lastError().isValid()) {
        qCDebug(log_database_drivers_qtsqlite) << "Transaction start failed:"
                                               << db.lastError().text();
    }
    return false;
}

bool QtSqlite::commitTransaction()
{
    db.exec("COMMIT TRANSACTION");
    if (db.lastError().isValid()) {
        qCDebug(log_database_drivers_qtsqlite) << "Transaction commit failed:"
                                               << db.lastError().text();
        db.exec("ROLLBACK");
        return false;
    }
    return true;
}

void QtSqlite::rollbackTransaction()
{
    db.exec("ROLLBACK");
}

bool QtSqlite::createTable(const TableDefinition &table)
{ return common_createTable(table); }

Driver::Operation *QtSqlite::merge(const std::string &table,
                                   const Types::ColumnSet &keys,
                                   const Types::ColumnSet &values)
{
    Types::ColumnSet combined(keys);
    for (const auto &add : values) {
        combined.insert(add);
    }
    return merge(table, combined);
}

Driver::Operation *QtSqlite::merge(const std::string &table, const Types::ColumnSet &columns)
{ return general(common_merge(table, columns)); }

void QtSqlite::optimizeDatabase()
{ return common_optimizeDatabase(); }

}
}
}