/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <libpq-fe.h>
#if defined(HAVE_POSTGRESQL_TYPES_D)
#include <catalog/pg_type_d.h>
#else
#define BOOLOID         16
#define BYTEAOID        17
#define CHAROID         18
#define NAMEOID         19
#define INT8OID         20
#define INT2OID         21
#define INT4OID         23
#define TEXTOID         25
#define FLOAT4OID       700
#define FLOAT8OID       701
#define BPCHAROID       1042
#define VARCHAROID      1043
#define NUMERICOID      1700
#endif

#include <cstring>
#include <cmath>

#include <QLoggingCategory>
#include <QtEndian>

#include "postgresql.hxx"
#include "core/waitutils.hxx"
#include "core/timeutils.hxx"
#include "core/qtcompat.hxx"



Q_LOGGING_CATEGORY(log_database_drivers_postgresql, "cpd3.database.drivers.postgresql",
                   QtWarningMsg)


namespace CPD3 {
namespace Database {
namespace Drivers {

PostgreSQL::Connection::Connection() : port(0)
{}

PostgreSQL::PostgreSQL(Connection connection) : connection(std::move(connection)), db(nullptr),
                                                sessionUID(1), uniqueCounter(1)
{}

PostgreSQL::~PostgreSQL()
{
    if (db) {
        ::PQfinish(static_cast<::PGconn *>(db));
    }
}

namespace {
using Result = std::unique_ptr<PGresult, decltype(&(::PQclear))>;
}

static Result execResult(void *db, const char *str)
{ return Result(::PQexec(static_cast<PGconn *>(db), str), &(::PQclear)); }

static Result execResult(void *db, const std::string &str)
{ return execResult(db, str.c_str()); }

static Result prepareStatement(void *db, const std::string &name,
                               const std::string &query,
                               const std::vector<::Oid> &types)
{
    return Result(::PQprepare(static_cast<PGconn *>(db), name.c_str(), query.c_str(),
                              static_cast<int>(types.size()), types.data()),
                  &(::PQclear));
}

static Result execPrepared(void *db, const std::string &name,
                           const std::vector<const char *> &value,
                           const std::vector<int> &size,
                           const std::vector<int> &format)
{
    return Result(::PQexecPrepared(static_cast<PGconn *>(db), name.c_str(),
                                   static_cast<int>(value.size()), value.data(), size.data(), format.data(), 1),
                  &(::PQclear));
}

static Result execParameters(void *db, const std::string &query,
                             const std::vector<::Oid> &types,
                             const std::vector<const char *> &value,
                             const std::vector<int> &size,
                             const std::vector<int> &format)
{
    return Result(::PQexecParams(static_cast<PGconn *>(db), query.c_str(),
                                 static_cast<int>(types.size()), types.data(),
                                 value.data(), size.data(), format.data(), 1),
                  &(::PQclear));
}

static Types::Variant decodeType(::Oid type, const void *data, std::size_t size)
{
    switch (type) {
    case BOOLOID:
        if (!size)
            return {};
        return {*reinterpret_cast<const char *>(data) ? 1 : 0};

    case BYTEAOID:
        return {CPD3::Util::ByteArray(data, size)};

    case CHAROID:
        if (!size)
            return {};
        return {std::string(reinterpret_cast<const char *>(data), 1)};

    case VARCHAROID:
    case BPCHAROID:
    case TEXTOID:
    case NAMEOID:
        return {std::string(reinterpret_cast<const char *>(data), size)};

    case INT2OID:
        if (size < 2)
            return {};
        return {static_cast<std::int_fast64_t>(qFromBigEndian<qint16>(reinterpret_cast<const uchar *>(data)))};
    case INT4OID:
        if (size < 4)
            return {};
        return {static_cast<std::int_fast64_t>(qFromBigEndian<qint32>(reinterpret_cast<const uchar *>(data)))};
    case INT8OID:
        if (size < 8)
            return {};
        return {static_cast<std::int_fast64_t>(qFromBigEndian<qint64>(reinterpret_cast<const uchar *>(data)))};

    case NUMERICOID: {
        if (size < 8)
            return {};
        auto ptr = reinterpret_cast<const std::uint8_t *>(data);
        auto nDigits = qFromBigEndian<quint16>(reinterpret_cast<const uchar *>(ptr));
        ptr += 2;
        auto weight = qFromBigEndian<qint16>(reinterpret_cast<const uchar *>(ptr));
        ptr += 2;
        auto sign = qFromBigEndian<quint16>(reinterpret_cast<const uchar *>(ptr));
        ptr += 2;
        auto dscale = qFromBigEndian<quint16>(reinterpret_cast<const uchar *>(ptr));
        ptr += 2;
        std::vector<quint16> digits;
        for (size -= 8; nDigits != 0 && size >= 2; --nDigits, size -= 2) {
            digits.emplace_back(qFromBigEndian<quint16>(reinterpret_cast<const uchar *>(ptr)));
            ptr += 2;
        }
        if (sign == 0xC000)
            return {FP::undefined()};

        double result = 0;
        auto digit = digits.begin();
        auto end = digits.end();
        for (qint16 i = 0; i <= weight; i++) {
            result *= 10000;
            if (digit == end)
                continue;

            auto v = *digit;
            ++digit;

            auto d = v / 1000;
            v -= d * 1000;
            result += v * 1000;

            d = v / 100;
            v -= d * 100;
            result += v * 100;

            d = v / 10;
            v -= d * 10;
            result += v * 10;

            result += v;
        }

        for (quint16 i = 0; i <= (dscale & 0x3FF); i++) {
            if (digit == end)
                break;

            auto v = *digit;
            ++digit;

            auto d = v / 1000;
            v -= d * 1000;
            result += v * std::pow(10.0, -(i * 4 + 1));

            d = v / 100;
            v -= d * 100;
            result += v * std::pow(10.0, -(i * 4 + 2));

            d = v / 10;
            v -= d * 10;
            result += v * std::pow(10.0, -(i * 4 + 3));

            result += v * std::pow(10.0, -(i * 4 + 4));
        }

        if (sign == 0x4000)
            result = -result;

        return {result};
    }

    case FLOAT4OID: {
        if (size < 4)
            return {};
        auto decoded = qFromBigEndian<quint32>(reinterpret_cast<const uchar *>(data));
        float result = 0;
        std::memcpy(&result, &decoded, 4);
        if (!std::isfinite(result))
            return {FP::undefined()};
        return {static_cast<double>(result)};
    }
    case FLOAT8OID: {
        if (size < 8)
            return {};
        auto decoded = qFromBigEndian<quint64>(reinterpret_cast<const uchar *>(data));
        double result = 0;
        std::memcpy(&result, &decoded, 8);
        if (!std::isfinite(result))
            return {FP::undefined()};
        return {result};
    }

    default:
        break;
    }

    return {};
}

static void translateType(const Types::Variant &variant, ::Oid &type, const char *&data, CPD3::Util::ByteArray &storage,
                          int &size, int &format)
{
    switch (variant.type()) {
    case Types::Variant::Type::Empty:
    case Types::Variant::Type::Null:
        size = 0;
        data = nullptr;
        break;
    case Types::Variant::Type::Integer:
        type = INT8OID;
        if (!INTEGER::defined(variant.toInteger())) {
            size = 0;
            data = nullptr;
            break;
        }
        format = 1;
        size = 8;
        storage.resize(8);
        qToBigEndian<qint64>(variant.toInteger(), storage.data<uchar *>());
        data = storage.data<const char *>();
        break;
    case Types::Variant::Type::Real: {
        type = FLOAT8OID;
        if (!FP::defined(variant.toReal())) {
            size = 0;
            data = nullptr;
            break;
        }
        quint64 encoded;
        double temp = variant.toReal();
        std::memcpy(&encoded, &temp, 8);
        format = 1;
        size = 8;
        storage.resize(8);
        qToBigEndian<qint64>(encoded, storage.data<uchar *>());
        data = storage.data<const char *>();
        break;
    }
    case Types::Variant::Type::String: {
        type = TEXTOID;
        format = 1;
        const auto &str = variant.toString();
        size = static_cast<int>(str.size());
        data = str.c_str();
        break;
    }
    case Types::Variant::Type::Bytes: {
        type = BYTEAOID;
        format = 1;
        const auto &b = variant.toBytes();
        size = static_cast<int>(b.size());
        data = b.data<const char *>();
        break;
    }
    }
}


class PostgreSQL::PostgreSQLQueryExtract : public Driver::Query {
    std::size_t extract;
    Result res;
    enum class State {
        Initialize, First, Valid, Complete, Error,
    } state;
    std::size_t totalRows;
    std::size_t readRows;
    std::vector<::Oid> types;

protected:
    virtual Result firstResult() = 0;

    virtual Result nextResult(bool &)
    { return {nullptr, &(::PQclear)}; }

    explicit PostgreSQLQueryExtract(std::size_t extract) : extract(extract),
                                                           res(nullptr, &(::PQclear)), state(State::Initialize),
                                                           totalRows(0), readRows(0)
    {
        Q_ASSERT(extract > 0);
    }

    void releaseResult()
    { res.reset(); }

private:
    bool readAdvance()
    {
        switch (::PQresultStatus(res.get())) {
        case PGRES_TUPLES_OK:
            break;
        case PGRES_COMMAND_OK: {
            state = State::Complete;
            res.reset();
            return false;
        }
        default:
            qCDebug(log_database_drivers_postgresql) << "Query execution failed:"
                                                     << ::PQresultErrorMessage(res.get());
            state = State::Error;
            res.reset();
            return false;
        }

        totalRows = static_cast<std::size_t>(::PQntuples(res.get()));
        if (!totalRows) {
            state = State::Complete;
            res.reset();
            return false;
        }

        auto cols = ::PQnfields(res.get());
        Q_ASSERT(cols >= 0);
        types.resize(static_cast<std::size_t>(cols), 0);
        for (int col = 0; col < cols; ++col) {
            types[col] = ::PQftype(res.get(), col);
        }

        readRows = 0;

        return true;
    }

public:
    virtual ~PostgreSQLQueryExtract() = default;

    void begin() override
    {
        if (state != State::Initialize)
            return;
        Q_ASSERT(res.get() == nullptr);

        res = firstResult();
        if (!res) {
            state = State::Error;
            return;
        }

        state = State::First;
    }

    bool isOk() override
    { return state != State::Error; }

    bool advance() override
    {
        switch (state) {
        case State::Initialize:
            return false;
        case State::First:
            if (!readAdvance())
                return false;
            state = State::Valid;
            return true;
        case State::Valid: {
            Q_ASSERT(res.get() != nullptr);

            ++readRows;
            if (readRows >= totalRows) {
                bool ok = true;
                res = nextResult(ok);
                if (!res) {
                    if (ok) {
                        state = State::Complete;
                    } else {
                        state = State::Error;
                    }
                    return false;
                }
                Q_ASSERT(ok);
                if (!readAdvance())
                    return false;
            }

            return true;
        }
        case State::Complete:
            return false;
        case State::Error:
            return false;
        }
        Q_ASSERT(false);
        return false;
    }

    Types::Results values() override
    {
        Q_ASSERT(state == State::Valid);
        Q_ASSERT(res.get() != nullptr);

        int row = static_cast<int>(readRows);
        int totalColumns = static_cast<int>(types.size());

        Types::Results result;
        for (int col = 0; col < totalColumns; col++) {
            if (::PQgetisnull(res.get(), row, col)) {
                result.emplace_back(Types::Variant::Type::Null);
                continue;
            }

            auto data = ::PQgetvalue(res.get(), row, col);
            auto size = ::PQgetlength(res.get(), row, col);
            if (size < 0)
                size = 0;
            result.emplace_back(decodeType(types[col], data, static_cast<std::size_t>(size)));
        }

        result.resize(extract);
        return result;
    }

    void end() override
    {
        switch (state) {
        case State::Initialize:
        case State::First:
        case State::Valid:
            break;
        case State::Complete:
        case State::Error:
            return;
        }

        res.reset();
        state = State::Complete;
    }
};

class PostgreSQL::PostgreSQLQueryCursor : public PostgreSQL::PostgreSQLQueryExtract {
    PostgreSQL &driver;
    std::string fetch;
    std::string cursor;

    void releaseAll()
    {
        releaseResult();

        if (!cursor.empty()) {
            auto q = execResult(driver.db, "CLOSE " + cursor);
            if (!q) {
                qCWarning(log_database_drivers_postgresql) << "Close failed for" << cursor << ":"
                                                           << ::PQerrorMessage(static_cast<PGconn *>(driver.db));
            } else if (::PQresultStatus(q.get()) != PGRES_COMMAND_OK) {
                qCWarning(log_database_drivers_postgresql) << "Close failed for" << cursor << ":"
                                                           << ::PQresultErrorMessage(q.get());
            }

            cursor.clear();
        }
    }

protected:

    Result firstResult() override
    {
        auto res = ::PQexecParams(static_cast<PGconn *>(driver.db),
                                  fetch.c_str(), 0, nullptr, nullptr, nullptr, nullptr, 1);
        if (!res) {
            qCDebug(log_database_drivers_postgresql) << "Execution of" << fetch << "failed";
        }
        return {res, &(::PQclear)};
    }

    Result nextResult(bool &ok) override
    {
        auto res = ::PQexecParams(static_cast<PGconn *>(driver.db),
                                  fetch.c_str(), 0, nullptr, nullptr, nullptr, nullptr, 1);
        if (!res) {
            qCDebug(log_database_drivers_postgresql) << "Execution of" << fetch << "failed";
            ok = false;
        }
        return {res, &(::PQclear)};
    }

public:
    PostgreSQLQueryCursor(PostgreSQL &driver, std::string fetch, std::string cursor, std::size_t extract) :
            PostgreSQLQueryExtract(extract),
            driver(driver), fetch(std::move(fetch)), cursor(std::move(cursor))
    {
        Q_ASSERT(!this->fetch.empty());
    }

    virtual ~PostgreSQLQueryCursor()
    { releaseAll(); }

    void end() override
    {
        PostgreSQLQueryExtract::end();
        releaseAll();
    }
};

class PostgreSQL::PostgreSQLQueryBuffer : public PostgreSQL::PostgreSQLQueryExtract {
    Result res;
protected:

    Result firstResult() override
    { return std::move(res); }

public:
    PostgreSQLQueryBuffer(Result &&res, std::size_t extract) :
            PostgreSQLQueryExtract(extract), res(std::move(res))
    {}

    virtual ~PostgreSQLQueryBuffer() = default;
};

class PostgreSQL::PostgreSQLQuerySimple : public Driver::Query {
    Result res;
    enum class State {
        Initialize, Complete, Error,
    } state;

public:
    explicit PostgreSQLQuerySimple(Result &&res) : res(std::move(res)), state(State::Initialize)
    {}

    virtual ~PostgreSQLQuerySimple() = default;

    void begin() override
    {
        if (state != State::Initialize)
            return;
        Q_ASSERT(res.get() != nullptr);

        switch (::PQresultStatus(res.get())) {
        case PGRES_TUPLES_OK:
        case PGRES_COMMAND_OK:
            state = State::Complete;
            break;

        default:
            qCDebug(log_database_drivers_postgresql) << "Query execution failed:"
                                                     << ::PQresultErrorMessage(res.get());
            state = State::Error;
            break;
        }

        res.reset();
    }

    bool isOk() override
    { return state != State::Error; }

    bool advance() override
    { return false; }

    Types::Results values() override
    { return {}; }

    void end() override
    {
        res.reset();
        state = State::Complete;
    }
};


class PostgreSQL::PostgreSQLOperationBase : public Driver::Operation {
public:
    enum class Type {
        Unknown,
        Select,
        Insert,
        Update,
        Delete,
        Values,
    };
private:
    static bool isValidParameter(char c)
    { return std::isalnum(c) || c == '_'; }

    static std::pair<std::string, std::vector<std::string>> formatQuery(std::string query,
                                                                        const std::unordered_map<std::string, std::string> &types = {})
    {
        std::vector<std::string> parameters;
        std::unordered_map<std::string, std::string> existing;

        auto pos = query.begin();
        char closing = 0;
        for (; pos != query.end();) {
            if (closing) {
                if (*pos == closing) {
                    ++pos;
                    closing = 0;
                    continue;
                }
                if (*pos == '\\') {
                    ++pos;
                    if (pos == query.end())
                        break;
                }
                ++pos;
                continue;
            }

            switch (*pos) {
            case ':': {
                if (pos != query.begin() && isValidParameter(*(pos - 1))) {
                    ++pos;
                    break;
                }
                if (pos+1 != query.end() && *(pos + 1) == ':') {
                    pos += 2;
                    break;
                }

                auto begin = pos;
                for (++pos; pos != query.end() && isValidParameter(*pos); ++pos) {}
                std::string name(begin + 1, pos);
                if (name.empty()) {
                    qCDebug(log_database_drivers_postgresql) << "Nameless parameter in" << query;
                    break;
                }

                std::string id;
                auto check = existing.find(name);
                if (check != existing.end()) {
                    id = check->second;
                } else {
                    id = "$" + std::to_string(parameters.size() + 1);

                    auto type = types.find(name);
                    if (type != types.end()) {
                        id += "::";
                        id += type->second;
                    }

                    existing.emplace(name, id);
                    parameters.emplace_back(std::move(name));
                }

                auto offset = begin - query.begin();
                query.replace(begin, pos, id);
                pos = query.begin() + (offset + id.size());
                break;
            }

            case '\'':
            case '"':
                closing = *pos;
                ++pos;
                break;

            default:
                ++pos;
                break;
            }
        }

        return {std::move(query), std::move(parameters)};
    }

protected:
    PostgreSQL &driver;
    Type type;
    std::uint_fast64_t formattedSession;
    std::uint_fast64_t preparedSession;
    std::string rawStatement;

    std::string pairedTable;

    std::string formattedQuery;
    std::vector<std::string> parameters;

    std::string preparedName;
    std::vector<::Oid> preparedTypes;

    bool canUsePrepared() const
    {
        switch (type) {
        case Type::Unknown:
            return false;
        case Type::Select:
        case Type::Insert:
        case Type::Update:
        case Type::Delete:
        case Type::Values:
            return true;
        }
        Q_ASSERT(false);
        return false;
    }

    bool formattedInvalidated() const
    { return formattedSession != driver.sessionUID; }

    bool preparedInvalidated() const
    { return preparedSession != driver.sessionUID; }

    PostgreSQLOperationBase(PostgreSQL &driver, Type type, std::string statement, std::string pairedTable = {}) :
            driver(driver), type(type), formattedSession(0), preparedSession(0),
            rawStatement(CPD3::Util::trimmed(std::move(statement))),
            pairedTable(std::move(pairedTable))
    {}

    bool updateQuery()
    {
        std::unordered_map<std::string, std::string> parameterTypeMapping;

        switch (type) {
        case Type::Unknown:
        case Type::Select:
        case Type::Delete:
        case Type::Values:
            break;
        case Type::Insert:
        case Type::Update:
            if (!pairedTable.empty()) {
                auto res = execResult(driver.db, R"(
SELECT c.column_name, c.data_type
    FROM pg_catalog.pg_statio_all_tables AS st
    INNER JOIN pg_catalog.pg_description pgd ON (pgd.objoid = st.relid)
    RIGHT OUTER JOIN information_schema.columns c ON (pgd.objsubid = c.ordinal_position
       AND c.table_schema = st.schemaname AND c.table_name = st.relname)
    WHERE table_schema = 'public' AND table_name = ')" + pairedTable + "'");
                if (res) {
                    if (::PQresultStatus(res.get()) == PGRES_TUPLES_OK) {
                        Q_ASSERT(::PQnfields(res.get()) >= 2);

                        for (int i = 0, max = ::PQntuples(res.get()); i < max; i++) {
                            auto column = ::PQgetvalue(res.get(), i, 0);
                            if (!column || !column[0])
                                continue;
                            auto tname = ::PQgetvalue(res.get(), i, 1);
                            if (!tname || !tname[0])
                                continue;
                            parameterTypeMapping.emplace(column, tname);
                        }
                    } else {
                        qCWarning(log_database_drivers_postgresql) << "Type fetch failed on table" << pairedTable
                                                                   << ":" << ::PQresultErrorMessage(res.get());
                    }
                } else {
                    qCWarning(log_database_drivers_postgresql) << "Type fetch failed on table" << pairedTable
                                                               << "connection error:"
                                                               << ::PQerrorMessage(static_cast<PGconn *>(driver.db));
                }
            }
            break;
        }

        auto fmt = formatQuery(rawStatement, parameterTypeMapping);
        if (fmt.first.empty()) {
            qCWarning(log_database_drivers_postgresql) << "Translation of statement" << rawStatement << "failed";
            return false;
        }

        formattedQuery = std::move(fmt.first);
        parameters = std::move(fmt.second);
        preparedSession = 0;
        preparedTypes.clear();
        formattedSession = driver.sessionUID;

        return true;
    }

    bool updatePrepared()
    {
        Q_ASSERT(canUsePrepared());

        if (!preparedName.empty() && !preparedInvalidated()) {
            auto res = execResult(driver.db, "DEALLOCATE " + preparedName);
            if (!res) {
                qCDebug(log_database_drivers_postgresql) << "Error freeing statement" << preparedName
                                                         << "connection error:"
                                                         << ::PQerrorMessage(static_cast<PGconn *>(driver.db));
            } else if (::PQresultStatus(res.get()) != PGRES_COMMAND_OK) {
                qCDebug(log_database_drivers_postgresql) << "Error freeing statement" << preparedName << ":"
                                                         << ::PQresultErrorMessage(res.get());
            }

            preparedName.clear();
        }

        auto name = driver.makePreparedStatementName();
        auto res = prepareStatement(driver.db, name, formattedQuery, preparedTypes);
        if (!res) {
            qCDebug(log_database_drivers_postgresql) << "Error preparing" << rawStatement
                                                     << "connection error:"
                                                     << ::PQerrorMessage(static_cast<PGconn *>(driver.db));
            return false;
        } else if (::PQresultStatus(res.get()) != PGRES_COMMAND_OK) {
            qCDebug(log_database_drivers_postgresql) << "Error preparing" << rawStatement << ":"
                                                     << ::PQresultErrorMessage(res.get());
            return false;
        }

        preparedName = std::move(name);
        preparedSession = driver.sessionUID;
        return true;
    }

    Result prepareAndExecute(const Types::Binds &binds)
    {
        std::vector<const char *> parameterValues(parameters.size(), nullptr);
        std::vector<CPD3::Util::ByteArray> parameterStorage(parameters.size());
        std::vector<int> parameterSize(parameters.size(), 0);
        std::vector<int> parameterFormat(parameters.size(), 0);

        bool prepare = preparedInvalidated();
        if (parameters.empty()) {
            if (!preparedTypes.empty())
                prepare = true;
            preparedTypes.clear();
        } else if (!parameters.empty() && preparedTypes.empty()) {
            prepare = true;
            preparedTypes.resize(parameters.size(), TEXTOID);

            for (std::size_t p = 0, nP = parameters.size(); p < nP; ++p) {
                auto source = binds.find(parameters[p]);
                if (source == binds.end()) {
                    qCDebug(log_database_drivers_postgresql) << "No bound values for parameter"
                                                             << parameters[p] << "in query" << rawStatement;
                    translateType({}, preparedTypes[p], parameterValues[p],
                                  parameterStorage[p], parameterSize[p], parameterFormat[p]);
                    continue;
                }

                translateType(source->second, preparedTypes[p], parameterValues[p],
                              parameterStorage[p], parameterSize[p], parameterFormat[p]);
            }
        } else {
            if (preparedTypes.size() != parameters.size()) {
                prepare = true;
                preparedTypes.resize(parameters.size(), TEXTOID);
            }
            for (std::size_t p = 0, nP = parameters.size(); p < nP; ++p) {
                ::Oid type = preparedTypes[p];

                auto source = binds.find(parameters[p]);
                if (source == binds.end()) {
                    qCDebug(log_database_drivers_postgresql) << "No bound values for parameter"
                                                             << parameters[p] << "in query" << rawStatement;
                    translateType({}, type, parameterValues[p],
                                  parameterStorage[p], parameterSize[p], parameterFormat[p]);
                    continue;
                }

                translateType(source->second, type, parameterValues[p],
                              parameterStorage[p], parameterSize[p], parameterFormat[p]);

                if (type != preparedTypes[p]) {
                    preparedTypes[p] = type;
                    prepare = true;
                }
            }
        }

        if (prepare) {
            if (!updatePrepared()) {
                return {nullptr, &(::PQclear)};
            }
        }

        auto res = execPrepared(driver.db, preparedName, parameterValues, parameterSize, parameterFormat);
        if (!res) {
            qCDebug(log_database_drivers_postgresql) << "Error executing" << rawStatement << "connection error:"
                                                     << ::PQerrorMessage(static_cast<PGconn *>(driver.db));
            return {nullptr, &(::PQclear)};
        }
        return res;
    }

    Result executeStandalone(const Types::Binds &binds)
    {
        std::vector<::Oid> types(parameters.size(), TEXTOID);
        std::vector<const char *> parameterValues(parameters.size(), nullptr);
        std::vector<CPD3::Util::ByteArray> parameterStorage(parameters.size());
        std::vector<int> parameterSize(parameters.size(), 0);
        std::vector<int> parameterFormat(parameters.size(), 0);

        for (std::size_t p = 0, nP = parameters.size(); p < nP; ++p) {
            auto source = binds.find(parameters[p]);
            if (source == binds.end()) {
                qCDebug(log_database_drivers_postgresql) << "No bound values for parameter"
                                                         << parameters[p] << "in query" << formattedQuery;
                translateType({}, types[p], parameterValues[p],
                              parameterStorage[p], parameterSize[p], parameterFormat[p]);
                continue;
            }

            translateType(source->second, types[p], parameterValues[p],
                          parameterStorage[p], parameterSize[p], parameterFormat[p]);
        }

        auto res = execParameters(driver.db, formattedQuery, types, parameterValues, parameterSize, parameterFormat);
        if (!res) {
            qCDebug(log_database_drivers_postgresql) << "Error executing" << rawStatement << "connection error:"
                                                     << ::PQerrorMessage(static_cast<PGconn *>(driver.db));
            return {nullptr, &(::PQclear)};
        }
        return res;
    }

public:

    virtual ~PostgreSQLOperationBase()
    {
        if (!preparedName.empty()) {
            auto res = execResult(driver.db, "DEALLOCATE " + preparedName);
            if (!res) {
                qCDebug(log_database_drivers_postgresql) << "Error freeing statement" << preparedName
                                                         << "connection error:"
                                                         << ::PQerrorMessage(static_cast<PGconn *>(driver.db));
            } else if (::PQresultStatus(res.get()) != PGRES_COMMAND_OK) {
                qCDebug(log_database_drivers_postgresql) << "Error freeing statement" << preparedName << ":"
                                                         << ::PQresultErrorMessage(res.get());
            }
        }
    }

    bool batch(const Types::BatchBinds &binds) override
    {
        if (!driver.db)
            return false;
        std::size_t rows = 0;
        for (const auto &add : binds) {
            if (add.second.size() > rows)
                rows = add.second.size();
        }
        if (!rows)
            return true;

        if (formattedInvalidated()) {
            if (!updateQuery())
                return false;
        }

        std::vector<const Types::BatchVariants *> parameterSources;
        for (const auto &p : parameters) {
            auto check = binds.find(p);
            if (check == binds.end()) {
                parameterSources.emplace_back(nullptr);
                continue;
            }
            parameterSources.emplace_back(&(check->second));
        }

        std::vector<const char *> parameterValues(parameters.size(), nullptr);
        std::vector<CPD3::Util::ByteArray> parameterStorage(parameters.size());
        std::vector<int> parameterSize(parameters.size(), 0);
        std::vector<int> parameterFormat(parameters.size(), 0);

        if (!canUsePrepared()) {
            std::vector<::Oid> types(parameters.size(), TEXTOID);

            for (std::size_t row = 0; row < rows; ++row) {
                for (std::size_t p = 0, nP = parameters.size(); p < nP; ++p) {
                    auto source = parameterSources[p];
                    if (!source || row >= source->size()) {
                        translateType({}, types[p], parameterValues[p],
                                      parameterStorage[p], parameterSize[p], parameterFormat[p]);
                    } else {
                        translateType((*source)[row], types[p], parameterValues[p],
                                      parameterStorage[p], parameterSize[p], parameterFormat[p]);
                    }
                }

                auto res = execParameters(driver.db, formattedQuery,
                        types, parameterValues, parameterSize, parameterFormat);
                if (!res) {
                    qCDebug(log_database_drivers_postgresql) << "Error executing" << rawStatement << "on row" << row
                                                             << "connection error:"
                                                             << ::PQerrorMessage(static_cast<PGconn *>(driver.db));
                    return false;
                } else {
                    switch (::PQresultStatus(res.get())) {
                    case PGRES_COMMAND_OK:
                    case PGRES_TUPLES_OK:
                        break;
                    default:
                        qCDebug(log_database_drivers_postgresql) << "Error preparing" << rawStatement << ":" << "on row"
                                                                 << row << ::PQresultErrorMessage(res.get());
                        return false;
                    }
                }
            }

            return true;
        }

        bool prepare = preparedInvalidated();
        for (std::size_t row = 0; row < rows; ++row) {

            if (parameters.empty()) {
                if (!preparedTypes.empty())
                    prepare = true;
                preparedTypes.clear();
            } else if (preparedTypes.empty()) {
                prepare = true;

                preparedTypes.resize(parameters.size(), TEXTOID);

                for (std::size_t p = 0, nP = parameters.size(); p < nP; ++p) {
                    auto source = parameterSources[p];
                    if (!source || row >= source->size()) {
                        translateType({}, preparedTypes[p], parameterValues[p],
                                      parameterStorage[p], parameterSize[p], parameterFormat[p]);
                    } else {
                        translateType((*source)[row], preparedTypes[p], parameterValues[p],
                                      parameterStorage[p], parameterSize[p], parameterFormat[p]);
                    }
                }
            } else {
                if (preparedTypes.size() != parameters.size()) {
                    prepare = true;
                    preparedTypes.resize(parameters.size(), TEXTOID);
                }
                for (std::size_t p = 0, nP = parameters.size(); p < nP; ++p) {
                    ::Oid type = preparedTypes[p];

                    auto source = parameterSources[p];
                    if (!source || row >= source->size()) {
                        translateType({}, type, parameterValues[p],
                                      parameterStorage[p], parameterSize[p], parameterFormat[p]);
                    } else {
                        translateType((*source)[row], type, parameterValues[p],
                                      parameterStorage[p], parameterSize[p], parameterFormat[p]);
                    }

                    if (type != preparedTypes[p]) {
                        preparedTypes[p] = type;
                        prepare = true;
                    }
                }
            }

            if (prepare) {
                if (!updatePrepared())
                    return false;
                prepare = false;
            }

            auto res = execPrepared(driver.db, preparedName, parameterValues, parameterSize, parameterFormat);
            if (!res) {
                qCDebug(log_database_drivers_postgresql) << "Error executing" << rawStatement << "on row" << row
                                                         << "connection error:"
                                                         << ::PQerrorMessage(static_cast<PGconn *>(driver.db));
                return false;
            } else  {
                switch (::PQresultStatus(res.get())) {
                case PGRES_COMMAND_OK:
                case PGRES_TUPLES_OK:
                    break;
                default:
                    qCDebug(log_database_drivers_postgresql) << "Error preparing" << rawStatement << ":" << "on row" << row
                                                             << ::PQresultErrorMessage(res.get());
                    return false;
                }
            }
        }

        return true;
    }
};

class PostgreSQL::PostgreSQLOperationExtract : public PostgreSQLOperationBase {
    std::size_t extract;
    bool first;

    bool canUseCursor() const
    {
        switch (type) {
        case Type::Unknown:
        case Type::Insert:
        case Type::Update:
        case Type::Delete:
            return false;
        case Type::Select:
        case Type::Values:
            return true;
        }
        Q_ASSERT(false);
        return false;
    }

public:
    PostgreSQLOperationExtract(PostgreSQL &driver, PostgreSQLOperationBase::Type type,
                               std::string query, std::size_t outputs, std::string pairedTable = {}) :
            PostgreSQLOperationBase(driver, type, std::move(query), std::move(pairedTable)),
            extract(outputs), first(true)
    {
        Q_ASSERT(extract > 0);
    }

    virtual ~PostgreSQLOperationExtract() = default;

    Query *issue(const Types::Binds &binds = Types::Binds(), bool buffer = false) override
    {
        if (formattedInvalidated()) {
            if (!updateQuery())
                return nullptr;
            first = true;
        }

        if (buffer || !canUseCursor()) {
            /* Simple heuristic: if it's used more than once, switch to prepared mode, under
             * the assumption that it will be used many times */
            if (first || !canUsePrepared()) {
                first = false;

                auto res = executeStandalone(binds);
                if (!res)
                    return nullptr;

                return new PostgreSQLQueryBuffer(std::move(res), extract);
            }

            auto res = prepareAndExecute(binds);
            if (!res)
                return nullptr;

            return new PostgreSQLQueryBuffer(std::move(res), extract);
        }

        std::vector<::Oid> types(parameters.size(), TEXTOID);
        std::vector<const char *> parameterValues(parameters.size(), nullptr);
        std::vector<CPD3::Util::ByteArray> parameterStorage(parameters.size());
        std::vector<int> parameterSize(parameters.size(), 0);
        std::vector<int> parameterFormat(parameters.size(), 0);

        for (std::size_t p = 0, nP = parameters.size(); p < nP; ++p) {
            auto source = binds.find(parameters[p]);
            if (source == binds.end()) {
                qCDebug(log_database_drivers_postgresql) << "No bound values for parameter"
                                                         << parameters[p] << "in query" << rawStatement;
                translateType({}, types[p], parameterValues[p],
                              parameterStorage[p], parameterSize[p], parameterFormat[p]);
                continue;
            }

            translateType(source->second, types[p], parameterValues[p],
                          parameterStorage[p], parameterSize[p], parameterFormat[p]);
        }


        auto cursorName = driver.makeCursorName();
        std::string fetch = "FETCH FORWARD 64 FROM " + cursorName;

        auto res = execParameters(driver.db,
                                  "DECLARE " + cursorName + " BINARY NO SCROLL CURSOR WITHOUT HOLD FOR " + formattedQuery,
                                  types, parameterValues, parameterSize, parameterFormat);
        if (!res) {
            qCDebug(log_database_drivers_postgresql) << "Error creating cursor for" << rawStatement
                                                     << "connection error:"
                                                     << ::PQerrorMessage(static_cast<PGconn *>(driver.db));
            return nullptr;
        }

        return new PostgreSQLQueryCursor(driver, std::move(fetch), std::move(cursorName), extract);
    }
};

class PostgreSQL::PostgreSQLOperationSimple : public PostgreSQLOperationBase {
    bool first;

public:
    PostgreSQLOperationSimple(PostgreSQL &driver, PostgreSQLOperationBase::Type type, std::string query,
                              std::string targetTable = {}) :
            PostgreSQLOperationBase(driver, type, std::move(query), std::move(targetTable)), first(true)
    {}

    virtual ~PostgreSQLOperationSimple() = default;

    Query *issue(const Types::Binds &binds = Types::Binds(), bool = false) override
    {
        if (formattedInvalidated()) {
            if (!updateQuery())
                return nullptr;
            first = true;
        }

        /* Simple heuristic: if it's used more than once, switch to prepared mode, under
         * the assumption that it will be used many times */
        if (first || !canUsePrepared()) {
            first = false;

            auto res = executeStandalone(binds);
            if (!res)
                return nullptr;

            return new PostgreSQLQuerySimple(std::move(res));
        }

        auto res = prepareAndExecute(binds);
        if (!res)
            return nullptr;

        return new PostgreSQLQuerySimple(std::move(res));
    }
};


std::string PostgreSQL::makePreparedStatementName()
{ return "pst_" + std::to_string(uniqueCounter++); }

std::string PostgreSQL::makeCursorName()
{ return "cur_" + std::to_string(uniqueCounter++); }

Driver::Operation *PostgreSQL::common_general(const std::string &statement, size_t outputs, GeneralType type,
                                              const std::string &pairedTable)
{
    PostgreSQLOperationBase::Type op = PostgreSQLOperationBase::Type::Unknown;
    switch (type) {
    case GeneralType::Unknown:
        break;
    case GeneralType::Select:
        op = PostgreSQLOperationBase::Type::Select;
        break;
    case GeneralType::Insert:
        op = PostgreSQLOperationBase::Type::Insert;
        break;
    }
    if (outputs == 0)
        return new PostgreSQLOperationSimple(*this, op, statement, pairedTable);
    return new PostgreSQLOperationExtract(*this, op, statement, outputs, pairedTable);
}

Driver::Operation *PostgreSQL::common_insert(const std::string &table, const Types::ColumnSet &columns)
{ return insert(table, columns); }

bool PostgreSQL::common_typeless() const
{ return false; }

bool PostgreSQL::open()
{
    if (db) {
        ::PQfinish(static_cast<::PGconn *>(db));
        db = nullptr;
        ++sessionUID;
    }

    std::vector<std::pair<std::string, std::string>> parameters;
    if (!connection.host.empty())
        parameters.emplace_back("host", connection.host);
    if (connection.port)
        parameters.emplace_back("port", std::to_string(connection.port));
    if (!connection.db.empty())
        parameters.emplace_back("dbname", connection.db);
    if (!connection.username.empty())
        parameters.emplace_back("user", connection.username);
    if (!connection.password.empty())
        parameters.emplace_back("password", connection.username);

    parameters.emplace_back("connect_timeout", "60");

    /* A bit of a hack, but there's no way before 9.0 to actually get the runtime version, and
     * after that, all these are available */
#ifdef HAVE_POSTGRESQL_PQCONNECTDBPARAMS
    parameters.emplace_back("fallback_application_name", "CPD3");
    parameters.emplace_back("keepalives", "1");
    parameters.emplace_back("keepalives_idle", "15");
    parameters.emplace_back("keepalives_interval", "10");
    parameters.emplace_back("keepalives_count", "8");
#endif

    {
#ifdef HAVE_POSTGRESQL_PQCONNECTDBPARAMS
        std::vector<const char *> keywords;
        std::vector<const char *> values;

        for (const auto &p : parameters) {
            keywords.emplace_back(p.first.c_str());
            values.emplace_back(p.second.c_str());
        }
        keywords.emplace_back(nullptr);
        values.emplace_back(nullptr);

        auto conn = ::PQconnectdbParams(keywords.data(), values.data(), 0);

#else
        std::string conninfo;
        for (const auto &p : parameters) {
            if (!conninfo.empty())
                conninfo += " ";
            conninfo += p.first;
            conninfo += "='";

            std::string param = p.second;
            CPD3::Util::replace_all(param, "\\", "\\\\");
            CPD3::Util::replace_all(param, "\'", "\\'");
            conninfo += param;

            conninfo += "'";
        }

        auto conn = ::PQconnectdb(conninfo.c_str());
#endif

        if (!conn) {
            qCWarning(log_database_drivers_postgresql) << "Connection failed to database" <<
                                                       connection.host << connection.db;
            return false;
        }
        if (::PQstatus(conn) != CONNECTION_OK) {
            qCWarning(log_database_drivers_postgresql) << "Connection failed to database" <<
                                                       connection.host << connection.db << ":" <<
                                                       ::PQerrorMessage(conn);
            ::PQfinish(conn);
            return false;
        }

        db = conn;
    }

    execResult(db, "SET client_min_messages TO ERROR");
    execResult(db, "SET CLIENT_ENCODING TO 'UNICODE'");
    execResult(db, "SET DATESTYLE TO 'ISO'");

    return common_open();
}

bool PostgreSQL::beginTransaction(int flags, double timeout)
{
    double end = FP::undefined();
    if (FP::defined(timeout))
        end = Time::time() + timeout;

    std::string transactionBegin = "BEGIN " + common_getTransactionIsolation(flags);
    for (int tc = 0;; tc++) {
        if (!db)
            return false;

        auto res = execResult(db, transactionBegin);
        if (res) {
            if (::PQresultStatus(res.get()) == PGRES_COMMAND_OK)
                return true;
        }

        std::this_thread::yield();
        if (timeout <= 0.0 || tc > 10000 || (FP::defined(end) && Time::time() > end)) {
            if (res) {
                qCDebug(log_database_drivers_postgresql) << "Timeout starting transaction, last error:"
                                                         << ::PQresultErrorMessage(res.get());
            } else {
                qCDebug(log_database_drivers_postgresql) << "Timeout starting transaction, connection error:"
                                                         << ::PQerrorMessage(static_cast<PGconn *>(db));
            }
            return false;
        }

        if (tc != 0 && (tc % 10) == 0) {
            ::PQreset(static_cast<PGconn *>(db));
            ++sessionUID;

            if (::PQstatus(static_cast<PGconn *>(db)) != CONNECTION_OK) {
                qCWarning(log_database_drivers_postgresql) << "Database reconnect failed:"
                                                           << ::PQerrorMessage(static_cast<PGconn *>(db));
                ::PQfinish(static_cast<::PGconn *>(db));
                db = nullptr;

                if (!this->open()) {
                    qCDebug(log_database_drivers_postgresql) << "Database reopen failed";
                    return false;
                }
            } else {
                if (!common_open()) {
                    qCDebug(log_database_drivers_postgresql) << "Database reconfigure failed";
                    ::PQfinish(static_cast<::PGconn *>(db));
                    db = nullptr;
                    return false;
                }
            }
        }

        Wait::backoffWait(tc);
    }
    return false;
}

bool PostgreSQL::commitTransaction()
{
    if (!db)
        return false;

    switch (::PQtransactionStatus(static_cast<PGconn *>(db))) {
    case PQTRANS_IDLE:
        qCDebug(log_database_drivers_postgresql) << "No transaction active to commit";
        return false;
    case PQTRANS_ACTIVE:
    case PQTRANS_INTRANS:
        break;
    case PQTRANS_INERROR: {
        qCDebug(log_database_drivers_postgresql) << "Rolling back transaction in error";

        execResult(db, "ROLLBACK");
        return false;
    }
    case PQTRANS_UNKNOWN:
        return false;
    }

    auto res = execResult(db, "COMMIT");
    if (!res) {
        qCDebug(log_database_drivers_postgresql) << "Error ending transaction, connection error:"
                                                 << ::PQerrorMessage(static_cast<PGconn *>(db));

        execResult(db, "ROLLBACK");
        return false;
    }
    if (::PQresultStatus(res.get()) != PGRES_COMMAND_OK) {
        qCDebug(log_database_drivers_postgresql) << "Error committing transaction error:"
                                                 << ::PQresultErrorMessage(res.get());
        res.reset();

        execResult(db, "ROLLBACK");
        return false;
    }

    switch (::PQtransactionStatus(static_cast<PGconn *>(db))) {
    case PQTRANS_IDLE:
        break;
    case PQTRANS_ACTIVE:
    case PQTRANS_INTRANS:
        qCWarning(log_database_drivers_postgresql) << "Transaction still active after commit";
        return false;
    case PQTRANS_INERROR: {
        qCDebug(log_database_drivers_postgresql) << "Rolling back transaction in error";

        execResult(db, "ROLLBACK");
        return false;
    }
    case PQTRANS_UNKNOWN:
        return false;
    }

    /* This check is from Qt, which has a note about not having an API to check transaction status.
     * Since PGtransactionStatus should cover that, this is demoted to an assertion. */
    Q_ASSERT(std::strcmp(::PQcmdStatus(res.get()), "ROLLBACK") != 0);

    return true;
}

void PostgreSQL::rollbackTransaction()
{
    if (!db)
        return;
    execResult(db, "ROLLBACK");
}

std::unordered_set<std::string> PostgreSQL::loadTables()
{
    if (!db)
        return {};
    /* Start as "psql -E" then run "\dt" to get the source, modified to only get table names */
    auto res = execResult(db, R"(
SELECT c.relname
FROM pg_catalog.pg_class c
    LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
WHERE c.relkind = 'r'
    AND n.nspname <> 'pg_catalog'
    AND n.nspname <> 'information_schema'
    AND n.nspname !~ '^pg_toast'
    AND pg_catalog.pg_table_is_visible(c.oid)
 )");
    if (!res)
        return {};

    switch (::PQresultStatus(res.get())) {
    case PGRES_TUPLES_OK:
        break;
    case PGRES_COMMAND_OK:
        return {};
    default:
        qCDebug(log_database_drivers_postgresql) << "Error reading tables:" << ::PQresultErrorMessage(res.get());
        return {};
    }

    Q_ASSERT(::PQnfields(res.get()) >= 1);

    std::unordered_set<std::string> result;
    for (int i = 0, max = ::PQntuples(res.get()); i < max; i++) {
        auto table = ::PQgetvalue(res.get(), i, 0);
        if (!table || !table[0])
            continue;
        result.emplace(table);
    }
    return result;
}

bool PostgreSQL::createTable(const TableDefinition &table)
{ return common_createTable(table); }

bool PostgreSQL::removeTable(const std::string &table)
{
    if (!db)
        return {};
    auto res = execResult(db, "DROP TABLE " + table);
    if (!res)
        return false;

    if (!res) {
        qCDebug(log_database_drivers_postgresql) << "Error removing table" << table << " connection error:"
                                                 << ::PQerrorMessage(static_cast<PGconn *>(db));
        return false;
    }
    if (::PQresultStatus(res.get()) != PGRES_COMMAND_OK) {
        qCDebug(log_database_drivers_postgresql) << "Error removing table" << table << ":"
                                                 << ::PQresultErrorMessage(res.get());
        return false;
    }
    return true;
}

void PostgreSQL::optimizeDatabase()
{ return common_optimizeDatabase(loadTables()); }


Driver::Operation *PostgreSQL::general(const std::string &statement, std::size_t outputs)
{
    if (outputs == 0)
        return new PostgreSQLOperationSimple(*this, PostgreSQLOperationExtract::Type::Unknown, statement);
    return new PostgreSQLOperationExtract(*this, PostgreSQLOperationExtract::Type::Unknown, statement, outputs);
}

Driver::Operation *PostgreSQL::select(const Types::TableSet &from, const Types::Outputs &select,
                                      const std::string &where, const std::string &order)
{
    if (select.empty()) {
        return new PostgreSQLOperationSimple(*this, PostgreSQLOperationExtract::Type::Select,
                                             assembleSelect(from, select, where, order));
    }
    return new PostgreSQLOperationExtract(*this, PostgreSQLOperationExtract::Type::Select,
                                          assembleSelect(from, select, where, order), select.size());
}

Driver::Operation *PostgreSQL::insert(const std::string &table, const Types::ColumnSet &columns)
{
    return new PostgreSQLOperationSimple(*this, PostgreSQLOperationExtract::Type::Insert,
                                         assembleInsert(table, columns), table);
}

Driver::Operation *PostgreSQL::insertSelect(const std::string &table, const Types::Outputs &columns,
                                            const Types::TableSet &from, const Types::Outputs &select,
                                            const std::string &where,
                                            const std::string &order)
{
    return new PostgreSQLOperationSimple(*this, PostgreSQLOperationExtract::Type::Insert,
                                         assembleInsertSelect(table, columns, from, select, where, order));
}

Driver::Operation *PostgreSQL::merge(const std::string &table,
                                     const Types::ColumnSet &keys,
                                     const Types::ColumnSet &values)
{ return common_merge(table, keys, values); }

Driver::Operation *PostgreSQL::merge(const std::string &table, const Types::ColumnSet &columns)
{ return common_merge(table, columns); }

Driver::Operation *PostgreSQL::update(const std::string &table, const Types::ColumnSet &columns,
                                      const std::string &where)
{
    return new PostgreSQLOperationSimple(*this, PostgreSQLOperationExtract::Type::Update,
                                         assembleUpdate(table, columns, where), table);
}

Driver::Operation *PostgreSQL::del(const std::string &table, const std::string &where)
{
    return new PostgreSQLOperationSimple(*this, PostgreSQLOperationExtract::Type::Delete,
                                         assembleDel(table, where));
}

}
}
}