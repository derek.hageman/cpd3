/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QSqlError>
#include <QSqlQuery>
#include <QSqlResult>
#include <QSqlRecord>
#include <QLoggingCategory>

#include "qtmysql.hxx"
#include "database/tabledefinition.hxx"
#include "core/timeutils.hxx"
#include "core/waitutils.hxx"


Q_LOGGING_CATEGORY(log_database_drivers_qtmysql, "cpd3.database.drivers.qtmysql", QtWarningMsg)

namespace CPD3 {
namespace Database {
namespace Drivers {

QtMySQL::QtMySQL(const QSqlDatabase &d) : QtDatabase(d)
{ }

QtMySQL::~QtMySQL() = default;

bool QtMySQL::beginTransaction(int flags, double timeout)
{
    double end = FP::undefined();
    if (FP::defined(timeout))
        end = Time::time() + timeout;
    for (int tc = 0;; tc++) {
        if (!db.isOpen())
            break;

        if (flags & Driver::Transaction_Serializable) {
            db.exec("SET TRANSACTION ISOLATION LEVEL SERIALIZABLE");
            if (db.lastError().isValid()) {
                qCDebug(log_database_drivers_qtmysql)
                    << "Failed to set transaction isolation level: " << db.lastError().text();
                return false;
            }
        }

        if (db.transaction())
            return true;

        std::this_thread::yield();
        if (timeout <= 0.0 || tc > 10000 || (FP::defined(end) && Time::time() > end)) {
            qCWarning(log_database_drivers_qtmysql) << "Timeout starting transaction:"
                                                    << db.lastError().text();
            return false;
        }

        if (!db.isOpen() || (tc != 0 && (tc % 10) == 0)) {
            db.close();
            std::this_thread::yield();
            if (!this->open())
                return false;
        }

        Wait::backoffWait(tc);
    }
    if (db.lastError().isValid()) {
        qCDebug(log_database_drivers_qtmysql) << "Transaction start failed: "
                                              << db.lastError().text();
    }
    return false;
}

std::unordered_set<std::string> QtMySQL::loadTables()
{
    /* Qt's driver does not restrict this to the active database, so we have to
     * do it manually */
    QSqlQuery q(db);
    std::unordered_set<std::string> result;
    if (q.exec("SHOW TABLES")) {
        if (q.first()) {
            do {
                result.emplace(q.record().value(0).toString().toStdString());
            } while (q.next());
        }
        q.finish();
    } else {
        qCDebug(log_database_drivers_qtmysql) << "Failed to get MySQL tables: "
                                              << q.lastError().text();
    }
    return result;
}

bool QtMySQL::createTable(const TableDefinition &table)
{
    Q_ASSERT(!table.columns.empty());

    QStringList columnDefinitions;
    for (const auto &column : table.columns) {
        QString def(QString::fromStdString(column.name));

        QString type;
        switch (column.type) {
        case TableDefinition::Integer:
            switch (column.width) {
            case 1:
                type = "TINYINT";
                break;
            case 2:
                type = "SMALLINT";
                break;
            case 3:
                type = "MEDIUMINT";
                break;
            case 4:
                type = "INTEGER";
                break;
            default:
                type = "BIGINT";
                break;
            }
            break;
        case TableDefinition::UnsignedInteger:
            switch (column.width) {
            case 1:
                type = "TINYINT UNSIGNED";
                break;
            case 2:
                type = "SMALLINT UNSIGNED";
                break;
            case 3:
                type = "MEDIUMINT UNSIGNED";
                break;
            case 4:
                type = "INTEGER UNSIGNED";
                break;
            default:
                type = "BIGINT UNSIGNED";
                break;
            }
            break;
        case TableDefinition::Real:
            type = "DOUBLE";
            break;
        case TableDefinition::String:
            type = "TEXT";
            break;
        case TableDefinition::FiniteString:
            type = QString("VARCHAR(%1)").arg(column.width);
            break;
        case TableDefinition::Bytes:
            type = "LONGBLOB";
            break;
        }

        def.append(' ');
        def.append(type);

        if (column.allowNull) {
            def.append(" NULL");
        } else {
            def.append(" NOT NULL");
        }

        columnDefinitions.append(def);
    }
    Q_ASSERT(!columnDefinitions.empty());

    QString statement("CREATE TABLE ");
    statement.append(QString::fromStdString(table.name));
    statement.append('(');
    statement.append(columnDefinitions.join(','));

    if (!table.primary.indices.empty()) {
        QStringList keys;
        for (auto ki : table.primary.indices) {
            keys.append(QString::fromStdString(table.columns[ki].name));
        }
        Q_ASSERT(!keys.empty());

        statement.append(", PRIMARY KEY(");
        statement.append(keys.join(','));
        statement.append(')');
    }

    statement.append(") ENGINE=INNODB ROW_FORMAT=DYNAMIC");

    db.exec(statement);
    if (db.lastError().isValid()) {
        qCDebug(log_database_drivers_qtmysql) << "Failed to create table:" << statement << "-"
                                              << db.lastError().text();
        return false;
    }

    for (const auto &idef : table.indices) {
        Q_ASSERT(!idef.first.empty());
        Q_ASSERT(!idef.second.indices.empty());

        QStringList keys;
        for (auto ki : idef.second.indices) {
            keys.append(QString::fromStdString(table.columns[ki].name));
        }
        Q_ASSERT(!keys.empty());

        statement = "CREATE UNIQUE INDEX ";
        statement.append(QString::fromStdString(table.name));
        statement.append("Index");
        statement.append(QString::fromStdString(idef.first));
        statement.append(" ON ");
        statement.append(QString::fromStdString(table.name));
        statement.append(" (");
        statement.append(keys.join(','));
        statement.append(')');

        db.exec(statement);
        if (db.lastError().isValid()) {
            qCDebug(log_database_drivers_qtmysql) << "Failed to create index:" << statement << "-"
                                                  << db.lastError().text();
            return false;
        }
    }

    return true;
}

Driver::Operation *QtMySQL::merge(const std::string &table,
                                  const Types::ColumnSet &keys,
                                  const Types::ColumnSet &values)
{
    Types::ColumnSet combined(keys);
    for (const auto &add : values) {
        combined.insert(add);
    }
    return merge(table, combined);
}

Driver::Operation *QtMySQL::merge(const std::string &table, const Types::ColumnSet &columns)
{
    std::string query = "REPLACE INTO ";
    query += table;
    query += "(";

    {
        bool first = true;
        for (const auto &c : columns) {
            if (first)
                first = false;
            else
                query += ",";
            query += c;
        }
    }

    query += ") VALUES ( ";

    {
        bool first = true;
        for (const auto &c : columns) {
            if (first)
                first = false;
            else
                query += " , ";
            query += ":";
            query += c;
        }
    }

    query += " )";

    return general(query);
}

void QtMySQL::optimizeDatabase()
{
    for (const auto &table : loadTables()) {
        QString statement = "OPTIMIZE TABLE ";
        statement.append(QString::fromStdString(table));
        db.exec(statement);
        if (db.lastError().isValid()) {
            qCDebug(log_database_drivers_qtmysql) << "Failed to optimize table:" << statement << "-"
                                                  << db.lastError().text();
            return;
        }
    }
}

}
}
}