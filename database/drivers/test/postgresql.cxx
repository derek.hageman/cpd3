/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include "core/first.hxx"

#include <QTest>
#include <cstdint>
#include <memory>
#include <QtSql>
#include <QString>
#include <QTemporaryFile>

#include "database/drivers/postgresql.hxx"
#include "database/tabledefinition.hxx"

using namespace CPD3;
using namespace CPD3::Database;

//#define SERVER      "localhost"
#define USER        "aer"
#define PASSWORD    "aer"
#define DATABASE    "test"

class TestDatabase : public QObject {
Q_OBJECT

    QSqlDatabase getDatabase()
    {
        QSqlDatabase db = QSqlDatabase::addDatabase("QPSQL", QUuid::createUuid().toString());
#ifdef SERVER
        db.setHostName(SERVER);
#endif
#ifdef USER
        db.setUserName(USER);
#endif
#ifdef PASSWORD
        db.setPassword(PASSWORD);
#endif
#ifdef DATABASE
        db.setDatabaseName(DATABASE);
#endif
        return db;
    }

    Drivers::PostgreSQL::Connection getConnection()
    {
        Drivers::PostgreSQL::Connection c;
#ifdef SERVER
        c.host = SERVER;
#endif
#ifdef USER
        c.username = USER;
#endif
#ifdef PASSWORD
        c.password = PASSWORD;
#endif
#ifdef DATABASE
        c.db = DATABASE;
#endif
        return c;
    }

private slots:

    void initTestCase()
    {
        /* Set this here, since Qt5's connections sequence tries a warning generating query
         * before anything else */
        qputenv("PGOPTIONS", "-c client_min_messages=ERROR");

#ifndef SERVER
        return;
#endif
        QSqlDatabase db(getDatabase());
        QVERIFY(db.open());
        db.exec("DROP TABLE t1");
        db.exec("DROP TABLE t2");
        db.exec("DROP TABLE t");
        db.exec("DROP TABLE o");
    }

    void cleanup()
    {
        for (const auto &name : QSqlDatabase::connectionNames()) {
            QSqlDatabase::removeDatabase(name);
        }

#ifndef SERVER
        return;
#endif
        QSqlDatabase db(getDatabase());
        QVERIFY(db.open());
        db.exec("DROP TABLE t1");
        db.exec("DROP TABLE t2");
        db.exec("DROP TABLE t");
        db.exec("DROP TABLE o");
    }

    void cleanupTestCase()
    {
#ifndef SERVER
        return;
#endif
        QSqlDatabase db(getDatabase());
        QVERIFY(db.open());
        db.exec("DROP TABLE t1");
        db.exec("DROP TABLE t2");
        db.exec("DROP TABLE t");
        db.exec("DROP TABLE o");
    }

    void createTable()
    {
#ifndef SERVER
        return;
#endif
        {
            std::unique_ptr<Driver> driver(new Drivers::PostgreSQL(getConnection()));
            QVERIFY(driver->open());

            QVERIFY(driver->beginTransaction());

            {
                TableDefinition td("t1");
                td.integer<std::uint16_t>("i", false);
                td.real("r");
                td.string("s");
                td.column("fs", TableDefinition::FiniteString, false, 16);
                td.primaryKey("i");
                td.index("Finite", "fs");
                driver->createTable(td);
            }
            QCOMPARE(driver->loadTables(), (std::unordered_set<std::string>{"t1"}));

            {
                TableDefinition td("t2");
                td.column("fs", TableDefinition::FiniteString, false, 16);
                td.primaryKey("fs");
                driver->createTable(td);
            }
            QCOMPARE(driver->loadTables(), (std::unordered_set<std::string>{"t1", "t2"}));

            {
                TableDefinition td("t3");
                td.column("fs", TableDefinition::FiniteString, false, 16);
                td.primaryKey("fs");
                driver->createTable(td);
                driver->removeTable("t3");
            }
            QCOMPARE(driver->loadTables(), (std::unordered_set<std::string>{"t1", "t2"}));

            QVERIFY(driver->commitTransaction());
        }

        QSqlDatabase db(getDatabase());
        QVERIFY(db.open());
        QStringList check(db.tables());
        std::sort(check.begin(), check.end());
        QCOMPARE(check, QStringList("t1") << "t2");
    }

    void insert()
    {
#ifndef SERVER
        return;
#endif
        {
            std::unique_ptr<Driver> driver(new Drivers::PostgreSQL(getConnection()));
            QVERIFY(driver->open());

            QVERIFY(driver->beginTransaction());
            {
                TableDefinition td("t");
                td.integer<std::uint16_t>("i", false);
                td.integer<std::uint16_t>("j");
                td.primaryKey("i");
                driver->createTable(td);
            }
            QCOMPARE(driver->loadTables(), (std::unordered_set<std::string>{"t"}));

            auto op = driver->insert("t", {"i", "j"});
            QVERIFY(op != nullptr);

            auto s = op->issue({{"i", 1},
                                {"j", 2}});
            QVERIFY(s != nullptr);
            s->begin();
            QVERIFY(s->isOk());
            s->end();
            QVERIFY(s->isOk());
            delete s;

            delete op;

            QVERIFY(driver->commitTransaction());


            QVERIFY(driver->beginTransaction());

            op = driver->insert("t", {"i"});
            QVERIFY(op != nullptr);

            s = op->issue({{"i", 2}});
            QVERIFY(s != nullptr);
            s->begin();
            QVERIFY(s->isOk());
            s->end();
            QVERIFY(s->isOk());
            delete s;

            QVERIFY(op->batch({{"i", Types::BatchVariants{3}}}));

            delete op;

            QVERIFY(driver->commitTransaction());
        }


        QSqlDatabase db(getDatabase());
        QVERIFY(db.open());
        QSqlQuery q(db);

        QVERIFY(q.prepare("SELECT COUNT(i) FROM t"));
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 3);
        q.finish();

        QVERIFY(q.prepare("SELECT i,j FROM t WHERE i = ?"));
        q.bindValue(0, 1);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 1);
        QCOMPARE(q.value(1).toInt(), 2);
        q.finish();

        q.bindValue(0, 2);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 2);
        QVERIFY(q.value(1).isNull());
        q.finish();

        q.bindValue(0, 3);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 3);
        QVERIFY(q.value(1).isNull());
        q.finish();
    }

    void mergeSplit()
    {
#ifndef SERVER
        return;
#endif
        {
            std::unique_ptr<Driver> driver(new Drivers::PostgreSQL(getConnection()));
            QVERIFY(driver->open());

            QVERIFY(driver->beginTransaction());
            {
                TableDefinition td("t");
                td.integer<std::uint16_t>("i", false);
                td.integer<std::uint16_t>("j");
                td.primaryKey("i");
                driver->createTable(td);
            }
            QCOMPARE(driver->loadTables(), (std::unordered_set<std::string>{"t"}));

            auto op = driver->merge("t", {"i"}, {"j"});
            QVERIFY(op != nullptr);

            auto s = op->issue({{"i", 1},
                                {"j", 2}});
            QVERIFY(s != nullptr);
            s->begin();
            QVERIFY(s->isOk());
            s->end();
            QVERIFY(s->isOk());
            delete s;

            s = op->issue({{"i", 1},
                           {"j", 3}});
            QVERIFY(s != nullptr);
            s->begin();
            QVERIFY(s->isOk());
            s->end();
            QVERIFY(s->isOk());
            delete s;

            QVERIFY(op->batch({{"i", Types::BatchVariants{2}},
                               {"j", Types::BatchVariants{4}}}));

            delete op;

            QVERIFY(driver->commitTransaction());
        }


        QSqlDatabase db(getDatabase());
        QVERIFY(db.open());
        QSqlQuery q(db);

        QVERIFY(q.prepare("SELECT COUNT(i) FROM t"));
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 2);
        q.finish();

        QVERIFY(q.prepare("SELECT i,j FROM t WHERE i = ?"));
        q.bindValue(0, 1);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 1);
        QCOMPARE(q.value(1).toInt(), 3);
        q.finish();

        q.bindValue(0, 2);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 2);
        QCOMPARE(q.value(1).toInt(), 4);
        q.finish();
    }

    void mergeSingle()
    {
#ifndef SERVER
        return;
#endif
        {
            std::unique_ptr<Driver> driver(new Drivers::PostgreSQL(getConnection()));
            QVERIFY(driver->open());

            QVERIFY(driver->beginTransaction());
            {
                TableDefinition td("t");
                td.integer<std::uint16_t>("i", false);
                td.integer<std::uint16_t>("j");
                td.primaryKey("i");
                driver->createTable(td);
            }
            QCOMPARE(driver->loadTables(), (std::unordered_set<std::string>{"t"}));

            auto op = driver->merge("t", {"i", "j"});
            QVERIFY(op != nullptr);

            auto s = op->issue({{"i", 1},
                                {"j", 2}});
            QVERIFY(s != nullptr);
            s->begin();
            QVERIFY(s->isOk());
            s->end();
            QVERIFY(s->isOk());
            delete s;

            s = op->issue({{"i", 1},
                           {"j", 3}});
            QVERIFY(s != nullptr);
            s->begin();
            QVERIFY(s->isOk());
            s->end();
            QVERIFY(s->isOk());
            delete s;

            QVERIFY(op->batch({{"i", Types::BatchVariants{2}},
                               {"j", Types::BatchVariants{4}}}));

            delete op;

            op = driver->merge("t", {"i"});
            QVERIFY(op != nullptr);

            s = op->issue({{"i", 3}});
            QVERIFY(s != nullptr);
            s->begin();
            QVERIFY(s->isOk());
            s->end();
            QVERIFY(s->isOk());
            delete s;

            delete op;

            QVERIFY(driver->commitTransaction());
        }


        QSqlDatabase db(getDatabase());
        QVERIFY(db.open());
        QSqlQuery q(db);

        QVERIFY(q.prepare("SELECT COUNT(i) FROM t"));
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 3);
        q.finish();

        QVERIFY(q.prepare("SELECT i,j FROM t WHERE i = ?"));
        q.bindValue(0, 1);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 1);
        QCOMPARE(q.value(1).toInt(), 3);
        q.finish();

        q.bindValue(0, 2);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 2);
        QCOMPARE(q.value(1).toInt(), 4);
        q.finish();

        q.bindValue(0, 3);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 3);
        QVERIFY(q.value(1).isNull());
        q.finish();
    }

    void update()
    {
#ifndef SERVER
        return;
#endif
        {
            std::unique_ptr<Driver> driver(new Drivers::PostgreSQL(getConnection()));
            QVERIFY(driver->open());

            QVERIFY(driver->beginTransaction());
            {
                TableDefinition td("t");
                td.integer<std::uint16_t>("i", false);
                td.integer<std::uint16_t>("j");
                td.primaryKey("i");
                driver->createTable(td);
            }
            QCOMPARE(driver->loadTables(), (std::unordered_set<std::string>{"t"}));

            auto op = driver->insert("t", {"i", "j"});
            QVERIFY(op != nullptr);
            QVERIFY(op->batch({{"i", Types::BatchVariants{1, 2, 3}},
                               {"j", Types::BatchVariants{3, 4, 5}}}));
            delete op;

            op = driver->update("t", {"j"}, "i = :key");
            QVERIFY(op != nullptr);

            auto s = op->issue({{"key", 2},
                                {"j",   6}});
            QVERIFY(s != nullptr);
            s->begin();
            QVERIFY(s->isOk());
            s->end();
            QVERIFY(s->isOk());
            delete s;

            s = op->issue({{"key", 3},
                           {"j",   {}}});
            QVERIFY(s != nullptr);
            s->begin();
            QVERIFY(s->isOk());
            s->end();
            QVERIFY(s->isOk());
            delete s;

            delete op;

            QVERIFY(driver->commitTransaction());
        }

        QSqlDatabase db(getDatabase());
        QVERIFY(db.open());
        QSqlQuery q(db);

        QVERIFY(q.prepare("SELECT COUNT(i) FROM t"));
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 3);
        q.finish();

        QVERIFY(q.prepare("SELECT i,j FROM t WHERE i = ?"));
        q.bindValue(0, 1);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 1);
        QCOMPARE(q.value(1).toInt(), 3);
        q.finish();

        q.bindValue(0, 2);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 2);
        QCOMPARE(q.value(1).toInt(), 6);
        q.finish();

        q.bindValue(0, 3);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 3);
        QVERIFY(q.value(1).isNull());
        q.finish();
    }

    void del()
    {
#ifndef SERVER
        return;
#endif
        {
            std::unique_ptr<Driver> driver(new Drivers::PostgreSQL(getConnection()));
            QVERIFY(driver->open());

            QVERIFY(driver->beginTransaction());
            {
                TableDefinition td("t");
                td.integer<std::uint16_t>("i", false);
                td.integer<std::uint16_t>("j");
                td.primaryKey("i");
                driver->createTable(td);
            }
            QCOMPARE(driver->loadTables(), (std::unordered_set<std::string>{"t"}));

            auto op = driver->insert("t", {"i", "j"});
            QVERIFY(op != nullptr);
            QVERIFY(op->batch({{"i", Types::BatchVariants{1, 2, 3}},
                               {"j", Types::BatchVariants{3, 4, 5}}}));
            delete op;

            QVERIFY(driver->commitTransaction());
        }

        {
            QSqlDatabase db(getDatabase());
            QVERIFY(db.open());
            QSqlQuery q(db);

            QVERIFY(q.prepare("SELECT COUNT(i) FROM t"));
            QVERIFY(q.exec());
            QVERIFY(q.first());
            QCOMPARE(q.value(0).toInt(), 3);
            q.finish();
        }

        {
            std::unique_ptr<Driver> driver(new Drivers::PostgreSQL(getConnection()));
            QVERIFY(driver->open());

            auto op = driver->del("t", "i = :key");
            QVERIFY(op != nullptr);

            driver->beginTransaction();

            auto s = op->issue({{"key", 3}});
            QVERIFY(s != nullptr);
            s->begin();
            QVERIFY(s->isOk());
            s->end();
            QVERIFY(s->isOk());
            delete s;

            delete op;

            QVERIFY(driver->commitTransaction());
        }

        QSqlDatabase db(getDatabase());
        QVERIFY(db.open());
        QSqlQuery q(db);

        QVERIFY(q.prepare("SELECT COUNT(i) FROM t"));
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 2);
        q.finish();

        QVERIFY(q.prepare("SELECT i,j FROM t WHERE i = ?"));
        q.bindValue(0, 1);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 1);
        QCOMPARE(q.value(1).toInt(), 3);
        q.finish();

        q.bindValue(0, 2);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 2);
        QCOMPARE(q.value(1).toInt(), 4);
        q.finish();
    }

    void select()
    {
#ifndef SERVER
        return;
#endif
        std::unique_ptr<Driver> driver(new Drivers::PostgreSQL(getConnection()));
        QVERIFY(driver->open());

        QVERIFY(driver->beginTransaction());

        {
            TableDefinition td("t");
            td.integer<std::uint16_t>("i", false);
            td.integer<std::uint16_t>("j");
            td.primaryKey("i");
            driver->createTable(td);
        }
        QCOMPARE(driver->loadTables(), (std::unordered_set<std::string>{"t"}));

        auto op = driver->insert("t", {"i", "j"});
        QVERIFY(op != nullptr);
        QVERIFY(op->batch({{"i", Types::BatchVariants{1, 2, 3}},
                           {"j", Types::BatchVariants{3, 4, 5}}}));
        delete op;

        op = driver->select({{std::string(), "t"}}, {"i", "j"}, "i = :in");

        auto s = op->issue({{"in", 1}});
        QVERIFY(s != nullptr);
        s->begin();
        QVERIFY(s->isOk());
        QVERIFY(s->advance());
        QVERIFY(s->isOk());
        auto v = s->values();
        QCOMPARE((int)v.at(0).toInteger(), 1);
        QCOMPARE((int)v.at(1).toInteger(), 3);
        QVERIFY(!s->advance());
        delete s;

        s = op->issue({{"in", 2}});
        QVERIFY(s != nullptr);
        s->begin();
        QVERIFY(s->isOk());
        QVERIFY(s->advance());
        QVERIFY(s->isOk());
        v = s->values();
        s->end();
        QVERIFY(s->isOk());
        QCOMPARE((int)v.at(0).toInteger(), 2);
        QCOMPARE((int)v.at(1).toInteger(), 4);

        delete s;

        delete op;


        op = driver->select({{std::string(), "t"}}, {"i", "j"}, "i = :in AND j = :in + 2");

        s = op->issue({{"in", 1}});
        QVERIFY(s != nullptr);
        s->begin();
        QVERIFY(s->isOk());
        QVERIFY(s->advance());
        QVERIFY(s->isOk());
        v = s->values();
        QCOMPARE((int)v.at(0).toInteger(), 1);
        QCOMPARE((int)v.at(1).toInteger(), 3);
        QVERIFY(!s->advance());
        delete s;

        delete op;


        op = driver->select({{std::string(), "t"}}, {"i", "j"}, "i = :a AND j = :b");

        s = op->issue({{"a", 1}, {"b", 3}});
        QVERIFY(s != nullptr);
        s->begin();
        QVERIFY(s->isOk());
        QVERIFY(s->advance());
        QVERIFY(s->isOk());
        v = s->values();
        QCOMPARE((int)v.at(0).toInteger(), 1);
        QCOMPARE((int)v.at(1).toInteger(), 3);
        QVERIFY(!s->advance());
        delete s;

        delete op;


        op = driver->select({{"tbl", "t"}}, {"tbl.i", "tbl.j"}, "tbl.i >= :in", "tbl.i ASC");

        s = op->issue({{"in", 2}});
        QVERIFY(s != nullptr);
        s->begin();
        QVERIFY(s->isOk());
        QVERIFY(s->advance());
        QVERIFY(s->isOk());
        v = s->values();
        QCOMPARE((int)v.at(0).toInteger(), 2);
        QCOMPARE((int)v.at(1).toInteger(), 4);

        QVERIFY(s->advance());
        QVERIFY(s->isOk());
        v = s->values();
        QCOMPARE((int)v.at(0).toInteger(), 3);
        QCOMPARE((int)v.at(1).toInteger(), 5);

        QVERIFY(!s->advance());
        QVERIFY(s->isOk());
        delete s;

        s = op->issue({{"in", 2}});
        QVERIFY(s != nullptr);
        s->begin();
        QVERIFY(s->isOk());
        QVERIFY(s->advance());
        QVERIFY(s->isOk());
        v = s->values();
        QCOMPARE((int)v.at(0).toInteger(), 2);
        QCOMPARE((int)v.at(1).toInteger(), 4);

        QVERIFY(s->advance());
        QVERIFY(s->isOk());
        v = s->values();
        QCOMPARE((int)v.at(0).toInteger(), 3);
        QCOMPARE((int)v.at(1).toInteger(), 5);

        QVERIFY(!s->advance());
        QVERIFY(s->isOk());
        delete s;

        s = op->issue({{"in", 2}}, true);
        QVERIFY(s != nullptr);
        s->begin();
        QVERIFY(s->isOk());
        QVERIFY(s->advance());
        QVERIFY(s->isOk());
        v = s->values();
        QCOMPARE((int)v.at(0).toInteger(), 2);
        QCOMPARE((int)v.at(1).toInteger(), 4);

        QVERIFY(s->advance());
        QVERIFY(s->isOk());
        v = s->values();
        QCOMPARE((int)v.at(0).toInteger(), 3);
        QCOMPARE((int)v.at(1).toInteger(), 5);

        QVERIFY(!s->advance());
        QVERIFY(s->isOk());
        delete s;

        s = op->issue({{"in", 2}}, true);
        QVERIFY(s != nullptr);
        s->begin();
        QVERIFY(s->isOk());
        QVERIFY(s->advance());
        QVERIFY(s->isOk());
        v = s->values();
        QCOMPARE((int)v.at(0).toInteger(), 2);
        QCOMPARE((int)v.at(1).toInteger(), 4);

        QVERIFY(s->advance());
        QVERIFY(s->isOk());
        v = s->values();
        QCOMPARE((int)v.at(0).toInteger(), 3);
        QCOMPARE((int)v.at(1).toInteger(), 5);

        QVERIFY(!s->advance());
        QVERIFY(s->isOk());
        delete s;

        s = op->issue({{"in", 2}}, true);
        QVERIFY(s != nullptr);
        s->begin();
        QVERIFY(s->isOk());
        QVERIFY(s->advance());
        QVERIFY(s->isOk());
        v = s->values();
        QCOMPARE((int)v.at(0).toInteger(), 2);
        QCOMPARE((int)v.at(1).toInteger(), 4);

        QVERIFY(s->advance());
        QVERIFY(s->isOk());
        v = s->values();
        QCOMPARE((int)v.at(0).toInteger(), 3);
        QCOMPARE((int)v.at(1).toInteger(), 5);

        QVERIFY(!s->advance());
        QVERIFY(s->isOk());
        delete s;


        delete op;

        QVERIFY(driver->commitTransaction());
    }

    void insertSelect()
    {
#ifndef SERVER
        return;
#endif
        std::unique_ptr<Driver> driver(new Drivers::PostgreSQL(getConnection()));
        QVERIFY(driver->open());

        QVERIFY(driver->beginTransaction());

        {
            TableDefinition td("t");
            td.integer<std::uint16_t>("i", false);
            td.integer<std::uint16_t>("j");
            td.primaryKey("i");
            driver->createTable(td);
        }
        {
            TableDefinition td("o");
            td.integer<std::uint16_t>("i", false);
            td.integer<std::uint16_t>("j");
            td.integer<std::uint16_t>("k");
            td.primaryKey("i");
            driver->createTable(td);
        }
        QCOMPARE(driver->loadTables(), (std::unordered_set<std::string>{"t", "o"}));

        auto op = driver->insert("t", {"i", "j"});
        QVERIFY(op != nullptr);
        QVERIFY(op->batch({{"i", Types::BatchVariants{1, 2, 3}},
                           {"j", Types::BatchVariants{3, 4, 5}}}));
        delete op;

        op = driver->insertSelect("o", {"i", "j", "k"}, {{"tbl", "t"}}, {"tbl.i", "tbl.j", ":out"},
                                  "tbl.i = :in");

        auto s = op->issue({{"in",  3},
                            {"out", 42}});
        QVERIFY(s != nullptr);
        s->begin();
        QVERIFY(s->isOk());
        s->end();
        QVERIFY(s->isOk());
        delete s;

        s = op->issue({{"in",  1},
                       {"out", 42}});
        QVERIFY(s != nullptr);
        s->begin();
        QVERIFY(s->isOk());
        s->end();
        QVERIFY(s->isOk());
        delete s;

        s = op->issue({{"in",  99},
                       {"out", 42}});
        QVERIFY(s != nullptr);
        s->begin();
        QVERIFY(s->isOk());
        s->end();
        QVERIFY(s->isOk());
        delete s;

        delete op;


        op = driver->select({{"tbl", "o"}}, {"tbl.i", "tbl.j", "tbl.k"}, {}, "tbl.i ASC");

        s = op->issue();
        QVERIFY(s != nullptr);
        s->begin();
        QVERIFY(s->isOk());
        QVERIFY(s->advance());
        QVERIFY(s->isOk());
        auto v = s->values();
        QCOMPARE((int)v.at(0).toInteger(), 1);
        QCOMPARE((int)v.at(1).toInteger(), 3);
        QCOMPARE((int)v.at(2).toInteger(), 42);

        QVERIFY(s->advance());
        QVERIFY(s->isOk());
        v = s->values();
        QCOMPARE((int)v.at(0).toInteger(), 3);
        QCOMPARE((int)v.at(1).toInteger(), 5);
        QCOMPARE((int)v.at(2).toInteger(), 42);

        QVERIFY(!s->advance());
        QVERIFY(s->isOk());
        delete s;

        delete op;

        QVERIFY(driver->commitTransaction());
    }

    void types()
    {
#ifndef SERVER
        return;
#endif
        {
            std::unique_ptr<Driver> driver(new Drivers::PostgreSQL(getConnection()));
            QVERIFY(driver->open());

            QVERIFY(driver->beginTransaction());
            {
                TableDefinition td("t");
                td.integer<std::uint16_t>("u16", false);
                td.real("r");
                td.string("str");
                td.column("fs", TableDefinition::FiniteString, true, 16);
                td.integer<std::uint32_t>("u32");
                td.integer<std::int64_t>("i64");
                td.bytes("by");
                td.primaryKey("u16");
                driver->createTable(td);
            }
            QCOMPARE(driver->loadTables(), (std::unordered_set<std::string>{"t"}));

            auto op = driver->insert("t", {"u16", "r", "str", "fs", "u32", "i64", "by"});
            QVERIFY(op != nullptr);

            auto s = op->issue({{"u16", 1},
                                {"r", 2.0},
                                {"str", "abc"},
                                {"fs", "qwerty"},
                                {"u32", static_cast<std::int_fast64_t>(0x7FFFF00F)},
                                {"i64", static_cast<std::int_fast64_t>(Q_INT64_C(-0xFFFFFFFFFFFF))},
                                {"by", CPD3::Util::ByteArray("\x01\x00\x04", 3)}});
            QVERIFY(s != nullptr);
            s->begin();
            QVERIFY(s->isOk());
            s->end();
            QVERIFY(s->isOk());
            delete s;

            delete op;

            QVERIFY(driver->commitTransaction());


            QVERIFY(driver->beginTransaction());

            op = driver->insert("t", {"u16", "r", "u32", "by"});
            QVERIFY(op != nullptr);

            s = op->issue({{"u16", 2},
                           {"r", 3},
                           {"u32", 12.0},
                           {"by", Types::Variant(Types::Variant::Type::Null)},
                           {"zzzz", 42}});
            QVERIFY(s != nullptr);
            s->begin();
            QVERIFY(s->isOk());
            s->end();
            QVERIFY(s->isOk());
            delete s;

            QVERIFY(op->batch({{"u16", Types::BatchVariants{3}}}));

            delete op;

            QVERIFY(driver->commitTransaction());


            QVERIFY(driver->beginTransaction());

            op = driver->select({{std::string(), "t"}}, {"u16", "r", "str", "fs", "u32", "i64", "by"}, "u16 = :target");
            QVERIFY(op != nullptr);

            s = op->issue({{"target", 1}}, true);
            QVERIFY(s != nullptr);
            s->begin();
            QVERIFY(s->isOk());
            QVERIFY(s->advance());
            QVERIFY(s->isOk());
            auto v = s->values();
            QCOMPARE((int)v.at(0).toInteger(), 1);
            QCOMPARE(v.at(1).toReal(), 2.0);
            QCOMPARE(v.at(2).toString(), std::string("abc"));
            QCOMPARE(v.at(3).toString(), std::string("qwerty"));
            QCOMPARE(v.at(4).toInteger(), static_cast<std::int_fast64_t>(0x7FFFF00F));
            QCOMPARE(v.at(5).toInteger(), static_cast<std::int_fast64_t>(Q_INT64_C(-0xFFFFFFFFFFFF)));
            QCOMPARE(v.at(6).toBytes(), CPD3::Util::ByteArray("\x01\x00\x04", 3));
            s->end();
            QVERIFY(s->isOk());
            delete s;

            s = op->issue({{"target", 2}}, true);
            QVERIFY(s != nullptr);
            s->begin();
            QVERIFY(s->isOk());
            QVERIFY(s->advance());
            QVERIFY(s->isOk());
            v = s->values();
            QCOMPARE((int)v.at(0).toInteger(), 2);
            QCOMPARE(v.at(1).toReal(), 3.0);
            QVERIFY(v.at(2).isNull());
            QVERIFY(v.at(3).isNull());
            QCOMPARE((int)v.at(4).toInteger(), 12);
            QVERIFY(v.at(5).isNull());
            QVERIFY(v.at(6).isNull());
            s->end();
            QVERIFY(s->isOk());
            delete s;

            s = op->issue({{"target", 3}}, true);
            QVERIFY(s != nullptr);
            s->begin();
            QVERIFY(s->isOk());
            QVERIFY(s->advance());
            QVERIFY(s->isOk());
            v = s->values();
            QCOMPARE((int)v.at(0).toInteger(), 3);
            QVERIFY(v.at(1).isNull());
            QVERIFY(v.at(2).isNull());
            QVERIFY(v.at(3).isNull());
            QVERIFY(v.at(4).isNull());
            QVERIFY(v.at(5).isNull());
            QVERIFY(v.at(6).isNull());
            s->end();
            QVERIFY(s->isOk());
            delete s;

            delete op;


            QVERIFY(driver->commitTransaction());
        }


        QSqlDatabase db(getDatabase());
        QVERIFY(db.open());
        QSqlQuery q(db);

        QVERIFY(q.prepare("SELECT COUNT(u16) FROM t"));
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 3);
        q.finish();

        QVERIFY(q.prepare("SELECT u16, r, str, fs, u32, i64, by FROM t WHERE u16 = ?"));
        q.bindValue(0, 1);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 1);
        QCOMPARE(q.value(1).toDouble(), 2.0);
        QCOMPARE(q.value(2).toString(), QString("abc"));
        QCOMPARE(q.value(3).toString(), QString("qwerty"));
        QCOMPARE(q.value(4).toLongLong(), Q_INT64_C(0x7FFFF00F));
        QCOMPARE(q.value(5).toLongLong(), Q_INT64_C(-0xFFFFFFFFFFFF));
        QCOMPARE(q.value(6).toByteArray(), QByteArray("\x01\x00\x04", 3));
        q.finish();

        q.bindValue(0, 2);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 2);
        QCOMPARE(q.value(1).toDouble(), 3.0);
        QVERIFY(q.value(2).isNull());
        QVERIFY(q.value(3).isNull());
        QCOMPARE(q.value(4).toInt(), 12);
        QVERIFY(q.value(5).isNull());
        QVERIFY(q.value(6).isNull());
        q.finish();

        q.bindValue(0, 3);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 3);
        QVERIFY(q.value(1).isNull());
        QVERIFY(q.value(2).isNull());
        QVERIFY(q.value(3).isNull());
        QVERIFY(q.value(4).isNull());
        QVERIFY(q.value(5).isNull());
        QVERIFY(q.value(6).isNull());
        q.finish();
    }
};

QTEST_MAIN(TestDatabase)

#include "postgresql.moc"