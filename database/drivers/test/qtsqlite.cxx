/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include "core/first.hxx"

#include <QTest>
#include <cstdint>
#include <memory>
#include <QtSql>
#include <QString>
#include <QTemporaryFile>

#include "database/drivers/qtsqlite.hxx"
#include "database/tabledefinition.hxx"

using namespace CPD3;
using namespace CPD3::Database;

class TestDatabase : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

    QSqlDatabase getDatabase()
    {
        QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE", QUuid::createUuid().toString());
        db.setDatabaseName(databaseFile.fileName());
        return db;
    }

private slots:

    void initTestCase()
    {
        QVERIFY(databaseFile.open());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void cleanup()
    {
        for (const auto &name : QSqlDatabase::connectionNames()) {
            QSqlDatabase::removeDatabase(name);
        }

        QFile(databaseFile.fileName() + "-shm").remove();
        QFile(databaseFile.fileName() + "-wal").remove();
        QFile(databaseFile.fileName() + "-journal").remove();
    }

    void createTable()
    {
        {
            std::unique_ptr<Driver> driver(new Drivers::QtSqlite(getDatabase()));
            QVERIFY(driver->open());

            QVERIFY(driver->beginTransaction());

            {
                TableDefinition td("t1");
                td.integer<std::uint16_t>("i", false);
                td.real("r");
                td.string("s");
                td.column("fs", TableDefinition::FiniteString, false, 16);
                td.primaryKey("i");
                td.index("Finite", "fs");
                driver->createTable(td);
            }
            QCOMPARE(driver->loadTables(), (std::unordered_set<std::string>{"t1"}));

            {
                TableDefinition td("t2");
                td.column("fs", TableDefinition::FiniteString, false, 16);
                td.primaryKey("fs");
                driver->createTable(td);
            }
            QCOMPARE(driver->loadTables(), (std::unordered_set<std::string>{"t1", "t2"}));

            {
                TableDefinition td("t3");
                td.column("fs", TableDefinition::FiniteString, false, 16);
                td.primaryKey("fs");
                driver->createTable(td);
                driver->removeTable("t3");
            }
            QCOMPARE(driver->loadTables(), (std::unordered_set<std::string>{"t1", "t2"}));

            QVERIFY(driver->commitTransaction());
        }

        QSqlDatabase db(getDatabase());
        QVERIFY(db.open());
        QStringList check(db.tables());
        std::sort(check.begin(), check.end());
        QCOMPARE(check, QStringList("t1") << "t2");
    }

    void insert()
    {
        {
            std::unique_ptr<Driver> driver(new Drivers::QtSqlite(getDatabase()));
            QVERIFY(driver->open());

            QVERIFY(driver->beginTransaction());
            {
                TableDefinition td("t");
                td.integer<std::uint16_t>("i", false);
                td.integer<std::uint16_t>("j");
                td.primaryKey("i");
                driver->createTable(td);
            }
            QCOMPARE(driver->loadTables(), (std::unordered_set<std::string>{"t"}));

            auto op = driver->insert("t", {"i", "j"});
            QVERIFY(op != nullptr);

            auto s = op->issue({{"i", 1},
                                {"j", 2}});
            QVERIFY(s != nullptr);
            s->begin();
            QVERIFY(s->isOk());
            s->end();
            QVERIFY(s->isOk());
            delete s;

            delete op;

            QVERIFY(driver->commitTransaction());


            QVERIFY(driver->beginTransaction());

            op = driver->insert("t", {"i"});
            QVERIFY(op != nullptr);

            s = op->issue({{"i", 2}});
            QVERIFY(s != nullptr);
            s->begin();
            QVERIFY(s->isOk());
            s->end();
            QVERIFY(s->isOk());
            delete s;

            QVERIFY(op->batch({{"i", Types::BatchVariants{3}}}));

            delete op;

            QVERIFY(driver->commitTransaction());
        }


        QSqlDatabase db(getDatabase());
        QVERIFY(db.open());
        QSqlQuery q(db);

        QVERIFY(q.prepare("SELECT COUNT(i) FROM t"));
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 3);
        q.finish();

        QVERIFY(q.prepare("SELECT i,j FROM t WHERE i = ?"));
        q.bindValue(0, 1);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 1);
        QCOMPARE(q.value(1).toInt(), 2);
        q.finish();

        q.bindValue(0, 2);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 2);
        QVERIFY(q.value(1).isNull());
        q.finish();

        q.bindValue(0, 3);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 3);
        QVERIFY(q.value(1).isNull());
        q.finish();
    }

    void mergeSplit()
    {
        {
            std::unique_ptr<Driver> driver(new Drivers::QtSqlite(getDatabase()));
            QVERIFY(driver->open());

            QVERIFY(driver->beginTransaction());
            {
                TableDefinition td("t");
                td.integer<std::uint16_t>("i", false);
                td.integer<std::uint16_t>("j");
                td.primaryKey("i");
                driver->createTable(td);
            }
            QCOMPARE(driver->loadTables(), (std::unordered_set<std::string>{"t"}));

            auto op = driver->merge("t", {"i"}, {"j"});
            QVERIFY(op != nullptr);

            auto s = op->issue({{"i", 1},
                                {"j", 2}});
            QVERIFY(s != nullptr);
            s->begin();
            QVERIFY(s->isOk());
            s->end();
            QVERIFY(s->isOk());
            delete s;

            s = op->issue({{"i", 1},
                           {"j", 3}});
            QVERIFY(s != nullptr);
            s->begin();
            QVERIFY(s->isOk());
            s->end();
            QVERIFY(s->isOk());
            delete s;

            QVERIFY(op->batch({{"i", Types::BatchVariants{2}},
                               {"j", Types::BatchVariants{4}}}));

            delete op;

            QVERIFY(driver->commitTransaction());
        }


        QSqlDatabase db(getDatabase());
        QVERIFY(db.open());
        QSqlQuery q(db);

        QVERIFY(q.prepare("SELECT COUNT(i) FROM t"));
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 2);
        q.finish();

        QVERIFY(q.prepare("SELECT i,j FROM t WHERE i = ?"));
        q.bindValue(0, 1);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 1);
        QCOMPARE(q.value(1).toInt(), 3);
        q.finish();

        q.bindValue(0, 2);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 2);
        QCOMPARE(q.value(1).toInt(), 4);
        q.finish();
    }

    void mergeSingle()
    {
        {
            std::unique_ptr<Driver> driver(new Drivers::QtSqlite(getDatabase()));
            QVERIFY(driver->open());

            QVERIFY(driver->beginTransaction());
            {
                TableDefinition td("t");
                td.integer<std::uint16_t>("i", false);
                td.integer<std::uint16_t>("j");
                td.primaryKey("i");
                driver->createTable(td);
            }
            QCOMPARE(driver->loadTables(), (std::unordered_set<std::string>{"t"}));

            auto op = driver->merge("t", {"i", "j"});
            QVERIFY(op != nullptr);

            auto s = op->issue({{"i", 1},
                                {"j", 2}});
            QVERIFY(s != nullptr);
            s->begin();
            QVERIFY(s->isOk());
            s->end();
            QVERIFY(s->isOk());
            delete s;

            s = op->issue({{"i", 1},
                           {"j", 3}});
            QVERIFY(s != nullptr);
            s->begin();
            QVERIFY(s->isOk());
            s->end();
            QVERIFY(s->isOk());
            delete s;

            QVERIFY(op->batch({{"i", Types::BatchVariants{2}},
                               {"j", Types::BatchVariants{4}}}));

            delete op;

            op = driver->merge("t", {"i"});
            QVERIFY(op != nullptr);

            s = op->issue({{"i", 3}});
            QVERIFY(s != nullptr);
            s->begin();
            QVERIFY(s->isOk());
            s->end();
            QVERIFY(s->isOk());
            delete s;

            delete op;

            QVERIFY(driver->commitTransaction());
        }


        QSqlDatabase db(getDatabase());
        QVERIFY(db.open());
        QSqlQuery q(db);

        QVERIFY(q.prepare("SELECT COUNT(i) FROM t"));
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 3);
        q.finish();

        QVERIFY(q.prepare("SELECT i,j FROM t WHERE i = ?"));
        q.bindValue(0, 1);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 1);
        QCOMPARE(q.value(1).toInt(), 3);
        q.finish();

        q.bindValue(0, 2);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 2);
        QCOMPARE(q.value(1).toInt(), 4);
        q.finish();

        q.bindValue(0, 3);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 3);
        QVERIFY(q.value(1).isNull());
        q.finish();
    }

    void update()
    {
        {
            std::unique_ptr<Driver> driver(new Drivers::QtSqlite(getDatabase()));
            QVERIFY(driver->open());

            QVERIFY(driver->beginTransaction());
            {
                TableDefinition td("t");
                td.integer<std::uint16_t>("i", false);
                td.integer<std::uint16_t>("j");
                td.primaryKey("i");
                driver->createTable(td);
            }
            QCOMPARE(driver->loadTables(), (std::unordered_set<std::string>{"t"}));

            auto op = driver->insert("t", {"i", "j"});
            QVERIFY(op != nullptr);
            QVERIFY(op->batch({{"i", Types::BatchVariants{1, 2, 3}},
                               {"j", Types::BatchVariants{3, 4, 5}}}));
            delete op;

            op = driver->update("t", {"j"}, "i = :key");
            QVERIFY(op != nullptr);

            auto s = op->issue({{"key", 2},
                                {"j",   6}});
            QVERIFY(s != nullptr);
            s->begin();
            QVERIFY(s->isOk());
            s->end();
            QVERIFY(s->isOk());
            delete s;

            s = op->issue({{"key", 3},
                           {"j",   {}}});
            QVERIFY(s != nullptr);
            s->begin();
            QVERIFY(s->isOk());
            s->end();
            QVERIFY(s->isOk());
            delete s;

            delete op;

            QVERIFY(driver->commitTransaction());
        }

        QSqlDatabase db(getDatabase());
        QVERIFY(db.open());
        QSqlQuery q(db);

        QVERIFY(q.prepare("SELECT COUNT(i) FROM t"));
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 3);
        q.finish();

        QVERIFY(q.prepare("SELECT i,j FROM t WHERE i = ?"));
        q.bindValue(0, 1);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 1);
        QCOMPARE(q.value(1).toInt(), 3);
        q.finish();

        q.bindValue(0, 2);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 2);
        QCOMPARE(q.value(1).toInt(), 6);
        q.finish();

        q.bindValue(0, 3);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 3);
        QVERIFY(q.value(1).isNull());
        q.finish();
    }

    void del()
    {
        {
            std::unique_ptr<Driver> driver(new Drivers::QtSqlite(getDatabase()));
            QVERIFY(driver->open());

            QVERIFY(driver->beginTransaction());
            {
                TableDefinition td("t");
                td.integer<std::uint16_t>("i", false);
                td.integer<std::uint16_t>("j");
                td.primaryKey("i");
                driver->createTable(td);
            }
            QCOMPARE(driver->loadTables(), (std::unordered_set<std::string>{"t"}));

            auto op = driver->insert("t", {"i", "j"});
            QVERIFY(op != nullptr);
            QVERIFY(op->batch({{"i", Types::BatchVariants{1, 2, 3}},
                               {"j", Types::BatchVariants{3, 4, 5}}}));
            delete op;

            QVERIFY(driver->commitTransaction());
        }

        {
            QSqlDatabase db(getDatabase());
            QVERIFY(db.open());
            QSqlQuery q(db);

            QVERIFY(q.prepare("SELECT COUNT(i) FROM t"));
            QVERIFY(q.exec());
            QVERIFY(q.first());
            QCOMPARE(q.value(0).toInt(), 3);
            q.finish();
        }

        {
            std::unique_ptr<Driver> driver(new Drivers::QtSqlite(getDatabase()));
            QVERIFY(driver->open());

            auto op = driver->del("t", "i = :key");
            QVERIFY(op != nullptr);

            driver->beginTransaction();

            auto s = op->issue({{"key", 3}});
            QVERIFY(s != nullptr);
            s->begin();
            QVERIFY(s->isOk());
            s->end();
            QVERIFY(s->isOk());
            delete s;

            delete op;

            QVERIFY(driver->commitTransaction());
        }

        QSqlDatabase db(getDatabase());
        QVERIFY(db.open());
        QSqlQuery q(db);

        QVERIFY(q.prepare("SELECT COUNT(i) FROM t"));
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 2);
        q.finish();

        QVERIFY(q.prepare("SELECT i,j FROM t WHERE i = ?"));
        q.bindValue(0, 1);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 1);
        QCOMPARE(q.value(1).toInt(), 3);
        q.finish();

        q.bindValue(0, 2);
        QVERIFY(q.exec());
        QVERIFY(q.first());
        QCOMPARE(q.value(0).toInt(), 2);
        QCOMPARE(q.value(1).toInt(), 4);
        q.finish();
    }

    void select()
    {
        std::unique_ptr<Driver> driver(new Drivers::QtSqlite(getDatabase()));
        QVERIFY(driver->open());

        QVERIFY(driver->beginTransaction());

        {
            TableDefinition td("t");
            td.integer<std::uint16_t>("i", false);
            td.integer<std::uint16_t>("j");
            td.primaryKey("i");
            driver->createTable(td);
        }
        QCOMPARE(driver->loadTables(), (std::unordered_set<std::string>{"t"}));

        auto op = driver->insert("t", {"i", "j"});
        QVERIFY(op != nullptr);
        QVERIFY(op->batch({{"i", Types::BatchVariants{1, 2, 3}},
                           {"j", Types::BatchVariants{3, 4, 5}}}));
        delete op;

        op = driver->select({{std::string(), "t"}}, {"i", "j"}, "i = :in");

        auto s = op->issue({{"in", 1}});
        QVERIFY(s != nullptr);
        s->begin();
        QVERIFY(s->isOk());
        QVERIFY(s->advance());
        QVERIFY(s->isOk());
        auto v = s->values();
        QCOMPARE((int)v.at(0).toInteger(), 1);
        QCOMPARE((int)v.at(1).toInteger(), 3);
        QVERIFY(!s->advance());
        delete s;

        s = op->issue({{"in", 2}});
        QVERIFY(s != nullptr);
        s->begin();
        QVERIFY(s->isOk());
        QVERIFY(s->advance());
        QVERIFY(s->isOk());
        v = s->values();
        s->end();
        QVERIFY(s->isOk());
        QCOMPARE((int)v.at(0).toInteger(), 2);
        QCOMPARE((int)v.at(1).toInteger(), 4);

        delete s;

        delete op;


        op = driver->select({{"tbl", "t"}}, {"tbl.i", "tbl.j"}, "tbl.i >= :in", "tbl.i ASC");

        s = op->issue({{"in", 2}});
        QVERIFY(s != nullptr);
        s->begin();
        QVERIFY(s->isOk());
        QVERIFY(s->advance());
        QVERIFY(s->isOk());
        v = s->values();
        QCOMPARE((int)v.at(0).toInteger(), 2);
        QCOMPARE((int)v.at(1).toInteger(), 4);

        QVERIFY(s->advance());
        QVERIFY(s->isOk());
        v = s->values();
        QCOMPARE((int)v.at(0).toInteger(), 3);
        QCOMPARE((int)v.at(1).toInteger(), 5);

        QVERIFY(!s->advance());
        QVERIFY(s->isOk());
        delete s;

        delete op;

        QVERIFY(driver->commitTransaction());
    }

    void insertSelect()
    {
        std::unique_ptr<Driver> driver(new Drivers::QtSqlite(getDatabase()));
        QVERIFY(driver->open());

        QVERIFY(driver->beginTransaction());

        {
            TableDefinition td("t");
            td.integer<std::uint16_t>("i", false);
            td.integer<std::uint16_t>("j");
            td.primaryKey("i");
            driver->createTable(td);
        }
        {
            TableDefinition td("o");
            td.integer<std::uint16_t>("i", false);
            td.integer<std::uint16_t>("j");
            td.integer<std::uint16_t>("k");
            td.primaryKey("i");
            driver->createTable(td);
        }
        QCOMPARE(driver->loadTables(), (std::unordered_set<std::string>{"t", "o"}));

        auto op = driver->insert("t", {"i", "j"});
        QVERIFY(op != nullptr);
        QVERIFY(op->batch({{"i", Types::BatchVariants{1, 2, 3}},
                           {"j", Types::BatchVariants{3, 4, 5}}}));
        delete op;

        op = driver->insertSelect("o", {"i", "j", "k"}, {{"tbl", "t"}}, {"tbl.i", "tbl.j", ":out"},
                                  "tbl.i = :in");

        auto s = op->issue({{"in",  3},
                            {"out", 42}});
        QVERIFY(s != nullptr);
        s->begin();
        QVERIFY(s->isOk());
        s->end();
        QVERIFY(s->isOk());
        delete s;

        s = op->issue({{"in",  1},
                       {"out", 42}});
        QVERIFY(s != nullptr);
        s->begin();
        QVERIFY(s->isOk());
        s->end();
        QVERIFY(s->isOk());
        delete s;

        s = op->issue({{"in",  99},
                       {"out", 42}});
        QVERIFY(s != nullptr);
        s->begin();
        QVERIFY(s->isOk());
        s->end();
        QVERIFY(s->isOk());
        delete s;

        delete op;


        op = driver->select({{"tbl", "o"}}, {"tbl.i", "tbl.j", "tbl.k"}, {}, "tbl.i ASC");

        s = op->issue();
        QVERIFY(s != nullptr);
        s->begin();
        QVERIFY(s->isOk());
        QVERIFY(s->advance());
        QVERIFY(s->isOk());
        auto v = s->values();
        QCOMPARE((int)v.at(0).toInteger(), 1);
        QCOMPARE((int)v.at(1).toInteger(), 3);
        QCOMPARE((int)v.at(2).toInteger(), 42);

        QVERIFY(s->advance());
        QVERIFY(s->isOk());
        v = s->values();
        QCOMPARE((int)v.at(0).toInteger(), 3);
        QCOMPARE((int)v.at(1).toInteger(), 5);
        QCOMPARE((int)v.at(2).toInteger(), 42);

        QVERIFY(!s->advance());
        QVERIFY(s->isOk());
        delete s;

        delete op;

        QVERIFY(driver->commitTransaction());
    }
};

QTEST_MAIN(TestDatabase)

#include "qtsqlite.moc"