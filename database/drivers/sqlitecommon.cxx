/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <sstream>

#include <QLoggingCategory>

#include "sqlitecommon.hxx"
#include "database/tabledefinition.hxx"
#include "core/number.hxx"
#include "core/waitutils.hxx"
#include "core/timeutils.hxx"
#include "core/qtcompat.hxx"


Q_LOGGING_CATEGORY(log_database_drivers_sqlitecommon, "cpd3.database.drivers.sqlitecommon",
                   QtWarningMsg)

namespace CPD3 {
namespace Database {
namespace Drivers {

SQLiteCommon::SQLiteCommon() : supports_OptionalRowID(false)
{}

SQLiteCommon::~SQLiteCommon() = default;

bool SQLiteCommon::issueVoid(const std::string &statement)
{
    std::unique_ptr<Driver::Operation> op(common_general(statement));
    if (!op)
        return false;
    std::unique_ptr<Driver::Query> q(op->issue());
    if (!q)
        return false;
    q->begin();
    q->end();
    return q->isOk();
}

std::uint_fast32_t SQLiteCommon::getVersion()
{
    std::string versionString;
    {
        std::unique_ptr<Driver::Operation> op(common_general("SELECT sqlite_version()", 1));
        if (!op)
            return 0;
        std::unique_ptr<Driver::Query> q(op->issue());
        if (!q)
            return 0;
        q->begin();
        if (!q->advance())
            return 0;
        auto v = q->values();
        if (!v.empty()) {
            versionString = v.front().takeString();
        }
        q->end();
    }

    if (versionString.empty())
        return 0;

    auto end = versionString.find('.');
    if (end == std::string::npos)
        return 0;

    try {
        std::stringstream stream(versionString);

        std::uint_fast32_t result = 0;
        std::string part;

        if (!std::getline(stream, part, '.'))
            return 0;
        result |= static_cast<std::uint_fast32_t>(std::stoi(part)) << 16;

        if (!std::getline(stream, part, '.'))
            return 0;
        result |= static_cast<std::uint_fast32_t>(std::stoi(part)) << 8;

        if (!std::getline(stream, part, '.'))
            return 0;
        result |= static_cast<std::uint_fast32_t>(std::stoi(part)) << 0;

        if (stream.fail())
            return 0;

        return result;
    } catch (std::exception &) {
        return 0;
    }

    return 0;
}

static std::uint_fast32_t versionCode(int release, int major, int minor)
{
    std::uint_fast32_t version = static_cast<std::uint_fast32_t>(release & 0xFF) << 16;
    version |= static_cast<std::uint_fast32_t>(major & 0xFF) << 8;
    version |= static_cast<std::uint_fast32_t>(minor & 0xFF) << 0;
    return version;
}

bool SQLiteCommon::common_open()
{
#if 0
    {
        const char *env;
        if ((env = getenv("CPD3SQLITE_ASYNCHRONOUS")) != NULL) {
            if (env[0] && std::atoi(env) != 0) {
                if (!issueVoid("PRAGMA synchronous = 0"))
                    return false;
            }
        }
    }
#endif

    auto version = getVersion();
    if (!version) {
        qCWarning(log_database_drivers_sqlitecommon) << "SQLite version read failed";
    }

    if (version >= versionCode(3, 7, 0)) {
        issueVoid("PRAGMA journal_mode = WAL");
    }

    if (version >= versionCode(3, 6, 19)) {
        issueVoid("PRAGMA foreign_keys = ON");
    }

    if (!issueVoid("PRAGMA temp_store = MEMORY")) {
        return false;
    }

    supports_OptionalRowID = (version >= versionCode(3, 8, 2));

    return true;
}

bool SQLiteCommon::common_createTable(const TableDefinition &table)
{
    Q_ASSERT(!table.columns.empty());

    std::size_t rowIDIndex = static_cast<std::size_t>(-1);
    if (supports_OptionalRowID) {
        if (table.primary.indices.size() == 1) {
            switch (table.columns[table.primary.indices.front()].type) {
            case TableDefinition::Integer:
            case TableDefinition::UnsignedInteger:
                rowIDIndex = table.primary.indices.front();
                break;
            default:
                break;
            }
        }
    }

    std::vector<std::string> columnDefinitions;
    for (const auto &column : table.columns) {
        std::string def = column.name;

        std::string type;
        switch (column.type) {
        case TableDefinition::Integer:
            if (rowIDIndex == columnDefinitions.size()) {
                type = "INTEGER";
                break;
            }
            switch (column.width) {
            case 1:
                type = "TINYINT";
                break;
            case 2:
                type = "SMALLINT";
                break;
            case 3:
                type = "MEDIUMINT";
                break;
            case 4:
                type = "INTEGER";
                break;
            default:
                type = "BIGINT";
                break;
            }
            break;
        case TableDefinition::UnsignedInteger:
            if (rowIDIndex == columnDefinitions.size()) {
                type = "INTEGER UNSIGNED";
                break;
            }
            switch (column.width) {
            case 1:
                type = "TINYINT UNSIGNED";
                break;
            case 2:
                type = "SMALLINT UNSIGNED";
                break;
            case 3:
                type = "MEDIUMINT UNSIGNED";
                break;
            case 4:
                type = "INTEGER UNSIGNED";
                break;
            default:
                type = "BIGINT UNSIGNED";
                break;
            }
            break;
        case TableDefinition::Real:
            type = "DOUBLE";
            break;
        case TableDefinition::String:
            type = "TEXT";
            break;
        case TableDefinition::FiniteString:
            type = "VARCHAR(" + std::to_string(column.width) + ")";
            break;
        case TableDefinition::Bytes:
            type = "LONGBLOB";
            break;
        }

        def += " ";
        def += type;

        if (column.allowNull) {
            def.append(" NULL");
        } else {
            def.append(" NOT NULL");
        }

        columnDefinitions.emplace_back(std::move(def));
    }

    std::string statement = "CREATE TABLE ";
    statement += table.name;
    statement += "(";

    {
        Q_ASSERT(!columnDefinitions.empty());
        bool first = true;
        for (const auto &def : columnDefinitions) {
            if (!first)
                statement += ", ";
            first = false;
            statement += def;
        }
    }

    if (!table.primary.indices.empty()) {
        statement += ", PRIMARY KEY(";

        bool first = true;
        for (auto ki : table.primary.indices) {
            if (!first)
                statement += ",";
            first = false;
            statement += table.columns[ki].name;
        }

        statement += ")";
    }

    statement += ")";

    if (rowIDIndex != static_cast<std::size_t>(-1)) {
        statement.append(" WITHOUT ROWID");
    }

    if (!issueVoid(statement))
        return false;


    for (const auto &idef : table.indices) {
        Q_ASSERT(!idef.first.empty());
        Q_ASSERT(!idef.second.indices.empty());


        statement = "CREATE UNIQUE INDEX ";
        statement += table.name;
        statement += "Index";
        statement += idef.first;
        statement += " ON ";
        statement += table.name;
        statement += " (";
        {
            bool first = true;
            for (auto ki : idef.second.indices) {
                if (!first)
                    statement += ",";
                first = false;
                statement += table.columns[ki].name;
            }
        }
        statement += ")";

        if (!issueVoid(statement))
            return false;
    }

    return true;
}

void SQLiteCommon::common_optimizeDatabase()
{
    issueVoid("VACUUM");
}

std::string SQLiteCommon::common_merge(const std::string &table, const Types::ColumnSet &columns)
{
    std::string query = "INSERT OR REPLACE INTO ";
    query += table;
    query += "(";

    {
        bool first = true;
        for (const auto &c : columns) {
            if (first)
                first = false;
            else
                query += ",";
            query += c;
        }
    }

    query += ") VALUES ( ";

    {
        bool first = true;
        for (const auto &c : columns) {
            if (first)
                first = false;
            else
                query += " , ";
            query += ":";
            query += c;
        }
    }

    query += " )";

    return query;
}

}
}
}