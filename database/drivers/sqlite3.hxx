/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATABASEDRIVERS_SQLITE3_HXX
#define CPD3DATABASEDRIVERS_SQLITE3_HXX

#include "core/first.hxx"

#include "database/database.hxx"
#include "database/driver.hxx"
#include "sqlitecommon.hxx"

namespace CPD3 {
namespace Database {
namespace Drivers {

/**
 * A backend driver that uses the SQLite3 library directly.
 */
class CPD3DATABASE_EXPORT SQLite3 : public Driver, private SQLiteCommon {
    std::string filename;
    void *db;

    class Sqlite3Query;

    class Sqlite3Operation;

    friend class Sqlite3Operation;

public:
    SQLite3(std::string filename);

    virtual ~SQLite3();

    bool open() override;

    bool beginTransaction(int flags, double timeout) override;

    bool commitTransaction() override;

    void rollbackTransaction() override;

    std::unordered_set<std::string> loadTables() override;

    bool createTable(const TableDefinition &table) override;

    bool removeTable(const std::string &table) override;

    Driver::Operation *general(const std::string &statement, size_t outputs = 0) override;

    Operation *merge(const std::string &table,
                     const Types::ColumnSet &keys,
                     const Types::ColumnSet &values) override;

    Operation *merge(const std::string &table, const Types::ColumnSet &columns) override;

    void optimizeDatabase() override;

protected:
    Operation *common_general(const std::string &statement, size_t outputs = 0) override;
};

}
}
}

#endif //CPD3DATABASEDRIVERS_SQLITE3_HXX
