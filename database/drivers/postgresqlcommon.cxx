/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <sstream>

#include <QLoggingCategory>

#include "postgresqlcommon.hxx"
#include "database/tabledefinition.hxx"
#include "database/driver.hxx"
#include "core/number.hxx"
#include "core/waitutils.hxx"
#include "core/timeutils.hxx"
#include "core/qtcompat.hxx"


Q_LOGGING_CATEGORY(log_database_drivers_postgresqlcommon, "cpd3.database.drivers.postgresqlcommon",
                   QtWarningMsg)


/* Expand column widths and use an explict check when creating unsigned integer columns.
 * Otherwise, we just ignore the unsigned qualifier and will get errors for the upper
 * half of the values. */
//#define EXPLICIT_UNSIGNED

namespace CPD3 {
namespace Database {
namespace Drivers {

PostgreSQLCommon::PostgreSQLCommon() : supports_InsertConflictClause(false),
                                       supports_DeferrableTransaction(false)
{}

PostgreSQLCommon::~PostgreSQLCommon() = default;

bool PostgreSQLCommon::issueVoid(const std::string &statement)
{
    std::unique_ptr<Driver::Operation> op(common_general(statement));
    if (!op)
        return false;
    std::unique_ptr<Driver::Query> q(op->issue({}, true));
    if (!q)
        return false;
    q->begin();
    q->end();
    return q->isOk();
}

static std::unique_ptr<Driver::Query> tryStringRead(const std::unique_ptr<Driver::Operation> &op)
{
    if (!op)
        return {};
    std::unique_ptr<Driver::Query> q(op->issue({}, true));
    if (!q)
        return {};
    q->begin();
    if (!q->isOk())
        return {};
    if (!q->advance())
        return {};
    return q;
}

std::string PostgreSQLCommon::getVerionString()
{
    {
        std::unique_ptr<Driver::Operation> op(common_general("SELECT version()", 1, GeneralType::Select));
        auto q = tryStringRead(op);
        if (q) {
            auto v = q->values();
            if (q->isOk()) {
                q->end();
                if (!v.empty()) {
                    auto versionString = v.front().takeString();
                    /* "PostgreSQL 11.1 on ... */
                    if (versionString.size() > 11) {
                        versionString.erase(0, 11);
                        auto end = versionString.find(' ');
                        if (end != versionString.npos) {
                            versionString.erase(end);
                        }
                        return versionString;
                    }
                }
            }
        }
    }
    {
        std::unique_ptr<Driver::Operation> op(common_general("SHOW server_version", 1));
        auto q = tryStringRead(op);
        if (q) {
            auto v = q->values();
            if (q->isOk()) {
                q->end();
                if (!v.empty()) {
                    return v.front().takeString();
                }
            }
        }
    }

    return {};
}

std::uint_fast32_t PostgreSQLCommon::getVersion()
{
    std::string versionString = getVerionString();
    if (versionString.empty())
        return 0;

    auto end = versionString.find('.');
    if (end == std::string::npos)
        return 0;

    try {
        std::stringstream stream(versionString);

        std::uint_fast32_t result = 0;
        std::string part;

        if (!std::getline(stream, part, '.'))
            return 0;
        result |= static_cast<std::uint_fast32_t>(std::stoi(part)) << 16;

        if (!stream.eof() && std::getline(stream, part, '.')) {
            result |= static_cast<std::uint_fast32_t>(std::stoi(part)) << 8;

            if (!stream.eof() && std::getline(stream, part, '.')) {
                result |= static_cast<std::uint_fast32_t>(std::stoi(part)) << 0;
            }
        }

        if (stream.fail())
            return 0;

        return result;
    } catch (std::exception &) {
        return 0;
    }

    return 0;
}

static std::uint_fast32_t versionCode(int major, int minor, int patch = 0)
{
    std::uint_fast32_t version = static_cast<std::uint_fast32_t>(major & 0xFF) << 16;
    version |= static_cast<std::uint_fast32_t>(minor & 0xFF) << 8;
    version |= static_cast<std::uint_fast32_t>(patch & 0xFF) << 0;
    return version;
}

bool PostgreSQLCommon::common_open()
{
    auto version = getVersion();
    if (!version) {
        qCWarning(log_database_drivers_postgresqlcommon) << "PostgreSQL version read failed";
    }

    supports_InsertConflictClause = (version >= versionCode(9, 5));
    supports_DeferrableTransaction = (version >= versionCode(9, 1));

    if (!supports_InsertConflictClause) {
        if (!prepareLanguage())
            return false;
    }

    return true;
}

bool PostgreSQLCommon::prepareLanguage()
{
    {
        std::unique_ptr<Driver::Operation> op(
                common_general(R"( SELECT 1 FROM pg_language WHERE lanname = 'plpgsql' )", 1));
        if (!op)
            return false;
        std::unique_ptr<Driver::Query> q(op->issue({}, true));
        if (!q)
            return false;
        q->begin();
        if (q->advance() && q->isOk()) {
            q->end();
            return true;
        }
        q->end();
    }

    return issueVoid("CREATE LANGUAGE plpgsql");
}

std::string PostgreSQLCommon::common_getTransactionIsolation(int flags)
{
    std::string isolationCommand;

    if (flags & Driver::Transaction_Serializable)
        isolationCommand = "ISOLATION LEVEL SERIALIZABLE";
    else
        isolationCommand = "ISOLATION LEVEL REPEATABLE READ";
    if (flags & Driver::Transaction_ReadOnly) {
        isolationCommand += " READ ONLY";
        if ((flags & Driver::Transaction_Serializable) && supports_DeferrableTransaction)
            isolationCommand += " DEFERRABLE";
    }

    return isolationCommand;
}

bool PostgreSQLCommon::common_createTable(const TableDefinition &table)
{
    Q_ASSERT(!table.columns.empty());

    std::vector<std::string> columnDefinitions;
    for (const auto &column : table.columns) {
        std::string def = column.name;

        std::string type;
        switch (column.type) {
        case TableDefinition::UnsignedInteger:
#ifdef EXPLICIT_UNSIGNED
            switch (column.width) {
            case 1:
            case 2:
                type = "INTEGER";
                break;
            default:
                type = "BIGINT";
                break;
            }
            break;
#endif
        case TableDefinition::Integer:
            switch (column.width) {
            case 1:
            case 2:
                type = "SMALLINT";
                break;
            case 3:
            case 4:
                type = "INTEGER";
                break;
            default:
                type = "BIGINT";
                break;
            }
            break;
        case TableDefinition::Real:
            type = "DOUBLE PRECISION";
            break;
        case TableDefinition::String:
            type = "TEXT";
            break;
        case TableDefinition::FiniteString:
            type = "VARCHAR(" + std::to_string(column.width) + ")";
            break;
        case TableDefinition::Bytes:
            type = "BYTEA";
            break;
        }

        def += " ";
        def += type;

        if (column.allowNull) {
            def += " NULL";
        } else {
            def += " NOT NULL";
        }

        columnDefinitions.emplace_back(std::move(def));

#ifdef EXPLICIT_UNSIGNED
        if (column.type == TableDefinition::UnsignedInteger) {
            columnDefinitions.emplace_back("CHECK ( " + column.name + " IS NULL OR " + column.name + " >= 0 )");
        }
#endif
    }
    Q_ASSERT(!columnDefinitions.empty());

    std::string statement = "CREATE TABLE ";
    statement += table.name;
    statement += "(";

    {
        Q_ASSERT(!columnDefinitions.empty());
        bool first = true;
        for (const auto &def : columnDefinitions) {
            if (!first)
                statement += ", ";
            first = false;
            statement += def;
        }
    }

    if (!table.primary.indices.empty()) {
        statement += ", PRIMARY KEY(";

        bool first = true;
        for (auto ki : table.primary.indices) {
            if (!first)
                statement += ",";
            first = false;
            statement += table.columns[ki].name;
        }

        statement += ")";
    }

    statement += ")";

    if (!issueVoid(statement))
        return false;

    for (const auto &idef : table.indices) {
        Q_ASSERT(!idef.first.empty());
        Q_ASSERT(!idef.second.indices.empty());


        statement = "CREATE UNIQUE INDEX ";
        statement += table.name;
        statement += "Index";
        statement += idef.first;
        statement += " ON ";
        statement += table.name;
        statement += " (";
        {
            bool first = true;
            for (auto ki : idef.second.indices) {
                if (!first)
                    statement += ",";
                first = false;
                statement += table.columns[ki].name;
            }
        }
        statement += ")";

        if (!issueVoid(statement))
            return false;
    }

    return true;
}

void PostgreSQLCommon::common_optimizeDatabase(const std::unordered_set<std::string> &tables)
{
    if (!issueVoid("VACUUM FULL ANALYZE"))
        return;

    for (const auto &table : tables) {
        if (!issueVoid("REINDEX TABLE " + table))
            return;
    }
}

Driver::Operation *PostgreSQLCommon::common_merge(const std::string &table,
                                                  const Types::ColumnSet &keys,
                                                  const Types::ColumnSet &values)
{
    if (supports_InsertConflictClause) {
        Types::ColumnSet combined(keys);
        for (const auto &add : values) {
            combined.insert(add);
        }

        std::string query = "INSERT INTO ";
        query += table;
        query += "(";

        {
            bool first = true;
            for (const auto &c : combined) {
                if (first)
                    first = false;
                else
                    query += ",";
                query += c;
            }
        }

        query += ") VALUES ( ";

        {
            bool first = true;
            for (const auto &c : combined) {
                if (first)
                    first = false;
                else
                    query += " , ";
                query += ":";
                query += c;
            }
        }

        query += " ) ON CONFLICT (";
        {
            bool first = true;
            for (const auto &c : keys) {
                if (first)
                    first = false;
                else
                    query += ",";
                query += c;
            }
        }
        query += ") DO ";

        if (values.empty()) {
            query += "NOTHING";
        } else {
            query += "UPDATE SET ";

            {
                bool first = true;
                for (const auto &v : values) {
                    if (first)
                        first = false;
                    else
                        query += " , ";
                    query += v;
                    query += " = :";
                    query += v;
                }
            }
        }

        return common_general(query, 0, GeneralType::Insert, table);
    }

    std::vector<std::string> sorted(keys.begin(), keys.end());
    std::copy(values.begin(), values.end(), CPD3::Util::back_emplacer(sorted));
    std::sort(sorted.begin(), sorted.end());

    std::vector<std::string> castFunctions;
    if (!common_typeless()) {
        for (const auto &v : sorted) {
            std::string functionName = "pg_temp.cast_";
            std::transform(table.begin(), table.end(), std::back_inserter(functionName), ::tolower);
            functionName += "_";
            std::transform(v.begin(), v.end(), std::back_inserter(functionName), ::tolower);

            std::string procedure = "CREATE OR REPLACE FUNCTION ";
            procedure += functionName;
            procedure += "( input_value anyelement ) RETURNS ";
            procedure += table;
            procedure += ".";
            procedure += v;
            procedure += "%TYPE AS\n$$\n"
                         "BEGIN\n"
                         "    RETURN input_value;\n"
                         "END\n"
                         "$$\nLANGUAGE 'plpgsql' ;\n";

            if (!issueVoid(procedure))
                return new Driver::InvalidOperation;

            castFunctions.emplace_back(std::move(functionName));
        }
    }

    std::string functionName = "pg_temp.merge_";
    std::transform(table.begin(), table.end(), std::back_inserter(functionName), ::tolower);
    functionName += "_";

    for (const auto &c : sorted) {
        functionName += "_";
        std::transform(c.begin(), c.end(), std::back_inserter(functionName), ::tolower);
    }


    std::string updateQuery = "UPDATE ";
    updateQuery += table;
    updateQuery += " SET ";
    {
        bool first = true;
        for (const auto &v : values) {
            if (first)
                first = false;
            else
                updateQuery += " , ";
            updateQuery += v;
            updateQuery += " = a_";
            updateQuery += v;
        }
    }
    updateQuery += " WHERE ";
    {
        bool first = true;
        for (const auto &v : keys) {
            if (first)
                first = false;
            else
                updateQuery += " AND ";
            updateQuery += v;
            updateQuery += " = a_";
            updateQuery += v;
        }
    }

    std::string insertQuery = "INSERT INTO ";
    insertQuery += table;
    insertQuery += "(";
    {
        bool first = true;
        for (const auto &c : keys) {
            if (first)
                first = false;
            else
                insertQuery += ",";
            insertQuery += c;
        }
        for (const auto &c : values) {
            if (first)
                first = false;
            else
                insertQuery += ",";
            insertQuery += c;
        }
    }
    insertQuery += ") VALUES ( ";
    {
        bool first = true;
        for (const auto &v : keys) {
            if (first)
                first = false;
            else
                insertQuery += " , ";
            insertQuery += "a_";
            insertQuery += v;
        }
        for (const auto &v : values) {
            if (first)
                first = false;
            else
                insertQuery += " , ";
            insertQuery += "a_";
            insertQuery += v;
        }
    }
    insertQuery += " )";

    std::string procedure = "CREATE OR REPLACE FUNCTION ";
    procedure += functionName;
    procedure += "( ";
    {
        bool first = true;
        for (const auto &v : sorted) {
            if (first)
                first = false;
            else
                procedure += " , ";
            procedure += "a_";
            procedure += v;
            procedure += " ";
            procedure += table;
            procedure += ".";
            procedure += v;
            procedure += "%TYPE";
        }
    }
    procedure += ") RETURNS VOID AS\n$$\n";

    if (values.empty()) {
        procedure += "BEGIN\n"
                     "   BEGIN\n";
        procedure += insertQuery;
        procedure += ";\n"
                     "   EXCEPTION WHEN unique_violation THEN\n"
                     "\n"
                     "   END;\n"
                     "   RETURN;\n"
                     "END;\n";
    } else {
        /* This is more or less straight out of the manual */
        procedure += "BEGIN\n"
                     "   LOOP\n";
        procedure += updateQuery;
        procedure += ";\n"
                     "       IF found THEN\n"
                     "           RETURN;\n"
                     "       END IF;\n"
                     "       BEGIN\n";
        procedure += insertQuery;
        procedure += ";\n"
                     "           RETURN;\n"
                     "       EXCEPTION WHEN unique_violation THEN\n"
                     "\n"
                     "       END;\n"
                     "   END LOOP;\n"
                     "END;\n";
    }
    procedure += "$$\nLANGUAGE 'plpgsql' ;\n";

    if (!issueVoid(procedure))
        return new Driver::InvalidOperation;

    std::string query = "SELECT ";
    query += functionName;
    query += "( ";
    for (std::size_t i=0, max=sorted.size(); i<max; i++) {
        if (i != 0)
            query += " , ";
        if (!common_typeless()) {
            query += castFunctions[i];
            query += "( :";
            query += sorted[i];
            query += " )";
        } else {
            query += ":";
            query += sorted[i];
        }
    }
    query += " )";

    return common_general(query, 0, GeneralType::Select);
}

Driver::Operation *PostgreSQLCommon::common_merge(const std::string &table, const Types::ColumnSet &columns)
{
    if (supports_InsertConflictClause) {
        std::string query = "INSERT INTO ";
        query += table;
        query += "(";

        {
            bool first = true;
            for (const auto &c : columns) {
                if (first)
                    first = false;
                else
                    query += ",";
                query += c;
            }
        }

        query += ") VALUES ( ";

        {
            bool first = true;
            for (const auto &c : columns) {
                if (first)
                    first = false;
                else
                    query += " , ";
                query += ":";
                query += c;
            }
        }

        query += " ) ON CONFLICT ON CONSTRAINT ";
        query += table;
        query += "_pkey DO UPDATE SET ";

        {
            bool first = true;
            for (const auto &v : columns) {
                if (first)
                    first = false;
                else
                    query += " , ";
                query += v;
                query += " = :";
                query += v;
            }
        }

        return common_general(query, 0, GeneralType::Insert, table);
    }

    /* Figure out which columns are the primary key, taken mostly from the wiki:
     * https://wiki.postgresql.org/wiki/Retrieve_primary_key_columns */
    Types::ColumnSet values;
    for (const auto &c : columns) {
        std::string lc;
        std::transform(c.begin(), c.end(), std::back_inserter(lc), ::tolower);
        values.insert(std::move(lc));
    }
    Types::ColumnSet keys;
    {
        std::unique_ptr<Driver::Operation> op;
        std::unique_ptr<Driver::Query> q;
        {
            op.reset(common_general(R"(
SELECT a.attname FROM pg_index i
    JOIN pg_attribute a ON a.attrelid = i.indrelid AND a.attnum = ANY(i.indkey)
    WHERE i.indrelid = :tableName :: regclass AND i.indisprimary
 )", 1, GeneralType::Select));
            if (!op)
                return new Driver::InvalidOperation;

            std::string lc(table.size(), '_');
            std::transform(table.begin(), table.end(), lc.begin(), ::tolower);

            q.reset(op->issue({{"tableName", Types::Variant(std::move(lc))}}, true));
        }
        if (!q) {
            qCDebug(log_database_drivers_postgresqlcommon) << "Table index read failed";
            return new Driver::InvalidOperation;
        }

        q->begin();
        while (q->advance()) {
            auto qv = q->values();
            if (qv.empty()) {
                qCDebug(log_database_drivers_postgresqlcommon) << "Invalid table index read";
                return new Driver::InvalidOperation;
            }
            auto column = qv.front().takeString();
            if (column.empty()) {
                qCDebug(log_database_drivers_postgresqlcommon) << "Invalid column name";
                return new Driver::InvalidOperation;
            }

            std::transform(column.begin(), column.end(), column.begin(), ::tolower);
            values.erase(column);
            keys.insert(std::move(column));
        }
        if (!q->isOk()) {
            qCDebug(log_database_drivers_postgresqlcommon) << "Error reading table index";
            return new Driver::InvalidOperation;
        }
        q->end();
    }

    if (keys.empty())
        return common_insert(table, columns);
    return common_merge(table, keys, values);
}

}
}
}