/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3DATABASE_UTIL_HXX
#define CPD3DATABASE_UTIL_HXX

#include "core/first.hxx"

#include <utility>
#include <string>
#include <vector>
#include <cstdint>
#include <unordered_map>
#include <unordered_set>
#include <QString>
#include <QByteArray>
#include <QDataStream>
#include <QDebug>

#include "database/database.hxx"
#include "database/storage.hxx"
#include "core/util.hxx"


namespace CPD3 {
namespace Database {

namespace Util {

/**
 * Get the default database to connect to.
 *
 * @return  the default database
 */
CPD3DATABASE_EXPORT Storage getDefaultDatabase();

}

namespace Types {

typedef CPD3::Util::ByteArray Bytes;

class CPD3DATABASE_EXPORT Variant {
public:
    enum class Type {
        Empty,
        Null,
        Integer,
        Real,
        String,
        Bytes,
    };
private:
    CPD3::Util::ExplicitUnion<std::int_fast64_t, double, std::string, Bytes> storedData;
    Type storedType;
public:
    Variant();

    ~Variant();

    Variant(const Variant &other);

    Variant &operator=(const Variant &other);

    Variant(Variant &&other) noexcept;

    Variant &operator=(Variant &&other) noexcept;

    explicit Variant(Type type);

    Variant(double value);

    Variant(std::int_fast64_t value);

    template<class Integer, class = typename std::enable_if<
            ((std::is_same<Integer, int>::value || std::is_same<Integer, qint64>::value) &&
             !std::is_same<std::int_fast64_t, Integer>::value)>::type>
    inline Variant(Integer value) : Variant(static_cast<std::int_fast64_t>(value))
    { }


    Variant(const std::string &value);

    Variant(std::string &&value);

    Variant(const char *value);

    Variant(const QString &value);

    Variant(const Bytes &value);

    Variant(Bytes &&value);

    Variant(const QByteArray &value);


    inline Type type() const
    { return storedType; }

    inline bool isEmpty() const
    { return storedType == Type::Empty; }

    inline bool isNull() const
    { return storedType == Type::Null; }

    inline bool isSet() const
    { return !isEmpty() && !isNull(); }


    std::int_fast64_t toInteger() const;

    std::int_fast64_t takeInteger()
    { return toInteger(); }

    double toReal() const;

    inline double takeReal()
    { return toReal(); }

    const std::string &toString() const;

    std::string takeString();

    const Bytes &toBytes() const;

    Bytes takeBytes();

    QString toQString() const
    { return QString::fromStdString(toString()); }

    QByteArray toQByteArray() const
    { return toBytes().toQByteArray(); }
};

CPD3DATABASE_EXPORT QDataStream &operator<<(QDataStream &stream, const Variant &a);

CPD3DATABASE_EXPORT QDataStream &operator>>(QDataStream &stream, Variant &a);

CPD3DATABASE_EXPORT QDebug operator<<(QDebug stream, const Variant &a);

CPD3DATABASE_EXPORT bool operator==(const Variant &a, const Variant &b);

CPD3DATABASE_EXPORT bool operator!=(const Variant &a, const Variant &b);

/**
 * The type used to specify a unique set of columns
 */
typedef std::unordered_set<std::string> ColumnSet;

/**
 * The type used to specify the output values
 */
typedef std::vector<std::string> Outputs;

/**
 * The type used to specify the tables being operated on (mapping of join name to table name)
 */
typedef std::unordered_map<std::string, std::string> TableSet;

/**
 * The type used for bindings in queries.
 */
typedef std::unordered_map<std::string, Variant> Binds;

/**
 * A list of variants used in batch binding.
 */
typedef std::vector<Variant> BatchVariants;

/**
 * The type used for batch binding in queries
 */
typedef std::unordered_map<std::string, BatchVariants> BatchBinds;

/**
 * The type used for the output of results.
 */
typedef std::vector<Variant> Results;

}

}
}

#endif //CPD3DATABASE_UTIL_HXX
