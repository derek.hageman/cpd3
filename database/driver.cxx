/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include "driver.hxx"


namespace CPD3 {
namespace Database {


Driver::Driver() = default;

Driver::~Driver() = default;

Driver::Query::Query() = default;

Driver::Query::~Query() = default;


Driver::Operation::Operation() = default;

Driver::Operation::~Operation() = default;


void Driver::optimizeDatabase()
{}

std::string Driver::assembleSelect(const Types::TableSet &from,
                                   const Types::Outputs &select,
                                   const std::string &where,
                                   const std::string &order)
{
    std::string query = "SELECT ";

    {
        bool first = true;
        for (const auto &c : select) {
            if (first)
                first = false;
            else
                query += ",";

            query += c;
        }
    }

    query += " FROM ";

    {
        bool first = true;
        for (const auto &c : from) {
            if (first)
                first = false;
            else
                query += ",";

            query += c.second;
            if (!c.first.empty()) {
                query += " AS ";
                query += c.first;
            }
        }
    }

    if (!where.empty()) {
        query += " WHERE ";
        query += where;
    }

    if (!order.empty()) {
        query += " ORDER BY ";
        query += order;
    }

    return query;
}

Driver::Operation *Driver::select(const Types::TableSet &from,
                                  const Types::Outputs &select,
                                  const std::string &where,
                                  const std::string &order)
{ return general(assembleSelect(from, select, where, order), select.size()); }

std::string Driver::assembleInsert(const std::string &table, const Types::ColumnSet &columns)
{
    std::string query = "INSERT INTO ";
    query += table;
    query += "(";

    {
        bool first = true;
        for (const auto &c : columns) {
            if (first)
                first = false;
            else
                query += ",";
            query += c;
        }
    }

    query += ") VALUES ( ";

    {
        bool first = true;
        for (const auto &c : columns) {
            if (first)
                first = false;
            else
                query += " , ";
            query += ":";
            query += c;
        }
    }

    query += " )";

    return query;
}

Driver::Operation *Driver::insert(const std::string &table, const Types::ColumnSet &columns)
{ return general(assembleInsert(table, columns)); }

std::string Driver::assembleInsertSelect(const std::string &table,
                                         const Types::Outputs &columns,
                                         const Types::TableSet &from,
                                         const Types::Outputs &select,
                                         const std::string &where,
                                         const std::string &order)
{
    std::string query = "INSERT INTO ";
    query += table;
    query += "(";

    {
        bool first = true;
        for (const auto &c : columns) {
            if (first)
                first = false;
            else
                query += ",";
            query += c;
        }
    }

    query += ") ";
    query += assembleSelect(from, select, where, order);

    return query;
}

Driver::Operation *Driver::insertSelect(const std::string &table,
                                        const Types::Outputs &columns,
                                        const Types::TableSet &from,
                                        const Types::Outputs &select,
                                        const std::string &where,
                                        const std::string &order)
{ return general(assembleInsertSelect(table, columns, from, select, where, order)); }

std::string Driver::assembleMerge(const std::string &table,
                                  const Types::ColumnSet &keys,
                                  const Types::ColumnSet &values)
{
    std::string query = "MERGE INTO ";
    query += table;

    query += " AS output USING (SELECT ";

    {
        bool first = true;
        for (const auto &k : keys) {
            if (first)
                first = false;
            else
                query += ", ";
            query += ":";
            query += k;
            query += " AS ";
            query += k;
        }
    }

    query += " ) AS input ON (";
    {
        bool first = true;
        for (const auto &k : keys) {
            if (first)
                first = false;
            else
                query += " AND ";
            query += "output.";
            query += k;
            query += " = ";
            query += "input.";
            query += k;
        }
    }

    query += " ) WHEN MATCHED THEN UPDATE SET ";
    {
        bool first = true;
        for (const auto &v : values) {
            if (first)
                first = false;
            else
                query += " , ";
            query += "output.";
            query += v;
            query += " = :";
            query += v;
        }
    }

    query += " WHEN WHEN NOT MATCHED THEN INSERT(";
    {
        bool first = true;
        for (const auto &v : values) {
            if (first)
                first = false;
            else
                query += ",";
            query += v;
        }
    }

    query += ") VALUES ( ";
    {
        bool first = true;
        for (const auto &v : values) {
            if (first)
                first = false;
            else
                query += " , ";
            query += ":";
            query += v;
        }
    }

    query += " )";

    return query;
}

Driver::Operation *Driver::merge(const std::string &table,
                                 const Types::ColumnSet &keys,
                                 const Types::ColumnSet &values)
{ return general(assembleMerge(table, keys, values)); }

Driver::Operation *Driver::merge(const std::string &table, const Types::ColumnSet &columns)
{
    /* No standardized way of doing this, without knowing what the key is */
    return insert(table, columns);
}

std::string Driver::assembleUpdate(const std::string &table,
                                   const Types::ColumnSet &columns,
                                   const std::string &where)
{
    std::string query = "UPDATE ";
    query += table;

    query += " SET ";

    {
        bool first = true;
        for (const auto &c : columns) {
            if (first)
                first = false;
            else
                query += " , ";
            query += c;
            query += " = :";
            query += c;
        }
    }

    if (!where.empty()) {
        query += " WHERE ";
        query += where;
    }

    return query;
}

Driver::Operation *Driver::update(const std::string &table,
                                  const Types::ColumnSet &columns,
                                  const std::string &where)
{ return general(assembleUpdate(table, columns, where)); }

std::string Driver::assembleDel(const std::string &table, const std::string &where)
{
    std::string query = "DELETE FROM ";
    query += table;

    if (!where.empty()) {
        query += " WHERE ";
        query += where;
    }

    return query;
}

Driver::Operation *Driver::del(const std::string &table, const std::string &where)
{ return general(assembleDel(table, where)); }


Driver::InvalidOperation::InvalidOperation() = default;

Driver::InvalidOperation::~InvalidOperation() = default;

Driver::Query *Driver::InvalidOperation::issue(const Types::Binds &, bool)
{ return nullptr; }

bool Driver::InvalidOperation::batch(const Types::BatchBinds &)
{ return false; }

}
}