/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <cmath>
#include <QLoggingCategory>

#include "runlock.hxx"
#include "util.hxx"
#include "connection.hxx"
#include "tabledefinition.hxx"
#include "core/timeutils.hxx"
#include "core/waitutils.hxx"


Q_LOGGING_CATEGORY(log_database_runlock, "cpd3.database.runlock", QtWarningMsg)


namespace CPD3 {
namespace Database {


static constexpr double pingPurgeTime = 60.0;

RunLock::RunLock() : RunLock(Util::getDefaultDatabase())
{ }

static constexpr int systemIdentifier = 3;
static constexpr int systemVersion = 1;

static Connection::RequiredResult initialize(Connection *connection)
{
    if (!connection->hasTable("CPD3DBVersion")) {
        TableDefinition table("CPD3DBVersion");
        table.column("system", TableDefinition::Integer, false);
        table.column("version", TableDefinition::Integer, false);
        table.primaryKey("system");
        connection->createTable(table);
    }

    auto version = connection->select("CPD3DBVersion", {"version"}, "system = :system")
                             .single({{"system", systemIdentifier}})
                             .toInteger();
    if (INTEGER::defined(version)) {
        if (version != systemVersion) {
            qFatal("Run lock version (%d) not supported", static_cast<int>(systemVersion));
            return Connection::Required_Failed;
        }
        Q_ASSERT(version == systemVersion);
        Q_ASSERT(connection->hasTable("CPD3Run"));
    } else {
        connection->insert("CPD3DBVersion", {"system", "version"})
                  .synchronous({{"system",  systemIdentifier},
                                {"version", systemVersion}});

        TableDefinition table("CPD3Run");
        table.column("keyname", TableDefinition::FiniteString, false, 255);
        table.real("lastrun");
        table.integer<std::int64_t>("lockid");
        table.integer<std::int64_t>("ping");
        table.bytes("data");
        table.primaryKey("keyname");
        connection->createTable(table);
    }

    return Connection::Required_Ok;
}

RunLock::RunLock(Storage &&db) : connection(new Connection(std::move(db))),
                                 firstAcquireTime(FP::undefined()),
                                 lockIdentifer(0),
                                 mutex(),
                                 heldKeys(),
                                 pingState(Ping_Inactive),
                                 pingThread()
{
    connection->start();

    connection->requiredTransaction(std::bind(&initialize, connection));
}

RunLock::RunLock(const Storage &db) : connection(new Connection(db)),
                                      firstAcquireTime(FP::undefined()),
                                      lockIdentifer(0),
                                      mutex(),
                                      heldKeys(),
                                      pingState(Ping_Inactive),
                                      pingThread()
{
    connection->start();

    connection->requiredTransaction(std::bind(&initialize, connection));
}

RunLock::~RunLock()
{
    fail();

    connection->signalTerminate();
    connection->wait();
    delete connection;
}

double RunLock::acquire(const QString &key, double timeout, bool *ok)
{
    if (ok) *ok = false;
    std::string effectiveKey = key.toStdString();
    if (effectiveKey.length() > 254) {
        qCWarning(log_database_runlock) << "Truncating long key:" << key;
        effectiveKey.resize(254);
    }


    Statement purgeExpired = connection->update("CPD3Run", {"lockid"}, "keyname = :keyname AND "
            " ping IS NULL OR ping < :oldest");
    Statement getNewLockID = connection->select("CPD3Run", {"MAX(lockid)"});
    Statement getCurrentState =
            connection->select("CPD3Run", {"lockid", "lastrun"}, "keyname = :keyname");
    Statement createLocked = connection->insert("CPD3Run", {"keyname", "lockid", "ping"});
    Statement setLocked = connection->update("CPD3Run", {"lockid", "ping"}, "keyname = :keyname");


    double effectiveStart = Time::time();
    /* Due to Qt's handling of some databases (string conversion of doubles, WHY?!?), exact values
     * here are lossy, so round down the start time.  Since this already allows modification
     * times in the past (from when it returns), this is always valid. */
    effectiveStart = std::floor(effectiveStart * 1000.0) / 1000.0;

    double lastKeyRunTime = FP::undefined();
    std::int_fast64_t currentLockIdentifier = 0;
    if (!connection->synchronousTransaction([&]() -> Connection::SynchronousResult {
        purgeExpired.synchronous({{"keyname", effectiveKey},
                                  {"oldest",  static_cast<std::int_fast64_t>(std::floor(Time::time() - pingPurgeTime))},
                                  {"lockid",  Types::Variant(INTEGER::undefined())}});

        if (lockIdentifer == 0) {
            currentLockIdentifier = getNewLockID.single().toInteger();
            if (!INTEGER::defined(currentLockIdentifier)) {
                currentLockIdentifier = 1;
            } else {
                ++currentLockIdentifier;
            }
        } else {
            currentLockIdentifier = lockIdentifer;
        }

        auto currentState = getCurrentState.extract({{"keyname", effectiveKey}});

        if (currentState.size() != 2) {
            createLocked.synchronous({{"keyname", effectiveKey},
                                      {"lockid",  static_cast<std::int_fast64_t>(currentLockIdentifier)},
                                      {"ping",    static_cast<std::int_fast64_t>(std::ceil(Time::time()))}});
        } else if (lockIdentifer != 0 && currentState[0].toInteger() == lockIdentifer) {
            lastKeyRunTime = currentState[1].toReal();
        } else if (currentState[0].isSet()) {
            return Connection::Synchronous_Failed;
        } else {
            lastKeyRunTime = currentState[1].toReal();
            setLocked.synchronous({{"keyname", effectiveKey},
                                   {"lockid",  static_cast<std::int_fast64_t>(currentLockIdentifier)},
                                   {"ping",    static_cast<std::int_fast64_t>(std::ceil(Time::time()))}});
        }

        return Connection::Synchronous_Ok;
    }, Connection::Transaction_Serializable, timeout))
        return FP::undefined();

    if (!FP::defined(firstAcquireTime))
        firstAcquireTime = effectiveStart;
    lockIdentifer = currentLockIdentifier;
    startupPing(key);
    if (ok)
        *ok = true;

    if (FP::defined(lastKeyRunTime))
        lastKeyRunTime = std::floor(lastKeyRunTime * 1000.0) / 1000.0;
    return lastKeyRunTime;
}

void RunLock::fail()
{
    shutdownPing();

    if (lockIdentifer == 0)
        return;

    auto release = connection->update("CPD3Run", {"lockid", "ping"}, "lockid = :id");

    connection->synchronousTransaction([&]() -> Connection::SynchronousResult {
        release.synchronous({{"id",     static_cast<std::int_fast64_t>(lockIdentifer)},
                             {"lockid", Types::Variant(INTEGER::undefined())},
                             {"ping",   Types::Variant(INTEGER::undefined())}});
        return Connection::Synchronous_Ok;
    }, Connection::Transaction_Serializable);

    lockIdentifer = 0;
    firstAcquireTime = FP::undefined();
    updateData.clear();
}

void RunLock::release()
{
    shutdownPing();

    if (lockIdentifer == 0)
        return;

    Statement update = connection->update("CPD3Run", {"lastrun", "lockid", "ping"}, "lockid = :id");
    Statement setData = connection->update("CPD3Run", {"data"}, "keyname = :keyname");

    Types::Variant updateTime;
    if (FP::defined(firstAcquireTime))
        updateTime = Types::Variant(firstAcquireTime);

    connection->synchronousTransaction([&]() -> Connection::SynchronousResult {
        update.synchronous({{"id",      static_cast<std::int_fast64_t>(lockIdentifer)},
                            {"lastrun", updateTime},
                            {"lockid",  Types::Variant(INTEGER::undefined())},
                            {"ping",    Types::Variant(INTEGER::undefined())}});

        for (const auto &data : updateData) {
            setData.synchronous({{"keyname", data.first},
                                 {"data",    Types::Variant(data.second)}});
        }
        return Connection::Synchronous_Ok;
    }, Connection::Transaction_Serializable);

    lockIdentifer = 0;
    firstAcquireTime = FP::undefined();
    updateData.clear();
}

QByteArray RunLock::get(const QString &key) const
{
    Q_ASSERT(lockIdentifer != 0);

    QString effectiveKey(key);
    if (effectiveKey.length() >= 254) {
        qCWarning(log_database_runlock) << "Truncating long key: " << key;
        effectiveKey.truncate(254);
    }

    {
        auto check = updateData.find(effectiveKey);
        if (check != updateData.end())
            return check->second;
    }


    Statement fetch = connection->select("CPD3Run", {"data"}, "keyname = :keyname");
    QByteArray data;

    connection->synchronousTransaction([&]() -> Connection::SynchronousResult {
        auto v = fetch.single({{"keyname", effectiveKey}});
        if (v.isEmpty()) {
            data.clear();
            qCDebug(log_database_runlock) << "No row for key" << key << "(is this key locked?)";
            return Connection::Synchronous_Abort;
        }

        data = v.toQByteArray();
        return Connection::Synchronous_Ok;
    }, Connection::Transaction_Serializable);

    return data;
}

void RunLock::set(const QString &key, const QByteArray &data)
{
    Q_ASSERT(lockIdentifer != 0);

    QString effectiveKey(key);
    if (effectiveKey.length() >= 254) {
        qCWarning(log_database_runlock) << "Truncating long key:" << key;
        effectiveKey.truncate(254);
    }

    updateData[effectiveKey] = data;
}

QByteArray RunLock::blindGet(const QString &key) const
{
    QString effectiveKey(key);
    if (effectiveKey.length() >= 254) {
        qCWarning(log_database_runlock) << "Truncating long key:" << key;
        effectiveKey.truncate(254);
    }

    Statement fetch = connection->select("CPD3Run", {"data"}, "keyname = :keyname");
    QByteArray data;

    connection->synchronousTransaction([&]() -> Connection::SynchronousResult {
        auto v = fetch.single({{"keyname", effectiveKey}});
        if (v.isEmpty()) {
            data.clear();
            return Connection::Synchronous_Abort;
        }

        data = v.toQByteArray();
        return Connection::Synchronous_Ok;
    }, Connection::Transaction_Serializable);

    return data;
}

void RunLock::blindSet(const QString &key, const QByteArray &data) const
{
    std::string effectiveKey = key.toStdString();
    if (effectiveKey.length() >= 254) {
        qCWarning(log_database_runlock) << "Truncating long key:" << key;
        effectiveKey.resize(254);
    }

    Statement setData = connection->merge("CPD3Run", {"keyname"}, {"data"});

    connection->synchronousTransaction([&]() -> Connection::SynchronousResult {
        setData.synchronous({{"keyname", effectiveKey},
                             {"data",    Types::Variant(data)}});
        return Connection::Synchronous_Ok;
    }, Connection::Transaction_Serializable);
}

void RunLock::seenTime(double time)
{
    if (lockIdentifer == 0)
        return;
    if (!FP::defined(time))
        return;
    if (!FP::defined(firstAcquireTime))
        return;
    /* Due to Qt's handling of some databases (string conversion of doubles, WHY?!?), exact values
     * here are lossy, so round up the start time.  Since this is used to exclude already
     * modified data, moving it forward makes more sense (so we don't hit data again). */
    time = std::ceil(time * 1000.0) / 1000.0;
    if (time > firstAcquireTime)
        return;
    firstAcquireTime = time;
}


void RunLock::startupPing(const QString &key)
{
    std::unique_lock<std::mutex> lock(mutex);
    heldKeys.insert(key);
    for (;;) {
        switch (pingState) {
        case Ping_Inactive:
            if (pingThread.joinable())
                pingThread.join();
            pingState = Ping_Active;
            pingThread = std::thread(&RunLock::pingExecute, this);
            return;
        case Ping_Active:
            return;
        case Ping_Terminate:
            break;
        }
        cv.wait(lock);
    }
}

void RunLock::shutdownPing()
{
    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        switch (pingState) {
        case Ping_Inactive:
            if (pingThread.joinable())
                pingThread.join();
            return;
        case Ping_Active:
            pingState = Ping_Terminate;
            cv.notify_all();
            break;
        case Ping_Terminate:
            break;
        }
        cv.wait(lock);
    }
}

void RunLock::pingExecute()
{
    Statement update = connection->update("CPD3Run", {"ping"}, "lockid = :lockid");

    int failures = 0;
    for (;;) {
        std::unique_lock<std::mutex> lock(mutex);
        switch (pingState) {
        case Ping_Inactive:
            Q_ASSERT(false);
            break;
        case Ping_Active:
            break;
        case Ping_Terminate:
            pingState = Ping_Inactive;
            cv.notify_all();
            return;
        }

        if (failures <= 0) {
            cv.wait_for(lock, std::chrono::seconds(5));
        } else if (failures <= 10) {
            cv.wait_for(lock, std::chrono::milliseconds(100));
        }

        if (pingState != Ping_Active)
            continue;
        lock.unlock();

        double timeout = 10.0;
        if (failures < 10)
            timeout = 0.0;
        else if (failures < 20)
            timeout = 0.1;

        if (!connection->synchronousTransaction([&]() -> Connection::SynchronousResult {
            update.synchronous({{"ping",   static_cast<std::int_fast64_t>(std::ceil(Time::time()))},
                                {"lockid", static_cast<std::int_fast64_t>(lockIdentifer)}});
            return Connection::Synchronous_Ok;
        }, Connection::Transaction_Serializable, timeout))
            ++failures;
        else
            failures = 0;
    }
}


void RunLock::cleanupStale()
{ return cleanupStale(Util::getDefaultDatabase()); }

void RunLock::cleanupStale(const Storage &db)
{ return cleanupStale(std::move(Storage(db))); }

void RunLock::cleanupStale(Storage &&db)
{
    Connection connection(std::move(db));
    connection.start();

    Statement purgeExpired =
            connection.update("CPD3Run", {"lockid"}, "ping is NULL OR ping < :oldest");

    connection.synchronousTransaction([&]() -> Connection::SynchronousResult {
        if (!connection.hasTable("CPD3Run"))
            return Connection::Synchronous_Ok;

        purgeExpired.synchronous({{"oldest", static_cast<std::int_fast64_t>(std::floor(Time::time() - pingPurgeTime))},
                                  {"lockid", Types::Variant(INTEGER::undefined())}});
        return Connection::Synchronous_Ok;
    }, Connection::Transaction_Serializable);
}


}
}