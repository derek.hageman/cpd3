if(NOT UNIX AND NOT WINDOWS)
    set(QT_SERIAL_COMPONENTS SerialPort)
else(NOT UNIX AND NOT WINDOWS)
    set(QT_SERIAL_COMPONENTS)
endif(NOT UNIX AND NOT WINDOWS)

find_package(Qt5 5.4 REQUIRED
             COMPONENTS Core Svg Sql Xml Network Concurrent Test
             OPTIONAL_COMPONENTS Widgets PrintSupport ${QT_SERIAL_COMPONENTS})

find_package(OpenSSL REQUIRED)

# Optional language support currently not implemented, so just do this
# only if we find a GNU compiler
#enable_language(Fortran OPTIONAL)
find_program(Fortran_FOUND NAMES gfortran g77)
if(Fortran_FOUND)
    enable_language(Fortran)
endif(Fortran_FOUND)

# Prefer luajit if available
find_path(LUA_INCLUDE_DIR lua.h
          HINTS ENV LUAJIT_DIR
          PATH_SUFFIXES
          luajit-2.0 luajit2.0 luajit-2.1 luajit2.1
          include/luajit-2.0 include/luajit2.0
          include/luajit-2.1 include/luajit2.1
          include/luajit include
          PATHS ~/Library/Frameworks /Library/Frameworks
          /sw /opt/local /opt/sw /opt)
if(MSVC)
    find_library(LUA_LIBRARIES NAMES luajit-5.1 luajit5.1 luajit51 luajit lua51
                 HINTS ENV LUAJIT_DIR)
else(MSVC)
    find_library(LUA_LIBRARIES NAMES luajit-5.1 luajit5.1 luajit51 luajit
                 HINTS ENV LUAJIT_DIR
                 PATH_SUFFIXES lib lib64
                 PATHS ~/Library/Frameworks /Library/Frameworks
                 /sw /opt/local /opt/sw /opt)
endif(MSVC)
if(LUA_INCLUDE_DIR AND LUA_LIBRARIES)
    set(HAVE_LUAJIT TRUE)
else(LUA_INCLUDE_DIR AND LUA_LIBRARIES)
    find_package(Lua REQUIRED)
endif(LUA_INCLUDE_DIR AND LUA_LIBRARIES)


if(NOT ALLOCATOR_LIBRARIES AND (ALLOCATOR STREQUAL "any" OR ALLOCATOR STREQUAL "tbb"))
    if(PKG_CONFIG_FOUND)
        include(FindPkgConfig)
        pkg_check_modules(TBBPROXY tbbmalloc_proxy)
        if(TBBPROXY_FOUND)
            include_directories(${TBBPROXY_INCLUDE_DIRS})
        endif(TBBPROXY_FOUND)
    endif(PKG_CONFIG_FOUND)
    if(TBBPROXY_LIBRARIES)
        set(ALLOCATOR_LIBRARIES ${TBBPROXY_LIBRARIES})
    else(TBBPROXY_LIBRARIES)
        find_library(ALLOCATOR_LIBRARIES
                     NAMES tbbmalloc_proxy)
    endif(TBBPROXY_LIBRARIES)
    if(ALLOCATOR_LIBRARIES)
        set(ALLOCATE_TYPE 1)
    endif(ALLOCATOR_LIBRARIES)

    include(CheckSymbolExists)
    set(CMAKE_REQUIRED_LIBRARIES ${ALLOCATOR_LIBRARIES})
    check_symbol_exists(scalable_allocation_command "tbb/scalable_allocator.h"
                        HAVE_TBB_COMMAND)
    set(CMAKE_REQUIRED_LIBRARIES "")
    if(HAVE_TBB_COMMAND)
        add_definitions(-DHAVE_TBB_COMMAND)
    endif(HAVE_TBB_COMMAND)
endif(NOT ALLOCATOR_LIBRARIES AND (ALLOCATOR STREQUAL "any" OR ALLOCATOR STREQUAL "tbb"))

if(NOT ALLOCATOR_LIBRARIES AND (ALLOCATOR STREQUAL "any" OR ALLOCATOR STREQUAL "tcmalloc"))
    if(PKG_CONFIG_FOUND)
        include(FindPkgConfig)
        pkg_check_modules(LIBTCMALLOC libtcmalloc)
        set(ALLOCATOR_INCLUDE ${LIBTCMALLOC_INCLUDE_DIRS})
    endif(PKG_CONFIG_FOUND)
    find_library(ALLOCATOR_LIBRARIES
                 NAMES tcmalloc_minimal tcmalloc
                 PATHS ${LIBTCMALLOC_LIBDIR})
    if(ALLOCATOR_LIBRARIES)
        set(ALLOCATE_TYPE 2)
    endif(ALLOCATOR_LIBRARIES)
endif(NOT ALLOCATOR_LIBRARIES AND (ALLOCATOR STREQUAL "any" OR ALLOCATOR STREQUAL "tcmalloc"))

if(NOT ALLOCATE_TYPE)
    set(ALLOCATE_TYPE 0)
    set(ALLOCATOR_LIBRARIES "")
endif(NOT ALLOCATE_TYPE)


if(UNIX AND CMAKE_SYSTEM_NAME STREQUAL "Linux")
    find_package(PkgConfig)
    if(PKG_CONFIG_FOUND)
        include(FindPkgConfig)
        pkg_check_modules(SYSTEMD libsystemd QUIET)
        if(SYSTEMD_FOUND)
            include_directories(${SYSTEMD_INCLUDE_DIRS})
        endif(SYSTEMD_FOUND)
    endif(PKG_CONFIG_FOUND)

    find_library(SYSTEMD_LIBRARY
                 NAMES systemd libsystemd
                 PATHS ${SYSTEMD_LIBDIR})
    if(SYSTEMD_LIBRARY)
        include(CheckSymbolExists)
        set(CMAKE_REQUIRED_LIBRARIES ${SYSTEMD_LIBRARY})
        check_symbol_exists(sd_notify "systemd/sd-daemon.h"
                            HAVE_SD_NOTIFY)
        set(CMAKE_REQUIRED_LIBRARIES "")
        if(HAVE_SD_NOTIFY)
            add_definitions(-DHAVE_SD_NOTIFY)
        endif(HAVE_SD_NOTIFY)
    endif(SYSTEMD_LIBRARY)


    if(PKG_CONFIG_FOUND)
        include(FindPkgConfig)
        pkg_check_modules(LIBUDEV libudev QUIET)
        if(LIBUDEV_FOUND)
            include_directories(${LIBUDEV_INCLUDE_DIRS})
        endif(LIBUDEV_FOUND)
    endif(PKG_CONFIG_FOUND)

    find_library(LIBUDEV_LIBRARY
                 NAMES udev libudev
                 PATHS ${LIBUDEV_LIBDIR})
    if(LIBUDEV_LIBRARY)
        include(CheckSymbolExists)
        set(CMAKE_REQUIRED_LIBRARIES ${LIBUDEV_LIBRARY})
        check_symbol_exists(udev_new "libudev.h"
                            HAVE_UDEV)
        set(CMAKE_REQUIRED_LIBRARIES "")
        if(HAVE_UDEV)
            add_definitions(-DHAVE_UDEV)
        endif(HAVE_UDEV)
    endif(LIBUDEV_LIBRARY)
endif(UNIX AND CMAKE_SYSTEM_NAME STREQUAL "Linux")

find_library(NETCDF_LIBRARY
             NAMES netcdf libnetcdf)
if(NETCDF_LIBRARY)
    find_path(NETCDF_INCLUDE_DIR NAMES netcdf.h)

    include(CheckSymbolExists)
    set(CMAKE_REQUIRED_LIBRARIES ${NETCDF_LIBRARY})
    set(CMAKE_REQUIRED_INCLUDES ${NETCDF_INCLUDE_DIR})
    check_symbol_exists(nc_open "netcdf.h"
                        HAVE_NETCDF)
    set(CMAKE_REQUIRED_LIBRARIES "")
    set(CMAKE_REQUIRED_INCLUDES "")
endif(NETCDF_LIBRARY)

find_library(SQLITE3_LIBRARY NAMES sqlite3 libsqlite3
             HINTS ENV SQLITE3_DIR)
if(SQLITE3_LIBRARY)
    find_path(SQLITE3_INCLUDE_DIR NAMES sqlite3.h
              HINTS ENV SQLITE3_DIR)

    include(CheckSymbolExists)
    set(CMAKE_REQUIRED_LIBRARIES ${SQLITE3_LIBRARY})
    set(CMAKE_REQUIRED_INCLUDES ${SQLITE3_INCLUDE_DIR})
    check_symbol_exists(sqlite3_open_v2 "sqlite3.h"
                        HAVE_SQLITE3)
    check_symbol_exists(sqlite3_prepare_v3 "sqlite3.h"
                        HAVE_SQLITE3_PREPARE3)
    check_symbol_exists(sqlite3_bind_text64 "sqlite3.h"
                        HAVE_SQLITE3_TEXT64)
    check_symbol_exists(sqlite3_bind_blob64 "sqlite3.h"
                        HAVE_SQLITE3_BLOB64)
    check_symbol_exists(sqlite3_errstr "sqlite3.h"
                        HAVE_SQLITE3_ERRSTR)
    set(CMAKE_REQUIRED_LIBRARIES "")
    set(CMAKE_REQUIRED_INCLUDES "")
endif(SQLITE3_LIBRARY)

find_package(PostgreSQL)
if(PostgreSQL_FOUND)
    include(CheckSymbolExists)
    set(CMAKE_REQUIRED_LIBRARIES ${PostgreSQL_LIBRARIES})
    set(CMAKE_REQUIRED_INCLUDES ${PostgreSQL_INCLUDE_DIRS})
    check_symbol_exists(PQconnectdbParams "libpq-fe.h"
                        HAVE_POSTGRESQL_PQCONNECTDBPARAMS)
    set(CMAKE_REQUIRED_LIBRARIES "")
    set(CMAKE_REQUIRED_INCLUDES "")
endif(PostgreSQL_FOUND)

if(MSVC)
    if("${CMAKE_CXX_FLAGS}" MATCHES "/W[0-4]")
        string(REGEX REPLACE "/W[0-4]" "/W3" CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")
    else()
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /W3")
    endif()
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /wd4290 /wd4458 /wd4127 /wd4267 /wd4068 /wd4180")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /wd4065 /wd4789 /wd4521")
    add_definitions(-DUNICODE)
    add_definitions(-D_SCL_SECURE_NO_WARNINGS)

    include(CheckCXXCompilerFlag)
    check_cxx_compiler_flag("/std:c++latest" COMPILER_SUPPORTS_LATEST)
    if(COMPILER_SUPPORTS_LATEST)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /std:c++latest")
    endif(COMPILER_SUPPORTS_LATEST)
elseif(CMAKE_COMPILER_IS_GNUCC OR CMAKE_COMPILER_IS_GNUCXX)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")

    execute_process(COMMAND ${CMAKE_C_COMPILER} -dumpversion
                    OUTPUT_VARIABLE GCC_VERSION)
    if(GCC_VERSION VERSION_GREATER 4.4 OR GCC_VERSION VERSION_EQUAL 4.4)
        # Turn this off, since it takes a long time for minimal gains
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fno-var-tracking")
    endif(GCC_VERSION VERSION_GREATER 4.4 OR GCC_VERSION VERSION_EQUAL 4.4)

    # Seems to hit false positives in some Qt containers
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-strict-overflow")

    include(CheckCXXCompilerFlag)
    foreach(version 17 14 11)
        check_cxx_compiler_flag("-std=c++${version}" COMPILER_SUPPORTS_CXX${version})
        if(COMPILER_SUPPORTS_CXX${version})
            set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++${version}")
            set(CXX_VERSION ${version})
            break()
        endif()
    endforeach()
    if(NOT CXX_VERSION)
        message(FATAL_ERROR "No C++ 0x/11/14/17 support found")
    endif(NOT CXX_VERSION)

    set(THREADS_PREFER_PTHREAD_FLAG ON)
    find_package(Threads REQUIRED)
elseif("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")
    #set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Weverything")

    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wno-inconsistent-missing-override")
    include(CheckCXXCompilerFlag)
    foreach(version 17 14 11)
        check_cxx_compiler_flag("-std=c++${version}" COMPILER_SUPPORTS_CXX${version})
        if(COMPILER_SUPPORTS_CXX${version})
            set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++${version} -stdlib=libc++")
            set(CXX_VERSION ${version})
            break()
        endif()
    endforeach()
    if(NOT CXX_VERSION)
        message(FATAL_ERROR "No C++ 0x/11/14/17 support found")
    endif(NOT CXX_VERSION)

    set(THREADS_PREFER_PTHREAD_FLAG ON)
    find_package(Threads REQUIRED)
endif()

if(APPLE AND HAVE_LUAJIT)
    # Special flags to free up addresses for GC
    set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -pagezero_size 10000 -image_base 100000000")
endif(APPLE AND HAVE_LUAJIT)

check_cxx_source_compiles("
#include <execution>
void test() { (void)std::execution::par_unseq; };
int main() { return 0; };
" HAVE_CXX_PARALLEL)
if(HAVE_CXX_PARALLEL)
    add_definitions(-DHAVE_CXX_PARALLEL)
elseif(CMAKE_COMPILER_IS_GNUCC)
    find_package(OpenMP)
    if(OPENMP_FOUND)
        set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
    endif()
endif()

if(IS_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/libs")
    install(DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/libs/" DESTINATION ${LIB_INSTALL_DIR})
    file(GLOB CPD3_AUXILIARY_LIBS "${CMAKE_CURRENT_BINARY_DIR}/libs/*")
endif(IS_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/libs")
