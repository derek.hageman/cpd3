/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CPD3FILEOUTPUT_ENGINE_HXX
#define CPD3FILEOUTPUT_ENGINE_HXX

#include "core/first.hxx"

#include <memory>
#include <mutex>
#include <condition_variable>
#include <QThread>

#include "fileoutput.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsink.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/segment.hxx"
#include "datacore/streampipeline.hxx"
#include "io/drivers/file.hxx"

namespace CPD3 {
namespace Output {

/**
 * The main engine used to generate output files.  The primary purpose is to handle
 * multiple passes through the data.
 */
class CPD3FILEOUTPUT_EXPORT Engine {
public:
    /**
     * A stage in the the data processing.
     */
    class CPD3FILEOUTPUT_EXPORT Stage {
    public:
        Stage();

        virtual ~Stage();

        /**
         * Test if the stage is always the final one (i.e. no data needs to be preserved).
         * This is called before any data are processed.
         *
         * @return  true if the stage is the last one
         */
        virtual bool isFinalStage() const;

        /**
         * Test if the stage is both modifies data (odifyData(CPD3::Data::SequenceValue::Transfer &)
         * and those modifications should be persisted (i.e. written out).  The first stage
         * always retains modifications.
         *
         * @return  true if the stage modifies data and those modifications should be retained
         */
        virtual bool retainModifiedData() const;

        /**
         * Get the next stage of processing, or null if there is none.  This is called after
         * data completion.
         *
         * @return  the next processing stage, if any
         */
        virtual std::unique_ptr<Stage> getNextStage();

        /**
         * Apply data modification before processing.  The results are only retained
         * if retainModifiedData() returns true.  This is run from the data generator
         * thread, rather than the main thread, so it may require synchronization.
         *
         * @param values    the input and output values
         */
        virtual void modifyData(CPD3::Data::SequenceValue::Transfer &values);

        /**
         * Handle incoming data to the processing stage.  This is called from a dedicated
         * processing thread, so it can block as required.
         *
         * @param values    the incoming data values
         */
        virtual void processData(CPD3::Data::SequenceValue::Transfer &&values) = 0;

        /**
         * Called at the end of incoming data.  This is called from a dedicated
         * processing thread, so it can block as required.
         */
        virtual void finalizeData();
    };

    /**
     * A simple wrapper stage that fans out data to a series of StreamSinks.
     */
    class CPD3FILEOUTPUT_EXPORT SinkFanoutStage : public Stage {
        std::vector<CPD3::Data::StreamSink *> first;
        CPD3::Data::StreamSink *last;
    public:
        explicit SinkFanoutStage(const std::vector<CPD3::Data::StreamSink *> &stages);

        explicit SinkFanoutStage(std::vector<CPD3::Data::StreamSink *> &&stages);

        virtual ~SinkFanoutStage();

        void processData(CPD3::Data::SequenceValue::Transfer &&values) override;

        void finalizeData() override;
    };

    /**
     * A simple implementation of a static dispatcher stage.
     */
    class CPD3FILEOUTPUT_EXPORT SinkDispatchStage : public Stage {
        struct Target {
            std::vector<CPD3::Data::StreamSink *> first;
            CPD3::Data::StreamSink *last;

            explicit Target(std::vector<CPD3::Data::StreamSink *> &&targets);
        };

        CPD3::Data::SequenceName::Map<Target> dispatch;
    public:
        SinkDispatchStage();

        virtual ~SinkDispatchStage();

        void processData(CPD3::Data::SequenceValue::Transfer &&values) override;

        void finalizeData() override;

    protected:
        virtual std::vector<
                CPD3::Data::StreamSink *> getTargets(const CPD3::Data::SequenceName &name) = 0;
    };

    /**
     * A simple implementation of a stage that generates segments for incoming data.
     */
    class CPD3FILEOUTPUT_EXPORT SegmentStage : public Stage {
    public:
        SegmentStage();

        virtual ~SegmentStage();

        void processData(CPD3::Data::SequenceValue::Transfer &&values) override;

        void finalizeData() override;

    protected:
        CPD3::Data::SequenceSegment::Stream stream;

        virtual CPD3::Data::SequenceSegment::Transfer integrateValue(CPD3::Data::SequenceValue &&value);

        virtual void processSegment(CPD3::Data::SequenceSegment &&segment) = 0;
    };

    /**
     * A simple sink wrapper that applies clipping to its input values
     */
    class CPD3FILEOUTPUT_EXPORT ClipSinkWrapper final : public CPD3::Data::StreamSink {
        Time::Bounds clip;
        CPD3::Data::StreamSink *sink;

    public:
        ClipSinkWrapper(const Time::Bounds &clip, CPD3::Data::StreamSink *sink);

        virtual ~ClipSinkWrapper();

        void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

        void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

        void incomingData(const CPD3::Data::SequenceValue &value) override;

        void incomingData(CPD3::Data::SequenceValue &&value) override;

        void endData() override;
    };

private:
    std::unique_ptr<Stage> stage;

    bool terminateRequested;
    bool completed;

    class IncomingHandler : public CPD3::Data::StreamSink {
        Engine &engine;

        void stall(std::unique_lock<std::mutex> &lock);

    public:
        explicit IncomingHandler(Engine &engine);

        virtual ~IncomingHandler();

        void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

        void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

        void incomingData(const CPD3::Data::SequenceValue &value) override;

        void incomingData(CPD3::Data::SequenceValue &&value) override;

        void endData() override;
    };

    friend class IncomingHandler;

    IncomingHandler incomingHandler;

    bool filesUpdated;
    bool pendingEnd;
    CPD3::Data::SequenceValue::Transfer pendingValues;

    std::shared_ptr<IO::File::Backing> inputFile;
    std::unique_ptr<IO::Generic::Stream> inputStream;
    std::shared_ptr<IO::File::Backing> outputFile;
    std::unique_ptr<CPD3::Data::StandardDataOutput> outputWriter;
    std::unique_ptr<CPD3::Data::StandardDataInput> inputReader;


    std::thread thread;
    std::mutex mutex;
    std::condition_variable external;
    std::condition_variable internal;

    bool prepareStage();

    bool runStage();

    bool advanceStage();

    void processingAborted();

    void run();

public:
    explicit Engine(std::unique_ptr<Stage> &&stage);

    virtual ~Engine();

    /**
     * Signals termination of the output.
     */
    void signalTerminate();

    /**
     * Launch the output engine.
     */
    void start();

    /**
     * Test if the engine has completed.
     *
     * @return  true if the engine has completed
     */
    bool isFinished();

    /**
     * Wait for the engine to finish.
     *
     * @param timeout   the maximum time to wait
     * @return          true if the input has finished
     */
    bool wait(double timeout = FP::undefined());

    ActionFeedback::Source feedback;

    /**
     * Emitted when the engine has finished running.
     */
    Threading::Signal<> finished;

    static bool configurePipelineInput(const CPD3::Data::Variant::Read &processing,
                                       const CPD3::Data::SequenceName::Component &station,
                                       CPD3::Data::StreamPipeline *target,
                                       CPD3::Data::Archive::Selection::List selections,
                                       const Time::Bounds &clip = {});

protected:

    /**
     * Get the initial data sink that input should be directed to.  Only valid between
     * the calls of startFirstStage() and firstStageComplete().
     *
     * @return  the data sink
     */
    inline CPD3::Data::StreamSink *getDataSink()
    { return &incomingHandler; }

    /**
     * Called from the processing thread at initial startup.  If the engine is self contained
     * (i.e. requests its own data) this should create the data access chain.
     *
     * @param stage the first stage
     * @return      true on success
     */
    virtual bool startFirstStage(Stage *stage);

    /**
     * Called from the processing thread after the very first stage completes.  This can be
     * used to clean up anything created with startFirstStage()
     *
     * @param stage the first stage
     */
    virtual void firstStageComplete(Stage *stage);

    /**
     * Called from the processing thread if the first stage start was successful but
     * further processing has been aborted (due to termination or another error).
     */
    virtual void firstStageAborted(Stage *stage);
};

}
}

#endif //CPD3FILEOUTPUT_ENGINE_HXX
