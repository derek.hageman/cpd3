/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QTest>

#include "fileoutput/variablevalue.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"

using namespace CPD3;
using namespace CPD3::Output;
using namespace CPD3::Data;

class TestOutputVariableValue : public QObject {
Q_OBJECT

    static void send(const std::vector<StreamSink *> &sinks, const SequenceValue &value)
    {
        for (auto sink : sinks) {
            sink->incomingData(value);
        }
    }

    static void end(const std::vector<StreamSink *> &sinks)
    {
        for (auto sink : sinks) {
            sink->endData();
        }
    }

private slots:

    void basic()
    {
        FragmentTracker ft;
        VariableValue::Base v(ft);
        Variant::Root config;

        config["Constant"] = 2.0;

        v.addInput(std::make_shared<VariableValue::Base::DataInput>(config), 100, 200);
        v.endConfigure();

        QVERIFY(v.toArchiveSelections().empty());
        QVERIFY(v.getTargets({"bnd", "raw", "BsG_S11"}).empty());

        auto r = ft.getFragments();
        QCOMPARE((int) r.size(), 1);
        QCOMPARE(r[0].getStart(), 100.0);
        QCOMPARE(r[0].getEnd(), 200.0);
        QCOMPARE(v.get(r[0].contents).toReal(), 2.0);
    }

    void dataInput()
    {
        FragmentTracker ft;
        VariableValue::Base v(ft);
        Variant::Root config;

        config["Constant"] = 2.0;
        config["Input/#0/Variable"] = "BsG_S11";

        v.addInput(std::make_shared<VariableValue::Base::DataInput>(config), 100, 200);
        v.endConfigure();

        QVERIFY(!v.toArchiveSelections().empty());
        auto t = v.getTargets({"bnd", "raw", "BsG_S11"});
        QVERIFY(!t.empty());
        QVERIFY(v.getTargets({"bnd", "raw", "BsR_S11"}).empty());

        send(t, {SequenceIdentity({"bnd", "raw", "BsG_S11"}, 100, 150), Variant::Root(3.0)});
        send(t, {SequenceIdentity({"bnd", "raw", "BsG_S11"}, 150, 180), Variant::Root(4.0)});
        end(t);

        auto r = ft.getFragments();
        QCOMPARE((int) r.size(), 3);
        QCOMPARE(r[0].getStart(), 100.0);
        QCOMPARE(r[0].getEnd(), 150.0);
        QCOMPARE(v.get(r[0].contents).toReal(), 3.0);
        QCOMPARE(r[1].getStart(), 150.0);
        QCOMPARE(r[1].getEnd(), 180.0);
        QCOMPARE(v.get(r[1].contents).toReal(), 4.0);
        QCOMPARE(r[2].getStart(), 180.0);
        QCOMPARE(r[2].getEnd(), 200.0);
        QCOMPARE(v.get(r[2].contents).toReal(), 2.0);
    }

    void changeInput()
    {
        FragmentTracker ft;
        VariableValue::Base v(ft);
        Variant::Root config;

        config["Input/#0/Variable"] = "BsG_S11";
        v.addInput(std::make_shared<VariableValue::Base::DataInput>(config), 100, 200);
        config["Input/#0/Variable"] = "BsR_S11";
        v.addInput(std::make_shared<VariableValue::Base::DataInput>(config), 200, 300);

        v.endConfigure();

        QVERIFY(!v.toArchiveSelections().empty());
        auto t1 = v.getTargets({"bnd", "raw", "BsG_S11"});
        QVERIFY(!t1.empty());
        auto t2 = v.getTargets({"bnd", "raw", "BsR_S11"});
        QVERIFY(!t2.empty());

        send(t1, {SequenceIdentity({"bnd", "raw", "BsG_S11"}, 100, 200), Variant::Root(3.0)});
        send(t1, {SequenceIdentity({"bnd", "raw", "BsG_S11"}, 200, 300), Variant::Root(4.0)});
        send(t2, {SequenceIdentity({"bnd", "raw", "BsR_S11"}, 200, 300), Variant::Root(5.0)});
        end(t1);
        end(t2);

        auto r = ft.getFragments();
        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].getStart(), 100.0);
        QCOMPARE(r[0].getEnd(), 200.0);
        QCOMPARE(v.get(r[0].contents).toReal(), 3.0);
        QCOMPARE(r[1].getStart(), 200.0);
        QCOMPARE(r[1].getEnd(), 300.0);
        QCOMPARE(v.get(r[1].contents).toReal(), 5.0);
    }


};

QTEST_MAIN(TestOutputVariableValue)

#include "variablevalue.moc"
