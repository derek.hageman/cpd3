/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QTest>
#include <QTemporaryFile>

#include "fileoutput/ebas.hxx"
#include "core/number.hxx"
#include "core/qtcompat.hxx"
#include "datacore/archive/access.hxx"

using namespace CPD3;
using namespace CPD3::Output;
using namespace CPD3::Data;

class TestEBAS : public QObject {
Q_OBJECT

    static QByteArray takeLine(QByteArray &buffer)
    {
        int idx = buffer.indexOf('\n');
        if (idx < 0)
            return buffer;
        QByteArray result(buffer.mid(0, idx));
        buffer = buffer.mid(idx + 1);
        return result;
    }

    void baselineFile(Variant::Write wr)
    {
        wr["Originator"] = "Originator";
        wr["Organization"] = "Organization";
        wr["Submitter"] = "Submitter";
        wr["Projects"] = "Projects";
        wr["Interval"] = "Interval";
    }

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();
    }

    void trivialFile()
    {
        EBAS ctx;
        Variant::Root config;

        QTemporaryFile outputFile;
        QVERIFY(outputFile.open());
        int fileResults = 0;
        ctx.createOutput = [&](const std::string &name) -> std::unique_ptr<QFile> {
            Q_ASSERT(name == "stuff");
            ++fileResults;
            std::unique_ptr<QFile> result(new QFile(outputFile.fileName()));
            if (!result->open(QIODevice::ReadWrite))
                return {};
            return std::move(result);
        };

        config["Types/Main/Filename"] = "stuff";
        baselineFile(config["Types/Main"]);
        config["Types/Main/Variables/One/Data/Input/#0/Variable"] = "v1";
        config["Types/Main/Variables/One/Header/Title/Full"] = "var_title";
        config["Types/Main/Variables/One/Header/Column"] = "col_header";
        ctx.config.emplace_back(FP::undefined(), FP::undefined(), config);

        auto stage = EBAS::stage(std::move(ctx));
        QVERIFY(stage.get() != nullptr);
        QVERIFY(!stage->isFinalStage());
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 864, 1728), Variant::Root(2.0)}});
        stage->finalizeData();

        stage = stage->getNextStage();
        QVERIFY(stage.get() != nullptr);
        QVERIFY(!stage->isFinalStage());
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 864, 1728), Variant::Root(2.0)}});
        stage->finalizeData();

        stage = stage->getNextStage();
        QVERIFY(stage.get() != nullptr);
        QVERIFY(stage->isFinalStage());
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 864, 1728), Variant::Root(2.0)}});
        stage->finalizeData();
        stage.reset();

        QCOMPARE(fileResults, 1);

        QFile read(outputFile.fileName());
        QVERIFY(read.open(QIODevice::ReadOnly));
        auto contents = read.readAll();

        QCOMPARE(takeLine(contents), QByteArray("18 1001"));
        QCOMPARE(takeLine(contents), QByteArray("Originator"));
        QCOMPARE(takeLine(contents), QByteArray("Organization"));
        QCOMPARE(takeLine(contents), QByteArray("Submitter"));
        QCOMPARE(takeLine(contents), QByteArray("Projects"));
        QCOMPARE(takeLine(contents), QByteArray("1 1"));
        QVERIFY(takeLine(contents).startsWith("1970 01 01 "));
        QCOMPARE(takeLine(contents), QByteArray("Interval"));
        QCOMPARE(takeLine(contents), QByteArray("days from file reference point"));
        QCOMPARE(takeLine(contents), QByteArray("3"));
        QCOMPARE(takeLine(contents), QByteArray("1 1 1"));
        QCOMPARE(takeLine(contents), QByteArray("9999.999999 9999.99 9.999999999"));
        QCOMPARE(takeLine(contents),
                 QByteArray("end_time of measurement, days from the file reference point"));
        QCOMPARE(takeLine(contents), QByteArray("var_title"));
        QCOMPARE(takeLine(contents), QByteArray("numflag"));
        QCOMPARE(takeLine(contents), QByteArray("0"));
        QCOMPARE(takeLine(contents), QByteArray("1"));
        QCOMPARE(takeLine(contents), QByteArray("start_time end_time col_header numflag"));
        QCOMPARE(takeLine(contents), QByteArray("   0.010000    0.020000    2.00 0.000000000"));
    }

    void ebasHeaders()
    {
        EBAS ctx;
        Variant::Root config;

        QTemporaryFile outputFile;
        QVERIFY(outputFile.open());
        int fileResults = 0;
        ctx.createOutput = [&](const std::string &name) -> std::unique_ptr<QFile> {
            Q_ASSERT(name == "stuff");
            ++fileResults;
            std::unique_ptr<QFile> result(new QFile(outputFile.fileName()));
            if (!result->open(QIODevice::ReadWrite))
                return {};
            return std::move(result);
        };

        config["Types/Main/Filename"] = "stuff";
        baselineFile(config["Types/Main"]);
        config["Types/Main/Headers/First/Name"] = "A key";
        config["Types/Main/Headers/First/Contents"] = "The Value";
        config["Types/Main/Headers/First/SortPriority"] = 0;
        config["Types/Main/Headers/Second/Name"] = "Another key";
        config["Types/Main/Headers/Second/Contents"] = "The: value";
        config["Types/Main/Headers/Second/SortPriority"] = 1;
        config["Types/Main/Variables/One/Data/Input/#0/Variable"] = "v1";
        config["Types/Main/Variables/One/Header/Title/Full"] = "var_title";
        config["Types/Main/Variables/One/Header/Column"] = "col_header";
        ctx.config.emplace_back(FP::undefined(), FP::undefined(), config);

        auto stage = EBAS::stage(std::move(ctx));
        QVERIFY(stage.get() != nullptr);
        QVERIFY(!stage->isFinalStage());
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 864, 1728), Variant::Root(2.0)}});
        stage->finalizeData();

        stage = stage->getNextStage();
        QVERIFY(stage.get() != nullptr);
        QVERIFY(!stage->isFinalStage());
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 864, 1728), Variant::Root(2.0)}});
        stage->finalizeData();

        stage = stage->getNextStage();
        QVERIFY(stage.get() != nullptr);
        QVERIFY(stage->isFinalStage());
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 864, 1728), Variant::Root(2.0)}});
        stage->finalizeData();
        stage.reset();

        QCOMPARE(fileResults, 1);

        QFile read(outputFile.fileName());
        QVERIFY(read.open(QIODevice::ReadOnly));
        auto contents = read.readAll();

        QCOMPARE(takeLine(contents), QByteArray("20 1001"));
        QCOMPARE(takeLine(contents), QByteArray("Originator"));
        QCOMPARE(takeLine(contents), QByteArray("Organization"));
        QCOMPARE(takeLine(contents), QByteArray("Submitter"));
        QCOMPARE(takeLine(contents), QByteArray("Projects"));
        QCOMPARE(takeLine(contents), QByteArray("1 1"));
        QVERIFY(takeLine(contents).startsWith("1970 01 01 "));
        QCOMPARE(takeLine(contents), QByteArray("Interval"));
        QCOMPARE(takeLine(contents), QByteArray("days from file reference point"));
        QCOMPARE(takeLine(contents), QByteArray("3"));
        QCOMPARE(takeLine(contents), QByteArray("1 1 1"));
        QCOMPARE(takeLine(contents), QByteArray("9999.999999 9999.99 9.999999999"));
        QCOMPARE(takeLine(contents),
                 QByteArray("end_time of measurement, days from the file reference point"));
        QCOMPARE(takeLine(contents), QByteArray("var_title"));
        QCOMPARE(takeLine(contents), QByteArray("numflag"));
        QCOMPARE(takeLine(contents), QByteArray("0"));
        QCOMPARE(takeLine(contents), QByteArray("3"));
        QCOMPARE(takeLine(contents), QByteArray(R"(A key:       The Value)"));
        QCOMPARE(takeLine(contents), QByteArray(R"(Another key: "The: value")"));
        QCOMPARE(takeLine(contents), QByteArray("start_time end_time col_header numflag"));
        QCOMPARE(takeLine(contents), QByteArray("   0.010000    0.020000    2.00 0.000000000"));
    }

    void numflagSet()
    {
        EBAS ctx;
        Variant::Root config;

        QTemporaryFile outputFile;
        QVERIFY(outputFile.open());
        int fileResults = 0;
        ctx.createOutput = [&](const std::string &name) -> std::unique_ptr<QFile> {
            Q_ASSERT(name == "stuff");
            ++fileResults;
            std::unique_ptr<QFile> result(new QFile(outputFile.fileName()));
            if (!result->open(QIODevice::ReadWrite))
                return {};
            return std::move(result);
        };

        config["Types/Main/Filename"] = "stuff";
        baselineFile(config["Types/Main"]);
        config["Types/Main/Variables/One/Data/Input/#0/Variable"] = "v1";
        config["Types/Main/Variables/One/Header/Title/Full"] = "var_title";
        config["Types/Main/Variables/One/Header/Column"] = "col_header";
        config["Types/Main/Numflag/DataFlags/#0/Variable"] = "vf";
        ctx.config.emplace_back(FP::undefined(), FP::undefined(), config);

        auto stage = EBAS::stage(std::move(ctx));
        QVERIFY(stage.get() != nullptr);
        QVERIFY(!stage->isFinalStage());
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 864, 1728), Variant::Root(2.0)}});
        stage->processData({{SequenceIdentity({"s1", "a1", "vf"}, 864, 1728), Variant::Root(
                Variant::Flags{"EBASFlag123"})}});
        stage->processData(
                {{SequenceIdentity({"s1", "a1", "v1"}, 1728, 2592), Variant::Root(3.0)}});
        stage->finalizeData();

        stage = stage->getNextStage();
        QVERIFY(stage.get() != nullptr);
        QVERIFY(!stage->isFinalStage());
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 864, 1728), Variant::Root(2.0)}});
        stage->processData({{SequenceIdentity({"s1", "a1", "vf"}, 864, 1728), Variant::Root(
                Variant::Flags{"EBASFlag123"})}});
        stage->processData(
                {{SequenceIdentity({"s1", "a1", "v1"}, 1728, 2592), Variant::Root(3.0)}});
        stage->finalizeData();

        stage = stage->getNextStage();
        QVERIFY(stage.get() != nullptr);
        QVERIFY(stage->isFinalStage());
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 864, 1728), Variant::Root(2.0)}});
        stage->processData({{SequenceIdentity({"s1", "a1", "vf"}, 864, 1728), Variant::Root(
                Variant::Flags{"EBASFlag123"})}});
        stage->processData(
                {{SequenceIdentity({"s1", "a1", "v1"}, 1728, 2592), Variant::Root(3.0)}});
        stage->finalizeData();
        stage.reset();

        QCOMPARE(fileResults, 1);

        QFile read(outputFile.fileName());
        QVERIFY(read.open(QIODevice::ReadOnly));
        auto contents = read.readAll();

        QCOMPARE(takeLine(contents), QByteArray("18 1001"));
        QCOMPARE(takeLine(contents), QByteArray("Originator"));
        QCOMPARE(takeLine(contents), QByteArray("Organization"));
        QCOMPARE(takeLine(contents), QByteArray("Submitter"));
        QCOMPARE(takeLine(contents), QByteArray("Projects"));
        QCOMPARE(takeLine(contents), QByteArray("1 1"));
        QVERIFY(takeLine(contents).startsWith("1970 01 01 "));
        QCOMPARE(takeLine(contents), QByteArray("Interval"));
        QCOMPARE(takeLine(contents), QByteArray("days from file reference point"));
        QCOMPARE(takeLine(contents), QByteArray("3"));
        QCOMPARE(takeLine(contents), QByteArray("1 1 1"));
        QCOMPARE(takeLine(contents), QByteArray("9999.999999 9999.99 9.999999999"));
        QCOMPARE(takeLine(contents),
                 QByteArray("end_time of measurement, days from the file reference point"));
        QCOMPARE(takeLine(contents), QByteArray("var_title"));
        QCOMPARE(takeLine(contents), QByteArray("numflag"));
        QCOMPARE(takeLine(contents), QByteArray("0"));
        QCOMPARE(takeLine(contents), QByteArray("1"));
        QCOMPARE(takeLine(contents), QByteArray("start_time end_time col_header numflag"));
        QCOMPARE(takeLine(contents), QByteArray("   0.010000    0.020000    2.00 0.123000000"));
        QCOMPARE(takeLine(contents), QByteArray("   0.020000    0.030000    3.00 0.000000000"));
    }

    void dataFill()
    {
        EBAS ctx;
        Variant::Root config;

        QTemporaryFile outputFile;
        QVERIFY(outputFile.open());
        int fileResults = 0;
        ctx.createOutput = [&](const std::string &name) -> std::unique_ptr<QFile> {
            Q_ASSERT(name == "stuff");
            ++fileResults;
            std::unique_ptr<QFile> result(new QFile(outputFile.fileName()));
            if (!result->open(QIODevice::ReadWrite))
                return {};
            return std::move(result);
        };

        config["Types/Main/Filename"] = "stuff";
        baselineFile(config["Types/Main"]);
        config["Types/Main/Variables/One/Data/Input/#0/Variable"] = "v1";
        config["Types/Main/Variables/One/Header/Title/Full"] = "var_title";
        config["Types/Main/Variables/One/Header/Column"] = "col_header";
        config["Types/Main/Fill"] = 864.0;
        ctx.config.emplace_back(FP::undefined(), FP::undefined(), config);

        ctx.start = 0;
        ctx.end = 6048;

        auto stage = EBAS::stage(std::move(ctx));
        QVERIFY(stage.get() != nullptr);
        QVERIFY(!stage->isFinalStage());
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 864, 1728), Variant::Root(2.0)}});
        stage->processData(
                {{SequenceIdentity({"s1", "a1", "v1"}, 1728, 2592), Variant::Root(3.0)}});
        stage->processData(
                {{SequenceIdentity({"s1", "a1", "v1"}, 3456, 4320), Variant::Root(4.0)}});
        stage->finalizeData();

        stage = stage->getNextStage();
        QVERIFY(stage.get() != nullptr);
        QVERIFY(!stage->isFinalStage());
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 864, 1728), Variant::Root(2.0)}});
        stage->processData(
                {{SequenceIdentity({"s1", "a1", "v1"}, 1728, 2592), Variant::Root(3.0)}});
        stage->processData(
                {{SequenceIdentity({"s1", "a1", "v1"}, 3456, 4320), Variant::Root(4.0)}});
        stage->finalizeData();

        stage = stage->getNextStage();
        QVERIFY(stage.get() != nullptr);
        QVERIFY(stage->isFinalStage());
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 864, 1728), Variant::Root(2.0)}});
        stage->processData(
                {{SequenceIdentity({"s1", "a1", "v1"}, 1728, 2592), Variant::Root(3.0)}});
        stage->processData(
                {{SequenceIdentity({"s1", "a1", "v1"}, 3456, 4320), Variant::Root(4.0)}});
        stage->finalizeData();
        stage.reset();

        QCOMPARE(fileResults, 1);

        QFile read(outputFile.fileName());
        QVERIFY(read.open(QIODevice::ReadOnly));
        auto contents = read.readAll();

        QCOMPARE(takeLine(contents), QByteArray("18 1001"));
        QCOMPARE(takeLine(contents), QByteArray("Originator"));
        QCOMPARE(takeLine(contents), QByteArray("Organization"));
        QCOMPARE(takeLine(contents), QByteArray("Submitter"));
        QCOMPARE(takeLine(contents), QByteArray("Projects"));
        QCOMPARE(takeLine(contents), QByteArray("1 1"));
        QVERIFY(takeLine(contents).startsWith("1970 01 01 "));
        QCOMPARE(takeLine(contents), QByteArray("Interval"));
        QCOMPARE(takeLine(contents), QByteArray("days from file reference point"));
        QCOMPARE(takeLine(contents), QByteArray("3"));
        QCOMPARE(takeLine(contents), QByteArray("1 1 1"));
        QCOMPARE(takeLine(contents), QByteArray("9999.999999 9999.99 9.999999999"));
        QCOMPARE(takeLine(contents),
                 QByteArray("end_time of measurement, days from the file reference point"));
        QCOMPARE(takeLine(contents), QByteArray("var_title"));
        QCOMPARE(takeLine(contents), QByteArray("numflag"));
        QCOMPARE(takeLine(contents), QByteArray("0"));
        QCOMPARE(takeLine(contents), QByteArray("1"));
        QCOMPARE(takeLine(contents), QByteArray("start_time end_time col_header numflag"));
        QCOMPARE(takeLine(contents), QByteArray("   0.000000    0.010000 9999.99 0.999000000"));
        QCOMPARE(takeLine(contents), QByteArray("   0.010000    0.020000    2.00 0.000000000"));
        QCOMPARE(takeLine(contents), QByteArray("   0.020000    0.030000    3.00 0.000000000"));
        QCOMPARE(takeLine(contents), QByteArray("   0.030000    0.040000 9999.99 0.999000000"));
        QCOMPARE(takeLine(contents), QByteArray("   0.040000    0.050000    4.00 0.000000000"));
        QCOMPARE(takeLine(contents), QByteArray("   0.050000    0.060000 9999.99 0.999000000"));
        QCOMPARE(takeLine(contents), QByteArray("   0.060000    0.070000 9999.99 0.999000000"));
    }

    void dependencyCheck()
    {
        EBAS ctx;
        Variant::Root config;

        QTemporaryFile outputFile;
        QVERIFY(outputFile.open());
        int fileResults = 0;
        ctx.createOutput = [&](const std::string &name) -> std::unique_ptr<QFile> {
            Q_ASSERT(name == "stuff");
            ++fileResults;
            std::unique_ptr<QFile> result(new QFile(outputFile.fileName()));
            if (!result->open(QIODevice::ReadWrite))
                return {};
            return std::move(result);
        };

        config["Types/Main/Filename"] = "stuff";
        baselineFile(config["Types/Main"]);
        config["Types/Main/Variables/One/Data/Input/#0/Variable"] = "v1";
        config["Types/Main/Variables/One/Header/Title/Full"] = "var_title";
        config["Types/Main/Variables/One/Header/Column"] = "col_header";
        config["Types/Main/Variables/One/Dependencies/Provide"].setFlags({"DepFlag"});
        config["Types/Main/Dependencies/Require"].setFlags({"DepFlag"});

        config["Types/Alternate/Filename"] = "other";
        baselineFile(config["Types/Alternate"]);
        config["Types/Alternate/Variables/One/Data/Input/#0/Variable"] = "v2";
        config["Types/Alternate/Variables/One/Header/Title/Full"] = "var_title2";
        config["Types/Alternate/Variables/One/Header/Column"] = "col_header2";
        config["Types/Alternate/Variables/One/Dependencies/Provide"].setFlags({"MissingFlag"});
        config["Types/Alternate/Dependencies/Require"].setFlags({"MissingFlag"});
        ctx.config.emplace_back(FP::undefined(), FP::undefined(), config);

        auto stage = EBAS::stage(std::move(ctx));
        QVERIFY(stage.get() != nullptr);
        QVERIFY(!stage->isFinalStage());
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 864, 1728), Variant::Root(2.0)}});
        stage->finalizeData();

        stage = stage->getNextStage();
        QVERIFY(stage.get() != nullptr);
        QVERIFY(!stage->isFinalStage());
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 864, 1728), Variant::Root(2.0)}});
        stage->finalizeData();

        stage = stage->getNextStage();
        QVERIFY(stage.get() != nullptr);
        QVERIFY(stage->isFinalStage());
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 864, 1728), Variant::Root(2.0)}});
        stage->finalizeData();
        stage.reset();

        QCOMPARE(fileResults, 1);

        QFile read(outputFile.fileName());
        QVERIFY(read.open(QIODevice::ReadOnly));
        auto contents = read.readAll();

        QCOMPARE(takeLine(contents), QByteArray("18 1001"));
        QCOMPARE(takeLine(contents), QByteArray("Originator"));
        QCOMPARE(takeLine(contents), QByteArray("Organization"));
        QCOMPARE(takeLine(contents), QByteArray("Submitter"));
        QCOMPARE(takeLine(contents), QByteArray("Projects"));
        QCOMPARE(takeLine(contents), QByteArray("1 1"));
        QVERIFY(takeLine(contents).startsWith("1970 01 01 "));
        QCOMPARE(takeLine(contents), QByteArray("Interval"));
        QCOMPARE(takeLine(contents), QByteArray("days from file reference point"));
        QCOMPARE(takeLine(contents), QByteArray("3"));
        QCOMPARE(takeLine(contents), QByteArray("1 1 1"));
        QCOMPARE(takeLine(contents), QByteArray("9999.999999 9999.99 9.999999999"));
        QCOMPARE(takeLine(contents),
                 QByteArray("end_time of measurement, days from the file reference point"));
        QCOMPARE(takeLine(contents), QByteArray("var_title"));
        QCOMPARE(takeLine(contents), QByteArray("numflag"));
        QCOMPARE(takeLine(contents), QByteArray("0"));
        QCOMPARE(takeLine(contents), QByteArray("1"));
        QCOMPARE(takeLine(contents), QByteArray("start_time end_time col_header numflag"));
        QCOMPARE(takeLine(contents), QByteArray("   0.010000    0.020000    2.00 0.000000000"));
    }

    void substitutions()
    {
        EBAS ctx;
        Variant::Root config;

        QTemporaryFile outputFile;
        QVERIFY(outputFile.open());
        int fileResults = 0;
        ctx.createOutput = [&](const std::string &name) -> std::unique_ptr<QFile> {
            Q_ASSERT(name == "stuff");
            ++fileResults;
            std::unique_ptr<QFile> result(new QFile(outputFile.fileName()));
            if (!result->open(QIODevice::ReadWrite))
                return {};
            return std::move(result);
        };

        config["Types/Main/Substitutions/Filename/Constant"] = "stuff";
        config["Types/Main/Substitutions/SubOne"] = "SubFirstValue";
        config["Types/Main/Substitutions/SubTwo/Input/#0/Variable"] = "vsub";
        config["Types/Main/Substitutions/SubThree/Input/#0/Variable"] = "vsub";
        config["Types/Main/Substitutions/SubThree/Path"] = "/SubOther";
        config["Types/Main/Filename"] = "${Filename}";
        baselineFile(config["Types/Main"]);
        config["Types/Main/Headers/First/Name"] = "A${SUBONE}";
        config["Types/Main/Headers/First/Contents"] = "${SUBTWO|STR|/SubPath}";
        config["Types/Main/Headers/First/SortPriority"] = 0;
        config["Types/Main/Headers/Second/Name"] = "${SUBTHREE}";
        config["Types/Main/Headers/Second/Contents"] = "A value";
        config["Types/Main/Headers/Second/SortPriority"] = 1;
        config["Types/Main/Variables/One/Data/Input/#0/Variable"] = "v1";
        config["Types/Main/Variables/One/Header/Title/Full"] = "var_title ${FILENAME}";
        config["Types/Main/Variables/One/Header/Column"] = "col_${META|AUTO|/^MetaPath}";
        ctx.config.emplace_back(FP::undefined(), FP::undefined(), config);

        Variant::Root vsub;
        vsub["SubPath"] = "SubSecondValue";
        vsub["SubOther"] = "SubThirdValue";

        Variant::Root vmeta;
        vmeta["*dMetaPath"] = "zmeta";

        auto stage = EBAS::stage(std::move(ctx));
        QVERIFY(stage.get() != nullptr);
        QVERIFY(!stage->isFinalStage());
        stage->processData(
                {{SequenceIdentity({"s1", "a1", "vsub"}, FP::undefined(), FP::undefined()), vsub}});
        stage->processData({{SequenceIdentity({"s1", "a1_meta", "v1"}, FP::undefined(),
                                              FP::undefined()), vmeta}});
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 864, 1728), Variant::Root(2.0)}});
        stage->finalizeData();

        stage = stage->getNextStage();
        QVERIFY(stage.get() != nullptr);
        QVERIFY(!stage->isFinalStage());
        stage->processData(
                {{SequenceIdentity({"s1", "a1", "vsub"}, FP::undefined(), FP::undefined()), vsub}});
        stage->processData({{SequenceIdentity({"s1", "a1_meta", "v1"}, FP::undefined(),
                                              FP::undefined()), vmeta}});
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 864, 1728), Variant::Root(2.0)}});
        stage->finalizeData();

        stage = stage->getNextStage();
        QVERIFY(stage.get() != nullptr);
        QVERIFY(stage->isFinalStage());
        stage->processData(
                {{SequenceIdentity({"s1", "a1", "vsub"}, FP::undefined(), FP::undefined()), vsub}});
        stage->processData({{SequenceIdentity({"s1", "a1_meta", "v1"}, FP::undefined(),
                                              FP::undefined()), vmeta}});
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 864, 1728), Variant::Root(2.0)}});
        stage->finalizeData();
        stage.reset();

        QCOMPARE(fileResults, 1);

        QFile read(outputFile.fileName());
        QVERIFY(read.open(QIODevice::ReadOnly));
        auto contents = read.readAll();

        QCOMPARE(takeLine(contents), QByteArray("20 1001"));
        QCOMPARE(takeLine(contents), QByteArray("Originator"));
        QCOMPARE(takeLine(contents), QByteArray("Organization"));
        QCOMPARE(takeLine(contents), QByteArray("Submitter"));
        QCOMPARE(takeLine(contents), QByteArray("Projects"));
        QCOMPARE(takeLine(contents), QByteArray("1 1"));
        QVERIFY(takeLine(contents).startsWith("1970 01 01 "));
        QCOMPARE(takeLine(contents), QByteArray("Interval"));
        QCOMPARE(takeLine(contents), QByteArray("days from file reference point"));
        QCOMPARE(takeLine(contents), QByteArray("3"));
        QCOMPARE(takeLine(contents), QByteArray("1 1 1"));
        QCOMPARE(takeLine(contents), QByteArray("9999.999999 9999.99 9.999999999"));
        QCOMPARE(takeLine(contents),
                 QByteArray("end_time of measurement, days from the file reference point"));
        QCOMPARE(takeLine(contents), QByteArray("var_title stuff"));
        QCOMPARE(takeLine(contents), QByteArray("numflag"));
        QCOMPARE(takeLine(contents), QByteArray("0"));
        QCOMPARE(takeLine(contents), QByteArray("3"));
        QCOMPARE(takeLine(contents), QByteArray("ASubFirstValue: SubSecondValue"));
        QCOMPARE(takeLine(contents), QByteArray("SubThirdValue:  A value"));
        QCOMPARE(takeLine(contents), QByteArray("start_time end_time col_zmeta numflag"));
        QCOMPARE(takeLine(contents), QByteArray("   0.010000    0.020000    2.00 0.000000000"));
    }
};

QTEST_MAIN(TestEBAS)

#include "ebas.moc"
