/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QTest>

#include "fileoutput/fragment.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"

using namespace CPD3;
using namespace CPD3::Output;
using namespace CPD3::Data;

class TestValue : public FragmentTracker::Value {
public:
    int mask;
    int overlayed;
    int merged;

    virtual ~TestValue() = default;

    TestValue() : mask(0), overlayed(0), merged(0)
    { }

    explicit TestValue(int mask) : mask(mask), overlayed(mask), merged(mask)
    { }

    TestValue(const TestValue &) = default;

    FragmentTracker::ValueHandle overlay(const FragmentTracker::ValueHandle &under,
                                         const FragmentTracker::ValueHandle &over) override
    {
        auto result = std::make_shared<TestValue>(*std::static_pointer_cast<TestValue>(under));
        result->overlayed |= std::static_pointer_cast<TestValue>(over)->overlayed;
        return result;
    }

    void merge(Value *other) override
    {
        merged |= overlayed;
        merged |= static_cast<TestValue *>(other)->overlayed;
    }
};

class TestOutputFragment : public QObject {
Q_OBJECT

private slots:

    void single()
    {
        FragmentTracker ft;
        auto k1 = ft.allocateKey();
        ft.add(k1, std::make_shared<TestValue>(0x01), 100, 200);

        ft.merge([](const FragmentTracker::Key &key,
                    FragmentTracker::Value *first,
                    FragmentTracker::Value *second,
                    const FragmentTracker::MergeTestContext &context) { return false; });

        auto r = ft.getFragments();

        QCOMPARE((int) r.size(), 1);
        QCOMPARE(r[0].getStart(), 100.0);
        QCOMPARE(r[0].getEnd(), 200.0);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[0].contents[k1])->mask, 0x01);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[0].contents[k1])->merged, 0x01);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[0].contents[k1])->overlayed, 0x01);

        auto rf = ft.getContents();
        QCOMPARE((int) rf.size(), 1);
        QCOMPARE(std::static_pointer_cast<TestValue>(rf[0][k1])->mask, 0x01);
        QCOMPARE(std::static_pointer_cast<TestValue>(rf[0][k1])->merged, 0x01);
        QCOMPARE(std::static_pointer_cast<TestValue>(rf[0][k1])->overlayed, 0x01);
    }

    void overlap()
    {
        FragmentTracker ft;
        auto k1 = ft.allocateKey();
        ft.add(k1, std::make_shared<TestValue>(0x01), 100, 200);
        ft.add(k1, std::make_shared<TestValue>(0x02), 150, 300);

        ft.merge([](const FragmentTracker::Key &key,
                    FragmentTracker::Value *first,
                    FragmentTracker::Value *second,
                    const FragmentTracker::MergeTestContext &context) { return false; });

        auto r = ft.getFragments();

        QCOMPARE((int) r.size(), 3);
        QCOMPARE(r[0].getStart(), 100.0);
        QCOMPARE(r[0].getEnd(), 150.0);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[0].contents[k1])->mask, 0x01);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[0].contents[k1])->merged, 0x01);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[0].contents[k1])->overlayed, 0x01);
        QCOMPARE(r[1].getStart(), 150.0);
        QCOMPARE(r[1].getEnd(), 200.0);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[1].contents[k1])->overlayed, 0x03);
        QCOMPARE(r[2].getStart(), 200.0);
        QCOMPARE(r[2].getEnd(), 300.0);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[2].contents[k1])->mask, 0x02);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[2].contents[k1])->merged, 0x02);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[2].contents[k1])->overlayed, 0x02);

        auto rf = ft.getContents();
        QCOMPARE((int) rf.size(), 3);
        QCOMPARE(std::static_pointer_cast<TestValue>(rf[0][k1])->mask, 0x01);
        QCOMPARE(std::static_pointer_cast<TestValue>(rf[0][k1])->merged, 0x01);
        QCOMPARE(std::static_pointer_cast<TestValue>(rf[0][k1])->overlayed, 0x01);
        QCOMPARE(std::static_pointer_cast<TestValue>(rf[1][k1])->overlayed, 0x03);
        QCOMPARE(std::static_pointer_cast<TestValue>(rf[2][k1])->mask, 0x02);
        QCOMPARE(std::static_pointer_cast<TestValue>(rf[2][k1])->merged, 0x02);
        QCOMPARE(std::static_pointer_cast<TestValue>(rf[2][k1])->overlayed, 0x02);
    }

    void merge()
    {
        FragmentTracker ft;
        auto k1 = ft.allocateKey();
        ft.add(k1, std::make_shared<TestValue>(0x01), 100, 200);
        ft.add(k1, std::make_shared<TestValue>(0x02), 150, 300);

        ft.merge([](const FragmentTracker::Key &key,
                    FragmentTracker::Value *first,
                    FragmentTracker::Value *second,
                    const FragmentTracker::MergeTestContext &context) {
            Q_ASSERT(first);
            Q_ASSERT(second);
            return (static_cast<TestValue *>(first)->overlayed & 0x1) != 0 &&
                    (static_cast<TestValue *>(second)->overlayed & 0x1) != 0;
        });

        auto r = ft.getFragments();

        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].getStart(), 100.0);
        QCOMPARE(r[0].getEnd(), 200.0);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[0].contents[k1])->merged, 0x03);
        QCOMPARE(r[1].getStart(), 200.0);
        QCOMPARE(r[1].getEnd(), 300.0);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[1].contents[k1])->mask, 0x02);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[1].contents[k1])->merged, 0x02);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[1].contents[k1])->overlayed, 0x02);
    }

    void extend()
    {
        FragmentTracker ft;
        auto k1 = ft.allocateKey();
        ft.addEmpty();
        ft.add(k1, std::make_shared<TestValue>(0x01), 100, 200);
        ft.add(k1, std::make_shared<TestValue>(0x02), 250, 300);
        ft.add(k1, std::make_shared<TestValue>(0x10), 300, 400);

        ft.extend(k1,
                  [](FragmentTracker::Value *check, const FragmentTracker::ExtendTestContext &) {
                      Q_ASSERT(check);
                      return (static_cast<TestValue *>(check)->mask & 0xF) != 0;
                  });

        auto r = ft.getFragments();

        QCOMPARE((int) r.size(), 6);
        QVERIFY(!FP::defined(r[0].getStart()));
        QCOMPARE(r[0].getEnd(), 100.0);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[0].contents[k1])->mask, 0x01);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[0].contents[k1])->merged, 0x01);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[0].contents[k1])->overlayed, 0x01);
        QCOMPARE(r[1].getStart(), 100.0);
        QCOMPARE(r[1].getEnd(), 200.0);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[1].contents[k1])->mask, 0x01);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[1].contents[k1])->merged, 0x01);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[1].contents[k1])->overlayed, 0x01);
        QCOMPARE(r[2].getStart(), 200.0);
        QCOMPARE(r[2].getEnd(), 250.0);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[2].contents[k1])->mask, 0x01);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[2].contents[k1])->merged, 0x01);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[2].contents[k1])->overlayed, 0x01);
        QCOMPARE(r[3].getStart(), 250.0);
        QCOMPARE(r[3].getEnd(), 300.0);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[3].contents[k1])->mask, 0x02);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[3].contents[k1])->merged, 0x02);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[3].contents[k1])->overlayed, 0x02);
        QCOMPARE(r[4].getStart(), 300.0);
        QCOMPARE(r[4].getEnd(), 400.0);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[4].contents[k1])->mask, 0x02);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[4].contents[k1])->merged, 0x02);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[4].contents[k1])->overlayed, 0x02);
        QCOMPARE(r[5].getStart(), 400.0);
        QVERIFY(!FP::defined(r[5].getEnd()));
        QCOMPARE(std::static_pointer_cast<TestValue>(r[5].contents[k1])->mask, 0x02);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[5].contents[k1])->merged, 0x02);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[5].contents[k1])->overlayed, 0x02);
    }

    void multipleKeys()
    {
        FragmentTracker ft;
        auto k1 = ft.allocateKey();
        ft.add(k1, std::make_shared<TestValue>(0x01), 100, 200);
        ft.add(k1, std::make_shared<TestValue>(0x02), 150, 300);
        auto k2 = ft.allocateKey();
        ft.add(k2, std::make_shared<TestValue>(0x04), 150, 300);
        auto k3 = ft.allocateKey();
        ft.add(k3, std::make_shared<TestValue>(0x08), 200, 300);

        ft.merge([&](const FragmentTracker::Key &key,
                     FragmentTracker::Value *first,
                     FragmentTracker::Value *second,
                     const FragmentTracker::MergeTestContext &context) {
            if (key != k1)
                return true;
            Q_ASSERT(first);
            Q_ASSERT(second);
            return (static_cast<TestValue *>(first)->overlayed & 0x1) != 0 &&
                    (static_cast<TestValue *>(second)->overlayed & 0x1) != 0;
        });

        auto r = ft.getFragments();

        QCOMPARE((int) r.size(), 2);
        QCOMPARE(r[0].getStart(), 100.0);
        QCOMPARE(r[0].getEnd(), 200.0);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[0].contents[k1])->merged, 0x03);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[0].contents[k2])->mask, 0x04);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[0].contents[k2])->merged, 0x04);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[0].contents[k2])->overlayed, 0x04);
        QVERIFY(r[0].contents.count(k3) == 0);
        QCOMPARE(r[1].getStart(), 200.0);
        QCOMPARE(r[1].getEnd(), 300.0);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[1].contents[k1])->mask, 0x02);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[1].contents[k1])->merged, 0x02);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[1].contents[k1])->overlayed, 0x02);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[1].contents[k2])->mask, 0x04);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[1].contents[k2])->merged, 0x04);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[1].contents[k2])->overlayed, 0x04);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[1].contents[k3])->mask, 0x08);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[1].contents[k3])->merged, 0x08);
        QCOMPARE(std::static_pointer_cast<TestValue>(r[1].contents[k3])->overlayed, 0x08);
    }

};

QTEST_MAIN(TestOutputFragment)

#include "fragment.moc"
