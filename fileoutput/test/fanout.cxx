/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QTest>

#include "fileoutput/fanout.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "luascript/engine.hxx"
#include "luascript/libs/sequencename.hxx"

using namespace CPD3;
using namespace CPD3::Output;
using namespace CPD3::Data;

class TestFanout : public StagedFanout {
public:
    std::unique_ptr<Lua::Engine> scriptEngine;

    explicit TestFanout(const ValueSegment::Transfer &config,
                        bool enableDataInput = false,
                        bool enableMetadataInput = true) : StagedFanout(config, enableDataInput,
                                                                        enableMetadataInput),
                                                           scriptEngine(new Lua::Engine)
    { }

    virtual ~TestFanout() = default;

    class TestInput : public InputStage {
    public:
        SequenceSegment::Transfer data;
        SequenceSegment::Transfer metadata;

        explicit TestInput(TestFanout &parent) : InputStage(parent)
        { }

        virtual ~TestInput() = default;

        void processDataSegment(SequenceSegment &segment) override
        { data.emplace_back(segment); }

        void processMetadataSegment(SequenceSegment &segment) override
        { metadata.emplace_back(segment); }
    };

protected:
    std::unique_ptr<InputStage> createInput() override
    { return std::unique_ptr<InputStage>(new TestInput(*this)); }

    std::unique_ptr<Lua::Engine::Frame> scriptFrame(const SequenceName &target) const override
    {
        std::unique_ptr<Lua::Engine::Frame> root(new Lua::Engine::Frame(*scriptEngine));
        {
            Lua::Engine::Assign assign(*root, root->global(), "TARGET");
            assign.pushData<Lua::Libs::SequenceName>(target);
        }
        return std::move(root);
    }
};

class TestOutputFanout : public QObject {
Q_OBJECT

    static void send(const std::vector<StreamSink *> &sinks, const SequenceValue &value)
    {
        for (auto sink : sinks) {
            sink->incomingData(value);
        }
    }

    static void end(const std::vector<StreamSink *> &sinks)
    {
        for (auto sink : sinks) {
            sink->endData();
        }
    }

private slots:

    void basic()
    {
        Variant::Root config;
        config["Input/#0/Variable"] = "BsG_S11";
        TestFanout fanout({ValueSegment(config)});

        auto t1 = fanout.getTargets({"bnd", "raw", "BsG_S11"});
        QVERIFY(t1.empty());
        auto t2 = fanout.getTargets({"bnd", "raw_meta", "BsG_S11"});
        QVERIFY(!t2.empty());
        send(t2, {SequenceIdentity({"bnd", "raw_meta", "BsG_S11"}, 100, 200), Variant::Root(2.0)});
        QVERIFY(fanout.getTargets({"bnd", "raw_meta", "BsB_S11"}).empty());
        end(t1);
        end(t2);

        auto inputs = fanout.getInputs();
        QCOMPARE((int) inputs.size(), 1);
        QCOMPARE(inputs.front()->get(FP::undefined(), FP::undefined()),
                 (SequenceName::Set{{"bnd", "raw", "BsG_S11"}}));
        QVERIFY(inputs.front()->isActive(100, 200));
        QCOMPARE((int) static_cast<TestFanout::TestInput *>(inputs.front())->data.size(), 0);
        QCOMPARE((int) static_cast<TestFanout::TestInput *>(inputs.front())->metadata.size(), 1);
        QCOMPARE(
                static_cast<TestFanout::TestInput *>(inputs.front())->metadata.front()[SequenceName(
                        "bnd", "raw_meta", "BsG_S11")].toReal(), 2.0);

        StagedFanout::OutputStage output(*inputs.front());
        QVERIFY(output.isPossiblyUsed({"bnd", "raw", "BsG_S11"}));
        QVERIFY(output.isDirectlyUsed({"bnd", "raw", "BsG_S11"}));
        QVERIFY(output.isPossiblyUsed({"bnd", "raw_meta", "BsG_S11"}));
        QVERIFY(!output.isDirectlyUsed({"bnd", "raw_meta", "BsG_S11"}));
        QVERIFY(!output.isPossiblyUsed({"bnd", "raw", "BsR_S11"}));
        QVERIFY(!output.isDirectlyUsed({"bnd", "raw", "BsR_S11"}));
        QCOMPARE(output.get(FP::undefined(), FP::undefined()),
                 (SequenceName::Set{{"bnd", "raw", "BsG_S11"}}));
    }

    void split()
    {
        Variant::Root config;
        config["Input/#0/Variable"] = "Bs[GR]_S11";
        TestFanout fanout({ValueSegment(config)}, true, false);

        auto t1 = fanout.getTargets({"bnd", "raw", "BsG_S11"});
        QVERIFY(!t1.empty());
        auto t2 = fanout.getTargets({"bnd", "raw", "BsR_S11"});
        QVERIFY(!t2.empty());
        QVERIFY(fanout.getTargets({"bnd", "raw_meta", "BsG_S11"}).empty());
        QVERIFY(fanout.getTargets({"bnd", "raw", "BsB_S11"}).empty());
        send(t1, {SequenceIdentity({"bnd", "raw", "BsG_S11"}, 100, 200), Variant::Root(1.0)});
        send(t2, {SequenceIdentity({"bnd", "raw", "BsR_S11"}, 100, 200), Variant::Root(2.0)});
        end(t1);
        end(t2);

        auto inputs = fanout.getInputs();
        QCOMPARE((int) inputs.size(), 2);
        TestFanout::TestInput *BsG_S11;
        TestFanout::TestInput *BsR_S11;
        if (inputs.front()
                  ->get(FP::undefined(), FP::undefined())
                  .count(SequenceName("bnd", "raw", "BsG_S11")) != 0) {
            BsG_S11 = static_cast<TestFanout::TestInput *>(inputs.front());
            BsR_S11 = static_cast<TestFanout::TestInput *>(inputs.back());
        } else {
            BsR_S11 = static_cast<TestFanout::TestInput *>(inputs.front());
            BsG_S11 = static_cast<TestFanout::TestInput *>(inputs.back());
        }

        QCOMPARE(BsG_S11->get(FP::undefined(), FP::undefined()),
                 (SequenceName::Set{{"bnd", "raw", "BsG_S11"}}));
        QCOMPARE(BsR_S11->get(FP::undefined(), FP::undefined()),
                 (SequenceName::Set{{"bnd", "raw", "BsR_S11"}}));
        QVERIFY(BsG_S11->isActive(100, 200));
        QVERIFY(BsR_S11->isActive(100, 200));
        QCOMPARE((int) BsG_S11->data.size(), 1);
        QCOMPARE((int) BsG_S11->metadata.size(), 0);
        QCOMPARE((int) BsR_S11->data.size(), 1);
        QCOMPARE((int) BsR_S11->metadata.size(), 0);
        QCOMPARE(BsG_S11->data.front()[SequenceName("bnd", "raw", "BsG_S11")].toReal(), 1.0);
        QCOMPARE(BsR_S11->data.front()[SequenceName("bnd", "raw", "BsR_S11")].toReal(), 2.0);

        {
            StagedFanout::OutputStage output(*BsG_S11);
            QCOMPARE(output.get(FP::undefined(), FP::undefined()),
                     (SequenceName::Set{{"bnd", "raw", "BsG_S11"}}));
            QVERIFY(output.isPossiblyUsed({"bnd", "raw", "BsG_S11"}));
            QVERIFY(!output.isPossiblyUsed({"bnd", "raw", "BsR_S11"}));
        }
        {
            StagedFanout::OutputStage output(*BsR_S11);
            QCOMPARE(output.get(FP::undefined(), FP::undefined()),
                     (SequenceName::Set{{"bnd", "raw", "BsR_S11"}}));
            QVERIFY(!output.isPossiblyUsed({"bnd", "raw", "BsG_S11"}));
            QVERIFY(output.isPossiblyUsed({"bnd", "raw", "BsR_S11"}));
        }
    }

    void overrideScript()
    {
        Variant::Root config;
        config["Input/#0/Variable"] = "Bs[GR]_S1[12]";
        config["Fanout/RemapScript"] =
                R"(TARGET.variable = string.gsub(TARGET.variable, "_S12$", "_S11"); return TARGET;)";
        TestFanout fanout({ValueSegment(config)}, true, false);

        auto t1 = fanout.getTargets({"bnd", "raw", "BsG_S11"});
        QVERIFY(!t1.empty());
        auto t2 = fanout.getTargets({"bnd", "raw", "BsR_S11"});
        QVERIFY(!t2.empty());
        auto t3 = fanout.getTargets({"bnd", "raw", "BsR_S12"});
        QVERIFY(!t2.empty());
        QVERIFY(fanout.getTargets({"bnd", "raw", "BsB_S11"}).empty());
        QVERIFY(fanout.getTargets({"bnd", "raw", "BsB_S12"}).empty());
        send(t1, {SequenceIdentity({"bnd", "raw", "BsG_S11"}, 100, 200), Variant::Root(1.0)});
        send(t2, {SequenceIdentity({"bnd", "raw", "BsR_S11"}, 100, 200), Variant::Root(2.0)});
        send(t3, {SequenceIdentity({"bnd", "raw", "BsR_S12"}, 200, 300), Variant::Root(3.0)});
        end(t1);
        end(t2);
        end(t3);

        auto inputs = fanout.getInputs();
        QCOMPARE((int) inputs.size(), 2);
        TestFanout::TestInput *BsG_S11;
        TestFanout::TestInput *BsR_S11;
        if (inputs.front()
                  ->get(FP::undefined(), FP::undefined())
                  .count(SequenceName("bnd", "raw", "BsG_S11")) != 0) {
            BsG_S11 = static_cast<TestFanout::TestInput *>(inputs.front());
            BsR_S11 = static_cast<TestFanout::TestInput *>(inputs.back());
        } else {
            BsR_S11 = static_cast<TestFanout::TestInput *>(inputs.front());
            BsG_S11 = static_cast<TestFanout::TestInput *>(inputs.back());
        }

        QCOMPARE(BsG_S11->get(FP::undefined(), FP::undefined()),
                 (SequenceName::Set{{"bnd", "raw", "BsG_S11"}}));
        QCOMPARE(BsR_S11->get(FP::undefined(), FP::undefined()),
                 (SequenceName::Set{{"bnd", "raw", "BsR_S11"},
                                    {"bnd", "raw", "BsR_S12"}}));
        QVERIFY(BsG_S11->isActive(100, 200));
        QVERIFY(BsR_S11->isActive(100, 200));
        QCOMPARE((int) BsG_S11->data.size(), 2);
        QCOMPARE((int) BsG_S11->metadata.size(), 0);
        QCOMPARE((int) BsR_S11->data.size(), 2);
        QCOMPARE((int) BsR_S11->metadata.size(), 0);
        QCOMPARE(BsG_S11->data.front()[SequenceName("bnd", "raw", "BsG_S11")].toReal(), 1.0);
        QVERIFY(!FP::defined(BsG_S11->data.back()[SequenceName("bnd", "raw", "BsG_S11")].toReal()));
        QCOMPARE(BsR_S11->data.front()[SequenceName("bnd", "raw", "BsR_S11")].toReal(), 2.0);
        QCOMPARE(BsR_S11->data.back()[SequenceName("bnd", "raw", "BsR_S12")].toReal(), 3.0);

        {
            StagedFanout::OutputStage output(*BsG_S11);
            QCOMPARE(output.get(FP::undefined(), FP::undefined()),
                     (SequenceName::Set{{"bnd", "raw", "BsG_S11"}}));
            QVERIFY(output.isPossiblyUsed({"bnd", "raw", "BsG_S11"}));
            QVERIFY(!output.isPossiblyUsed({"bnd", "raw", "BsR_S11"}));
            QVERIFY(!output.isPossiblyUsed({"bnd", "raw", "BsR_S12"}));
        }
        {
            StagedFanout::OutputStage output(*BsR_S11);
            QCOMPARE(output.get(FP::undefined(), FP::undefined()),
                     (SequenceName::Set{{"bnd", "raw", "BsR_S11"},
                                        {"bnd", "raw", "BsR_S12"}}));
            QVERIFY(!output.isPossiblyUsed({"bnd", "raw", "BsG_S11"}));
            QVERIFY(!output.isPossiblyUsed({"bnd", "raw", "BsG_S12"}));
            QVERIFY(output.isPossiblyUsed({"bnd", "raw", "BsR_S11"}));
            QVERIFY(output.isPossiblyUsed({"bnd", "raw", "BsR_S12"}));
        }
    }
};

QTEST_MAIN(TestOutputFanout)

#include "fanout.moc"
