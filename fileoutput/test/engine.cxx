/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QTest>

#include "fileoutput/engine.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"

using namespace CPD3;
using namespace CPD3::Output;
using namespace CPD3::Data;

struct StageResult {
    bool receivedFinalize;
    SequenceValue::Transfer result;

    StageResult() : receivedFinalize(false)
    { }
};

class TestStage : public Engine::Stage {
    std::shared_ptr<StageResult> result;
public:
    std::unique_ptr<Stage> next;

    explicit TestStage(std::shared_ptr<StageResult> result) : result(std::move(result))
    { }

    virtual ~TestStage() = default;

    void processData(SequenceValue::Transfer &&values) override
    {
        Util::append(std::move(values), result->result);
    }

    void finalizeData() override
    {
        result->receivedFinalize = true;
    }

    bool isFinalStage() const override
    { return next.get() == nullptr; }

    std::unique_ptr<Stage> getNextStage() override
    { return std::move(next); }
};

class TestEngine : public Engine {
    bool firstStageReady;
    std::mutex firstStageMutex;
    std::condition_variable firstStageNotify;
public:
    bool aborting;

    explicit TestEngine(std::unique_ptr<Engine::Stage> &&stage) : Engine(std::move(stage)),
                                                                  firstStageReady(false),
                                                                  aborting(false)
    { }

    virtual ~TestEngine() = default;

    void incomingData(const SequenceValue::Transfer &values)
    { getDataSink()->incomingData(values); }

    void endData()
    { getDataSink()->endData(); }

    void waitForReady() {
        std::unique_lock<std::mutex> lock(firstStageMutex);
        for (;;) {
            if (firstStageReady)
                return;
            firstStageNotify.wait(lock);
        }
    }

    bool firstStageActive() {
        std::lock_guard<std::mutex> lock(firstStageMutex);
        return firstStageReady;
    }

protected:
    bool startFirstStage(Stage *) override
    {
        {
            std::lock_guard<std::mutex> lock(firstStageMutex);
            Q_ASSERT(!firstStageReady);
            firstStageReady = true;
        }
        firstStageNotify.notify_all();
        return true;
    }

    void firstStageComplete(Stage *) override
    {
        {
            std::lock_guard<std::mutex> lock(firstStageMutex);
            Q_ASSERT(firstStageReady);
            firstStageReady = false;
        }
    }

    void firstStageAborted(Stage *) override
    {
        Q_ASSERT(aborting);
    }
};

namespace CPD3 {
namespace Data {
bool operator==(const SequenceValue &a, const SequenceValue &b)
{ return SequenceValue::ValueEqual()(a, b); }
}
}

class TestOutputEngine : public QObject {
Q_OBJECT

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();
    }

    void singleStage()
    {
        auto r1 = std::make_shared<StageResult>();
        auto s1 = std::unique_ptr<TestStage>(new TestStage(r1));

        TestEngine engine(std::move(s1));
        engine.start();
        engine.waitForReady();

        SequenceValue::Transfer values;
        values.emplace_back(SequenceIdentity("bnd", "raw", "BsG_S11", {}, 100, 200),
                            Variant::Root(1.0));
        values.emplace_back(SequenceIdentity("bnd", "raw", "BsB_S11", {}, 200, 300),
                            Variant::Root(2.0));
        values.emplace_back(SequenceIdentity("bnd", "raw", "BsR_S11", {}, 200, 300),
                            Variant::Root(3.0));

        engine.incomingData(values);
        engine.endData();

        engine.wait();
        QVERIFY(!engine.firstStageActive());

        QVERIFY(r1->receivedFinalize);
        QCOMPARE(r1->result, values);
    }

    void twoStage()
    {
        auto r2 = std::make_shared<StageResult>();
        auto s2 = std::unique_ptr<TestStage>(new TestStage(r2));
        auto r1 = std::make_shared<StageResult>();
        auto s1 = std::unique_ptr<TestStage>(new TestStage(r1));
        s1->next = std::move(s2);

        TestEngine engine(std::move(s1));
        engine.start();
        engine.waitForReady();

        SequenceValue::Transfer values;
        values.emplace_back(SequenceIdentity("bnd", "raw", "BsG_S11", {}, 100, 200),
                            Variant::Root(1.0));
        values.emplace_back(SequenceIdentity("bnd", "raw", "BsB_S11", {}, 200, 300),
                            Variant::Root(2.0));
        values.emplace_back(SequenceIdentity("bnd", "raw", "BsR_S11", {}, 200, 300),
                            Variant::Root(3.0));

        engine.incomingData(values);
        engine.endData();

        engine.wait();
        QVERIFY(!engine.firstStageActive());

        QVERIFY(r1->receivedFinalize);
        QCOMPARE(r1->result, values);
        QVERIFY(r2->receivedFinalize);
        QCOMPARE(r2->result, values);
    }

    void threeStage()
    {
        auto r3 = std::make_shared<StageResult>();
        auto s3 = std::unique_ptr<TestStage>(new TestStage(r3));
        auto r2 = std::make_shared<StageResult>();
        auto s2 = std::unique_ptr<TestStage>(new TestStage(r2));
        s2->next = std::move(s3);
        auto r1 = std::make_shared<StageResult>();
        auto s1 = std::unique_ptr<TestStage>(new TestStage(r1));
        s1->next = std::move(s2);

        TestEngine engine(std::move(s1));
        engine.start();
        engine.waitForReady();

        SequenceValue::Transfer values;
        values.emplace_back(SequenceIdentity("bnd", "raw", "BsG_S11", {}, 100, 200),
                            Variant::Root(1.0));
        values.emplace_back(SequenceIdentity("bnd", "raw", "BsB_S11", {}, 200, 300),
                            Variant::Root(2.0));
        values.emplace_back(SequenceIdentity("bnd", "raw", "BsR_S11", {}, 200, 300),
                            Variant::Root(3.0));

        engine.incomingData(values);
        engine.endData();

        engine.wait();
        QVERIFY(!engine.firstStageActive());

        QVERIFY(r1->receivedFinalize);
        QCOMPARE(r1->result, values);
        QVERIFY(r2->receivedFinalize);
        QCOMPARE(r2->result, values);
        QVERIFY(r3->receivedFinalize);
        QCOMPARE(r3->result, values);
    }

    void stall()
    {
        auto r3 = std::make_shared<StageResult>();
        auto s3 = std::unique_ptr<TestStage>(new TestStage(r3));
        auto r2 = std::make_shared<StageResult>();
        auto s2 = std::unique_ptr<TestStage>(new TestStage(r2));
        s2->next = std::move(s3);
        auto r1 = std::make_shared<StageResult>();
        auto s1 = std::unique_ptr<TestStage>(new TestStage(r1));
        s1->next = std::move(s2);

        TestEngine engine(std::move(s1));
        engine.start();
        engine.waitForReady();

        SequenceValue::Transfer values;

        SequenceValue::Transfer add;
        for (size_t i=0; i<StreamSink::stallThreshold; i++) {
            add.emplace_back(SequenceIdentity("bnd", "raw", "BsG_S11", {}, 100, 200),
                                Variant::Root((double)i));
            add.emplace_back(SequenceIdentity("bnd", "raw", "BsB_S11", {}, 100, 300),
                                Variant::Root((double)i));
        }

        Util::append(add, values);
        Util::append(add, values);
        Util::append(add, values);
        engine.incomingData(add);
        engine.incomingData(add);
        engine.incomingData(add);

        engine.endData();

        engine.wait();
        QVERIFY(!engine.firstStageActive());

        QVERIFY(r1->receivedFinalize);
        QCOMPARE(r1->result, values);
        QVERIFY(r2->receivedFinalize);
        QCOMPARE(r2->result, values);
        QVERIFY(r3->receivedFinalize);
        QCOMPARE(r3->result, values);
    }

    void terminate()
    {
        auto r1 = std::make_shared<StageResult>();
        auto s1 = std::unique_ptr<TestStage>(new TestStage(r1));

        TestEngine engine(std::move(s1));
        engine.aborting = true;
        engine.start();
        engine.waitForReady();

        SequenceValue::Transfer values;
        values.emplace_back(SequenceIdentity("bnd", "raw", "BsG_S11", {}, 100, 200),
                            Variant::Root(1.0));
        values.emplace_back(SequenceIdentity("bnd", "raw", "BsB_S11", {}, 200, 300),
                            Variant::Root(2.0));
        values.emplace_back(SequenceIdentity("bnd", "raw", "BsR_S11", {}, 200, 300),
                            Variant::Root(3.0));

        engine.incomingData(values);
        engine.signalTerminate();

        QVERIFY(engine.wait(30000));
    }

};

QTEST_MAIN(TestOutputEngine)

#include "engine.moc"
