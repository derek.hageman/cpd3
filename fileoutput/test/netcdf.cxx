/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <netcdf.h>

#include <QtGlobal>
#include <QTest>
#include <QTemporaryFile>

#include "fileoutput/netcdf.hxx"
#include "core/number.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Output;
using namespace CPD3::Data;

class TestNetCDF : public QObject {
Q_OBJECT

    static QString getTextAttribute(const QString &name, int ncid, int varid = NC_GLOBAL)
    {
        size_t len = 0;
        if (::nc_inq_attlen(ncid, varid, name.toUtf8().constData(), &len) != NC_NOERR)
            return QString();
        QByteArray target((int) len + 1, 0);
        if (::nc_get_att_text(ncid, varid, name.toUtf8().constData(), target.data()) != NC_NOERR)
            return QString();
        return QString::fromUtf8(target);
    }

    static double getRealAttribute(const QString &name, int ncid, int varid = NC_GLOBAL)
    {
        size_t len = 0;
        if (::nc_inq_attlen(ncid, varid, name.toUtf8().constData(), &len) != NC_NOERR)
            return FP::undefined();
        if (len != 1)
            return FP::undefined();
        double target = FP::undefined();;
        if (::nc_get_att_double(ncid, varid, name.toUtf8().constData(), &target) != NC_NOERR)
            return FP::undefined();
        return target;
    }

    static qint64 getIntegerAttribute(const QString &name, int ncid, int varid = NC_GLOBAL)
    {
        size_t len = 0;
        if (::nc_inq_attlen(ncid, varid, name.toUtf8().constData(), &len) != NC_NOERR)
            return INTEGER::undefined();
        if (len != 1)
            return INTEGER::undefined();
        qint64 target = INTEGER::undefined();;
        if (::nc_get_att_longlong(ncid, varid, name.toUtf8().constData(), &target) != NC_NOERR)
            return INTEGER::undefined();
        return target;
    }

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();
    }

    void trivialFile()
    {
        NetCDF ctx;
        Variant::Root config;

        QTemporaryFile outputFile;
        QVERIFY(outputFile.open());
        int fileResults = 0;
        ctx.createOutput = [&](const std::string &name) -> std::unique_ptr<QFile> {
            Q_ASSERT(name == "stuff");
            ++fileResults;
            std::unique_ptr<QFile> result(new QFile(outputFile.fileName()));
            if (!result->open(QIODevice::ReadWrite))
                return {};
            return std::move(result);
        };

        config["Filename"] = "stuff";
        config["Attributes/A1/Name/Constant"] = "a1name";
        config["Attributes/A1/Value/Constant"] = "a1value";
        ctx.config.emplace_back(FP::undefined(), FP::undefined(), config);

        auto stage = NetCDF::stage(std::move(ctx));
        QVERIFY(stage.get() != nullptr);
        QVERIFY(!stage->isFinalStage());
        stage->finalizeData();

        stage = stage->getNextStage();
        QVERIFY(stage.get() != nullptr);
        QVERIFY(stage->isFinalStage());
        stage->finalizeData();
        stage.reset();

        QCOMPARE(fileResults, 1);

        int ncid = -1;
        QVERIFY(::nc_open(outputFile.fileName().toUtf8(), NC_NOWRITE, &ncid) == NC_NOERR);

        QCOMPARE(getTextAttribute("a1name", ncid), QString("a1value"));

        QVERIFY(::nc_close(ncid) == NC_NOERR);
    }

    void singleDataVariable()
    {
        NetCDF ctx;
        Variant::Root config;

        QTemporaryFile outputFile;
        QVERIFY(outputFile.open());
        int fileResults = 0;
        ctx.createOutput = [&](const std::string &name) -> std::unique_ptr<QFile> {
            Q_ASSERT(name == "stuff");
            ++fileResults;
            std::unique_ptr<QFile> result(new QFile(outputFile.fileName()));
            if (!result->open(QIODevice::ReadWrite))
                return {};
            return std::move(result);
        };

        config["Filename"] = "stuff";
        config["Dimensions/Time/DimTime/Name/Constant"] = "time";
        config["Dimensions/Time/DimTime/Coordinates/CoordTime/Name/Constant"] = "ctime";
        config["Dimensions/Time/DimTime/Coordinates/CoordTime/Attributes/A1/Name/Constant"] =
                "ca1name";
        config["Dimensions/Time/DimTime/Coordinates/CoordTime/Attributes/A1/Value/Constant"] =
                "ca1value";
        config["Variables/VarValue/Name/Constant"] = "value";
        config["Variables/VarValue/Data/Input/#0/Variable"] = "v1";
        config["Variables/VarValue/Time/Dimension"] = "DimTime";
        config["Variables/VarValue/Attributes/A1/Name/Constant"] = "a1name";
        config["Variables/VarValue/Attributes/A1/Value/Constant"] = "a1value";
        ctx.config.emplace_back(FP::undefined(), FP::undefined(), config);

        auto stage = NetCDF::stage(std::move(ctx));
        QVERIFY(stage.get() != nullptr);
        QVERIFY(!stage->isFinalStage());
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 100, 200), Variant::Root(1.0)}});
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 200, 300), Variant::Root(2.0)}});
        stage->finalizeData();

        stage = stage->getNextStage();
        QVERIFY(stage.get() != nullptr);
        QVERIFY(stage->isFinalStage());
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 100, 200), Variant::Root(1.0)}});
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 200, 300), Variant::Root(2.0)}});
        stage->finalizeData();
        stage.reset();

        QCOMPARE(fileResults, 1);

        int ncid = -1;
        QVERIFY(::nc_open(outputFile.fileName().toUtf8(), NC_NOWRITE, &ncid) == NC_NOERR);

        int dimid = -1;
        QVERIFY(::nc_inq_dimid(ncid, "time", &dimid) == NC_NOERR);
        ::size_t dimlen = 0;
        QVERIFY(::nc_inq_dimlen(ncid, dimid, &dimlen) == NC_NOERR);
        QCOMPARE((int) dimlen, 2);

        int varid = -1;
        QVERIFY(::nc_inq_varid(ncid, "value", &varid) == NC_NOERR);
        int ndims = 0;
        QVERIFY(::nc_inq_varndims(ncid, varid, &ndims) == NC_NOERR);
        QCOMPARE(ndims, 1);
        double varvalue = 0;
        dimlen = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 1.0);
        dimlen = 1;
        QVERIFY(::nc_get_var1_double(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 2.0);

        QCOMPARE(getTextAttribute("a1name", ncid, varid), QString("a1value"));


        QVERIFY(::nc_inq_varid(ncid, "ctime", &varid) == NC_NOERR);
        QVERIFY(::nc_inq_varndims(ncid, varid, &ndims) == NC_NOERR);
        QCOMPARE(ndims, 1);
        dimlen = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 100.0);
        dimlen = 1;
        QVERIFY(::nc_get_var1_double(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 200.0);

        QCOMPARE(getTextAttribute("ca1name", ncid, varid), QString("ca1value"));

        QVERIFY(::nc_close(ncid) == NC_NOERR);
    }

    void constantFanout()
    {
        NetCDF ctx;
        Variant::Root config;

        QTemporaryFile outputFile;
        QVERIFY(outputFile.open());
        int fileResults = 0;
        ctx.createOutput = [&](const std::string &name) -> std::unique_ptr<QFile> {
            Q_ASSERT(name == "stuff");
            ++fileResults;
            std::unique_ptr<QFile> result(new QFile(outputFile.fileName()));
            if (!result->open(QIODevice::ReadWrite))
                return {};
            return std::move(result);
        };

        config["Filename"] = "stuff";
        config["Dimensions/Time/DimTime/Name/Constant"] = "time";
        config["Dimensions/Time/DimTime/Unlimited"] = true;
        config["Dimensions/Real/DimCut/Name/Constant"] = "cut";
        config["Dimensions/Real/DimCut/Coordinates/CoordValue/Name/Constant"] = "ccut";
        config["Dimensions/Real/DimCut/Coordinates/CoordValue/Attributes/A1/Name/Constant"] =
                "ca1name";
        config["Dimensions/Real/DimCut/Coordinates/CoordValue/Attributes/A1/Value/Constant"] =
                "ca1value";
        config["Variables/VarValue/Name/Constant"] = "value";
        config["Variables/VarValue/Data/Input/#0/Variable"] = "v1";
        config["Variables/VarValue/Time/Dimension"] = "DimTime";
        config["Variables/VarValue/Dimensions/#0/Dimension"] = "DimCut";
        config["Variables/VarValue/Dimensions/#0/Values/#0/Match/HasFlavors"] = "pm10";
        config["Variables/VarValue/Dimensions/#0/Values/#0/Value"] = 10.0;
        config["Variables/VarValue/Dimensions/#0/Values/#1/Match/HasFlavors"] = "pm1";
        config["Variables/VarValue/Dimensions/#0/Values/#1/Value"] = 1.0;
        config["Variables/VarValue/Dimensions/#0/Values/#2/Value"] = FP::undefined();

        ctx.config.emplace_back(FP::undefined(), FP::undefined(), config);

        SequenceValue::Transfer values
                {{SequenceIdentity({"s1", "a1", "v1", {"pm1"}}, 100, 200),  Variant::Root(1.0)},
                 {SequenceIdentity({"s1", "a1", "v1", {"pm10"}}, 100, 200), Variant::Root(2.0)},
                 {SequenceIdentity({"s1", "a1", "v1"}, 200, 300),           Variant::Root(3.0)},
                 {SequenceIdentity({"s1", "a1", "v1", {"pm1"}}, 200, 300),  Variant::Root(4.0)},
                 {SequenceIdentity({"s1", "a1", "v1", {"pm10"}}, 200, 300), Variant::Root(5.0)},};

        auto stage = NetCDF::stage(std::move(ctx));
        QVERIFY(stage.get() != nullptr);
        QVERIFY(!stage->isFinalStage());
        stage->processData(SequenceValue::Transfer(values));
        stage->finalizeData();

        stage = stage->getNextStage();
        QVERIFY(stage.get() != nullptr);
        QVERIFY(stage->isFinalStage());
        stage->processData(std::move(values));
        stage->finalizeData();
        stage.reset();

        QCOMPARE(fileResults, 1);

        int ncid = -1;
        QVERIFY(::nc_open(outputFile.fileName().toUtf8(), NC_NOWRITE, &ncid) == NC_NOERR);

        int dimid = -1;
        QVERIFY(::nc_inq_dimid(ncid, "time", &dimid) == NC_NOERR);
        ::size_t dimlen = 0;
        QVERIFY(::nc_inq_dimlen(ncid, dimid, &dimlen) == NC_NOERR);
        QCOMPARE((int) dimlen, 2);

        QVERIFY(::nc_inq_dimid(ncid, "cut", &dimid) == NC_NOERR);
        QVERIFY(::nc_inq_dimlen(ncid, dimid, &dimlen) == NC_NOERR);
        QCOMPARE((int) dimlen, 3);

        int varid = -1;
        QVERIFY(::nc_inq_varid(ncid, "value", &varid) == NC_NOERR);
        int ndims = 0;
        QVERIFY(::nc_inq_varndims(ncid, varid, &ndims) == NC_NOERR);
        QCOMPARE(ndims, 2);

        double varvalue = 0;
        ::size_t dimindex[2];
        dimindex[0] = 0;
        dimindex[1] = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, dimindex, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 1.0);
        dimindex[0] = 1;
        dimindex[1] = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, dimindex, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 2.0);
        dimindex[0] = 2;
        dimindex[1] = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, dimindex, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, (double) NC_FILL_DOUBLE);
        dimindex[0] = 0;
        dimindex[1] = 1;
        QVERIFY(::nc_get_var1_double(ncid, varid, dimindex, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 4.0);
        dimindex[0] = 1;
        dimindex[1] = 1;
        QVERIFY(::nc_get_var1_double(ncid, varid, dimindex, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 5.0);
        dimindex[0] = 2;
        dimindex[1] = 1;
        QVERIFY(::nc_get_var1_double(ncid, varid, dimindex, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 3.0);


        QVERIFY(::nc_inq_varid(ncid, "ccut", &varid) == NC_NOERR);
        QVERIFY(::nc_inq_varndims(ncid, varid, &ndims) == NC_NOERR);
        QCOMPARE(ndims, 1);
        dimlen = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 1.0);
        dimlen = 1;
        QVERIFY(::nc_get_var1_double(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 10.0);
        dimlen = 2;
        QVERIFY(::nc_get_var1_double(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, (double) NC_FILL_DOUBLE);

        QCOMPARE(getTextAttribute("ca1name", ncid, varid), QString("ca1value"));

        QVERIFY(::nc_close(ncid) == NC_NOERR);
    }

    void metadataFanout()
    {
        NetCDF ctx;
        Variant::Root config;

        QTemporaryFile outputFile;
        QVERIFY(outputFile.open());
        int fileResults = 0;
        ctx.createOutput = [&](const std::string &name) -> std::unique_ptr<QFile> {
            Q_ASSERT(name == "stuff");
            ++fileResults;
            std::unique_ptr<QFile> result(new QFile(outputFile.fileName()));
            if (!result->open(QIODevice::ReadWrite))
                return {};
            return std::move(result);
        };

        config["Filename"] = "stuff";
        config["Dimensions/Time/DimTime/Name/Constant"] = "time";
        config["Dimensions/Real/DimWL/Name/Constant"] = "wavelength";
        config["Dimensions/Real/DimWL/Coordinates/CoordValue/Name/Constant"] = "cwavelength";
        config["Variables/VarValue/Name/Constant"] = "value";
        config["Variables/VarValue/Data/Input/#0/Variable"] = "v1";
        config["Variables/VarValue/Time/Dimension"] = "DimTime";
        config["Variables/VarValue/Dimensions/#0/Dimension"] = "DimWL";
        config["Variables/VarValue/Dimensions/#0/Values/#0/Match/HasFlavors"] = "pm10";
        config["Variables/VarValue/Dimensions/#0/Values/#0/Metadata/Path"] = "^Wavelength";
        config["Variables/VarValue/Dimensions/#0/Values/#0/Metadata/Calibration/#0"] = 10.0;
        config["Variables/VarValue/Dimensions/#0/Values/#0/Metadata/Calibration/#1"] = 1.0;
        config["Variables/VarValue/Dimensions/#0/Values/#1/Metadata/Path"] = "^Wavelength";

        ctx.config.emplace_back(FP::undefined(), FP::undefined(), config);

        Variant::Root metadata1;
        metadata1["*dWavelength"] = 100.0;
        Variant::Root metadata2;
        metadata2["*dWavelength"] = 200.0;

        SequenceValue::Transfer values
                {{SequenceIdentity({"s1", "a1_meta", "v1", {"pm1"}}, 100, 300),  metadata1},
                 {SequenceIdentity({"s1", "a1_meta", "v1", {"pm10"}}, 100, 400), metadata1},
                 {SequenceIdentity({"s1", "a1_meta", "v1"}, 100, 200),           Variant::Root()},
                 {SequenceIdentity({"s1", "a1", "v1", {"pm10"}}, 100, 200),      Variant::Root(
                         1.0)},
                 {SequenceIdentity({"s1", "a1", "v1"}, 100, 200),                Variant::Root(
                         2.0)},
                 {SequenceIdentity({"s1", "a1_meta", "v1"}, 200, 400),           metadata2},
                 {SequenceIdentity({"s1", "a1", "v1", {"pm1"}}, 200, 300),       Variant::Root(
                         3.0)},
                 {SequenceIdentity({"s1", "a1", "v1", {"pm10"}}, 200, 300),      Variant::Root(
                         4.0)},
                 {SequenceIdentity({"s1", "a1", "v1"}, 200, 300),                Variant::Root(
                         5.0)},};

        auto stage = NetCDF::stage(std::move(ctx));
        QVERIFY(stage.get() != nullptr);
        QVERIFY(!stage->isFinalStage());
        stage->processData(SequenceValue::Transfer(values));
        stage->finalizeData();

        stage = stage->getNextStage();
        QVERIFY(stage.get() != nullptr);
        QVERIFY(stage->isFinalStage());
        stage->processData(std::move(values));
        stage->finalizeData();
        stage.reset();

        QCOMPARE(fileResults, 1);

        int ncid = -1;
        QVERIFY(::nc_open(outputFile.fileName().toUtf8(), NC_NOWRITE, &ncid) == NC_NOERR);

        int dimid = -1;
        QVERIFY(::nc_inq_dimid(ncid, "time", &dimid) == NC_NOERR);
        ::size_t dimlen = 0;
        QVERIFY(::nc_inq_dimlen(ncid, dimid, &dimlen) == NC_NOERR);
        QCOMPARE((int) dimlen, 2);

        QVERIFY(::nc_inq_dimid(ncid, "wavelength", &dimid) == NC_NOERR);
        QVERIFY(::nc_inq_dimlen(ncid, dimid, &dimlen) == NC_NOERR);
        QCOMPARE((int) dimlen, 4);

        int varid = -1;
        QVERIFY(::nc_inq_varid(ncid, "value", &varid) == NC_NOERR);
        int ndims = 0;
        QVERIFY(::nc_inq_varndims(ncid, varid, &ndims) == NC_NOERR);
        QCOMPARE(ndims, 2);

        double varvalue = 0;
        ::size_t dimindex[2];
        dimindex[0] = 0;
        dimindex[1] = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, dimindex, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, (double) NC_FILL_DOUBLE);
        dimindex[0] = 1;
        dimindex[1] = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, dimindex, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 1.0);
        dimindex[0] = 2;
        dimindex[1] = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, dimindex, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, (double) NC_FILL_DOUBLE);
        dimindex[0] = 3;
        dimindex[1] = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, dimindex, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 2.0);
        dimindex[0] = 0;
        dimindex[1] = 1;
        QVERIFY(::nc_get_var1_double(ncid, varid, dimindex, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 3.0);
        dimindex[0] = 1;
        dimindex[1] = 1;
        QVERIFY(::nc_get_var1_double(ncid, varid, dimindex, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 4.0);
        dimindex[0] = 2;
        dimindex[1] = 1;
        QVERIFY(::nc_get_var1_double(ncid, varid, dimindex, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 5.0);
        dimindex[0] = 3;
        dimindex[1] = 1;
        QVERIFY(::nc_get_var1_double(ncid, varid, dimindex, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, (double) NC_FILL_DOUBLE);


        QVERIFY(::nc_inq_varid(ncid, "cwavelength", &varid) == NC_NOERR);
        QVERIFY(::nc_inq_varndims(ncid, varid, &ndims) == NC_NOERR);
        QCOMPARE(ndims, 1);
        dimlen = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 100.0);
        dimlen = 1;
        QVERIFY(::nc_get_var1_double(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 110.0);
        dimlen = 2;
        QVERIFY(::nc_get_var1_double(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 200.0);
        dimlen = 3;
        QVERIFY(::nc_get_var1_double(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, (double) NC_FILL_DOUBLE);

        QVERIFY(::nc_close(ncid) == NC_NOERR);
    }

    void dimensionScalar()
    {
        NetCDF ctx;
        Variant::Root config;

        QTemporaryFile outputFile;
        QVERIFY(outputFile.open());
        int fileResults = 0;
        ctx.createOutput = [&](const std::string &name) -> std::unique_ptr<QFile> {
            Q_ASSERT(name == "stuff");
            ++fileResults;
            std::unique_ptr<QFile> result(new QFile(outputFile.fileName()));
            if (!result->open(QIODevice::ReadWrite))
                return {};
            return std::move(result);
        };

        config["Filename"] = "stuff";
        config["Dimensions/Time/DimTime/Name/Constant"] = "time";
        config["Dimensions/Real/DimCut/Name/Constant"] = "cut";
        config["Dimensions/Real/DimCut/Coordinates/CoordValue/Name/Constant"] = "ccut";
        config["Variables/VarValue/Name/Constant"] = "value";
        config["Variables/VarValue/Data/Input/#0/Variable"] = "v1";
        config["Variables/VarValue/Time/Dimension"] = "DimTime";
        config["Variables/VarValue/Dimensions/#0/Dimension"] = "DimCut";
        config["Variables/VarValue/Dimensions/#0/Values/#0/Match/HasFlavors"] = "pm10";
        config["Variables/VarValue/Dimensions/#0/Values/#0/Value"] = 10.0;
        config["Variables/VarValue/Dimensions/#0/Values/#1/Value"] = FP::undefined();

        ctx.config.emplace_back(FP::undefined(), FP::undefined(), config);

        SequenceValue::Transfer values
                {{SequenceIdentity({"s1", "a1", "v1", {"pm10"}}, 100, 200), Variant::Root(1.0)},
                 {SequenceIdentity({"s1", "a1", "v1", {"pm10"}}, 200, 300), Variant::Root(2.0)},
                 {SequenceIdentity({"s1", "a1", "v1", {"pm10"}}, 300, 400), Variant::Root(3.0)},};

        auto stage = NetCDF::stage(std::move(ctx));
        QVERIFY(stage.get() != nullptr);
        QVERIFY(!stage->isFinalStage());
        stage->processData(SequenceValue::Transfer(values));
        stage->finalizeData();

        stage = stage->getNextStage();
        QVERIFY(stage.get() != nullptr);
        QVERIFY(stage->isFinalStage());
        stage->processData(std::move(values));
        stage->finalizeData();
        stage.reset();

        QCOMPARE(fileResults, 1);

        int ncid = -1;
        QVERIFY(::nc_open(outputFile.fileName().toUtf8(), NC_NOWRITE, &ncid) == NC_NOERR);

        int dimid = -1;
        QVERIFY(::nc_inq_dimid(ncid, "time", &dimid) == NC_NOERR);
        ::size_t dimlen = 0;
        QVERIFY(::nc_inq_dimlen(ncid, dimid, &dimlen) == NC_NOERR);
        QCOMPARE((int) dimlen, 3);

        QVERIFY(::nc_inq_dimid(ncid, "cut", &dimid) != NC_NOERR);

        int varid = -1;
        QVERIFY(::nc_inq_varid(ncid, "value", &varid) == NC_NOERR);
        int ndims = 0;
        QVERIFY(::nc_inq_varndims(ncid, varid, &ndims) == NC_NOERR);
        QCOMPARE(ndims, 1);

        double varvalue = 0;
        dimlen = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 1.0);
        dimlen = 1;
        QVERIFY(::nc_get_var1_double(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 2.0);
        dimlen = 2;
        QVERIFY(::nc_get_var1_double(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 3.0);


        QVERIFY(::nc_inq_varid(ncid, "ccut", &varid) == NC_NOERR);
        QVERIFY(::nc_inq_varndims(ncid, varid, &ndims) == NC_NOERR);
        QCOMPARE(ndims, 0);
        dimlen = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 10.0);

        QVERIFY(::nc_close(ncid) == NC_NOERR);
    }

    void dimensionEliminate()
    {
        NetCDF ctx;
        Variant::Root config;

        QTemporaryFile outputFile;
        QVERIFY(outputFile.open());
        int fileResults = 0;
        ctx.createOutput = [&](const std::string &name) -> std::unique_ptr<QFile> {
            Q_ASSERT(name == "stuff");
            ++fileResults;
            std::unique_ptr<QFile> result(new QFile(outputFile.fileName()));
            if (!result->open(QIODevice::ReadWrite))
                return {};
            return std::move(result);
        };

        config["Filename"] = "stuff";
        config["Dimensions/Time/DimTime/Name/Constant"] = "time";
        config["Dimensions/Real/DimCut/Name/Constant"] = "cut";
        config["Dimensions/Real/DimCut/ScalarRemove"] = "Always";
        config["Dimensions/Real/DimCut/Coordinates/CoordValue/Name/Constant"] = "ccut";
        config["Dimensions/Real/DimWL/Name/Constant"] = "wavelength";
        config["Dimensions/Real/DimWL/Coordinates/CoordValue/Name/Constant"] = "cwavelength";
        config["Variables/VarValue/Name/Constant"] = "value";
        config["Variables/VarValue/Data/Input/#0/Variable"] = "v1";
        config["Variables/VarValue/Time/Dimension"] = "DimTime";
        config["Variables/VarValue/Dimensions/#0/Dimension"] = "DimCut";
        config["Variables/VarValue/Dimensions/#0/Values/#0/Match/HasFlavors"] = "pm10";
        config["Variables/VarValue/Dimensions/#0/Values/#0/Value"] = 10.0;
        config["Variables/VarValue/Dimensions/#0/Values/#1/Value"] = FP::undefined();

        ctx.config.emplace_back(FP::undefined(), FP::undefined(), config);

        SequenceValue::Transfer values
                {{SequenceIdentity({"s1", "a1", "v1", {"pm10"}}, 100, 200), Variant::Root(1.0)},
                 {SequenceIdentity({"s1", "a1", "v1", {"pm10"}}, 200, 300), Variant::Root(2.0)},
                 {SequenceIdentity({"s1", "a1", "v1", {"pm10"}}, 300, 400), Variant::Root(3.0)},};

        auto stage = NetCDF::stage(std::move(ctx));
        QVERIFY(stage.get() != nullptr);
        QVERIFY(!stage->isFinalStage());
        stage->processData(SequenceValue::Transfer(values));
        stage->finalizeData();

        stage = stage->getNextStage();
        QVERIFY(stage.get() != nullptr);
        QVERIFY(stage->isFinalStage());
        stage->processData(std::move(values));
        stage->finalizeData();
        stage.reset();

        QCOMPARE(fileResults, 1);

        int ncid = -1;
        QVERIFY(::nc_open(outputFile.fileName().toUtf8(), NC_NOWRITE, &ncid) == NC_NOERR);

        int dimid = -1;
        QVERIFY(::nc_inq_dimid(ncid, "time", &dimid) == NC_NOERR);
        ::size_t dimlen = 0;
        QVERIFY(::nc_inq_dimlen(ncid, dimid, &dimlen) == NC_NOERR);
        QCOMPARE((int) dimlen, 3);

        QVERIFY(::nc_inq_dimid(ncid, "cut", &dimid) != NC_NOERR);
        QVERIFY(::nc_inq_dimid(ncid, "wavelength", &dimid) != NC_NOERR);

        int varid = -1;
        QVERIFY(::nc_inq_varid(ncid, "value", &varid) == NC_NOERR);
        int ndims = 0;
        QVERIFY(::nc_inq_varndims(ncid, varid, &ndims) == NC_NOERR);
        QCOMPARE(ndims, 1);

        double varvalue = 0;
        dimlen = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 1.0);
        dimlen = 1;
        QVERIFY(::nc_get_var1_double(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 2.0);
        dimlen = 2;
        QVERIFY(::nc_get_var1_double(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 3.0);

        QVERIFY(::nc_inq_varid(ncid, "ccut", &varid) != NC_NOERR);
        QVERIFY(::nc_inq_varid(ncid, "cwavelength", &varid) != NC_NOERR);

        QVERIFY(::nc_close(ncid) == NC_NOERR);
    }

    void integerDataVariable()
    {
        NetCDF ctx;
        Variant::Root config;

        QTemporaryFile outputFile;
        QVERIFY(outputFile.open());
        int fileResults = 0;
        ctx.createOutput = [&](const std::string &name) -> std::unique_ptr<QFile> {
            Q_ASSERT(name == "stuff");
            ++fileResults;
            std::unique_ptr<QFile> result(new QFile(outputFile.fileName()));
            if (!result->open(QIODevice::ReadWrite))
                return {};
            return std::move(result);
        };

        config["Filename"] = "stuff";
        config["Dimensions/Time/DimTime/Name/Constant"] = "time";
        config["Variables/VarValue/Name/Constant"] = "value";
        config["Variables/VarValue/Data/Input/#0/Variable"] = "v1";
        config["Variables/VarValue/Time/Dimension"] = "DimTime";
        config["Variables/VarValue/Type"] = "Integer";
        ctx.config.emplace_back(FP::undefined(), FP::undefined(), config);

        auto stage = NetCDF::stage(std::move(ctx));
        QVERIFY(stage.get() != nullptr);
        QVERIFY(!stage->isFinalStage());
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 100, 200), Variant::Root(
                (std::int_fast64_t) 1)}});
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 200, 300), Variant::Root(
                (std::int_fast64_t) 2)}});
        stage->finalizeData();

        stage = stage->getNextStage();
        QVERIFY(stage.get() != nullptr);
        QVERIFY(stage->isFinalStage());
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 100, 200), Variant::Root(
                (std::int_fast64_t) 1)}});
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 200, 300), Variant::Root(
                (std::int_fast64_t) 2)}});
        stage->finalizeData();
        stage.reset();

        QCOMPARE(fileResults, 1);

        int ncid = -1;
        QVERIFY(::nc_open(outputFile.fileName().toUtf8(), NC_NOWRITE, &ncid) == NC_NOERR);

        int dimid = -1;
        QVERIFY(::nc_inq_dimid(ncid, "time", &dimid) == NC_NOERR);
        ::size_t dimlen = 0;
        QVERIFY(::nc_inq_dimlen(ncid, dimid, &dimlen) == NC_NOERR);
        QCOMPARE((int) dimlen, 2);

        int varid = -1;
        QVERIFY(::nc_inq_varid(ncid, "value", &varid) == NC_NOERR);
        int ndims = 0;
        QVERIFY(::nc_inq_varndims(ncid, varid, &ndims) == NC_NOERR);
        QCOMPARE(ndims, 1);
        long long varvalue = 0;
        dimlen = 0;
        QVERIFY(::nc_get_var1_longlong(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE((int) varvalue, 1);
        dimlen = 1;
        QVERIFY(::nc_get_var1_longlong(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE((int) varvalue, 2);

        QVERIFY(::nc_close(ncid) == NC_NOERR);
    }

    void stringDataVariable()
    {
        NetCDF ctx;
        Variant::Root config;

        QTemporaryFile outputFile;
        QVERIFY(outputFile.open());
        int fileResults = 0;
        ctx.createOutput = [&](const std::string &name) -> std::unique_ptr<QFile> {
            Q_ASSERT(name == "stuff");
            ++fileResults;
            std::unique_ptr<QFile> result(new QFile(outputFile.fileName()));
            if (!result->open(QIODevice::ReadWrite))
                return {};
            return std::move(result);
        };

        config["Filename"] = "stuff";
        config["Dimensions/Time/DimTime/Name/Constant"] = "time";
        config["Variables/VarValue/Name/Constant"] = "value";
        config["Variables/VarValue/Data/Input/#0/Variable"] = "v1";
        config["Variables/VarValue/Time/Dimension"] = "DimTime";
        config["Variables/VarValue/Type"] = "String";
        ctx.config.emplace_back(FP::undefined(), FP::undefined(), config);

        auto stage = NetCDF::stage(std::move(ctx));
        QVERIFY(stage.get() != nullptr);
        QVERIFY(!stage->isFinalStage());
        stage->processData(
                {{SequenceIdentity({"s1", "a1", "v1"}, 100, 200), Variant::Root("Abc")}});
        stage->processData(
                {{SequenceIdentity({"s1", "a1", "v1"}, 200, 300), Variant::Root("DEF")}});
        stage->finalizeData();

        stage = stage->getNextStage();
        QVERIFY(stage.get() != nullptr);
        QVERIFY(stage->isFinalStage());
        stage->processData(
                {{SequenceIdentity({"s1", "a1", "v1"}, 100, 200), Variant::Root("Abc")}});
        stage->processData(
                {{SequenceIdentity({"s1", "a1", "v1"}, 200, 300), Variant::Root("DEF")}});
        stage->finalizeData();
        stage.reset();

        QCOMPARE(fileResults, 1);

        int ncid = -1;
        QVERIFY(::nc_open(outputFile.fileName().toUtf8(), NC_NOWRITE, &ncid) == NC_NOERR);

        int dimid = -1;
        QVERIFY(::nc_inq_dimid(ncid, "time", &dimid) == NC_NOERR);
        ::size_t dimlen = 0;
        QVERIFY(::nc_inq_dimlen(ncid, dimid, &dimlen) == NC_NOERR);
        QCOMPARE((int) dimlen, 2);

        int varid = -1;
        QVERIFY(::nc_inq_varid(ncid, "value", &varid) == NC_NOERR);
        int ndims = 0;
        QVERIFY(::nc_inq_varndims(ncid, varid, &ndims) == NC_NOERR);
        QCOMPARE(ndims, 1);
        char *varvalue = nullptr;
        dimlen = 0;
        QVERIFY(::nc_get_var1_string(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(QString(varvalue), QString("Abc"));
        dimlen = 1;
        QVERIFY(::nc_get_var1_string(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(QString(varvalue), QString("DEF"));

        QVERIFY(::nc_close(ncid) == NC_NOERR);
    }

    void realFragmentVariable()
    {
        NetCDF ctx;
        Variant::Root config;

        QTemporaryFile outputFile;
        QVERIFY(outputFile.open());
        int fileResults = 0;
        ctx.createOutput = [&](const std::string &name) -> std::unique_ptr<QFile> {
            Q_ASSERT(name == "stuff");
            ++fileResults;
            std::unique_ptr<QFile> result(new QFile(outputFile.fileName()));
            if (!result->open(QIODevice::ReadWrite))
                return {};
            return std::move(result);
        };

        config["Filename"] = "stuff";
        config["Dimensions/Fragment/DimTime/Name/Constant"] = "time";
        config["Dimensions/Fragment/DimTime/Coordinates/CoordTime/Name/Constant"] = "ctime";
        config["Dimensions/Fragment/DimTime/Coordinates/CoordTime/Attributes/A1/Name/Constant"] =
                "ca1name";
        config["Dimensions/Fragment/DimTime/Coordinates/CoordTime/Attributes/A1/Value/Constant"] =
                "ca1value";
        config["Fragments/VarValue/Name/Constant"] = "value";
        config["Fragments/VarValue/Value/Input/#0/Variable"] = "v1";
        config["Fragments/VarValue/Fragment/Dimension"] = "DimTime";
        config["Fragments/VarValue/Attributes/A1/Name/Constant"] = "a1name";
        config["Fragments/VarValue/Attributes/A1/Value/Constant"] = "a1value";
        ctx.config.emplace_back(FP::undefined(), FP::undefined(), config);

        auto stage = NetCDF::stage(std::move(ctx));
        QVERIFY(stage.get() != nullptr);
        QVERIFY(!stage->isFinalStage());
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 100, 200), Variant::Root(1.0)}});
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 200, 300), Variant::Root(2.0)}});
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 300, 400), Variant::Root(2.0)}});
        stage->finalizeData();

        stage = stage->getNextStage();
        QVERIFY(stage.get() != nullptr);
        QVERIFY(stage->isFinalStage());
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 100, 200), Variant::Root(1.0)}});
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 200, 300), Variant::Root(2.0)}});
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 300, 400), Variant::Root(2.0)}});
        stage->finalizeData();
        stage.reset();

        QCOMPARE(fileResults, 1);

        int ncid = -1;
        QVERIFY(::nc_open(outputFile.fileName().toUtf8(), NC_NOWRITE, &ncid) == NC_NOERR);

        int dimid = -1;
        QVERIFY(::nc_inq_dimid(ncid, "time", &dimid) == NC_NOERR);
        ::size_t dimlen = 0;
        QVERIFY(::nc_inq_dimlen(ncid, dimid, &dimlen) == NC_NOERR);
        QCOMPARE((int) dimlen, 2);

        int varid = -1;
        QVERIFY(::nc_inq_varid(ncid, "value", &varid) == NC_NOERR);
        int ndims = 0;
        QVERIFY(::nc_inq_varndims(ncid, varid, &ndims) == NC_NOERR);
        QCOMPARE(ndims, 1);
        double varvalue = 0;
        dimlen = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 1.0);
        dimlen = 1;
        QVERIFY(::nc_get_var1_double(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 2.0);

        QCOMPARE(getTextAttribute("a1name", ncid, varid), QString("a1value"));


        QVERIFY(::nc_inq_varid(ncid, "ctime", &varid) == NC_NOERR);
        QVERIFY(::nc_inq_varndims(ncid, varid, &ndims) == NC_NOERR);
        QCOMPARE(ndims, 1);
        dimlen = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, (double) NC_FILL_DOUBLE);
        dimlen = 1;
        QVERIFY(::nc_get_var1_double(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 200.0);

        QCOMPARE(getTextAttribute("ca1name", ncid, varid), QString("ca1value"));

        QVERIFY(::nc_close(ncid) == NC_NOERR);
    }

    void integerFragmentVariable()
    {
        NetCDF ctx;
        Variant::Root config;

        QTemporaryFile outputFile;
        QVERIFY(outputFile.open());
        int fileResults = 0;
        ctx.createOutput = [&](const std::string &name) -> std::unique_ptr<QFile> {
            Q_ASSERT(name == "stuff");
            ++fileResults;
            std::unique_ptr<QFile> result(new QFile(outputFile.fileName()));
            if (!result->open(QIODevice::ReadWrite))
                return {};
            return std::move(result);
        };

        config["Filename"] = "stuff";
        config["Dimensions/Fragment/DimTime/Name/Constant"] = "time";
        config["Fragments/VarValue/Name/Constant"] = "value";
        config["Fragments/VarValue/Value/Input/#0/Variable"] = "v1";
        config["Fragments/VarValue/Type"] = "Integer";
        config["Fragments/VarValue/Fragment/Dimension"] = "DimTime";
        ctx.config.emplace_back(FP::undefined(), FP::undefined(), config);

        auto stage = NetCDF::stage(std::move(ctx));
        QVERIFY(stage.get() != nullptr);
        QVERIFY(!stage->isFinalStage());
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 100, 200), Variant::Root(
                static_cast<std::int_fast64_t>(1))}});
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 200, 300), Variant::Root(
                static_cast<std::int_fast64_t>(2))}});
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 300, 400), Variant::Root(
                static_cast<std::int_fast64_t>(2))}});
        stage->finalizeData();

        stage = stage->getNextStage();
        QVERIFY(stage.get() != nullptr);
        QVERIFY(stage->isFinalStage());
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 100, 200), Variant::Root(
                static_cast<std::int_fast64_t>(1))}});
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 200, 300), Variant::Root(
                static_cast<std::int_fast64_t>(2))}});
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 300, 400), Variant::Root(
                static_cast<std::int_fast64_t>(2))}});
        stage->finalizeData();
        stage.reset();

        QCOMPARE(fileResults, 1);

        int ncid = -1;
        QVERIFY(::nc_open(outputFile.fileName().toUtf8(), NC_NOWRITE, &ncid) == NC_NOERR);

        int dimid = -1;
        QVERIFY(::nc_inq_dimid(ncid, "time", &dimid) == NC_NOERR);
        ::size_t dimlen = 0;
        QVERIFY(::nc_inq_dimlen(ncid, dimid, &dimlen) == NC_NOERR);
        QCOMPARE((int) dimlen, 2);

        int varid = -1;
        QVERIFY(::nc_inq_varid(ncid, "value", &varid) == NC_NOERR);
        int ndims = 0;
        QVERIFY(::nc_inq_varndims(ncid, varid, &ndims) == NC_NOERR);
        QCOMPARE(ndims, 1);
        long long varvalue = 0;
        dimlen = 0;
        QVERIFY(::nc_get_var1_longlong(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE((int) varvalue, 1);
        dimlen = 1;
        QVERIFY(::nc_get_var1_longlong(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE((int) varvalue, 2);

        QVERIFY(::nc_close(ncid) == NC_NOERR);
    }

    void stringFragmentVariable()
    {
        NetCDF ctx;
        Variant::Root config;

        QTemporaryFile outputFile;
        QVERIFY(outputFile.open());
        int fileResults = 0;
        ctx.createOutput = [&](const std::string &name) -> std::unique_ptr<QFile> {
            Q_ASSERT(name == "stuff");
            ++fileResults;
            std::unique_ptr<QFile> result(new QFile(outputFile.fileName()));
            if (!result->open(QIODevice::ReadWrite))
                return {};
            return std::move(result);
        };

        config["Filename"] = "stuff";
        config["Dimensions/Fragment/DimTime/Name/Constant"] = "time";
        config["Fragments/VarValue/Name/Constant"] = "value";
        config["Fragments/VarValue/Value/Input/#0/Variable"] = "v1";
        config["Fragments/VarValue/Type"] = "String";
        config["Fragments/VarValue/Fragment/Dimension"] = "DimTime";
        ctx.config.emplace_back(FP::undefined(), FP::undefined(), config);

        auto stage = NetCDF::stage(std::move(ctx));
        QVERIFY(stage.get() != nullptr);
        QVERIFY(!stage->isFinalStage());
        stage->processData(
                {{SequenceIdentity({"s1", "a1", "v1"}, 100, 200), Variant::Root("Abc")}});
        stage->processData(
                {{SequenceIdentity({"s1", "a1", "v1"}, 200, 300), Variant::Root("DEF")}});
        stage->processData(
                {{SequenceIdentity({"s1", "a1", "v1"}, 300, 400), Variant::Root("DEF")}});
        stage->finalizeData();

        stage = stage->getNextStage();
        QVERIFY(stage.get() != nullptr);
        QVERIFY(stage->isFinalStage());
        stage->processData(
                {{SequenceIdentity({"s1", "a1", "v1"}, 100, 200), Variant::Root("Abc")}});
        stage->processData(
                {{SequenceIdentity({"s1", "a1", "v1"}, 200, 300), Variant::Root("DEF")}});
        stage->processData(
                {{SequenceIdentity({"s1", "a1", "v1"}, 300, 400), Variant::Root("DEF")}});
        stage->finalizeData();
        stage.reset();

        QCOMPARE(fileResults, 1);

        int ncid = -1;
        QVERIFY(::nc_open(outputFile.fileName().toUtf8(), NC_NOWRITE, &ncid) == NC_NOERR);

        int dimid = -1;
        QVERIFY(::nc_inq_dimid(ncid, "time", &dimid) == NC_NOERR);
        ::size_t dimlen = 0;
        QVERIFY(::nc_inq_dimlen(ncid, dimid, &dimlen) == NC_NOERR);
        QCOMPARE((int) dimlen, 2);

        int varid = -1;
        QVERIFY(::nc_inq_varid(ncid, "value", &varid) == NC_NOERR);
        int ndims = 0;
        QVERIFY(::nc_inq_varndims(ncid, varid, &ndims) == NC_NOERR);
        QCOMPARE(ndims, 1);
        char *varvalue = nullptr;
        dimlen = 0;
        QVERIFY(::nc_get_var1_string(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(QString(varvalue), QString("Abc"));
        dimlen = 1;
        QVERIFY(::nc_get_var1_string(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(QString(varvalue), QString("DEF"));

        QVERIFY(::nc_close(ncid) == NC_NOERR);
    }

    void integerIndexDimension()
    {
        NetCDF ctx;
        Variant::Root config;

        QTemporaryFile outputFile;
        QVERIFY(outputFile.open());
        int fileResults = 0;
        ctx.createOutput = [&](const std::string &name) -> std::unique_ptr<QFile> {
            Q_ASSERT(name == "stuff");
            ++fileResults;
            std::unique_ptr<QFile> result(new QFile(outputFile.fileName()));
            if (!result->open(QIODevice::ReadWrite))
                return {};
            return std::move(result);
        };

        config["Filename"] = "stuff";
        config["Dimensions/Time/DimTime/Name/Constant"] = "time";
        config["Dimensions/Integer/DimCut/Name/Constant"] = "cut";
        config["Dimensions/Integer/DimCut/Coordinates/CoordValue/Name/Constant"] = "ccut";
        config["Dimensions/Integer/DimCut/Coordinates/CoordValue/Attributes/A1/Name/Constant"] =
                "ca1name";
        config["Dimensions/Integer/DimCut/Coordinates/CoordValue/Attributes/A1/Value/Constant"] =
                "ca1value";
        config["Variables/VarValue/Name/Constant"] = "value";
        config["Variables/VarValue/Data/Input/#0/Variable"] = "v1";
        config["Variables/VarValue/Time/Dimension"] = "DimTime";
        config["Variables/VarValue/Dimensions/#0/Dimension"] = "DimCut";
        config["Variables/VarValue/Dimensions/#0/Values/#0/Match/HasFlavors"] = "pm10";
        config["Variables/VarValue/Dimensions/#0/Values/#0/Value"] = 10;
        config["Variables/VarValue/Dimensions/#0/Values/#1/Match/HasFlavors"] = "pm1";
        config["Variables/VarValue/Dimensions/#0/Values/#1/Value"] = 1;
        config["Variables/VarValue/Dimensions/#0/Values/#2/Value"] = FP::undefined();

        ctx.config.emplace_back(FP::undefined(), FP::undefined(), config);

        SequenceValue::Transfer values
                {{SequenceIdentity({"s1", "a1", "v1", {"pm1"}}, 100, 200),  Variant::Root(1.0)},
                 {SequenceIdentity({"s1", "a1", "v1", {"pm10"}}, 100, 200), Variant::Root(2.0)},
                 {SequenceIdentity({"s1", "a1", "v1"}, 200, 300),           Variant::Root(3.0)},
                 {SequenceIdentity({"s1", "a1", "v1", {"pm1"}}, 200, 300),  Variant::Root(4.0)},
                 {SequenceIdentity({"s1", "a1", "v1", {"pm10"}}, 200, 300), Variant::Root(5.0)},};

        auto stage = NetCDF::stage(std::move(ctx));
        QVERIFY(stage.get() != nullptr);
        QVERIFY(!stage->isFinalStage());
        stage->processData(SequenceValue::Transfer(values));
        stage->finalizeData();

        stage = stage->getNextStage();
        QVERIFY(stage.get() != nullptr);
        QVERIFY(stage->isFinalStage());
        stage->processData(std::move(values));
        stage->finalizeData();
        stage.reset();

        QCOMPARE(fileResults, 1);

        int ncid = -1;
        QVERIFY(::nc_open(outputFile.fileName().toUtf8(), NC_NOWRITE, &ncid) == NC_NOERR);

        int dimid = -1;
        QVERIFY(::nc_inq_dimid(ncid, "time", &dimid) == NC_NOERR);
        ::size_t dimlen = 0;
        QVERIFY(::nc_inq_dimlen(ncid, dimid, &dimlen) == NC_NOERR);
        QCOMPARE((int) dimlen, 2);

        QVERIFY(::nc_inq_dimid(ncid, "cut", &dimid) == NC_NOERR);
        QVERIFY(::nc_inq_dimlen(ncid, dimid, &dimlen) == NC_NOERR);
        QCOMPARE((int) dimlen, 3);

        int varid = -1;
        QVERIFY(::nc_inq_varid(ncid, "value", &varid) == NC_NOERR);
        int ndims = 0;
        QVERIFY(::nc_inq_varndims(ncid, varid, &ndims) == NC_NOERR);
        QCOMPARE(ndims, 2);

        double varvalue = 0;
        ::size_t dimindex[2];
        dimindex[0] = 0;
        dimindex[1] = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, dimindex, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 1.0);
        dimindex[0] = 1;
        dimindex[1] = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, dimindex, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 2.0);
        dimindex[0] = 2;
        dimindex[1] = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, dimindex, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, (double) NC_FILL_DOUBLE);
        dimindex[0] = 0;
        dimindex[1] = 1;
        QVERIFY(::nc_get_var1_double(ncid, varid, dimindex, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 4.0);
        dimindex[0] = 1;
        dimindex[1] = 1;
        QVERIFY(::nc_get_var1_double(ncid, varid, dimindex, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 5.0);
        dimindex[0] = 2;
        dimindex[1] = 1;
        QVERIFY(::nc_get_var1_double(ncid, varid, dimindex, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 3.0);


        long long dimvalue = 0;
        QVERIFY(::nc_inq_varid(ncid, "ccut", &varid) == NC_NOERR);
        QVERIFY(::nc_inq_varndims(ncid, varid, &ndims) == NC_NOERR);
        QCOMPARE(ndims, 1);
        dimlen = 0;
        QVERIFY(::nc_get_var1_longlong(ncid, varid, &dimlen, &dimvalue) == NC_NOERR);
        QCOMPARE((int) dimvalue, 1);
        dimlen = 1;
        QVERIFY(::nc_get_var1_longlong(ncid, varid, &dimlen, &dimvalue) == NC_NOERR);
        QCOMPARE((int) dimvalue, 10);
        dimlen = 2;
        QVERIFY(::nc_get_var1_longlong(ncid, varid, &dimlen, &dimvalue) == NC_NOERR);
        QCOMPARE(dimvalue, (long long) NC_FILL_INT64);

        QCOMPARE(getTextAttribute("ca1name", ncid, varid), QString("ca1value"));

        QVERIFY(::nc_close(ncid) == NC_NOERR);
    }

    void stringIndexDimension()
    {
        NetCDF ctx;
        Variant::Root config;

        QTemporaryFile outputFile;
        QVERIFY(outputFile.open());
        int fileResults = 0;
        ctx.createOutput = [&](const std::string &name) -> std::unique_ptr<QFile> {
            Q_ASSERT(name == "stuff");
            ++fileResults;
            std::unique_ptr<QFile> result(new QFile(outputFile.fileName()));
            if (!result->open(QIODevice::ReadWrite))
                return {};
            return std::move(result);
        };

        config["Filename"] = "stuff";
        config["Dimensions/Time/DimTime/Name/Constant"] = "time";
        config["Dimensions/String/DimCut/Name/Constant"] = "cut";
        config["Dimensions/String/DimCut/Coordinates/CoordValue/Name/Constant"] = "ccut";
        config["Dimensions/String/DimCut/Coordinates/CoordValue/Attributes/A1/Name/Constant"] =
                "ca1name";
        config["Dimensions/String/DimCut/Coordinates/CoordValue/Attributes/A1/Value/Constant"] =
                "ca1value";
        config["Variables/VarValue/Name/Constant"] = "value";
        config["Variables/VarValue/Data/Input/#0/Variable"] = "v1";
        config["Variables/VarValue/Time/Dimension"] = "DimTime";
        config["Variables/VarValue/Dimensions/#0/Dimension"] = "DimCut";
        config["Variables/VarValue/Dimensions/#0/Values/#0/Match/HasFlavors"] = "pm10";
        config["Variables/VarValue/Dimensions/#0/Values/#0/Value"] = "10";
        config["Variables/VarValue/Dimensions/#0/Values/#1/Match/HasFlavors"] = "pm1";
        config["Variables/VarValue/Dimensions/#0/Values/#1/Value"] = "1";
        config["Variables/VarValue/Dimensions/#0/Values/#2/Value"] = "none";

        ctx.config.emplace_back(FP::undefined(), FP::undefined(), config);

        SequenceValue::Transfer values
                {{SequenceIdentity({"s1", "a1", "v1", {"pm1"}}, 100, 200),  Variant::Root(1.0)},
                 {SequenceIdentity({"s1", "a1", "v1", {"pm10"}}, 100, 200), Variant::Root(2.0)},
                 {SequenceIdentity({"s1", "a1", "v1"}, 200, 300),           Variant::Root(3.0)},
                 {SequenceIdentity({"s1", "a1", "v1", {"pm1"}}, 200, 300),  Variant::Root(4.0)},
                 {SequenceIdentity({"s1", "a1", "v1", {"pm10"}}, 200, 300), Variant::Root(5.0)},};

        auto stage = NetCDF::stage(std::move(ctx));
        QVERIFY(stage.get() != nullptr);
        QVERIFY(!stage->isFinalStage());
        stage->processData(SequenceValue::Transfer(values));
        stage->finalizeData();

        stage = stage->getNextStage();
        QVERIFY(stage.get() != nullptr);
        QVERIFY(stage->isFinalStage());
        stage->processData(std::move(values));
        stage->finalizeData();
        stage.reset();

        QCOMPARE(fileResults, 1);

        int ncid = -1;
        QVERIFY(::nc_open(outputFile.fileName().toUtf8(), NC_NOWRITE, &ncid) == NC_NOERR);

        int dimid = -1;
        QVERIFY(::nc_inq_dimid(ncid, "time", &dimid) == NC_NOERR);
        ::size_t dimlen = 0;
        QVERIFY(::nc_inq_dimlen(ncid, dimid, &dimlen) == NC_NOERR);
        QCOMPARE((int) dimlen, 2);

        QVERIFY(::nc_inq_dimid(ncid, "cut", &dimid) == NC_NOERR);
        QVERIFY(::nc_inq_dimlen(ncid, dimid, &dimlen) == NC_NOERR);
        QCOMPARE((int) dimlen, 3);

        int varid = -1;
        QVERIFY(::nc_inq_varid(ncid, "value", &varid) == NC_NOERR);
        int ndims = 0;
        QVERIFY(::nc_inq_varndims(ncid, varid, &ndims) == NC_NOERR);
        QCOMPARE(ndims, 2);

        double varvalue = 0;
        ::size_t dimindex[2];
        dimindex[0] = 0;
        dimindex[1] = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, dimindex, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 1.0);
        dimindex[0] = 1;
        dimindex[1] = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, dimindex, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 2.0);
        dimindex[0] = 2;
        dimindex[1] = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, dimindex, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, (double) NC_FILL_DOUBLE);
        dimindex[0] = 0;
        dimindex[1] = 1;
        QVERIFY(::nc_get_var1_double(ncid, varid, dimindex, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 4.0);
        dimindex[0] = 1;
        dimindex[1] = 1;
        QVERIFY(::nc_get_var1_double(ncid, varid, dimindex, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 5.0);
        dimindex[0] = 2;
        dimindex[1] = 1;
        QVERIFY(::nc_get_var1_double(ncid, varid, dimindex, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 3.0);


        char *dimvalue = nullptr;
        QVERIFY(::nc_inq_varid(ncid, "ccut", &varid) == NC_NOERR);
        QVERIFY(::nc_inq_varndims(ncid, varid, &ndims) == NC_NOERR);
        QCOMPARE(ndims, 1);
        dimlen = 0;
        QVERIFY(::nc_get_var1_string(ncid, varid, &dimlen, &dimvalue) == NC_NOERR);
        QCOMPARE(QString(dimvalue), QString("1"));
        dimlen = 1;
        QVERIFY(::nc_get_var1_string(ncid, varid, &dimlen, &dimvalue) == NC_NOERR);
        QCOMPARE(QString(dimvalue), QString("10"));
        dimlen = 2;
        QVERIFY(::nc_get_var1_string(ncid, varid, &dimlen, &dimvalue) == NC_NOERR);
        QCOMPARE(QString(dimvalue), QString("none"));

        QCOMPARE(getTextAttribute("ca1name", ncid, varid), QString("ca1value"));

        QVERIFY(::nc_close(ncid) == NC_NOERR);
    }

    void groupNesting()
    {
        NetCDF ctx;
        Variant::Root config;

        QTemporaryFile outputFile;
        QVERIFY(outputFile.open());
        int fileResults = 0;
        ctx.createOutput = [&](const std::string &name) -> std::unique_ptr<QFile> {
            Q_ASSERT(name == "stuff");
            ++fileResults;
            std::unique_ptr<QFile> result(new QFile(outputFile.fileName()));
            if (!result->open(QIODevice::ReadWrite))
                return {};
            return std::move(result);
        };

        config["Filename"] = "stuff";
        config["Dimensions/Time/DimTime/Name/Constant"] = "time";
        config["Dimensions/Time/DimTime/Coordinates/CoordTime/Name/Constant"] = "ctime";
        config["Groups/First/Name/Constant"] = "group1";
        config["Groups/First/Variables/VarValue/Name/Constant"] = "value";
        config["Groups/First/Variables/VarValue/Time/Dimension"] = "DimTime";
        config["Groups/First/Variables/VarValue/Data/Input/#0/Variable"] = "v1";
        config["Groups/First/Variables/VarValue/Attributes/A1/Name/Constant"] = "a1name";
        config["Groups/First/Variables/VarValue/Attributes/A1/Value/Constant"] = "a1value";
        config["Groups/Second/Name/Constant"] = "group2";
        config["Groups/Second/Dimensions/Time/DimTime2/Name/Constant"] = "time2";
        config["Groups/Second/Dimensions/Time/DimTime2/Coordinates/CoordTime/Name/Constant"] =
                "ctime2";
        config["Groups/Second/Variables/VarValue/Name/Constant"] = "value";
        config["Groups/Second/Variables/VarValue/Time/Dimension"] = "DimTime2";
        config["Groups/Second/Variables/VarValue/Data/Input/#0/Variable"] = "v1";
        config["Groups/Second/Variables/VarValue/Attributes/A1/Name/Constant"] = "a2name";
        config["Groups/Second/Variables/VarValue/Attributes/A1/Value/Constant"] = "a2value";
        config["Groups/Second/Groups/Third/Name/Constant"] = "group3";
        config["Groups/Second/Groups/Third/Variables/VarValue/Name/Constant"] = "value2";
        config["Groups/Second/Groups/Third/Variables/VarValue/Time/Dimension"] = "DimTime";
        config["Groups/Second/Groups/Third/Variables/VarValue/Data/Input/#0/Variable"] = "v1";
        config["Groups/Second/Groups/Third/Variables/VarValue/Attributes/A1/Name/Constant"] =
                "a3name";
        config["Groups/Second/Groups/Third/Variables/VarValue/Attributes/A1/Value/Constant"] =
                "a3value";
        ctx.config.emplace_back(FP::undefined(), FP::undefined(), config);

        auto stage = NetCDF::stage(std::move(ctx));
        QVERIFY(stage.get() != nullptr);
        QVERIFY(!stage->isFinalStage());
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 100, 200), Variant::Root(1.0)}});
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 200, 300), Variant::Root(2.0)}});
        stage->finalizeData();

        stage = stage->getNextStage();
        QVERIFY(stage.get() != nullptr);
        QVERIFY(stage->isFinalStage());
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 100, 200), Variant::Root(1.0)}});
        stage->processData({{SequenceIdentity({"s1", "a1", "v1"}, 200, 300), Variant::Root(2.0)}});
        stage->finalizeData();
        stage.reset();

        QCOMPARE(fileResults, 1);

        int ncid = -1;
        QVERIFY(::nc_open(outputFile.fileName().toUtf8(), NC_NOWRITE, &ncid) == NC_NOERR);

        int dimid = -1;
        QVERIFY(::nc_inq_dimid(ncid, "time", &dimid) == NC_NOERR);
        ::size_t dimlen = 0;
        QVERIFY(::nc_inq_dimlen(ncid, dimid, &dimlen) == NC_NOERR);
        QCOMPARE((int) dimlen, 2);

        int varid = -1;
        QVERIFY(::nc_inq_varid(ncid, "ctime", &varid) == NC_NOERR);
        int ndims = 0;
        QVERIFY(::nc_inq_varndims(ncid, varid, &ndims) == NC_NOERR);
        QCOMPARE(ndims, 1);
        dimlen = 0;
        double varvalue = 0;
        QVERIFY(::nc_get_var1_double(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 100.0);
        dimlen = 1;
        QVERIFY(::nc_get_var1_double(ncid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 200.0);


        int gid = -1;
        QVERIFY(::nc_inq_grp_ncid(ncid, "group1", &gid) == NC_NOERR);

        QVERIFY(::nc_inq_varid(gid, "value", &varid) == NC_NOERR);
        QVERIFY(::nc_inq_varndims(gid, varid, &ndims) == NC_NOERR);
        QCOMPARE(ndims, 1);
        dimlen = 0;
        QVERIFY(::nc_get_var1_double(gid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 1.0);
        dimlen = 1;
        QVERIFY(::nc_get_var1_double(gid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 2.0);

        QCOMPARE(getTextAttribute("a1name", gid, varid), QString("a1value"));


        QVERIFY(::nc_inq_grp_ncid(ncid, "group2", &gid) == NC_NOERR);

        QVERIFY(::nc_inq_varid(gid, "value", &varid) == NC_NOERR);
        QVERIFY(::nc_inq_varndims(gid, varid, &ndims) == NC_NOERR);
        QCOMPARE(ndims, 1);
        dimlen = 0;
        QVERIFY(::nc_get_var1_double(gid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 1.0);
        dimlen = 1;
        QVERIFY(::nc_get_var1_double(gid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 2.0);

        QCOMPARE(getTextAttribute("a2name", gid, varid), QString("a2value"));

        QVERIFY(::nc_inq_dimid(gid, "time2", &dimid) == NC_NOERR);
        QVERIFY(::nc_inq_dimlen(gid, dimid, &dimlen) == NC_NOERR);
        QCOMPARE((int) dimlen, 2);

        QVERIFY(::nc_inq_varid(gid, "ctime2", &varid) == NC_NOERR);
        QVERIFY(::nc_inq_varndims(gid, varid, &ndims) == NC_NOERR);
        QCOMPARE(ndims, 1);
        dimlen = 0;
        QVERIFY(::nc_get_var1_double(gid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 100.0);
        dimlen = 1;
        QVERIFY(::nc_get_var1_double(gid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 200.0);


        QVERIFY(::nc_inq_grp_ncid(gid, "group3", &gid) == NC_NOERR);

        QVERIFY(::nc_inq_varid(gid, "value2", &varid) == NC_NOERR);
        QVERIFY(::nc_inq_varndims(gid, varid, &ndims) == NC_NOERR);
        QCOMPARE(ndims, 1);
        dimlen = 0;
        QVERIFY(::nc_get_var1_double(gid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 1.0);
        dimlen = 1;
        QVERIFY(::nc_get_var1_double(gid, varid, &dimlen, &varvalue) == NC_NOERR);
        QCOMPARE(varvalue, 2.0);

        QCOMPARE(getTextAttribute("a3name", gid, varid), QString("a3value"));


        QVERIFY(::nc_close(ncid) == NC_NOERR);
    }
};

QTEST_MAIN(TestNetCDF)

#include "netcdf.moc"