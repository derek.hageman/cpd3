/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtGlobal>
#include <QTest>

#include "fileoutput/csv.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"

using namespace CPD3;
using namespace CPD3::Output;
using namespace CPD3::Data;

Q_DECLARE_METATYPE(CPD3::Output::CSV);

Q_DECLARE_METATYPE(std::vector<QStringList>);


class TestOutputCSV : public QObject {
Q_OBJECT

private slots:

    void basic()
    {
        QFETCH(CPD3::Output::CSV, config);
        QFETCH(SequenceValue::Transfer, values);
        QFETCH(std::vector<QStringList>, expectedHeaders);
        QFETCH(std::vector<QStringList>, expectedLines);

        std::vector<QStringList> headers;
        std::vector<QStringList> lines;
        config.outputHeader = [&headers](QStringList &&header) {
            headers.emplace_back(std::move(header));
        };
        config.outputLine = [&lines](QStringList &&line) {
            lines.emplace_back(std::move(line));
        };

        auto s = CPD3::Output::CSV::stage(config);
        QVERIFY(!s->isFinalStage());
        s->processData(SequenceValue::Transfer(values));
        s->finalizeData();

        s = s->getNextStage();
        QVERIFY(s.get() != nullptr);
        QVERIFY(!s->isFinalStage());
        s->processData(SequenceValue::Transfer(values));
        s->finalizeData();

        s = s->getNextStage();
        QVERIFY(s.get() != nullptr);
        QVERIFY(s->isFinalStage());
        s->processData(SequenceValue::Transfer(values));
        s->finalizeData();

        QCOMPARE(headers, expectedHeaders);
        QCOMPARE(lines, expectedLines);
    }

    // @formatter:off
    void basic_data()
    {
        QTest::addColumn<CPD3::Output::CSV>("config");
        QTest::addColumn<SequenceValue::Transfer>("values");
        QTest::addColumn<std::vector<QStringList>>("expectedHeaders");
        QTest::addColumn<std::vector<QStringList>>("expectedLines");

        QTest::newRow("Defaults") << CSV() <<
            SequenceValue::Transfer{
                SequenceValue(SequenceIdentity({"bnd", "raw", "BsG_S11"}, 100.0, 200.0),
                              Variant::Root(1.0)),
                SequenceValue(SequenceIdentity({"bnd", "raw", "BsG_S11"}, 200.0, 300.0),
                              Variant::Root(2.0))
            } << (std::vector<QStringList>{
                QStringList{"DateTimeUTC", "BsG_S11"},
            }) << (std::vector<QStringList>{
                QStringList{"1970-01-01 00:01:40", "00001.000"},
                QStringList{"1970-01-01 00:03:20", "00002.000"},
            });
        QTest::newRow("Two variable") << CSV() <<
            SequenceValue::Transfer{
                SequenceValue(SequenceIdentity({"bnd", "raw", "BsG_S11"}, 100.0, 200.0),
                              Variant::Root(1.0)),
                SequenceValue(SequenceIdentity({"bnd", "raw", "BsB_S11"}, 200.0, 300.0),
                              Variant::Root(2.0))
            } << (std::vector<QStringList>{
                QStringList{"DateTimeUTC", "BsB_S11", "BsG_S11"},
            }) << (std::vector<QStringList>{
                QStringList{"1970-01-01 00:01:40", "99999.999", "00001.000"},
                QStringList{"1970-01-01 00:03:20", "00002.000", "99999.999"},
            });

        QTest::newRow("Cut overlap split") << CSV() <<
            SequenceValue::Transfer{
                SequenceValue(SequenceIdentity({"bnd", "raw", "BsG_S11", {"pm1"}}, 100.0, 200.0),
                              Variant::Root(1.0)),
                SequenceValue(SequenceIdentity({"bnd", "raw", "BsG_S11", {"pm10"}}, 100.0, 300.0),
                              Variant::Root(2.0)),
            } << (std::vector<QStringList>{
                QStringList{"DateTimeUTC", "BsG0_S11", "BsG1_S11"},
            }) << (std::vector<QStringList>{
                QStringList{"1970-01-01 00:01:40", "00002.000", "00001.000"},
                QStringList{"1970-01-01 00:03:20", "00002.000", "99999.999"},
            });
        QTest::newRow("Cut no overlap") << CSV() <<
            SequenceValue::Transfer{
                SequenceValue(SequenceIdentity({"bnd", "raw", "BsG_S11", {"pm1"}}, 100.0, 200.0),
                              Variant::Root(1.0)),
                SequenceValue(SequenceIdentity({"bnd", "raw", "BsG_S11", {"pm10"}}, 200.0, 300.0),
                              Variant::Root(2.0)),
            } << (std::vector<QStringList>{
                QStringList{"DateTimeUTC", "BsG_S11"},
            }) << (std::vector<QStringList>{
                QStringList{"1970-01-01 00:01:40", "00001.000"},
                QStringList{"1970-01-01 00:03:20", "00002.000"},
            });


        Variant::Root array1;
        array1["#0"] = 1.0;
        array1["#1"] = 2.0;
        array1["#2"] = 3.0;
        array1["#3"] = 4.0;

        Variant::Root array2;
        array2["#0"] = 1.5;
        array2["#1"] = 2.5;
        array2["#2"] = 3.5;

        QTest::newRow("Array") << CSV() <<
            SequenceValue::Transfer{
                SequenceValue(SequenceIdentity({"bnd", "raw", "Nb_N11"}, 100.0, 200.0),
                              array1),
                SequenceValue(SequenceIdentity({"bnd", "raw", "Nb_N11"}, 200.0, 300.0),
                              array2),
            } << (std::vector<QStringList>{
                QStringList{"DateTimeUTC", "Nb1_N11", "Nb2_N11", "Nb3_N11", "Nb4_N11"},
            }) << (std::vector<QStringList>{
                QStringList{"1970-01-01 00:01:40", "00001.000", "00002.000", "00003.000", "00004.000"},
                QStringList{"1970-01-01 00:03:20", "00001.500", "00002.500", "00003.500", "99999.999"},
            });


        CSV config;
        config.cutSplitMode = CSV::CutSplitMode::Always;
        QTest::newRow("Cut explicit split") << config <<
            SequenceValue::Transfer{
                SequenceValue(SequenceIdentity({"bnd", "raw", "BsG_S11", {"pm1"}}, 100.0, 200.0),
                              Variant::Root(1.0)),
                SequenceValue(SequenceIdentity({"bnd", "raw", "BsG_S11", {"pm10"}}, 200.0, 300.0),
                              Variant::Root(2.0)),
            } << (std::vector<QStringList>{
                QStringList{"DateTimeUTC", "BsG0_S11", "BsG1_S11"},
            }) << (std::vector<QStringList>{
                QStringList{"1970-01-01 00:01:40", "99999.999", "00001.000"},
                QStringList{"1970-01-01 00:03:20", "00002.000", "99999.999"},
            });


        config = CSV();
        config.strictNumbers = true;
        config.explicitMVC = "NA";
        config.useExplicitMVC = true;
        QTest::newRow("Strict numbers") << config <<
            SequenceValue::Transfer{
                SequenceValue(SequenceIdentity({"bnd", "raw", "BsG_S11"}, 100.0, 200.0),
                              Variant::Root(1.0)),
                SequenceValue(SequenceIdentity({"bnd", "raw", "BsG_S11"}, 200.0, 300.0),
                              Variant::Root(2.0)),
                SequenceValue(SequenceIdentity({"bnd", "raw", "BsG_S11"}, 300.0, 400.0),
                              Variant::Root("asdf")),
            } << (std::vector<QStringList>{
                QStringList{"DateTimeUTC", "BsG_S11"},
            }) << (std::vector<QStringList>{
                QStringList{"1970-01-01 00:01:40", "00001.000"},
                QStringList{"1970-01-01 00:03:20", "00002.000"},
                QStringList{"1970-01-01 00:05:00", "NA"},
            });


        Variant::Root stats;
        stats["Count"].setInteger(10);
        stats["StandardDeviation"] = 0.125;
        stats["Mean"] = 0.85;
        stats["Quantiles/@0"] = 0.0;
        stats["Quantiles/@1"] = 100.0;
        stats["Minimum"] = -1.0;
        stats["Maximum"] = 101.0;

        config = CSV();
        config.enableQuantiles = true;
        config.enableBounds = true;
        config.enableCover = true;
        config.enableEnd = true;
        config.useUnweightedMean = true;
        QTest::newRow("Stats generation") << config <<
            SequenceValue::Transfer{
                SequenceValue(SequenceIdentity({"bnd", "raw", "BsG_S11"}, 100.0, 200.0),
                              Variant::Root(99.0)),
                SequenceValue(SequenceIdentity({"bnd", "raw", "BsG_S11", {"end"}}, 100.0, 200.0),
                              Variant::Root(2.0)),
                SequenceValue(SequenceIdentity({"bnd", "raw", "BsG_S11", {"cover"}}, 100.0, 200.0),
                              Variant::Root(0.15)),
                SequenceValue(SequenceIdentity({"bnd", "raw", "BsG_S11", {"stats"}}, 100.0, 200.0),
                              stats),
            } << (std::vector<QStringList>{
                QStringList{"DateTimeUTC", "BsG_S11",
                            "BsGe_S11", "BsGg_S11", "BsGN_S11", "BsGNr_S11",
                            "BsGq00135_S11", "BsGq00621_S11", "BsGq02275_S11", "BsGq05000_S11",
                            "BsGq06681_S11", "BsGq15866_S11", "BsGq25000_S11", "BsGq30854_S11",
                            "BsGq50000_S11", "BsGq69146_S11", "BsGq75000_S11", "BsGq84134_S11",
                            "BsGq93319_S11", "BsGq95000_S11", "BsGq97725_S11", "BsGq99379_S11",
                            "BsGq99865_S11", "BsGqMIN_S11", "BsGqMAX_S11",},
            }) << (std::vector<QStringList>{
                QStringList{"1970-01-01 00:01:40", "00000.850",
                            "00002.000", "00000.125", "00010", "0.15",
                            "00000.135", "00000.621", "00002.275", "00005.000",
                            "00006.681", "00015.866", "00025.000", "00030.854",
                            "00050.000", "00069.146", "00075.000", "00084.134",
                            "00093.319", "00095.000", "00097.725", "00099.379",
                            "00099.865", "-0001.000", "00101.000",},
            });


        Variant::Root meta;
        meta["*rDescription"] = "A description";
        meta["*rMVC"] = "ZZZ";
        meta["*rFormat"] = "0.00";
        meta["*rWavelength"] = 100.0;
        meta["*rSource/Manufacturer"] = "MFG";

        config = CSV();
        config.headerWavelength = true;
        config.headerDescription = true;
        config.headerMVCs = true;
        QTest::newRow("Metadata") << config <<
            SequenceValue::Transfer{
                SequenceValue(SequenceIdentity({"bnd", "raw_meta", "BsG_S11"}, 100.0, 300.0),
                              meta),
                SequenceValue(SequenceIdentity({"bnd", "raw", "BsG_S11"}, 100.0, 200.0),
                              Variant::Root(2.0)),
                SequenceValue(SequenceIdentity({"bnd", "raw", "BsG_S11"}, 200.0, 300.0),
                              Variant::Root(FP::undefined())),
            } << (std::vector<QStringList>{
                QStringList{"NA", "100;0;MFG"},
                QStringList{
                    QObject::tr("Date String (YYYY-MM-DD hh:mm:ss) UTC", "description header"),
                    "A description"},
                QStringList{"9999-99-99 99:99:99", "ZZZ"},
                QStringList{"DateTimeUTC", "BsG_S11"},
            }) << (std::vector<QStringList>{
                QStringList{"1970-01-01 00:01:40", "2.00"},
                QStringList{"1970-01-01 00:03:20", "ZZZ"},
            });
    }
    // @formatter:on

};

QTEST_MAIN(TestOutputCSV)

#include "csv.moc"
