/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CPD3FILEOUTPUT_EBAS_HXX
#define CPD3FILEOUTPUT_EBAS_HXX

#include "core/first.hxx"

#include "fileoutput.hxx"
#include "engine.hxx"
#include "datacore/stream.hxx"
#include "datacore/segment.hxx"
#include "datacore/streampipeline.hxx"

namespace CPD3 {
namespace Output {

/**
 * The configuration for a EBAS output file generator.
 */
struct CPD3FILEOUTPUT_EXPORT EBAS {
    double start;
    double end;
    CPD3::Data::SequenceName::Component station;
    CPD3::Data::ValueSegment::Transfer config;
    std::string typeRestrict;

    std::function<std::unique_ptr<QFile>(const std::string &fileName)> createOutput;

    EBAS();

    /**
     * Create the first output stage.
     *
     * @param config    the output configuration
     * @return          the first output stage
     */
    static std::unique_ptr<Engine::Stage> stage(EBAS config);

    /**
     * Configure a pipeline to generate the required data for a configuration.
     *
     * @param firstStage    the first stage
     * @param target        the pipeline to configure
     * @return              true on success
     */
    static bool configurePipeline(const Engine::Stage *firstStage,
                                  CPD3::Data::StreamPipeline *target);
};

}
}

#endif //CPD3FILEOUTPUT_EBAS_HXX
