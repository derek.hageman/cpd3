/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CPD3FILEOUTPUT_CSV_HXX
#define CPD3FILEOUTPUT_CSV_HXX

#include "core/first.hxx"

#include <functional>

#include "engine.hxx"
#include "datacore/dynamictimeinterval.hxx"

namespace CPD3 {
namespace Output {

/**
 * The configuration for a CSV style output generator stage chain.
 */
struct CPD3FILEOUTPUT_EXPORT CSV {
    bool station;
    bool timeEpoch;
    bool timeExcel;
    bool timeISO;
    bool timeFYear;
    bool timeJulian;
    bool timeYear;
    bool timeDOY;

    bool cutString;
    bool enableStdDev;
    bool enableCount;
    bool enableCover;
    bool enableEnd;
    bool enableQuantiles;
    bool enableBounds;
    bool useUnweightedMean;
    bool splitOutputPaths;

    bool useExplicitMVC;
    QString explicitMVC;

    enum class CutSplitMode : int {
        Automatic, Always, Never
    } cutSplitMode;

    enum class MVCFlagMode : int {
        None, Follow, End
    } mvcFlagMode;

    enum class FlagGenerateMode : int {
        Default, HexFlags, Breakdown, List
    } flagGenerateMode;

    bool strictNumbers;
    bool useNumericFormat;
    QString numericFormat;

#ifdef Q_OS_UNIX
    bool headerNamesStdErr;
#endif

    bool headerNames;
    bool headerDescription;
    bool headerWavelength;
    bool headerFlags;
    bool headerMVCs;
    bool headerCutSize;

    bool longStationHeaderName;

    std::function<void(QStringList &&)> outputHeader;
    std::function<void(QStringList &&)> outputLine;

    CPD3::Data::DynamicTimeInterval *fillThreshold;
    CPD3::Data::DynamicTimeInterval *squashThreshold;

    CSV();

    /**
     * Create the first output stage.
     *
     * @param config    the output configuration
     * @return          the first output stage
     */
    static std::unique_ptr<Engine::Stage> stage(CSV config);
};

}
}

#endif //CPD3FILEOUTPUT_CSV_HXX
