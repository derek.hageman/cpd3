/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <netcdf.h>
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>

#include <QLoggingCategory>
#include <QRegularExpression>

#include "netcdf.hxx"
#include "fragment.hxx"
#include "variablevalue.hxx"
#include "fanout.hxx"
#include "core/environment.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "luascript/libs/variant.hxx"
#include "luascript/libs/sequencename.hxx"


Q_LOGGING_CATEGORY(log_fileoutput_netcdf, "cpd3.fileoutput.netcdf", QtWarningMsg)


using namespace CPD3::Data;

namespace CPD3 {
namespace Output {
namespace {

class FileStructureElement;

class SubstitutionStructure;

class GroupStructure;

class TimeDimensionStructure;

class FragmentDimensionStructure;

class FixedDimensionStructure;

class FileOutputElement;

struct StructureDynamicState {
    Lua::Engine scriptEngine;

    double start;
    double end;
    double modified;

    SequenceName::Component station;

    explicit StructureDynamicState(const NetCDF &config) : start(config.start),
                                                           end(config.end),
                                                           modified(Time::time()),
                                                           station(config.station)
    {
        Lua::Engine::Frame root(scriptEngine);
        {
            Lua::Engine::Assign assign(root, root.global(), "print");
            assign.push(std::function<void(Lua::Engine::Entry &)>([](Lua::Engine::Entry &entry) {
                for (std::size_t i = 0, max = entry.size(); i < max; i++) {
                    qCInfo(log_fileoutput_netcdf) << entry[i].toOutputString();
                }
            }));
        }
        {
            Lua::Engine::Assign assign(root, root.global(), "MODIFIED");
            assign.push(modified);
        }
        {
            Lua::Engine::Assign assign(root, root.global(), "STATION");
            assign.push(station);
        }
    }

    ~StructureDynamicState() = default;

    std::unique_ptr<Lua::Engine::Frame> scriptFrame()
    {
        std::unique_ptr<Lua::Engine::Frame> root(new Lua::Engine::Frame(scriptEngine));
        {
            Lua::Engine::Assign assign(*root, root->global(), "START");
            assign.push(start);
        }
        {
            Lua::Engine::Assign assign(*root, root->global(), "END");
            assign.push(end);
        }
        return std::move(root);
    }
};

struct StructureAssemblyContext {
    NetCDF &config;
    FragmentTracker &fragment;
    std::shared_ptr<StructureDynamicState> dynamic;

    std::vector<std::unique_ptr<FileStructureElement>> structure;

    StructureAssemblyContext(NetCDF &config,
                             FragmentTracker &fragment,
                             std::shared_ptr<StructureDynamicState> dynamic);

    ~StructureAssemblyContext();

    /* Use ordered containers, since the NetCDF file preserves definition order */
    struct Unique {
        Variant::Path path;

        std::unordered_set<std::string> substitutions;
        std::set<std::string> attributes;

        struct Variable {
            std::set<std::string> attributes;
        };
        std::map<std::string, Variable> dataVariables;
        std::map<std::string, Variable> fragmentVariables;

        struct Dimension {
            std::map<std::string, Variable> coordinates;
        };
        std::map<std::string, Dimension> timeDimensions;
        std::map<std::string, Dimension> fragmentDimensions;
        std::map<std::string, Dimension> realDimensions;
        std::map<std::string, Dimension> integerDimensions;
        std::map<std::string, Dimension> stringDimensions;

        std::map<std::string, std::unique_ptr<Unique>> groups;
    };

    struct State {
        GroupStructure *group;
        ValueSegment::Transfer base;

        std::unordered_map<std::string, SubstitutionStructure *> substitutions;
        std::unordered_map<std::string, TimeDimensionStructure *> timeDimensions;
        std::unordered_map<std::string, FragmentDimensionStructure *> fragmentDimensions;
        std::unordered_map<std::string, FixedDimensionStructure *> fixedDimensions;

        State(const State &parent, ValueSegment::Transfer config, GroupStructure *group);

        explicit State(ValueSegment::Transfer config);

        ~State();

        static ValueSegment::Transfer deriveConfig(const ValueSegment::Transfer &config,
                                                   const Variant::Path &path)
        {
            ValueSegment::Transfer result;
            for (const auto &add : config) {
                result.emplace_back(add.getStart(), add.getEnd(),
                                    Variant::Root(add.read().getPath(path)));
            }
            return result;
        }

        ValueSegment::Transfer deriveConfig(const Variant::Path &path) const
        { return deriveConfig(base, path); }

        ValueSegment::Transfer deriveConfig(std::string first) const
        {
            return deriveConfig({{Variant::PathElement::Type::Hash, std::move(first)}});
        }

        ValueSegment::Transfer deriveConfig(std::string first, std::string second) const
        {
            return deriveConfig({{Variant::PathElement::Type::Hash, std::move(first)},
                                 {Variant::PathElement::Type::Hash, std::move(second)}});
        }

        ValueSegment::Transfer deriveConfig(std::string first,
                                            std::string second,
                                            std::string third) const
        {
            return deriveConfig({{Variant::PathElement::Type::Hash, std::move(first)},
                                 {Variant::PathElement::Type::Hash, std::move(second)},
                                 {Variant::PathElement::Type::Hash, std::move(third)}});
        }
    };
};

using Dependency = Variant::Flag;
using DependencySet = Variant::Flags;

struct OutputAssemblyContext {
    NetCDF &config;
    const FragmentTracker::Result &structure;

    int ncid;
    std::unique_ptr<QFile> outputFile;

    std::vector<std::unique_ptr<FileOutputElement>> output;

    OutputAssemblyContext(NetCDF &config, const FragmentTracker::Result &structure);

    ~OutputAssemblyContext();

    bool enabled(const GroupStructure *parent) const;

    void provideDependencies(GroupStructure *parent, const DependencySet &provide);

    int parent(const GroupStructure *parent) const;
};

class FileStructureElement {
public:
    FileStructureElement(const ValueSegment::Transfer &,
                         StructureAssemblyContext &,
                         const StructureAssemblyContext::State &)
    { }

    virtual ~FileStructureElement() = default;

    virtual Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                         const Archive::Selection::Match &defaultArchives = {},
                                                         const Archive::Selection::Match &defaultVariables = {}) const
    { return {}; }

    virtual std::vector<StreamSink *> getFirstStageTargets(const SequenceName &name)
    { return {}; }

    virtual void structureExtend()
    { }

    virtual FragmentTracker::MergeTestList structureMerge()
    { return {}; }

    virtual void structureFinalize()
    { }

    virtual void prepareOutput(OutputAssemblyContext &)
    { }

    virtual bool assembleOutput(OutputAssemblyContext &)
    { return true; }
};

class FileOutputElement {
public:
    explicit FileOutputElement(OutputAssemblyContext &)
    { }

    virtual ~FileOutputElement() = default;

    virtual std::vector<StreamSink *> getOutputTargets(const SequenceName &)
    { return {}; }

    virtual bool initializeOutput(OutputAssemblyContext &)
    { return true; }

    virtual bool finishOutput()
    { return true; }
};


class SubstitutionStructure final : public FileStructureElement {
    VariableValue::Base value;
    bool enforceSplit;
public:
    SubstitutionStructure(const ValueSegment::Transfer &config,
                          StructureAssemblyContext &context,
                          const StructureAssemblyContext::State &state) : FileStructureElement(
            config, context, state), value(context.fragment), enforceSplit(false)
    {
        for (const auto &seg : config) {
            if (seg["EnforceSplit"].exists()) {
                enforceSplit = seg["EnforceSplit"].toBoolean();
            }
            value.addInput(std::make_shared<VariableValue::Base::DataInput>(seg.read()),
                           seg.getStart(), seg.getEnd());
        }
        value.endConfigure();
    }

    virtual ~SubstitutionStructure() = default;

    Variant::Read get(const FragmentTracker::Contents &contents) const
    { return value.get(contents); }

    Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                 const Archive::Selection::Match &defaultArchives = {},
                                                 const Archive::Selection::Match &defaultVariables = {}) const override
    { return value.toArchiveSelections(defaultStations, defaultArchives, defaultVariables); }

    std::vector<StreamSink *> getFirstStageTargets(const SequenceName &name) override
    { return value.getTargets(name); }

    void structureExtend() override
    { value.extendDefined(); }

    FragmentTracker::MergeTestList structureMerge() override
    {
        if (!enforceSplit)
            return {};
        return {{[this](const FragmentTracker::Key &key,
                        FragmentTracker::Value *,
                        FragmentTracker::Value *) {
            return key == value.getKey();
        }, [this](const FragmentTracker::Key &,
                  FragmentTracker::Value *,
                  FragmentTracker::Value *,
                  const FragmentTracker::MergeTestContext &context) {
            auto v1 = get(context.firstContents);
            if (!Variant::Composite::isDefined(v1))
                return true;
            auto v2 = get(context.secondContents);
            if (!Variant::Composite::isDefined(v2))
                return true;
            return v1 == v2;
        }}};
    }
};

class GeneralValueComponent : public VariableValue::String {
    std::unordered_map<std::string, SubstitutionStructure *> substitutions;
    std::shared_ptr<StructureDynamicState> dynamic;

    class RealProcessing : public VariableValue::Real::RealInput {
        QRegularExpression stage;
    public:
        explicit RealProcessing(const Variant::Read &config) : RealInput(config),
                                                               stage(config["Stage"].toQString(),
                                                                     config["CaseSensitive"].toBoolean()
                                                                     ? QRegularExpression::NoPatternOption
                                                                     : QRegularExpression::CaseInsensitiveOption)
        { }

        virtual ~RealProcessing() = default;

    protected:
        Variant::Read fetch(const Variant::Read &input) override
        {
            for (auto check : input.metadata("Processing").toArray()) {
                if (!Util::exact_match(check.hash("By").toQString(), stage))
                    continue;
                return VariableValue::Real::RealInput::fetch(check);
            }
            return VariableValue::Real::RealInput::fetch(Variant::Read::empty());
        }
    };

    static QString applyValueFormat(const Variant::Read &input, const QStringList &elements)
    {
        int index = 0;
        QString type;
        if (index < elements.size())
            type = elements.at(index++).toLower();

        if (type == "netcdfhistory") {
            QStringList lines;
            for (auto cv : input.metadata("Processing").toArray()) {
                QStringList arguments;
                arguments.append(Time::toISO8601(cv.hash("At").toDouble()));
                arguments.append(cv.hash("By").toQString());
                arguments.append(cv.hash("Revision").toQString());
                arguments.append(cv.hash("Environment").toQString());
                lines.append(arguments.join(","));
            }

            {
                QStringList arguments;
                arguments.append(Time::toISO8601(Time::time()));
                arguments.append("fileoutput_netcdf");
                arguments.append(Environment::revision());
                arguments.append(Environment::describe());
                lines.append(arguments.join(","));
            }

            return lines.join("\n");
        }

        return Variant::Composite::applyFormat(input, elements);
    }

public:
    GeneralValueComponent(FragmentTracker &fragment,
                          const ValueSegment::Transfer &config,
                          StructureAssemblyContext &context,
                          const StructureAssemblyContext::State &state,
                          const std::string &path = {}) : VariableValue::String(fragment),
                                                          substitutions(state.substitutions),
                                                          dynamic(context.dynamic)
    {
        for (const auto &seg : config) {
            auto sconfig = seg.read().getPath(path);
            const auto &type = sconfig["Type"].toString();

            std::shared_ptr<VariableValue::Base::DataInput> input;
            if (Util::equal_insensitive(type, "RealProcessing", "RealFromMetadataProcessing")) {
                input = std::make_shared<RealProcessing>(sconfig);
            } else {
                input = std::make_shared<VariableValue::Real::RealInput>(sconfig);
            }

            addInput(std::move(input), seg.getStart(), seg.getEnd());
        }
        endConfigure();
    }

    GeneralValueComponent(const ValueSegment::Transfer &config,
                          StructureAssemblyContext &context,
                          const StructureAssemblyContext::State &state,
                          const std::string &path = {}) : GeneralValueComponent(context.fragment,
                                                                                config, context,
                                                                                state, path)
    { }

    virtual ~GeneralValueComponent() = default;

    FragmentTracker::MergeTestList mergeTestTypePreserve(bool mergeIfEmpty = false) const
    {
        return {{[this](const FragmentTracker::Key &key,
                        FragmentTracker::Value *,
                        FragmentTracker::Value *) {
            return key == getKey();
        }, [this, mergeIfEmpty](const FragmentTracker::Key &,
                                FragmentTracker::Value *,
                                FragmentTracker::Value *,
                                const FragmentTracker::MergeTestContext &context) {
            auto c1 = get(context.firstContents);
            auto c2 = get(context.secondContents);
            switch (c1.getType()) {
            case Variant::Type::Empty:
                if (!mergeIfEmpty)
                    break;
                if (c2.getType() == Variant::Type::Empty)
                    return true;
                break;
            case Variant::Type::Integer:
                if (c2.getType() != Variant::Type::Integer)
                    return false;
                return c1.toInteger() == c2.toInteger();
            case Variant::Type::Real:
                if (c2.getType() != Variant::Type::Real)
                    return false;
                return FP::equal(c1.toReal(), c2.toReal());
            default:
                break;
            }
            return toString(context.firstContents, c1.toQString()) ==
                    toString(context.secondContents, c2.toQString());
        }}};
    }

    bool setAttribute(const FragmentTracker::Contents &contents,
                      const QString &name,
                      int ncid,
                      int varid = NC_GLOBAL,
                      bool writeUndefined = false)
    {
        if (name.isEmpty())
            return true;

        auto val = get(contents);
        switch (val.getType()) {
        case Variant::Type::Integer: {
            long long wr = static_cast<long long>(val.toInteger());
            if (!INTEGER::defined(wr)) {
                if (!writeUndefined)
                    return true;
                wr = NC_FILL_INT64;
            }
            int err = -1;
            if ((err = ::nc_put_att_longlong(ncid, varid, name.toUtf8().constData(), NC_INT64, 1,
                                             &wr)) != NC_NOERR) {
                qCInfo(log_fileoutput_netcdf) << "Failed to set attribute" << name << ':'
                                              << ::nc_strerror(err);
                return false;
            }
            break;
        }
        case Variant::Type::Real: {
            double wr = val.toReal();
            if (!FP::defined(wr)) {
                if (!writeUndefined)
                    return true;
                wr = NC_FILL_DOUBLE;
            }
            int err = -1;
            if ((err = ::nc_put_att_double(ncid, varid, name.toUtf8().constData(), NC_DOUBLE, 1,
                                           &wr)) != NC_NOERR) {
                qCInfo(log_fileoutput_netcdf) << "Failed to set attribute" << name << ':'
                                              << ::nc_strerror(err);
                return false;
            }
            break;
        }
        default: {
            auto wr = toString(contents, val.toQString()).toUtf8();
            if (wr.isEmpty()) {
                if (!writeUndefined)
                    return true;
            }
            int err = -1;
            if ((err = ::nc_put_att_text(ncid, varid, name.toUtf8().constData(),
                                         static_cast<::size_t>(wr.length()), wr.constData())) !=
                    NC_NOERR) {
                qCInfo(log_fileoutput_netcdf) << "Failed to set attribute" << name << ':'
                                              << ::nc_strerror(err);
                return false;
            }
            break;
        }
        }

        return true;
    }

protected:
    virtual const FragmentTracker::Contents &substitutionContext(const FragmentTracker::Contents &contents) const
    { return contents; }

    std::unique_ptr<
            Lua::Engine::Frame> scriptFrame(const FragmentTracker::Contents &contents) const override
    {
        auto root = dynamic->scriptFrame();
        const auto &context = substitutionContext(contents);
        for (const auto &add : substitutions) {
            Lua::Engine::Assign assign(*root, root->global(), Util::to_upper(add.first));
            assign.pushData<Lua::Libs::Variant>(Variant::Root(add.second->get(context)).write());
        }
        return std::move(root);
    }

    QString applySubstitution(const QStringList &elements,
                              const FragmentTracker::Contents &contents) const override
    {
        auto lk = Util::to_lower(elements.front().toStdString());

        if (lk == "start") {
            return TextSubstitution::formatTime(dynamic->start, elements.mid(1), false);
        } else if (lk == "end") {
            return TextSubstitution::formatTime(dynamic->end, elements.mid(1), false);
        } else if (lk == "modified") {
            return TextSubstitution::formatTime(dynamic->modified, elements.mid(1), false);
        } else if (lk == "station") {
            return TextSubstitution::formatString(QString::fromStdString(dynamic->station),
                                                  elements.mid(1));
        }

        auto check = substitutions.find(lk);
        if (check == substitutions.end())
            return applyValueFormat(Variant::Read::empty(), elements.mid(1));
        return applyValueFormat(check->second->get(substitutionContext(contents)), elements.mid(1));
    }
};


class OutputNetCDFStructure final : public FileStructureElement {
    GeneralValueComponent name;
public:
    OutputNetCDFStructure(const ValueSegment::Transfer &config,
                          StructureAssemblyContext &context,
                          const StructureAssemblyContext::State &state) : FileStructureElement(
            config, context, state), name(config, context, state, "Filename")
    { }

    virtual ~OutputNetCDFStructure() = default;

    Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                 const Archive::Selection::Match &defaultArchives = {},
                                                 const Archive::Selection::Match &defaultVariables = {}) const override
    { return name.toArchiveSelections(defaultStations, defaultArchives, defaultVariables); }

    std::vector<StreamSink *> getFirstStageTargets(const SequenceName &key) override
    { return name.getTargets(key); }

    FragmentTracker::MergeTestList structureMerge() override
    { return name.mergeTest(); }

    bool assembleOutput(OutputAssemblyContext &context) override
    {
        auto requestedName = name.toString(context.structure.contents).toStdString();
        if (requestedName.empty())
            return false;
        std::unique_ptr<QFile> targetFile(context.config.createOutput(requestedName));
        if (!targetFile)
            return false;
        targetFile->close();

        int globalID = -1;
        int err = 0;
        if ((err = ::nc_create(targetFile->fileName().toUtf8().constData(), NC_NETCDF4 | NC_CLOBBER,
                               &globalID)) != NC_NOERR) {
            qCInfo(log_fileoutput_netcdf) << "Error creating file" << targetFile->fileName() << ':'
                                          << ::nc_strerror(err);
            return false;
        }

        qCDebug(log_fileoutput_netcdf) << "Using" << targetFile->fileName()
                                       << "for NetCDF file output at"
                                       << Logging::range(context.structure.getStart(),
                                                         context.structure.getEnd());

        context.ncid = globalID;
        context.outputFile = std::move(targetFile);

        return true;
    }
};

class GroupStructure final : public FileStructureElement {
    FragmentTracker &fragment;
    GroupStructure *parent;
    GeneralValueComponent name;

    using RequireDependencies = FragmentTracker::StaticValue<DependencySet>;
    FragmentTracker::Key requireDependencies;

    using ExcludeDependencies = FragmentTracker::StaticValue<DependencySet>;
    FragmentTracker::Key excludeDependencies;

    DependencySet availableDependencies;
    int ncid;
public:
    GroupStructure(const ValueSegment::Transfer &config,
                   StructureAssemblyContext &context,
                   const StructureAssemblyContext::State &state) : FileStructureElement(config,
                                                                                        context,
                                                                                        state),
                                                                   fragment(context.fragment),
                                                                   parent(state.group),
                                                                   name(config, context, state,
                                                                        "Name"),
                                                                   requireDependencies(
                                                                           fragment.allocateKey()),
                                                                   excludeDependencies(
                                                                           fragment.allocateKey())
    {
        for (const auto &seg : config) {
            fragment.add(requireDependencies, std::make_shared<RequireDependencies>(
                    seg["Dependencies/Require"].toFlags()), seg.getStart(), seg.getEnd());
            fragment.add(excludeDependencies, std::make_shared<ExcludeDependencies>(
                    seg["Dependencies/Exclude"].toFlags()), seg.getStart(), seg.getEnd());
        }
    }

    virtual ~GroupStructure() = default;

    Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                 const Archive::Selection::Match &defaultArchives = {},
                                                 const Archive::Selection::Match &defaultVariables = {}) const override
    { return name.toArchiveSelections(defaultStations, defaultArchives, defaultVariables); }

    std::vector<StreamSink *> getFirstStageTargets(const SequenceName &key) override
    { return name.getTargets(key); }

    void structureExtend() override
    {
        RequireDependencies::extendOverEmpty(fragment, requireDependencies);
        ExcludeDependencies::extendOverEmpty(fragment, excludeDependencies);
    }

    FragmentTracker::MergeTestList structureMerge() override
    { return name.mergeTest(); }

    inline int getNCID() const
    { return ncid; }

    bool isEnabled(const OutputAssemblyContext &context) const
    {
        if (ncid == -1)
            return false;
        return context.enabled(parent);
    }

    void provideDependencies(OutputAssemblyContext &context, const DependencySet &provide)
    {
        Util::merge(provide, availableDependencies);
        return context.provideDependencies(parent, provide);
    }

    void prepareOutput(OutputAssemblyContext &) override
    {
        availableDependencies.clear();
    }

    bool assembleOutput(OutputAssemblyContext &context) override
    {
        ncid = -1;
        for (const auto &check : RequireDependencies::value(context.structure,
                                                            requireDependencies)) {
            if (!availableDependencies.count(check)) {
                qCDebug(log_fileoutput_netcdf) << "Group omitted due to missing dependency:"
                                               << check;
                return true;
            }
        }
        for (const auto &check : ExcludeDependencies::value(context.structure,
                                                            excludeDependencies)) {
            if (availableDependencies.count(check)) {
                qCDebug(log_fileoutput_netcdf) << "Group omitted due to excluded dependency:"
                                               << check;
                return true;
            }
        }

        if (!context.enabled(parent))
            return true;

        int err = -1;
        auto groupName = name.toString(context.structure.contents);
        if ((err = ::nc_def_grp(context.parent(parent), groupName.toUtf8().constData(), &ncid)) !=
                NC_NOERR) {
            qCInfo(log_fileoutput_netcdf) << "Error creating group" << groupName << ':'
                                          << ::nc_strerror(err);
            return false;
        }

        return true;
    }
};

class GlobalAttributeStructure final : public FileStructureElement {
    GroupStructure *group;
    GeneralValueComponent name;
    GeneralValueComponent value;
    bool alwaysMergeValues;
    bool alwaysExtendValues;
public:
    GlobalAttributeStructure(const ValueSegment::Transfer &config,
                             StructureAssemblyContext &context,
                             const StructureAssemblyContext::State &state) : FileStructureElement(
            config, context, state),
                                                                             group(state.group),
                                                                             name(config, context,
                                                                                  state, "Name"),
                                                                             value(config, context,
                                                                                   state, "Value"),
                                                                             alwaysMergeValues(
                                                                                     false),
                                                                             alwaysExtendValues(
                                                                                     false)
    {
        for (const auto &seg : config) {
            if (seg["Value/AlwaysMerge"].exists()) {
                alwaysMergeValues = seg["Value/AlwaysMerge"].toBoolean();
            }
            if (seg["Value/AlwaysExtend"].exists()) {
                alwaysExtendValues = seg["Value/AlwaysExtend"].toBoolean();
            }
        }
    }

    virtual ~GlobalAttributeStructure() = default;

    Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                 const Archive::Selection::Match &defaultArchives = {},
                                                 const Archive::Selection::Match &defaultVariables = {}) const override
    {
        auto result = name.toArchiveSelections(defaultStations, defaultArchives, defaultVariables);
        Util::append(value.toArchiveSelections(defaultStations, defaultArchives, defaultVariables),
                     result);
        return result;
    }

    std::vector<StreamSink *> getFirstStageTargets(const SequenceName &key) override
    {
        auto result = name.getTargets(key);
        Util::append(value.getTargets(key), result);
        return result;
    }

    void structureExtend() override
    {
        if (alwaysExtendValues) {
            value.extendDefined();
        }
    }

    FragmentTracker::MergeTestList structureMerge() override
    {
        auto result = name.mergeTest();
        if (!alwaysMergeValues) {
            Util::append(value.mergeTestTypePreserve(), result);
        }
        return result;
    }

    bool assembleOutput(OutputAssemblyContext &context) override
    {
        if (!context.enabled(group))
            return true;
        return value.setAttribute(context.structure.contents,
                                  name.toString(context.structure.contents), context.parent(group));
    }
};


class VariableStructure : public FileStructureElement {
    FragmentTracker &fragment;
    GroupStructure *group;
    GeneralValueComponent name;

    using ProvideDependencies = FragmentTracker::StaticValue<DependencySet>;
    FragmentTracker::Key provideDependencies;

    bool exists;
    int varid;
public:
    VariableStructure(const ValueSegment::Transfer &config,
                      StructureAssemblyContext &context,
                      const StructureAssemblyContext::State &state) : FileStructureElement(config,
                                                                                           context,
                                                                                           state),
                                                                      fragment(context.fragment),
                                                                      group(state.group),
                                                                      name(config, context, state,
                                                                           "Name"),
                                                                      provideDependencies(
                                                                              fragment.allocateKey()),
                                                                      exists(false)
    {
        for (const auto &seg : config) {
            fragment.add(provideDependencies, std::make_shared<ProvideDependencies>(
                    seg["Dependencies/Provide"].toFlags()), seg.getStart(), seg.getEnd());
        }
    }

    virtual ~VariableStructure() = default;

    Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                 const Archive::Selection::Match &defaultArchives = {},
                                                 const Archive::Selection::Match &defaultVariables = {}) const override
    { return name.toArchiveSelections(defaultStations, defaultArchives, defaultVariables); }

    std::vector<StreamSink *> getFirstStageTargets(const SequenceName &key) override
    { return name.getTargets(key); }

    void structureExtend() override
    {
        ProvideDependencies::extendOverEmpty(fragment, provideDependencies);
    }

    FragmentTracker::MergeTestList structureMerge() override
    { return name.mergeTest(); }

    inline int getVARID() const
    { return varid; }

    inline bool variableExists() const
    { return exists; }

    void prepareOutput(OutputAssemblyContext &context) override
    {
        context.provideDependencies(group, ProvideDependencies::value(context.structure,
                                                                      provideDependencies));
    }

    bool assembleOutput(OutputAssemblyContext &context) override
    {
        varid = -1;
        exists = false;
        return true;
    }

    class ValueOutput {
        int ncid;
        int varid;
    public:
        enum class Type {
            Real, Integer, String,
        };

        ValueOutput(OutputAssemblyContext &context, VariableStructure &variable) : ncid(
                context.parent(variable.group)), varid(variable.getVARID())
        { }

        virtual ~ValueOutput() = default;

    protected:

        bool writeString(const std::vector<::size_t> &index, const QString &value) const
        {
            QByteArray str(value.toUtf8());
            int err = -1;
            const char *ptr = str.constData();
            if ((err = ::nc_put_var1_string(ncid, varid, &index[0], &ptr)) != NC_NOERR) {
                qCInfo(log_fileoutput_netcdf) << "Error writing string value:"
                                              << ::nc_strerror(err);
                return false;
            }
            return true;
        }

        bool writeReal(const std::vector<::size_t> &index, double value) const
        {
            int err = -1;
            if ((err = ::nc_put_var1_double(ncid, varid, &index[0], &value)) != NC_NOERR) {
                qCInfo(log_fileoutput_netcdf) << "Error writing real value:" << ::nc_strerror(err);
                return false;
            }
            return true;
        }

        bool writeReal(const std::vector<::size_t> &index, const std::vector<double> &values) const
        {
            std::vector<::size_t> countp(index.size(), 1);
            if (!countp.empty())
                countp.back() = values.size();

            int err = -1;
            if ((err = ::nc_put_vara_double(ncid, varid, &index[0], &countp[0], values.data())) !=
                    NC_NOERR) {
                qCInfo(log_fileoutput_netcdf) << "Error writing real values:" << ::nc_strerror(err);
                return false;
            }
            return true;
        }

        bool writeInteger(const std::vector<::size_t> &index, long long value) const
        {
            int err = -1;
            if ((err = ::nc_put_var1_longlong(ncid, varid, &index[0], &value)) != NC_NOERR) {
                qCInfo(log_fileoutput_netcdf) << "Error writing integer value:"
                                              << ::nc_strerror(err);
                return false;
            }
            return true;
        }

        bool writeInteger(const std::vector<::size_t> &index,
                          const std::vector<long long> &values) const
        {
            std::vector<::size_t> countp(index.size(), 1);
            if (!countp.empty())
                countp.back() = values.size();

            int err = -1;
            if ((err = ::nc_put_vara_longlong(ncid, varid, &index[0], &countp[0], values.data())) !=
                    NC_NOERR) {
                qCInfo(log_fileoutput_netcdf) << "Error writing integer values:"
                                              << ::nc_strerror(err);
                return false;
            }
            return true;
        }
    };

    friend class ValueOutput;

protected:

    bool declareVariable(OutputAssemblyContext &context,
                         ValueOutput::Type type,
                         const std::vector<int> &dimensions)
    {
        varid = -1;
        exists = false;

        if (!context.enabled(group))
            return true;

        auto varName = name.toString(context.structure.contents);
        if (varName.isEmpty())
            return false;

        ::nc_type dataType = NC_DOUBLE;
        switch (type) {
        case ValueOutput::Type::Real:
            dataType = NC_DOUBLE;
            break;
        case ValueOutput::Type::Integer:
            dataType = NC_INT64;
            break;
        case ValueOutput::Type::String:
            dataType = NC_STRING;
            break;
        }

        int dummy = 0;
        const int *dimensionsPointer;
        int nDimensions = static_cast<int>(dimensions.size());
        if (nDimensions <= 0)
            dimensionsPointer = &dummy;
        else
            dimensionsPointer = dimensions.data();

        int err = -1;
        if ((err = ::nc_def_var(context.parent(group), varName.toUtf8().constData(), dataType,
                                nDimensions, dimensionsPointer, &varid)) != NC_NOERR) {
            qCInfo(log_fileoutput_netcdf) << "Error creating variable" << varName << ':'
                                          << ::nc_strerror(err);
            return false;
        }

        exists = true;
        return true;
    }
};

class VariableAttributeStructure final : public FileStructureElement {
    GroupStructure *group;
    VariableStructure *variable;
    GeneralValueComponent name;
    GeneralValueComponent value;
    bool alwaysMergeValues;
    bool alwaysExtendValues;
public:
    VariableAttributeStructure(const ValueSegment::Transfer &config,
                               StructureAssemblyContext &context,
                               const StructureAssemblyContext::State &state,
                               VariableStructure *variable) : FileStructureElement(config, context,
                                                                                   state),
                                                              group(state.group),
                                                              variable(variable),
                                                              name(config, context, state, "Name"),
                                                              value(config, context, state,
                                                                    "Value"),
                                                              alwaysMergeValues(false),
                                                              alwaysExtendValues(false)
    {
        Q_ASSERT(variable != nullptr);

        for (const auto &seg : config) {
            if (seg["Value/AlwaysMerge"].exists()) {
                alwaysMergeValues = seg["Value/AlwaysMerge"].toBoolean();
            }
            if (seg["Value/AlwaysExtend"].exists()) {
                alwaysExtendValues = seg["Value/AlwaysExtend"].toBoolean();
            }
        }
    }

    virtual ~VariableAttributeStructure() = default;

    Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                 const Archive::Selection::Match &defaultArchives = {},
                                                 const Archive::Selection::Match &defaultVariables = {}) const override
    {
        auto result = name.toArchiveSelections(defaultStations, defaultArchives, defaultVariables);
        Util::append(value.toArchiveSelections(defaultStations, defaultArchives, defaultVariables),
                     result);
        return result;
    }

    std::vector<StreamSink *> getFirstStageTargets(const SequenceName &key) override
    {
        auto result = name.getTargets(key);
        Util::append(value.getTargets(key), result);
        return result;
    }

    void structureExtend() override
    {
        if (alwaysExtendValues) {
            value.extendDefined();
        }
    }

    FragmentTracker::MergeTestList structureMerge() override
    {
        auto result = name.mergeTest();
        if (!alwaysMergeValues) {
            Util::append(value.mergeTestTypePreserve(), result);
        }
        return result;
    }

    bool assembleOutput(OutputAssemblyContext &context) override
    {
        if (!variable->variableExists())
            return true;
        return value.setAttribute(context.structure.contents,
                                  name.toString(context.structure.contents), context.parent(group),
                                  variable->getVARID());
    }
};


class DimensionStructure : public FileStructureElement {
    GroupStructure *group;
    GeneralValueComponent name;
    bool unlimited;
    enum class ScalarElimination {
        Never, Always, IfUndefined,
    } scalarElimination;
    std::size_t removeSizeThreshold;

    enum class Presence {
        Normal, Scalar, Eliminated,
    };
    Presence presence;
    int dimid;
public:
    DimensionStructure(const ValueSegment::Transfer &config,
                       StructureAssemblyContext &context,
                       const StructureAssemblyContext::State &state) : FileStructureElement(config,
                                                                                            context,
                                                                                            state),
                                                                       group(state.group),
                                                                       name(config, context, state,
                                                                            "Name"),
                                                                       unlimited(false),
                                                                       scalarElimination(
                                                                               ScalarElimination::Never),
                                                                       removeSizeThreshold(unlimited
                                                                                           ? static_cast<std::size_t>(-1)
                                                                                           : static_cast<std::size_t>(1))
    {
        for (const auto &seg : config) {
            if (seg["Unlimited"].exists()) {
                unlimited = seg["Unlimited"].toBoolean();
            }

            auto s = seg["RemoveSizeThreshold"].toInteger();
            if (INTEGER::defined(s)) {
                if (s < 0) {
                    removeSizeThreshold = static_cast<std::size_t>(-1);
                } else {
                    removeSizeThreshold = static_cast<std::size_t>(s);
                }
            }
            if (seg["AlwaysRemove"].toBoolean()) {
                removeSizeThreshold = std::numeric_limits<std::size_t>::max() - 1;
            }

            if (seg["ScalarRemove"].exists()) {
                const auto &check = seg["ScalarRemove"].toString();
                if (Util::equal_insensitive(check, "always", "eliminate")) {
                    scalarElimination = ScalarElimination::Always;
                } else if (Util::equal_insensitive(check, "undefined", "missing")) {
                    scalarElimination = ScalarElimination::IfUndefined;
                } else {
                    scalarElimination = ScalarElimination::Never;
                }
            }
        }
    }

    virtual ~DimensionStructure() = default;

    Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                 const Archive::Selection::Match &defaultArchives = {},
                                                 const Archive::Selection::Match &defaultVariables = {}) const override
    { return name.toArchiveSelections(defaultStations, defaultArchives, defaultVariables); }

    std::vector<StreamSink *> getFirstStageTargets(const SequenceName &key) override
    { return name.getTargets(key); }

    FragmentTracker::MergeTestList structureMerge() override
    { return name.mergeTest(); }

    inline int getDIMID() const
    { return dimid; }

    bool assembleOutput(OutputAssemblyContext &context) override
    {
        presence = Presence::Eliminated;
        dimid = -1;

        if (!context.enabled(group))
            return true;

        if (scalarElimination == ScalarElimination::IfUndefined) {
            if (isAllUndefined(context))
                return true;
        }

        ::size_t len = NC_UNLIMITED;
        {
            auto size = getDimensionSize(context);
            if (!unlimited && size != static_cast<std::size_t>(-1)) {
                len = static_cast<::size_t>(size);
                if (removeSizeThreshold != static_cast<std::size_t>(-1)) {
                    if (size <= removeSizeThreshold) {
                        if (scalarElimination != ScalarElimination::Always && size != 0) {
                            presence = Presence::Scalar;
                        }
                        return true;
                    }
                } else if (size == 0) {
                    len = NC_UNLIMITED;
                }
            }
        }

        auto dimensionName = name.toString(context.structure.contents);
        if (dimensionName.isEmpty())
            return true;

        int err = 0;
        if ((err = ::nc_def_dim(context.parent(group), dimensionName.toUtf8().constData(), len,
                                &dimid)) != NC_NOERR) {
            qCInfo(log_fileoutput_netcdf) << "Error declaring dimension" << dimensionName << ":"
                                          << ::nc_strerror(err);
            return false;
        }

        presence = Presence::Normal;
        return true;
    }

    inline bool isEliminated() const
    { return presence == Presence::Eliminated; }

    inline bool isScalar() const
    { return presence == Presence::Scalar; }

    inline bool isIndexed() const
    { return presence == Presence::Normal; }

protected:

    virtual std::size_t getDimensionSize(OutputAssemblyContext &context) const = 0;

    virtual bool isAllUndefined(OutputAssemblyContext &) const
    { return false; }
};


class TimeDimensionStructure final : public DimensionStructure {
public:
    class SegmentTarget {
    public:
        virtual void processSegment(SequenceSegment &segment) = 0;

        enum class InputType {
            Unused, Fragment, Inject
        };

        virtual InputType getInputType(const SequenceName &name) = 0;
    };

    class Output : public FileOutputElement {
        SequenceSegment::Stream stream;

        class FragmentSink : public CPD3::Data::StreamSink {
            Output &parent;
        public:
            explicit FragmentSink(Output &parent) : parent(parent)
            { }

            virtual ~FragmentSink() = default;

            void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override
            {
                for (auto &add : parent.stream.add(values)) {
                    parent.processSegment(std::move(add));
                }
            }

            void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override
            {
                for (auto &add : parent.stream.add(std::move(values))) {
                    parent.processSegment(std::move(add));
                }
            }

            void incomingData(const CPD3::Data::SequenceValue &value) override
            {
                for (auto &add : parent.stream.add(value)) {
                    parent.processSegment(std::move(add));
                }
            }

            void incomingData(CPD3::Data::SequenceValue &&value) override
            {
                for (auto &add : parent.stream.add(std::move(value))) {
                    parent.processSegment(std::move(add));
                }
            }

            void endData() override
            { }
        };

        friend class FragmentSink;

        class InjectSink : public CPD3::Data::StreamSink {
            Output &parent;
        public:
            explicit InjectSink(Output &parent) : parent(parent)
            { }

            virtual ~InjectSink() = default;

            void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override
            {
                for (auto &add : parent.stream.inject(values)) {
                    parent.processSegment(std::move(add));
                }
            }

            void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override
            {
                for (auto &add : parent.stream.inject(std::move(values))) {
                    parent.processSegment(std::move(add));
                }
            }

            void incomingData(const CPD3::Data::SequenceValue &value) override
            {
                for (auto &add : parent.stream.inject(value)) {
                    parent.processSegment(std::move(add));
                }
            }

            void incomingData(CPD3::Data::SequenceValue &&value) override
            {
                for (auto &add : parent.stream.inject(std::move(value))) {
                    parent.processSegment(std::move(add));
                }
            }

            void endData() override
            { }
        };

        friend class MetadataSink;

        FragmentSink fragmentSink;
        InjectSink injectSink;

        std::vector<SegmentTarget *> targets;

        void processSegment(SequenceSegment &&segment)
        {
            for (auto t : targets) {
                t->processSegment(segment);
            }
        }

    public:
        Output(OutputAssemblyContext &context, TimeDimensionStructure &dimension)
                : FileOutputElement(context), stream(), fragmentSink(*this), injectSink(*this)
        {

        }

        virtual ~Output() = default;

        std::vector<StreamSink *> getOutputTargets(const SequenceName &name) override
        {
            bool injected = false;
            for (auto check : targets) {
                switch (check->getInputType(name)) {
                case SegmentTarget::InputType::Unused:
                    break;
                case SegmentTarget::InputType::Fragment:
                    return {&fragmentSink};
                case SegmentTarget::InputType::Inject:
                    injected = true;
                    break;
                }
            }
            if (injected)
                return {&injectSink};
            return {};
        }

        bool finishOutput() override
        {
            for (auto &add : stream.finish()) {
                processSegment(std::move(add));
            }
            return true;
        }

        void attach(SegmentTarget *target)
        {
            targets.emplace_back(target);
        }
    };

private:
    Output *output;

public:
    TimeDimensionStructure(const ValueSegment::Transfer &config,
                           StructureAssemblyContext &context,
                           const StructureAssemblyContext::State &state) : DimensionStructure(
            config, context, state), output(nullptr)
    { }

    virtual ~TimeDimensionStructure() = default;

    Output *getCurrentOutput(OutputAssemblyContext &) const
    { return output; }

    bool assembleOutput(OutputAssemblyContext &context) override
    {
        if (!DimensionStructure::assembleOutput(context))
            return false;

        std::unique_ptr<Output> o(new Output(context, *this));
        output = o.get();
        context.output.emplace_back(std::move(o));
        return true;
    }

protected:
    std::size_t getDimensionSize(OutputAssemblyContext &) const override
    { return static_cast<std::size_t>(-1); }
};

enum class EpochTimeOrigin : std::uint_fast8_t {
    Start, End, Middle
};
typedef DynamicPrimitive<EpochTimeOrigin> DynamicEpochTimeOrigin;

static EpochTimeOrigin toEpochTimeOrigin(const Variant::Read &v)
{
    const auto &str = v.toString();
    if (Util::equal_insensitive(str, "end")) {
        return EpochTimeOrigin::End;
    } else if (Util::equal_insensitive(str, "middle")) {
        return EpochTimeOrigin::Middle;
    } else {
        return EpochTimeOrigin::Start;
    }
}

QDataStream &operator<<(QDataStream &stream, EpochTimeOrigin value)
{
    stream << static_cast<quint8>(value);
    return stream;
}

QDataStream &operator>>(QDataStream &stream, EpochTimeOrigin &value)
{
    quint8 v;
    stream >> v;
    value = static_cast<EpochTimeOrigin>(v);
    return stream;
}

QDebug operator<<(QDebug stream, EpochTimeOrigin value)
{ return stream; }

class TimeCoordinateVariableStructure final : public VariableStructure {
    class Output
            : public FileOutputElement,
              public VariableStructure::ValueOutput,
              virtual public TimeDimensionStructure::SegmentTarget {
        std::unique_ptr<DynamicEpochTimeOrigin> origin;
        std::size_t offset;
        bool isOk;
        std::vector<double> buffer;

        static constexpr std::size_t bufferSize = 1024;

        void flush()
        {
            if (buffer.empty())
                return;

            std::vector<::size_t> index(1);
            index.back() = offset;

            if (!writeReal(index, buffer))
                isOk = false;
            offset += buffer.size();
            buffer.clear();
        }

        void append(double value)
        {
            if (!FP::defined(value))
                value = NC_FILL_DOUBLE;

            buffer.emplace_back(value);
            if (buffer.size() < bufferSize)
                return;
            flush();
        }

    public:
        Output(OutputAssemblyContext &context, TimeCoordinateVariableStructure &parent)
                : FileOutputElement(context),
                  VariableStructure::ValueOutput(context, parent),
                  origin(parent.timeOrigin->clone()),
                  offset(0),
                  isOk(true)
        { }

        virtual ~Output() = default;

        void processSegment(SequenceSegment &segment) override
        {
            switch (origin->get(segment)) {
            case EpochTimeOrigin::Start:
                append(segment.getStart());
                break;
            case EpochTimeOrigin::End:
                append(segment.getEnd());
                break;
            case EpochTimeOrigin::Middle:
                if (!FP::defined(segment.getEnd()))
                    append(segment.getStart());
                else if (!FP::defined(segment.getStart()))
                    append(segment.getEnd());
                else
                    append((segment.getStart() + segment.getEnd()) * 0.5);
                break;
            }
        }

        bool finishOutput() override
        {
            flush();
            return isOk;
        }

        InputType getInputType(const SequenceName &) override
        { return InputType::Unused; }
    };

    friend class Output;

    TimeDimensionStructure *dim;
    std::unique_ptr<DynamicEpochTimeOrigin> timeOrigin;

public:
    TimeCoordinateVariableStructure(const ValueSegment::Transfer &config,
                                    StructureAssemblyContext &context,
                                    const StructureAssemblyContext::State &state,
                                    TimeDimensionStructure *dim) : VariableStructure(config,
                                                                                     context,
                                                                                     state),
                                                                   dim(dim),
                                                                   timeOrigin(
                                                                           DynamicEpochTimeOrigin::fromConfiguration(
                                                                                   toEpochTimeOrigin,
                                                                                   config,
                                                                                   "Origin"))
    {
        Q_ASSERT(dim != nullptr);
    }

    virtual ~TimeCoordinateVariableStructure() = default;

    bool assembleOutput(OutputAssemblyContext &context) override
    {
        if (!VariableStructure::assembleOutput(context))
            return false;
        if (!declareVariable(context, ValueOutput::Type::Real, {dim->getDIMID()}))
            return false;

        std::unique_ptr<Output> output(new Output(context, *this));
        dim->getCurrentOutput(context)->attach(output.get());
        context.output.emplace_back(std::move(output));
        return true;
    }
};


class FragmentDimensionStructure final : public DimensionStructure {
public:
    class Value final : public GeneralValueComponent {
        friend class FragmentDimensionStructure;

        const FragmentTracker::Contents *structureContents;

        Value(FragmentDimensionStructure &dimension,
              const ValueSegment::Transfer &config,
              StructureAssemblyContext &context,
              const StructureAssemblyContext::State &state,
              const std::string &path = {}) : GeneralValueComponent(dimension.fragmentTracker,
                                                                    config, context, state, path),
                                              structureContents(nullptr)
        { }

    public:

        void activate(OutputAssemblyContext &context)
        {
            structureContents = &context.structure.contents;
        }

        void deactivate(OutputAssemblyContext &)
        {
            structureContents = nullptr;
        }

    protected:
        const FragmentTracker::Contents &substitutionContext(const FragmentTracker::Contents &contents) const override
        {
            Q_ASSERT(structureContents != nullptr);
            return *structureContents;
        }
    };

    friend class Value;

private:
    FragmentTracker fragmentTracker;
    FragmentTracker::MergeTestList mergeTests;
    FragmentTracker::Fragments globalFragments;
    FragmentTracker::Fragments current;
public:
    FragmentDimensionStructure(const ValueSegment::Transfer &config,
                               StructureAssemblyContext &context,
                               const StructureAssemblyContext::State &state) : DimensionStructure(
            config, context, state)
    { }

    virtual ~FragmentDimensionStructure() = default;

    void structureFinalize() override
    {
        fragmentTracker.merge(mergeTests);
        globalFragments = fragmentTracker.getFragments();
    }

    bool assembleOutput(OutputAssemblyContext &context) override
    {
        auto effective = Range::findAllIntersecting(globalFragments, context.structure);
        current = FragmentTracker::Fragments(effective.first, effective.second);
        if (!current.empty()) {
            if (Range::compareStart(current.front().getStart(), context.structure.getStart()) < 0)
                current.front().setStart(context.structure.getStart());
            if (Range::compareEnd(current.back().getEnd(), context.structure.getEnd()) > 0)
                current.back().setEnd(context.structure.getEnd());
        }

        return DimensionStructure::assembleOutput(context);
    }

    const FragmentTracker::Fragments &getResult() const
    { return current; }

    std::unique_ptr<Value> attach(const ValueSegment::Transfer &config,
                                  StructureAssemblyContext &context,
                                  const StructureAssemblyContext::State &state,
                                  const std::string &path = {})
    {
        std::unique_ptr<Value> value(new Value(*this, config, context, state, path));

        bool doMerge = false;
        for (const auto &seg : config) {
            if (seg["AlwaysMerge"].exists())
                doMerge = seg["AlwaysMerge"].toBoolean();
        }
        if (!doMerge) {
            Util::append(value->mergeTestTypePreserve(true), mergeTests);
        }

        return std::move(value);
    }

protected:
    std::size_t getDimensionSize(OutputAssemblyContext &context) const override
    { return current.size(); }

    bool isAllUndefined(OutputAssemblyContext &) const override
    {
        if (current.size() != 1)
            return false;
        return !FP::defined(current.front().getStart());
    }
};

class FragmentCoordinateVariableStructure final : public VariableStructure {
    class Output : public FileOutputElement, public VariableStructure::ValueOutput {
        std::vector<double> values;
    public:
        Output(OutputAssemblyContext &context, FragmentCoordinateVariableStructure &parent)
                : FileOutputElement(context), VariableStructure::ValueOutput(context, parent)
        {
            for (const auto &segment : parent.dim->getResult()) {
                double v = FP::undefined();
                switch (parent.timeOrigin->getConst(segment)) {
                case EpochTimeOrigin::Start:
                    v = segment.getStart();
                    break;
                case EpochTimeOrigin::End:
                    v = segment.getEnd();
                    break;
                case EpochTimeOrigin::Middle:
                    if (!FP::defined(segment.getEnd()))
                        v = segment.getStart();
                    else if (!FP::defined(segment.getStart()))
                        v = segment.getEnd();
                    else
                        v = (segment.getStart() + segment.getEnd()) * 0.5;
                    break;
                }
                if (!FP::defined(v))
                    v = NC_FILL_DOUBLE;
                values.emplace_back(v);
            }
        }

        virtual ~Output() = default;

        bool initializeOutput(OutputAssemblyContext &) override
        { return writeReal({0}, values); }
    };

    friend class Output;

    FragmentDimensionStructure *dim;
    std::unique_ptr<DynamicEpochTimeOrigin> timeOrigin;

public:
    FragmentCoordinateVariableStructure(const ValueSegment::Transfer &config,
                                        StructureAssemblyContext &context,
                                        const StructureAssemblyContext::State &state,
                                        FragmentDimensionStructure *dim) : VariableStructure(config,
                                                                                             context,
                                                                                             state),
                                                                           dim(dim),
                                                                           timeOrigin(
                                                                                   DynamicEpochTimeOrigin::fromConfiguration(
                                                                                           toEpochTimeOrigin,
                                                                                           config,
                                                                                           "Origin"))
    {
        Q_ASSERT(dim != nullptr);
    }

    virtual ~FragmentCoordinateVariableStructure() = default;

    bool assembleOutput(OutputAssemblyContext &context) override
    {
        if (!dim->isIndexed())
            return true;
        if (!VariableStructure::assembleOutput(context))
            return false;
        if (!declareVariable(context, ValueOutput::Type::Real, {dim->getDIMID()}))
            return false;

        std::unique_ptr<Output> output(new Output(context, *this));
        context.output.emplace_back(std::move(output));
        return true;
    }
};


static const std::string emptyString;

/*
 * Fixed dimension structures track their unique values in stage one, which means that they
 * may have possible values that could be eliminated (but that would require another data pass or
 * some fragment tracking of their possible values).
 */
class FixedDimensionStructure : public DimensionStructure {
public:
    FixedDimensionStructure(const ValueSegment::Transfer &config,
                            StructureAssemblyContext &context,
                            const StructureAssemblyContext::State &state) : DimensionStructure(
            config, context, state)
    { }

    virtual ~FixedDimensionStructure() = default;
};

class IndexDimensionStructure : public FixedDimensionStructure {
public:
    class Index {
    public:
        enum class Type {
            Empty, Integer, Real, String,
        };
    private:
        CPD3::Util::ExplicitUnion<std::int_fast64_t, double, std::string> storedData;
        Type storedType;
    public:
        Index() : storedType(Type::Empty)
        { }

        ~Index()
        {
            switch (storedType) {
            case Type::Empty:
            case Type::Integer:
            case Type::Real:
                break;
            case Type::String:
                storedData.destruct<std::string>();
                break;
            }
        }

        Index(double value) : storedType(Type::Real)
        { storedData.construct<double>(value); }

        Index(std::int_fast64_t value) : storedType(Type::Integer)
        { storedData.construct<std::int_fast64_t>(value); }

        Index(const std::string &value) : storedType(Type::String)
        { storedData.construct<std::string>(value); }

        Index(std::string &&value) : storedType(Type::String)
        { storedData.construct<std::string>(std::move(value)); }

        explicit Index(const Variant::Read &value)
        {
            switch (value.getType()) {
            case Variant::Type::Integer:
                storedType = Type::Integer;
                storedData.construct<std::int_fast64_t>(value.toInteger());
                break;
            case Variant::Type::Real:
                storedType = Type::Real;
                storedData.construct<double>(value.toReal());
                break;
            case Variant::Type::String:
                storedType = Type::String;
                storedData.construct<std::string>(value.toString());
                break;
            default:
                storedType = Type::Empty;
                break;
            }
        }

        inline Type type() const
        { return storedType; }


        std::int_fast64_t toInteger() const
        {
            switch (type()) {
            case Type::Integer:
                return storedData.ref<std::int_fast64_t>();
            case Type::Real: {
                auto r = storedData.ref<double>();
                if (!FP::defined(r))
                    break;
                return static_cast<std::int_fast64_t>(r);
            }
            default:
                break;
            }
            return INTEGER::undefined();
        }

        double toReal() const
        {
            switch (type()) {
            case Type::Real:
                return storedData.ref<double>();
            case Type::Integer: {
                auto i = storedData.ref<std::int_fast64_t>();
                if (!INTEGER::defined(i))
                    break;
                return static_cast<double>(i);
            }
            default:
                break;
            }
            return FP::undefined();
        }

        const std::string &toString() const
        {
            switch (type()) {
            case Type::String:
                return storedData.ref<std::string>();
            default:
                break;
            }
            return emptyString;
        }
    };

    IndexDimensionStructure(const ValueSegment::Transfer &config,
                            StructureAssemblyContext &context,
                            const StructureAssemblyContext::State &state) : FixedDimensionStructure(
            config, context, state)
    { }

    virtual ~IndexDimensionStructure() = default;

    virtual void claimDimensionValue(const Index &index, double start, double end) = 0;

    class IndexDimensionOutput : public FileOutputElement {
    public:
        IndexDimensionOutput(OutputAssemblyContext &context, FixedDimensionStructure &dimension)
                : FileOutputElement(context)
        { }

        virtual ~IndexDimensionOutput() = default;

        virtual ::size_t lookupDimensionIndex(const Index &index, double start, double end) = 0;
    };

    virtual IndexDimensionOutput *getCurrentOutput(OutputAssemblyContext &context) const = 0;
};


class RealDimensionStructure final : public IndexDimensionStructure {
    std::unordered_set<double> unique;
    bool requireDefined;

    static inline bool sortCompare(double a, double b)
    {
        if (b == NC_FILL_DOUBLE) {
            return a != NC_FILL_DOUBLE;
        }
        if (a == NC_FILL_DOUBLE)
            return false;
        return a < b;
    }

public:
    class Output : public IndexDimensionOutput {
        std::vector<double> values;
        bool requireDefined;
    public:
        Output(OutputAssemblyContext &context, RealDimensionStructure &dimension)
                : IndexDimensionOutput(context, dimension),
                  values(dimension.unique.begin(), dimension.unique.end()),
                  requireDefined(dimension.requireDefined)
        {
            std::sort(values.begin(), values.end(), sortCompare);
        }

        virtual ~Output() = default;

        ::size_t lookupDimensionIndex(const Index &index, double, double) override
        {
            auto v = index.toReal();
            if (!FP::defined(v)) {
                if (requireDefined)
                    return static_cast<::size_t>(-1);
                v = NC_FILL_DOUBLE;
            }
            auto idx = std::lower_bound(values.cbegin(), values.cend(), v, sortCompare);
            if (idx == values.cend()) {
                qCDebug(log_fileoutput_netcdf) << "No dimension index for" << v
                                               << "(probably an unclaimed value)";
                return static_cast<::size_t>(-1);
            }
            return static_cast<::size_t>(idx - values.begin());
        }

        inline const std::vector<double> &getValues() const
        { return values; }

        inline double getScalar() const
        {
            if (values.empty())
                return NC_FILL_DOUBLE;
            return values.front();
        }
    };

    friend class Output;

private:
    Output *output;
public:
    RealDimensionStructure(const ValueSegment::Transfer &config,
                           StructureAssemblyContext &context,
                           const StructureAssemblyContext::State &state) : IndexDimensionStructure(
            config, context, state), requireDefined(false), output(nullptr)
    {
        for (const auto &seg : config) {
            if (seg["RequireDefined"].exists())
                requireDefined = seg["RequireDefined"].toBoolean();
        }
    }

    virtual ~RealDimensionStructure() = default;

    void claimDimensionValue(const Index &index, double, double) override
    {
        auto v = index.toReal();
        if (!FP::defined(v)) {
            if (requireDefined)
                return;
            v = NC_FILL_DOUBLE;
        }
        unique.insert(v);
    }

    IndexDimensionOutput *getCurrentOutput(OutputAssemblyContext &) const override
    { return output; }

    bool assembleOutput(OutputAssemblyContext &context) override
    {
        if (!DimensionStructure::assembleOutput(context))
            return false;

        std::unique_ptr<Output> o(new Output(context, *this));
        output = o.get();
        context.output.emplace_back(std::move(o));
        return true;
    }

protected:
    std::size_t getDimensionSize(OutputAssemblyContext &) const override
    { return static_cast<std::size_t>(unique.size()); }

    bool isAllUndefined(OutputAssemblyContext &) const override
    {
        if (unique.size() != 1)
            return false;
        return *(unique.begin()) == NC_FILL_DOUBLE;
    }
};

class RealCoordinateVariableStructure final : public VariableStructure {
    RealDimensionStructure *dim;

    class Output : public FileOutputElement, public VariableStructure::ValueOutput {
        RealDimensionStructure::Output *output;
    public:
        Output(OutputAssemblyContext &context, RealCoordinateVariableStructure &parent)
                : FileOutputElement(context),
                  VariableStructure::ValueOutput(context, parent),
                  output(static_cast<RealDimensionStructure::Output *>(parent.dim
                                                                             ->getCurrentOutput(
                                                                                     context)))
        { }

        virtual ~Output() = default;

        bool initializeOutput(OutputAssemblyContext &) override
        { return writeReal({0}, output->getValues()); }
    };

    class ScalarOutput : public FileOutputElement, public VariableStructure::ValueOutput {
        RealDimensionStructure::Output *output;
    public:
        ScalarOutput(OutputAssemblyContext &context, RealCoordinateVariableStructure &parent)
                : FileOutputElement(context),
                  VariableStructure::ValueOutput(context, parent),
                  output(static_cast<RealDimensionStructure::Output *>(parent.dim
                                                                             ->getCurrentOutput(
                                                                                     context)))
        { }

        virtual ~ScalarOutput() = default;

        bool initializeOutput(OutputAssemblyContext &) override
        { return writeReal({0}, output->getScalar()); }
    };

    friend class Output;

public:
    RealCoordinateVariableStructure(const ValueSegment::Transfer &config,
                                    StructureAssemblyContext &context,
                                    const StructureAssemblyContext::State &state,
                                    RealDimensionStructure *dim) : VariableStructure(config,
                                                                                     context,
                                                                                     state),
                                                                   dim(dim)
    {
        Q_ASSERT(dim != nullptr);
    }

    virtual ~RealCoordinateVariableStructure() = default;

    bool assembleOutput(OutputAssemblyContext &context) override
    {
        if (dim->isEliminated())
            return true;

        if (!VariableStructure::assembleOutput(context))
            return false;

        if (dim->isScalar()) {
            if (!declareVariable(context, ValueOutput::Type::Real, {}))
                return false;

            std::unique_ptr<ScalarOutput> output(new ScalarOutput(context, *this));
            context.output.emplace_back(std::move(output));
            return true;
        }

        if (!declareVariable(context, ValueOutput::Type::Real, {dim->getDIMID()}))
            return false;

        std::unique_ptr<Output> output(new Output(context, *this));
        context.output.emplace_back(std::move(output));
        return true;
    }
};


class IntegerDimensionStructure final : public IndexDimensionStructure {
    std::unordered_set<std::int_fast64_t> unique;
    bool requireDefined;

    static inline bool sortCompare(std::int_fast64_t a, std::int_fast64_t b)
    {
        if (b == NC_FILL_INT64) {
            return a != NC_FILL_INT64;
        }
        if (a == NC_FILL_INT64)
            return false;
        return a < b;
    }

public:
    class Output : public IndexDimensionOutput {
        std::vector<long long> values;
        bool requireDefined;
    public:
        Output(OutputAssemblyContext &context, IntegerDimensionStructure &dimension)
                : IndexDimensionOutput(context, dimension),
                  values(dimension.unique.begin(), dimension.unique.end()),
                  requireDefined(dimension.requireDefined)
        {
            std::sort(values.begin(), values.end(), sortCompare);
        }

        virtual ~Output() = default;

        ::size_t lookupDimensionIndex(const Index &index, double, double) override
        {
            auto v = index.toInteger();
            if (!INTEGER::defined(v)) {
                if (requireDefined)
                    return static_cast<::size_t>(-1);
                v = NC_FILL_INT64;
            }
            auto idx = std::lower_bound(values.cbegin(), values.cend(), static_cast<long long>(v),
                                        sortCompare);
            if (idx == values.cend()) {
                qCDebug(log_fileoutput_netcdf) << "No dimension index for" << v
                                               << "(probably an unclaimed value)";
                return static_cast<::size_t>(-1);
            }
            return static_cast<::size_t>(idx - values.begin());
        }

        inline const std::vector<long long> &getValues() const
        { return values; }

        inline long long getScalar() const
        {
            if (values.empty())
                return NC_FILL_INT64;
            return values.front();
        }
    };

    friend class Output;

private:
    Output *output;
public:
    IntegerDimensionStructure(const ValueSegment::Transfer &config,
                              StructureAssemblyContext &context,
                              const StructureAssemblyContext::State &state)
            : IndexDimensionStructure(config, context, state),
              requireDefined(false),
              output(nullptr)
    {
        for (const auto &seg : config) {
            if (seg["RequireDefined"].exists())
                requireDefined = seg["RequireDefined"].toBoolean();
        }
    }

    virtual ~IntegerDimensionStructure() = default;

    void claimDimensionValue(const Index &index, double, double) override
    {
        auto v = index.toInteger();
        if (!INTEGER::defined(v)) {
            if (requireDefined)
                return;
            v = NC_FILL_INT64;
        }
        unique.insert(v);
    }

    IndexDimensionOutput *getCurrentOutput(OutputAssemblyContext &) const override
    { return output; }

    bool assembleOutput(OutputAssemblyContext &context) override
    {
        if (!DimensionStructure::assembleOutput(context))
            return false;

        std::unique_ptr<Output> o(new Output(context, *this));
        output = o.get();
        context.output.emplace_back(std::move(o));
        return true;
    }

protected:
    std::size_t getDimensionSize(OutputAssemblyContext &) const override
    { return static_cast<std::size_t>(unique.size()); }

    bool isAllUndefined(OutputAssemblyContext &) const override
    {
        if (unique.size() != 1)
            return false;
        return *(unique.begin()) == NC_FILL_INT64;
    }
};

class IntegerCoordinateVariableStructure final : public VariableStructure {
    IntegerDimensionStructure *dim;

    class Output : public FileOutputElement, public VariableStructure::ValueOutput {
        IntegerDimensionStructure::Output *output;
    public:
        Output(OutputAssemblyContext &context, IntegerCoordinateVariableStructure &parent)
                : FileOutputElement(context),
                  VariableStructure::ValueOutput(context, parent),
                  output(static_cast<IntegerDimensionStructure::Output *>(parent.dim
                                                                                ->getCurrentOutput(
                                                                                        context)))
        { }

        virtual ~Output() = default;

        bool initializeOutput(OutputAssemblyContext &) override
        { return writeInteger({0}, output->getValues()); }
    };

    class ScalarOutput : public FileOutputElement, public VariableStructure::ValueOutput {
        IntegerDimensionStructure::Output *output;
    public:
        ScalarOutput(OutputAssemblyContext &context, IntegerCoordinateVariableStructure &parent)
                : FileOutputElement(context),
                  VariableStructure::ValueOutput(context, parent),
                  output(static_cast<IntegerDimensionStructure::Output *>(parent.dim
                                                                                ->getCurrentOutput(
                                                                                        context)))
        { }

        virtual ~ScalarOutput() = default;

        bool initializeOutput(OutputAssemblyContext &) override
        { return writeInteger({0}, output->getScalar()); }
    };

    friend class Output;

public:
    IntegerCoordinateVariableStructure(const ValueSegment::Transfer &config,
                                       StructureAssemblyContext &context,
                                       const StructureAssemblyContext::State &state,
                                       IntegerDimensionStructure *dim) : VariableStructure(config,
                                                                                           context,
                                                                                           state),
                                                                         dim(dim)
    {
        Q_ASSERT(dim != nullptr);
    }

    virtual ~IntegerCoordinateVariableStructure() = default;

    bool assembleOutput(OutputAssemblyContext &context) override
    {
        if (dim->isEliminated())
            return true;

        if (!VariableStructure::assembleOutput(context))
            return false;

        if (dim->isScalar()) {
            if (!declareVariable(context, ValueOutput::Type::Integer, {}))
                return false;

            std::unique_ptr<ScalarOutput> output(new ScalarOutput(context, *this));
            context.output.emplace_back(std::move(output));
            return true;
        }

        if (!declareVariable(context, ValueOutput::Type::Integer, {dim->getDIMID()}))
            return false;

        std::unique_ptr<Output> output(new Output(context, *this));
        context.output.emplace_back(std::move(output));
        return true;
    }
};


class StringDimensionStructure final : public IndexDimensionStructure {
    std::unordered_set<std::string> unique;
    bool requireDefined;

public:
    class Output : public IndexDimensionOutput {
        std::vector<std::string> values;
        bool requireDefined;
    public:
        Output(OutputAssemblyContext &context, StringDimensionStructure &dimension)
                : IndexDimensionOutput(context, dimension),
                  values(dimension.unique.begin(), dimension.unique.end()),
                  requireDefined(dimension.requireDefined)
        {
            std::sort(values.begin(), values.end());
        }

        virtual ~Output() = default;

        ::size_t lookupDimensionIndex(const Index &index, double, double) override
        {
            const auto &v = index.toString();
            if (requireDefined && v.empty())
                return static_cast<::size_t>(-1);
            auto idx = std::lower_bound(values.cbegin(), values.cend(), v);
            if (idx == values.cend()) {
                qCDebug(log_fileoutput_netcdf) << "No dimension index for" << v
                                               << "(probably an unclaimed value)";
                return static_cast<::size_t>(-1);
            }
            return static_cast<::size_t>(idx - values.begin());
        }

        inline const std::vector<std::string> &getValues() const
        { return values; }
    };

    friend class Output;

private:
    Output *output;
public:
    StringDimensionStructure(const ValueSegment::Transfer &config,
                             StructureAssemblyContext &context,
                             const StructureAssemblyContext::State &state)
            : IndexDimensionStructure(config, context, state),
              requireDefined(false),
              output(nullptr)
    {
        for (const auto &seg : config) {
            if (seg["RequireDefined"].exists())
                requireDefined = seg["RequireDefined"].toBoolean();
        }
    }

    virtual ~StringDimensionStructure() = default;

    void claimDimensionValue(const Index &index, double, double) override
    {
        const auto &v = index.toString();
        if (requireDefined && v.empty())
            return;
        unique.insert(v);
    }

    IndexDimensionOutput *getCurrentOutput(OutputAssemblyContext &) const override
    { return output; }

    bool assembleOutput(OutputAssemblyContext &context) override
    {
        if (!DimensionStructure::assembleOutput(context))
            return false;

        std::unique_ptr<Output> o(new Output(context, *this));
        output = o.get();
        context.output.emplace_back(std::move(o));
        return true;
    }

protected:
    std::size_t getDimensionSize(OutputAssemblyContext &) const override
    { return static_cast<std::size_t>(unique.size()); }

    bool isAllUndefined(OutputAssemblyContext &) const override
    {
        if (unique.size() != 1)
            return false;
        return unique.begin()->empty();
    }
};

class StringCoordinateVariableStructure final : public VariableStructure {
    StringDimensionStructure *dim;

    class Output : public FileOutputElement, public VariableStructure::ValueOutput {
        StringDimensionStructure::Output *output;
    public:
        Output(OutputAssemblyContext &context, StringCoordinateVariableStructure &parent)
                : FileOutputElement(context),
                  VariableStructure::ValueOutput(context, parent),
                  output(static_cast<StringDimensionStructure::Output *>(parent.dim
                                                                               ->getCurrentOutput(
                                                                                       context)))
        { }

        virtual ~Output() = default;

        bool initializeOutput(OutputAssemblyContext &) override
        {
            std::vector<::size_t> index(1, 0);
            for (const auto &str : output->getValues()) {
                if (!writeString(index, QString::fromStdString(str)))
                    return false;
                index.front()++;
            }
            return true;
        }
    };

public:
    StringCoordinateVariableStructure(const ValueSegment::Transfer &config,
                                      StructureAssemblyContext &context,
                                      const StructureAssemblyContext::State &state,
                                      StringDimensionStructure *dim) : VariableStructure(config,
                                                                                         context,
                                                                                         state),
                                                                       dim(dim)
    {
        Q_ASSERT(dim != nullptr);
    }

    virtual ~StringCoordinateVariableStructure() = default;

    bool assembleOutput(OutputAssemblyContext &context) override
    {
        if (dim->isEliminated())
            return true;

        if (!VariableStructure::assembleOutput(context))
            return false;

        if (dim->isScalar()) {
            if (!declareVariable(context, ValueOutput::Type::String, {}))
                return false;
        } else {
            if (!declareVariable(context, ValueOutput::Type::String, {dim->getDIMID()}))
                return false;
        }

        std::unique_ptr<Output> output(new Output(context, *this));
        context.output.emplace_back(std::move(output));
        return true;
    }
};


/*
 * The structure maps a set of data values into a one variable (per output segment).  The
 * variable within that output segment has one or more output instances, each representing
 * a combination of dimensions that give the final coordinate (i.e. everything before the
 * time dimension).  The time dimension itself handles the segmentation, which then calls
 * back to the output to handle the actual data assign.
 *
 * Auxiliary dimensions are handled in both stages with each possible input having a set
 * of binding resolvers that applies to it.  The input binding resolver claims the
 * possible values in indexed dimensions, while the  output resolver looks up the final
 * output index.
 */
class DataVariableStructure final : public VariableStructure {
    class Fanout;

    class FixedBindingOutputResolver {
    public:
        FixedBindingOutputResolver() = default;

        virtual ~FixedBindingOutputResolver() = default;

        virtual ::size_t resolveSegment(SequenceSegment &segment,
                                        const Variant::Read &metadata) = 0;
    };

    class FixedBindingInvalid final : public FixedBindingOutputResolver {
    public:
        FixedBindingInvalid() = default;

        virtual ~FixedBindingInvalid() = default;

        ::size_t resolveSegment(SequenceSegment &, const Variant::Read &) override
        { return 0; }
    };

    class FixedBindingIndexResolver : public FixedBindingOutputResolver {
        IndexDimensionStructure::IndexDimensionOutput *output;
    public:
        explicit FixedBindingIndexResolver(IndexDimensionStructure::IndexDimensionOutput *output)
                : output(output)
        { }

        virtual ~FixedBindingIndexResolver() = default;

    protected:
        inline ::size_t lookupDimensionIndex(const IndexDimensionStructure::Index &index,
                                             double start,
                                             double end) const
        { return output->lookupDimensionIndex(index, start, end); }
    };

    class FixedBindingConstantIndexResolver final : public FixedBindingIndexResolver {
        IndexDimensionStructure::Index index;
    public:
        FixedBindingConstantIndexResolver(IndexDimensionStructure::IndexDimensionOutput *output,
                                          const Variant::Read &value) : FixedBindingIndexResolver(
                output), index(value)
        { }

        virtual ~FixedBindingConstantIndexResolver() = default;

        ::size_t resolveSegment(SequenceSegment &segment, const Variant::Read &) override
        { return lookupDimensionIndex(index, segment.getStart(), segment.getEnd()); }
    };

    class FixedBindingMetadataIndexResolver : public FixedBindingIndexResolver {
        Variant::Path path;
        Variant::Root constant;
    public:
        FixedBindingMetadataIndexResolver(IndexDimensionStructure::IndexDimensionOutput *output,
                                          const std::string &path,
                                          const Variant::Read &constant)
                : FixedBindingIndexResolver(output),
                  path(Variant::PathElement::parse(path)),
                  constant(constant)
        { }

        virtual ~FixedBindingMetadataIndexResolver() = default;

        ::size_t resolveSegment(SequenceSegment &segment, const Variant::Read &metadata) override
        {
            return lookupDimensionIndex(
                    IndexDimensionStructure::Index(lookupMetadataValue(metadata)),
                    segment.getStart(), segment.getEnd());
        }

    protected:
        Variant::Read lookupMetadataValue(const Variant::Read &metadata)
        {
            auto value = metadata.getPath(path);
            if (value.exists())
                return value;
            return constant.read();
        }
    };

    class FixedBindingMetadataCalibrationIndexResolver final
            : public FixedBindingMetadataIndexResolver {
        Calibration calibration;
    public:
        FixedBindingMetadataCalibrationIndexResolver(IndexDimensionStructure::IndexDimensionOutput *output,
                                                     const std::string &path,
                                                     const Calibration &calibration,
                                                     const Variant::Read &constant)
                : FixedBindingMetadataIndexResolver(output, path, constant),
                  calibration(calibration)
        { }

        virtual ~FixedBindingMetadataCalibrationIndexResolver() = default;

        ::size_t resolveSegment(SequenceSegment &segment, const Variant::Read &metadata) override
        {
            auto value = lookupMetadataValue(metadata).toReal();
            value = calibration.apply(value);
            return lookupDimensionIndex(IndexDimensionStructure::Index(value), segment.getStart(),
                                        segment.getEnd());
        }
    };

    struct FixedDimensionBinding {
        FragmentTracker::Key key;

        std::unordered_set<IndexDimensionStructure *> indexStructure;

        struct Index : public Time::Bounds {
            SequenceMatch::Composite inputMatch;
            std::string metadataPath;
            Calibration metadataCalibration;
            Variant::Root constant;

            bool isEffective(const SequenceName::Set &names) const
            {
                for (const auto &check : names) {
                    if (inputMatch.matches(check))
                        return true;
                }
                return false;
            }

            bool hasCalibration() const
            {
                return metadataCalibration.isValid() && !metadataCalibration.isIdentity();
            }

            Index() = default;

            static Calibration toCalibration(const Variant::Read &config)
            {
                auto cal = Variant::Composite::toCalibration(config);
                if (!config.exists())
                    cal.clear();
                return cal;
            }

            Index(const Variant::Read &config, double start, double end) : Time::Bounds(start, end),
                                                                           inputMatch(
                                                                                   config["Match"].exists()
                                                                                   ? SequenceMatch::Composite(
                                                                                           config["Match"])
                                                                                   : SequenceMatch::Composite(
                                                                                           SequenceMatch::Element::SpecialMatch::All)),
                                                                           metadataPath(
                                                                                   config["Metadata/Path"]
                                                                                           .toString()),
                                                                           metadataCalibration(
                                                                                   toCalibration(
                                                                                           config["Metadata/Calibration"])),
                                                                           constant(config["Value"])
            { }
        };

        std::vector<std::deque<Index>> levels;

        using Binding = FragmentTracker::StaticValue<FixedDimensionStructure *>;

        FixedDimensionBinding(const ValueSegment::Transfer &config,
                              StructureAssemblyContext &context,
                              const StructureAssemblyContext::State &state) : key(
                context.fragment.allocateKey())
        {
            for (const auto &seg : config) {
                const auto &targetName = seg["Dimension"].toString();

                auto dim = state.fixedDimensions.find(targetName);
                if (dim == state.fixedDimensions.end()) {
                    qCInfo(log_fileoutput_netcdf) << "Fixed dimension" << targetName
                                                  << "not defined";
                    continue;
                }

                context.fragment
                       .add(key, std::make_shared<Binding>(dim->second), seg.getStart(),
                            seg.getEnd());

                if (auto check = dynamic_cast<IndexDimensionStructure *>(dim->second)) {
                    indexStructure.insert(check);
                }

                auto lcfg = seg["Values"].toArray();
                for (std::size_t lidx = 0, max = lcfg.size(); lidx < max; lidx++) {
                    while (lidx >= levels.size()) {
                        levels.emplace_back();
                    }

                    levels[lidx].emplace_back(lcfg[lidx], seg.getStart(), seg.getEnd());
                }
            }
        }

        FixedDimensionBinding() = default;

        void claimIndex(const IndexDimensionStructure::Index &index, double start, double end) const
        {
            for (auto dim : indexStructure) {
                dim->claimDimensionValue(index, start, end);
            }
        }

        void processMetadataSegment(SequenceSegment &segment,
                                    const SequenceName::Set &names,
                                    const Variant::Read &metadata) const
        {
            for (const auto &level : levels) {
                auto effective = Range::findAllIntersecting(level, segment);

                /*
                 * Note that because we operate on a metadata segment at this stage, that the
                 * level claiming may have unexpected results if one level supersedes another
                 * at finer granularity.  That is, the actual assignment works at a value level,
                 * so if the claiming doesn't catch it due to the time granularity, it may
                 * have an unclaimed value involved.
                 */
                for (auto idx = effective.first; idx != effective.second; ++idx) {
                    if (!idx->isEffective(names))
                        continue;

                    if (!idx->metadataPath.empty()) {
                        auto value = metadata.getPath(idx->metadataPath);
                        if (!value.exists())
                            value = idx->constant.read();
                        if (idx->hasCalibration()) {
                            claimIndex(IndexDimensionStructure::Index(
                                    idx->metadataCalibration.apply(value.toReal())),
                                       segment.getStart(), segment.getEnd());
                            return;
                        }
                        claimIndex(IndexDimensionStructure::Index(value), segment.getStart(),
                                   segment.getEnd());
                        return;
                    }

                    claimIndex(IndexDimensionStructure::Index(idx->constant), segment.getStart(),
                               segment.getEnd());
                    return;
                }
            }
        }

        void claimPossibleNames(const SequenceName::Set &names, double start, double end) const
        {
            for (const auto &level : levels) {
                auto effective = Range::findAllIntersecting(level, start, end);

                for (auto idx = effective.first; idx != effective.second; ++idx) {
                    if (!idx->isEffective(names))
                        continue;
                    if (!idx->metadataPath.empty())
                        continue;

                    claimIndex(IndexDimensionStructure::Index(idx->constant), start, end);
                    return;
                }
            }
        }

        FixedDimensionStructure *getDimensionStructure(OutputAssemblyContext &context) const
        {
            auto dimension = Binding::value(context.structure, key, nullptr);
            if (!dimension)
                return nullptr;
            if (!dimension->isIndexed())
                return nullptr;
            return dimension;
        }

        std::unique_ptr<FixedBindingOutputResolver> toOutputResolver(OutputAssemblyContext &context,
                                                                     const SequenceName::Set &names) const
        {
            using Result = std::unique_ptr<FixedBindingOutputResolver>;

            auto dimension = getDimensionStructure(context);
            if (!dimension)
                return {};

            IndexDimensionStructure::IndexDimensionOutput *output = nullptr;
            if (auto index = dynamic_cast<IndexDimensionStructure *>(dimension)) {
                output = index->getCurrentOutput(context);
            }

            for (const auto &level : levels) {
                auto effective = Range::findAllIntersecting(level, context.structure);

                for (auto idx = effective.first; idx != effective.second; ++idx) {
                    if (!output)
                        continue;
                    if (!idx->isEffective(names))
                        continue;

                    if (!idx->metadataPath.empty()) {
                        if (idx->hasCalibration()) {
                            return Result(new FixedBindingMetadataCalibrationIndexResolver(output,
                                                                                           idx->metadataPath,
                                                                                           idx->metadataCalibration,
                                                                                           idx->constant));
                        }
                        return Result(
                                new FixedBindingMetadataIndexResolver(output, idx->metadataPath,
                                                                      idx->constant));
                    }

                    return Result(new FixedBindingConstantIndexResolver(output, idx->constant));
                }
            }

            qCDebug(log_fileoutput_netcdf) << "Using invalid dimension index due to no level match";
            return Result(new FixedBindingInvalid);
        }
    };

    std::vector<FixedDimensionBinding> fixedBindings;

    class Input final : public StagedFanout::InputStage {
        Fanout &parent;
    public:
        explicit Input(Fanout &parent) : StagedFanout::InputStage(parent), parent(parent)
        { }

        virtual ~Input() = default;

        void processMetadataSegment(SequenceSegment &segment) override
        {
            auto names = get(segment);
            auto metadata = getMetadata(segment);
            for (const auto &binding : parent.parent.fixedBindings) {
                binding.processMetadataSegment(segment, names, metadata);
            }
        }

        DataVariableStructure &getStructure() const
        { return parent.parent; }

        std::vector<std::unique_ptr<FixedBindingOutputResolver>> createFixedResolvers(
                OutputAssemblyContext &context) const
        {
            std::vector<std::unique_ptr<FixedBindingOutputResolver>> result;
            for (const auto &binding : parent.parent.fixedBindings) {
                auto resolver = binding.toOutputResolver(context, get(context.structure));
                if (!resolver)
                    continue;
                result.emplace_back(std::move(resolver));
            }
            return std::move(result);
        }

        void prepareOutput(OutputAssemblyContext &context) const
        {
            for (const auto &binding : parent.parent.fixedBindings) {
                binding.claimPossibleNames(get(context.structure), context.structure.getStart(),
                                           context.structure.getEnd());
            }
        }
    };

    friend class Input;

    class Fanout final : public StagedFanout {
        DataVariableStructure &parent;
        std::shared_ptr<StructureDynamicState> dynamic;

        friend class Input;

    public:
        /* Use the metadata target and just extract possibilities from it */
        Fanout(const CPD3::Data::ValueSegment::Transfer &config,
               StructureAssemblyContext &context,
               DataVariableStructure &parent) : StagedFanout(config, false, true),
                                                parent(parent),
                                                dynamic(context.dynamic)
        { }

        virtual ~Fanout() = default;

    protected:
        virtual std::unique_ptr<InputStage> createInput()
        { return std::unique_ptr<InputStage>(new Input(*this)); }

        std::unique_ptr<Lua::Engine::Frame> scriptFrame(const SequenceName &target) const override
        {
            auto root = dynamic->scriptFrame();
            {
                Lua::Engine::Assign assign(*root, root->global(), "TARGET");
                assign.pushData<Lua::Libs::SequenceName>(target);
            }
            return std::move(root);
        }
    };

    class Output
            : public StagedFanout::OutputStage,
              public FileOutputElement,
              virtual public TimeDimensionStructure::SegmentTarget {
        std::vector<std::unique_ptr<FixedBindingOutputResolver>> fixedResolvers;
        std::vector<::size_t> priorDimensionIndex;
        std::vector<::size_t> nextDimensionIndex;
    public:
        Output(const Input &origin, OutputAssemblyContext &context) : StagedFanout::OutputStage(
                origin, context.structure.getStart(), context.structure.getEnd()),
                                                                      FileOutputElement(context),
                                                                      fixedResolvers(
                                                                              origin.createFixedResolvers(
                                                                                      context)),
                                                                      priorDimensionIndex(
                                                                              fixedResolvers.size(),
                                                                              0),
                                                                      nextDimensionIndex(
                                                                              fixedResolvers.size(),
                                                                              0)
        { }

        virtual ~Output() = default;

        InputType getInputType(const SequenceName &name) override
        {
            if (!isPossiblyUsed(name))
                return InputType::Unused;
            if (isDirectlyUsed(name))
                return InputType::Fragment;
            return InputType::Inject;
        }

    protected:

        bool resolveDimensions(SequenceSegment &segment, const Variant::Read &metadata)
        {
            for (std::size_t dimIndex = 0, maxIndex = fixedResolvers.size();
                    dimIndex < maxIndex;
                    ++dimIndex) {
                auto idx = fixedResolvers[dimIndex]->resolveSegment(segment, metadata);
                if (idx == static_cast<::size_t>(-1))
                    return false;
                nextDimensionIndex[dimIndex] = idx;
            }
            if (nextDimensionIndex != priorDimensionIndex) {
                dimensionIndexChanging();
                priorDimensionIndex = nextDimensionIndex;
            }
            return true;
        }

        virtual void dimensionIndexChanging()
        { }

        inline std::vector<::size_t> getCurrentDimensionIndex(::size_t timeIndex)
        {
            std::vector<::size_t> result = priorDimensionIndex;
            /* We can always add the time dimension, even if it was eliminated, since it just
             * adds an unused index on the back */
            result.emplace_back(timeIndex);
            return result;
        }
    };

    static constexpr std::size_t bufferSize = 1024;

    class RealOutput final : public Output, public ValueOutput {
        std::size_t offset;
        bool isOk;
        std::vector<double> buffer;

        void flush()
        {
            if (buffer.empty())
                return;

            if (!writeReal(getCurrentDimensionIndex(offset), buffer))
                isOk = false;
            offset += buffer.size();
            buffer.clear();
        }

        void append(double value)
        {
            if (!FP::defined(value))
                value = NC_FILL_DOUBLE;

            buffer.emplace_back(value);
            if (buffer.size() < bufferSize)
                return;
            flush();
        }

    public:
        RealOutput(const Input &origin, OutputAssemblyContext &context) : Output(origin, context),
                                                                          ValueOutput(context,
                                                                                      origin.getStructure()),
                                                                          offset(0),
                                                                          isOk(true)
        { }

        virtual ~RealOutput() = default;

        void processSegment(SequenceSegment &segment) override
        {
            auto metadata = getMetadata(segment);
            auto value = getData(segment);
            if (!resolveDimensions(segment, metadata)) {
                flush();
                offset++;
                return;
            }
            append(value.toReal());
        }

        bool finishOutput() override
        {
            flush();
            return isOk;
        }

    protected:
        void dimensionIndexChanging() override
        { flush(); }
    };

    class IntegerOutput final : public Output, public ValueOutput {
        std::size_t offset;
        bool isOk;
        std::vector<long long> buffer;

        void flush()
        {
            if (buffer.empty())
                return;

            if (!writeInteger(getCurrentDimensionIndex(offset), buffer))
                isOk = false;
            offset += buffer.size();
            buffer.clear();
        }

        void append(std::int_fast64_t value)
        {
            if (!INTEGER::defined(value))
                value = NC_FILL_INT64;

            buffer.emplace_back(static_cast<long long>(value));
            if (buffer.size() < bufferSize)
                return;
            flush();
        }

    public:
        IntegerOutput(const Input &origin, OutputAssemblyContext &context) : Output(origin,
                                                                                    context),
                                                                             ValueOutput(context,
                                                                                         origin.getStructure()),
                                                                             offset(0),
                                                                             isOk(true)
        { }

        virtual ~IntegerOutput() = default;

        void processSegment(SequenceSegment &segment) override
        {
            auto metadata = getMetadata(segment);
            auto value = getData(segment);
            if (!resolveDimensions(segment, metadata)) {
                flush();
                offset++;
                return;
            }
            append(value.toInteger());
        }

        bool finishOutput() override
        {
            flush();
            return isOk;
        }

    protected:
        void dimensionIndexChanging() override
        { flush(); }
    };

    class StringOutput final : public Output, public ValueOutput {
        std::size_t offset;
        bool isOk;

    public:
        StringOutput(const Input &origin, OutputAssemblyContext &context) : Output(origin, context),
                                                                            ValueOutput(context,
                                                                                        origin.getStructure()),
                                                                            offset(0),
                                                                            isOk(true)
        { }

        virtual ~StringOutput() = default;

        void processSegment(SequenceSegment &segment) override
        {
            auto metadata = getMetadata(segment);
            auto value = getData(segment);
            if (!resolveDimensions(segment, metadata)) {
                offset++;
                return;
            }
            writeString(getCurrentDimensionIndex(offset), value.toQString());
            offset++;
        }

        bool finishOutput() override
        { return isOk; }
    };

    Fanout data;

    using OutputType = FragmentTracker::StaticValue<ValueOutput::Type>;
    FragmentTracker::Key outputType;

    using TimeBinding = FragmentTracker::StaticValue<TimeDimensionStructure *>;
    FragmentTracker::Key timeBinding;

public:
    DataVariableStructure(const ValueSegment::Transfer &config,
                          StructureAssemblyContext &context,
                          const StructureAssemblyContext::State &state) : VariableStructure(config,
                                                                                            context,
                                                                                            state),
                                                                          data(ValueSegment::withPath(
                                                                                  config, "Data"),
                                                                               context, *this),
                                                                          outputType(
                                                                                  context.fragment
                                                                                         .allocateKey()),
                                                                          timeBinding(
                                                                                  context.fragment
                                                                                         .allocateKey())
    {
        std::size_t totalDimensions = 0;
        for (const auto &seg : config) {
            {
                const auto &targetName = seg["Time/Dimension"].toString();
                auto dim = state.timeDimensions.find(targetName);
                if (dim == state.timeDimensions.end()) {
                    qCInfo(log_fileoutput_netcdf) << "Time dimension" << targetName
                                                  << "not defined";
                    continue;
                }
                context.fragment
                       .add(timeBinding, std::make_shared<TimeBinding>(dim->second), seg.getStart(),
                            seg.getEnd());
            }

            {
                const auto &typeName = seg["Type"].toString();
                if (Util::equal_insensitive(typeName, "integer", "int")) {
                    context.fragment
                           .add(outputType,
                                std::make_shared<OutputType>(ValueOutput::Type::Integer),
                                seg.getStart(), seg.getEnd());
                } else if (Util::equal_insensitive(typeName, "string", "str")) {
                    context.fragment
                           .add(outputType, std::make_shared<OutputType>(ValueOutput::Type::String),
                                seg.getStart(), seg.getEnd());
                } else {
                    context.fragment
                           .add(outputType, std::make_shared<OutputType>(ValueOutput::Type::Real),
                                seg.getStart(), seg.getEnd());
                }
            }

            totalDimensions = std::max(totalDimensions, seg["Dimensions"].toArray().size());
        }

        for (std::size_t index = 0; index < totalDimensions; ++index) {
            fixedBindings.emplace_back(state.deriveConfig(config,
                                                          {{Variant::PathElement::Type::Hash,  "Dimensions"},
                                                           {Variant::PathElement::Type::Array, index}}),
                                       context, state);
        }
    }

    virtual ~DataVariableStructure() = default;

    Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                 const Archive::Selection::Match &defaultArchives = {},
                                                 const Archive::Selection::Match &defaultVariables = {}) const override
    {
        auto result = VariableStructure::toArchiveSelections(defaultStations, defaultArchives,
                                                             defaultVariables);
        Util::append(data.toArchiveSelections(defaultStations, defaultArchives, defaultVariables),
                     result);
        return result;
    }

    std::vector<StreamSink *> getFirstStageTargets(const SequenceName &key) override
    {
        auto result = VariableStructure::getFirstStageTargets(key);
        Util::append(data.getTargets(key), result);
        return result;
    }

    FragmentTracker::MergeTestList structureMerge() override
    {
        auto result = VariableStructure::structureMerge();
        Util::append(OutputType::mergeTest(outputType), result);
        Util::append(TimeBinding::mergeTest(timeBinding), result);
        for (const auto &binding : fixedBindings) {
            Util::append(FixedDimensionBinding::Binding::mergeTest(binding.key, false), result);
        }
        return result;
    }

    void prepareOutput(OutputAssemblyContext &context) override
    {
        VariableStructure::prepareOutput(context);

        for (auto input : data.getInputs()) {
            static_cast<Input *>(input)->prepareOutput(context);
        }
    }

    bool assembleOutput(OutputAssemblyContext &context) override
    {
        if (!VariableStructure::assembleOutput(context))
            return false;

        auto time = TimeBinding::value(context.structure, timeBinding, nullptr);
        if (!time) {
            qCInfo(log_fileoutput_netcdf) << "No time dimension available for data variable";
            return false;
        }

        ValueOutput::Type variableType =
                OutputType::value(context.structure, outputType, ValueOutput::Type::Real);

        std::vector<int> dimensionBindings;
        for (const auto &binding : fixedBindings) {
            auto dimension = binding.getDimensionStructure(context);
            if (!dimension)
                continue;
            dimensionBindings.emplace_back(dimension->getDIMID());
        }
        if (time->isIndexed()) {
            dimensionBindings.emplace_back(time->getDIMID());
        }

        bool haveCreated = false;
        for (auto input : data.getInputs()) {
            if (!input->isActive(context.structure.getStart(), context.structure.getEnd()))
                continue;
            if (!haveCreated) {
                if (!declareVariable(context, variableType, dimensionBindings))
                    return false;
                if (!variableExists())
                    return true;
                haveCreated = true;
            }

            std::unique_ptr<Output> output;
            switch (variableType) {
            case ValueOutput::Type::Real:
                output.reset(new RealOutput(*static_cast<Input *>(input), context));
                break;
            case ValueOutput::Type::Integer:
                output.reset(new IntegerOutput(*static_cast<Input *>(input), context));
                break;
            case ValueOutput::Type::String:
                output.reset(new StringOutput(*static_cast<Input *>(input), context));
                break;
            }
            time->getCurrentOutput(context)->attach(output.get());
            context.output.emplace_back(std::move(output));
        }
        return true;
    }
};

class FragmentVariableStructure final : public VariableStructure {
    std::unordered_map<FragmentDimensionStructure *,
                       std::unique_ptr<FragmentDimensionStructure::Value>> data;

    using OutputType = FragmentTracker::StaticValue<ValueOutput::Type>;
    FragmentTracker::Key outputType;

    using FragmentBinding = FragmentTracker::StaticValue<FragmentDimensionStructure *>;
    FragmentTracker::Key fragmentBinding;

    class Output : public FileOutputElement {
    public:
        Output(OutputAssemblyContext &context,
               FragmentVariableStructure &,
               FragmentDimensionStructure *) : FileOutputElement(context)
        { }

        virtual ~Output() = default;
    };

    class RealOutput : public Output, public ValueOutput {
        std::vector<double> values;
    public:
        RealOutput(OutputAssemblyContext &context,
                   FragmentVariableStructure &variable,
                   FragmentDimensionStructure *fragment,
                   FragmentDimensionStructure::Value *data) : Output(context, variable, fragment),
                                                              ValueOutput(context, variable)
        {
            data->activate(context);
            for (const auto &seg : fragment->getResult()) {
                auto v = Variant::Composite::toNumber(data->get(seg.contents), true);
                if (!FP::defined(v))
                    v = NC_FILL_DOUBLE;
                values.emplace_back(v);
            }
            data->deactivate(context);
        }

        virtual ~RealOutput() = default;

        bool initializeOutput(OutputAssemblyContext &) override
        { return writeReal({0}, values); }
    };

    friend class RealOutput;

    class IntegerOutput : public Output, public ValueOutput {
        std::vector<long long> values;
    public:
        IntegerOutput(OutputAssemblyContext &context,
                      FragmentVariableStructure &variable,
                      FragmentDimensionStructure *fragment,
                      FragmentDimensionStructure::Value *data) : Output(context, variable,
                                                                        fragment),
                                                                 ValueOutput(context, variable)
        {
            data->activate(context);
            for (const auto &seg : fragment->getResult()) {
                auto v = Variant::Composite::toInteger(data->get(seg.contents), true);
                if (!FP::defined(v))
                    v = NC_FILL_INT64;
                values.emplace_back(static_cast<long long>(v));
            }
            data->deactivate(context);
        }

        virtual ~IntegerOutput() = default;

        bool initializeOutput(OutputAssemblyContext &) override
        { return writeInteger({0}, values); }
    };

    friend class IntegerOutput;

    class StringOutput : public Output, public ValueOutput {
        std::vector<QString> values;
    public:
        StringOutput(OutputAssemblyContext &context,
                     FragmentVariableStructure &variable,
                     FragmentDimensionStructure *fragment,
                     FragmentDimensionStructure::Value *data) : Output(context, variable, fragment),
                                                                ValueOutput(context, variable)
        {
            data->activate(context);
            for (const auto &seg : fragment->getResult()) {
                auto v = data->get(seg.contents);
                switch (v.getType()) {
                case Variant::Type::Real:
                    values.emplace_back(data->toString(seg.contents, QString::number(v.toReal())));
                    break;
                case Variant::Type::Integer:
                    values.emplace_back(
                            data->toString(seg.contents, QString::number(v.toInteger())));
                    break;
                default:
                    values.emplace_back(data->toString(seg.contents, v.toQString()));
                    break;
                }
            }
            data->deactivate(context);
        }

        virtual ~StringOutput() = default;

        bool initializeOutput(OutputAssemblyContext &) override
        {
            std::vector<::size_t> index(1, 0);
            for (const auto &str : values) {
                if (!writeString(index, str))
                    return false;
                index.front()++;
            }
            return true;
        }
    };

    friend class StringOutput;

public:
    FragmentVariableStructure(const ValueSegment::Transfer &config,
                              StructureAssemblyContext &context,
                              const StructureAssemblyContext::State &state) : VariableStructure(
            config, context, state),
                                                                              outputType(
                                                                                      context.fragment
                                                                                             .allocateKey()),
                                                                              fragmentBinding(
                                                                                      context.fragment
                                                                                             .allocateKey())
    {
        for (const auto &seg : config) {
            const auto &targetName = seg["Fragment/Dimension"].toString();
            auto dim = state.fragmentDimensions.find(targetName);
            if (dim == state.fragmentDimensions.end()) {
                qCInfo(log_fileoutput_netcdf) << "Fragment dimension" << targetName
                                              << "not defined";
                continue;
            }
            context.fragment
                   .add(fragmentBinding, std::make_shared<FragmentBinding>(dim->second),
                        seg.getStart(), seg.getEnd());

            if (data.count(dim->second) == 0) {
                data.emplace(dim->second, dim->second->attach(config, context, state, "Value"));
            }

            {
                const auto &typeName = seg["Type"].toString();
                if (Util::equal_insensitive(typeName, "integer", "int")) {
                    context.fragment
                           .add(outputType,
                                std::make_shared<OutputType>(ValueOutput::Type::Integer),
                                seg.getStart(), seg.getEnd());
                } else if (Util::equal_insensitive(typeName, "string", "str")) {
                    context.fragment
                           .add(outputType, std::make_shared<OutputType>(ValueOutput::Type::String),
                                seg.getStart(), seg.getEnd());
                } else {
                    context.fragment
                           .add(outputType, std::make_shared<OutputType>(ValueOutput::Type::Real),
                                seg.getStart(), seg.getEnd());
                }
            }
        }
    }

    virtual ~FragmentVariableStructure() = default;

    Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                 const Archive::Selection::Match &defaultArchives = {},
                                                 const Archive::Selection::Match &defaultVariables = {}) const override
    {
        auto result = VariableStructure::toArchiveSelections(defaultStations, defaultArchives,
                                                             defaultVariables);
        for (const auto &dbinding : data) {
            Util::append(dbinding.second
                                 ->toArchiveSelections(defaultStations, defaultArchives,
                                                       defaultVariables), result);
        }
        return result;
    }

    std::vector<StreamSink *> getFirstStageTargets(const SequenceName &key) override
    {
        auto result = VariableStructure::getFirstStageTargets(key);
        for (const auto &dbinding : data) {
            Util::append(dbinding.second->getTargets(key), result);
        }
        return result;
    }

    FragmentTracker::MergeTestList structureMerge() override
    {
        auto result = VariableStructure::structureMerge();
        Util::append(OutputType::mergeTest(outputType), result);
        Util::append(FragmentBinding::mergeTest(fragmentBinding), result);
        return result;
    }

    void structureExtend() override
    {
        for (const auto &dbinding : data) {
            dbinding.second->extendDefined();
        }
    }

    bool assembleOutput(OutputAssemblyContext &context) override
    {
        if (!VariableStructure::assembleOutput(context))
            return false;

        auto fragment = FragmentBinding::value(context.structure, fragmentBinding, nullptr);
        if (!fragment) {
            qCInfo(log_fileoutput_netcdf) << "No time fragment available for variable";
            return false;
        }
        if (fragment->isEliminated())
            return true;

        auto dbinding = data.find(fragment);
        Q_ASSERT(dbinding != data.end());

        ValueOutput::Type variableType =
                OutputType::value(context.structure, outputType, ValueOutput::Type::Real);

        {
            std::vector<int> dimensionBindings;
            if (fragment->isIndexed()) {
                dimensionBindings.emplace_back(fragment->getDIMID());
            }
            if (!declareVariable(context, variableType, dimensionBindings))
                return false;
        }
        if (!variableExists())
            return true;

        std::unique_ptr<Output> output;
        switch (variableType) {
        case ValueOutput::Type::Real:
            output.reset(new RealOutput(context, *this, fragment, dbinding->second.get()));
            break;
        case ValueOutput::Type::Integer:
            output.reset(new IntegerOutput(context, *this, fragment, dbinding->second.get()));
            break;
        case ValueOutput::Type::String:
            output.reset(new StringOutput(context, *this, fragment, dbinding->second.get()));
            break;
        }
        context.output.emplace_back(std::move(output));
        return true;
    }
};

OutputAssemblyContext::OutputAssemblyContext(NetCDF &config,
                                             const FragmentTracker::Result &structure) : config(
        config), structure(structure), ncid(-1)
{ }

OutputAssemblyContext::~OutputAssemblyContext()
{
    if (outputFile) {
        int err = -1;
        if ((err = ::nc_close(ncid)) != NC_NOERR) {
            qCDebug(log_fileoutput_netcdf) << "Error closing file:" << nc_strerror(err);
        }
        outputFile->remove();
    }
}

int OutputAssemblyContext::parent(const GroupStructure *parent) const
{
    if (!parent)
        return ncid;
    return parent->getNCID();
}

bool OutputAssemblyContext::enabled(const GroupStructure *parent) const
{
    if (!parent)
        return true;
    return parent->isEnabled(*this);
}

void OutputAssemblyContext::provideDependencies(GroupStructure *parent,
                                                const DependencySet &provide)
{
    if (!parent)
        return;
    if (provide.empty())
        return;
    parent->provideDependencies(*this, provide);
}

struct OutputSegment : public Time::Bounds {
    std::vector<std::unique_ptr<FileOutputElement>> output;
    int ncid;
    std::unique_ptr<QFile> outputFile;

    OutputSegment(OutputAssemblyContext &&context) : Time::Bounds(context.structure),
                                                     output(std::move(context.output)),
                                                     ncid(context.ncid),
                                                     outputFile(std::move(context.outputFile))
    { }

    ~OutputSegment() = default;

    OutputSegment(OutputSegment &&) = default;

    OutputSegment &operator=(OutputSegment &&) = default;
};

class OutputStage : public Engine::SinkDispatchStage {
    NetCDF config;

    std::vector<OutputSegment> segments;
    std::vector<std::unique_ptr<Engine::ClipSinkWrapper>> clipWrappers;

public:
    explicit OutputStage(NetCDF config, std::vector<OutputSegment> &&segments) : config(
            std::move(config)), segments(std::move(segments))
    { }

    virtual ~OutputStage() = default;

    bool isFinalStage() const override
    { return true; }

    void finalizeData() override
    {
        Engine::SinkDispatchStage::finalizeData();
        clipWrappers.clear();

        for (auto &seg : segments) {
            bool segOk = true;
            for (const auto &out : seg.output) {
                if (!out->finishOutput()) {
                    segOk = false;
                    break;
                }
            }

            int err = -1;
            if ((err = ::nc_close(seg.ncid)) != NC_NOERR) {
                qCDebug(log_fileoutput_netcdf) << "Error closing file:" << ::nc_strerror(err);
            }

            if (!segOk) {
                seg.outputFile->remove();
            }
        }
    }

protected:
    std::vector<StreamSink *> getTargets(const SequenceName &name) override
    {
        std::vector<StreamSink *> result;

        for (const auto &seg : segments) {
            for (const auto &out : seg.output) {
                for (auto base : out->getOutputTargets(name)) {
                    std::unique_ptr<Engine::ClipSinkWrapper>
                            wrap(new Engine::ClipSinkWrapper(seg, base));
                    result.emplace_back(wrap.get());
                    clipWrappers.emplace_back(std::move(wrap));
                }
            }
        }

        return result;
    }
};


StructureAssemblyContext::StructureAssemblyContext(NetCDF &config,
                                                   FragmentTracker &fragment,
                                                   std::shared_ptr<StructureDynamicState> dynamic)
        : config(config), fragment(fragment), dynamic(std::move(dynamic))
{ }

StructureAssemblyContext::~StructureAssemblyContext() = default;

StructureAssemblyContext::State::State(const State &parent,
                                       ValueSegment::Transfer config,
                                       GroupStructure *group) : group(group),
                                                                base(std::move(config)),
                                                                substitutions(parent.substitutions),
                                                                timeDimensions(
                                                                        parent.timeDimensions),
                                                                fragmentDimensions(
                                                                        parent.fragmentDimensions),
                                                                fixedDimensions(
                                                                        parent.fixedDimensions)
{ }

StructureAssemblyContext::State::State(ValueSegment::Transfer config) : group(nullptr),
                                                                        base(std::move(config))
{ }

StructureAssemblyContext::State::~State() = default;

class FirstStage : public Engine::SinkDispatchStage {
    NetCDF config;
    FragmentTracker fragment;
    std::shared_ptr<StructureDynamicState> dynamic;
    std::vector<std::unique_ptr<FileStructureElement>> structure;

    static void assembleUnique(StructureAssemblyContext &context,
                               StructureAssemblyContext::Unique &target)
    {
        for (const auto &seg : context.config.config) {
            auto rp = seg.read().getPath(target.path);

            Util::merge(rp["Substitutions"].toHash().keys(), target.substitutions);
            Util::merge(rp["Attributes"].toHash().keys(), target.attributes);

            for (const auto &add : rp["Variables"].toHash()) {
                auto &data = target.dataVariables[add.first];

                Util::merge(add.second["Attributes"].toHash().keys(), data.attributes);
            }

            for (const auto &add : rp["Fragments"].toHash()) {
                auto &data = target.fragmentVariables[add.first];

                Util::merge(add.second["Attributes"].toHash().keys(), data.attributes);
            }

            for (const auto &add : rp["Dimensions/Time"].toHash()) {
                auto &data = target.timeDimensions[add.first];

                for (const auto &cadd : add.second["Coordinates"].toHash()) {
                    auto &cdata = data.coordinates[cadd.first];

                    Util::merge(cadd.second["Attributes"].toHash().keys(), cdata.attributes);
                }
            }

            for (const auto &add : rp["Dimensions/Fragment"].toHash()) {
                auto &data = target.fragmentDimensions[add.first];

                for (const auto &cadd : add.second["Coordinates"].toHash()) {
                    auto &cdata = data.coordinates[cadd.first];

                    Util::merge(cadd.second["Attributes"].toHash().keys(), cdata.attributes);
                }
            }

            for (const auto &add : rp["Dimensions/Real"].toHash()) {
                auto &data = target.realDimensions[add.first];

                for (const auto &cadd : add.second["Coordinates"].toHash()) {
                    auto &cdata = data.coordinates[cadd.first];

                    Util::merge(cadd.second["Attributes"].toHash().keys(), cdata.attributes);
                }
            }

            for (const auto &add : rp["Dimensions/Integer"].toHash()) {
                auto &data = target.integerDimensions[add.first];

                for (const auto &cadd : add.second["Coordinates"].toHash()) {
                    auto &cdata = data.coordinates[cadd.first];

                    Util::merge(cadd.second["Attributes"].toHash().keys(), cdata.attributes);
                }
            }

            for (const auto &add : rp["Dimensions/String"].toHash()) {
                auto &data = target.stringDimensions[add.first];

                for (const auto &cadd : add.second["Coordinates"].toHash()) {
                    auto &cdata = data.coordinates[cadd.first];

                    Util::merge(cadd.second["Attributes"].toHash().keys(), cdata.attributes);
                }
            }

            for (const auto &add : rp["Groups"].toHash().keys()) {
                target.groups
                      .emplace(add, std::unique_ptr<StructureAssemblyContext::Unique>(
                              new StructureAssemblyContext::Unique));
            }
        }

        for (auto &group : target.groups) {
            group.second->path = target.path;
            group.second
                 ->path
                 .emplace_back(Variant::PathElement(Variant::PathElement::Type::Hash, "Groups"));
            group.second
                 ->path
                 .emplace_back(Variant::PathElement(Variant::PathElement::Type::Hash, group.first));

            assembleUnique(context, *group.second);
        }
    }

    static void assembleSubstitutions(StructureAssemblyContext &context,
                                      const StructureAssemblyContext::Unique &unique,
                                      StructureAssemblyContext::State &state)
    {
        for (const auto &cfg : unique.substitutions) {
            auto add = std::unique_ptr<SubstitutionStructure>(
                    new SubstitutionStructure(state.deriveConfig("Substitutions", cfg), context,
                                              state));
            Util::insert_or_assign(state.substitutions, Util::to_lower(cfg), add.get());
            context.structure.emplace_back(std::move(add));
        }
    }

    static void assembleStructure(StructureAssemblyContext &context,
                                  const StructureAssemblyContext::Unique &unique,
                                  StructureAssemblyContext::State &state)
    {
        for (const auto &cfg : unique.attributes) {
            auto add = std::unique_ptr<GlobalAttributeStructure>(
                    new GlobalAttributeStructure(state.deriveConfig("Attributes", cfg), context,
                                                 state));
            context.structure.emplace_back(std::move(add));
        }

        for (const auto &cfg : unique.timeDimensions) {
            auto config = state.deriveConfig("Dimensions", "Time", cfg.first);
            auto add = std::unique_ptr<TimeDimensionStructure>(
                    new TimeDimensionStructure(config, context, state));
            auto dim = add.get();
            if (!state.timeDimensions.emplace(cfg.first, dim).second)
                continue;
            context.structure.emplace_back(std::move(add));

            for (const auto &vcfg : cfg.second.coordinates) {
                auto vconfig = state.deriveConfig(config,
                                                  {{Variant::PathElement::Type::Hash, "Coordinates"},
                                                   {Variant::PathElement::Type::Hash, vcfg.first}});
                auto vadd = std::unique_ptr<TimeCoordinateVariableStructure>(
                        new TimeCoordinateVariableStructure(vconfig, context, state, dim));
                auto var = vadd.get();
                context.structure.emplace_back(std::move(vadd));

                for (const auto &acfg : vcfg.second.attributes) {
                    auto aadd = std::unique_ptr<VariableAttributeStructure>(
                            new VariableAttributeStructure(state.deriveConfig(vconfig,
                                                                              {{Variant::PathElement::Type::Hash, "Attributes"},
                                                                               {Variant::PathElement::Type::Hash, acfg}}),
                                                           context, state, var));
                    context.structure.emplace_back(std::move(aadd));
                }
            }
        }

        for (const auto &cfg : unique.fragmentDimensions) {
            auto config = state.deriveConfig("Dimensions", "Fragment", cfg.first);
            auto add = std::unique_ptr<FragmentDimensionStructure>(
                    new FragmentDimensionStructure(config, context, state));
            auto dim = add.get();
            if (!state.fragmentDimensions.emplace(cfg.first, dim).second)
                continue;
            context.structure.emplace_back(std::move(add));

            for (const auto &vcfg : cfg.second.coordinates) {
                auto vconfig = state.deriveConfig(config,
                                                  {{Variant::PathElement::Type::Hash, "Coordinates"},
                                                   {Variant::PathElement::Type::Hash, vcfg.first}});
                auto vadd = std::unique_ptr<FragmentCoordinateVariableStructure>(
                        new FragmentCoordinateVariableStructure(vconfig, context, state, dim));
                auto var = vadd.get();
                context.structure.emplace_back(std::move(vadd));

                for (const auto &acfg : vcfg.second.attributes) {
                    auto aadd = std::unique_ptr<VariableAttributeStructure>(
                            new VariableAttributeStructure(state.deriveConfig(vconfig,
                                                                              {{Variant::PathElement::Type::Hash, "Attributes"},
                                                                               {Variant::PathElement::Type::Hash, acfg}}),
                                                           context, state, var));
                    context.structure.emplace_back(std::move(aadd));
                }
            }
        }

        for (const auto &cfg : unique.realDimensions) {
            auto config = state.deriveConfig("Dimensions", "Real", cfg.first);
            auto add = std::unique_ptr<RealDimensionStructure>(
                    new RealDimensionStructure(config, context, state));
            auto dim = add.get();
            if (!state.fixedDimensions.emplace(cfg.first, dim).second)
                continue;
            context.structure.emplace_back(std::move(add));

            for (const auto &vcfg : cfg.second.coordinates) {
                auto vconfig = state.deriveConfig(config,
                                                  {{Variant::PathElement::Type::Hash, "Coordinates"},
                                                   {Variant::PathElement::Type::Hash, vcfg.first}});
                auto vadd = std::unique_ptr<RealCoordinateVariableStructure>(
                        new RealCoordinateVariableStructure(vconfig, context, state, dim));
                auto var = vadd.get();
                context.structure.emplace_back(std::move(vadd));

                for (const auto &acfg : vcfg.second.attributes) {
                    auto aadd = std::unique_ptr<VariableAttributeStructure>(
                            new VariableAttributeStructure(state.deriveConfig(vconfig,
                                                                              {{Variant::PathElement::Type::Hash, "Attributes"},
                                                                               {Variant::PathElement::Type::Hash, acfg}}),
                                                           context, state, var));
                    context.structure.emplace_back(std::move(aadd));
                }
            }
        }

        for (const auto &cfg : unique.integerDimensions) {
            auto config = state.deriveConfig("Dimensions", "Integer", cfg.first);
            auto add = std::unique_ptr<IntegerDimensionStructure>(
                    new IntegerDimensionStructure(config, context, state));
            auto dim = add.get();
            if (!state.fixedDimensions.emplace(cfg.first, dim).second)
                continue;
            context.structure.emplace_back(std::move(add));

            for (const auto &vcfg : cfg.second.coordinates) {
                auto vconfig = state.deriveConfig(config,
                                                  {{Variant::PathElement::Type::Hash, "Coordinates"},
                                                   {Variant::PathElement::Type::Hash, vcfg.first}});
                auto vadd = std::unique_ptr<IntegerCoordinateVariableStructure>(
                        new IntegerCoordinateVariableStructure(vconfig, context, state, dim));
                auto var = vadd.get();
                context.structure.emplace_back(std::move(vadd));

                for (const auto &acfg : vcfg.second.attributes) {
                    auto aadd = std::unique_ptr<VariableAttributeStructure>(
                            new VariableAttributeStructure(state.deriveConfig(vconfig,
                                                                              {{Variant::PathElement::Type::Hash, "Attributes"},
                                                                               {Variant::PathElement::Type::Hash, acfg}}),
                                                           context, state, var));
                    context.structure.emplace_back(std::move(aadd));
                }
            }
        }

        for (const auto &cfg : unique.stringDimensions) {
            auto config = state.deriveConfig("Dimensions", "String", cfg.first);
            auto add = std::unique_ptr<StringDimensionStructure>(
                    new StringDimensionStructure(config, context, state));
            auto dim = add.get();
            if (!state.fixedDimensions.emplace(cfg.first, dim).second)
                continue;
            context.structure.emplace_back(std::move(add));

            for (const auto &vcfg : cfg.second.coordinates) {
                auto vconfig = state.deriveConfig(config,
                                                  {{Variant::PathElement::Type::Hash, "Coordinates"},
                                                   {Variant::PathElement::Type::Hash, vcfg.first}});
                auto vadd = std::unique_ptr<StringCoordinateVariableStructure>(
                        new StringCoordinateVariableStructure(vconfig, context, state, dim));
                auto var = vadd.get();
                context.structure.emplace_back(std::move(vadd));

                for (const auto &acfg : vcfg.second.attributes) {
                    auto aadd = std::unique_ptr<VariableAttributeStructure>(
                            new VariableAttributeStructure(state.deriveConfig(vconfig,
                                                                              {{Variant::PathElement::Type::Hash, "Attributes"},
                                                                               {Variant::PathElement::Type::Hash, acfg}}),
                                                           context, state, var));
                    context.structure.emplace_back(std::move(aadd));
                }
            }
        }

        for (const auto &cfg : unique.dataVariables) {
            auto config = state.deriveConfig("Variables", cfg.first);
            auto add = std::unique_ptr<DataVariableStructure>(
                    new DataVariableStructure(config, context, state));
            auto var = add.get();
            context.structure.emplace_back(std::move(add));

            for (const auto &acfg : cfg.second.attributes) {
                auto aadd = std::unique_ptr<VariableAttributeStructure>(
                        new VariableAttributeStructure(state.deriveConfig(config,
                                                                          {{Variant::PathElement::Type::Hash, "Attributes"},
                                                                           {Variant::PathElement::Type::Hash, acfg}}),
                                                       context, state, var));
                context.structure.emplace_back(std::move(aadd));
            }
        }

        for (const auto &cfg : unique.fragmentVariables) {
            auto config = state.deriveConfig("Fragments", cfg.first);
            auto add = std::unique_ptr<FragmentVariableStructure>(
                    new FragmentVariableStructure(config, context, state));
            auto var = add.get();
            context.structure.emplace_back(std::move(add));

            for (const auto &acfg : cfg.second.attributes) {
                auto aadd = std::unique_ptr<VariableAttributeStructure>(
                        new VariableAttributeStructure(state.deriveConfig(config,
                                                                          {{Variant::PathElement::Type::Hash, "Attributes"},
                                                                           {Variant::PathElement::Type::Hash, acfg}}),
                                                       context, state, var));
                context.structure.emplace_back(std::move(aadd));
            }
        }

        for (const auto &gcfg : unique.groups) {
            auto base = state.deriveConfig("Groups", gcfg.first);
            auto group = std::unique_ptr<GroupStructure>(new GroupStructure(base, context, state));
            auto gptr = group.get();
            context.structure.emplace_back(std::move(group));
            StructureAssemblyContext::State next(state, std::move(base), gptr);
            assembleSubstitutions(context, *gcfg.second, next);
            assembleStructure(context, *gcfg.second, next);
        }
    }

public:
    explicit FirstStage(NetCDF input) : config(std::move(input)),
                                        dynamic(std::make_shared<StructureDynamicState>(config))
    {
        fragment.addEmpty(config.start, config.end);

        {
            auto save = std::move(config.config);
            config.config.clear();
            auto applicable = Range::findAllIntersecting(save, config.start, config.end);
            std::move(applicable.first, applicable.second, Util::back_emplacer(config.config));
        }
        if (!config.config.empty()) {
            if (Range::compareStart(config.config.front().getStart(), config.start) < 0)
                config.config.front().setStart(config.start);
            if (Range::compareEnd(config.config.front().getEnd(), config.end) > 0)
                config.config.front().setEnd(config.end);
        }

        StructureAssemblyContext context(config, fragment, dynamic);
        StructureAssemblyContext::Unique root;
        assembleUnique(context, root);
        StructureAssemblyContext::State state(config.config);
        assembleSubstitutions(context, root, state);
        context.structure.emplace_back(new OutputNetCDFStructure(config.config, context, state));
        assembleStructure(context, root, state);

        structure = std::move(context.structure);
    }

    virtual ~FirstStage() = default;

    bool isFinalStage() const override
    { return false; }

    std::unique_ptr<Stage> getNextStage() override
    {
        std::vector<OutputSegment> segments;
        for (const auto &fileContents : fragment.getFragments()) {
            dynamic->start = fileContents.getStart();
            dynamic->end = fileContents.getEnd();

            if (Range::compareStart(dynamic->start, config.start) < 0)
                dynamic->start = config.start;
            if (Range::compareEnd(dynamic->end, config.end) > 0)
                dynamic->end = config.end;

            OutputAssemblyContext context(config, fileContents);
            for (const auto &add : structure) {
                add->prepareOutput(context);
            }

            bool isOk = true;
            for (const auto &add : structure) {
                if (!add->assembleOutput(context)) {
                    isOk = false;
                }
            }
            if (!isOk)
                continue;

            int err = -1;
            if ((err = ::nc_enddef(context.ncid)) != NC_NOERR) {
                qCDebug(log_fileoutput_netcdf) << "Error leaving define mode:"
                                               << ::nc_strerror(err);
            }

            for (const auto &add : context.output) {
                if (!add->initializeOutput(context)) {
                    isOk = false;
                }
            }
            if (!isOk)
                continue;

            segments.emplace_back(std::move(context));
        }

        if (segments.empty()) {
            qCDebug(log_fileoutput_netcdf) << "No output segments, output stage aborted";
            return {};
        }

        return std::unique_ptr<Engine::Stage>(
                new OutputStage(std::move(config), std::move(segments)));
    }

    void finalizeData() override
    {
        Engine::SinkDispatchStage::finalizeData();

        FragmentTracker::MergeTestList mergeTests;
        for (const auto &add : structure) {
            add->structureExtend();
            Util::append(add->structureMerge(), mergeTests);
        }
        fragment.merge(mergeTests);
        for (const auto &add : structure) {
            add->structureFinalize();
        }
    }

    bool configurePipeline(StreamPipeline *target) const
    {
        Archive::Selection::List selections;
        for (const auto &add : structure) {
            Util::append(add->toArchiveSelections({config.station}), selections);
        }

        Variant::Read processing = Variant::Read::empty();
        if (!config.config.empty()) {
            processing = config.config.back()["Processing"];
        }
        return Engine::configurePipelineInput(processing, config.station, target,
                                              std::move(selections),
                                              Time::Bounds(config.start, config.end));
    }

    const std::vector<std::unique_ptr<FileStructureElement>> &getStructure() const
    { return structure; }

protected:
    std::vector<StreamSink *> getTargets(const SequenceName &name) override
    {
        std::vector<StreamSink *> result;
        for (const auto &add : structure) {
            Util::append(add->getFirstStageTargets(name), result);
        }
        return result;
    }
};

}

NetCDF::NetCDF() : start(FP::undefined()), end(FP::undefined())
{ }

std::unique_ptr<Engine::Stage> NetCDF::stage(NetCDF config)
{ return std::unique_ptr<Engine::Stage>(new FirstStage(std::move(config))); }

bool NetCDF::configurePipeline(const Engine::Stage *firstStage, StreamPipeline *target)
{ return static_cast<const FirstStage *>(firstStage)->configurePipeline(target); }

}
}