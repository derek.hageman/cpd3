/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include "variablevalue.hxx"
#include "datacore/variant/composite.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Output {
namespace VariableValue {

Base::DataInput::DataInput(const Variant::Read &config)
{
    if (config.getType() == Variant::Type::Hash) {
        if (config["Input"].exists()) {
            input = SequenceMatch::OrderedLookup(config["Input"]);
        }
        path = Variant::PathElement::parse(config["Path"].toString());
        constant.write().set(config["Constant"]);
    } else {
        constant.write().set(config);
    }
}

Base::DataInput::~DataInput() = default;

FragmentTracker::ValueHandle Base::DataInput::overlay(const FragmentTracker::ValueHandle &,
                                                      const FragmentTracker::ValueHandle &over)
{ return over; }

bool Base::DataInput::registerInput(const SequenceName &name)
{ return input.registerInput(name); }

Archive::Selection::List Base::DataInput::toArchiveSelections(const Archive::Selection::Match &defaultStations,
                                                              const Archive::Selection::Match &defaultArchives,
                                                              const Archive::Selection::Match &defaultVariables) const
{ return input.toArchiveSelections(defaultStations, defaultArchives, defaultVariables); }

Variant::Read Base::DataInput::fetch(const Variant::Read &in)
{ return in.getPath(path); }

Variant::Root Base::DataInput::convert(const Variant::Read &value)
{ return Variant::Root(value); }

Variant::Root Base::DataInput::processSegment(SequenceSegment &segment)
{
    auto base = fetch(input.lookup(segment));
    if (!Variant::Composite::isDefined(base))
        return convert(constant);
    return convert(base);
}

Base::DataInput::DataInput(const Base::DataInput &) = default;

namespace {
class OutputValue : public FragmentTracker::Value {
public:
    Variant::Root result;

    explicit OutputValue(Variant::Root result) : result(std::move(result))
    { }

    virtual ~OutputValue() = default;

    FragmentTracker::ValueHandle overlay(const FragmentTracker::ValueHandle &,
                                         const FragmentTracker::ValueHandle &over) override
    { return over; }

    void merge(FragmentTracker::Value *other) override
    {
        if (Variant::Composite::isDefined(result.read()))
            return;
        result = static_cast<const OutputValue *>(other)->result;
    }
};
}

Base::TargetControl::TargetControl(std::shared_ptr<DataInput> input, double start, double end)
        : Time::Bounds(start, end), input(std::move(input))
{

}

Base::TargetControl::~TargetControl() = default;

Base::Sink::Sink(Base &parent) : parent(parent)
{ }

Base::Sink::~Sink() = default;

void Base::Sink::processSegment(SequenceSegment &&segment)
{
    if (!Range::intersectShift(control, segment.getStart(), segment.getEnd()))
        return;

    parent.outputTracker
          .add(parent.outputKey,
               std::make_shared<OutputValue>(control.front().input->processSegment(segment)),
               segment.getStart(), segment.getEnd());
}

void Base::Sink::incomingData(const SequenceValue::Transfer &values)
{
    for (auto &add : stream.add(values)) {
        processSegment(std::move(add));
    }
}

void Base::Sink::incomingData(SequenceValue::Transfer &&values)
{
    for (auto &add : stream.add(std::move(values))) {
        processSegment(std::move(add));
    }
}

void Base::Sink::incomingData(const SequenceValue &value)
{
    for (auto &add : stream.add(value)) {
        processSegment(std::move(add));
    }
}

void Base::Sink::incomingData(SequenceValue &&value)
{
    for (auto &add : stream.add(std::move(value))) {
        processSegment(std::move(add));
    }
}

void Base::Sink::endData()
{
    for (auto &add : stream.finish()) {
        processSegment(std::move(add));
    }
}

Base::Base(FragmentTracker &tracker) : configureTracker(new FragmentTracker),
                                       configureDataKey(configureTracker->allocateKey()),
                                       outputTracker(tracker),
                                       outputKey(tracker.allocateKey()),
                                       sink(*this)
{ }

Base::~Base() = default;

void Base::addInput(const std::shared_ptr<DataInput> &value, double start, double end)
{ configureTracker->add(configureDataKey, value, start, end); }

void Base::addInput(std::shared_ptr<DataInput> &&value, double start, double end)
{ configureTracker->add(configureDataKey, std::move(value), start, end); }

void Base::endConfigure()
{
    for (auto &add : configureTracker->getFragments()) {
        auto data = std::static_pointer_cast<DataInput>(std::move(add.contents[configureDataKey]));
        if (!data)
            continue;
        outputTracker.add(outputKey, std::make_shared<OutputValue>(data->convert(data->constant)),
                          add.getStart(), add.getEnd());
        sink.control.emplace_back(std::move(data), add.getStart(), add.getEnd());
    }
    configureTracker.reset();
}

Archive::Selection::List Base::toArchiveSelections(const Archive::Selection::Match &defaultStations,
                                                   const Archive::Selection::Match &defaultArchives,
                                                   const Archive::Selection::Match &defaultVariables) const
{
    Archive::Selection::List result;
    for (const auto &add : sink.control) {
        Util::append(
                add.input->toArchiveSelections(defaultStations, defaultArchives, defaultVariables),
                result);
    }
    return result;
}

std::vector<StreamSink *> Base::getTargets(const SequenceName &name)
{
    bool isInput = false;
    for (const auto &check : sink.control) {
        if (!check.input->registerInput(name))
            continue;
        isInput = true;
    }
    if (!isInput)
        return {};
    return {&sink};
}

Variant::Read Base::get(const FragmentTracker::Contents &contents) const
{
    auto frag = contents.find(outputKey);
    if (frag == contents.end())
        return Variant::Read::empty();
    if (!frag->second)
        return Variant::Read::empty();
    return static_cast<const OutputValue *>(frag->second.get())->result.read();
}

void Base::extendDefined()
{
    outputTracker.extend(outputKey, [](FragmentTracker::Value *check,
                                       const FragmentTracker::ExtendTestContext &) {
        return Variant::Composite::isDefined(static_cast<const OutputValue *>(check)->result);
    });
}

void Base::extendDefined(const std::function<
        bool(const Variant::Read &, const FragmentTracker::Contents &)> &test)
{
    outputTracker.extend(outputKey, [&test](FragmentTracker::Value *check,
                                            const FragmentTracker::ExtendTestContext &context) {
        return test(static_cast<const OutputValue *>(check)->result, context.contents);
    });
}


String::String(FragmentTracker &tracker) : Base(tracker)
{ }

String::~String() = default;

QString String::toString(const FragmentTracker::Contents &contents, const QString &input) const
{
    Sub sub(*this, contents);
    return sub.apply(input);
}

QString String::toString(const FragmentTracker::Contents &contents) const
{ return toString(contents, get(contents).toQString()); }

FragmentTracker::MergeTestList String::mergeTest() const
{
    return {{[this](const FragmentTracker::Key &key,
                    FragmentTracker::Value *,
                    FragmentTracker::Value *) {
        return key == getKey();
    }, [this](const FragmentTracker::Key &,
              FragmentTracker::Value *,
              FragmentTracker::Value *,
              const FragmentTracker::MergeTestContext &context) {
        return toString(context.firstContents) == toString(context.secondContents);
    }}};
}

std::unique_ptr<Lua::Engine::Frame> String::scriptFrame(const FragmentTracker::Contents &) const
{ return {}; }

QString String::applySubstitution(const QStringList &, const FragmentTracker::Contents &) const
{ return {}; }

String::Sub::Sub(const String &parent, const FragmentTracker::Contents &contents) : parent(parent),
                                                                                    contents(
                                                                                            contents)
{ }

String::Sub::~Sub() = default;

QString String::Sub::substitution(const QStringList &elements) const
{ return parent.applySubstitution(elements, contents); }

QString String::Sub::literalCheck(QStringRef &key) const
{
    if (key.length() < 1)
        return {};
    if (key.at(0) == '<') {
        key = QStringRef(key.string(), key.position() + 1, key.length() - 1);
        return {'>'};
    }
    return TextSubstitution::literalCheck(key);
}

QString String::Sub::literalSubstitution(const QStringRef &key, const QString &) const
{
    if (key.isEmpty())
        return {};
    auto frame = parent.scriptFrame(contents);
    if (!frame)
        return {};

    Lua::Engine::Call call(*frame);
    if (!call.pushChunk(key.toString().toStdString(), false)) {
#ifndef NDEBUG
        auto error = call.errorDescription();
        call.clearError();
        return QString::fromStdString("_SCRIPT_PARSE_ERROR(" + error + ")");
#else
        call.clearError();
        return "_SCRIPT_PARSE_ERROR";
#endif
    }
    if (!call.execute(1)) {
#ifndef NDEBUG
        auto error = call.errorDescription();
        call.clearError();
        return QString::fromStdString("_SCRIPT_EXECUTE_ERROR(" + error + ")");
#else
        call.clearError();
        return "_SCRIPT_EXECUTE_ERROR";
#endif
    }
    return call.back().toQString();
}


Real::Real(FragmentTracker &tracker) : Base(tracker)
{ }

Real::~Real() = default;

Real::RealInput::RealInput(const Variant::Read &config) : DataInput(config),
                                                          cal(Variant::Composite::toCalibration(
                                                                  config["Calibration"]))
{ }

Real::RealInput::RealInput(const RealInput &) = default;

Real::RealInput::~RealInput() = default;

Variant::Root Real::RealInput::convert(const Variant::Read &input)
{
    if (input.getType() == Variant::Type::Real) {
        return Variant::Root(cal.apply(input.toReal()));
    }
    return DataInput::convert(input);
}

}
}
}