/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <QLoggingCategory>

#include "fanout.hxx"
#include "datacore/variant/composite.hxx"
#include "luascript/libs/sequencename.hxx"


Q_LOGGING_CATEGORY(log_fileoutput_fanout, "cpd3.fileoutput.fanout", QtWarningMsg)

using namespace CPD3::Data;

namespace CPD3 {
namespace Output {


StagedFanout::StagedFanout(const ValueSegment::Transfer &config,
                           bool enableDataInput,
                           bool enableMetadataInput) : baseSelection(
        DynamicSequenceSelection::fromConfiguration(config, "Input")),
                                                       fanoutStation(true),
                                                       fanoutArchive(true),
                                                       fanoutVariable(true),
                                                       fanoutFlavors(true),
                                                       fanoutFlattenMetadata(true),
                                                       fanoutFlattenStatistics(true),
                                                       fanoutTargetRemap(),
                                                       inputStageSelection(baseSelection->clone())
{
    if (enableDataInput)
        dataSink.reset(new DataSink(*this));
    if (enableMetadataInput)
        metadataSink.reset(new MetadataSink(*this));

    for (const auto &seg : config) {
        auto fanout = seg["Fanout"];

        if (fanout["Disable"].toBoolean()) {
            fanoutStation = false;
            fanoutArchive = false;
            fanoutVariable = false;
            fanoutFlavors = false;
        }

        if (fanout["Station"].exists())
            fanoutStation = fanout["Station"].toBoolean();
        if (fanout["Archive"].exists())
            fanoutArchive = fanout["Archive"].toBoolean();
        if (fanout["Variable"].exists())
            fanoutVariable = fanout["Variable"].toBoolean();
        if (fanout["Flavors"].exists())
            fanoutFlavors = fanout["Flavors"].toBoolean();
        if (fanout["FlattenMetadata"].exists())
            fanoutFlattenMetadata = fanout["FlattenMetadata"].toBoolean();
        if (fanout["FlattenStatistics"].exists())
            fanoutFlattenStatistics = fanout["FlattenStatistics"].toBoolean();
        if (fanout["RemapScript"].exists())
            fanoutTargetRemap = fanout["RemapScript"].toString();
    }
}

StagedFanout::~StagedFanout() = default;

Archive::Selection::List StagedFanout::toArchiveSelections(const Archive::Selection::Match &defaultStations,
                                                           const Archive::Selection::Match &defaultArchives,
                                                           const Archive::Selection::Match &defaultVariables) const
{
    auto result =
            baseSelection->toArchiveSelections(defaultStations, defaultArchives, defaultVariables);

    for (auto &mod : result) {
        mod.includeDefaultStation = true;
        mod.includeMetaArchive = fanoutFlattenMetadata;
    }

    return result;
}

std::unique_ptr<Lua::Engine::Frame> StagedFanout::scriptFrame(const SequenceName &target) const
{ return {}; }

std::vector<StreamSink *> StagedFanout::getTargets(const SequenceName &name)
{
    SequenceName target = name;
    if (fanoutFlattenMetadata) {
        target.clearMeta();
    }
    if (fanoutFlattenStatistics) {
        target.removeFlavor(SequenceName::flavor_cover);
        target.removeFlavor(SequenceName::flavor_stats);
    }
    if (!inputStageSelection->registerInput(target))
        return {};

    if (!name.isDefaultStation()) {
        SequenceName inputName = target;

        if (!fanoutStation)
            target.setStation(SequenceName::Component());
        if (!fanoutArchive)
            target.setArchive(SequenceName::Component());
        if (!fanoutVariable)
            target.setVariable(SequenceName::Component());
        if (!fanoutFlavors)
            target.clearFlavors();

        if (!fanoutTargetRemap.empty()) {
            auto frame = scriptFrame(target);
            if (frame) {
                Lua::Engine::Call call(*frame);
                if (!call.pushChunk(fanoutTargetRemap, false)) {
                    qCDebug(log_fileoutput_fanout) << "Error parsing script remapping for" << name
                                                   << "(" << target << "):"
                                                   << call.errorDescription();
                    call.clearError();
                } else if (!call.execute(1)) {
                    qCDebug(log_fileoutput_fanout) << "Error evaluating script remapping for"
                                                   << name << "(" << target << "):"
                                                   << call.errorDescription();
                    call.clearError();
                } else {
                    target = Lua::Libs::SequenceName::extract(call, call.back());
                }
            }
        }

        auto input = inputStageTargets.find(target);
        if (input == inputStageTargets.end()) {
            input = inputStageTargets.emplace(target, createInput()).first;
        }

        input->second->selection->registerInput(inputName);
    }

    std::vector<StreamSink *> result;
    if (dataSink) {
        if (!fanoutFlattenMetadata || !name.isMeta()) {
            result.emplace_back(dataSink.get());
        }
    }
    if (metadataSink) {
        if (name.isMeta()) {
            result.emplace_back(metadataSink.get());
        }
    }
    return result;
}

std::vector<StagedFanout::InputStage *> StagedFanout::getInputs() const
{
    std::vector<InputStage *> result;
    for (const auto &add : inputStageTargets) {
        result.emplace_back(add.second.get());
    }
    return result;
}

void StagedFanout::processDataSegments(CPD3::Data::SequenceSegment::Transfer &&segments)
{
    for (auto &seg : segments) {
        for (const auto &target : inputStageTargets) {
            target.second->processDataSegment(seg);
        }
    }
}

void StagedFanout::processMetadataSegments(CPD3::Data::SequenceSegment::Transfer &&segments)
{
    for (auto &seg : segments) {
        for (const auto &target : inputStageTargets) {
            target.second->processMetadataSegment(seg);
        }
    }
}

StagedFanout::DataSink::DataSink(StagedFanout &parent) : parent(parent)
{ }

StagedFanout::DataSink::~DataSink() = default;

void StagedFanout::DataSink::incomingData(const SequenceValue::Transfer &values)
{ parent.processDataSegments(parent.inputStageDataStream.add(values)); }

void StagedFanout::DataSink::incomingData(SequenceValue::Transfer &&values)
{ parent.processDataSegments(parent.inputStageDataStream.add(std::move(values))); }

void StagedFanout::DataSink::incomingData(const SequenceValue &value)
{ parent.processDataSegments(parent.inputStageDataStream.add(value)); }

void StagedFanout::DataSink::incomingData(SequenceValue &&value)
{ parent.processDataSegments(parent.inputStageDataStream.add(std::move(value))); }

void StagedFanout::DataSink::endData()
{ parent.processDataSegments(parent.inputStageDataStream.finish()); }

StagedFanout::MetadataSink::MetadataSink(StagedFanout &parent) : parent(parent)
{ }

StagedFanout::MetadataSink::~MetadataSink() = default;

void StagedFanout::MetadataSink::incomingData(const SequenceValue::Transfer &values)
{ parent.processMetadataSegments(parent.inputStageMetadataStream.add(values)); }

void StagedFanout::MetadataSink::incomingData(SequenceValue::Transfer &&values)
{ parent.processMetadataSegments(parent.inputStageMetadataStream.add(std::move(values))); }

void StagedFanout::MetadataSink::incomingData(const SequenceValue &value)
{ parent.processMetadataSegments(parent.inputStageMetadataStream.add(value)); }

void StagedFanout::MetadataSink::incomingData(SequenceValue &&value)
{ parent.processMetadataSegments(parent.inputStageMetadataStream.add(std::move(value))); }

void StagedFanout::MetadataSink::endData()
{ parent.processMetadataSegments(parent.inputStageMetadataStream.finish()); }

StagedFanout::InputStage::InputStage(StagedFanout &parent) : parent(parent),
                                                             selection(
                                                                     parent.baseSelection->clone())
{ }

StagedFanout::InputStage::~InputStage() = default;

bool StagedFanout::InputStage::isActive(double start, double end) const
{ return selection->getConst(start, end).size() != 0; }

void StagedFanout::InputStage::processDataSegment(SequenceSegment &)
{ }

void StagedFanout::InputStage::processMetadataSegment(SequenceSegment &)
{ }

SequenceName::Set StagedFanout::InputStage::get(double start, double end) const
{ return selection->getConst(start, end); }

static Variant::Read accessData(SequenceSegment &segment, const SequenceName::Set &set)
{
    for (const auto &name : set) {
        auto check = segment[name];
        if (!Variant::Composite::isDefined(check))
            continue;
        return check;
    }
    return Variant::Read::empty();
}

static Variant::Read accessMetadata(SequenceSegment &segment, const SequenceName::Set &set)
{
    for (auto name : set) {
        name.setMeta();
        auto check = segment[std::move(name)];
        if (!Variant::Composite::isDefined(check))
            continue;
        return check;
    }
    return Variant::Read::empty();
}

Variant::Read StagedFanout::InputStage::getData(SequenceSegment &segment) const
{ return accessData(segment, get(segment.getStart(), segment.getEnd())); }

Variant::Read StagedFanout::InputStage::getMetadata(SequenceSegment &segment) const
{ return accessMetadata(segment, get(segment.getStart(), segment.getEnd())); }

StagedFanout::OutputStage::OutputStage(const StagedFanout::InputStage &origin, double start, double)
        : selection(origin.selection->clone()),
          includeMetadata(origin.parent.fanoutFlattenMetadata),
          includeStatistics(origin.parent.fanoutFlattenStatistics)
{
    if (FP::defined(start))
        selection->get(start);
}

StagedFanout::OutputStage::~OutputStage() = default;

SequenceName::Set StagedFanout::OutputStage::get(double start, double end)
{ return selection->get(start, end); }

Variant::Read StagedFanout::OutputStage::getData(SequenceSegment &segment)
{ return accessData(segment, get(segment.getStart(), segment.getEnd())); }

Variant::Read StagedFanout::OutputStage::getMetadata(SequenceSegment &segment)
{ return accessMetadata(segment, get(segment.getStart(), segment.getEnd())); }

bool StagedFanout::OutputStage::isPossiblyUsed(const SequenceName &name) const
{
    auto possible = selection->getAllUnits();
    if (possible.count(name) != 0)
        return true;

    SequenceName target = name;
    if (includeMetadata) {
        target.clearMeta();
    }
    if (includeStatistics) {
        target.removeFlavor(SequenceName::flavor_cover);
        target.removeFlavor(SequenceName::flavor_stats);
    }
    if (possible.count(target) != 0)
        return true;

    if (name.isDefaultStation()) {
        for (const auto &check : possible) {
            if (check.getVariable() != target.getVariable())
                continue;
            if (check.getArchive() != target.getArchive())
                continue;
            if (check.getFlavorsString() != target.getFlavorsString())
                continue;
            return true;
        }
    }

    return false;
}

bool StagedFanout::OutputStage::isDirectlyUsed(const SequenceName &name) const
{ return selection->getAllUnits().count(name) != 0; }

}
}