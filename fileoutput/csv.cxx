/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <cstdint>
#include <cmath>
#include <array>

#include "csv.hxx"
#include "datacore/variant/composite.hxx"
#include "datacore/variant/matrix.hxx"
#include "io/drivers/stdio.hxx"

using namespace CPD3::Data;

namespace CPD3 {
namespace Output {

namespace {

class Source {
public:
    Source() = default;

    virtual ~Source() = default;

    virtual Variant::Read fetch(SequenceSegment &row) = 0;
};

class Source_Simple : public Source {
    SequenceName name;
public:
    Source_Simple(SequenceName name) : name(std::move(name))
    { }

    virtual ~Source_Simple() = default;

    Variant::Read fetch(SequenceSegment &row) override
    { return row[name]; }
};

class Source_FirstExists : public Source {
    std::vector<std::unique_ptr<Source>> sources;
public:
    Source_FirstExists(std::vector<std::unique_ptr<Source>> &&sources) : sources(std::move(sources))
    { }

    virtual ~Source_FirstExists() = default;

    Variant::Read fetch(SequenceSegment &row) override
    {
        for (const auto &s : sources) {
            auto check = s->fetch(row);
            if (!check.exists())
                continue;
            return check;
        }
        return Variant::Read::empty();
    }
};

class Source_Path : public Source {
    std::unique_ptr<Source> source;
    Variant::Path path;
public:
    Source_Path(std::unique_ptr<Source> &&source, Variant::Path path) : source(std::move(source)),
                                                                        path(std::move(path))
    { }

    virtual ~Source_Path() = default;

    Variant::Read fetch(SequenceSegment &row) override
    { return source->fetch(row).getPath(path); }
};

class Column {
public:
    Column() : header_cutSize(QObject::tr("NA", "cut header MVC"))
    { }

    virtual ~Column() = default;

    virtual QString value(SequenceSegment &row) = 0;

    QString mvc;
    QString header_name;
    QString header_description;
    QString header_cutSize;
    SequenceName header_wavelength;

    virtual QStringList header_flags() const
    { return {}; }
};

class Column_Constant : public Column {
    QString output;
public:
    explicit Column_Constant(const QString &output) : output(output)
    { }

    virtual ~Column_Constant() = default;

    QString value(SequenceSegment &) override
    { return output; }
};

class Column_DateTime : public Column {
    QString format;
public:
    explicit Column_DateTime(const QString &format) : format(format)
    { }

    virtual ~Column_DateTime() = default;

    QString value(SequenceSegment &row) override
    {
        if (!FP::defined(row.getStart()))
            return mvc;
        return Time::toDateTime(row.getStart(), true).toString(format);
    }
};

class Column_EpochTime : public Column {
public:
    Column_EpochTime() = default;

    virtual ~Column_EpochTime() = default;

    QString value(SequenceSegment &row) override
    {
        if (!FP::defined(row.getStart()))
            return mvc;
        return NumberFormat(1, 0).apply(row.getStart(), QChar());
    }
};

class Column_YearTime : public Column {
    int decimals;
    NumberFormat format;
public:
    explicit Column_YearTime(int decimals) : decimals(decimals), format(1, decimals)
    { }

    virtual ~Column_YearTime() = default;

    QString value(SequenceSegment &row) override
    {
        if (!FP::defined(row.getStart()))
            return mvc;
        int year = Time::toDateTime(row.getStart(), true).date().year();
        if (decimals == 0)
            return QString::number(year);

        double yearStart =
                Time::fromDateTime(QDateTime(QDate(year, 1, 1), QTime(0, 0, 0), Qt::UTC));
        double yearEnd =
                Time::fromDateTime(QDateTime(QDate(year + 1, 1, 1), QTime(0, 0, 0), Qt::UTC));

        return format.apply(year + (row.getStart() - yearStart) / (yearEnd - yearStart), QChar());
    }
};

class Column_JulianDayTime : public Column {
    NumberFormat format;
public:
    Column_JulianDayTime() : format(1, 5)
    { }

    virtual ~Column_JulianDayTime() = default;

    QString value(SequenceSegment &row) override
    {
        if (!FP::defined(row.getStart()))
            return mvc;
        return NumberFormat(1, 5).apply(row.getStart() / 86400.0 + 2440587.5, QChar())
                                 .rightJustified(13, '0');
    }
};

class Column_DOYTime : public Column {
public:
    Column_DOYTime() = default;

    virtual ~Column_DOYTime() = default;

    QString value(SequenceSegment &row) override
    {
        if (!FP::defined(row.getStart()))
            return mvc;
        double secondsAfterYear = row.getStart() -
                Time::fromDateTime(
                        QDateTime(QDate(Time::toDateTime(row.getStart()).date().year(), 1, 1),
                                  QTime(0, 0, 0), Qt::UTC));
        return QString::number(secondsAfterYear / 86400.0 + 1.0, 'f', 5).rightJustified(9, '0');
    }
};

class Column_PresentCutSizes : public Column {
public:
    Column_PresentCutSizes() = default;

    virtual ~Column_PresentCutSizes() = default;

    QString value(SequenceSegment &row) override
    {
        bool hasPM1 = false;
        bool hasPM10 = false;
        bool hasPM25 = false;
        for (const auto &name : row.getUnits()) {
            if (!hasPM1 && name.hasFlavor(SequenceName::flavor_pm1)) {
                hasPM1 = true;
                if (hasPM10 && hasPM25)
                    break;
            }
            if (!hasPM10 && name.hasFlavor(SequenceName::flavor_pm10)) {
                hasPM10 = true;
                if (hasPM1 && hasPM25)
                    break;
            }
            if (!hasPM25 && name.hasFlavor(SequenceName::flavor_pm25)) {
                hasPM25 = true;
                if (hasPM1 && hasPM10)
                    break;
            }
        }
        if (!hasPM1 && !hasPM10 && !hasPM25)
            return mvc;
        QString result;
        if (hasPM1) {
            if (!result.isEmpty())
                result += ';';
            result += "PM1";
        }
        if (hasPM25) {
            if (!result.isEmpty())
                result += ';';
            result += "PM25";
        }
        if (hasPM10) {
            if (!result.isEmpty())
                result += ';';
            result += "PM10";
        }
        return result;
    }
};

class Column_Standard : public Column {
    std::unique_ptr<Source> source;
public:
    NumberFormat format;
    bool strictNumeric;

    explicit Column_Standard(std::unique_ptr<Source> &&source) : source(std::move(source)),
                                                                 format(),
                                                                 strictNumeric(false)
    { }

    virtual ~Column_Standard() = default;

    QString value(SequenceSegment &row) override
    {
        auto v = source->fetch(row);
        if (!Variant::Composite::isDefined(v))
            return mvc;
        switch (v.getType()) {
        case Variant::Type::Real: {
            auto r = v.toReal();
            if (!FP::defined(r))
                return mvc;
            return format.apply(r);
        }
        case Variant::Type::Integer: {
            auto i = v.toInteger();
            if (!INTEGER::defined(i))
                return mvc;
            return format.apply((qint64) i);
        }
        default:
            if (strictNumeric)
                return mvc;
            return v.toQString().rightJustified(format.width(), ' ');
        }
    }
};

class Column_IsMissing : public Column {
    std::unique_ptr<Source> source;

    static const QString value_valid;
    static const QString value_missing;
public:
    explicit Column_IsMissing(std::unique_ptr<Source> &&source) : source(std::move(source))
    { }

    virtual ~Column_IsMissing() = default;

    QString value(SequenceSegment &row) override
    {
        return Variant::Composite::isDefined(source->fetch(row)) ? value_valid : value_missing;
    }
};

const QString Column_IsMissing::value_valid = "0";
const QString Column_IsMissing::value_missing = "1";

class Column_BitFlags : public Column {
    std::unique_ptr<Source> source;
    std::unordered_map<Variant::Flag, std::uint_fast64_t> bits;
public:
    NumberFormat format;
    std::unordered_map<Variant::Flag, QString> flagDescriptions;

    explicit Column_BitFlags(std::unique_ptr<Source> &&source,
                             std::unordered_map<Variant::Flag, std::uint_fast64_t> bits) : source(
            std::move(source)), bits(std::move(bits))
    { }

    virtual ~Column_BitFlags() = default;

    QStringList header_flags() const override
    {
        std::vector<std::pair<Variant::Flag, std::uint_fast64_t>> sorted;
        for (const auto &add : bits) {
            sorted.emplace_back(add);
        }
        std::sort(sorted.begin(), sorted.end(),
                  [](const std::pair<Variant::Flag, std::uint_fast64_t> &a,
                     const std::pair<Variant::Flag, std::uint_fast64_t> &b) {
                      return a.second < b.second;
                  });

        QStringList result;
        for (const auto &add : sorted) {
            auto numeric = format.apply(static_cast<qint64>(add.second));

            auto desc = flagDescriptions.find(add.first);
            if (desc != flagDescriptions.end()) {
                result.push_back(
                        QObject::tr("%1 - %2", "flag bits description").arg(numeric, desc->second));
            } else {
                result.push_back(QObject::tr("%1 - %2", "flag bits name").arg(numeric,
                                                                              QString::fromStdString(
                                                                                      add.first)));
            }

        }
        return result;
    }

    QString value(SequenceSegment &row) override
    {
        auto v = source->fetch(row);
        if (v.getType() != Variant::Type::Flags)
            return mvc;
        const auto &flags = v.toConstFlags();
        std::uint_fast64_t set = 0;
        for (const auto &f : flags) {
            auto check = bits.find(f);
            if (check == bits.end())
                continue;
            set |= check->second;
        }
        return format.apply(static_cast<qint64>(set));
    }
};

class Column_FlagSet : public Column {
    std::unique_ptr<Source> source;
    Variant::Flag test;

    static const QString value_set;
    static const QString value_clear;
public:
    QString flagDescription;

    explicit Column_FlagSet(std::unique_ptr<Source> &&source, Variant::Flag flag) : source(
            std::move(source)), test(std::move(flag))
    { }

    virtual ~Column_FlagSet() = default;

    QStringList header_flags() const override
    {
        if (flagDescription.isEmpty())
            return {};
        return {flagDescription};
    }

    QString value(SequenceSegment &row) override
    {
        auto v = source->fetch(row);
        if (v.getType() != Variant::Type::Flags)
            return mvc;
        return v.testFlag(test) ? value_set : value_clear;
    }
};

class Column_ListFlags : public Column {
    std::unique_ptr<Source> source;
public:
    std::unordered_map<Variant::Flag, QString> flagDescriptions;

    explicit Column_ListFlags(std::unique_ptr<Source> &&source) : source(std::move(source))
    { }

    virtual ~Column_ListFlags() = default;

    QStringList header_flags() const override
    {
        std::vector<Variant::Flag> sorted;
        for (const auto &add : flagDescriptions) {
            sorted.emplace_back(add.first);
        }
        std::sort(sorted.begin(), sorted.end());

        QStringList result;
        for (const auto &flag : sorted) {
            const auto &desc = flagDescriptions.find(flag)->second;
            result.push_back(QObject::tr("%1 - %2", "flag list description").arg(
                    QString::fromStdString(flag), desc));
        }
        return result;
    }

    QString value(SequenceSegment &row) override
    {
        auto v = source->fetch(row);
        if (v.getType() != Variant::Type::Flags)
            return mvc;
        QStringList flags = Util::to_qstringlist(v.toConstFlags());
        std::sort(flags.begin(), flags.end());
        return flags.join(';');
    }
};

const QString Column_FlagSet::value_set = "1";
const QString Column_FlagSet::value_clear = "0";

class Column_Quantile : public Column {
    std::unique_ptr<Source> source;
    double q;

    double convert(SequenceSegment &row) const
    {
        auto quantiles = source->fetch(row).toKeyframe();
        auto lower = quantiles.lower_bound(q);
        auto upper = quantiles.upper_bound(q);
        if (lower == quantiles.end())
            return FP::undefined();
        if (std::fabs(lower.key() - q) < 1E-6)
            return lower.value().toReal();
        if (lower == quantiles.begin())
            return FP::undefined();
        --lower;
        if (upper == quantiles.end())
            return lower.value().toReal();
        if (upper == lower)
            return FP::undefined();
        Q_ASSERT(lower.key() != upper.key());
        double vUpper = upper.value().toDouble();
        if (!FP::defined(vUpper))
            return FP::undefined();
        double vLower = lower.value().toDouble();
        if (!FP::defined(vLower))
            return FP::undefined();
        return vLower + (vUpper - vLower) * (q - lower.key()) / (upper.key() - lower.key());
    }

public:
    NumberFormat format;

    explicit Column_Quantile(std::unique_ptr<Source> &&source, double q) : source(
            std::move(source)), q(q)
    { }

    virtual ~Column_Quantile() = default;

    QString value(SequenceSegment &row) override
    {
        double v = convert(row);
        if (!FP::defined(v))
            return mvc;
        return format.apply(v);
    }
};


struct State {
    CSV config;

    struct CutOverlapData {
        SequenceName priorName;
        double currentEnd;
        bool overlapDetected;

        CutOverlapData() : currentEnd(FP::undefined()), overlapDetected(false)
        { }

        explicit CutOverlapData(const SequenceValue &value) : priorName(value.getName()),
                                                              currentEnd(value.getEnd()),
                                                              overlapDetected(false)
        { }

        void advance(const SequenceValue &value)
        {
            priorName = value.getName();
            currentEnd = value.getEnd();
        }
    };

    SequenceName::Map<CutOverlapData> overlapTracking;

    explicit State(CSV config) : config(std::move(config))
    { }

    static bool prepareNameForOverlapDetection(SequenceName &name)
    {
        if (name.isMeta())
            return false;
        if (name.hasFlavor(SequenceName::flavor_cover))
            return false;
        if (name.hasFlavor(SequenceName::flavor_stats))
            return false;
        if (name.hasFlavor(SequenceName::flavor_end))
            return false;

        name.removeFlavor(SequenceName::flavor_pm1);
        name.removeFlavor(SequenceName::flavor_pm10);
        name.removeFlavor(SequenceName::flavor_pm25);
        return true;
    }

    void processFirstStage(SequenceValue::Transfer &&values)
    {
        for (auto &i : values) {
            auto name = i.getName();
            if (!prepareNameForOverlapDetection(name))
                continue;

            auto ov = overlapTracking.find(name);
            if (ov == overlapTracking.end()) {
                overlapTracking.emplace(name, CutOverlapData(i));
                continue;
            }

            auto &data = ov->second;

            /* Already overlapping, so no detection needed */
            if (data.overlapDetected)
                continue;

            /* Starts after the end, so can't overlap */
            if (Range::compareStartEnd(i.getStart(), data.currentEnd) >= 0) {
                data.advance(i);
                continue;
            }

            /* An identical name, so it can't overlap (just advance the end) */
            if (i.getName() == data.priorName) {
                if (Range::compareEnd(i.getEnd(), data.currentEnd) > 0)
                    data.currentEnd = i.getEnd();
                continue;
            }

            data.overlapDetected = true;
        }
    }

    struct RemapData {
        SequenceName target;
        bool merged;

        RemapData() : merged(false)
        { }

        RemapData(const SequenceName &target, bool merged) : target(target), merged(merged)
        { }
    };

    SequenceName::Map<RemapData> remap;
    SequenceName::ComponentSet seenStations;
    SequenceName::ComponentSet seenArchives;

    void completeFirstStage()
    {
        for (const auto &input : overlapTracking) {
            seenStations.insert(input.first.getStation());
            seenArchives.insert(input.first.getArchive());

            bool doSplit = input.second.overlapDetected;

            switch (config.cutSplitMode) {
            case CSV::CutSplitMode::Automatic:
                break;
            case CSV::CutSplitMode::Always:
                doSplit = true;
                break;
            case CSV::CutSplitMode::Never:
                doSplit = false;
                break;
            }

            if (!doSplit) {
                RemapData rt(input.first, true);
                remap.emplace(input.first, rt);
                remap.emplace(input.first.withFlavor(SequenceName::flavor_pm1), rt);
                remap.emplace(input.first.withFlavor(SequenceName::flavor_pm10), rt);
                remap.emplace(input.first.withFlavor(SequenceName::flavor_pm25), rt);
            } else {
                remap.emplace(input.first, RemapData(input.first, false));
                remap.emplace(input.first.withFlavor(SequenceName::flavor_pm1),
                              RemapData(input.first.withFlavor(SequenceName::flavor_pm1), false));
                remap.emplace(input.first.withFlavor(SequenceName::flavor_pm10),
                              RemapData(input.first.withFlavor(SequenceName::flavor_pm10), false));
                remap.emplace(input.first.withFlavor(SequenceName::flavor_pm25),
                              RemapData(input.first.withFlavor(SequenceName::flavor_pm25), false));
            }
        }

        seenStations.erase("_");
    }


    SequenceName::Set requiredOutputs;
    SequenceName::Set seenStats;
    SequenceName::Set seenCover;
    SequenceName::Set seenEnd;

    struct ArrayTracking {
        std::vector<std::size_t> dimensions;
        QString format;
        QString mvc;

        bool isValid() const
        {
            if (dimensions.empty())
                return false;

            for (auto check : dimensions) {
                if (check == 0)
                    return false;
            }
            return true;
        }
    };

    SequenceName::Map<ArrayTracking> arrays;

    void processSecondStageMetadataArray(SequenceValue &value)
    {
        auto rv = value.read();
        {
            auto mvc = rv.metadata("Children").metadata("MVC").toDisplayString();
            if (!mvc.isEmpty())
                arrays[value.getName()].mvc = mvc;
        }
        {
            auto format = rv.metadata("Children").metadata("Format").toDisplayString();
            if (!format.isEmpty())
                arrays[value.getName()].format = format;
        }

        switch (rv.getType()) {
        case Variant::Type::MetadataArray: {
            auto size = rv.metadataArray("Count").toInteger();
            if (!INTEGER::defined(size) || size <= 0)
                return;
            auto &tracking = arrays[value.getName()];
            if (tracking.dimensions.empty()) {
                tracking.dimensions.emplace_back(static_cast<std::size_t>(size));
            } else {
                tracking.dimensions.front() =
                        std::max(tracking.dimensions.front(), static_cast<std::size_t>(size));
            }
            break;
        }
        case Variant::Type::MetadataMatrix: {
            std::vector<std::size_t> shape;
            for (const auto &add : rv.metadataMatrix("Size").toArray()) {
                auto dim = add.toInteger();
                if (!INTEGER::defined(dim) || dim <= 0)
                    dim = 0;
                shape.emplace_back(dim);
            }
            while (!shape.empty() && shape.back() == 0) {
                shape.pop_back();
            }
            if (shape.empty())
                return;
            auto &tracking = arrays[value.getName()];
            if (tracking.dimensions.empty()) {
                tracking.dimensions = std::move(shape);
            } else {
                for (size_t dim = 0; dim < shape.size(); dim++) {
                    if (dim > tracking.dimensions.size()) {
                        tracking.dimensions.emplace_back(shape[dim]);
                    } else {
                        tracking.dimensions[dim] = std::max(tracking.dimensions[dim], shape[dim]);
                    }
                }
            }
            break;
        }
        default:
            break;
        }
    }

    void processSecondStageDataArray(SequenceValue &value)
    {
        auto rv = value.read();
        switch (rv.getType()) {
        case Variant::Type::Array: {
            auto &tracking = arrays[value.getName()];
            if (tracking.dimensions.empty()) {
                tracking.dimensions.emplace_back(rv.toArray().size());
            } else {
                tracking.dimensions.front() =
                        std::max(tracking.dimensions.front(), rv.toArray().size());
            }
            break;
        }
        case Variant::Type::Matrix: {
            auto &tracking = arrays[value.getName()];
            if (tracking.dimensions.empty()) {
                tracking.dimensions = rv.toMatrix().shape();
            } else {
                auto shape = rv.toMatrix().shape();
                for (size_t dim = 0; dim < shape.size(); dim++) {
                    if (dim > tracking.dimensions.size()) {
                        tracking.dimensions.emplace_back(shape[dim]);
                    } else {
                        tracking.dimensions[dim] = std::max(tracking.dimensions[dim], shape[dim]);
                    }
                }
            }
            break;
        }
        default:
            break;
        }
    }

    struct PathTracking {
        std::unordered_set<Variant::Path> children;

        void integrateData(const Variant::Read &base)
        {
            switch (base.getType()) {
            case Variant::Type::Hash:
            case Variant::Type::Array:
            case Variant::Type::Matrix:
            case Variant::Type::Keyframe:
                for (auto child : base.toConstChildren()) {
                    integrateData(child);
                }
                break;
            default:
                if (base.currentPath().empty())
                    break;
                children.insert(base.currentPath());
                break;
            }
        }
    };

    SequenceName::Map<PathTracking> paths;

    void processSecondStagePaths(SequenceValue &value)
    {
        if (!config.splitOutputPaths)
            return;
        paths[value.getName()].integrateData(value.read());
    }

    struct MetadataTracking {
        QString mvc;
        QString numberFormat;
        QString description;
        QString units;

        std::unordered_map<Variant::Flag, std::uint_fast64_t> flagBits;
        std::unordered_map<Variant::Flag, QString> flagDescriptions;
    };
    SequenceName::Map<MetadataTracking> metadata;

    void integrateSecondStageMetadata(MetadataTracking &tracking, const Variant::Read &value)
    {
        {
            QString mvc = value.metadata("MVC").toDisplayString();
            if (!mvc.isEmpty()) {
                tracking.mvc = mvc;
            }
        }
        {
            QString format = value.metadata("Format").toDisplayString();
            if (!format.isEmpty()) {
                tracking.numberFormat = format;
            }
        }
        {
            QString desc = value.metadata("Description").toDisplayString();
            if (!desc.isEmpty()) {
                tracking.description = desc;
            }
        }
        {
            QString units = value.metadata("Units").toDisplayString();
            if (!units.isEmpty()) {
                tracking.units = units;
            }
        }

        switch (value.getType()) {
        case Variant::Type::MetadataFlags: {
            for (auto flag : value.toMetadataSingleFlag()) {
                auto bits = flag.second.hash("Bits").toInteger();
                if (INTEGER::defined(bits) && bits != 0) {
                    tracking.flagBits[flag.first] |= bits;
                }

                auto desc = flag.second.hash("Description").toDisplayString();
                if (!desc.isEmpty()) {
                    tracking.flagDescriptions[flag.first] = desc;
                }
            }
            break;
        }
        default:
            break;
        }
    }

    void processSecondStageMetadata(SequenceValue &value)
    {
        auto rv = value.read();
        auto &tracking = metadata[value.getName()];

        integrateSecondStageMetadata(tracking, rv);

        switch (rv.getType()) {
        case Variant::Type::MetadataArray:
        case Variant::Type::MetadataMatrix:
            integrateSecondStageMetadata(tracking, rv.metadata("Children"));
            break;
        default:
            break;
        }
    }

    struct WavelengthTracking {
        struct WavelengthOverlay : public Time::Bounds {
            double wavelength;

            WavelengthOverlay(double start, double end, double wavelength) : Time::Bounds(start,
                                                                                          end),
                                                                             wavelength(wavelength)
            { }
        };

        struct SourceOverlay : public Time::Bounds {
            QString source;

            SourceOverlay(double start, double end, const QString &source) : Time::Bounds(start,
                                                                                          end),
                                                                             source(source)
            { }
        };

        struct Segment : public Time::Bounds {
            double wavelength;
            QString source;

            Segment(const Segment &other, double start, double end) : Time::Bounds(start, end),
                                                                      wavelength(other.wavelength),
                                                                      source(other.source)
            { }

            Segment(const WavelengthOverlay &over, double start, double end) : Time::Bounds(start,
                                                                                            end),
                                                                               wavelength(
                                                                                       over.wavelength),
                                                                               source()
            { }

            Segment(const Segment &under, const WavelengthOverlay &over, double start, double end)
                    : Time::Bounds(start, end), wavelength(over.wavelength), source(under.source)
            { }

            Segment(const SourceOverlay &over, double start, double end) : Time::Bounds(start, end),
                                                                           wavelength(
                                                                                   FP::undefined()),
                                                                           source(over.source)
            { }

            Segment(const Segment &under, const SourceOverlay &over, double start, double end)
                    : Time::Bounds(start, end), wavelength(under.wavelength), source(over.source)
            { }

            bool canEliminate() const
            {
                return !FP::defined(wavelength);
            }

            bool canMerge(const Segment &other) const
            {
                return FP::equal(wavelength, other.wavelength) && source == other.source;
            }
        };

        std::deque<Segment> segments;

        void overlayWavelength(double start, double end, double wavelength)
        {
            Range::overlayFragmenting(segments, WavelengthOverlay(start, end, wavelength));
        }

        void overlaySource(double start, double end, const QString &source)
        {
            Range::overlayFragmenting(segments, SourceOverlay(start, end, source));
        }

        bool completeMerge()
        {
            for (auto first = segments.begin(); first != segments.end();) {
                if (first->canEliminate()) {
                    first = segments.erase(first);
                    continue;
                }

                auto second = first;
                ++second;
                if (second == segments.end())
                    break;

                if (first->canMerge(*second)) {
                    second->setStart(first->getStart());
                    first = segments.erase(first);
                    continue;
                }

                first = second;
            }
            return !segments.empty();
        }
    };

    SequenceName::Map<WavelengthTracking> wavelength;

    void processSecondStageWavelength(SequenceValue &value)
    {
        auto rv = value.read();
        {
            double wl = rv.metadata("Wavelength").toReal();
            if (FP::defined(wl)) {
                wavelength[value.getName()].overlayWavelength(value.getStart(), value.getEnd(), wl);
            }
        }
        {
            QString source;
            auto sdata = rv.metadata("Source");
            if (sdata.getType() == Variant::Type::Hash) {
                auto manf = rv.metadata("Source").hash("Manufacturer").toDisplayString();
                auto inst = rv.metadata("Source").hash("Model").toDisplayString();
                if (!manf.isEmpty()) {
                    if (!source.isEmpty())
                        source += " ";
                    source += manf;
                }
                if (!inst.isEmpty()) {
                    if (!source.isEmpty())
                        source += " ";
                    source += inst;
                }
            } else {
                source = sdata.toDisplayString();
            }
            if (!source.isEmpty()) {
                wavelength[value.getName()].overlaySource(value.getStart(), value.getEnd(), source);
            }
        }
    }

    SequenceName::Map<Variant::Flags> seenFlags;

    void integrateSecondStageFlags(const SequenceName &name, const Variant::Read &value)
    {
        if (value.getType() != Variant::Type::Flags)
            return;

        auto &target = seenFlags[name];
        for (const auto &v : value.toFlags()) {
            target.insert(v);
        }
    }

    void processSecondStageDataFlags(SequenceValue &value)
    {
        auto rv = value.read();
        switch (rv.getType()) {
        case Variant::Type::Flags:
            integrateSecondStageFlags(value.getName(), rv);
            break;
        case Variant::Type::Array:
        case Variant::Type::Matrix:
            for (auto child : rv.toConstChildren()) {
                integrateSecondStageFlags(value.getName(), child);
            }
            break;
        default:
            break;
        }
    }

    void processSecondStage(SequenceValue::Transfer &&values)
    {
        for (auto &v : values) {
            auto &name = v.getName();

            if (name.isMeta()) {
                name.clearMeta();

                auto remapped = remap.find(name);
                if (remapped == remap.end())
                    continue;
                name = remapped->second.target;

                processSecondStageMetadataArray(v);
                processSecondStageMetadata(v);
                processSecondStageWavelength(v);
            } else {
                if (name.hasFlavor(SequenceName::flavor_stats)) {
                    name.removeFlavor(SequenceName::flavor_stats);
                    auto remapped = remap.find(name);
                    if (remapped != remap.end()) {
                        seenStats.insert(remapped->second.target);
                    }
                    continue;
                }
                if (name.hasFlavor(SequenceName::flavor_cover)) {
                    name.removeFlavor(SequenceName::flavor_cover);
                    auto remapped = remap.find(name);
                    if (remapped != remap.end()) {
                        seenCover.insert(remapped->second.target);
                    }
                    continue;
                }
                if (name.hasFlavor(SequenceName::flavor_end)) {
                    name.removeFlavor(SequenceName::flavor_end);
                    auto remapped = remap.find(name);
                    if (remapped != remap.end()) {
                        seenEnd.insert(remapped->second.target);
                    }
                    continue;
                }

                auto remapped = remap.find(name);
                if (remapped == remap.end())
                    continue;
                name = remapped->second.target;

                requiredOutputs.insert(name);
                processSecondStageDataArray(v);
                processSecondStagePaths(v);
                processSecondStageDataFlags(v);
            }
        }
    }

    void completeSecondStage()
    {
        for (auto a = arrays.begin(); a != arrays.end();) {
            if (a->second.isValid()) {
                if (!a->second.mvc.isEmpty()) {
                    metadata[a->first].mvc = a->second.mvc;
                }
                if (!a->second.format.isEmpty()) {
                    metadata[a->first].numberFormat = a->second.format;
                }

                ++a;
            } else {
                a = arrays.erase(a);
            }
        }
        for (auto wl = wavelength.begin(); wl != wavelength.end();) {
            if (wl->second.completeMerge()) {
                ++wl;
            } else {
                wl = wavelength.erase(wl);
            }
        }
    }


    std::vector<std::unique_ptr<Column>> outputColumns;

    SequenceSegment pendingOutput;
    bool havePendingOutput;
    double priorEndTime;

    bool arrayFanout(const SequenceName &name, std::vector<Variant::Path> &fanout)
    {
        auto check = arrays.find(name);
        if (check == arrays.end())
            return false;
        const auto &tracking = check->second;
        Q_ASSERT(!tracking.dimensions.empty());

        if (tracking.dimensions.size() == 1) {
            for (std::size_t i = 0; i < tracking.dimensions.front(); i++) {
                fanout.emplace_back(Variant::Path{
                        Variant::PathElement(Variant::PathElement::Type::Array,
                                             static_cast<std::size_t>(i))});
            }
            return true;
        }

        size_t total = 1;
        for (auto s : tracking.dimensions) {
            total *= s;
        }
        for (std::size_t i = 0; i < total; i++) {
            fanout.emplace_back(Variant::Path{
                    Variant::PathElement(Variant::PathElement::Type::Matrix,
                                         Variant::NodeMatrix::fromIndex(i, tracking.dimensions))});
        }
        return true;
    }

    bool pathFanout(const SequenceName &name, std::vector<Variant::Path> &fanout)
    {
        if (!config.splitOutputPaths)
            return false;
        auto check = paths.find(name);
        if (check == paths.end())
            return false;
        const auto &tracking = check->second;
        if (tracking.children.empty())
            return false;

        std::vector<Variant::Path> sorted(tracking.children.begin(), tracking.children.end());
        std::sort(sorted.begin(), sorted.end(), [](const Variant::Path &a, const Variant::Path &b) {
            std::size_t max = std::min(a.size(), b.size());
            std::size_t i = 0;
            for (auto ca = a.begin(), cb = b.begin(); i < max; ++i, ++ca, ++cb) {
                if (*ca == *cb)
                    continue;
                return Variant::PathElement::OrderDisplay()(*ca, *cb);
            }

            if (i >= a.size()) {
                return i < b.size();
            }

            return false;
        });

        Util::append(std::move(sorted), fanout);
        return true;
    }

    void beginOutput()
    {
        createFixedColumns();

        std::vector<std::pair<SequenceName, std::vector<Variant::Path>>> exportFanout;
        {
            std::vector<SequenceName> exportOrder(requiredOutputs.begin(), requiredOutputs.end());
            struct HeaderSorter {
                SequenceName::OrderDisplayCaching order;

                bool operator()(const SequenceName &a, const SequenceName &b)
                {
                    int p1 = order.priority(a);
                    int p2 = order.priority(b);
                    if (p1 != p2)
                        return p1 < p2;

                    int cr = a.getStation().compare(b.getStation());
                    if (cr != 0)
                        return cr < 0;

                    cr = a.getArchive().compare(b.getArchive());
                    if (cr != 0)
                        return cr < 0;

                    return a.getVariable() < b.getVariable();
                }
            };
            std::sort(exportOrder.begin(), exportOrder.end(), HeaderSorter());

            for (auto &name : exportOrder) {
                std::vector<Variant::Path> fanout;

                if (!pathFanout(name, fanout) && !arrayFanout(name, fanout)) {
                    fanout.emplace_back();
                }

                exportFanout.emplace_back(std::move(name), std::move(fanout));
            }
        }
        createDataColumns(exportFanout);

        for (auto &col : outputColumns) {
            if (config.useExplicitMVC) {
                col->mvc = config.explicitMVC;
            }
        }

        outputHeaders();
        havePendingOutput = false;
        priorEndTime = FP::undefined();
    }

    void createFixedColumns()
    {
        if (config.station) {
            QStringList stations = Util::to_qstringlist(seenStations);
            std::sort(stations.begin(), stations.end());
            if (stations.empty())
                stations.append("NIL");
            std::unique_ptr<Column_Constant> col(new Column_Constant(stations.join(';').toUpper()));
            if (config.longStationHeaderName) {
                col->header_name = QObject::tr("Station", "name header");
            } else {
                col->header_name = QObject::tr("STN", "name header");
            }
            col->header_description = QObject::tr("Station code", "description header");
            col->mvc = QObject::tr("ZZZ", "mvc header");
            outputColumns.emplace_back(std::move(col));
        }

        if (config.timeExcel) {
            std::unique_ptr<Column_DateTime> col(new Column_DateTime("yyyy-MM-dd hh:mm:ss"));
            col->header_name = QObject::tr("DateTimeUTC", "name header");
            col->header_description =
                    QObject::tr("Date String (YYYY-MM-DD hh:mm:ss) UTC", "description header");
            col->mvc = QObject::tr("9999-99-99 99:99:99", "mvc header");
            outputColumns.emplace_back(std::move(col));
        }

        if (config.timeEpoch) {
            std::unique_ptr<Column_EpochTime> col(new Column_EpochTime);
            col->header_name = QObject::tr("EPOCH", "name header");
            col->header_description = QObject::tr("Epoch time: seconds from 1970-01-01T00:00:00Z",
                                                  "description header");
            col->mvc = QObject::tr("0", "mvc header");
            outputColumns.emplace_back(std::move(col));
        }

        if (config.timeISO) {
            std::unique_ptr<Column_DateTime> col(new Column_DateTime("yyyy-MM-ddThh:mm:ssZ"));
            col->header_name = QObject::tr("DateTimeUTC", "name header");
            col->header_description =
                    QObject::tr("Date String (YYYY-MM-DDThh:mm:ssZ) UTC", "description header");
            col->mvc = QObject::tr("9999-99-99T99:99:99Z", "mvc header");
            outputColumns.emplace_back(std::move(col));
        }

        if (config.timeFYear) {
            std::unique_ptr<Column_YearTime> col(new Column_YearTime(8));
            col->header_name = QObject::tr("FractionalYear", "name header");
            col->header_description = QObject::tr("Fractional year", "description header");
            col->mvc = QObject::tr("9999.99999999", "mvc header");
            outputColumns.emplace_back(std::move(col));
        }

        if (config.timeJulian) {
            std::unique_ptr<Column_JulianDayTime> col(new Column_JulianDayTime);
            col->header_name = QObject::tr("JulianDay", "name header");
            col->header_description =
                    QObject::tr("Fractional Julian day (12:00 January 1, 4713 BC UTC)",
                                "description header");
            col->mvc = QObject::tr("9999999.99999", "mvc header");
            outputColumns.emplace_back(std::move(col));
        }

        if (config.timeYear) {
            std::unique_ptr<Column_YearTime> col(new Column_YearTime(0));
            col->header_name = QObject::tr("Year", "name header");
            col->header_description = QObject::tr("Year", "description header");
            col->mvc = QObject::tr("9999", "mvc header");
            outputColumns.emplace_back(std::move(col));
        }

        if (config.timeDOY) {
            std::unique_ptr<Column_DOYTime> col(new Column_DOYTime);
            col->header_name = QObject::tr("DOY", "name header");
            col->header_description =
                    QObject::tr("Fractional day of year (Midnight January 1 UTC = 1.00000)",
                                "description header");
            col->mvc = QObject::tr("999.99999", "mvc header");
            outputColumns.emplace_back(std::move(col));
        }

        if (config.cutString) {
            std::unique_ptr<Column_PresentCutSizes> col(new Column_PresentCutSizes);
            col->header_name = QObject::tr("Size", "name header");
            col->header_description = QObject::tr("Semicolon delimited list of cut sizes present",
                                                  "description header");
            col->mvc = QObject::tr("Z", "mvc header");
            outputColumns.emplace_back(std::move(col));
        }
    }

    QString dataColumnHeaderName(const SequenceName &name,
                                 const QString &typeSuffix = {},
                                 const Variant::Path &path = {})
    {
        bool needCutSuffix = false;
        if (config.cutSplitMode == CSV::CutSplitMode::Always) {
            needCutSuffix = true;
        } else if (config.cutSplitMode == CSV::CutSplitMode::Automatic) {
            SequenceName trackingName = name;
            trackingName.removeFlavor(SequenceName::flavor_pm1);
            trackingName.removeFlavor(SequenceName::flavor_pm10);
            trackingName.removeFlavor(SequenceName::flavor_pm25);
            needCutSuffix = overlapTracking[trackingName].overlapDetected;
        }

        std::size_t arrayIndex = static_cast<std::size_t>(-1);
        if (path.size() == 1) {
            arrayIndex = path.front().toArrayIndex();
        }

        QString prefix = name.getVariableQString();
        QString suffix = prefix.section('_', 1);
        prefix = prefix.section('_', 0, 0);

        if (arrayIndex != static_cast<std::size_t>(-1)) {
            prefix += QString::number(static_cast<qulonglong>(arrayIndex + 1));
        }

        if (needCutSuffix) {
            if (arrayIndex != static_cast<std::size_t>(-1)) {
                prefix += 'c';
            }

            if (name.hasFlavor(SequenceName::flavor_pm1))
                prefix += '1';
            else if (name.hasFlavor(SequenceName::flavor_pm10))
                prefix += '0';
            else if (name.hasFlavor(SequenceName::flavor_pm25))
                prefix += '2';
        }

        prefix += typeSuffix;

        if (seenStations.size() > 1) {
            suffix += "_" + name.getStationQString();
        }
        if (seenArchives.size() > 1) {
            suffix += "_" + name.getArchiveQString();
        }

        if (!path.empty() && arrayIndex == static_cast<std::size_t>(-1)) {
            for (const auto &add : path) {
                suffix += "/" + QString::fromStdString(add.toString());
            }
        }

        return prefix + "_" + suffix;
    }

    void dataColumnStandard(const SequenceName &name, Column &column)
    {
        const auto &meta = metadata[name];

        column.header_description = meta.description;
        if (!meta.units.isEmpty()) {
            if (column.header_description.isEmpty()) {
                column.header_description = meta.units;
            } else {
                column.header_description = QObject::tr("%1 (%2)", "description units format").arg(
                        column.header_description, meta.units);
            }
        }

        column.header_wavelength = name;

        if (name.hasFlavor(SequenceName::flavor_pm10)) {
            column.header_cutSize = "PM10";
        } else if (name.hasFlavor(SequenceName::flavor_pm1)) {
            column.header_cutSize = "PM1";
        } else if (name.hasFlavor(SequenceName::flavor_pm25)) {
            column.header_cutSize = "PM2.5";
        }
    }

    void dataColumnFormats(const SequenceName &name, NumberFormat &format, QString &mvc)
    {
        const auto &meta = metadata[name];

        if (config.useNumericFormat) {
            format = NumberFormat(config.numericFormat);
        } else if (meta.numberFormat.isEmpty() && !meta.mvc.isEmpty()) {
            format = NumberFormat(meta.mvc);
        } else if (!meta.numberFormat.isEmpty()) {
            format = NumberFormat(meta.numberFormat);
        } else {
            format = NumberFormat("99999.999");
        }

        if (config.useExplicitMVC) {
            mvc = config.explicitMVC;
        } else if (!meta.mvc.isEmpty()) {
            mvc = meta.mvc;
        } else {
            mvc = format.mvc();
        }
    }

    void flagsColumnFormats(const SequenceName &name,
                            NumberFormat &format,
                            QString &mvc,
                            const QString &prefix = {})
    {
        const auto &meta = metadata[name];

        std::uint_fast64_t allBits = 0;
        for (const auto &add : meta.flagBits) {
            allBits |= add.second;
        }
        std::int_fast64_t highestBit;
        for (highestBit = 63; highestBit >= 0; highestBit--) {
            if (allBits &
                    (static_cast<std::uint_fast64_t>(1)
                            << static_cast<std::uint_fast64_t>(highestBit)))
                break;
        }
        if (highestBit < 0)
            highestBit = 4;
        else
            highestBit++;
        int digits = static_cast<int>(std::ceil(static_cast<double>(highestBit) / 4.0));

        mvc = meta.mvc;

        if (config.strictNumbers) {
            if (mvc.isEmpty()) {
                mvc = "-1";
            }
            format = NumberFormat(digits, 0);
            return;
        }

        if (meta.numberFormat.isEmpty() && !mvc.isEmpty()) {
            format = NumberFormat(mvc);
        } else if (!meta.numberFormat.isEmpty()) {
            format = NumberFormat(meta.numberFormat);
        } else {
            format = NumberFormat(QString("F").repeated(digits));
        }

        if (!prefix.isEmpty())
            format.setBasePrefix(prefix);

        if (mvc.isEmpty()) {
            mvc = format.mvc();
        }
    }

    std::unique_ptr<Source> createDataSource(const SequenceName &name,
                                             const SequenceName::Flavors &extraFlavors = SequenceName::Flavors())
    {
        auto &remapped = remap[name];

        std::vector<std::unique_ptr<Source>> order;

        SequenceName source = name;
        source.addFlavors(extraFlavors);
        order.emplace_back(new Source_Simple(std::move(source)));

        if (remapped.merged) {
            {
                source = name.withFlavor(SequenceName::flavor_pm10);
                source.addFlavors(extraFlavors);
                order.emplace_back(new Source_Simple(std::move(source)));
            }
            {
                source = name.withFlavor(SequenceName::flavor_pm1);
                source.addFlavors(extraFlavors);
                order.emplace_back(new Source_Simple(std::move(source)));
            }
            {
                source = name.withFlavor(SequenceName::flavor_pm25);
                source.addFlavors(extraFlavors);
                order.emplace_back(new Source_Simple(std::move(source)));
            }
        }

        if (order.size() == 1) {
            return std::move(order.front());
        }
        return std::unique_ptr<Source>(new Source_FirstExists(std::move(order)));
    }

    std::unique_ptr<Source> applyDataPath(std::unique_ptr<Source> &&source,
                                          const Variant::Path &path)
    {
        if (path.empty())
            return std::move(source);
        return std::unique_ptr<Source>(new Source_Path(std::move(source), path));
    }

    void addMVCIndicator(const SequenceName &name,
                         const Variant::Path &path,
                         std::vector<std::unique_ptr<Column>> &tail)
    {
        if (config.mvcFlagMode == CSV::MVCFlagMode::None)
            return;

        std::unique_ptr<Column_IsMissing>
                out(new Column_IsMissing(applyDataPath(createDataSource(name), path)));
        dataColumnStandard(name, *out);
        out->header_name = dataColumnHeaderName(name, {}, path) + "_MVC";
        out->mvc = "1";

        if (config.mvcFlagMode == CSV::MVCFlagMode::End)
            tail.emplace_back(std::move(out));
        else
            outputColumns.emplace_back(std::move(out));
    }

    void addFlagsColumns(const SequenceName &name,
                         const Variant::Path &path,
                         const Variant::Flags &allFlags)
    {
        auto source = applyDataPath(createDataSource(name), path);
        const auto &meta = metadata[name];

        switch (config.flagGenerateMode) {
        case CSV::FlagGenerateMode::Default: {
            std::unique_ptr<Column_BitFlags>
                    out(new Column_BitFlags(std::move(source), meta.flagBits));
            dataColumnStandard(name, *out);
            out->header_name = dataColumnHeaderName(name, {}, path);
            out->flagDescriptions = metadata[name].flagDescriptions;
            flagsColumnFormats(name, out->format, out->mvc);

            outputColumns.emplace_back(std::move(out));
            break;
        }
        case CSV::FlagGenerateMode::HexFlags: {
            std::unique_ptr<Column_BitFlags>
                    out(new Column_BitFlags(std::move(source), meta.flagBits));
            dataColumnStandard(name, *out);
            out->header_name = dataColumnHeaderName(name, {}, path);
            out->flagDescriptions = metadata[name].flagDescriptions;
            flagsColumnFormats(name, out->format, out->mvc, "0x");

            outputColumns.emplace_back(std::move(out));
            break;
        }
        case CSV::FlagGenerateMode::Breakdown: {
            std::vector<Variant::Flag> sorted(allFlags.begin(), allFlags.end());
            std::sort(sorted.begin(), sorted.end());

            for (const auto &flag :allFlags) {
                if (!source) {
                    source = applyDataPath(createDataSource(name), path);
                }
                std::unique_ptr<Column_FlagSet> out(new Column_FlagSet(std::move(source), flag));
                dataColumnStandard(name, *out);
                out->header_name =
                        dataColumnHeaderName(name, "f" + QString::fromStdString(flag), path);
                {
                    const auto &fd = metadata[name].flagDescriptions;
                    auto desc = fd.find(flag);
                    if (desc != fd.end() && !desc->second.isEmpty()) {
                        out->header_description =
                                QObject::tr("%1 - %2", "description flags format").arg(
                                        out->header_description, desc->second);
                    }
                }

                outputColumns.emplace_back(std::move(out));
            }
            break;
        }
        case CSV::FlagGenerateMode::List: {
            std::unique_ptr<Column_ListFlags> out(new Column_ListFlags(std::move(source)));
            dataColumnStandard(name, *out);
            out->header_name = dataColumnHeaderName(name, {}, path);

            out->flagDescriptions = meta.flagDescriptions;
            out->mvc = "_";

            outputColumns.emplace_back(std::move(out));
            break;
        }
        }
    }

    void createDataColumns(const std::vector<
            std::pair<SequenceName, std::vector<Variant::Path>>> &columns)
    {
        std::vector<std::unique_ptr<Column>> tailColumns;

        for (const auto &col : columns) {
            auto flags = seenFlags.find(col.first);

            for (const auto &path : col.second) {
                if (flags != seenFlags.end()) {
                    addFlagsColumns(col.first, path, flags->second);
                    addMVCIndicator(col.first, path, tailColumns);
                    continue;
                }

                auto source = applyDataPath(createDataSource(col.first), path);
                if (config.useUnweightedMean && seenStats.count(col.first)) {
                    Variant::Path mpath = path;
                    mpath.emplace_back(
                            Variant::PathElement(Variant::PathElement::Type::Hash, "Mean"));

                    std::vector<std::unique_ptr<Source>> order;
                    order.emplace_back(
                            applyDataPath(createDataSource(col.first, {SequenceName::flavor_stats}),
                                          mpath));
                    order.emplace_back(std::move(source));

                    source.reset(new Source_FirstExists(std::move(order)));
                }

                std::unique_ptr<Column_Standard> out(new Column_Standard(std::move(source)));
                dataColumnStandard(col.first, *out);
                out->header_name = dataColumnHeaderName(col.first, {}, path);

                out->strictNumeric = config.strictNumbers;
                dataColumnFormats(col.first, out->format, out->mvc);

                outputColumns.emplace_back(std::move(out));
                addMVCIndicator(col.first, path, tailColumns);
            }
        }

        if (config.enableEnd) {
            for (const auto &col : columns) {
                if (!seenEnd.count(col.first))
                    continue;
                for (const auto &path : col.second) {
                    std::unique_ptr<Column_Standard> out(new Column_Standard(
                            applyDataPath(createDataSource(col.first, {SequenceName::flavor_end}),
                                          path)));
                    dataColumnStandard(col.first, *out);
                    out->header_name = dataColumnHeaderName(col.first, "e", path);
                    out->header_description += QObject::tr(" (end)");

                    out->strictNumeric = config.strictNumbers;
                    dataColumnFormats(col.first, out->format, out->mvc);

                    outputColumns.emplace_back(std::move(out));
                }
            }
        }

        if (config.enableStdDev) {
            for (const auto &col : columns) {
                if (!seenStats.count(col.first))
                    continue;
                for (const auto &path : col.second) {
                    Variant::Path vpath = path;
                    vpath.emplace_back(Variant::PathElement(Variant::PathElement::Type::Hash,
                                                            "StandardDeviation"));
                    std::unique_ptr<Column_Standard> out(new Column_Standard(
                            applyDataPath(createDataSource(col.first, {SequenceName::flavor_stats}),
                                          vpath)));
                    dataColumnStandard(col.first, *out);
                    out->header_name = dataColumnHeaderName(col.first, "g", path);
                    out->header_description += QObject::tr(" (stddev)");

                    out->strictNumeric = true;
                    dataColumnFormats(col.first, out->format, out->mvc);

                    outputColumns.emplace_back(std::move(out));
                }
            }
        }

        if (config.enableCount) {
            for (const auto &col : columns) {
                if (!seenStats.count(col.first))
                    continue;
                for (const auto &path : col.second) {
                    Variant::Path vpath = path;
                    vpath.emplace_back(
                            Variant::PathElement(Variant::PathElement::Type::Hash, "Count"));
                    std::unique_ptr<Column_Standard> out(new Column_Standard(
                            applyDataPath(createDataSource(col.first, {SequenceName::flavor_stats}),
                                          vpath)));
                    dataColumnStandard(col.first, *out);
                    out->header_name = dataColumnHeaderName(col.first, "N", path);
                    out->header_description += QObject::tr(" (number of points)");

                    out->mvc = "99999";
                    out->format = NumberFormat("00000");
                    out->strictNumeric = true;

                    outputColumns.emplace_back(std::move(out));
                }
            }
        }

        if (config.enableCover) {
            for (const auto &col : columns) {
                if (!seenCover.count(col.first))
                    continue;
                for (const auto &path : col.second) {
                    std::unique_ptr<Column_Standard> out(new Column_Standard(
                            applyDataPath(createDataSource(col.first, {SequenceName::flavor_cover}),
                                          path)));
                    dataColumnStandard(col.first, *out);
                    out->header_name = dataColumnHeaderName(col.first, "Nr", path);
                    out->header_description += QObject::tr(" (coverage)");

                    out->mvc = "1.00";
                    out->format = NumberFormat("0.00");
                    out->strictNumeric = true;

                    outputColumns.emplace_back(std::move(out));
                }
            }
        }

        if (config.enableQuantiles) {
            static const std::array<double, 17> quantiles =
                    {0.00135, 0.00621, 0.02275, 0.05, 0.06681, 0.15866, 0.25, 0.30854, 0.50,
                     0.69146, 0.75, 0.84134, 0.93319, 0.95, 0.97725, 0.99379, 0.99865};

            for (const auto &col : columns) {
                if (!seenStats.count(col.first))
                    continue;
                for (const auto &path : col.second) {
                    for (auto q : quantiles) {
                        auto rounded = std::lround(q * 1E5);
                        if (rounded < 1)
                            rounded = 1;
                        else if (rounded > 99999)
                            rounded = 99999;

                        Variant::Path vpath = path;
                        vpath.emplace_back(Variant::PathElement(Variant::PathElement::Type::Hash,
                                                                "Quantiles"));
                        std::unique_ptr<Column_Quantile> out(new Column_Quantile(applyDataPath(
                                createDataSource(col.first, {SequenceName::flavor_stats}), vpath),
                                                                                 q));

                        dataColumnStandard(col.first, *out);
                        out->header_name = dataColumnHeaderName(col.first, "q" +
                                QString::number(rounded).rightJustified(5, '0'), path);
                        out->header_description += QObject::tr(" (%1 quantile)").arg(
                                QString::number(q * 100.0, 'f', 3));

                        dataColumnFormats(col.first, out->format, out->mvc);

                        outputColumns.emplace_back(std::move(out));
                    }
                }
            }
        }

        if (config.enableBounds) {
            for (const auto &col : columns) {
                if (!seenStats.count(col.first))
                    continue;
                for (const auto &path : col.second) {
                    {
                        Variant::Path vpath = path;
                        vpath.emplace_back(
                                Variant::PathElement(Variant::PathElement::Type::Hash, "Minimum"));
                        std::unique_ptr<Column_Standard> out(new Column_Standard(applyDataPath(
                                createDataSource(col.first, {SequenceName::flavor_stats}), vpath)));

                        dataColumnStandard(col.first, *out);
                        out->header_name = dataColumnHeaderName(col.first, "qMIN", path);
                        out->header_description += QObject::tr(" (minimum)");

                        dataColumnFormats(col.first, out->format, out->mvc);

                        outputColumns.emplace_back(std::move(out));
                    }
                    {
                        Variant::Path vpath = path;
                        vpath.emplace_back(
                                Variant::PathElement(Variant::PathElement::Type::Hash, "Maximum"));
                        std::unique_ptr<Column_Standard> out(new Column_Standard(applyDataPath(
                                createDataSource(col.first, {SequenceName::flavor_stats}), vpath)));

                        dataColumnStandard(col.first, *out);
                        out->header_name = dataColumnHeaderName(col.first, "qMAX", path);
                        out->header_description += QObject::tr(" (maximum)");

                        dataColumnFormats(col.first, out->format, out->mvc);

                        outputColumns.emplace_back(std::move(out));
                    }
                }
            }
        }

        Util::append(std::move(tailColumns), outputColumns);
    }

    void outputHeaders()
    {
        if (config.headerWavelength) {
            QStringList fields;
            for (const auto &col : outputColumns) {
                auto source = wavelength.find(col->header_wavelength);
                if (source == wavelength.end()) {
                    fields.append("NA");
                    continue;
                }
                QString field;
                for (const auto &seg : source->second.segments) {
                    QString add = QString::number(seg.wavelength, 'f', 0);
                    if (field.isEmpty() || !FP::defined(seg.getStart())) {
                        add += ";0";
                    } else {
                        add += ';';
                        add += Time::toISO8601(seg.getStart());
                    }
                    if (!seg.source.isEmpty()) {
                        add += ';';
                        add += seg.source;
                    }
                    if (!field.isEmpty())
                        field += '/';
                    field += add;
                }
                fields.append(field);
            }
            config.outputHeader(std::move(fields));
        }
        if (config.headerFlags && config.flagGenerateMode != CSV::FlagGenerateMode::Breakdown) {
            std::vector<QStringList> rows;
            for (size_t colIndex = 0, colMax = outputColumns.size();
                    colIndex < colMax;
                    ++colIndex) {
                auto &col = outputColumns[colIndex];

                QStringList colRows = col->header_flags();
                if (colRows.empty())
                    continue;

                if (static_cast<std::size_t>(colRows.size()) > rows.size()) {
                    QStringList emptyRow;
                    for (std::size_t i = 0; i < outputColumns.size(); i++) {
                        emptyRow.append(QString());
                    }
                    rows.resize(colRows.size(), emptyRow);
                }
                for (size_t rowIndex = 0, rowMax = colRows.size(); rowIndex < rowMax; ++rowIndex) {
                    rows[rowIndex][colIndex] = std::move(colRows[rowIndex]);
                }
            }
            if (!rows.empty()) {
                /* Shift everything down to the bottom */
                auto &firstRow = rows.front();
                auto &lastRow = rows.back();
                for (size_t colIndex = 0, colMax = outputColumns.size();
                        colIndex < colMax;
                        ++colIndex) {
                    if (firstRow[colIndex].isEmpty())
                        continue;
                    while (lastRow[colIndex].isEmpty()) {
                        for (size_t rowIndex = rows.size() - 1; rowIndex > 0; --rowIndex) {
                            rows[rowIndex][colIndex] = std::move(rows[rowIndex - 1][colIndex]);
                            rows[rowIndex - 1][colIndex].clear();
                        }
                    }
                }
                for (auto &row : rows) {
                    config.outputHeader(std::move(row));
                }
            }
        }
        if (config.headerDescription) {
            QStringList fields;
            for (const auto &col : outputColumns) {
                fields.append(col->header_description);
            }
            config.outputHeader(std::move(fields));
        }
        if (config.headerCutSize) {
            QStringList fields;
            for (const auto &col : outputColumns) {
                fields.append(col->header_cutSize);
            }
            config.outputHeader(std::move(fields));
        }
        if (config.headerMVCs) {
            QStringList fields;
            for (const auto &col : outputColumns) {
                fields.append(col->mvc);
            }
            config.outputHeader(std::move(fields));
        }
        if (config.headerNames) {
            QStringList fields;
            for (const auto &col : outputColumns) {
                fields.append(col->header_name);
            }
            config.outputHeader(std::move(fields));
        }

#ifdef Q_OS_UNIX
        if (config.headerNamesStdErr) {
            QStringList fields;
            for (const auto &col : outputColumns) {
                fields.append(col->header_name);
            }
            if (auto stderrFile = IO::Access::stdio_out(false, true)->stream()) {
                stderrFile->write(fields.join(",").toUtf8() + "\n");
            }
        }
#endif
    }

    void outputLine(SequenceSegment &&segment)
    {
        priorEndTime = segment.getEnd();

        QStringList fields;
        for (const auto &col : outputColumns) {
            fields.append(col->value(segment));
        }
        config.outputLine(std::move(fields));
    }

    void processOutput(SequenceSegment &&segment)
    {
        if (havePendingOutput) {
            if (config.squashThreshold) {
                double limit = config.squashThreshold
                                     ->apply(segment.getStart(), pendingOutput.getStart(), true);
                if (!FP::defined(limit) || segment.getStart() < limit) {
                    pendingOutput.setEnd(segment.getEnd());
                    pendingOutput.overlay(std::move(segment));
                    return;
                }
            }

            outputLine(std::move(pendingOutput));
            havePendingOutput = false;
        }

        if (config.fillThreshold &&
                FP::defined(priorEndTime) &&
                priorEndTime < segment.getStart()) {
            double limit = config.fillThreshold->apply(priorEndTime, priorEndTime, true);
            if (!FP::defined(limit)) {
                SequenceSegment empty;
                empty.setStart(priorEndTime);
                empty.setEnd(segment.getStart());
                outputLine(std::move(empty));
            } else {
                for (;;) {
                    if (limit >= segment.getStart()) {
                        SequenceSegment empty;
                        empty.setStart(priorEndTime);
                        empty.setEnd(segment.getStart());
                        outputLine(std::move(empty));
                        break;
                    }

                    SequenceSegment empty;
                    empty.setStart(priorEndTime);
                    empty.setEnd(limit);
                    outputLine(std::move(empty));

                    priorEndTime = limit;
                    limit = config.fillThreshold->apply(priorEndTime, priorEndTime, true);
                }
            }
        }

        if (config.squashThreshold) {
            havePendingOutput = true;
            pendingOutput = std::move(segment);
        } else {
            outputLine(std::move(segment));
        }
    }

    void completeOutput()
    {
        if (havePendingOutput) {
            outputLine(std::move(pendingOutput));
            havePendingOutput = false;
        }
    }
};

class OutputStage : public Engine::SegmentStage {
    std::unique_ptr<State> state;

public:
    explicit OutputStage(std::unique_ptr<State> &&state) : state(std::move(state))
    {
        this->state->beginOutput();
    }

    virtual ~OutputStage() = default;

    void finalizeData() override
    {
        SegmentStage::finalizeData();
        state->completeOutput();
    }

protected:
    SequenceSegment::Transfer integrateValue(SequenceValue &&value) override
    {
        if (value.getName().isMeta())
            return {};
        return SegmentStage::integrateValue(std::move(value));
    }

    void processSegment(CPD3::Data::SequenceSegment &&segment) override
    { state->processOutput(std::move(segment)); }
};

class SecondStage : public Engine::Stage {
    std::unique_ptr<State> state;

public:
    explicit SecondStage(std::unique_ptr<State> &&state) : state(std::move(state))
    { }

    virtual ~SecondStage() = default;

    bool isFinalStage() const override
    { return false; }

    std::unique_ptr<Stage> getNextStage() override
    { return std::unique_ptr<Engine::Stage>(new OutputStage(std::move(state))); }

    void processData(SequenceValue::Transfer &&values) override
    { state->processSecondStage(std::move(values)); }

    void finalizeData() override
    { state->completeSecondStage(); }
};

class FirstStage : public Engine::Stage {
    std::unique_ptr<State> state;

public:
    explicit FirstStage(CSV config) : state(new State(config))
    { }

    virtual ~FirstStage() = default;

    bool isFinalStage() const override
    { return false; }

    std::unique_ptr<Stage> getNextStage() override
    { return std::unique_ptr<Engine::Stage>(new SecondStage(std::move(state))); }

    void processData(SequenceValue::Transfer &&values) override
    { state->processFirstStage(std::move(values)); }

    void finalizeData() override
    { state->completeFirstStage(); }
};

}

CSV::CSV() : station(false),
             timeEpoch(false),
             timeExcel(true),
             timeISO(false),
             timeFYear(false),
             timeJulian(false),
             timeYear(false),
             timeDOY(false),
             cutString(false),
             enableStdDev(true),
             enableCount(true),
             enableCover(false),
             enableEnd(false),
             enableQuantiles(false), enableBounds(false),
             useUnweightedMean(false),
             splitOutputPaths(false),
             useExplicitMVC(false),
             explicitMVC(),
             cutSplitMode(CutSplitMode::Automatic),
             mvcFlagMode(MVCFlagMode::None),
             flagGenerateMode(FlagGenerateMode::Default),
             strictNumbers(false),
             useNumericFormat(false),
             numericFormat(),
#ifdef Q_OS_UNIX
             headerNamesStdErr(false),
#endif
             headerNames(true),
             headerDescription(false),
             headerWavelength(false),
             headerFlags(false),
             headerMVCs(false),
             headerCutSize(false),
             longStationHeaderName(false),
             outputHeader(),
             outputLine(),
             fillThreshold(nullptr),
             squashThreshold(nullptr)
{ }

std::unique_ptr<Engine::Stage> CSV::stage(CSV config)
{ return std::unique_ptr<Engine::Stage>(new FirstStage(std::move(config))); }

}
}