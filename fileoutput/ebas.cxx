/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <QLoggingCategory>
#include <QRegularExpression>

#include "ebas.hxx"
#include "fragment.hxx"
#include "variablevalue.hxx"
#include "fanout.hxx"
#include "core/environment.hxx"
#include "datacore/variant/composite.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/segment.hxx"
#include "algorithms/rayleigh.hxx"
#include "luascript/libs/variant.hxx"
#include "luascript/libs/sequencename.hxx"


Q_LOGGING_CATEGORY(log_fileoutput_ebas, "cpd3.fileoutput.ebas", QtWarningMsg)

using namespace CPD3::Data;

namespace CPD3 {
namespace Output {
namespace {

class FileStructureElement;

class DataAnalysisElement;

class FileOutputElement;

struct EBASStructureState;

class ValueSubstitution;

class LineController;

class ColumnController;

static bool writeLine(QFile *file, const QString &line)
{
    auto data = line.toUtf8();
    data.push_back('\n');

    int offset = 0;
    int size = data.size();
    while (offset < size) {
        qint64 n = file->write(data.constData() + offset, size - offset);
        if (n < 0) {
            qCWarning(log_fileoutput_ebas) << "Error writing line:" << file->errorString();
            return false;
        }
        offset += n;
    }
    return true;
}

static void unitsFixup(QString &units)
{
    ExternalSink::simplifyUnicode(units);
    if (units.length() > 2 &&
            units.at(units.length() - 2) == '-' &&
            units.at(units.length() - 1).isDigit()) {
        /* Convert Xx-N to 1/XxN */
        QString exponent(units.mid(units.length() - 1));
        units = "1/" + units.mid(0, units.length() - 2);
        if (exponent != "1")
            units = units + exponent;
    } else if (units == "lpm") {
        units = "l/min";
    }
}

static bool isIntervalDivisible(double interval, double divisor)
{
    if (interval < divisor * 0.99)
        return false;
    double rounded = std::round(interval / divisor) * divisor;
    double delta = std::fabs(interval - rounded);
    return (delta / divisor) < 0.01;
}

static int divideInterval(double interval, double divisor)
{
    return static_cast<int>(std::round(interval / divisor));
}

static QString toIntervalCode(double interval)
{
    if (!FP::defined(interval))
        return "0";
    if (isIntervalDivisible(interval, 365 * 86400)) {
        return QString::number(divideInterval(interval, 365 * 86400)) + "y";
    } else if (isIntervalDivisible(interval, 366 * 86400)) {
        return QString::number(divideInterval(interval, 366 * 86400)) + "y";
    } else if (isIntervalDivisible(interval, 86400)) {
        return QString::number(divideInterval(interval, 86400)) + "d";
    } else if (isIntervalDivisible(interval, 3600)) {
        return QString::number(divideInterval(interval, 3600)) + "h";
    } else if (isIntervalDivisible(interval, 60)) {
        return QString::number(divideInterval(interval, 60)) + "mn";
    } else if (interval >= 1.0) {
        return QString::number(static_cast<int>(std::round(interval))) + "s";
    }
    return "0";
}

using DependencyKeys = std::unordered_set<std::string>;


Archive::Selection::List dynamicToArchiveSelections(DynamicInput *input,
                                                    const Archive::Selection::Match &defaultStations = {},
                                                    const Archive::Selection::Match &defaultArchives = {},
                                                    const Archive::Selection::Match &defaultVariables = {})
{
    for (const auto &stn : defaultStations) {
        for (const auto &arc : defaultArchives) {
            for (const auto &var : defaultVariables) {
                input->registerExpected(stn, arc, var);
            }
            input->registerExpected(stn, arc, {});
        }
        for (const auto &var : defaultVariables) {
            input->registerExpected(stn, {}, var);
        }
    }
    {
        for (const auto &arc : defaultArchives) {
            for (const auto &var : defaultVariables) {
                input->registerExpected({}, arc, var);
            }
            input->registerExpected({}, arc, {});
        }
        for (const auto &var : defaultVariables) {
            input->registerExpected({}, {}, var);
        }
    }
    for (const auto &var : defaultVariables) {
        input->registerExpected({}, {}, var);
    }
    Archive::Selection::List result;
    for (const auto &add :input->getUsedInputs()) {
        result.emplace_back(add);
    }
    return result;
}


struct StructureDynamicState {
    Lua::Engine scriptEngine;

    double start;
    double end;
    double modified;

    SequenceName::Component station;

    QString periodCode;
    double dataStartTime;
    double dataEndTime;
    double fileReferenceDate;

    explicit StructureDynamicState(const EBAS &config) : start(config.start),
                                                         end(config.end),
                                                         modified(Time::time()),
                                                         station(config.station),
                                                         dataStartTime(FP::undefined()),
                                                         dataEndTime(FP::undefined()),
                                                         fileReferenceDate(FP::undefined())
    {
        Lua::Engine::Frame root(scriptEngine);
        {
            Lua::Engine::Assign assign(root, root.global(), "print");
            assign.push(std::function<void(Lua::Engine::Entry &)>([](Lua::Engine::Entry &entry) {
                for (std::size_t i = 0, max = entry.size(); i < max; i++) {
                    qCInfo(log_fileoutput_ebas) << entry[i].toOutputString();
                }
            }));
        }
        {
            Lua::Engine::Assign assign(root, root.global(), "MODIFIED");
            assign.push(modified);
        }
        {
            Lua::Engine::Assign assign(root, root.global(), "STATION");
            assign.push(station);
        }
    }

    ~StructureDynamicState() = default;

    std::unique_ptr<Lua::Engine::Frame> scriptFrame()
    {
        std::unique_ptr<Lua::Engine::Frame> root(new Lua::Engine::Frame(scriptEngine));
        {
            Lua::Engine::Assign assign(*root, root->global(), "START");
            assign.push(start);
        }
        {
            Lua::Engine::Assign assign(*root, root->global(), "END");
            assign.push(end);
        }
        return std::move(root);
    }
};

struct StructureAssemblyContext {
    EBAS &config;
    const ValueSegment::Transfer &base;
    FragmentTracker &fragment;
    std::shared_ptr<StructureDynamicState> dynamic;

    std::vector<std::unique_ptr<FileStructureElement>> structure;

    std::unordered_map<std::string, std::shared_ptr<ValueSubstitution>> substitutions;

    StructureAssemblyContext(EBAS &config,
                             const ValueSegment::Transfer &base,
                             FragmentTracker &fragment,
                             std::shared_ptr<StructureDynamicState> dynamic);

    ~StructureAssemblyContext();

    static ValueSegment::Transfer deriveConfig(const ValueSegment::Transfer &config,
                                               const Variant::Path &path)
    {
        ValueSegment::Transfer result;
        for (const auto &add : config) {
            result.emplace_back(add.getStart(), add.getEnd(),
                                Variant::Root(add.read().getPath(path)));
        }
        return result;
    }

    ValueSegment::Transfer deriveConfig(const Variant::Path &path) const
    { return deriveConfig(base, path); }

    ValueSegment::Transfer deriveConfig(std::string first, std::string second) const
    {
        return deriveConfig({{Variant::PathElement::Type::Hash, std::move(first)},
                             {Variant::PathElement::Type::Hash, std::move(second)}});
    }

    void forAllChildren(const std::string &key,
                        const std::function<void(const ValueSegment::Transfer &config)> &call) const
    {
        std::unordered_set<std::string> known;
        for (const auto &seg : base) {
            for (auto check : seg[key].toHash()) {
                if (check.first.empty())
                    continue;
                if (!check.second.exists())
                    continue;
                known.insert(check.first);
            }
        }
        for (const auto &child : known) {
            auto childConfig = deriveConfig(key, child);
            call(std::move(childConfig));
        }
    }

    void forAllChildren(const std::string &key,
                        const std::function<void(const ValueSegment::Transfer &config,
                                                 const std::string &key)> &call) const
    {
        std::unordered_set<std::string> known;
        for (const auto &seg : base) {
            for (auto check : seg[key].toHash()) {
                if (check.first.empty())
                    continue;
                if (!check.second.exists())
                    continue;
                known.insert(check.first);
            }
        }
        for (const auto &child : known) {
            auto childConfig = deriveConfig(key, child);
            call(std::move(childConfig), child);
        }
    }
};

struct EBASStructureState {
    struct NASAAmes {
        QString originator;
        QString organization;
        QString submitter;
        QString projects;
        QString interval;
    };
    NASAAmes headers_NASAAmes;

    class EBASHeader {
    public:
        virtual QString getEBASKey() = 0;

        virtual QString getEBASValue() = 0;

        virtual std::int_fast64_t getSortPriority() = 0;
    };

    std::vector<EBASHeader *> headers_EBAS;

    class DataColumn {
    public:
        virtual QString getScale() = 0;

        virtual QString getMVC(bool header = true) = 0;

        virtual QString getTitle() = 0;

        virtual QString getColumnHeader() = 0;

        virtual std::vector<std::int_fast64_t> getSortPriority() = 0;

        struct Output {
            QString columnValue;
            enum class State {
                /* Value present (e.g. a normal, non-MVC data value) */
                Present,

                /* Value missing (e.g. no data value at all) */
                Missing,

                /* Value present, but explicitly MVC (e.g. an undefined value) */
                PresentButExplicitlyMVC,

                /* Value present, but line discarding allowed (e.g. neph zero data, numflag) */
                PresentAndAllowDiscard,

                /* The value makes the whole line invalid (MVC) */
                LineInvalid,

                /* The value makes the whole line invalid (MVC), but line discarding allowed */
                LineInvalidAllowDiscard,
            } state;

            Output() : state(State::Missing)
            { }

            explicit Output(const QString &value, State state = State::Present) : columnValue(
                    value), state(state)
            { }

            explicit Output(QString &&value, State state = State::Present) : columnValue(
                    std::move(value)), state(state)
            { }
        };

        virtual Output getValue(SequenceSegment &segment) = 0;
    };

    std::vector<DataColumn *> dataColumns;

    bool outputHeader(QFile *file, StructureDynamicState &dynamic);
};

struct AnalysisAssemblyContext {
    EBAS &config;
    const FragmentTracker::Result &structure;
    std::shared_ptr<StructureDynamicState> dynamic;

    std::vector<std::unique_ptr<DataAnalysisElement>> analysis;
    EBASStructureState ebas;
    LineController *lineController;

    DependencyKeys availableKeys;

    AnalysisAssemblyContext(EBAS &config,
                            const FragmentTracker::Result &structure,
                            std::shared_ptr<StructureDynamicState> dynamic);

    ~AnalysisAssemblyContext();
};

struct OutputAssemblyContext {
    EBAS &config;
    EBASStructureState &ebas;
    const Time::Bounds &fileBounds;
    std::shared_ptr<StructureDynamicState> dynamic;
    std::vector<std::unique_ptr<FileOutputElement>> output;

    std::unique_ptr<QFile> targetFile;
    LineController *lineController;
    ColumnController *columnController;

    OutputAssemblyContext(EBAS &config,
                          EBASStructureState &ebas,
                          const Time::Bounds &fileBounds,
                          std::shared_ptr<StructureDynamicState> dynamic);

    ~OutputAssemblyContext();
};

class FileStructureElement {
public:
    FileStructureElement(const ValueSegment::Transfer &, StructureAssemblyContext &)
    { }

    virtual ~FileStructureElement() = default;

    virtual Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                         const Archive::Selection::Match &defaultArchives = {},
                                                         const Archive::Selection::Match &defaultVariables = {}) const
    { return {}; }

    virtual std::vector<StreamSink *> getFirstStageTargets(const SequenceName &name)
    { return {}; }

    virtual void structureExtend()
    { }

    virtual FragmentTracker::MergeTestList structureMerge()
    { return {}; }

    virtual void structureFinalize()
    { }

    virtual void prepareAnalysis(AnalysisAssemblyContext &)
    { }

    virtual bool assembleAnalysis(AnalysisAssemblyContext &)
    { return true; }
};

class DataAnalysisElement {
public:
    explicit DataAnalysisElement(AnalysisAssemblyContext &)
    { }

    virtual ~DataAnalysisElement() = default;

    virtual std::vector<StreamSink *> getSecondStageTargets(const SequenceName &name)
    { return {}; }

    virtual void prepareOutput(OutputAssemblyContext &)
    { }

    virtual bool assembleOutput(OutputAssemblyContext &)
    { return true; }
};

class FileOutputElement {
public:
    explicit FileOutputElement(OutputAssemblyContext &)
    { }

    virtual ~FileOutputElement() = default;

    virtual std::vector<StreamSink *> getOutputTargets(const SequenceName &)
    { return {}; }

    virtual bool initializeOutput(OutputAssemblyContext &)
    { return true; }

    virtual bool finishOutput()
    { return true; }
};


class ValueSubstitution {
public:
    virtual ~ValueSubstitution() = default;

    virtual Variant::Read getSubstitution(const FragmentTracker::Contents &contents) const = 0;
};

class DataSubstitutionStructure final : public FileStructureElement {
    bool enforceSplit;

    class Conversion : public FragmentTracker::Value {
    public:
        explicit Conversion(const Variant::Read &)
        { }

        virtual ~Conversion() = default;

        FragmentTracker::ValueHandle overlay(const FragmentTracker::ValueHandle &,
                                             const FragmentTracker::ValueHandle &over) override
        { return over; }

        virtual Variant::Read convert(const Variant::Read &input) const = 0;
    };

    class ProcessingLookup final : public Conversion {
        QRegularExpression stage;
    public:
        explicit ProcessingLookup(const Variant::Read &config) : Conversion(config),
                                                                 stage(config["Stage"].toQString(),
                                                                       config["CaseSensitive"].toBoolean()
                                                                       ? QRegularExpression::NoPatternOption
                                                                       : QRegularExpression::CaseInsensitiveOption)
        { }

        virtual ~ProcessingLookup() = default;

        Variant::Read convert(const Variant::Read &input) const override
        {
            for (auto check : input.metadata("Processing").toArray()) {
                if (!Util::exact_match(check.hash("By").toQString(), stage))
                    continue;
                return check;
            }
            return Variant::Read::empty();
        }
    };

    class StringTranslate final : public Conversion {
        struct Translation {
            QRegularExpression match;
            QString replacement;

            Translation() = default;

            explicit Translation(const Variant::Read &config) : match(config["Match"].toQString(),
                                                                      config["CaseSensitive"].toBoolean()
                                                                      ? QRegularExpression::NoPatternOption
                                                                      : QRegularExpression::CaseInsensitiveOption),
                                                                replacement(
                                                                        config["Replacement"].toQString())
            { }
        };

        std::vector<Translation> translation;
    public:
        explicit StringTranslate(const Variant::Read &config) : Conversion(config)
        {
            for (auto t : config["Translation"].toArray()) {
                translation.emplace_back(t);
            }
        }

        virtual ~StringTranslate() = default;

        Variant::Read convert(const Variant::Read &input) const override
        {
            auto base = input.toQString();
            for (const auto &check : translation) {
                auto r = check.match
                              .match(base, 0, QRegularExpression::NormalMatch,
                                     QRegularExpression::AnchoredMatchOption);
                if (!r.hasMatch())
                    continue;

                return Variant::Root(NumberedSubstitution(r).apply(check.replacement));
            }
            return input;
        }
    };

    class Substitution : public ValueSubstitution {
    public:
        VariableValue::Base value;
        FragmentTracker::Key conversionKey;

        explicit Substitution(StructureAssemblyContext &context) : value(context.fragment),
                                                                   conversionKey(context.fragment
                                                                                        .allocateKey())
        { }

        virtual ~Substitution() = default;

        Variant::Read getSubstitution(const FragmentTracker::Contents &contents) const override
        {
            auto result = value.get(contents);
            auto frag = contents.find(conversionKey);
            if (frag == contents.end())
                return result;
            if (!frag->second)
                return result;
            return static_cast<const Conversion *>(frag->second.get())->convert(result);
        }
    };

    std::shared_ptr<Substitution> substitution;

public:
    DataSubstitutionStructure(const ValueSegment::Transfer &config,
                              StructureAssemblyContext &context) : FileStructureElement(config,
                                                                                        context),
                                                                   enforceSplit(false),
                                                                   substitution(std::make_shared<
                                                                           Substitution>(context))
    {
        for (const auto &seg : config) {
            if (seg["EnforceSplit"].exists()) {
                enforceSplit = seg["EnforceSplit"].toBoolean();
            }

            substitution->value
                        .addInput(std::make_shared<VariableValue::String::DataInput>(seg.read()),
                                  seg.getStart(), seg.getEnd());

            const auto &type = seg["Type"].toString();
            if (Util::equal_insensitive(type, "Processing")) {
                context.fragment
                       .add(substitution->conversionKey,
                            std::make_shared<ProcessingLookup>(seg.read()), seg.getStart(),
                            seg.getEnd());
            } else if (Util::equal_insensitive(type, "Translate", "Translation")) {
                context.fragment
                       .add(substitution->conversionKey,
                            std::make_shared<StringTranslate>(seg.read()), seg.getStart(),
                            seg.getEnd());
            }
        }
        substitution->value.endConfigure();
    }

    virtual ~DataSubstitutionStructure() = default;

    Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                 const Archive::Selection::Match &defaultArchives = {},
                                                 const Archive::Selection::Match &defaultVariables = {}) const override
    {
        return substitution->value
                           .toArchiveSelections(defaultStations, defaultArchives, defaultVariables);
    }

    std::vector<StreamSink *> getFirstStageTargets(const SequenceName &name) override
    { return substitution->value.getTargets(name); }

    void structureExtend() override
    { substitution->value.extendDefined(); }

    FragmentTracker::MergeTestList structureMerge() override
    {
        if (!enforceSplit)
            return {};
        return {{[this](const FragmentTracker::Key &key,
                        FragmentTracker::Value *,
                        FragmentTracker::Value *) {
            return key == substitution->value.getKey();
        }, [this](const FragmentTracker::Key &,
                  FragmentTracker::Value *,
                  FragmentTracker::Value *,
                  const FragmentTracker::MergeTestContext &context) {
            auto v1 = substitution->getSubstitution(context.firstContents);
            if (!Variant::Composite::isDefined(v1))
                return true;
            auto v2 = substitution->getSubstitution(context.secondContents);
            if (!Variant::Composite::isDefined(v2))
                return true;
            return v1 == v2;
        }}};
    }

    std::shared_ptr<ValueSubstitution> toValueSubstitution() const
    { return std::static_pointer_cast<ValueSubstitution>(substitution); }
};

class MeanSubstitutionStructure final : public FileStructureElement {
    std::unique_ptr<DynamicInput> input;

    class Substitution : public ValueSubstitution {
        double sum;
        std::size_t count;
    public:

        explicit Substitution(StructureAssemblyContext &) : sum(0), count(0)
        { }

        virtual ~Substitution() = default;

        Variant::Read getSubstitution(const FragmentTracker::Contents &) const override
        {
            if (!count)
                return Variant::Root(FP::undefined()).read();
            return Variant::Root(sum / static_cast<double>(count)).read();
        }

        void add(double value)
        {
            if (!FP::defined(value))
                return;
            sum += value;
            count++;
        }
    };

    std::shared_ptr<Substitution> substitution;

    class Sink : public CPD3::Data::StreamSink {
        DynamicInput *input;
        Substitution *substitution;
        CPD3::Data::SequenceSegment::Stream stream;

        void processSegment(CPD3::Data::SequenceSegment &&segment)
        { substitution->add(input->get(segment)); }

    public:
        explicit Sink(MeanSubstitutionStructure &parent) : input(parent.input.get()),
                                                           substitution(parent.substitution.get())
        { }

        virtual ~Sink() = default;

        void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override
        {
            for (auto &add : stream.add(values)) {
                processSegment(std::move(add));
            }
        }

        void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override
        {
            for (auto &add : stream.add(std::move(values))) {
                processSegment(std::move(add));
            }
        }

        void incomingData(const CPD3::Data::SequenceValue &value) override
        {
            for (auto &add : stream.add(value)) {
                processSegment(std::move(add));
            }
        }

        void incomingData(CPD3::Data::SequenceValue &&value) override
        {
            for (auto &add : stream.add(std::move(value))) {
                processSegment(std::move(add));
            }
        }

        void endData() override
        {
            for (auto &add : stream.finish()) {
                processSegment(std::move(add));
            }
        }
    };

    friend class Sink;

    Sink sink;
public:
    MeanSubstitutionStructure(const ValueSegment::Transfer &config,
                              StructureAssemblyContext &context) : FileStructureElement(config,
                                                                                        context),
                                                                   input(DynamicInput::fromConfiguration(
                                                                           config)),
                                                                   substitution(std::make_shared<
                                                                           Substitution>(context)),
                                                                   sink(*this)
    { }

    virtual ~MeanSubstitutionStructure() = default;

    Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                 const Archive::Selection::Match &defaultArchives = {},
                                                 const Archive::Selection::Match &defaultVariables = {}) const override
    {
        return dynamicToArchiveSelections(input.get(), defaultStations, defaultArchives,
                                          defaultVariables);
    }

    std::vector<StreamSink *> getFirstStageTargets(const SequenceName &name) override
    {
        if (!input->registerInput(name))
            return {};
        return {&sink};
    }

    std::shared_ptr<ValueSubstitution> toValueSubstitution() const
    { return std::static_pointer_cast<ValueSubstitution>(substitution); }
};

class StringValueComponent : public VariableValue::String {
    std::unordered_map<std::string, std::shared_ptr<ValueSubstitution>> substitutions;
    std::shared_ptr<StructureDynamicState> dynamic;
    bool enforceSplit;

    static QString ebasTimeFormat(double time, const QStringList &elements)
    {
        int index = 0;
        QString type;
        if (index < elements.size())
            type = elements.at(index++).toLower();

        if (!type.isEmpty() && type != "ebas")
            return TextSubstitution::formatTime(time, elements);

        if (!FP::defined(time))
            time = 0;
        /* Make sure it agrees with six decimal digit fractional days */
        static constexpr double dayScale = 1E6 / 86400.0;
        time = std::round(time * dayScale) / dayScale;
        time = std::round(time);
        return Time::toDateTime(time).toString("yyyyMMddhhmmss");
    }

public:
    StringValueComponent(FragmentTracker &fragment,
                         const ValueSegment::Transfer &config,
                         StructureAssemblyContext &context,
                         const std::string &path = {}) : VariableValue::String(fragment),
                                                         substitutions(context.substitutions),
                                                         dynamic(context.dynamic),
                                                         enforceSplit(false)
    {
        for (const auto &seg : config) {
            auto sconfig = seg.read().getPath(path);
            auto input = std::make_shared<VariableValue::String::DataInput>(sconfig);
            addInput(std::move(input), seg.getStart(), seg.getEnd());
        }
        endConfigure();
    }

    StringValueComponent(const ValueSegment::Transfer &config,
                         StructureAssemblyContext &context,
                         const std::string &path = {}) : StringValueComponent(context.fragment,
                                                                              config, context, path)
    { }

    virtual ~StringValueComponent() = default;

    FragmentTracker::MergeTestList splitMergeTest() const
    {
        if (!enforceSplit)
            return {};
        return VariableValue::String::mergeTest();
    }

    void structureExtend()
    {
        extendDefined([this](const Variant::Read &, const FragmentTracker::Contents &contents) {
            return !toString(contents).isEmpty();
        });
    }

    using Detachable = std::shared_ptr<StringValueComponent>;

    class Detached {
        Detachable ptr;
        FragmentTracker::Contents contents;
    public:
        Detached(Detachable ptr, FragmentTracker::Contents contents) : ptr(std::move(ptr)),
                                                                       contents(std::move(contents))
        { }

        Detached(Detachable ptr, const AnalysisAssemblyContext &context) : Detached(std::move(ptr),
                                                                                    context.structure
                                                                                           .contents)
        { }

        Detached(const Detached &) = default;

        Detached(Detached &&) = default;

        Detached &operator=(const Detached &) = default;

        Detached &operator=(Detached &&) = default;

        virtual QString toString() const
        { return ptr->toString(contents); }
    };

protected:
    std::unique_ptr<
            Lua::Engine::Frame> scriptFrame(const FragmentTracker::Contents &contents) const override
    {
        auto root = dynamic->scriptFrame();
        for (const auto &add : substitutions) {
            Lua::Engine::Assign assign(*root, root->global(), Util::to_upper(add.first));
            assign.pushData<Lua::Libs::Variant>(
                    Variant::Root(add.second->getSubstitution(contents)).write());
        }
        return std::move(root);
    }

    QString applySubstitution(const QStringList &elements,
                              const FragmentTracker::Contents &contents) const override
    {
        auto lk = Util::to_lower(elements.front().toStdString());

        if (lk == "start") {
            return ebasTimeFormat(dynamic->start, elements.mid(1));
        } else if (lk == "end") {
            return ebasTimeFormat(dynamic->end, elements.mid(1));
        } else if (lk == "modified") {
            return ebasTimeFormat(dynamic->modified, elements.mid(1));
        } else if (lk == "station") {
            return TextSubstitution::formatString(QString::fromStdString(dynamic->station),
                                                  elements.mid(1));
        } else if (lk == "period") {
            return TextSubstitution::formatString(dynamic->periodCode, elements.mid(1));
        } else if (lk == "revision") {
            return TextSubstitution::formatString(Environment::revision(), elements.mid(1));
        } else if (lk == "environment") {
            return TextSubstitution::formatString(Environment::describe(), elements.mid(1));
        } else if (lk == "datastart") {
            return ebasTimeFormat(dynamic->dataStartTime, elements.mid(1));
        } else if (lk == "dataend") {
            return ebasTimeFormat(dynamic->dataEndTime, elements.mid(1));
        } else if (lk == "referencetime") {
            return ebasTimeFormat(dynamic->fileReferenceDate, elements.mid(1));
        }

        auto check = substitutions.find(lk);
        if (check == substitutions.end())
            return Variant::Composite::applyFormat(Variant::Read::empty(), elements.mid(1));
        return Variant::Composite::applyFormat(check->second->getSubstitution(contents),
                                               elements.mid(1));
    }
};


class LineController {
public:
    enum class InputType {
        Unused, Fragment, Inject
    };

    class ControllerInput {
    public:
        virtual InputType getInputType(const SequenceName &name) = 0;
    };

    virtual void attachInput(ControllerInput *input) = 0;
};

class ColumnController {
public:
    virtual void attachDataColumns(const std::vector<
            EBASStructureState::DataColumn *> &columns) = 0;
};

class LineControllerStructure final : public FileStructureElement {
    FragmentTracker &fragment;

    using Fill = FragmentTracker::StaticValue<double>;
    FragmentTracker::Key fill;

    class Analysis;

    class Output
            : public FileOutputElement,
              virtual public LineController,
              virtual public ColumnController {
        double fill;

        SequenceSegment::Stream stream;

        class DataSink : public StreamSink {
            Output &controller;
        public:
            explicit DataSink(Output &controller) : controller(controller)
            { }

            virtual ~DataSink() = default;

            void incomingData(const SequenceValue::Transfer &values) override
            { controller.processSegments(controller.stream.add(values)); }

            void incomingData(SequenceValue::Transfer &&values) override
            { controller.processSegments(controller.stream.add(std::move(values))); }

            void incomingData(const SequenceValue &value) override
            { controller.processSegments(controller.stream.add(value)); }

            void incomingData(SequenceValue &&value) override
            { controller.processSegments(controller.stream.add(std::move(value))); }

            void endData() override
            { }
        };

        DataSink dataSink;

        friend class DataSink;

        class InjectSink : public StreamSink {
            Output &controller;
        public:
            explicit InjectSink(Output &controller) : controller(controller)
            { }

            virtual ~InjectSink() = default;

            void incomingData(const SequenceValue::Transfer &values) override
            { controller.processSegments(controller.stream.inject(values)); }

            void incomingData(SequenceValue::Transfer &&values) override
            { controller.processSegments(controller.stream.inject(std::move(values))); }

            void incomingData(const SequenceValue &value) override
            { controller.processSegments(controller.stream.inject(value)); }

            void incomingData(SequenceValue &&value) override
            { controller.processSegments(controller.stream.inject(std::move(value))); }

            void endData() override
            { }
        };

        InjectSink injectSink;

        friend class InjectSink;

        NumberFormat timeFormat;
        QFile *targetFile;
        double fileReferenceOrigin;
        double lastLineEnd;
        double dataEndTime;

        std::vector<EBASStructureState::DataColumn *> dataColumns;
        std::vector<LineController::ControllerInput *> inputs;

        void writeFileLine(const QString &line)
        {
            if (!targetFile)
                return;
            if (!writeLine(targetFile, line)) {
                targetFile = nullptr;
                return;
            }
        }

        QString toLineTime(double time) const
        { return timeFormat.apply((time - fileReferenceOrigin) / 86400.0, ' '); }

        void outputFillLine(double start, double end)
        {
            QStringList fields;
            fields += toLineTime(start);
            fields += toLineTime(end);

            for (auto column : dataColumns) {
                fields += column->getMVC(false);
            }
            writeFileLine(fields.join(' '));
        }

        void fillUntil(double nextStart)
        {
            if (!FP::defined(nextStart))
                return;
            if (!FP::defined(lastLineEnd))
                return;
            if (!FP::defined(fill) || fill <= 0.0)
                return;

            double fillBegin = lastLineEnd;
            double fillEnd = fillBegin + fill;
            while (fillBegin < nextStart - 1.0) {
                if (fillEnd > nextStart) {
                    fillEnd = nextStart;
                }

                outputFillLine(fillBegin, fillEnd);
                lastLineEnd = fillEnd;

                fillBegin = fillEnd;
                fillEnd = fillBegin + fill;
            }
        }

        void generateLine(SequenceSegment &segment)
        {
            if (!FP::defined(segment.getStart()))
                return;
            if (!FP::defined(segment.getEnd()))
                return;

            /* Round to one second for checker compatibility */
            double effectiveStart = std::round(segment.getStart());
            double effectiveEnd = std::round(segment.getEnd());

            /*
             * Apply fill as rounding.  This may need to be split out if fill is ever
             * used without an interval (i.e. with TI data)
             */
            if (FP::defined(fill) && fill > 0.0) {
                effectiveStart = std::floor(effectiveStart / fill) * fill;
                effectiveEnd = std::ceil(effectiveEnd / fill) * fill;
            }

            if (FP::defined(fill) && fill > 0.0) {
                double limit = effectiveStart + fill;
                if (effectiveEnd > limit)
                    effectiveEnd = limit;
            }

            /* Do filling after all rounding */
            fillUntil(effectiveStart);

            if (FP::defined(lastLineEnd) && effectiveStart < lastLineEnd)
                effectiveStart = lastLineEnd;

            if (effectiveStart >= effectiveEnd)
                return;
            if (FP::defined(lastLineEnd) && effectiveEnd <= lastLineEnd)
                return;

            /* Ignore lines that would have the same output start and end time (due to
             * significant digits) so we don't confuse the EBAS parser. */
            {
                static constexpr double timeResolution = 1E6 / 86400;
                if (std::round(effectiveStart * timeResolution) ==
                        std::round(effectiveEnd * timeResolution)) {
                    return;
                }
            }

            QStringList fields;
            fields += toLineTime(effectiveStart);
            fields += toLineTime(effectiveEnd);

            bool discardPossible = true;
            bool allMissing = true;
            bool lineInvalid = false;
            for (auto column : dataColumns) {
                auto output = column->getValue(segment);
                fields += output.columnValue;

                switch (output.state) {
                case EBASStructureState::DataColumn::Output::State::Present:
                    discardPossible = false;
                    allMissing = false;
                    break;
                case EBASStructureState::DataColumn::Output::State::Missing:
                    break;
                case EBASStructureState::DataColumn::Output::State::PresentAndAllowDiscard:
                    allMissing = false;
                    break;
                case EBASStructureState::DataColumn::Output::State::PresentButExplicitlyMVC:
                    discardPossible = false;
                    break;
                case EBASStructureState::DataColumn::Output::State::LineInvalid:
                    lineInvalid = true;
                    discardPossible = false;
                    break;
                case EBASStructureState::DataColumn::Output::State::LineInvalidAllowDiscard:
                    lineInvalid = true;
                    break;
                }
            }

            if (discardPossible)
                return;

            lastLineEnd = effectiveEnd;

            if (allMissing || lineInvalid) {
                outputFillLine(effectiveStart, effectiveEnd);
                return;
            }

            writeFileLine(fields.join(' '));
        }

        void processSegments(SequenceSegment::Transfer &&segments)
        {
            for (auto &seg : segments) {
                generateLine(seg);
            }
        }

    public:
        Output(OutputAssemblyContext &context, Analysis &parent) : FileOutputElement(context),
                                                                   fill(parent.fill),
                                                                   dataSink(*this),
                                                                   injectSink(*this),
                                                                   timeFormat(4, 6),
                                                                   targetFile(nullptr)
        {
            context.lineController = this;
            context.columnController = this;

            if (!FP::defined(fill) || fill <= 0.0) {
                lastLineEnd = context.dynamic->dataStartTime;
                dataEndTime = context.dynamic->dataEndTime;
            } else {
                context.dynamic->start = std::floor(context.dynamic->start / fill) * fill;
                context.dynamic->end = std::ceil(context.dynamic->end / fill) * fill;
                lastLineEnd = context.dynamic->start;
                dataEndTime = context.dynamic->end;

                context.dynamic->dataStartTime = lastLineEnd;
                context.dynamic->dataEndTime = dataEndTime;
            }

            if (FP::defined(context.dynamic->dataStartTime)) {
                context.dynamic->fileReferenceDate =
                        std::floor(context.dynamic->dataStartTime / 86400.0) * 86400.0;
            }

            if (FP::defined(lastLineEnd) && FP::defined(dataEndTime)) {
                context.dynamic->periodCode = toIntervalCode(dataEndTime - lastLineEnd);
            }
        }

        virtual ~Output() = default;

        bool initializeOutput(OutputAssemblyContext &context) override
        {
            targetFile = context.targetFile.get();
            fileReferenceOrigin = context.dynamic->fileReferenceDate;
            lastLineEnd = context.dynamic->dataStartTime;
            if (!FP::defined(fileReferenceOrigin)) {
                qCInfo(log_fileoutput_ebas) << "No time reference available for file"
                                            << targetFile->fileName();
                return false;
            }
            return true;
        }

        bool finishOutput() override
        {
            processSegments(stream.finish());
            fillUntil(dataEndTime);
            return targetFile != nullptr;
        }

        std::vector<StreamSink *> getOutputTargets(const SequenceName &name) override
        {
            bool injected = false;
            for (auto check : inputs) {
                switch (check->getInputType(name)) {
                case LineController::InputType::Unused:
                    break;
                case LineController::InputType::Fragment:
                    return {&dataSink};
                case LineController::InputType::Inject:
                    injected = true;
                    break;
                }
            }
            if (injected)
                return {&injectSink};
            return {};
        }

        void attachDataColumns(const std::vector<
                EBASStructureState::DataColumn *> &columns) override
        { Util::append(columns, dataColumns); }

        void attachInput(LineController::ControllerInput *input) override
        { inputs.emplace_back(input); }
    };

    class Analysis : public DataAnalysisElement, virtual public LineController {
        double fill;

        friend class Output;

        std::vector<LineController::ControllerInput *> inputs;

        class DataSink : public StreamSink {
            Analysis &controller;
        public:
            explicit DataSink(Analysis &controller) : controller(controller)
            { }

            virtual ~DataSink() = default;

            void incomingData(const SequenceValue::Transfer &values) override
            {
                for (const auto &v : values) {
                    controller.processBounds(v);
                }
            }

            void incomingData(SequenceValue::Transfer &&values) override
            {
                for (auto &v : values) {
                    controller.processBounds(v);
                }
            }

            void incomingData(const SequenceValue &value) override
            { controller.processBounds(value); }

            void incomingData(SequenceValue &&value) override
            { controller.processBounds(value); }

            void endData() override
            { }
        };

        DataSink dataSink;

        friend class DataSink;

        double firstSeenStart;
        double lastSeenEnd;

        void processBounds(double start, double end)
        {
            /* Round to one second for checker compatibility */
            if (FP::defined(start))
                start = std::round(start);
            if (FP::defined(end))
                end = std::round(end);

            if (!FP::defined(firstSeenStart)) {
                firstSeenStart = start;
            } else {
                Q_ASSERT(FP::defined(start));
            }

            if (FP::defined(end)) {
                if (!FP::defined(lastSeenEnd) || end > lastSeenEnd)
                    lastSeenEnd = end;
            }
        }

        template<typename T>
        void processBounds(const T &value)
        { return processBounds(value.getStart(), value.getEnd()); }

    public:
        Analysis(AnalysisAssemblyContext &context, LineControllerStructure &parent)
                : DataAnalysisElement(context),
                  fill(Fill::value(context.structure, parent.fill, 0)),
                  dataSink(*this),
                  firstSeenStart(FP::undefined()),
                  lastSeenEnd(FP::undefined())
        {
            context.lineController = this;
        }

        virtual ~Analysis() = default;

        std::vector<StreamSink *> getSecondStageTargets(const SequenceName &name) override
        {
            for (auto check : inputs) {
                switch (check->getInputType(name)) {
                case LineController::InputType::Unused:
                    break;
                case LineController::InputType::Fragment:
                    return {&dataSink};
                case LineController::InputType::Inject:
                    break;
                }
            }
            return {};
        }

        void prepareOutput(OutputAssemblyContext &context) override
        {
            context.dynamic->dataStartTime = firstSeenStart;
            context.dynamic->dataEndTime = lastSeenEnd;
        }

        bool assembleOutput(OutputAssemblyContext &context) override
        {
            context.output.emplace_back(new Output(context, *this));
            return true;
        }

        void attachInput(LineController::ControllerInput *input) override
        { inputs.emplace_back(input); }
    };

    friend class Analysis;

public:
    LineControllerStructure(const ValueSegment::Transfer &config, StructureAssemblyContext &context)
            : FileStructureElement(config, context),
              fragment(context.fragment),
              fill(fragment.allocateKey())
    {
        for (const auto &seg : config) {
            {
                auto fillInterval = seg["Fill"];
                if (fillInterval.exists()) {
                    fragment.add(fill, std::make_shared<Fill>(fillInterval.toReal()),
                                 seg.getStart(), seg.getEnd());
                }
            }
        }
    }

    virtual ~LineControllerStructure() = default;

    void structureExtend() override
    {
        Fill::extendOverEmpty(fragment, fill);
    }

    bool assembleAnalysis(AnalysisAssemblyContext &context) override
    {
        context.analysis.emplace_back(new Analysis(context, *this));
        return true;
    }
};


class EBASFileStructure final : public FileStructureElement {
    StringValueComponent::Detachable filename;
    StringValueComponent::Detachable originator;
    StringValueComponent::Detachable organization;
    StringValueComponent::Detachable submitter;
    StringValueComponent::Detachable projects;
    StringValueComponent::Detachable interval;

    class Output : public FileOutputElement {
    public:
        explicit Output(OutputAssemblyContext &context) : FileOutputElement(context)
        { }

        virtual ~Output() = default;

        virtual bool initializeOutput(OutputAssemblyContext &context)
        {
            if (!context.ebas.outputHeader(context.targetFile.get(), *context.dynamic))
                return false;
            context.columnController->attachDataColumns(context.ebas.dataColumns);
            return true;
        }
    };

    class Analysis : public DataAnalysisElement {
        StringValueComponent::Detached filename;
        StringValueComponent::Detached originator;
        StringValueComponent::Detached organization;
        StringValueComponent::Detached submitter;
        StringValueComponent::Detached projects;
        StringValueComponent::Detached interval;
    public:
        Analysis(AnalysisAssemblyContext &context, EBASFileStructure &parent) : DataAnalysisElement(
                context),
                                                                                filename(
                                                                                        parent.filename,
                                                                                        context.structure
                                                                                               .contents),
                                                                                originator(
                                                                                        parent.originator,
                                                                                        context.structure
                                                                                               .contents),
                                                                                organization(
                                                                                        parent.organization,
                                                                                        context.structure
                                                                                               .contents),
                                                                                submitter(
                                                                                        parent.submitter,
                                                                                        context.structure
                                                                                               .contents),
                                                                                projects(
                                                                                        parent.projects,
                                                                                        context.structure
                                                                                               .contents),
                                                                                interval(
                                                                                        parent.interval,
                                                                                        context.structure
                                                                                               .contents)
        { }

        virtual ~Analysis() = default;

        bool assembleOutput(OutputAssemblyContext &context) override
        {
            context.ebas.headers_NASAAmes.originator = originator.toString();
            if (context.ebas.headers_NASAAmes.originator.isEmpty()) {
                qCInfo(log_fileoutput_ebas) << "Originator NASA-Ames header empty";
                return false;
            }
            context.ebas.headers_NASAAmes.organization = organization.toString();
            if (context.ebas.headers_NASAAmes.organization.isEmpty()) {
                qCInfo(log_fileoutput_ebas) << "Organization NASA-Ames header empty";
                return false;
            }
            context.ebas.headers_NASAAmes.submitter = submitter.toString();
            if (context.ebas.headers_NASAAmes.submitter.isEmpty()) {
                qCInfo(log_fileoutput_ebas) << "Submitter NASA-Ames header empty";
                return false;
            }
            context.ebas.headers_NASAAmes.projects = projects.toString();
            if (context.ebas.headers_NASAAmes.projects.isEmpty()) {
                qCInfo(log_fileoutput_ebas) << "Projects NASA-Ames header empty";
                return false;
            }
            context.ebas.headers_NASAAmes.interval = interval.toString();
            if (context.ebas.headers_NASAAmes.interval.isEmpty()) {
                qCInfo(log_fileoutput_ebas) << "Interval NASA-Ames header empty";
                return false;
            }

            auto requestedName = filename.toString().toStdString();
            if (requestedName.empty()) {
                qCInfo(log_fileoutput_ebas) << "Output file name is empty";
                return false;
            }
            std::unique_ptr<QFile> targetFile(context.config.createOutput(requestedName));
            if (!targetFile)
                return false;

            context.targetFile = std::move(targetFile);

            context.output.emplace_back(new Output(context));
            return true;
        }
    };

    friend class Analysis;

public:
    EBASFileStructure(const ValueSegment::Transfer &config, StructureAssemblyContext &context)
            : FileStructureElement(config, context),
              filename(std::make_shared<StringValueComponent>(config, context, "Filename")),
              originator(std::make_shared<StringValueComponent>(config, context, "Originator")),
              organization(std::make_shared<StringValueComponent>(config, context, "Organization")),
              submitter(std::make_shared<StringValueComponent>(config, context, "Submitter")),
              projects(std::make_shared<StringValueComponent>(config, context, "Projects")),
              interval(std::make_shared<StringValueComponent>(config, context, "Interval"))
    { }

    virtual ~EBASFileStructure() = default;

    Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                 const Archive::Selection::Match &defaultArchives = {},
                                                 const Archive::Selection::Match &defaultVariables = {}) const override
    {
        auto result =
                filename->toArchiveSelections(defaultStations, defaultArchives, defaultVariables);
        Util::append(
                originator->toArchiveSelections(defaultStations, defaultArchives, defaultVariables),
                result);
        Util::append(organization->toArchiveSelections(defaultStations, defaultArchives,
                                                       defaultVariables), result);
        Util::append(
                submitter->toArchiveSelections(defaultStations, defaultArchives, defaultVariables),
                result);
        Util::append(
                projects->toArchiveSelections(defaultStations, defaultArchives, defaultVariables),
                result);
        Util::append(
                interval->toArchiveSelections(defaultStations, defaultArchives, defaultVariables),
                result);
        return result;
    }

    std::vector<StreamSink *> getFirstStageTargets(const SequenceName &key) override
    {
        auto result = filename->getTargets(key);
        Util::append(originator->getTargets(key), result);
        Util::append(organization->getTargets(key), result);
        Util::append(submitter->getTargets(key), result);
        Util::append(projects->getTargets(key), result);
        Util::append(interval->getTargets(key), result);
        return result;
    }

    void structureExtend() override
    {
        filename->structureExtend();
        originator->structureExtend();
        organization->structureExtend();
        submitter->structureExtend();
        projects->structureExtend();
        interval->structureExtend();
    }

    FragmentTracker::MergeTestList structureMerge() override
    {
        auto result = filename->splitMergeTest();
        Util::append(originator->splitMergeTest(), result);
        Util::append(organization->splitMergeTest(), result);
        Util::append(submitter->splitMergeTest(), result);
        Util::append(projects->splitMergeTest(), result);
        Util::append(interval->splitMergeTest(), result);
        return result;
    }

    bool assembleAnalysis(AnalysisAssemblyContext &context) override
    {
        context.analysis.emplace_back(new Analysis(context, *this));
        return true;
    }
};

class EBASHeaderStructure final : public FileStructureElement {
    StringValueComponent::Detachable key;
    StringValueComponent::Detachable value;

    FragmentTracker &fragment;

    using SortPriority = FragmentTracker::StaticValue<std::int_fast64_t>;
    FragmentTracker::Key sortPriority;

    class Output : public FileOutputElement, virtual public EBASStructureState::EBASHeader {
        QString key;
        QString value;
        std::int_fast64_t sortPriority;
    public:
        Output(OutputAssemblyContext &context,
               QString key,
               QString value,
               std::int_fast64_t sortPriority) : FileOutputElement(context),
                                                 key(std::move(key)),
                                                 value(std::move(value)),
                                                 sortPriority(sortPriority)
        {
            context.ebas.headers_EBAS.emplace_back(this);
        }

        virtual ~Output() = default;

        QString getEBASKey() override
        { return key; }

        QString getEBASValue() override
        { return value; }

        std::int_fast64_t getSortPriority() override
        { return sortPriority; }
    };

    class Analysis : public DataAnalysisElement {
        StringValueComponent::Detached key;
        StringValueComponent::Detached value;
        std::int_fast64_t sortPriority;
    public:
        Analysis(AnalysisAssemblyContext &context, EBASHeaderStructure &parent)
                : DataAnalysisElement(context),
                  key(parent.key, context.structure.contents),
                  value(parent.value, context.structure.contents),
                  sortPriority(SortPriority::value(context.structure, parent.sortPriority, 0))
        { }

        virtual ~Analysis() = default;

        bool assembleOutput(OutputAssemblyContext &context) override
        {
            QString ebasKey = key.toString();
            if (ebasKey.isEmpty()) {
                qCInfo(log_fileoutput_ebas) << "EBAS header key empty";
                return false;
            }

            QString ebasValue = value.toString();
            if (ebasValue.isEmpty())
                return true;

            context.output
                   .emplace_back(new Output(context, std::move(ebasKey), std::move(ebasValue),
                                            sortPriority));
            return true;
        }
    };

    friend class Analysis;

public:
    EBASHeaderStructure(const ValueSegment::Transfer &config, StructureAssemblyContext &context)
            : FileStructureElement(config, context),
              key(std::make_shared<StringValueComponent>(config, context, "Name")),
              value(std::make_shared<StringValueComponent>(config, context, "Contents")),
              fragment(context.fragment),
              sortPriority(context.fragment.allocateKey())
    {
        for (const auto &seg : config) {
            {
                auto priority = seg["SortPriority"].toInteger();
                if (INTEGER::defined(priority)) {
                    fragment.add(sortPriority, std::make_shared<SortPriority>(priority),
                                 seg.getStart(), seg.getEnd());
                }
            }
        }
    }

    virtual ~EBASHeaderStructure() = default;

    Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                 const Archive::Selection::Match &defaultArchives = {},
                                                 const Archive::Selection::Match &defaultVariables = {}) const override
    {
        auto result = key->toArchiveSelections(defaultStations, defaultArchives, defaultVariables);
        Util::append(value->toArchiveSelections(defaultStations, defaultArchives, defaultVariables),
                     result);
        return result;
    }

    std::vector<StreamSink *> getFirstStageTargets(const SequenceName &name) override
    {
        auto result = key->getTargets(name);
        Util::append(value->getTargets(name), result);
        return result;
    }

    void structureExtend() override
    {
        key->structureExtend();
        value->structureExtend();
        SortPriority::extendOverEmpty(fragment, sortPriority);
    }

    FragmentTracker::MergeTestList structureMerge() override
    {
        auto result = key->splitMergeTest();
        Util::append(value->splitMergeTest(), result);
        return result;
    }

    bool assembleAnalysis(AnalysisAssemblyContext &context) override
    {
        context.analysis.emplace_back(new Analysis(context, *this));
        return true;
    }
};

class DependencyStructure final : public FileStructureElement {
    FragmentTracker &fragment;

    using RequireKeys = FragmentTracker::StaticValue<DependencyKeys>;
    FragmentTracker::Key requireKeys;

    using ExcludeKeys = FragmentTracker::StaticValue<DependencyKeys>;
    FragmentTracker::Key excludeKeys;

public:
    DependencyStructure(const ValueSegment::Transfer &config, StructureAssemblyContext &context)
            : FileStructureElement(config, context),
              fragment(context.fragment),
              requireKeys(fragment.allocateKey()),
              excludeKeys(fragment.allocateKey())
    {
        for (const auto &seg : config) {
            fragment.add(requireKeys,
                         std::make_shared<RequireKeys>(seg["Dependencies/Require"].toFlags()),
                         seg.getStart(), seg.getEnd());
            fragment.add(excludeKeys,
                         std::make_shared<RequireKeys>(seg["Dependencies/Exclude"].toFlags()),
                         seg.getStart(), seg.getEnd());
        }
    }

    virtual ~DependencyStructure() = default;

    void structureExtend() override
    {
        RequireKeys::extendOverEmpty(fragment, requireKeys);
        ExcludeKeys::extendOverEmpty(fragment, excludeKeys);
    }

    bool assembleAnalysis(AnalysisAssemblyContext &context) override
    {
        for (const auto &key : RequireKeys::value(context.structure, requireKeys)) {
            if (!context.availableKeys.count(key)) {
                qCDebug(log_fileoutput_ebas)
                    << "File generation aborted due to missing required key" << key;
                return false;
            }
        }
        for (const auto &key : ExcludeKeys::value(context.structure, excludeKeys)) {
            if (context.availableKeys.count(key)) {
                qCDebug(log_fileoutput_ebas)
                    << "File generation aborted due to present excluded key" << key;
                return false;
            }
        }
        return true;
    }
};


class VariableSetStructure final : public FileStructureElement {
    class Fanout;

    class Column;

    FragmentTracker &fragment;

    using SortPriority = FragmentTracker::StaticValue<std::int_fast64_t>;
    FragmentTracker::Key sortPriority;

    using ProvideKeys = FragmentTracker::StaticValue<DependencyKeys>;
    FragmentTracker::Key provideKeys;

    struct SubstitutionContext {
        using Metadata = FragmentTracker::StaticValue<Variant::Read>;
        using Units = FragmentTracker::StaticValue<QString>;
        using MVC = FragmentTracker::StaticValue<QString>;
        using Format = FragmentTracker::StaticValue<QString>;
        using Wavelength = FragmentTracker::StaticValue<double>;

        FragmentTracker::Key metadata;
        FragmentTracker::Key units;
        FragmentTracker::Key mvc;
        FragmentTracker::Key format;
        FragmentTracker::Key wavelength;

        explicit SubstitutionContext(FragmentTracker &fragment) : metadata(fragment.allocateKey()),
                                                                  units(fragment.allocateKey()),
                                                                  mvc(fragment.allocateKey()),
                                                                  format(fragment.allocateKey()),
                                                                  wavelength(fragment.allocateKey())
        { }
    };

    class Input final : public StagedFanout::InputStage {
        Fanout &parent;

        FragmentTracker &fragment;

        SubstitutionContext substitutions;

        friend class Column;

    public:
        explicit Input(Fanout &parent) : StagedFanout::InputStage(parent),
                                         parent(parent),
                                         fragment(parent.parent.fragment),
                                         substitutions(fragment)
        { }

        virtual ~Input() = default;

        void processMetadataSegment(SequenceSegment &segment) override
        {
            auto base = getMetadata(segment);
            if (!base.exists() || !base.isMetadata())
                return;

            fragment.add(substitutions.metadata,
                         std::make_shared<SubstitutionContext::Metadata>(base), segment.getStart(),
                         segment.getEnd());

            {
                auto add = base.metadata("Units").toQString();
                if (add.isEmpty())
                    add = base.metadata("Children").metadata("Units").toQString();
                if (!add.isEmpty()) {
                    fragment.add(substitutions.units,
                                 std::make_shared<SubstitutionContext::Units>(add),
                                 segment.getStart(), segment.getEnd());
                }
            }

            {
                auto add = base.metadata("MVC").toQString();
                if (add.isEmpty())
                    add = base.metadata("Children").metadata("MVC").toQString();
                if (!add.isEmpty()) {
                    fragment.add(substitutions.mvc, std::make_shared<SubstitutionContext::MVC>(add),
                                 segment.getStart(), segment.getEnd());
                }
            }

            {
                auto add = base.metadata("Format").toQString();
                if (add.isEmpty())
                    add = base.metadata("Children").metadata("Format").toQString();
                if (!add.isEmpty()) {
                    fragment.add(substitutions.format,
                                 std::make_shared<SubstitutionContext::Format>(add),
                                 segment.getStart(), segment.getEnd());
                }
            }

            {
                auto add = base.metadata("Wavelength").toReal();
                if (!FP::defined(add) || add <= 0.0)
                    add = base.metadata("Children").metadata("Wavelength").toReal();
                if (FP::defined(add) && add > 0.0) {
                    fragment.add(substitutions.wavelength,
                                 std::make_shared<SubstitutionContext::Wavelength>(add),
                                 segment.getStart(), segment.getEnd());
                }
            }
        }

        void structureExtend()
        {
            SubstitutionContext::Metadata::extendOverEmpty(fragment, substitutions.metadata);
            SubstitutionContext::Units::extendOverEmpty(fragment, substitutions.units);
            SubstitutionContext::MVC::extendOverEmpty(fragment, substitutions.mvc);
            SubstitutionContext::Format::extendOverEmpty(fragment, substitutions.format);
            SubstitutionContext::Wavelength::extendOverEmpty(fragment, substitutions.wavelength);
        }

        FragmentTracker::MergeTestList structureMerge()
        { return {}; }

        const SubstitutionContext &getSubstitutionContext() const
        { return substitutions; }
    };

    friend class Input;

    class InputString final : public StringValueComponent {
    public:
        const SubstitutionContext *substitutions;

        InputString(const ValueSegment::Transfer &config,
                    StructureAssemblyContext &context,
                    const std::string &path = {}) : StringValueComponent(config, context, path),
                                                    substitutions(nullptr)
        { }

        virtual ~InputString() = default;

        class DetachedInput : public Detached {
            InputString *input;
            SubstitutionContext substitutions;
        public:
            DetachedInput(const Detachable &ptr,
                          const AnalysisAssemblyContext &context,
                          const Input &input) : Detached(ptr, context),
                                                input(static_cast<InputString *>(ptr.get())),
                                                substitutions(input.getSubstitutionContext())
            {
                Q_ASSERT(dynamic_cast<InputString *>(ptr.get()) != nullptr);
            }

            QString toString() const override
            {
                input->substitutions = &substitutions;
                auto result = Detached::toString();
                input->substitutions = nullptr;
                return result;
            }
        };

    protected:
        QString applySubstitution(const QStringList &elements,
                                  const FragmentTracker::Contents &contents) const override
        {
            if (substitutions) {
                auto lk = Util::to_lower(elements.front().toStdString());

                if (lk == "meta" || lk == "metadata") {
                    return Variant::Composite::applyFormat(
                            SubstitutionContext::Metadata::value(contents, substitutions->metadata,
                                                                 Variant::Read::empty()),
                            elements.mid(1));
                } else if (lk == "unit" || lk == "units") {
                    return TextSubstitution::formatString(
                            SubstitutionContext::Units::value(contents, substitutions->units),
                            elements.mid(1));
                } else if (lk == "mvc") {
                    return TextSubstitution::formatString(
                            SubstitutionContext::MVC::value(contents, substitutions->mvc),
                            elements.mid(1));
                } else if (lk == "format") {
                    return TextSubstitution::formatString(
                            SubstitutionContext::Format::value(contents, substitutions->format),
                            elements.mid(1));
                } else if (lk == "wavelength") {
                    return TextSubstitution::formatDouble(
                            SubstitutionContext::Wavelength::value(contents,
                                                                   substitutions->wavelength,
                                                                   FP::undefined()),
                            elements.mid(1));
                }
            }

            return StringValueComponent::applySubstitution(elements, contents);
        }
    };

    class Fanout final : public StagedFanout {
        VariableSetStructure &parent;
        std::shared_ptr<StructureDynamicState> dynamic;

        friend class Input;

        friend class Column;

    public:
        /* Use the metadata target and just extract possibilities from it */
        Fanout(const CPD3::Data::ValueSegment::Transfer &config,
               StructureAssemblyContext &context,
               VariableSetStructure &parent) : StagedFanout(config, false, true),
                                               parent(parent),
                                               dynamic(context.dynamic)
        { }

        virtual ~Fanout() = default;

    protected:
        virtual std::unique_ptr<InputStage> createInput()
        { return std::unique_ptr<InputStage>(new Input(*this)); }

        std::unique_ptr<Lua::Engine::Frame> scriptFrame(const SequenceName &target) const override
        {
            auto root = dynamic->scriptFrame();
            {
                Lua::Engine::Assign assign(*root, root->global(), "TARGET");
                assign.pushData<Lua::Libs::SequenceName>(target);
            }
            return std::move(root);
        }
    };

    Fanout data;

    class Conversion {
    public:
        explicit Conversion(const Variant::Read &)
        { }

        virtual ~Conversion() = default;

        virtual EBASStructureState::DataColumn::Output convert(SequenceSegment &segment,
                                                               Column &column) const = 0;

        virtual LineController::InputType getInputType(const SequenceName &name,
                                                       const Column &column) const
        {
            if (column.isDirectlyUsed(name))
                return LineController::InputType::Fragment;
            if (!column.isPossiblyUsed(name))
                return LineController::InputType::Inject;
            return LineController::InputType::Unused;
        }

        virtual void registerInput(const SequenceName &)
        { }

        virtual Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                             const Archive::Selection::Match &defaultArchives = {},
                                                             const Archive::Selection::Match &defaultVariables = {}) const
        { return {}; }
    };

    class ConstantConversion : public Conversion {
        QString constant;
    public:
        explicit ConstantConversion(const Variant::Read &config) : Conversion(config),
                                                                   constant(
                                                                           config["Value"].toQString())
        { }

        virtual ~ConstantConversion() = default;

        EBASStructureState::DataColumn::Output convert(SequenceSegment &segment,
                                                       Column &column) const override
        {
            using Output = EBASStructureState::DataColumn::Output;
            if (constant.isEmpty())
                return Output(column.getMVC(false), Output::State::Missing);
            return Output(constant, Output::State::PresentAndAllowDiscard);
        }
    };

    class RealConversion : public Conversion {
        Calibration cal;
        Variant::Path path;
        double lineInvalidLower;
        double lineInvalidUpper;
    public:
        explicit RealConversion(const Variant::Read &config) : Conversion(config),
                                                               cal(Variant::Composite::toCalibration(
                                                                       config["Calibration"])),
                                                               path(Variant::PathElement::parse(
                                                                       config["Path"].toString())),
                                                               lineInvalidLower(
                                                                       config["LineInvalid/Below"].toReal()),
                                                               lineInvalidUpper(
                                                                       config["LineInvalid/Above"].toReal())
        { }

        virtual ~RealConversion() = default;

        EBASStructureState::DataColumn::Output convert(SequenceSegment &segment,
                                                       Column &column) const override
        {
            using Output = EBASStructureState::DataColumn::Output;

            Output::State missingState = Output::State::Missing;
            for (const auto &name : column.get(segment)) {
                if (segment.exists(name)) {
                    missingState = Output::State::PresentButExplicitlyMVC;
                }

                double value = cal.apply(Variant::Composite::toNumber(segment[name].getPath(path)));
                if (!FP::defined(value))
                    continue;
                if (FP::defined(lineInvalidLower) && value < lineInvalidLower)
                    return Output(column.getMVC(false), Output::State::LineInvalid);
                if (FP::defined(lineInvalidUpper) && value > lineInvalidUpper)
                    return Output(column.getMVC(false), Output::State::LineInvalid);
                return Output(column.getFormat().applyClipped(value, ' '), Output::State::Present);
            }

            return Output(column.getMVC(false), missingState);
        }
    };

    class OptionalRealConversion : public Conversion {
        Calibration cal;
        Variant::Path path;
        double lineInvalidLower;
        double lineInvalidUpper;
    public:
        explicit OptionalRealConversion(const Variant::Read &config) : Conversion(config),
                                                                       cal(Variant::Composite::toCalibration(
                                                                               config["Calibration"])),
                                                                       path(Variant::PathElement::parse(
                                                                               config["Path"].toString())),
                                                                       lineInvalidLower(
                                                                               config["LineInvalid/Below"]
                                                                                       .toReal()),
                                                                       lineInvalidUpper(
                                                                               config["LineInvalid/Above"]
                                                                                       .toReal())
        { }

        virtual ~OptionalRealConversion() = default;

        EBASStructureState::DataColumn::Output convert(SequenceSegment &segment,
                                                       Column &column) const override
        {
            return convert(Variant::Composite::toNumber(column.getData(segment).getPath(path)),
                           column);
        }

        LineController::InputType getInputType(const SequenceName &name,
                                               const Column &column) const override
        {
            if (column.isPossiblyUsed(name))
                return LineController::InputType::Inject;
            return LineController::InputType::Unused;
        }

    protected:
        EBASStructureState::DataColumn::Output convert(double value, Column &column) const
        {
            using Output = EBASStructureState::DataColumn::Output;

            value = cal.apply(value);
            if (!FP::defined(value)) {
                return Output(column.getMVC(false), Output::State::Missing);
            }
            if (FP::defined(lineInvalidLower) && value < lineInvalidLower)
                return Output(column.getMVC(false), Output::State::LineInvalidAllowDiscard);
            if (FP::defined(lineInvalidUpper) && value > lineInvalidUpper)
                return Output(column.getMVC(false), Output::State::LineInvalidAllowDiscard);
            return Output(column.getFormat().applyClipped(value, ' '),
                          Output::State::PresentAndAllowDiscard);
        }

        inline const Variant::Path &getPath() const
        { return path; }
    };

    class StatisticsConversion : public OptionalRealConversion {
    public:
        explicit StatisticsConversion(const Variant::Read &config) : OptionalRealConversion(config)
        { }

        virtual ~StatisticsConversion() = default;

        EBASStructureState::DataColumn::Output convert(SequenceSegment &segment,
                                                       Column &column) const override
        {
            for (const auto &name : column.get(segment)) {
                auto check = segment[name.withFlavor(SequenceName::flavor_stats)];
                if (!check.exists())
                    continue;
                double value = check.getPath(getPath()).toReal();
                if (!FP::defined(value))
                    continue;

                return OptionalRealConversion::convert(value, column);
            }

            return OptionalRealConversion::convert(FP::undefined(), column);
        }

        LineController::InputType getInputType(const SequenceName &name,
                                               const Column &column) const override
        {
            if (!name.hasFlavor(SequenceName::flavor_stats))
                return LineController::InputType::Unused;
            if (column.isPossiblyUsed(name.withoutFlavor(SequenceName::flavor_stats)))
                return LineController::InputType::Inject;
            return LineController::InputType::Unused;
        }
    };

    class QuantileConversion : public OptionalRealConversion {
        double quantile;
    public:
        explicit QuantileConversion(const Variant::Read &config) : OptionalRealConversion(config),
                                                                   quantile(
                                                                           config["Quantile"].toReal())
        { }

        virtual ~QuantileConversion() = default;

        EBASStructureState::DataColumn::Output convert(SequenceSegment &segment,
                                                       Column &column) const override
        {
            if (!FP::defined(quantile))
                return OptionalRealConversion::convert(FP::undefined(), column);

            for (const auto &name : column.get(segment)) {
                auto check = segment[name.withFlavor(SequenceName::flavor_stats)];
                if (!check.exists())
                    continue;

                if (quantile <= 0.0) {
                    return OptionalRealConversion::convert(check.hash("Minimum").toReal(), column);
                } else if (quantile >= 1.0) {
                    return OptionalRealConversion::convert(check.hash("Maximum").toReal(), column);
                }

                auto quantiles = check.hash("Quantiles").toKeyframe();
                if (quantiles.empty())
                    return OptionalRealConversion::convert(FP::undefined(), column);

                auto upper = quantiles.upper_bound(quantile);
                if (upper == quantiles.end())
                    return OptionalRealConversion::convert((--upper).value().toReal(), column);
                if (upper == quantiles.begin() || std::fabs(upper.key() - quantile) < 1E-6)
                    return OptionalRealConversion::convert(upper.value().toReal(), column);
                auto lower = upper;
                --lower;

                double vLower = lower.value().toDouble();
                double vUpper = upper.value().toDouble();
                double qLower = lower.key();
                double qUpper = upper.key();
                if (!FP::defined(vLower))
                    return OptionalRealConversion::convert(vUpper, column);
                if (!FP::defined(vUpper) || qLower == qUpper)
                    return OptionalRealConversion::convert(vLower, column);

                return OptionalRealConversion::convert(
                        ((vUpper - vLower) / (qUpper - qLower)) * (quantile - qLower) + vLower,
                        column);
            }

            return OptionalRealConversion::convert(FP::undefined(), column);
        }

        LineController::InputType getInputType(const SequenceName &name,
                                               const Column &column) const override
        {
            if (!name.hasFlavor(SequenceName::flavor_stats))
                return LineController::InputType::Unused;
            if (column.isPossiblyUsed(name.withoutFlavor(SequenceName::flavor_stats)))
                return LineController::InputType::Inject;
            return LineController::InputType::Unused;
        }
    };

    class NephZeroConditions : public OptionalRealConversion {
        SequenceMatch::OrderedLookup temperature;
        SequenceMatch::OrderedLookup pressure;

        double getTemperature(SequenceSegment &segment) const
        { return temperature.lookup(segment).toReal(); }

        double getPressure(SequenceSegment &segment) const
        { return pressure.lookup(segment).toReal(); }

    public:
        explicit NephZeroConditions(const Variant::Read &config) : OptionalRealConversion(config),
                                                                   temperature(
                                                                           config["Data/Temperature"]),
                                                                   pressure(config["Data/Pressure"])
        { }

        virtual ~NephZeroConditions() = default;

        LineController::InputType getInputType(const SequenceName &name,
                                               const Column &column) const override
        {
            if (temperature.matches(name) || pressure.matches(name))
                return LineController::InputType::Inject;
            return OptionalRealConversion::getInputType(name, column);
        }

        void registerInput(const SequenceName &name) override
        {
            temperature.registerInput(name);
            pressure.registerInput(name);
        }

        Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                     const Archive::Selection::Match &defaultArchives = {},
                                                     const Archive::Selection::Match &defaultVariables = {}) const override
        {
            auto result = temperature.toArchiveSelections(defaultStations, defaultArchives,
                                                          defaultVariables);
            Util::append(pressure.toArchiveSelections(defaultStations, defaultArchives,
                                                      defaultVariables), result);
            return result;
        }

    protected:
        double getRayleigh(SequenceSegment &segment, Column &column, double angle) const
        {
            double T = getTemperature(segment);
            if (!FP::defined(T) || T < -100.0 || T > 100.0)
                return FP::undefined();

            double P = getPressure(segment);
            if (!FP::defined(P) || P < 10.0 || P > 1200.0)
                return FP::undefined();

            double wavelength = column.getMetadata(segment).metadata("Wavelength").toReal();
            if (!FP::defined(wavelength) || wavelength <= 0.0)
                return FP::undefined();

            return Algorithms::Rayleigh::angleScattering(wavelength, angle, 180.0,
                                                         Algorithms::Rayleigh::Air, T, P);
        }
    };

    class NephZeroConversion : public NephZeroConditions {
        double angle;
    public:
        explicit NephZeroConversion(const Variant::Read &config) : NephZeroConditions(config),
                                                                   angle(config["Angle"].toReal())
        {
            if (!FP::defined(angle)) {
                if (config["Backscatter"].exists()) {
                    angle = config["Backscatter"].toBool() ? 90.0 : 0.0;
                } else {
                    angle = 0.0;
                }
            }
        }

        virtual ~NephZeroConversion() = default;

        EBASStructureState::DataColumn::Output convert(SequenceSegment &segment,
                                                       Column &column) const override
        {
            double r = getRayleigh(segment, column, angle);
            if (!FP::defined(r)) {
                return NephZeroConditions::convert(FP::undefined(), column);
            }

            double v = column.getData(segment).toReal();
            if (!FP::defined(v)) {
                return NephZeroConditions::convert(FP::undefined(), column);
            }

            return NephZeroConditions::convert(v + r, column);
        }
    };

    class NephRayleighConversion : public NephZeroConditions {
    public:
        explicit NephRayleighConversion(const Variant::Read &config) : NephZeroConditions(config)
        { }

        virtual ~NephRayleighConversion() = default;

        EBASStructureState::DataColumn::Output convert(SequenceSegment &segment,
                                                       Column &column) const override
        {
            return NephZeroConditions::convert(getRayleigh(segment, column, 0), column);
        }
    };

    class HexFlagsConversion : public Conversion {
        std::uint_fast64_t shift;
        std::uint_fast64_t mask;
        std::unordered_map<Variant::Flag, std::uint_fast64_t> bits;
    public:
        explicit HexFlagsConversion(const Variant::Read &config) : Conversion(config),
                                                                   shift(0),
                                                                   mask(static_cast<std::int_fast64_t>(-1))
        {
            if (config["Shift"].exists()) {
                shift = static_cast<std::uint_fast64_t>(config["Shift"].toInteger());
            }
            if (config["Mask"].exists()) {
                mask = static_cast<std::uint_fast64_t>(config["Mask"].toInteger());
            }
            for (auto add : config["Translate"].toHash()) {
                auto b = add.second["Bits"].toInteger();
                if (!INTEGER::defined(b) || b < 0)
                    continue;
                bits.emplace(add.first, static_cast<std::uint_fast64_t>(b));
            }
        }

        virtual ~HexFlagsConversion() = default;

        EBASStructureState::DataColumn::Output convert(SequenceSegment &segment,
                                                       Column &column) const override
        {
            using Output = EBASStructureState::DataColumn::Output;

            auto value = column.getData(segment);
            if (value.getType() != Variant::Type::Flags)
                return Output(column.getMVC(false), Output::State::Missing);

            auto metadata = column.getMetadata(segment);

            std::uint_fast64_t flagBits = 0;
            for (const auto &flag : value.toFlags()) {
                {
                    auto check = bits.find(flag);
                    if (check != bits.end()) {
                        flagBits |= check->second;
                        continue;
                    }
                }

                auto flagMeta = metadata.metadataSingleFlag(flag);
                auto check = flagMeta.hash("Bits").toInteger();
                if (!INTEGER::defined(check) || check <= 0)
                    continue;
                flagBits |= (static_cast<std::uint_fast64_t>(check) & mask) >> shift;
            }

            return Output(column.getFormat().apply(static_cast<qint64>(flagBits)),
                          Output::State::PresentAndAllowDiscard);
        }

        LineController::InputType getInputType(const SequenceName &name,
                                               const Column &column) const override
        {
            if (column.isPossiblyUsed(name))
                return LineController::InputType::Inject;
            return LineController::InputType::Unused;
        }
    };

    class ConversionSegment final : public Time::Bounds {
        std::shared_ptr<Conversion> conversion;
    public:
        explicit ConversionSegment(const ValueSegment &config) : Time::Bounds(config.getStart(),
                                                                              config.getEnd())
        {
            const auto &type = config["Processing"].toString();

            if (Util::equal_insensitive(type, "Constant", "Fixed")) {
                conversion = std::make_shared<ConstantConversion>(config.read());
            } else if (Util::equal_insensitive(type, "Optional", "RealOptional")) {
                conversion = std::make_shared<OptionalRealConversion>(config.read());
            } else if (Util::equal_insensitive(type, "Statistics", "Stats")) {
                conversion = std::make_shared<StatisticsConversion>(config.read());
            } else if (Util::equal_insensitive(type, "Quantile")) {
                conversion = std::make_shared<QuantileConversion>(config.read());
            } else if (Util::equal_insensitive(type, "NephZero", "NephelometerZero")) {
                conversion = std::make_shared<NephZeroConversion>(config.read());
            } else if (Util::equal_insensitive(type, "NephRayleigh", "NephelometerRayleigh")) {
                conversion = std::make_shared<NephRayleighConversion>(config.read());
            } else if (Util::equal_insensitive(type, "HexFlags", "HexadecimalFlags")) {
                conversion = std::make_shared<HexFlagsConversion>(config.read());
            } else {
                conversion = std::make_shared<RealConversion>(config.read());
            }
        }

        ~ConversionSegment() = default;

        ConversionSegment(const ConversionSegment &) = default;

        ConversionSegment &operator=(const ConversionSegment &) = default;

        ConversionSegment(ConversionSegment &&) = default;

        ConversionSegment &operator=(ConversionSegment &&) = default;

        ConversionSegment(const ConversionSegment &other, double start, double end) : Time::Bounds(
                other), conversion(other.conversion)
        { }

        ConversionSegment(const ConversionSegment &under,
                          const ConversionSegment &over,
                          double start,
                          double end) : Time::Bounds(under, over, start, end),
                                        conversion(over.conversion)
        { }


        EBASStructureState::DataColumn::Output convert(SequenceSegment &segment,
                                                       Column &column) const
        { return conversion->convert(segment, column); }

        LineController::InputType getInputType(const SequenceName &name, const Column &column) const
        { return conversion->getInputType(name, column); }

        void registerInput(const SequenceName &name)
        { conversion->registerInput(name); }

        Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                     const Archive::Selection::Match &defaultArchives = {},
                                                     const Archive::Selection::Match &defaultVariables = {}) const
        {
            return conversion->toArchiveSelections(defaultStations, defaultArchives,
                                                   defaultVariables);
        }
    };

    class Column final
            : public StagedFanout::OutputStage,
              virtual public LineController::ControllerInput,
              virtual public EBASStructureState::DataColumn {
        InputString::DetachedInput mvc;
        InputString::DetachedInput format;
        InputString::DetachedInput title_exact;
        InputString::DetachedInput title_component;
        InputString::DetachedInput title_wavelength;
        InputString::DetachedInput title_units;
        std::vector<std::unique_ptr<InputString::DetachedInput>> title_extra;
        InputString::DetachedInput columnHeader;

        QString metadataUnits;
        double metadataWavelength;

        std::int64_t sortPriority;
        std::deque<ConversionSegment> conversion;

        QString processedMVC;
        NumberFormat processedFormat;
        QString processedTitle;
        QString processedColumnHeader;

    public:
        Column(const Input &origin, AnalysisAssemblyContext &context) : StagedFanout::OutputStage(
                origin, context.structure.getStart(), context.structure.getEnd()),
                                                                        mvc(origin.parent
                                                                                  .parent
                                                                                  .mvc, context,
                                                                            origin),
                                                                        format(origin.parent
                                                                                     .parent
                                                                                     .format,
                                                                               context, origin),
                                                                        title_exact(origin.parent
                                                                                          .parent
                                                                                          .title_exact,
                                                                                    context,
                                                                                    origin),
                                                                        title_component(
                                                                                origin.parent
                                                                                      .parent
                                                                                      .title_component,
                                                                                context, origin),
                                                                        title_wavelength(
                                                                                origin.parent
                                                                                      .parent
                                                                                      .title_wavelength,
                                                                                context, origin),
                                                                        title_units(origin.parent
                                                                                          .parent
                                                                                          .title_units,
                                                                                    context,
                                                                                    origin),
                                                                        columnHeader(origin.parent
                                                                                           .parent
                                                                                           .columnHeader,
                                                                                     context,
                                                                                     origin),
                                                                        processedFormat(4, 2)
        {
            auto &vs = origin.parent.parent;

            for (const auto &extra : vs.title_extra) {
                title_extra.emplace_back(new InputString::DetachedInput(extra, context, origin));
            }

            metadataUnits = SubstitutionContext::Units::value(context.structure,
                                                              origin.getSubstitutionContext()
                                                                    .units);
            unitsFixup(metadataUnits);

            metadataWavelength = SubstitutionContext::Wavelength::value(context.structure,
                                                                        origin.getSubstitutionContext()
                                                                              .wavelength,
                                                                        FP::undefined());

            processedMVC = SubstitutionContext::MVC::value(context.structure,
                                                           origin.getSubstitutionContext().mvc);
            {
                auto check = SubstitutionContext::Format::value(context.structure,
                                                                origin.getSubstitutionContext()
                                                                      .format);
                if (!check.isEmpty()) {
                    processedFormat = NumberFormat(check);
                } else if (!processedMVC.isEmpty()) {
                    processedFormat = NumberFormat(processedMVC);
                }
            }

            sortPriority = SortPriority::value(context.structure, vs.sortPriority, 0);

            {
                auto applicable =
                        Range::findAllIntersecting(origin.parent.parent.conversion.begin(),
                                                   origin.parent.parent.conversion.end(),
                                                   context.structure.getStart(),
                                                   context.structure.getEnd());
                std::copy(applicable.first, applicable.second, Util::back_emplacer(conversion));
            }

            context.ebas.dataColumns.emplace_back(this);
        }

        virtual ~Column() = default;

        LineController::InputType getInputType(const SequenceName &name) override
        {
            LineController::InputType result = LineController::InputType::Unused;
            for (const auto &conv : conversion) {
                switch (conv.getInputType(name, *this)) {
                case LineController::InputType::Unused:
                    break;
                case LineController::InputType::Fragment:
                    return LineController::InputType::Fragment;
                case LineController::InputType::Inject:
                    result = LineController::InputType::Inject;
                    break;
                }
            }
            return result;
        }

        void registerInput(const SequenceName &name)
        {
            for (auto &conv : conversion) {
                conv.registerInput(name);
            }
        }

        QString getScale() override
        { return "1"; }

        QString getMVC(bool = false) override
        { return processedMVC; }

        QString getTitle() override
        { return processedTitle; }

        QString getColumnHeader() override
        { return processedColumnHeader; }

        std::vector<std::int_fast64_t> getSortPriority() override
        {
            int namePriority = 0;
            {
                auto names = get(FP::undefined(), FP::undefined());
                if (!names.empty())
                    namePriority = SequenceName::OrderDisplay::priority(*names.begin());
            }
            return {sortPriority, namePriority};
        }

        EBASStructureState::DataColumn::Output getValue(SequenceSegment &segment) override
        {
            if (!Range::intersectShift(conversion, segment))
                return Output(getMVC(false), Output::State::Missing);
            return conversion.front().convert(segment, *this);
        }

        const NumberFormat &getFormat() const
        { return processedFormat; }

        bool assembleOutput(OutputAssemblyContext &)
        {
            QString overrideMVC = mvc.toString();
            if (!overrideMVC.isEmpty()) {
                processedMVC = overrideMVC;
                processedFormat = NumberFormat(processedMVC);
            }
            QString overrideFormat = format.toString();
            if (!overrideFormat.isEmpty()) {
                processedFormat = NumberFormat(overrideFormat);
            }
            if (processedMVC.isEmpty()) {
                processedMVC = processedFormat.mvc();
            }

            if (processedMVC.isEmpty()) {
                qCInfo(log_fileoutput_ebas) << "No MVC defined for variable";
                return false;
            }

            processedColumnHeader = columnHeader.toString();
            if (processedColumnHeader.isEmpty()) {
                qCInfo(log_fileoutput_ebas) << "No column header defined for variable";
                return false;
            }

            processedTitle = title_exact.toString();
            if (processedTitle.isEmpty()) {
                QString component = title_component.toString();
                if (component.isEmpty()) {
                    qCInfo(log_fileoutput_ebas) << "No title component defined for variable";
                    return false;
                }

                QStringList titleFields;
                titleFields += component;

                {
                    QString units = title_units.toString();
                    if (units.isEmpty()) {
                        units = metadataUnits;
                    }
                    if (units.isEmpty()) {
                        units = "no unit";
                    }
                    titleFields += units;
                }

                {
                    QString wavelength = title_wavelength.toString();
                    if (wavelength.isEmpty()) {
                        if (FP::defined(metadataWavelength) && metadataWavelength > 0.0) {
                            wavelength =
                                    QString::number(static_cast<int>(qRound(metadataWavelength)));
                        }
                    }
                    if (!wavelength.isEmpty()) {
                        titleFields += "Wavelength=" + wavelength + " nm";
                    }
                }

                for (const auto &add : title_extra) {
                    auto field = add->toString();
                    if (field.isEmpty())
                        continue;
                    titleFields += field;
                }

                processedTitle = titleFields.join(", ");

                Q_ASSERT(!processedTitle.isEmpty());
            }

            return true;
        }
    };

    class Output final : public FileOutputElement {
        std::vector<std::unique_ptr<Column>> columns;
    public:
        Output(OutputAssemblyContext &context, std::vector<std::unique_ptr<Column>> &&columns)
                : FileOutputElement(context), columns(std::move(columns))
        {
            for (const auto &col : this->columns) {
                context.lineController->attachInput(col.get());
            }
        }

        virtual ~Output() = default;
    };

    class Analysis final : public DataAnalysisElement {
        std::vector<std::unique_ptr<Column>> columns;
    public:
        Analysis(AnalysisAssemblyContext &context, std::vector<std::unique_ptr<Column>> &&columns)
                : DataAnalysisElement(context), columns(std::move(columns))
        {
            for (const auto &col : this->columns) {
                context.lineController->attachInput(col.get());
            }
        }

        virtual ~Analysis() = default;

        std::vector<StreamSink *> getSecondStageTargets(const SequenceName &name) override
        {
            for (auto &col : this->columns) {
                col->registerInput(name);
            }
            return {};
        }

        bool assembleOutput(OutputAssemblyContext &context) override
        {
            for (auto &col : this->columns) {
                if (!col->assembleOutput(context))
                    return false;
            }

            context.output.emplace_back(new Output(context, std::move(columns)));
            return true;
        }
    };


    InputString::Detachable mvc;
    InputString::Detachable format;
    InputString::Detachable title_exact;
    InputString::Detachable title_component;
    InputString::Detachable title_wavelength;
    InputString::Detachable title_units;
    std::vector<InputString::Detachable> title_extra;
    InputString::Detachable columnHeader;

    std::deque<ConversionSegment> conversion;

public:
    VariableSetStructure(const ValueSegment::Transfer &config, StructureAssemblyContext &context)
            : FileStructureElement(config, context),
              fragment(context.fragment),
              sortPriority(fragment.allocateKey()),
              provideKeys(fragment.allocateKey()),
              data(ValueSegment::withPath(config, "Data"), context, *this),
              mvc(std::make_shared<InputString>(config, context, "MVC")),
              format(std::make_shared<InputString>(config, context, "Format")),
              title_exact(std::make_shared<InputString>(config, context, "Header/Title/Full")),
              title_component(
                      std::make_shared<InputString>(config, context, "Header/Title/Component")),
              title_wavelength(
                      std::make_shared<InputString>(config, context, "Header/Title/Wavelength")),
              title_units(std::make_shared<InputString>(config, context, "Header/Title/Units")),
              columnHeader(std::make_shared<InputString>(config, context, "Header/Column"))
    {
        std::size_t totalExtra = 0;
        for (const auto &seg : config) {
            {
                auto priority = seg["SortPriority"].toInteger();
                if (INTEGER::defined(priority)) {
                    fragment.add(sortPriority, std::make_shared<SortPriority>(priority),
                                 seg.getStart(), seg.getEnd());
                }
            }
            fragment.add(provideKeys,
                         std::make_shared<ProvideKeys>(seg["Dependencies/Provide"].toFlags()),
                         seg.getStart(), seg.getEnd());

            Range::overlayFragmenting(conversion, ConversionSegment(seg));

            totalExtra = std::max(totalExtra, seg["Header/Title/Extra"].toArray().size());
        }

        for (std::size_t i = 0; i < totalExtra; i++) {
            title_extra.emplace_back(std::make_shared<InputString>(config, context,
                                                                   "Header/Title/Extra/#" +
                                                                           std::to_string(i)));
        }
    }

    virtual ~VariableSetStructure() = default;

    Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                 const Archive::Selection::Match &defaultArchives = {},
                                                 const Archive::Selection::Match &defaultVariables = {}) const override
    {
        auto result = data.toArchiveSelections(defaultStations, defaultArchives, defaultVariables);
        Util::append(mvc->toArchiveSelections(defaultStations, defaultArchives, defaultVariables),
                     result);
        Util::append(
                format->toArchiveSelections(defaultStations, defaultArchives, defaultVariables),
                result);
        Util::append(title_exact->toArchiveSelections(defaultStations, defaultArchives,
                                                      defaultVariables), result);
        Util::append(title_component->toArchiveSelections(defaultStations, defaultArchives,
                                                          defaultVariables), result);
        Util::append(title_wavelength->toArchiveSelections(defaultStations, defaultArchives,
                                                           defaultVariables), result);
        Util::append(title_units->toArchiveSelections(defaultStations, defaultArchives,
                                                      defaultVariables), result);
        Util::append(columnHeader->toArchiveSelections(defaultStations, defaultArchives,
                                                       defaultVariables), result);
        for (const auto &extra : title_extra) {
            Util::append(
                    extra->toArchiveSelections(defaultStations, defaultArchives, defaultVariables),
                    result);
        }
        for (const auto &conv : conversion) {
            Util::append(
                    conv.toArchiveSelections(defaultStations, defaultArchives, defaultVariables),
                    result);
        }
        return result;
    }

    std::vector<StreamSink *> getFirstStageTargets(const SequenceName &key) override
    {
        auto result = data.getTargets(key);
        Util::append(mvc->getTargets(key), result);
        Util::append(format->getTargets(key), result);
        Util::append(title_exact->getTargets(key), result);
        Util::append(title_component->getTargets(key), result);
        Util::append(title_wavelength->getTargets(key), result);
        Util::append(title_units->getTargets(key), result);
        Util::append(columnHeader->getTargets(key), result);
        for (const auto &extra : title_extra) {
            Util::append(extra->getTargets(key), result);
        }
        return result;
    }

    void structureExtend() override
    {
        for (auto input : data.getInputs()) {
            static_cast<Input *>(input)->structureExtend();
        }

        mvc->structureExtend();
        format->structureExtend();
        title_exact->structureExtend();
        title_component->structureExtend();
        title_wavelength->structureExtend();
        title_units->structureExtend();
        columnHeader->structureExtend();
        for (const auto &extra : title_extra) {
            extra->structureExtend();
        }

        SortPriority::extendOverEmpty(fragment, sortPriority);
        ProvideKeys::extendOverEmpty(fragment, provideKeys);
    }

    FragmentTracker::MergeTestList structureMerge() override
    {
        FragmentTracker::MergeTestList result;
        for (auto input : data.getInputs()) {
            Util::append(static_cast<Input *>(input)->structureMerge(), result);
        }

        Util::append(mvc->splitMergeTest(), result);
        Util::append(format->splitMergeTest(), result);
        Util::append(title_exact->splitMergeTest(), result);
        Util::append(title_component->splitMergeTest(), result);
        Util::append(title_wavelength->splitMergeTest(), result);
        Util::append(title_units->splitMergeTest(), result);
        Util::append(columnHeader->splitMergeTest(), result);
        for (const auto &extra : title_extra) {
            Util::append(extra->splitMergeTest(), result);
        }

        return result;
    }

    bool assembleAnalysis(AnalysisAssemblyContext &context) override
    {
        std::vector<std::unique_ptr<Column>> columns;
        for (auto input : data.getInputs()) {
            if (!input->isActive(context.structure.getStart(), context.structure.getEnd()))
                continue;
            columns.emplace_back(new Column(*static_cast<Input *>(input), context));
        }

        if (columns.empty())
            return true;

        Util::merge(ProvideKeys::value(context.structure, provideKeys), context.availableKeys);

        context.analysis.emplace_back(new Analysis(context, std::move(columns)));
        return true;
    }
};


class NumflagStructure final : public FileStructureElement {
    class Output
            : public FileOutputElement,
              virtual public LineController::ControllerInput,
              virtual public EBASStructureState::DataColumn {
        static constexpr int maximumFlags = 3;

        std::unique_ptr<DynamicSequenceSelection> dataFlags;
    public:
        Output(OutputAssemblyContext &context,
               std::unique_ptr<DynamicSequenceSelection> &&dataFlags) : FileOutputElement(context),
                                                                        dataFlags(std::move(
                                                                                dataFlags))
        {
            context.ebas.dataColumns.emplace_back(this);
            context.lineController->attachInput(this);
        }

        LineController::InputType getInputType(const SequenceName &name) override
        {
            if (!dataFlags->registerInput(name))
                return LineController::InputType::Unused;
            return LineController::InputType::Inject;
        }

        QString getScale() override
        { return "1"; }

        QString getMVC(bool header = true) override
        {
            if (header) {
                return "9." + QString("999").repeated(maximumFlags);
            }
            return "0.999" + QString("000").repeated(maximumFlags - 1);
        }

        QString getTitle() override
        { return "numflag"; }

        QString getColumnHeader() override
        { return "numflag"; }

        std::vector<std::int_fast64_t> getSortPriority() override
        { return {}; }

        EBASStructureState::DataColumn::Output getValue(SequenceSegment &segment) override
        {
            std::unordered_set<int> codes;

            for (const auto &name : dataFlags->get(segment)) {
                for (const auto &flag : segment[name].toFlags()) {
                    if (!Util::starts_with(flag, "EBASFlag"))
                        continue;
                    int code = std::atoi(flag.data() + 8);
                    if (code <= 0)
                        continue;
                    codes.insert(code);
                }
            }

            std::vector<int> sorted(codes.begin(), codes.end());
            std::sort(sorted.begin(), sorted.end(), [](int a, int b) { return a > b; });
            sorted.resize(maximumFlags, 0);
            EBASStructureState::DataColumn::Output result
                    ("0.", EBASStructureState::DataColumn::Output::State::PresentAndAllowDiscard);
            for (auto add : sorted) {
                result.columnValue += QString::number(add).rightJustified(3, '0');
            }
            return result;
        }
    };

    class Analysis : public DataAnalysisElement {
        std::unique_ptr<DynamicSequenceSelection> dataFlags;
    public:
        Analysis(AnalysisAssemblyContext &context, NumflagStructure &parent) : DataAnalysisElement(
                context), dataFlags(parent.dataFlags->clone())
        { }

        virtual ~Analysis() = default;

        bool assembleOutput(OutputAssemblyContext &context) override
        {
            context.output.emplace_back(new Output(context, std::move(dataFlags)));
            return true;
        }
    };

    friend class Analysis;

    std::unique_ptr<DynamicSequenceSelection> dataFlags;

public:
    NumflagStructure(const ValueSegment::Transfer &config, StructureAssemblyContext &context)
            : FileStructureElement(config, context),
              dataFlags(DynamicSequenceSelection::fromConfiguration(config, "Numflag/DataFlags",
                                                                    context.config.start,
                                                                    context.config.end))
    { }

    virtual ~NumflagStructure() = default;

    Archive::Selection::List toArchiveSelections(const Archive::Selection::Match &defaultStations = {},
                                                 const Archive::Selection::Match &defaultArchives = {},
                                                 const Archive::Selection::Match &defaultVariables = {}) const override
    {
        return dataFlags->toArchiveSelections(defaultStations, defaultArchives, defaultVariables);
    }

    bool assembleAnalysis(AnalysisAssemblyContext &context) override
    {
        context.analysis.emplace_back(new Analysis(context, *this));
        return true;
    }
};


struct OutputSegment : public Time::Bounds {
    std::vector<std::unique_ptr<FileOutputElement>> output;
    std::unique_ptr<QFile> targetFile;

    explicit OutputSegment(OutputAssemblyContext &&context) : Time::Bounds(context.fileBounds),
                                                              output(std::move(context.output)),
                                                              targetFile(
                                                                      std::move(context.targetFile))
    { }

    ~OutputSegment()
    {
        if (targetFile) {
            targetFile->close();
            targetFile->remove();
        }
    }

    OutputSegment(OutputSegment &&) = default;

    OutputSegment &operator=(OutputSegment &&) = default;
};

bool EBASStructureState::outputHeader(QFile *file, StructureDynamicState &dynamic)
{
    if (dataColumns.empty()) {
        qCDebug(log_fileoutput_ebas) << "No data variables defined";
        return false;
    }

    std::sort(dataColumns.begin(), dataColumns.end(), [](DataColumn *a, DataColumn *b) {
        auto pa = a->getSortPriority();
        auto pb = b->getSortPriority();

        for (std::size_t idx = 0, max = std::min(pa.size(), pb.size()); idx < max; idx++) {
            auto pia = pa[idx];
            auto pib = pb[idx];
            if (pia != pib)
                return pia < pib;
        }
        return pa.size() > pb.size();
    });

    std::sort(headers_EBAS.begin(), headers_EBAS.end(), [](EBASHeader *a, EBASHeader *b) {
        return a->getSortPriority() < b->getSortPriority();
    });


    class EndTimeColumn : virtual public DataColumn {
    public:
        EndTimeColumn() = default;

        virtual ~EndTimeColumn() = default;

        virtual QString getScale()
        { return "1"; }

        virtual QString getMVC(bool)
        { return "9999.999999"; }

        virtual QString getTitle()
        { return "end_time of measurement, days from the file reference point"; }

        virtual QString getColumnHeader()
        { return "end_time"; }

        virtual std::vector<std::int_fast64_t> getSortPriority()
        { return {}; }

        virtual Output getValue(SequenceSegment &)
        { return {}; }
    };

    EndTimeColumn endColumn;

    dataColumns.insert(dataColumns.begin(), &endColumn);

    QStringList headerLines;
    /* Placeholder for total header count */
    headerLines += QString();

    headerLines += headers_NASAAmes.originator;
    headerLines += headers_NASAAmes.organization;
    headerLines += headers_NASAAmes.submitter;
    headerLines += headers_NASAAmes.projects;
    headerLines += "1 1"; /* File splitting, constant for EBAS */

    {
        QString dates;

        if (!FP::defined(dynamic.fileReferenceDate)) {
            qCInfo(log_fileoutput_ebas) << "No file reference data set";
            return false;
        }

        auto dt = Time::toDateTime(dynamic.fileReferenceDate);
        dates += QString::number(dt.date().year()).rightJustified(4, '0');
        dates += " ";
        dates += QString::number(dt.date().month()).rightJustified(2, '0');
        dates += " ";
        dates += QString::number(dt.date().day()).rightJustified(2, '0');
        dates += " ";

        dt = Time::toDateTime(dynamic.modified);
        dates += QString::number(dt.date().year()).rightJustified(4, '0');
        dates += " ";
        dates += QString::number(dt.date().month()).rightJustified(2, '0');
        dates += " ";
        dates += QString::number(dt.date().day()).rightJustified(2, '0');

        headerLines += dates;
    }

    headerLines += headers_NASAAmes.interval;
    headerLines += "days from file reference point"; /* Time units */

    headerLines += QString::number(dataColumns.size());
    {
        QStringList scales;
        QStringList mvcs;
        for (auto add : dataColumns) {
            scales += add->getScale();
            mvcs += add->getMVC();
        }
        headerLines += scales.join(' ');
        headerLines += mvcs.join(' ');
    }
    for (auto add : dataColumns) {
        headerLines += add->getTitle();
    }

    headerLines += "0"; /* Number of special comment lines, reserved by EBAS */

    {
        QStringList normalComments;

        struct OutputHeader {
            QString key;
            QString value;
        };

        std::vector<OutputHeader> outputHeaders;

        int keyWidth = 0;
        for (auto input : headers_EBAS) {
            OutputHeader header;

            header.key = input->getEBASKey();
            if (header.key.contains(':')) {
                header.key.replace('"', R"(""")");
                header.key.prepend('"');
                header.key.append('"');
            }

            header.value = input->getEBASValue();
            if (header.value.contains(':')) {
                header.value.replace('"', R"(""")");
                header.value.prepend('"');
                header.value.append('"');
            }

            keyWidth = std::max(header.key.length(), keyWidth);

            outputHeaders.emplace_back(std::move(header));
        }
        keyWidth += 2; /* ": " */

        for (const auto &header : outputHeaders) {
            if (header.key.isEmpty() || header.value.isEmpty())
                continue;

            QString headerContents = header.key;
            headerContents += ": ";
            headerContents = headerContents.leftJustified(keyWidth, ' ');
            headerContents += header.value;
            normalComments += headerContents;
        }


        {
            QStringList columnHeaders;
            columnHeaders += "start_time";
            for (auto add : dataColumns) {
                columnHeaders += add->getColumnHeader();
            }
            normalComments += columnHeaders.join(' ');
        }
        headerLines += QString::number(normalComments.size());
        Util::append(normalComments, headerLines);
    }

    dataColumns.erase(dataColumns.begin());

    headerLines.front() = QString::number(headerLines.size()) + " 1001";
    for (const auto &line : headerLines) {
        if (!writeLine(file, line))
            return false;
    }
    return true;
}

class OutputStage : public Engine::SinkDispatchStage {
    EBAS config;

    std::vector<OutputSegment> segments;
    std::vector<std::unique_ptr<Engine::ClipSinkWrapper>> clipWrappers;
public:
    OutputStage(EBAS &&input, std::vector<OutputSegment> &&segs) : config(std::move(input)),
                                                                   segments(std::move(segs))
    { }

    virtual ~OutputStage() = default;

    bool isFinalStage() const override
    { return true; }

    void finalizeData() override
    {
        Engine::SinkDispatchStage::finalizeData();
        clipWrappers.clear();

        for (auto &seg : segments) {
            bool segOk = true;
            for (const auto &out : seg.output) {
                if (!out->finishOutput()) {
                    segOk = false;
                    break;
                }
            }

            seg.targetFile->close();

            if (!segOk) {
                seg.targetFile->remove();
            }

            seg.targetFile.reset();
        }
    }

protected:
    std::vector<StreamSink *> getTargets(const SequenceName &name) override
    {
        std::vector<StreamSink *> result;

        for (const auto &seg : segments) {
            for (const auto &out : seg.output) {
                for (auto base : out->getOutputTargets(name)) {
                    std::unique_ptr<Engine::ClipSinkWrapper>
                            wrap(new Engine::ClipSinkWrapper(seg, base));
                    result.emplace_back(wrap.get());
                    clipWrappers.emplace_back(std::move(wrap));
                }
            }
        }

        return result;
    }
};

struct AnalysisSegment : public Time::Bounds {
    std::vector<std::unique_ptr<DataAnalysisElement>> analysis;
    std::shared_ptr<StructureDynamicState> dynamic;
    EBASStructureState ebas;

    AnalysisSegment(AnalysisAssemblyContext &&context) : Time::Bounds(context.structure),
                                                         analysis(std::move(context.analysis)),
                                                         dynamic(std::move(context.dynamic)),
                                                         ebas(std::move(context.ebas))
    { }

    ~AnalysisSegment() = default;

    AnalysisSegment(AnalysisSegment &&) = default;

    AnalysisSegment &operator=(AnalysisSegment &&) = default;
};

class SecondStage : public Engine::SinkDispatchStage {
    EBAS config;

    std::vector<AnalysisSegment> segments;
    std::vector<std::unique_ptr<Engine::ClipSinkWrapper>> clipWrappers;
public:
    SecondStage(EBAS &&input, std::vector<AnalysisSegment> &&segs) : config(std::move(input)),
                                                                     segments(std::move(segs))
    { }

    virtual ~SecondStage() = default;

    bool isFinalStage() const override
    { return false; }

    void finalizeData() override
    {
        Engine::SinkDispatchStage::finalizeData();
        clipWrappers.clear();
    }

    std::unique_ptr<Stage> getNextStage() override
    {
        std::vector<OutputSegment> outputSegments;
        for (auto &input : segments) {
            input.dynamic->start = input.getStart();
            input.dynamic->end = input.getEnd();

            OutputAssemblyContext context(config, input.ebas, input, std::move(input.dynamic));
            for (const auto &add : input.analysis) {
                add->prepareOutput(context);
            }

            bool isOk = true;
            for (const auto &add : input.analysis) {
                if (!add->assembleOutput(context)) {
                    isOk = false;
                }
            }
            if (!isOk)
                continue;
            if (!context.targetFile)
                continue;

            Q_ASSERT(context.targetFile->isOpen());

            for (const auto &add : context.output) {
                if (!add->initializeOutput(context)) {
                    isOk = false;
                }
            }
            if (!isOk)
                continue;

            outputSegments.emplace_back(std::move(context));
        }

        if (outputSegments.empty()) {
            qCDebug(log_fileoutput_ebas) << "No output segments, output stage aborted";
            return {};
        }

        return std::unique_ptr<Engine::Stage>(
                new OutputStage(std::move(config), std::move(outputSegments)));
    }

protected:
    std::vector<StreamSink *> getTargets(const SequenceName &name) override
    {
        std::vector<StreamSink *> result;

        for (const auto &seg : segments) {
            for (const auto &out : seg.analysis) {
                for (auto base : out->getSecondStageTargets(name)) {
                    std::unique_ptr<Engine::ClipSinkWrapper>
                            wrap(new Engine::ClipSinkWrapper(seg, base));
                    result.emplace_back(wrap.get());
                    clipWrappers.emplace_back(std::move(wrap));
                }
            }
        }

        return result;
    }
};

class FirstStage : public Engine::SinkDispatchStage {
    EBAS config;

    struct Type {
        FragmentTracker fragment;
        std::vector<std::unique_ptr<FileStructureElement>> structure;
        std::shared_ptr<StructureDynamicState> dynamic;

        explicit Type(const EBAS &config) : dynamic(std::make_shared<StructureDynamicState>(config))
        {
            fragment.addEmpty(config.start, config.end);
        }
    };

    std::vector<std::unique_ptr<Type>> types;


    std::unique_ptr<Type> assembleType(const ValueSegment::Transfer &typeConfig)
    {
        std::unique_ptr<Type> result(new Type(config));
        StructureAssemblyContext context(config, typeConfig, result->fragment, result->dynamic);
        context.structure.emplace_back(new LineControllerStructure(context.base, context));

        context.forAllChildren("Substitutions",
                               [&](const ValueSegment::Transfer &scfg, const std::string &key) {
                                   auto add = std::unique_ptr<DataSubstitutionStructure>(
                                           new DataSubstitutionStructure(scfg, context));
                                   if (!context.substitutions
                                               .emplace(Util::to_lower(key),
                                                        add->toValueSubstitution())
                                               .second)
                                       return;
                                   context.structure.emplace_back(std::move(add));
                               });
        context.forAllChildren("GlobalMean",
                               [&](const ValueSegment::Transfer &scfg, const std::string &key) {
                                   auto add = std::unique_ptr<MeanSubstitutionStructure>(
                                           new MeanSubstitutionStructure(scfg, context));
                                   if (!context.substitutions
                                               .emplace(Util::to_lower(key),
                                                        add->toValueSubstitution())
                                               .second)
                                       return;
                                   context.structure.emplace_back(std::move(add));
                               });

        context.structure.emplace_back(new EBASFileStructure(context.base, context));
        context.structure.emplace_back(new NumflagStructure(context.base, context));

        context.forAllChildren("Headers", [&](const ValueSegment::Transfer &hcfg) {
            auto add = std::unique_ptr<EBASHeaderStructure>(new EBASHeaderStructure(hcfg, context));
            context.structure.emplace_back(std::move(add));
        });

        context.forAllChildren("Variables", [&](const ValueSegment::Transfer &vcfg) {
            auto add =
                    std::unique_ptr<VariableSetStructure>(new VariableSetStructure(vcfg, context));
            context.structure.emplace_back(std::move(add));
        });

        context.structure.emplace_back(new DependencyStructure(context.base, context));


        result->structure = std::move(context.structure);
        return std::move(result);
    }

public:
    explicit FirstStage(EBAS input) : config(std::move(input))
    {
        {
            auto save = std::move(config.config);
            config.config.clear();
            auto applicable = Range::findAllIntersecting(save, config.start, config.end);
            std::move(applicable.first, applicable.second, Util::back_emplacer(config.config));
        }
        if (!config.config.empty()) {
            if (Range::compareStart(config.config.front().getStart(), config.start) < 0)
                config.config.front().setStart(config.start);
            if (Range::compareEnd(config.config.front().getEnd(), config.end) > 0)
                config.config.front().setEnd(config.end);
        }

        std::unordered_set<std::string> knownTypes;
        for (const auto &seg : config.config) {
            Util::merge(seg["Types"].toHash().keys(), knownTypes);
        }
        for (const auto &type : knownTypes) {
            if (type.empty())
                continue;
            if (!config.typeRestrict.empty()) {
                if (!Util::exact_match(type, QRegularExpression(
                        QString::fromStdString(config.typeRestrict),
                        QRegularExpression::CaseInsensitiveOption))) {
                    continue;
                }
            }

            auto typeConfig = StructureAssemblyContext::deriveConfig(config.config,
                                                                     {{Variant::PathElement::Type::Hash, "Types"},
                                                                      {Variant::PathElement::Type::Hash, type}});
            auto typeStructure = assembleType(typeConfig);
            if (!typeStructure)
                continue;
            types.emplace_back(std::move(typeStructure));
        }

        if (types.empty()) {
            qCDebug(log_fileoutput_ebas) << "No EBAS output types available";
        }
    }

    virtual ~FirstStage() = default;

    bool isFinalStage() const override
    { return false; }

    std::unique_ptr<Stage> getNextStage() override
    {
        std::vector<AnalysisSegment> segments;
        for (const auto &type : types) {
            for (auto &fileContents : type->fragment.getFragments()) {
                if (Range::compareStart(fileContents.getStart(), config.start) < 0)
                    fileContents.setStart(config.start);
                if (Range::compareEnd(fileContents.getEnd(), config.end) > 0)
                    fileContents.setEnd(config.end);

                type->dynamic->start = fileContents.getStart();
                type->dynamic->end = fileContents.getEnd();

                AnalysisAssemblyContext context(config, fileContents, type->dynamic);
                for (const auto &add : type->structure) {
                    add->prepareAnalysis(context);
                }

                bool isOk = true;
                for (const auto &add : type->structure) {
                    if (!add->assembleAnalysis(context)) {
                        isOk = false;
                    }
                }
                if (!isOk)
                    continue;

                segments.emplace_back(std::move(context));
            }
        }

        if (segments.empty()) {
            qCDebug(log_fileoutput_ebas) << "No analysis segments, second stage aborted";
            return {};
        }

        return std::unique_ptr<Engine::Stage>(
                new SecondStage(std::move(config), std::move(segments)));
    }

    void finalizeData() override
    {
        Engine::SinkDispatchStage::finalizeData();

        for (auto &type : types) {
            FragmentTracker::MergeTestList mergeTests;
            for (const auto &add : type->structure) {
                add->structureExtend();
                Util::append(add->structureMerge(), mergeTests);
            }
            type->fragment.merge(mergeTests);
            for (const auto &add : type->structure) {
                add->structureFinalize();
            }
        }
    }

    bool configurePipeline(StreamPipeline *target) const
    {
        if (types.empty())
            return false;

        Archive::Selection::List selections;
        for (const auto &type : types) {
            for (const auto &add : type->structure) {
                Util::append(add->toArchiveSelections({config.station}), selections);
            }
        }

        Variant::Read processing = Variant::Read::empty();
        if (!config.config.empty()) {
            processing = config.config.back()["Processing"];
        }
        return Engine::configurePipelineInput(processing, config.station, target,
                                              std::move(selections),
                                              Time::Bounds(config.start, config.end));
    }

protected:
    std::vector<StreamSink *> getTargets(const SequenceName &name) override
    {
        std::vector<StreamSink *> result;
        for (const auto &type : types) {
            for (const auto &add : type->structure) {
                Util::append(add->getFirstStageTargets(name), result);
            }
        }
        return result;
    }
};

StructureAssemblyContext::StructureAssemblyContext(EBAS &config,
                                                   const ValueSegment::Transfer &base,
                                                   FragmentTracker &fragment,
                                                   std::shared_ptr<StructureDynamicState> dynamic)
        : config(config), base(base), fragment(fragment), dynamic(std::move(dynamic))
{ }

StructureAssemblyContext::~StructureAssemblyContext() = default;

AnalysisAssemblyContext::AnalysisAssemblyContext(EBAS &config,
                                                 const FragmentTracker::Result &structure,
                                                 std::shared_ptr<StructureDynamicState> dynamic)
        : config(config), structure(structure), dynamic(std::move(dynamic)), lineController(nullptr)
{ }

AnalysisAssemblyContext::~AnalysisAssemblyContext() = default;

OutputAssemblyContext::OutputAssemblyContext(EBAS &config,
                                             EBASStructureState &ebas,
                                             const Time::Bounds &fileBounds,
                                             std::shared_ptr<StructureDynamicState> dynamic)
        : config(config),
          ebas(ebas),
          fileBounds(fileBounds),
          dynamic(std::move(dynamic)),
          lineController(nullptr),
          columnController(nullptr)
{ }

OutputAssemblyContext::~OutputAssemblyContext()
{
    if (targetFile) {
        targetFile->close();
        targetFile->remove();
    }
}

}

EBAS::EBAS() : start(FP::undefined()), end(FP::undefined())
{ }

std::unique_ptr<Engine::Stage> EBAS::stage(EBAS config)
{ return std::unique_ptr<Engine::Stage>(new FirstStage(std::move(config))); }

bool EBAS::configurePipeline(const Engine::Stage *firstStage, StreamPipeline *target)
{ return static_cast<const FirstStage *>(firstStage)->configurePipeline(target); }

}
}