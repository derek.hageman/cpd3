/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CPD3FILEOUTPUT_H
#define CPD3FILEOUTPUT_H

#include <QtCore/QtGlobal>

#if defined(cpd3fileoutput_EXPORTS)
#   define CPD3FILEOUTPUT_EXPORT Q_DECL_EXPORT
#else
#   define CPD3FILEOUTPUT_EXPORT Q_DECL_IMPORT
#endif

#endif
