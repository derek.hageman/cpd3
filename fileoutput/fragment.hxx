/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CPD3FILEOUTPUT_FRAGMENT_HXX
#define CPD3FILEOUTPUT_FRAGMENT_HXX

#include "core/first.hxx"

#include <unordered_map>
#include <deque>
#include <memory>
#include <functional>

#include "fileoutput.hxx"
#include "core/timeutils.hxx"
#include "core/number.hxx"

namespace CPD3 {
namespace Output {

/**
 * A tracker for assembling and merging segments composed of multiple types.
 */
class CPD3FILEOUTPUT_EXPORT FragmentTracker {
    class Segment;

public:
    using Key = std::size_t;

    static constexpr Key invalidKey = static_cast<Key>(-1);

    class Value;

    using ValueHandle = std::shared_ptr<Value>;

    /**
     * The base type for the values added to the the fragment tracker.
     */
    class CPD3FILEOUTPUT_EXPORT Value {
    public:
        Value();

        virtual ~Value();

        /**
         * Perform an overlay of a value.  This is called on the under value and must
         * return a new value if any changes are required.
         *
         * @param under     the under value handle
         * @param over      the overlay value
         * @return          under if no changes are made or a new value if any are
         */
        virtual ValueHandle overlay(const ValueHandle &under, const ValueHandle &over) = 0;

        /**
         * Perform a merge of two values.
         *
         * @param other     the value to merge with this one
         */
        virtual void merge(Value *other);

        /**
         * Convert a value to its output representation.  If no changes are required, then
         * this can simply return self.
         *
         * @param self      the value handler
         * @param start     the output segment start time
         * @param end       the output segment end time
         * @return          the output segment value
         */
        virtual ValueHandle toResult(const ValueHandle &self, double start, double end);
    };

    using Contents = std::unordered_map<Key, ValueHandle>;

    /**
     * A container for the results of a fragment tracker segment.
     */
    struct CPD3FILEOUTPUT_EXPORT Result : public Time::Bounds {
        /**
         * The contents of the segment.
         */
        Contents contents;

        inline Result() = default;

        Result(const Contents &contents, double start, double end);

        Result(Contents &&contents, double start, double end);
    };

    /**
     * A context that merge tests are performed with.
     */
    struct CPD3FILEOUTPUT_EXPORT MergeTestContext {
        /**
         * The times of the first fragment.
         */
        const Time::Bounds &firstFragment;

        /**
         * The contents of the first fragment.
         */
        const Contents &firstContents;

        /**
         * The times of the second fragment.
         */
        const Time::Bounds &secondFragment;

        /**
         * The contents of the second fragment.
         */
        const Contents &secondContents;

        MergeTestContext(const Segment &first, const Segment &second);
    };

    using MergeTest = std::function<
            bool(const Key &key, Value *first, Value *second, const MergeTestContext &context)>;

    /**
     * A context that extension tests are performed with.
     */
    struct CPD3FILEOUTPUT_EXPORT ExtendTestContext {
        /**
         * The times of the first fragment.
         */
        const Time::Bounds &fragment;

        /**
         * The contents of the first fragment.
         */
        const Contents &contents;

        explicit ExtendTestContext(const Segment &segment);
    };

    using ExtendTest = std::function<bool(Value *check, const ExtendTestContext &context)>;

private:

    class Overlay : public Time::Bounds {
    public:
        Key key;
        ValueHandle value;

        Overlay(Key key, const ValueHandle &value, double start, double end);

        Overlay(Key key, ValueHandle &&value, double start, double end);
    };

    class Segment : public Time::Bounds {
        Contents values;

    public:
        inline Segment() = default;

        inline virtual ~Segment() = default;

        inline Segment(const Segment &) = default;

        inline Segment(Segment &&) = default;

        inline Segment &operator=(const Segment &) = default;

        inline Segment &operator=(Segment &&) = default;

        explicit Segment(const Overlay &over);

        Segment(double start, double end);

        Segment(const Segment &other, double start, double end);

        Segment(const Segment &under, const Segment &over, double start, double end);

        Segment(const Overlay &other, double start, double end);

        Segment(const Segment &under, const Overlay &over, double start, double end);

        bool mergeCandidate(const Segment &first, const MergeTest &canMerge);

        void merge(const Segment &first);

        Contents getResult() const;

        inline const Contents &getContents() const
        { return values; }

        inline Contents &getContents()
        { return values; }
    };

    std::deque<Segment> segments;
    Key nextKey;

public:
    FragmentTracker();

    ~FragmentTracker();

    /**
     * Allocate a new key to disambiguate overlay types.
     *
     * @return
     */
    Key allocateKey();

    /**
     * Add a value to the fragment tracking.
     *
     * @param key       the key the value is associated with
     * @param value     the value to add
     * @param start     the start time
     * @param end       the end time
     */
    void add(Key key,
             const ValueHandle &value,
             double start = CPD3::FP::undefined(),
             double end = CPD3::FP::undefined());

    void add(Key key,
             ValueHandle &&value,
             double start = CPD3::FP::undefined(),
             double end = CPD3::FP::undefined());

    /**
     * Add an empty fragment that can be used to fill gaps
     *
     * @param start     the start time
     * @param end       the end time
     */
    void addEmpty(double start = FP::undefined(), double end = FP::undefined());

    /**
     * Perform a merge pass on all segments in the fragment tracker.
     *
     * @param canMerge  the merge test function, either key may be null if it is not present
     */
    void merge(const MergeTest &canMerge);

    using MergeTestApplies = std::function<bool(const Key &key, Value *first, Value *second)>;

    using MergeComposite = std::pair<MergeTestApplies, MergeTest>;

    using MergeTestList = std::vector<MergeComposite>;

    /**
     * Perform a merge pass on all segments in the fragment tracker.
     *
     * @param tests the various merge tests, where all applicable ones must pass
     * @param mergeIfNone perform the merge even if no tests apply to the segment
     */
    void merge(const MergeTestList &tests, bool mergeIfNone = true);

    /**
     * Extend a key over any existing segments forward in time
     *
     * @param key   the key to extend
     * @param test  the test to extend with (true replaces false)
     * @param extendBackToFirst if the first segment does not test true extend backwards to it as well
     */
    void extend(const Key &key, const ExtendTest &test, bool extendBackToFirst = true);


    /**
     * Get the current results of the fragment tracker with implied times.
     *
     * @return   the series of key to value maps for each segment
     */
    std::vector<Contents> getContents() const;

    using Fragments = std::vector<Result>;

    /**
     * Get the current results of the fragment tracker.
     *
     * @return  the series of fragment segments
     */
    Fragments getFragments() const;


    /**
     * A simple static value that always fragments but allows merging exactly equal contents.
     *
     * @tparam T    the data type
     */
    template<typename T>
    class StaticValue : public Value {
        T contents;
    public:
        StaticValue() = default;

        virtual ~StaticValue() = default;

        explicit StaticValue(const T &base) : contents(base)
        { }

        explicit StaticValue(T &&base) : contents(std::move(base))
        { }

        /**
         * Get the contents of the value.
         *
         * @return  the value contents
         */
        const T &value() const
        { return contents; }

        /**
         * Get the contents of the value.
         *
         * @return  the value contents
         */
        T &value()
        { return contents; }

        /**
         * Get the contents of the value
         *
         * @param handle    the value handle
         * @return          the value contents
         */
        static const T &value(const ValueHandle &handle)
        {
            Q_ASSERT(dynamic_cast<StaticValue<T> *>(handle.get()) != nullptr);
            return static_cast<const StaticValue<T> *>(handle.get())->value();
        }

        /**
         * Get the contents of the value
         *
         * @param handle    the value handle
         * @return          the value contents
         */
        static T &value(ValueHandle &handle)
        {
            Q_ASSERT(dynamic_cast<StaticValue<T> *>(handle.get()) != nullptr);
            return static_cast<StaticValue<T> *>(handle.get())->value();
        }

        /**
         * Get the contents from a general fragment contents.
         *
         * @param contents  the fragment contents
         * @param key       the fragment key
         * @param def       the default value if the key is not in the contents
         * @return          the value
         */
        static const T &value(const Contents &contents, const Key &key, const T &def = T())
        {
            auto found = contents.find(key);
            if (found == contents.end())
                return def;
            return value(found->second);
        }

        /**
         * Get the contents from a general fragment result.
         *
         * @param result    the fragment result
         * @param key       the fragment key
         * @param def       the default value if the key is not in the contents
         * @return          the value
         */
        static const T &value(const Result &result, const Key &key, const T &def = T())
        { return value(result.contents, key, def); }

        ValueHandle overlay(const ValueHandle &under, const ValueHandle &over) override
        {
            Q_ASSERT(dynamic_cast<StaticValue<T> *>(under.get()) != nullptr);
            Q_ASSERT(dynamic_cast<StaticValue<T> *>(over.get()) != nullptr);
            return std::make_shared<StaticValue<T>>(
                    static_cast<const StaticValue<T> *>(over.get())->contents);
        }

        /**
         * Get the tests used for merging identical values.
         *
         * @param key   the key used
         * @param mergeIntoEmpty merge into empty values
         * @return      the merge tests
         */
        static MergeTestList mergeTest(const Key &key, bool mergeIntoEmpty = true)
        {
            return {{[key](const FragmentTracker::Key &check,
                           FragmentTracker::Value *,
                           FragmentTracker::Value *) {
                return key == check;
            }, [mergeIntoEmpty](const FragmentTracker::Key &,
                                FragmentTracker::Value *first,
                                FragmentTracker::Value *second,
                                const FragmentTracker::MergeTestContext &context) {
                if (!first || !second)
                    return mergeIntoEmpty;
                Q_ASSERT(dynamic_cast<StaticValue<T> *>(first) != nullptr);
                Q_ASSERT(dynamic_cast<StaticValue<T> *>(second) != nullptr);
                return static_cast<const StaticValue<T> *>(first)->contents ==
                        static_cast<const StaticValue<T> *>(second)->contents;
            }}};
        }

        /**
         * Perform extension on a tracker that always fills empty values.
         *
         * @param tracker   the tracker
         * @param key       the key to extend
         */
        static void extendOverEmpty(FragmentTracker &tracker, const Key &key)
        {
            tracker.extend(key, [](FragmentTracker::Value *,
                                   const FragmentTracker::ExtendTestContext &) { return true; });
        }
    };
};

}
}

#endif //CPD3FILEOUTPUT_FRAGMENT_HXX
