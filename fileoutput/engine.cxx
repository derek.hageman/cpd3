/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLoggingCategory>

#include "engine.hxx"
#include "datacore/sinkmultiplexer.hxx"
#include "datacore/archive/access.hxx"

Q_LOGGING_CATEGORY(log_fileoutput_engine, "cpd3.fileoutput.engine", QtWarningMsg)

using namespace CPD3::Data;

namespace CPD3 {
namespace Output {

namespace {
class AdditionalMultiplexer : public ProcessingStage {
    SinkMultiplexer mux;
    Archive::Access archive;

    SinkMultiplexer::Sink *input;
    SinkMultiplexer::Sink *inject;

public:
    explicit AdditionalMultiplexer(const Archive::Selection::List &selections)
    {
        input = mux.createSink();
        inject = mux.createSink();
        mux.sinkCreationComplete();

        archive.readStream(selections, inject)->detach();

        mux.finished.connect([this] { finished(); });
    }

    void incomingData(const SequenceValue::Transfer &values) override
    { return input->incomingData(values); }

    void incomingData(SequenceValue::Transfer &&values) override
    { return input->incomingData(std::move(values)); }

    void incomingData(const SequenceValue &value) override
    { return input->incomingData(value); }

    void incomingData(SequenceValue &&value) override
    { return input->incomingData(std::move(value)); }

    void endData() override
    { return input->endData(); }

    void setEgress(StreamSink *egress) override
    { return mux.setEgress(egress); }


    bool isFinished() override
    { return mux.isFinished(); }

    bool wait(double timeout = FP::undefined()) override
    { return mux.wait(timeout); }

    void signalTerminate() override
    {
        archive.signalTerminate();
        mux.signalTerminate(true);
    }

    void start() override
    { return mux.start(); }
};
}

bool Engine::configurePipelineInput(const Variant::Read &processing,
                                    const SequenceName::Component &station,
                                    StreamPipeline *target,
                                    Archive::Selection::List selections,
                                    const Time::Bounds &clip)
{
    if (!processing.exists()) {
        for (auto &selection : selections) {
            if (Range::compareStart(selection.start, clip.start) < 0)
                selection.start = clip.start;
            if (Range::compareEnd(selection.end, clip.end) > 0)
                selection.end = clip.end;
        }
        return target->setInputArchive(selections, clip);
    }

    auto inputConfig = processing["Input"];
    if (inputConfig.exists()) {
        auto componentName = inputConfig["Name"].toQString();
        if (componentName.isEmpty()) {
            qCWarning(log_fileoutput_engine) << "Empty input component name";
            return false;
        }

        auto component = ComponentLoader::create(componentName);
        if (!component) {
            qCWarning(log_fileoutput_engine) << "Can't load component" << componentName;
            return false;
        }

        if (ExternalConverterComponent
                *ingressComponent = qobject_cast<ExternalConverterComponent *>(component)) {
            ComponentOptions options(ingressComponent->getOptions());
            ValueOptionParse::parse(options, inputConfig["Options"]);

            std::unique_ptr<ExternalConverter>
                    inputComponent(ingressComponent->createDataIngress(options));

            if (ingressComponent->requiresInputDevice()) {
                auto inputFileName = inputConfig["File"].toQString();
                if (inputFileName.isEmpty()) {
                    qCWarning(log_fileoutput_engine) << "Component" << componentName
                                                     << "requires an input file but none was specified";
                    return false;
                }
                if (!QFile::exists(inputFileName)) {
                    qCWarning(log_fileoutput_engine) << "Input file" << inputFileName
                                                     << "does not exist for component"
                                                     << componentName;
                    return false;
                }

                if (!target->setInputGeneral(std::move(inputComponent), inputFileName))
                    return false;
            } else {
                if (!target->setInputGeneral(std::move(inputComponent)))
                    return false;
            }
        } else if (ExternalSourceComponent
                *ingressComponentExternal = qobject_cast<ExternalSourceComponent *>(component)) {
            std::vector<SequenceName::Component> effectiveStations{station};
            if (static_cast<int>(effectiveStations.size()) >
                    ingressComponentExternal->ingressAllowStations()) {
                effectiveStations.clear();
            }
            auto vCheck = inputConfig["Station"];
            if (vCheck.getType() == Variant::Type::String) {
                effectiveStations.clear();
                for (const auto &add : vCheck.toQString()
                                             .toLower()
                                             .split(QRegExp("\\s+"), QString::SkipEmptyParts)) {
                    effectiveStations.emplace_back(add.toStdString());
                }
            } else if (vCheck.exists()) {
                for (const auto &add : vCheck.toChildren().keys()) {
                    effectiveStations.push_back(
                            QString::fromStdString(add).toLower().toStdString());
                }
            }
            if (static_cast<int>(effectiveStations.size()) <
                    ingressComponentExternal->ingressRequireStations()) {
                qCWarning(log_fileoutput_engine) << "Component" << componentName << "requires"
                                                 << ingressComponentExternal->ingressRequireStations()
                                                 << "station(s), but only"
                                                 << effectiveStations.size() << "are available";
                return false;
            }
            if (static_cast<int>(effectiveStations.size()) >
                    ingressComponentExternal->ingressAllowStations()) {
                qCWarning(log_fileoutput_engine) << "Component" << componentName << "permits"
                                                 << ingressComponentExternal->ingressRequireStations()
                                                 << "station(s), but" << effectiveStations.size()
                                                 << "are specified";
                return false;
            }

            bool provideTimes = !inputConfig["IgnoreTimes"].toBool();
            double effectiveStart = clip.start;
            double effectiveEnd = clip.end;
            vCheck = inputConfig["Start"];
            if (vCheck.getType() == Variant::Type::Real) {
                effectiveStart = vCheck.toDouble();
            }
            vCheck = inputConfig["End"];
            if (vCheck.getType() == Variant::Type::Real) {
                effectiveEnd = vCheck.toDouble();
            }
            if (!ingressComponentExternal->ingressAcceptsUndefinedBounds() &&
                    (!FP::defined(effectiveStart) || !FP::defined(effectiveEnd))) {
                provideTimes = false;
            }
            if (!provideTimes && ingressComponentExternal->ingressRequiresTime()) {
                qCWarning(log_fileoutput_engine) << "Component" << componentName
                                                 << "requires time bounds but none are available";
                return false;
            }

            ComponentOptions options(ingressComponentExternal->getOptions());
            ValueOptionParse::parse(options, inputConfig["Options"]);
            std::unique_ptr<ExternalConverter> inputComponent;
            if (provideTimes) {
                inputComponent.reset(
                        ingressComponentExternal->createExternalIngress(options, effectiveStart,
                                                                        effectiveEnd,
                                                                        effectiveStations));
            } else {
                inputComponent.reset(ingressComponentExternal->createExternalIngress(options,
                                                                                     effectiveStations));
            }

            if (!target->setInputGeneral(std::move(inputComponent)))
                return false;
        } else {
            qCWarning(log_fileoutput_engine) << "Component" << componentName << "is not an ingress";
            return false;
        }

        if (processing["Additional"].exists()) {
            SequenceMatch::Composite baseSelection(processing["Additional"]);
            auto additionalSelections = baseSelection.toArchiveSelections({station});
            for (auto &selection : additionalSelections) {
                if (Range::compareStart(selection.start, clip.start) < 0)
                    selection.start = clip.start;
                if (Range::compareEnd(selection.end, clip.end) > 0)
                    selection.end = clip.end;
            }
            if (!target->addProcessingStage(std::unique_ptr<ProcessingStage>(
                    new AdditionalMultiplexer(additionalSelections)))) {
                qCWarning(log_fileoutput_engine) << "Additional selection injection failed:"
                                                 << target->getChainError();
                return false;
            }
        }
    } else {
        if (processing["Additional"].exists()) {
            SequenceMatch::Composite selection(processing["Additional"]);
            Util::append(selection.toArchiveSelections({station}), selections);
        }

        for (auto &selection : selections) {
            if (Range::compareStart(selection.start, clip.start) < 0)
                selection.start = clip.start;
            if (Range::compareEnd(selection.end, clip.end) > 0)
                selection.end = clip.end;
        }

        if (!target->setInputArchive(selections, clip)) {
            qWarning(log_fileoutput_engine) << "Failed to set archive input:"
                                            << target->getInputError();
            return false;
        }
    }

    for (const auto &componentConfig : processing["Components"].toArray()) {
        auto componentName = componentConfig["Name"].toQString();
        if (componentName.isEmpty()) {
            qCWarning(log_fileoutput_engine) << "Empty component name";
            return false;
        }

        QObject *component = ComponentLoader::create(componentName);
        if (!component) {
            qCWarning(log_fileoutput_engine) << "Can't load component" << componentName;
            return false;
        }
        ProcessingStageComponent *processingComponent;
        if (!(processingComponent = qobject_cast<ProcessingStageComponent *>(component))) {
            qCWarning(log_fileoutput_engine) << "Component" << componentName
                                             << "is not a processing stage";
            return false;
        }

        ComponentOptions options(processingComponent->getOptions());
        ValueOptionParse::parse(options, componentConfig["Options"]);
        std::unique_ptr<ProcessingStage>
                processingStage(processingComponent->createGeneralFilterDynamic(options));
        if (!processingStage) {
            qCWarning(log_fileoutput_engine) << "Can't create processing stage for"
                                             << componentName;
            return false;
        }

        if (!target->addProcessingStage(std::move(processingStage), processingComponent)) {
            qCWarning(log_fileoutput_engine) << "Failed to add processing stage:"
                                             << target->getChainError();
            return false;
        }
    }

    return true;
}

Engine::Engine(std::unique_ptr<CPD3::Output::Engine::Stage> &&stage) : stage(std::move(stage)),
                                                                       terminateRequested(false),
                                                                       completed(false),
                                                                       incomingHandler(*this),
                                                                       filesUpdated(false),
                                                                       pendingEnd(false)
{
    Q_ASSERT(this->stage.get() != nullptr);
}

Engine::~Engine()
{
    signalTerminate();
    if (thread.joinable())
        thread.join();
}

void Engine::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminateRequested = true;
    }
    external.notify_all();
}

bool Engine::startFirstStage(Stage *)
{ return true; }

void Engine::firstStageComplete(Stage *)
{ }

void Engine::firstStageAborted(Stage *)
{ }

void Engine::processingAborted()
{
    if (outputWriter) {
        outputWriter->signalTerminate();
        outputWriter.reset();
    }
    if (inputReader) {
        inputReader->stallRequested.disconnect();
    }
    if (inputStream) {
        inputStream->read.disconnect();
        inputStream->ended.disconnect();
        inputStream.reset();
    }
    if (inputReader) {
        inputReader->signalTerminate();
        inputReader->wait();
        inputReader.reset();
    }
    inputFile.reset();
    outputFile.reset();

    qCDebug(log_fileoutput_engine) << "Processing aborted";

    {
        std::lock_guard<std::mutex> lock(mutex);
        completed = true;
    }
    internal.notify_all();
    finished();
}

void Engine::run()
{
    if (!prepareStage())
        return processingAborted();

    if (!startFirstStage(stage.get())) {
        qCDebug(log_fileoutput_engine) << "First stage start failed";
        return processingAborted();
    }
    if (!runStage()) {
        firstStageAborted(stage.get());
        return processingAborted();
    }
    firstStageComplete(stage.get());

    while (advanceStage()) {
        if (!prepareStage())
            return processingAborted();
        if (!runStage())
            return processingAborted();
    }

    outputWriter.reset();
    if (inputReader) {
        inputReader->stallRequested.disconnect();
    }
    inputStream.reset();
    inputReader.reset();
    inputFile.reset();
    outputFile.reset();

    {
        std::lock_guard<std::mutex> lock(mutex);
        completed = true;
    }
    internal.notify_all();
    finished();
}

bool Engine::prepareStage()
{
    /* If we have an input file, we're on the subsequent stages */
    if (inputFile) {
        inputStream = inputFile->stream();
        if (!inputStream) {
            qCWarning(log_fileoutput_engine) << "Failed to create input file stream";
            return false;
        }

        inputReader.reset(new StandardDataInput);
        inputReader->stallRequested
                   .connect(std::bind(&IO::Generic::Stream::readStall, inputStream.get(),
                                      std::placeholders::_1));
        inputReader->finished.connect([this] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                filesUpdated = true;
            }
            external.notify_all();
        });
        inputReader->start();
        inputReader->setEgress(&incomingHandler);

        inputStream->read
                   .connect(std::bind(
                           static_cast<void (StandardDataInput::*)(const Util::ByteArray &)>(&StandardDataInput::incomingData),
                           inputReader.get(), std::placeholders::_1));
        inputStream->ended.connect(std::bind(&StandardDataInput::endData, inputReader.get()));
        inputStream->start();

        if (!stage->isFinalStage()) {
            if (stage->retainModifiedData()) {
                outputFile = IO::Access::temporaryFile();
                if (!outputFile) {
                    qCWarning(log_fileoutput_engine) << "Failed to create temporary output file";
                    return false;
                }

                auto outputStream = outputFile->stream();
                if (!outputStream) {
                    qCWarning(log_fileoutput_engine)
                        << "Failed to create temporary output file stream";
                    return false;
                }

                outputWriter.reset(new StandardDataOutput(std::move(outputStream),
                                                          StandardDataOutput::OutputType::Raw));
                outputWriter->finished.connect([this] {
                    {
                        std::lock_guard<std::mutex> lock(mutex);
                        filesUpdated = true;
                    }
                    external.notify_all();
                });
                outputWriter->start();

                qCDebug(log_fileoutput_engine) << "Writing stage output to temporary file";
            }

            qCDebug(log_fileoutput_engine) << "Re-reading input from temporary file of size"
                                           << inputFile->size();

            feedback.emitStage(QObject::tr("Analyzing data"),
                               QObject::tr("Data are being re-read to analyze further."), true);
        } else {
            qCDebug(log_fileoutput_engine) << "Reading final input from temporary file of size"
                                           << inputFile->size();

            feedback.emitStage(QObject::tr("Generating output"),
                               QObject::tr("Data are being read to create the final output."),
                               true);
        }

        return true;
    }

    /* First stage */

    if (!stage->isFinalStage()) {
        outputFile = IO::Access::temporaryFile();
        if (!outputFile) {
            qCWarning(log_fileoutput_engine) << "Failed to create temporary output file";
            return false;
        }

        auto outputStream = outputFile->stream();
        if (!outputStream) {
            qCWarning(log_fileoutput_engine) << "Failed to create temporary output file stream";
            return false;
        }

        outputWriter.reset(new StandardDataOutput(std::move(outputStream),
                                                  StandardDataOutput::OutputType::Raw));
        outputWriter->finished.connect([this] {
            {
                std::lock_guard<std::mutex> lock(mutex);
                filesUpdated = true;
            }
            external.notify_all();
        });
        outputWriter->start();

        qCDebug(log_fileoutput_engine) << "Writing stage output to temporary file";

        feedback.emitStage(QObject::tr("Reading data"),
                           QObject::tr("Waiting for all data to be read."), true);

    } else {
        qCDebug(log_fileoutput_engine) << "Starting final stage, no output write enabled";

        feedback.emitStage(QObject::tr("Generating output"),
                           QObject::tr("Data are being read to create the final output."), true);
    }

    return true;
}

bool Engine::advanceStage()
{
    if (inputReader) {
        inputReader->stallRequested.disconnect();
    }
    inputStream.reset();
    inputReader.reset();
    outputWriter.reset();

    if (outputFile) {
        inputFile = std::move(outputFile);
        outputFile.reset();
    }

    stage->finalizeData();

    stage = stage->getNextStage();
    if (!stage) {
        qCDebug(log_fileoutput_engine) << "Stage advance completed";
        return false;
    }

    if (!inputFile) {
        qCCritical(log_fileoutput_engine) << "Stage advance requested, but no input available";
        return false;
    }

    return true;
}

bool Engine::runStage()
{
    bool receivedEnd = false;
    for (;;) {
        std::unique_lock<std::mutex> lock(mutex);
        for (;;) {
            if (terminateRequested) {
                qCDebug(log_fileoutput_engine) << "Terminating stage processing";
                return false;
            }
            if (filesUpdated)
                break;
            if (pendingEnd || !pendingValues.empty())
                break;
            external.wait(lock);
        }

        filesUpdated = false;

        bool processEnd = pendingEnd;
        pendingEnd = false;
        SequenceValue::Transfer processValues = std::move(pendingValues);
        pendingValues.clear();

        lock.unlock();


        if (inputReader && inputReader->isFinished()) {
            inputReader->stallRequested.disconnect();
            inputStream.reset();
            inputReader.reset();

            qCDebug(log_fileoutput_engine) << "Input reader finalized";
        }

        if (!processValues.empty()) {
            feedback.emitProgressTime(processValues.back().getStart());

            if (processValues.size() >= StreamSink::stallThreshold) {
                internal.notify_all();
            }
            stage->processData(std::move(processValues));
        }

        if (processEnd && !receivedEnd) {
            receivedEnd = true;

            qCDebug(log_fileoutput_engine) << "Input data ended";

            if (outputWriter) {
                outputWriter->endData();
            }
        }

        if (receivedEnd && outputWriter && outputWriter->isFinished()) {
            outputWriter.reset();

            qCDebug(log_fileoutput_engine) << "Output writer finalized";
        }

        if (receivedEnd && !inputReader && !outputWriter)
            return true;
    }
}

void Engine::start()
{
    thread = std::thread(std::bind(&Engine::run, this));
}

bool Engine::isFinished()
{
    std::unique_lock<std::mutex> lock(mutex);
    return completed;
}

bool Engine::wait(double timeout)
{ return Threading::waitForTimeout(timeout, mutex, internal, [this] { return completed; }); }

Engine::IncomingHandler::IncomingHandler(Engine &engine) : engine(engine)
{ }

Engine::IncomingHandler::~IncomingHandler() = default;

void Engine::IncomingHandler::incomingData(const SequenceValue::Transfer &values)
{
    SequenceValue::Transfer add = values;
    engine.stage->modifyData(add);
    if (add.empty())
        return;
    /*
     * Since the end is processed by the main thread, the writer is not modified
     * by anything else, so this is safe.
     */
    if (engine.outputWriter) {
        engine.outputWriter->incomingData(add);
    }

    std::unique_lock<std::mutex> lock(engine.mutex);
    stall(lock);
    Util::append(std::move(add), engine.pendingValues);
    lock.unlock();
    engine.external.notify_all();
}

void Engine::IncomingHandler::incomingData(SequenceValue::Transfer &&values)
{
    engine.stage->modifyData(values);
    if (values.empty())
        return;

    if (engine.outputWriter) {
        engine.outputWriter->incomingData(values);
    }

    std::unique_lock<std::mutex> lock(engine.mutex);
    stall(lock);
    Util::append(std::move(values), engine.pendingValues);
    lock.unlock();
    engine.external.notify_all();
}

void Engine::IncomingHandler::incomingData(const SequenceValue &value)
{ return incomingData(SequenceValue::Transfer{value}); }

void Engine::IncomingHandler::incomingData(SequenceValue &&value)
{ return incomingData(SequenceValue::Transfer{std::move(value)}); }

void Engine::IncomingHandler::endData()
{
    std::unique_lock<std::mutex> lock(engine.mutex);
    if (engine.pendingEnd)
        return;
    engine.pendingEnd = true;
    engine.external.notify_all();
}

void Engine::IncomingHandler::stall(std::unique_lock<std::mutex> &lock)
{
    while (engine.pendingValues.size() > stallThreshold) {
        if (engine.terminateRequested)
            return;
        engine.internal.wait(lock);
    }
}


Engine::Stage::Stage() = default;

Engine::Stage::~Stage() = default;

bool Engine::Stage::isFinalStage() const
{ return true; }

bool Engine::Stage::retainModifiedData() const
{ return false; }

std::unique_ptr<Engine::Stage> Engine::Stage::getNextStage()
{ return {}; }

void Engine::Stage::modifyData(CPD3::Data::SequenceValue::Transfer &)
{ }

void Engine::Stage::finalizeData()
{ }


Engine::SinkFanoutStage::SinkFanoutStage(const std::vector<StreamSink *> &stages) : first(stages)
{
    Q_ASSERT(!first.empty());
    last = first.back();
    first.pop_back();
}

Engine::SinkFanoutStage::SinkFanoutStage(std::vector<StreamSink *> &&stages) : first(
        std::move(stages))
{
    Q_ASSERT(!first.empty());
    last = first.back();
    first.pop_back();
}

Engine::SinkFanoutStage::~SinkFanoutStage() = default;

void Engine::SinkFanoutStage::processData(SequenceValue::Transfer &&values)
{
    for (auto f : first) {
        f->incomingData(values);
    }
    last->incomingData(std::move(values));
}

void Engine::SinkFanoutStage::finalizeData()
{
    for (auto f : first) {
        f->endData();
    }
    last->endData();
}

Engine::SinkDispatchStage::SinkDispatchStage() = default;

Engine::SinkDispatchStage::~SinkDispatchStage() = default;

void Engine::SinkDispatchStage::processData(SequenceValue::Transfer &&values)
{
    for (auto &v : values) {
        auto t = dispatch.find(v.getName());
        if (t == dispatch.end()) {
            t = dispatch.emplace(v.getName(), Target(getTargets(v.getName()))).first;
        }

        for (auto f : t->second.first) {
            f->incomingData(v);
        }
        if (t->second.last) {
            t->second.last->incomingData(std::move(v));
        }
    }
}

void Engine::SinkDispatchStage::finalizeData()
{
    for (const auto &t : dispatch) {
        for (auto f : t.second.first) {
            f->endData();
        }
        if (t.second.last) {
            t.second.last->endData();
        }
    }
}

Engine::SinkDispatchStage::Target::Target(std::vector<StreamSink *> &&targets) : first(
        std::move(targets))
{
    if (!first.empty()) {
        last = first.back();
        first.pop_back();
    } else {
        last = nullptr;
    }
}

Engine::SegmentStage::SegmentStage() = default;

Engine::SegmentStage::~SegmentStage() = default;

void Engine::SegmentStage::processData(SequenceValue::Transfer &&values)
{
    for (auto &v : values) {
        for (auto &s : integrateValue(std::move(v))) {
            processSegment(std::move(s));
        }
    }
}

void Engine::SegmentStage::finalizeData()
{
    for (auto &s : stream.finish()) {
        processSegment(std::move(s));
    }
}

SequenceSegment::Transfer Engine::SegmentStage::integrateValue(SequenceValue &&value)
{ return stream.add(std::move(value)); }

void Engine::ClipSinkWrapper::incomingData(const SequenceValue::Transfer &values)
{
    if (values.empty())
        return;
    if (Range::compareStartEnd(values.front().getStart(), clip.getEnd()) >= 0)
        return;

    for (auto check = values.cbegin(), endCheck = values.cend(); check != endCheck; ++check) {
        if (Range::compareEnd(check->getEnd(), clip.getEnd()) <= 0 &&
                Range::compareStart(check->getStart(), clip.getStart()) >= 0)
            continue;

        SequenceValue::Transfer result;
        std::copy(values.cbegin(), check, Util::back_emplacer(result));

        for (; check != endCheck; ++check) {
            if (Range::compareStartEnd(check->getStart(), clip.getEnd()) >= 0)
                break;
            if (Range::compareStartEnd(clip.getStart(), check->getEnd()) >= 0)
                continue;

            SequenceValue add = *check;
            if (Range::compareEnd(add.getEnd(), clip.end) > 0)
                add.setEnd(clip.end);
            if (Range::compareStart(add.getStart(), clip.start) < 0)
                add.setStart(clip.start);
            result.emplace_back(std::move(add));
        }

        sink->incomingData(std::move(result));
        return;
    }

    sink->incomingData(values);
}

void Engine::ClipSinkWrapper::incomingData(SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;
    if (Range::compareStartEnd(values.front().getStart(), clip.getEnd()) >= 0)
        return;

    for (auto check = values.begin(), endCheck = values.end(); check != endCheck; ++check) {
        if (Range::compareEnd(check->getEnd(), clip.getEnd()) <= 0 &&
                Range::compareStart(check->getStart(), clip.getStart()) >= 0)
            continue;

        for (; check != endCheck; ++check) {
            if (Range::compareStartEnd(check->getStart(), clip.getEnd()) >= 0) {
                values.erase(check, endCheck);
                break;
            }
            if (Range::compareStartEnd(clip.getStart(), check->getEnd()) >= 0) {
                auto endRemove = check;
                ++endRemove;
                for (; endRemove != endCheck; ++endRemove) {
                    if (Range::compareStartEnd(endRemove->getStart(), clip.getEnd()) >= 0) {
                        endRemove = endCheck;
                        break;
                    }
                    if (Range::compareStartEnd(clip.getStart(), endRemove->getEnd()) < 0)
                        break;
                }
                check = values.erase(check, endRemove);
                endCheck = values.end();
                continue;
            }

            if (Range::compareEnd(check->getEnd(), clip.end) > 0)
                check->setEnd(clip.end);
            if (Range::compareStart(check->getStart(), clip.start) < 0)
                check->setStart(clip.start);
        }

        sink->incomingData(std::move(values));
        return;
    }

    sink->incomingData(values);
}

void Engine::ClipSinkWrapper::incomingData(const SequenceValue &value)
{
    if (Range::compareStartEnd(value.getStart(), clip.getEnd()) >= 0)
        return;
    if (Range::compareStartEnd(clip.getStart(), value.getEnd()) >= 0)
        return;

    if (Range::compareEnd(value.getEnd(), clip.end) > 0) {
        SequenceValue add = value;
        add.setEnd(clip.end);
        if (Range::compareStart(add.getStart(), clip.start) < 0)
            add.setStart(clip.start);
        sink->incomingData(std::move(add));
        return;
    } else if (Range::compareStart(value.getStart(), clip.start) < 0) {
        SequenceValue add = value;
        add.setStart(clip.start);
        sink->incomingData(std::move(add));
        return;
    }

    sink->incomingData(value);
}

void Engine::ClipSinkWrapper::incomingData(SequenceValue &&value)
{
    if (Range::compareStartEnd(value.getStart(), clip.getEnd()) >= 0)
        return;
    if (Range::compareStartEnd(clip.getStart(), value.getEnd()) >= 0)
        return;

    if (Range::compareEnd(value.getEnd(), clip.end) > 0)
        value.setEnd(clip.end);
    if (Range::compareStart(value.getStart(), clip.start) < 0)
        value.setStart(clip.start);

    sink->incomingData(std::move(value));
}

void Engine::ClipSinkWrapper::endData()
{ sink->endData(); }

Engine::ClipSinkWrapper::ClipSinkWrapper(const Time::Bounds &clip, CPD3::Data::StreamSink *sink)
        : clip(clip), sink(sink)
{
    Q_ASSERT(sink);
}

Engine::ClipSinkWrapper::~ClipSinkWrapper() = default;

}
}