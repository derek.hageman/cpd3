/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#include "core/first.hxx"

#include <unordered_set>

#include "fragment.hxx"
#include "core/range.hxx"
#include "core/util.hxx"


namespace CPD3 {
namespace Output {

FragmentTracker::FragmentTracker() : segments(), nextKey(0)
{ }

FragmentTracker::~FragmentTracker() = default;

FragmentTracker::Key FragmentTracker::allocateKey()
{ return nextKey++; }

void FragmentTracker::add(Key key, const ValueHandle &value, double start, double end)
{ Range::overlayFragmenting(segments, Overlay(key, value, start, end)); }

void FragmentTracker::add(Key key, ValueHandle &&value, double start, double end)
{ Range::overlayFragmenting(segments, Overlay(key, std::move(value), start, end)); }

void FragmentTracker::addEmpty(double start, double end)
{ Range::overlayFragmenting(segments, Segment(start, end)); }

void FragmentTracker::merge(const MergeTest &canMerge)
{
    if (segments.size() < 2)
        return;

    for (auto first = segments.begin();;) {
        auto second = first;
        ++second;
        if (second == segments.end())
            break;

        if (!second->mergeCandidate(*first, canMerge)) {
            first = second;
            continue;
        }

        second->merge(*first);
        second->setStart(first->getStart());
        first = segments.erase(first);
    }
}

void FragmentTracker::merge(const MergeTestList &tests, bool mergeIfNone)
{
    return merge([&tests, mergeIfNone](const Key &key,
                                       Value *first,
                                       Value *second,
                                       const MergeTestContext &context) {
        for (const auto &test : tests) {
            if (test.first && !test.first(key, first, second))
                continue;
            if (!test.second(key, first, second, context))
                return false;
        }
        return mergeIfNone;
    });
}

void FragmentTracker::extend(const FragmentTracker::Key &key,
                             const ExtendTest &test,
                             bool extendBackToFirst)
{
    auto lastValid = segments.begin();
    auto end = segments.end();
    bool haveValid = false;
    for (auto check = lastValid; check != end; ++check) {
        auto &contents = check->getContents();
        auto candidate = contents.find(key);
        if (candidate == contents.end())
            continue;
        {
            ExtendTestContext context(*check);
            if (!test(candidate->second.get(), context))
                continue;
        }

        if (!haveValid) {
            if (extendBackToFirst) {
                for (; lastValid != check; ++lastValid) {
                    Util::insert_or_assign(lastValid->getContents(), candidate->first,
                                           candidate->second);
                }
            }
        } else {
            auto fillBegin = lastValid;
            ++fillBegin;
            if (fillBegin != check) {
                auto fill = lastValid->getContents().find(key);
                Q_ASSERT(fill != lastValid->getContents().end());
                for (; fillBegin != check; ++fillBegin) {
                    Util::insert_or_assign(fillBegin->getContents(), fill->first, fill->second);
                }
            }
        }

        haveValid = true;
        lastValid = check;
    }

    if (!haveValid)
        return;

    auto fillBegin = lastValid;
    ++fillBegin;
    if (fillBegin == end)
        return;

    auto fill = lastValid->getContents().find(key);
    Q_ASSERT(fill != lastValid->getContents().end());
    for (; fillBegin != end; ++fillBegin) {
        Util::insert_or_assign(fillBegin->getContents(), fill->first, fill->second);
    }
}

std::vector<FragmentTracker::Contents> FragmentTracker::getContents() const
{
    std::vector<Contents> result;
    for (const auto &add : segments) {
        result.emplace_back(add.getResult());
    }
    return result;
}

FragmentTracker::Fragments FragmentTracker::getFragments() const
{
    FragmentTracker::Fragments result;
    for (const auto &add : segments) {
        result.emplace_back(add.getResult(), add.getStart(), add.getEnd());
    }
    return result;
}

FragmentTracker::Result::Result(const Contents &contents, double start, double end) : Time::Bounds(
        start, end), contents(contents)
{ }

FragmentTracker::Result::Result(Contents &&contents, double start, double end) : Time::Bounds(start,
                                                                                              end),
                                                                                 contents(std::move(
                                                                                         contents))
{ }

FragmentTracker::Value::Value() = default;

FragmentTracker::Value::~Value() = default;

void FragmentTracker::Value::merge(Value *)
{ }

FragmentTracker::ValueHandle FragmentTracker::Value::toResult(const ValueHandle &self,
                                                              double,
                                                              double)
{ return self; }

FragmentTracker::Overlay::Overlay(FragmentTracker::Key key,
                                  const ValueHandle &value,
                                  double start,
                                  double end) : Time::Bounds(start, end),
                                                key(std::move(key)),
                                                value(value)
{ }

FragmentTracker::Overlay::Overlay(FragmentTracker::Key key,
                                  FragmentTracker::ValueHandle &&value,
                                  double start,
                                  double end) : Time::Bounds(start, end),
                                                key(std::move(key)),
                                                value(std::move(value))
{ }

FragmentTracker::Segment::Segment(const Overlay &over) : Time::Bounds(over)
{
    Util::insert_or_assign(values, over.key, over.value);
}

FragmentTracker::Segment::Segment(double start, double end) : Time::Bounds(start, end)
{ }

FragmentTracker::Segment::Segment(const Segment &other, double start, double end) : Time::Bounds(
        start, end), values(other.values)
{ }

FragmentTracker::Segment::Segment(const Segment &under,
                                  const Segment &over,
                                  double start,
                                  double end) : Time::Bounds(start, end), values(under.values)
{
    for (const auto &add : over.values) {
        auto check = values.find(add.first);
        if (check == values.end()) {
            values.emplace(add.first, add.second);
            continue;
        }

        check->second = check->second->overlay(check->second, add.second);
    }
}

FragmentTracker::Segment::Segment(const Overlay &other, double start, double end) : Time::Bounds(
        start, end)
{ Util::insert_or_assign(values, other.key, other.value); }

FragmentTracker::Segment::Segment(const Segment &under,
                                  const Overlay &over,
                                  double start,
                                  double end) : Time::Bounds(start, end), values(under.values)
{
    auto check = values.find(over.key);
    if (check == values.end()) {
        values.emplace(over.key, over.value);
        return;
    }

    check->second = check->second->overlay(check->second, over.value);
}

bool FragmentTracker::Segment::mergeCandidate(const Segment &first, const MergeTest &canMerge)
{
    MergeTestContext context(first, *this);
    for (const auto &sv : values) {
        auto fv = first.values.find(sv.first);
        if (fv == first.values.end()) {
            if (!canMerge(sv.first, nullptr, sv.second.get(), context))
                return false;
        } else {
            if (!canMerge(sv.first, fv->second.get(), sv.second.get(), context))
                return false;
        }
    }
    for (const auto &fv : first.values) {
        /* Values in both are checked above */
        if (values.count(fv.first))
            continue;
        if (!canMerge(fv.first, fv.second.get(), nullptr, context))
            return false;
    }
    return true;
}

void FragmentTracker::Segment::merge(const Segment &first)
{
    for (auto &sv : values) {
        auto fv = first.values.find(sv.first);
        if (fv == first.values.end()) {
            continue;
        }
        sv.second->merge(fv->second.get());
    }
    for (const auto &fv : first.values) {
        /* Handled above */
        if (values.count(fv.first))
            continue;
        values.emplace(fv);
    }
}

FragmentTracker::Contents FragmentTracker::Segment::getResult() const
{
    Contents result;
    for (const auto &add : values) {
        result.emplace(add.first, add.second->toResult(add.second, getStart(), getEnd()));
    }
    return result;
}


FragmentTracker::MergeTestContext::MergeTestContext(const Segment &first, const Segment &second)
        : firstFragment(first),
          firstContents(first.getContents()),
          secondFragment(second),
          secondContents(second.getContents())
{ }

FragmentTracker::ExtendTestContext::ExtendTestContext(const Segment &segment) : fragment(segment),
                                                                                contents(
                                                                                        segment.getContents())
{ }

}
}