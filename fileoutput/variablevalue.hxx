/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CPD3FILEOUTPUT_VARIABLEVALUE_HXX
#define CPD3FILEOUTPUT_VARIABLEVALUE_HXX

#include "core/first.hxx"

#include <memory>
#include <string>
#include <vector>
#include <unordered_map>

#include "fileoutput.hxx"
#include "fragment.hxx"
#include "core/number.hxx"
#include "core/timeutils.hxx"
#include "core/textsubstitution.hxx"
#include "datacore/segment.hxx"
#include "datacore/sequencematch.hxx"
#include "luascript/engine.hxx"

namespace CPD3 {
namespace Output {
namespace VariableValue {

/**
 * The base class for dynamic input values used in fragmentation.
 */
class CPD3FILEOUTPUT_EXPORT Base {
public:
    /**
     * An input from data to the dynamic value.
     */
    class CPD3FILEOUTPUT_EXPORT DataInput : public FragmentTracker::Value {
        friend class Base;

        CPD3::Data::SequenceMatch::OrderedLookup input;
        CPD3::Data::Variant::Path path;
        CPD3::Data::Variant::Root constant;
    public:
        /**
         * Create the input.
         *
         * @param config    the configuration segment
         */
        explicit DataInput(const CPD3::Data::Variant::Read &config);

        virtual ~DataInput();

        FragmentTracker::ValueHandle overlay(const FragmentTracker::ValueHandle &under,
                                             const FragmentTracker::ValueHandle &over) override;

        /**
         * Register an input name.
         *
         * @param input the name
         * @return      true if the input is used
         */
        bool registerInput(const CPD3::Data::SequenceName &input);

        /**
         * Convert the input to archive selections.
         *
         * @param defaultStations   the default stations
         * @param defaultArchives   the default archives
         * @param defaultVariables  the default variables
         * @return                  any archive selections used by the input
         */
        CPD3::Data::Archive::Selection::List toArchiveSelections(const CPD3::Data::Archive::Selection::Match &defaultStations = {},
                                                                 const CPD3::Data::Archive::Selection::Match &defaultArchives = {},
                                                                 const CPD3::Data::Archive::Selection::Match &defaultVariables = {}) const;

        /**
         * Process a segment from the data input.
         *
         * @param segment   the data segment
         * @return          the output value
         */
        CPD3::Data::Variant::Root processSegment(CPD3::Data::SequenceSegment &segment);

    protected:
        DataInput(const DataInput &);

        /**
         * Fetch the value from within an input.
         * <br>
         * The default implementation looks up the path from the input.
         *
         * @param input     the input value
         * @return          the output value
         */
        virtual CPD3::Data::Variant::Read fetch(const CPD3::Data::Variant::Read &input);

        /**
         * Convert the input value to an output.
         * <br>
         * The default implementation just returns the input
         *
         * @param input     the input value
         * @return          the output value
         */
        virtual CPD3::Data::Variant::Root convert(const CPD3::Data::Variant::Read &input);
    };

private:
    class TargetControl : public Time::Bounds {
    public:
        std::shared_ptr<DataInput> input;

        TargetControl(std::shared_ptr<DataInput> input, double start, double end);

        ~TargetControl();
    };

    std::unique_ptr<FragmentTracker> configureTracker;
    FragmentTracker::Key configureDataKey;

    FragmentTracker &outputTracker;
    FragmentTracker::Key outputKey;

    class Sink : public CPD3::Data::StreamSink {
        Base &parent;
        CPD3::Data::SequenceSegment::Stream stream;

        void processSegment(CPD3::Data::SequenceSegment &&segment);

    public:
        std::deque<TargetControl> control;

        explicit Sink(Base &parent);

        virtual ~Sink();

        void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

        void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

        void incomingData(const CPD3::Data::SequenceValue &value) override;

        void incomingData(CPD3::Data::SequenceValue &&value) override;

        void endData() override;
    };

    friend class Sink;

    Sink sink;

public:
    /**
     * Create the value.
     *
     * @param tracker   the output fragment tracker
     */
    explicit Base(FragmentTracker &tracker);

    ~Base();

    /**
     * Add an input to to the dynamic value.
     *
     * @param value     the input value
     * @param start     the effective start time
     * @param end       the effective end time
     */
    void addInput(const std::shared_ptr<DataInput> &value,
                  double start = CPD3::FP::undefined(),
                  double end = CPD3::FP::undefined());

    void addInput(std::shared_ptr<DataInput> &&value,
                  double start = CPD3::FP::undefined(),
                  double end = CPD3::FP::undefined());

    /**
     * End configuration of the value.
     */
    void endConfigure();

    /**
     * Convert the input to archive selections.
     *
     * @param defaultStations   the default stations
     * @param defaultArchives   the default archives
     * @param defaultVariables  the default variables
     * @return                  any archive selections used by the input
     */
    CPD3::Data::Archive::Selection::List toArchiveSelections(const CPD3::Data::Archive::Selection::Match &defaultStations = {},
                                                             const CPD3::Data::Archive::Selection::Match &defaultArchives = {},
                                                             const CPD3::Data::Archive::Selection::Match &defaultVariables = {}) const;

    /**
     * Get any targets used by the value for a given name.
     *
     * @param name  the name
     * @return      any stream targets
     */
    std::vector<CPD3::Data::StreamSink *> getTargets(const CPD3::Data::SequenceName &name);

    /**
     * Get the value given fragment contents.
     *
     * @param contents  the fragment contents
     * @return          the value
     */
    CPD3::Data::Variant::Read get(const FragmentTracker::Contents &contents) const;

    /**
     * Get the output key used by the value.
     *
     * @return  the output key
     */
    inline FragmentTracker::Key getKey() const
    { return outputKey; }

    /**
     * Extend the value in the tracker, filling undefined segments.
     */
    void extendDefined();

    /**
     * Extend the value in the tracker, filling undefined segments.
     *
     * @param test  the test for if a value is undefined
     */
    void extendDefined(const std::function<bool(const CPD3::Data::Variant::Read &value,
                                                const FragmentTracker::Contents &contents)> &test);
};


/**
 * A string output value.
 */
class CPD3FILEOUTPUT_EXPORT String : public Base {
    class Sub : public TextSubstitution {
        const String &parent;
        const FragmentTracker::Contents &contents;
    public:
        Sub(const String &parent, const FragmentTracker::Contents &contents);

        virtual ~Sub();

    protected:
        QString substitution(const QStringList &elements) const override;

        QString literalCheck(QStringRef &key) const override;

        QString literalSubstitution(const QStringRef &key, const QString &close) const override;
    };

    friend class Sub;

public:
    explicit String(FragmentTracker &tracker);

    ~String();

    /**
     * Get the string value, applying any substitutions.
     *
     * @param contents  the fragment contents
     * @param input     the input string
     * @return          the final output string
     */
    QString toString(const FragmentTracker::Contents &contents, const QString &input) const;

    /**
     * Get the string value.
     *
     * @param contents  the fragment contents
     * @return          the final output string
     */
    QString toString(const FragmentTracker::Contents &contents) const;

    /**
     * Get the tests used for merging identical string results.
     *
     * @return  the merge tests
     */
    FragmentTracker::MergeTestList mergeTest() const;

protected:
    /**
     * Get a script frame used for substitution evaluation.
     *
     * @param contents  the fragment contents
     * @return  a script frame created by the implementation
     */
    virtual std::unique_ptr<
            CPD3::Lua::Engine::Frame> scriptFrame(const FragmentTracker::Contents &contents) const;

    /**
     * Apply a substitution from the element list.
     *
     * @param elements
     * @param contents
     * @return
     */
    virtual QString applySubstitution(const QStringList &elements,
                                      const FragmentTracker::Contents &contents) const;
};

/**
 * A real number output value.
 */
class CPD3FILEOUTPUT_EXPORT Real : public Base {
public:
    /**
     * An input to a real number value.
     */
    class CPD3FILEOUTPUT_EXPORT RealInput : public Base::DataInput {
        Calibration cal;
    public:
        explicit RealInput(const CPD3::Data::Variant::Read &config);

        virtual ~RealInput();

    protected:
        RealInput(const RealInput &);

        CPD3::Data::Variant::Root convert(const CPD3::Data::Variant::Read &input) override;
    };

    explicit Real(FragmentTracker &tracker);

    ~Real();
};

}
}
}

#endif //CPD3FILEOUTPUT_VARIABLEVALUE_HXX
