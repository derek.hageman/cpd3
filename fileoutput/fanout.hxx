/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CPD3FILEOUTPUt_FANOUT_HXX
#define CPD3FILEOUTPUt_FANOUT_HXX

#include "core/first.hxx"

#include "fileoutput.hxx"
#include "datacore/segment.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "luascript/engine.hxx"

namespace CPD3 {
namespace Output {

/**
 * A fanout stage controller.  This is used for multiple pass file creation where the
 * first pass determines the structure (input) then subsequent passes use those inputs
 * to actually construct the output.
 */
class CPD3FILEOUTPUT_EXPORT StagedFanout {
public:
    class OutputStage;

    /**
     * The base class for the input stage of the fanout controller.  This is what is created
     * during the initial analysis pass.  It can optionally receive segmented (meta)data
     * from this pass.
     */
    class CPD3FILEOUTPUT_EXPORT InputStage {
        StagedFanout &parent;
        std::unique_ptr<CPD3::Data::DynamicSequenceSelection> selection;

        friend class StagedFanout;

        friend class OutputStage;

    public:
        /**
         * Construct the input stage.
         *
         * @param parent    the parent controller
         */
        explicit InputStage(StagedFanout &parent);

        virtual ~InputStage();

        /**
         * Test if the input is active in a given time range.  This can be used to
         * disable the creation of an output when the input would never result in
         * anything.
         *
         * @param start the start time of the range
         * @param end   the end time of the range
         */
        bool isActive(double start, double end) const;

        /**
         * Process a data segment for this input.  The segment itself also includes metadata
         * if that metadata was squashed.
         *
         * @param segment the input data segment
         */
        virtual void processDataSegment(CPD3::Data::SequenceSegment &segment);

        /**
         * Process a metadata segment for this input.
         *
         * @param segment   the input metadata segment
         */
        virtual void processMetadataSegment(CPD3::Data::SequenceSegment &segment);

        /**
         * Get all input names to the input handler.
         *
         * @param start     the start time
         * @param end       the end time
         * @return          all direct inputs
         */
        CPD3::Data::SequenceName::Set get(double start, double end) const;

        /**
         * Retrieve a defined data value from the segment.
         *
         * @param segment   the data segment
         * @return          a defined data value
         */
        CPD3::Data::Variant::Read getData(CPD3::Data::SequenceSegment &segment) const;

        /**
         * Retrieve a defined metadata value from the segment.
         *
         * @param segment   the metadata segment
         * @return          a defined metadata value
         */
        CPD3::Data::Variant::Read getMetadata(CPD3::Data::SequenceSegment &segment) const;

        /** @see get(double, double) */
        template<typename T>
        inline CPD3::Data::SequenceName::Set get(const T &range) const
        { return get(range.getStart(), range.getEnd()); }
    };

    /**
     * The base class for outputs constructed from inputs to the fanout.  This provides
     * some access to the known/used inputs.  Once constructed, it is independent of the
     * parent input or fanout controller.
     */
    class CPD3FILEOUTPUT_EXPORT OutputStage {
        std::unique_ptr<CPD3::Data::DynamicSequenceSelection> selection;
        bool includeMetadata;
        bool includeStatistics;
    public:
        /**
         * Construct the output.
         *
         * @param origin    the base input
         * @param start     the start time the output is effective for
         * @param end       the end time the output is effective for
         */
        OutputStage(const InputStage &origin,
                    double start = FP::undefined(),
                    double end = FP::undefined());

        virtual ~OutputStage();

        /**
         * Get all used names to the output handler.
         *
         * @param start     the start time
         * @param end       the end time
         * @return          all directly used names
         */
        CPD3::Data::SequenceName::Set get(double start, double end);

        /**
         * Retrieve a defined data value from the segment.
         *
         * @param segment   the data segment
         * @return          a defined data value
         */
        CPD3::Data::Variant::Read getData(CPD3::Data::SequenceSegment &segment);

        /**
         * Retrieve a defined metadata value from the segment.
         *
         * @param segment   the metadata segment
         * @return          a defined metadata value
         */
        CPD3::Data::Variant::Read getMetadata(CPD3::Data::SequenceSegment &segment);

        /**
         * Test if an input name is possibly used by the output handler.  This is generally
         * used to create a dispatch to a segmenter for the output.
         *
         * @param name      the name to check
         * @return          true if the name is possibly used by the output
         */
        bool isPossiblyUsed(const CPD3::Data::SequenceName &name) const;

        /**
         * Test if an input name is directly used by the output handler.  This is generally
         * used to create a dispatch to a segmenter for the output.
         *
         * @param name      the name to check
         * @return          true if the name is directly used by the output
         */
        bool isDirectlyUsed(const CPD3::Data::SequenceName &name) const;

        /** @see get(double, double) */
        template<typename T>
        inline CPD3::Data::SequenceName::Set get(const T &range)
        { return get(range.getStart(), range.getEnd()); }
    };

private:
    std::unique_ptr<CPD3::Data::DynamicSequenceSelection> baseSelection;

    bool fanoutStation;
    bool fanoutArchive;
    bool fanoutVariable;
    bool fanoutFlavors;
    bool fanoutFlattenMetadata;
    bool fanoutFlattenStatistics;
    std::string fanoutTargetRemap;

    std::unique_ptr<CPD3::Data::DynamicSequenceSelection> inputStageSelection;
    CPD3::Data::SequenceName::Map<std::unique_ptr<InputStage>> inputStageTargets;
    CPD3::Data::SequenceSegment::Stream inputStageDataStream;
    CPD3::Data::SequenceSegment::Stream inputStageMetadataStream;

    void processDataSegments(CPD3::Data::SequenceSegment::Transfer &&segments);

    class DataSink : public CPD3::Data::StreamSink {
        StagedFanout &parent;
    public:
        explicit DataSink(StagedFanout &parent);

        virtual ~DataSink();

        void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

        void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

        void incomingData(const CPD3::Data::SequenceValue &value) override;

        void incomingData(CPD3::Data::SequenceValue &&value) override;

        void endData() override;
    };

    friend class DataSink;

    std::unique_ptr<DataSink> dataSink;


    void processMetadataSegments(CPD3::Data::SequenceSegment::Transfer &&segments);

    class MetadataSink : public CPD3::Data::StreamSink {
        StagedFanout &parent;
    public:
        explicit MetadataSink(StagedFanout &parent);

        virtual ~MetadataSink();

        void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

        void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

        void incomingData(const CPD3::Data::SequenceValue &value) override;

        void incomingData(CPD3::Data::SequenceValue &&value) override;

        void endData() override;
    };

    friend class MetadataSink;

    std::unique_ptr<MetadataSink> metadataSink;
public:
    /**
     * Create the fanout controller.
     *
     * @param config                the configuration
     * @param enableDataInput       enable data segment dispatch to the inputs
     * @param enableMetadataInput   enable metadata segment dispatch to the inputs
     */
    explicit StagedFanout(const CPD3::Data::ValueSegment::Transfer &config,
                          bool enableDataInput = false,
                          bool enableMetadataInput = true);

    virtual ~StagedFanout();

    /**
     * Convert the fanout to archive selections.
     *
     * @param defaultStations   the default stations
     * @param defaultArchives   the default archives
     * @param defaultVariables  the default variables
     * @return                  any archive selections used by the input
     */
    CPD3::Data::Archive::Selection::List toArchiveSelections(const CPD3::Data::Archive::Selection::Match &defaultStations = {},
                                                             const CPD3::Data::Archive::Selection::Match &defaultArchives = {},
                                                             const CPD3::Data::Archive::Selection::Match &defaultVariables = {}) const;

    /**
     * Get any targets used by the fanout for a given name.
     *
     * @param name  the name
     * @return      any stream targets
     */
    std::vector<CPD3::Data::StreamSink *> getTargets(const CPD3::Data::SequenceName &name);

    /**
     * Get all inputs used by the fanout.
     *
     * @return  all used inputs
     */
    std::vector<InputStage *> getInputs() const;

protected:
    /**
     * Create a new input target for fanout.
     *
     * @return  the input target
     */
    virtual std::unique_ptr<InputStage> createInput() = 0;

    /**
     * Get a script frame used for remapping evaluation.
     *
     * @param target    the target fanout name
     * @return          a script frame created by the implementation
     */
    virtual std::unique_ptr<
            CPD3::Lua::Engine::Frame> scriptFrame(const CPD3::Data::SequenceName &target) const;
};

}
}

#endif //CPD3FILEOUTPUt_FANOUT_HXX
