/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#ifdef BUILD_GUI

#include <QMessageBox>

#include "graphing/displaylayout.hxx"

#endif

#include <QRegularExpression>

#include "core/timeparse.hxx"
#include "core/component.hxx"
#include "core/number.hxx"
#include "datacore/archive/access.hxx"
#include "io/drivers/file.hxx"

#include "arguments.hxx"
#include "execthread.hxx"
#include "pipeline.hxx"

using namespace CPD3;

Arguments::Arguments(ExecThread *thread, const QStringList &commandArguments, bool usingGUI)
        : thread(thread), commandArguments(commandArguments), usingGUI(usingGUI), evaluated(false)
{ }

Arguments::~Arguments() = default;

void Arguments::evaluate(Lua::Engine::Frame &frame)
{
    if (evaluated)
        return;
    evaluated = true;

    CPD3::Data::Archive::Access::ReadLock archiveLock(thread->archive);

    std::vector<std::shared_ptr<Output>> operating;
    for (const auto &weak : outputs) {
        auto check = weak.lock();
        if (!check)
            continue;
        auto opt = dynamic_cast<Option *>(check.get());
        if (!opt)
            continue;
        if (!opt->option)
            continue;

        QString id = opt->option->getArgumentName();
        for (int counter = 1; outputOptions.get(id); ++counter) {
            id = opt->option->getArgumentName() + "-" + QString::number(counter);
        }
        outputOptions.add(id, opt->option.release());
        opt->optionID = std::move(id);
        operating.emplace_back(std::move(check));
    }

    bool ok = false;
#ifdef BUILD_GUI
    if (usingGUI) {
        ok = evaluateGUI();
    } else
#endif
    {
        ok = evaluateArguments();
    }

    if (ok) {
        for (const auto &out : operating) {
            out->ready = true;
        }
    }
    ready(frame);

    if (!ok) {
        frame.throwError("Error parsing arguments");
        thread->silentAbort();
        thread->signalTerminate();
    }
}

std::shared_ptr<Arguments::Option> Arguments::option(std::unique_ptr<ComponentOptionBase> &&opt)
{
    if (evaluated)
        return {};
    auto result = std::make_shared<Option>(*this, std::move(opt));
    outputs.emplace_back(result);
    return result;
}

std::shared_ptr<Arguments::Stations> Arguments::stations(const QString &name,
                                                         const QString &display,
                                                         const QString &description,
                                                         const QString &defaultBehavior)
{
    if (evaluated)
        return {};
    auto result = std::make_shared<Stations>(*this, name, display, description, defaultBehavior);
    outputs.emplace_back(result);
    return result;
}

std::shared_ptr<Arguments::TimePoint> Arguments::time(const QString &name, const QString &display,
                                                      const QString &description,
                                                      const QString &defaultBehavior)
{
    if (evaluated)
        return {};
    auto result = std::make_shared<TimePoint>(*this, name, display, description, defaultBehavior);
    outputs.emplace_back(result);
    return result;
}

std::shared_ptr<Arguments::Bounds> Arguments::bounds(const QString &name, const QString &display,
                                                     const QString &description,
                                                     const QString &defaultBehavior)
{
    if (evaluated)
        return {};
    auto result = std::make_shared<Bounds>(*this, name, display, description, defaultBehavior);
    outputs.emplace_back(result);
    return result;
}

std::shared_ptr<Arguments::ReadStream> Arguments::readStream(const QString &name,
                                                             const QString &display,
                                                             const QString &description)
{
    if (evaluated)
        return {};
    auto result = std::make_shared<ReadStream>(*this, name, display, description);
    outputs.emplace_back(result);
    return result;
}

std::shared_ptr<Arguments::WriteStream> Arguments::writeStream(const QString &name,
                                                               const QString &display,
                                                               const QString &description)
{
    if (evaluated)
        return {};
    auto result = std::make_shared<WriteStream>(*this, name, display, description);
    outputs.emplace_back(result);
    return result;
}

std::shared_ptr<Arguments::PipelineInput> Arguments::pipelineInput(LuaPipeline *origin,
                                                                   const QString &name,
                                                                   const QString &display)
{
    if (evaluated)
        return {};
    auto result = std::make_shared<PipelineInput>(*this, origin, name, display);
    outputs.emplace_back(result);
    return result;
}

std::shared_ptr<Arguments::PipelineOutput> Arguments::pipelineOutput(LuaPipeline *origin,
                                                                     const QString &name,
                                                                     const QString &display)
{
    if (evaluated)
        return {};
    auto result = std::make_shared<PipelineOutput>(*this, origin, name, display);
    outputs.emplace_back(result);
    return result;
}

std::shared_ptr<Arguments::PipelineExternalSinkOutput> Arguments::pipelineExternalSinkOutput(
        LuaPipeline *origin, const QString &name, const QString &display,
        CPD3::Data::ExternalSinkComponent *component,
        const CPD3::ComponentOptions &options)
{
    if (evaluated)
        return {};
    auto result =
            std::make_shared<PipelineExternalSinkOutput>(*this, origin, name, display, component,
                                                         options);
    outputs.emplace_back(result);
    return result;
}

std::shared_ptr<
        Arguments::PipelineDisplayOutput> Arguments::pipelineDisplayOutput(LuaPipeline *origin,
                                                                           const QString &name,
                                                                           const QString &display,
                                                                           CPD3::Data::ValueSegment::Transfer config)
{
    if (evaluated)
        return {};
    auto result = std::make_shared<PipelineDisplayOutput>(*this, origin, name, display,
                                                          std::move(config));
    outputs.emplace_back(result);
    return result;
}

Arguments::Output::Output(Arguments &arguments) : arguments(arguments), ready(false)
{ }

Arguments::Output::~Output() = default;

bool Arguments::Output::fetch(Lua::Engine::Frame &frame)
{
    if (!ready)
        arguments.evaluate(frame);
    return ready;
}


Arguments::Option::Option(Arguments &arguments, std::unique_ptr<CPD3::ComponentOptionBase> &&option)
        : Output(arguments), option(std::move(option))
{ }

Arguments::Option::~Option() = default;

CPD3::ComponentOptionBase *Arguments::Option::retrieve(Lua::Engine::Frame &frame)
{
    if (!fetch(frame))
        return nullptr;
    if (optionID.isEmpty())
        return nullptr;
    return arguments.outputOptions.get(optionID);
}


Arguments::Stations::Stations(Arguments &arguments, const QString &name, const QString &display,
                              const QString &description,
                              const QString &defaultBehavior) : Output(arguments),
                                                                name(name),
                                                                display(display),
                                                                description(description),
                                                                defaultBehavior(defaultBehavior),
                                                                minimumRequired(1),
                                                                maximumAllowed(std::numeric_limits<
                                                                        std::size_t>::max())
{ }

Arguments::Stations::~Stations() = default;

CPD3::Data::SequenceName::ComponentSet Arguments::Stations::value(Lua::Engine::Frame &frame)
{
    if (!fetch(frame))
        return CPD3::Data::SequenceName::ComponentSet();
    return result;
}

Arguments::TimePoint::TimePoint(Arguments &arguments, const QString &name, const QString &display,
                                const QString &description,
                                const QString &defaultBehavior) : Output(arguments),
                                                                  name(name),
                                                                  display(display),
                                                                  description(description),
                                                                  defaultBehavior(defaultBehavior),
                                                                  allowUndefined(false),
                                                                  defaultUnit(CPD3::Time::Day),
                                                                  result(FP::undefined())
{ }

Arguments::TimePoint::~TimePoint() = default;

double Arguments::TimePoint::value(Lua::Engine::Frame &frame)
{
    if (!fetch(frame))
        return FP::undefined();
    return result;
}

Arguments::Bounds::Bounds(Arguments &arguments, const QString &name, const QString &display,
                          const QString &description,
                          const QString &defaultBehavior) : Output(arguments),
                                                            name(name),
                                                            display(display),
                                                            description(description),
                                                            defaultBehavior(defaultBehavior),
                                                            allowUndefined(true),
                                                            defaultUnit(CPD3::Time::Day)
{ }

Arguments::Bounds::~Bounds() = default;

CPD3::Time::Bounds Arguments::Bounds::value(Lua::Engine::Frame &frame)
{
    if (!fetch(frame))
        return CPD3::Time::Bounds();
    return result;
}

Arguments::StreamFile::StreamFile(Arguments &arguments, const QString &name, const QString &display,
                                  const QString &description) : Output(arguments),
                                                                name(name),
                                                                display(display),
                                                                description(description)
{ }

Arguments::StreamFile::~StreamFile() = default;

Arguments::ReadStream::ReadStream(Arguments &arguments,
                                  const QString &name,
                                  const QString &display,
                                  const QString &description) : StreamFile(arguments, name, display,
                                                                           description)
{ }

Arguments::ReadStream::~ReadStream() = default;

void Arguments::ReadStream::push(Lua::Engine::Frame &frame, bool textMode)
{
    if (!fetch(frame)) {
        frame.pushNil();
        return;
    }
    if (filename.empty()) {
        Lua::Engine::Frame local(frame);
        local.push(local.global(), "io");
        local.push(local.back(), "stdin");
        local.replaceWithBack(0);
        local.propagate(1);
        return;
    }
    Lua::Engine::Call call(frame);
    call.push(call.global(), "io");
    call.push(call.back(), "open");
    call.replaceWithBack(0);
    call.resize(1);
    call.push(filename);
    if (textMode)
        call.push("r");
    else
        call.push("rb");
    if (!call.executeVariable())
        return;
    call.propagate();
}

Arguments::WriteStream::WriteStream(Arguments &arguments,
                                    const QString &name,
                                    const QString &display,
                                    const QString &description) : StreamFile(arguments, name,
                                                                             display, description)
{ }

Arguments::WriteStream::~WriteStream() = default;

void Arguments::WriteStream::push(Lua::Engine::Frame &frame, bool textMode)
{
    if (!fetch(frame)) {
        frame.pushNil();
        return;
    }
    if (filename.empty()) {
        Lua::Engine::Frame local(frame);
        local.push(local.global(), "io");
        local.push(local.back(), "stdout");
        local.replaceWithBack(0);
        local.propagate(1);
        return;
    }
    Lua::Engine::Call call(frame);
    call.push(call.global(), "io");
    call.push(call.back(), "open");
    call.replaceWithBack(0);
    call.resize(1);
    call.push(filename);
    if (textMode)
        call.push("w");
    else
        call.push("wb");
    if (!call.executeVariable())
        return;
    call.propagate();
}

Arguments::Pipeline::Pipeline(Arguments &arguments,
                              LuaPipeline *origin, const QString &name, const QString &display)
        : Output(arguments), origin(origin), name(name), display(display)
{ }

Arguments::Pipeline::~Pipeline() = default;

Arguments::PipelineInput::PipelineInput(Arguments &arguments,
                                        LuaPipeline *origin,
                                        const QString &name,
                                        const QString &display) : Pipeline(arguments, origin, name,
                                                                           display),
                                                                  mode(Mode::Pipeline)
{ }

Arguments::PipelineInput::~PipelineInput() = default;

bool Arguments::PipelineInput::configure(CPD3::Data::StreamPipeline *pipeline)
{
    switch (mode) {
    case Mode::Pipeline:
        return pipeline->setInputPipeline();
    case Mode::File:
        return pipeline->setInputFile(QString::fromStdString(filename));
    case Mode::Archive:
        Q_ASSERT(origin);
        return pipeline->setInputArchive(archive, clip, &(origin->archive()));
    case Mode::Requested:
        Q_ASSERT(origin);
        return pipeline->setInputArchiveRequested(clip, true, &(origin->archive()));
    }
    Q_ASSERT(false);
    return false;
}

Arguments::PipelineOutput::PipelineOutput(Arguments &arguments,
                                          LuaPipeline *origin,
                                          const QString &name,
                                          const QString &display) : Pipeline(arguments, origin,
                                                                             name, display),
                                                                    mode(Mode::Pipeline)
{ }

Arguments::PipelineOutput::~PipelineOutput() = default;

bool Arguments::PipelineOutput::configure(CPD3::Data::StreamPipeline *pipeline)
{
    switch (mode) {
    case Mode::Pipeline:
        return pipeline->setOutputPipeline();
    case Mode::File: {
        CPD3::Data::StandardDataOutput::OutputType
                type = CPD3::Data::StandardDataOutput::OutputType::Direct;
        auto name = QString::fromStdString(filename);
        if (name.endsWith(".c3r", Qt::CaseInsensitive))
            type = CPD3::Data::StandardDataOutput::OutputType::Raw;
        else if (name.endsWith(".xml", Qt::CaseInsensitive))
            type = CPD3::Data::StandardDataOutput::OutputType::XML;
        else if (name.endsWith(".json", Qt::CaseInsensitive))
            type = CPD3::Data::StandardDataOutput::OutputType::JSON;
        return pipeline->setOutputFile(name, type);
    }
    }
    Q_ASSERT(false);
    return false;
}

Arguments::PipelineExternalSinkOutput::PipelineExternalSinkOutput(Arguments &arguments,
                                                                  LuaPipeline *origin,
                                                                  const QString &name,
                                                                  const QString &display,
                                                                  CPD3::Data::ExternalSinkComponent *component,
                                                                  const CPD3::ComponentOptions &options)
        : Pipeline(arguments, origin, name, display),
          component(component),
          options(options),
          mode(Mode::Pipeline)
{ }

Arguments::PipelineExternalSinkOutput::~PipelineExternalSinkOutput() = default;

bool Arguments::PipelineExternalSinkOutput::configure(CPD3::Data::StreamPipeline *pipeline)
{
    switch (mode) {
    case Mode::Pipeline: {
        auto device = pipeline->pipelineOutputDevice();
        if (!device)
            return false;
        auto stream = device->stream();
        if (!stream)
            return false;
        std::unique_ptr<CPD3::Data::ExternalSink>
                output(component->createDataSink(std::move(stream), options));
        if (!output)
            return false;
        return pipeline->setOutputGeneral(std::move(output));
    }
    case Mode::File: {
        auto target = IO::Access::file(filename, IO::File::Mode::writeOnly().textMode());
        if (!target)
            return false;
        auto stream = target->stream();
        if (!stream)
            return false;
        std::unique_ptr<CPD3::Data::ExternalSink>
                output(component->createDataSink(std::move(stream), options));
        if (!output)
            return false;
        return pipeline->setOutputGeneral(std::move(output));
    }
    }
    Q_ASSERT(false);
    return false;
}

Arguments::PipelineDisplayOutput::PipelineDisplayOutput(Arguments &arguments,
                                                        LuaPipeline *origin,
                                                        const QString &name,
                                                        const QString &display,
                                                        CPD3::Data::ValueSegment::Transfer config)
        : Pipeline(arguments, origin, name, display),
          config(std::move(config)),
          width(1024),
          height(768)
{ }

Arguments::PipelineDisplayOutput::~PipelineDisplayOutput() = default;

bool Arguments::PipelineDisplayOutput::configure(CPD3::Data::StreamPipeline *pipeline)
{
    LuaPipeline::CreateDisplay create = [this]() {
        Graphing::DisplayDefaults defaults;

        if (selectionArchives.size() == 1 &&
                QRegularExpression::escape(QString::fromStdString(selectionArchives.front())) ==
                        QString::fromStdString(selectionArchives.front())) {
            defaults.setArchive(selectionArchives.front());
        } else {
            defaults.setArchive("raw");
        }

        auto display = new Graphing::DisplayLayout(defaults);
        display->initialize(config, defaults);
        return display;
    };

    auto bounds = selectionBounds;
    auto stations = selectionStations;
    auto archives = selectionArchives;
    auto variables = selectionVariables;
    LuaPipeline::ConfigureDisplayChain configure =
            [bounds, stations, archives, variables](CPD3::Data::ProcessingTapChain *chain) {
                chain->setDefaultSelection(bounds.start, bounds.end, stations, archives, variables);
            };

    if (!filename.empty()) {
        return origin->setDisplayOutputFile(pipeline, filename, width, height, create, configure);
    }
    return origin->setDisplayOutputWindow(pipeline, create, configure);
}