/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#ifdef BUILD_GUI

#include <QMessageBox>
#include <QImageWriter>

#endif

#include <QStringList>
#include <QRegularExpression>

#include "arguments.hxx"
#include "execthread.hxx"
#include "core/threading.hxx"
#include "core/timeutils.hxx"
#include "core/timeparse.hxx"
#include "core/util.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"
#include "clicore/archiveparse.hxx"
#include "clicore/optionparse.hxx"
#include "clicore/argumentparser.hxx"
#include "clicore/terminaloutput.hxx"

using namespace CPD3;
using namespace CPD3::CLI;
using namespace CPD3::Data;

static SequenceName::ComponentSet parseStationSpecification(QStringList &arguments,
                                                            std::size_t allowStations,
                                                            std::size_t requireStations,
                                                            Archive::Access &archive)
{
    if (allowStations <= 0 && requireStations <= 0)
        return {};

    /* If we want exactly one station, and the first looks like a station, accept
     * it regardless, so that we can take a non-existent station (for data generation) */
    if (allowStations == 1 &&
            requireStations == 1 &&
            !arguments.isEmpty() &&
            arguments.front().length() == 3) {
        static QRegularExpression
                reCheck("[a-z][a-z0-9]{2}", QRegularExpression::CaseInsensitiveOption);
        if (Util::exact_match(arguments.front(), reCheck)) {
            SequenceName::ComponentSet stations{arguments.front().toLower().toStdString()};
            arguments.removeFirst();
            return stations;
        }
    }

    SequenceName::ComponentSet stations;
    bool useAllStations = false;
    while (!arguments.isEmpty()) {
        auto split = arguments.front().split(QRegularExpression("[:;,]+"), QString::SkipEmptyParts);
        bool anyMissed = split.isEmpty();
        SequenceName::ComponentSet maybeStations;
        for (const auto &comp : split) {
            if (comp.toLower() == QObject::tr("allstations", "all stations string")) {
                useAllStations = true;
                continue;
            }
            auto add = archive.availableStations({comp.toStdString()});
            if (add.empty()) {
                anyMissed = true;
                break;
            }
            maybeStations.insert(add.begin(), add.end());
        }
        if (anyMissed)
            break;
        stations.insert(maybeStations.begin(), maybeStations.end());
        arguments.removeFirst();
    }
    if (useAllStations) {
        stations = archive.availableStations();
    }

    if (stations.empty() && !useAllStations && requireStations == 1 && allowStations <= 1) {
        stations = SequenceName::impliedStations(&archive);
    }

    return stations;
}

void Arguments::argumentsAbort(const QString &text, bool isError)
{
#ifndef NO_CONSOLE
    if (!usingGUI) {
        if (isError) {
            TerminalOutput::simpleWordWrap(text, true);
            QCoreApplication::exit(1);
        } else {
            TerminalOutput::simpleWordWrap(text);
            QCoreApplication::exit(0);
        }
        return;
    }
#endif
#ifdef BUILD_GUI
    Threading::runBlockingFunctor(thread, [text, isError]() {
        if (isError) {
            QMessageBox::critical(nullptr, QObject::tr("Error in arguments"), text);
            QCoreApplication::exit(1);
        } else {
            QMessageBox::information(nullptr, QObject::tr("Arguments"), text);
            QCoreApplication::exit(0);
        }
    });
#endif
}

bool Arguments::evaluateArguments()
{
    class Parser : public ArgumentParser {
        QString programName;
    public:
        enum class Mode {
            NoBarewords,
            GenericDataInput,
            DisplayDataInput,
            ExternalFileInput,
            BoundsOnly,
            TimeOnly,
            StationOnly,
            StationAndBounds,
            StationAndTime,
            StationAndFile
        };
        Mode mode;
        bool allowUndefined;
        bool allowZeroLength;
        CPD3::Time::LogicalTimeUnit defaultUnit;
        bool inferStation;

        Parser(Mode mode, QString programName) : programName(std::move(programName)),
                                                 mode(mode),
                                                 allowUndefined(true),
                                                 allowZeroLength(false),
                                                 defaultUnit(CPD3::Time::Day),
                                                 inferStation(true)
        { }

        virtual ~Parser() = default;

    protected:
        QString getProgramName() override
        { return programName; }

        QList<BareWordHelp> getBareWordHelp() override
        {
            switch (mode) {
            case Mode::NoBarewords:
                return QList<BareWordHelp>();

            case Mode::GenericDataInput:
            case Mode::DisplayDataInput:
                return QList<BareWordHelp>() << BareWordHelp(tr("station", "station argument name"),
                                                             tr("The (default) station to read data from"),
                                                             inferStation ? tr(
                                                                     "Inferred from the current directory")
                                                                          : QString(),
                                                             tr("The \"station\" bare word argument is used to specify the station(s) used to "
                                                                "look up variables that do not include an explicit station as part of an "
                                                                "archive read specification.  This is the three digit station identification "
                                                                "code.  For example \"brw\".  If a variable does not specify a station, then "
                                                                "this is the station it will be looked up from.  Multiple stations may be "
                                                                "specified by separating them with \":\", \";\" or \",\".  Regular expressions "
                                                                "are accepted.")) << BareWordHelp(
                        tr("variables", "variables argument name"),
                        tr("The list of variable specifies to read from the archive"), QString(),
                        tr("The \"variables\" bare word argument sets the list of variables to read "
                           "from the archive.  This can be either a direct list of variables (such as "
                           "\"T_S11\" or a alias for multiple variables, such as \"S11a\".  In the single "
                           "variable form regular expressions may be used, as long as they match the "
                           "entire variable name.  This list is delimited by commas.  If a specification "
                           "includes a colon then the part before the colon is treated as an override to "
                           "the default archive.  If it includes two colons then the first field is "
                           "treated as the station, the second as the archive, and the third as the "
                           "actual variable specification.  If it includes four or more colons then "
                           "the specification is treated as with three except that the trailing "
                           "components specify the flavor (e.x. PM1 or PM10) restriction.  This "
                           "restriction consists of an optional prefix and the flavor name.  If there is "
                           "no prefix then the flavor is required.  If the prefix is \"!\" then the flavor "
                           "is excluded, that is the results will never include it.  If the prefix is"
                           "\"=\" then the flavor is added to the list of flavors that any match must "
                           "have exactly."
                           "\nFor example:"
                           "\"T_S11\" specifies the variable in the \"default\" station (set "
                           "either explicitly on the command line or inferred from the current directory) "
                           "and the \"default\" archive (either the \"raw\" archive or the one set on "
                           "the command line.\n"
                           "\"raw:T_S1[12]\" specifies the variables T_S11 and T_S12 from the raw archive\n"
                           "on the \"default\" station.\n"
                           "\"brw:raw:S11a\" specifies the \"S11a\" alias record for the station \"brw\"\n"
                           "and the \"raw\" archive.\n"
                           "\":avgh:T_S11:pm1:!stats\" specifies T_S11 from the \"default\" station "
                           "hourly averages restricted to only PM1 data, but not any calculated statistics.\n\n"
                           "The string \"everything\" can also be used to retrieve all available "
                           "variables.")) << BareWordHelp(tr("times", "times argument name"),
                                                          tr("The time range of data to read"),
                                                          QString(),
                                                          TimeParse::describeListBoundsUsage(
                                                                  allowZeroLength, allowUndefined,
                                                                  defaultUnit))
                                             << BareWordHelp(tr("archive", "archive argument name"),
                                                             tr("The (default) archive to read data from"),
                                                             tr("The \"raw\" data archive"),
                                                             tr("The \"archive\" bare word argument is used to specify the archive used to "
                                                                "Look up variables that do not include an archive station as part of an "
                                                                "archive read specification.  This is the internal archive name, such as "
                                                                "\"raw\" or \"clean\".  Multiple archives may be specified by separating them "
                                                                "with \":\", \";\" or \",\".  Regular expressions are accepted."))
                                             << BareWordHelp(tr("inputFile", "file argument name"),
                                                             tr("The file to read data from or - for standard input"),
                                                             QString(),
                                                             tr("The \"inputFile\" bare word argument is used to specify the the file to read "
                                                                "data from.  If it is present and exists then data is read from the given file "
                                                                "name instead of from standard input.  Alternatively \"-\" may be used to "
                                                                "explicitly specify standard input."));

            case Mode::ExternalFileInput:
                return QList<BareWordHelp>() << BareWordHelp(tr("inputFile", "file argument name"),
                                                             tr("The file to read data from or - for standard input"),
                                                             QString(),
                                                             tr("The \"inputFile\" bare word argument is used to specify the the file to read "
                                                                "data from.  If it is present and exists then data is read from the given file "
                                                                "name instead of from standard input.  Alternatively \"-\" may be used to "
                                                                "explicitly specify standard input."));

            case Mode::BoundsOnly:
                return QList<BareWordHelp>() << BareWordHelp(tr("times", "times argument name"),
                                                             tr("The time range of data to access"),
                                                             QString(),
                                                             TimeParse::describeListBoundsUsage(
                                                                     allowZeroLength,
                                                                     allowUndefined, defaultUnit));
            case Mode::TimeOnly:
                return QList<BareWordHelp>() << BareWordHelp(tr("times", "times argument name"),
                                                             tr("The time to process"), QString(),
                                                             TimeParse::describeSingleTime(
                                                                     allowUndefined));

            case Mode::StationOnly:
                return QList<BareWordHelp>() << BareWordHelp(tr("station", "station argument name"),
                                                             tr("The station to process on"),
                                                             inferStation ? tr(
                                                                     "Inferred from the current directory")
                                                                          : QString(),
                                                             tr("The \"station\" bare word argument is used to specify the station(s) used to "
                                                                "look up variables that do not include an explicit station as part of an "
                                                                "archive read specification.  This is the three digit station identification "
                                                                "code.  For example \"brw\".  If a variable does not specify a station, then "
                                                                "this is the station it will be looked up from.  Multiple stations may be "
                                                                "specified by separating them with \":\", \";\" or \",\".  Regular expressions "
                                                                "are accepted."));

            case Mode::StationAndBounds:
                return QList<BareWordHelp>() << BareWordHelp(tr("station", "station argument name"),
                                                             tr("The station to process on"),
                                                             inferStation ? tr(
                                                                     "Inferred from the current directory")
                                                                          : QString(),
                                                             tr("The \"station\" bare word argument is used to specify the station(s) used to "
                                                                "look up variables that do not include an explicit station as part of an "
                                                                "archive read specification.  This is the three digit station identification "
                                                                "code.  For example \"brw\".  If a variable does not specify a station, then "
                                                                "this is the station it will be looked up from.  Multiple stations may be "
                                                                "specified by separating them with \":\", \";\" or \",\".  Regular expressions "
                                                                "are accepted."))
                                             << BareWordHelp(tr("times", "times argument name"),
                                                             tr("The time range of data to access"),
                                                             QString(),
                                                             TimeParse::describeListBoundsUsage(
                                                                     allowZeroLength,
                                                                     allowUndefined, defaultUnit));

            case Mode::StationAndTime:
                return QList<BareWordHelp>() << BareWordHelp(tr("station", "station argument name"),
                                                             tr("The station to process on"),
                                                             inferStation ? tr(
                                                                     "Inferred from the current directory")
                                                                          : QString(),
                                                             tr("The \"station\" bare word argument is used to specify the station(s) used to "
                                                                "look up variables that do not include an explicit station as part of an "
                                                                "archive read specification.  This is the three digit station identification "
                                                                "code.  For example \"brw\".  If a variable does not specify a station, then "
                                                                "this is the station it will be looked up from.  Multiple stations may be "
                                                                "specified by separating them with \":\", \";\" or \",\".  Regular expressions "
                                                                "are accepted."))
                                             << BareWordHelp(tr("times", "times argument name"),
                                                             tr("The time to process"), QString(),
                                                             TimeParse::describeSingleTime(
                                                                     allowUndefined));

            case Mode::StationAndFile:
                return QList<BareWordHelp>() << BareWordHelp(tr("station", "station argument name"),
                                                             tr("The station to process on"),
                                                             inferStation ? tr(
                                                                     "Inferred from the current directory")
                                                                          : QString(),
                                                             tr("The \"station\" bare word argument is used to specify the station(s) used to "
                                                                "look up variables that do not include an explicit station as part of an "
                                                                "archive read specification.  This is the three digit station identification "
                                                                "code.  For example \"brw\".  If a variable does not specify a station, then "
                                                                "this is the station it will be looked up from.  Multiple stations may be "
                                                                "specified by separating them with \":\", \";\" or \",\".  Regular expressions "
                                                                "are accepted."))
                                             << BareWordHelp(tr("inputFile", "file argument name"),
                                                             tr("The file to read data from or - for standard input"),
                                                             QString(),
                                                             tr("The \"inputFile\" bare word argument is used to specify the the file to read "
                                                                "data from.  If it is present and exists then data is read from the given file "
                                                                "name instead of from standard input.  Alternatively \"-\" may be used to "
                                                                "explicitly specify standard input."));

            }
            Q_ASSERT(false);
            return QList<BareWordHelp>();
        }

        QString getBareWordCommandLine() override
        {
            switch (mode) {
            case Mode::NoBarewords:
                return QString();

            case Mode::GenericDataInput:
                return tr("[[station] variables times [archive]]|[inputFile]",
                          "generic read bare words");

            case Mode::DisplayDataInput:
                return tr("[[station] [variables] times [archive]]|[inputFile]",
                          "filter bare word command line");

            case Mode::ExternalFileInput:
                return tr("[inputFile]", "external file bare word command line");

            case Mode::StationOnly:
                return tr("station", "station only command line");

            case Mode::BoundsOnly:
            case Mode::TimeOnly:
                return tr("times", "times only command line");

            case Mode::StationAndBounds:
            case Mode::StationAndTime:
                return tr("station times", "station and times command line");

            case Mode::StationAndFile:
                return tr("station [inputFile]", "station and file command line");
            }
            Q_ASSERT(false);
            return QString();
        }

        QString getDescription() override
        {
            switch (mode) {
            case Mode::NoBarewords:
                return QString();

            case Mode::GenericDataInput:
            case Mode::DisplayDataInput: {
                QString text;
#ifndef NO_CONSOLE
                text.append(
                        tr("If used without any data specification (variables, start, etc) then input will "
                           "be read from standard input.  "));
#endif
                text.append(
                        tr("If an existing, readable file is given input will be read from that file.  "
                           "Otherwise the data given on the command line is read, with the variables "
                           "determined by the graph's switch(es) if not explicitly specified."));
                return text;
            }

            case Mode::ExternalFileInput: {
                QString text;
#ifndef NO_CONSOLE
                text.append(
                        tr("If used without a file then input will be read from standard input."));
#else
                text.append(tr("If used without a file then input will be read from the default file."));
#endif
                return text;
            }

            case Mode::StationOnly:
                return tr("A station is required for processing.");

            case Mode::BoundsOnly:
                return tr("A time range is required for processing.");
            case Mode::TimeOnly:
                return tr("A time is required for processing.");

            case Mode::StationAndBounds:
                return tr("A station and time range is required for processing.");
            case Mode::StationAndTime:
                return tr("A station and time is required for processing.");
            case Mode::StationAndFile: {
                QString text = tr("A station and time is required for processing.");
#ifndef NO_CONSOLE
                text.append(
                        tr("  If used without a file then input will be read from standard input."));
#else
                text.append(tr("  If used without a file then input will be read from the default file."));
#endif
                return text;
            }
            }
            Q_ASSERT(false);
            return QString();
        }

        QHash<QString, QString> getDocumentationTypeID() override
        {
            QHash<QString, QString> result;
            result.insert("type", "script");
            switch (mode) {
            case Mode::NoBarewords:
                result.insert("mode", "no_barewords");
                break;
            case Mode::GenericDataInput:
                result.insert("mode", "datainput");
                break;
            case Mode::DisplayDataInput:
                result.insert("mode", "datainput");
                result.insert("display", QString());
                break;
            case Mode::ExternalFileInput:
                result.insert("mode", "fileinput");
                break;
            case Mode::BoundsOnly:
                result.insert("bounds", QString());
                break;
            case Mode::TimeOnly:
                result.insert("time", "required");
                break;
            case Mode::StationOnly:
                result.insert("stations", QString());
                break;
            case Mode::StationAndBounds:
                result.insert("bounds", QString());
                result.insert("stations", QString());
                break;
            case Mode::StationAndTime:
                result.insert("time", "required");
                result.insert("stations", QString());
                break;
            case Mode::StationAndFile:
                result.insert("mode", "fileinput");
                result.insert("stations", QString());
                break;
            }
            return result;
        }
    };

    Parser::Mode mode = Parser::Mode::NoBarewords;

    std::vector<std::shared_ptr<Stations>> stations;
    std::vector<std::shared_ptr<TimePoint>> timePoints;
    std::vector<std::shared_ptr<Bounds>> bounds;
    std::vector<std::shared_ptr<ReadStream>> readStreams;
    std::vector<std::shared_ptr<WriteStream>> writeStreams;
    std::vector<std::shared_ptr<PipelineInput>> pipelineInputs;
    std::vector<std::shared_ptr<PipelineOutput>> pipelineOutputs;
    std::vector<std::shared_ptr<PipelineExternalSinkOutput>> pipelineExternalSinkOutputs;
    std::vector<std::shared_ptr<PipelineDisplayOutput>> pipelineDisplayOutputs;
    for (const auto &weak : outputs) {
        auto check = weak.lock();
        if (!check)
            continue;
        if (auto station = std::dynamic_pointer_cast<Stations>(check)) {
            stations.emplace_back(std::move(station));
        } else if (auto timePoint = std::dynamic_pointer_cast<TimePoint>(check)) {
            timePoints.emplace_back(std::move(timePoint));
        } else if (auto bound = std::dynamic_pointer_cast<Bounds>(check)) {
            bounds.emplace_back(std::move(bound));
        } else if (auto stream = std::dynamic_pointer_cast<ReadStream>(check)) {
            readStreams.emplace_back(std::move(stream));
        } else if (auto stream = std::dynamic_pointer_cast<WriteStream>(check)) {
            writeStreams.emplace_back(std::move(stream));
        } else if (auto pipelineInput = std::dynamic_pointer_cast<PipelineInput>(check)) {
            if (!pipelineInput->origin)
                continue;
            pipelineInputs.emplace_back(std::move(pipelineInput));
        } else if (auto pipelineOutput = std::dynamic_pointer_cast<PipelineOutput>(check)) {
            if (!pipelineOutput->origin)
                continue;
            pipelineOutputs.emplace_back(std::move(pipelineOutput));
        } else if (auto pipelineExternalSinkOutput =
                std::dynamic_pointer_cast<PipelineExternalSinkOutput>(check)) {
            if (!pipelineExternalSinkOutput->origin)
                continue;
            pipelineExternalSinkOutputs.emplace_back(std::move(pipelineExternalSinkOutput));
        } else if (auto
                pipelineDisplayOutput = std::dynamic_pointer_cast<PipelineDisplayOutput>(check)) {
            if (!pipelineDisplayOutput->origin)
                continue;
            pipelineDisplayOutputs.emplace_back(std::move(pipelineDisplayOutput));
        }
    }

    bool primaryPipelineDisplay = false;
    std::shared_ptr<PipelineInput> primaryPipelineInput;
    std::shared_ptr<Stations> primaryStation;
    std::shared_ptr<Bounds> primaryBounds;
    std::shared_ptr<TimePoint> primaryTimePoint;
    std::shared_ptr<ReadStream> primaryReadStream;
    if (!pipelineInputs.empty()) {
        mode = Parser::Mode::GenericDataInput;
        primaryPipelineInput = std::move(pipelineInputs.front());
        pipelineInputs.erase(pipelineInputs.begin());
        if (!pipelineDisplayOutputs.empty() &&
                pipelineDisplayOutputs.front()->origin == primaryPipelineInput->origin) {
            primaryPipelineDisplay = true;
            mode = Parser::Mode::DisplayDataInput;
        }
    } else if (!stations.empty()) {
        mode = Parser::Mode::StationOnly;
        primaryStation = std::move(stations.front());
        stations.erase(stations.begin());

        if (!bounds.empty()) {
            mode = Parser::Mode::StationAndBounds;
            primaryBounds = std::move(bounds.front());
            bounds.erase(bounds.begin());
        } else if (!timePoints.empty()) {
            mode = Parser::Mode::StationAndTime;
            primaryTimePoint = std::move(timePoints.front());
            timePoints.erase(timePoints.begin());
        } else if (!readStreams.empty()) {
            mode = Parser::Mode::StationAndFile;
            primaryReadStream = std::move(readStreams.front());
            readStreams.erase(readStreams.begin());
        }
    } else if (!bounds.empty()) {
        mode = Parser::Mode::BoundsOnly;
        primaryBounds = std::move(bounds.front());
        bounds.erase(bounds.begin());
    } else if (!timePoints.empty()) {
        mode = Parser::Mode::TimeOnly;
        primaryTimePoint = std::move(timePoints.front());
        timePoints.erase(timePoints.begin());
    } else if (!readStreams.empty()) {
        mode = Parser::Mode::ExternalFileInput;
        primaryReadStream = std::move(readStreams.front());
        readStreams.erase(readStreams.begin());
    }

    std::shared_ptr<PipelineOutput> primaryPipelineOutput;
    std::shared_ptr<PipelineExternalSinkOutput> primaryPipelineExternalSinkOutput;
    std::shared_ptr<WriteStream> primaryWriteStream;
#ifndef NO_CONSOLE
    if (!pipelineOutputs.empty()) {
        primaryPipelineOutput = std::move(pipelineOutputs.front());
        pipelineOutputs.erase(pipelineOutputs.begin());
    } else if (!pipelineExternalSinkOutputs.empty()) {
        primaryPipelineExternalSinkOutput = std::move(pipelineExternalSinkOutputs.front());
        pipelineExternalSinkOutputs.erase(pipelineExternalSinkOutputs.begin());
    } else
#endif
    if (!writeStreams.empty()) {
        primaryWriteStream = std::move(writeStreams.front());
        writeStreams.erase(writeStreams.begin());
    }


    for (const auto &add : stations) {
        outputOptions.add(add->name,
                          new ComponentOptionStringSet(add->name, add->display, add->description,
                                                       add->defaultBehavior));
    }
    for (const auto &add : timePoints) {
        auto opt = new ComponentOptionSingleTime(add->name, add->display, add->description,
                                                 add->defaultBehavior);
        opt->setAllowUndefined(add->allowUndefined);
        outputOptions.add(add->name, opt);
    }
    for (const auto &add : bounds) {
        auto opt = new ComponentOptionSingleTime(
                QObject::tr("start-%1", "bounds start option").arg(add->name), add->display,
                add->description, add->defaultBehavior);
        opt->setAllowUndefined(add->allowUndefined);
        outputOptions.add("start-" + add->name, opt);

        opt = new ComponentOptionSingleTime(
                QObject::tr("end-%1", "bounds end option").arg(add->name), add->display,
                add->description, add->defaultBehavior);
        opt->setAllowUndefined(add->allowUndefined);
        outputOptions.add("end-" + add->name, opt);
    }
    for (const auto &add : readStreams) {
        auto opt = new ComponentOptionFile(add->name, add->display, add->description, {});
        opt->setMode(ComponentOptionFile::Read);
        outputOptions.add(add->name, opt);
    }
    for (const auto &add : writeStreams) {
        auto opt = new ComponentOptionFile(add->name, add->display, add->description, {});
        opt->setMode(ComponentOptionFile::Write);
        outputOptions.add(add->name, opt);
    }
    for (const auto &add : pipelineInputs) {
        auto opt = new ComponentOptionFile(
                QObject::tr("input-%1", "pipeline input option").arg(add->name), add->display, {},
                {});
        opt->setMode(ComponentOptionFile::Read);
        outputOptions.add("input-" + add->name, opt);
    }
    for (const auto &add : pipelineOutputs) {
        auto opt = new ComponentOptionFile(
                QObject::tr("output-%1", "pipeline output option").arg(add->name), add->display, {},
                {});
        opt->setMode(ComponentOptionFile::Write);
        outputOptions.add("output-" + add->name, opt);
    }
    {
        QSet<QString> extensions;
#ifdef BUILD_GUI
        for (const auto &add : QImageWriter::supportedImageFormats()) {
            QString str(*add);
            if (str.isEmpty() || str.contains('%'))
                continue;
            extensions.insert(str.toLower());
        }
#endif
        extensions.insert("svg");

        for (const auto &add : pipelineDisplayOutputs) {
            auto opt = new ComponentOptionFile(
                    QObject::tr("display-%1", "display output option").arg(add->name), add->display,
                    {}, QObject::tr("%1.png", "default display output").arg(add->name));
            opt->setMode(ComponentOptionFile::Write);
            opt->setTypeDescription(QObject::tr("Image files"));
            opt->setExtensions(extensions);
            outputOptions.add("display-" + add->name, opt);

            auto size = new ComponentOptionSingleInteger(
                    QObject::tr("width-%1", "display output option").arg(add->name),
                    QObject::tr("Output file width"),
                    QObject::tr("This is the width of the output file."),
                    QObject::tr("1024", "default output width"));
            size->setMinimum(1);
            size->setAllowUndefined(false);
            outputOptions.add("width-" + add->name, size);

            size = new ComponentOptionSingleInteger(
                    QObject::tr("height-%1", "display output option").arg(add->name),
                    QObject::tr("Output file width"),
                    QObject::tr("This is the width of the output file."),
                    QObject::tr("768", "default output width"));
            size->setMinimum(1);
            size->setAllowUndefined(false);
            outputOptions.add("height-" + add->name, size);
        }
    }


    Parser parser(mode, QString::fromStdString(thread->getDisplay()));
    if (primaryTimePoint) {
        parser.allowUndefined = primaryTimePoint->allowUndefined;
        parser.allowZeroLength = true;
        parser.defaultUnit = primaryTimePoint->defaultUnit;
    } else if (primaryBounds) {
        parser.allowUndefined = primaryBounds->allowUndefined;
        parser.allowZeroLength = true;
        parser.defaultUnit = primaryBounds->defaultUnit;
    }
    if (primaryStation) {
        parser.inferStation = primaryStation->getMinimumRequired() == 1 &&
                primaryStation->getMaximumAllowed() <= 1;
    }
    try {
        parser.parse(commandArguments, outputOptions);
    } catch (ArgumentParsingException &ape) {
        argumentsAbort(ape.getText(), ape.isError());
        return false;
    }

#ifndef NO_CONSOLE
    bool usedStdin = false;
#endif

    if (primaryPipelineInput) {
#ifndef NO_CONSOLE
        if (!usedStdin &&
                (commandArguments.isEmpty() ||
                        (commandArguments.size() == 1 &&
                                commandArguments.front() ==
                                        QObject::tr("-", "standard input file name")))) {
            primaryPipelineInput->mode = PipelineInput::Mode::Pipeline;
            commandArguments.clear();
            usedStdin = true;
        } else
#endif
        if (commandArguments.size() == 1 && QFile::exists(commandArguments.front())) {
            primaryPipelineInput->mode = PipelineInput::Mode::File;
            primaryPipelineInput->filename = commandArguments.front().toStdString();
            commandArguments.clear();
        } else {
            try {
                primaryPipelineInput->archive =
                        CLI::ArchiveParse::parse(commandArguments, &primaryPipelineInput->clip,
                                                 &thread->archive, primaryPipelineDisplay);
            } catch (AcrhiveParsingException &ape) {
                argumentsAbort(ape.getDescription());
                return false;
            }
            if (primaryPipelineDisplay) {
                Q_ASSERT(!pipelineDisplayOutputs.empty());
                auto targetDisplay = pipelineDisplayOutputs.front().get();

                targetDisplay->selectionBounds = primaryPipelineInput->clip;

                if (!primaryPipelineInput->archive.empty()) {
                    if (primaryPipelineInput->archive.size() == 1) {
                        targetDisplay->selectionStations =
                                primaryPipelineInput->archive.front().stations;
                        targetDisplay->selectionArchives =
                                primaryPipelineInput->archive.front().archives;
                        targetDisplay->selectionVariables =
                                primaryPipelineInput->archive.front().variables;
                    }
                    if (primaryPipelineInput->archive.size() == 1 &&
                            primaryPipelineInput->archive.front().variables.empty()) {
                        primaryPipelineInput->archive.clear();
                    }
                }

#ifdef BUILD_GUI
                if (targetDisplay->selectionStations.size() == 1 &&
                        QRegularExpression::escape(
                                QString::fromStdString(targetDisplay->selectionStations.front())) ==
                                QString::fromStdString(targetDisplay->selectionStations.front())) {
                    thread->displayDynamicContext
                          .setStation(targetDisplay->selectionStations.front());
                }
#endif
            } else {
                if (primaryPipelineInput->archive.empty()) {
                    return false;
                }
            }

            primaryPipelineInput->mode = PipelineInput::Mode::Archive;
        }
        primaryPipelineInput->ready = true;
    }

    if (primaryStation) {
        auto result = parseStationSpecification(commandArguments, primaryStation->maximumAllowed,
                                                primaryStation->minimumRequired, thread->archive);

        if (result.size() < primaryStation->getMinimumRequired()) {
            argumentsAbort(QObject::tr("At least %n station(s) are required.", "",
                                       static_cast<int>(primaryStation->getMinimumRequired())));
            return false;
        }
        if (result.size() > primaryStation->getMaximumAllowed()) {
            argumentsAbort(QObject::tr("Only %n station(s) are accepted.", "",
                                       static_cast<int>(primaryStation->getMaximumAllowed())));
            return false;
        }
        primaryStation->result = std::move(result);
        primaryStation->ready = true;
    }

    if (primaryReadStream) {
#ifndef NO_CONSOLE
        if (!usedStdin &&
                (commandArguments.isEmpty() ||
                        (commandArguments.size() == 1 &&
                                commandArguments.back() ==
                                        QObject::tr("-", "standard input file name")))) {
            primaryReadStream->filename.clear();
            commandArguments.clear();
            usedStdin = true;
        } else
#endif
        if (commandArguments.size() == 1 && !commandArguments.back().isEmpty()) {
            primaryReadStream->filename = commandArguments.back().toStdString();
            commandArguments.clear();
        } else {
            argumentsAbort(QObject::tr("No input file specified."));
            return false;
        }
        primaryReadStream->ready = true;
    }

    if (primaryTimePoint) {
        class ParseReference : public TimeReference {
            double point;
        public:
            ParseReference() : point(CPD3::Time::time())
            { }

            virtual double getReference() noexcept(false)
            { return point; }

            virtual bool isLeading() noexcept(false)
            { return true; }

            virtual double getUndefinedValue() noexcept(false)
            { return FP::undefined(); }
        };

        double result = FP::undefined();
        try {
            ParseReference ref;
            TimeParseNOOPListHandler handler;
            result = TimeParse::parseListSingle(commandArguments, &ref, &handler, true, nullptr,
                                                primaryTimePoint->defaultUnit);
        } catch (TimeParsingException &tpe) {
            argumentsAbort(tpe.getDescription());
            return false;
        }
        if (!primaryTimePoint->allowUndefined && !FP::defined(result)) {
            argumentsAbort(QObject::tr("Undefined times are not permitted."));
            return false;
        }
        primaryTimePoint->result = result;
        primaryTimePoint->ready = true;
    } else if (primaryBounds) {
        CPD3::Time::Bounds result;
        try {
            result = TimeParse::parseListBounds(commandArguments, true,
                                                primaryBounds->allowUndefined,
                                                primaryBounds->defaultUnit);
        } catch (TimeParsingException &tpe) {
            argumentsAbort(tpe.getDescription());
            return false;
        }
        if (!primaryBounds->allowUndefined &&
                (!FP::defined(result.start) || !FP::defined(result.end))) {
            argumentsAbort(QObject::tr("Undefined bounds are not permitted."));
            return false;
        }
        primaryBounds->result = std::move(result);
        primaryBounds->ready = true;
    }

    if (primaryPipelineOutput) {
        primaryPipelineOutput->mode = PipelineOutput::Mode::Pipeline;
        primaryPipelineOutput->filename.clear();
        primaryPipelineOutput->ready = true;
    } else if (primaryPipelineExternalSinkOutput) {
        primaryPipelineExternalSinkOutput->mode = PipelineExternalSinkOutput::Mode::Pipeline;
        primaryPipelineExternalSinkOutput->filename.clear();
        primaryPipelineExternalSinkOutput->ready = true;
    } else if (primaryWriteStream) {
        primaryWriteStream->filename.clear();
        primaryWriteStream->ready = true;
    }


    for (const auto &add : stations) {
        if (!outputOptions.isSet(add->name))
            continue;
        auto opt = qobject_cast<ComponentOptionStringSet *>(outputOptions.get(add->name));
        if (!opt)
            continue;
        for (const auto &v : opt->get()) {
            add->result.insert(v.toStdString());
        }
        if (add->result.size() < add->getMinimumRequired()) {
            argumentsAbort(QObject::tr("At least %n station(s) are required.", "",
                                       static_cast<int>(primaryStation->getMinimumRequired())));
            return false;
        }
        if (add->result.size() > add->getMaximumAllowed()) {
            argumentsAbort(QObject::tr("Only %n station(s) are accepted.", "",
                                       static_cast<int>(primaryStation->getMaximumAllowed())));
            return false;
        }
        add->ready = true;
    }
    for (const auto &add : timePoints) {
        if (!outputOptions.isSet(add->name))
            continue;
        auto opt = qobject_cast<ComponentOptionSingleTime *>(outputOptions.get(add->name));
        if (!opt)
            continue;
        add->result = opt->get();
        if (!add->allowUndefined && !FP::defined(add->result)) {
            argumentsAbort(QObject::tr("Undefined times are not permitted."));
            return false;
        }
        add->ready = true;
    }
    for (const auto &add : bounds) {
        {
            auto name = "start-" + add->name;
            if (outputOptions.isSet(name)) {
                auto opt = qobject_cast<ComponentOptionSingleTime *>(outputOptions.get(name));
                if (opt) {
                    add->result.setStart(opt->get());
                }
            }
        }
        {
            auto name = "end-" + add->name;
            if (outputOptions.isSet(name)) {
                auto opt = qobject_cast<ComponentOptionSingleTime *>(outputOptions.get(name));
                if (opt) {
                    add->result.setEnd(opt->get());
                }
            }
        }
        if (!add->allowUndefined &&
                (!FP::defined(add->result.start) || !FP::defined(add->result.end))) {
            argumentsAbort(QObject::tr("Undefined bounds are not permitted."));
            return false;
        }
        add->ready = true;
    }
    for (const auto &add : readStreams) {
        if (!outputOptions.isSet(add->name)) {
#ifndef NO_CONSOLE
            if (!usedStdin) {
                add->filename.clear();
                add->ready = true;
                usedStdin = true;
            }
#endif
            continue;
        }
        auto opt = qobject_cast<ComponentOptionFile *>(outputOptions.get(add->name));
        if (!opt)
            continue;
        add->filename = opt->get().toStdString();
        add->ready = true;
    }
    for (const auto &add : writeStreams) {
        if (!outputOptions.isSet(add->name))
            continue;
        auto opt = qobject_cast<ComponentOptionFile *>(outputOptions.get(add->name));
        if (!opt)
            continue;
        add->filename = opt->get().toStdString();
        add->ready = true;
    }
    for (const auto &add : pipelineInputs) {
        auto name = "input-" + add->name;
        if (!outputOptions.isSet(name))
            continue;
        auto opt = qobject_cast<ComponentOptionFile *>(outputOptions.get(name));
        if (!opt)
            continue;
        add->filename = opt->get().toStdString();
        add->ready = true;
    }
    for (const auto &add : pipelineOutputs) {
        auto name = "output-" + add->name;
        if (!outputOptions.isSet(name))
            continue;
        auto opt = qobject_cast<ComponentOptionFile *>(outputOptions.get(name));
        if (!opt)
            continue;
        add->filename = opt->get().toStdString();
        add->ready = true;
    }
    for (const auto &add : pipelineDisplayOutputs) {
        add->filename =
                QObject::tr("%1.png", "default display output").arg(add->name).toStdString();
        {
            auto name = "display-" + add->name;
            if (outputOptions.isSet(name)) {
                auto opt = qobject_cast<ComponentOptionFile *>(outputOptions.get(name));
                if (opt) {
                    add->filename = opt->get().toStdString();
                }
            }
        }

        auto widthName = "width-" + add->name;
        if (outputOptions.isSet(widthName)) {
            auto opt = qobject_cast<ComponentOptionSingleInteger *>(outputOptions.get(widthName));
            if (opt) {
                add->width = static_cast<int>(opt->get());
                add->height = add->width * 3 / 4;
                if (add->height < 1)
                    add->height = 1;
            }
        }

        auto heightName = "height-" + add->name;
        if (outputOptions.isSet(heightName)) {
            auto opt = qobject_cast<ComponentOptionSingleInteger *>(outputOptions.get(heightName));
            if (opt) {
                add->height = static_cast<int>(opt->get());
                if (!outputOptions.isSet(widthName)) {
                    add->width = add->height * 4 / 3;
                    if (add->width < 1)
                        add->width = 1;
                }
            }
        }

        add->ready = true;
    }

    return true;
}