/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3LUAEXEC_PIPELINE_HXX
#define CPD3LUAEXEC_PIPELINE_HXX

#include "core/first.hxx"

#include <mutex>
#include <condition_variable>
#include <QThread>

#include "core/threading.hxx"
#include "luascript/engine.hxx"
#include "datacore/stream.hxx"
#include "datacore/streampipeline.hxx"
#include "datacore/sinkmultiplexer.hxx"
#include "datacore/archive/access.hxx"
#include "arguments.hxx"

class ExecThread;

namespace CPD3 {
namespace Graphing {
class Display;
}
}

class LuaPipeline : public CPD3::Lua::Engine::Data, public CPD3::Threading::Receiver {
public:
    class MultiplexerInput final : public CPD3::Data::StreamSink, public CPD3::Threading::Receiver {
        std::shared_ptr<CPD3::Data::SinkMultiplexer> mux;
        CPD3::Data::SinkMultiplexer::Sink *muxSink;

        std::mutex mutex;

        MultiplexerInput(ExecThread *thread,
                         const std::shared_ptr<CPD3::Data::SinkMultiplexer> &mux,
                         bool unsorted = false);

        friend class LuaPipeline;

    public:
        virtual ~MultiplexerInput();

        void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

        void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

        void incomingData(const CPD3::Data::SequenceValue &value) override;

        void incomingData(CPD3::Data::SequenceValue &&value) override;

        void endData() override;
    };

private:
    void meta_index(CPD3::Lua::Engine::Entry &entry,
                    const std::vector<CPD3::Lua::Engine::Reference> &upvalues);

    void meta_tostring(CPD3::Lua::Engine::Entry &entry);

    void method_execute(CPD3::Lua::Engine::Entry &entry);

    void method_processing(CPD3::Lua::Engine::Entry &entry);

    void method_editing(CPD3::Lua::Engine::Entry &entry);

    void method_input_select(CPD3::Lua::Engine::Entry &entry);

    void method_input_external(CPD3::Lua::Engine::Entry &entry);

    void method_input_archive(CPD3::Lua::Engine::Entry &entry);

    void method_input_file(CPD3::Lua::Engine::Entry &entry);

    void method_input_none(CPD3::Lua::Engine::Entry &entry);

    void method_input_values(CPD3::Lua::Engine::Entry &entry);

    void method_input_multiplexer(CPD3::Lua::Engine::Entry &entry);

    void method_output_select(CPD3::Lua::Engine::Entry &entry);

    void method_output_external(CPD3::Lua::Engine::Entry &entry);

    void method_output_display(CPD3::Lua::Engine::Entry &entry);

    void method_output_file(CPD3::Lua::Engine::Entry &entry);

    void method_output_discard(CPD3::Lua::Engine::Entry &entry);

    void method_segments(CPD3::Lua::Engine::Entry &entry);

    void method_values(CPD3::Lua::Engine::Entry &entry);

    void method_processor_segments(CPD3::Lua::Engine::Entry &entry);

    void method_processor_values(CPD3::Lua::Engine::Entry &entry);

    void method_fanout_segments(CPD3::Lua::Engine::Entry &entry);

    void method_fanout_values(CPD3::Lua::Engine::Entry &entry);

    void method_buffer(CPD3::Lua::Engine::Entry &entry);

    void method_start(CPD3::Lua::Engine::Entry &entry);

    void method_wait(CPD3::Lua::Engine::Entry &entry);

    class InputConnector final : public CPD3::Threading::Receiver {
        std::mutex mutex;
        CPD3::Data::StreamSink *pipeline;
        CPD3::Data::SequenceValue::Transfer buffer;
        double receivedStart;
        bool started;
        bool ended;
    public:
        InputConnector(ExecThread *thread);

        virtual ~InputConnector();

        void incomingData(const CPD3::Data::SequenceValue &value);

        void incomingData(CPD3::Data::SequenceValue &&value);

        void endData();

        void disconnect();

        void connect(CPD3::Data::StreamSink *target);

        void start();
    };

    class PipelineThread : public QThread {
        friend class LuaPipeline;

        LuaPipeline &parent;

        std::unique_ptr<QObject> context;
        std::unique_ptr<CPD3::Data::StreamPipeline> pipeline;
        std::shared_ptr<InputConnector> inputConnector;

        class ExitBlocker;

        std::list<std::unique_ptr<ExitBlocker>> blockers;

        void exitPoll();

        void blockPipeline();

        void manipulate(std::unique_lock<std::mutex> &lock,
                        const std::function<void(CPD3::Data::StreamPipeline *)> &function);
    public:
        PipelineThread(LuaPipeline &parent);

        virtual ~PipelineThread();

        void manipulate(const std::function<void(CPD3::Data::StreamPipeline *)> &function);

    protected:

        virtual void run();
    };

    friend class PipelineThread;

    bool canAlterPipeline(CPD3::Lua::Engine::Frame &frame);

    PipelineThread pipelineThread;
    std::string id;
    std::string display;
    bool started;
    ExecThread *thread;
    bool haveScriptControlledStage;
    CPD3::Threading::Connection startConnection;

    std::mutex mutex;
    std::condition_variable notify;
    std::shared_ptr<InputConnector> inputConnector;
    CPD3::Threading::Receiver inputMultiplexerReceiver;
    std::shared_ptr<CPD3::Data::SinkMultiplexer> inputMultiplexer;
    std::unique_ptr<MultiplexerInput> outputMultiplexer;

    std::shared_ptr<Arguments::Pipeline> inputArgument;
    std::shared_ptr<Arguments::Pipeline> outputArgument;

    void signalTerminate();

    void startPipeline();

    void finalizeMultiplexer();

    void complete();

    bool detachScriptStage(CPD3::Lua::Engine::Frame &frame);

    void argumentsReady(CPD3::Lua::Engine::Frame &frame);

public:
    LuaPipeline(ExecThread *thread, std::string id, std::string display);

    virtual ~LuaPipeline();

    static void install(ExecThread *thread, CPD3::Lua::Engine::Frame &root);

    static void operate(CPD3::Lua::Engine::Frame &root,
                        const std::function<void(CPD3::Lua::Engine::Frame &,
                                                 const CPD3::Lua::Engine::Reference &)> &function);

    std::unique_ptr<MultiplexerInput> multiplexerInput(CPD3::Lua::Engine::Frame &frame,
                                                       bool unsorted = false);

    CPD3::Data::Archive::Access &archive();

    using CreateDisplay = std::function<CPD3::Graphing::Display *()>;
    using ConfigureDisplayChain = std::function<void(CPD3::Data::ProcessingTapChain *chain)>;

    bool setDisplayOutputFile(CPD3::Data::StreamPipeline *pipeline,
                              const std::string &filename,
                              int width,
                              int height,
                              const CreateDisplay &create,
                              const ConfigureDisplayChain &configure = {});

    bool setDisplayOutputWindow(CPD3::Data::StreamPipeline *pipeline,
                                const CreateDisplay &create,
                                const ConfigureDisplayChain &configure = {});

protected:
    virtual void initialize(const CPD3::Lua::Engine::Reference &self,
                            CPD3::Lua::Engine::Frame &frame);
};


#endif //CPD3LUAEXEC_PIPELINE_HXX
