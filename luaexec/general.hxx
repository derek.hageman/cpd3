/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3LUAEXEC_GENERAL_HXX
#define CPD3LUAEXEC_GENERAL_HXX

#include "core/first.hxx"

#include <mutex>
#include <condition_variable>

#include "core/threading.hxx"
#include "core/merge.hxx"
#include "luascript/streambuffer.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/segment.hxx"

class ExecThread;


class GeneralBuffered : public CPD3::Data::ProcessingStage, public CPD3::Threading::Receiver {

    std::unique_ptr<CPD3::Lua::StreamBuffer> buffer;

    enum class State {
        Initialize, Active, EndReceived, EndSent, Finished, Terminate,
    } state;
    std::size_t controllerIndex;
    CPD3::Data::StreamSink *egress;

    std::mutex mutex;
    std::mutex egressLock;
    std::condition_variable notify;

    bool shouldProcessExternal() const;

    void stall(std::unique_lock<std::mutex> &lock);

    void finalize(CPD3::Lua::Engine::Frame &frame);

    void processingCompleted();

public:
    GeneralBuffered(ExecThread *thread,
                    CPD3::Lua::Engine::Frame &target,
                    CPD3::Lua::StreamBuffer *buffer);

    virtual ~GeneralBuffered();

    void start() override;

    bool isFinished() override;

    bool wait(double timeout = -1) override;

    void signalTerminate() override;


    void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

    void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

    void incomingData(const CPD3::Data::SequenceValue &value) override;

    void incomingData(CPD3::Data::SequenceValue &&value) override;

    void endData() override;

    void setEgress(CPD3::Data::StreamSink *egress) override;

    static void install(ExecThread *thread, CPD3::Lua::Engine::Frame &root);

protected:

    virtual bool stall() const = 0;

    virtual bool processIncoming(const CPD3::Data::SequenceValue::Transfer &values) = 0;

    virtual bool processIncoming(CPD3::Data::SequenceValue::Transfer &&values) = 0;

    virtual bool processIncoming(const CPD3::Data::SequenceValue &value) = 0;

    virtual bool processIncoming(CPD3::Data::SequenceValue &&value) = 0;

    virtual void processEnd();


    bool pushNext(CPD3::Lua::Engine::Frame &target);

    virtual bool fetchNext(CPD3::Lua::Engine::Frame &target) = 0;

    std::pair<CPD3::Data::StreamSink *,
              std::unique_lock<std::mutex>> acquireOutput(bool isEnding = false);

    void outputEnded();

    std::unique_lock<std::mutex> acquireState();
};

class GeneralValues : public GeneralBuffered {
    class Buffer : public CPD3::Lua::StreamBuffer::Value {
        GeneralValues &parent;
    public:
        Buffer(GeneralValues &parent);

        virtual ~Buffer();

    protected:
        bool pushNext(CPD3::Lua::Engine::Frame &target) override;

        void outputReady(CPD3::Lua::Engine::Frame &frame,
                         const CPD3::Lua::Engine::Reference &ref) override;

        void endReady() override;
    };

    friend class Buffer;

    std::deque<CPD3::Data::SequenceValue> incoming;

public:
    GeneralValues(ExecThread *thread, CPD3::Lua::Engine::Frame &target);

    virtual ~GeneralValues();

protected:
    bool stall() const override;

    bool processIncoming(const CPD3::Data::SequenceValue::Transfer &values) override;

    bool processIncoming(CPD3::Data::SequenceValue::Transfer &&values) override;

    bool processIncoming(const CPD3::Data::SequenceValue &value) override;

    bool processIncoming(CPD3::Data::SequenceValue &&value) override;

    bool fetchNext(CPD3::Lua::Engine::Frame &target) override;
};

class GeneralSegments : public GeneralBuffered {
    class Buffer : public CPD3::Lua::StreamBuffer::Segment {
        GeneralSegments &parent;
    public:
        Buffer(GeneralSegments &parent);

        virtual ~Buffer();

    protected:
        bool pushNext(CPD3::Lua::Engine::Frame &target) override;

        void convertFromLua(CPD3::Lua::Engine::Frame &frame) override;

        void outputReady(CPD3::Lua::Engine::Frame &frame,
                         const CPD3::Lua::Engine::Reference &ref) override;

        void endReady() override;

        bool bufferAssigned(CPD3::Lua::Engine::Frame &frame,
                            const CPD3::Lua::Engine::Reference &target,
                            const CPD3::Lua::Engine::Reference &value) override;
    };

    friend class Buffer;

    bool addInputsToOutputs;
    CPD3::Data::SequenceName::Set outputs;
    std::deque<CPD3::Data::SequenceSegment> incoming;
    CPD3::Data::SequenceSegment::Stream reader;

public:
    GeneralSegments(ExecThread *thread, CPD3::Lua::Engine::Frame &target);

    virtual ~GeneralSegments();

    void setExplicitOutputs(CPD3::Data::SequenceName::Set outputs);

protected:
    bool stall() const override;

    bool processIncoming(const CPD3::Data::SequenceValue::Transfer &values) override;

    bool processIncoming(CPD3::Data::SequenceValue::Transfer &&values) override;

    bool processIncoming(const CPD3::Data::SequenceValue &value) override;

    bool processIncoming(CPD3::Data::SequenceValue &&value) override;

    void processEnd() override;

    bool fetchNext(CPD3::Lua::Engine::Frame &target) override;
};


#endif //CPD3LUAEXEC_GENERAL_HXX
