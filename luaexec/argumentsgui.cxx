/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#ifdef BUILD_GUI

#include <QMessageBox>
#include <QDialog>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QDialogButtonBox>
#include <QTabWidget>
#include <QComboBox>
#include <QListWidget>
#include <QPushButton>
#include <QInputDialog>
#include <QLineEdit>
#include <QPushButton>
#include <QApplication>
#include <QFileDialog>
#include <QSizePolicy>

#include "guidata/optionseditor.hxx"
#include "guicore/timesingleselection.hxx"
#include "guicore/timeboundselection.hxx"
#include "core/threading.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"
#include "clicore/terminaloutput.hxx"

#endif

#include "arguments.hxx"
#include "execthread.hxx"

using namespace CPD3;
using namespace CPD3::CLI;
using namespace CPD3::Data;

#ifdef BUILD_GUI

static void addDefaultDisplays(QWidget *parent,
                               QVBoxLayout *layout,
                               const QString &display,
                               const QString &description = {},
                               const QString &defaultBehavior = {})
{
    if (!display.isEmpty()) {
        auto label = new QLabel(display, parent);
        label->setWordWrap(true);
        layout->addWidget(label);
    }
    if (!description.isEmpty()) {
        auto label = new QLabel(description, parent);
        label->setWordWrap(true);
        layout->addWidget(label);
    }
    if (!defaultBehavior.isEmpty()) {
        auto label = new QLabel(QObject::tr("Default: %1").arg(description), parent);
        label->setWordWrap(true);
        layout->addWidget(label);
    }
}

#endif

std::vector<Arguments::Tab> Arguments::constructTabs(QWidget *parent,
                                                     Threading::Signal<> &accept,
                                                     Threading::Signal<> &updateValid,
                                                     std::vector<std::function<bool()>> &validity)
{
#ifndef BUILD_GUI
    Q_UNUSED(parent);
    Q_UNUSED(accept);
    Q_ASSERT(false);
    return {};
#else
    std::vector<Arguments::Tab> result;

    struct Context {
        Archive::Access &archive;
        Archive::Access::ReadLock lock;
        Threading::Signal<const Archive::Access::SelectedComponents &> available;
        Threading::Signal<const SequenceName::ComponentSet &> implied;

        explicit Context(Archive::Access &archive) : archive(archive), lock(archive)
        { }

        ~Context() = default;
    };
    auto context = std::make_shared<Context>(thread->archive);
    bool needArchiveAvailable = false;

    if (!outputOptions.getAll().empty()) {
        auto editor = new GUI::Data::OptionEditor(parent);
        editor->setOptions(outputOptions);
        result.emplace_back(QObject::tr("Options"), editor);

        accept.connect([this, editor] {
            outputOptions = editor->getOptions();
        });

        needArchiveAvailable = true;
        context->available
               .connect(editor, [editor](const Archive::Access::SelectedComponents &available) {
                   editor->setAvailableStations(available.stations);
                   editor->setAvailableArchives(available.archives);
                   editor->setAvailableVariables(available.variables);
                   editor->setAvailableFlavors(available.flavors);
               });
    }

    for (const auto &weak : outputs) {
        auto check = weak.lock();
        if (!check)
            continue;
        if (auto station = std::dynamic_pointer_cast<Stations>(check)) {
            needArchiveAvailable = true;

            auto container = new QWidget(parent);
            auto layout = new QVBoxLayout(container);
            layout->setContentsMargins(0, 0, 0, 0);
            container->setLayout(layout);
            addDefaultDisplays(container, layout, station->display, station->description,
                               station->defaultBehavior);

            if (station->minimumRequired == 1 && station->maximumAllowed == 1) {
                auto select = new QComboBox(container);
                layout->addWidget(select);
                select->setEditable(true);
                select->setDuplicatesEnabled(false);
                select->setInsertPolicy(QComboBox::NoInsert);
                select->setCurrentText({});
                select->setToolTip(QObject::tr("Select or enter a station"));
                select->setWhatsThis(
                        QObject::tr("This is selection for a single station used by the script."));

                accept.connect([select, station] {
                    auto selected = select->currentText();
                    if (selected.isEmpty())
                        return;
                    station->result.clear();
                    station->result.insert(selected.toLower().toStdString());
                    station->ready = true;
                });

                context->available
                       .connect(select,
                                [select](const Archive::Access::SelectedComponents &available) {
                                    QStringList sorted;
                                    for (const auto &add : available.stations) {
                                        sorted.append(QString::fromStdString(add).toUpper());
                                    }
                                    std::sort(sorted.begin(), sorted.end());
                                    select->insertItems(0, sorted);
                                });
                context->implied
                       .connect(select, [select](const SequenceName::ComponentSet &stations) {
                           if (stations.empty())
                               return;
                           if (!select->currentText().isEmpty() &&
                                   select->currentText().toLower() != "nil")
                               return;
                           auto selected = *(stations.begin());
                           for (int i = 0, max = select->count(); i < max; i++) {
                               if (select->itemText(i) ==
                                       QString::fromStdString(selected).toUpper()) {
                                   select->setCurrentIndex(i);
                                   break;
                               }
                           }
                       });

                validity.emplace_back([select] {
                    return !select->currentText().isEmpty();
                });
                QObject::connect(select, &QComboBox::currentTextChanged, parent,
                                 std::bind(&Threading::Signal<>::e<>, &updateValid));

                layout->addStretch(1);
            } else {
                auto select = new QListWidget(container);
                layout->addWidget(select, 1);
                select->setSortingEnabled(true);
                select->setSelectionMode(QAbstractItemView::MultiSelection);
                select->setToolTip(QObject::tr("Select stations"));
                select->setWhatsThis(
                        QObject::tr("This is selection for stations used by the script."));

                accept.connect([select, station] {
                    station->result.clear();
                    for (const auto &item : select->selectedItems()) {
                        station->result.insert(item->text().toLower().toStdString());
                    }
                    if (station->result.size() < station->getMinimumRequired())
                        return;
                    if (station->result.size() > station->getMaximumAllowed())
                        return;
                    station->ready = true;
                });

                context->available
                       .connect(select,
                                [select](const Archive::Access::SelectedComponents &available) {
                                    QStringList sorted;
                                    for (const auto &add : available.stations) {
                                        sorted.append(QString::fromStdString(add).toUpper());
                                    }
                                    std::sort(sorted.begin(), sorted.end());
                                    select->insertItems(0, sorted);
                                });
                context->implied
                       .connect(select, [select](const SequenceName::ComponentSet &stations) {
                           if (stations.empty())
                               return;
                           if (!select->selectedItems().isEmpty())
                               return;
                           for (const auto &station : stations) {
                               for (auto add : select->findItems(QString::fromStdString(station),
                                                                 Qt::MatchFixedString)) {
                                   add->setSelected(true);
                               }
                           }
                       });

                validity.emplace_back([select, station] {
                    auto n = static_cast<std::size_t>(select->selectedItems().size());
                    if (n < station->getMinimumRequired())
                        return false;
                    if (n > station->getMaximumAllowed())
                        return false;
                    return true;
                });
                QObject::connect(select, &QListWidget::itemSelectionChanged, parent,
                                 std::bind(&Threading::Signal<>::e<>, &updateValid));

                auto add = new QPushButton(QObject::tr("Other"), container);
                layout->addWidget(add);
                add->setToolTip(QObject::tr("Add a station not currently listed"));
                add->setWhatsThis(QObject::tr("Use this to add a station not currently listed."));
                QObject::connect(add, &QPushButton::clicked, select, [select] {
                    bool ok;
                    auto station = QInputDialog::getText(select, QObject::tr("Add Station"),
                                                         QObject::tr("Add station:"),
                                                         QLineEdit::Normal, QString(), &ok);
                    if (!ok || station.isEmpty())
                        return;
                    station = station.toUpper();
                    {
                        auto check = select->findItems(station, Qt::MatchFixedString);
                        if (!check.isEmpty()) {
                            for (auto add : check) {
                                add->setSelected(true);
                            }
                            return;
                        }
                    }
                    auto item = new QListWidgetItem(station);
                    select->addItem(item);
                    item->setSelected(true);
                });
            }

            result.emplace_back(station->name, container);
        } else if (auto timePoint = std::dynamic_pointer_cast<TimePoint>(check)) {
            auto container = new QWidget(parent);
            auto layout = new QVBoxLayout(container);
            layout->setContentsMargins(0, 0, 0, 0);
            container->setLayout(layout);
            addDefaultDisplays(container, layout, timePoint->display, timePoint->description,
                               timePoint->defaultBehavior);

            auto select = new CPD3::GUI::TimeSingleSelection(container);
            layout->addWidget(select);
            select->setToolTip(QObject::tr("Select time"));
            select->setWhatsThis(QObject::tr("This is selection for a time used by the script."));
            select->setAllowUndefined(timePoint->allowUndefined);

            accept.connect([select, timePoint] {
                timePoint->result = select->getTime();
                if (!timePoint->allowUndefined && !FP::defined(timePoint->result))
                    return;
                timePoint->ready = true;
            });

            if (!timePoint->allowUndefined) {
                validity.emplace_back([select] {
                    return FP::defined(select->getTime());
                });
                QObject::connect(select, &CPD3::GUI::TimeSingleSelection::changed, parent,
                                 std::bind(&Threading::Signal<>::e<>, &updateValid));
            }

            layout->addStretch(1);
            result.emplace_back(timePoint->name, container);
        } else if (auto bound = std::dynamic_pointer_cast<Bounds>(check)) {
            auto container = new QWidget(parent);
            auto layout = new QVBoxLayout(container);
            layout->setContentsMargins(0, 0, 0, 0);
            container->setLayout(layout);
            addDefaultDisplays(container, layout, bound->display, bound->description,
                               bound->defaultBehavior);

            auto select = new CPD3::GUI::TimeBoundSelection(container);
            layout->addWidget(select);
            select->setToolTip(QObject::tr("Select bounds"));
            select->setWhatsThis(
                    QObject::tr("This is selection for time bounds used by the script."));
            select->setAllowUndefinedStart(bound->allowUndefined);
            select->setAllowUndefinedEnd(bound->allowUndefined);

            accept.connect([select, bound] {
                bound->result.setStart(select->getStart());
                bound->result.setEnd(select->getEnd());
                if (!bound->allowUndefined &&
                        (!FP::defined(bound->result.getStart()) ||
                                !FP::defined(bound->result.getEnd())))
                    return;
                bound->ready = true;
            });

            if (!bound->allowUndefined) {
                validity.emplace_back([select] {
                    return FP::defined(select->getStart()) && FP::defined(select->getEnd());
                });
                QObject::connect(select, &CPD3::GUI::TimeBoundSelection::boundsChanged, parent,
                                 std::bind(&Threading::Signal<>::e<>, &updateValid));
            }

            layout->addStretch(1);
            result.emplace_back(bound->name, container);
        } else if (auto stream = std::dynamic_pointer_cast<StreamFile>(check)) {
            auto container = new QWidget(parent);
            auto layout = new QVBoxLayout(container);
            layout->setContentsMargins(0, 0, 0, 0);
            container->setLayout(layout);
            addDefaultDisplays(container, layout, stream->display, stream->description);

            auto lineContainer = new QWidget(container);
            layout->addWidget(lineContainer);
            auto lineLayout = new QHBoxLayout(lineContainer);
            lineLayout->setContentsMargins(0, 0, 0, 0);
            lineContainer->setLayout(lineLayout);

            auto label = new QLabel(QObject::tr("&File:"), lineContainer);
            lineLayout->addWidget(label);

            auto select = new QLineEdit(lineContainer);
            lineLayout->addWidget(select, 1);
            select->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Ignored,
                                              QSizePolicy::LineEdit));
            label->setBuddy(select);
            if (dynamic_cast<ReadStream *>(stream.get()) != nullptr) {
                select->setToolTip(QObject::tr("The filename that the script will read from"));
                select->setWhatsThis(QObject::tr(
                        "This sets the input file name that the script will read from."));

                auto button = new QPushButton(
                        QApplication::style()->standardIcon(QStyle::SP_DialogOpenButton), QString(),
                        lineContainer);
                button->setContentsMargins(0, 0, 0, 0);
                lineLayout->addWidget(button);
                button->setToolTip(QObject::tr("Select input file"));
                button->setWhatsThis(
                        QObject::tr("Press this button to select the file to read from."));

                QObject::connect(button, &QPushButton::clicked, lineContainer, [button, select] {
                    auto file = QFileDialog::getOpenFileName(button, QString(), select->text());
                    if (file.isEmpty())
                        return;
                    select->setText(file);
                });
            } else {
                select->setToolTip(QObject::tr("The filename that the script will write to"));
                select->setWhatsThis(QObject::tr(
                        "This sets the input file name that the script will write to."));

                auto button = new QPushButton(
                        QApplication::style()->standardIcon(QStyle::SP_DialogSaveButton), QString(),
                        lineContainer);
                button->setContentsMargins(0, 0, 0, 0);
                lineLayout->addWidget(button);
                button->setToolTip(QObject::tr("Select output file"));
                button->setWhatsThis(
                        QObject::tr("Press this button to select the file to write to."));

                QObject::connect(button, &QPushButton::clicked, lineContainer, [button, select] {
                    auto file = QFileDialog::getSaveFileName(button, QString(), select->text());
                    if (file.isEmpty())
                        return;
                    select->setText(file);
                });
            }

            accept.connect([select, stream] {
                stream->filename = select->text().toStdString();
                if (stream->filename.empty())
                    return;
                stream->ready = true;
            });

            validity.emplace_back([select] {
                return !select->text().isEmpty();
            });
            QObject::connect(select, &QLineEdit::textChanged, parent,
                             std::bind(&Threading::Signal<>::e<>, &updateValid));

            layout->addStretch(1);
            result.emplace_back(stream->name, container);
        } else if (auto pipelineInput = std::dynamic_pointer_cast<PipelineInput>(check)) {
            if (!pipelineInput->origin)
                continue;
            needArchiveAvailable = true;

            auto container = new QWidget(parent);
            auto layout = new QVBoxLayout(container);
            layout->setContentsMargins(0, 0, 0, 0);
            container->setLayout(layout);
            addDefaultDisplays(container, layout, pipelineInput->display);

            enum {
                Input_Archive, Input_File,
            };
            auto type = new QComboBox(container);
            layout->addWidget(type);
            type->setToolTip(QObject::tr("Select input source"));
            type->setWhatsThis(QObject::tr("Select the type of input for the script."));

            {
                type->addItem(QObject::tr("Read data from the archive"), Input_Archive);

                auto pane = new QWidget(container);
                layout->addWidget(pane);
                auto paneLayout = new QVBoxLayout(pane);
                paneLayout->setContentsMargins(0, 0, 0, 0);
                QObject::connect(type,
                                 static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
                                 parent, [type, pane] {
                            pane->setVisible(type->currentData().toInt() == Input_Archive);
                        });

                auto bounds = new CPD3::GUI::TimeBoundSelection(pane);
                paneLayout->addWidget(bounds);
                bounds->setToolTip(QObject::tr("The time range of data loaded"));
                bounds->setWhatsThis(QObject::tr(
                        "This sets the time range of data to be loaded from the archive."));
                bounds->setAllowUndefinedStart(true);
                bounds->setAllowUndefinedEnd(true);
                {
                    QSizePolicy policy(bounds->sizePolicy());
                    policy.setVerticalPolicy(QSizePolicy::MinimumExpanding);
                    bounds->setSizePolicy(policy);
                }

                auto select = new CPD3::GUI::Data::VariableSelect(pane);
                paneLayout->addWidget(select, 1);

                context->available
                       .connect(select,
                                [select](const Archive::Access::SelectedComponents &available) {
                                    select->setAvailableStations(available.stations);
                                    select->setAvailableArchives(available.archives);
                                    select->setAvailableVariables(available.variables);
                                    select->setAvailableFlavors(available.flavors);
                                });
                auto defaults =
                        std::make_shared<CPD3::GUI::Data::VariableSelect::ArchiveDefaults>();
                defaults->setArchive("raw");
                defaults->setEmptySelectAll(false);
                context->implied
                       .connect(select, [defaults](const SequenceName::ComponentSet &stations) {
                           if (stations.empty())
                               return;
                           defaults->setStation(*stations.begin());
                       });

                accept.connect([bounds, select, type, pipelineInput, defaults] {
                    if (type->currentData().toInt() != Input_Archive)
                        return;

                    pipelineInput->archive =
                            select->getArchiveSelection(*defaults, bounds->getStart(),
                                                        bounds->getEnd());
                    pipelineInput->clip.setStart(bounds->getStart());
                    pipelineInput->clip.setEnd(bounds->getEnd());
                    pipelineInput->mode = PipelineInput::Mode::Archive;
                    pipelineInput->ready = true;
                });
            }

            {
                type->addItem(QObject::tr("Read data from a file"), Input_File);

                auto pane = new QWidget(container);
                layout->addWidget(pane);
                auto paneLayout = new QVBoxLayout(pane);
                paneLayout->setContentsMargins(0, 0, 0, 0);
                QObject::connect(type,
                                 static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
                                 parent, [type, pane] {
                            pane->setVisible(type->currentData().toInt() == Input_File);
                        });
                pane->setVisible(false);

                auto lineContainer = new QWidget(pane);
                paneLayout->addWidget(lineContainer);
                auto lineLayout = new QHBoxLayout(lineContainer);
                lineLayout->setContentsMargins(0, 0, 0, 0);
                lineContainer->setLayout(lineLayout);

                auto label = new QLabel(QObject::tr("&File:"), lineContainer);
                lineLayout->addWidget(label);

                auto select = new QLineEdit(lineContainer);
                lineLayout->addWidget(select, 1);
                select->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Ignored,
                                                  QSizePolicy::LineEdit));
                label->setBuddy(select);
                select->setToolTip(QObject::tr("The filename that the script will read data from"));
                select->setWhatsThis(QObject::tr(
                        "This sets the input file name that the script will read data from."));

                auto button = new QPushButton(
                        QApplication::style()->standardIcon(QStyle::SP_DialogOpenButton), QString(),
                        lineContainer);
                button->setContentsMargins(0, 0, 0, 0);
                lineLayout->addWidget(button);
                button->setToolTip(QObject::tr("Select input file"));
                button->setWhatsThis(
                        QObject::tr("Press this button to select the file to read data from."));
                QObject::connect(button, &QPushButton::clicked, lineContainer, [button, select] {
                    QStringList filters;
                    filters << QObject::tr("CPD3 data (*.c3d)");
                    filters << QObject::tr("CPD3 binary data (*.c3r)");
                    filters << QObject::tr("CPD3 XML data (*.xml)");
                    filters << QObject::tr("CPD3 JSON data (*.json)");
                    filters << QObject::tr("Any files (*)");

                    auto file = QFileDialog::getSaveFileName(button, QString(), select->text(),
                                                             filters.join(";;"));
                    if (file.isEmpty())
                        return;

                    select->setText(file);
                });

                accept.connect([type, select, pipelineInput] {
                    if (type->currentData().toInt() != Input_File)
                        return;

                    pipelineInput->filename = select->text().toStdString();
                    if (pipelineInput->filename.empty())
                        return;
                    pipelineInput->mode = PipelineInput::Mode::File;
                    pipelineInput->ready = true;
                });

                validity.emplace_back([type, select] {
                    if (type->currentData().toInt() != Input_File)
                        return true;
                    return !select->text().isEmpty();
                });
                QObject::connect(select, &QLineEdit::textChanged, parent,
                                 std::bind(&Threading::Signal<>::e<>, &updateValid));

                paneLayout->addStretch(1);
            }

            QObject::connect(type,
                             static_cast<void (QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
                             parent, std::bind(&Threading::Signal<>::e<>, &updateValid));

            result.emplace_back(pipelineInput->name, container);
        } else if (auto pipelineOutput = std::dynamic_pointer_cast<PipelineOutput>(check)) {
            if (!pipelineOutput->origin)
                continue;

            auto container = new QWidget(parent);
            auto layout = new QVBoxLayout(container);
            layout->setContentsMargins(0, 0, 0, 0);
            container->setLayout(layout);
            addDefaultDisplays(container, layout, pipelineOutput->display);

            auto lineContainer = new QWidget(container);
            layout->addWidget(lineContainer);
            auto lineLayout = new QHBoxLayout(lineContainer);
            lineLayout->setContentsMargins(0, 0, 0, 0);
            lineContainer->setLayout(lineLayout);

            auto label = new QLabel(QObject::tr("&File:"), lineContainer);
            lineLayout->addWidget(label);

            auto select = new QLineEdit(lineContainer);
            lineLayout->addWidget(select, 1);
            select->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Ignored,
                                              QSizePolicy::LineEdit));
            label->setBuddy(select);
            select->setToolTip(QObject::tr("The filename that the script will write data to"));
            select->setWhatsThis(QObject::tr(
                    "This sets the input file name that the script will write data to."));

            auto button = new QPushButton(
                    QApplication::style()->standardIcon(QStyle::SP_DialogSaveButton), QString(),
                    lineContainer);
            button->setContentsMargins(0, 0, 0, 0);
            lineLayout->addWidget(button);
            button->setToolTip(QObject::tr("Select input file"));
            button->setWhatsThis(
                    QObject::tr("Press this button to select the file to write data to."));
            QObject::connect(button, &QPushButton::clicked, lineContainer, [button, select] {
                QStringList filters;
                filters << QObject::tr("CPD3 data (*.c3d)");
                filters << QObject::tr("CPD3 binary data (*.c3r)");
                filters << QObject::tr("CPD3 XML data (*.xml)");
                filters << QObject::tr("CPD3 JSON data (*.json)");
                filters << QObject::tr("Any files (*)");

                QString selectedFilter;
                auto file = QFileDialog::getSaveFileName(button, QString(), select->text(),
                                                         filters.join(";;"), &selectedFilter);
                if (file.isEmpty())
                    return;

                QFileInfo info(file);
                if (info.completeSuffix().isEmpty() && !info.exists()) {
                    if (selectedFilter == QObject::tr("CPD3 data (*.c3d)")) {
                        file.append(QObject::tr(".c3d", "default extension"));
                    } else if (selectedFilter == QObject::tr("CPD3 binary data (*.c3r)")) {
                        file.append(QObject::tr(".c3r", "default extension"));
                    } else if (selectedFilter == QObject::tr("CPD3 XML data (*.xml)")) {
                        file.append(QObject::tr(".xml", "default extension"));
                    } else if (selectedFilter == QObject::tr("CPD3 XML data (*.json)")) {
                        file.append(QObject::tr(".json", "default extension"));
                    }
                }

                select->setText(file);
            });

            accept.connect([select, pipelineOutput] {
                pipelineOutput->filename = select->text().toStdString();
                if (pipelineOutput->filename.empty())
                    return;
                pipelineOutput->mode = PipelineOutput::Mode::File;
                pipelineOutput->ready = true;
            });

            validity.emplace_back([select] {
                return !select->text().isEmpty();
            });
            QObject::connect(select, &QLineEdit::textChanged, parent,
                             std::bind(&Threading::Signal<>::e<>, &updateValid));

            layout->addStretch(1);
            result.emplace_back(pipelineOutput->name, container);
        } else if (auto pipelineExternalSinkOutput =
                std::dynamic_pointer_cast<PipelineExternalSinkOutput>(check)) {
            if (!pipelineExternalSinkOutput->origin)
                continue;

            auto container = new QWidget(parent);
            auto layout = new QVBoxLayout(container);
            layout->setContentsMargins(0, 0, 0, 0);
            container->setLayout(layout);
            addDefaultDisplays(container, layout, pipelineExternalSinkOutput->display);

            auto lineContainer = new QWidget(container);
            layout->addWidget(lineContainer);
            auto lineLayout = new QHBoxLayout(lineContainer);
            lineLayout->setContentsMargins(0, 0, 0, 0);
            lineContainer->setLayout(lineLayout);

            auto label = new QLabel(QObject::tr("&File:"), lineContainer);
            lineLayout->addWidget(label);

            auto select = new QLineEdit(lineContainer);
            lineLayout->addWidget(select, 1);
            select->setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Ignored,
                                              QSizePolicy::LineEdit));
            label->setBuddy(select);
            select->setToolTip(QObject::tr("The filename that the script will read from"));
            select->setWhatsThis(
                    QObject::tr("This sets the input file name that the script will read from."));

            auto button = new QPushButton(
                    QApplication::style()->standardIcon(QStyle::SP_DialogSaveButton), QString(),
                    lineContainer);
            button->setContentsMargins(0, 0, 0, 0);
            lineLayout->addWidget(button);
            button->setToolTip(QObject::tr("Select input file"));
            button->setWhatsThis(QObject::tr("Press this button to select the file to read from."));
            QObject::connect(button, &QPushButton::clicked, lineContainer, [button, select] {
                QStringList filters;
                filters << QObject::tr("Text files (*.txt *.text)");
                filters << QObject::tr("Any files (*)");

                QString selectedFilter;
                QString file(QFileDialog::getSaveFileName(button, QString(), select->text(),
                                                          filters.join(";;"), &selectedFilter));
                if (file.isEmpty())
                    return;

                QFileInfo info(file);
                if (info.completeSuffix().isEmpty() && !info.exists()) {
                    if (selectedFilter == QObject::tr("Text files (*.txt *.text)")) {
                        file.append(QObject::tr(".txt", "default extension"));
                    }
                }

                select->setText(file);
            });

            accept.connect([select, pipelineExternalSinkOutput] {
                pipelineExternalSinkOutput->filename = select->text().toStdString();
                if (pipelineExternalSinkOutput->filename.empty())
                    return;
                pipelineExternalSinkOutput->mode = PipelineExternalSinkOutput::Mode::File;
                pipelineExternalSinkOutput->ready = true;
            });

            validity.emplace_back([select] {
                return !select->text().isEmpty();
            });
            QObject::connect(select, &QLineEdit::textChanged, parent,
                             std::bind(&Threading::Signal<>::e<>, &updateValid));

            layout->addStretch(1);
            result.emplace_back(pipelineExternalSinkOutput->name, container);
        }
    }

    if (needArchiveAvailable) {
        Threading::pollFuture(parent, thread->archive.availableSelectedFuture(),
                              [context](const Archive::Access::SelectedComponents &result) {
                                  context->available(result);
                                  context->implied(
                                          SequenceName::impliedStations(&context->archive));
                              });
    }

    return result;
#endif
}

bool Arguments::evaluateGUI()
{
#ifndef BUILD_GUI
    Q_ASSERT(false);
    return false;
#else


    bool ok = false;
    Threading::runBlockingFunctor(thread, [this, &ok]() {
        QDialog dialog;
        dialog.setWindowTitle(QObject::tr("CPD3 Script"));
        dialog.setWindowModality(Qt::ApplicationModal);

        auto layout = new QVBoxLayout(&dialog);
        dialog.setLayout(layout);

        Threading::Signal<> accept;
        Threading::Signal<> updateValid;
        std::vector<std::function<bool()>> validity;
        auto tabs = constructTabs(&dialog, accept, updateValid, validity);
        if (tabs.empty()) {
            ok = true;
            return;
        }
        if (tabs.size() == 1) {
            layout->addWidget(tabs.front().tab, 1);
        } else {
            auto tabWidget = new QTabWidget(&dialog);
            layout->addWidget(tabWidget, 1);
            for (const auto &tab : tabs) {
                tabWidget->addTab(tab.tab, tab.title);
            }
        }

        auto buttons = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
                                            Qt::Horizontal, &dialog);
        layout->addWidget(buttons);
        QObject::connect(buttons, &QDialogButtonBox::accepted, &dialog, &QDialog::accept);
        QObject::connect(buttons, &QDialogButtonBox::rejected, &dialog, &QDialog::reject);

        updateValid.connect(buttons, [buttons, &validity]() {
            auto ok = buttons->button(QDialogButtonBox::Ok);
            if (!ok)
                return;
            bool enabled = true;
            for (const auto &update : validity) {
                if (!update()) {
                    enabled = false;
                    break;
                }
            }
            ok->setEnabled(enabled);
        });
        updateValid();

        if (dialog.exec() != QDialog::Accepted) {
            ok = false;
            return;
        }

        accept();
        ok = true;
    });
    if (!ok) {
        Threading::runQueuedFunctor(thread, std::bind(&QCoreApplication::exit, 0));
        return false;
    }

    /* Display outputs require no configuration, they just show the window */
    for (const auto &weak : outputs) {
        auto check = weak.lock();
        if (!check)
            continue;
        if (auto pipelineDisplayOutput = std::dynamic_pointer_cast<PipelineDisplayOutput>(check)) {
            if (!pipelineDisplayOutput->origin)
                continue;
            pipelineDisplayOutput->filename.clear();
            pipelineDisplayOutput->ready = true;
        }
    }

    return true;
#endif
}