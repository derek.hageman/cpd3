/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3LUAEXEC_ARGUMENTS_HXX
#define CPD3LUAEXEC_ARGUMENTS_HXX

#include "core/first.hxx"

#include <cstdint>
#include <QString>

#include "core/component.hxx"
#include "core/threading.hxx"
#include "luascript/engine.hxx"
#include "datacore/stream.hxx"
#include "datacore/streampipeline.hxx"
#include "datacore/archive/selection.hxx"

class ExecThread;

class LuaPipeline;

class QWidget;

class Arguments final {
    ExecThread *thread;
    QStringList commandArguments;
    bool usingGUI;
    bool evaluated;

    class Output {
        friend class Arguments;

    protected:
        Arguments &arguments;
    private:
        bool ready;
    public:
        Output() = delete;

        Output(Arguments &arguments);

        virtual ~Output();

        bool fetch(CPD3::Lua::Engine::Frame &frame);
    };

    friend class Output;

    std::vector<std::weak_ptr<Output>> outputs;

    CPD3::ComponentOptions outputOptions;

    void argumentsAbort(const QString &text, bool isError = true);

    bool evaluateArguments();

    struct Tab {
        QString title;
        QWidget *tab;

        inline Tab() = default;

        inline Tab(const QString &title, QWidget *tab) : title(std::move(title)), tab(tab)
        { }
    };

    std::vector<Tab> constructTabs(QWidget *parent,
                                   CPD3::Threading::Signal<> &accept,
                                   CPD3::Threading::Signal<> &updateValid,
                                   std::vector<std::function<bool()>> &validity);

    bool evaluateGUI();

public:
    Arguments(ExecThread *thread, const QStringList &commandArguments, bool usingGUI);

    ~Arguments();

    void install(CPD3::Lua::Engine::Frame &root);

    void evaluate(CPD3::Lua::Engine::Frame &frame);

    CPD3::Threading::Signal<CPD3::Lua::Engine::Frame &> ready;

    class Option final : public Output {
        friend class Arguments;

        std::unique_ptr<CPD3::ComponentOptionBase> option;
        QString optionID;

        CPD3::ComponentOptionBase *retrieve(CPD3::Lua::Engine::Frame &frame);

    public:
        Option(Arguments &arguments, std::unique_ptr<CPD3::ComponentOptionBase> &&option);

        Option() = delete;

        virtual ~Option();

        template<typename T = CPD3::ComponentOptionBase>
        T *value(CPD3::Lua::Engine::Frame &frame)
        { return qobject_cast<T *>(retrieve(frame)); }

        template<typename T = CPD3::ComponentOptionBase>
        T *modify() const
        { return qobject_cast<T *>(option.get()); }
    };

    std::shared_ptr<Option> option(std::unique_ptr<CPD3::ComponentOptionBase> &&opt);

    inline std::shared_ptr<Option> option(CPD3::ComponentOptionBase *opt)
    { return option(std::unique_ptr<CPD3::ComponentOptionBase>(opt)); }

    class Stations final : public Output {
        friend class Arguments;

        QString name;
        QString display;
        QString description;
        QString defaultBehavior;
        std::size_t minimumRequired;
        std::size_t maximumAllowed;

        CPD3::Data::SequenceName::ComponentSet result;
    public:
        Stations(Arguments &arguments,
                 const QString &name,
                 const QString &display,
                 const QString &description,
                 const QString &defaultBehavior);

        Stations() = delete;

        virtual ~Stations();

        CPD3::Data::SequenceName::ComponentSet value(CPD3::Lua::Engine::Frame &frame);

        inline std::size_t getMinimumRequired() const
        { return minimumRequired; }

        inline void setMinimumRequired(std::size_t n)
        { minimumRequired = n; }

        inline std::size_t getMaximumAllowed() const
        { return maximumAllowed; }

        inline void setMaximumAllowed(std::size_t n)
        { maximumAllowed = n; }
    };

    std::shared_ptr<Stations> stations(const QString &name, const QString &display,
                                       const QString &description,
                                       const QString &defaultBehavior);

    class TimePoint final : public Output {
        friend class Arguments;

        QString name;
        QString display;
        QString description;
        QString defaultBehavior;
        bool allowUndefined;
        CPD3::Time::LogicalTimeUnit defaultUnit;

        double result;
    public:
        TimePoint(Arguments &arguments, const QString &name, const QString &display,
                  const QString &description,
                  const QString &defaultBehavior);

        TimePoint() = delete;

        virtual ~TimePoint();

        double value(CPD3::Lua::Engine::Frame &frame);

        inline void setAllowUndefined(bool allow)
        { allowUndefined = allow; }

        inline void setDefaultUnit(CPD3::Time::LogicalTimeUnit unit)
        { defaultUnit = unit; }
    };

    std::shared_ptr<TimePoint> time(const QString &name, const QString &display,
                                    const QString &description,
                                    const QString &defaultBehavior);

    class Bounds final : public Output {
        friend class Arguments;

        QString name;
        QString display;
        QString description;
        QString defaultBehavior;
        bool allowUndefined;
        CPD3::Time::LogicalTimeUnit defaultUnit;

        CPD3::Time::Bounds result;
    public:
        Bounds(Arguments &arguments, const QString &name, const QString &display,
               const QString &description,
               const QString &defaultBehavior);

        Bounds() = delete;

        virtual ~Bounds();

        CPD3::Time::Bounds value(CPD3::Lua::Engine::Frame &frame);

        inline void setAllowUndefined(bool allow)
        { allowUndefined = allow; }

        inline void setDefaultUnit(CPD3::Time::LogicalTimeUnit unit)
        { defaultUnit = unit; }
    };

    std::shared_ptr<Bounds> bounds(const QString &name, const QString &display,
                                   const QString &description,
                                   const QString &defaultBehavior);

    class StreamFile : public Output {
        friend class Arguments;

        QString name;
        QString display;
        QString description;

    protected:
        std::string filename;
    public:
        StreamFile(Arguments &arguments,
                   const QString &name,
                   const QString &display,
                   const QString &description);

        StreamFile() = delete;

        virtual ~StreamFile();

        virtual void push(CPD3::Lua::Engine::Frame &frame, bool textMode = true) = 0;
    };

    class ReadStream final : public StreamFile {
        friend class Arguments;

    public:
        ReadStream(Arguments &arguments,
                   const QString &name,
                   const QString &display,
                   const QString &description);

        ReadStream() = delete;

        virtual ~ReadStream();

        void push(CPD3::Lua::Engine::Frame &frame, bool textMode = true) override;
    };

    std::shared_ptr<ReadStream> readStream(const QString &name,
                                           const QString &display,
                                           const QString &description);

    class WriteStream final : public StreamFile {
        friend class Arguments;

    public:
        WriteStream(Arguments &arguments,
                    const QString &name,
                    const QString &display,
                    const QString &description);

        WriteStream() = delete;

        virtual ~WriteStream();

        void push(CPD3::Lua::Engine::Frame &frame, bool textMode = true) override;
    };

    std::shared_ptr<WriteStream> writeStream(const QString &name,
                                             const QString &display,
                                             const QString &description);


    class Pipeline : public Output {
        friend class Arguments;

        LuaPipeline *origin;
        QString name;
        QString display;
    public:
        Pipeline(Arguments &arguments,
                 LuaPipeline *origin, const QString &name, const QString &display);

        Pipeline() = delete;

        virtual ~Pipeline();

        inline void disconnect()
        { origin = nullptr; }

        virtual bool configure(CPD3::Data::StreamPipeline *pipeline) = 0;
    };

    class PipelineInput final : public Pipeline {
        friend class Arguments;

        enum class Mode {
            Pipeline, File, Archive, Requested,
        } mode;
        std::string filename;
        CPD3::Data::Archive::Selection::List archive;
        CPD3::Time::Bounds clip;
    public:
        PipelineInput(Arguments &arguments,
                      LuaPipeline *origin, const QString &name, const QString &display);

        PipelineInput() = delete;

        virtual ~PipelineInput();

        bool configure(CPD3::Data::StreamPipeline *pipeline) override;
    };

    std::shared_ptr<PipelineInput> pipelineInput(LuaPipeline *origin,
                                                 const QString &name,
                                                 const QString &display);

    class PipelineOutput final : public Pipeline {
        friend class Arguments;

        enum class Mode {
            Pipeline, File
        } mode;
        std::string filename;
    public:
        PipelineOutput(Arguments &arguments,
                       LuaPipeline *origin, const QString &name, const QString &display);

        PipelineOutput() = delete;

        virtual ~PipelineOutput();

        bool configure(CPD3::Data::StreamPipeline *pipeline) override;
    };

    std::shared_ptr<PipelineOutput> pipelineOutput(LuaPipeline *origin,
                                                   const QString &name,
                                                   const QString &display);


    class PipelineExternalSinkOutput final : public Pipeline {
        friend class Arguments;

        CPD3::Data::ExternalSinkComponent *component;
        CPD3::ComponentOptions options;
        enum class Mode {
            Pipeline, File
        } mode;
        std::string filename;
    public:
        PipelineExternalSinkOutput(Arguments &arguments,
                                   LuaPipeline *origin,
                                   const QString &name,
                                   const QString &display,
                                   CPD3::Data::ExternalSinkComponent *component,
                                   const CPD3::ComponentOptions &options);

        PipelineExternalSinkOutput() = delete;

        virtual ~PipelineExternalSinkOutput();

        bool configure(CPD3::Data::StreamPipeline *pipeline) override;
    };

    std::shared_ptr<PipelineExternalSinkOutput> pipelineExternalSinkOutput(LuaPipeline *origin,
                                                                           const QString &name,
                                                                           const QString &display,
                                                                           CPD3::Data::ExternalSinkComponent *component,
                                                                           const CPD3::ComponentOptions &options);

    class PipelineDisplayOutput final : public Pipeline {
        friend class Arguments;

        CPD3::Data::ValueSegment::Transfer config;
        std::string filename;
        int width;
        int height;

        CPD3::Time::Bounds selectionBounds;
        CPD3::Data::Archive::Selection::Match selectionStations;
        CPD3::Data::Archive::Selection::Match selectionArchives;
        CPD3::Data::Archive::Selection::Match selectionVariables;
    public:
        PipelineDisplayOutput(Arguments &arguments,
                              LuaPipeline *origin,
                              const QString &name,
                              const QString &display,
                              CPD3::Data::ValueSegment::Transfer config);

        PipelineDisplayOutput() = delete;

        virtual ~PipelineDisplayOutput();

        bool configure(CPD3::Data::StreamPipeline *pipeline) override;
    };

    std::shared_ptr<PipelineDisplayOutput> pipelineDisplayOutput(LuaPipeline *origin,
                                                                 const QString &name,
                                                                 const QString &display,
                                                                 CPD3::Data::ValueSegment::Transfer config);
};

#endif //CPD3LUAEXEC_ARGUMENTS_HXX
