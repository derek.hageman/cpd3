/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QObject>
#include <QtDebug>
#include <QProcess>
#include <QString>
#include <QTemporaryFile>

#include "core/qtcompat.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/stream.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class TestLuaScriptArguments : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

private slots:

    void initTestCase()
    {
        CPD3::Logging::suppressForTesting();

        QVERIFY(databaseFile.open());

        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());

        SequenceValue::Transfer data;
        for (int i = 0; i < 100; i++) {
            data.emplace_back(
                    SequenceValue{{"brw", "raw", "T_S11"}, Variant::Root(20.0), 1000.0 + i,
                                  1000.0 + (i + 1)});
        }
        Archive::Access(databaseFile).writeSynchronous(std::move(data));
    }

    void argumentBoolean()
    {
        QProcess p;
        p.start("da.luaexec", QStringList() << R"EOF(
a = CPD3.Arguments.Boolean("arg");
local v = a.value;
if v ~= true then error(); end
)EOF" << "--arg");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
    }

    void argumentString()
    {
        QProcess p;
        p.start("da.luaexec", QStringList() << R"EOF(
a = CPD3.Arguments.String("arg");
local v = a.value;
if v ~= "foo" then error(); end
)EOF" << "--arg=foo");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
    }

    void argumentInteger()
    {
        QProcess p;
        p.start("da.luaexec", QStringList() << R"EOF(
a = CPD3.Arguments.Integer("arg");
local v = a.value;
if v ~= 42 then error(); end
)EOF" << "--arg=42");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
    }

    void argumentReal()
    {
        QProcess p;
        p.start("da.luaexec", QStringList() << R"EOF(
a = CPD3.Arguments.Real("arg");
local v = a.value;
if v ~= 42.5 then error(); end
)EOF" << "--arg=42.5");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
    }

    void argumentTime()
    {
        QProcess p;
        p.start("da.luaexec", QStringList() << R"EOF(
a = CPD3.Arguments.OptionalTime("arg");
local v = a.value;
if v ~= 1525132800 then error(); end
)EOF" << "--arg=2018-05-01");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
    }

    void argumentFile()
    {
        QTemporaryFile file;
        QVERIFY(file.open());
        file.write("abcd");
        file.close();

        QProcess p;
        p.start("da.luaexec", QStringList() << QString(R"EOF(
a = CPD3.Arguments.File("arg");
local v = a.value;
if v ~= "%1" then error(); end
f = a("r");
local contents = f:read("*a");
if contents ~= "abcd" then error(); end
)EOF").arg(file.fileName()) << QString("--arg=%1").arg(file.fileName()));
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
    }

#ifndef NO_CONSOLE

    void argumentFileStdin()
    {
        QProcess p;
        p.start("da.luaexec", QStringList() << R"EOF(
a = CPD3.Arguments.File("arg");
local v = a.value;
f = a();
local contents = f:read("*a");
if contents ~= "abcd" then error(); end
)EOF" << "--arg=-");
        QVERIFY(p.waitForStarted());
        p.write("abcd");
        p.closeWriteChannel();
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
    }

#endif
};

QTEST_MAIN(TestLuaScriptArguments)

#include "arguments.moc"
