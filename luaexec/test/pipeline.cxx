/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QObject>
#include <QtDebug>
#include <QProcess>
#include <QString>
#include <QTemporaryFile>
#include <QBuffer>

#include "core/qtcompat.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsink.hxx"
#include "datacore/externalsource.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class TestLuaScriptPipeline : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

private slots:

    void initTestCase()
    {
        CPD3::Logging::suppressForTesting();

        QVERIFY(databaseFile.open());

        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());

        qputenv("QT_LOGGING_RULES", "cpd3.luaexec=true");

        SequenceValue::Transfer data;
        for (int i = 0; i < 100; i++) {
            data.emplace_back(
                    SequenceValue{{"brw", "raw", "T_S11"}, Variant::Root(20.0), 1000.0 + i,
                                  1000.0 + (i + 1)});
        }
        Archive::Access(databaseFile).writeSynchronous(std::move(data));
    }

    void valueGeneral()
    {
        QProcess p;
        p.start("da.luaexec", QStringList() << R"EOF(
c = CPD3.Pipeline();
c:input_archive({
    station = "brw",
    archive = "raw",
    variable = "T_S11",
});
c:output_discard();
p = c:values();
c:start();
local counter = 0;
for v in p do
    if v.value:toReal() ~= 20.0 then error(); end
    counter = counter + 1;
end
p:finish();
c:wait();
print("LUAMARKERMARKERMARKER = " .. tostring(counter));
)EOF");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
#ifndef NO_CONSOLE
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        output += QString::fromUtf8(p.readAllStandardError());
        QVERIFY(output.contains("LUAMARKERMARKERMARKER = 100"));
#endif
    }

    void segmentGeneral()
    {
        QProcess p;
        p.start("da.luaexec", QStringList() << R"EOF(
c = CPD3.Pipeline();
c:input_archive({
    station = "brw",
    archive = "raw",
    variable = "T_S11",
});
c:output_discard();
p = c:segments();
c:start();
local counter = 0;
for v in p do
    if v.T_S11 ~= 20.0 then error(); end
    counter = counter + 1;
end
c:wait();
print("LUAMARKERMARKERMARKER = " .. tostring(counter));
)EOF");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
#ifndef NO_CONSOLE
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        output += QString::fromUtf8(p.readAllStandardError());
        QVERIFY(output.contains("LUAMARKERMARKERMARKER = 100"));
#endif
    }

    void valueProcessor()
    {
        QProcess p;
        p.start("da.luaexec", QStringList() << R"EOF(
c = CPD3.Pipeline();
c:input_archive({
    station = "brw",
    archive = "raw",
    variable = "T_S11",
});
c:output_discard();
local counter = 0;
c:value_processor(function(v)
    if v.value:toReal() ~= 20.0 then error(); end
    counter = counter + 1;
end);
c:exec();
print("LUAMARKERMARKERMARKER = " .. tostring(counter));
)EOF");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
#ifndef NO_CONSOLE
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        output += QString::fromUtf8(p.readAllStandardError());
        QVERIFY(output.contains("LUAMARKERMARKERMARKER = 100"));
#endif
    }

    void segmentProcessor()
    {
        QProcess p;
        p.start("da.luaexec", QStringList() << R"EOF(
c = CPD3.Pipeline();
c:input_archive({
    station = "brw",
    archive = "raw",
    variable = "T_S11",
});
c:output_discard();
local counter = 0;
c:segment_processor(function(v)
    if v.T_S11 ~= 20.0 then error(); end
    counter = counter + 1;
end);
c();
print("LUAMARKERMARKERMARKER = " .. tostring(counter));
)EOF");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
#ifndef NO_CONSOLE
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        output += QString::fromUtf8(p.readAllStandardError());
        QVERIFY(output.contains("LUAMARKERMARKERMARKER = 100"));
#endif
    }

    void valueFanout()
    {
        QProcess p;
        p.start("da.luaexec", QStringList() << R"EOF(
c = CPD3.Pipeline();
c:input_archive({
    station = "brw",
    archive = "raw",
    variable = "T_S11",
});
c:output_discard();
local counter = 0;
c:fanout_values(function()
    counter = counter + 10;
    return function(v)
        if v.value:toReal() ~= 20.0 then error(); end
        counter = counter + 1;
    end;
end);
c();
print("LUAMARKERMARKERMARKER = " .. tostring(counter));
)EOF");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
#ifndef NO_CONSOLE
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        output += QString::fromUtf8(p.readAllStandardError());
        QVERIFY(output.contains("LUAMARKERMARKERMARKER = 110"));
#endif
    }

    void segmentFanout()
    {
        QProcess p;
        p.start("da.luaexec", QStringList() << R"EOF(
c = CPD3.Pipeline();
c:input_archive({
    station = "brw",
    archive = "raw",
    variable = "T_S11",
});
c:output_discard();
local counter = 0;
c:fanout(function()
    counter = counter + 10;
    return function(v)
        if v.T_S11 ~= 20.0 then error(); end
        counter = counter + 1;
    end;
end);
c();
print("LUAMARKERMARKERMARKER = " .. tostring(counter));
)EOF");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
#ifndef NO_CONSOLE
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        output += QString::fromUtf8(p.readAllStandardError());
        QVERIFY(output.contains("LUAMARKERMARKERMARKER = 110"));
#endif
    }

    void multiplexer()
    {
        QProcess p;
        p.start("da.luaexec", QStringList() << R"EOF(
p1 = CPD3.Pipeline();
p1:input_archive({
    station = "brw",
    archive = "raw",
    variable = "T_S11",
});
p2 = CPD3.Pipeline();
p2:input_archive({
    station = "brw",
    archive = "raw",
    variable = "T_S11",
});
local counter = 0;
c = CPD3.Pipeline();
c:input_multiplexer(p1, p2);
c:value_processor(function(v)
    if v.value:toReal() ~= 20.0 then error(); end
    counter = counter + 1;
end);
c:output_discard();
c();
print("LUAMARKERMARKERMARKER = " .. tostring(counter));
)EOF");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
#ifndef NO_CONSOLE
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        output += QString::fromUtf8(p.readAllStandardError());
        QVERIFY(output.contains("LUAMARKERMARKERMARKER = 200"));
#endif
    }

    void processor()
    {
        QProcess p;
        p.start("da.luaexec", QStringList() << R"EOF(
c = CPD3.Pipeline();
c:input_archive({
    station = "brw",
    archive = "raw",
    variable = "T_S11",
});
c:output_discard();
local counter = 0;
c:processing("calc_simple", {
   options = {
       operation = "add",
       a = "T_S11",
       b = 30.0,
       output = "T_S11",
       ["fanout-variable"] = "T_S11",
   }
});
c:value_processor(function(v)
    if v.value:toReal() ~= 50.0 then error(); end
    counter = counter + 1;
end);
c:exec();
print("LUAMARKERMARKERMARKER = " .. tostring(counter));
)EOF");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
#ifndef NO_CONSOLE
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        output += QString::fromUtf8(p.readAllStandardError());
        QVERIFY(output.contains("LUAMARKERMARKERMARKER = 100"));
#endif
    }

    void editing()
    {
        QProcess p;
        p.start("da.luaexec", QStringList() << R"EOF(
c = CPD3.Pipeline();
c:input_archive({
    station = "brw",
    archive = "raw",
    variable = "T_S11",
});
c:output_discard();
local counter = 0;
c:editing("calc_simple", {
   configuration = {
       {
           value = {
               Basic = {
                   A = "T_S11",
                   B = 30.0,
                   Operation = "Add",
                   Output = "T_S11",
               }
           }
       }
   }
});
c:value_processor(function(v)
    if v.value:toReal() ~= 50.0 then error(); end
    counter = counter + 1;
end);
c:exec();
print("LUAMARKERMARKERMARKER = " .. tostring(counter));
)EOF");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
#ifndef NO_CONSOLE
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        output += QString::fromUtf8(p.readAllStandardError());
        QVERIFY(output.contains("LUAMARKERMARKERMARKER = 100"));
#endif
    }

    void acquisition()
    {
        QProcess p;
        p.start("da.luaexec", QStringList() << R"EOF(
acq = CPD3.Acquisition("acquire_tsi_neph3563");
c = CPD3.Pipeline();
acq.data = c;
c:output_discard();
local counter = 0;
c:value_processor(function(v)
    if v.variable:sub(-3) ~= "S11" then error(); end
    if v.archive == "raw" then
        if v.variable == "Vl_S11" then
            if v.value:toReal() ~= 12.8 then error(); end
            counter = counter + 1;
        end
    end
end);
c:start();
for i=1535477837,1535477837+100 do
    acq("T,2017,10,24,15,23,23", i);
    acq("B,4717,157,2,11,3048,68,1,12,667.8,301.3", i);
    acq("G,7060,143,0,11,4672,52,0,12,667.8,301.3", i);
    acq("R,5644,162,8,11,3617,90,10,12,667.8,301.3", i);
    acq("D,NBXX,30410,-6.836e-7,2.185e-6,1.496e-6,-2.507e-6,-6.847e-7,-4.635e-7", i);
    acq("Y,133174,667.8,301.3,298.2,22.3,12.8,5.9,0,0000",i);
end
c:wait();
print("LUAMARKERMARKERMARKER = " .. tostring(counter));
)EOF");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
#ifndef NO_CONSOLE
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        output += QString::fromUtf8(p.readAllStandardError());
        QVERIFY(output.contains("LUAMARKERMARKERMARKER = 100"));
#endif
    }

    void inputSelectArchive()
    {
        QProcess p;
        p.start("da.luaexec", QStringList() << R"EOF(
c = CPD3.Pipeline();
c:input_select();
c:output_discard();
p = c:values();
c:start();
local counter = 0;
for v in p do
    if v.value:toReal() ~= 20.0 then error(); end
    counter = counter + 1;
end
p:finish();
c:wait();
print("LUAMARKERMARKERMARKER = " .. tostring(counter));
)EOF" << "brw" << "T_S11" << "forever");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
#ifndef NO_CONSOLE
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        output += QString::fromUtf8(p.readAllStandardError());
        QVERIFY(output.contains("LUAMARKERMARKERMARKER = 100"));
#endif
    }

    void inputSelectFile()
    {
        QTemporaryFile file;
        QVERIFY(file.open());
        {
            StandardDataOutput out(&file, StandardDataOutput::OutputType::Direct);
            out.start();
            for (int i = 0; i < 100; i++) {
                out.incomingData(
                        SequenceValue{{"brw", "raw", "T_S11"}, Variant::Root(21.0), 1000.0 + i,
                                      1000.0 + (i + 1)});
            }
            out.endData();
            Threading::pollInEventLoop([&] { return !out.isFinished(); });
            out.wait();
            QCoreApplication::processEvents();
        }
        file.close();

        QProcess p;
        p.start("da.luaexec", QStringList() << R"EOF(
c = CPD3.Pipeline();
c:input_select();
c:output_discard();
p = c:values();
c:start();
local counter = 0;
for v in p do
    if v.value:toReal() ~= 21.0 then error(); end
    counter = counter + 1;
end
p:finish();
c:wait();
print("LUAMARKERMARKERMARKER = " .. tostring(counter));
)EOF" << file.fileName());
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
#ifndef NO_CONSOLE
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        output += QString::fromUtf8(p.readAllStandardError());
        QVERIFY(output.contains("LUAMARKERMARKERMARKER = 100"));
#endif
    }

    void inputExplicitFile()
    {
        QTemporaryFile file;
        QVERIFY(file.open());
        {
            StandardDataOutput out(&file, StandardDataOutput::OutputType::Direct);
            out.start();
            for (int i = 0; i < 100; i++) {
                out.incomingData(
                        SequenceValue{{"brw", "raw", "T_S11"}, Variant::Root(21.0), 1000.0 + i,
                                      1000.0 + (i + 1)});
            }
            out.endData();
            Threading::pollInEventLoop([&] { return !out.isFinished(); });
            out.wait();
            QCoreApplication::processEvents();
        }
        file.close();

        QProcess p;
        p.start("da.luaexec", QStringList() << QString(R"EOF(
c = CPD3.Pipeline();
c:input_file("%1");
c:output_discard();
p = c:values();
c:start();
local counter = 0;
for v in p do
    if v.value:toReal() ~= 21.0 then error(); end
    counter = counter + 1;
end
p:finish();
c:wait();
print("LUAMARKERMARKERMARKER = " .. tostring(counter));
)EOF").arg(file.fileName()));
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
#ifndef NO_CONSOLE
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        output += QString::fromUtf8(p.readAllStandardError());
        QVERIFY(output.contains("LUAMARKERMARKERMARKER = 100"));
#endif
    }

    void inputExternalSource()
    {
        QProcess p;
        p.start("da.luaexec", QStringList() << R"EOF(
c = CPD3.Pipeline();
c:input_external("generate_edited", {
    station = "brw",
});
c:output_discard();
c();
print("LUAMARKERMARKERMARKER");
)EOF");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
#ifndef NO_CONSOLE
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        output += QString::fromUtf8(p.readAllStandardError());
        QVERIFY(output.contains("LUAMARKERMARKERMARKER"));
#endif
    }

    void inputExternalConverter()
    {
        QTemporaryFile file;
        QVERIFY(file.open());

        file.write(R"EOF(
T,2017,10,24,15,23,23
B,4717,157,2,11,3048,68,1,12,667.8,301.3
G,7060,143,0,11,4672,52,0,12,667.8,301.3
R,5644,162,8,11,3617,90,10,12,667.8,301.3
D,NBXX,30410,-6.836e-7,2.185e-6,1.496e-6,-2.507e-6,-6.847e-7,-4.635e-7
Y,133174,667.8,301.3,298.2,22.3,12.8,5.9,0,0000
T,2017,10,24,15,23,24
B,4717,157,2,11,3048,68,1,12,667.8,301.3
G,7060,143,0,11,4672,52,0,12,667.8,301.3
R,5644,162,8,11,3617,90,10,12,667.8,301.3
D,NBXX,30410,-6.836e-7,2.185e-6,1.496e-6,-2.507e-6,-6.847e-7,-4.635e-7
Y,133174,667.8,301.3,298.2,22.3,12.8,5.9,0,0000
)EOF");
        file.close();

        QProcess p;
        p.start("da.luaexec", QStringList() << QString(R"EOF(
c = CPD3.Pipeline();
c:input_external("acquire_tsi_neph3563", {
    file = "%1",
});
c:output_discard();
c();
print("LUAMARKERMARKERMARKER");
)EOF").arg(file.fileName()));
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
#ifndef NO_CONSOLE
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        output += QString::fromUtf8(p.readAllStandardError());
        QVERIFY(output.contains("LUAMARKERMARKERMARKER"));
#endif
    }

    void outputExternalFile()
    {
        QProcess p;
        p.start("da.luaexec", QStringList() << R"EOF(
c = CPD3.Pipeline();
c:input_archive({
    station = "brw",
    archive = "raw",
    variable = "T_S11",
});
c:output_external("export", {
    file = "-",
});
c();
print("LUAMARKERMARKERMARKER");
)EOF");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
#ifndef NO_CONSOLE
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        output += QString::fromUtf8(p.readAllStandardError());
        QVERIFY(output.contains("DateTimeUTC,T_S11"));
        QVERIFY(output.contains("LUAMARKERMARKERMARKER"));
#endif
    }

    void outputExternalDirect()
    {
        QProcess p;
        p.start("da.luaexec", QStringList() << R"EOF(
c = CPD3.Pipeline();
c:input_archive({
    station = "brw",
    archive = "raw",
    variable = "T_S11",
});
c:output_external("archive", {
    remove = "none",
});
c();
print("LUAMARKERMARKERMARKER");
)EOF");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
#ifndef NO_CONSOLE
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        output += QString::fromUtf8(p.readAllStandardError());
        QVERIFY(output.contains("LUAMARKERMARKERMARKER"));
#endif
    }

#ifndef NO_CONSOLE

    void inputSelectStdin()
    {
        QByteArray data;
        {
            QBuffer outBuffer(&data);
            outBuffer.open(QIODevice::WriteOnly);
            StandardDataOutput out(&outBuffer, StandardDataOutput::OutputType::Direct);
            out.start();
            for (int i = 0; i < 100; i++) {
                out.incomingData(
                        SequenceValue{{"brw", "raw", "T_S11"}, Variant::Root(21.0), 1000.0 + i,
                                      1000.0 + (i + 1)});
            }
            out.endData();
            Threading::pollInEventLoop([&] { return !out.isFinished(); });
            out.wait();
            QCoreApplication::processEvents();
        }

        QProcess p;
        p.start("da.luaexec", QStringList() << R"EOF(
c = CPD3.Pipeline();
c:input_select();
c:output_discard();
p = c:values();
c:start();
local counter = 0;
for v in p do
    if v.value:toReal() ~= 21.0 then error(); end
    counter = counter + 1;
end
p:finish();
c:wait();
print("LUAMARKERMARKERMARKER = " .. tostring(counter));
)EOF");
        QVERIFY(p.waitForStarted());
        p.write(data);
        p.closeWriteChannel();
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        output += QString::fromUtf8(p.readAllStandardError());
        QVERIFY(output.contains("LUAMARKERMARKERMARKER = 100"));
    }

    void inputExternalConverterStdin()
    {
        QProcess p;
        p.start("da.luaexec", QStringList() << R"EOF(
c = CPD3.Pipeline();
c:input_external("acquire_tsi_neph3563");
c:output_discard();
c();
print("LUAMARKERMARKERMARKER");
)EOF");
        QVERIFY(p.waitForStarted());
        p.write(R"EOF(
T,2017,10,24,15,23,23
B,4717,157,2,11,3048,68,1,12,667.8,301.3
G,7060,143,0,11,4672,52,0,12,667.8,301.3
R,5644,162,8,11,3617,90,10,12,667.8,301.3
D,NBXX,30410,-6.836e-7,2.185e-6,1.496e-6,-2.507e-6,-6.847e-7,-4.635e-7
Y,133174,667.8,301.3,298.2,22.3,12.8,5.9,0,0000
T,2017,10,24,15,23,24
B,4717,157,2,11,3048,68,1,12,667.8,301.3
G,7060,143,0,11,4672,52,0,12,667.8,301.3
R,5644,162,8,11,3617,90,10,12,667.8,301.3
D,NBXX,30410,-6.836e-7,2.185e-6,1.496e-6,-2.507e-6,-6.847e-7,-4.635e-7
Y,133174,667.8,301.3,298.2,22.3,12.8,5.9,0,0000
)EOF");
        p.closeWriteChannel();
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        output += QString::fromUtf8(p.readAllStandardError());
        QVERIFY(output.contains("LUAMARKERMARKERMARKER"));
    }

    void outputSelectStdout()
    {
        QProcess p;
        p.start("da.luaexec", QStringList() << R"EOF(
c = CPD3.Pipeline();
c:input_archive({
    station = "brw",
    archive = "raw",
    variable = "T_S11",
});
c:output_select();
c();
)EOF");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);

        StandardDataInput reader;
        StreamSink::Buffer buffer;
        reader.start();
        reader.setEgress(&buffer);
        reader.incomingData(p.readAllStandardOutput());
        reader.endData();
        QVERIFY(reader.wait());

        QCOMPARE((int) buffer.values().size(), 100);
    }

#endif

#ifdef BUILD_GUI

    void outputDisplay()
    {
        QTemporaryFile file(QDir(QDir::tempPath()).filePath("XXXXXX.png"));
        QVERIFY(file.open());
        file.close();

        QProcess p;
        p.start("da.luaexec", QStringList() << QString(R"EOF(
c = CPD3.Pipeline();
c:input_archive({
    station = "brw",
    archive = "raw",
    variable = "T_S11",
});
c:output_display("timeseries", {
    file = "%1",
    options = {
        y = "T_S11"
    },
});
c();
print("LUAMARKERMARKERMARKER");
)EOF").arg(file.fileName()));
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
#ifndef NO_CONSOLE
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        output += QString::fromUtf8(p.readAllStandardError());
        QVERIFY(output.contains("LUAMARKERMARKERMARKER"));
#endif

        QVERIFY(file.size() > 0);
    }

#endif
};

QTEST_MAIN(TestLuaScriptPipeline)

#include "pipeline.moc"
