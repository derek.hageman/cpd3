/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QObject>
#include <QtDebug>
#include <QProcess>
#include <QString>
#include <QTemporaryFile>

#include "core/qtcompat.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/stream.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class TestLuaScriptBasic : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

private slots:

    void initTestCase()
    {
        CPD3::Logging::suppressForTesting();

        QVERIFY(databaseFile.open());

        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());

        qputenv("QT_LOGGING_RULES", "cpd3.luaexec=true");

        Archive::Access(databaseFile).writeSynchronous(
                SequenceValue::Transfer{{{"brw", "raw", "T_S11"}, Variant::Root(
                        20.0), 1000.0, 2000.0}});
    }

    void evaluate()
    {
        QProcess p;
        p.start("da.luaexec", QStringList() << R"EOF(
print("LUAMARKERMARKERMARKER");
)EOF");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        output += QString::fromUtf8(p.readAllStandardError());
        QVERIFY(output.contains("LUAMARKERMARKERMARKER"));
    }

    void pipelineCreate()
    {
        QProcess p;
        p.start("da.luaexec", QStringList() << R"EOF(
c = CPD3.Pipeline();
c:input_archive({
    station = "brw",
    archive = "raw",
    variable = "T_S11",
});
c:output_discard();
c();
print("LUAMARKERMARKERMARKER");
)EOF");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QCOMPARE(p.exitCode(), 0);
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        output += QString::fromUtf8(p.readAllStandardError());
        QVERIFY(output.contains("LUAMARKERMARKERMARKER"));
    }

    void error()
    {
        QProcess p;
        p.start("da.luaexec", QStringList() << R"EOF(
c = CPD3.Pipeline();
c:input_archive({
    station = "brw",
    archive = "raw",
    variable = "T_S11",
});
c:output_discard();
error();
print("LUAMARKERMARKERMARKER");
)EOF");
        QVERIFY(p.waitForFinished());
        QVERIFY(p.exitStatus() == QProcess::NormalExit);
        QVERIFY(p.exitCode() != 0);
        QString output(QString::fromUtf8(p.readAllStandardOutput()));
        output += QString::fromUtf8(p.readAllStandardError());
        QVERIFY(!output.contains("LUAMARKERMARKERMARKER"));
    }

};

QTEST_MAIN(TestLuaScriptBasic)

#include "basic.moc"
