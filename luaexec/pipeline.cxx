/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <mutex>
#include <thread>
#include <condition_variable>

#include <stdlib.h>
#include <QLoggingCategory>
#include <QTimer>

#include "core/threading.hxx"
#include "core/component.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/valueoptionparse.hxx"
#include "datacore/externalsink.hxx"
#include "datacore/externalsource.hxx"
#include "luascript/libs/archive.hxx"
#include "luascript/libs/time.hxx"
#include "luascript/libs/streamvalue.hxx"
#include "luascript/libs/sequencename.hxx"
#include "luascript/libs/valuesegment.hxx"
#include "luascript/libs/variant.hxx"
#include "luascript/libs/time.hxx"
#include "io/drivers/file.hxx"
#include "pipeline.hxx"
#include "execthread.hxx"
#include "general.hxx"
#include "processor.hxx"
#include "fanout.hxx"

#ifdef BUILD_GUI

#include <QSvgGenerator>
#include <QImage>
#include <QMainWindow>

#include "graphing/displaycomponent.hxx"
#include "graphing/displaylayout.hxx"
#include "graphing/displaywidget.hxx"

#endif

Q_DECLARE_LOGGING_CATEGORY(log_luaexec)


Q_LOGGING_CATEGORY(log_luaexec_pipeline, "cpd3.luaexec.pipeline", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Lua;

static const std::string metatableRegistryName = "CPD3_luaexecpipeline_metatable";
static const std::string pipelinesRegistryName = "CPD3_luaexecpipeline_list";


class LuaPipeline::PipelineThread::ExitBlocker {
public:
    ExitBlocker() = default;

    virtual ~ExitBlocker() = default;

    virtual bool isComplete() = 0;

    virtual void abort()
    { }

    virtual void wait()
    { }
};

void LuaPipeline::install(ExecThread *thread, Engine::Frame &root)
{
    Engine::Frame frame(root);

    {
        Engine::Assign assign(frame, frame.registry(), pipelinesRegistryName);
        assign.pushTable();
    }

    auto mt = pushMetatable(frame);

    {
        Engine::Assign assign(frame, mt, "__tostring");
        pushMethod<LuaPipeline>(assign, &LuaPipeline::meta_tostring);
    }
    {
        Engine::Assign assign(frame, mt, "__call");
        pushMethod<LuaPipeline>(assign, &LuaPipeline::method_execute);
    }

    {
        Engine::Assign assign(frame, mt, "__index");
        auto methods = assign.pushTable();

        {
            Engine::Frame mf(assign);
            pushMethod<LuaPipeline>(mf, &LuaPipeline::method_processing);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "processing");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "processing_stage");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "addProcessingStage");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "add_processing_stage");
                ma.push(m);
            }
        }

        {
            Engine::Frame mf(assign);
            pushMethod<LuaPipeline>(mf, &LuaPipeline::method_editing);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "editing");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "editing_stage");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "addEditingStage");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "add_editing_stage");
                ma.push(m);
            }
        }

        {
            Engine::Frame mf(assign);
            pushMethod<LuaPipeline>(mf, &LuaPipeline::method_input_select);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "input_select");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "inputSelect");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "input");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<LuaPipeline>(mf, &LuaPipeline::method_input_external);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "input_external");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "inputExternal");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<LuaPipeline>(mf, &LuaPipeline::method_input_archive);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "input_archive");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "inputArchive");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<LuaPipeline>(mf, &LuaPipeline::method_input_file);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "input_file");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "inputFile");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<LuaPipeline>(mf, &LuaPipeline::method_input_none);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "input_discard");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "inputDiscard");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "input_none");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "inputNone");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "input_disable");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "inputDisable");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "disable_input");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "disableInput");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<LuaPipeline>(mf, &LuaPipeline::method_input_values);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "input_values");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "inputValues");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<LuaPipeline>(mf, &LuaPipeline::method_input_multiplexer);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "input_multiplexer");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "inputMultiplexer");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "input_multiplex");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "inputMultiplex");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "add_input");
                ma.push(m);
            }
        }

        {
            Engine::Frame mf(assign);
            pushMethod<LuaPipeline>(mf, &LuaPipeline::method_output_select);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "output_select");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "outputSelect");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<LuaPipeline>(mf, &LuaPipeline::method_output_external);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "output_external");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "outputExternal");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<LuaPipeline>(mf, &LuaPipeline::method_output_display);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "output_display");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "outputDisplay");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<LuaPipeline>(mf, &LuaPipeline::method_output_file);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "output_file");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "outputFile");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<LuaPipeline>(mf, &LuaPipeline::method_output_discard);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "output_discard");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "outputDiscard");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "output_none");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "outputNone");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "output_disable");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "outputDisable");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "disable_output");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "disableOutput");
                ma.push(m);
            }
        }

        {
            Engine::Assign ma(assign, methods, "segments");
            pushMethod<LuaPipeline>(ma, &LuaPipeline::method_segments);
        }
        {
            Engine::Assign ma(assign, methods, "values");
            pushMethod<LuaPipeline>(ma, &LuaPipeline::method_values);
        }
        {
            Engine::Frame mf(assign);
            pushMethod<LuaPipeline>(mf, &LuaPipeline::method_processor_segments);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "processor_segments");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "processorSegments");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "segment_processor");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "segmentProcessor");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<LuaPipeline>(mf, &LuaPipeline::method_processor_values);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "processor_values");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "processorValues");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "value_processor");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "valueProcessor");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<LuaPipeline>(mf, &LuaPipeline::method_fanout_segments);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "fanout");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "fanout_segments");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "fanoutSegments");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "segmentFanout");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<LuaPipeline>(mf, &LuaPipeline::method_fanout_values);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "fanout_values");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "fanoutValue");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "valueFanout");
                ma.push(m);
            }
        }
        {
            Engine::Assign ma(assign, methods, "buffer");
            pushMethod<LuaPipeline>(ma, &LuaPipeline::method_buffer);
        }

        {
            Engine::Frame mf(assign);
            pushMethod<LuaPipeline>(mf, &LuaPipeline::method_execute);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "exec");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "execute");
                ma.push(m);
            }
        }
        {
            Engine::Assign ma(assign, methods, "start");
            pushMethod<LuaPipeline>(ma, &LuaPipeline::method_start);
        }
        {
            Engine::Assign ma(assign, methods, "wait");
            pushMethod<LuaPipeline>(ma, &LuaPipeline::method_wait);
        }

        pushMethod<LuaPipeline>(assign, &LuaPipeline::meta_index, 1);
    }

    frame.push(frame.global(), "CPD3");
    auto ns = frame.back();
    Q_ASSERT(ns.getType() == Engine::Reference::Type::Table);

    frame.registry().set(metatableRegistryName, mt);
    {
        Engine::Assign assign(frame, ns, "Pipeline");
        assign.push(std::function<void(Engine::Entry &)>([thread](Engine::Entry &entry) {
            std::string id;
            if (!entry.empty())
                id = entry[0].toString();
            if (id.empty()) {
                static std::atomic_uint_fast32_t counter;
                auto n = counter.fetch_add(1);
                id = std::to_string(n + 1);
            }

            std::string display = id;
            if (entry.size() > 1)
                display = entry[1].toString();

            qCDebug(log_luaexec_pipeline) << "Creating pipeline" << id;

            entry.clear();
            entry.pushData<LuaPipeline>(thread, std::move(id), std::move(display));
            auto ref = entry.back();
            entry.push(entry.registry(), pipelinesRegistryName);
            auto reg = entry.back();
            {
                Engine::Append append(entry, reg, true);
                append.push(ref);
            }
            entry.propagate(1);
        }));
    }
}

void LuaPipeline::operate(CPD3::Lua::Engine::Frame &root,
                          const std::function<void(CPD3::Lua::Engine::Frame &,
                                                   const CPD3::Lua::Engine::Reference &)> &function)
{
    Engine::Frame frame(root);
    frame.push(frame.registry(), pipelinesRegistryName);
    Engine::Iterator it(frame, frame.back());
    while (it.next()) {
        function(it, it.value());
    }
}

void LuaPipeline::initialize(const Engine::Reference &self, Engine::Frame &frame)
{
    frame.push(frame.registry(), metatableRegistryName);
    setMetatable(self, frame.back());
}

LuaPipeline::LuaPipeline(ExecThread *thread, std::string id, std::string display) : pipelineThread(
        *this),
                                                                                    id(std::move(
                                                                                            id)),
                                                                                    display(std::move(
                                                                                            display)),
                                                                                    started(false),
                                                                                    thread(thread),
                                                                                    haveScriptControlledStage(
                                                                                            false)
{
    thread->terminateRequested.connect(*this, std::bind(&LuaPipeline::signalTerminate, this));
    startConnection =
            thread->startPipelines.connect(*this, std::bind(&LuaPipeline::startPipeline, this));
    thread->finalizeBuffers.connect(*this, std::bind(&LuaPipeline::finalizeMultiplexer, this));
    thread->completePipelines.connect(*this, std::bind(&LuaPipeline::complete, this));
    thread->arguments
          .ready
          .connect(*this, std::bind(&LuaPipeline::argumentsReady, this, std::placeholders::_1));

    pipelineThread.setObjectName(QString::fromStdString("LuaPipeline-" + this->id));

    pipelineThread.start();
    std::unique_lock<std::mutex> lock(mutex);
    notify.wait(lock, [this]() { return pipelineThread.pipeline.get() != nullptr; });
}

LuaPipeline::~LuaPipeline()
{
    disconnect();
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (pipelineThread.pipeline) {
            Threading::runQueuedFunctor(pipelineThread.context.get(), [this]() {
                Q_ASSERT(pipelineThread.pipeline);
                pipelineThread.pipeline->signalTerminate();
            });
        }
    }
    if (inputArgument) {
        inputArgument->disconnect();
        inputArgument.reset();
    }
    if (outputArgument) {
        outputArgument->disconnect();
        outputArgument.reset();
    }
    inputMultiplexerReceiver.disconnect();
    if (inputMultiplexer) {
        inputMultiplexer->signalTerminate();
        inputMultiplexer->wait();
    }
    outputMultiplexer.reset();
    pipelineThread.wait();
}

void LuaPipeline::signalTerminate()
{
    qCDebug(log_luaexec_pipeline) << "Terminating pipeline" << id;

    std::lock_guard<std::mutex> lock(mutex);
    bool abortThread = false;
    if (!started) {
        started = true;
        abortThread = true;
    }
    if (pipelineThread.pipeline) {
        Threading::runQueuedFunctor(pipelineThread.context.get(), [this, abortThread]() {
            Q_ASSERT(pipelineThread.pipeline);
            pipelineThread.pipeline->signalTerminate();
            if (abortThread) {
                pipelineThread.exit(1);
            } else {
                for (const auto &b : pipelineThread.blockers) {
                    b->abort();
                }
                pipelineThread.exitPoll();
            }
        });
    }
}

void LuaPipeline::startPipeline()
{
    if (inputConnector)
        inputConnector->endData();
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (started)
            return;
        started = true;
    }
    qCDebug(log_luaexec_pipeline) << "Start finalization for pipeline" << id;
    pipelineThread.manipulate(std::bind(&StreamPipeline::start, std::placeholders::_1));
    if (inputConnector) {
        inputConnector->start();
    }
    if (inputMultiplexer) {
        inputMultiplexer->sinkCreationComplete();
        inputMultiplexer->start();
    }
}

void LuaPipeline::finalizeMultiplexer()
{
    if (!inputMultiplexer)
        return;
    inputMultiplexer->wait();
    inputMultiplexerReceiver.disconnect();
    inputMultiplexer.reset();
}

void LuaPipeline::complete()
{
    pipelineThread.wait();
}

void LuaPipeline::meta_index(Engine::Entry &entry, const std::vector<Engine::Reference> &upvalues)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid index");
        return;
    }
    if (methodIndexHandler(entry, Engine::Table(upvalues.front())))
        return;

    auto key = entry[1].toString();
    if (key == "inputs") {
        {
            std::lock_guard<std::mutex> lock(mutex);
            if (started)
                return;
        }
        SequenceName::Set names;
        pipelineThread.manipulate([&names](StreamPipeline *pipeline) {
            names = pipeline->requestedInputs();
        });
        entry.clear();
        for (const auto &add : names) {
            entry.pushData<Libs::SequenceName>(add);
        }
        entry.propagate();
        return;
    } else if (key == "outputs") {
        {
            std::lock_guard<std::mutex> lock(mutex);
            if (started)
                return;
        }
        SequenceName::Set names;
        pipelineThread.manipulate([&names](StreamPipeline *pipeline) {
            names = pipeline->predictedOutputs();
        });
        entry.clear();
        for (const auto &add : names) {
            entry.pushData<Libs::SequenceName>(add);
        }
        entry.propagate();
        return;
    }
}

void LuaPipeline::meta_tostring(Engine::Entry &)
{
    std::string name = id;
    if (display != name) {
        name += " - ";
        name += display;
    }
    bool active;
    {
        std::lock_guard<std::mutex> lock(mutex);
        active = pipelineThread.pipeline.get() != nullptr;
    }
    if (!active) {
        name += " - COMPLETE";
    }
}

void LuaPipeline::method_execute(Engine::Entry &entry)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        if (!started) {
            started = true;
            lock.unlock();
            qCDebug(log_luaexec_pipeline) << "Starting pipeline" << id;

            Archive::Access::ReadLock archiveLock(archive());
            if (inputArgument || outputArgument) {
                thread->arguments.evaluate(entry);
                if (entry.inError()) {
                    pipelineThread.exit(1);
                    return;
                }
            }
            pipelineThread.manipulate(std::bind(&StreamPipeline::start, std::placeholders::_1));

            if (inputConnector) {
                inputConnector->start();
            }
            if (inputMultiplexer) {
                inputMultiplexer->sinkCreationComplete();
                inputMultiplexer->start();
            }
        }
    }
    qCDebug(log_luaexec_pipeline) << "Completing pipeline" << id;

    if (inputConnector)
        inputConnector->endData();
    /* Have to start all pipelines, since the processor state is shared */
    startConnection.disconnectImmediate();
    thread->startPipelines(entry);
    thread->startPipelines.disconnectImmediate();

    thread->finalizeAcquisition(entry);
    thread->finalizeAcquisition.disconnectImmediate();
    if (!thread->executeProcessors(entry))
        return;
    thread->finalizeBuffers(entry);
    thread->finalizeBuffers.disconnectImmediate();
    pipelineThread.wait();
}

void LuaPipeline::method_start(Engine::Entry &entry)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (started)
            return;
        started = true;
    }
    qCDebug(log_luaexec_pipeline) << "Starting pipeline" << id;

    Archive::Access::ReadLock archiveLock(archive());
    if (inputArgument || outputArgument) {
        thread->arguments.evaluate(entry);
        if (entry.inError()) {
            pipelineThread.exit(1);
            return;
        }
    }
    pipelineThread.manipulate(std::bind(&StreamPipeline::start, std::placeholders::_1));

    if (inputConnector) {
        inputConnector->start();
    }
    if (inputMultiplexer) {
        inputMultiplexer->sinkCreationComplete();
        inputMultiplexer->start();
    }
}

void LuaPipeline::method_wait(Engine::Entry &entry)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (!started) {
            entry.throwError("Pipeline has not been started yet");
            return;
        }
    }
    qCDebug(log_luaexec_pipeline) << "Completing pipeline" << id;

    if (inputConnector)
        inputConnector->endData();
    /* Have to start all pipelines, since the processor state is shared */
    startConnection.disconnectImmediate();
    thread->startPipelines(entry);
    thread->startPipelines.disconnectImmediate();

    thread->finalizeAcquisition(entry);
    thread->finalizeAcquisition.disconnectImmediate();
    if (!thread->executeProcessors(entry))
        return;
    thread->finalizeBuffers(entry);
    thread->finalizeBuffers.disconnectImmediate();
    pipelineThread.wait();
}

void LuaPipeline::method_processing(Engine::Entry &entry)
{
    if (!canAlterPipeline(entry))
        return;
    if (entry.size() < 2) {
        entry.throwError("Insufficient arguments");
        return;
    }

    auto componentName = entry[1].toString();
    auto component = ComponentLoader::create(QString::fromStdString(componentName));
    if (!component) {
        entry.throwError("Failed to load component " + componentName);
        return;
    }
    ProcessingStageComponent
            *processingComponent = qobject_cast<ProcessingStageComponent *>(component);
    if (!processingComponent) {
        entry.throwError("Component " + componentName + " is not a processing stage");
        return;
    }

    std::unique_ptr<ProcessingStage> processingStage;
    if (entry.size() < 3) {
        processingStage.reset(processingComponent->createGeneralFilterDynamic());
    } else {
        auto options = processingComponent->getOptions();

        auto base = entry[2];
        if (base.getType() == Engine::Reference::Type::Table) {
            Engine::Table table(base);

            bool usePredefined = false;
            double stageStart = FP::undefined();
            double stageEnd = FP::undefined();
            {
                Engine::Frame local(entry);
                auto start = Libs::Time::pushStart(local, table);
                auto end = Libs::Time::pushEnd(local, table);

                if (!start.isNil()) {
                    usePredefined = true;
                    if (!end.isNil()) {
                        auto bounds = Libs::Time::extractBounds(local, start, end);
                        stageStart = bounds.start;
                        stageEnd = bounds.end;
                    } else {
                        stageStart = Libs::Time::extractSingle(local, start);
                    }
                } else if (!end.isNil()) {
                    usePredefined = true;
                    stageEnd = Libs::Time::extractSingle(local, end);
                }
            }

            QList<SequenceName> inputs;
            {
                Engine::Frame local(entry);
                local.push(table, "inputs");
                if (!local.back().isNil()) {
                    usePredefined = true;
                    Engine::Iterator it(local, local.back());
                    while (it.next()) {
                        if (it.key().getType() == Engine::Reference::Type::Number) {
                            auto name = Libs::SequenceName::extract(it, it.value());
                            if (!name.isValid())
                                continue;
                            inputs.append(std::move(name));
                            continue;
                        }

                        if (!it.value().toBoolean())
                            continue;
                        auto name = Libs::SequenceName::extract(it, it.key());
                        if (!name.isValid())
                            continue;
                        inputs.append(std::move(name));
                    }
                }
            }

            {
                Engine::Frame local(entry);
                local.push(table, "options");
                if (!local.back().isNil()) {
                    ValueOptionParse::parse(options, Libs::Variant::extract(local, local.back()));
                }
            }

            if (usePredefined) {
                processingStage.reset(
                        processingComponent->createGeneralFilterPredefined(options, stageStart,
                                                                           stageEnd, inputs));
            } else {
                processingStage.reset(processingComponent->createGeneralFilterDynamic(options));
            }
        } else {
            ValueOptionParse::parse(options, Libs::Variant::extract(entry, base));
            processingStage.reset(processingComponent->createGeneralFilterDynamic(options));
        }
    }

    if (!processingStage) {
        entry.throwError("Failed to create " + componentName);
        return;
    }

    qCDebug(log_luaexec_pipeline) << "Adding processing stage" << componentName << "to pipeline"
                                  << id;

    bool ok = true;
    std::string error;
    pipelineThread.manipulate(
            [&ok, &error, &processingStage, processingComponent](StreamPipeline *pipeline) {
                if (pipeline->addProcessingStage(std::move(processingStage), processingComponent))
                    return;
                ok = false;
                error = pipeline->getOutputError().toStdString();
            });
    if (!ok) {
        entry.throwError(error);
        return;
    }
}

void LuaPipeline::method_editing(Engine::Entry &entry)
{
    if (!canAlterPipeline(entry))
        return;
    if (entry.size() < 2) {
        entry.throwError("Insufficient arguments");
        return;
    }

    auto componentName = entry[1].toString();
    auto component = ComponentLoader::create(QString::fromStdString(componentName));
    if (!component) {
        entry.throwError("Failed to load component " + componentName);
        return;
    }
    ProcessingStageComponent
            *processingComponent = qobject_cast<ProcessingStageComponent *>(component);
    if (!processingComponent) {
        entry.throwError("Component " + componentName + " is not a processing stage");
        return;
    }

    double stageStart = FP::undefined();
    double stageEnd = FP::undefined();
    SequenceName::Component station;
    SequenceName::Component archive;
    ValueSegment::Transfer config;

    if (entry.size() > 2) {
        Engine::Table table(entry[2]);

        {
            Engine::Frame local(entry);
            auto start = Libs::Time::pushStart(local, table);
            auto end = Libs::Time::pushEnd(local, table);

            if (!start.isNil()) {
                if (!end.isNil()) {
                    auto bounds = Libs::Time::extractBounds(local, start, end);
                    stageStart = bounds.start;
                    stageEnd = bounds.end;
                } else {
                    stageStart = Libs::Time::extractSingle(local, start);
                }
            } else if (!end.isNil()) {
                stageEnd = Libs::Time::extractSingle(local, end);
            }
        }

        station = table.get("station").toString();
        archive = table.get("archive").toString();

        {
            Engine::Frame local(entry);
            local.push(table, "configuration");
            if (local.back().isNil()) {
                local.push(table, "config");
            }
            if (!local.back().isNil()) {
                Engine::Table segments(local.back());
                if (segments.length() > 0) {
                    for (std::size_t i = 1, max = segments.length(); i <= max; i++) {
                        Engine::Frame lf(local);
                        lf.push(segments, i);
                        if (lf.back().isNil())
                            continue;
                        config.emplace_back(Libs::ValueSegment::extract(lf, lf.back()));
                    }
                    config = CPD3::Data::ValueSegment::merge(config);
                } else {
                    config.emplace_back(Libs::ValueSegment::extract(local, local.back()));
                }
            }
        }
    }

    std::unique_ptr<ProcessingStage> processingStage
            (processingComponent->createGeneralFilterEditing(stageStart, stageEnd, station, archive,
                                                             config));
    if (!processingStage) {
        entry.throwError("Failed to create " + componentName);
        return;
    }

    qCDebug(log_luaexec_pipeline) << "Adding editing stage" << componentName << "to pipeline" << id;

    bool ok = true;
    std::string error;
    pipelineThread.manipulate(
            [&ok, &error, &processingStage, processingComponent](StreamPipeline *pipeline) {
                if (pipeline->addProcessingStage(std::move(processingStage), processingComponent))
                    return;
                ok = false;
                error = pipeline->getOutputError().toStdString();
            });
    if (!ok) {
        entry.throwError(error);
        return;
    }
}

void LuaPipeline::method_input_select(Engine::Entry &entry)
{
    if (!canAlterPipeline(entry))
        return;
    if (inputConnector)
        inputConnector->disconnect();
    if (inputMultiplexer) {
        inputMultiplexer->setEgress(nullptr);
        inputMultiplexerReceiver.disconnect();
        inputMultiplexer.reset();
    }
    if (inputArgument) {
        inputArgument->disconnect();
        inputArgument.reset();
    }

    qCDebug(log_luaexec_pipeline) << "Setting pipeline" << id << "to user input selection";

    inputArgument = thread->arguments
                          .pipelineInput(this, QString::fromStdString(id),
                                         QString::fromStdString(display));
}

void LuaPipeline::method_input_external(Engine::Entry &entry)
{
    if (!canAlterPipeline(entry))
        return;
    if (entry.size() < 2) {
        entry.throwError("Insufficient arguments");
        return;
    }

    auto componentName = entry[1].toString();
    auto component = ComponentLoader::create(QString::fromStdString(componentName));
    if (!component) {
        entry.throwError("Failed to load component " + componentName);
        return;
    }

    if (inputConnector)
        inputConnector->disconnect();
    if (inputMultiplexer) {
        inputMultiplexer->setEgress(nullptr);
        inputMultiplexerReceiver.disconnect();
        inputMultiplexer.reset();
    }
    if (inputArgument) {
        inputArgument->disconnect();
        inputArgument.reset();
    }

    if (ExternalConverterComponent
            *converterComponent = qobject_cast<ExternalConverterComponent *>(component)) {
        ComponentOptions options = converterComponent->getOptions();
        QString filename;
        if (entry.size() > 2) {
            Engine::Table table(entry[2]);
            {
                Engine::Frame local(entry);
                local.push(table, "options");
                if (!local.back().isNil()) {
                    ValueOptionParse::parse(options, Libs::Variant::extract(local, local.back()));
                }
            }
            {
                Engine::Frame local(entry);
                local.push(table, "file");
                if (!local.back().isNil()) {
                    filename = local.back().toQString();
                }
            }
        }
        std::unique_ptr<ExternalConverter>
                converter(converterComponent->createDataIngress(options));
        if (!converter) {
            entry.throwError("Failed to create " + componentName);
            return;
        }

        if (!converterComponent->requiresInputDevice()) {
            qCDebug(log_luaexec_pipeline) << "Setting pipeline" << id << "to external converter"
                                          << componentName;

            bool ok = true;
            std::string error;
            pipelineThread.manipulate([&ok, &error, &converter](StreamPipeline *pipeline) {
                if (pipeline->setInputGeneral(std::move(converter)))
                    return;
                ok = false;
                error = pipeline->getInputError().toStdString();
            });
            if (!ok) {
                entry.throwError(error);
                return;
            }
            return;
        }

        qCDebug(log_luaexec_pipeline) << "Setting pipeline" << id << "to external converter"
                                      << componentName << "from" << filename;

        bool ok = true;
        std::string error;
        pipelineThread.manipulate([&ok, &error, &filename, &converter](StreamPipeline *pipeline) {
#ifndef NO_CONSOLE
            if (filename.isEmpty() || filename == "-") {
                if (pipeline->setInputGeneralPipeline(std::move(converter)))
                    return;
            } else
#endif
            {
                if (pipeline->setInputGeneral(std::move(converter), filename))
                    return;
            }
            ok = false;
            error = pipeline->getInputError().toStdString();
        });
        if (!ok) {
            entry.throwError(error);
            return;
        }
        return;
    } else if (ExternalSourceComponent
            *sourceComponent = qobject_cast<ExternalSourceComponent *>(component)) {
        ComponentOptions options = sourceComponent->getOptions();
        CPD3::Data::SequenceName::ComponentSet stations;
        Time::Bounds bounds;
        bool haveBounds = false;
        if (entry.size() > 2) {
            Engine::Table table(entry[2]);
            {
                Engine::Frame local(entry);
                local.push(table, "options");
                if (!local.back().isNil()) {
                    ValueOptionParse::parse(options, Libs::Variant::extract(local, local.back()));
                }
            }
            {
                Engine::Frame local(entry);
                local.push(table, "station");
                if (!local.back().isNil()) {
                    stations.insert(local.back().toString());
                }
            }
            {
                Engine::Frame local(entry);
                local.push(table, "stations");
                if (!local.back().isNil()) {
                    Engine::Iterator it(local, local.back());
                    while (it.next()) {
                        stations.insert(it.value().toString());
                    }
                }
            }
            stations.erase(CPD3::Data::SequenceName::Component());
            {
                Engine::Frame local(entry);
                auto start = Libs::Time::pushStart(local, table);
                auto end = Libs::Time::pushEnd(local, table);

                if (!start.isNil()) {
                    haveBounds = true;
                    if (!end.isNil()) {
                        bounds = Libs::Time::extractBounds(local, start, end);
                    } else {
                        bounds.start = Libs::Time::extractSingle(local, start);
                    }
                } else if (!end.isNil()) {
                    haveBounds = true;
                    bounds.end = Libs::Time::extractSingle(local, end);
                }
            }
        }

        if (stations.size() < static_cast<std::size_t>(sourceComponent->ingressRequireStations())) {
            entry.throwError("Insufficient stations provided");
            return;
        }
        if (stations.size() > static_cast<std::size_t>(sourceComponent->ingressAllowStations())) {
            entry.throwError("Too many stations provided");
            return;
        }

        std::vector<SequenceName::Component> stationsList(stations.begin(), stations.end());
        std::unique_ptr<ExternalConverter> source;
        if (!haveBounds && !sourceComponent->ingressRequiresTime()) {
            source.reset(sourceComponent->createExternalIngress(options, stationsList));
        } else {
            if (!sourceComponent->ingressAcceptsUndefinedBounds() &&
                    (!FP::defined(bounds.getStart()) || !FP::defined(bounds.getEnd()))) {
                entry.throwError("Undefined bounds are not accepted");
                return;
            }
            source.reset(sourceComponent->createExternalIngress(options, bounds.getStart(),
                                                                bounds.getEnd(), stationsList));
        }
        if (!source) {
            entry.throwError("Failed to create " + componentName);
            return;
        }

        qCDebug(log_luaexec_pipeline) << "Setting pipeline" << id << "to external source"
                                      << componentName;

        bool ok = true;
        std::string error;
        pipelineThread.manipulate([&ok, &error, &source](StreamPipeline *pipeline) {
            if (pipeline->setInputGeneral(std::move(source)))
                return;
            ok = false;
            error = pipeline->getInputError().toStdString();
        });
        if (!ok) {
            entry.throwError(error);
            return;
        }
        return;
    } else {
        entry.throwError("Component " + componentName + " is not a source component");
        return;
    }
}

void LuaPipeline::method_input_archive(Engine::Entry &entry)
{
    if (!canAlterPipeline(entry))
        return;
    if (entry.size() < 2) {
        entry.throwError("Insufficient arguments");
        return;
    }
    if (inputConnector)
        inputConnector->disconnect();
    if (inputMultiplexer) {
        inputMultiplexer->setEgress(nullptr);
        inputMultiplexerReceiver.disconnect();
        inputMultiplexer.reset();
    }
    if (inputArgument) {
        inputArgument->disconnect();
        inputArgument.reset();
    }

    Archive::Selection::List selections;
    Time::Bounds clip;

    for (std::size_t i = 1, max = entry.size(); i < max; i++) {
        auto arg = entry[i];

        Archive::Selection add = Libs::Archive::Selection::extract(entry, arg);

        {
            Engine::Frame frame(entry);
            frame.push(arg, "clip");
            auto check = frame.back();
            if (!check.isNil()) {
                if (check.getType() == Engine::Reference::Type::Boolean) {
                    if (check.toBoolean()) {
                        if (Range::compareStart(add.start, clip.start) > 0)
                            clip.start = add.start;
                        if (Range::compareEnd(add.start, clip.start) < 0)
                            clip.end = add.end;
                    }
                } else {
                    auto start = Libs::Time::pushStart(frame, check);
                    auto end = Libs::Time::pushEnd(frame, check);
                    clip = Libs::Time::extractBounds(frame, start, end);
                }
            }
        }

        selections.emplace_back(std::move(add));
    }

    qCDebug(log_luaexec_pipeline) << "Setting pipeline" << id << "input to archive" << selections;

    bool ok = true;
    std::string error;
    pipelineThread.manipulate([&ok, &error, &selections, &clip, this](StreamPipeline *pipeline) {
        if (pipeline->setInputArchive(selections, clip, &archive()))
            return;
        ok = false;
        error = pipeline->getInputError().toStdString();
    });
    if (!ok) {
        entry.throwError(error);
        return;
    }
}

void LuaPipeline::method_input_file(Engine::Entry &entry)
{
    if (!canAlterPipeline(entry))
        return;
    if (entry.size() < 2) {
        entry.throwError("Insufficient arguments");
        return;
    }
    if (inputConnector)
        inputConnector->disconnect();
    if (inputMultiplexer) {
        inputMultiplexer->setEgress(nullptr);
        inputMultiplexerReceiver.disconnect();
        inputMultiplexer.reset();
    }
    if (inputArgument) {
        inputArgument->disconnect();
        inputArgument.reset();
    }

    QString filename = entry[1].toQString();

    qCDebug(log_luaexec_pipeline) << "Setting pipeline" << id << "input to file" << filename;

    bool ok = true;
    std::string error;
    pipelineThread.manipulate([&ok, &error, &filename](StreamPipeline *pipeline) {
        if (pipeline->setInputFile(filename))
            return;
        ok = false;
        error = pipeline->getInputError().toStdString();
    });
    if (!ok) {
        entry.throwError(error);
        return;
    }
}

void LuaPipeline::method_input_none(Engine::Entry &entry)
{
    if (!canAlterPipeline(entry))
        return;
    if (inputConnector)
        inputConnector->disconnect();
    if (inputMultiplexer) {
        inputMultiplexer->setEgress(nullptr);
        inputMultiplexerReceiver.disconnect();
        inputMultiplexer.reset();
    }
    if (inputArgument) {
        inputArgument->disconnect();
        inputArgument.reset();
    }

    qCDebug(log_luaexec_pipeline) << "Setting pipeline" << id << "to no input";

    bool ok = true;
    std::string error;
    pipelineThread.manipulate([&ok, &error](StreamPipeline *pipeline) {
        if (auto target = pipeline->setInputExternal()) {
            target->endData();
            return;
        }
        ok = false;
        error = pipeline->getInputError().toStdString();
    });
    if (!ok) {
        entry.throwError(error);
        return;
    }
}

void LuaPipeline::method_input_values(Engine::Entry &entry)
{
    if (!canAlterPipeline(entry))
        return;
    if (inputMultiplexer) {
        inputMultiplexer->setEgress(nullptr);
        inputMultiplexerReceiver.disconnect();
        inputMultiplexer.reset();
    }
    if (inputArgument) {
        inputArgument->disconnect();
        inputArgument.reset();
    }

    qCDebug(log_luaexec_pipeline) << "Setting pipeline" << id << "Lua value input";

    /* This isn't quite right, since the script could potentially set the input
     * after adding filters, but we don't really have a choice */
    haveScriptControlledStage = true;

    bool ok = true;
    std::string error;
    pipelineThread.manipulate([this, &ok, &error](StreamPipeline *pipeline) {
        auto target = pipeline->setInputExternal();
        if (!target) {
            ok = false;
            error = pipeline->getInputError().toStdString();
            return;
        }

        if (!inputConnector) {
            Q_ASSERT(!pipelineThread.inputConnector);
            inputConnector = std::make_shared<InputConnector>(thread);
            pipelineThread.inputConnector = inputConnector;
            inputConnector->connect(target);
        } else {
            Q_ASSERT(pipelineThread.inputConnector);
            Q_ASSERT(inputConnector.get() == pipelineThread.inputConnector.get());
            inputConnector->connect(target);
        }
    });
    if (!ok) {
        entry.throwError(error);
        return;
    }
    if (!inputConnector) {
        entry.throwError("Unable to connect input");
        return;
    }

    if (entry.size() > 1) {
        SequenceValue::Transfer initial;
        for (std::size_t i = 1, max = entry.size(); i < max; i++) {
            auto value = Libs::SequenceValue::extract(entry, entry[i]);
            if (!value.isValid()) {
                entry.throwError("Invalid value");
                return;
            }
            if (Range::compareStartEnd(value.getStart(), value.getEnd()) > 0) {
                entry.throwError("Invalid value times");
                return;
            }
            initial.emplace_back(std::move(value));
        }
        std::sort(initial.begin(), initial.end(), SequenceIdentity::OrderTime());
        for (auto &value : initial) {
            inputConnector->incomingData(std::move(value));
        }
    }

    auto connector = inputConnector;
    entry.push([connector](Engine::Entry &entry) {
        for (std::size_t i = 0, max = entry.size(); i < max; i++) {
            auto ref = entry[i];
            if (ref.isNil()) {
                connector->endData();
                return;
            }
            auto value = Libs::SequenceValue::extract(entry, entry[i]);
            if (!value.isValid()) {
                entry.throwError("Invalid value");
                return;
            }
            if (Range::compareStartEnd(value.getStart(), value.getEnd()) > 0) {
                entry.throwError("Invalid value times");
                return;
            }
            connector->incomingData(std::move(value));
        }
    });
    entry.replaceWithBack(0);
    entry.propagate(1);
}

void LuaPipeline::method_input_multiplexer(Engine::Entry &entry)
{
    if (!canAlterPipeline(entry))
        return;
    if (entry.size() < 2) {
        entry.throwError("Insufficient arguments");
        return;
    }

    std::unique_ptr<MultiplexerInput> immediateInput;
    for (std::size_t i = 1, max = entry.size(); i < max; i++) {
        auto check = entry[i];

        if (auto val = check.toData<LuaPipeline>()) {
            if (!val->canAlterPipeline(entry))
                return;
            auto target = multiplexerInput(entry);
            if (!target)
                return;

            qCDebug(log_luaexec_pipeline) << "Connecting pipeline" << val->id
                                          << "to multiplexer for" << id;

            bool ok = true;
            std::string error;
            val->pipelineThread.manipulate([&ok, &error, &target, val](StreamPipeline *pipeline) {
                if (!pipeline->setOutputIngress(target.get())) {
                    ok = false;
                    error = pipeline->getOutputError().toStdString();
                    return;
                }

                val->outputMultiplexer = std::move(target);
            });
            if (!ok) {
                entry.throwError(error);
                return;
            }
            continue;
        }

        if (!immediateInput) {
            immediateInput = multiplexerInput(entry);
            if (!immediateInput)
                return;
        }
        auto value = Libs::SequenceValue::extract(entry, check);
        if (!value.isValid()) {
            entry.throwError("Invalid value");
            return;
        }
        if (Range::compareStartEnd(value.getStart(), value.getEnd()) > 0) {
            entry.throwError("Invalid value times");
            return;
        }
        immediateInput->incomingData(std::move(value));
    }
}

void LuaPipeline::method_output_select(Engine::Entry &entry)
{
    if (!canAlterPipeline(entry))
        return;
    outputMultiplexer.reset();
    if (outputArgument) {
        outputArgument->disconnect();
        outputArgument.reset();
    }

    qCDebug(log_luaexec_pipeline) << "Setting pipeline" << id << "to user output selection";

    outputArgument = thread->arguments
                           .pipelineOutput(this, QString::fromStdString(id),
                                           QString::fromStdString(display));
}

void LuaPipeline::method_output_external(Engine::Entry &entry)
{
    if (!canAlterPipeline(entry))
        return;
    if (entry.size() < 2) {
        entry.throwError("Insufficient arguments");
        return;
    }
    outputMultiplexer.reset();
    if (outputArgument) {
        outputArgument->disconnect();
        outputArgument.reset();
    }

    auto componentName = entry[1].toString();
    auto component = ComponentLoader::create(QString::fromStdString(componentName));
    if (!component) {
        entry.throwError("Failed to load component " + componentName);
        return;
    }

    ExternalSinkComponent *sinkComponent = qobject_cast<ExternalSinkComponent *>(component);
    if (!sinkComponent) {
        entry.throwError("Component " + componentName + " is not a sink");
        return;
    }

    ComponentOptions options = sinkComponent->getOptions();
    QString filename;
    bool haveFilename = false;
    if (entry.size() > 2) {
        Engine::Table table(entry[2]);
        {
            Engine::Frame local(entry);
            local.push(table, "options");
            if (!local.back().isNil()) {
                ValueOptionParse::parse(options, Libs::Variant::extract(local, local.back()));
            }
        }
        {
            Engine::Frame local(entry);
            local.push(table, "file");
            if (!local.back().isNil()) {
                filename = local.back().toQString();
                haveFilename = true;
            }
        }
    }

    if (!sinkComponent->requiresOutputDevice() && !haveFilename) {
        std::unique_ptr<ExternalSink> converter(sinkComponent->createDataEgress(nullptr, options));
        if (!converter) {
            entry.throwError("Failed to create " + componentName);
            return;
        }

        qCDebug(log_luaexec_pipeline) << "Setting pipeline" << id << "to external sink"
                                      << componentName;

        bool ok = true;
        std::string error;
        pipelineThread.manipulate([&ok, &error, &converter](StreamPipeline *pipeline) {
            if (pipeline->setOutputGeneral(std::move(converter)))
                return;
            ok = false;
            error = pipeline->getInputError().toStdString();
        });
        if (!ok) {
            entry.throwError(error);
            return;
        }
        return;
    } else if (haveFilename) {
        qCDebug(log_luaexec_pipeline) << "Setting pipeline" << id << "to external converter"
                                      << componentName << "from" << haveFilename;

        bool ok = true;
        std::string error;
        pipelineThread.manipulate(
                [&ok, &error, &filename, &options, &componentName, sinkComponent](StreamPipeline *pipeline) {
                    IO::Access::Handle target;
                    if (filename.isEmpty() || filename == "-") {
                        target = pipeline->pipelineOutputDevice();
                    } else {
                        target = IO::Access::file(filename, IO::File::Mode::writeOnly().textMode()
                                                                                       .bufferedMode());
                        if (!target) {
                            error = "Error opening" + filename.toStdString();
                            ok = false;
                            return;
                        }
                    }
                    auto stream = target->stream();
                    if (!stream) {
                        error = "Error opening output stream";
                        ok = false;
                        return;
                    }
                    std::unique_ptr<ExternalSink>
                            add(sinkComponent->createDataSink(std::move(stream), options));
                    if (!add) {
                        error = "Failed to create " + componentName;
                        ok = false;
                        return;
                    }
                    if (pipeline->setOutputGeneral(std::move(add)))
                        return;
                    ok = false;
                    error = pipeline->getInputError().toStdString();
                });
        if (!ok) {
            entry.throwError(error);
            return;
        }
        return;
    }


    qCDebug(log_luaexec_pipeline) << "Setting pipeline" << id << "to external sink" << componentName
                                  << "with user selection";

    inputArgument = thread->arguments
                          .pipelineExternalSinkOutput(this, QString::fromStdString(id),
                                                      QString::fromStdString(display),
                                                      sinkComponent, options);
}

void LuaPipeline::method_output_display(Engine::Entry &entry)
{
#ifndef BUILD_GUI
    entry.throwError("No display support");
    return;
#else
    if (!canAlterPipeline(entry))
        return;
    if (entry.size() < 2) {
        entry.throwError("Insufficient arguments");
        return;
    }
    outputMultiplexer.reset();
    if (outputArgument) {
        outputArgument->disconnect();
        outputArgument.reset();
    }

    ValueSegment::Transfer config;

    if (entry[1].getType() == Engine::Reference::Type::String) {
        auto typeName = entry[1].toString();

        Graphing::DisplayComponent::DisplayType
                type = Graphing::DisplayComponent::Display_TimeSeries2D;
        if (Util::equal_insensitive(typeName, "scatter")) {
            type = Graphing::DisplayComponent::Display_Scatter2D;
        } else if (Util::equal_insensitive(typeName, "density")) {
            type = Graphing::DisplayComponent::Display_Density2D;
        } else if (Util::equal_insensitive(typeName, "timeseries")) {
            type = Graphing::DisplayComponent::Display_TimeSeries2D;
        } else if (Util::equal_insensitive(typeName, "cycle")) {
            type = Graphing::DisplayComponent::Display_Cycle2D;
        } else if (Util::equal_insensitive(typeName, "cdf")) {
            type = Graphing::DisplayComponent::Display_CDF;
        } else if (Util::equal_insensitive(typeName, "pdf")) {
            type = Graphing::DisplayComponent::Display_PDF;
        } else if (Util::equal_insensitive(typeName, "allan")) {
            type = Graphing::DisplayComponent::Display_Allan;
        } else {
            entry.throwError("Unrecognized display type: " + typeName);
            return;
        }

        auto options = Graphing::DisplayComponent::options(type);

        if (entry.size() > 2) {
            Engine::Table table(entry[2]);
            Engine::Frame local(entry);

            local.push(table, "options");
            if (!local.back().isNil()) {
                ValueOptionParse::parse(options, Libs::Variant::extract(local, local.back()));
            }

            local.push(table, "file");
            if (!local.back().isNil()) {
                auto width = table.get("width").toInteger();
                if (!INTEGER::defined(width) || width <= 0)
                    width = 1024;
                auto height = table.get("height").toInteger();
                if (!INTEGER::defined(height) || height <= 0) {
                    height = width * 3 / 4;
                    if (height < 1)
                        height = 1;
                }

                auto name = local.back().toString();
                if (!name.empty()) {
                    qCDebug(log_luaexec_pipeline) << "Setting pipeline" << id << "to display output"
                                                  << typeName << "writing to" << name;

                    config.emplace_back(FP::undefined(), FP::undefined(),
                                        Graphing::DisplayComponent::baselineConfiguration(type,
                                                                                          options));

                    bool ok = true;
                    std::string error;
                    pipelineThread.manipulate(
                            [&ok, &error, this, &config, &name, width, height](StreamPipeline *pipeline) {
                                if (setDisplayOutputFile(pipeline, name, static_cast<int>(width),
                                                         static_cast<int>(height), [&config] {
                                            Graphing::DisplayDefaults defaults;
                                            auto display = new Graphing::DisplayLayout(defaults);
                                            display->initialize(config, defaults);
                                            return display;
                                        })) {
                                    return;
                                }
                                ok = false;
                                error = pipeline->getChainError().toStdString();
                            });
                    if (!ok) {
                        entry.throwError(error);
                        return;
                    }
                    return;
                }
            }
        }

        config.emplace_back(FP::undefined(), FP::undefined(),
                            Graphing::DisplayComponent::baselineConfiguration(type, options));
    } else {
        Engine::Table table(entry[1]);
        Engine::Frame local(entry);
        local.push(table, "configuration");
        if (local.back().isNil()) {
            local.push(table, "config");
        }
        if (!local.back().isNil()) {
            Engine::Table segments(local.back());
            if (segments.length() > 0) {
                for (std::size_t i = 1, max = segments.length(); i <= max; i++) {
                    Engine::Frame lf(local);
                    lf.push(segments, i);
                    if (lf.back().isNil())
                        continue;
                    config.emplace_back(Libs::ValueSegment::extract(lf, lf.back()));
                }
                config = CPD3::Data::ValueSegment::merge(config);
            } else {
                config.emplace_back(Libs::ValueSegment::extract(local, local.back()));
            }
        }

        local.push(table, "file");
        if (!local.back().isNil()) {
            auto width = table.get("width").toInteger();
            if (!INTEGER::defined(width) || width <= 0)
                width = 1024;
            auto height = table.get("height").toInteger();
            if (!INTEGER::defined(height) || height <= 0) {
                height = width * 3 / 4;
                if (height < 1)
                    height = 1;
            }

            auto name = local.back().toString();
            if (!name.empty()) {
                qCDebug(log_luaexec_pipeline) << "Setting pipeline" << id
                                              << "to display output writing to" << name;

                bool ok = true;
                std::string error;
                pipelineThread.manipulate(
                        [&ok, &error, this, &config, &name, width, height](StreamPipeline *pipeline) {
                            if (setDisplayOutputFile(pipeline, name, static_cast<int>(width),
                                                     static_cast<int>(height), [&config] {
                                        Graphing::DisplayDefaults defaults;
                                        auto display = new Graphing::DisplayLayout(defaults);
                                        display->initialize(config, defaults);
                                        return display;
                                    })) {
                                return;
                            }
                            ok = false;
                            error = pipeline->getChainError().toStdString();
                        });
                if (!ok) {
                    entry.throwError(error);
                    return;
                }
                return;
            }
        }
    }

    qCDebug(log_luaexec_pipeline) << "Setting pipeline" << id << "to display output";

    outputArgument = thread->arguments
                           .pipelineDisplayOutput(this, QString::fromStdString(id),
                                                  QString::fromStdString(display),
                                                  std::move(config));

#endif
}

void LuaPipeline::method_output_file(Engine::Entry &entry)
{
    if (!canAlterPipeline(entry))
        return;
    if (entry.size() < 2) {
        entry.throwError("Insufficient arguments");
        return;
    }
    outputMultiplexer.reset();
    if (outputArgument) {
        outputArgument->disconnect();
        outputArgument.reset();
    }

    QString filename = entry[1].toQString();
    StandardDataOutput::OutputType mode = StandardDataOutput::OutputType::Direct;
    if (entry.size() > 2) {
        auto type = entry[2].toString();
        if (Util::equal_insensitive(type, "raw", "binary")) {
            mode = StandardDataOutput::OutputType::Raw;
        } else if (Util::equal_insensitive(type, "xml")) {
            mode = StandardDataOutput::OutputType::XML;
        } else if (Util::equal_insensitive(type, "json")) {
            mode = StandardDataOutput::OutputType::JSON;
        }
    }

    qCDebug(log_luaexec_pipeline) << "Setting pipeline" << id << "output to file" << filename;

    bool ok = true;
    std::string error;
    pipelineThread.manipulate([&ok, &error, &filename, mode](StreamPipeline *pipeline) {
        if (pipeline->setOutputFile(filename, mode))
            return;
        ok = false;
        error = pipeline->getOutputError().toStdString();
    });
    if (!ok) {
        entry.throwError(error);
        return;
    }
}

namespace {
class DiscardSink : public StreamSink {
public:
    DiscardSink() = default;

    virtual ~DiscardSink() = default;

    void incomingData(const SequenceValue::Transfer &) override
    { }

    void incomingData(SequenceValue::Transfer &&) override
    { }

    void incomingData(const SequenceValue &) override
    { }

    void incomingData(SequenceValue &&) override
    { }

    void endData() override
    { }
};

static DiscardSink discardSink;

class ForwardBuffer : public ProcessingStage {
    StreamSink *egress;

    SequenceValue::Transfer pending;
    bool dataEnded;

    enum class State {
        NotStarted, Running, Terminated, Completed,
    } state;

    std::mutex mutex;
    std::mutex egressLock;
    std::condition_variable notify;

    std::thread thread;

    void run()
    {
        for (;;) {
            bool finishData;
            SequenceValue::Transfer incoming;

            std::unique_lock<std::mutex> lock(mutex);
            std::unique_lock<std::mutex> egressLocker(egressLock);
            for (;;) {
                if (state == State::Terminated) {
                    lock.unlock();

                    if (egress)
                        egress->endData();

                    egressLocker.unlock();
                    lock.lock();
                    state = State::Completed;
                    lock.unlock();
                    notify.notify_all();
                    finished();
                    return;
                }

                if (!egress) {
                    egressLocker.unlock();
                    notify.wait(lock);
                    egressLocker.lock();
                    continue;
                }

                incoming = std::move(pending);
                pending.clear();
                finishData = dataEnded;

                if (!finishData && incoming.empty()) {
                    egressLocker.unlock();
                    notify.wait(lock);
                    egressLocker.lock();
                    continue;
                }
                break;
            }
            lock.unlock();

            egress->incomingData(std::move(incoming));
            if (finishData) {
                egress->endData();
                egressLocker.unlock();
                lock.lock();
                state = State::Completed;
                lock.unlock();
                finished();
                notify.notify_all();
                return;
            }
        }
    }

public:
    ForwardBuffer() : egress(nullptr), dataEnded(false), state(State::NotStarted)
    { }

    ~ForwardBuffer()
    {
        signalTerminate();
        if (thread.joinable())
            thread.join();
    }

    void setEgress(StreamSink *set) override
    {
        std::lock_guard<std::mutex> lock(mutex);
        std::lock_guard<std::mutex> egressLocker(egressLock);
        egress = set;
        notify.notify_all();
    }

    void signalTerminate() override
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (state != State::Completed)
            state = State::Terminated;
        notify.notify_all();
    }

    void incomingData(const SequenceValue::Transfer &values) override
    {
        if (values.empty())
            return;
        {
            std::lock_guard<std::mutex> lock(mutex);
            Q_ASSERT(!dataEnded);
            Util::append(values, pending);
        }
        notify.notify_all();
    }

    void incomingData(SequenceValue::Transfer &&values) override
    {
        if (values.empty())
            return;
        {
            std::lock_guard<std::mutex> lock(mutex);
            Q_ASSERT(!dataEnded);
            Util::append(std::move(values), pending);
        }
        notify.notify_all();
    }

    void incomingData(const SequenceValue &value) override
    {
        {
            std::lock_guard<std::mutex> lock(mutex);
            Q_ASSERT(!dataEnded);
            pending.emplace_back(value);
        }
        notify.notify_all();
    }

    void incomingData(SequenceValue &&value) override
    {
        {
            std::lock_guard<std::mutex> lock(mutex);
            Q_ASSERT(!dataEnded);
            pending.emplace_back(std::move(value));
        }
        notify.notify_all();
    }

    void endData() override
    {
        std::lock_guard<std::mutex> lock(mutex);
        dataEnded = true;
        notify.notify_all();
    }

    bool isFinished() override
    {
        std::lock_guard<std::mutex> lock(mutex);
        return state == State::Completed;
    }

    bool wait(double timeout = FP::undefined()) override
    {
        if (!Threading::waitForTimeout(timeout, mutex, notify,
                                       [this] { return state == State::Completed; }))
            return false;
        if (thread.joinable())
            thread.join();
        return true;
    }

    void start() override
    {
        std::lock_guard<std::mutex> lock(mutex);
        Q_ASSERT(state == State::NotStarted);
        state = State::Running;
        thread = std::thread(std::bind(&ForwardBuffer::run, this));
    }

};
}

void LuaPipeline::method_output_discard(Engine::Entry &entry)
{
    if (!canAlterPipeline(entry))
        return;
    outputMultiplexer.reset();
    if (outputArgument) {
        outputArgument->disconnect();
        outputArgument.reset();
    }

    qCDebug(log_luaexec_pipeline) << "Setting pipeline" << id << "to discard output";

    bool ok = true;
    std::string error;
    pipelineThread.manipulate([&ok, &error](StreamPipeline *pipeline) {
        if (pipeline->setOutputIngress(&discardSink))
            return;
        ok = false;
        error = pipeline->getOutputError().toStdString();
    });
    if (!ok) {
        entry.throwError(error);
        return;
    }
}

void LuaPipeline::method_segments(Engine::Entry &entry)
{
    if (!canAlterPipeline(entry))
        return;

    qCDebug(log_luaexec_pipeline) << "Adding segment interface to pipeline" << id;

    if (!detachScriptStage(entry))
        return;

    std::unique_ptr<GeneralSegments> processor(new GeneralSegments(thread, entry));
    if (entry.size() > 1) {
        CPD3::Data::SequenceName::Set outputs;
        for (std::size_t i = 1, max = entry.size(); i < max; i++) {
            auto add = Libs::SequenceName::extract(entry, entry[i]);
            if (!add.isValid())
                continue;
            outputs.insert(std::move(add));
        }
        processor->setExplicitOutputs(std::move(outputs));
    }

    bool ok = true;
    std::string error;
    pipelineThread.manipulate([&ok, &error, &processor](StreamPipeline *pipeline) {
        if (pipeline->addProcessingStage(std::move(processor)))
            return;
        ok = false;
        error = pipeline->getChainError().toStdString();
    });
    if (!ok) {
        entry.throwError(error);
        return;
    }

    entry.replaceWithBack(0);
    entry.propagate(1);
}

void LuaPipeline::method_values(Engine::Entry &entry)
{
    if (!canAlterPipeline(entry))
        return;

    qCDebug(log_luaexec_pipeline) << "Adding value interface to pipeline" << id;

    if (!detachScriptStage(entry))
        return;

    std::unique_ptr<GeneralValues> processor(new GeneralValues(thread, entry));

    bool ok = true;
    std::string error;
    pipelineThread.manipulate([&ok, &error, &processor](StreamPipeline *pipeline) {
        if (pipeline->addProcessingStage(std::move(processor)))
            return;
        ok = false;
        error = pipeline->getChainError().toStdString();
    });
    if (!ok) {
        entry.throwError(error);
        return;
    }

    entry.replaceWithBack(0);
    entry.propagate(1);
}

void LuaPipeline::method_processor_segments(Engine::Entry &entry)
{
    if (!canAlterPipeline(entry))
        return;
    if (entry.size() < 2) {
        entry.throwError("Insufficient arguments");
        return;
    }
    auto call = entry[1];

    qCDebug(log_luaexec_pipeline) << "Adding segment processor to pipeline" << id;

    if (!detachScriptStage(entry))
        return;

    std::unique_ptr<SegmentProcessor> processor(new SegmentProcessor(thread, entry, call));
    if (entry.size() > 1) {
        CPD3::Data::SequenceName::Set outputs;
        for (std::size_t i = 1, max = entry.size(); i < max; i++) {
            auto add = Libs::SequenceName::extract(entry, entry[i]);
            if (!add.isValid())
                continue;
            outputs.insert(std::move(add));
        }
        processor->setExplicitOutputs(std::move(outputs));
    }

    bool ok = true;
    std::string error;
    pipelineThread.manipulate([&ok, &error, &processor](StreamPipeline *pipeline) {
        if (pipeline->addProcessingStage(std::move(processor)))
            return;
        ok = false;
        error = pipeline->getChainError().toStdString();
    });
    if (!ok) {
        entry.throwError(error);
        return;
    }

    entry.replaceWithBack(0);
    entry.propagate(1);
}

void LuaPipeline::method_processor_values(Engine::Entry &entry)
{
    if (!canAlterPipeline(entry))
        return;
    if (entry.size() < 2) {
        entry.throwError("Insufficient arguments");
        return;
    }
    auto call = entry[1];

    qCDebug(log_luaexec_pipeline) << "Adding value processor to pipeline" << id;

    if (!detachScriptStage(entry))
        return;

    std::unique_ptr<ValueProcessor> processor(new ValueProcessor(thread, entry, call));

    bool ok = true;
    std::string error;
    pipelineThread.manipulate([&ok, &error, &processor](StreamPipeline *pipeline) {
        if (pipeline->addProcessingStage(std::move(processor)))
            return;
        ok = false;
        error = pipeline->getChainError().toStdString();
    });
    if (!ok) {
        entry.throwError(error);
        return;
    }

    entry.replaceWithBack(0);
    entry.propagate(1);
}

void LuaPipeline::method_fanout_segments(Engine::Entry &entry)
{
    if (!canAlterPipeline(entry))
        return;
    bool doCall = entry.size() > 1;

    qCDebug(log_luaexec_pipeline) << "Adding segment fanout to pipeline" << id;

    if (!detachScriptStage(entry))
        return;

    std::unique_ptr<FanoutSegments> processor(new FanoutSegments(thread, entry));

    bool ok = true;
    std::string error;
    pipelineThread.manipulate([&ok, &error, &processor](StreamPipeline *pipeline) {
        if (pipeline->addProcessingStage(std::move(processor)))
            return;
        ok = false;
        error = pipeline->getChainError().toStdString();
    });
    if (!ok) {
        entry.throwError(error);
        return;
    }

    if (doCall && !entry[1].isNil()) {
        Engine::Call call(entry, entry.back());
        call.push(entry[1]);
        if (!call.execute())
            return;
    }

    entry.replaceWithBack(0);
    entry.propagate(1);
}

void LuaPipeline::method_fanout_values(Engine::Entry &entry)
{
    if (!canAlterPipeline(entry))
        return;
    bool doCall = entry.size() > 1;

    qCDebug(log_luaexec_pipeline) << "Adding value fanout to pipeline" << id;

    if (!detachScriptStage(entry))
        return;

    std::unique_ptr<FanoutValues> processor(new FanoutValues(thread, entry));

    bool ok = true;
    std::string error;
    pipelineThread.manipulate([&ok, &error, &processor](StreamPipeline *pipeline) {
        if (pipeline->addProcessingStage(std::move(processor)))
            return;
        ok = false;
        error = pipeline->getChainError().toStdString();
    });
    if (!ok) {
        entry.throwError(error);
        return;
    }

    if (doCall && !entry[1].isNil()) {
        Engine::Call call(entry, entry.back());
        call.push(entry[1]);
        if (!call.execute())
            return;
    }

    entry.replaceWithBack(0);
    entry.propagate(1);
}

void LuaPipeline::method_buffer(Engine::Entry &entry)
{
    if (!canAlterPipeline(entry))
        return;

    qCDebug(log_luaexec_pipeline) << "Adding buffer to pipeline" << id;

    /* Once we have a buffer in line, another script stage is safe to add
     * without a forced detach, even if it doesn't immediately preceded the
     * script stage.  That is, as long as a prior script stage can eventually
     * unstall it's safe to not-detach. */
    haveScriptControlledStage = false;

    bool ok = true;
    std::string error;
    pipelineThread.manipulate([&ok, &error](StreamPipeline *pipeline) {
        if (pipeline->addProcessingStage(std::unique_ptr<ForwardBuffer>(new ForwardBuffer)))
            return;
        ok = false;
        error = pipeline->getChainError().toStdString();
    });
    if (!ok) {
        entry.throwError(error);
        return;
    }
}

bool LuaPipeline::canAlterPipeline(Engine::Frame &frame)
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (started) {
            frame.throwError("Running pipelines cannot be altered");
            return false;
        }
    }
    return true;
}

bool LuaPipeline::detachScriptStage(Engine::Frame &frame)
{
    if (haveScriptControlledStage) {
        bool ok = true;
        std::string error;
        pipelineThread.manipulate([&ok, &error](StreamPipeline *pipeline) {
            if (pipeline->addProcessingStage(std::unique_ptr<ForwardBuffer>(new ForwardBuffer)))
                return;
            ok = false;
            error = pipeline->getChainError().toStdString();
        });
        if (!ok) {
            frame.throwError(error);
            return false;
        }
    }
    haveScriptControlledStage = true;
    return true;
}

std::unique_ptr<LuaPipeline::MultiplexerInput> LuaPipeline::multiplexerInput(Engine::Frame &frame,
                                                                             bool unsorted)
{
    if (inputConnector)
        inputConnector->disconnect();

    class Blocker : public PipelineThread::ExitBlocker {
        std::weak_ptr<CPD3::Data::SinkMultiplexer> mux;
        StreamPipeline *pipeline;
    public:
        Blocker(const std::shared_ptr<CPD3::Data::SinkMultiplexer> &mux, StreamPipeline *pipeline)
                : mux(mux), pipeline(pipeline)
        { }

        virtual ~Blocker()
        {
            /*
             * Since the pipeline is about to be deleted, make sure the multiplexer doesn't
             * still have a reference to it.
             */
            if (auto p = mux.lock()) {
                p->setEgress(nullptr);
            }
        }

        bool isComplete() override
        {
            /*
             * Only wait for the pipeline, since we only care about if it's complete so
             * we can disconnect from it.
             */
            if (pipeline) {
                if (!pipeline->isFinished())
                    return false;
                pipeline = nullptr;
            }
            return true;
        }
    };

    if (!inputMultiplexer) {
        if (!canAlterPipeline(frame))
            return {};

        qCDebug(log_luaexec_pipeline) << "Setting pipeline" << id << "multiplexer input";

        bool ok = true;
        std::string error;
        pipelineThread.manipulate([this, &ok, &error](StreamPipeline *pipeline) {
            auto target = pipeline->setInputExternal();
            if (!target) {
                ok = false;
                error = pipeline->getInputError().toStdString();
                return;
            }

            inputMultiplexer = std::make_shared<CPD3::Data::SinkMultiplexer>(false);
            thread->terminateRequested
                  .connect(inputMultiplexerReceiver,
                           std::bind(&CPD3::Data::SinkMultiplexer::signalTerminate,
                                     inputMultiplexer.get(), false));
            inputMultiplexer->setEgress(target);
            pipelineThread.blockers.emplace_back(new Blocker(inputMultiplexer, pipeline));
        });
        if (!ok) {
            frame.throwError(error);
            inputMultiplexerReceiver.disconnect();
            inputMultiplexer.reset();
            return {};
        }
    }

    Q_ASSERT(inputMultiplexer);
    return std::unique_ptr<MultiplexerInput>(
            new MultiplexerInput(thread, inputMultiplexer, unsorted));
}

bool LuaPipeline::setDisplayOutputFile(CPD3::Data::StreamPipeline *pipeline,
                                       const std::string &filename,
                                       int width,
                                       int height,
                                       const CreateDisplay &create,
                                       const ConfigureDisplayChain &configure)
{
#ifndef BUILD_GUI
    Q_UNUSED(pipeline);
    Q_UNUSED(filename);
    Q_UNUSED(width);
    Q_UNUSED(height);
    Q_UNUSED(create);
    Q_UNUSED(configure);
    return false;
#else
    Q_ASSERT(QThread::currentThread() == &pipelineThread);

    std::unique_ptr<Graphing::Display> display(create());
    if (!display) {
        qCDebug(log_luaexec_pipeline) << "Failed to create output display for pipeline" << id;
        return false;
    }

    std::unique_ptr<ProcessingTapChain> chain(new ProcessingTapChain);
    if (configure)
        configure(chain.get());
    if (!pipeline->setOutputFilterChain(chain.get(), false))
        return false;
    display->registerChain(chain.get());

    class Render : public PipelineThread::ExitBlocker {
        Graphing::DisplayDynamicContext context;
        std::unique_ptr<ProcessingTapChain> chain;
        std::unique_ptr<Graphing::Display> display;
        QString filename;
        QSize size;
        bool opaque;
        enum class State {
            Waiting, Ready, Terminated,
        } state;
    public:
        Render(ExecThread *execThread,
               LuaPipeline::PipelineThread *pipelineThread,
               std::unique_ptr<Graphing::Display> &&display,
               std::unique_ptr<ProcessingTapChain> &&chain,
               QString filename,
               QSize size) : context(execThread->displayDynamicContext),
                             chain(std::move(chain)),
                             display(std::move(display)),
                             filename(std::move(filename)),
                             size(std::move(size)),
                             opaque(false),
                             state(State::Waiting)
        {
            context.setInteractive(false);

            QObject::connect(this->display.get(), &Graphing::Display::readyForFinalPaint,
                             pipelineThread, std::bind(&Render::makeReady, this),
                             Qt::DirectConnection);
            QObject::connect(this->display.get(), &Graphing::Display::readyForFinalPaint,
                             pipelineThread, std::bind(&PipelineThread::exitPoll, pipelineThread),
                             Qt::QueuedConnection);

            QTimer::singleShot(0, pipelineThread,
                               std::bind(&PipelineThread::exitPoll, pipelineThread));
        }

        virtual ~Render() = default;

        void makeReady()
        { state = State::Ready; }

        bool isComplete() override
        { return state != State::Waiting; }

        void wait() override
        {
            if (state != State::Ready)
                return;

            qCDebug(log_luaexec_pipeline) << "Writing display to" << filename;

            if (filename.endsWith(".svg", Qt::CaseInsensitive)) {
                QSvgGenerator svg;
                svg.setFileName(filename);
                svg.setSize(size);
                svg.setViewBox(QRect(0, 0, size.width(), size.height()));
                QPainter painter;
                painter.begin(&svg);
                display->paint(&painter, QRectF(0, 0, size.width(), size.height()), context);
                painter.end();
            } else {
                QImage img(size, QImage::Format_ARGB32);
                if (opaque)
                    img.fill(0xFFFFFFFF);
                else
                    img.fill(0x00FFFFFF);
                QPainter painter;
                painter.begin(&img);
                display->paint(&painter, QRectF(0, 0, size.width(), size.height()), context);
                painter.end();
                img.save(filename);
            }

            display.reset();
        }

        void abort() override
        {
            chain->signalTerminate();
            state = State::Terminated;
        }
    };

    pipelineThread.blockers
                  .emplace_back(
                          new Render(thread, &pipelineThread, std::move(display), std::move(chain),
                                     QString::fromStdString(filename), QSize(width, height)));

    return true;
#endif
}

bool LuaPipeline::setDisplayOutputWindow(StreamPipeline *pipeline,
                                         const CreateDisplay &create,
                                         const ConfigureDisplayChain &configure)
{
#ifndef BUILD_GUI
    Q_UNUSED(pipeline);
    Q_UNUSED(create);
    Q_UNUSED(configure);
    return false;
#else
    Q_ASSERT(QThread::currentThread() == &pipelineThread);

    class DisplayWindow;

    struct Shared {
        std::mutex mutex;
        LuaPipeline *pipeline;
        DisplayWindow *window;

        explicit Shared(LuaPipeline *pipeline) : pipeline(pipeline), window(nullptr)
        { }

        void releaseWindow()
        {
            std::unique_lock<std::mutex> lock(mutex);
            window = nullptr;
            if (!pipeline)
                return;
            Q_ASSERT(QThread::currentThread() != &pipeline->pipelineThread);
            std::unique_lock<std::mutex> pipelineLock(pipeline->mutex);
            if (!pipeline->pipelineThread.pipeline)
                return;
            /* The pipeline itself cannot be deleted without acquiring the mutex, so
             * this is safe to release */
            lock.unlock();

            Q_ASSERT(QThread::currentThread() != &pipeline->pipelineThread);
            Threading::runQueuedFunctor(pipeline->pipelineThread.context.get(),
                                        std::bind(&PipelineThread::exitPoll,
                                                  &pipeline->pipelineThread));
        }

        void releasePipeline()
        {
            std::lock_guard<std::mutex> lock(mutex);
            pipeline = nullptr;
        }

        bool isWindowClosed()
        {
            std::lock_guard<std::mutex> lock(mutex);
            return !window;
        }

        bool attachChain(Graphing::DisplayWidgetTapChain *chain)
        {
            std::unique_lock<std::mutex> lock(mutex);
            if (!pipeline)
                return false;
            Q_ASSERT(QThread::currentThread() != &pipeline->pipelineThread);
            std::unique_lock<std::mutex> pipelineLock(pipeline->mutex);
            if (!pipeline->pipelineThread.pipeline)
                return false;
            /* The pipeline itself cannot be deleted without acquiring the mutex, so
             * this is safe to release */
            lock.unlock();

            bool ok = true;
            pipeline->pipelineThread
                    .manipulate(pipelineLock, [this, &ok, chain](StreamPipeline *streamPipeline) {
                        {
                            std::unique_lock<std::mutex> startedLock(pipeline->mutex);
                            if (pipeline->started) {
                                startedLock.unlock();
                                streamPipeline->waitInEventLoop();
                            }
                        }
                        ok = streamPipeline->setOutputFilterChain(chain, true);
                    });

            return true;
        }

        void chainRestart()
        {
            std::unique_lock<std::mutex> lock(mutex);
            if (!pipeline)
                return;
            Q_ASSERT(QThread::currentThread() != &pipeline->pipelineThread);
            std::unique_lock<std::mutex> pipelineLock(pipeline->mutex);
            if (!pipeline->pipelineThread.pipeline)
                return;
            /* The pipeline itself cannot be deleted without acquiring the mutex, so
             * this is safe to release */
            lock.unlock();

            pipeline->pipelineThread
                    .manipulate(pipelineLock, [this](StreamPipeline *streamPipeline) {
                        {
                            std::lock_guard<std::mutex> startedLock(pipeline->mutex);
                            if (!pipeline->started)
                                return;
                        }
                        streamPipeline->start();
                    });
        }
    };
    auto shared = std::make_shared<Shared>(this);

    class DisplayWindow : public QMainWindow {
        class TimerBackoffDisplayWidget : public Graphing::DisplayWidget {
            QTimer *repaintTimer;
            int baseInterval;
            int thresholdInterval;
        public:
            TimerBackoffDisplayWidget(std::unique_ptr<CPD3::Graphing::Display> &&setDisplay,
                                      QWidget *parent,
                                      QTimer *timer) : Graphing::DisplayWidget(
                    std::move(setDisplay), parent),
                                                       repaintTimer(timer),
                                                       baseInterval(timer->interval()),
                                                       thresholdInterval(
                                                               static_cast<int>(timer->interval() *
                                                                       0.1))
            { }

        protected:
            void paintEvent(QPaintEvent *event) override
            {
                ElapsedTimer timeTaken;
                timeTaken.start();
                DisplayWidget::paintEvent(event);
                int elapsed = static_cast<int>(timeTaken.elapsed());
                if (elapsed > thresholdInterval) {
                    int adjustedInterval = baseInterval + elapsed;
                    if (abs(repaintTimer->interval() - adjustedInterval) > 10) {
                        repaintTimer->setInterval(adjustedInterval);
                    }
                } else {
                    if (repaintTimer->interval() != baseInterval) {
                        repaintTimer->setInterval(baseInterval);
                    }
                }
            }
        };

        TimerBackoffDisplayWidget *displayWidget;
        std::unique_ptr<Graphing::DisplayWidgetTapChain> chain;
        ConfigureDisplayChain configure;
        std::shared_ptr<Shared> shared;

    public:
        DisplayWindow(LuaPipeline *pipeline,
                      std::unique_ptr<Graphing::Display> &&display,
                      const ConfigureDisplayChain &configure,
                      const std::shared_ptr<Shared> &shared)
                : QMainWindow(), configure(configure), shared(shared)
        {
            Q_ASSERT(
                    QThread::currentThread() == static_cast<QObject *>(pipeline->thread)->thread());

            setWindowTitle(tr("CPD3 Display - %1").arg(QString::fromStdString(pipeline->id)));

            auto timer = new QTimer(this);
            if (pipeline->thread->settings) {
                timer->setInterval(
                        pipeline->thread->settings->value("display/repaintinterval", 1000).toInt());
            } else {
                timer->setInterval(1000);
            }
            timer->setSingleShot(false);

            displayWidget = new TimerBackoffDisplayWidget(std::move(display), this, timer);
            {
                auto context = pipeline->thread->displayDynamicContext;
                context.setInteractive(true);
                displayWidget->setContext(context);
            }
            displayWidget->setResetSelectsAll(true);
            QObject::connect(displayWidget, &Graphing::DisplayWidget::internalZoom, displayWidget,
                             &Graphing::DisplayWidget::applyZoom);
            QObject::connect(displayWidget, &Graphing::DisplayWidget::visibleZoom, displayWidget,
                             &Graphing::DisplayWidget::applyZoom);
            QObject::connect(displayWidget, &Graphing::DisplayWidget::globalZoom, displayWidget,
                             &Graphing::DisplayWidget::applyZoom);
            QObject::connect(displayWidget, &Graphing::DisplayWidget::timeRangeSelected,
                             displayWidget, &Graphing::DisplayWidget::setVisibleTimeRange,
                             Qt::QueuedConnection);
            setCentralWidget(displayWidget);
            QObject::connect(timer, &QTimer::timeout, displayWidget,
                             static_cast<void (TimerBackoffDisplayWidget::*)()>(&TimerBackoffDisplayWidget::update));

            setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

            QObject::connect(displayWidget, &Graphing::DisplayWidget::requestChainReload, this,
                             &DisplayWindow::chainReload);

            setAttribute(Qt::WA_DeleteOnClose);
        }

        virtual ~DisplayWindow()
        { shared->releaseWindow(); }

        QSize sizeHint() const override
        { return QSize(801, 600); }

        void chainReload()
        {
            if (chain) {
                chain->signalTerminate();
                chain->wait();
            }
            chain.reset(new Graphing::DisplayWidgetTapChain);
            if (configure)
                configure(chain.get());
            if (!shared->attachChain(chain.get()))
                return;
            displayWidget->registerChain(chain.get());
            shared->chainRestart();
        }

        bool initializeChain(StreamPipeline *pipeline)
        {
            Q_ASSERT(!chain);
            chain.reset(new Graphing::DisplayWidgetTapChain);
            if (configure)
                configure(chain.get());
            if (!pipeline->setOutputFilterChain(chain.get(), true))
                return false;
            displayWidget->registerChain(chain.get());
            return true;
        }
    };

    class Blocker : public PipelineThread::ExitBlocker {
        std::shared_ptr<Shared> shared;
    public:
        Blocker(LuaPipeline::PipelineThread *pipelineThread, const std::shared_ptr<Shared> &shared)
                : shared(shared)
        { }

        virtual ~Blocker()
        {
            if (shared)
                shared->releasePipeline();
        }

        bool isComplete() override
        {
            if (!shared)
                return true;
            return shared->isWindowClosed();
        }

        void wait() override
        {
            if (!shared)
                return;
            shared->releasePipeline();
            shared.reset();
        }

        void abort() override
        {
            if (!shared)
                return;
            shared->releasePipeline();
            shared.reset();
        }
    };

    /* Create the window on the main thread */
    bool ok = true;
    Threading::runBlockingFunctor(thread, [&]() {
        std::unique_ptr<Graphing::Display> display(create());
        if (!display) {
            qCDebug(log_luaexec_pipeline) << "Failed to create display for pipeline" << id;
            ok = false;
            return;
        }

        auto window = new DisplayWindow(this, std::move(display), configure, shared);
        if (!window->initializeChain(pipeline)) {
            qCDebug(log_luaexec_pipeline) << "Display chain initialize failed for pipeline" << id;
            delete window;
            return;
        }

        shared->window = window;
        pipelineThread.blockers.emplace_back(new Blocker(&pipelineThread, shared));

        window->show();

        qCDebug(log_luaexec_pipeline) << "Pipeline" << id << "set to window display";
    });

    QTimer::singleShot(0, &pipelineThread, std::bind(&PipelineThread::exitPoll, &pipelineThread));

    return ok;
#endif
}

void LuaPipeline::argumentsReady(CPD3::Lua::Engine::Frame &frame)
{
    if (!inputArgument && !outputArgument)
        return;

    bool ok = true;
    std::string error;
    pipelineThread.manipulate([this, &ok, &error](StreamPipeline *pipeline) {
        if (inputArgument) {
            if (!inputArgument->configure(pipeline)) {
                ok = false;
                error = pipeline->getInputError().toStdString();
                return;
            }
        }
        if (outputArgument) {
            if (!outputArgument->configure(pipeline)) {
                ok = false;
                error = pipeline->getOutputError().toStdString();
                return;
            }
        }
    });
    if (!ok) {
        frame.throwError(error);
        inputArgument.reset();
        outputArgument.reset();
        return;
    }

    inputArgument.reset();
    outputArgument.reset();
}

CPD3::Data::Archive::Access &LuaPipeline::archive()
{ return thread->archive; }


LuaPipeline::PipelineThread::PipelineThread(LuaPipeline &parent) : parent(parent), pipeline(nullptr)
{ }

LuaPipeline::PipelineThread::~PipelineThread() = default;

void LuaPipeline::PipelineThread::run()
{
    bool enablePipelineAcceleration = false;
    bool enablePipelineSerialization = false;
    if (char *env = getenv("CPD3CLI_ACCELERATION")) {
        if (env[0] && atoi(env))
            enablePipelineAcceleration = true;
    }
    if (char *env = getenv("CPD3CLI_COMBINEPROCESSES")) {
        if (env[0] && atoi(env))
            enablePipelineSerialization = true;
    }

    {
        auto ctx = new QObject;
        auto ptr = new StreamPipeline(enablePipelineAcceleration, enablePipelineSerialization);
        ptr->setOutputIngress(&discardSink);
        if (auto target = ptr->setInputExternal()) {
            target->endData();
        }

        ptr->finished.connect(ctx, std::bind(&PipelineThread::exitPoll, this), true);

        std::lock_guard<std::mutex> lock(parent.mutex);
        pipeline.reset(ptr);
        context.reset(ctx);
    }
    parent.notify.notify_all();

    blockPipeline();

    qCDebug(log_luaexec_pipeline) << "Thread started for pipeline" << parent.id;
    int rc = QThread::exec();
    qCDebug(log_luaexec_pipeline) << "Thread exited for pipeline" << parent.id;

    if (rc == 0) {
        for (const auto &b : blockers) {
            b->wait();
        }
    } else {
        for (const auto &b : blockers) {
            b->abort();
        }
    }
    blockers.clear();
    pipeline->finished.disconnect();

    if (inputConnector) {
        inputConnector->disconnect();
        inputConnector.reset();
    }

    qCDebug(log_luaexec_pipeline) << "Thread completed for pipeline" << parent.id;
    std::unique_ptr<StreamPipeline> release;
    {
        std::lock_guard<std::mutex> lock(parent.mutex);
        release = std::move(pipeline);
        pipeline.reset();
        context.reset();
        parent.notify.notify_all();
    }
}

void LuaPipeline::PipelineThread::manipulate(std::unique_lock<std::mutex> &lock,
                                             const std::function<void(StreamPipeline *)> &function)
{
    if (!pipeline)
        return;
    bool complete = false;
    Threading::runQueuedFunctor(context.get(), [this, &function, &complete]() {
        Q_ASSERT(pipeline);
        function(pipeline.get());
        std::lock_guard<std::mutex> inner(parent.mutex);
        complete = true;
        parent.notify.notify_all();
    });
    parent.notify.wait(lock, [this, &complete]() { return complete || context.get() == nullptr; });
}

void LuaPipeline::PipelineThread::manipulate(const std::function<void(StreamPipeline *)> &function)
{
    std::unique_lock<std::mutex> lock(parent.mutex);
    return manipulate(lock, function);
}

void LuaPipeline::PipelineThread::exitPoll()
{
    for (auto check = blockers.begin(); check != blockers.end();) {
        if (!(*check)->isComplete()) {
            ++check;
            continue;
        }
        (*check)->wait();
        check = blockers.erase(check);
    }

    if (blockers.empty()) {
        this->exit(0);
    }
}

void LuaPipeline::PipelineThread::blockPipeline()
{
    class PipelineBlocker : public ExitBlocker {
        StreamPipeline *pipeline;
    public:
        explicit PipelineBlocker(StreamPipeline *pipeline) : pipeline(pipeline)
        { }

        virtual ~PipelineBlocker() = default;

        bool isComplete() override
        { return pipeline->isFinished(); }

        void wait() override
        { pipeline->waitInEventLoop(); }

        void abort() override
        { pipeline->signalTerminate(); }
    };
    blockers.emplace_back(new PipelineBlocker(pipeline.get()));

    QTimer::singleShot(0, this, std::bind(&PipelineThread::exitPoll, this));
}

LuaPipeline::InputConnector::InputConnector(ExecThread *thread) : pipeline(nullptr),
                                                                  receivedStart(FP::undefined()),
                                                                  started(false),
                                                                  ended(false)
{
    thread->terminateRequested.connect(*this, std::bind(&InputConnector::endData, this));
}

LuaPipeline::InputConnector::~InputConnector()
{ Threading::Receiver::disconnect(); }

void LuaPipeline::InputConnector::incomingData(const SequenceValue &value)
{
    std::lock_guard<std::mutex> lock(mutex);
    if (!pipeline || ended)
        return;
    if (Range::compareStart(receivedStart, value.getStart()) > 0)
        return;
    receivedStart = value.getStart();
    if (!started) {
        buffer.emplace_back(value);
        return;
    }
    pipeline->incomingData(value);
}

void LuaPipeline::InputConnector::incomingData(SequenceValue &&value)
{
    std::lock_guard<std::mutex> lock(mutex);
    if (!pipeline || ended)
        return;
    if (Range::compareStart(receivedStart, value.getStart()) > 0)
        return;
    receivedStart = value.getStart();
    if (!started) {
        buffer.emplace_back(std::move(value));
        return;
    }
    pipeline->incomingData(std::move(value));
}

void LuaPipeline::InputConnector::endData()
{
    std::lock_guard<std::mutex> lock(mutex);
    if (started) {
        if (pipeline && !ended)
            pipeline->endData();
    }
    ended = true;
}

void LuaPipeline::InputConnector::disconnect()
{
    std::lock_guard<std::mutex> lock(mutex);
    pipeline = nullptr;
}

void LuaPipeline::InputConnector::connect(CPD3::Data::StreamSink *target)
{
    std::lock_guard<std::mutex> lock(mutex);
    pipeline = target;
    ended = false;
    receivedStart = FP::undefined();
}

void LuaPipeline::InputConnector::start()
{
    std::lock_guard<std::mutex> lock(mutex);
    if (!pipeline)
        return;
    Q_ASSERT(!started);
    started = true;
    if (!buffer.empty()) {
        pipeline->incomingData(std::move(buffer));
        buffer = CPD3::Data::SequenceValue::Transfer();
    }
    if (ended) {
        pipeline->endData();
    }
}

LuaPipeline::MultiplexerInput::MultiplexerInput(ExecThread *thread,
                                                const std::shared_ptr<
                                                        CPD3::Data::SinkMultiplexer> &mux,
                                                bool unsorted) : mux(mux)
{
    if (unsorted) {
        muxSink = mux->createUnsorted();
    } else {
        muxSink = mux->createSink();
    }
    thread->terminateRequested.connect(*this, std::bind(&MultiplexerInput::endData, this));
}

LuaPipeline::MultiplexerInput::~MultiplexerInput()
{
    Threading::Receiver::disconnect();
    if (muxSink) {
        muxSink->endData();
        muxSink = nullptr;
    }
}

void LuaPipeline::MultiplexerInput::incomingData(const CPD3::Data::SequenceValue::Transfer &values)
{
    std::lock_guard<std::mutex> lock(mutex);
    if (!muxSink)
        return;
    muxSink->incomingData(values);
}

void LuaPipeline::MultiplexerInput::incomingData(CPD3::Data::SequenceValue::Transfer &&values)
{
    std::lock_guard<std::mutex> lock(mutex);
    if (!muxSink)
        return;
    muxSink->incomingData(std::move(values));
}

void LuaPipeline::MultiplexerInput::incomingData(const CPD3::Data::SequenceValue &value)
{
    std::lock_guard<std::mutex> lock(mutex);
    if (!muxSink)
        return;
    muxSink->incomingData(value);
}

void LuaPipeline::MultiplexerInput::incomingData(CPD3::Data::SequenceValue &&value)
{
    std::lock_guard<std::mutex> lock(mutex);
    if (!muxSink)
        return;
    muxSink->incomingData(std::move(value));
}

void LuaPipeline::MultiplexerInput::endData()
{
    std::lock_guard<std::mutex> lock(mutex);
    if (!muxSink)
        return;
    muxSink->endData();
    muxSink = nullptr;
}
