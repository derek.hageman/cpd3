/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QFile>
#include <QLoggingCategory>

#ifdef BUILD_GUI

#include <QMessageBox>
#include <QSettings>
#include "guicore/guicore.hxx"

#endif

#include "luascript/engine.hxx"
#include "clicore/terminaloutput.hxx"
#include "core/qtcompat.hxx"
#include "execthread.hxx"
#include "pipeline.hxx"
#include "general.hxx"
#include "processor.hxx"
#include "fanout.hxx"
#include "acquisition.hxx"
#include "action.hxx"

Q_DECLARE_LOGGING_CATEGORY(log_luaexec)

using namespace CPD3;
using namespace CPD3::CLI;

ExecThread::ExecThread(const QStringList &arguments, bool usingGUI, std::string target) : arguments(
        this, arguments, usingGUI),
                                                                                          archive(),
                                                                                          usingGUI(
                                                                                                  usingGUI),
                                                                                          target(std::move(
                                                                                                  target)),
                                                                                          inSilentAbort(
                                                                                                  false),
                                                                                          processorsTerminated(
                                                                                                  false)
{
#ifdef BUILD_GUI
    settings = new QSettings(CPD3GUI_ORGANIZATION, CPD3GUI_APPLICATION, this);
    displayDynamicContext.setInteractive(usingGUI);
    displayDynamicContext.setSettings(settings);
#endif
}

ExecThread::~ExecThread()
{
    thread.join();
#ifdef BUILD_GUI
    delete settings;
#endif
}

void ExecThread::start()
{
    thread = std::thread(std::bind(&ExecThread::run, this));
}

void ExecThread::signalTerminate()
{
    qCDebug(log_luaexec) << "Requesting script thread termination";

    terminateRequested();

    {
        std::lock_guard<std::mutex> lock(processorMutex);
        processorsTerminated = true;
    }
    processorNotify.notify_all();
}

void ExecThread::run()
{
    Lua::Engine engine;

    qCDebug(log_luaexec) << "Lua thread started";

    {
        Lua::Engine::Frame frame(engine);
        setup(frame);
    }

    {
        Lua::Engine::Call exec(engine);
        if (QFile::exists(QString::fromStdString(target))) {
            if (!exec.pushFile(target, false)) {
                auto error = engine.errorDescription();
                Threading::runQueuedFunctor(this, [this, error]() {
#ifndef NO_CONSOLE
                    if (!usingGUI) {
                        TerminalOutput::simpleWordWrap(QObject::tr("Error loading file %1").arg(
                                QString::fromStdString(error)), true);
                        QCoreApplication::exit(1);
                        return;
                    }
#endif
#ifdef BUILD_GUI
                    QMessageBox::critical(nullptr, QObject::tr("Error loading file %1").arg(
                            QString::fromStdString(target)), QString::fromStdString(error));
#endif
                    QCoreApplication::exit(1);
                });
                qCDebug(log_luaexec) << "Lua thread exiting due to file load failure on" << target
                                     << error;
                return;
            }
            displayName = target;
        } else {
            if (!exec.pushChunk(target, false)) {
                auto error = engine.errorDescription();
                Threading::runQueuedFunctor(this, [this, error]() {
#ifndef NO_CONSOLE
                    if (!usingGUI) {
                        TerminalOutput::simpleWordWrap(
                                QObject::tr("Error parsing script code: %1").arg(
                                        QString::fromStdString(error)), true);
                        QCoreApplication::exit(1);
                        return;
                    }
#endif
#ifdef BUILD_GUI
                    QMessageBox::critical(nullptr, QObject::tr("Cannot parse script").arg(
                            QString::fromStdString(target)),
                                          QObject::tr("Error loading parsing script code: %1").arg(
                                                  QString::fromStdString(error)));
#endif
                    QCoreApplication::exit(1);
                });
                qCDebug(log_luaexec) << "Lua thread exiting due to parse failure" << error;
                return;
            }
            if (usingGUI) {
                displayName = "da.gluaexec";
            } else {
                displayName = "da.luaexec";
            }
        }

        if (!exec.execute()) {
            auto error = engine.errorDescription();
            engine.clearError();

            terminateRequested();
            {
                qCDebug(log_luaexec) << "Finalizing Lua script";
                Lua::Engine::Frame frame(exec);
                startPipelines(frame);
                startPipelines.disconnectImmediate();
                frame.clear();
                finalizeAcquisition(frame);
                finalizeAcquisition.disconnectImmediate();
                frame.clearError();
                finalizeBuffers(frame);
                finalizeBuffers.disconnectImmediate();
                frame.clearError();
                completePipelines(frame);
                completePipelines.disconnectImmediate();
            }

            if (inSilentAbort) {
                qCDebug(log_luaexec) << "Lua thread exiting due to abort" << error;
                return;
            }

            Threading::runQueuedFunctor(this, [this, error]() {
#ifndef NO_CONSOLE
                if (!usingGUI) {
                    TerminalOutput::simpleWordWrap(QObject::tr("Error executing script: %1").arg(
                            QString::fromStdString(error)), true);
                    QCoreApplication::exit(1);
                    return;
                }
#endif
#ifdef BUILD_GUI
                QMessageBox::critical(nullptr, QObject::tr("Script error"),
                                      QObject::tr("Error executing script: %1").arg(
                                              QString::fromStdString(error)));
#endif
                QCoreApplication::exit(1);
            });
            qCDebug(log_luaexec) << "Lua thread exiting due to execution failure" << error;
            return;
        }
    }

    {
        qCDebug(log_luaexec) << "Finalizing Lua script";
        Lua::Engine::Frame frame(engine);
        arguments.evaluate(frame);
        startPipelines(frame);
        startPipelines.disconnectImmediate();
        frame.clearError();
        finalizeAcquisition(frame);
        finalizeAcquisition.disconnectImmediate();
        frame.clearError();

        if (!executeProcessors(frame)) {
            auto error = engine.errorDescription();
            frame.clearError();
            finalizeBuffers(frame);
            finalizeBuffers.disconnectImmediate();
            frame.clearError();
            completePipelines(frame);
            completePipelines.disconnectImmediate();

            if (inSilentAbort) {
                qCDebug(log_luaexec) << "Lua thread exiting due to abort" << error;
                return;
            }

            Threading::runQueuedFunctor(this, [this, error]() {
#ifndef NO_CONSOLE
                if (!usingGUI) {
                    TerminalOutput::simpleWordWrap(
                            QObject::tr("Error executing script processor: %1").arg(
                                    QString::fromStdString(error)), true);
                    QCoreApplication::exit(1);
                    return;
                }
#endif
#ifdef BUILD_GUI
                QMessageBox::critical(nullptr, QObject::tr("Script error"),
                                      QObject::tr("Error executing script processor: %1").arg(
                                              QString::fromStdString(error)));
#endif
                QCoreApplication::exit(1);
            });

            qCDebug(log_luaexec) << "Lua thread exiting due to processor failure" << error;
            return;
        }

        frame.clearError();
        finalizeBuffers(frame);
        finalizeBuffers.disconnectImmediate();
        frame.clearError();
        completePipelines(frame);
        completePipelines.disconnectImmediate();
    }

    Threading::runQueuedFunctor(this, std::bind(&QCoreApplication::exit, 0));

    qCDebug(log_luaexec) << "Lua thread exiting normally";
}

void ExecThread::setup(Lua::Engine::Frame &frame)
{
    {
        Lua::Engine::Assign assign(frame, frame.global(), "print");
        assign.push(std::function<void(Lua::Engine::Entry &)>([](Lua::Engine::Entry &entry) {
            for (std::size_t i = 0, max = entry.size(); i < max; i++) {
                qCInfo(log_luaexec) << entry[i].toOutputString();
            }
        }));
    }
    {
        Lua::Engine::Assign assign(frame, frame.global(), "alert");
        assign.push(std::function<void(Lua::Engine::Entry &)>([this](Lua::Engine::Entry &entry) {
            std::string message;
            for (std::size_t i = 0, max = entry.size(); i < max; i++) {
                message += entry[i].toOutputString();
            }

#ifndef NO_CONSOLE
            if (!usingGUI) {
                TerminalOutput::simpleWordWrap(QString::fromStdString(message), true);
                return;
            }
#endif
#ifdef BUILD_GUI
            Threading::runBlockingFunctor(this, [message]() {
                QMessageBox::information(nullptr, QObject::tr("CPD3", "alert title"),
                                         QString::fromStdString(message));
            });
#endif
        }));
    }

    arguments.install(frame);
    LuaPipeline::install(this, frame);
    GeneralBuffered::install(this, frame);
    ProcessorBuffered::install(this, frame);
    FanoutProcessor::install(this, frame);
    LuaAcquisition::install(this, frame);
    LuaAction::install(this, frame);
}

bool ExecThread::executeProcessors(Lua::Engine::Frame &frame)
{
    std::unique_lock<std::mutex> lock(processorMutex);
    for (;;) {
        if (processorsTerminated)
            return true;
        if (processorHandlers.empty())
            return true;

        bool spin = false;
        for (auto p = processorHandlers.begin(), endP = processorHandlers.end(); p != endP; ++p) {
            Q_ASSERT(lock);

            auto processor = p->lock();
            if (!processor) {
                processorHandlers.erase(p);
                spin = true;
                break;
            }

            switch (processor->execute(frame, lock)) {
            case Processor::ExecutionState::Waiting:
                break;
            case Processor::ExecutionState::Complete:
                processorHandlers.erase(p);
                spin = true;
                break;
            case Processor::ExecutionState::DidUnlock:
                spin = true;
                break;
            }
            if (frame.inError())
                return false;
            if (spin)
                break;
        }
        if (!lock) {
            lock.lock();
            continue;
        }
        if (spin)
            continue;

        processorNotify.wait(lock);
    }
}


ExecThread::Processor::Processor(ExecThread *thread) : thread(thread),
                                                       mutex(thread->processorMutex),
                                                       notify(thread->processorNotify)
{ }

ExecThread::Processor::~Processor() = default;

void ExecThread::Processor::connect()
{
    std::lock_guard<std::mutex> lock(mutex);
    thread->processorHandlers.emplace_back(shared_from_this());
}