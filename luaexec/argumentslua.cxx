/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <limits>

#include "arguments.hxx"
#include "core/component.hxx"
#include "core/timeutils.hxx"
#include "luascript/libs/time.hxx"

using namespace CPD3;

namespace {
class LuaOptionBase : public Lua::Engine::Data {
    std::shared_ptr<Arguments::Option> option;

    static const std::string metatableRegistryName;

    void meta_index(Lua::Engine::Entry &entry, const std::vector<Lua::Engine::Reference> &upvalues)
    {
        if (entry.size() < 2) {
            entry.throwError("Invalid index");
            return;
        }
        if (methodIndexHandler(entry, Lua::Engine::Table(upvalues.front())))
            return;

        auto key = entry[1].toString();
        if (key == "value") {
            auto opt = option->value(entry);
            if (!opt) {
                entry.clear();
                entry.pushNil();
                return;
            }
            entry.pushNil();
            push(entry, opt);
            entry.replaceWithBack(0);
            entry.propagate(1);
            return;
        }
    }

public:
    virtual ~LuaOptionBase() = default;

    static void install(Lua::Engine::Frame &root)
    {
        Lua::Engine::Frame frame(root);

        auto mt = pushMetatable(frame);

        {
            Lua::Engine::Assign assign(frame, mt, "__call");
            pushMethod<LuaOptionBase>(assign, &LuaOptionBase::method_get);
        }
        {
            Lua::Engine::Assign assign(frame, mt, "__newindex");
            pushMethod<LuaOptionBase>(assign, &LuaOptionBase::meta_newindex);
        }

        {
            Lua::Engine::Assign assign(frame, mt, "__index");
            auto methods = assign.pushTable();

            {
                Lua::Engine::Assign ma(assign, methods, "get");
                pushMethod<LuaOptionBase>(ma, &LuaOptionBase::method_get);
            }

            pushMethod<LuaOptionBase>(assign, &LuaOptionBase::meta_index, 1);
        }

        frame.registry().set(metatableRegistryName, mt);
    }

    template<typename OptionType>
    static std::unique_ptr<OptionType> constructStandard(Lua::Engine::Entry &entry)
    {
        if (entry.empty()) {
            entry.throwError("Argument name missing");
            return {};
        }
        auto name = entry[0].toQString();
        if (name.isEmpty()) {
            entry.throwError("Argument name empty");
            return {};
        }

        QString display;
        if (entry.size() > 1)
            display = entry[1].toQString();
        QString description;
        if (entry.size() > 2)
            description = entry[2].toQString();
        QString defaultBehavior;
        if (entry.size() > 3)
            defaultBehavior = entry[3].toQString();
        int sortPriority = 0;
        if (entry.size() > 4) {
            auto check = entry[4].toInteger();
            if (INTEGER::defined(check))
                sortPriority = static_cast<int>(check);
        }

        return std::unique_ptr<OptionType>(
                new OptionType(name, display, description, defaultBehavior, sortPriority));
    }

protected:
    explicit LuaOptionBase(std::shared_ptr<Arguments::Option> option) : option(std::move(option))
    { }

    void initialize(const Lua::Engine::Reference &self, Lua::Engine::Frame &frame) override
    {
        frame.push(frame.registry(), metatableRegistryName);
        setMetatable(self, frame.back());
    }

    virtual void meta_newindex(Lua::Engine::Entry &entry)
    {
        entry.throwError("Invalid assignment");
    }

    virtual void method_get(Lua::Engine::Entry &entry)
    {
        auto opt = value(entry);
        if (!opt)
            return;
        push(entry, opt);
        entry.replaceWithBack(0);
        entry.propagate(1);
    }

    virtual void push(Lua::Engine::Frame &frame, ComponentOptionBase *option) = 0;

    template<typename T = CPD3::ComponentOptionBase>
    T *modify() const
    { return option->modify<T>(); }

    template<typename T = CPD3::ComponentOptionBase>
    T *value(Lua::Engine::Frame &frame) const
    { return option->value<T>(frame); }
};

const std::string LuaOptionBase::metatableRegistryName("CPD3_luaexecargumentsoption_metatable");


class LuaArgumentStations final : public Lua::Engine::Data {
    std::shared_ptr<Arguments::Stations> argument;

    static const std::string metatableRegistryName;

    void meta_index(Lua::Engine::Entry &entry, const std::vector<Lua::Engine::Reference> &upvalues)
    {
        if (entry.size() < 2) {
            entry.throwError("Invalid index");
            return;
        }
        if (methodIndexHandler(entry, Lua::Engine::Table(upvalues.front())))
            return;

        auto key = entry[1].toString();
        if (key == "value") {
            auto val = argument->value(entry);
            entry.clear();
            if (val.size() == 1 &&
                    argument->getMaximumAllowed() == 1 &&
                    argument->getMinimumRequired() == 1) {
                entry.push(*(val.begin()));
            } else {
                auto target = entry.pushTable();
                Lua::Engine::Append append(entry, target, true);
                for (const auto &add : val) {
                    append.push(add);
                }
            }
            entry.propagate(1);
            return;
        }
    }

    void meta_newindex(Lua::Engine::Entry &entry)
    {
        if (entry.size() < 3) {
            entry.throwError("Invalid assignment");
            return;
        }

        auto key = entry[1].toString();
        if (key == "min" || key == "minimum" || key == "required") {
            auto n = entry[2].toInteger();
            if (!INTEGER::defined(n) || n < 0) {
                entry.throwError("Invalid minimum");
                return;
            }
            argument->setMinimumRequired(static_cast<std::size_t>(n));
            return;
        } else if (key == "max" || key == "maximum" || key == "allowed") {
            auto n = entry[2].toInteger();
            if (!INTEGER::defined(n) || n < 0) {
                entry.throwError("Invalid maximum");
                return;
            }
            argument->setMinimumRequired(static_cast<std::size_t>(n));
            return;
        }

        entry.throwError("Invalid assignment");
    }

    void method_list(Lua::Engine::Entry &entry)
    {
        auto val = argument->value(entry);
        entry.clear();
        auto target = entry.pushTable();
        Lua::Engine::Append append(entry, target, true);
        for (const auto &add : val) {
            append.push(add);
        }
        entry.propagate(1);
    }

    void method_get(Lua::Engine::Entry &entry)
    {
        auto val = argument->value(entry);
        entry.clear();
        for (const auto &add : val) {
            entry.push(add);
        }
        entry.propagate();
    }

public:
    explicit LuaArgumentStations(std::shared_ptr<Arguments::Stations> argument) : argument(
            std::move(argument))
    { }

    virtual ~LuaArgumentStations() = default;

    static void install(Lua::Engine::Frame &root)
    {
        Lua::Engine::Frame frame(root);

        auto mt = pushMetatable(frame);

        {
            Lua::Engine::Assign assign(frame, mt, "__call");
            pushMethod<LuaArgumentStations>(assign, &LuaArgumentStations::method_get);
        }
        {
            Lua::Engine::Assign assign(frame, mt, "__newindex");
            pushMethod<LuaArgumentStations>(assign, &LuaArgumentStations::meta_newindex);
        }

        {
            Lua::Engine::Assign assign(frame, mt, "__index");
            auto methods = assign.pushTable();

            {
                Lua::Engine::Assign ma(assign, methods, "get");
                pushMethod<LuaArgumentStations>(ma, &LuaArgumentStations::method_get);
            }
            {
                Lua::Engine::Assign ma(assign, methods, "list");
                pushMethod<LuaArgumentStations>(ma, &LuaArgumentStations::method_list);
            }

            pushMethod<LuaArgumentStations>(assign, &LuaArgumentStations::meta_index, 1);
        }

        frame.registry().set(metatableRegistryName, mt);
    }

protected:

    void initialize(const Lua::Engine::Reference &self, Lua::Engine::Frame &frame) override
    {
        frame.push(frame.registry(), metatableRegistryName);
        setMetatable(self, frame.back());
    }
};

const std::string
        LuaArgumentStations::metatableRegistryName("CPD3_luaexecargumentsstations_metatable");


class LuaArgumentTimePoint final : public Lua::Engine::Data {
    std::shared_ptr<Arguments::TimePoint> argument;

    static const std::string metatableRegistryName;

    void meta_index(Lua::Engine::Entry &entry, const std::vector<Lua::Engine::Reference> &upvalues)
    {
        if (entry.size() < 2) {
            entry.throwError("Invalid index");
            return;
        }
        if (methodIndexHandler(entry, Lua::Engine::Table(upvalues.front())))
            return;

        auto key = entry[1].toString();
        if (key == "value") {
            entry.push(argument->value(entry));
            entry.replaceWithBack(0);
            entry.propagate(1);
            return;
        }
    }

    void meta_newindex(Lua::Engine::Entry &entry)
    {
        if (entry.size() < 3) {
            entry.throwError("Invalid assignment");
            return;
        }

        auto key = entry[1].toString();
        if (key == "undefined" || key == "allowUndefined") {
            argument->setAllowUndefined(entry[2].toBoolean());
            return;
        } else if (key == "unit" || key == "defaultUnit") {
            argument->setDefaultUnit(Lua::Libs::Time::extractUnit(entry, entry[2]));
            return;
        }

        entry.throwError("Invalid assignment");
    }

    void method_get(Lua::Engine::Entry &entry)
    {
        entry.push(argument->value(entry));
        entry.replaceWithBack(0);
        entry.propagate(1);
    }

public:
    explicit LuaArgumentTimePoint(std::shared_ptr<Arguments::TimePoint> argument) : argument(
            std::move(argument))
    { }

    virtual ~LuaArgumentTimePoint() = default;

    static void install(Lua::Engine::Frame &root)
    {
        Lua::Engine::Frame frame(root);

        auto mt = pushMetatable(frame);

        {
            Lua::Engine::Assign assign(frame, mt, "__call");
            pushMethod<LuaArgumentTimePoint>(assign, &LuaArgumentTimePoint::method_get);
        }
        {
            Lua::Engine::Assign assign(frame, mt, "__newindex");
            pushMethod<LuaArgumentTimePoint>(assign, &LuaArgumentTimePoint::meta_newindex);
        }

        {
            Lua::Engine::Assign assign(frame, mt, "__index");
            auto methods = assign.pushTable();

            {
                Lua::Engine::Assign ma(assign, methods, "get");
                pushMethod<LuaArgumentTimePoint>(ma, &LuaArgumentTimePoint::method_get);
            }

            pushMethod<LuaArgumentTimePoint>(assign, &LuaArgumentTimePoint::meta_index, 1);
        }

        frame.registry().set(metatableRegistryName, mt);
    }

protected:

    void initialize(const Lua::Engine::Reference &self, Lua::Engine::Frame &frame) override
    {
        frame.push(frame.registry(), metatableRegistryName);
        setMetatable(self, frame.back());
    }
};

const std::string
        LuaArgumentTimePoint::metatableRegistryName("CPD3_luaexecargumentstimepoint_metatable");


class LuaArgumentBounds final : public Lua::Engine::Data {
    std::shared_ptr<Arguments::Bounds> argument;

    static const std::string metatableRegistryName;

    void meta_index(Lua::Engine::Entry &entry, const std::vector<Lua::Engine::Reference> &upvalues)
    {
        if (entry.size() < 2) {
            entry.throwError("Invalid index");
            return;
        }
        if (methodIndexHandler(entry, Lua::Engine::Table(upvalues.front())))
            return;

        auto key = entry[1].toString();
        if (Util::equal_insensitive(key, "start", "front")) {
            auto bounds = argument->value(entry);
            entry.clear();
            entry.push(bounds.getStart());
            entry.propagate(1);
            return;
        } else if (Util::equal_insensitive(key, "end", "back")) {
            auto bounds = argument->value(entry);
            entry.clear();
            entry.push(bounds.getEnd());
            entry.propagate(1);
            return;
        } else if (key == "value") {
            auto bounds = argument->value(entry);
            entry.clear();
            auto target = entry.pushTable();
            {
                Lua::Engine::Assign assign(entry, target, "START");
                assign.push(bounds.getStart());
            }
            {
                Lua::Engine::Assign assign(entry, target, "END");
                assign.push(bounds.getEnd());
            }
        }
    }

    void meta_newindex(Lua::Engine::Entry &entry)
    {
        if (entry.size() < 3) {
            entry.throwError("Invalid assignment");
            return;
        }

        auto key = entry[1].toString();
        if (key == "undefined" || key == "allowUndefined") {
            argument->setAllowUndefined(entry[2].toBoolean());
            return;
        } else if (key == "unit" || key == "defaultUnit") {
            argument->setDefaultUnit(Lua::Libs::Time::extractUnit(entry, entry[2]));
            return;
        }

        entry.throwError("Invalid assignment");
    }

    void method_get(Lua::Engine::Entry &entry)
    {
        auto bounds = argument->value(entry);
        entry.clear();
        entry.push(bounds.getStart());
        entry.push(bounds.getEnd());
        entry.propagate(2);
    }

    void method_list(Lua::Engine::Entry &entry)
    {
        auto bounds = argument->value(entry);
        entry.clear();
        auto target = entry.pushTable();
        {
            Lua::Engine::Assign assign(entry, target, "START");
            assign.push(bounds.getStart());
        }
        {
            Lua::Engine::Assign assign(entry, target, "END");
            assign.push(bounds.getEnd());
        }
    }

public:
    explicit LuaArgumentBounds(std::shared_ptr<Arguments::Bounds> argument) : argument(
            std::move(argument))
    { }

    virtual ~LuaArgumentBounds() = default;

    static void install(Lua::Engine::Frame &root)
    {
        Lua::Engine::Frame frame(root);

        auto mt = pushMetatable(frame);

        {
            Lua::Engine::Assign assign(frame, mt, "__call");
            pushMethod<LuaArgumentBounds>(assign, &LuaArgumentBounds::method_get);
        }
        {
            Lua::Engine::Assign assign(frame, mt, "__newindex");
            pushMethod<LuaArgumentBounds>(assign, &LuaArgumentBounds::meta_newindex);
        }

        {
            Lua::Engine::Assign assign(frame, mt, "__index");
            auto methods = assign.pushTable();

            {
                Lua::Engine::Assign ma(assign, methods, "list");
                pushMethod<LuaArgumentBounds>(ma, &LuaArgumentBounds::method_list);
            }
            {
                Lua::Engine::Assign ma(assign, methods, "get");
                pushMethod<LuaArgumentBounds>(ma, &LuaArgumentBounds::method_get);
            }

            pushMethod<LuaArgumentBounds>(assign, &LuaArgumentBounds::meta_index, 1);
        }

        frame.registry().set(metatableRegistryName, mt);
    }

protected:
    void initialize(const Lua::Engine::Reference &self, Lua::Engine::Frame &frame) override
    {
        frame.push(frame.registry(), metatableRegistryName);
        setMetatable(self, frame.back());
    }
};

const std::string
        LuaArgumentBounds::metatableRegistryName("CPD3_luaexecargumentsbounds_metatable");


class LuaArgumentStreamFile final : public Lua::Engine::Data {
    std::shared_ptr<Arguments::StreamFile> argument;

    static const std::string metatableRegistryName;

    void meta_index(Lua::Engine::Entry &entry, const std::vector<Lua::Engine::Reference> &upvalues)
    {
        if (entry.size() < 2) {
            entry.throwError("Invalid index");
            return;
        }
        if (methodIndexHandler(entry, Lua::Engine::Table(upvalues.front())))
            return;
    }

    void method_get(Lua::Engine::Entry &entry)
    {
        bool textMode = true;
        if (entry.size() > 1) {
            textMode = entry[1].toBoolean();
        }
        entry.resize(1);
        argument->push(entry, textMode);
        entry.erase(0);
        entry.propagate();
    }

public:
    explicit LuaArgumentStreamFile(std::shared_ptr<Arguments::StreamFile> argument) : argument(
            std::move(argument))
    { }

    virtual ~LuaArgumentStreamFile() = default;

    static void install(Lua::Engine::Frame &root)
    {
        Lua::Engine::Frame frame(root);

        auto mt = pushMetatable(frame);

        {
            Lua::Engine::Assign assign(frame, mt, "__call");
            pushMethod<LuaArgumentStreamFile>(assign, &LuaArgumentStreamFile::method_get);
        }

        {
            Lua::Engine::Assign assign(frame, mt, "__index");
            auto methods = assign.pushTable();

            {
                Lua::Engine::Assign ma(assign, methods, "get");
                pushMethod<LuaArgumentStreamFile>(ma, &LuaArgumentStreamFile::method_get);
            }

            pushMethod<LuaArgumentStreamFile>(assign, &LuaArgumentStreamFile::meta_index, 1);
        }

        frame.registry().set(metatableRegistryName, mt);
    }

protected:
    void initialize(const Lua::Engine::Reference &self, Lua::Engine::Frame &frame) override
    {
        frame.push(frame.registry(), metatableRegistryName);
        setMetatable(self, frame.back());
    }
};

const std::string
        LuaArgumentStreamFile::metatableRegistryName("CPD3_luaexecargumentsstreamfile_metatable");


}

void Arguments::install(Lua::Engine::Frame &root)
{
    LuaOptionBase::install(root);
    LuaArgumentStations::install(root);
    LuaArgumentTimePoint::install(root);
    LuaArgumentBounds::install(root);
    LuaArgumentStreamFile::install(root);

    Lua::Engine::Frame frame(root);

    frame.push(frame.global(), "CPD3");
    auto outer = frame.back();
    Q_ASSERT(outer.getType() == Lua::Engine::Reference::Type::Table);
    auto ns = frame.pushTable();
    {
        Lua::Engine::Assign assign(frame, outer, "Arguments");
        assign.push(ns);
    }
    {
        Lua::Engine::Assign assign(frame, ns, "evaluate");
        assign.push(std::function<void(Lua::Engine::Entry &)>([this](Lua::Engine::Entry &entry) {
            evaluate(entry);
        }));
    }


    {
        Lua::Engine::Assign assign(frame, ns, "Boolean");
        assign.push(std::function<void(Lua::Engine::Entry &)>([this](Lua::Engine::Entry &entry) {
            class Wrapper : public LuaOptionBase {
            public:
                explicit Wrapper(std::shared_ptr<Arguments::Option> option) : LuaOptionBase(
                        std::move(option))
                { }

                virtual ~Wrapper() = default;

            protected:
                void push(Lua::Engine::Frame &frame, ComponentOptionBase *option) override
                {
                    auto opt = qobject_cast<ComponentOptionBoolean *>(option);
                    if (!opt || !opt->isSet()) {
                        frame.pushNil();
                        return;
                    }
                    frame.push(opt->get());
                }
            };
            auto opt = option(Wrapper::constructStandard<ComponentOptionBoolean>(entry));
            if (!opt) {
                entry.throwError("Arguments already evaluated");
                return;
            }
            entry.clear();
            entry.pushData<Wrapper>(std::move(opt));
            entry.propagate(1);
        }));
    }
    {
        Lua::Engine::Assign assign(frame, ns, "String");
        assign.push(std::function<void(Lua::Engine::Entry &)>([this](Lua::Engine::Entry &entry) {
            class Wrapper : public LuaOptionBase {
            public:
                explicit Wrapper(std::shared_ptr<Arguments::Option> option) : LuaOptionBase(
                        std::move(option))
                { }

                virtual ~Wrapper() = default;

            protected:
                void push(Lua::Engine::Frame &frame, ComponentOptionBase *option) override
                {
                    auto opt = qobject_cast<ComponentOptionSingleString *>(option);
                    if (!opt || !opt->isSet()) {
                        frame.pushNil();
                        return;
                    }
                    frame.push(opt->get());
                }
            };
            auto opt = option(Wrapper::constructStandard<ComponentOptionSingleString>(entry));
            if (!opt) {
                entry.throwError("Arguments already evaluated");
                return;
            }
            entry.clear();
            entry.pushData<Wrapper>(std::move(opt));
            entry.propagate(1);
        }));
    }
    {
        Lua::Engine::Assign assign(frame, ns, "Integer");
        assign.push(std::function<void(Lua::Engine::Entry &)>([this](Lua::Engine::Entry &entry) {
            class Wrapper : public LuaOptionBase {
            public:
                explicit Wrapper(std::shared_ptr<Arguments::Option> option) : LuaOptionBase(
                        std::move(option))
                { }

                virtual ~Wrapper() = default;

            protected:
                void push(Lua::Engine::Frame &frame, ComponentOptionBase *option) override
                {
                    auto opt = qobject_cast<ComponentOptionSingleInteger *>(option);
                    if (!opt || !opt->isSet()) {
                        frame.pushNil();
                        return;
                    }
                    frame.push(opt->get());
                }

                void meta_newindex(Lua::Engine::Entry &entry) override
                {
                    if (entry.size() < 3) {
                        entry.throwError("Invalid assignment");
                        return;
                    }

                    auto key = entry[1].toString();
                    if (key == "min" || key == "minimum") {
                        auto opt = modify<ComponentOptionSingleInteger>();
                        if (!opt)
                            return;
                        opt->setMinimum(entry[2].toInteger());
                        return;
                    } else if (key == "max" || key == "maximum") {
                        auto opt = modify<ComponentOptionSingleInteger>();
                        if (!opt)
                            return;
                        opt->setMaximum(entry[2].toInteger());
                        return;
                    } else if (key == "undefined" || key == "allowUndefined") {
                        auto opt = modify<ComponentOptionSingleInteger>();
                        if (!opt)
                            return;
                        opt->setAllowUndefined(entry[2].toBoolean());
                        return;
                    }

                    return LuaOptionBase::meta_newindex(entry);
                }
            };
            auto opt = option(Wrapper::constructStandard<ComponentOptionSingleInteger>(entry));
            if (!opt) {
                entry.throwError("Arguments already evaluated");
                return;
            }
            entry.clear();
            entry.pushData<Wrapper>(std::move(opt));
            entry.propagate(1);
        }));
    }
    {
        Lua::Engine::Assign assign(frame, ns, "Real");
        assign.push(std::function<void(Lua::Engine::Entry &)>([this](Lua::Engine::Entry &entry) {
            class Wrapper : public LuaOptionBase {
            public:
                explicit Wrapper(std::shared_ptr<Arguments::Option> option) : LuaOptionBase(
                        std::move(option))
                { }

                virtual ~Wrapper() = default;

            protected:
                void push(Lua::Engine::Frame &frame, ComponentOptionBase *option) override
                {
                    auto opt = qobject_cast<ComponentOptionSingleDouble *>(option);
                    if (!opt || !opt->isSet()) {
                        frame.pushNil();
                        return;
                    }
                    frame.push(opt->get());
                }

                void meta_newindex(Lua::Engine::Entry &entry) override
                {
                    if (entry.size() < 3) {
                        entry.throwError("Invalid assignment");
                        return;
                    }

                    auto key = entry[1].toString();
                    if (key == "min" || key == "minimum") {
                        auto opt = modify<ComponentOptionSingleDouble>();
                        if (!opt)
                            return;
                        opt->setMinimum(entry[2].toReal(), opt->getMinimumInclusive());
                        return;
                    } else if (key == "max" || key == "maximum") {
                        auto opt = modify<ComponentOptionSingleDouble>();
                        if (!opt)
                            return;
                        opt->setMaximum(entry[2].toReal(), opt->getMaximumInclusive());
                        return;
                    } else if (key == "undefined" || key == "allowUndefined") {
                        auto opt = modify<ComponentOptionSingleDouble>();
                        if (!opt)
                            return;
                        opt->setAllowUndefined(entry[2].toBoolean());
                        return;
                    } else if (key == "minimumInclusive") {
                        auto opt = modify<ComponentOptionSingleDouble>();
                        if (!opt)
                            return;
                        opt->setMinimum(opt->getMinimum(), entry[2].toBoolean());
                        return;
                    } else if (key == "maximumInclusive") {
                        auto opt = modify<ComponentOptionSingleDouble>();
                        if (!opt)
                            return;
                        opt->setMaximum(opt->getMaximum(), entry[2].toBoolean());
                        return;
                    }

                    return LuaOptionBase::meta_newindex(entry);
                }
            };
            auto opt = option(Wrapper::constructStandard<ComponentOptionSingleDouble>(entry));
            if (!opt) {
                entry.throwError("Arguments already evaluated");
                return;
            }
            entry.clear();
            entry.pushData<Wrapper>(std::move(opt));
            entry.propagate(1);
        }));
    }
    {
        Lua::Engine::Assign assign(frame, ns, "OptionalTime");
        assign.push(std::function<void(Lua::Engine::Entry &)>([this](Lua::Engine::Entry &entry) {
            class Wrapper : public LuaOptionBase {
            public:
                explicit Wrapper(std::shared_ptr<Arguments::Option> option) : LuaOptionBase(
                        std::move(option))
                { }

                virtual ~Wrapper() = default;

            protected:
                void push(Lua::Engine::Frame &frame, ComponentOptionBase *option) override
                {
                    auto opt = qobject_cast<ComponentOptionSingleTime *>(option);
                    if (!opt || !opt->isSet()) {
                        frame.pushNil();
                        return;
                    }
                    frame.push(opt->get());
                }

                void meta_newindex(Lua::Engine::Entry &entry) override
                {
                    if (entry.size() < 3) {
                        entry.throwError("Invalid assignment");
                        return;
                    }

                    auto key = entry[1].toString();
                    if (key == "undefined" || key == "allowUndefined") {
                        auto opt = modify<ComponentOptionSingleTime>();
                        if (!opt)
                            return;
                        opt->setAllowUndefined(entry[2].toBoolean());
                        return;
                    }

                    return LuaOptionBase::meta_newindex(entry);
                }
            };
            auto opt = option(Wrapper::constructStandard<ComponentOptionSingleTime>(entry));
            if (!opt) {
                entry.throwError("Arguments already evaluated");
                return;
            }
            entry.clear();
            entry.pushData<Wrapper>(std::move(opt));
            entry.propagate(1);
        }));
    }
    {
        Lua::Engine::Assign assign(frame, ns, "File");
        assign.push(std::function<void(Lua::Engine::Entry &)>([this](Lua::Engine::Entry &entry) {
            class Wrapper : public LuaOptionBase {
            public:
                explicit Wrapper(std::shared_ptr<Arguments::Option> option) : LuaOptionBase(
                        std::move(option))
                { }

                virtual ~Wrapper() = default;

            protected:
                void push(Lua::Engine::Frame &frame, ComponentOptionBase *option) override
                {
                    auto opt = qobject_cast<ComponentOptionFile *>(option);
                    if (!opt || !opt->isSet()) {
                        frame.pushNil();
                        return;
                    }
                    frame.push(opt->get());
                }

                void method_get(Lua::Engine::Entry &entry) override
                {
                    auto opt = value<ComponentOptionFile>(entry);
                    if (!opt || !opt->isSet()) {
                        entry.clear();
                        entry.pushNil();
                        entry.propagate(1);
                        return;
                    }

                    auto filename = opt->get();
#ifndef NO_CONSOLE
                    if (filename.isEmpty() || filename == "-") {
                        entry.push(entry.global(), "io");
                        if (opt->getMode() != ComponentOptionFile::Read)
                            entry.push(entry.back(), "stdout");
                        else
                            entry.push(entry.back(), "stdin");
                        entry.replaceWithBack(0);
                        entry.propagate(1);
                        return;
                    }
#endif

                    std::string mode;
                    if (entry.size() > 1 &&
                            entry[1].getType() == Lua::Engine::Reference::Type::String) {
                        mode = entry[1].toString();
                    } else {
                        switch (opt->getMode()) {
                        case ComponentOptionFile::Read:
                            if (entry.size() < 2 || entry[1].toBoolean())
                                mode = "r";
                            else
                                mode = "rb";
                            break;
                        case ComponentOptionFile::Write:
                            if (entry.size() < 2 || entry[1].toBoolean())
                                mode = "w";
                            else
                                mode = "wb";
                            break;
                        case ComponentOptionFile::ReadWrite:
                            if (entry.size() < 2 || entry[1].toBoolean())
                                mode = "r+";
                            else
                                mode = "r+b";
                            break;
                        }
                    }

                    entry.clear();
                    {
                        Lua::Engine::Call call(entry);
                        call.push(call.global(), "io");
                        call.push(call.back(), "open");
                        call.replaceWithBack(0);
                        call.resize(1);
                        call.push(filename);
                        call.push(mode);
                        if (!call.executeVariable())
                            return;
                        call.propagate();
                    }
                    entry.propagate();
                }

                void meta_newindex(Lua::Engine::Entry &entry) override
                {
                    if (entry.size() < 3) {
                        entry.throwError("Invalid assignment");
                        return;
                    }

                    auto key = entry[1].toString();
                    if (key == "mode") {
                        auto opt = modify<ComponentOptionFile>();
                        if (!opt)
                            return;
                        auto value = entry[2].toString();
                        if (Util::equal_insensitive(value, "r", "read")) {
                            opt->setMode(ComponentOptionFile::Read);
                        } else if (Util::equal_insensitive(value, "w", "write")) {
                            opt->setMode(ComponentOptionFile::Write);
                        } else {
                            opt->setMode(ComponentOptionFile::ReadWrite);
                        }
                        return;
                    } else if (key == "extension" || key == "extensions") {
                        auto opt = modify<ComponentOptionFile>();
                        if (!opt)
                            return;
                        auto value = entry[2];
                        if (value.getType() == Lua::Engine::Reference::Type::Table) {
                            Lua::Engine::Iterator it(entry, value);
                            QSet<QString> ext;
                            while (it.next()) {
                                ext.insert(it.value().toQString());
                            }
                            opt->setExtensions(ext);
                        } else if (!value.isNil()) {
                            opt->setExtensions({value.toQString()});
                        } else {
                            opt->setExtensions({});
                        }
                        return;
                    }

                    return LuaOptionBase::meta_newindex(entry);
                }
            };
            auto opt = option(Wrapper::constructStandard<ComponentOptionFile>(entry));
            if (!opt) {
                entry.throwError("Arguments already evaluated");
                return;
            }
            entry.clear();
            entry.pushData<Wrapper>(std::move(opt));
            entry.propagate(1);
        }));
    }
    {
        Lua::Engine::Assign assign(frame, ns, "Station");
        assign.push(std::function<void(Lua::Engine::Entry &)>([this](Lua::Engine::Entry &entry) {
            QString name;
            if (!entry.empty())
                name = entry[0].toQString();
            if (name.isEmpty()) {
                static std::atomic_uint_fast32_t counter;
                auto n = counter.fetch_add(1) + 1;
                name = QObject::tr("station-%1", "default option name").arg(n);
            }
            QString display;
            if (entry.size() > 1)
                display = entry[1].toQString();
            QString description;
            if (entry.size() > 2)
                description = entry[2].toQString();
            QString defaultBehavior;
            if (entry.size() > 3)
                defaultBehavior = entry[3].toQString();

            auto arg = stations(name, display, description, defaultBehavior);
            if (!arg) {
                entry.throwError("Arguments already evaluated");
                return;
            }
            arg->setMinimumRequired(1);
            arg->setMaximumAllowed(1);
            entry.clear();
            entry.pushData<LuaArgumentStations>(std::move(arg));
            entry.propagate(1);
        }));
    }
    {
        Lua::Engine::Assign assign(frame, ns, "Stations");
        assign.push(std::function<void(Lua::Engine::Entry &)>([this](Lua::Engine::Entry &entry) {
            QString name;
            if (!entry.empty())
                name = entry[0].toQString();
            if (name.isEmpty()) {
                static std::atomic_uint_fast32_t counter;
                auto n = counter.fetch_add(1) + 1;
                name = QObject::tr("stations-%1", "default option name").arg(n);
            }
            QString display;
            if (entry.size() > 1)
                display = entry[1].toQString();
            QString description;
            if (entry.size() > 2)
                description = entry[2].toQString();
            QString defaultBehavior;
            if (entry.size() > 3)
                defaultBehavior = entry[3].toQString();

            auto arg = stations(name, display, description, defaultBehavior);
            if (!arg) {
                entry.throwError("Arguments already evaluated");
                return;
            }
            entry.clear();
            entry.pushData<LuaArgumentStations>(std::move(arg));
            entry.propagate(1);
        }));
    }
    {
        Lua::Engine::Assign assign(frame, ns, "Time");
        assign.push(std::function<void(Lua::Engine::Entry &)>([this](Lua::Engine::Entry &entry) {
            QString name;
            if (!entry.empty())
                name = entry[0].toQString();
            if (name.isEmpty()) {
                static std::atomic_uint_fast32_t counter;
                auto n = counter.fetch_add(1) + 1;
                name = QObject::tr("time-%1", "default option name").arg(n);
            }
            QString display;
            if (entry.size() > 1)
                display = entry[1].toQString();
            QString description;
            if (entry.size() > 2)
                description = entry[2].toQString();
            QString defaultBehavior;
            if (entry.size() > 3)
                defaultBehavior = entry[3].toQString();

            auto arg = time(name, display, description, defaultBehavior);
            if (!arg) {
                entry.throwError("Arguments already evaluated");
                return;
            }
            entry.clear();
            entry.pushData<LuaArgumentTimePoint>(std::move(arg));
            entry.propagate(1);
        }));
    }
    {
        Lua::Engine::Frame local(frame);
        local.push(std::function<void(Lua::Engine::Entry &)>([this](Lua::Engine::Entry &entry) {
            QString name;
            if (!entry.empty())
                name = entry[0].toQString();
            if (name.isEmpty()) {
                static std::atomic_uint_fast32_t counter;
                auto n = counter.fetch_add(1) + 1;
                name = QObject::tr("bounds-%1", "default option name").arg(n);
            }
            QString display;
            if (entry.size() > 1)
                display = entry[1].toQString();
            QString description;
            if (entry.size() > 2)
                description = entry[2].toQString();
            QString defaultBehavior;
            if (entry.size() > 3)
                defaultBehavior = entry[3].toQString();

            auto arg = bounds(name, display, description, defaultBehavior);
            if (!arg) {
                entry.throwError("Arguments already evaluated");
                return;
            }
            entry.clear();
            entry.pushData<LuaArgumentBounds>(std::move(arg));
            entry.propagate(1);
        }));
        auto ctor = local.back();
        {
            Lua::Engine::Assign assign(local, ns, "Bounds");
            assign.push(ctor);
        }
        {
            Lua::Engine::Assign assign(local, ns, "Range");
            assign.push(ctor);
        }
    }
    {
        Lua::Engine::Assign assign(frame, ns, "ReadStream");
        assign.push(std::function<void(Lua::Engine::Entry &)>([this](Lua::Engine::Entry &entry) {
            QString name;
            if (!entry.empty())
                name = entry[0].toQString();
            if (name.isEmpty()) {
                static std::atomic_uint_fast32_t counter;
                auto n = counter.fetch_add(1) + 1;
                name = QObject::tr("read-%1", "default option name").arg(n);
            }
            QString display;
            if (entry.size() > 1)
                display = entry[1].toQString();
            QString description;
            if (entry.size() > 2)
                description = entry[2].toQString();

            auto arg = readStream(name, display, description);
            if (!arg) {
                entry.throwError("Arguments already evaluated");
                return;
            }
            entry.clear();
            entry.pushData<LuaArgumentStreamFile>(std::move(arg));
            entry.propagate(1);
        }));
    }
    {
        Lua::Engine::Assign assign(frame, ns, "WriteStream");
        assign.push(std::function<void(Lua::Engine::Entry &)>([this](Lua::Engine::Entry &entry) {
            QString name;
            if (!entry.empty())
                name = entry[0].toQString();
            if (name.isEmpty()) {
                static std::atomic_uint_fast32_t counter;
                auto n = counter.fetch_add(1) + 1;
                name = QObject::tr("write-%1", "default option name").arg(n);
            }
            QString display;
            if (entry.size() > 1)
                display = entry[1].toQString();
            QString description;
            if (entry.size() > 2)
                description = entry[2].toQString();

            auto arg = writeStream(name, display, description);
            if (!arg) {
                entry.throwError("Arguments already evaluated");
                return;
            }
            entry.clear();
            entry.pushData<LuaArgumentStreamFile>(std::move(arg));
            entry.propagate(1);
        }));
    }
}