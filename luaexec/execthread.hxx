/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3LUAEXEC_EXECTHREAD_HXX
#define CPD3LUAEXEC_EXECTHREAD_HXX

#include "core/first.hxx"

#include <thread>
#include <mutex>
#include <functional>
#include <condition_variable>
#include <list>
#include <memory>
#include <QObject>

#include "core/threading.hxx"
#include "luascript/engine.hxx"
#include "arguments.hxx"
#include "datacore/archive/access.hxx"

#ifdef BUILD_GUI

#include "graphing/display.hxx"

class QSettings;

#endif

class ExecThread : public QObject {
Q_OBJECT

public:

    class Processor : public std::enable_shared_from_this<Processor> {
        ExecThread *thread;
    public:
        std::mutex &mutex;
        std::condition_variable &notify;

        Processor(ExecThread *thread);

        virtual ~Processor();

        void connect();

        enum class ExecutionState {
            Waiting, Complete, DidUnlock,
        };

        virtual ExecutionState execute(CPD3::Lua::Engine::Frame &frame,
                                       std::unique_lock<std::mutex> &lock) = 0;
    };

    friend class Processor;

    Arguments arguments;
    CPD3::Data::Archive::Access archive;
#ifdef BUILD_GUI
    QSettings *settings;
    CPD3::Graphing::DisplayDynamicContext displayDynamicContext;
#endif

private:
    bool usingGUI;
    std::string target;

    bool inSilentAbort;
    bool processorsTerminated;
    std::mutex processorMutex;
    std::condition_variable processorNotify;
    std::list<std::weak_ptr<Processor>> processorHandlers;

    std::thread thread;
    std::string displayName;

    void run();

    void setup(CPD3::Lua::Engine::Frame &frame);

public:
    ExecThread(const QStringList &arguments, bool usingGUI, std::string target);

    virtual ~ExecThread();

    void start();

    inline void silentAbort()
    { inSilentAbort = true; }

    inline const std::string &getDisplay() const
    { return displayName; }

    CPD3::Threading::Signal<CPD3::Lua::Engine::Frame &> startPipelines;
    CPD3::Threading::Signal<CPD3::Lua::Engine::Frame &> finalizeAcquisition;

    bool executeProcessors(CPD3::Lua::Engine::Frame &frame);

    CPD3::Threading::Signal<CPD3::Lua::Engine::Frame &> finalizeBuffers;
    CPD3::Threading::Signal<CPD3::Lua::Engine::Frame &> completePipelines;

    CPD3::Threading::Signal<> terminateRequested;

public slots:

    void signalTerminate();
};

#endif //CPD3LUAEXEC_EXECTHREAD_HXX
