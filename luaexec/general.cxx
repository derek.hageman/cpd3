/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <atomic>
#include <QLoggingCategory>

#include "luascript/libs/streamvalue.hxx"
#include "luascript/libs/sequencesegment.hxx"
#include "luascript/libs/sequencename.hxx"
#include "core/threadpool.hxx"

#include "general.hxx"
#include "execthread.hxx"

Q_DECLARE_LOGGING_CATEGORY(log_luaexec)

Q_DECLARE_LOGGING_CATEGORY(log_luaexec_pipeline)

using namespace CPD3;
using namespace CPD3::Data;

static const std::string listRegistryName = "CPD3_luaexecgeneral_list";
static std::atomic_uint_fast32_t listInsertIndex(1);

void GeneralBuffered::install(ExecThread *, Lua::Engine::Frame &root)
{
    Lua::Engine::Frame frame(root);

    {
        Lua::Engine::Assign assign(frame, frame.registry(), listRegistryName);
        assign.pushTable();
    }
}

GeneralBuffered::GeneralBuffered(ExecThread *thread,
                                 Lua::Engine::Frame &target,
                                 Lua::StreamBuffer *buffer) : buffer(buffer),
                                                              state(State::Initialize),
                                                              controllerIndex(0),
                                                              egress(nullptr)
{
    buffer->pushLuaController(target);
    auto controller = target.back();
    {
        Lua::Engine::Frame frame(target);
        frame.push(frame.registry(), listRegistryName);
        controllerIndex = listInsertIndex.fetch_add(1);
        Lua::Engine::Assign assign(frame, frame.back(), controllerIndex);
        assign.push(controller);
    }

    qCDebug(log_luaexec_pipeline) << "General interface" << controllerIndex << "created";

    thread->finalizeBuffers
          .connect(*this, std::bind(&GeneralBuffered::finalize, this, std::placeholders::_1));
    thread->terminateRequested.connect(*this, std::bind(&GeneralBuffered::signalTerminate, this));
}

GeneralBuffered::~GeneralBuffered()
{
    Threading::Receiver::disconnect();
    {
        std::lock_guard<std::mutex> lock(mutex);
        state = State::Terminate;
    }
    notify.notify_all();
}

void GeneralBuffered::processingCompleted()
{ finished(); }

void GeneralBuffered::finalize(Lua::Engine::Frame &frame)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        notify.wait(lock, [this] {
            switch (state) {
            case State::EndSent:
            case State::Terminate:
            case State::Finished:
                return true;
            case State::Initialize:
            case State::Active:
            case State::EndReceived:
                return false;
            }
            Q_ASSERT(false);
            return true;
        });
        if (state == State::Finished) {
            qCDebug(log_luaexec_pipeline) << "General interface" << controllerIndex
                                          << "already finished";
            return;
        }
    }

    qCDebug(log_luaexec_pipeline) << "General interface" << controllerIndex
                                  << "starting finalization";

    Lua::Engine::Frame local(frame);
    local.push(local.registry(), listRegistryName);
    Lua::Engine::Table reg(local.back());
    local.push(reg, controllerIndex);
    reg.erase(controllerIndex);
    Q_ASSERT(!local.back().isNil());
    buffer->finish(local, local.back());

    qCDebug(log_luaexec_pipeline) << "General interface" << controllerIndex << "finalized";

    /* State set to ended via the call to outputEnded() */
#ifndef NDEBUG
    std::lock_guard<std::mutex> lock(mutex);
    Q_ASSERT(state == State::Finished);
#endif
}

void GeneralBuffered::outputEnded()
{
    qCDebug(log_luaexec_pipeline) << "General interface" << controllerIndex << "processed end";

    std::lock_guard<std::mutex> lock(mutex);
    state = State::Finished;
    notify.notify_all();
    processingCompleted();
}

void GeneralBuffered::start()
{
    qCDebug(log_luaexec_pipeline) << "General interface" << controllerIndex << "starting";

    {
        std::lock_guard<std::mutex> lock(mutex);
        if (state != State::Initialize)
            return;
        state = State::Active;
    }
    notify.notify_all();
}

bool GeneralBuffered::isFinished()
{
    std::lock_guard<std::mutex> lock(mutex);
    switch (state) {
    case State::Initialize:
    case State::Active:
    case State::EndReceived:
    case State::EndSent:
        return false;
    case State::Finished:
    case State::Terminate:
        return true;
    }
    Q_ASSERT(false);
    return true;
}

bool GeneralBuffered::wait(double timeout)
{
    return Threading::waitForTimeout(timeout, mutex, notify, [this] {
        switch (state) {
        case State::Finished:
        case State::Terminate:
            return true;
        case State::Active:
        case State::EndReceived:
        case State::EndSent:
        case State::Initialize:
            return false;
        }
        Q_ASSERT(false);
        return true;
    });
}

void GeneralBuffered::signalTerminate()
{
    qCDebug(log_luaexec_pipeline) << "Terminating handler";

    std::unique_lock<std::mutex> lock(mutex);
    if (state == State::Terminate)
        return;
    state = State::Terminate;
    notify.notify_all();
    std::lock_guard<std::mutex> egressLocker(egressLock);
    if (!egress) {
        qCDebug(log_luaexec_pipeline) << "Termination discarding end";
        return;
    }
    lock.unlock();
    egress->endData();
    egress = nullptr;
}


bool GeneralBuffered::shouldProcessExternal() const
{
    switch (state) {
    case State::Initialize:
        Q_ASSERT(false);
        break;
    case State::EndReceived:
    case State::EndSent:
        /* Duplicate endData() event */
        return false;
    case State::Active:
        return true;
    case State::Finished:
        /* Script code may have already ended it, so just ignore this */
        return false;
    case State::Terminate:
        return false;
    }
    Q_ASSERT(false);
    return false;
}

void GeneralBuffered::incomingData(const SequenceValue::Transfer &values)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        if (!shouldProcessExternal())
            return;
        if (!processIncoming(values))
            return;
    }
    notify.notify_all();
}

void GeneralBuffered::incomingData(SequenceValue::Transfer &&values)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        if (!shouldProcessExternal())
            return;
        if (!processIncoming(std::move(values)))
            return;
    }
    notify.notify_all();
}

void GeneralBuffered::incomingData(const SequenceValue &value)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        if (!shouldProcessExternal())
            return;
        if (!processIncoming(value))
            return;
    }
    notify.notify_all();
}

void GeneralBuffered::incomingData(SequenceValue &&value)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        if (!shouldProcessExternal())
            return;
        if (!processIncoming(std::move(value)))
            return;
    }
    notify.notify_all();
}

void GeneralBuffered::endData()
{
    qCDebug(log_luaexec_pipeline) << "General interface" << controllerIndex << "received end";

    {
        std::unique_lock<std::mutex> lock(mutex);
        if (!shouldProcessExternal())
            return;
        processEnd();
        state = State::EndReceived;
    }
    notify.notify_all();
}

void GeneralBuffered::setEgress(StreamSink *egress)
{
    std::lock_guard<std::mutex> lock(mutex);
    std::lock_guard<std::mutex> egressLocker(egressLock);
    this->egress = egress;
    notify.notify_all();
}

void GeneralBuffered::stall(std::unique_lock<std::mutex> &lock)
{
    while (stall()) {
        switch (state) {
        case State::Initialize:
        case State::Active:
            break;
        case State::EndReceived:
        case State::EndSent:
        case State::Finished:
        case State::Terminate:
            return;
        }
        notify.wait(lock);
    }
}

void GeneralBuffered::processEnd()
{ }

std::pair<StreamSink *, std::unique_lock<std::mutex>> GeneralBuffered::acquireOutput(bool isEnding)
{
    std::unique_lock<std::mutex> egressLocker(egressLock);
    if (egress) {
        auto e = egress;
        if (isEnding)
            egress = nullptr;
        return {e, std::move(egressLocker)};
    }
    egressLocker.unlock();
    std::unique_lock<std::mutex> lock(mutex);
    egressLocker.lock();
    for (;;) {
        if (egress)
            break;
        if (state == State::Terminate)
            return {nullptr, std::unique_lock<std::mutex>()};
        egressLocker.unlock();
        notify.wait(lock, [this] { return egress || state == State::Terminate; });
        egressLocker.lock();
    }
    auto e = egress;
    if (isEnding)
        egress = nullptr;
    return {e, std::move(egressLocker)};
}

std::unique_lock<std::mutex> GeneralBuffered::acquireState()
{ return std::unique_lock<std::mutex>(mutex); }

bool GeneralBuffered::pushNext(Lua::Engine::Frame &target)
{
    std::unique_lock<std::mutex> lock(mutex);
    for (;;) {
        switch (state) {
        case State::Initialize:
            Q_ASSERT(false);
            return false;
        case State::Active: {
            bool wasStalled = stall();
            if (fetchNext(target)) {
                if (wasStalled)
                    notify.notify_all();
                return true;
            }
            notify.wait(lock);
            break;
        }
        case State::EndReceived:
            if (fetchNext(target))
                return true;
            state = State::EndSent;
            notify.notify_all();
            return false;
        case State::EndSent:
        case State::Finished:
        case State::Terminate:
            return false;
        }
    }
}


GeneralValues::Buffer::Buffer(GeneralValues &parent) : parent(parent)
{ }

GeneralValues::Buffer::~Buffer() = default;

bool GeneralValues::Buffer::pushNext(Lua::Engine::Frame &target)
{ return parent.pushNext(target); }

void GeneralValues::Buffer::outputReady(Lua::Engine::Frame &frame,
                                        const Lua::Engine::Reference &ref)
{
    auto value = extract(frame, ref);
    auto lock = parent.acquireOutput();
    if (!lock.second)
        return;
    lock.first->incomingData(std::move(value));
}

void GeneralValues::Buffer::endReady()
{
    {
        auto lock = parent.acquireOutput(true);
        if (lock.second) {
            lock.first->endData();
        } else {
            qCDebug(log_luaexec_pipeline) << "Value handler discarded end";
        }
    }
    parent.outputEnded();
}

GeneralValues::GeneralValues(ExecThread *thread, Lua::Engine::Frame &target) : GeneralBuffered(
        thread, target, new Buffer(*this))
{ }

GeneralValues::~GeneralValues() = default;

bool GeneralValues::processIncoming(const SequenceValue::Transfer &values)
{
    Util::append(values, incoming);
    return !incoming.empty();
}

bool GeneralValues::processIncoming(SequenceValue::Transfer &&values)
{
    Util::append(std::move(values), incoming);
    return !incoming.empty();
}

bool GeneralValues::processIncoming(const SequenceValue &value)
{
    incoming.emplace_back(value);
    return true;
}

bool GeneralValues::processIncoming(SequenceValue &&value)
{
    incoming.emplace_back(std::move(value));
    return true;
}

bool GeneralValues::stall() const
{ return incoming.size() > stallThreshold; }

bool GeneralValues::fetchNext(CPD3::Lua::Engine::Frame &target)
{
    if (incoming.empty())
        return false;
    target.pushData<Lua::Libs::SequenceValue>(std::move(incoming.front()));
    incoming.pop_front();
    return true;
}


namespace {
class OutputSegment : public Lua::Libs::SequenceSegment {
    CPD3::Data::SequenceName::Set outputs;
    bool requireExists;
public:
    OutputSegment() : requireExists(true)
    { }

    virtual ~OutputSegment() = default;

    explicit OutputSegment(const CPD3::Data::SequenceSegment &segment) : Lua::Libs::SequenceSegment(
            segment), requireExists(false)
    { }

    explicit OutputSegment(CPD3::Data::SequenceSegment &&segment) : Lua::Libs::SequenceSegment(
            std::move(segment)), requireExists(false)
    { }

    OutputSegment(const CPD3::Data::SequenceSegment &segment,
                  const CPD3::Data::SequenceName::Set &outputs,
                  bool requireExists = true) : Lua::Libs::SequenceSegment(segment),
                                               outputs(std::move(outputs)),
                                               requireExists(requireExists)
    { }

    OutputSegment(CPD3::Data::SequenceSegment &&segment,
                  const CPD3::Data::SequenceName::Set &outputs,
                  bool requireExists = true) : Lua::Libs::SequenceSegment(std::move(segment)),
                                               outputs(std::move(outputs)),
                                               requireExists(requireExists)
    { }

    void setOutputs(CPD3::Data::SequenceName::Set out)
    {
        outputs = std::move(out);
        requireExists = false;
    }

    CPD3::Data::SequenceValue::Transfer extract(Lua::Engine::Frame &frame,
                                                const Lua::Engine::Reference &ref)
    { return Lua::StreamBuffer::Segment::extract(frame, ref, outputs, requireExists); }
};
}

GeneralSegments::Buffer::Buffer(GeneralSegments &parent) : parent(parent)
{ }

GeneralSegments::Buffer::~Buffer() = default;

bool GeneralSegments::Buffer::pushNext(CPD3::Lua::Engine::Frame &target)
{ return parent.pushNext(target); }

void GeneralSegments::Buffer::convertFromLua(Lua::Engine::Frame &frame)
{
    if (frame.back().toData<OutputSegment>())
        return;
    auto value = Lua::Libs::SequenceSegment::extract(frame, frame.back());
    frame.pop();
    auto lock = parent.acquireState();
    frame.pushData<OutputSegment>(std::move(value), parent.outputs, parent.addInputsToOutputs);
}

void GeneralSegments::Buffer::outputReady(Lua::Engine::Frame &frame,
                                          const Lua::Engine::Reference &ref)
{
    auto value = ref.toData<OutputSegment>();
    if (!value)
        return;
    auto lock = parent.acquireOutput();
    if (!lock.second)
        return;
    lock.first->incomingData(value->extract(frame, ref));
}

void GeneralSegments::Buffer::endReady()
{
    {
        auto lock = parent.acquireOutput(true);
        if (lock.second) {
            lock.first->endData();
        } else {
            qCDebug(log_luaexec_pipeline) << "Segment handler discarded end";
        }
    }
    parent.outputEnded();
}

bool GeneralSegments::Buffer::bufferAssigned(Lua::Engine::Frame &frame,
                                             const Lua::Engine::Reference &target,
                                             const Lua::Engine::Reference &value)
{
    auto output = target.toData<OutputSegment>();
    if (!output)
        return false;
    if (value.getType() == Lua::Engine::Reference::Type::Table) {
        CPD3::Data::SequenceName::Set result;
        Lua::Engine::Iterator it(frame, value);
        bool allNumeric = true;
        while (it.next()) {
            if (it.key().getType() != Lua::Engine::Reference::Type::Number) {
                allNumeric = false;
                break;
            }
            auto name = Lua::Libs::SequenceName::extract(frame, value);
            if (!name.isValid()) {
                allNumeric = false;
                break;
            }
            result.insert(std::move(name));
        }
        if (allNumeric) {
            output->setOutputs(std::move(result));
            return true;
        }
    }
    auto name = Lua::Libs::SequenceName::extract(frame, value);
    if (!name.isValid())
        return false;
    output->setOutputs({std::move(name)});
    return true;
}

GeneralSegments::GeneralSegments(ExecThread *thread, Lua::Engine::Frame &target) : GeneralBuffered(
        thread, target, new Buffer(*this)), addInputsToOutputs(true)
{ }

GeneralSegments::~GeneralSegments() = default;

bool GeneralSegments::processIncoming(const SequenceValue::Transfer &values)
{
    for (const auto &v : values) {
        if (addInputsToOutputs)
            outputs.insert(v.getName());
        Util::append(reader.add(v), incoming);
    }
    return !incoming.empty();
}

bool GeneralSegments::processIncoming(SequenceValue::Transfer &&values)
{
    for (auto &v : values) {
        if (addInputsToOutputs)
            outputs.insert(v.getName());
        Util::append(reader.add(std::move(v)), incoming);
    }
    return !incoming.empty();
}

bool GeneralSegments::processIncoming(const SequenceValue &value)
{
    if (addInputsToOutputs)
        outputs.insert(value.getName());
    Util::append(reader.add(value), incoming);
    return !incoming.empty();
}

bool GeneralSegments::processIncoming(SequenceValue &&value)
{
    if (addInputsToOutputs)
        outputs.insert(value.getName());
    Util::append(reader.add(std::move(value)), incoming);
    return !incoming.empty();
}

void GeneralSegments::processEnd()
{
    Util::append(reader.finish(), incoming);
}

bool GeneralSegments::stall() const
{ return incoming.size() > stallThreshold; }

bool GeneralSegments::fetchNext(CPD3::Lua::Engine::Frame &target)
{
    if (incoming.empty())
        return false;
    target.pushData<OutputSegment>(std::move(incoming.front()), outputs, addInputsToOutputs);
    incoming.pop_front();
    return true;
}

void GeneralSegments::setExplicitOutputs(SequenceName::Set outputs)
{
    addInputsToOutputs = false;
    this->outputs = std::move(outputs);
}