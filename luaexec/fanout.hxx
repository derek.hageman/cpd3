/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3LUAEXEC_FANOUT_HXX
#define CPD3LUAEXEC_FANOUT_HXX

#include "core/first.hxx"

#include <mutex>
#include <condition_variable>

#include "core/threading.hxx"
#include "core/merge.hxx"
#include "luascript/streambuffer.hxx"
#include "luascript/fanoutcontroller.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/segment.hxx"
#include "execthread.hxx"

class FanoutProcessor : public CPD3::Data::ProcessingStage, public CPD3::Threading::Receiver {
protected:
    typedef CPD3::StreamMultiplexer<CPD3::Data::SequenceValue> Multiplexer;

    class BaseTarget : public CPD3::Lua::FanoutController::Target {
    public:
        BaseTarget();

        virtual ~BaseTarget();

        virtual void incomingSequenceValue(CPD3::Data::SequenceValue &value) = 0;

        virtual void incomingDataAdvance(double time);

        virtual bool process(CPD3::Lua::Engine::Frame &frame,
                             const CPD3::Lua::Engine::Reference &controller);

        virtual void finalize(CPD3::Lua::Engine::Frame &frame,
                              const CPD3::Lua::Engine::Reference &controller);
    };

    class ForegroundTarget : public BaseTarget {
    protected:
        Multiplexer::Simple *sink;
    public:
        ForegroundTarget(FanoutProcessor &parent);

        virtual ~ForegroundTarget();

        virtual void addedDispatch(CPD3::Lua::Engine::Frame &frame,
                                   const CPD3::Lua::Engine::Reference &controller,
                                   const CPD3::Data::SequenceName &name);
    };

    friend class ForegroundTarget;

    class BackgroundTarget : public BaseTarget {
    public:
        BackgroundTarget();

        virtual ~BackgroundTarget();

        void incomingSequenceValue(CPD3::Data::SequenceValue &value) override;

    protected:
        bool initializeCall(CPD3::Lua::Engine::Frame &arguments,
                            const CPD3::Lua::Engine::Reference &controller,
                            const CPD3::Lua::Engine::Reference &key,
                            const std::vector<std::shared_ptr<Target>> &background,
                            CPD3::Lua::Engine::Table &context) override;
    };

private:
    class BypassTarget : public BaseTarget {
        Multiplexer::Simple *sink;
    public:
        BypassTarget(FanoutProcessor &parent);

        virtual ~BypassTarget();

        void incomingSequenceValue(CPD3::Data::SequenceValue &value) override;
    };

    friend class BypassTarget;


    class Processor : public ExecThread::Processor {
        FanoutProcessor &parent;
    public:
        Processor(ExecThread *thread, FanoutProcessor &parent);

        virtual ~Processor();

        ExecutionState execute(CPD3::Lua::Engine::Frame &frame,
                               std::unique_lock<std::mutex> &lock) override;
    };

    friend class Processor;

    std::shared_ptr<ExecThread::Processor> processor;

    class Fanout : public CPD3::Lua::FanoutController {
        FanoutProcessor &parent;
    public:
        Fanout(FanoutProcessor &parent);

        virtual ~Fanout();

    protected:
        std::shared_ptr<Target> createTarget(const CPD3::Lua::Engine::Output &type) override;

        void addedDispatch(CPD3::Lua::Engine::Frame &frame,
                           const CPD3::Lua::Engine::Reference &controller,
                           const CPD3::Data::SequenceName &name,
                           std::vector<std::shared_ptr<Target>> &targets) override;
    };

    Fanout fanout;

    Multiplexer mux;
    Multiplexer::Simple *bypassSink;

    enum class State {
        Initialize, Active, Ended, Finished, Terminate,
    } state;
    std::size_t controllerIndex;
    CPD3::Data::StreamSink *egress;

    std::mutex egressLock;

    CPD3::Data::SequenceValue::Transfer incoming;

    void stall(std::unique_lock<std::mutex> &lock);

    void executeFinalize(CPD3::Lua::Engine::Frame &frame);

    void finalize(CPD3::Lua::Engine::Frame &frame);

    void processingCompleted();

public:
    FanoutProcessor(ExecThread *thread, CPD3::Lua::Engine::Frame &target);

    virtual ~FanoutProcessor();

    void start() override;

    bool isFinished() override;

    bool wait(double timeout = -1) override;

    void signalTerminate() override;


    void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

    void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

    void incomingData(const CPD3::Data::SequenceValue &value) override;

    void incomingData(CPD3::Data::SequenceValue &&value) override;

    void endData() override;

    void setEgress(CPD3::Data::StreamSink *egress) override;

    static void install(ExecThread *thread, CPD3::Lua::Engine::Frame &root);

    double getOutputAdvance() const;

protected:
    virtual std::shared_ptr<ForegroundTarget> createForegroundTarget() = 0;

    virtual std::shared_ptr<BackgroundTarget> createBackgroundTarget();
};

class FanoutValues : public FanoutProcessor {

    class Foreground : public FanoutProcessor::ForegroundTarget {
        class Buffer : public CPD3::Lua::StreamBuffer::Value {
            Foreground &parent;
        public:
            std::deque<CPD3::Data::SequenceValue> pending;
            double advance;
            bool sinkReady;

            Buffer(Foreground &parent);

            virtual ~Buffer();

        protected:
            bool pushNext(CPD3::Lua::Engine::Frame &target) override;

            void outputReady(CPD3::Lua::Engine::Frame &frame,
                             const CPD3::Lua::Engine::Reference &ref) override;

            void advanceReady(double time) override;

            void endReady() override;
        };

        friend class Buffer;

        FanoutValues &parent;

        Buffer buffer;

        void processBuffer(CPD3::Lua::Engine::Frame &frame,
                           const CPD3::Lua::Engine::Reference &controller,
                           bool ignoreEnd = true);

    public:
        Foreground(FanoutValues &parent);

        virtual ~Foreground();

        void incomingSequenceValue(CPD3::Data::SequenceValue &value) override;

        void incomingDataAdvance(double time) override;

        bool process(CPD3::Lua::Engine::Frame &frame,
                     const CPD3::Lua::Engine::Reference &controller) override;

        void finalize(CPD3::Lua::Engine::Frame &frame,
                      const CPD3::Lua::Engine::Reference &controller) override;

    protected:
        bool initializeCall(CPD3::Lua::Engine::Frame &arguments,
                            const CPD3::Lua::Engine::Reference &controller,
                            const CPD3::Lua::Engine::Reference &key,
                            const std::vector<std::shared_ptr<Target>> &background,
                            CPD3::Lua::Engine::Table &context) override;

        void processSaved(CPD3::Lua::Engine::Frame &saved,
                          const CPD3::Lua::Engine::Reference &controller,
                          const CPD3::Lua::Engine::Reference &key,
                          const std::vector<std::shared_ptr<Target>> &background,
                          CPD3::Lua::Engine::Table &context) override;
    };

public:
    FanoutValues(ExecThread *thread, CPD3::Lua::Engine::Frame &target);

    virtual ~FanoutValues();

protected:

    std::shared_ptr<ForegroundTarget> createForegroundTarget() override;
};

class FanoutSegments : public FanoutProcessor {

    class Foreground : public FanoutProcessor::ForegroundTarget {
        class Buffer : public CPD3::Lua::StreamBuffer::Segment {
            Foreground &parent;
        public:
            std::deque<CPD3::Data::SequenceSegment> pending;
            double advance;
            bool sinkReady;

            Buffer(Foreground &parent);

            virtual ~Buffer();

        protected:
            bool pushNext(CPD3::Lua::Engine::Frame &target) override;

            void convertFromLua(CPD3::Lua::Engine::Frame &frame) override;

            void outputReady(CPD3::Lua::Engine::Frame &frame,
                             const CPD3::Lua::Engine::Reference &ref) override;

            void advanceReady(double time) override;

            void endReady() override;

            bool bufferAssigned(CPD3::Lua::Engine::Frame &frame,
                                const CPD3::Lua::Engine::Reference &target,
                                const CPD3::Lua::Engine::Reference &value) override;

        };

        friend class Buffer;

        FanoutSegments &parent;

        Buffer buffer;
        bool outputsFromScript;
        CPD3::Data::SequenceName::Set outputs;

        CPD3::Data::SequenceSegment::Stream reader;

        void processBuffer(CPD3::Lua::Engine::Frame &frame,
                           const CPD3::Lua::Engine::Reference &controller,
                           bool ignoreEnd = true);

    public:
        Foreground(FanoutSegments &parent);

        virtual ~Foreground();

        void incomingSequenceValue(CPD3::Data::SequenceValue &value) override;

        void incomingDataAdvance(double time) override;

        bool process(CPD3::Lua::Engine::Frame &frame,
                     const CPD3::Lua::Engine::Reference &controller) override;

        void finalize(CPD3::Lua::Engine::Frame &frame,
                      const CPD3::Lua::Engine::Reference &controller) override;

        void addedDispatch(CPD3::Lua::Engine::Frame &frame,
                           const CPD3::Lua::Engine::Reference &controller,
                           const CPD3::Data::SequenceName &name) override;

    protected:
        bool initializeCall(CPD3::Lua::Engine::Frame &arguments,
                            const CPD3::Lua::Engine::Reference &controller,
                            const CPD3::Lua::Engine::Reference &key,
                            const std::vector<std::shared_ptr<Target>> &background,
                            CPD3::Lua::Engine::Table &context) override;

        void processSaved(CPD3::Lua::Engine::Frame &saved,
                          const CPD3::Lua::Engine::Reference &controller,
                          const CPD3::Lua::Engine::Reference &key,
                          const std::vector<std::shared_ptr<Target>> &background,
                          CPD3::Lua::Engine::Table &context) override;
    };

    class Background : public FanoutProcessor::BackgroundTarget {
    public:
        CPD3::Data::SequenceSegment::Stream reader;
        double advance;

        Background();

        virtual ~Background();

        void incomingSequenceValue(CPD3::Data::SequenceValue &value) override;

        void incomingDataAdvance(double time) override;
    };

public:
    FanoutSegments(ExecThread *thread, CPD3::Lua::Engine::Frame &target);

    virtual ~FanoutSegments();

protected:
    std::shared_ptr<ForegroundTarget> createForegroundTarget() override;

    std::shared_ptr<BackgroundTarget> createBackgroundTarget() override;
};

#endif //CPD3LUAEXEC_FANOUT_HXX
