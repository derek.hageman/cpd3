/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <memory>
#include <QLoggingCategory>

#include "action.hxx"
#include "execthread.hxx"
#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "datacore/valueoptionparse.hxx"
#include "datacore/stream.hxx"
#include "luascript/libs/variant.hxx"
#include "luascript/libs/time.hxx"

Q_DECLARE_LOGGING_CATEGORY(log_luaexec)

using namespace CPD3;
using namespace CPD3::Lua;
using namespace CPD3::Data;

void LuaAction::install(ExecThread *thread, CPD3::Lua::Engine::Frame &root)
{
    Engine::Frame frame(root);

    frame.push(frame.global(), "CPD3");
    auto ns = frame.back();
    Q_ASSERT(ns.getType() == Engine::Reference::Type::Table);

    {
        Engine::Assign assign(frame, ns, "action");
        assign.push(std::function<void(Engine::Entry &)>([thread](Engine::Entry &entry) {
            if (entry.empty()) {
                entry.throwError("Insufficient arguments");
                return;
            }

            auto componentName = entry[0].toString();
            auto component = ComponentLoader::create(QString::fromStdString(componentName));
            if (!component) {
                entry.throwError("Failed to load component " + componentName);
                return;
            }

            std::unique_ptr<CPD3Action> action;
            if (auto actionComponent = qobject_cast<ActionComponent *>(component)) {
                auto options = actionComponent->getOptions();

                if (entry.size() < 2) {
                    if (actionComponent->actionRequireStations() > 0) {
                        entry.throwError("Stations required for component " + componentName);
                        return;
                    }

                    action.reset(actionComponent->createAction(options));
                } else {
                    Engine::Table table(entry[1]);

                    CPD3::Data::SequenceName::ComponentSet stations;
                    {
                        Engine::Frame local(entry);
                        local.push(table, "station");
                        if (!local.back().isNil()) {
                            stations.insert(local.back().toString());
                        }
                    }
                    {
                        Engine::Frame local(entry);
                        local.push(table, "stations");
                        if (!local.back().isNil()) {
                            Engine::Iterator it(local, local.back());
                            while (it.next()) {
                                stations.insert(it.value().toString());
                            }
                        }
                    }
                    stations.erase(CPD3::Data::SequenceName::Component());
                    if (stations.size() <
                            static_cast<std::size_t>(actionComponent->actionRequireStations())) {
                        entry.throwError("Insufficient stations provided");
                        return;
                    }
                    if (stations.size() >
                            static_cast<std::size_t>(actionComponent->actionAllowStations())) {
                        entry.throwError("Too many stations provided");
                        return;
                    }

                    {
                        Engine::Frame local(entry);
                        local.push(table, "options");
                        if (!local.back().isNil()) {
                            ValueOptionParse::parse(options,
                                                    Libs::Variant::extract(local, local.back()));
                        }
                    }

                    action.reset(actionComponent->createAction(options, std::vector<std::string>(stations.begin(), stations.end())));
                }
            } else if (auto actionComponentTime = qobject_cast<ActionComponentTime *>(component)) {
                auto options = actionComponentTime->getOptions();

                if (entry.size() < 2) {
                    if (actionComponentTime->actionRequireStations() > 0) {
                        entry.throwError("Stations required for component " + componentName);
                        return;
                    }
                    if (actionComponentTime->actionRequiresTime()) {
                        entry.throwError("Time required for component " + componentName);
                        return;
                    }

                    action.reset(actionComponentTime->createTimeAction(options));
                } else {
                    Engine::Table table(entry[1]);

                    CPD3::Data::SequenceName::ComponentSet stations;
                    {
                        Engine::Frame local(entry);
                        local.push(table, "station");
                        if (!local.back().isNil()) {
                            stations.insert(local.back().toString());
                        }
                    }
                    {
                        Engine::Frame local(entry);
                        local.push(table, "stations");
                        if (!local.back().isNil()) {
                            Engine::Iterator it(local, local.back());
                            while (it.next()) {
                                stations.insert(it.value().toString());
                            }
                        }
                    }
                    stations.erase(CPD3::Data::SequenceName::Component());
                    if (stations.size() <
                            static_cast<std::size_t>(actionComponentTime->actionRequireStations())) {
                        entry.throwError("Insufficient stations provided");
                        return;
                    }
                    if (stations.size() >
                            static_cast<std::size_t>(actionComponentTime->actionAllowStations())) {
                        entry.throwError("Too many stations provided");
                        return;
                    }

                    {
                        Engine::Frame local(entry);
                        local.push(table, "options");
                        if (!local.back().isNil()) {
                            ValueOptionParse::parse(options,
                                                    Libs::Variant::extract(local, local.back()));
                        }
                    }

                    std::vector<std::string> stationsList(stations.begin(), stations.end());

                    {
                        Engine::Frame local(entry);
                        auto start = Libs::Time::pushStart(local, table);
                        auto end = Libs::Time::pushEnd(local, table);

                        if (!start.isNil()) {
                            if (!end.isNil()) {
                                auto bounds = Libs::Time::extractBounds(local, start, end);
                                if (Range::compareStartEnd(bounds.getStart(), bounds.getEnd()) >=
                                        0) {
                                    entry.throwError("Invalid time bounds");
                                    return;
                                }

                                if (!actionComponentTime->actionAcceptsUndefinedBounds() &&
                                        (!FP::defined(bounds.getStart()) ||
                                                !FP::defined(bounds.getEnd()))) {
                                    entry.throwError("Undefined bounds are not permitted");
                                    return;
                                }

                                action.reset(actionComponentTime->createTimeAction(options,
                                                                                   bounds.getStart(),
                                                                                   bounds.getEnd(),
                                                                                   stationsList));
                            } else {
                                if (!actionComponentTime->actionAcceptsUndefinedBounds()) {
                                    entry.throwError("Undefined bounds are not permitted");
                                    return;
                                }
                                double actionStart = Libs::Time::extractSingle(local, start);
                                action.reset(
                                        actionComponentTime->createTimeAction(options, actionStart,
                                                                              FP::undefined(),
                                                                              stationsList));
                            }
                        } else if (!end.isNil()) {
                            if (!actionComponentTime->actionAcceptsUndefinedBounds()) {
                                entry.throwError("Undefined bounds are not permitted");
                                return;
                            }
                            double actionEnd = Libs::Time::extractSingle(local, end);
                            action.reset(
                                    actionComponentTime->createTimeAction(options, FP::undefined(),
                                                                          actionEnd, stationsList));
                        } else {
                            if (actionComponentTime->actionRequiresTime()) {
                                entry.throwError("Time required for component " + componentName);
                                return;
                            }

                            action.reset(
                                    actionComponentTime->createTimeAction(options, stationsList));
                        }
                    }
                }
            } else {
                entry.throwError("Component " + componentName + " is not an action component");
                return;
            }

            if (!action) {
                entry.throwError("Failed to create " + componentName);
                return;
            }

            qCDebug(log_luaexec) << "Execution action" << componentName;

            Threading::Receiver rx;
            thread->terminateRequested
                  .connect(rx, std::bind(&CPD3Action::signalTerminate, action.get()));

            action->start();
            action->wait();
        }));
    }
}