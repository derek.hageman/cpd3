/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <atomic>
#include <QLoggingCategory>

#include "luascript/libs/streamvalue.hxx"
#include "luascript/libs/sequencesegment.hxx"
#include "luascript/libs/sequencename.hxx"
#include "core/threadpool.hxx"

#include "fanout.hxx"

Q_DECLARE_LOGGING_CATEGORY(log_luaexec)

Q_DECLARE_LOGGING_CATEGORY(log_luaexec_pipeline)

using namespace CPD3;
using namespace Data;

static const std::string listRegistryName = "CPD3_luaexecfanout_list";
static std::atomic_uint_fast32_t listInsertIndex(1);

void FanoutProcessor::install(ExecThread *, Lua::Engine::Frame &root)
{
    Lua::Engine::Frame frame(root);

    {
        Lua::Engine::Assign assign(frame, frame.registry(), listRegistryName);
        assign.pushTable();
    }
}

FanoutProcessor::FanoutProcessor(ExecThread *thread, Lua::Engine::Frame &target) : processor(
        std::make_shared<Processor>(thread, *this)),
                                                                                   fanout(*this),
                                                                                   mux(),
                                                                                   bypassSink(
                                                                                           mux.createSimple()),
                                                                                   state(State::Initialize),
                                                                                   controllerIndex(
                                                                                           0),
                                                                                   egress(nullptr)
{
    fanout.pushController(target);
    auto controller = target.back();
    {
        Lua::Engine::Frame frame(target);
        frame.push(frame.registry(), listRegistryName);
        controllerIndex = listInsertIndex.fetch_add(1);
        Lua::Engine::Assign assign(frame, frame.back(), controllerIndex);
        assign.push(controller);
    }

    processor->connect();

    qCDebug(log_luaexec_pipeline) << "Fanout" << controllerIndex << "created";

    thread->finalizeBuffers
          .connect(*this, std::bind(&FanoutProcessor::finalize, this, std::placeholders::_1));
    thread->terminateRequested.connect(*this, std::bind(&FanoutProcessor::signalTerminate, this));
}

FanoutProcessor::~FanoutProcessor()
{
    Threading::Receiver::disconnect();
    {
        std::lock_guard<std::mutex> lock(processor->mutex);
        state = State::Terminate;
    }
    processor->notify.notify_all();
}

void FanoutProcessor::processingCompleted()
{ finished(); }

void FanoutProcessor::executeFinalize(Lua::Engine::Frame &frame)
{
    {
        Lua::Engine::Frame local(frame);
        local.push(local.registry(), listRegistryName);
        Lua::Engine::Table reg(local.back());
        local.push(reg, controllerIndex);
        reg.erase(controllerIndex);
        auto controller = local.back();
        Q_ASSERT(!controller.isNil());

        for (const auto &base : fanout.allTargets()) {
            static_cast<BaseTarget *>(base.get())->finalize(local, controller);
        }
    }

    mux.creationComplete();
    bypassSink->end();
    bypassSink = nullptr;

    egress->incomingData(mux.output());
    egress->endData();
    egress = nullptr;
}

void FanoutProcessor::finalize(Lua::Engine::Frame &frame)
{
    qCDebug(log_luaexec_pipeline) << "Fanout" << controllerIndex << "starting finalization";

    std::unique_lock<std::mutex> egressLocker(egressLock);
    if (!egress) {
        egressLocker.unlock();
        std::unique_lock<std::mutex> lock(processor->mutex);
        egressLocker.lock();
        for (;;) {
            if (egress)
                break;
            if (state == State::Terminate)
                return;
            egressLocker.unlock();
            processor->notify.wait(lock, [this] { return egress || state == State::Terminate; });
            egressLocker.lock();
        }
    }

    executeFinalize(frame);

    egressLocker.unlock();
    {
        std::lock_guard<std::mutex> lock(processor->mutex);
        state = State::Finished;
    }
    processingCompleted();
    processor->notify.notify_all();

    qCDebug(log_luaexec_pipeline) << "Fanout" << controllerIndex << "finalized";
}

void FanoutProcessor::start()
{
    qCDebug(log_luaexec_pipeline) << "Fanout" << controllerIndex << "started";

    {
        std::lock_guard<std::mutex> lock(processor->mutex);
        if (state != State::Initialize)
            return;
        state = State::Active;
    }
    processor->notify.notify_all();
}

bool FanoutProcessor::isFinished()
{
    std::lock_guard<std::mutex> lock(processor->mutex);
    switch (state) {
    case State::Initialize:
    case State::Active:
    case State::Ended:
        return false;
    case State::Finished:
    case State::Terminate:
        return true;
    }
    Q_ASSERT(false);
    return true;
}

bool FanoutProcessor::wait(double timeout)
{
    return Threading::waitForTimeout(timeout, processor->mutex, processor->notify, [this] {
        switch (state) {
        case State::Finished:
        case State::Terminate:
            return true;
        case State::Active:
        case State::Ended:
        case State::Initialize:
            return false;
        }
        Q_ASSERT(false);
        return true;
    });
}

void FanoutProcessor::signalTerminate()
{
    qCDebug(log_luaexec_pipeline) << "Terminating fanout";

    std::unique_lock<std::mutex> lock(processor->mutex);
    if (state == State::Terminate)
        return;
    state = State::Terminate;
    processingCompleted();
    processor->notify.notify_all();
    std::lock_guard<std::mutex> egressLocker(egressLock);
    if (!egress) {
        qCDebug(log_luaexec_pipeline) << "Termination discarding end";
        return;
    }
    lock.unlock();
    egress->endData();
    egress = nullptr;
}

void FanoutProcessor::incomingData(const SequenceValue::Transfer &values)
{
    {
        std::unique_lock<std::mutex> lock(processor->mutex);
        stall(lock);
        if (state == State::Terminate)
            return;
        Q_ASSERT(state == State::Active);
        Util::append(values, incoming);
    }
    processor->notify.notify_all();
}

void FanoutProcessor::incomingData(SequenceValue::Transfer &&values)
{
    {
        std::unique_lock<std::mutex> lock(processor->mutex);
        stall(lock);
        if (state == State::Terminate)
            return;
        Q_ASSERT(state == State::Active);
        Util::append(std::move(values), incoming);
    }
    processor->notify.notify_all();
}

void FanoutProcessor::incomingData(const SequenceValue &value)
{
    {
        std::unique_lock<std::mutex> lock(processor->mutex);
        stall(lock);
        if (state == State::Terminate)
            return;
        Q_ASSERT(state == State::Active);
        incoming.emplace_back(value);
    }
    processor->notify.notify_all();
}

void FanoutProcessor::incomingData(SequenceValue &&value)
{
    {
        std::unique_lock<std::mutex> lock(processor->mutex);
        stall(lock);
        if (state == State::Terminate)
            return;
        Q_ASSERT(state == State::Active);
        incoming.emplace_back(std::move(value));
    }
    processor->notify.notify_all();
}

void FanoutProcessor::endData()
{
    qCDebug(log_luaexec_pipeline) << "Fanout" << controllerIndex << "received end";

    {
        std::lock_guard<std::mutex> lock(processor->mutex);
        if (state == State::Terminate || state == State::Ended)
            return;
        Q_ASSERT(state == State::Active);
        state = State::Ended;
    }
    processor->notify.notify_all();
}

void FanoutProcessor::setEgress(StreamSink *egress)
{
    std::lock_guard<std::mutex> lock(processor->mutex);
    std::lock_guard<std::mutex> egressLocker(egressLock);
    this->egress = egress;
    processor->notify.notify_all();
}

void FanoutProcessor::stall(std::unique_lock<std::mutex> &lock)
{
    while (incoming.size() > stallThreshold) {
        switch (state) {
        case State::Initialize:
        case State::Active:
            break;
        case State::Ended:
        case State::Finished:
        case State::Terminate:
            return;
        }
        processor->notify.wait(lock);
    }
}

double FanoutProcessor::getOutputAdvance() const
{ return mux.getCurrentAdvance(); }

std::shared_ptr<FanoutProcessor::BackgroundTarget> FanoutProcessor::createBackgroundTarget()
{ return std::make_shared<BackgroundTarget>(); }

FanoutProcessor::Processor::Processor(ExecThread *thread, FanoutProcessor &parent)
        : ExecThread::Processor(thread), parent(parent)
{ }

FanoutProcessor::Processor::~Processor() = default;

ExecThread::Processor::ExecutionState FanoutProcessor::Processor::execute(Lua::Engine::Frame &frame,
                                                                          std::unique_lock<
                                                                                  std::mutex> &lock)
{
    if (parent.state == State::Terminate || parent.state == State::Initialize) {
        parent.state = State::Terminate;
        notify.notify_all();
        parent.processingCompleted();
        std::unique_lock<std::mutex> egressLocker(parent.egressLock);
        if (parent.egress) {
            auto egress = parent.egress;
            parent.egress = nullptr;
            lock.unlock();
            egress->endData();
        }
        return ExecutionState::Complete;
    }

    std::unique_lock<std::mutex> egressLocker(parent.egressLock);
    if (!parent.egress)
        return ExecThread::Processor::ExecutionState::Waiting;
    if (parent.incoming.empty()) {
        switch (parent.state) {
        case State::Initialize:
        case State::Terminate:
            Q_ASSERT(false);
            break;
        case State::Active:
            return ExecThread::Processor::ExecutionState::Waiting;
        case State::Ended: {
            lock.unlock();

            static_cast<Threading::Receiver &>(parent).disconnectImmediate();

            parent.executeFinalize(frame);

            qCDebug(log_luaexec_pipeline) << "Fanout" << parent.controllerIndex << "completed";

            egressLocker.unlock();
            lock.lock();
            parent.state = State::Finished;
            parent.processingCompleted();
            return ExecutionState::Complete;
        }
        case State::Finished:
            return ExecutionState::Complete;
        }
    }


    Lua::Engine::Frame local(frame);
    local.push(local.registry(), listRegistryName);
    local.push(local.back(), parent.controllerIndex);
    auto controller = local.back();
    Q_ASSERT(!controller.isNil());

    auto toProcess = std::move(parent.incoming);
    parent.incoming.clear();
    lock.unlock();

    if (toProcess.size() > stallThreshold)
        notify.notify_all();

    bool muxOutputReady = false;
    if (!toProcess.empty()) {
        double advance = toProcess.back().getStart();

        for (auto &value : toProcess) {
            const auto &targets = parent.fanout.dispatch(local, controller, value.getName());

            for (const auto &base : targets) {
                static_cast<BaseTarget *>(base.get())->incomingSequenceValue(value);
            }
        }

        if (FP::defined(advance)) {
            for (const auto &base : parent.fanout.allTargets()) {
                auto target = static_cast<BaseTarget *>(base.get());
                target->incomingDataAdvance(advance);
            }
            if (parent.bypassSink->advance(advance))
                muxOutputReady = true;
        }
        for (const auto &base : parent.fanout.allTargets()) {
            if (!static_cast<BaseTarget *>(base.get())->process(local, controller))
                continue;
            muxOutputReady = true;
        }
    }

    if (muxOutputReady) {
        parent.egress->incomingData(parent.mux.output());
    }

    return ExecutionState::DidUnlock;
}

FanoutProcessor::Fanout::Fanout(FanoutProcessor &parent) : parent(parent)
{ }

FanoutProcessor::Fanout::~Fanout() = default;

std::shared_ptr<
        Lua::FanoutController::Target> FanoutProcessor::Fanout::createTarget(const Lua::Engine::Output &type)
{
    if (type.toBoolean())
        return parent.createBackgroundTarget();
    return parent.createForegroundTarget();
}

void FanoutProcessor::Fanout::addedDispatch(Lua::Engine::Frame &frame,
                                            const Lua::Engine::Reference &controller,
                                            const Data::SequenceName &name,
                                            std::vector<std::shared_ptr<
                                                    Lua::FanoutController::Target>> &targets)
{
    bool haveBackground = false;
    for (const auto &check : targets) {
        if (dynamic_cast<BackgroundTarget *>(check.get())) {
            haveBackground = true;
            break;
        }
    }
    if (haveBackground) {
        targets.emplace_back(std::make_shared<BypassTarget>(parent));
        return;
    }

    bool haveForeground = false;
    for (const auto &check : targets) {
        if (auto fg = dynamic_cast<ForegroundTarget *>(check.get())) {
            fg->addedDispatch(frame, controller, name);
            haveForeground = true;
        }
    }
    if (!haveForeground) {
        targets.emplace_back(std::make_shared<BypassTarget>(parent));
    }
}

FanoutProcessor::BaseTarget::BaseTarget() = default;

FanoutProcessor::BaseTarget::~BaseTarget() = default;

void FanoutProcessor::BaseTarget::incomingDataAdvance(double)
{ }

bool FanoutProcessor::BaseTarget::process(Lua::Engine::Frame &, const Lua::Engine::Reference &)
{ return false; }

void FanoutProcessor::BaseTarget::finalize(Lua::Engine::Frame &, const Lua::Engine::Reference &)
{ }

FanoutProcessor::ForegroundTarget::ForegroundTarget(FanoutProcessor &parent) : sink(
        parent.mux.createSimple())
{ }

FanoutProcessor::ForegroundTarget::~ForegroundTarget() = default;

void FanoutProcessor::ForegroundTarget::addedDispatch(Lua::Engine::Frame &,
                                                      const Lua::Engine::Reference &,
                                                      const Data::SequenceName &)
{ }

FanoutProcessor::BackgroundTarget::BackgroundTarget() = default;

FanoutProcessor::BackgroundTarget::~BackgroundTarget() = default;

void FanoutProcessor::BackgroundTarget::incomingSequenceValue(Data::SequenceValue &)
{ }

bool FanoutProcessor::BackgroundTarget::initializeCall(Lua::Engine::Frame &arguments,
                                                       const Lua::Engine::Reference &controller,
                                                       const Lua::Engine::Reference &key,
                                                       const std::vector<
                                                               std::shared_ptr<Target>> &background,
                                                       Lua::Engine::Table &context)
{
    arguments.clear();
    return false;
}

FanoutProcessor::BypassTarget::BypassTarget(FanoutProcessor &parent) : sink(parent.bypassSink)
{ }

FanoutProcessor::BypassTarget::~BypassTarget() = default;

void FanoutProcessor::BypassTarget::incomingSequenceValue(Data::SequenceValue &value)
{
    /* Bypass targets are always last, so we can do a move */
    sink->incomingValue(std::move(value));
}


FanoutValues::FanoutValues(ExecThread *thread, Lua::Engine::Frame &target) : FanoutProcessor(thread,
                                                                                             target)
{ }

FanoutValues::~FanoutValues() = default;

std::shared_ptr<FanoutProcessor::ForegroundTarget> FanoutValues::createForegroundTarget()
{ return std::make_shared<Foreground>(*this); }

FanoutValues::Foreground::Foreground(FanoutValues &parent) : FanoutProcessor::ForegroundTarget(
        parent), parent(parent), buffer(*this)
{ }

FanoutValues::Foreground::~Foreground() = default;

void FanoutValues::Foreground::incomingSequenceValue(Data::SequenceValue &value)
{
    buffer.advance = FP::undefined();
    buffer.pending.emplace_back(value);
}

bool FanoutValues::Foreground::process(Lua::Engine::Frame &frame,
                                       const Lua::Engine::Reference &controller)
{
    processBuffer(frame, controller);

    bool result = buffer.sinkReady;
    buffer.sinkReady = false;
    return result;
}

void FanoutValues::Foreground::finalize(Lua::Engine::Frame &frame,
                                        const Lua::Engine::Reference &controller)
{
    processBuffer(frame, controller, false);

    Lua::Engine::Frame local(frame);
    pushSaved(local, controller, 1);
    auto bcontroller = local.back();
    buffer.finish(local, bcontroller);
}

bool FanoutValues::Foreground::initializeCall(Lua::Engine::Frame &arguments,
                                              const Lua::Engine::Reference &controller,
                                              const Lua::Engine::Reference &key,
                                              const std::vector<std::shared_ptr<
                                                      Lua::FanoutController::Target>> &background,
                                              Lua::Engine::Table &context)
{
    buffer.advance = parent.getOutputAdvance();

    buffer.pushExternalController(arguments);
    context.set("bcontrol", arguments.back());

    arguments.push(key);

    arguments.propagate(2);
    return true;
}

void FanoutValues::Foreground::processSaved(Lua::Engine::Frame &saved,
                                            const Lua::Engine::Reference &controller,
                                            const Lua::Engine::Reference &key,
                                            const std::vector<std::shared_ptr<
                                                    Lua::FanoutController::Target>> &background,
                                            Lua::Engine::Table &context)
{
    saved.resize(1);
    if (saved[0].isNil())
        return;
    saved.push(context, "bcontrol");
}

void FanoutValues::Foreground::processBuffer(Lua::Engine::Frame &frame,
                                             const Lua::Engine::Reference &controller,
                                             bool ignoreEnd)
{
    Lua::Engine::Frame local(frame);
    pushSaved(local, controller, 1);
    auto bcontroller = local.back();
    pushSaved(local, controller, 0);
    auto invoke = local.back();

    for (;;) {
        Lua::Engine::Call call(local);
        call.push(invoke);
        if (call.front().isNil())
            return;
        if (!buffer.pushExternal(call, bcontroller, ignoreEnd))
            return;
        if (!call.execute()) {
            call.clearError();
            continue;
        }
    }
}

void FanoutValues::Foreground::incomingDataAdvance(double time)
{ buffer.advance = time; }

FanoutValues::Foreground::Buffer::Buffer(Foreground &parent) : parent(parent),
                                                               pending(),
                                                               advance(FP::undefined()),
                                                               sinkReady(false)
{ }

FanoutValues::Foreground::Buffer::~Buffer() = default;

bool FanoutValues::Foreground::Buffer::pushNext(CPD3::Lua::Engine::Frame &target)
{
    if (!pending.empty()) {
        target.pushData<Lua::Libs::SequenceValue>(std::move(pending.front()));
        pending.pop_front();
        return true;
    }
    if (FP::defined(advance)) {
        target.push(advance);
        advance = FP::undefined();
        return true;
    }
    return false;
}

void FanoutValues::Foreground::Buffer::outputReady(CPD3::Lua::Engine::Frame &frame,
                                                   const CPD3::Lua::Engine::Reference &ref)
{
    Q_ASSERT(parent.sink);
    if (parent.sink->incomingValue(extract(frame, ref)))
        sinkReady = true;
}

void FanoutValues::Foreground::Buffer::advanceReady(double time)
{
    Q_ASSERT(parent.sink);
    if (parent.sink->advance(time))
        sinkReady = true;
}

void FanoutValues::Foreground::Buffer::endReady()
{
    if (parent.sink->end())
        sinkReady = true;
    parent.sink = nullptr;
}


FanoutSegments::FanoutSegments(ExecThread *thread, Lua::Engine::Frame &target) : FanoutProcessor(
        thread, target)
{ }

FanoutSegments::~FanoutSegments() = default;

std::shared_ptr<FanoutProcessor::ForegroundTarget> FanoutSegments::createForegroundTarget()
{ return std::make_shared<Foreground>(*this); }

std::shared_ptr<FanoutProcessor::BackgroundTarget> FanoutSegments::createBackgroundTarget()
{ return std::make_shared<Background>(); }

FanoutSegments::Background::Background() : advance(FP::undefined())
{ }

FanoutSegments::Background::~Background() = default;

void FanoutSegments::Background::incomingSequenceValue(Data::SequenceValue &value)
{
    reader.add(value);
    advance = value.getStart();
}

void FanoutSegments::Background::incomingDataAdvance(double time)
{
    reader.advance(time);
    advance = time;
}

FanoutSegments::Foreground::Foreground(FanoutSegments &parent) : FanoutProcessor::ForegroundTarget(
        parent), parent(parent), buffer(*this), outputsFromScript(false)
{ }

FanoutSegments::Foreground::~Foreground() = default;

void FanoutSegments::Foreground::incomingSequenceValue(Data::SequenceValue &value)
{
    buffer.advance = FP::undefined();
    Util::append(reader.add(value), buffer.pending);
}

void FanoutSegments::Foreground::incomingDataAdvance(double time)
{
    Util::append(reader.advance(time), buffer.pending);
    buffer.advance = time;
}

bool FanoutSegments::Foreground::process(Lua::Engine::Frame &frame,
                                         const Lua::Engine::Reference &controller)
{
    processBuffer(frame, controller);

    bool result = buffer.sinkReady;
    buffer.sinkReady = false;
    return result;
}

void FanoutSegments::Foreground::finalize(Lua::Engine::Frame &frame,
                                          const Lua::Engine::Reference &controller)
{
    Util::append(reader.finish(), buffer.pending);
    processBuffer(frame, controller, false);

    Lua::Engine::Frame local(frame);
    pushSaved(local, controller, 2);
    auto bcontroller = local.back();
    buffer.finish(local, bcontroller);
}

bool FanoutSegments::Foreground::initializeCall(Lua::Engine::Frame &arguments,
                                                const Lua::Engine::Reference &controller,
                                                const Lua::Engine::Reference &key,
                                                const std::vector<std::shared_ptr<
                                                        Lua::FanoutController::Target>> &background,
                                                Lua::Engine::Table &context)
{
    buffer.advance = parent.getOutputAdvance();
    for (const auto &check : background) {
        auto btarget = dynamic_cast<Background *>(check.get());
        if (!btarget)
            continue;
        reader.overlay(btarget->reader);
        if (FP::defined(btarget->advance) &&
                (!FP::defined(buffer.advance) || buffer.advance < btarget->advance)) {
            buffer.advance = btarget->advance;
        }
    }
    reader.advance(buffer.advance);

    buffer.pushExternalController(arguments);
    context.set("bcontrol", arguments.back());

    arguments.push(key);

    arguments.propagate(2);
    return true;
}

void FanoutSegments::Foreground::processSaved(Lua::Engine::Frame &saved,
                                              const Lua::Engine::Reference &controller,
                                              const Lua::Engine::Reference &key,
                                              const std::vector<std::shared_ptr<
                                                      Lua::FanoutController::Target>> &background,
                                              Lua::Engine::Table &context)
{
    saved.resize(2);
    if (saved[0].isNil())
        return;
    if (!saved[1].isNil()) {
        outputsFromScript = true;
    }
    saved.push(context, "bcontrol");
}

void FanoutSegments::Foreground::processBuffer(Lua::Engine::Frame &frame,
                                               const Lua::Engine::Reference &controller,
                                               bool ignoreEnd)
{
    Lua::Engine::Frame local(frame);
    pushSaved(local, controller, 2);
    auto bcontroller = local.back();
    pushSaved(local, controller, 0);
    auto invoke = local.back();

    for (;;) {
        Lua::Engine::Call call(local);
        call.push(invoke);
        if (call.front().isNil())
            return;
        if (!buffer.pushExternal(call, bcontroller, ignoreEnd))
            return;
        if (!call.execute()) {
            call.clearError();
            continue;
        }
    }
}

void FanoutSegments::Foreground::addedDispatch(CPD3::Lua::Engine::Frame &frame,
                                               const CPD3::Lua::Engine::Reference &controller,
                                               const CPD3::Data::SequenceName &name)
{
    if (!outputsFromScript) {
        outputs.insert(name);
        return;
    }

    Lua::Engine::Call call(frame);
    pushSaved(call, controller, 1);
    call.pushData<Lua::Libs::SequenceName>(name);
    if (!call.executeVariable()) {
        call.clearError();
        return;
    }
    for (std::size_t i = 0, max = call.size(); i < max; i++) {
        outputs.insert(Lua::Libs::SequenceName::extract(call, call[i]));
    }
}

namespace {
class OutputSegment : public Lua::Libs::SequenceSegment {
    CPD3::Data::SequenceName::Set outputs;
    bool requireExists;
public:
    OutputSegment() : requireExists(true)
    { }

    virtual ~OutputSegment() = default;

    explicit OutputSegment(const CPD3::Data::SequenceSegment &segment) : Lua::Libs::SequenceSegment(
            segment), requireExists(false)
    { }

    explicit OutputSegment(CPD3::Data::SequenceSegment &&segment) : Lua::Libs::SequenceSegment(
            std::move(segment)), requireExists(false)
    { }

    OutputSegment(const CPD3::Data::SequenceSegment &segment,
                  const CPD3::Data::SequenceName::Set &outputs,
                  bool requireExists = true) : Lua::Libs::SequenceSegment(segment),
                                               outputs(std::move(outputs)),
                                               requireExists(requireExists)
    { }

    OutputSegment(CPD3::Data::SequenceSegment &&segment,
                  const CPD3::Data::SequenceName::Set &outputs,
                  bool requireExists = true) : Lua::Libs::SequenceSegment(std::move(segment)),
                                               outputs(std::move(outputs)),
                                               requireExists(requireExists)
    { }

    void setOutputs(CPD3::Data::SequenceName::Set out)
    {
        outputs = std::move(out);
        requireExists = false;
    }

    CPD3::Data::SequenceValue::Transfer extract(Lua::Engine::Frame &frame,
                                                const Lua::Engine::Reference &ref)
    { return Lua::StreamBuffer::Segment::extract(frame, ref, outputs, requireExists); }
};
}

FanoutSegments::Foreground::Buffer::Buffer(Foreground &parent) : parent(parent),
                                                                 pending(),
                                                                 advance(FP::undefined()),
                                                                 sinkReady(false)
{ }

FanoutSegments::Foreground::Buffer::~Buffer() = default;

bool FanoutSegments::Foreground::Buffer::pushNext(CPD3::Lua::Engine::Frame &target)
{
    if (!pending.empty()) {
        target.pushData<OutputSegment>(std::move(pending.front()), parent.outputs,
                                       !parent.outputsFromScript);
        pending.pop_front();
        return true;
    }
    if (FP::defined(advance)) {
        target.push(advance);
        advance = FP::undefined();
        return true;
    }
    return false;
}

void FanoutSegments::Foreground::Buffer::convertFromLua(Lua::Engine::Frame &frame)
{
    if (frame.back().toData<OutputSegment>())
        return;
    auto value = Lua::Libs::SequenceSegment::extract(frame, frame.back());
    frame.pop();
    frame.pushData<OutputSegment>(std::move(value), parent.outputs, !parent.outputsFromScript);
}

void FanoutSegments::Foreground::Buffer::outputReady(CPD3::Lua::Engine::Frame &frame,
                                                     const CPD3::Lua::Engine::Reference &ref)
{
    auto value = ref.toData<OutputSegment>();
    if (!value)
        return;
    if (parent.sink->incoming(value->extract(frame, ref)))
        sinkReady = true;
}

void FanoutSegments::Foreground::Buffer::advanceReady(double time)
{
    Q_ASSERT(parent.sink);
    if (parent.sink->advance(time))
        sinkReady = true;
}

void FanoutSegments::Foreground::Buffer::endReady()
{
    if (parent.sink->end())
        sinkReady = true;
    parent.sink = nullptr;
}

bool FanoutSegments::Foreground::Buffer::bufferAssigned(Lua::Engine::Frame &frame,
                                                        const Lua::Engine::Reference &target,
                                                        const Lua::Engine::Reference &value)
{
    auto output = target.toData<OutputSegment>();
    if (!output)
        return false;
    if (value.getType() == Lua::Engine::Reference::Type::Table) {
        CPD3::Data::SequenceName::Set result;
        Lua::Engine::Iterator it(frame, value);
        bool allNumeric = true;
        while (it.next()) {
            if (it.key().getType() != Lua::Engine::Reference::Type::Number) {
                allNumeric = false;
                break;
            }
            auto name = Lua::Libs::SequenceName::extract(frame, value);
            if (!name.isValid()) {
                allNumeric = false;
                break;
            }
            result.insert(std::move(name));
        }
        if (allNumeric) {
            output->setOutputs(std::move(result));
            return true;
        }
    }
    auto name = Lua::Libs::SequenceName::extract(frame, value);
    if (!name.isValid())
        return false;
    output->setOutputs({std::move(name)});
    return true;
}