/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3LUAEXEC_PROCESSOR_HXX
#define CPD3LUAEXEC_PROCESSOR_HXX

#include "core/first.hxx"

#include <mutex>
#include <condition_variable>

#include "core/threading.hxx"
#include "luascript/streambuffer.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/segment.hxx"
#include "execthread.hxx"

class ProcessorBuffered : public CPD3::Data::ProcessingStage, public CPD3::Threading::Receiver {

    std::unique_ptr<CPD3::Lua::StreamBuffer> buffer;

    class Processor : public ExecThread::Processor {
        ProcessorBuffered &parent;
    public:
        Processor(ExecThread *thread, ProcessorBuffered &parent);

        virtual ~Processor();

        ExecutionState execute(CPD3::Lua::Engine::Frame &frame,
                               std::unique_lock<std::mutex> &lock) override;
    };

    friend class Processor;

    std::shared_ptr<ExecThread::Processor> processor;

    enum class State {
        Initialize, Active, Ended, Finished, Terminate,
    } state;
    std::size_t controllerIndex;
    CPD3::Data::StreamSink *egress;

    std::mutex egressLock;

    void stall(std::unique_lock<std::mutex> &lock);

    void finalize(CPD3::Lua::Engine::Frame &frame);

    void processingCompleted();

public:
    ProcessorBuffered(ExecThread *thread,
                      CPD3::Lua::Engine::Frame &target,
                      const CPD3::Lua::Engine::Reference &call,
                      CPD3::Lua::StreamBuffer *buffer);

    virtual ~ProcessorBuffered();

    void start() override;

    bool isFinished() override;

    bool wait(double timeout = -1) override;

    void signalTerminate() override;


    void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

    void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

    void incomingData(const CPD3::Data::SequenceValue &value) override;

    void incomingData(CPD3::Data::SequenceValue &&value) override;

    void endData() override;

    void setEgress(CPD3::Data::StreamSink *egress) override;

    static void install(ExecThread *thread, CPD3::Lua::Engine::Frame &root);

protected:

    virtual bool stall() const = 0;

    virtual bool processIncoming(const CPD3::Data::SequenceValue::Transfer &values) = 0;

    virtual bool processIncoming(CPD3::Data::SequenceValue::Transfer &&values) = 0;

    virtual bool processIncoming(const CPD3::Data::SequenceValue &value) = 0;

    virtual bool processIncoming(CPD3::Data::SequenceValue &&value) = 0;

    virtual void processEnd();

    virtual void processResult(CPD3::Lua::Engine::Frame &frame,
                               const CPD3::Lua::Engine::Reference &value);


    inline CPD3::Data::StreamSink *acquireOutput()
    { return egress; }

    std::unique_lock<std::mutex> acquireState();

    virtual bool nextReady() const = 0;
};

class ValueProcessor : public ProcessorBuffered {
    class Buffer : public CPD3::Lua::StreamBuffer::Value {
        ValueProcessor &parent;
    public:
        Buffer(ValueProcessor &parent);

        virtual ~Buffer();

    protected:
        bool pushNext(CPD3::Lua::Engine::Frame &target) override;

        void outputReady(CPD3::Lua::Engine::Frame &frame,
                         const CPD3::Lua::Engine::Reference &ref) override;

        void endReady() override;
    };

    friend class Buffer;

    std::deque<CPD3::Data::SequenceValue> incoming;

public:
    ValueProcessor(ExecThread *thread,
                   CPD3::Lua::Engine::Frame &target,
                   const CPD3::Lua::Engine::Reference &call);

    virtual ~ValueProcessor();

protected:
    bool stall() const override;

    bool processIncoming(const CPD3::Data::SequenceValue::Transfer &values) override;

    bool processIncoming(CPD3::Data::SequenceValue::Transfer &&values) override;

    bool processIncoming(const CPD3::Data::SequenceValue &value) override;

    bool processIncoming(CPD3::Data::SequenceValue &&value) override;

    bool nextReady() const override;
};

class SegmentProcessor : public ProcessorBuffered {
    class Buffer : public CPD3::Lua::StreamBuffer::Segment {
        SegmentProcessor &parent;
    public:
        Buffer(SegmentProcessor &parent);

        virtual ~Buffer();

    protected:
        bool pushNext(CPD3::Lua::Engine::Frame &target) override;

        void convertFromLua(CPD3::Lua::Engine::Frame &frame) override;

        void outputReady(CPD3::Lua::Engine::Frame &frame,
                         const CPD3::Lua::Engine::Reference &ref) override;

        void endReady() override;

        bool bufferAssigned(CPD3::Lua::Engine::Frame &frame,
                            const CPD3::Lua::Engine::Reference &target,
                            const CPD3::Lua::Engine::Reference &value) override;
    };

    friend class Buffer;

    bool addInputsToOutputs;
    CPD3::Data::SequenceName::Set outputs;
    std::deque<CPD3::Data::SequenceSegment> incoming;
    CPD3::Data::SequenceSegment::Stream reader;

public:
    SegmentProcessor(ExecThread *thread,
                     CPD3::Lua::Engine::Frame &target,
                     const CPD3::Lua::Engine::Reference &call);

    virtual ~SegmentProcessor();

    void setExplicitOutputs(CPD3::Data::SequenceName::Set outputs);

protected:
    bool stall() const override;

    bool processIncoming(const CPD3::Data::SequenceValue::Transfer &values) override;

    bool processIncoming(CPD3::Data::SequenceValue::Transfer &&values) override;

    bool processIncoming(const CPD3::Data::SequenceValue &value) override;

    bool processIncoming(CPD3::Data::SequenceValue &&value) override;

    void processEnd() override;

    bool nextReady() const override;

    void processResult(CPD3::Lua::Engine::Frame &frame,
                       const CPD3::Lua::Engine::Reference &value) override;
};

#endif //CPD3LUAEXEC_PROCESSOR_HXX
