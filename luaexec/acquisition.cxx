/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include "acquisition.hxx"
#include "execthread.hxx"
#include "luascript/libs/valuesegment.hxx"
#include "luascript/libs/variant.hxx"
#include "luascript/libs/streamvalue.hxx"
#include "luascript/libs/time.hxx"

Q_DECLARE_LOGGING_CATEGORY(log_luaexec)


Q_LOGGING_CATEGORY(log_luaexec_acquisition, "cpd3.luaexec.acquisition", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Lua;
using namespace CPD3::Acquisition;

static const std::string listRegistryName = "CPD3_luaexecacquisition_list";
static std::atomic_uint_fast32_t listInsertIndex(1);

void LuaAcquisition::install(ExecThread *thread, Engine::Frame &root)
{
    Engine::Frame frame(root);

    {
        Lua::Engine::Assign assign(frame, frame.registry(), listRegistryName);
        assign.pushTable();
    }

    frame.push(frame.global(), "CPD3");
    auto ns = frame.back();
    Q_ASSERT(ns.getType() == Engine::Reference::Type::Table);

    {
        Engine::Assign assign(frame, ns, "Acquisition");
        assign.push(std::function<void(Engine::Entry &)>([thread](Engine::Entry &entry) {
            if (entry.empty()) {
                entry.throwError("Insufficient arguments");
                return;
            }

            auto componentName = entry[0].toString();
            auto component = ComponentLoader::create(QString::fromStdString(componentName));
            if (!component) {
                entry.throwError("Failed to load component " + componentName);
                return;
            }
            AcquisitionComponent
                    *acquisitionComponent = qobject_cast<AcquisitionComponent *>(component);
            if (!acquisitionComponent) {
                entry.throwError("Component " + componentName + " is not an acquisition component");
                return;
            }

            std::unique_ptr<AcquisitionInterface> interface;
            if (entry.size() < 2) {
                auto options = acquisitionComponent->getPassiveOptions();
                interface = acquisitionComponent->createAcquisitionPassive(options);
            } else {
                Engine::Table table(entry[1]);
                auto loggingContext = table.get("loggingContext").toString();
                ValueSegment::Transfer config;

                {
                    Engine::Frame local(entry);
                    local.push(table, "configuration");
                    if (local.back().isNil()) {
                        local.push(table, "config");
                    }
                    if (!local.back().isNil()) {
                        Engine::Table segments(local.back());
                        if (segments.length() > 0) {
                            for (std::size_t i = 1, max = segments.length(); i <= max; i++) {
                                Engine::Frame lf(local);
                                lf.push(segments, i);
                                if (lf.back().isNil())
                                    continue;
                                config.emplace_back(Libs::ValueSegment::extract(lf, lf.back()));
                            }
                            config = CPD3::Data::ValueSegment::merge(config);
                        } else {
                            config.emplace_back(Libs::ValueSegment::extract(local, local.back()));
                        }
                    }
                }

                auto mode = table.get("mode").toString();
                if (Util::equal_insensitive(mode, "interactive")) {
                    interface = acquisitionComponent->createAcquisitionInteractive(config,
                                                                                   loggingContext);
                } else if (Util::equal_insensitive(mode, "autoprobe")) {
                    interface = acquisitionComponent->createAcquisitionAutoprobe(config,
                                                                                 loggingContext);
                } else {
                    Engine::Frame local(entry);
                    local.push(table, "options");
                    if (!local.back().isNil()) {
                        auto options = acquisitionComponent->getPassiveOptions();
                        ValueOptionParse::parse(options,
                                                Libs::Variant::extract(local, local.back()));
                        interface = acquisitionComponent->createAcquisitionPassive(options,
                                                                                   loggingContext);
                    } else {
                        interface = acquisitionComponent->createAcquisitionPassive(config,
                                                                                   loggingContext);
                    }
                }
            }

            if (!interface) {
                entry.throwError("Failed to create " + componentName);
                return;
            }

            qCDebug(log_luaexec_acquisition) << "Created acquistion for component" << componentName;

            entry.clear();
            entry.pushData<LuaAcquisition>(thread, std::move(interface));
            entry.propagate(1);
        }));
    }
}

LuaAcquisition::LuaAcquisition(ExecThread *thread,
                               std::unique_ptr<CPD3::Acquisition::AcquisitionInterface> &&in)
        : interface(std::move(in)),
          selfIndex(0),
          advanceTime(FP::undefined()),
          remapEnabled(false),
          station("nil"),
          activeCallbackProcessing(nullptr),
          state(*this),
          loggingSink(*this, "logging"),
          realtimeSink(*this, "realtime"),
          persistentSink(*this, "persistent")
{
    thread->finalizeAcquisition
          .connect(*this, std::bind(&LuaAcquisition::finalize, this, std::placeholders::_1));
    thread->terminateRequested.connect(*this, std::bind(&LuaAcquisition::signalTerminate, this));

    auto info = interface->getDefaults();
    {
        auto name = info.name;
        if (!name.empty()) {
            Util::replace_all(name, "$2", "1");
            Util::replace_all(name, "$1", "1");
            suffix = "_" + name;
        }
    }

    interface->start();
}

LuaAcquisition::~LuaAcquisition()
{
    Threading::Receiver::disconnect();

    interface->signalTerminate();
    interface->wait();
}

void LuaAcquisition::initialize(const Engine::Reference &self, Engine::Frame &frame)
{
    {
        frame.push(frame.registry(), listRegistryName);
        selfIndex = listInsertIndex.fetch_add(1);
        Lua::Engine::Assign assign(frame, frame.back(), selfIndex);
        assign.push(self);
    }

    auto mt = pushMetatable(frame);

    {
        Engine::Assign assign(frame, mt, "__call");
        pushMethod<LuaAcquisition>(assign, &LuaAcquisition::method_incoming_data);
    }
    {
        Engine::Assign assign(frame, mt, "__newindex");
        pushMethod<LuaAcquisition>(assign, &LuaAcquisition::meta_newindex);
    }

    {
        Engine::Assign assign(frame, mt, "__index");
        auto methods = assign.pushTable();

        {
            Engine::Assign ma(assign, methods, "advance");
            pushMethod<LuaAcquisition>(ma, &LuaAcquisition::method_advance);
        }
        {
            Engine::Frame mf(assign);
            pushMethod<LuaAcquisition>(mf, &LuaAcquisition::method_command);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "command");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "incomingCommand");
                ma.push(m);
            }
        }
        {
            Engine::Assign ma(assign, methods, "incomingRealtime");
            pushMethod<LuaAcquisition>(ma, &LuaAcquisition::method_incomingRealtime);
        }
        {
            Engine::Assign ma(assign, methods, "initializeState");
            pushMethod<LuaAcquisition>(ma, &LuaAcquisition::method_initializeState);
        }
        {
            Engine::Assign ma(assign, methods, "finish");
            pushMethod<LuaAcquisition>(ma, &LuaAcquisition::method_finish);
        }

        {
            Engine::Frame mf(assign);
            pushMethod<LuaAcquisition>(mf, &LuaAcquisition::method_incoming_data);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "incoming_data");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "incomingData");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "data");
                ma.push(m);
            }
        }
        {
            Engine::Frame mf(assign);
            pushMethod<LuaAcquisition>(mf, &LuaAcquisition::method_incoming_control);
            auto m = mf.back();
            {
                Engine::Assign ma(mf, methods, "incoming_control");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "incomingControl");
                ma.push(m);
            }
            {
                Engine::Assign ma(mf, methods, "control");
                ma.push(m);
            }
        }

        pushMethod<LuaAcquisition>(assign, &LuaAcquisition::meta_index, 1);
    }

    {
        Engine::Assign assign(frame, mt, "logging");
        assign.pushNil();
    }
    {
        Engine::Assign assign(frame, mt, "realtime");
        assign.pushNil();
    }
    {
        Engine::Assign assign(frame, mt, "persistent");
        assign.pushNil();
    }
    {
        Engine::Assign assign(frame, mt, "remap");
        assign.pushNil();
    }

    setMetatable(self, mt);
}

void LuaAcquisition::finalize(Engine::Frame &frame)
{
    if (selfIndex) {
        qCDebug(log_luaexec_acquisition) << "Finalizing acquisition";

        Lua::Engine::Frame local(frame);
        local.push(local.registry(), listRegistryName);
        Lua::Engine::Table reg(local.back());
        local.push(reg, selfIndex);
        auto self = local.back();
        reg.erase(selfIndex);

        {
            CallbackProcessing processing(*this, local, self);
            processing.release();
            interface->initiateShutdown();
            interface->wait();
        }
    } else {
        signalTerminate();
        interface->wait();
    }
    completeOutputs(frame);
}

void LuaAcquisition::signalTerminate()
{
    qCDebug(log_luaexec_acquisition) << "Terminating acquisition";

    {
        std::lock_guard<std::mutex> lock(mutex);
        remapEnabled = false;
    }
    notify.notify_all();
    interface->signalTerminate();
}

void LuaAcquisition::completeOutputs(Engine::Frame &)
{
    loggingSink.clear();
    realtimeSink.clear();
    persistentSink.clear();
}

void LuaAcquisition::modifyValue(CPD3::Data::SequenceValue &value)
{
    if (!station.empty()) {
        value.setStation(station);
    }
    if (!suffix.empty()) {
        if (!Util::contains(value.getVariable(), '_')) {
            value.setVariable(value.getVariable() + suffix);
        }
    }
}

void LuaAcquisition::meta_index(Engine::Entry &entry,
                                const std::vector<Engine::Reference> &upvalues)
{
    if (entry.size() < 2) {
        entry.throwError("Invalid index");
        return;
    }
    if (methodIndexHandler(entry, Engine::Table(upvalues.front())))
        return;

    auto key = entry[1].toString();
    if (key == "state") {
        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            interface->serializeState(stream);
        }
        entry.clear();
        entry.push(std::string(data.constData(), static_cast<std::size_t>(data.size())));
        entry.propagate(1);
        return;
    } else if (key == "systemFlagsMetadata") {
        auto result = interface->getSystemFlagsMetadata();
        entry.clear();
        entry.pushData<Libs::Variant>(result.write());
        entry.propagate(1);
        return;
    } else if (key == "sourceMetadata") {
        auto result = interface->getSourceMetadata();
        entry.clear();
        entry.pushData<Libs::Variant>(result.write());
        entry.propagate(1);
        return;
    } else if (key == "status") {
        switch (interface->getGeneralStatus()) {
        case AcquisitionInterface::GeneralStatus::Normal:
            entry.clear();
            entry.pushNil();
            break;
        case AcquisitionInterface::GeneralStatus::NoCommunications:
            entry.clear();
            entry.push("nocomms");
            break;
        case AcquisitionInterface::GeneralStatus::Disabled:
            entry.clear();
            entry.push("disabled");
            break;
        }
        entry.propagate(1);
        return;
    } else if (key == "autoprobe") {
        switch (interface->getAutoprobeStatus()) {
        case AcquisitionInterface::AutoprobeStatus::InProgress:
            entry.clear();
            entry.pushNil();
            break;
        case AcquisitionInterface::AutoprobeStatus::Success:
            entry.clear();
            entry.push(true);
            break;
        case AcquisitionInterface::AutoprobeStatus::Failure:
            entry.clear();
            entry.push(false);
            break;
        }
        entry.propagate(1);
        return;
    } else if (key == "commands") {
        auto result = interface->getCommands();
        entry.clear();
        entry.pushData<Libs::Variant>(result.write());
        entry.propagate(1);
        return;
    } else if (key == "persistent") {
        auto result = interface->getPersistentValues();
        entry.clear();
        {
            auto table = entry.pushTable();
            Engine::Append append(entry, table, true);
            for (auto &add : result) {
                append.pushData<Libs::SequenceValue>(std::move(add));
            }
        }
        entry.propagate(1);
        return;
    }
}

void LuaAcquisition::meta_newindex(Engine::Entry &entry)
{
    if (entry.size() < 3) {
        entry.throwError("Invalid assignment");
        return;
    }

    auto self = entry[0];
    auto key = entry[1].toString();
    auto value = entry[2];
    if (key == "remap") {
        std::unique_lock<std::mutex> lock(mutex);
        remapEnabled = !value.isNil();
        auto mt = Engine::Table(self).pushMetatable(entry);
        mt.set("remap", value);
        notify.notify_all();
        CallbackProcessing processing(*this, entry, self, std::move(lock));
        return;
    } else if (key == "suffix") {
        if (!value.toBoolean()) {
            std::lock_guard<std::mutex> lock(mutex);
            suffix.clear();
        } else {
            std::lock_guard<std::mutex> lock(mutex);
            suffix = "_" + value.toString();
        }
        return;
    } else if (key == "station") {
        if (!value.toBoolean()) {
            std::lock_guard<std::mutex> lock(mutex);
            station.clear();
        } else {
            std::lock_guard<std::mutex> lock(mutex);
            station = value.toString();
        }
        return;
    } else if (key == "state") {
        auto str = value.toString();
        QByteArray data(str.data(), static_cast<int>(str.length()));
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            interface->deserializeState(stream);
        }
        return;
    } else if (key == "logging" || key == "data") {
        if (value.isNil()) {
            interface->setLoggingEgress(nullptr);
            loggingSink.clear();
        } else if (auto pipeline = value.toData<LuaPipeline>()) {
            std::unique_lock<std::mutex> lock(mutex);
            loggingSink.setPipeline(entry, *pipeline);
            interface->setLoggingEgress(&loggingSink);
            CallbackProcessing processing(*this, entry, self, std::move(lock));
        } else {
            std::unique_lock<std::mutex> lock(mutex);
            auto mt = Engine::Table(self).pushMetatable(entry);
            mt.set("logging", value);
            loggingSink.setCallback();
            interface->setLoggingEgress(&loggingSink);
            CallbackProcessing processing(*this, entry, self, std::move(lock));
        }
        return;
    } else if (key == "realtime") {
        if (value.isNil()) {
            interface->setRealtimeEgress(nullptr);
            realtimeSink.clear();
        } else if (auto pipeline = value.toData<LuaPipeline>()) {
            std::unique_lock<std::mutex> lock(mutex);
            realtimeSink.setPipeline(entry, *pipeline);
            interface->setRealtimeEgress(&realtimeSink);
            CallbackProcessing processing(*this, entry, self, std::move(lock));
        } else {
            std::unique_lock<std::mutex> lock(mutex);
            auto mt = Engine::Table(self).pushMetatable(entry);
            mt.set("realtime", value);
            realtimeSink.setCallback();
            interface->setRealtimeEgress(&realtimeSink);
            CallbackProcessing processing(*this, entry, self, std::move(lock));
        }
        return;
    } else if (key == "persistent") {
        if (value.isNil()) {
            interface->setPersistentEgress(nullptr);
            persistentSink.clear();
        } else if (auto pipeline = value.toData<LuaPipeline>()) {
            std::unique_lock<std::mutex> lock(mutex);
            persistentSink.setPipeline(entry, *pipeline, true);
            interface->setPersistentEgress(&persistentSink);
            CallbackProcessing processing(*this, entry, self, std::move(lock));
        } else {
            std::unique_lock<std::mutex> lock(mutex);
            auto mt = Engine::Table(self).pushMetatable(entry);
            mt.set("persistent", value);
            persistentSink.setCallback();
            interface->setPersistentEgress(&persistentSink);
            CallbackProcessing processing(*this, entry, self, std::move(lock));
        }
        return;
    } else {
        entry.throwError("Invalid assignment");
        return;
    }
}

void LuaAcquisition::method_advance(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Insufficient arguments");
        return;
    }
    double time = Libs::Time::extractSingle(entry, entry[1]);
    if (!FP::defined(time)) {
        entry.throwError("Invalid time");
        return;
    }

    if (FP::defined(advanceTime) && time < advanceTime) {
        entry.throwError("Backwards time advance");
        return;
    }
    advanceTime = time;

    interface->incomingAdvance(advanceTime);
}

void LuaAcquisition::method_command(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Insufficient arguments");
        return;
    }
    interface->incomingCommand(Libs::Variant::extract(entry, entry[1]));
}

void LuaAcquisition::method_incomingRealtime(Engine::Entry &entry)
{
    auto target = interface->getRealtimeIngress();
    if (!target)
        return;
    for (std::size_t i = 0, max = entry.size(); i < max; i++) {
        auto ref = entry[i];
        if (ref.isNil())
            continue;
        target->incomingData(Libs::SequenceValue::extract(entry, entry[i]));
    }
}

void LuaAcquisition::method_initializeState(Engine::Entry &entry)
{ interface->initializeState(); }

void LuaAcquisition::method_finish(Engine::Entry &entry)
{
    qCDebug(log_luaexec_acquisition) << "Completing acquisition";

    {
        Lua::Engine::Frame local(entry);
        local.push(local.registry(), listRegistryName);
        Lua::Engine::Table(local.back()).erase(selfIndex);
        selfIndex = 0;
    }
    {
        CallbackProcessing processing(*this, entry, entry[0]);
        processing.release();
        interface->initiateShutdown();
        interface->wait();
    }
    completeOutputs(entry);
}

void LuaAcquisition::method_incoming_data(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Insufficient arguments");
        return;
    }
    auto data = entry[1].toString();
    double time = FP::undefined();
    AcquisitionInterface::IncomingDataType type = AcquisitionInterface::IncomingDataType::Complete;
    if (entry.size() > 2) {
        if (entry[2].getType() == Engine::Reference::Type::Number) {
            time = Libs::Time::extractSingle(entry, entry[2]);
        } else {
            Engine::Table table(entry[2]);
            entry.push(table, "time");
            time = Libs::Time::extractSingle(entry, entry.back());
            auto check = table.get("type").toString();
            if (Util::equal_insensitive(check, "stream")) {
                type = AcquisitionInterface::IncomingDataType::Stream;
            } else if (Util::equal_insensitive(check, "final")) {
                type = AcquisitionInterface::IncomingDataType::Final;
            } else {
                type = AcquisitionInterface::IncomingDataType::Complete;
            }
        }
    }

    if (FP::defined(time)) {
        if (FP::defined(advanceTime) && time < advanceTime) {
            entry.throwError("Backwards time advance");
            return;
        }
        advanceTime = time;
    }

    CallbackProcessing processing(*this, entry, entry[0]);
    processing.release();
    interface->incomingData(QByteArray(data.data(), static_cast<int>(data.size())), time, type);
}

void LuaAcquisition::method_incoming_control(Engine::Entry &entry)
{
    if (entry.size() < 2) {
        entry.throwError("Insufficient arguments");
        return;
    }
    auto data = entry[1].toString();
    double time = FP::undefined();
    AcquisitionInterface::IncomingDataType type = AcquisitionInterface::IncomingDataType::Complete;
    if (entry.size() > 2) {
        if (entry[2].getType() == Engine::Reference::Type::Number) {
            time = Libs::Time::extractSingle(entry, entry[2]);
        } else {
            Engine::Table table(entry[2]);
            entry.push(table, "time");
            time = Libs::Time::extractSingle(entry, entry.back());
            auto check = table.get("type").toString();
            if (Util::equal_insensitive(check, "stream")) {
                type = AcquisitionInterface::IncomingDataType::Stream;
            } else if (Util::equal_insensitive(check, "final")) {
                type = AcquisitionInterface::IncomingDataType::Final;
            } else {
                type = AcquisitionInterface::IncomingDataType::Complete;
            }
        }
    }

    if (FP::defined(time)) {
        if (FP::defined(advanceTime) && time < advanceTime) {
            entry.throwError("Backwards time advance");
            return;
        }
        advanceTime = time;
    }

    CallbackProcessing processing(*this, entry, entry[0]);
    processing.release();
    interface->incomingControl(QByteArray(data.data(), static_cast<int>(data.size())), time, type);
}


void LuaAcquisition::CallbackProcessing::begin()
{
    Q_ASSERT(lock);
    if (parent.activeCallbackProcessing) {
        frame.throwError("Acquisition callback reentry is not supported");
        return;
    }
    parent.activeCallbackProcessing = this;

    if (parent.state.isWaiting()) {
        lock.unlock();
        parent.notify.notify_all();
    }
}

LuaAcquisition::CallbackProcessing::CallbackProcessing(LuaAcquisition &parent,
                                                       Engine::Frame &frame,
                                                       Engine::Reference self,
                                                       std::unique_lock<std::mutex> &&lock)
        : parent(parent), frame(frame), self(std::move(self)), lock(std::move(lock)), held(false)
{ begin(); }

LuaAcquisition::CallbackProcessing::CallbackProcessing(LuaAcquisition &parent,
                                                       Engine::Frame &frame,
                                                       Engine::Reference self) : parent(parent),
                                                                                 frame(frame),
                                                                                 self(std::move(
                                                                                         self)),
                                                                                 lock(parent.mutex),
                                                                                 held(false)
{ begin(); }

LuaAcquisition::CallbackProcessing::~CallbackProcessing()
{
    if (!lock)
        lock.lock();

    parent.notify.wait(lock, [this] { return !held; });

    if (parent.activeCallbackProcessing != this)
        return;

    parent.loggingSink.processQueued(frame, self, lock);
    parent.realtimeSink.processQueued(frame, self, lock);
    parent.persistentSink.processQueued(frame, self, lock);

    Q_ASSERT(lock);

    parent.activeCallbackProcessing = nullptr;
}

void LuaAcquisition::CallbackProcessing::release()
{
    if (lock)
        lock.unlock();
}

void LuaAcquisition::CallbackProcessing::remap(std::unique_lock<std::mutex> &lock,
                                               const SequenceName::Component &name,
                                               CPD3::Data::Variant::Root &value)
{
    Q_ASSERT(lock);

    if (!parent.remapEnabled)
        return;
    Engine::Frame local(frame);
    auto mt = Engine::Table(self).pushMetatable(local);
    local.push(mt, "remap");
    auto remap = local.back();
    if (remap.isNil())
        return;
    held = true;
    lock.unlock();

    Engine::Call call(local, remap);
    call.push(name);
    call.pushData<Libs::Variant>(value);
    if (!call.execute()) {
        call.clearError();
    }

    lock.lock();
    held = false;
    parent.notify.notify_all();
}

LuaAcquisition::State::State(LuaAcquisition &parent) : parent(parent), remapWaiting(false)
{ }

LuaAcquisition::State::~State() = default;

void LuaAcquisition::State::remap(const SequenceName::Component &name,
                                  CPD3::Data::Variant::Root &value)
{
    std::unique_lock<std::mutex> lock(parent.mutex);
    for (;;) {
        if (!parent.remapEnabled)
            return;
        if (parent.activeCallbackProcessing) {
            parent.activeCallbackProcessing->remap(lock, name, value);
            return;
        }
        remapWaiting = true;
        parent.notify.wait(lock);
        remapWaiting = false;
    }
}

void LuaAcquisition::State::setBypassFlag(const CPD3::Data::Variant::Flag &)
{ }

void LuaAcquisition::State::clearBypassFlag(const CPD3::Data::Variant::Flag &)
{ }

void LuaAcquisition::State::setSystemFlag(const CPD3::Data::Variant::Flag &)
{ }

void LuaAcquisition::State::clearSystemFlag(const CPD3::Data::Variant::Flag &)
{ }

void LuaAcquisition::State::requestFlush(double)
{ }

void LuaAcquisition::State::sendCommand(const std::string &, const CPD3::Data::Variant::Read &)
{ }

void LuaAcquisition::State::setSystemFlavors(const SequenceName::Flavors &)
{ }

void LuaAcquisition::State::setAveragingTime(Time::LogicalTimeUnit, int, bool)
{ }

void LuaAcquisition::State::requestStateSave()
{ }

void LuaAcquisition::State::requestGlobalStateSave()
{ }

void LuaAcquisition::State::event(double, const QString &, bool, const CPD3::Data::Variant::Read &)
{ }

LuaAcquisition::DataSink::DataSink(LuaAcquisition &parent, std::string callback) : parent(parent),
                                                                                   callback(
                                                                                           std::move(
                                                                                                   callback))
{ }

LuaAcquisition::DataSink::~DataSink() = default;

void LuaAcquisition::DataSink::incomingData(const CPD3::Data::SequenceValue::Transfer &values)
{
    std::unique_lock<std::mutex> lock(parent.mutex);
    CPD3::Data::SequenceValue::Transfer modify = values;
    for (auto &m : modify) {
        parent.modifyValue(m);
    }
    if (direct)
        return direct->incomingData(std::move(modify));
    Util::append(std::move(modify), queued);
}

void LuaAcquisition::DataSink::incomingData(CPD3::Data::SequenceValue::Transfer &&values)
{
    std::unique_lock<std::mutex> lock(parent.mutex);
    for (auto &m : values) {
        parent.modifyValue(m);
    }
    if (direct)
        return direct->incomingData(std::move(values));
    Util::append(std::move(values), queued);
}

void LuaAcquisition::DataSink::incomingData(const CPD3::Data::SequenceValue &value)
{
    std::unique_lock<std::mutex> lock(parent.mutex);
    CPD3::Data::SequenceValue m = value;
    parent.modifyValue(m);
    if (direct)
        return direct->incomingData(std::move(m));
    queued.emplace_back(std::move(m));
}

void LuaAcquisition::DataSink::incomingData(CPD3::Data::SequenceValue &&value)
{
    std::unique_lock<std::mutex> lock(parent.mutex);
    parent.modifyValue(value);
    if (direct)
        return direct->incomingData(std::move(value));
    queued.emplace_back(std::move(value));
}

void LuaAcquisition::DataSink::endData()
{
    std::lock_guard<std::mutex> lock(parent.mutex);
    direct.reset();
}

void LuaAcquisition::DataSink::clear()
{
    std::lock_guard<std::mutex> lock(parent.mutex);
    direct.reset();
    queued.clear();
}

void LuaAcquisition::DataSink::setPipeline(Lua::Engine::Frame &frame,
                                           LuaPipeline &pipeline,
                                           bool unsorted)
{
    direct = pipeline.multiplexerInput(frame, unsorted);
    if (direct) {
        direct->incomingData(std::move(queued));
        queued.clear();
    }
}

void LuaAcquisition::DataSink::setCallback()
{
    direct.reset();
}

void LuaAcquisition::DataSink::processQueued(Engine::Frame &frame,
                                             Engine::Reference &self,
                                             std::unique_lock<std::mutex> &lock)
{
    Q_ASSERT(lock);
    if (queued.empty())
        return;
    auto toProcess = std::move(queued);
    queued.clear();

    Engine::Frame local(frame);
    auto mt = Engine::Table(self).pushMetatable(local);
    local.push(mt, callback);
    auto callback = local.back();
    if (callback.isNil())
        return;
    lock.unlock();

    for (auto &v : toProcess) {
        Engine::Call call(local, callback);
        call.pushData<Libs::SequenceValue>(std::move(v));
        if (!call.execute()) {
            call.clearError();
        }
    }

    lock.lock();
}
