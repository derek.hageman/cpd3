/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3LUAEXEC_ACTION_HXX
#define CPD3LUAEXEC_ACTION_HXX

#include "core/first.hxx"

#include "luascript/engine.hxx"

class ExecThread;

namespace LuaAction {

void install(ExecThread *thread, CPD3::Lua::Engine::Frame &root);

}

#endif //CPD3LUAEXEC_ACTION_HXX
