/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#ifdef Q_OS_UNIX

#include <string.h>

#endif

#ifdef BUILD_GUI

#include <QGuiApplication>
#include <QApplication>
#include <QMessageBox>

#else
#include <QCoreApplication>
#endif

#include <QLoggingCategory>
#include <QTranslator>
#include <QLibraryInfo>
#include <QIcon>
#include <QObject>
#include <QStringList>
#include <QFile>

#include "core/abort.hxx"
#include "core/threadpool.hxx"
#include "core/memory.hxx"
#include "clicore/terminaloutput.hxx"
#include "clicore/graphics.hxx"

#ifdef BUILD_GUI

#include "guicore/guicore.hxx"

#endif

#include "execthread.hxx"


Q_LOGGING_CATEGORY(log_luaexec, "cpd3.luaexec", QtWarningMsg)


using namespace CPD3;
using namespace CPD3::CLI;


#if defined(ALLOCATOR_TYPE) && ALLOCATOR_TYPE == 1
#include <tbb/scalable_allocator.h>
static void allocatorRelease()
{
    ::free(nullptr);
#ifdef HAVE_TBB_COMMAND
    scalable_allocation_command(TBBMALLOC_CLEAN_ALL_BUFFERS, nullptr);
#endif
}
static void installAllocator()
{ CPD3::Memory::install_release_function(allocatorRelease); }
#elif defined(ALLOCATOR_TYPE) && ALLOCATOR_TYPE == 2
#include <gperftools/malloc_extension.h>
static void allocatorRelease()
{ MallocExtension::instance()->ReleaseFreeMemory(); }
static void installAllocator()
{ CPD3::Memory::install_release_function(allocatorRelease); }
#else
static void installAllocator()
{ }
#endif

int main(int argc, char **argv)
{
    installAllocator();

    bool guiMode;

#ifdef NO_CONSOLE
    guiMode = true;
#else
    guiMode = false;
#endif

#ifdef Q_OS_UNIX
    if (!guiMode) {
        if (argc > 0 && !::strncmp(argv[0], "da.gluaexec", 11))
            guiMode = true;
    }
#endif
    for (int i = 1; i < argc; i++) {
        if (!::strcmp(argv[i], "--"))
            break;
        if (!::strcmp(argv[i], "--gui")) {
            argc--;
            memmove(argv + i, argv + i + 1, (argc - i) * sizeof(char *));
            guiMode = true;
            break;
        }
        if (!::strcmp(argv[i], "--nogui")) {
            argc--;
            memmove(argv + i, argv + i + 1, (argc - i) * sizeof(char *));
            guiMode = false;
            break;
        }
    }

    std::unique_ptr<QCoreApplication> app;

#ifdef BUILD_GUI
    if (!guiMode) {
        app.reset(CLIGraphics::createApplication(argc, argv));
    } else {
        app.reset(new QApplication(argc, argv));
    }
    if (dynamic_cast<QApplication *>(QCoreApplication::instance())) {
        QApplication::setWindowIcon(QIcon(":/icon.svg"));
        QApplication::setQuitOnLastWindowClosed(false);
    }
#else
    app.reset(new QCoreApplication(argc, argv));
#endif

    Q_INIT_RESOURCE(luaexec);

    QTranslator qtTranslator;
    qtTranslator.load("qt_" + QLocale::system().name(),
                      QLibraryInfo::location(QLibraryInfo::TranslationsPath));
    app->installTranslator(&qtTranslator);

    QString translateLocation;
    QByteArray cpd3(qgetenv("CPD3"));
    if (cpd3.length() > 0) {
        translateLocation = QString(cpd3) + "/locale";
    }
    QTranslator cpd3Translator;
    cpd3Translator.load("cpd3_" + QLocale::system().name(), translateLocation);
    app->installTranslator(&cpd3Translator);
    QTranslator cliTranslator;
    cliTranslator.load("cli_" + QLocale::system().name(), translateLocation);
    app->installTranslator(&cliTranslator);
    QTranslator guiTranslator;
    guiTranslator.load("gui_" + QLocale::system().name(), translateLocation);
    app->installTranslator(&guiTranslator);
    QTranslator scriptTranslator;
    scriptTranslator.load("script_" + QLocale::system().name(), translateLocation);
    app->installTranslator(&scriptTranslator);


    auto arguments = QCoreApplication::arguments();
    if (arguments.size() < 2) {
#ifndef NO_CONSOLE
        if (!guiMode) {
            TerminalOutput::simpleWordWrap(QObject::tr("Usage: da.luaexec <program>"), true);
            return 1;
        }
#endif
#ifdef BUILD_GUI
        QMessageBox::critical(NULL, QObject::tr("Error in Command Line"),
                              QObject::tr("Usage: da.gluaexec <program>"));
#endif
        QCoreApplication::exit(1);
        return 1;
    }
    arguments.removeFirst();
    auto target = arguments.takeFirst().toStdString();


    if (!guiMode)
        Abort::installAbortHandler(false);

    std::unique_ptr<ExecThread>
            exec(new ExecThread(std::move(arguments), guiMode, std::move(target)));

    std::unique_ptr<AbortPoller> abortPoller(new AbortPoller);
    QObject::connect(abortPoller.get(), &AbortPoller::aborted, app.get(), &QCoreApplication::quit);
    QObject::connect(abortPoller.get(), &AbortPoller::aborted, exec.get(),
                     &ExecThread::signalTerminate, Qt::DirectConnection);

    exec->start();
    if (!guiMode)
        abortPoller->start();
    int rc = app->exec();

    abortPoller.reset();
    exec.reset();

    ThreadPool::system()->wait();
    return rc;
}
