/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3LUAEXEC_ACQUISITION_HXX
#define CPD3LUAEXEC_ACQUISITION_HXX

#include "core/first.hxx"

#include <memory>
#include <mutex>
#include <condition_variable>

#include "core/threading.hxx"
#include "luascript/engine.hxx"
#include "datacore/stream.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "pipeline.hxx"

class ExecThread;

class LuaAcquisition : public CPD3::Lua::Engine::Data, public CPD3::Threading::Receiver {
    std::unique_ptr<CPD3::Acquisition::AcquisitionInterface> interface;
    std::size_t selfIndex;

    double advanceTime;

    bool remapEnabled;
    CPD3::Data::SequenceName::Component station;
    CPD3::Data::SequenceName::Component suffix;
    std::mutex mutex;
    std::condition_variable notify;

    void meta_index(CPD3::Lua::Engine::Entry &entry,
                    const std::vector<CPD3::Lua::Engine::Reference> &upvalues);

    void meta_newindex(CPD3::Lua::Engine::Entry &entry);

    void method_advance(CPD3::Lua::Engine::Entry &entry);

    void method_command(CPD3::Lua::Engine::Entry &entry);

    void method_incomingRealtime(CPD3::Lua::Engine::Entry &entry);

    void method_initializeState(CPD3::Lua::Engine::Entry &entry);

    void method_finish(CPD3::Lua::Engine::Entry &entry);

    void method_incoming_data(CPD3::Lua::Engine::Entry &entry);

    void method_incoming_control(CPD3::Lua::Engine::Entry &entry);

    void finalize(CPD3::Lua::Engine::Frame &frame);

    void signalTerminate();

    class CallbackProcessing final {
        LuaAcquisition &parent;
        CPD3::Lua::Engine::Frame &frame;
        CPD3::Lua::Engine::Reference self;
        std::unique_lock<std::mutex> lock;
        bool held;

        void begin();

    public:
        CallbackProcessing() = delete;

        CallbackProcessing(LuaAcquisition &parent,
                           CPD3::Lua::Engine::Frame &frame,
                           CPD3::Lua::Engine::Reference self,
                           std::unique_lock<std::mutex> &&lock);

        CallbackProcessing(LuaAcquisition &parent,
                           CPD3::Lua::Engine::Frame &frame,
                           CPD3::Lua::Engine::Reference self);

        ~CallbackProcessing();

        void release();

        void remap(std::unique_lock<std::mutex> &lock,
                   const CPD3::Data::SequenceName::Component &name,
                   CPD3::Data::Variant::Root &value);
    };

    friend class CallbackProcessing;

    CallbackProcessing *activeCallbackProcessing;

    class State final : public CPD3::Acquisition::AcquisitionState {
        LuaAcquisition &parent;
        bool remapWaiting;
    public:
        State(LuaAcquisition &parent);

        virtual ~State();

        void remap(const CPD3::Data::SequenceName::Component &name,
                   CPD3::Data::Variant::Root &value) override;

        void setBypassFlag(const CPD3::Data::Variant::Flag &flag = "Bypass") override;

        void clearBypassFlag(const CPD3::Data::Variant::Flag &flag = "Bypass") override;

        void setSystemFlag(const CPD3::Data::Variant::Flag &flag = "Contaminated") override;

        void clearSystemFlag(const CPD3::Data::Variant::Flag &flag = "Contaminated") override;

        void requestFlush(double seconds = -1.0) override;

        void sendCommand(const std::string &target,
                         const CPD3::Data::Variant::Read &command) override;

        void setSystemFlavors(const CPD3::Data::SequenceName::Flavors &flavors) override;


        void setAveragingTime(CPD3::Time::LogicalTimeUnit unit,
                              int count,
                              bool setAligned = true) override;

        void requestStateSave() override;

        void requestGlobalStateSave() override;

        void event(double time,
                   const QString &text,
                   bool showRealtime = true,
                   const CPD3::Data::Variant::Read &information = CPD3::Data::Variant::Read::empty()) override;

        inline bool isWaiting() const
        { return remapWaiting; }
    };

    friend class state;

    State state;

    class DataSink : public CPD3::Data::StreamSink {
        LuaAcquisition &parent;
        std::string callback;
        std::unique_ptr<LuaPipeline::MultiplexerInput> direct;
        CPD3::Data::SequenceValue::Transfer queued;
    public:
        DataSink(LuaAcquisition &parent, std::string callback);

        virtual ~DataSink();

        void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

        void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

        void incomingData(const CPD3::Data::SequenceValue &value) override;

        void incomingData(CPD3::Data::SequenceValue &&value) override;

        void endData() override;

        void clear();

        void setPipeline(CPD3::Lua::Engine::Frame &frame,
                         LuaPipeline &pipeline,
                         bool unsorted = false);

        void setCallback();

        void processQueued(CPD3::Lua::Engine::Frame &frame,
                           CPD3::Lua::Engine::Reference &self,
                           std::unique_lock<std::mutex> &lock);
    };

    void modifyValue(CPD3::Data::SequenceValue &value);

    friend class DataSink;

    DataSink loggingSink;
    DataSink realtimeSink;
    DataSink persistentSink;

    void completeOutputs(CPD3::Lua::Engine::Frame &frame);

public:
    LuaAcquisition(ExecThread *thread,
                   std::unique_ptr<CPD3::Acquisition::AcquisitionInterface> &&interface);

    virtual ~LuaAcquisition();

    static void install(ExecThread *thread, CPD3::Lua::Engine::Frame &root);


protected:
    virtual void initialize(const CPD3::Lua::Engine::Reference &self,
                            CPD3::Lua::Engine::Frame &frame);
};

#endif //CPD3LUAEXEC_ACQUISITION_HXX
