/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <atomic>
#include <QLoggingCategory>

#include "luascript/libs/streamvalue.hxx"
#include "luascript/libs/sequencesegment.hxx"
#include "luascript/libs/sequencename.hxx"
#include "core/threadpool.hxx"

#include "processor.hxx"

Q_DECLARE_LOGGING_CATEGORY(log_luaexec)

Q_DECLARE_LOGGING_CATEGORY(log_luaexec_pipeline)

using namespace CPD3;
using namespace CPD3::Data;

static const std::string listRegistryName = "CPD3_luaexecprocessor_list";
static std::atomic_uint_fast32_t listInsertIndex(1);

void ProcessorBuffered::install(ExecThread *, Lua::Engine::Frame &root)
{
    Lua::Engine::Frame frame(root);

    {
        Lua::Engine::Assign assign(frame, frame.registry(), listRegistryName);
        assign.pushTable();
    }
}

ProcessorBuffered::ProcessorBuffered(ExecThread *thread,
                                     Lua::Engine::Frame &target,
                                     const Lua::Engine::Reference &call,
                                     Lua::StreamBuffer *buffer) : buffer(buffer),
                                                                  processor(std::make_shared<
                                                                          Processor>(thread,
                                                                                     *this)),
                                                                  state(State::Initialize),
                                                                  controllerIndex(0),
                                                                  egress(nullptr)
{
    buffer->pushExternalController(target);
    auto controller = target.back();
    {
        Lua::Engine::Frame frame(target);
        frame.push(frame.registry(), listRegistryName);
        controllerIndex = listInsertIndex.fetch_add(1);
        Lua::Engine::Assign assign(frame, frame.back(), controllerIndex);
        auto ctable = assign.pushTable();
        {
            Lua::Engine::Assign sa(assign, ctable, "controller");
            sa.push(controller);
        }
        {
            Lua::Engine::Assign sa(assign, ctable, "call");
            sa.push(call);
        }
    }

    processor->connect();

    qCDebug(log_luaexec_pipeline) << "Processor" << controllerIndex << "created";

    thread->finalizeBuffers
          .connect(*this, std::bind(&ProcessorBuffered::finalize, this, std::placeholders::_1));
    thread->terminateRequested.connect(*this, std::bind(&ProcessorBuffered::signalTerminate, this));
}

ProcessorBuffered::~ProcessorBuffered()
{
    Threading::Receiver::disconnect();
    {
        std::lock_guard<std::mutex> lock(processor->mutex);
        state = State::Terminate;
    }
    processor->notify.notify_all();
}

void ProcessorBuffered::processingCompleted()
{ finished(); }

void ProcessorBuffered::finalize(Lua::Engine::Frame &frame)
{
    qCDebug(log_luaexec_pipeline) << "Processor" << controllerIndex << "starting finalization";

    Lua::Engine::Frame local(frame);
    local.push(local.registry(), listRegistryName);
    Lua::Engine::Table reg(local.back());
    local.push(reg, controllerIndex);
    reg.erase(controllerIndex);
    auto data = local.back();
    local.push(data, "controller");
    auto controller = local.back();

    std::unique_lock<std::mutex> egressLocker(egressLock);
    if (!egress) {
        egressLocker.unlock();
        std::unique_lock<std::mutex> lock(processor->mutex);
        egressLocker.lock();
        for (;;) {
            if (egress)
                break;
            if (state == State::Terminate)
                return;
            egressLocker.unlock();
            processor->notify.wait(lock, [this] { return egress || state == State::Terminate; });
            egressLocker.lock();
        }
    }

    buffer->finish(local, controller);
    egress = nullptr;

    egressLocker.unlock();
    {
        std::lock_guard<std::mutex> lock(processor->mutex);
        state = State::Finished;
    }
    processingCompleted();
    processor->notify.notify_all();

    qCDebug(log_luaexec_pipeline) << "Processor" << controllerIndex << "finalized";
}

void ProcessorBuffered::start()
{
    qCDebug(log_luaexec_pipeline) << "Processor" << controllerIndex << "started";

    {
        std::lock_guard<std::mutex> lock(processor->mutex);
        if (state != State::Initialize)
            return;
        state = State::Active;
    }
    processor->notify.notify_all();
}

bool ProcessorBuffered::isFinished()
{
    std::lock_guard<std::mutex> lock(processor->mutex);
    switch (state) {
    case State::Initialize:
    case State::Active:
    case State::Ended:
        return false;
    case State::Finished:
    case State::Terminate:
        return true;
    }
    Q_ASSERT(false);
    return true;
}

bool ProcessorBuffered::wait(double timeout)
{
    return Threading::waitForTimeout(timeout, processor->mutex, processor->notify, [this] {
        switch (state) {
        case State::Finished:
        case State::Terminate:
            return true;
        case State::Active:
        case State::Ended:
        case State::Initialize:
            return false;
        }
        Q_ASSERT(false);
        return true;
    });
}

void ProcessorBuffered::signalTerminate()
{
    qCDebug(log_luaexec_pipeline) << "Terminating processor";

    std::unique_lock<std::mutex> lock(processor->mutex);
    if (state == State::Terminate)
        return;
    state = State::Terminate;
    processingCompleted();
    processor->notify.notify_all();
    std::lock_guard<std::mutex> egressLocker(egressLock);
    if (!egress) {
        qCDebug(log_luaexec_pipeline) << "Termination discarding end";
        return;
    }
    lock.unlock();
    egress->endData();
    egress = nullptr;
}

void ProcessorBuffered::incomingData(const SequenceValue::Transfer &values)
{
    {
        std::unique_lock<std::mutex> lock(processor->mutex);
        stall(lock);
        if (state == State::Terminate)
            return;
        Q_ASSERT(state == State::Active);
        if (!processIncoming(values))
            return;
    }
    processor->notify.notify_all();
}

void ProcessorBuffered::incomingData(SequenceValue::Transfer &&values)
{
    {
        std::unique_lock<std::mutex> lock(processor->mutex);
        stall(lock);
        if (state == State::Terminate)
            return;
        Q_ASSERT(state == State::Active);
        if (!processIncoming(std::move(values)))
            return;
    }
    processor->notify.notify_all();
}

void ProcessorBuffered::incomingData(const SequenceValue &value)
{
    {
        std::unique_lock<std::mutex> lock(processor->mutex);
        stall(lock);
        if (state == State::Terminate)
            return;
        Q_ASSERT(state == State::Active);
        if (!processIncoming(value))
            return;
    }
    processor->notify.notify_all();
}

void ProcessorBuffered::incomingData(SequenceValue &&value)
{
    {
        std::unique_lock<std::mutex> lock(processor->mutex);
        stall(lock);
        if (state == State::Terminate)
            return;
        Q_ASSERT(state == State::Active);
        if (!processIncoming(std::move(value)))
            return;
    }
    processor->notify.notify_all();
}

void ProcessorBuffered::endData()
{
    qCDebug(log_luaexec_pipeline) << "Processor" << controllerIndex << "received end";

    {
        std::lock_guard<std::mutex> lock(processor->mutex);
        if (state == State::Terminate || state == State::Ended)
            return;
        Q_ASSERT(state == State::Active);
        processEnd();
        state = State::Ended;
    }
    processor->notify.notify_all();
}

void ProcessorBuffered::setEgress(StreamSink *egress)
{
    std::lock_guard<std::mutex> lock(processor->mutex);
    std::lock_guard<std::mutex> egressLocker(egressLock);
    this->egress = egress;
    processor->notify.notify_all();
}

void ProcessorBuffered::stall(std::unique_lock<std::mutex> &lock)
{
    while (stall()) {
        switch (state) {
        case State::Initialize:
        case State::Active:
            break;
        case State::Ended:
        case State::Finished:
        case State::Terminate:
            return;
        }
        processor->notify.wait(lock);
    }
}

void ProcessorBuffered::processEnd()
{ }

void ProcessorBuffered::processResult(Lua::Engine::Frame &, const Lua::Engine::Reference &)
{ }

std::unique_lock<std::mutex> ProcessorBuffered::acquireState()
{ return std::unique_lock<std::mutex>(processor->mutex); }

ProcessorBuffered::Processor::Processor(ExecThread *thread, ProcessorBuffered &parent)
        : ExecThread::Processor(thread), parent(parent)
{ }

ProcessorBuffered::Processor::~Processor() = default;

ExecThread::Processor::ExecutionState ProcessorBuffered::Processor::execute(CPD3::Lua::Engine::Frame &frame,
                                                                            std::unique_lock<
                                                                                    std::mutex> &lock)
{
    if (parent.state == State::Terminate || parent.state == State::Initialize) {
        parent.state = State::Terminate;
        notify.notify_all();
        parent.processingCompleted();
        std::unique_lock<std::mutex> egressLocker(parent.egressLock);
        if (parent.egress) {
            auto egress = parent.egress;
            parent.egress = nullptr;
            lock.unlock();
            egress->endData();
        }
        return ExecutionState::Complete;
    }

    std::unique_lock<std::mutex> egressLocker(parent.egressLock);
    if (!parent.egress)
        return ExecThread::Processor::ExecutionState::Waiting;
    if (parent.state == State::Active && !parent.nextReady())
        return ExecThread::Processor::ExecutionState::Waiting;

    {
        Lua::Engine::Frame local(frame);
        local.push(local.registry(), listRegistryName);
        Lua::Engine::Table reg(local.back());
        local.push(reg, parent.controllerIndex);
        auto data = local.back();
        Q_ASSERT(!data.isNil());
        local.push(data, "controller");
        auto controller = local.back();
        Q_ASSERT(!controller.isNil());

        bool wasStalled = parent.stall();
        if (!parent.buffer->pushExternal(local, controller, parent.state == State::Active)) {
            if (parent.state != State::Active) {
                lock.unlock();

                reg.erase(parent.controllerIndex);

                static_cast<Threading::Receiver &>(parent).disconnectImmediate();
                parent.buffer->finish(local, controller);
                parent.egress = nullptr;

                qCDebug(log_luaexec_pipeline) << "Processor" << parent.controllerIndex
                                              << "completed";

                egressLocker.unlock();
                lock.lock();
                parent.state = State::Finished;
                parent.processingCompleted();
                return ExecutionState::Complete;
            }
            return ExecutionState::Waiting;
        }
        auto value = local.back();
        lock.unlock();

        if (wasStalled)
            notify.notify_all();

        Lua::Engine::Call call(local);
        call.push(data, "call");
        call.push(value);

        if (!call.executeVariable()) {
            parent.egress->endData();
            parent.egress = nullptr;

            egressLocker.unlock();
            lock.lock();
            parent.state = State::Terminate;
            parent.processingCompleted();
            return ExecutionState::Complete;
        }
        parent.processResult(call, value);
    }

    return ExecutionState::DidUnlock;
}


ValueProcessor::Buffer::Buffer(ValueProcessor &parent) : parent(parent)
{ }

ValueProcessor::Buffer::~Buffer() = default;

bool ValueProcessor::Buffer::pushNext(Lua::Engine::Frame &target)
{
    if (parent.incoming.empty())
        return false;
    target.pushData<Lua::Libs::SequenceValue>(std::move(parent.incoming.front()));
    parent.incoming.pop_front();
    return true;
}

void ValueProcessor::Buffer::outputReady(Lua::Engine::Frame &frame,
                                         const Lua::Engine::Reference &ref)
{
    auto value = extract(frame, ref);
    auto egress = parent.acquireOutput();
    Q_ASSERT(egress);
    egress->incomingData(std::move(value));
}

void ValueProcessor::Buffer::endReady()
{
    auto egress = parent.acquireOutput();
    Q_ASSERT(egress);
    egress->endData();
}

ValueProcessor::ValueProcessor(ExecThread *thread,
                               Lua::Engine::Frame &target,
                               const Lua::Engine::Reference &call) : ProcessorBuffered(thread,
                                                                                       target, call,
                                                                                       new Buffer(
                                                                                               *this))
{ }

ValueProcessor::~ValueProcessor() = default;

bool ValueProcessor::processIncoming(const SequenceValue::Transfer &values)
{
    Util::append(values, incoming);
    return !incoming.empty();
}

bool ValueProcessor::processIncoming(SequenceValue::Transfer &&values)
{
    Util::append(std::move(values), incoming);
    return !incoming.empty();
}

bool ValueProcessor::processIncoming(const SequenceValue &value)
{
    incoming.emplace_back(value);
    return true;
}

bool ValueProcessor::processIncoming(SequenceValue &&value)
{
    incoming.emplace_back(std::move(value));
    return true;
}

bool ValueProcessor::stall() const
{ return incoming.size() > stallThreshold; }

bool ValueProcessor::nextReady() const
{ return !incoming.empty(); }


namespace {
class OutputSegment : public Lua::Libs::SequenceSegment {
    CPD3::Data::SequenceName::Set outputs;
    bool requireExists;
public:
    OutputSegment() : requireExists(true)
    { }

    virtual ~OutputSegment() = default;

    explicit OutputSegment(const CPD3::Data::SequenceSegment &segment) : Lua::Libs::SequenceSegment(
            segment), requireExists(false)
    { }

    explicit OutputSegment(CPD3::Data::SequenceSegment &&segment) : Lua::Libs::SequenceSegment(
            std::move(segment)), requireExists(false)
    { }

    OutputSegment(const CPD3::Data::SequenceSegment &segment,
                  const CPD3::Data::SequenceName::Set &outputs,
                  bool requireExists = true) : Lua::Libs::SequenceSegment(segment),
                                               outputs(std::move(outputs)),
                                               requireExists(requireExists)
    { }

    OutputSegment(CPD3::Data::SequenceSegment &&segment,
                  const CPD3::Data::SequenceName::Set &outputs,
                  bool requireExists = true) : Lua::Libs::SequenceSegment(std::move(segment)),
                                               outputs(std::move(outputs)),
                                               requireExists(requireExists)
    { }

    void setOutputs(CPD3::Data::SequenceName::Set out)
    {
        outputs = std::move(out);
        requireExists = false;
    }

    CPD3::Data::SequenceValue::Transfer extract(Lua::Engine::Frame &frame,
                                                const Lua::Engine::Reference &ref)
    { return Lua::StreamBuffer::Segment::extract(frame, ref, outputs, requireExists); }
};
}

SegmentProcessor::Buffer::Buffer(SegmentProcessor &parent) : parent(parent)
{ }

SegmentProcessor::Buffer::~Buffer() = default;

bool SegmentProcessor::Buffer::pushNext(Lua::Engine::Frame &target)
{
    if (parent.incoming.empty())
        return false;
    target.pushData<OutputSegment>(std::move(parent.incoming.front()), parent.outputs,
                                   parent.addInputsToOutputs);
    parent.incoming.pop_front();
    return true;
}

void SegmentProcessor::Buffer::convertFromLua(Lua::Engine::Frame &frame)
{
    if (frame.back().toData<OutputSegment>())
        return;
    auto value = Lua::Libs::SequenceSegment::extract(frame, frame.back());
    frame.pop();
    auto lock = parent.acquireState();
    frame.pushData<OutputSegment>(std::move(value), parent.outputs, parent.addInputsToOutputs);
}

void SegmentProcessor::Buffer::outputReady(Lua::Engine::Frame &frame,
                                           const Lua::Engine::Reference &ref)
{
    auto value = ref.toData<OutputSegment>();
    if (!value)
        return;
    auto egress = parent.acquireOutput();
    Q_ASSERT(egress);
    egress->incomingData(value->extract(frame, ref));
}

void SegmentProcessor::Buffer::endReady()
{
    auto egress = parent.acquireOutput();
    Q_ASSERT(egress);
    egress->endData();
}

bool SegmentProcessor::Buffer::bufferAssigned(Lua::Engine::Frame &frame,
                                              const Lua::Engine::Reference &target,
                                              const Lua::Engine::Reference &value)
{
    auto output = target.toData<OutputSegment>();
    if (!output)
        return false;
    if (value.getType() == Lua::Engine::Reference::Type::Table) {
        CPD3::Data::SequenceName::Set result;
        Lua::Engine::Iterator it(frame, value);
        bool allNumeric = true;
        while (it.next()) {
            if (it.key().getType() != Lua::Engine::Reference::Type::Number) {
                allNumeric = false;
                break;
            }
            auto name = Lua::Libs::SequenceName::extract(frame, value);
            if (!name.isValid()) {
                allNumeric = false;
                break;
            }
            result.insert(std::move(name));
        }
        if (allNumeric) {
            output->setOutputs(std::move(result));
            return true;
        }
    }
    auto name = Lua::Libs::SequenceName::extract(frame, value);
    if (!name.isValid())
        return false;
    output->setOutputs({std::move(name)});
    return true;
}

SegmentProcessor::SegmentProcessor(ExecThread *thread,
                                   Lua::Engine::Frame &target,
                                   const Lua::Engine::Reference &call) : ProcessorBuffered(thread,
                                                                                           target,
                                                                                           call,
                                                                                           new Buffer(
                                                                                                   *this))
{ }

SegmentProcessor::~SegmentProcessor() = default;

bool SegmentProcessor::processIncoming(const SequenceValue::Transfer &values)
{
    for (const auto &v : values) {
        if (addInputsToOutputs)
            outputs.insert(v.getName());
        Util::append(reader.add(v), incoming);
    }
    return !incoming.empty();
}

bool SegmentProcessor::processIncoming(SequenceValue::Transfer &&values)
{
    for (auto &v : values) {
        if (addInputsToOutputs)
            outputs.insert(v.getName());
        Util::append(reader.add(std::move(v)), incoming);
    }
    return !incoming.empty();
}

bool SegmentProcessor::processIncoming(const SequenceValue &value)
{
    if (addInputsToOutputs)
        outputs.insert(value.getName());
    Util::append(reader.add(value), incoming);
    return !incoming.empty();
}

bool SegmentProcessor::processIncoming(SequenceValue &&value)
{
    if (addInputsToOutputs)
        outputs.insert(value.getName());
    Util::append(reader.add(std::move(value)), incoming);
    return !incoming.empty();
}

void SegmentProcessor::processEnd()
{
    Util::append(reader.finish(), incoming);
}

void SegmentProcessor::processResult(Lua::Engine::Frame &frame, const Lua::Engine::Reference &value)
{
    if (frame.empty())
        return;
    auto output = value.toData<OutputSegment>();
    if (!output)
        return;

    CPD3::Data::SequenceName::Set result;
    for (std::size_t i = 0, max = frame.size(); i < max; i++) {
        auto name = Lua::Libs::SequenceName::extract(frame, value);
        if (!name.isValid())
            continue;
        result.insert(std::move(name));
    }
    output->setOutputs(std::move(result));
}

bool SegmentProcessor::stall() const
{ return incoming.size() > stallThreshold; }

bool SegmentProcessor::nextReady() const
{ return !incoming.empty(); }

void SegmentProcessor::setExplicitOutputs(SequenceName::Set outputs)
{
    addInputsToOutputs = false;
    this->outputs = std::move(outputs);
}
