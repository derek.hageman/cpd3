/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QDir>
#include <QFile>
#include <QTemporaryFile>

#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "core/waitutils.hxx"
#include "core/actioncomponent.hxx"
#include "core/number.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"
#include "database/connection.hxx"
#include "database/storage.hxx"
#include "database/tabledefinition.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class TestComponent : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

    ActionComponentTime *component;
private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component = qobject_cast<ActionComponentTime *>(ComponentLoader::create("output_sqldb"));
        QVERIFY(component);

        QVERIFY(databaseFile.open());
        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("profile")));
    }

    void basic()
    {
        QTemporaryFile outputDBFile;
        QVERIFY(outputDBFile.open());

        {
            Database::Connection
                    c(Database::Storage::sqlite(outputDBFile.fileName().toStdString()));
            QVERIFY(c.start());
            c.synchronousTransaction([&] {
                Database::TableDefinition t1("Table_bnd");

                t1.integer<std::uint64_t>("epoch_time", false);
                t1.primaryKey("epoch_time");
                t1.real("N");
                t1.real("BsG");
                t1.real("WDg");
                c.createTable(t1);

                Database::TableDefinition t2("Table_check");

                t2.integer<std::uint16_t>("year", false);
                t2.real("doy", false);
                t2.primaryKey(std::vector<std::string>{"year", "doy"});
                t2.real("N");
                t2.real("BsG");
                t2.real("WDg");
                c.createTable(t2);

                return Database::Connection::Synchronous_Ok;
            });
        }

        Variant::Root cfg;

        cfg["Profiles/aerosol/Table1/Database/Driver"] = "QSQLITE";
        cfg["Profiles/aerosol/Table1/Database/Name"] = outputDBFile.fileName();
        cfg["Profiles/aerosol/Table1/Database/Table"] = "Table_${STATION}";
        cfg["Profiles/aerosol/Table1/Purge/Type"] = "Epoch";
        cfg["Profiles/aerosol/Table1/Purge/Epoch"] = "epoch_time";
        cfg["Profiles/aerosol/Table1/Columns/epoch_time/Type"] = "Epoch";
        cfg["Profiles/aerosol/Table1/Columns/N/Type"] = "Value";
        cfg["Profiles/aerosol/Table1/Columns/N/Value/Input/Archive"] = "avgh";
        cfg["Profiles/aerosol/Table1/Columns/N/Value/Input/Variable"] = "N_N61";
        cfg["Profiles/aerosol/Table1/Columns/BsG/Type"] = "Value";
        cfg["Profiles/aerosol/Table1/Columns/BsG/Value/Input/Archive"] = "avgh";
        cfg["Profiles/aerosol/Table1/Columns/BsG/Value/Input/Variable"] = "BsG_S11";
        cfg["Profiles/aerosol/Table1/Columns/WDg/Type"] = "General";
        cfg["Profiles/aerosol/Table1/Columns/WDg/Value/Archive"] = "avgh";
        cfg["Profiles/aerosol/Table1/Columns/WDg/Value/Variable"] = "WD_XM1";
        cfg["Profiles/aerosol/Table1/Columns/WDg/Value/Flavors/#0"] = "stats";
        cfg["Profiles/aerosol/Table1/Columns/WDg/Path"] = "StabilityFactor";

        cfg["Profiles/aerosol/Table2/Database/Driver"] = "QSQLITE";
        cfg["Profiles/aerosol/Table2/Database/Name"] = outputDBFile.fileName();
        cfg["Profiles/aerosol/Table2/Database/Table"] = "Table_check";
        cfg["Profiles/aerosol/Table2/Purge/Type"] = "YearDOY";
        cfg["Profiles/aerosol/Table2/Purge/Year"] = "year";
        cfg["Profiles/aerosol/Table2/Purge/DOY"] = "doy";
        cfg["Profiles/aerosol/Table2/Columns/year/Type"] = "Year";
        cfg["Profiles/aerosol/Table2/Columns/doy/Type"] = "DOY";
        cfg["Profiles/aerosol/Table2/Columns/N/Type"] = "Value";
        cfg["Profiles/aerosol/Table2/Columns/N/Value/Input/Archive"] = "avgh";
        cfg["Profiles/aerosol/Table2/Columns/N/Value/Input/Variable"] = "N_N61";

        Variant::Root windStats;
        windStats.write().hash("StabilityFactor").setDouble(0.75);


        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"bnd", "configuration", "sqldb"}, cfg, FP::undefined(),
                              FP::undefined()),
                SequenceValue({"bnd", "avgh", "WD_XM1", {"stats"}}, windStats, 5000, 8000),
                SequenceValue({"bnd", "avgh", "N_N61"}, Variant::Root(1.0), 5000, 6000),
                SequenceValue({"bnd", "avgh", "BsG_S11"}, Variant::Root(2.0), 5000, 6000),
                SequenceValue({"bnd", "avgh", "BsG_S11"}, Variant::Root(3.0), 6000, 7000),
                SequenceValue({"bnd", "avgh", "N_N61"}, Variant::Root(4.0), 6000, 7000),
                SequenceValue({"bnd", "avgh", "BsG_S11"}, Variant::Root(5.0), 7000, 8000),
                SequenceValue({"bnd", "avgh", "N_N61"}, Variant::Root(6.0), 7000, 8000)});


        ComponentOptions options(component->getOptions());
        CPD3Action *action = component->createTimeAction(options, 1000, 9000, {"bnd"});
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait(30000));
        delete action;


        {
            Database::Connection
                    c(Database::Storage::sqlite(outputDBFile.fileName().toStdString()));
            QVERIFY(c.start());
            auto tx = c.transaction(Database::Connection::Transaction_ReadOnly);

            auto results =
                    c.select("Table_bnd", {"epoch_time", "N", "BsG", "WDg"}, "", "epoch_time ASC")
                     .execute()
                     .all();
            QCOMPARE(results.size(), static_cast<std::size_t>(3));

            QCOMPARE(results[0].size(), static_cast<std::size_t>(4));
            QCOMPARE(results[0][0].toReal(), 5000.0);
            QCOMPARE(results[0][1].toReal(), 1.0);
            QCOMPARE(results[0][2].toReal(), 2.0);
            QCOMPARE(results[0][3].toReal(), 0.75);

            QCOMPARE(results[1].size(), static_cast<std::size_t>(4));
            QCOMPARE(results[1][0].toReal(), 6000.0);
            QCOMPARE(results[1][1].toReal(), 4.0);
            QCOMPARE(results[1][2].toReal(), 3.0);
            QCOMPARE(results[1][3].toReal(), 0.75);

            QCOMPARE(results[2].size(), static_cast<std::size_t>(4));
            QCOMPARE(results[2][0].toReal(), 7000.0);
            QCOMPARE(results[2][1].toReal(), 6.0);
            QCOMPARE(results[2][2].toReal(), 5.0);
            QCOMPARE(results[2][3].toReal(), 0.75);
        }

        options = component->getOptions();
        action = component->createTimeAction(options, 1000, 9000, {"bnd"});
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait(30000));
        delete action;

        {
            Database::Connection
                    c(Database::Storage::sqlite(outputDBFile.fileName().toStdString()));
            QVERIFY(c.start());
            auto tx = c.transaction(Database::Connection::Transaction_ReadOnly);

            auto results =
                    c.select("Table_bnd", {"epoch_time", "N", "BsG", "WDg"}, "", "epoch_time ASC")
                     .execute()
                     .all();
            QCOMPARE(results.size(), static_cast<std::size_t>(3));

            QCOMPARE(results[0].size(), static_cast<std::size_t>(4));
            QCOMPARE(results[0][0].toReal(), 5000.0);
            QCOMPARE(results[0][1].toReal(), 1.0);
            QCOMPARE(results[0][2].toReal(), 2.0);
            QCOMPARE(results[0][3].toReal(), 0.75);

            QCOMPARE(results[1].size(), static_cast<std::size_t>(4));
            QCOMPARE(results[1][0].toReal(), 6000.0);
            QCOMPARE(results[1][1].toReal(), 4.0);
            QCOMPARE(results[1][2].toReal(), 3.0);
            QCOMPARE(results[1][3].toReal(), 0.75);

            QCOMPARE(results[2].size(), static_cast<std::size_t>(4));
            QCOMPARE(results[2][0].toReal(), 7000.0);
            QCOMPARE(results[2][1].toReal(), 6.0);
            QCOMPARE(results[2][2].toReal(), 5.0);
            QCOMPARE(results[2][3].toReal(), 0.75);


            results = c.select("Table_check", {"Year", "DOY", "N"}, "", "DOY ASC").execute().all();

            QCOMPARE(results.size(), static_cast<std::size_t>(3));

            QCOMPARE(results[0].size(), static_cast<std::size_t>(3));
            QCOMPARE(results[0][0].toReal(), 1970.0);
            QCOMPARE(results[0][2].toReal(), 1.0);

            QCOMPARE(results[1].size(), static_cast<std::size_t>(3));
            QCOMPARE(results[1][0].toReal(), 1970.0);
            QCOMPARE(results[1][2].toReal(), 4.0);

            QCOMPARE(results[2].size(), static_cast<std::size_t>(3));
            QCOMPARE(results[2][0].toReal(), 1970.0);
            QCOMPARE(results[2][2].toReal(), 6.0);
        }
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
