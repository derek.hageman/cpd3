/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef OUTPUTSQLDB_H
#define OUTPUTSQLDB_H

#include "core/first.hxx"

#include <string>
#include <unordered_map>
#include <functional>
#include <mutex>
#include <condition_variable>
#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QList>
#include <QHash>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "database/connection.hxx"
#include "database/statement.hxx"
#include "datacore/stream.hxx"
#include "datacore/processingtapchain.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/segment.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "datacore/sequencematch.hxx"

class OutputSQLDB;

class SQLColumnTarget {
    bool key;
public:
    SQLColumnTarget(const CPD3::Data::ValueSegment::Transfer &config);

    virtual ~SQLColumnTarget();

    inline bool isKey() const
    { return key; }

    virtual CPD3::Database::Types::Variant processSegment(CPD3::Data::SequenceSegment &segment) = 0;

    virtual void registerInput(const CPD3::Data::SequenceName &unit);

    virtual CPD3::Data::SequenceMatch::Composite requestedInputs(const CPD3::Data::SequenceName::Component &station);

    static bool canMerge(const CPD3::Data::Variant::Read &a, const CPD3::Data::Variant::Read &b);

    static std::unique_ptr<SQLColumnTarget> create(const CPD3::Data::ValueSegment::Transfer &config);
};

class SQLColumnTime : public SQLColumnTarget {
    enum TimePosition {
        Time_Start, Time_End, Time_Middle,
    };
    std::unique_ptr<CPD3::Data::DynamicPrimitive<TimePosition>> timePosition;
    std::unique_ptr<CPD3::Data::DynamicPrimitive<CPD3::Database::Types::Variant>> undefinedValue;

    static TimePosition convertTimePosition(const CPD3::Data::Variant::Read &value);

public:
    SQLColumnTime(const CPD3::Data::ValueSegment::Transfer &config);

    virtual ~SQLColumnTime();

    virtual CPD3::Database::Types::Variant processSegment(CPD3::Data::SequenceSegment &segment);

protected:
    virtual CPD3::Database::Types::Variant convertTime(double time) = 0;
};

class SQLTableTarget : public CPD3::Data::StreamSink {
    OutputSQLDB *parent;

    CPD3::Data::SequenceName::Component station;
    double dataStart;
    double dataEnd;

    CPD3::Data::Variant::Root chainProcessing;
    CPD3::Data::SequenceMatch::Composite chainInput;

    CPD3::Data::SequenceSegment::Stream reader;
    std::vector<std::string> columnNames;
    std::vector<std::unique_ptr<SQLColumnTarget>> columns;
    CPD3::Data::SequenceName::Set dispatch;

    std::unique_ptr<CPD3::Data::DynamicBool> allowUndefinedStart;
    std::unique_ptr<CPD3::Data::DynamicBool> allowUndefinedEnd;

    QString description;

    std::unique_ptr<CPD3::Database::Connection> db;
    std::vector<std::string> preWrite;
    std::vector<std::string> postWrite;

    CPD3::Database::Statement query;

    typedef std::function<CPD3::Database::Types::Variant(double)> TimeBinding;

    CPD3::Database::Statement purgeQuery;
    std::unordered_map<std::string, TimeBinding> purgeQueryStartBind;
    std::unordered_map<std::string, TimeBinding> purgeQueryEndBind;
    CPD3::Database::Statement purgeQueryBefore;
    std::unordered_map<std::string, TimeBinding> purgeQueryBeforeBind;
    CPD3::Database::Statement purgeQueryAfter;
    std::unordered_map<std::string, TimeBinding> purgeQueryAfterBind;
    CPD3::Database::Statement purgeQueryAll;

    std::vector<CPD3::Database::Types::BatchVariants> bufferedData;
    int bufferedSize;
    double bufferBegin;
    double bufferEnd;

    CPD3::Data::SequenceValue::Transfer pendingProcessing;
    bool pendingEnd;

    void executePurge(double start, double end);

    void flushBuffer(bool last = false);

    void processSegment(CPD3::Data::SequenceSegment &&segment);

    void processSegments(CPD3::Data::SequenceSegment::Transfer &&segments);

    void processData(CPD3::Data::SequenceValue::Transfer &&values);

    static bool canMergePurge(const CPD3::Data::Variant::Read &a,
                              const CPD3::Data::Variant::Read &b);

    static bool canMergeConnection(const CPD3::Data::Variant::Read &a,
                                   const CPD3::Data::Variant::Read &b);

    void stall(std::unique_lock<std::mutex> &lock);

public:
    SQLTableTarget(OutputSQLDB *parent,
                   const CPD3::Data::ValueSegment::Transfer &config,
                   const CPD3::Data::SequenceName::Component &station);

    virtual ~SQLTableTarget();

    void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

    void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

    void incomingData(const CPD3::Data::SequenceValue &value) override;

    void incomingData(CPD3::Data::SequenceValue &&value) override;

    void endData() override;

    void connectToChain(CPD3::Data::ProcessingTapChain *chain);

    void process();

    bool isValid();

    static bool canMerge(const CPD3::Data::Variant::Read &a, const CPD3::Data::Variant::Read &b);
};

class OutputSQLDB : public CPD3::CPD3Action {
Q_OBJECT

    std::string profile;
    std::vector<CPD3::Data::SequenceName::Component> stations;
    double start;
    double end;

    bool terminated;
    std::mutex mutex;
    std::condition_variable notify;

    QList<SQLTableTarget *> tables;

    CPD3::Data::ProcessingTapChain *chain;

    OutputSQLDB();

    bool testTerminated();

    friend class SQLTableTarget;

    void createTableTarget(const CPD3::Data::ValueSegment::Transfer &config,
                           const CPD3::Data::SequenceName::Component &station);

    void createTable(const CPD3::Data::ValueSegment::Transfer &config,
                     const CPD3::Data::SequenceName::Component &station);

    void createStation(const CPD3::Data::ValueSegment::Transfer &config,
                       const CPD3::Data::SequenceName::Component &station);

public:
    OutputSQLDB(const CPD3::ComponentOptions &options,
                double start,
                double end,
                const std::vector<CPD3::Data::SequenceName::Component> &setStations = {});

    virtual ~OutputSQLDB();

    virtual void signalTerminate();

signals:

    void terminateRequested();


protected:
    virtual void run();
};

class OutputSQLDBComponent : public QObject, virtual public CPD3::ActionComponentTime {
Q_OBJECT
    Q_INTERFACES(CPD3::ActionComponentTime)

    Q_PLUGIN_METADATA(IID
                              "CPD3.output_sqldb"
                              FILE
                              "output_sqldb.json")
public:
    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual int actionRequireStations();

    virtual int actionAllowStations();

    virtual bool actionRequiresTime();

    virtual CPD3::CPD3Action *createTimeAction(const CPD3::ComponentOptions &options,
                                               double start,
                                               double end,
                                               const std::vector<std::string> &stations = {});
};

#endif
