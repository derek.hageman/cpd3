/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QSqlDatabase>
#include <QUuid>
#include <QLoggingCategory>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/threadpool.hxx"
#include "core/qtcompat.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/archive/access.hxx"
#include "io/drivers/file.hxx"

#include "output_sqldb.hxx"


Q_LOGGING_CATEGORY(log_component_output_sqldb, "cpd3.component.output.sqldb", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;

namespace {

static Database::Types::Variant valueToVariant(const Variant::Read &v)
{
    switch (v.getType()) {
    case Variant::Type::Empty:
        return {};
    case Variant::Type::Real:
        return Database::Types::Variant(v.toReal());
    case Variant::Type::Integer:
        return Database::Types::Variant(v.toInteger());
    case Variant::Type::String:
        return Database::Types::Variant(v.toString());
    case Variant::Type::Boolean:
        return Database::Types::Variant(v.toBoolean() ? 1 : 0);
    case Variant::Type::Bytes:
        return Database::Types::Variant(v.toBytes());
    default:
        break;
    }
    return {};
}

static double toFractionalYear(double time)
{
    int year = Time::toDateTime(time).date().year();
    QDateTime dt(QDate(year, 1, 1), QTime(0, 0, 0), Qt::UTC);
    double yearStart = Time::fromDateTime(dt);
    dt = dt.addYears(1);
    double yearEnd = Time::fromDateTime(dt);
    return (time - yearStart) / (yearEnd - yearStart) + (double) year;
}

static Database::Types::Variant bindEpoch(double time)
{ return time; }

static Database::Types::Variant bindQDateTime(const QDateTime &dt)
{ return dt.toString(Qt::ISODate); }

static Database::Types::Variant bindQDate(const QDate &dt)
{ return dt.toString(Qt::ISODate); }

static Database::Types::Variant bindQTime(const QTime &dt)
{ return dt.toString(Qt::ISODate); }

class Column_Epoch : public SQLColumnTime {
public:
    explicit Column_Epoch(const ValueSegment::Transfer &config) : SQLColumnTime(config)
    { }

    virtual ~Column_Epoch() = default;

protected:
    Database::Types::Variant convertTime(double time) override
    { return time; }
};

class Column_DateTimeFormat : public SQLColumnTime {
    std::unique_ptr<DynamicString> format;
public:
    explicit Column_DateTimeFormat(const ValueSegment::Transfer &config) : SQLColumnTime(config),
                                                                           format(DynamicStringOption::fromConfiguration(
                                                                                   config,
                                                                                   "Format"))
    { }

    virtual ~Column_DateTimeFormat() = default;

protected:
    Database::Types::Variant convertTime(double time) override
    {
        if (!FP::defined(time))
            return {};
        return Time::toDateTime(time).toString(format->get(time));
    }
};

class Column_DateTime : public SQLColumnTime {
public:
    explicit Column_DateTime(const ValueSegment::Transfer &config) : SQLColumnTime(config)
    { }

    virtual ~Column_DateTime() = default;

protected:
    virtual Database::Types::Variant convertDateTime(const QDateTime &dt)
    { return bindQDateTime(dt); }

    Database::Types::Variant convertTime(double time) override
    {
        if (!FP::defined(time))
            return {};
        return convertDateTime(Time::toDateTime(time));
    }
};

class Column_Date : public Column_DateTime {
public:
    explicit Column_Date(const ValueSegment::Transfer &config) : Column_DateTime(config)
    { }

    virtual ~Column_Date() = default;

protected:
    Database::Types::Variant convertDateTime(const QDateTime &dt) override
    { return bindQDate(dt.date()); }
};

class Column_Time : public Column_DateTime {
public:
    explicit Column_Time(const ValueSegment::Transfer &config) : Column_DateTime(config)
    { }

    virtual ~Column_Time() = default;

protected:
    Database::Types::Variant convertDateTime(const QDateTime &dt) override
    { return bindQTime(dt.time()); }
};

class Column_Hour : public Column_DateTime {
public:
    explicit Column_Hour(const ValueSegment::Transfer &config) : Column_DateTime(config)
    { }

    virtual ~Column_Hour() = default;

protected:
    Database::Types::Variant convertDateTime(const QDateTime &dt) override
    { return dt.time().hour(); }
};

class Column_PerYear : public SQLColumnTime {
    bool boundsValid;
    double yearStart;
    double yearEnd;
public:
    explicit Column_PerYear(const ValueSegment::Transfer &config) : SQLColumnTime(config),
                                                                    boundsValid(false),
                                                                    yearStart(FP::undefined()),
                                                                    yearEnd(FP::undefined())
    { }

    virtual ~Column_PerYear() = default;

protected:
    virtual Database::Types::Variant convertYearTime(double time) = 0;

    virtual void yearChanged(int year, double yearStart, double yearEnd) = 0;

    Database::Types::Variant convertTime(double time) override
    {
        if (!FP::defined(time))
            return {};
        if (!boundsValid || time < yearStart || time > yearEnd) {
            boundsValid = true;
            int year = Time::toDateTime(time).date().year();
            QDateTime dt(QDate(year, 1, 1), QTime(0, 0, 0), Qt::UTC);
            yearStart = Time::fromDateTime(dt);
            dt = dt.addYears(1);
            yearEnd = Time::fromDateTime(dt);
            yearChanged(year, yearStart, yearEnd);
        }
        return convertYearTime(time);
    }
};

class Column_Year : public Column_PerYear {
    Database::Types::Variant year;
public:
    explicit Column_Year(const ValueSegment::Transfer &config) : Column_PerYear(config), year(-1)
    { }

    virtual ~Column_Year() = default;

protected:
    Database::Types::Variant convertYearTime(double) override
    { return year; }

    void yearChanged(int year, double, double) override
    { this->year = Database::Types::Variant(year); }
};

class Column_FractionalYear : public Column_PerYear {
    int year;
    double yearStart;
    double fractionPerSecond;
public:
    explicit Column_FractionalYear(const ValueSegment::Transfer &config) : Column_PerYear(config),
                                                                           year(-1),
                                                                           yearStart(
                                                                                   FP::undefined()),
                                                                           fractionPerSecond(
                                                                                   FP::undefined())
    { }

    virtual ~Column_FractionalYear() = default;

protected:
    Database::Types::Variant convertYearTime(double time) override
    { return (time - yearStart) * fractionPerSecond + (double) year; }

    void yearChanged(int year, double yearStart, double yearEnd) override
    {
        this->year = year;
        this->yearStart = yearStart;
        fractionPerSecond = 1.0 / (yearEnd - yearStart);
    }
};

class Column_DOY : public Column_PerYear {
    double yearStart;
public:
    explicit Column_DOY(const ValueSegment::Transfer &config) : Column_PerYear(config),
                                                                yearStart(FP::undefined())
    { }

    virtual ~Column_DOY() = default;

protected:
    Database::Types::Variant convertYearTime(double time) override
    { return (time - yearStart) / 86400.0 + 1.0; }

    void yearChanged(int, double yearStart, double) override
    { this->yearStart = yearStart; }
};


class Column_Value : public SQLColumnTarget {
    std::unique_ptr<DynamicInput> input;
    std::unique_ptr<DynamicPrimitive<Database::Types::Variant>> undefinedValue;
public:
    explicit Column_Value(const ValueSegment::Transfer &config) : SQLColumnTarget(config),
                                                                  input(DynamicInput::fromConfiguration(
                                                                          config, "Value")),
                                                                  undefinedValue(DynamicPrimitive<
                                                                          Database::Types::Variant>::fromConfiguration(
                                                                          valueToVariant, config,
                                                                          "MVC"))
    { }

    virtual ~Column_Value() = default;

    Database::Types::Variant processSegment(SequenceSegment &segment) override
    {
        double value = input->get(segment);
        if (!FP::defined(value)) {
            return undefinedValue->get(segment);
        }
        return value;
    }

    void registerInput(const SequenceName &unit) override
    { input->registerInput(unit); }

    SequenceMatch::Composite requestedInputs(const SequenceName::Component &station) override
    {
        input->registerExpectedFlavorless(station);
        input->registerExpected(station);
        SequenceMatch::Composite result;
        for (const auto &n : input->getUsedInputs()) {
            result.append(n);
        }
        return result;
    }
};

class Column_Generic : public SQLColumnTarget {
    std::unique_ptr<DynamicSequenceSelection> input;
    std::unique_ptr<DynamicString> path;
    std::unique_ptr<DynamicPrimitive<Database::Types::Variant>> undefinedValue;
public:
    explicit Column_Generic(const ValueSegment::Transfer &config) : SQLColumnTarget(config),
                                                                    input(DynamicSequenceSelection::fromConfiguration(
                                                                            config, "Value")),
                                                                    path(DynamicStringOption::fromConfiguration(
                                                                            config, "Path")),
                                                                    undefinedValue(DynamicPrimitive<
                                                                            Database::Types::Variant>::fromConfiguration(
                                                                            valueToVariant, config,
                                                                            "MVC"))
    { }

    virtual ~Column_Generic() = default;

    Database::Types::Variant processSegment(SequenceSegment &segment) override
    {
        QString p(path->get(segment));
        for (const auto &n : input->get(segment)) {
            auto v = segment[n].getPath(p);
            if (v.exists())
                return valueToVariant(v);
        }
        return undefinedValue->get(segment);
    }

    void registerInput(const SequenceName &unit) override
    { input->registerInput(unit); }

    SequenceMatch::Composite requestedInputs(const SequenceName::Component &station) override
    {
        input->registerExpectedFlavorless(station);
        input->registerExpected(station);
        SequenceMatch::Composite result;
        for (const auto &n : input->getAllUnits()) {
            result.append(n);
        }
        return result;
    }
};

class Column_Constant : public SQLColumnTarget {
    std::unique_ptr<DynamicPrimitive<Database::Types::Variant>> value;
public:
    explicit Column_Constant(const ValueSegment::Transfer &config) : SQLColumnTarget(config),
                                                                     value(DynamicPrimitive<
                                                                             Database::Types::Variant>::fromConfiguration(
                                                                             valueToVariant, config,
                                                                             "Value"))
    { }

    virtual ~Column_Constant() = default;

    Database::Types::Variant processSegment(SequenceSegment &segment) override
    {
        return value->get(segment);
    }
};

}


SQLColumnTime::SQLColumnTime(const ValueSegment::Transfer &config) : SQLColumnTarget(config),
                                                                     timePosition(DynamicPrimitive<
                                                                             TimePosition>::fromConfiguration(
                                                                             convertTimePosition,
                                                                             config, "Position")),
                                                                     undefinedValue(
                                                                             DynamicPrimitive<
                                                                                     Database::Types::Variant>::fromConfiguration(
                                                                                     valueToVariant,
                                                                                     config, "MVC"))
{
}

SQLColumnTime::~SQLColumnTime() = default;

SQLColumnTime::TimePosition SQLColumnTime::convertTimePosition(const Variant::Read &value)
{
    const auto &type = value.toString();
    if (Util::equal_insensitive(type, "end"))
        return Time_End;
    else if (Util::equal_insensitive(type, "middle", "center"))
        return Time_Middle;
    return Time_Start;
}

Database::Types::Variant SQLColumnTime::processSegment(SequenceSegment &segment)
{
    Database::Types::Variant result;
    switch (timePosition->get(segment)) {
    case Time_Start:
        result = convertTime(segment.getStart());
        break;
    case Time_End:
        result = convertTime(segment.getEnd());
        break;
    case Time_Middle:
        if (!FP::defined(segment.getStart()) || !FP::defined(segment.getEnd()))
            break;
        result = convertTime((segment.getStart() + segment.getEnd()) * 0.5);
        break;
    }
    if (result.isNull())
        return undefinedValue->get(segment);
    return result;
}


SQLColumnTarget::SQLColumnTarget(const ValueSegment::Transfer &config) : key(false)
{
    for (const auto &c : config) {
        if (c["Key"].toBool())
            key = true;
    }
}

SQLColumnTarget::~SQLColumnTarget()
{ }

void SQLColumnTarget::registerInput(const SequenceName &unit)
{ Q_UNUSED(unit); }

SequenceMatch::Composite SQLColumnTarget::requestedInputs(const SequenceName::Component &station)
{ return SequenceMatch::Composite(); }

bool SQLColumnTarget::canMerge(const Variant::Read &a, const Variant::Read &b)
{
    if (!a.exists())
        return !b.exists();
    else if (!b.exists())
        return false;

    const auto &typeA = a["Type"].toString();
    const auto &typeB = b["Type"].toString();
    if (Util::equal_insensitive(typeA, "epoch"))
        return Util::equal_insensitive(typeB, "epoch");
    else if (Util::equal_insensitive(typeA, "datetime"))
        return Util::equal_insensitive(typeB, "datetime");
    else if (Util::equal_insensitive(typeA, "datetimeformat"))
        return Util::equal_insensitive(typeB, "datetimeformat");
    else if (Util::equal_insensitive(typeA, "date"))
        return Util::equal_insensitive(typeB, "date");
    else if (Util::equal_insensitive(typeA, "time"))
        return Util::equal_insensitive(typeB, "time");
    else if (Util::equal_insensitive(typeA, "hour", "hourofday"))
        return Util::equal_insensitive(typeB, "hour", "hourofday");
    else if (Util::equal_insensitive(typeA, "year"))
        return Util::equal_insensitive(typeB, "year");
    else if (Util::equal_insensitive(typeA, "fyear", "fractionalyear"))
        return Util::equal_insensitive(typeB, "fyear", "fractionalyear");
    else if (Util::equal_insensitive(typeA, "doy", "dayofyear"))
        return Util::equal_insensitive(typeB, "doy", "dayofyear");
    else if (Util::equal_insensitive(typeA, "generic", "general"))
        return Util::equal_insensitive(typeB, "generic", "general");
    else if (Util::equal_insensitive(typeA, "constant"))
        return Util::equal_insensitive(typeB, "constant");

    if (Util::equal_insensitive(typeB, "epoch"))
        return false;
    else if (Util::equal_insensitive(typeB, "datetime"))
        return false;
    else if (Util::equal_insensitive(typeB, "datetimeformat"))
        return false;
    else if (Util::equal_insensitive(typeB, "date"))
        return false;
    else if (Util::equal_insensitive(typeB, "time"))
        return false;
    else if (Util::equal_insensitive(typeB, "hour", "hourofday"))
        return false;
    else if (Util::equal_insensitive(typeB, "year"))
        return false;
    else if (Util::equal_insensitive(typeB, "fyear", "fractionalyear"))
        return false;
    else if (Util::equal_insensitive(typeB, "doy", "dayofyear"))
        return false;
    else if (Util::equal_insensitive(typeB, "generic", "general"))
        return false;
    else if (Util::equal_insensitive(typeB, "constant"))
        return false;

    return true;
}

std::unique_ptr<SQLColumnTarget> SQLColumnTarget::create(const ValueSegment::Transfer &config)
{
    Q_ASSERT(!config.empty());

    if (!config.front().read().exists())
        return {};

    const auto &type = config.front()["Type"].toString();

    if (Util::equal_insensitive(type, "epoch")) {
        return std::unique_ptr<SQLColumnTarget>(new Column_Epoch(config));
    } else if (Util::equal_insensitive(type, "datetime")) {
        return std::unique_ptr<SQLColumnTarget>(new Column_DateTime(config));
    } else if (Util::equal_insensitive(type, "datetimeformat")) {
        return std::unique_ptr<SQLColumnTarget>(new Column_DateTimeFormat(config));
    } else if (Util::equal_insensitive(type, "date")) {
        return std::unique_ptr<SQLColumnTarget>(new Column_Date(config));
    } else if (Util::equal_insensitive(type, "time")) {
        return std::unique_ptr<SQLColumnTarget>(new Column_Time(config));
    } else if (Util::equal_insensitive(type, "hour", "hourofday")) {
        return std::unique_ptr<SQLColumnTarget>(new Column_Hour(config));
    } else if (Util::equal_insensitive(type, "year")) {
        return std::unique_ptr<SQLColumnTarget>(new Column_Year(config));
    } else if (Util::equal_insensitive(type, "fyear", "fractionalyear")) {
        return std::unique_ptr<SQLColumnTarget>(new Column_FractionalYear(config));
    } else if (Util::equal_insensitive(type, "doy", "dayofyear")) {
        return std::unique_ptr<SQLColumnTarget>(new Column_DOY(config));
    } else if (Util::equal_insensitive(type, "generic", "general")) {
        return std::unique_ptr<SQLColumnTarget>(new Column_Generic(config));
    } else if (Util::equal_insensitive(type, "constant")) {
        return std::unique_ptr<SQLColumnTarget>(new Column_Constant(config));
    }
    return std::unique_ptr<SQLColumnTarget>(new Column_Value(config));
}


static std::vector<std::string> buildSQLQueries(const Variant::Read &v)
{
    std::vector<std::string> result;
    for (auto child : v.toArray()) {
        auto q = Util::trimmed(child.toString());
        if (q.empty())
            continue;
        result.emplace_back(std::move(q));
    }
    return result;
}

static const size_t bufferedVariantSize = 64;

/* Some drivers might sometimes allow for less strict table naming,
 * but that's a bit much to check for, so just flatten anything that's
 * not an ASCII letter, number or underscore */
static void flattenString(std::string &str)
{
    for (auto &code : str) {
        if (code >= '0' && code <= '9')
            continue;
        if (code >= 'A' && code <= 'Z')
            continue;
        if (code >= 'a' && code <= 'z')
            continue;
        if (code == '_')
            continue;
        code = '_';
    }
}

static double roundEnd(double end, double i = 1.0)
{ return ceil(end / i) * i; }

static double roundStart(double end, double i = 1.0)
{ return floor(end / i) * i; }

template<int rounding>
static Database::Types::Variant bindDateTimeStart(double time)
{ return bindQDateTime(Time::toDateTime(roundStart(time, rounding))); }

template<int rounding>
static Database::Types::Variant bindDateTimeEnd(double time)
{ return bindQDateTime(Time::toDateTime(roundEnd(time, rounding))); }

template<int rounding>
static Database::Types::Variant bindDateStart(double time)
{ return bindQDate(Time::toDateTime(roundStart(time, rounding)).date()); }

template<int rounding>
static Database::Types::Variant bindDateEnd(double time)
{ return bindQDate(Time::toDateTime(roundEnd(time, rounding)).date()); }

template<int rounding>
static Database::Types::Variant bindTimeStart(double time)
{ return bindQTime(Time::toDateTime(roundStart(time, rounding)).time()); }

template<int rounding>
static Database::Types::Variant bindTimeEnd(double time)
{ return bindQTime(Time::toDateTime(roundEnd(time, rounding)).time()); }

static Database::Types::Variant bindHourStart(double time)
{ return Time::toDateTime(roundStart(time, 3600)).time().hour(); }

static Database::Types::Variant bindHourEnd(double time)
{ return Time::toDateTime(roundEnd(time, 3600)).time().hour(); }

static Database::Types::Variant bindYear(double time)
{ return Time::toDateTime(time).date().year(); }

static Database::Types::Variant bindFractionalYear(double time)
{ return toFractionalYear(time); }

template<int origin>
static Database::Types::Variant bindDOY(double time)
{
    int year = Time::toDateTime(time).date().year();
    double begin = Time::fromDateTime(QDateTime(QDate(year, 1, 1), QTime(0, 0, 0), Qt::UTC));
    return (time - begin) / 86400.0 + (double) origin;
}

SQLTableTarget::SQLTableTarget(OutputSQLDB *parent,
                               const ValueSegment::Transfer &config,
                               const SequenceName::Component &station) : parent(parent),
                                                                         station(station),
                                                                         dataStart(config.front()
                                                                                         .getStart()),
                                                                         dataEnd(config.back()
                                                                                       .getEnd()),
                                                                         chainProcessing(),
                                                                         chainInput(
                                                                                 config.front()["Input"]),
                                                                         reader(),
                                                                         columnNames(),
                                                                         columns(),
                                                                         dispatch(),
                                                                         allowUndefinedStart(
                                                                                 DynamicBoolOption::fromConfiguration(
                                                                                         config,
                                                                                         "AllowUndefined/Start")),
                                                                         allowUndefinedEnd(
                                                                                 DynamicBoolOption::fromConfiguration(
                                                                                         config,
                                                                                         "AllowUndefined/End")),
                                                                         description(),
                                                                         db(),
                                                                         preWrite(buildSQLQueries(
                                                                                 config.front()["Execute/PreWrite"])),
                                                                         postWrite(buildSQLQueries(
                                                                                 config.front()["Execute/PostWrite"])),
                                                                         query(),
                                                                         purgeQuery(),
                                                                         purgeQueryBefore(),
                                                                         purgeQueryAfter(),
                                                                         purgeQueryAll(),
                                                                         bufferedData(),
                                                                         bufferedSize(0),
                                                                         bufferBegin(
                                                                                 FP::undefined()),
                                                                         bufferEnd(FP::undefined()),
                                                                         pendingProcessing(),
                                                                         pendingEnd(false)
{
    for (const auto &add : config.front()["Columns"].toHash().keys()) {
        columnNames.emplace_back(add);
    }
    if (columnNames.empty())
        return;

    auto latest = config.front().read();

    chainProcessing.write().set(latest["Processing"]);

    auto tableName = latest["Database/Table"].toString();
    if (tableName.empty())
        tableName = station;
    Util::replace_insensitive(tableName, "${STATION}", station);
    flattenString(tableName);

    {
        QString hostname = latest["Database/Hostname"].toQString();
        if (!hostname.isEmpty()) {
            if (!description.isEmpty())
                description.append(':');
            description.append(hostname);
        }

        quint16 port = 0;
        if (INTEGER::defined(latest["Database/Port"].toInt64()))
            port = (quint16) latest["Database/Port"].toInt64();

        QString name = latest["Database/Name"].toQString();
        if (!name.isEmpty()) {
            if (!description.isEmpty())
                description.append(':');
            description.append(name);
        }

        QString user = latest["Database/Username"].toQString();
        QString password = latest["Database/Password"].toQString();
        {
            QString fileName(latest["Database/PasswordFile"].toQString());
            if (!fileName.isEmpty() && QFile::exists(fileName)) {
                if (auto file = IO::Access::file(fileName, IO::File::Mode::readOnly())->block()) {
                    QByteArray contents;
                    file->read(contents);
                    password = QString::fromUtf8(contents);
                } else {
                    qCDebug(log_component_output_sqldb) << "Failed to open password file"
                                                        << fileName;
                }
            }
        }

        QString driver(latest["Database/Driver"].toQString().toUpper());
        if (driver.toLower() == "sqlite") {
            db.reset(new Database::Connection(Database::Storage::sqlite(name.toStdString())));
        } else if (driver.toLower() == "mysql" || driver.toLower() == "mariadb") {
            db.reset(new Database::Connection(
                    Database::Storage::mysql(hostname.toStdString(), name.toStdString(),
                                             user.toStdString(), password.toStdString(), port)));
        } else if (driver.toLower() == "psql" || driver == "postgresql") {
            db.reset(new Database::Connection(
                    Database::Storage::postgresql(hostname.toStdString(), name.toStdString(),
                                                  user.toStdString(), password.toStdString(),
                                                  port)));
        } else {
            if (driver.isEmpty())
                driver = "QODBC";
            QSqlDatabase
                    database = QSqlDatabase::addDatabase(driver, QUuid::createUuid().toString());
            if (!hostname.isEmpty())
                database.setHostName(hostname);
            if (port != 0)
                database.setPort((int) port);
            if (!name.isEmpty())
                database.setDatabaseName(name);
            if (!user.isEmpty())
                database.setUserName(user);
            if (!password.isEmpty())
                database.setPassword(password);
            db.reset(new Database::Connection(Database::Storage(database)));
        }
    }

    if (!db->start()) {
        db.reset();
        return;
    }

    auto exec = buildSQLQueries(latest["Execute/Connect"]);
    if (!exec.empty()) {
        if (!db->synchronousTransaction([&]() -> Database::Connection::SynchronousResult {
            for (const auto &query : exec) {
                auto s = db->general(query);
                if (!s)
                    return Database::Connection::Synchronous_Abort;
                s.synchronous();
            }
            return Database::Connection::Synchronous_Ok;
        }, Database::Connection::Transaction_Default, 120.0)) {
            qCWarning(log_component_output_sqldb) << "Database initialization failed";
            db->signalTerminate();
            db->wait();
            db.reset();
            return;
        }
    }

    std::sort(columnNames.begin(), columnNames.end());
    std::unordered_set<std::string> keyNames;
    std::unordered_set<std::string> insertNames;
    for (auto column = columnNames.begin(); column != columnNames.end();) {
        flattenString(*column);
        if (column->empty()) {
            column = columnNames.erase(column);
            continue;
        }

        auto add = SQLColumnTarget::create(ValueSegment::withPath(config, QString("Columns/%1").arg(
                QString::fromStdString(*column))));
        if (!add) {
            column = columnNames.erase(column);
            continue;
        }
        if (add->isKey())
            keyNames.insert(*column);
        else
            insertNames.insert(*column);
        columns.emplace_back(std::move(add));
        bufferedData.emplace_back();

        ++column;
    }

    if (!keyNames.empty()) {
        query = db->merge(tableName, keyNames, insertNames);
    } else {
        query = db->merge(tableName, insertNames);
    }
    if (!query) {
        qCWarning(log_component_output_sqldb) << "Merge query creation failed";
        db->signalTerminate();
        db->wait();
        db.reset();
        return;
    }

    const auto &type = latest["Purge/Type"].toString();
    if (Util::equal_insensitive(type, "epoch", "datetime", "fyear", "fractionalyear", "date")) {
        std::string colName;
        TimeBinding startBinding;
        TimeBinding endBinding;
        if (Util::equal_insensitive(type, "epoch")) {
            colName = latest["Purge/Epoch"].toString();
            startBinding = bindEpoch;
            endBinding = bindEpoch;
        } else if (Util::equal_insensitive(type, "datetime")) {
            colName = latest["Purge/DateTime"].toString();
            startBinding = bindDateTimeStart<1>;
            endBinding = bindDateTimeEnd<1>;
        } else if (Util::equal_insensitive(type, "date")) {
            colName = latest["Purge/Date"].toString();
            startBinding = bindDateStart<86400>;
            endBinding = bindDateEnd<86400>;
        } else {
            colName = latest["Purge/Year"].toString();
            startBinding = bindFractionalYear;
            endBinding = bindFractionalYear;
        }
        flattenString(colName);

        if (colName.empty()) {
            qCWarning(log_component_output_sqldb) << "Invalid column to purge";
            db->signalTerminate();
            db->wait();
            db.reset();
            return;
        }

        purgeQuery = db->del(tableName, colName + " >= :start AND " + colName + " < :end");
        if (!purgeQuery) {
            qCWarning(log_component_output_sqldb) << "Purge query creation failed";
            db->signalTerminate();
            db->wait();
            db.reset();
            return;
        }
        purgeQueryStartBind.emplace("start", startBinding);
        purgeQueryEndBind.emplace("end", endBinding);

        purgeQueryBefore = db->del(tableName, colName + " < :end");
        if (!purgeQueryBefore) {
            qCWarning(log_component_output_sqldb) << "Purge before query creation failed";
            db->signalTerminate();
            db->wait();
            db.reset();
            return;
        }
        purgeQueryBeforeBind.emplace("end", endBinding);

        purgeQueryAfter = db->del(tableName, colName + " >= :start");
        if (!purgeQueryAfter) {
            qCWarning(log_component_output_sqldb) << "Purge after query creation failed";
            db->signalTerminate();
            db->wait();
            db.reset();
            return;
        }
        purgeQueryAfterBind.emplace(":start", startBinding);

        purgeQueryAll = db->del(tableName);
        if (!purgeQueryAll) {
            qCWarning(log_component_output_sqldb) << "Purge global query creation failed";
            db->signalTerminate();
            db->wait();
            db.reset();
            return;
        }
    } else if (Util::equal_insensitive(type, "dateandtime", "dateandhour", "yeardoy",
                                       "yearanddoy")) {
        std::string colMajor;
        std::string colMinor;
        TimeBinding startMajorBinding;
        TimeBinding startMinorBinding;
        TimeBinding endMajorBinding;
        TimeBinding endMinorBinding;
        if (Util::equal_insensitive(type, "dateandtime")) {
            colMajor = latest["Purge/Date"].toString();
            colMinor = latest["Purge/Time"].toString();
            startMajorBinding = bindDateStart<1>;
            startMinorBinding = bindTimeStart<1>;
            endMajorBinding = bindDateEnd<1>;
            endMinorBinding = bindTimeEnd<1>;
        } else if (Util::equal_insensitive(type, "dateandhour")) {
            colMajor = latest["Purge/Date"].toString();
            colMinor = latest["Purge/Hour"].toString();
            startMajorBinding = bindDateStart<3600>;
            startMinorBinding = bindHourStart;
            endMajorBinding = bindDateEnd<3600>;
            endMinorBinding = bindHourEnd;
        } else {
            colMajor = latest["Purge/Year"].toString();
            colMinor = latest["Purge/DOY"].toString();
            startMajorBinding = bindYear;
            startMinorBinding = bindDOY<1>;
            endMajorBinding = bindYear;
            endMinorBinding = bindDOY<1>;
        }
        flattenString(colMajor);
        flattenString(colMinor);

        if (colMajor.empty() || colMinor.empty()) {
            qCWarning(log_component_output_sqldb) << "Invalid column to purge";
            db->signalTerminate();
            db->wait();
            db.reset();
            return;
        }

        purgeQuery = db->del(tableName, "(" +
                colMajor +
                " > :containedStart AND " +
                colMajor +
                " < :containedEnd) OR "
                "((:checkDifferentStart <> :checkDifferentEnd) AND "
                "((" +
                colMajor +
                " = :differentStart AND " +
                colMinor +
                " >= :differentStartMinor) OR "
                "(" +
                colMajor +
                " = :differentEnd AND " +
                colMinor +
                " < :differentEndMinor))) OR "
                "((:checkSameStart = :checkSameEnd) AND "
                "(" +
                colMajor +
                " = :sameStart AND " +
                colMinor +
                " >= :sameStartMinor AND " +
                colMinor +
                " < :sameEndMinor))");
        if (!purgeQuery) {
            qCWarning(log_component_output_sqldb) << "Purge query creation failed";
            db->signalTerminate();
            db->wait();
            db.reset();
            return;
        }
        purgeQueryStartBind.emplace("containedStart", startMajorBinding);
        purgeQueryEndBind.emplace("containedEnd", endMajorBinding);
        purgeQueryStartBind.emplace("checkDifferentStart", startMajorBinding);
        purgeQueryEndBind.emplace("checkDifferentEnd", endMajorBinding);
        purgeQueryStartBind.emplace("differentStart", startMajorBinding);
        purgeQueryStartBind.emplace("differentStartMinor", startMinorBinding);
        purgeQueryEndBind.emplace("differentEnd", endMajorBinding);
        purgeQueryEndBind.emplace("differentEndMinor", endMinorBinding);
        purgeQueryStartBind.emplace("checkSameStart", startMajorBinding);
        purgeQueryEndBind.emplace("checkSameEnd", endMajorBinding);
        purgeQueryStartBind.emplace("sameStart", startMajorBinding);
        purgeQueryStartBind.emplace("sameStartMinor", startMinorBinding);
        purgeQueryEndBind.emplace("sameEndMinor", endMinorBinding);

        purgeQueryBefore = db->del(tableName, "(" +
                colMajor +
                " < :before) OR "
                "(" +
                colMajor +
                " = :on AND " +
                colMinor +
                " < :minor)");
        if (!purgeQueryBefore) {
            qCWarning(log_component_output_sqldb) << "Purge before query creation failed";
            db->signalTerminate();
            db->wait();
            db.reset();
            return;
        }
        purgeQueryBeforeBind.emplace("before", endMajorBinding);
        purgeQueryBeforeBind.emplace("on", endMajorBinding);
        purgeQueryBeforeBind.emplace("minor", endMinorBinding);

        purgeQueryAfter = db->del(tableName, "(" +
                colMajor +
                " > :after) OR "
                "(" +
                colMajor +
                " = :on AND " +
                colMinor +
                " >= :minor)");
        if (!purgeQueryAfter) {
            qCWarning(log_component_output_sqldb) << "Purge after query creation failed";
            db->signalTerminate();
            db->wait();
            db.reset();
            return;
        }
        purgeQueryAfterBind.emplace("after", startMajorBinding);
        purgeQueryAfterBind.emplace("on", startMajorBinding);
        purgeQueryAfterBind.emplace("minor", startMinorBinding);

        purgeQueryAll = db->del(tableName);
        if (!purgeQueryAll) {
            qCWarning(log_component_output_sqldb) << "Purge global query creation failed";
            db->signalTerminate();
            db->wait();
            db.reset();
            return;
        }
    }

    qCDebug(log_component_output_sqldb) << "Output" << description << "initialized on"
                                        << Logging::range(dataStart, dataEnd);
}

SQLTableTarget::~SQLTableTarget()
{
    if (db) {
        db->signalTerminate();
        db->wait();
        db.reset();
    }
}

bool SQLTableTarget::canMerge(const Variant::Read &a, const Variant::Read &b)
{
    if (!canMergePurge(a["Purge"], b["Purge"]))
        return false;
    if (!canMergeConnection(a["Database"], b["Database"]))
        return false;
    if (a["Processing"].exists()) {
        if (!b["Processing"].exists())
            return false;
        if (a["Processing"] != b["Processing"])
            return false;
    } else if (b["Processing"].exists()) {
        return false;
    }
    if (a["Input"].exists()) {
        if (!b["Input"].exists())
            return false;
        if (a["Input"] != b["Input"])
            return false;
    } else if (b["Input"].exists()) {
        return false;
    }

    auto columnsA = a["Columns"].toHash();
    auto columnsB = b["Columns"].toHash();
    std::unordered_set<Variant::PathElement::HashIndex> hitA;
    for (auto cA : columnsA) {
        if (cA.first.empty())
            continue;
        auto cB = columnsB.find(cA.first);
        if (cB == columnsB.end())
            return false;
        if (!SQLColumnTarget::canMerge(cA.second, cB->second))
            return false;
        hitA.insert(cA.first);
    }
    auto hitB = columnsB.keys();
    hitB.erase(Variant::PathElement::HashIndex());
    if (hitA != hitB)
        return false;
    return true;
}

bool SQLTableTarget::canMergePurge(const Variant::Read &a, const Variant::Read &b)
{
    const auto &typeA = a["Type"].toString();
    const auto &typeB = b["Type"].toString();
    if (Util::equal_insensitive(typeA, "epoch")) {
        if (!Util::equal_insensitive(typeB, "epoch"))
            return false;
        return a["Epoch"].toString() == b["Epoch"].toString();
    } else if (Util::equal_insensitive(typeA, "datetime")) {
        if (!Util::equal_insensitive(typeB, "datetime"))
            return false;
        return a["DateTime"].toString() == b["DateTime"].toString();
    } else if (Util::equal_insensitive(typeA, "dateandtime")) {
        if (!Util::equal_insensitive(typeB, "dateandtime"))
            return false;
        return a["Date"].toString() == b["Date"].toString() &&
                a["Time"].toString() == b["Time"].toString();
    } else if (Util::equal_insensitive(typeA, "dateandhour")) {
        if (!Util::equal_insensitive(typeB, "dateandhour"))
            return false;
        return a["Date"].toString() == b["Date"].toString() &&
                a["Hour"].toString() == b["Hour"].toString();
    } else if (Util::equal_insensitive(typeA, "yeardoy", "yearanddoy")) {
        if (!Util::equal_insensitive(typeB, "yeardoy", "yearanddoy"))
            return false;
        return a["Year"].toString() == b["Year"].toString() &&
                a["DOY"].toString() == b["DOY"].toString();
    } else if (Util::equal_insensitive(typeA, "fyear", "fractionalyear")) {
        if (!Util::equal_insensitive(typeB, "fyear", "fractionalyear"))
            return false;
        return a["Year"].toString() == b["Year"].toString();
    }
    return true;
}

bool SQLTableTarget::canMergeConnection(const Variant::Read &a, const Variant::Read &b)
{
    if (a["Driver"].toString() != b["Driver"].toString())
        return false;
    if (a["Hostname"].toString() != b["Hostname"].toString())
        return false;
    if (a["Port"].toInt64() != b["Port"].toInt64())
        return false;
    if (a["Name"].toString() != b["Name"].toString())
        return false;

    if (a["Username"].toString() != b["Username"].toString())
        return false;
    if (a["Password"].toString() != b["Password"].toString())
        return false;
    if (a["PasswordFile"].toString() != b["PasswordFile"].toString())
        return false;

    if (a["Table"].toString() != b["Table"].toString())
        return false;

    if (buildSQLQueries(a["Execute/Connect"]) != buildSQLQueries(b["Execute/Connect"]))
        return false;
    if (buildSQLQueries(a["Execute/PreWrite"]) != buildSQLQueries(b["Execute/PreWrite"]))
        return false;
    if (buildSQLQueries(a["Execute/PostWrite"]) != buildSQLQueries(b["Execute/PostWrite"]))
        return false;
    return true;
}

void SQLTableTarget::executePurge(double start, double end)
{
    Database::Statement q;
    Database::Types::Binds binds;
    if (!FP::defined(start)) {
        if (!FP::defined(end)) {
            q = purgeQueryAll;
        } else {
            q = purgeQueryBefore;
            for (const auto &bind : purgeQueryBeforeBind) {
                binds.emplace(bind.first, bind.second(end));
            }
        }
    } else if (!FP::defined(end)) {
        q = purgeQueryAfter;
        for (const auto &bind : purgeQueryAfterBind) {
            binds.emplace(bind.first, bind.second(start));
        }
    } else {
        q = purgeQuery;
        for (const auto &bind : purgeQueryStartBind) {
            binds.emplace(bind.first, bind.second(start));
        }
        for (const auto &bind : purgeQueryEndBind) {
            binds.emplace(bind.first, bind.second(end));
        }
    }

    if (!q)
        return;

    q.synchronous(binds);

    qCDebug(log_component_output_sqldb) << "Purge completed on" << Logging::range(start, end);
}

void SQLTableTarget::flushBuffer(bool last)
{
    if (bufferedSize <= 0)
        return;
    if (!db)
        return;

    double startProcessing = Time::time();
    qCDebug(log_component_output_sqldb) << "Writing" << bufferedSize << "row(s) to the database";

    if (!db->synchronousTransaction([&]() -> Database::Connection::SynchronousResult {
        if (parent->testTerminated())
            return Database::Connection::Synchronous_Abort;

        for (const auto &query : preWrite) {
            db->general(query).synchronous();
        }

        executePurge(bufferBegin, bufferEnd);

        {
            Database::Types::BatchBinds binds;
            for (std::size_t i = 0, max = columns.size(); i < max; i++) {
                binds.emplace(columnNames[i], bufferedData[i]);
            }
            query.batch(binds);
        }

        for (const auto &query : postWrite) {
            db->general(query).synchronous();
        }

        return Database::Connection::Synchronous_Ok;
    }, Database::Connection::Transaction_Default, last ? FP::undefined() : 120.0)) {
        double endProcessing = Time::time();
        qCWarning(log_component_output_sqldb) << "Database write failed after"
                                              << Logging::elapsed(endProcessing - startProcessing);
        return;
    }

    for (std::size_t i = 0, max = columns.size(); i < max; i++) {
        bufferedData[i].clear();
    }
    bufferedSize = 0;

    double endProcessing = Time::time();
    qCDebug(log_component_output_sqldb) << "Writing rows finished after "
                                        << Logging::elapsed(endProcessing - startProcessing);
}

void SQLTableTarget::processSegment(SequenceSegment &&segment)
{
    if (!FP::defined(segment.getStart()) && !allowUndefinedStart->get(segment))
        return;
    if (!FP::defined(segment.getEnd()) && !allowUndefinedEnd->get(segment))
        return;
    for (std::size_t i = 0, max = columns.size(); i < max; i++) {
        bufferedData[i].emplace_back(columns[i]->processSegment(segment));
    }

    if (bufferedSize == 0) {
        bufferBegin = segment.getStart();
    }
    bufferEnd = segment.getEnd();
    ++bufferedSize;
}

void SQLTableTarget::processSegments(SequenceSegment::Transfer &&segments)
{
    for (auto &s : segments) {
        processSegment(std::move(s));

        if (bufferedSize >= 16384)
            flushBuffer();
    }
}

void SQLTableTarget::processData(SequenceValue::Transfer &&values)
{
    for (auto &add : values) {
        if (FP::defined(dataStart) && (!FP::defined(add.getStart()) || add.getStart() < dataStart))
            add.setStart(dataStart);
        if (FP::defined(dataEnd) && (!FP::defined(add.getEnd()) || add.getEnd() > dataEnd))
            add.setEnd(dataEnd);
        if (FP::defined(add.getStart()) &&
                FP::defined(add.getEnd()) &&
                add.getStart() >= add.getEnd())
            continue;
        if (!dispatch.count(add.getUnit())) {
            for (const auto &column : columns) {
                column->registerInput(add.getUnit());
            }
        }
        processSegments(reader.add(std::move(add)));
    }
}

void SQLTableTarget::process()
{
    SequenceValue::Transfer toProcess;
    bool processEnd = false;
    {
        std::unique_lock<std::mutex> locker(parent->mutex);
        toProcess = std::move(pendingProcessing);
        pendingProcessing.clear();
        processEnd = pendingEnd;
        pendingEnd = false;
    }
    if (toProcess.size() > stallThreshold)
        parent->notify.notify_all();

    if (!toProcess.empty()) {
        processData(std::move(toProcess));
    }

    if (processEnd) {
        processSegments(reader.finish());
        flushBuffer(true);
    }
}

void SQLTableTarget::incomingData(const CPD3::Data::SequenceValue::Transfer &values)
{
    if (values.empty())
        return;
    {
        std::unique_lock<std::mutex> lock(parent->mutex);
        stall(lock);
        Util::append(values, pendingProcessing);
    }
    parent->notify.notify_all();
}

void SQLTableTarget::incomingData(CPD3::Data::SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;
    {
        std::unique_lock<std::mutex> lock(parent->mutex);
        stall(lock);
        Util::append(std::move(values), pendingProcessing);
    }
    parent->notify.notify_all();
}

void SQLTableTarget::incomingData(const CPD3::Data::SequenceValue &value)
{
    {
        std::unique_lock<std::mutex> lock(parent->mutex);
        stall(lock);
        pendingProcessing.emplace_back(value);
    }
    parent->notify.notify_all();
}

void SQLTableTarget::incomingData(CPD3::Data::SequenceValue &&value)
{
    {
        std::unique_lock<std::mutex> lock(parent->mutex);
        stall(lock);
        pendingProcessing.emplace_back(std::move(value));
    }
    parent->notify.notify_all();
}

void SQLTableTarget::stall(std::unique_lock<std::mutex> &lock)
{
    while (pendingProcessing.size() > stallThreshold) {
        if (parent->terminated)
            return;
        parent->notify.wait(lock);
    }
}

void SQLTableTarget::endData()
{
    {
        std::lock_guard<std::mutex> locker(parent->mutex);
        pendingEnd = true;
    }
    parent->notify.notify_all();
}

bool SQLTableTarget::isValid()
{
    return !columns.empty() && db;
}

void SQLTableTarget::connectToChain(ProcessingTapChain *chain)
{
    for (const auto &column : columns) {
        chainInput.append(column->requestedInputs(station));
    }
    chain->add(this, chainProcessing, chainInput);
}


OutputSQLDB::OutputSQLDB()
{ Q_ASSERT(false); }

OutputSQLDB::OutputSQLDB(const ComponentOptions &options,
                         double setStart,
                         double setEnd,
                         const std::vector<SequenceName::Component> &setStations) : profile(
        SequenceName::impliedProfile()),
                                                                                    stations(
                                                                                            setStations),
                                                                                    start(setStart),
                                                                                    end(setEnd),
                                                                                    terminated(
                                                                                            false),
                                                                                    mutex(),
                                                                                    notify(),
                                                                                    tables(),
                                                                                    chain(NULL)
{
    if (options.isSet("profile")) {
        profile = qobject_cast<ComponentOptionSingleString *>(options.get("profile"))->get()
                                                                                     .toLower()
                                                                                     .toStdString();
    }
}

OutputSQLDB::~OutputSQLDB()
{
    if (chain != NULL) {
        chain->signalTerminate();
        chain->wait();
        delete chain;
    }
    qDeleteAll(tables);
}

void OutputSQLDB::signalTerminate()
{
    {
        std::lock_guard<std::mutex> locker(mutex);
        terminated = true;
    }
    notify.notify_all();
    emit terminateRequested();
}

bool OutputSQLDB::testTerminated()
{
    std::lock_guard<std::mutex> locker(mutex);
    return terminated;
}

void OutputSQLDB::createTableTarget(const ValueSegment::Transfer &config,
                                    const SequenceName::Component &station)
{
    tables.append(new SQLTableTarget(this, config, station));
}

void OutputSQLDB::createTable(const ValueSegment::Transfer &config,
                              const SequenceName::Component &station)
{
    ValueSegment::Transfer active;
    for (const auto &add : config) {
        if (!add.getValue().exists()) {
            if (!active.empty()) {
                createTableTarget(active, station);
            }
            active.clear();
            continue;
        }
        if (!active.empty() &&
                !SQLTableTarget::canMerge(active.back().getValue(), add.getValue())) {
            createTableTarget(active, station);
            active.clear();
        }
        active.emplace_back(add);
    }
    if (!active.empty()) {
        createTableTarget(active, station);
    }
}

void OutputSQLDB::createStation(const ValueSegment::Transfer &config,
                                const SequenceName::Component &station)
{
    QSet<QString> tableNames;
    for (const auto &check : config) {
        for (const auto &add : check.getValue().toHash().keys()) {
            tableNames.insert(QString::fromStdString(add));
        }
    }
    tableNames.remove(QString());

    for (QSet<QString>::const_iterator table = tableNames.constBegin(), end = tableNames.constEnd();
            table != end;
            ++table) {
        createTable(ValueSegment::withPath(config, *table), station);
    }

    for (QList<SQLTableTarget *>::iterator t = tables.begin(); t != tables.end();) {
        if (!(*t)->isValid()) {
            delete *t;
            t = tables.erase(t);
            continue;
        }
        ++t;
    }
}

void OutputSQLDB::run()
{
    if (testTerminated())
        return;

    profile = Util::to_lower(std::move(profile));
    if (profile.empty()) {
        qCDebug(log_component_output_sqldb) << "Invalid profile";
        return;
    }

    double startProcessing = Time::time();

    SequenceSegment::Transfer globalConfiguration;
    {
        Archive::Access access;
        Archive::Access::ReadLock lock(access);

        auto allStations = access.availableStations(stations);
        if (allStations.empty()) {
            qCDebug(log_component_output_sqldb) << "No stations available";
            return;
        }
        stations.clear();
        std::copy(allStations.begin(), allStations.end(), Util::back_emplacer(stations));

        globalConfiguration = SequenceSegment::Stream::read(
                Archive::Selection(start, end, stations, {"configuration"}, {"sqldb"}), &access);
    }
    if (stations.empty())
        return;

    if (testTerminated())
        return;

    std::sort(stations.begin(), stations.end());

    qCDebug(log_component_output_sqldb) << "Starting output for" << stations.size() << "station(s)";

    for (const auto &station : stations) {
        if (testTerminated())
            break;

        SequenceName stationUnit(station, "configuration", "sqldb");
        ValueSegment::Transfer stationConfig;
        for (auto &add : globalConfiguration) {
            double s = add.getStart();
            if (Range::compareStart(s, start) < 0)
                s = start;
            double e = add.getEnd();
            if (Range::compareEnd(e, end) > 0)
                e = end;
            if (Range::compareStartEnd(s, e) >= 0)
                continue;
            auto v = add.takeValue(stationUnit)
                        .read()
                        .getPath(QString("Profiles/%1").arg(QString::fromStdString(profile)));
            stationConfig.emplace_back(s, e, Variant::Root(v));
        }
        if (stationConfig.empty())
            continue;

        feedback.emitStage(tr("Start %1").arg(QString::fromStdString(station).toUpper()),
                           tr("The system is starting the output for %1.").arg(
                                   QString::fromStdString(station).toUpper()), false);

        qCDebug(log_component_output_sqldb) << "Creating output for" << station;

        createStation(stationConfig, station);


        if (tables.isEmpty()) {
            feedback.emitFailure();
            qCDebug(log_component_output_sqldb) << "No table targets available for" << station;
            continue;
        }

        chain = new ProcessingTapChain;
        chain->setDefaultSelection(start, end, {station});

        for (QList<SQLTableTarget *>::const_iterator target = tables.constBegin(),
                end = tables.constEnd(); target != end; ++target) {
            (*target)->connectToChain(chain);
        }

        chain->feedback.forward(feedback);
        connect(this, SIGNAL(terminateRequested()), chain, SLOT(signalTerminate()),
                Qt::DirectConnection);

        chain->start();
        if (testTerminated())
            break;

        std::unique_lock<std::mutex> lock(mutex);
        for (;;) {
            lock.unlock();

            for (QList<SQLTableTarget *>::const_iterator t = tables.constBegin(),
                    endT = tables.constEnd(); t != endT; ++t) {
                (*t)->process();
            }

            lock.lock();
            if (terminated)
                break;
            if (chain->isFinished())
                break;
            notify.wait_for(lock, std::chrono::milliseconds(500));
        }
        if (terminated) {
            lock.unlock();
            chain->signalTerminate();
        } else {
            lock.unlock();
        }

        chain->wait();
        delete chain;
        chain = NULL;

        for (QList<SQLTableTarget *>::const_iterator t = tables.constBegin(),
                endT = tables.constEnd(); t != endT; ++t) {
            (*t)->process();
        }
        qDeleteAll(tables);
        tables.clear();
    }

    if (chain != NULL) {
        chain->signalTerminate();
        chain->wait();
        delete chain;
        chain = NULL;
    }
    qDeleteAll(tables);
    tables.clear();

    double endProcessing = Time::time();
    qCDebug(log_component_output_sqldb) << "Finished output after"
                                        << Logging::elapsed(endProcessing - startProcessing);

    QCoreApplication::processEvents();
}


ComponentOptions OutputSQLDBComponent::getOptions()
{
    ComponentOptions options;

    options.add("profile",
                new ComponentOptionSingleString(tr("profile", "name"), tr("Execution profile"),
                                                tr("This is the profile name to output.  Multiple profiles can be "
                                                   "defined to specify different sets of data that can be output "
                                                   "independently.  Consult the station configuration for available profiles."),
                                                tr("aerosol")));

    return options;
}

QList<ComponentExample> OutputSQLDBComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Output SQL data", "default example name"),
                                     tr("This will output data for the default \"aerosol\" profile.")));

    return examples;
}

int OutputSQLDBComponent::actionRequireStations()
{ return 1; }

int OutputSQLDBComponent::actionAllowStations()
{ return INT_MAX; }

bool OutputSQLDBComponent::actionRequiresTime()
{ return true; }

CPD3Action *OutputSQLDBComponent::createTimeAction(const ComponentOptions &options,
                                                   double start,
                                                   double end,
                                                   const std::vector<std::string> &stations)
{ return new OutputSQLDB(options, start, end, stations); }
