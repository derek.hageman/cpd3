/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QTemporaryFile>

#include "datacore/processingstage.hxx"
#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "datacore/stream.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/archive/access.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class TestComponent : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;
    ProcessingStageComponent *component;

    static bool valuesEqual(const Variant::Read &a, const Variant::Read &b)
    {
        if (a.getType() != Variant::Type::Real)
            return a == b;
        if (b.getType() != Variant::Type::Real)
            return a == b;
        double va = a.toDouble();
        double vb = b.toDouble();
        if (!FP::defined(va))
            return !FP::defined(vb);
        if (!FP::defined(vb))
            return false;
        double eps = qMax(fabs(va), fabs(vb)) * 1E-6;
        if (eps == 0)
            return va == vb;
        return fabs(va - vb) < eps;
    }

    static bool testCompare(SequenceValue::Transfer values, SequenceValue::Transfer expected)
    {
        for (auto f = expected.begin(); f != expected.end();) {
            bool hit = false;
            for (auto c = values.begin(), endC = values.end(); c != endC; ++c) {
                if (fabs(c->getStart() - f->getStart()) > 1E-6)
                    continue;
                if (fabs(c->getEnd() - f->getEnd()) > 1E-6)
                    continue;
                if (c->getUnit() != f->getUnit())
                    continue;

                auto merged = Variant::Root::overlay(c->root(), f->root());
                if (!valuesEqual(merged, c->getValue())) {
                    qDebug() << "Value overlay mismatch for" << *f << ".  Merged:" << merged
                             << "Input: " << c->getValue();
                    return false;
                }

                values.erase(c);
                f = expected.erase(f);
                hit = true;
                break;
            }
            if (!hit)
                ++f;
        }
        if (!values.empty()) {
            qDebug() << "Unmatched values in result:" << values;
        }
        if (!expected.empty()) {
            qDebug() << "Unmatched values in expected:" << expected;
        }
        return values.empty() && expected.empty();
    }

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component =
                qobject_cast<ProcessingStageComponent *>(ComponentLoader::create("smooth_segment"));
        QVERIFY(component);

        QVERIFY(databaseFile.open());
        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());

        Variant::Root config;
        config["Affected/aerosol/Variable"].setString("T_S11");

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"_", "configuration", "contamination"}, config)});
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("segment")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("output")));
        QVERIFY(qobject_cast<TimeIntervalSelectionOption *>(options.get("gap")));
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("contamination")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("continuous")));
        QVERIFY(qobject_cast<DynamicDoubleOption *>(options.get("cover")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("discard-intersecting")));
    }

    void conventional()
    {
        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("segment"))->set("brw", "raw",
                                                                                    "ZZero_S11");

        ProcessingStage *filter = component->createGeneralFilterDynamic(options);
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName flags("brw", "raw", "F1_S11");
        SequenceName value("brw", "raw", "T_S11");
        SequenceName zero("brw", "raw", "ZZero_S11");

        filter->incomingData(SequenceValue(flags, Variant::Root(Variant::Flags()), 100.0, 110.0));
        filter->incomingData(SequenceValue(zero, Variant::Root(-1.0), 100.0, 110.0));
        filter->incomingData(SequenceValue(value, Variant::Root(1.0), 100.0, 105.0));
        filter->incomingData(SequenceValue(value, Variant::Root(2.0), 105.0, 110.0));
        filter->incomingData(
                SequenceValue(flags, Variant::Root(Variant::Flags{"Contaminated"}), 110.0, 120.0));
        filter->incomingData(SequenceValue(value, Variant::Root(3.0), 110.0, 120.0));
        filter->incomingData(SequenceValue(zero, Variant::Root(-2.0), 110.0, 120.0));
        filter->setEgress(NULL);

        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            filter->serialize(stream);
        }
        filter->signalTerminate();
        QVERIFY(filter->wait(30));
        delete filter;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            filter = component->deserializeGeneralFilter(stream);
            QVERIFY(stream.atEnd());
        }
        QVERIFY(filter != NULL);
        filter->start();
        filter->setEgress(&e);

        filter->incomingData(
                SequenceValue(flags, Variant::Root(Variant::Flags{"Contaminated"}), 120.0, 130.0));
        filter->incomingData(SequenceValue(zero, Variant::Root(-3.0), 120.0, 130.0));
        filter->incomingData(SequenceValue(value, Variant::Root(4.0), 120.0, 130.0));
        filter->incomingData(SequenceValue(zero, Variant::Root(-4.0), 130.0, 140.0));
        filter->incomingData(SequenceValue(flags, Variant::Root(Variant::Flags()), 130.0, 140.0));
        filter->incomingData(SequenceValue(value, Variant::Root(5.0), 130.0, 135.0));
        filter->incomingData(SequenceValue(value, Variant::Root(6.0), 135.0, 140.0));
        filter->endData();

        QVERIFY(filter->wait(30));
        delete filter;

        Variant::Root stats(Variant::Type::Hash);

        QVERIFY(testCompare(e.values(), SequenceValue::Transfer{
                SequenceValue(zero, Variant::Root(-1.0), 100.0, 110.0),
                SequenceValue(zero, Variant::Root(-2.0), 110.0, 120.0),
                SequenceValue(zero, Variant::Root(-3.0), 120.0, 130.0),
                SequenceValue(zero, Variant::Root(-4.0), 130.0, 140.0),
                SequenceValue(zero.withFlavor("stats"), stats,
                                                                  100.0, 110.0),
                SequenceValue(zero.withFlavor("stats"), stats,
                                                                  110.0, 120.0),
                SequenceValue(zero.withFlavor("stats"), stats,
                                                                  120.0, 130.0),
                SequenceValue(zero.withFlavor("stats"), stats,
                                                                  130.0, 140.0),
                SequenceValue(flags, Variant::Root(Variant::Flags()), 100.0,
                              110.0), SequenceValue(flags,
                                                    Variant::Root(Variant::Flags{"Contaminated"}),
                                                    110.0,
                                                    120.0),
                SequenceValue(flags, Variant::Root(Variant::Flags{"Contaminated"}),
                              120.0, 130.0),
                SequenceValue(flags, Variant::Root(Variant::Flags()), 130.0,
                              140.0), SequenceValue(value, Variant::Root(1.5), 100.0, 110.0),
                SequenceValue(value, Variant::Root(FP::undefined()), 110.0,
                                                                  120.0),
                SequenceValue(value, Variant::Root(FP::undefined()), 120.0,
                              130.0), SequenceValue(value, Variant::Root(5.5), 130.0, 140.0),
                SequenceValue(value.withFlavor("stats"), stats,
                                                                  100.0, 110.0),
                SequenceValue(value.withFlavor("stats"), stats,
                                                                  110.0, 120.0),
                SequenceValue(value.withFlavor("stats"), stats,
                                                                  120.0, 130.0),
                SequenceValue(value.withFlavor("stats"), stats,
                                                                  130.0, 140.0)}));
    }

    void continuous()
    {
        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("segment"))->set("brw", "raw",
                                                                                    "ZZero_S11");
        qobject_cast<ComponentOptionBoolean *>(options.get("continuous"))->set(true);

        ProcessingStage *filter = component->createGeneralFilterDynamic(options);
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName flags("brw", "raw", "F1_S11");
        SequenceName value("brw", "raw", "T_S11");
        SequenceName zero("brw", "raw", "ZZero_S11");

        filter->incomingData(SequenceValue(zero, Variant::Root(-1.0), 3600.0, 7200.0));
        filter->incomingData(SequenceValue(flags, Variant::Root(Variant::Flags()), 3600.0, 7200.0));
        filter->incomingData(SequenceValue(value, Variant::Root(1.0), 3600.0, 5400.0));
        filter->incomingData(SequenceValue(value, Variant::Root(2.0), 5400.0, 7200.0));
        filter->incomingData(
                SequenceValue(flags, Variant::Root(Variant::Flags{"Contaminated"}), 7200.0,
                              10800.0));
        filter->incomingData(SequenceValue(value, Variant::Root(3.0), 7200.0, 10800.0));
        filter->incomingData(SequenceValue(zero, Variant::Root(-2.0), 7200.0, 10800.0));
        filter->setEgress(NULL);

        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            filter->serialize(stream);
        }
        filter->signalTerminate();
        QVERIFY(filter->wait(30));
        delete filter;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            filter = component->deserializeGeneralFilter(stream);
            QVERIFY(stream.atEnd());
        }
        QVERIFY(filter != NULL);
        filter->start();
        filter->setEgress(&e);

        filter->incomingData(
                SequenceValue(flags, Variant::Root(Variant::Flags{"Contaminated"}), 10800.0,
                              14400.0));
        filter->incomingData(SequenceValue(zero, Variant::Root(-3.0), 10800.0, 14400.0));
        filter->incomingData(SequenceValue(value, Variant::Root(4.0), 10800.0, 14400.0));
        filter->incomingData(
                SequenceValue(flags, Variant::Root(Variant::Flags()), 14400.0, 18000.0));
        filter->incomingData(SequenceValue(zero, Variant::Root(-4.0), 14400.0, 18000.0));
        filter->incomingData(SequenceValue(value, Variant::Root(5.0), 14400.0, 16200.0));
        filter->incomingData(SequenceValue(value, Variant::Root(6.0), 16200.0, 18000.0));
        filter->incomingData(SequenceValue(value, Variant::Root(7.0), 18000.0, 19799.0));
        filter->incomingData(SequenceValue(zero, Variant::Root(-5.0), 18000.0, 21600.0));
        filter->incomingData(SequenceValue(value, Variant::Root(8.0), 19800.0, 21600.0));
        filter->endData();

        QVERIFY(filter->wait(30));
        delete filter;

        Variant::Root stats(Variant::Type::Hash);

        QVERIFY(testCompare(e.values(), SequenceValue::Transfer{
                SequenceValue(zero, Variant::Root(-1.0), 3600.0, 7200.0),
                SequenceValue(zero, Variant::Root(-2.0), 7200.0, 10800.0),
                SequenceValue(zero, Variant::Root(-3.0), 10800.0, 14400.0),
                SequenceValue(zero, Variant::Root(-4.0), 14400.0, 18000.0),
                SequenceValue(zero, Variant::Root(-5.0), 18000.0, 21600.0),
                SequenceValue(zero.withFlavor("stats"), stats,
                                                                  3600.0, 7200.0),
                SequenceValue(zero.withFlavor("stats"), stats,
                                                                  7200.0, 10800.0),
                SequenceValue(zero.withFlavor("stats"), stats,
                                                                  10800.0, 14400.0),
                SequenceValue(zero.withFlavor("stats"), stats,
                                                                  14400.0, 18000.0),
                SequenceValue(zero.withFlavor("stats"), stats,
                                                                  18000.0, 21600.0),
                SequenceValue(flags, Variant::Root(Variant::Flags()), 3600.0,
                              7200.0), SequenceValue(flags,
                                                     Variant::Root(Variant::Flags{"Contaminated"}),
                                                     7200.0,
                                                     10800.0),
                SequenceValue(flags, Variant::Root(Variant::Flags{"Contaminated"}),
                              10800.0, 14400.0),
                SequenceValue(flags, Variant::Root(Variant::Flags()), 14400.0,
                              18000.0), SequenceValue(value, Variant::Root(1.5), 3600.0, 7200.0),
                SequenceValue(value, Variant::Root(FP::undefined()), 7200.0,
                                                                  10800.0),
                SequenceValue(value, Variant::Root(FP::undefined()), 10800.0,
                              14400.0), SequenceValue(value, Variant::Root(5.5), 14400.0, 18000.0),
                SequenceValue(value, Variant::Root(7.0), 18000.0, 19799.0),
                SequenceValue(value, Variant::Root(8.0), 19800.0, 21600.0),
                SequenceValue(value.withFlavor("stats"), stats,
                                                                  3600.0, 7200.0),
                SequenceValue(value.withFlavor("stats"), stats,
                                                                  7200.0, 10800.0),
                SequenceValue(value.withFlavor("stats"), stats,
                                                                  10800.0, 14400.0),
                SequenceValue(value.withFlavor("stats"), stats,
                                                                  14400.0, 18000.0),
                SequenceValue(value.withFlavor("stats"), stats,
                                                                  18000.0, 19799.0),
                SequenceValue(value.withFlavor("stats"), stats,
                                                                  19800.0, 21600.0)}));
    }

    void contaminationRemovalDisabled()
    {
        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("segment"))->set("brw", "raw",
                                                                                    "ZZero_S11");
        qobject_cast<ComponentOptionSingleString *>(options.get("contamination"))->set(tr("none"));

        ProcessingStage *filter = component->createGeneralFilterDynamic(options);
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName flags("brw", "raw", "F1_S11");
        SequenceName value("brw", "raw", "T_S11");
        SequenceName zero("brw", "raw", "ZZero_S11");

        filter->incomingData(SequenceValue(flags, Variant::Root(Variant::Flags()), 100.0, 110.0));
        filter->incomingData(SequenceValue(zero, Variant::Root(-1.0), 100.0, 110.0));
        filter->incomingData(SequenceValue(value, Variant::Root(1.0), 100.0, 105.0));
        filter->incomingData(SequenceValue(value, Variant::Root(2.0), 105.0, 110.0));
        filter->incomingData(
                SequenceValue(flags, Variant::Root(Variant::Flags{"Contaminated"}), 110.0, 120.0));
        filter->incomingData(SequenceValue(value, Variant::Root(3.0), 110.0, 120.0));
        filter->incomingData(SequenceValue(zero, Variant::Root(-2.0), 110.0, 120.0));
        filter->setEgress(NULL);

        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            filter->serialize(stream);
        }
        filter->signalTerminate();
        QVERIFY(filter->wait(30));
        delete filter;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            filter = component->deserializeGeneralFilter(stream);
            QVERIFY(stream.atEnd());
        }
        QVERIFY(filter != NULL);
        filter->start();
        filter->setEgress(&e);

        filter->incomingData(
                SequenceValue(flags, Variant::Root(Variant::Flags{"Contaminated"}), 120.0, 130.0));
        filter->incomingData(SequenceValue(zero, Variant::Root(-3.0), 120.0, 130.0));
        filter->incomingData(SequenceValue(value, Variant::Root(4.0), 120.0, 130.0));
        filter->incomingData(SequenceValue(zero, Variant::Root(-4.0), 130.0, 140.0));
        filter->incomingData(SequenceValue(flags, Variant::Root(Variant::Flags()), 130.0, 140.0));
        filter->incomingData(SequenceValue(value, Variant::Root(5.0), 130.0, 135.0));
        filter->incomingData(SequenceValue(value, Variant::Root(6.0), 135.0, 140.0));
        filter->endData();

        QVERIFY(filter->wait(30));
        delete filter;

        Variant::Root stats(Variant::Type::Hash);

        QVERIFY(testCompare(e.values(), SequenceValue::Transfer{
                SequenceValue(zero, Variant::Root(-1.0), 100.0, 110.0),
                SequenceValue(zero, Variant::Root(-2.0), 110.0, 120.0),
                SequenceValue(zero, Variant::Root(-3.0), 120.0, 130.0),
                SequenceValue(zero, Variant::Root(-4.0), 130.0, 140.0),
                SequenceValue(zero.withFlavor("stats"), stats,
                                                                  100.0, 110.0),
                SequenceValue(zero.withFlavor("stats"), stats,
                                                                  110.0, 120.0),
                SequenceValue(zero.withFlavor("stats"), stats,
                                                                  120.0, 130.0),
                SequenceValue(zero.withFlavor("stats"), stats,
                                                                  130.0, 140.0),
                SequenceValue(flags, Variant::Root(Variant::Flags()), 100.0,
                              110.0), SequenceValue(flags,
                                                    Variant::Root(Variant::Flags{"Contaminated"}),
                                                    110.0,
                                                    120.0),
                SequenceValue(flags, Variant::Root(Variant::Flags{"Contaminated"}),
                              120.0, 130.0),
                SequenceValue(flags, Variant::Root(Variant::Flags()), 130.0,
                              140.0), SequenceValue(value, Variant::Root(1.5), 100.0, 110.0),
                SequenceValue(value, Variant::Root(3.0), 110.0, 120.0),
                SequenceValue(value, Variant::Root(4.0), 120.0, 130.0),
                SequenceValue(value, Variant::Root(5.5), 130.0, 140.0),
                SequenceValue(value.withFlavor("stats"), stats,
                                                                  100.0, 110.0),
                SequenceValue(value.withFlavor("stats"), stats,
                                                                  110.0, 120.0),
                SequenceValue(value.withFlavor("stats"), stats,
                                                                  120.0, 130.0),
                SequenceValue(value.withFlavor("stats"), stats,
                                                                  130.0, 140.0)}));
    }

    void continuousContaminationRemovalDisabled()
    {
        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("segment"))->set("brw", "raw",
                                                                                    "ZZero_S11");
        qobject_cast<ComponentOptionSingleString *>(options.get("contamination"))->set(tr("none"));
        qobject_cast<ComponentOptionBoolean *>(options.get("continuous"))->set(true);

        ProcessingStage *filter = component->createGeneralFilterDynamic(options);
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName flags("brw", "raw", "F1_S11");
        SequenceName value("brw", "raw", "T_S11");
        SequenceName zero("brw", "raw", "ZZero_S11");

        filter->incomingData(SequenceValue(zero, Variant::Root(-1.0), 3600.0, 7200.0));
        filter->incomingData(SequenceValue(flags, Variant::Root(Variant::Flags()), 3600.0, 7200.0));
        filter->incomingData(SequenceValue(value, Variant::Root(1.0), 3600.0, 5400.0));
        filter->incomingData(SequenceValue(value, Variant::Root(2.0), 5400.0, 7200.0));
        filter->incomingData(
                SequenceValue(flags, Variant::Root(Variant::Flags{"Contaminated"}), 7200.0,
                              10800.0));
        filter->incomingData(SequenceValue(value, Variant::Root(3.0), 7200.0, 10800.0));
        filter->incomingData(SequenceValue(zero, Variant::Root(-2.0), 7200.0, 10800.0));
        filter->setEgress(NULL);

        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            filter->serialize(stream);
        }
        filter->signalTerminate();
        QVERIFY(filter->wait(30));
        delete filter;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            filter = component->deserializeGeneralFilter(stream);
            QVERIFY(stream.atEnd());
        }
        QVERIFY(filter != NULL);
        filter->start();
        filter->setEgress(&e);

        filter->incomingData(
                SequenceValue(flags, Variant::Root(Variant::Flags{"Contaminated"}), 10800.0,
                              14400.0));
        filter->incomingData(SequenceValue(zero, Variant::Root(-3.0), 10800.0, 14400.0));
        filter->incomingData(SequenceValue(value, Variant::Root(4.0), 10800.0, 14400.0));
        filter->incomingData(
                SequenceValue(flags, Variant::Root(Variant::Flags()), 14400.0, 18000.0));
        filter->incomingData(SequenceValue(zero, Variant::Root(-4.0), 14400.0, 18000.0));
        filter->incomingData(SequenceValue(value, Variant::Root(5.0), 14400.0, 16200.0));
        filter->incomingData(SequenceValue(value, Variant::Root(6.0), 16200.0, 18000.0));
        filter->incomingData(SequenceValue(value, Variant::Root(7.0), 18000.0, 19799.0));
        filter->incomingData(SequenceValue(zero, Variant::Root(-5.0), 18000.0, 21600.0));
        filter->incomingData(SequenceValue(value, Variant::Root(8.0), 19800.0, 21600.0));
        filter->endData();

        QVERIFY(filter->wait(30));
        delete filter;

        Variant::Root stats(Variant::Type::Hash);

        QVERIFY(testCompare(e.values(), SequenceValue::Transfer{
                SequenceValue(zero, Variant::Root(-1.0), 3600.0, 7200.0),
                SequenceValue(zero, Variant::Root(-2.0), 7200.0, 10800.0),
                SequenceValue(zero, Variant::Root(-3.0), 10800.0, 14400.0),
                SequenceValue(zero, Variant::Root(-4.0), 14400.0, 18000.0),
                SequenceValue(zero, Variant::Root(-5.0), 18000.0, 21600.0),
                SequenceValue(zero.withFlavor("stats"), stats,
                                                                  3600.0, 7200.0),
                SequenceValue(zero.withFlavor("stats"), stats,
                                                                  7200.0, 10800.0),
                SequenceValue(zero.withFlavor("stats"), stats,
                                                                  10800.0, 14400.0),
                SequenceValue(zero.withFlavor("stats"), stats,
                                                                  14400.0, 18000.0),
                SequenceValue(zero.withFlavor("stats"), stats,
                                                                  18000.0, 21600.0),
                SequenceValue(flags, Variant::Root(Variant::Flags()), 3600.0,
                              7200.0), SequenceValue(flags,
                                                     Variant::Root(Variant::Flags{"Contaminated"}),
                                                     7200.0,
                                                     10800.0),
                SequenceValue(flags, Variant::Root(Variant::Flags{"Contaminated"}),
                              10800.0, 14400.0),
                SequenceValue(flags, Variant::Root(Variant::Flags()), 14400.0,
                              18000.0), SequenceValue(value, Variant::Root(1.5), 3600.0, 7200.0),
                SequenceValue(value, Variant::Root(3.0), 7200.0, 10800.0),
                SequenceValue(value, Variant::Root(4.0), 10800.0, 14400.0),
                SequenceValue(value, Variant::Root(5.5), 14400.0, 18000.0),
                SequenceValue(value, Variant::Root(7.0), 18000.0, 19799.0),
                SequenceValue(value, Variant::Root(8.0), 19800.0, 21600.0),
                SequenceValue(value.withFlavor("stats"), stats,
                                                                  3600.0, 7200.0),
                SequenceValue(value.withFlavor("stats"), stats,
                                                                  7200.0, 10800.0),
                SequenceValue(value.withFlavor("stats"), stats,
                                                                  10800.0, 14400.0),
                SequenceValue(value.withFlavor("stats"), stats,
                                                                  14400.0, 18000.0),
                SequenceValue(value.withFlavor("stats"), stats,
                                                                  18000.0, 19799.0),
                SequenceValue(value.withFlavor("stats"), stats,
                                                                  19800.0, 21600.0)}));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
