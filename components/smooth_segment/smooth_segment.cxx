/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/component.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "datacore/archive/access.hxx"
#include "smoothing/contamfilter.hxx"
#include "smoothing/smoothingengine.hxx"
#include "smoothing/segmentcontrolled.hxx"

#include "smooth_segment.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Smoothing;


QString SmoothSegmentComponent::getGeneralSerializationName() const
{ return QString::fromLatin1("smooth_segment"); }

ComponentOptions SmoothSegmentComponent::getOptions()
{
    ComponentOptions options;

    options.add("contamination", new ComponentOptionSingleString(tr("contamination", "name"),
                                                                 tr("The contamination mode"),
                                                                 tr("This defines the contamination mode in effect.  The contamination "
                                                                            "mode determines which variables are removed when they are flagged "
                                                                            "as contaminated.  For example the \"aerosol\" mode generally "
                                                                            "removes scattering, absorption, extinction and concentrations.  "
                                                                            "The special mode \"none\" or \"disable\" turns off all "
                                                                            "contamination removal and values are averaged regardless of their "
                                                                            "contaminated state."),
                                                                 tr("aerosol",
                                                                    "default contamination mode")));

    options.add("segment", new DynamicSequenceSelectionOption(tr("segment", "name"),
                                                              tr("The averaging segment"),
                                                              tr("This defines the data input to create segment averages on.  The "
                                                   "resulting averages are output for any times that this exists.  "
                                                   "If there are overlaps they are fragmented into multiple segment "
                                                   "averages."), QString(), 1));

    options.add("output", new DynamicSequenceSelectionOption(tr("output", "name"),
                                                             tr("The output segment name"),
                                                             tr("If this is set then the final output segments are written out "
                                                   "as the variable or variables that this defines."),
                                                             QString()));

    TimeIntervalSelectionOption *tis =
            new TimeIntervalSelectionOption(tr("gap", "name"), tr("The maximum allowed gap"),
                                            tr("If two values are seperated by this much time then the average "
                                                       "is split.  Even if there is no gap the values may still be split "
                                                       "into separate bins if the interval demands it.  An undefined gap "
                                                       "allows for infinite separation."),
                                            tr("Infinite", "default gap"));
    tis->setAllowZero(true);
    tis->setAllowNegative(false);
    tis->setAllowUndefined(true);
    tis->setDefaultAligned(true);
    options.add("gap", tis);

    options.add("continuous", new ComponentOptionBoolean(tr("continuous", "name"),
                                                         tr("Produce only continuous averages"),
                                                         tr("If this option is set then the averages produced are the maximum "
                                                                    "continuous averages within the binning interval.  This allows "
                                                                    "difference measurements to be repeatable but may cause splitting "
                                                                    "within the interval."),
                                                         tr("Disabled",
                                                            "default continuous mode")));
    options.exclude("gap", "continuous");
    options.exclude("continuous", "gap");

    DynamicDoubleOption *fi = new DynamicDoubleOption(tr("cover", "name"),
                                                      tr("The fraction of data required to exist"),
                                                      tr("This is the fraction of data required to exist for an average "
                                                                     "bin to be produced at all.  For example if this is set to 0.95 "
                                                                     "then averages will only be produced when the fraction of "
                                                                     "missing data in a bin is greater than 95% of the total bin.  So, "
                                                                     "for a one hour bin that would require at least 57 minutes of "
                                                                     "valid data.  Setting this to undefined disables the requirement."),
                                                      tr("Disabled", "default cover fraction"));
    options.add("cover", fi);

    options.add("discard-intersecting",
                new ComponentOptionBoolean(tr("discard-intersecting", "name"),
                                           tr("Discard all values that are not completely within segments"),
                                           tr("If this option is set then all input data that is not completely "
                                                      "contained within an averaging segment is discard and does not "
                                                      "contribute to that segment.  That is, any value that overlaps "
                                                      "the start or end of the segment will not be used."),
                                           tr("Disabled; include intersecting data",
                                              "default discard intersecting mode")));

    return options;
}

QList<ComponentExample> SmoothSegmentComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    (qobject_cast<DynamicSequenceSelectionOption *>(options.get("segment")))->set("", "",
                                                                                  "ZZero_S11");
    examples.append(ComponentExample(options, tr("Default zero averages", "default example name"),
                                     tr("This will average all data available during the neph zero.  It "
                                                "will also remove contamination from the standard set of "
                                                "variables involved in an aerosol system (generally scattering, "
                                                "absorption, extinction and concentrations).")));

    options = getOptions();
    (qobject_cast<DynamicSequenceSelectionOption *>(options.get("segment")))->set("", "", "F2_S11");
    (qobject_cast<DynamicDoubleOption *>(options.get("cover")))->set(0.95);
    (qobject_cast<ComponentOptionSingleString *>(options.get("contamination")))->set(
            tr("none", "contamination none"));
    examples.append(ComponentExample(options, tr("Mode and coverage requirement"),
                                     tr("This will average data to whenever the neph has changed operating "
                                                "modes while requiring at least 95% of data within each segment "
                                                "to exist.  Contaminated data is not excluded from the "
                                                "averages.")));

    return examples;
}

enum SerializeMode {
    ContaminateFilter_Conventional = 0,
    ContaminateFilter_Continuous,
    Unfiltered_Conventional,
    Unfiltered_Continuous,
};

ProcessingStage *SmoothSegmentComponent::createGeneralFilterDynamic(const ComponentOptions &options)
{
    return createGeneralFilterPredefined(options, FP::undefined(), FP::undefined(),
                                         QList<SequenceName>());
}

class SegmentSmoothingEngine : public SmoothingEngineSegmentControlled {
    SerializeMode mode;
public:
    SegmentSmoothingEngine(SerializeMode sm,
                           DynamicSequenceSelection *segmentInput,
                           DynamicSequenceSelection *output,
                           bool intersectingDiscard = false, DynamicTimeInterval *setGap = NULL,
                           DynamicDouble *setRequiredCover = NULL,
                           bool setGapPreserving = false) : SmoothingEngineSegmentControlled(
            segmentInput, output, intersectingDiscard, setGap, setRequiredCover, setGapPreserving),
                                                            mode(sm)
    { }

    ~SegmentSmoothingEngine()
    { }

    virtual void serialize(QDataStream &stream)
    {
        stream << (quint8) mode;
        SmoothingEngineSegmentControlled::serialize(stream);
    }
};

ProcessingStage *SmoothSegmentComponent::createGeneralFilterPredefined(const ComponentOptions &options,
                                                                       double start,
                                                                       double end,
                                                                       const QList<
                                                                             SequenceName> &inputs)
{
    Q_UNUSED(start);
    Q_UNUSED(end);

    DynamicSequenceSelection *segment =
            qobject_cast<DynamicSequenceSelectionOption *>(options.get("segment"))->getOperator();

    DynamicSequenceSelection *output;
    if (options.isSet("output")) {
        output = qobject_cast<DynamicSequenceSelectionOption *>(
                options.get("output"))->getOperator();
    } else {
        output = NULL;
    }

    bool discardIntersecting = false;
    if (options.isSet("discard-intersecting")) {
        discardIntersecting =
                qobject_cast<ComponentOptionBoolean *>(options.get("discard-intersecting"))->get();
    }


    DynamicDouble *requiredCover;
    if (options.isSet("cover")) {
        requiredCover = qobject_cast<DynamicDoubleOption *>(options.get("cover"))->getInput();
    } else {
        requiredCover = NULL;
    }

    QString contamMode("aerosol");
    if (options.isSet("contamination")) {
        contamMode =
                qobject_cast<ComponentOptionSingleString *>(options.get("contamination"))->get()
                                                                                         .toLower();
        if (contamMode == tr("none", "contamination none") ||
                contamMode == tr("disable", "contamination disable")) {
            contamMode.clear();
        }
    }

    bool cont = false;
    if (options.isSet("continuous")) {
        cont = qobject_cast<ComponentOptionBoolean *>(options.get("continuous"))->get();
    }

    SmoothingEngineSegmentControlled *engine;
    if (cont) {
        engine = new SegmentSmoothingEngine(
                contamMode.isEmpty() ? Unfiltered_Continuous : ContaminateFilter_Continuous,
                segment, output, discardIntersecting, NULL, requiredCover, true);
    } else {
        DynamicTimeInterval *gap;
        if (options.isSet("gap")) {
            gap = qobject_cast<TimeIntervalSelectionOption *>(options.get("gap"))->getInterval();
        } else {
            gap = NULL;
        }

        engine = new SegmentSmoothingEngine(
                contamMode.isEmpty() ? Unfiltered_Conventional : ContaminateFilter_Conventional,
                segment, output, discardIntersecting, gap, requiredCover, false);
    }

    if (!inputs.isEmpty()) {
        engine->registerExpectedInputs(QSet<SequenceName>::fromList(inputs));
    }

    if (contamMode.isEmpty())
        return engine;

    return new SmoothingContaminationFilter(engine, contamMode);
}

ProcessingStage *SmoothSegmentComponent::createGeneralFilterEditing(double start,
                                                                    double end,
                                                                    const SequenceName::Component &station,
                                                                    const SequenceName::Component &archive,
                                                                    const ValueSegment::Transfer &config)
{
    Q_UNUSED(station);
    Q_UNUSED(archive);

    DynamicSequenceSelection
            *segment = DynamicSequenceSelection::fromConfiguration(config, "Segment", start, end);
    DynamicSequenceSelection
            *output = DynamicSequenceSelection::fromConfiguration(config, "Output", start, end);
    DynamicTimeInterval *gap = DynamicTimeInterval::fromConfiguration(config, "Gap", start, end);
    DynamicDouble *requiredCover =
            DynamicDoubleOption::fromConfiguration(config, "RequiredCoverage", start, end);

    return new SegmentSmoothingEngine(Unfiltered_Conventional, segment, output, false, gap,
                                      requiredCover, false);
}

void SmoothSegmentComponent::extendGeneralFilterEditing(double &start,
                                                        double &end,
                                                        const SequenceName::Component &station,
                                                        const SequenceName::Component &archive,
                                                        const ValueSegment::Transfer &config,
                                                        Archive::Access *access)
{
    DynamicSequenceSelection
            *segment = DynamicSequenceSelection::fromConfiguration(config, "Segment", start, end);
    segment->registerExpected(station, archive);
    auto units = segment->getAllUnits();
    delete segment;

    Archive::Selection::List selections;
    if (FP::defined(start)) {
        for (const auto &add : units) {
            selections.push_back(Archive::Selection(add, start, start + 1).withMetaArchive(false));
        }
    }
    if (FP::defined(end)) {
        for (const auto &add : units) {
            selections.push_back(Archive::Selection(add, end - 1, end).withMetaArchive(false));
        }
    }
    if (selections.empty())
        return;

    std::unique_ptr<Archive::Access> localAccess;
    if (!access) {
        localAccess.reset(new Archive::Access);
        access = localAccess.get();
    }

    StreamSink::Iterator data;
    access->readStream(selections, &data);
    while (data.hasNext()) {
        auto value = data.next();

        if (FP::defined(start)) {
            if (!FP::defined(value.getStart()) || value.getStart() < start)
                start = value.getStart();
        }
        if (FP::defined(end)) {
            if (!FP::defined(value.getEnd()) || value.getEnd() > end)
                end = value.getEnd();
        }
    }
}

ProcessingStage *SmoothSegmentComponent::deserializeGeneralFilter(QDataStream &stream)
{
    quint8 i8;
    stream >> i8;

    SmoothingEngine *engine = new SegmentSmoothingEngine((SerializeMode) i8, NULL, NULL);
    engine->deserialize(stream);

    switch ((SerializeMode) i8) {
    case Unfiltered_Conventional:
    case Unfiltered_Continuous:
        return engine;
    case ContaminateFilter_Continuous:
    case ContaminateFilter_Conventional: {
        SmoothingContaminationFilter *filter = new SmoothingContaminationFilter(engine, QString());
        filter->deserialize(stream);
        return filter;
    }
    }
    Q_ASSERT(false);
    return NULL;
}
