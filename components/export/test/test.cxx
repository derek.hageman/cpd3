/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QBuffer>

#include "datacore/externalsink.hxx"
#include "datacore/externalsource.hxx"
#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "io/access.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace CPD3 {
namespace Data {
bool operator==(const SequenceValue &a, const SequenceValue &b)
{ return SequenceValue::ValueEqual()(a, b); };
}
}

class TestComponent : public QObject {
Q_OBJECT

    ExternalSinkComponent *component;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component = qobject_cast<ExternalSinkComponent *>(ComponentLoader::create("export"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionEnum *>(options.get("mode")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("station")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("time-epoch")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("time-excel")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("time-iso")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("time-fyear")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("time-julian")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("time-yeardoy")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("time-doy")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("cut-split")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("stddev")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("count")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("cover")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("quantiles")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("mean")));
        QVERIFY(qobject_cast<ComponentOptionEnum *>(options.get("join")));
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("join-delimiter")));
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("join-quote")));
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("join-quote-escape")));
        QVERIFY(qobject_cast<ComponentOptionEnum *>(options.get("mvc")));
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("mvc-all")));
        QVERIFY(qobject_cast<ComponentOptionEnum *>(options.get("mvc-flag")));
        QVERIFY(qobject_cast<ComponentOptionEnum *>(options.get("flags")));
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("format")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("numeric-only")));
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("header-mark")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("header-names")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("header-description")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("header-wavelength")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("header-mvcs")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("header-cut")));
        QVERIFY(qobject_cast<TimeIntervalSelectionOption *>(options.get("fill")));
        QVERIFY(qobject_cast<TimeIntervalSelectionOption *>(options.get("squash")));
#ifdef Q_OS_UNIX
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("header-names-stderr")));
#endif

        QVERIFY(options.excluded().value("time-yeardoy").contains("time-doy"));
        QVERIFY(options.excluded().value("time-doy").contains("time-yeardoy"));
        QVERIFY(options.excluded().value("mvc").contains("mvc-all"));
        QVERIFY(options.excluded().value("mvc-all").contains("mvc"));
    }

    void exportDefaults()
    {
        auto data = std::make_shared<Util::ByteArray>();
        ComponentOptions options(component->getOptions());
        std::unique_ptr<ExternalSink>
                exp(component->createDataSink(IO::Access::buffer(data)->stream(), options));
        QVERIFY(exp.get() != nullptr);
        exp->start();

        exp->start();

        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(12.0),
                                        1288293573.0, 1288293574.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(), 1288293574.0,
                              1288293575.0));
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(14.0),
                                        1288293575.0, 1288293576.0));
        exp->endData();
        QVERIFY(exp->wait());
        exp.reset();

        Util::ByteArray::LineIterator it(*data);
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("DateTimeUTC,BsB_S11,BsG_S11"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("2010-10-28 19:19:33,,00012.000"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("2010-10-28 19:19:34,,"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("2010-10-28 19:19:35,00014.000,"));

        /* Duplicate output regression check */
        while (auto line = it.next()) {
            QVERIFY(!line.toQByteArrayRef().contains("DateTimeUTC"));
        }
    }

    void exportXL()
    {
        auto data = std::make_shared<Util::ByteArray>();
        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("xl");
        std::unique_ptr<ExternalSink>
                exp(component->createDataSink(IO::Access::buffer(data)->stream(), options));
        QVERIFY(exp.get() != nullptr);
        exp->start();

        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(12.0),
                                        1288293573.0, 1288293574.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(), 1288293574.0,
                              1288293575.0));
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(14.0),
                                        1288293575.0, 1288293576.0));
        exp->endData();
        QVERIFY(exp->wait());
        exp.reset();

        Util::ByteArray::LineIterator it(*data);
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("DateTimeUTC,BsB_S11,BsG_S11"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("2010-10-28 19:19:33,,00012.000"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("2010-10-28 19:19:34,,"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("2010-10-28 19:19:35,00014.000,"));
    }

    void exportCSV()
    {
        auto data = std::make_shared<Util::ByteArray>();
        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("csv");
        std::unique_ptr<ExternalSink>
                exp(component->createDataSink(IO::Access::buffer(data)->stream(), options));
        QVERIFY(exp.get() != nullptr);
        exp->start();

        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(12.0),
                                        1288293573.0, 1288293574.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(), 1288293574.0,
                              1288293575.0));
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(14.0),
                                        1288293575.0, 1288293576.0));
        exp->endData();
        QVERIFY(exp->wait());
        exp.reset();

        Util::ByteArray::LineIterator it(*data);
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("Year,DOY,BsB_S11,BsG_S11"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("2010,301.80524,,00012.000"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("2010,301.80525,,"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("2010,301.80527,00014.000,"));
    }

    void exportR()
    {
        auto data = std::make_shared<Util::ByteArray>();
        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("r");
        std::unique_ptr<ExternalSink>
                exp(component->createDataSink(IO::Access::buffer(data)->stream(), options));
        QVERIFY(exp.get() != nullptr);
        exp->start();

        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(12.0),
                                        1288293573.0, 1288293574.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(), 1288293574.0,
                              1288293575.0));
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(14.0),
                                        1288293575.0, 1288293576.0));
        exp->endData();
        QVERIFY(exp->wait());
        exp.reset();

        Util::ByteArray::LineIterator it(*data);
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("EPOCH,BsB_S11,BsG_S11"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("1288293573,NA,00012.000"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("1288293574,NA,NA"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("1288293575,00014.000,NA"));
    }

    void exportIDL()
    {
        auto data = std::make_shared<Util::ByteArray>();
        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("idl");
#ifdef Q_OS_UNIX
        qobject_cast<ComponentOptionBoolean *>(options.get("header-names-stderr"))->set(false);
#endif
        std::unique_ptr<ExternalSink>
                exp(component->createDataSink(IO::Access::buffer(data)->stream(), options));
        QVERIFY(exp.get() != nullptr);
        exp->start();

        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(12.0),
                                        1288293573.0, 1288293574.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(), 1288293574.0,
                              1288293575.0));
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(14.0),
                                        1288293575.0, 1288293576.0));
        exp->endData();
        QVERIFY(exp->wait());
        exp.reset();

        Util::ByteArray::LineIterator it(*data);
        QCOMPARE(it.next().toQByteArrayRef(),
                 QByteArray("JulianDay Year DOY BsB_S11 BsB_S11_MVC BsG_S11 BsG_S11_MVC"));
        QCOMPARE(it.next().toQByteArrayRef(),
                 QByteArray("2455498.30524 2010 301.80524 99999.999 1 00012.000 0"));
        QCOMPARE(it.next().toQByteArrayRef(),
                 QByteArray("2455498.30525 2010 301.80525 99999.999 1 99999.999 1"));
        QCOMPARE(it.next().toQByteArrayRef(),
                 QByteArray("2455498.30527 2010 301.80527 00014.000 0 99999.999 1"));
    }

    void exportIDLLegacy()
    {
        auto data = std::make_shared<Util::ByteArray>();
        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("idllegacy");
        std::unique_ptr<ExternalSink>
                exp(component->createDataSink(IO::Access::buffer(data)->stream(), options));
        QVERIFY(exp.get() != nullptr);
        exp->start();

        Variant::Root meta;
        meta.write().metadataReal("Format").setString("0000.00");
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw_meta", "BsG_S11"), meta, 1288293573.0,
                              1288293576.0));
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(12.0),
                                        1288293573.0, 1288293574.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(), 1288293574.0,
                              1288293575.0));
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(14.0),
                                        1288293575.0, 1288293576.0));
        exp->endData();
        QVERIFY(exp->wait());
        exp.reset();

        Util::ByteArray::LineIterator it(*data);
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("Station,Year,DOY,BsB_S11,BsG_S11"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("BRW,2010,301.80524,99999.999,0012.00"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("BRW,2010,301.80525,99999.999,9999.99"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("BRW,2010,301.80527,00014.000,9999.99"));
    }

    void exportIDLLegacyStartYear()
    {
        auto data = std::make_shared<Util::ByteArray>();
        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("idllegacy");
        std::unique_ptr<ExternalSink>
                exp(component->createDataSink(IO::Access::buffer(data)->stream(), options));
        QVERIFY(exp.get() != nullptr);
        exp->start();

        Variant::Root meta;
        meta.write().metadataReal("Format").setString("0000.00");
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw_meta", "BsG_S11"), meta, 1356998400.0,
                              1357063200.0));
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(12.0),
                                        1356998400.0, 1357020000.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(), 1357020000.0,
                              1357041600.0));
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(14.0),
                                        1357041600.0, 1357063200.0));
        exp->endData();
        QVERIFY(exp->wait());
        exp.reset();

        Util::ByteArray::LineIterator it(*data);
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("Station,Year,DOY,BsB_S11,BsG_S11"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("BRW,2013,001.00000,99999.999,0012.00"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("BRW,2013,001.25000,99999.999,9999.99"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("BRW,2013,001.50000,00014.000,9999.99"));
    }

    void exportCPD1()
    {
        auto data = std::make_shared<Util::ByteArray>();
        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("cpd1");
        std::unique_ptr<ExternalSink>
                exp(component->createDataSink(IO::Access::buffer(data)->stream(), options));
        QVERIFY(exp.get() != nullptr);
        exp->start();

        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(12.0),
                                        1288293573.0, 1288293574.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(), 1288293574.0,
                              1288293575.0));
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(14.0),
                                        1288293575.0, 1288293576.0));
        exp->endData();
        QVERIFY(exp->wait());
        exp.reset();

        Util::ByteArray::LineIterator it(*data);
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("STN,Year,DOY,BsB_S11,BsG_S11"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("BRW,2010,301.80524,99999.999,00012.000"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("BRW,2010,301.80525,99999.999,99999.999"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("BRW,2010,301.80527,00014.000,99999.999"));
    }

    void defaultOptions()
    {
        auto data = std::make_shared<Util::ByteArray>();
        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionBoolean *>(options.get("station"))->set(true);
        qobject_cast<ComponentOptionBoolean *>(options.get("time-epoch"))->set(true);
        qobject_cast<ComponentOptionBoolean *>(options.get("time-excel"))->set(false);
        qobject_cast<ComponentOptionBoolean *>(options.get("time-iso"))->set(true);
        qobject_cast<ComponentOptionBoolean *>(options.get("time-julian"))->set(true);
        qobject_cast<ComponentOptionBoolean *>(options.get("time-yeardoy"))->set(true);
        qobject_cast<ComponentOptionSingleString *>(options.get("mvc-all"))->set("BLARG");
        qobject_cast<ComponentOptionEnum *>(options.get("flags"))->set("0x");
        qobject_cast<ComponentOptionSingleString *>(options.get("header-mark"))->set("!");
        qobject_cast<ComponentOptionBoolean *>(options.get("header-names"))->set(false);
        qobject_cast<ComponentOptionBoolean *>(options.get("header-description"))->set(true);
        qobject_cast<ComponentOptionBoolean *>(options.get("header-wavelength"))->set(true);
        qobject_cast<ComponentOptionBoolean *>(options.get("header-mvcs"))->set(true);
        std::unique_ptr<ExternalSink>
                exp(component->createDataSink(IO::Access::buffer(data)->stream(), options));
        QVERIFY(exp.get() != nullptr);
        exp->start();

        Variant::Root meta;
        meta.write().metadataReal("Wavelength").setDouble(450);
        meta.write().metadataReal("Source").setString("TSI 1234");
        meta.write().metadataReal("Description")
            .setString("Aerosol total light scattering coefficient (Mm-1), blue");
        Variant::Root metaFlags;
        metaFlags.write().metadataSingleFlag("STP").hash("Bits").setInt64(0x01);
        Variant::Root flagsValue;
        flagsValue.write().applyFlag("STP");
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw_meta", "BsB_S11"), meta, FP::undefined(),
                              FP::undefined()));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw_meta", "F1_S11"), metaFlags, FP::undefined(),
                              FP::undefined()));
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(12.0),
                                        1288293573.0, 1288293574.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "F1_S11"), flagsValue, 1288293573.0,
                              1288293575.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(), 1288293574.0,
                              1288293575.0));
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(14.0),
                                        1288293575.0, 1288293576.0));
        exp->endData();
        QVERIFY(exp->wait());
        exp.reset();

        Util::ByteArray::LineIterator it(*data);
        QCOMPARE(it.next().toQByteArrayRef(),
                 QByteArray("!NA,NA,NA,NA,NA,NA,NA,450;0;TSI 1234,NA"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray(
                "!Station code,Epoch time: seconds from 1970-01-01T00:00:00Z,Date String (YYYY-MM-DDThh:mm:ssZ) UTC,\"Fractional Julian day (12:00 January 1, 4713 BC UTC)\",Year,Fractional day of year (Midnight January 1 UTC = 1.00000),,\"Aerosol total light scattering coefficient (Mm-1), blue\","));
        QCOMPARE(it.next().toQByteArrayRef(),
                 QByteArray("!BLARG,BLARG,BLARG,BLARG,BLARG,BLARG,BLARG,BLARG,BLARG"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray(
                "BRW,1288293573,2010-10-28T19:19:33Z,2455498.30524,2010,301.80524,0x1,BLARG,00012.000"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray(
                "BRW,1288293574,2010-10-28T19:19:34Z,2455498.30525,2010,301.80525,0x1,BLARG,BLARG"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray(
                "BRW,1288293575,2010-10-28T19:19:35Z,2455498.30527,2010,301.80527,BLARG,00014.000,BLARG"));
    }

    void exportCutSplit()
    {
        auto data = std::make_shared<Util::ByteArray>();
        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionBoolean *>(options.get("header-cut"))->set(true);
        std::unique_ptr<ExternalSink>
                exp(component->createDataSink(IO::Access::buffer(data)->stream(), options));
        QVERIFY(exp.get() != nullptr);
        exp->start();

        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "BsG_S11", {"pm1"}), Variant::Root(12.0),
                              1288293573.0, 1288293574.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "BsG_S11", {"pm10"}), Variant::Root(),
                              1288293573.0, 1288293574.0));
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(14.0),
                                        1288293573.0, 1288293574.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "BsB_S11", {"pm10"}), Variant::Root(15.0),
                              1288293574.0, 1288293575.0));
        exp->endData();
        QVERIFY(exp->wait());
        exp.reset();

        Util::ByteArray::LineIterator it(*data);
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("NA,NA,PM10,PM1"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("DateTimeUTC,BsB_S11,BsG0_S11,BsG1_S11"));
        QCOMPARE(it.next().toQByteArrayRef(),
                 QByteArray("2010-10-28 19:19:33,00014.000,,00012.000"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("2010-10-28 19:19:34,00015.000,,"));
    }

    void exportCutSplitForced()
    {
        auto data = std::make_shared<Util::ByteArray>();
        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionBoolean *>(options.get("cut-split"))->set(true);
        std::unique_ptr<ExternalSink>
                exp(component->createDataSink(IO::Access::buffer(data)->stream(), options));
        QVERIFY(exp.get() != nullptr);
        exp->start();

        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "BsG_S11", {"pm1"}), Variant::Root(12.0),
                              1288293573.0, 1288293574.0));
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(14.0),
                                        1288293573.0, 1288293574.0));
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(15.0),
                                        1288293574.0, 1288293575.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "BsG_S11", {"pm10"}), Variant::Root(16.0),
                              1288293574.0, 1288293575.0));
        exp->endData();
        QVERIFY(exp->wait());
        exp.reset();

        Util::ByteArray::LineIterator it(*data);
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("DateTimeUTC,BsB_S11,BsG0_S11,BsG1_S11"));
        QCOMPARE(it.next().toQByteArrayRef(),
                 QByteArray("2010-10-28 19:19:33,00014.000,,00012.000"));
        QCOMPARE(it.next().toQByteArrayRef(),
                 QByteArray("2010-10-28 19:19:34,00015.000,00016.000,"));
    }

    void exportExtendedStats()
    {
        auto data = std::make_shared<Util::ByteArray>();
        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionBoolean *>(options.get("cover"))->set(true);
        qobject_cast<ComponentOptionBoolean *>(options.get("quantiles"))->set(true);
        qobject_cast<ComponentOptionBoolean *>(options.get("mean"))->set(true);
        std::unique_ptr<ExternalSink>
                exp(component->createDataSink(IO::Access::buffer(data)->stream(), options));
        QVERIFY(exp.get() != nullptr);
        exp->start();

        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(12.0),
                                        1288293573.0, 1288293574.0));
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsR_S11"), Variant::Root(13.0),
                                        1288293573.0, 1288293574.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "BsG_S11", {"cover"}), Variant::Root(0.95),
                              1288293573.0, 1288293574.0));
        Variant::Root stats;
        stats.write().hash("Count").setInt64(10);
        stats.write().hash("Mean").setDouble(12.1);
        stats.write().hash("StandardDeviation").setDouble(1.95);
        stats.write().hash("Quantiles").keyframe(0.0).setDouble(0.0);
        stats.write().hash("Quantiles").keyframe(1.0).setDouble(100000.0);
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "BsG_S11", {"stats"}), stats, 1288293573.0,
                              1288293574.0));
        exp->endData();
        QVERIFY(exp->wait());
        exp.reset();

        Util::ByteArray::LineIterator it(*data);
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray(
                "DateTimeUTC,BsG_S11,BsR_S11,BsGg_S11,BsGN_S11,BsGNr_S11,BsGq00135_S11,BsGq00621_S11,BsGq02275_S11,BsGq05000_S11,BsGq06681_S11,BsGq15866_S11,BsGq25000_S11,BsGq30854_S11,BsGq50000_S11,BsGq69146_S11,BsGq75000_S11,BsGq84134_S11,BsGq93319_S11,BsGq95000_S11,BsGq97725_S11,BsGq99379_S11,BsGq99865_S11"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray(
                "2010-10-28 19:19:33,00012.100,00013.000,00001.950,00010,0.95,00135.000,00621.000,02275.000,05000.000,06681.000,15866.000,25000.000,30854.000,50000.000,69146.000,75000.000,84134.000,93319.000,95000.000,97725.000,99379.000,99865.000"));
    }

    void exportArray()
    {
        auto data = std::make_shared<Util::ByteArray>();
        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionBoolean *>(options.get("cover"))->set(true);
        qobject_cast<ComponentOptionBoolean *>(options.get("quantiles"))->set(true);
        std::unique_ptr<ExternalSink>
                exp(component->createDataSink(IO::Access::buffer(data)->stream(), options));
        QVERIFY(exp.get() != nullptr);
        exp->start();

        Variant::Root array;
        array.write().array(0).setDouble(1.0);
        array.write().array(1).setDouble(2.0);
        array.write().array(2).setDouble(3.0);
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "N_N11"), array, 1288293573.0,
                                        1288293574.0));
        exp->endData();
        QVERIFY(exp->wait());
        exp.reset();

        Util::ByteArray::LineIterator it(*data);
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("DateTimeUTC,N1_N11,N2_N11,N3_N11"));
        QCOMPARE(it.next().toQByteArrayRef(),
                 QByteArray("2010-10-28 19:19:33,00001.000,00002.000,00003.000"));
    }

    void exportXML()
    {
        auto data = std::make_shared<Util::ByteArray>();
        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("xml");
        std::unique_ptr<ExternalSink>
                exp(component->createDataSink(IO::Access::buffer(data)->stream(), options));
        QVERIFY(exp.get() != nullptr);
        exp->start();

        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(12.0),
                                        1288293573.0, 1288293574.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(), 1288293574.0,
                              1288293575.0));
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(14.0),
                                        1288293575.0, 1288293576.0));
        exp->endData();
        QVERIFY(exp->wait());
        exp.reset();

        QVERIFY(data->string_start("<?xml"));

        StreamSink::Buffer egress;
        StandardDataInput in;
        in.start();
        in.incomingData(*data);
        in.endData();
        in.setEgress(&egress);
        in.wait();

        QCOMPARE((int) egress.values().size(), 3);
        QCOMPARE(egress.values().at(0),
                 SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(12.0),
                               1288293573.0, 1288293574.0));
    }

    void fill()
    {
        auto data = std::make_shared<Util::ByteArray>();
        ComponentOptions options(component->getOptions());
        qobject_cast<TimeIntervalSelectionOption *>(options.get("fill"))->set(Time::Second, 2,
                                                                              false);
        std::unique_ptr<ExternalSink>
                exp(component->createDataSink(IO::Access::buffer(data)->stream(), options));
        QVERIFY(exp.get() != nullptr);
        exp->start();

        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(12.0),
                                        1288293573.0, 1288293575.0));
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(14.0),
                                        1288293580.0, 1288293585.0));
        exp->endData();
        QVERIFY(exp->wait());
        exp.reset();

        Util::ByteArray::LineIterator it(*data);
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("DateTimeUTC,BsB_S11,BsG_S11"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("2010-10-28 19:19:33,,00012.000"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("2010-10-28 19:19:35,,"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("2010-10-28 19:19:37,,"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("2010-10-28 19:19:39,,"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("2010-10-28 19:19:40,00014.000,"));
    }

    void squash()
    {
        auto data = std::make_shared<Util::ByteArray>();
        ComponentOptions options(component->getOptions());
        qobject_cast<TimeIntervalSelectionOption *>(options.get("squash"))->set(Time::Second, 2,
                                                                                false);
        std::unique_ptr<ExternalSink>
                exp(component->createDataSink(IO::Access::buffer(data)->stream(), options));
        QVERIFY(exp.get() != nullptr);
        exp->start();

        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(12.0),
                                        1288293573.0, 1288293574.0));
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(14.0),
                                        1288293574.0, 1288293575.0));
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(16.0),
                                        1288293576.0, 1288293578.0));
        exp->endData();
        QVERIFY(exp->wait());
        exp.reset();

        Util::ByteArray::LineIterator it(*data);
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("DateTimeUTC,BsB_S11,BsG_S11"));
        QCOMPARE(it.next().toQByteArrayRef(),
                 QByteArray("2010-10-28 19:19:33,00014.000,00012.000"));
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("2010-10-28 19:19:36,,00016.000"));
    }

    void exportJSON()
    {
        auto data = std::make_shared<Util::ByteArray>();
        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("json");
        std::unique_ptr<ExternalSink>
                exp(component->createDataSink(IO::Access::buffer(data)->stream(), options));
        QVERIFY(exp.get() != nullptr);
        exp->start();

        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(12.0),
                                        1288293573.0, 1288293574.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(), 1288293574.0,
                              1288293575.0));
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(14.0),
                                        1288293575.0, 1288293576.0));
        exp->endData();
        QVERIFY(exp->wait());
        exp.reset();

        QVERIFY(data->string_start("["));

        StreamSink::Buffer egress;
        StandardDataInput in;
        in.start();
        in.incomingData(*data);
        in.endData();
        in.setEgress(&egress);
        in.wait();

        QCOMPARE((int) egress.values().size(), 3);
        QCOMPARE(egress.values().at(0),
                 SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(12.0),
                               1288293573.0, 1288293574.0));
        QCOMPARE(egress.values().at(0).getValue().toDouble(), 12.0);
    }

    void exportRaw()
    {
        auto data = std::make_shared<Util::ByteArray>();
        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("raw");
        std::unique_ptr<ExternalSink>
                exp(component->createDataSink(IO::Access::buffer(data)->stream(), options));
        QVERIFY(exp.get() != nullptr);
        exp->start();

        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(12.0),
                                        1288293573.0, 1288293574.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(), 1288293574.0,
                              1288293575.0));
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(14.0),
                                        1288293575.0, 1288293576.0));
        exp->endData();
        QVERIFY(exp->wait());
        exp.reset();

        QVERIFY(!data->empty());
        QCOMPARE((char) (*data)[0], (char) 0xC4);

        StreamSink::Buffer egress;
        StandardDataInput in;
        in.start();
        in.incomingData(*data);
        in.endData();
        in.setEgress(&egress);
        in.wait();

        QCOMPARE((int) egress.values().size(), 3);
        QCOMPARE(egress.values().at(0),
                 SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(12.0),
                               1288293573.0, 1288293574.0));
        QCOMPARE(egress.values().at(0).getValue().toDouble(), 12.0);
    }

    void temporaryFile()
    {
        auto data = std::make_shared<Util::ByteArray>();
        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionBoolean *>(options.get("time-epoch"))->set(true);
        qobject_cast<ComponentOptionBoolean *>(options.get("time-excel"))->set(false);
        std::unique_ptr<ExternalSink>
                exp(component->createDataSink(IO::Access::buffer(data)->stream(), options));
        QVERIFY(exp.get() != nullptr);
        exp->start();

        for (int i = 0; i < 1000; i++) {
            exp->incomingData(
                    SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(i + 0.5),
                                  2000.0 + i, 2001.0 + i));
            if (i % 100 == 0)
                QTest::qSleep(50);
        }
        exp->endData();
        QVERIFY(exp->wait());
        exp.reset();

        Util::ByteArray::LineIterator it(*data);
        QCOMPARE(it.next().toQByteArrayRef(), QByteArray("EPOCH,BsG_S11"));
        for (int i = 0; i < 1000; i++) {
            QByteArray expected;
            expected.append(QByteArray::number(2000 + i));
            expected.append(',');
            expected.append(QByteArray::number(i + 0.5, 'f', 3).rightJustified(9, '0'));

            QCOMPARE(it.next().toQByteArrayRef(), expected);
        }

        QVERIFY(it.next().empty());
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
