/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef EXPORT_H
#define EXPORT_H

#include "core/first.hxx"

#include <mutex>
#include <condition_variable>
#include <memory>
#include <QtGlobal>
#include <QObject>
#include <QIODevice>
#include <QString>

#include "core/component.hxx"
#include "datacore/stream.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "core/waitutils.hxx"
#include "core/threading.hxx"
#include "fileoutput/engine.hxx"
#include "fileoutput/csv.hxx"

class Export : public CPD3::Data::ExternalSink {
    class Target : public CPD3::Output::Engine {
        Export &parent;

        friend class Export;

        enum class InputState {
            Startup, Ready, Done
        } inputState;
    public:
        Target(Export &parent, CPD3::Output::CSV config);

        virtual ~Target();

    protected:
        bool startFirstStage(Stage *) override;

        void firstStageComplete(Stage *) override;
    };

    friend class Target;

    std::unique_ptr<CPD3::IO::Generic::Stream> outputStream;
    std::unique_ptr<Target> engine;

    std::unique_ptr<CPD3::Data::DynamicTimeInterval> fillThreshold;
    std::unique_ptr<CPD3::Data::DynamicTimeInterval> squashThreshold;

    std::mutex mutex;
    std::condition_variable notify;

public:
    Export(std::unique_ptr<CPD3::IO::Generic::Stream> &&stream,
           const CPD3::ComponentOptions &options);

    virtual ~Export();

    void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

    void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

    void incomingData(const CPD3::Data::SequenceValue &value) override;

    void incomingData(CPD3::Data::SequenceValue &&value) override;

    void endData() override;

    void signalTerminate() override;

    void start() override;

    bool isFinished() override;

    bool wait(double timeout = CPD3::FP::undefined()) override;
};

class ExportComponent : public QObject, virtual public CPD3::Data::ExternalSinkComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalSinkComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.export"
                              FILE
                              "export.json")

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    CPD3::Data::ExternalSink *createDataSink(std::unique_ptr<
            CPD3::IO::Generic::Stream> &&stream = {},
                                             const CPD3::ComponentOptions &options = {}) override;
};


#endif
