/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#ifdef Q_OS_UNIX

#include <stdio.h>
#include <QFile>

#endif

#include <QTemporaryFile>
#include <QLoggingCategory>

#include "core/component.hxx"
#include "core/range.hxx"
#include "core/csv.hxx"
#include "core/actioncomponent.hxx"
#include "core/util.hxx"
#include "datacore/externalsink.hxx"
#include "datacore/segment.hxx"
#include "datacore/variant/composite.hxx"

#include "export.hxx"


Q_LOGGING_CATEGORY(log_component_export, "cpd3.component.export", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Output;

enum BaselineMode {
    ModeCSV,
    ModeXL,
    ModeR,
    ModeIDL,
    ModeIDLLegacy,
    ModeCPD1,
    ModeXML,
    ModeJSON,
    ModeRaw,
    ModeRawLegacy,
    ModeDirectLegacy
};
enum JoinMode {
    JoinCSV, JoinSpace, JoinTab
};
enum MVCMode {
    MVCDefault, MVCNA, MVCBlank
};

Export::Target::Target(Export &parent, CPD3::Output::CSV config) : Engine(
        CPD3::Output::CSV::stage(std::move(config))),
                                                                   parent(parent),
                                                                   inputState(InputState::Startup)
{ }

Export::Target::~Target() = default;

bool Export::Target::startFirstStage(Stage *)
{
    {
        std::lock_guard<std::mutex> lock(parent.mutex);
        inputState = InputState::Ready;
    }
    parent.notify.notify_all();
    return true;
}

void Export::Target::firstStageComplete(Stage *)
{
    std::lock_guard<std::mutex> lock(parent.mutex);
    inputState = InputState::Done;
    parent.notify.notify_all();
}

Export::Export(std::unique_ptr<CPD3::IO::Generic::Stream> &&stream, const ComponentOptions &options)
        : outputStream(std::move(stream))
{
    CPD3::Output::CSV config;
    config.useExplicitMVC = true;
    config.explicitMVC = "";

    QString fieldJoin = ",";
    QString fieldQuote = "\"";
    QString fieldEscapeQuote = "\"\"\"";
    QString headerMark;

    if (options.isSet("mode")) {
        switch (qobject_cast<ComponentOptionEnum *>(options.get("mode"))->get().getID()) {
        case ModeXL:
            break; /* Already in Excel mode */
        case ModeCSV:
            config.timeExcel = false;
            config.timeYear = true;
            config.timeDOY = true;
            break;
        case ModeCPD1:
            config.station = true;
            config.timeExcel = false;
            config.timeYear = true;
            config.timeDOY = true;
            config.useExplicitMVC = false;
            break;
        case ModeR:
            config.timeExcel = false;
            config.timeEpoch = true;
            config.strictNumbers = true;
            config.explicitMVC = "NA";
            config.useExplicitMVC = true;
            break;
        case ModeIDL:
            config.timeExcel = false;
            config.timeJulian = true;
            config.timeYear = true;
            config.timeDOY = true;
            config.useExplicitMVC = false;
            config.mvcFlagMode = CPD3::Output::CSV::MVCFlagMode::Follow;
            config.strictNumbers = true;
            fieldJoin = " ";
            fieldQuote = "";
            fieldEscapeQuote = "";
#ifdef Q_OS_UNIX
            config.headerNamesStdErr = true;
#endif
            break;
        case ModeIDLLegacy:
            config.station = true;
            config.timeExcel = false;
            config.timeYear = true;
            config.timeDOY = true;
            config.useExplicitMVC = false;
            config.longStationHeaderName = true;
            break;
        default:
            Q_ASSERT(false);
            break;
        }
    }

    if (options.isSet("station")) {
        config.station = qobject_cast<ComponentOptionBoolean *>(options.get("station"))->get();
    }

    if (options.isSet("time-epoch")) {
        config.timeEpoch = qobject_cast<ComponentOptionBoolean *>(options.get("time-epoch"))->get();
    }
    if (options.isSet("time-excel")) {
        config.timeExcel = qobject_cast<ComponentOptionBoolean *>(options.get("time-excel"))->get();
    }
    if (options.isSet("time-iso")) {
        config.timeISO = qobject_cast<ComponentOptionBoolean *>(options.get("time-iso"))->get();
    }
    if (options.isSet("time-fyear")) {
        config.timeFYear = qobject_cast<ComponentOptionBoolean *>(options.get("time-fyear"))->get();
    }
    if (options.isSet("time-julian")) {
        config.timeJulian =
                qobject_cast<ComponentOptionBoolean *>(options.get("time-julian"))->get();
    }
    if (options.isSet("time-yeardoy") &&
            qobject_cast<ComponentOptionBoolean *>(options.get("time-yeardoy"))->get()) {
        config.timeYear = true;
        config.timeDOY = true;
    } else if (options.isSet("time-doy") &&
            qobject_cast<ComponentOptionBoolean *>(options.get("time-doy"))->get()) {
        config.timeYear = false;
        config.timeDOY = true;
    }
    if (options.isSet("cut-string")) {
        config.cutString = qobject_cast<ComponentOptionBoolean *>(options.get("cut-string"))->get();
    }
    if (options.isSet("cut-split")) {
        if (qobject_cast<ComponentOptionBoolean *>(options.get("cut-split"))->get()) {
            config.cutSplitMode = CPD3::Output::CSV::CutSplitMode::Always;
        } else {
            config.cutSplitMode = CPD3::Output::CSV::CutSplitMode::Never;
        }
    }
    if (options.isSet("stddev")) {
        config.enableStdDev = qobject_cast<ComponentOptionBoolean *>(options.get("stddev"))->get();
    }
    if (options.isSet("count")) {
        config.enableCount = qobject_cast<ComponentOptionBoolean *>(options.get("count"))->get();
    }
    if (options.isSet("cover")) {
        config.enableCover = qobject_cast<ComponentOptionBoolean *>(options.get("cover"))->get();
    }
    if (options.isSet("end")) {
        config.enableEnd = qobject_cast<ComponentOptionBoolean *>(options.get("end"))->get();
    }
    if (options.isSet("quantiles")) {
        config.enableQuantiles =
                qobject_cast<ComponentOptionBoolean *>(options.get("quantiles"))->get();
    }
    if (options.isSet("bounds")) {
        config.enableBounds = qobject_cast<ComponentOptionBoolean *>(options.get("bounds"))->get();
    }
    if (options.isSet("mean")) {
        config.useUnweightedMean =
                qobject_cast<ComponentOptionBoolean *>(options.get("mean"))->get();
    }
    if (options.isSet("paths")) {
        config.splitOutputPaths =
                qobject_cast<ComponentOptionBoolean *>(options.get("paths"))->get();
    }


    if (options.isSet("join")) {
        switch (qobject_cast<ComponentOptionEnum *>(options.get("join"))->get().getID()) {
        case JoinCSV:
            fieldJoin = ",";
            fieldQuote = "\"";
            fieldEscapeQuote = "\"\"\"";
            break;
        case JoinSpace:
            fieldJoin = " ";
            fieldQuote = "\"";
            fieldEscapeQuote = "\"\"\"";
            break;
        case JoinTab:
            fieldJoin = "\t";
            fieldQuote = "";
            fieldEscapeQuote = "";
            break;
        default:
            Q_ASSERT(false);
            break;
        }
    }
    if (options.isSet("join-delimiter")) {
        fieldJoin =
                qobject_cast<ComponentOptionSingleString *>(options.get("join-delimiter"))->get();
    }
    if (options.isSet("join-quote")) {
        fieldQuote = qobject_cast<ComponentOptionSingleString *>(options.get("join-quote"))->get();
        fieldEscapeQuote = fieldQuote.repeated(3);
    }
    if (options.isSet("join-quote-escape")) {
        fieldEscapeQuote = qobject_cast<ComponentOptionSingleString *>(
                options.get("join-quote-escape"))->get();
    }

    if (options.isSet("mvc")) {
        switch (qobject_cast<ComponentOptionEnum *>(options.get("mvc"))->get().getID()) {
        case MVCDefault:
            config.useExplicitMVC = false;
            break;
        case MVCNA:
            config.useExplicitMVC = true;
            config.explicitMVC = "NA";
            break;
        case MVCBlank:
            config.useExplicitMVC = true;
            config.explicitMVC = "";
            break;
        }
    }
    if (options.isSet("mvc-all")) {
        config.useExplicitMVC = true;
        config.explicitMVC =
                qobject_cast<ComponentOptionSingleString *>(options.get("mvc-all"))->get();
    }
    if (options.isSet("mvc-flag")) {
        config.mvcFlagMode = static_cast<CPD3::Output::CSV::MVCFlagMode>(
                qobject_cast<ComponentOptionEnum *>(options.get("mvc-flag"))->get().getID());
    }

    if (options.isSet("flags")) {
        config.flagGenerateMode = static_cast<CPD3::Output::CSV::FlagGenerateMode>(
                qobject_cast<ComponentOptionEnum *>(options.get("flags"))->get().getID());
    }

    if (options.isSet("format")) {
        config.useNumericFormat = true;
        config.numericFormat =
                qobject_cast<ComponentOptionSingleString *>(options.get("format"))->get();
    }
    if (options.isSet("numeric-only")) {
        config.strictNumbers =
                qobject_cast<ComponentOptionBoolean *>(options.get("numeric-only"))->get();
    }

    if (options.isSet("header-mark")) {
        headerMark = qobject_cast<ComponentOptionSingleString *>(options.get("header-mark"))->get();
    }
    if (options.isSet("header-names")) {
        config.headerNames =
                qobject_cast<ComponentOptionBoolean *>(options.get("header-names"))->get();
    }
#ifdef Q_OS_UNIX
    if (options.isSet("header-names-stderr")) {
        config.headerNamesStdErr =
                qobject_cast<ComponentOptionBoolean *>(options.get("header-names-stderr"))->get();
    }
#endif
    if (options.isSet("header-description")) {
        config.headerDescription =
                qobject_cast<ComponentOptionBoolean *>(options.get("header-description"))->get();
    }
    if (options.isSet("header-wavelength")) {
        config.headerWavelength =
                qobject_cast<ComponentOptionBoolean *>(options.get("header-wavelength"))->get();
    }
    if (options.isSet("header-flags")) {
        config.headerFlags =
                qobject_cast<ComponentOptionBoolean *>(options.get("header-flags"))->get();
    }
    if (options.isSet("header-mvcs")) {
        config.headerMVCs =
                qobject_cast<ComponentOptionBoolean *>(options.get("header-mvcs"))->get();
    }
    if (options.isSet("header-cut")) {
        config.headerCutSize =
                qobject_cast<ComponentOptionBoolean *>(options.get("header-cut"))->get();
    }

    if (options.isSet("fill")) {
        fillThreshold.reset(
                qobject_cast<TimeIntervalSelectionOption *>(options.get("fill"))->getInterval());
        config.fillThreshold = fillThreshold.get();
    }
    if (options.isSet("squash")) {
        squashThreshold.reset(
                qobject_cast<TimeIntervalSelectionOption *>(options.get("squash"))->getInterval());
        config.squashThreshold = squashThreshold.get();
    }

    config.outputHeader = [=](QStringList &&fields) {
        auto line = CPD3::CSV::join(std::move(fields), fieldJoin, fieldQuote, fieldEscapeQuote);
        line += '\n';
        if (!headerMark.isEmpty())
            line.prepend(headerMark);

        outputStream->write(std::move(line));
    };
    config.outputLine = [=](QStringList &&fields) {
        auto line = CPD3::CSV::join(std::move(fields), fieldJoin, fieldQuote, fieldEscapeQuote);
        line += '\n';
        outputStream->write(std::move(line));
    };

    engine.reset(new Target(*this, std::move(config)));
    finished = engine->finished;
    feedback = engine->feedback;
    engine->start();

    {
        std::unique_lock<std::mutex> lock(mutex);
        notify.wait(lock, [this] { return engine->inputState != Target::InputState::Startup; });
    }
}

Export::~Export()
{
    engine->signalTerminate();
    engine->wait();
    engine.reset();
}

void Export::incomingData(const SequenceValue::Transfer &values)
{
    engine->getDataSink()->incomingData(values);
}

void Export::incomingData(SequenceValue::Transfer &&values)
{
    engine->getDataSink()->incomingData(std::move(values));
}

void Export::incomingData(const SequenceValue &value)
{
    engine->getDataSink()->incomingData(value);
}

void Export::incomingData(SequenceValue &&value)
{
    engine->getDataSink()->incomingData(std::move(value));
}

void Export::endData()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (engine->inputState != Target::InputState::Ready)
            return;
    }
    engine->getDataSink()->endData();
}

void Export::signalTerminate()
{ engine->signalTerminate(); }

void Export::start()
{ }

bool Export::isFinished()
{ return engine->isFinished(); }

bool Export::wait(double timeout)
{ return engine->wait(timeout); }


ComponentOptions ExportComponent::getOptions()
{
    ComponentOptions options;

    ComponentOptionEnum *mode = new ComponentOptionEnum(tr("mode", "name"), tr("Base mode"),
                                                        tr("This is the base export mode.  It defines the defaults for all "
                                                           "other options.  Those defaults can be overridden by setting the "
                                                           "option directly.  For some export modes (XML and raw) there are "
                                                           "no other options so any specified are ignored.  Those exports "
                                                           "are direct translations of the input data stream."),
                                                        tr("MS Excel compatible"), -1);
    mode->add(ModeXL, "xl", tr("xl", "mode name"),
              tr("MS Excel compatible (CSV, date-time, blank-mvc)"));
    mode->alias(ModeXL, tr("Excel", "mode name"));
    mode->add(ModeCSV, "csv", tr("CSV", "mode name"), tr("General CSV (CSV, year-doy, blank-mvc)"));
    mode->add(ModeR, "r", tr("R", "mode name"), tr("R/S compatible (CSV, epoch time, \"NA\"-mvc)"));
    mode->alias(ModeR, tr("S", "mode name"));
    mode->add(ModeIDL, "idl", tr("IDL", "mode name"),
              tr("IDL compatible (space-separated, Julian day, standard mvc)"));
    mode->add(ModeIDLLegacy, "idllegacy", tr("IDLLegacy", "mode name"),
              tr("IDL legacy code compatible (station, year and doy)"));
    mode->add(ModeCPD1, "cpd1", tr("CPD1", "mode name"),
              tr("CPD1 compatible (CSV, station, year and doy)"));
    mode->add(ModeXML, "xml", tr("XML", "mode name"), tr("XML data (no other options applied)"));
    mode->add(ModeJSON, "json", tr("JSON", "mode name"),
              tr("JSON data (no other options applied)"));
    mode->add(ModeRaw, "raw", tr("raw", "mode name"),
              tr("Raw binary data (no other options applied)"));
    mode->add(ModeRawLegacy, "raw1", tr("raw1", "mode name"),
              tr("Raw binary data in version 1 format (no other options applied)"));
    mode->add(ModeDirectLegacy, "direct1", tr("direct1", "mode name"),
              tr("Direct base64 data in version 1 format (no other options applied)"));
    options.add("mode", mode);


    options.add("station",
                new ComponentOptionBoolean(tr("station", "name"), tr("Include station code"),
                                           tr("If set then a station code is included in the output."),
                                           ""));

    options.add("time-epoch", new ComponentOptionBoolean(tr("time-epoch", "name"),
                                                         tr("Include the Unix Epoch time"),
                                                         tr("If set then the output includes the number of seconds since "
                                                            "1970-01-01T00:00:00Z."), ""));
    options.add("time-excel", new ComponentOptionBoolean(tr("time-excel", "name"),
                                                         tr("Include the Excel formatted date-time"),
                                                         tr("If set then the output includes the date and time in a format "
                                                            " recognized by Excel."), ""));
    options.add("time-iso", new ComponentOptionBoolean(tr("time-iso", "name"),
                                                       tr("Include the ISO 8601 formatted date-time"),
                                                       tr("If set then the output includes a date-time formatted in "
                                                          "compliance with ISO 8601."), ""));
    options.add("time-fyear", new ComponentOptionBoolean(tr("time-fractional-year", "name"),
                                                         tr("Include the fractional year"),
                                                         tr("If set then the output includes the fractional year with enough "
                                                            "precision to represent seconds."),
                                                         ""));
    options.add("time-julian", new ComponentOptionBoolean(tr("time-julian-day", "name"),
                                                          tr("Include the fractional Julian day"),
                                                          tr("If set then the output includes the fractional Julian day.  That "
                                                             "is the fractional number of days since 12:00 January 1, 4713 BC."),
                                                          ""));
    options.add("time-yeardoy", new ComponentOptionBoolean(tr("time-yeardoy", "name"),
                                                           tr("Include the year and fractional day of year"),
                                                           tr("If set then the output includes the year and fractional day of "
                                                              "year (with January 1 = 1.00000) as separate fields."),
                                                           ""));
    options.add("time-doy", new ComponentOptionBoolean(tr("time-doy", "name"),
                                                       tr("Include the fractional day of year"),
                                                       tr("If set then the output includes the fractional day of year (with "
                                                          "January 1 = 1.00000)."), ""));
    options.exclude("time-yeardoy", "time-doy");
    options.exclude("time-doy", "time-yeardoy");
    options.add("cut-string", new ComponentOptionBoolean(tr("cut-string", "name"),
                                                         tr("Include an active sizes indicator"),
                                                         tr("If set then the output includes an indicator containing a "
                                                            "semicolon delimited list of cut sizes."),
                                                         ""));
    options.add("cut-split",
                new ComponentOptionBoolean(tr("cut-split", "name"), tr("Set cut split splitting"),
                                           tr("If set then variables will be renamed/split based on their cut "
                                              "size even if they do not overlap.  If set to false then automatic "
                                              "detection is disabled and data are never split.  Disabling this can "
                                              "have unpredictable results if used with data that does overlap."),
                                           tr("Automatic")));
    options.add("stddev", new ComponentOptionBoolean(tr("stddev", "name"),
                                                     tr("Enable standard deviation columns"),
                                                     tr("If enabled then columns for the standard deviation of the averages "
                                                        "will be output."),
                                                     tr("Enabled", "stddev default")));
    options.add("count", new ComponentOptionBoolean(tr("count", "name"),
                                                    tr("Enable average input count columns"),
                                                    tr("If enabled then columns with the number of observations for each "
                                                       "column will be included."),
                                                    tr("Enabled", "count default")));
    options.add("cover", new ComponentOptionBoolean(tr("cover", "name"),
                                                    tr("Enable coverage fraction output"),
                                                    tr("If set then columns representing the fraction of data available in "
                                                       "each average will be output."), ""));
    options.add("end", new ComponentOptionBoolean(tr("end", "name"), tr("Enable end value output"),
                                                  tr("If set then columns representing the value at the end of the "
                                                     "average will be output."), ""));
    options.add("quantiles", new ComponentOptionBoolean(tr("quantiles", "name"),
                                                        tr("Enable standard quantile output"),
                                                        tr("If set then columns for the standard quantiles (0.00135, 0.00621, "
                                                           "0.02275, 0.05, 0.06681, 0.15866, 0.25, 0.30854, 0.5, 0.69146, "
                                                           "0.75, 0.84134, 0.93319, 0.95, 0.97725, 0.99379, and 0.99865) of "
                                                           "the data in the average will be output."),
                                                        ""));
    options.add("bounds", new ComponentOptionBoolean(tr("bounds", "name"),
                                                     tr("Enable absolute bounds output"),
                                                     tr("If set then columns for containing the absolute minimum and maximum "
                                                        "of the data in the average will be output."),
                                                     ""));
    options.add("mean",
                new ComponentOptionBoolean(tr("mean", "name"), tr("Output unweighted means"),
                                           tr("If set then the primary data columns will contain the unweighted "
                                              "means when they are available instead of coverage weighted and/or "
                                              "difference measurements."), ""));
    options.add("paths",
                new ComponentOptionBoolean(tr("paths", "name"), tr("Output all value paths"),
                                           tr("If set then all complex values have their component paths "
                                              "output separately, resulting in one column for each path."),
                                           ""));

    ComponentOptionEnum *join =
            new ComponentOptionEnum(tr("join", "name"), tr("Field join base mode"),
                                    tr("This is the base field joining mode.  The mode set here determines "
                                       "the defaults used to join together the output.  Those options can "
                                       "be overridden individually as well."), "");
    join->add(JoinCSV, "csv", tr("csv", "mode name"),
              tr("Standard comma separated values.  Fields are joined together with "
                 "a comma (ASCII 0x2C) and quoted with a double quote (ASCII 0x22)."));
    join->add(JoinSpace, "space", tr("space", "mode name"),
              tr("Space separated values.  Fields are joined together with a space "
                 "(ASCII 0x20) and quoted with double quotes (ASCII 0x22)."));
    join->add(JoinTab, "tab", tr("tab", "mode name"),
              tr("Tab separated values.  Fields are joined together with a tab "
                 "(ASCII 0x09)."));
    options.add("join", join);
    options.add("join-delimiter", new ComponentOptionSingleString(tr("join-delimiter", "name"),
                                                                  tr("The delimiter used to join fields together"),
                                                                  tr("If set then this overrides the string used to delimit the join "
                                                                     "between multiple fields."),
                                                                  ""));
    options.add("join-quote", new ComponentOptionSingleString(tr("join-quote", "name"),
                                                              tr("The string used at the start and end of a field to \"quote\" it"),
                                                              tr("If set then this overrides the string added to the start and end "
                                                                 "of a field if it needs to be quoted."),
                                                              ""));
    options.add("join-quote-escape",
                new ComponentOptionSingleString(tr("join-quote-escape", "name"),
                                                tr("The string that will replace the quote string in the middle of a "
                                                   "quoted field"),
                                                tr("If set then this overrides the string used to replace all "
                                                   "instances of the quote string within a quoted field."),
                                                tr("The quote string repeated three times")));

    ComponentOptionEnum *mvc = new ComponentOptionEnum(tr("mvc", "name"), tr("MVC output style"),
                                                       tr("This is the MVC output style.  Missing or undefined values are "
                                                          "written in accordance with this style.  Cannot be used with a "
                                                          "direct specification of the MVC."), "");
    mvc->add(MVCDefault, "default", tr("default", "mode name"),
             tr("Use the default MVC for each value."));
    mvc->add(MVCNA, "na", tr("NA", "mode name"),
             tr("Output the string \"NA\" for missing values."));
    mvc->add(MVCBlank, "blank", tr("blank", "mode name"),
             tr("Output an empty field for missing values."));
    options.add("mvc", mvc);
    options.add("mvc-all", new ComponentOptionSingleString(tr("mvc-all", "name"),
                                                           tr("Literal MVC for all values"),
                                                           tr("If set then this is the MVC string used for all values.  Cannot be "
                                                              "used with a MVC mode specification."),
                                                           ""));
    options.exclude("mvc", "mvc-all");
    options.exclude("mvc-all", "mvc");
    ComponentOptionEnum *mvcFlag =
            new ComponentOptionEnum(tr("mvc-flag", "name"), tr("Missing value flag mode"),
                                    tr("Sets the missing value flag mode.  If enabled missing value flags "
                                       "are fields that are either zero or one to indicate if the value "
                                       "corresponding to the flag was defined in the line."), "");
    mvcFlag->add(static_cast<int>(CPD3::Output::CSV::MVCFlagMode::None), "disable",
                 tr("disable", "mode name"), tr("Disable MVC flags."));
    mvcFlag->add(static_cast<int>(CPD3::Output::CSV::MVCFlagMode::Follow), "follow",
                 tr("follow", "mode name"),
                 tr("Output MVC flags immediately following the value they correspond "
                    "to."));
    mvcFlag->add(static_cast<int>(CPD3::Output::CSV::MVCFlagMode::End), "end",
                 tr("end", "mode name"),
                 tr("Output MVC flags at the end of the record in the same order as the "
                    "prior fields in the record."));
    options.add("mvc-flag", mvcFlag);

    ComponentOptionEnum *flags =
            new ComponentOptionEnum(tr("flags", "name"), tr("Flags output mode"),
                                    tr("Sets the flags breakdown and output mode.  This determines how "
                                       "flags values are written out."), "");
    flags->add(static_cast<int>(CPD3::Output::CSV::FlagGenerateMode::Default), "default",
               tr("default", "mode name"), tr("Output flags out like a conventional value."));
    flags->add(static_cast<int>(CPD3::Output::CSV::FlagGenerateMode::HexFlags), "0x",
               tr("0x", "mode name"),
               tr("Output flags out as hexadecimal numbers with a leading \"0x\"."));
    flags->add(static_cast<int>(CPD3::Output::CSV::FlagGenerateMode::Breakdown), "breakdown",
               tr("breakdown", "mode name"),
               tr("Output flags as a series of zero or one values corresponding to "
                  "each possible flag."));
    flags->add(static_cast<int>(CPD3::Output::CSV::FlagGenerateMode::List), "list",
               tr("list", "mode name"), tr("Output flags as a list of flag names in the field."));
    options.add("flags", flags);

    options.add("format", new ComponentOptionSingleString(tr("format", "name"),
                                                          tr("Override format for all numeric values"),
                                                          tr("If set then this is the format (like printf(2) or of the form "
                                                             "\"000.000\") used to output all numeric values."),
                                                          ""));
    options.add("numeric-only", new ComponentOptionBoolean(tr("numeric-only", "name"),
                                                           tr("Only output numeric values"),
                                                           tr("If set then only values with a numeric representation will be "
                                                              "output, all others will be considered missing."),
                                                           ""));

    options.add("header-mark", new ComponentOptionSingleString(tr("header-mark", "name"),
                                                               tr("Set the leading string to indicate a header line"),
                                                               tr("If set then this is added to the start of all header lines."),
                                                               ""));
    options.add("header-names", new ComponentOptionBoolean(tr("header-names", "name"),
                                                           tr("Include field names in the header"),
                                                           tr("If set then a line with field names is included in the header."),
                                                           ""));
#ifdef Q_OS_UNIX
    options.add("header-names-stderr", new ComponentOptionBoolean(tr("header-names-stderr", "name"),
                                                                  tr("Output header names to standard error"),
                                                                  tr("If set then a line with field names is output to standard error "
                                                                     "(regardless of invocation context)."),
                                                                  ""));
#endif
    options.add("header-description", new ComponentOptionBoolean(tr("header-description", "name"),
                                                                 tr("Include field descriptions in the header"),
                                                                 tr("If set then a line with field descriptions is included in the header."),
                                                                 ""));
    options.add("header-wavelength", new ComponentOptionBoolean(tr("header-wavelength", "name"),
                                                                tr("Include field wavelengths in the header"),
                                                                tr("If set then a line with field wavelengths (including possibly time "
                                                                   "dependent information) is included in the header."),
                                                                ""));
    options.add("header-mvcs", new ComponentOptionBoolean(tr("header-mvcs", "name"),
                                                          tr("Include field MVCs in the header"),
                                                          tr("If set then a line with field missing value codes is included in "
                                                             "the header."), ""));
    options.add("header-cut", new ComponentOptionBoolean(tr("header-cut", "name"),
                                                         tr("Include a header with the column cut size"),
                                                         tr("If set then a line with a cut size for the whole column is included in "
                                                            "the header."), ""));
    options.add("header-flags", new ComponentOptionBoolean(tr("header-flags", "name"),
                                                           tr("Include flags descriptions in the header"),
                                                           tr("If set then lines for each possible flag will be included in the header."),
                                                           ""));

    TimeIntervalSelectionOption *tis =
            new TimeIntervalSelectionOption(tr("fill", "name"), tr("Data fill interval"),
                                            tr("This is maximum interval to create filling lines at.  For example, five minutes would "
                                               "create an output every five minutes during gaps, starting at the beginning of the gap.  "
                                               "Setting it to undefined creates a single output for the entire gap.  This is best "
                                               "coupled with averaging of the same interval for uniform data output."),
                                            "");
    tis->setAllowZero(false);
    tis->setAllowNegative(false);
    tis->setAllowUndefined(true);
    tis->setDefaultAligned(true);
    options.add("fill", tis);

    tis = new TimeIntervalSelectionOption(tr("squash", "name"), tr("Data squash threshold"),
                                          tr("This sets a limit below which data are combined into a single record.  "
                                             "That is, if two variables have values separated by less than this threshold, "
                                             "they will be combined into a single output record."),
                                          "");
    tis->setAllowZero(false);
    tis->setAllowNegative(false);
    tis->setAllowUndefined(true);
    tis->setDefaultAligned(false);
    options.add("squash", tis);

    return options;
}

QList<ComponentExample> ExportComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Default MS Excel formatted")));

    options = getOptions();
    (qobject_cast<ComponentOptionEnum *>(options.get("mode")))->set("csv");
    examples.append(ComponentExample(options, tr("CSV with year and DOY")));

    options = getOptions();
    (qobject_cast<ComponentOptionEnum *>(options.get("mode")))->set("xl");
    (qobject_cast<ComponentOptionBoolean *>(options.get("time-fyear")))->set(true);
    (qobject_cast<ComponentOptionBoolean *>(options.get("station")))->set(true);
    examples.append(ComponentExample(options, tr("Excel with station and fractional year")));

    return examples;
}

ExternalSink *ExportComponent::createDataSink(std::unique_ptr<IO::Generic::Stream> &&stream,
                                              const ComponentOptions &options)
{
    if (options.isSet("mode")) {
        switch (qobject_cast<ComponentOptionEnum *>(options.get("mode"))->get().getID()) {
        case ModeXML:
            return new StandardDataOutput(std::move(stream), StandardDataOutput::OutputType::XML);
        case ModeRaw:
            return new StandardDataOutput(std::move(stream), StandardDataOutput::OutputType::Raw);
        case ModeRawLegacy:
            return new StandardDataOutput(std::move(stream),
                                          StandardDataOutput::OutputType::Raw_Legacy);
        case ModeDirectLegacy:
            return new StandardDataOutput(std::move(stream),
                                          StandardDataOutput::OutputType::Direct_Legacy);
        case ModeJSON:
            return new StandardDataOutput(std::move(stream), StandardDataOutput::OutputType::JSON);
        default:
            break;
        }
    }
    return new Export(std::move(stream), options);
}
