/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QBuffer>

#include "datacore/externalsink.hxx"
#include "core/component.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class TestComponent : public QObject {
Q_OBJECT

    ExternalSinkComponent *component;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component =
                qobject_cast<ExternalSinkComponent *>(ComponentLoader::create("summary_general"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionEnum *>(options.get("mode")));
        QVERIFY(qobject_cast<ComponentOptionEnum *>(options.get("rows")));
        QVERIFY(qobject_cast<ComponentOptionEnum *>(options.get("display")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("description")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("wavelength")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("instrument")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("mean")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("cover")));
        QVERIFY(qobject_cast<ComponentOptionStringSet *>(options.get("metadata")));
        QVERIFY(qobject_cast<ComponentOptionStringSet *>(options.get("data")));
    }

    void summaryDefaults()
    {
        QByteArray data;
        QBuffer device(&data);
        device.open(QIODevice::WriteOnly);
        ComponentOptions options(component->getOptions());
        ExternalSink *exp = component->createDataEgress(&device, options);
        QVERIFY(exp != NULL);

        Variant::Root meta;
        meta.write().metadataReal("Wavelength").setDouble(550);
        meta.write().metadataReal("Format").setString("00");
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw_meta", "BsG_S11"), meta, 1000.0, 5000.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(1.001), 1000.0,
                              5000.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "BsG_S11", {"cover"}), Variant::Root(0.9),
                              1000.0, 5000.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "N_N61"), Variant::Root(1.5), 1000.0,
                              2300.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "N_N61"), Variant::Root(2.5), 2300.0,
                              2600.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "N_N61"), Variant::Root(3.5), 2600.0,
                              3000.0));
        exp->endData();
        exp->start();
        Threading::pollInEventLoop([&] { return !exp->isFinished(); });
        exp->wait();
        delete exp;

        QBuffer input(&data);
        input.open(QIODevice::ReadOnly);
        QByteArray line;
        line = input.readLine();
        while (line.endsWith('\r') || line.endsWith('\n')) { line.chop(1); }
        QCOMPARE(line, QByteArray("        Wavelength Mean StdDev Coverage"));
        line = input.readLine();
        while (line.endsWith('\r') || line.endsWith('\n')) { line.chop(1); }
        QCOMPARE(line, QByteArray("  N_N61             2.5      1    0.500"));
        line = input.readLine();
        while (line.endsWith('\r') || line.endsWith('\n')) { line.chop(1); }
        QCOMPARE(line, QByteArray("BsG_S11        550    1           0.900"));
    }

    void summaryExplicit()
    {
        QByteArray data;
        QBuffer device(&data);
        device.open(QIODevice::WriteOnly);
        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionBoolean *>(options.get("description"))->set(true);
        qobject_cast<ComponentOptionBoolean *>(options.get("wavelength"))->set(true);
        qobject_cast<ComponentOptionBoolean *>(options.get("instrument"))->set(true);
        qobject_cast<ComponentOptionBoolean *>(options.get("mean"))->set(true);
        qobject_cast<ComponentOptionBoolean *>(options.get("stddev"))->set(true);
        qobject_cast<ComponentOptionBoolean *>(options.get("cover"))->set(true);
        ExternalSink *exp = component->createDataEgress(&device, options);
        QVERIFY(exp != NULL);

        Variant::Root meta;
        meta.write().metadataReal("Wavelength").setDouble(550);
        meta.write().metadataReal("Format").setString("00");
        exp->start();
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw_meta", "BsG_S11"), meta, 1000.0, 5000.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(1.001), 1000.0,
                              5000.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "BsG_S11", {"cover"}), Variant::Root(0.9),
                              1000.0, 5000.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "N_N61"), Variant::Root(1.5), 1000.0,
                              2300.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "N_N61"), Variant::Root(2.5), 2300.0,
                              2600.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "N_N61"), Variant::Root(3.5), 2600.0,
                              3000.0));
        exp->endData();
        Threading::pollInEventLoop([&] { return !exp->isFinished(); });
        QVERIFY(exp->wait());
        delete exp;

        QBuffer input(&data);
        input.open(QIODevice::ReadOnly);
        QByteArray line;
        line = input.readLine();
        while (line.endsWith('\r') || line.endsWith('\n')) { line.chop(1); }
        QCOMPARE(line,
                 QByteArray("        Description Wavelength Instrument Mean StdDev Coverage"));
        line = input.readLine();
        while (line.endsWith('\r') || line.endsWith('\n')) { line.chop(1); }
        QCOMPARE(line,
                 QByteArray("  N_N61                                    2.5      1    0.500"));
        line = input.readLine();
        while (line.endsWith('\r') || line.endsWith('\n')) { line.chop(1); }
        QCOMPARE(line,
                 QByteArray("BsG_S11                    550               1           0.900"));
    }

    void summaryInstrumentCSV()
    {
        QByteArray data;
        QBuffer device(&data);
        device.open(QIODevice::WriteOnly);
        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("instrument");
        qobject_cast<ComponentOptionEnum *>(options.get("display"))->set("csv");
        ExternalSink *exp = component->createDataEgress(&device, options);
        QVERIFY(exp != NULL);

        Variant::Root meta1;
        meta1.write().metadataReal("Source").hash("Manufacturer").setString("TSI");
        meta1.write().metadataReal("Source").hash("Model").setString("3563");
        meta1.write().metadataReal("Source").hash("SerialNumber").setInt64(1234);

        Variant::Root meta2;
        meta2.write().metadataReal("Source").hash("Manufacturer").setString("TSI");
        meta2.write().metadataReal("Source").hash("Model").setString("3010");

        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw_meta", "BsG_S11"), meta1, 1000.0, 5000.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw_meta", "BsB_S11"), meta1, 1000.0, 5000.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw_meta", "T_S11"), meta1, 1000.0, 5000.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw_meta", "N_N61"), meta2, 1000.0, 5000.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(1.001), 1000.0,
                              5000.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "BsB_S11"), Variant::Root(1.002), 1000.0,
                              5000.0));
        exp->endData();
        exp->start();
        Threading::pollInEventLoop([&] { return !exp->isFinished(); });
        exp->wait();
        delete exp;

        QBuffer input(&data);
        input.open(QIODevice::ReadOnly);
        QByteArray line;
        line = input.readLine();
        while (line.endsWith('\r') || line.endsWith('\n')) { line.chop(1); }
        QCOMPARE(line, QByteArray("Origin,Instrument,Coverage"));
        line = input.readLine();
        while (line.endsWith('\r') || line.endsWith('\n')) { line.chop(1); }
        QCOMPARE(line, QByteArray("N61,TSI 3010,"));
        line = input.readLine();
        while (line.endsWith('\r') || line.endsWith('\n')) { line.chop(1); }
        QCOMPARE(line, QByteArray("S11,TSI 3563 #1234,1.000"));
    }

    void summaryInstrumentCSVTranspose()
    {
        QByteArray data;
        QBuffer device(&data);
        device.open(QIODevice::WriteOnly);
        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("instrument");
        qobject_cast<ComponentOptionEnum *>(options.get("display"))->set("csvtranspose");
        ExternalSink *exp = component->createDataEgress(&device, options);
        QVERIFY(exp != NULL);

        Variant::Root meta1;
        meta1.write().metadataReal("Source").hash("Manufacturer").setString("TSI");
        meta1.write().metadataReal("Source").hash("Model").setString("3563");
        meta1.write().metadataReal("Source").hash("SerialNumber").setInt64(1234);

        Variant::Root meta2;
        meta2.write().metadataReal("Source").hash("Manufacturer").setString("TSI");
        meta2.write().metadataReal("Source").hash("Model").setString("3010");

        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw_meta", "BsG_S11"), meta1, 1000.0, 5000.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw_meta", "BsB_S11"), meta1, 1000.0, 5000.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw_meta", "T_S11"), meta1, 1000.0, 5000.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw_meta", "N_N61"), meta2, 1000.0, 5000.0));
        exp->incomingData(
                SequenceValue(SequenceName("_", "raw", "alias"), Variant::Root(), 1000.0, 5000.0));
        exp->endData();
        exp->start();
        Threading::pollInEventLoop([&] { return !exp->isFinished(); });
        exp->wait();
        delete exp;

        QBuffer input(&data);
        input.open(QIODevice::ReadOnly);
        QByteArray line;
        line = input.readLine();
        while (line.endsWith('\r') || line.endsWith('\n')) { line.chop(1); }
        QCOMPARE(line, QByteArray("Origin,N61,S11"));
        line = input.readLine();
        while (line.endsWith('\r') || line.endsWith('\n')) { line.chop(1); }
        QCOMPARE(line, QByteArray("Instrument,TSI 3010,TSI 3563 #1234"));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
