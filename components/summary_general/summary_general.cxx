/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLoggingCategory>

#include "summary_general.hxx"

#include "datacore/segment.hxx"
#include "algorithms/statistics.hxx"
#include "core/csv.hxx"
#include "core/util.hxx"
#include "core/threadpool.hxx"


Q_LOGGING_CATEGORY(log_component_summary_general, "cpd3.component.summary.general", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Algorithms;


namespace {

class Cell_LatestData : public SummaryCell {
    std::vector<SequenceValue> stack;

    void buildBreakdownMatrix(const Variant::Read &value,
                              const QVector<int> &size,
                              QStringList &result,
                              const QVector<int> &indices)
    {
        if (indices.size() < size.size()) {
            for (int i = 0, max = size.at(indices.size()); i < max; i++) {
                buildBreakdownMatrix(value, size, result, QVector<int>(indices) << i);
            }
            return;
        }

        QString add;
        for (QVector<int>::const_iterator index = indices.constBegin(),
                firstIndex = indices.constBegin(), endIndex = indices.constEnd();
                index != endIndex;
                ++index) {
            if (index != firstIndex)
                add.append(';');
            add.append(QString::number(*index));
        }


    }

public:
    Cell_LatestData() : stack()
    { }

    virtual ~Cell_LatestData()
    { }

    void incomingData(const SequenceValue &incoming) override
    { SequenceSegment::Stream::maintainValues(stack, incoming); }

    virtual QString outputValue()
    { return convert(SequenceSegment::Stream::overlayValues(stack)); }

protected:
    virtual QString convert(const Variant::Read &value) = 0;

    virtual QString formatValue(const Variant::Read &value)
    {
        switch (value.getType()) {
        case Variant::Type::Array: {
            QStringList components;
            for (auto v : value.toArray()) {
                components.append(formatValue(v));
            }
            return QObject::tr("(%1)", "array format").arg(
                    components.join(QObject::tr(":", "array join")));
        }
        case Variant::Type::Matrix: {
            QStringList components;
            for (auto v : value.toMatrix()) {
                QStringList indices;
                for (auto i : v.first) {
                    indices.append(QString::number(i));
                }
                components.append(QObject::tr("[%1]=%2", "matrix key format").arg(indices.join(';'),
                                                                                  formatValue(
                                                                                       v.second)));
            }
            return QObject::tr("(%1)", "matrix format").arg(
                    components.join(QObject::tr(":", "matrix join")));
        }
        case Variant::Type::Hash: {
            QStringList components;
            typedef std::pair<Variant::PathElement::HashIndex, Variant::Read> ChildType;
            std::vector<ChildType> children;
            Util::append(value.toHash(), children);
            std::sort(children.begin(), children.end(), [](const ChildType &a, const ChildType &b) {
                return a.first < b.first;
            });
            for (auto add : children) {
                components.append(QObject::tr("%1=%2", "hash key format").arg(
                        QString::fromStdString(add.first), formatValue(add.second)));
            }
            return QObject::tr("(%1)", "hash format").arg(
                    components.join(QObject::tr(":", "hash join")));
        }
        case Variant::Type::Keyframe: {
            QStringList components;
            for (auto v : value.toKeyframe()) {
                components.append(QObject::tr("%1=%2", "keyframe key format").arg(v.first)
                                                                             .arg(formatValue(
                                                                                            v.second)));
            }
            return QObject::tr("(%1)", "keyframe format").arg(
                    components.join(QObject::tr(":", "array join")));
        }
        default:
            break;
        }
        return value.toDisplayString();
    }
};

class Cell_ProcessValues : public SummaryCell {
    SequenceSegment::Stream reader;

    void processSegments(const SequenceSegment::Transfer &segments)
    {
        for (const auto &add : segments) {
            processSegment(add);
        }
    }

public:
    Cell_ProcessValues() : reader(), units()
    { }

    virtual ~Cell_ProcessValues()
    { }

    virtual bool registerInput(const SequenceName &unit)
    {
        units.insert(unit);
        return true;
    }

    void incomingData(const SequenceValue &incoming) override
    {
        processSegments(reader.add(incoming));
    }

protected:
    QSet<SequenceName> units;

    virtual void processSegment(SequenceSegment data) = 0;

    void finish()
    {
        processSegments(reader.finish());
    }
};


class Column_DataPath : public SummaryColumn {
    class Cell : public Cell_LatestData {
        Column_DataPath *parent;
    public:
        Cell(Column_DataPath *p) : parent(p)
        { }

        virtual ~Cell()
        { }

        virtual bool registerInput(const SequenceName &unit)
        {
            if (unit.hasFlavor(SequenceName::flavor_cover) ||
                    unit.hasFlavor(SequenceName::flavor_stats))
                return false;
            return !unit.isMeta();
        }

    protected:
        virtual QString convert(const Variant::Read &value)
        {
            return formatValue(value.getPath(parent->path));
        }
    };

    friend class Cell;

    QString path;
public:
    Column_DataPath(const QString &p) : path(p)
    { }

    virtual ~Column_DataPath()
    { }

    virtual QString header()
    { return path; }

    virtual SummaryCell *createCell(const SequenceName &unit)
    {
        Q_UNUSED(unit);
        return new Cell(this);
    }
};

class Column_MetadataPath : public SummaryColumn {
protected:
    class Cell : public Cell_LatestData {
        Column_MetadataPath *parent;
    public:
        Cell(Column_MetadataPath *p) : parent(p)
        { }

        virtual ~Cell()
        { }

        virtual bool registerInput(const SequenceName &unit)
        { return unit.isMeta(); }

    protected:
        virtual QString convert(const Variant::Read &meta)
        {
            auto v = meta.getPath(parent->path);
            if (!v.exists()) {
                switch (meta.getType()) {
                case Variant::Type::MetadataArray:
                case Variant::Type::MetadataMatrix:
                    v = meta.metadata("Children").getPath(parent->path);
                    break;
                default:
                    break;
                }
            }
            return formatValue(v);
        }
    };

    friend class Cell;

private:
    QString path;
public:
    Column_MetadataPath(const QString &p) : path(p)
    { }

    virtual ~Column_MetadataPath()
    { }

    virtual QString header()
    { return path; }

    virtual SummaryCell *createCell(const SequenceName &unit)
    {
        Q_UNUSED(unit);
        return new Cell(this);
    }
};

class Column_Wavelength : public Column_MetadataPath {
public:
    Column_Wavelength() : Column_MetadataPath("^Wavelength")
    { }

    virtual ~Column_Wavelength()
    { }

    virtual QString header()
    { return QObject::tr("Wavelength"); }
};

class Column_Description : public Column_MetadataPath {
public:
    Column_Description() : Column_MetadataPath("^Description")
    { }

    virtual ~Column_Description()
    { }

    virtual QString header()
    { return QObject::tr("Description"); }
};

class Column_Instrument : public Column_MetadataPath {
    class Cell : public Column_MetadataPath::Cell {
        Column_Instrument *parent;

        void addIfValid(QStringList &result,
                        const Variant::Read &v,
                        const QString &format = QString())
        {
            QString add(Column_MetadataPath::Cell::formatValue(v));
            if (add.isEmpty())
                return;
            if (format.isEmpty())
                result.append(add);
            else
                result.append(format.arg(add));
        }

    public:
        Cell(Column_Instrument *p) : Column_MetadataPath::Cell(p)
        { }

        virtual ~Cell()
        { }

    protected:
        virtual QString formatValue(const Variant::Read &value)
        {
            QStringList components;
            addIfValid(components, value["Manufacturer"]);
            addIfValid(components, value["Model"]);
            addIfValid(components, value["SerialNumber"], "#%1");
            return components.join(" ");
        }
    };

public:
    Column_Instrument() : Column_MetadataPath("^Source")
    { }

    virtual ~Column_Instrument()
    { }

    virtual QString header()
    { return QObject::tr("Instrument"); }

    virtual SummaryCell *createCell(const SequenceName &unit)
    {
        Q_UNUSED(unit);
        return new Cell(this);
    }
};

class Column_Mean : public SummaryColumn {
    class Cell : public Cell_ProcessValues {
        double sum;
        qint64 count;
        SequenceSegment latest;
    public:
        Cell() : sum(0), count(0), latest()
        { }

        virtual ~Cell()
        { }

        virtual QString outputValue()
        {
            finish();

            if (count == 0)
                return QString();

            QString format;
            for (QSet<SequenceName>::const_iterator u = units.constBegin(), endU = units.constEnd();
                    u != endU;
                    ++u) {
                if (!u->isMeta())
                    continue;
                format = latest[*u].metadata("Format").toDisplayString();
                if (!format.isEmpty())
                    break;
            }

            double v = sum / (double) count;

            if (format.isEmpty())
                return QString::number(v);
            return NumberFormat(format).apply(v, QChar());
        }

        virtual bool registerInput(const SequenceName &unit)
        {
            if (unit.hasFlavor(SequenceName::flavor_cover) ||
                    unit.hasFlavor(SequenceName::flavor_stats))
                return false;
            return Cell_ProcessValues::registerInput(unit);
        }

    protected:
        virtual void processSegment(SequenceSegment data)
        {
            for (QSet<SequenceName>::const_iterator u = units.constBegin(), endU = units.constEnd();
                    u != endU;
                    ++u) {
                if (u->isMeta())
                    continue;
                double v = data[*u].toDouble();
                if (!FP::defined(v))
                    continue;
                sum += v;
                ++count;
                break;
            }
            latest = data;
        }
    };

public:
    Column_Mean()
    { }

    virtual ~Column_Mean()
    { }

    virtual QString header()
    { return QObject::tr("Mean"); }

    virtual SummaryCell *createCell(const SequenceName &unit)
    {
        Q_UNUSED(unit);
        return new Cell;
    }

protected:
    bool eliminateIfSame() const override
    { return false; }
};

class Column_StdDev : public SummaryColumn {
    class Cell : public Cell_ProcessValues {
        double sum;
        double sumSq;
        qint64 count;
        SequenceSegment latest;
    public:
        Cell() : sum(0), sumSq(0), count(0), latest()
        { }

        virtual ~Cell()
        { }

        virtual QString outputValue()
        {
            finish();

            if (count == 0)
                return QString();

            double v = Statistics::calculateSD(sum / (double) count, sumSq, count);
            if (!FP::defined(v))
                return QString();

            QString format;
            for (QSet<SequenceName>::const_iterator u = units.constBegin(), endU = units.constEnd();
                    u != endU;
                    ++u) {
                if (!u->isMeta())
                    continue;
                format = latest[*u].metadata("Format").toDisplayString();
                if (!format.isEmpty())
                    break;
            }

            if (format.isEmpty())
                return QString::number(v);
            return NumberFormat(format).apply(v, QChar());
        }

        virtual bool registerInput(const SequenceName &unit)
        {
            if (unit.hasFlavor(SequenceName::flavor_cover) ||
                    unit.hasFlavor(SequenceName::flavor_stats))
                return false;
            return Cell_ProcessValues::registerInput(unit);
        }

    protected:
        virtual void processSegment(SequenceSegment data)
        {
            for (QSet<SequenceName>::const_iterator u = units.constBegin(), endU = units.constEnd();
                    u != endU;
                    ++u) {
                if (u->isMeta())
                    continue;
                double v = data[*u].toDouble();
                if (!FP::defined(v))
                    continue;
                sum += v;
                sumSq += v * v;
                ++count;
                break;
            }
            latest = data;
        }
    };

public:
    Column_StdDev()
    { }

    virtual ~Column_StdDev()
    { }

    virtual QString header()
    { return QObject::tr("StdDev"); }

    virtual SummaryCell *createCell(const SequenceName &unit)
    {
        Q_UNUSED(unit);
        return new Cell;
    }

protected:
    bool eliminateIfSame() const override
    { return false; }
};

class Column_Coverage : public SummaryColumn {
    class Cell : public Cell_ProcessValues {
        SummaryGeneral *summary;
        double timeDefined;
    public:
        Cell(SummaryGeneral *sum) : summary(sum), timeDefined(0.0)
        { }

        virtual ~Cell()
        { }

        virtual QString outputValue()
        {
            finish();

            if (timeDefined <= 0.0)
                return QString();

            double start = summary->getDataStart();
            double end = summary->getDataEnd();
            if (!FP::defined(start) || !FP::defined(end))
                return QString();
            double totalTime = end - start;
            if (totalTime <= 0.0)
                return QString();

            double cover = timeDefined / totalTime;
            if (cover <= 0.0)
                return QString();
            if (cover > 1.0)
                cover = 1.0;

            return NumberFormat(1, 3).apply(cover, QChar());
        }

        virtual bool registerInput(const SequenceName &unit)
        {
            if (unit.hasFlavor(SequenceName::flavor_stats))
                return false;
            if (unit.isMeta())
                return false;
            return Cell_ProcessValues::registerInput(unit);
        }

    protected:
        virtual void processSegment(SequenceSegment data)
        {
            if (!FP::defined(data.getStart()) || !FP::defined(data.getEnd()))
                return;

            double totalTime = data.getEnd() - data.getStart();
            if (totalTime <= 0.0)
                return;

            double bestTime = 0;
            for (QSet<SequenceName>::const_iterator u = units.constBegin(), endU = units.constEnd();
                    u != endU;
                    ++u) {
                if (u->hasFlavor(SequenceName::flavor_cover))
                    continue;

                if (!data[*u].exists())
                    continue;

                double c = data[u->withFlavor(SequenceName::flavor_cover)].toDouble();
                if (FP::defined(c) && c >= 0.0 && c <= 1.0) {
                    bestTime = std::max(bestTime, c * totalTime);
                } else {
                    bestTime = totalTime;
                    break;
                }
            }

            timeDefined += bestTime;
        }
    };

    SummaryGeneral *summary;
public:
    Column_Coverage(SummaryGeneral *sum) : summary(sum)
    { }

    virtual ~Column_Coverage()
    { }

    virtual QString header()
    { return QObject::tr("Coverage"); }

    virtual SummaryCell *createCell(const SequenceName &unit)
    {
        Q_UNUSED(unit);
        return new Cell(summary);
    }
};


class Row_Unit : public SummaryRow {
protected:
    SequenceName unit;

    virtual SequenceName compareUnit(SequenceName input) const
    {
        input.clearMeta();
        input.removeFlavor(SequenceName::flavor_cover);
        input.removeFlavor(SequenceName::flavor_stats);
        return input;
    }

public:
    Row_Unit(const SequenceName &u) : unit(u)
    { }

    virtual ~Row_Unit()
    { }

    virtual bool sortLessThan(const SummaryRow *other) const
    {
        const Row_Unit *row = static_cast<const Row_Unit *>(other);
        return SequenceName::OrderDisplay()(unit, row->unit);
    }

    virtual QStringList rowNameComponents() const
    {
        QStringList result;
        result.append(unit.getStationQString().toUpper());
        result.append(unit.getArchiveQString().toUpper());

        QStringList sorted = Util::to_qstringlist(unit.getFlavors());
        for (auto &f : sorted) {
            f = f.toUpper();
        }
        std::sort(sorted.begin(), sorted.end());
        result.append(sorted.join(";"));

        result.append(unit.getVariableQString());
        return result;
    }

    virtual bool primaryInput(const SequenceName &unit) const
    { return this->unit == compareUnit(unit); }
};

class Row_Unit_NoFlavors : public Row_Unit {
protected:
    virtual SequenceName compareUnit(SequenceName input) const
    { return Row_Unit::compareUnit(input).withoutAllFlavors(); }

public:
    Row_Unit_NoFlavors(const SequenceName &u) : Row_Unit(u)
    { }

    virtual ~Row_Unit_NoFlavors()
    { }
};

class Row_Instrument : public Row_Unit {
protected:
    virtual SequenceName compareUnit(SequenceName input) const
    {
        input.clearMeta();
        input.clearFlavors();
        input.setVariable(Util::suffix(input.getVariable(), '_'));
        return input;
    }

public:
    Row_Instrument(const SequenceName &u) : Row_Unit(u)
    { }

    virtual ~Row_Instrument()
    { }
};


enum GlobalMode {
    Mode_Variable, Mode_Instrument,
};

}


SummaryGeneral::SummaryGeneral(std::unique_ptr<IO::Generic::Stream> &&stream,
                               const ComponentOptions &options) : stream(std::move(stream)),
                                                                  pendingProcessing(),
                                                                  terminated(false),
                                                                  dataEnded(false),
                                                                  threadFinished(false),
                                                                  firstDefinedStart(
                                                                          FP::undefined()),
                                                                  lastDefinedEnd(FP::undefined())
{
    rowMode = NormalFanout;
    outputMode = Space;

    bool columnDescription = false;
    bool columnWavelength = true;
    bool columnInstrument = false;
    bool columnMean = true;
    bool columnStdDev = true;
    bool columnCoverage = true;

    if (options.isSet("mode")) {
        switch ((GlobalMode) (qobject_cast<ComponentOptionEnum *>(options.get("mode"))->get()
                                                                                      .getID())) {
        case Mode_Variable:
            break;
        case Mode_Instrument:
            rowMode = Instrument;
            columnDescription = false;
            columnWavelength = false;
            columnInstrument = true;
            columnMean = false;
            columnStdDev = false;
            columnCoverage = true;
            break;
        }
    }

    if (options.isSet("description")) {
        if (qobject_cast<ComponentOptionBoolean *>(options.get("description"))->get()) {
            columns.emplace_back(new Column_Description);
            columns.back()->setExplicitlyCreated(true);
        }
    } else if (columnDescription) {
        columns.emplace_back(new Column_Description);
    }

    if (options.isSet("wavelength")) {
        if (qobject_cast<ComponentOptionBoolean *>(options.get("wavelength"))->get()) {
            columns.emplace_back(new Column_Wavelength);
            columns.back()->setExplicitlyCreated(true);
        }
    } else if (columnWavelength) {
        columns.emplace_back(new Column_Wavelength);
    }

    if (options.isSet("instrument")) {
        if (qobject_cast<ComponentOptionBoolean *>(options.get("instrument"))->get()) {
            columns.emplace_back(new Column_Instrument);
            columns.back()->setExplicitlyCreated(true);
        }
    } else if (columnInstrument) {
        columns.emplace_back(new Column_Instrument);
    }

    if (options.isSet("mean")) {
        if (qobject_cast<ComponentOptionBoolean *>(options.get("mean"))->get()) {
            columns.emplace_back(new Column_Mean);
            columns.back()->setExplicitlyCreated(true);
        }
    } else if (columnMean) {
        columns.emplace_back(new Column_Mean);
    }

    if (options.isSet("stddev")) {
        if (qobject_cast<ComponentOptionBoolean *>(options.get("stddev"))->get()) {
            columns.emplace_back(new Column_StdDev);
            columns.back()->setExplicitlyCreated(true);
        }
    } else if (columnStdDev) {
        columns.emplace_back(new Column_StdDev);
    }

    if (options.isSet("cover")) {
        if (qobject_cast<ComponentOptionBoolean *>(options.get("cover"))->get()) {
            columns.emplace_back(new Column_Coverage(this));
            columns.back()->setExplicitlyCreated(true);
        }
    } else if (columnCoverage) {
        columns.emplace_back(new Column_Coverage(this));
    }

    if (options.isSet("metadata")) {
        QStringList sorted
                (qobject_cast<ComponentOptionStringSet *>(options.get("metadata"))->get().toList());
        std::sort(sorted.begin(), sorted.end());
        for (QList<QString>::const_iterator add = sorted.constBegin(), end = sorted.constEnd();
                add != end;
                ++add) {
            columns.emplace_back(new Column_MetadataPath(*add));
            columns.back()->setExplicitlyCreated(true);
        }
    }

    if (options.isSet("data")) {
        QStringList sorted
                (qobject_cast<ComponentOptionStringSet *>(options.get("data"))->get().toList());
        std::sort(sorted.begin(), sorted.end());
        for (QList<QString>::const_iterator add = sorted.constBegin(), end = sorted.constEnd();
                add != end;
                ++add) {
            columns.emplace_back(new Column_DataPath(*add));
            columns.back()->setExplicitlyCreated(true);
        }
    }


    if (options.isSet("rows")) {
        rowMode =
                (RowMode) (qobject_cast<ComponentOptionEnum *>(options.get("rows"))->get().getID());
    }
    if (options.isSet("display")) {
        outputMode =
                (OutputMode) (qobject_cast<ComponentOptionEnum *>(options.get("display"))->get()
                                                                                         .getID());
    }
}

SummaryGeneral::~SummaryGeneral()
{
    signalTerminate();
    if (thread.joinable())
        thread.join();
}

void SummaryGeneral::incomingData(const CPD3::Data::SequenceValue::Transfer &values)
{
    if (values.empty())
        return;
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        Util::append(values, pendingProcessing);
    }
    notify.notify_all();
}

void SummaryGeneral::incomingData(CPD3::Data::SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        Util::append(std::move(values), pendingProcessing);
    }
    notify.notify_all();
}

void SummaryGeneral::incomingData(const CPD3::Data::SequenceValue &value)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        pendingProcessing.emplace_back(value);
    }
    notify.notify_all();
}

void SummaryGeneral::incomingData(CPD3::Data::SequenceValue &&value)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        pendingProcessing.emplace_back(std::move(value));
    }
    notify.notify_all();
}

void SummaryGeneral::endData()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        dataEnded = true;
    }
    notify.notify_all();
}

void SummaryGeneral::start()
{ thread = std::thread(&SummaryGeneral::run, this); }

void SummaryGeneral::run()
{
    for (;;) {
        SequenceValue::Transfer toProcess;
        bool processEnd = false;
        {
            std::unique_lock<std::mutex> lock(mutex);
            for (;;) {
                if (terminated) {
                    threadFinished = true;
                    lock.unlock();
                    stream.reset();
                    notify.notify_all();
                    finished();
                    return;
                }
                toProcess = std::move(pendingProcessing);
                processEnd = dataEnded;
                if (!toProcess.empty() || processEnd)
                    break;
                notify.wait(lock);
            }
        }

        if (toProcess.size() > stallThreshold)
            notify.notify_all();

        dispatchData(toProcess);
        if (!processEnd)
            continue;

        generateOutput();

        {
            std::lock_guard<std::mutex> lock(mutex);
            threadFinished = true;
        }
        stream.reset();
        notify.notify_all();
        finished();
        return;
    }
}

void SummaryGeneral::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    notify.notify_all();
}

bool SummaryGeneral::isFinished()
{
    std::lock_guard<std::mutex> lock(mutex);
    return threadFinished;
}

bool SummaryGeneral::wait(double timeout)
{ return Threading::waitForTimeout(timeout, mutex, notify, [this] { return threadFinished; }); }

void SummaryGeneral::stall(std::unique_lock<std::mutex> &lock)
{
    while (pendingProcessing.size() > stallThreshold) {
        if (terminated)
            return;
        notify.wait(lock);
    }
}

template<typename T>
static void vectorAppend(std::vector<T> &target, const std::vector<T> &source)
{ target.insert(target.begin(), source.begin(), source.end()); }

static bool shouldCreateRow(const SequenceName &unit)
{
    if (unit.isDefaultStation())
        return false;
    if (unit.getVariable() == "alias") {
        if (unit.getArchive() == "raw" ||
                unit.getArchive() == "clean" ||
                unit.getArchive() == "avgh")
            return false;
    }
    return true;
}

void SummaryGeneral::dispatchData(const SequenceValue::Transfer &values)
{
    for (const auto &v : values) {
        if (!v.getUnit().isMeta()) {
            if (FP::defined(v.getStart())) {
                if (!FP::defined(firstDefinedStart))
                    firstDefinedStart = v.getStart();
            }
            if (FP::defined(v.getEnd())) {
                if (!FP::defined(lastDefinedEnd) || v.getEnd() > lastDefinedEnd)
                    lastDefinedEnd = v.getEnd();
            }
        }

        auto target = dispatch.find(v.getName());
        if (target == dispatch.end()) {
            auto created = dispatch.emplace(v.getName(), std::vector<SummaryCell *>()).first;

            bool havePrimary = false;
            for (const auto &r : rows) {
                if (r->primaryInput(v.getUnit())) {
                    havePrimary = true;
                    vectorAppend(created->second, r->registerInput(v.getUnit()));
                } else {
                    vectorAppend(created->second, r->registerInput(v.getUnit(), true));
                }
            }
            if (!havePrimary && shouldCreateRow(v.getUnit())) {
                std::unique_ptr<SummaryRow> row;
                switch (rowMode) {
                case NormalFanout: {
                    SequenceName unit(v.getUnit());
                    unit.clearMeta();
                    unit.removeFlavor(SequenceName::flavor_cover);
                    unit.removeFlavor(SequenceName::flavor_stats);
                    row.reset(new Row_Unit(unit));
                    break;
                }
                case FlattenFlavors: {
                    SequenceName unit(v.getUnit());
                    unit.clearMeta();
                    unit.clearFlavors();
                    row.reset(new Row_Unit_NoFlavors(unit));
                    break;
                }
                case Instrument: {
                    SequenceName unit(v.getUnit());
                    unit.clearMeta();
                    unit.clearFlavors();
                    unit.setVariable(Util::suffix(unit.getVariable(), '_'));
                    if (unit.getVariable().empty())
                        break;
                    row.reset(new Row_Instrument(unit));
                    break;
                }
                }

                if (row) {
                    for (const auto &c : columns) {
                        row->appendCell(c->createCell(v.getUnit()));
                    }
                    vectorAppend(created->second, row->registerInput(v.getUnit()));

                    rows.emplace_back(std::move(row));
                }
            }

            target = created;
        }

        for (auto c : target->second) {
            c->incomingData(v);
        }
    }
}

static bool rowSortCompare(const std::unique_ptr<SummaryRow> &a,
                           const std::unique_ptr<SummaryRow> &b)
{ return a->sortLessThan(b.get()); }

static QString buildRowTitle(const QStringList &components,
                             const QList<bool> &rowTitleComponentMultiple)
{
    QString result;
    for (int i = 0, max = components.size() - 1; i < max; i++) {
        if (!rowTitleComponentMultiple[i])
            continue;
        if (!result.isEmpty())
            result.append(':');
        result.append(components[i]);
    }
    if (!result.isEmpty())
        result.append(':');
    result.append(components.last());
    return result;
}

static QList<QStringList> transposeMatrix(const QList<QStringList> &matrix)
{
    QList<QStringList> result;
    for (int c = 0, maxC = matrix.first().size(); c < maxC; ++c) {
        result.append(QStringList());
        for (int r = 0, maxR = matrix.size(); r < maxR; ++r) {
            result.last().append(QString());
        }
    }
    for (int r = 0, maxR = matrix.size(); r < maxR; ++r) {
        for (int c = 0, maxC = matrix.at(r).size(); c < maxC; ++c) {
            result[c][r] = matrix[r][c];
        }
    }
    return result;
}

void SummaryGeneral::generateOutput()
{
    if (rows.empty())
        return;

    QStringList columnHeaders;
    for (const auto &c : columns) {
        columnHeaders.append(c->header());
    }

    /* Build row data */
    std::sort(rows.begin(), rows.end(), rowSortCompare);
    QList<QStringList> rowTitles;
    QList<bool> rowTitleComponentMultiple;
    QList<QStringList> rowColumnData;
    for (const auto &r : rows) {
        QStringList titleComponents(r->rowNameComponents());
        if (!rowTitles.isEmpty()) {
            while (rowTitleComponentMultiple.size() < rowTitles.last().size()) {
                rowTitleComponentMultiple.append(false);
            }
            while (rowTitleComponentMultiple.size() < titleComponents.size()) {
                rowTitleComponentMultiple.append(true);
            }
            for (int i = titleComponents.size(), max = rowTitleComponentMultiple.size();
                    i < max;
                    i++) {
                rowTitleComponentMultiple[i] = true;
            }
            for (int i = 0, max = qMin(titleComponents.size(), rowTitles.last().size());
                    i < max;
                    i++) {
                if (rowTitles.last().at(i) == titleComponents.at(i))
                    continue;
                rowTitleComponentMultiple[i] = true;
            }
        } else {
            while (rowTitleComponentMultiple.size() < titleComponents.size()) {
                rowTitleComponentMultiple.append(false);
            }
        }
        rowTitles.append(titleComponents);

        rowColumnData.append(r->columnValues());
        Q_ASSERT(rowColumnData.last().size() == columnHeaders.size());
    }

    /* Eliminate columns */
    QList<bool> displayColumn;
    for (int i = 0, max = columns.size(); i < max; i++) {
        displayColumn.append(true);

        QStringList check;
        for (QList<QStringList>::const_iterator row = rowColumnData.constBegin(),
                end = rowColumnData.constEnd(); row != end; ++row) {
            Q_ASSERT(i < row->size());
            check.append(row->at(i));
        }

        if (!columns.at(i)->shouldEliminate(check))
            continue;

        displayColumn.last() = false;
    }

    /* Build output matrix */
    QList<QStringList> matrix;
    matrix.append(QStringList() << QString());
    for (int i = 0, max = columnHeaders.size(); i < max; i++) {
        if (!displayColumn.at(i))
            continue;
        matrix.last().append(columnHeaders.at(i));
    }
    for (int i = 0, max = rowColumnData.size(); i < max; i++) {
        matrix.append(QStringList() << buildRowTitle(rowTitles.at(i), rowTitleComponentMultiple));

        for (int j = 0, max = rowColumnData.at(i).size(); j < max; j++) {
            if (!displayColumn.at(j))
                continue;
            matrix.last().append(rowColumnData.at(i).at(j));
        }
    }

    Q_ASSERT(!matrix.isEmpty());

    switch (outputMode) {
    case SpaceTranspose:
        matrix = transposeMatrix(matrix);
        /* Fall through */
    case Space: {
        QList<int> columnWidths;
        for (QList<QStringList>::const_iterator row = matrix.constBegin(), end = matrix.constEnd();
                row != end;
                ++row) {
            for (int i = 0, max = row->size(); i < max; i++) {
                while (i >= columnWidths.size()) {
                    columnWidths.append(0);
                }
                columnWidths[i] = qMax(columnWidths.at(i), row->at(i).length());
            }
        }
        for (QList<QStringList>::const_iterator row = matrix.constBegin(), end = matrix.constEnd();
                row != end;
                ++row) {
            QString output;
            for (int i = 0, max = row->size(); i < max; i++) {
                if (i != 0)
                    output.append(' ');
                output.append(row->at(i).rightJustified(columnWidths.at(i), ' '));
            }
            output.append('\n');
            stream->write(output);
        }
        break;
    }
    case CSVTranspose:
        matrix = transposeMatrix(matrix);
        /* Fall through */
    case CSV:
        matrix[0][0] = QObject::tr("Origin");
        for (QList<QStringList>::const_iterator row = matrix.constBegin(), end = matrix.constEnd();
                row != end;
                ++row) {
            QString output(CSV::join(*row));
            output.append('\n');
            stream->write(output);
        }
        break;
    }
}

SummaryCell::SummaryCell() = default;

SummaryCell::~SummaryCell() = default;

bool SummaryCell::registerExternal(const SequenceName &unit)
{
    Q_UNUSED(unit);
    return false;
}

SummaryColumn::SummaryColumn() : explicitlyCreated(false)
{ }

SummaryColumn::~SummaryColumn() = default;

bool SummaryColumn::shouldEliminate(const QStringList &rows)
{
    if (explicitlyCreated)
        return false;
    Q_ASSERT(!rows.isEmpty());

    bool haveNonEmpty = !rows.first().isEmpty();
    for (QStringList::const_iterator check = rows.constBegin() + 1, endCheck = rows.constEnd();
            check != endCheck;
            ++check) {
        if (*check != rows.first())
            return false;
        if (!check->isEmpty())
            haveNonEmpty = true;
    }
    if (!haveNonEmpty)
        return eliminateIfEmpty();
    if (rows.size() > 1)
        return eliminateIfSame();
    return false;
}

bool SummaryColumn::eliminateIfEmpty() const
{ return true; }

bool SummaryColumn::eliminateIfSame() const
{ return true; }

SummaryRow::SummaryRow() = default;

SummaryRow::~SummaryRow()
{
    qDeleteAll(cells);
}

void SummaryRow::appendCell(SummaryCell *cell)
{ cells.append(cell); }

QStringList SummaryRow::columnValues() const
{
    QStringList result;
    for (QList<SummaryCell *>::const_iterator cell = cells.constBegin(), end = cells.constEnd();
            cell != end;
            ++cell) {
        result.append((*cell)->outputValue());
    }
    return result;
}

std::vector<SummaryCell *> SummaryRow::registerInput(const SequenceName &unit, bool external)
{
    std::vector<SummaryCell *> result;
    for (QList<SummaryCell *>::const_iterator cell = cells.constBegin(), end = cells.constEnd();
            cell != end;
            ++cell) {
        if (external) {
            if (!(*cell)->registerExternal(unit))
                continue;
        } else {
            if (!(*cell)->registerInput(unit))
                continue;
        }
        result.push_back(*cell);
    }
    return result;
}


ComponentOptions SummaryGeneralComponent::getOptions()
{
    ComponentOptions options;

    ComponentOptionEnum *mode = new ComponentOptionEnum(tr("mode", "name"), tr("Base mode"),
                                                        tr("This is the base summary mode.  This determines the results "
                                                           "output as well as the method of displaying them."),
                                                        tr("Per-variable summary"), -1);
    mode->add(Mode_Variable, "variable", tr("variable", "mode name"),
              tr("Summary output on a per-variable basis"));
    mode->add(Mode_Instrument, "instrument", tr("instrument", "mode name"),
              tr("Summary output on an instrument"));
    options.add("mode", mode);


    ComponentOptionEnum *rows = new ComponentOptionEnum(tr("rows", "name"), tr("Row display mode"),
                                                        tr("This is the method that rows are created using.  This determines "
                                                           "how data are placed into various output binnings."),
                                                        tr("Determined by primary mode"));
    rows->add(SummaryGeneral::NormalFanout, "variable", tr("variable", "mode name"),
              tr("Create rows for each unique station, archive, variable and "
                 "flavors combination"));
    rows->add(SummaryGeneral::FlattenFlavors, "flavorless", tr("flavorless", "mode name"),
              tr("Create rows for each station, archive, and variable combination"));
    rows->add(SummaryGeneral::Instrument, "instrument", tr("instrument", "mode name"),
              tr("Create a row for each instrument code (e.x. \"S11\")"));
    options.add("rows", rows);

    ComponentOptionEnum *output = new ComponentOptionEnum(tr("display", "name"), tr("Display mode"),
                                                          tr("This is the mode used to format and display the final results."),
                                                          tr("Determined by primary mode"));
    output->add(SummaryGeneral::Space, "space", tr("space", "mode name"),
                tr("Space separated data with the unique identifiers as rows."));
    output->add(SummaryGeneral::SpaceTranspose, "transpose", tr("transpose", "mode name"),
                tr("Space separated data with the unique identifiers as columns."));
    output->add(SummaryGeneral::CSV, "csv", tr("csv", "mode name"),
                tr("Comma separated data with the unique identifiers as rows."));
    output->add(SummaryGeneral::CSVTranspose, "csvtranspose", tr("csvtranspose", "mode name"),
                tr("Comma separated data with the unique identifiers as columns."));
    options.add("display", output);


    options.add("description", new ComponentOptionBoolean(tr("description", "name"),
                                                          tr("Enable description output"),
                                                          tr("If set then the summary includes the variable description from "
                                                             "the metadata."), QString()));
    options.add("wavelength",
                new ComponentOptionBoolean(tr("wavelength", "name"), tr("Enable wavelength output"),
                                           tr("If set then the summary includes the variable wavelength from "
                                              "the metadata."), QString()));
    options.add("instrument",
                new ComponentOptionBoolean(tr("instrument", "name"), tr("Enable instrument output"),
                                           tr("If set then the summary includes the source instrument from the "
                                              "metadata."), QString()));
    options.add("mean", new ComponentOptionBoolean(tr("mean", "name"), tr("Enable mean output"),
                                                   tr("If set then the summary includes the global arithmetic mean of "
                                                      "each output."), QString()));
    options.add("stddev", new ComponentOptionBoolean(tr("stddev", "name"),
                                                     tr("Enable standard deviation output"),
                                                     tr("If set then the summary includes the global standard deviation of "
                                                        "each output."), QString()));
    options.add("cover",
                new ComponentOptionBoolean(tr("cover", "name"), tr("Enable coverage output"),
                                           tr("If set then the summary includes the global coverage percentage "
                                              "of each output."), QString()));


    options.add("metadata", new ComponentOptionStringSet(tr("metadata", "name"),
                                                         tr("Additional metadata paths"),
                                                         tr("This sets any additional metadata paths to include in the final "
                                                            "summary output.  Because these are looked up from metadata values "
                                                            "they will usually begin with a \"^\" indicating a metadata "
                                                            "path."), QString()));
    options.add("data",
                new ComponentOptionStringSet(tr("data", "name"), tr("Additional data paths"),
                                             tr("This sets any additional data paths to include in the final "
                                                "summary output."), QString()));

    return options;
}

QList<ComponentExample> SummaryGeneralComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Space separated columns by variable")));

    options = getOptions();
    qobject_cast<ComponentOptionEnum *>(options.get("rows"))->set("flavorless");
    qobject_cast<ComponentOptionEnum *>(options.get("display"))->set("csv");
    qobject_cast<ComponentOptionBoolean *>(options.get("wavelength"))->set(true);
    qobject_cast<ComponentOptionBoolean *>(options.get("mean"))->set(false);
    qobject_cast<ComponentOptionBoolean *>(options.get("cover"))->set(false);
    qobject_cast<ComponentOptionStringSet *>(options.get("metadata"))->set(
            QSet<QString>() << "^Units" << "^Format");
    examples.append(ComponentExample(options,
                                     tr("Comma separated displaying wavelengths, units and formats")));

    return examples;
}

ExternalSink *SummaryGeneralComponent::createDataSink(std::unique_ptr<IO::Generic::Stream> &&stream,
                                                      const ComponentOptions &options)
{ return new SummaryGeneral(std::move(stream), options); }