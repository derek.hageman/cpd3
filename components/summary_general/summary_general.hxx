/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef SUMMARYGENERAL_H
#define SUMMARYGENERAL_H

#include "core/first.hxx"

#include <vector>
#include <mutex>
#include <condition_variable>
#include <QtGlobal>
#include <QObject>
#include <QIODevice>
#include <QString>

#include "core/component.hxx"
#include "datacore/externalsink.hxx"

class SummaryCell {
public:
    SummaryCell();

    virtual ~SummaryCell();

    virtual bool registerInput(const CPD3::Data::SequenceName &unit) = 0;

    virtual bool registerExternal(const CPD3::Data::SequenceName &unit);

    virtual void incomingData(const CPD3::Data::SequenceValue &incoming) = 0;

    virtual QString outputValue() = 0;
};

class SummaryColumn {
    bool explicitlyCreated;
public:
    SummaryColumn();

    virtual ~SummaryColumn();

    virtual QString header() = 0;

    virtual SummaryCell *createCell(const CPD3::Data::SequenceName &unit) = 0;

    void setExplicitlyCreated(bool v)
    { explicitlyCreated = v; }

    virtual bool shouldEliminate(const QStringList &rows);

protected:
    virtual bool eliminateIfEmpty() const;

    virtual bool eliminateIfSame() const;
};

class SummaryRow {
    QList<SummaryCell *> cells;
public:
    SummaryRow();

    virtual ~SummaryRow();

    virtual void appendCell(SummaryCell *cell);

    virtual bool sortLessThan(const SummaryRow *other) const = 0;

    virtual QStringList rowNameComponents() const = 0;

    virtual bool primaryInput(const CPD3::Data::SequenceName &unit) const = 0;

    virtual QStringList columnValues() const;

    virtual std::vector<SummaryCell *> registerInput(const CPD3::Data::SequenceName &unit,
                                                     bool external = false);
};

class SummaryGeneralComponent;

class SummaryGeneral : public CPD3::Data::ExternalSink {
    friend class SummaryGeneralComponent;

    std::unique_ptr<CPD3::IO::Generic::Stream> stream;

    std::vector<std::unique_ptr<SummaryColumn>> columns;
    std::vector<std::unique_ptr<SummaryRow>> rows;
    CPD3::Data::SequenceName::Map<std::vector<SummaryCell *>> dispatch;

    std::mutex mutex;
    std::condition_variable notify;
    CPD3::Data::SequenceValue::Transfer pendingProcessing;
    bool terminated;
    bool dataEnded;
    bool threadFinished;

    double firstDefinedStart;
    double lastDefinedEnd;

    enum RowMode {
        NormalFanout, FlattenFlavors, Instrument,
    };
    RowMode rowMode;

    enum OutputMode {
        Space, SpaceTranspose, CSV, CSVTranspose,
    };
    OutputMode outputMode;

    std::thread thread;

    void dispatchData(const CPD3::Data::SequenceValue::Transfer &values);

    void generateOutput();

    void stall(std::unique_lock<std::mutex> &lock);

    void run();

public:
    SummaryGeneral(std::unique_ptr<CPD3::IO::Generic::Stream> &&stream,
                   const CPD3::ComponentOptions &options);

    virtual ~SummaryGeneral();

    void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

    void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

    void incomingData(const CPD3::Data::SequenceValue &values) override;

    void incomingData(CPD3::Data::SequenceValue &&values) override;

    void endData() override;

    void signalTerminate() override;

    void start() override;

    bool isFinished() override;

    bool wait(double timeout = CPD3::FP::undefined()) override;

    inline double getDataStart() const
    { return firstDefinedStart; }

    inline double getDataEnd() const
    { return lastDefinedEnd; }
};

class SummaryGeneralComponent : public QObject, virtual public CPD3::Data::ExternalSinkComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalSinkComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.summary_general"
                              FILE
                              "summary_general.json")

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    CPD3::Data::ExternalSink *createDataSink(std::unique_ptr<
            CPD3::IO::Generic::Stream> &&stream = {},
                                             const CPD3::ComponentOptions &options = {}) override;
};


#endif
