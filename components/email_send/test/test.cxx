/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QDir>
#include <QCryptographicHash>
#include <QHostInfo>
#include <QFile>
#include <QTemporaryFile>

#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "core/actioncomponent.hxx"
#include "core/number.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"
#include "database/runlock.hxx"
#include "database/storage.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class TestComponent : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

    ActionComponentTime *component;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component = qobject_cast<ActionComponentTime *>(ComponentLoader::create("email_send"));
        QVERIFY(component);

        QVERIFY(databaseFile.open());
        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("profile")));
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("timeout")));
    }

#ifdef Q_OS_UNIX

    void basic()
    {
        QTemporaryFile scriptFile;
        QVERIFY(scriptFile.open());
        QTemporaryFile outputFile;
        QVERIFY(outputFile.open());
        outputFile.close();

        scriptFile.write(QString("#!/bin/sh\n"
                                         "exec cat - > '%1'\n").arg(outputFile.fileName())
                                                               .toLatin1());
        scriptFile.close();

        Variant::Root config;
        config["/Send/aerosol/Relay/Program"].setString(
                QString("bash %1").arg(scriptFile.fileName()));
        config["/Send/aerosol/Recipients"].setString("Derek.Hageman@noaa.gov(html4)");
        config["/Send/aerosol/Actions/#0"].setString("echo Add this message");

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "configuration", "email"}, config, FP::undefined(),
                              FP::undefined())});
        {
            QString rcProcessingLog("processlog sfa");
            Database::RunLock rc(Database::Storage::sqlite(databaseFile.fileName().toStdString()));

            static const quint8 rcLogVersion = 1;
            QByteArray data;
            QDataStream stream(&data, QIODevice::WriteOnly);
            Variant::Root logValue;
            logValue["Type"].setString("CompletedFile");
            logValue["Time"].setDouble(1368457572.0);
            logValue["File/BaseName"].setString("inputFile.cpd3");
            logValue["File/Size"].setInt64(8192);
            logValue["File/ProcessingTime"].setDouble(60.0);
            logValue["File/AcceptedValues"].setInt64(100);
            stream << rcLogVersion << (std::vector<Variant::Root>{std::move(logValue)});
            rc.blindSet(rcProcessingLog, data);
        }

        ComponentOptions options(component->getOptions());
        CPD3Action *action = component->createTimeAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        QFile readData(outputFile.fileName());
        QVERIFY(readData.open(QIODevice::ReadOnly));
        QString outputEmail(QString::fromUtf8(readData.readAll()));
        readData.close();

        QVERIFY(outputEmail.contains("Add this message"));
        QVERIFY(outputEmail.contains("</table>"));
        QVERIFY(outputEmail.contains("inputFile.cpd3"));
    }

#endif
};

QTEST_MAIN(TestComponent)

#include "test.moc"
