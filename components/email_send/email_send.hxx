/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef EMAILSEND_H
#define EMAILSEND_H

#include "core/first.hxx"

#include <mutex>
#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QList>
#include <QStringList>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "core/textsubstitution.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/stream.hxx"

class EmailSend : public CPD3::CPD3Action {
Q_OBJECT

    std::string profile;
    std::vector<CPD3::Data::SequenceName::Component> stations;
    double start;
    double end;
    bool doLock;
    double lockTimeout;

    bool terminated;
    CPD3::CPD3Action *activeAction;
    CPD3::Data::ExternalConverter *activeInput;
    std::mutex mutex;

    CPD3::Threading::Signal<> terminateRequested;

    bool testTerminated();

    CPD3::TextSubstitutionStack substitutions;

    bool canAbort;

    std::vector<CPD3::Data::Variant::Root> processingLog;
    std::vector<CPD3::Data::Variant::Root> messageData;

    void convertBufferOutput(const CPD3::Data::SequenceValue::Transfer &values);

    bool invokeCommand(const QString &program, const QStringList &arguments,
                       const CPD3::Data::Variant::Read &config);

    bool executeAction(CPD3::CPD3Action *action, const CPD3::Data::Variant::Read &config);

    bool executeInput(CPD3::Data::ExternalConverter *input, const CPD3::Data::Variant::Read &config,
                      QIODevice *device = NULL);

    bool handleAction(const CPD3::Data::SequenceName::Component &station,
                      const CPD3::Data::Variant::Read &action);


    void expandInterval(const CPD3::Data::SequenceName::Component &station,
                        const CPD3::Data::Variant::Read &config,
                        double &start,
                        double end,
                        double priorStart);

    bool processStation(const CPD3::Data::SequenceName::Component &station,
                        const CPD3::Data::Variant::Read &config);

public:
    EmailSend();

    EmailSend(const CPD3::ComponentOptions &options,
              double setStart,
              double setEnd,
              const std::vector<CPD3::Data::SequenceName::Component> &setStations = {});

    EmailSend(const CPD3::ComponentOptions &options,
              const std::vector<CPD3::Data::SequenceName::Component> &setStations = {});

    virtual ~EmailSend();

    virtual void signalTerminate();

protected:
    virtual void run();
};

class EmailSendComponent : public QObject, virtual public CPD3::ActionComponentTime {
Q_OBJECT
    Q_INTERFACES(CPD3::ActionComponentTime)

    Q_PLUGIN_METADATA(IID
                              "CPD3.email_send"
                              FILE
                              "email_send.json")

public:
    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual int actionAllowStations();

    virtual bool actionRequiresTime();

    virtual CPD3::CPD3Action *createTimeAction(const CPD3::ComponentOptions &options,
                                               double start,
                                               double end,
                                               const std::vector<std::string> &stations = {});

    virtual CPD3::CPD3Action *createTimeAction(const CPD3::ComponentOptions &options = {},
                                               const std::vector<std::string> &stations = {});
};

#endif
