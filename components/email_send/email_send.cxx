/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#ifdef Q_OS_UNIX

#include <unistd.h>

#endif

#include <QBuffer>
#include <QUrl>
#include <QLoggingCategory>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "core/range.hxx"
#include "core/environment.hxx"
#include "core/process.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "database/runlock.hxx"
#include "datacore/valueoptionparse.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/segment.hxx"
#include "datacore/sequencematch.hxx"
#include "datacore/variant/composite.hxx"
#include "transfer/email.hxx"
#include "transfer/smtp.hxx"
#include "io/process.hxx"
#include "io/drivers/file.hxx"

#include "email_send.hxx"


Q_LOGGING_CATEGORY(log_email_send, "cpd3.email.send", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Transfer;

EmailSend::EmailSend() : profile(SequenceName::impliedProfile()),
                         stations(),
                         lockTimeout(3600.0),
                         terminated(false),
                         activeAction(NULL),
                         activeInput(NULL),
                         mutex()
{ }

EmailSend::EmailSend(const ComponentOptions &options,
                     double setStart,
                     double setEnd,
                     const std::vector<SequenceName::Component> &setStations) : profile(
        SequenceName::impliedProfile()),
                                                                                stations(
                                                                                        setStations),
                                                                                start(setStart),
                                                                                end(setEnd),
                                                                                doLock(false),
                                                                                lockTimeout(3600.0),
                                                                                terminated(false),
                                                                                activeAction(NULL),
                                                                                activeInput(NULL),
                                                                                mutex()
{
    if (options.isSet("profile")) {
        profile = qobject_cast<ComponentOptionSingleString *>(options.get("profile"))->get()
                                                                                     .toLower()
                                                                                     .toStdString();
    }
}

EmailSend::EmailSend(const ComponentOptions &options,
                     const std::vector<SequenceName::Component> &setStations) : profile(
        SequenceName::impliedProfile()),
                                                                                stations(
                                                                                        setStations),
                                                                                start(FP::undefined()),
                                                                                end(FP::undefined()),
                                                                                doLock(true),
                                                                                lockTimeout(30.0),
                                                                                terminated(false),
                                                                                activeAction(NULL),
                                                                                activeInput(NULL),
                                                                                mutex()
{
    if (options.isSet("profile")) {
        profile = qobject_cast<ComponentOptionSingleString *>(options.get("profile"))->get()
                                                                                     .toLower()
                                                                                     .toStdString();
    }
    if (options.isSet("timeout")) {
        lockTimeout = qobject_cast<ComponentOptionSingleDouble *>(options.get("timeout"))->get();
    }
}

EmailSend::~EmailSend()
{
}

void EmailSend::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
        if (activeAction)
            activeAction->signalTerminate();
        if (activeInput)
            activeInput->signalTerminate();
    }
    terminateRequested();
}

bool EmailSend::testTerminated()
{
    std::lock_guard<std::mutex> lock(mutex);
    return terminated;
}

void EmailSend::convertBufferOutput(const SequenceValue::Transfer &values)
{
    for (const auto &dv : values) {
        auto v = dv.read();
        if (!v.exists())
            continue;
        if (v.getType() == Variant::Type::String) {
            Variant::Root add;
            add["Type"].setString("Text");
            add["Text"].setString(substitutions.apply(v.toQString()));
            messageData.emplace_back(std::move(add));
        } else {
            Variant::Root add(v);
            /* Safe to modify, since the buffer ingress isn't referenced anymore */
            const auto &type = v["Type"].toString();
            if (Util::equal_insensitive(type, "text", "string", "")) {
                QString text(v["Text"].toQString());
                if (text.isEmpty())
                    continue;
                add["Text"].setString(substitutions.apply(text));
            }
            messageData.emplace_back(std::move(add));
        }
    }
}

bool EmailSend::invokeCommand(const QString &program,
                              const QStringList &arguments,
                              const Variant::Read &config)
{
    qCDebug(log_email_send) << "Invoking:" << program << arguments;
    feedback.emitState(program);

    IO::Process::Spawn spawn(program, arguments);

    bool captureOutput = false;
    bool outputText = false;
    std::unique_ptr<StandardDataInput> sdi;
    StreamSink::Buffer bufferIngress;

    spawn.inputStream = IO::Process::Spawn::StreamMode::Discard;
    if (config["IgnoreOutput"].toBool()) {
        spawn.outputStream = IO::Process::Spawn::StreamMode::Forward;
    } else if (config["DataOutput"].toBool()) {
        captureOutput = true;
        sdi.reset(new StandardDataInput);
        sdi->start();
        sdi->setEgress(&bufferIngress);
    } else {
        captureOutput = true;
        outputText = true;
    }
    auto proc = spawn.create();

    IO::Process::Instance::ExitMonitor exitOk(*proc);
    std::unique_ptr<Threading::Receiver> sdiReceiver;
    if (config["IgnoreTerminate"].toBool()) {
        canAbort = false;
    } else {
        exitOk.connectTerminate(terminateRequested);
        if (sdi) {
            sdiReceiver.reset(new Threading::Receiver);
            terminateRequested.connect(*sdiReceiver,
                                       std::bind(&StandardDataInput::signalTerminate, sdi.get()));
        }
    }

    std::unique_ptr<IO::Generic::Stream> outputStream;
    if (captureOutput) {
        outputStream = proc->outputStream();
    }
    std::unique_ptr<IO::Generic::Stream::Iterator> textCapture;
    if (outputStream) {
        if (outputText) {
            textCapture.reset(new IO::Generic::Stream::Iterator(*outputStream));
        } else if (sdi) {
            outputStream->read.connect([&sdi](const Util::ByteArray &contents) {
                sdi->incomingData(contents);
            });
            outputStream->ended.connect([&sdi]() {
                sdi->endData();
            });
            outputStream->start();
        }
    }

    if (canAbort && testTerminated())
        return false;

    if (!proc->start())
        return false;
    if (!proc->wait())
        return false;

    if (sdi) {
        sdi->wait();
        sdiReceiver.reset();
        sdi.reset();
        convertBufferOutput(bufferIngress.values());
    } else if (textCapture) {
        Variant::Root add;
        add["Type"].setString("Text");
        add["Text"].setString(substitutions.apply(
                QString::fromUtf8(textCapture->all().toQByteArrayRef()).trimmed()));
        messageData.emplace_back(std::move(add));
    }

    if (config["IgnoreExitStatus"].toBool())
        return true;
    return exitOk.success();
}

bool EmailSend::executeAction(CPD3Action *action, const Variant::Read &config)
{
    if (action == NULL) {
        qCWarning(log_email_send) << "Error creating action";
        return false;
    }

    action->feedback.forward(feedback);

    action->start();
    if (config["IgnoreTerminate"].toBool()) {
        action->wait();
        delete action;
        canAbort = false;
        return true;
    }

    {
        std::unique_lock<std::mutex> lock(mutex);
        if (terminated) {
            lock.unlock();
            action->signalTerminate();
            action->wait();
            delete action;
            return false;
        }
        activeAction = action;
    }

    action->wait();

    std::lock_guard<std::mutex> lock(mutex);
    delete action;
    activeAction = NULL;
    return !terminated;
}

bool EmailSend::executeInput(ExternalConverter *input,
                             const Variant::Read &config,
                             QIODevice *device)
{
    if (input == NULL) {
        qCWarning(log_email_send) << "Error creating input component";
        return false;
    }
    input->start();

    StreamSink::Buffer bufferIngress;
    input->setEgress(&bufferIngress);

    bool ignoreTerminate = config["IgnoreTerminate"].toBool();
    if (!ignoreTerminate) {
        std::unique_lock<std::mutex> lock(mutex);
        if (terminated) {
            lock.unlock();
            input->signalTerminate();
            input->wait();
            delete input;
            if (device != NULL) {
                device->close();
                delete device;
            }
            return false;
        }
        activeInput = input;
    }

    for (;;) {
        QEventLoop loop;

        input->finished.connect(&loop, std::bind(&QEventLoop::quit, &loop), true);
        if (input->isFinished())
            break;

        if (device == NULL) {
            loop.exec();
            continue;
        }

        connect(device, SIGNAL(readyRead()), &loop, SLOT(quit()));

        qint64 n = device->bytesAvailable();
        if (n > 0) {
            input->incomingData(device->read((int) qMin(Q_INT64_C(65536), n)));
            if (device->bytesAvailable() > 0)
                continue;
        }

        if (device->atEnd() && device->bytesAvailable() == 0) {
            input->endData();

            device->disconnect();
            device->close();
            delete device;
            device = NULL;
            continue;
        }

        loop.exec();
    }

    if (!ignoreTerminate) {
        std::unique_lock<std::mutex> lock(mutex);
        activeInput = NULL;
        if (terminated) {
            lock.unlock();
            input->signalTerminate();
            input->wait();
            delete input;
            return false;
        }
    }

    if (device) {
        input->endData();
        delete device;
        device = nullptr;
    }

    input->wait();
    delete input;

    convertBufferOutput(bufferIngress.values());
    return true;
}

bool EmailSend::handleAction(const SequenceName::Component &station, const Variant::Read &action)
{
    if (action.getType() == Variant::Type::String) {
        QStringList arguments
                (substitutions.apply(action.toQString()).split(' ', QString::SkipEmptyParts));
        if (arguments.isEmpty())
            return true;
        QString program(arguments.takeFirst());
        return invokeCommand(program, arguments, Variant::Root());
    }

    const auto &type = action["Type"].toString();
    if (Util::equal_insensitive(type, "program", "command")) {
        auto command = substitutions.apply(action["Command"].toQString());
        if (command.isEmpty())
            command = substitutions.apply(action["Program"].toQString());
        command = substitutions.apply(command);
        QStringList arguments;
        if (action["Arguments"].getType() == Variant::Type::String) {
            arguments = substitutions.apply(action["Arguments"].toQString())
                                     .split(' ', QString::SkipEmptyParts);
        } else {
            for (auto add : action["Arguments"].toArray()) {
                arguments.append(substitutions.apply(add.toQString()));
            }
        }
        if (command.isEmpty() && !arguments.isEmpty())
            command = arguments.takeFirst();
        if (command.isEmpty())
            return true;
        return invokeCommand(command, arguments, action);
    }

    QString componentName(action["Name"].toQString());
    if (componentName.isEmpty()) {
        qCDebug(log_email_send) << "Invalid action:" << action;
        return true;
    }

    QObject *component = ComponentLoader::create(componentName);
    if (component == NULL) {
        qCWarning(log_email_send) << "Can't load component" << componentName;
        return false;
    }

    feedback.emitState(componentName);

    Variant::Root options(action["Options"]);
    if (!options["profile"].exists()) {
        options["profile"].setString(profile);
    }
    if (!options["station"].exists()) {
        options["station"].setString(station);
    }

    ActionComponent *actionComponent;
    if ((actionComponent = qobject_cast<ActionComponent *>(component))) {
        std::vector<std::string> stations;
        if (actionComponent->actionAllowStations() > 0 &&
                actionComponent->actionRequireStations() <= 1) {
            stations.emplace_back(station);
        } else if (actionComponent->actionRequireStations() > 0) {
            qCWarning(log_email_send) << "Unable to provide required stations to" << componentName;
            return false;
        }

        ComponentOptions o = actionComponent->getOptions();
        ValueOptionParse::parse(o, options);
        CPD3Action *actionThread = actionComponent->createAction(o, stations);
        return executeAction(actionThread, action);
    }

    ActionComponentTime *actionComponentTime;
    if ((actionComponentTime = qobject_cast<ActionComponentTime *>(component))) {
        bool provideTimes = !action["IgnoreTimes"].toBool();
        if (!actionComponentTime->actionAcceptsUndefinedBounds() &&
                (!FP::defined(start) || !FP::defined(end))) {
            provideTimes = false;
        }
        if (!provideTimes && actionComponentTime->actionRequiresTime()) {
            qCWarning(log_email_send) << "Refusing to run" << componentName
                                      << "since it requires times and they are not provided";
            return false;
        }

        std::vector<std::string> stations;
        if (actionComponentTime->actionAllowStations() > 0 &&
                actionComponentTime->actionRequireStations() <= 1) {
            stations.emplace_back(station);
        } else if (actionComponentTime->actionRequireStations() > 0) {
            qCWarning(log_email_send) << "Unable to provide required stations to" << componentName;
            return false;
        }

        ComponentOptions o = actionComponentTime->getOptions();
        ValueOptionParse::parse(o, options);
        CPD3Action *actionThread;
        if (provideTimes) {
            actionThread = actionComponentTime->createTimeAction(o, start, end, stations);
        } else {
            actionThread = actionComponentTime->createTimeAction(o, stations);
        }
        return executeAction(actionThread, action);
    }

    ExternalConverterComponent *ingressComponent;
    if ((ingressComponent = qobject_cast<ExternalConverterComponent *>(component))) {
        QIODevice *inputDevice = NULL;
        QByteArray bufferWriteData;
        if (ingressComponent->requiresInputDevice()) {
            QString inputFileName(action["File"].toQString());
            if (!inputFileName.isEmpty()) {
                if (!QFile::exists(inputFileName)) {
                    qCWarning(log_email_send) << "Input file" << inputFileName
                                              << "does not exist for component" << componentName;
                    return false;
                }

                inputDevice = new QFile(inputFileName);
                if (!inputDevice->open(QIODevice::ReadOnly)) {
                    qCWarning(log_email_send) << "Can't open input file" << inputFileName << ':'
                                              << inputDevice->errorString() << "on component"
                                              << componentName;
                    delete inputDevice;
                    return false;
                }
            } else if (action["Write"].exists()) {
                if (action["Write"].getType() == Variant::Type::Bytes) {
                    bufferWriteData = action["Write"].toBinary();
                } else {
                    QString writeData(substitutions.apply(action["Write"].toQString()));
                    bufferWriteData = writeData.toUtf8();
                }
                inputDevice = new QBuffer(&bufferWriteData);
                if (!inputDevice->open(QIODevice::ReadOnly)) {
                    qCWarning(log_email_send) << "Unable to open buffer device:"
                                              << inputDevice->errorString() << "on component"
                                              << componentName;
                    delete inputDevice;
                    return false;
                }
            }
            if (inputDevice == NULL) {
                qCWarning(log_email_send) << "Component" << componentName
                                          << "requires an input file but none was specified";
                return false;
            }
        }

        ComponentOptions opts(ingressComponent->getOptions());
        ValueOptionParse::parse(opts, options);
        ExternalConverter *input = ingressComponent->createDataIngress(opts);
        return executeInput(input, action, inputDevice);
    }

    ExternalSourceComponent *ingressComponentExternal;
    if ((ingressComponentExternal = qobject_cast<ExternalSourceComponent *>(component))) {
        std::vector<std::string> effectiveStations{station};
        if ((int) effectiveStations.size() > ingressComponentExternal->ingressAllowStations()) {
            effectiveStations.clear();
        }
        auto vCheck = action["Station"];
        if (vCheck.getType() == Variant::Type::String) {
            effectiveStations.clear();
            for (const auto &add : vCheck.toQString()
                                         .toLower()
                                         .split(QRegExp("\\s+"), QString::SkipEmptyParts)) {
                effectiveStations.emplace_back(add.toStdString());
            }
        } else if (vCheck.getType() == Variant::Type::Array) {
            effectiveStations.clear();
            for (auto add : vCheck.toArray()) {
                effectiveStations.emplace_back(add.toQString().toLower().toStdString());
            }
        }
        if ((int) effectiveStations.size() < ingressComponentExternal->ingressRequireStations()) {
            qCWarning(log_email_send) << "Component" << componentName << "requires"
                                      << ingressComponentExternal->ingressRequireStations()
                                      << "station(s), but only" << effectiveStations.size()
                                      << "are available";
            return false;
        }

        bool provideTimes = !action["IgnoreTimes"].toBool();
        double effectiveStart = start;
        double effectiveEnd = end;
        vCheck = action["Start"];
        if (vCheck.getType() == Variant::Type::Real) {
            effectiveStart = vCheck.toDouble();
        }
        vCheck = action["End"];
        if (vCheck.getType() == Variant::Type::Real) {
            effectiveEnd = vCheck.toDouble();
        }
        if (!ingressComponentExternal->ingressAcceptsUndefinedBounds() &&
                (!FP::defined(effectiveStart) || !FP::defined(effectiveEnd))) {
            provideTimes = false;
        }
        if (!provideTimes && ingressComponentExternal->ingressRequiresTime()) {
            qCWarning(log_email_send) << "Component" << componentName
                                      << "requires time bounds but none are available";
            return false;
        }

        ComponentOptions opts(ingressComponentExternal->getOptions());
        ValueOptionParse::parse(opts, options);
        ExternalConverter *input;
        if (provideTimes) {
            input = ingressComponentExternal->createExternalIngress(opts, effectiveStart,
                                                                    effectiveEnd,
                                                                    effectiveStations);
        } else {
            input = ingressComponentExternal->createExternalIngress(opts, effectiveStations);
        }
        return executeInput(input, action);
    }

    qCWarning(log_email_send) << "Unable to execute component" << componentName
                              << "because the type is not supported";
    return false;
}

static QString translateSize(qint64 bytes)
{
    if (bytes < 1024) {
        return EmailSend::tr("%1 B").arg(bytes);
    }
    bytes /= 1024;
    if (bytes < 1024) {
        return EmailSend::tr("%1 KiB").arg(bytes);
    }
    bytes /= 1024;
    if (bytes < 1024) {
        return EmailSend::tr("%1 MiB").arg(bytes);
    }
    bytes /= 1024;
    return EmailSend::tr("%1 GiB").arg(bytes);
}

static bool messageSort(const CPD3::Data::Variant::Read &a, const CPD3::Data::Variant::Read &b)
{
    qint64 priorityA = a.hash("Priority").toInt64();
    qint64 priorityB = b.hash("Priority").toInt64();
    if (!INTEGER::defined(priorityA)) {
        return false;
    } else if (!INTEGER::defined(priorityB)) {
        return true;
    }
    if (priorityA != priorityB)
        return priorityA < priorityB;
    return false;
}

namespace {
struct MessageParameters {
    enum Type {
        Type_Text, Type_HTML4, Type_HTML5,
    };
    Type type;
    enum ImagesFormat {
        Images_Inline, Images_Attached, Images_Disable,
    };
    ImagesFormat images;

    MessageParameters() : type(Type_HTML5), images(Images_Inline)
    { }

    MessageParameters(const MessageParameters &other) : type(other.type), images(other.images)
    { }

    MessageParameters &operator=(const MessageParameters &other)
    {
        if (&other == this)
            return *this;
        type = other.type;
        images = other.images;
        return *this;
    }

    bool operator==(const MessageParameters &other) const
    {
        return type == other.type && images == other.images;
    }

    MessageParameters(const Variant::Read &config) : type(Type_HTML5), images(Images_Inline)
    {
        const auto &cimages = config["Images"].toString();
        if (Util::equal_insensitive(cimages, "attached", "attachment", "attachments")) {
            images = Images_Attached;
        } else if (Util::equal_insensitive(cimages, "none", "disable", "off")) {
            images = Images_Disable;
        } else if (config["Images"].getType() == Variant::Type::Boolean &&
                !config["Images"].toBool()) {
            images = Images_Disable;
        }

        const auto &ctype = config["Format"].toString();
        if (Util::equal_insensitive(ctype, "text")) {
            type = Type_Text;
            if (images == Images_Inline)
                images = Images_Attached;
        } else if (Util::equal_insensitive(ctype, "html", "html4")) {
            type = Type_HTML4;
        }
    }

    MessageParameters(const QString &parameters) : type(Type_HTML5), images(Images_Inline)
    {
        QStringList parts(parameters.split(QRegExp("[;,]+")));
        for (QList<QString>::const_iterator component = parts.constBegin(), end = parts.constEnd();
                component != end;
                ++component) {
            QStringList subc(component->split(QRegExp("[:;,]+")));
            if (subc.isEmpty())
                continue;
            QString key(subc.takeFirst());
            QString value;
            if (!subc.isEmpty())
                value = subc.at(0).toLower();
            if (key == "text" || (key == "format" && value == "text")) {
                type = Type_Text;
                if (images == Images_Inline)
                    images = Images_Attached;
            } else if (key == "html4" ||
                    (key == "format" && (value == "html" || value == "html4"))) {
                type = Type_HTML4;
            } else if (key == "html5" || (key == "format" && value == "html5")) {
                type = Type_HTML5;
            }

            if (key == "noimages" ||
                    (key == "images" &&
                            (value == "none" || value == "disable" || value == "off"))) {
                images = Images_Disable;
            } else if (key == "attachimages" ||
                    (key == "images" &&
                            (value == "attached" ||
                                    value == "attachment" ||
                                    value == "attachments"))) {
                images = Images_Attached;
            } else if (key == "inlineimages" || (key == "images" && (value == "inline"))) {
                if (type != Type_Text)
                    images = Images_Inline;
            }
        }
    }
};

uint qHash(const MessageParameters &p)
{
    return ((uint) p.images << 8) ^ (uint) (p.type);
}

struct HeadingData {
    QString text;
    qint64 priority;
    bool defaultOpen;

    HeadingData() : text(), priority(INTEGER::undefined()), defaultOpen(false)
    { }

    HeadingData(const HeadingData &other) : text(other.text),
                                            priority(other.priority),
                                            defaultOpen(other.defaultOpen)
    { }

    HeadingData &operator=(const HeadingData &other)
    {
        if (&other == this)
            return *this;
        text = other.text;
        priority = other.priority;
        defaultOpen = other.defaultOpen;
        return *this;
    }

    HeadingData(const Variant::Read &data, const Variant::Read &override) : text(
            data["Heading/Text"].toQString()),
                                                                            priority(
                                                                                    data["Heading/Priority"]
                                                                                            .toInt64()),
                                                                            defaultOpen(
                                                                                    data["Heading/DefaultOpen"]
                                                                                            .toBool())
    {
        auto headingOverride = override.hash(text);
        if (!headingOverride.exists())
            return;
        if (headingOverride["Priority"].exists()) {
            priority = headingOverride["Priority"].toInt64();
        }
        if (headingOverride["DefaultOpen"].exists()) {
            defaultOpen = headingOverride["DefaultOpen"].toBool();
        }
    }

    bool operator<(const HeadingData &other) const
    {
        if (text.isEmpty())
            return !other.text.isEmpty();
        if (other.text.isEmpty())
            return false;

        if (!INTEGER::defined(priority)) {
            return false;
        } else if (!INTEGER::defined(other.priority)) {
            return true;
        }
        if (priority != other.priority)
            return priority < other.priority;
        return false;
    }

    bool operator==(const HeadingData &other) const
    {
        return text == other.text;
    }
};

uint qHash(const HeadingData &d)
{
    return qHash(d.text);
}

}

static void addRecipient(QHash<MessageParameters, QStringList> &recipients,
                         QString key,
                         const Variant::Read &data)
{
    switch (data.getType()) {
    case Variant::Type::String: {
        if (!data.toString().empty())
            key = data.toQString();
        break;
    }
    case Variant::Type::Hash: {
        QString r(data["Address"].toQString());
        if (r.isEmpty())
            r = key;
        if (r.isEmpty())
            return;
        recipients[MessageParameters(data)].append(r);
        return;
    }
    default:
        break;
    }

    if (key.isEmpty())
        return;

    QRegExp reFlags("(.+)\\s*\\(([^\\)]+)\\)\\s*");
    if (reFlags.exactMatch(key)) {
        recipients[MessageParameters(reFlags.cap(2))].append(reFlags.cap(1));
    } else {
        recipients[MessageParameters()].append(key);
    }
}

static void addRecipientLine(QHash<MessageParameters, QStringList> &recipients,
                             QByteArray fileLine,
                             const SequenceName::Component &station)
{
    int idxComment = fileLine.indexOf('#');
    if (idxComment >= 0)
        fileLine.resize(idxComment);
    QString line(QString::fromUtf8(fileLine));
    line = line.trimmed();
    if (line.isEmpty())
        return;

    QStringList fields(line.split(','));
    if (fields.isEmpty())
        return;
    QString email(fields.takeFirst());
    if (email.isEmpty())
        return;

    bool hit = fields.isEmpty();
    for (QList<QString>::const_iterator check = fields.constBegin(), end = fields.constEnd();
            check != end;
            ++check) {
        QRegExp re(*check, Qt::CaseInsensitive);
        if (!re.isValid())
            continue;
        if (!re.exactMatch(QString::fromStdString(station)))
            continue;
        hit = true;
        break;
    }
    if (!hit)
        return;

    addRecipient(recipients, email, Variant::Root());
}

bool EmailSend::processStation(const SequenceName::Component &station, const Variant::Read &config)
{
    messageData.clear();
    canAbort = true;

    double sendBeginTime = Time::time();

    feedback.emitStage(tr("Processing %1").arg(QString::fromStdString(station).toUpper()),
                       tr("The system is generating the email for %1.").arg(
                               QString::fromStdString(station).toUpper()), true);

    {
        auto actions = config["Actions"];
        switch (actions.getType()) {
        case Variant::Type::Array: {
            auto children = actions.toArray();
            std::size_t n = 0;
            std::size_t nChildren = children.size();
            for (auto act : children) {
                feedback.emitProgressFraction(0.9 * ((double) (n++) / (double) nChildren));
                if (!handleAction(station, act)) {
                    if (canAbort)
                        return false;
                    continue;
                }
            }
            break;
        }

        case Variant::Type::Hash: {
            if (actions.hash("Type").exists()) {
                if (!handleAction(station, actions)) {
                    if (canAbort)
                        return false;
                }
            } else {
                auto children = actions.toHash();
                std::size_t n = 0;
                std::size_t nChildren = children.size();
                for (auto act : children) {
                    feedback.emitProgressFraction(0.9 * ((double) (n++) / (double) nChildren));
                    if (act.first.empty())
                        continue;
                    if (!handleAction(station, act.second)) {
                        if (canAbort)
                            return false;
                        continue;
                    }
                }
            }
            break;
        }

        case Variant::Type::String: {
            if (!handleAction(station, actions)) {
                if (canAbort)
                    return false;
            }
            break;
        }

        default:
            break;
        }
    }

    if (canAbort && testTerminated())
        return false;

    feedback.emitProgressFraction(0.9);
    feedback.emitState(tr("Building messages"));

    if (!processingLog.empty()) {
        Variant::Write processedTable = Variant::Write::empty();
        qint64 corrupted = 0;
        QHash<QString, QSet<QString> > unauthorized;
        QStringList completelyRejected;
        for (const auto &logEntry : processingLog) {
            const auto &type = logEntry["Type"].toString();
            if (Util::equal_insensitive(type, "completedfile")) {
                processedTable.array(0)
                              .toArray()
                              .after_back()
                              .setString(Time::toISO8601(logEntry["Time"].toDouble()));
                processedTable.array(1)
                              .toArray()
                              .after_back()
                              .setString(logEntry["File/BaseName"].toString());
                processedTable.array(2)
                              .toArray()
                              .after_back()
                              .setString(translateSize(logEntry["File/Size"].toInt64()));
                processedTable.array(3)
                              .toArray()
                              .after_back()
                              .setString(Time::describeDuration(
                                      logEntry["File/ProcessingTime"].toDouble()));

                if (logEntry["File/Existing"].exists()) {
                    processedTable.array(4).toArray().after_back().setString(tr("Duplicate"));
                    continue;
                }
                if (logEntry["File/Corrupt"].toBool()) {
                    processedTable.array(4).toArray().after_back().setString(tr("Corrupted"));
                    ++corrupted;
                    continue;
                }
                if (logEntry["File/Unauthorized"].toBool()) {
                    processedTable.array(4).toArray().after_back().setString(tr("Unauthorized"));
                    unauthorized[logEntry["File/UnauthorizedDigest"].toQString()].insert(
                            logEntry["File/BaseName"].toQString());
                    continue;
                }

                qint64 accepted = logEntry["File/AcceptedValues"].toInt64();
                if (!INTEGER::defined(accepted) || accepted < 0)
                    accepted = 0;
                qint64 rejected = logEntry["File/RejectedValues"].toInt64();
                if (!INTEGER::defined(rejected) || rejected < 0)
                    rejected = 0;

                if (accepted <= 0 && rejected > 0) {
                    completelyRejected.append(logEntry["File/BaseName"].toQString());
                }

                processedTable.array(4)
                              .toArray()
                              .after_back()
                              .setString(
                                      tr("%1/%2 Accepted").arg(accepted).arg(accepted + rejected));
                continue;
            }
        }

        for (QHash<QString, QSet<QString> >::const_iterator ua = unauthorized.constBegin(),
                endUA = unauthorized.constEnd(); ua != endUA; ++ua) {
            Variant::Root add;
            add["Type"].setString("Text");
            add["Color"].setString(tr("#7F0000", "unauthorized color"));
            add["Heading/Text"].setString(tr("Warnings", "unauthorized heading"));
            add["Heading/Priority"].setInt64(0);
            add["Heading/DefaultOpen"].setBool(true);
            add["Priority"].setInt64(0);
            if (ua.key().isEmpty()) {
                add["Text"].setString(
                        tr("There are %n file(s) that do not have an authorization certificate "
                           "but the station requires one to process data.  If this "
                           "is a legitimate change, then please use "
                           "da.process.authorize to add their encryption certificate "
                           "(if any) or allow unsigned data.  Then reprocess the "
                           "unauthorized file(s).", "", ua.value().size()));
            } else {
                add["Text"].setString(tr("The certificate ID %1 used with %n file(s) is not "
                                         "authorized to process data.  If this is a legitimate "
                                         "change then please add the certificate to the station "
                                         "with da.process.authorize and reprocess the unauthorized "
                                         "file(s).", "", ua.value().size()).arg(ua.key()));
            }
            messageData.emplace_back(std::move(add));
        }

        if (!completelyRejected.isEmpty()) {
            Variant::Root add;
            add["Type"].setString("Text");
            add["Color"].setString(tr("#FF8800", "rejected color"));
            add["Heading/Text"].setString(tr("Warnings", "rejected heading"));
            add["Heading/Priority"].setInt64(0);
            add["Heading/DefaultOpen"].setBool(true);
            add["Priority"].setInt64(0);
            add["Text"].setString(
                    tr("There are %n file(s) do not contain any data that is allowed to be "
                       "written to the archive.  Please verify the station is "
                       "configured correctly (for example, using the correct station "
                       "code) and if it is change the authorization to allow the "
                       "data to be processed.", "", completelyRejected.size()));
            messageData.emplace_back(std::move(add));
        }

        if (corrupted > 0) {
            Variant::Root add;
            add["Type"].setString("Text");
            add["Color"].setString(tr("#FF8800", "corrupted color"));
            add["Heading/Text"].setString(tr("Warnings", "corrupted heading"));
            add["Heading/Priority"].setInt64(0);
            add["Heading/DefaultOpen"].setBool(true);
            add["Priority"].setInt64(0);
            add["Text"].setString(tr("There where %n corrupted file(s) processed.  This can "
                                     "indicate problems with the connection or with the "
                                     "acquisition computer.  Please make sure both are operating "
                                     "normally.", "", (int) corrupted));
            messageData.emplace_back(std::move(add));
        }

        if (processedTable.exists()) {
            Variant::Root add;
            add["Type"].setString("Table");
            add["Heading/Text"].setString("Processing");
            add["Size"].setInt64(1);
            add["Columns"].set(processedTable);
            add["ColumnHeadings/#0"].setString(tr("Time", "processing table column heading"));
            add["ColumnHeadings/#1"].setString(tr("File", "processing table column heading"));
            add["ColumnHeadings/#2"].setString(tr("Size", "processing table column heading"));
            add["ColumnHeadings/#3"].setString(tr("Time Taken", "processing table column heading"));
            add["ColumnHeadings/#4"].setString(tr("Status", "processing table column heading"));
            messageData.emplace_back(std::move(add));
        }
    }

    if (messageData.empty()) {
        qCDebug(log_email_send) << "Message is empty, not sending";
        feedback.emitProgressFraction(1.0);
        return true;
    }

    std::stable_sort(messageData.begin(), messageData.end(), messageSort);

    QList<HeadingData> headings;
    QHash<HeadingData, QList<Variant::Read> > headingContents;
    {
        auto headingOverride = config["Headings"];
        for (const auto &component : messageData) {
            HeadingData hd(component, headingOverride);
            headingContents[hd].append(component);
            if (headings.contains(hd))
                continue;
            headings.append(hd);
        }
        std::stable_sort(headings.begin(), headings.end());
    }

    QHash<MessageParameters, QStringList> recipients;
    {
        switch (config["Recipients"].getType()) {
        case Variant::Type::String: {
            QStringList emails(config["Recipients"].toQString().split(","));
            for (QList<QString>::const_iterator add = emails.constBegin(), end = emails.constEnd();
                    add != end;
                    ++add) {
                addRecipient(recipients, *add, Variant::Root());
            }
            break;
        }
        default: {
            auto children = config["Recipients"].toChildren();
            for (auto add = children.begin(), endAdd = children.end(); add != endAdd; ++add) {
                addRecipient(recipients, QString::fromStdString(add.stringKey()), add.value());
            }
            break;
        }
        }
    }
    if (!config["RecipientsFile"].toString().empty()) {
        auto file = IO::Access::file(config["RecipientsFile"].toString(),
                                     IO::File::Mode::readOnly())->block();
        if (!file) {
            qCDebug(log_email_send) << "Unable to open external recipients file";
        } else {
            while (auto line = file->readLine()) {
                addRecipientLine(recipients, line.toQByteArrayRef(), station);
            }
        }
    }

    QStringList allRecipients;
    for (QHash<MessageParameters, QStringList>::const_iterator email = recipients.constBegin(),
            endEmail = recipients.constEnd(); email != endEmail; ++email) {
        allRecipients.append(email.value());
    }
    std::sort(allRecipients.begin(), allRecipients.end());

    QSet<QString> relyToEmails = allRecipients.toSet();
    switch (config["ReplyTo"].getType()) {
    case Variant::Type::String: {
        for (const auto &add: config["ReplyTo"].toQString().split(",")) {
            relyToEmails.insert(add);
        }
        break;
    }
    default: {
        auto children = config["ReplyTo"].toChildren();
        for (auto add = children.begin(), endAdd = children.end(); add != endAdd; ++add) {
            relyToEmails.insert(QString::fromStdString(add.stringKey()));
        }
        break;
    }
    }
    QStringList replyTo(relyToEmails.toList());
    std::sort(replyTo.begin(), replyTo.end());

    SMTPSend sender(config["Relay"]);
    {
        QString subject(substitutions.apply(config["Subject"].toQString()));
        if (subject.isEmpty()) {
            subject = tr("CPD3 - Data Processing - %1", "default subject").arg(
                    QString::fromStdString(station).toUpper());
        }

        for (QHash<MessageParameters, QStringList>::const_iterator email = recipients.constBegin(),
                endEmail = recipients.constEnd(); email != endEmail; ++email) {
            EmailBuilder builder;
            MessageParameters mp(email.key());

            if (mp.type == MessageParameters::Type_Text) {
                builder.setEscapeMode(EmailBuilder::Escape_UTF8);
            } else {
                builder.setEscapeMode(EmailBuilder::Escape_HTML);
            }
            builder.setSubject(subject);
            builder.setHeader("Reply-To", replyTo.join(", "));
            for (QStringList::const_iterator to = email.value().constBegin(),
                    end = email.value().constEnd(); to != end; ++to) {
                builder.addTo(*to);
            }

            QString body;
            int imageInstance = 0;
            QString imageDate(Time::toISO8601ND(Time::time()));
            QSet<QString> uniqueMessageKeys;
            for (QList<HeadingData>::const_iterator heading = headings.constBegin(),
                    endHeadings = headings.constEnd(); heading != endHeadings; ++heading) {
                const auto &contents = headingContents[*heading];

                QString closeTags;
                if (!heading->text.isEmpty()) {
                    if (mp.type == MessageParameters::Type_Text) {
                        body.append(
                                tr("\r\n=== %1 ===\r\n\r\n", "text heading").arg(heading->text));
                    } else if (mp.type == MessageParameters::Type_HTML4) {
                        body.append(tr("<br><h2>%1</h2>\r\n", "html4 heading").arg(heading->text));
                    } else if (mp.type == MessageParameters::Type_HTML5) {
                        if (heading->defaultOpen) {
                            body.append(tr("<br><details open><summary><b>%1</b></summary><br>\r\n",
                                           "html5 heading").arg(heading->text));
                        } else {
                            body.append(tr("<br><details><summary><b>%1</b></summary><br>\r\n",
                                           "html5 heading").arg(heading->text));
                        }
                        closeTags.prepend("</details>");
                    }
                }

                bool wasFirstItem = false;
                QSet<QString> uniqueHeadingKeys;
                for (const auto &item : contents) {
                    const auto &type = item["Type"].toString();

                    {
                        QString uniqueCheck = item["MessageUnique"].toQString();
                        if (!uniqueCheck.isEmpty()) {
                            if (uniqueMessageKeys.contains(uniqueCheck))
                                continue;
                            uniqueMessageKeys.insert(uniqueCheck);
                        }

                        uniqueCheck = item["HeadingUnique"].toQString();
                        if (!uniqueCheck.isEmpty()) {
                            if (uniqueHeadingKeys.contains(uniqueCheck))
                                continue;
                            uniqueHeadingKeys.insert(uniqueCheck);
                        }
                    }

                    bool isFirstItem = wasFirstItem;
                    wasFirstItem = false;

                    if (Util::equal_insensitive(type, "image", "png")) {
                        if (mp.images == MessageParameters::Images_Disable)
                            continue;

                        ++imageInstance;
                        QString fileName(tr("CPD3-%1-%2.png", "png file name").arg(imageDate,
                                                                                   imageInstance));

                        if (mp.type == MessageParameters::Type_Text ||
                                mp.images == MessageParameters::Images_Attached) {
                            builder.addAttachment(item["Image"].toBinary(), fileName, "image/png");
                            continue;
                        }

                        QString cid(tr("CPD3-%1-%2.png@cmdl.noaa.gov", "png CID").arg(imageDate,
                                                                                      imageInstance));
                        builder.addAttachment(item["Image"].toBinary(), fileName, "image/png",
                                              QString("<%1>").arg(cid));
                        body.append(QString("<img src=\"cid:%1\">").arg(cid));
                    } else if (Util::equal_insensitive(type, "table")) {
                        auto columns = item["Columns"].toArray();
                        std::size_t nRows = 0;
                        for (auto col : columns) {
                            nRows = std::max<std::size_t>(nRows, col.toArray().size());
                        }
                        if (mp.type == MessageParameters::Type_Text) {
                            QStringList cheadings;
                            for (auto add : item["ColumnHeadings"].toArray()) {
                                cheadings.append(add.toQString());
                            }
                            if (!cheadings.isEmpty()) {
                                body.append(cheadings.join(","));
                                body.append("\r\n");
                            }
                            for (std::size_t row = 0; row < nRows; ++row) {
                                for (std::size_t col = 0; col < columns.size(); ++col) {
                                    if (col != 0)
                                        body.append(",");
                                    body.append(columns[col].array(row).toQString());
                                }
                                body.append("\r\n");
                            }
                        } else {
                            QStringList cheadings;
                            for (auto add : item["ColumnHeadings"].toArray()) {
                                cheadings.append(add.toQString());
                            }
                            if (item["Border"].toBool()) {
                                body.append("<table border=\"1\">\r\n");
                            } else {
                                body.append("<table>\r\n");
                            }
                            if (!cheadings.isEmpty()) {
                                body.append("<tr>\r\n");
                                for (QList<QString>::iterator head = cheadings.begin(),
                                        end = cheadings.end(); head != end; ++head) {
                                    if (item["ColumnHeadingColor"].exists()) {
                                        *head = tr("<font color='%1'>%2</font>",
                                                   "colored text").arg(
                                                item["ColumnHeadingColor"].toQString(), *head);
                                    }
                                    if (item["ColumnHeadingSize"].exists()) {
                                        *head = tr("<font size='%1'>%2</font>",
                                                   "sized text").arg(item["ColumnHeadingSize"].toInt64())
                                                                .arg(*head);
                                    }

                                    body.append("<th>");
                                    body.append(*head);
                                    body.append("</th>\r\n");
                                }
                                body.append("</tr>\r\n");
                            }
                            for (std::size_t row = 0; row < nRows; ++row) {
                                body.append("<tr>");
                                for (std::size_t col = 0; col < columns.size(); ++col) {
                                    QString text(columns[col].array(row).toQString());
                                    if (item["Color"].exists()) {
                                        text = tr("<font color='%1'>%2</font>", "colored text").arg(
                                                item["Color"].toQString(), text);
                                    }
                                    if (item["Size"].exists()) {
                                        text = tr("<font size='%1'>%2</font>", "sized text").arg(
                                                item["Size"].toInt64()).arg(text);
                                    }

                                    body.append("<td>");
                                    body.append(text);
                                    body.append("</td>\r\n");
                                }
                                body.append("</tr>\r\n");
                            }
                            body.append("</table>\r\n");
                        }
                    } else if (Util::equal_insensitive(type, "list")) {
                        auto listItems = item["Items"].toArray();
                        if (listItems.empty())
                            continue;

                        if (mp.type == MessageParameters::Type_Text) {
                            for (auto add : listItems) {
                                QString text;
                                if (add.getType() == Variant::Type::String) {
                                    text = add.toQString();
                                } else {
                                    text = add["Text"].toQString();
                                    if (text.isEmpty())
                                        text = add["Link"].toQString();
                                }
                                if (text.isEmpty())
                                    continue;

                                body.append(" * ");
                                body.append(text);
                                body.append("\r\n");
                            }
                        } else {
                            body.append("<ul>\r\n");
                            for (auto add : listItems) {
                                QString text;
                                QString link;
                                if (add.getType() == Variant::Type::String) {
                                    text = add.toQString();
                                } else {
                                    text = add["Text"].toQString();
                                    link = add["Link"].toQString();
                                    if (text.isEmpty())
                                        text = link;
                                }
                                if (text.isEmpty())
                                    continue;

                                text.replace(QRegExp("(\r\n)|(\n\r)|(\r)|(\n)"), "<br>");

                                if (add["Color"].exists()) {
                                    text = tr("<font color='%1'>%2</font>", "colored text").arg(
                                            add["Color"].toQString(), text);
                                }
                                if (add["Size"].exists()) {
                                    text = tr("<font size='%1'>%2</font>", "sized text").arg(
                                            add["Size"].toInt64()).arg(text);
                                }

                                if (!link.isEmpty()) {
                                    link.replace("\"", "%22");
                                    text = tr("<a href=\"%1\">%2</a>", "link text").arg(link, text);
                                }

                                body.append("<li>");
                                body.append(text);
                                body.append("</li>\r\n");
                            }
                            body.append("</ul>\r\n");
                        }
                    } else if (Util::equal_insensitive(type, "break")) {
                        if (isFirstItem && !item["Always"].toBool())
                            continue;
                        if (mp.type == MessageParameters::Type_Text) {
                            body.append("\r\n");
                        } else {
                            body.append("<br>");
                        }
                    } else {
                        QString text(item["Text"].toQString());
                        QString link(item["Link"].toQString());
                        if (text.isEmpty()) {
                            text = link;
                            if (text.isEmpty())
                                continue;
                        }

                        if (mp.type != MessageParameters::Type_Text) {
                            text.replace(QRegExp("(\r\n)|(\n\r)|(\r)|(\n)"), "<br>");
                        }

                        if (item["Color"].exists()) {
                            if (mp.type != MessageParameters::Type_Text) {
                                text = tr("<font color='%1'>%2</font>", "colored text").arg(
                                        item["Color"].toQString(), text);
                            }
                        }
                        if (item["Size"].exists()) {
                            if (mp.type != MessageParameters::Type_Text) {
                                text = tr("<font size='%1'>%2</font>", "sized text").arg(
                                        item["Size"].toInt64()).arg(text);
                            }
                        }

                        if (!link.isEmpty()) {
                            if (mp.type == MessageParameters::Type_Text) {
                                if (text != link) {
                                    text = tr("%1 (%2)", "text is link").arg(text, link);
                                }
                            } else {
                                link.replace("\"", "%22");
                                text = tr("<a href=\"%1\">%2</a>", "link text").arg(link, text);
                            }
                        }

                        if (!item["Continue"].toBool()) {
                            if (mp.type == MessageParameters::Type_Text) {
                                text.append("\r\n");
                            } else {
                                text.append("<br>\r\n");
                            }
                        }

                        body.append(text);
                    }
                }

                body.append(closeTags);
            }

            if (body.isEmpty())
                continue;

            switch (mp.type) {
            case MessageParameters::Type_HTML4:
                body.prepend("<html><body>\r\n");
                body.append("\r\n</body></html>");
                builder.setBody(body, "text/html");
                break;
            case MessageParameters::Type_HTML5:
                body.prepend("<!DOCTYPE html>\r\n<html><body>\r\n");
                body.append("\r\n</body></html>");
                builder.setBody(body, "text/html");
                break;
            case MessageParameters::Type_Text:
                builder.setBody(body, "text/plain");
                break;
            }

            builder.queue(&sender);
        }
    }

    feedback.emitProgressFraction(0.95);

    if (canAbort && testTerminated())
        return false;
    feedback.emitState(tr("Sending email"));

    bool sendOk = true;
    sender.start();
    Threading::Receiver rx;
    if (canAbort) {
        terminateRequested.connect(rx, std::bind(&SMTPSend::signalTerminate, &sender));
        if (testTerminated())
            sender.signalTerminate();
    }
    if (!sender.wait(60)) {
        if (canAbort && testTerminated())
            return false;
        qCInfo(log_email_send) << "SMTP transmission failed for station" << station;
        sendOk = false;
        sender.signalTerminate();
        sender.wait();
    } else if (!sender.allMessagesSent()) {
        qCInfo(log_email_send) << "SMTP messages remained unsent for station" << station;
        sendOk = false;
    }

    feedback.emitProgressFraction(0.99);

    bool anySent = sender.anyMessageSent();
    double sendEndTime = Time::time();

    {
        Variant::Root event;

        if (anySent) {
            if (sendOk) {
                event["Text"].setString(
                        tr("Completed email send to %n recipient(s)", "email event text",
                           allRecipients.size()));
            } else {
                event["Text"].setString(
                        tr("Failed email send to some of %n recipient(s)", "email event text",
                           allRecipients.size()));
            }
        } else {
            event["Text"].setString(tr("Failed email send to %n recipient(s)", "email event text",
                                       allRecipients.size()));
        }
        event["Information"].hash("By").setString("email_send");
        event["Information"].hash("At").setDouble(Time::time());
        event["Information"].hash("Environment").setString(Environment::describe());
        event["Information"].hash("Revision").setString(Environment::revision());
        event["Information"].hash("MessageDataSize").setInt64(messageData.size());
        event["Information"].hash("ProcessingLogSize").setInt64(processingLog.size());
        event["Information"].hash("ElapsedTime").setDouble(sendEndTime - sendBeginTime);
        event["Information"].hash("Success").setBool(sendOk);

        for (const auto &r : allRecipients) {
            event["Information"].hash("Recipients").toArray().after_back().setString(r);
        }

        Archive::Access().writeSynchronous(SequenceValue::Transfer{
                SequenceValue({station, "events", "email"}, std::move(event), sendEndTime,
                              sendEndTime)});
    }

    feedback.emitProgressFraction(1.0);
    return anySent;
}

void EmailSend::expandInterval(const SequenceName::Component &station,
                               const Variant::Read &config,
                               double &start,
                               double end,
                               double priorStart)
{
    if (!FP::defined(start))
        return;
    if (!FP::defined(priorStart)) {
        if (!FP::defined(end) || end <= start)
            return;
        priorStart = start - (end - start);
    }

    if (!config["DataExtend"].exists())
        return;
    SequenceMatch::Composite
            selection(config["DataExtend"], {QString::fromStdString(station)}, {"raw"}, {});

    double modifiedStart = FP::undefined();
    if (config["MaximumAge"].exists()) {
        modifiedStart = Variant::Composite::offsetTimeInterval(config["MaximumAge"], start, false);
        if (FP::defined(modifiedStart) && modifiedStart > start)
            modifiedStart = FP::undefined();
    }
    auto selections = selection.toArchiveSelections();
    for (auto &m : selections) {
        m.start = modifiedStart;
        m.end = start;
        m.modifiedAfter = start;
        m.includeMetaArchive = false;
        m.includeDefaultStation = false;
    }

    /* Expand the start time to the start of the first value modified
     * in the prior interval and contained entirely in it.  This should
     * filter out old values that where modified to end them but also
     * get the interval set to the earliest new data processed. */

    StreamSink::Iterator data;
    Archive::Access access;
    auto reader = access.readStream(selections, &data);
    while (data.hasNext()) {
        auto value = data.next();
        if (testTerminated())
            return;
        if (!FP::defined(value.getStart()) || value.getStart() < priorStart)
            continue;
        if (!FP::defined(value.getEnd()) || value.getEnd() >= start)
            continue;
        start = value.getStart();
        break;
    }
    data.abort();
    reader->signalTerminate();
    reader->wait();
    reader.reset();
}

void EmailSend::run()
{
    if (testTerminated())
        return;

    double startProcessing = Time::time();

    std::unordered_map<SequenceName::Component, Variant::Read> emailActions;
    {
        Archive::Access reader;
        Archive::Access::ReadLock lock(reader);

        auto allStations = reader.availableStations(stations);
        if (allStations.empty()) {
            qCDebug(log_email_send) << "No stations available";
            return;
        }
        stations.clear();
        std::copy(allStations.begin(), allStations.end(), Util::back_emplacer(stations));

        double tnow = Time::time();
        SequenceSegment configSegment;
        {
            auto confList = SequenceSegment::Stream::read(
                    Archive::Selection(tnow - 1.0, tnow + 1.0, stations, {"configuration"},
                                       {"email"}), &reader);
            auto f = Range::findIntersecting(confList, tnow);
            if (f == confList.end()) {
                qCDebug(log_email_send) << "No configuration for" << stations;
                return;
            }
            configSegment = *f;
        }

        for (const auto &station : stations) {
            SequenceName stationUnit(station, "configuration", "email");
            auto config = configSegment.takeValue(stationUnit).read().hash("Send").hash(profile);
            if (!config.exists())
                continue;
            config.detachFromRoot();

            emailActions.emplace(station, std::move(config));
        }
    }
    if (emailActions.empty()) {
        qCDebug(log_email_send) << "No email send defined for" << stations;
        return;
    }

    if (testTerminated())
        return;

    stations.clear();
    for (const auto &add : emailActions) {
        stations.push_back(add.first);
    }
    std::sort(stations.begin(), stations.end());

    qCDebug(log_email_send) << "Starting email send for" << stations.size() << "station(s)";

    for (const auto &station : stations) {
        if (testTerminated())
            break;

        const auto &config = emailActions[station];
        Q_ASSERT(config.exists());

        QString rcKey(QString("emailsend %1 %2").arg(QString::fromStdString(profile),
                                                     QString::fromStdString(station)));
        QString rcProcessingLog(QString("processlog %1").arg(QString::fromStdString(station)));

        processingLog.clear();

        static const quint8 rcVersion = 1;

        Database::RunLock rc;
        bool enableLog = !config["ProcessingLog/Disable"].toBool();
        double priorStart = FP::undefined();
        if (doLock) {
            feedback.emitStage(tr("Lock %1").arg(QString::fromStdString(station).toUpper()),
                               tr("The system is acquiring an exclusive lock for %1.").arg(
                                       QString::fromStdString(station).toUpper()), false);
            bool ok = false;
            start = rc.acquire(rcKey, lockTimeout, &ok);
            if (ok && enableLog) {
                rc.acquire(rcProcessingLog, lockTimeout, &ok);
                if (!ok)
                    rc.fail();
            }
            if (!ok) {
                feedback.emitFailure();
                qCDebug(log_email_send) << "Email skipped for" << station;
                continue;
            }
            end = rc.startTime();

            if (enableLog) {
                static const quint8 rcLogVersion = 1;
                QByteArray data(rc.get(rcProcessingLog));
                if (data.isEmpty()) {
                    processingLog.clear();
                } else {
                    QDataStream stream(&data, QIODevice::ReadOnly);
                    quint8 version = 0;
                    stream >> version >> processingLog;
                    if (stream.status() != QDataStream::Ok || version != rcLogVersion) {
                        processingLog.clear();
                        qCDebug(log_email_send) << "Invalid processing log data";
                    } else {
                        qCDebug(log_email_send) << "Processing log contains" << processingLog.size()
                                                << "element(s)";
                    }
                }
            }

            {
                QByteArray data(rc.get(rcKey));
                if (data.isEmpty()) {
                    priorStart = FP::undefined();
                } else {
                    QDataStream stream(&data, QIODevice::ReadOnly);
                    quint8 version = 0;
                    stream >> version >> priorStart;
                    if (stream.status() != QDataStream::Ok || version != rcVersion) {
                        priorStart = FP::undefined();
                        qCDebug(log_email_send) << "Invalid data";
                    }
                }
            }
        }

        if (testTerminated()) {
            rc.fail();
            break;
        }

        double originalStart = start;

        expandInterval(station, config, start, end, priorStart);

        substitutions.clear();
        substitutions.setString("station", QString::fromStdString(station).toLower());
        substitutions.setString("profile", QString::fromStdString(profile));
        substitutions.setTime("time", end, false);
        substitutions.setTime("end", end);
        substitutions.setTime("lasttime", start, false);
        substitutions.setTime("start", start);
        substitutions.setTime("basestart", originalStart, false);
        if (doLock) {
            substitutions.setTime("modified", start);
            substitutions.setTime("modifiedafter", start);
        }

        qCDebug(log_email_send) << "Generating email for" << station << "in"
                                << Logging::range(start, end);

        if (processStation(station, config)) {
            if (doLock && enableLog && !config["ProcessingLog/Preserve"].toBool()) {
                rc.set(rcProcessingLog, QByteArray());
            }
            if (doLock) {
                QByteArray data;
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << rcVersion << originalStart;
                rc.set(rcKey, data);
            }
            rc.release();
            qCDebug(log_email_send) << "Email send completed for" << station;
        } else {
            feedback.emitFailure();
            rc.fail();
        }
    }

    double endProcessing = Time::time();
    qCDebug(log_email_send) << "Finished email send after"
                            << Logging::elapsed(endProcessing - startProcessing);

    QCoreApplication::processEvents();
}


ComponentOptions EmailSendComponent::getOptions()
{
    ComponentOptions options;

    options.add("profile",
                new ComponentOptionSingleString(tr("profile", "name"), tr("Execution profile"),
                                                tr("This is the profile name to execute.  Multiple profiles can be "
                                                   "defined to allow for different email types from a single "
                                                   "station.  Consult the station configuration for available profiles."),
                                                tr("aerosol")));

    ComponentOptionSingleDouble *timeout =
            new ComponentOptionSingleDouble(tr("wait", "name"), tr("Maximum wait time"),
                                            tr("This is the maximum time to wait for another process to "
                                               "complete, in seconds.  If there are two concurrent processes for "
                                               "the same station and profile, the new one will wait at most this "
                                               "many seconds before giving up without sending an email new data.  "
                                               "If this is set to undefined, it will wait forever for the other "
                                               "process to complete."), tr("One hour"));
    timeout->setMinimum(0.0);
    timeout->setAllowUndefined(true);
    options.add("timeout", timeout);

    return options;
}

QList<ComponentExample> EmailSendComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(
            ComponentExample(options, tr("Email the latest data report", "default example name"),
                             tr("This will generate and email out the latest processing report for "
                                "the \"aerosol\" profile.")));

    return examples;
}

int EmailSendComponent::actionAllowStations()
{ return INT_MAX; }

bool EmailSendComponent::actionRequiresTime()
{ return false; }

CPD3Action *EmailSendComponent::createTimeAction(const ComponentOptions &options,
                                                 double start,
                                                 double end,
                                                 const std::vector<std::string> &stations)
{ return new EmailSend(options, start, end, stations); }

CPD3Action *EmailSendComponent::createTimeAction(const ComponentOptions &options,
                                                 const std::vector<std::string> &stations)
{ return new EmailSend(options, stations); }
