if(Fortran_FOUND)
    cpd3_componenttest(acquire_dmt_ccn test.cxx cpd3datacore cpd3acquisition)
    set_property(TEST component_acquire_dmt_ccn PROPERTY ENVIRONMENT
                 "CPD3COMPONENTS=${CMAKE_BINARY_DIR}"
                 "PATH=${CMAKE_BINARY_DIR}/standalone/ccn_dmtmodel/:$ENV{PATH}")
endif(Fortran_FOUND)
