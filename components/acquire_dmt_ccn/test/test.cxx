/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <cstdint>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    double unpolledRemaining;

    double timeOfDay;
    double ssSP;
    bool tempStable;
    double T1;
    double T2;
    double T3;
    double Tsample;
    double Tinlet;
    double TOPC;
    double Tnafion;
    double Qsample;
    double Qsheath;
    double P;
    double Alaser;
    double V1stStage;
    double dtSP;
    double VpropValve;
    std::uint_fast16_t alarmBits;

    int minimumBin;
    double counts[21];
    bool multiLine;

    ModelInstrument() : incoming(), outgoing(), unpolledRemaining(0), timeOfDay(0.0)
    {
        ssSP = 1.0;
        tempStable = true;
        T1 = 21.0;
        T2 = 22.0;
        T3 = 23.0;
        Tsample = 24.0;
        Tinlet = 20.0;
        TOPC = 25.0;
        Tnafion = 26.0;
        Qsample = 0.05;
        Qsheath = 0.45;
        P = 1013.25;
        Alaser = 500.0;
        V1stStage = 3.9;
        VpropValve = 2.3;
        alarmBits = 0;
        dtSP = 2.0;

        minimumBin = 0;
        for (int i = 0; i < 21; i++) {
            counts[20 - i] = i * 10.0;
        }

        multiLine = true;
    }

    void advance(double seconds)
    {
        unpolledRemaining -= seconds;
        timeOfDay += seconds;

        while (unpolledRemaining <= 0.0) {
            unpolledRemaining += 1.0;

            outgoing.append("H,");
            outgoing.append(
                    QByteArray::number((int) (timeOfDay / 3600.0) % 24).rightJustified(2, '0'));
            outgoing.append(':');
            outgoing.append(
                    QByteArray::number((int) (timeOfDay / 60.0) % 60).rightJustified(2, '0'));
            outgoing.append(':');
            outgoing.append(QByteArray::number((int) (timeOfDay) % 60).rightJustified(2, '0'));
            outgoing.append(',');
            outgoing.append(QByteArray::number(ssSP, 'f', 2));
            outgoing.append(',');
            outgoing.append(tempStable ? "1.00" : "0.00");
            outgoing.append(',');
            outgoing.append(QByteArray::number(T1, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(T2, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(T3, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(Tsample, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(Tinlet, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(TOPC, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(Tnafion, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(Qsample * 1000.0, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(Qsheath * 1000.0, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(P, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(Alaser, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(V1stStage, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(dtSP, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(VpropValve, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(static_cast<quint16>(alarmBits)));
            if (multiLine) {
                outgoing.append('\r');
            } else {
                outgoing.append(',');
            }

            outgoing.append("C,");
            outgoing.append(QByteArray::number(minimumBin, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(totalConcentration(), 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(counts[20], 'f', 2));
            for (int i = 0; i < 20; i++) {
                outgoing.append(',');
                outgoing.append(QByteArray::number(counts[i], 'f', 2));
            }
            outgoing.append('\r');
        }

        while (incoming.length() >= 5) {
            QByteArray toParse(incoming.mid(0, 5));
            incoming.remove(0, 5);

            bool ok = false;
            double s = toParse.toDouble(&ok);
            if (!ok)
                continue;
            if (s < 0.0 || s > 99.99)
                continue;
            dtSP = s;
        }
    }

    double binConcentration(int idx) const
    {
        return counts[idx] / (Qsample * (1000.0 / 60.0));
    }

    Variant::Root allBinsCounts() const
    {
        Variant::Root result;
        for (int i = 0; i < 21; i++) {
            result.write().array(i).setDouble(counts[i]);
        }
        return result;
    }

    Variant::Root allBinsConcentrations() const
    {
        Variant::Root result;
        for (int i = 0; i < 21; i++) {
            result.write().array(i).setDouble(binConcentration(i));
        }
        return result;
    }

    double totalConcentration() const
    {
        double sum = 0.0;
        for (int i = minimumBin; i < 21; i++) {
            sum += binConcentration(i);
        }
        return sum;
    }
};

class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream,
                   double time = FP::undefined(),
                   const SequenceName::Component &suffix = {})
    {
        if (!stream.hasMeta("F1" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T1" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T2" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T3" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T4" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T5" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T6" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Tu" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("DT" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("DTg" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Q1" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Q2" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("U" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("P" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("V1" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("V2" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("A" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("N" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Np" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZBin" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Nb" + suffix, Variant::Root(21), "^Count", time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("ZNc", Variant::Root(21), "^Count", time))
            return false;
        return true;
    }

    bool checkContiguous(StreamCapture &stream)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("T1"))
            return false;
        if (!stream.checkContiguous("T2"))
            return false;
        if (!stream.checkContiguous("T3"))
            return false;
        if (!stream.checkContiguous("T4"))
            return false;
        if (!stream.checkContiguous("T5"))
            return false;
        if (!stream.checkContiguous("T6"))
            return false;
        if (!stream.checkContiguous("Tu"))
            return false;
        if (!stream.checkContiguous("DT"))
            return false;
        if (!stream.checkContiguous("DTg"))
            return false;
        if (!stream.checkContiguous("Q1"))
            return false;
        if (!stream.checkContiguous("Q2"))
            return false;
        if (!stream.checkContiguous("U"))
            return false;
        if (!stream.checkContiguous("P"))
            return false;
        if (!stream.checkContiguous("V1"))
            return false;
        if (!stream.checkContiguous("V2"))
            return false;
        if (!stream.checkContiguous("A"))
            return false;
        if (!stream.checkContiguous("N"))
            return false;
        if (!stream.checkContiguous("Np"))
            return false;
        if (!stream.checkContiguous("ZBin"))
            return false;
        if (!stream.checkContiguous("Nb"))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream, const ModelInstrument &model,
                     double time = FP::undefined(),
                     const SequenceName::Component &suffix = {})
    {
        if (!stream.hasAnyMatchingValue("F1" + suffix, Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("T1" + suffix, Variant::Root(model.T1), time))
            return false;
        if (!stream.hasAnyMatchingValue("T2" + suffix, Variant::Root(model.T2), time))
            return false;
        if (!stream.hasAnyMatchingValue("T3" + suffix, Variant::Root(model.T3), time))
            return false;
        if (!stream.hasAnyMatchingValue("T4" + suffix, Variant::Root(model.Tsample), time))
            return false;
        if (!stream.hasAnyMatchingValue("T5" + suffix, Variant::Root(model.TOPC), time))
            return false;
        if (!stream.hasAnyMatchingValue("T6" + suffix, Variant::Root(model.Tnafion), time))
            return false;
        if (!stream.hasAnyMatchingValue("Tu" + suffix, Variant::Root(model.Tinlet), time))
            return false;
        if (!stream.hasAnyMatchingValue("DT" + suffix, Variant::Root(model.dtSP), time))
            return false;
        if (!stream.hasAnyMatchingValue("Q1" + suffix, Variant::Root(model.Qsample), time))
            return false;
        if (!stream.hasAnyMatchingValue("Q2" + suffix, Variant::Root(model.Qsheath), time))
            return false;
        if (!stream.hasAnyMatchingValue("U" + suffix, Variant::Root(model.ssSP), time))
            return false;
        if (!stream.hasAnyMatchingValue("P" + suffix, Variant::Root(model.P), time))
            return false;
        if (!stream.hasAnyMatchingValue("V1" + suffix, Variant::Root(model.V1stStage), time))
            return false;
        if (!stream.hasAnyMatchingValue("V2" + suffix, Variant::Root(model.VpropValve), time))
            return false;
        if (!stream.hasAnyMatchingValue("A" + suffix, Variant::Root(model.Alaser), time))
            return false;
        if (!stream.hasAnyMatchingValue("N" + suffix, Variant::Root(model.totalConcentration()),
                                        time))
            return false;
        if (!stream.hasAnyMatchingValue("Nb" + suffix, model.allBinsConcentrations(), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZBin" + suffix, Variant::Root(model.minimumBin), time))
            return false;
        if (model.tempStable) {
            if (!stream.hasAnyMatchingValue("Np" + suffix,
                                            Variant::Root(model.totalConcentration()),
                                            time))
                return false;
        }
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream, const ModelInstrument &model,
                             double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("ZNc", model.allBinsCounts(), time))
            return false;
        return true;
    }

private slots:

    void initTestCase()
    {
        component =
                qobject_cast<AcquisitionComponent *>(ComponentLoader::create("acquire_dmt_ccn"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_dmt_ccn"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["StrictMode"].setBool(true);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 240; i++) {
            control.advance(0.5);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 240; i++) {
            control.advance(0.5);
            if (i % 10 == 0)
                QTest::qSleep(50);
        }
        interface->initiateShutdown();
        QVERIFY(interface->wait(30));
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(logging.checkContiguous("Uc"));
        QVERIFY(logging.hasMeta("Uc", Variant::Root(), QString()));
        QVERIFY(logging.hasAnyMatchingValue("Uc", Variant::Root(0.17279387), FP::undefined(),
                                            QString(),
                                            1E-5));
        QVERIFY(realtime.hasMeta("Uc", Variant::Root(), QString()));
        QVERIFY(realtime.hasAnyMatchingValue("Uc", Variant::Root(0.17279387), FP::undefined(),
                                             QString(),
                                             1E-5));
    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["StrictMode"].setBool(true);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.multiLine = false;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 240; i++) {
            control.advance(0.5);
        }
        for (int i = 0; i < 240; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        QVERIFY(interface->wait(30));
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(logging.checkContiguous("Uc"));
        QVERIFY(logging.hasMeta("Uc", Variant::Root(), QString()));
        QVERIFY(logging.hasAnyMatchingValue("Uc", Variant::Root(0.17279387), FP::undefined(),
                                            QString(),
                                            1E-5));
        QVERIFY(realtime.hasMeta("Uc", Variant::Root(), QString()));
        QVERIFY(realtime.hasAnyMatchingValue("Uc", Variant::Root(0.17279387), FP::undefined(),
                                             QString(),
                                             1E-5));
    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Model/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.tempStable = false;

        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 240; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }
        double checkTime = control.time();
        for (int i = 0; i < 240; i++) {
            control.advance(0.5);
        }
        while (control.time() < 360.0) {
            if (logging.hasAnyMatchingValue("U", Variant::Root(instrument.ssSP), checkTime))
                break;
            control.advance(0.5);
            QTest::qSleep(50);
        }
        QVERIFY(logging.hasAnyMatchingValue("U", Variant::Root(instrument.ssSP), checkTime));

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        QVERIFY(interface->wait(30));
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(!logging.hasAnyMatchingValue("Np", Variant::Root()));
        QVERIFY(!realtime.hasAnyMatchingValue("Np", Variant::Root()));
    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Model/Interval/Unit"].setString("Seconds");
        cv["Model/Interval/Count"].setInt64(30);
        cv["Model/Interval/Aligned"].setBool(false);
        cv["Model/MaximumBacklog"].setInt64(-1);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 240; i++) {
            control.advance(0.5);
        }
        Variant::Write cmd = Variant::Write::empty();
        cmd["Setpoint"].setDouble(1.5);
        interface->incomingCommand(cmd);
        for (int i = 0; i < 240; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        QVERIFY(interface->wait(30));
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(logging.checkContiguous("Uc"));
        QVERIFY(logging.hasMeta("Uc", Variant::Root(), QString()));
        QVERIFY(logging.hasAnyMatchingValue("Uc", Variant::Root(0.17279387), FP::undefined(),
                                            QString(),
                                            1E-5));
        QVERIFY(realtime.hasMeta("Uc", Variant::Root(), QString()));
        QVERIFY(realtime.hasAnyMatchingValue("Uc", Variant::Root(0.17279387), FP::undefined(),
                                             QString(),
                                             1E-5));

        QCOMPARE(instrument.dtSP, 1.5);
    }

    void badModel()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Model/Program"].setString("zzzz_bad");
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 240; i++) {
            control.advance(0.5);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 240; i++) {
            control.advance(0.5);
            if (i % 10 == 0)
                QTest::qSleep(50);
        }
        interface->initiateShutdown();
        QVERIFY(interface->wait(30));
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void instrumentTimeConvert()
    {
        ComponentOptions options;
        options = ingress->getOptions();

        qobject_cast<ComponentOptionSingleTime *>(options.get("basetime"))->set(86400.0);
        std::unique_ptr<ExternalConverter> input(ingress->createDataIngress(options));
        QVERIFY(input.get());
        input->start();

        ModelInstrument instrument;
        for (int i = 0; i < 480; i++) {
            instrument.advance(0.5);
        }

        StreamCapture logging;
        input->setEgress(&logging);
        input->incomingData(instrument.outgoing);
        input->endData();
        input->wait(30);
        input.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkMeta(logging, FP::undefined(), "_N11"));
        QVERIFY(checkValues(logging, instrument, 86460.0, "_N11"));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
