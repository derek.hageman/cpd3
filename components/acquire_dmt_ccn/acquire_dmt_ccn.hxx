/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIREDMTCCN_H
#define ACQUIREDMTCCN_H

#include "core/first.hxx"

#include <deque>
#include <map>
#include <cstdint>
#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "acquisition/spancheck.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"
#include "core/waitutils.hxx"
#include "smoothing/baseline.hxx"

class AcquireDMTCCN : public CPD3::Acquisition::FramedInstrument {
    double lastRecordTime;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum ResponseState {
        /* Waiting for enough valid records to confirm autoprobe success */
                RESP_AUTOPROBE_WAIT,

        /* Waiting for a valid record to start acquisition */
                RESP_WAIT,

        /* Acquiring data in passive mode */
                RESP_RUN,

        /* Same as wait, but discard the first timeout */
                RESP_INITIALIZE,

        /* Same as initialize but autoprobing */
                RESP_AUTOPROBE_INITIALIZE,
    };
    ResponseState responseState;

    enum LogStreamID {
        LogStream_Metadata = 0, LogStream_State, LogStream_Model, LogStream_Processing,

        LogStream_H, LogStream_C,

        LogStream_TOTAL, LogStream_RecordBegin = LogStream_H
    };
    CPD3::Data::StaticMultiplexer loggingMux;
    quint32 streamAge[LogStream_TOTAL];
    double streamTime[LogStream_TOTAL];

    class Configuration {
        double start;
        double end;
    public:
        bool enableModel;
        std::string modelProgram;
        std::string modelParameters;
        std::size_t maximumModelBacklog;

        bool useReportedStability;
        bool useCalculatedStability;

        double timeOrigin;
        double reportInterval;
        bool strictMode;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        ~Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under, const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    CPD3::Data::DynamicTimeInterval *modelInterval;
    CPD3::Data::DynamicTimeInterval *processingInterval;
    CPD3::Smoothing::BaselineSmoother *dtStability;

    CPD3::Data::Variant::Root instrumentMeta;
    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;

    double lastSampleFlow;
    double lastHRecordTime;

    struct ModelInput {
        double start;
        double end;
        double Qtot;
        double Qshaer;
        double P;
        double Tinlet;
        double Tcold;
        double Thot;

        bool valid() const;
    };

    struct ModelResult {
        double ss;
        bool valid;

        ModelInput input;
    };

    struct ProcessingPoint {
        double start;
        double end;
        double DT;
    };

    struct ModelPoint {
        double start;
        double end;
        double Q1;
        double Q2;
        double P;
        double T1;
        double T3;
        double Tu;
    };

    class ModelRunner;

    std::unique_ptr<ModelRunner> model;
    std::deque<ProcessingPoint> processingData;
    std::deque<ModelPoint> modelData;
    qint64 lastBinNumber;
    bool reportedStable;
    CPD3::Data::Variant::Flags instrumentAlarmFlags;

    void setDefaultInvalid();

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    void logValue(double startTime,
                  double endTime,
                  CPD3::Data::SequenceName::Component name,
                  CPD3::Data::Variant::Root &&value,
                  int streamID);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root &&value);

    void configAdvance(double frameTime);

    void emptyModelProcessing(double frameTime = CPD3::FP::undefined());

    void configurationChanged(double frameTime = CPD3::FP::undefined());

    void processModelPending(double frameTime = CPD3::FP::undefined());

    void emitMetadata(double frameTime);

    void invalidateLogValues(double frameTime);

    void advanceStream(int streamID, double frameTime);

    double remapModelInput(const CPD3::Data::SequenceName::Component &name,
                           const CPD3::Data::Variant::Read &input);

    int processH(std::deque<CPD3::Util::ByteView> &fields, double &frameTime);

    int processC(std::deque<CPD3::Util::ByteView> &fields, double &frameTime);

    int processRecord(const CPD3::Util::ByteView &line, double &frameTime);

public:
    AcquireDMTCCN(const CPD3::Data::ValueSegment::Transfer &config,
                  const std::string &loggingContext);

    AcquireDMTCCN(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireDMTCCN();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    virtual CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus();

    CPD3::Data::Variant::Root getCommands() override;

    virtual CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus();

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    virtual void command(const CPD3::Data::Variant::Read &value);

    virtual void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingInstrumentTimeout(double time);

    virtual void discardDataCompleted(double time);

    virtual void shutdownInProgress(double time);
};

class AcquireDMTCCNComponent
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_dmt_ccn"
                              FILE
                              "acquire_dmt_ccn.json")

    CPD3::ComponentOptions getBaseOptions();

public:

    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};

#endif
