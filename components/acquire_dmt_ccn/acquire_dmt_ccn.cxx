/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QEventLoop>

#include "core/threadpool.hxx"
#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/composite.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"
#include "algorithms/statistics.hxx"
#include "io/process.hxx"
#include "io/drivers/file.hxx"

#include "acquire_dmt_ccn.hxx"

#define TOTAL_BINS          21

//#define RECORD_MODEL_INPUTS

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;
using namespace CPD3::Algorithms;
using namespace CPD3::Smoothing;

static const char *instrumentAlarmTranslation[10] = {"LaserOverCurrent",             /* 1 */
                                                     "FirstStageMonitorOverVoltage", /* 2 */
                                                     "FlowOutOfRange",               /* 4 */
                                                     "TemperatureOutOfRange",        /* 8 */
                                                     "SampleTemperatureOutOfRange",  /* 16 */
                                                     "OPCError",                     /* 32 */
                                                     "CCNCountsLow",                 /* 64 */
                                                     "ColumnTemperaturesUnstableAlarm",/* 128 */
                                                     "NoOPCCommunications",          /* 256 */
                                                     "DuplicatedFile",               /* 512 */
};

static const std::string modelDefaultParameters = R"(*
* ==[ PARAMETERS FOR DMT CCN COUNTER SIMULATOR - UNITS IN SI ]==
*
===[ INLET PARAMETRES ]=================================================
Parabolic inlet velocity profile  [UPARAB]
.TRUE.
Sheath, Aerosol inlet RH (0-1 scale) [RHINS, RHINA]
1.0, 0.95
===[ GRID PARAMETERS ]==================================================
Number of cells in X-direction, Y-direction  [NI, NJ]
50, 50
===[ DEPENDENT VARIABLES ]==============================================
Solve for U, V, P equations ?
.false.
Solve for T, C equations ?
.true., .true.
===[ TERMINATION CRITERIA FOR ITERATIONS ]==============================
Maximum number of iterations per time step  [MAXIT]
100
Maximum acceptable value of residuals  [SORMAX]
1.0E-6
===[ UNDER-RELAXATION ]=================================================
Under-relaxation factor for U, P equation  [URFU, URFP]
0.7, 0.05
Under-relaxation factor for T, C equation  [URFT, URFC]
1.0, 1.0
===[ EQUATION SOLVER SELECTION ]========================================
Solver selection for U, V, P equation  [ISLVU, ISLVP]
1, 1
Solver selection for T, C equation  [ISLVT, ISLVC]
2, 2
===[ INPUT-OUTPUT PARAMETERS ]==========================================
Exit S is average over last (%) of chamber (0=exit point only) [SOAVGP]
5
)";


bool AcquireDMTCCN::ModelInput::valid() const
{
    if (!FP::defined(Qtot) ||
            !FP::defined(Qshaer) ||
            !FP::defined(P) ||
            !FP::defined(Tinlet) ||
            !FP::defined(Tcold) ||
            !FP::defined(Thot))
        return false;
    if (Qtot < 1.6667E-6 || Qtot > 1E-5 ||
            Qshaer < 2.0 ||
            Qshaer > 20.0 ||
            P < 50000.0 ||
            P > 120000.0 ||
            Tinlet < 290.0 ||
            Tinlet > 320.0 ||
            Tcold < 290.0 ||
            Tcold > 320.0 ||
            Thot < 290.0 ||
            Thot > 320.0)
        return false;
    return true;
}

class AcquireDMTCCN::ModelRunner {
    std::map<std::uint_fast64_t, ModelResult> resultReady;
    std::uint_fast64_t resultNext;
    std::uint_fast64_t resultSubmit;

    struct Worker {
        ModelRunner *runner;
        Util::ByteArray buffer;
        std::shared_ptr<IO::Process::Instance> process;
        std::unique_ptr<IO::Process::Stream> io;
        std::deque<std::pair<std::uint_fast64_t, ModelInput>> sent;

        Worker() = default;

        Worker(const Worker &) = delete;

        Worker &operator=(const Worker &) = delete;

        Worker(Worker &&) = default;

        Worker &operator=(Worker &&) = default;

        bool processReady()
        {
            bool anyAdded = false;
            Util::ByteArray::LineIterator it(buffer);
            while (auto line = it.next()) {
                if (sent.empty())
                    continue;

                auto data = line.toQByteArray().trimmed();
                bool ok = false;

                ModelResult result;
                result.input = sent.front().second;
                result.valid = true;
                result.ss = data.toDouble(&ok);
                if (!ok) {
                    result.ss = FP::undefined();
                }

                runner->resultReady.emplace(sent.front().first, std::move(result));
                sent.pop_front();
                anyAdded = true;
            }

            buffer.pop_front(it.getCurrentOffset());

            return anyAdded;
        }

        bool submit(const ModelInput &input, std::uint_fast64_t id)
        {
            if (!process) {
                process = runner->spawn.create();
                if (!process) {
                    qWarning("Failed to create CCN model process");
                    return false;
                }
                if (!process->start()) {
                    qWarning("Failed to start CCN model process");
                    process.reset();
                    return false;
                }
            }
            if (!io) {
                io = process->outputStream(true);
                if (!io) {
                    qWarning("Failed to get CCN model interface");
                    process.reset();
                    return false;
                }
                io->read.connect([this](const Util::ByteArray &data) {
                    {
                        std::lock_guard<std::mutex> lock(runner->mutex);
                        buffer += data;
                        if (!processReady())
                            return;
                    }
                    runner->available();
                });
                io->ended.connect([this]() {
                    std::lock_guard<std::mutex> lock(runner->mutex);
                    io.reset();
                    process.reset();
                });
                io->start();
                if (runner->parametersFile) {
                    io->write(runner->parametersFile->filename() + "\n");
                }
            }

            Util::ByteArray result;
            result += QByteArray::number(input.Qtot, 'E', 6);
            result += ", ";
            result += QByteArray::number(input.Qshaer, 'E', 6);
            result += ", ";
            result += QByteArray::number(input.P, 'E', 6);
            result += ", ";
            result += QByteArray::number(input.Tinlet, 'E', 6);
            result += ", ";
            result += QByteArray::number(input.Tcold, 'E', 6);
            result += ", ";
            result += QByteArray::number(input.Thot, 'E', 6);
            result += ", \'nul\'\n";
            io->write(std::move(result));

            sent.emplace_back(id, input);

            return true;
        }
    };

    std::vector<Worker> workers;

    std::mutex mutex;
    IO::Process::Spawn spawn;
    std::shared_ptr<IO::File::Backing> parametersFile;
    std::size_t maximumBacklog;

    bool expectingMore() const
    {
        for (const auto &worker : workers) {
            if (worker.sent.empty())
                continue;
            return true;
        }
        return false;
    }

public:
    ModelRunner(const std::string &command,
                const std::string &parameters,
                std::size_t maximumBacklog) : resultNext(0),
                                              resultSubmit(0),
                                              spawn(command),
                                              maximumBacklog(maximumBacklog)
    {
        spawn.inputStream = IO::Process::Spawn::StreamMode::Capture;
        spawn.outputStream = IO::Process::Spawn::StreamMode::Capture;
        spawn.environmentSet.emplace("GFORTRAN_UNBUFFERED_ALL", "1");

        auto n = std::thread::hardware_concurrency();
        if (n > 1) {
            workers.resize(n);
        } else {
            workers.resize(1);
        }
        for (auto &worker : workers) {
            worker.runner = this;
        }

        if (parameters.empty())
            return;

        parametersFile = IO::Access::temporaryFile();
        if (!parametersFile) {
            qWarning("Unable to create model parameters file");
            return;
        }
        auto write = parametersFile->block();
        if (!write) {
            qWarning("Unable to open parameters file for writing");
            return;
        }
        write->write(parameters);
    }

    ~ModelRunner() = default;

    void submit(const ModelInput &input)
    {
        {
            std::lock_guard<std::mutex> lock(mutex);
            Worker *target = nullptr;
            std::size_t backlogSize = 0;
            for (auto &worker : workers) {
                backlogSize += worker.sent.size();

                if (!target) {
                    target = &worker;
                } else if (worker.sent.size() < target->sent.size()) {
                    target = &worker;
                }
            }
            Q_ASSERT(target != nullptr);

            if (maximumBacklog == static_cast<std::size_t>(-1) || backlogSize <= maximumBacklog) {
                if (target->submit(input, resultSubmit)) {
                    resultSubmit++;
                    return;
                }
            }

            ModelResult result;
            result.input = input;
            result.valid = false;
            result.ss = FP::undefined();
            resultReady.emplace(resultSubmit, std::move(result));
            resultSubmit++;
        }
        available();
    }

    std::vector<ModelResult> consume()
    {
        std::vector<ModelResult> result;
        std::lock_guard<std::mutex> lock(mutex);
        while (!resultReady.empty()) {
            auto next = resultReady.begin();
            if (next->first > resultNext)
                break;

            result.emplace_back(std::move(next->second));
            resultNext = next->first + 1;
            resultReady.erase(next);
        }
        return result;
    }

    void finish()
    {
        std::condition_variable cv;
        Threading::Receiver rx;
        available.connect(rx, [&] { cv.notify_all(); });

        std::unique_lock<std::mutex> lock(mutex);
        if (maximumBacklog == static_cast<std::size_t>(-1)) {
            while (expectingMore()) {
                cv.wait(lock);
            }
        } else {
            using Clock = std::chrono::steady_clock;
            auto timeout = Clock::now() + std::chrono::seconds(30);

            while (timeout > Clock::now()) {
                if (!expectingMore())
                    break;
                cv.wait_until(lock, timeout);
            }
        }
    }

    Threading::Signal<> available;

    static std::size_t defaultMaximumBacklog()
    {
        auto n = std::thread::hardware_concurrency();
        if (n <= 1)
            return 2;
        return n * 2;
    }
};


AcquireDMTCCN::Configuration::Configuration() : start(FP::undefined()),
                                                end(FP::undefined()),
                                                enableModel(true),
#ifndef Q_OS_WIN32
                                                modelProgram("cpd3_ccn_dmtmodel"),
#else
        modelProgram(
                "cpd3_ccn_dmtmodel.exe"),
#endif
                                                modelParameters(modelDefaultParameters),
                                                maximumModelBacklog(
                                                        ModelRunner::defaultMaximumBacklog()),
                                                useReportedStability(true),
                                                useCalculatedStability(false),
                                                timeOrigin(FP::undefined()),
                                                reportInterval(2.0),
                                                strictMode(false)
{ }

AcquireDMTCCN::Configuration::~Configuration() = default;

AcquireDMTCCN::Configuration::Configuration(const Configuration &other, double s, double e) : start(
        s),
                                                                                              end(e),
                                                                                              enableModel(
                                                                                                      other.enableModel),
                                                                                              modelProgram(
                                                                                                      other.modelProgram),
                                                                                              modelParameters(
                                                                                                      other.modelParameters),
                                                                                              maximumModelBacklog(
                                                                                                      other.maximumModelBacklog),
                                                                                              useReportedStability(
                                                                                                      other.useReportedStability),
                                                                                              useCalculatedStability(
                                                                                                      other.useCalculatedStability),
                                                                                              timeOrigin(
                                                                                                      other.timeOrigin),
                                                                                              reportInterval(
                                                                                                      other.reportInterval),
                                                                                              strictMode(
                                                                                                      other.strictMode)
{
}

AcquireDMTCCN::Configuration::Configuration(const ValueSegment &other, double s, double e) : start(
        s),
                                                                                             end(e),
                                                                                             enableModel(
                                                                                                     true),
#ifndef Q_OS_WIN32
                                                                                             modelProgram(
                                                                                                     "cpd3_ccn_dmtmodel"),
#else
        modelProgram(
                "cpd3_ccn_dmtmodel.exe"),
#endif
                                                                                             modelParameters(
                                                                                                     modelDefaultParameters),
                                                                                             maximumModelBacklog(
                                                                                                     ModelRunner::defaultMaximumBacklog()),
                                                                                             useReportedStability(
                                                                                                     true),
                                                                                             useCalculatedStability(
                                                                                                     false),
                                                                                             timeOrigin(
                                                                                                     FP::undefined()),
                                                                                             reportInterval(
                                                                                                     2.0),
                                                                                             strictMode(
                                                                                                     false)
{
    setFromSegment(other);
}

AcquireDMTCCN::Configuration::Configuration(const Configuration &under,
                                            const ValueSegment &over,
                                            double s,
                                            double e) : start(s),
                                                        end(e),
                                                        enableModel(under.enableModel),
                                                        modelProgram(under.modelProgram),
                                                        modelParameters(under.modelParameters),
                                                        maximumModelBacklog(
                                                                under.maximumModelBacklog),
                                                        useReportedStability(
                                                                under.useReportedStability),
                                                        useCalculatedStability(
                                                                under.useCalculatedStability),
                                                        timeOrigin(under.timeOrigin),
                                                        reportInterval(under.reportInterval),
                                                        strictMode(under.strictMode)
{
    setFromSegment(over);
}

void AcquireDMTCCN::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["Model/Enable"].exists())
        enableModel = config["Model/Enable"].toBool();
    if (config["Model/Program"].exists())
        modelProgram = config["Model/Program"].toString();

    if (INTEGER::defined(config["Model/MaximumBacklog"].toInteger())) {
        auto n = config["Model/MaximumBacklog"].toInteger();
        if (n > 0) {
            maximumModelBacklog = static_cast<std::size_t>(n);
        } else {
            maximumModelBacklog = static_cast<std::size_t>(-1);
        }
    }

    if (config["Model/Parameters"].exists()) {
        modelParameters = config["Model/Parameters"].toString();
    }

    if (config["StrictMode"].exists())
        strictMode = config["StrictMode"].toBool();

    if (config["BaseTime"].exists())
        timeOrigin = config["BaseTime"].toDouble();

    if (config["ReportInterval"].exists())
        reportInterval = config["ReportInterval"].toDouble();

    if (config["Processing/UseReportedStability"].exists()) {
        useReportedStability = config["Processing/UseReportedStability"].toBool();
    }

    if (config["Processing/UseCalculatedStability"].exists()) {
        useCalculatedStability = config["Processing/UseCalculatedStability"].toBool();
    }
}


void AcquireDMTCCN::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("DMT");
    instrumentMeta["Model"].setString("CCN");

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;

    memset(streamAge, 0, sizeof(streamAge));
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
    }

    lastSampleFlow = FP::undefined();
    lastHRecordTime = FP::undefined();
    lastBinNumber = INTEGER::undefined();
    reportedStable = true;

    dtStability->reset();
}


AcquireDMTCCN::AcquireDMTCCN(const ValueSegment::Transfer &configData,
                             const std::string &loggingContext) : FramedInstrument("dmtccn",
                                                                                   loggingContext),
                                                                  autoprobeStatus(
                                                                          AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                  responseState(RESP_WAIT),
                                                                  loggingMux(LogStream_TOTAL)
{

    DynamicTimeInterval::Variable oneMinute;
    oneMinute.set(Time::Minute, 1, true);
    modelInterval =
            DynamicTimeInterval::fromConfiguration(configData, "Model/Interval", FP::undefined(),
                                                   FP::undefined(), true, &oneMinute);
    processingInterval = DynamicTimeInterval::fromConfiguration(configData, "Processing/Interval",
                                                                FP::undefined(), FP::undefined(),
                                                                true, &oneMinute);

    Variant::Write defaultSmoother = Variant::Write::empty();
    defaultSmoother["Type"].setString("SinglePoint");
    dtStability = BaselineSmoother::fromConfiguration(defaultSmoother, configData,
                                                      "Processing/TemperatureStability");

    setDefaultInvalid();
    config.append(Configuration());
    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
    configurationChanged();
}

void AcquireDMTCCN::setToAutoprobe()
{ responseState = RESP_AUTOPROBE_INITIALIZE; }

void AcquireDMTCCN::setToInteractive()
{ responseState = RESP_INITIALIZE; }


ComponentOptions AcquireDMTCCNComponent::getBaseOptions()
{
    ComponentOptions options;

    TimeIntervalSelectionOption *ti = new TimeIntervalSelectionOption(tr("model-interval", "name"),
                                                                      tr("Model processing interval"),
                                                                      tr("This is the interval the model is executed at.  This defines the "
                                                                         "amount of data averaged together to make a single request for the "
                                                                         "model to generate a supersaturation for."),
                                                                      tr("One minute",
                                                                         "default model interval"));
    ti->setDefaultAligned(true);
    options.add("model-interval", ti);

    ti = new TimeIntervalSelectionOption(tr("processing-interval", "name"),
                                         tr("General processing interval"),
                                         tr("This is the interval the that general processing is executed at.  "
                                            "This defines the amount of time that enters into the reported dT "
                                            "standard deviation."),
                                         tr("One minute", "default processing interval"));
    ti->setDefaultAligned(true);
    options.add("processing-interval", ti);

    BaselineSmootherOption *bs =
            new BaselineSmootherOption(tr("stability", "name"), tr("dT stability smoother"),
                                       tr("This is the smoother that is used to by the acquisition system to "
                                          "determine if T3-T1 is stable."),
                                       tr("Always stable", "default stability"));
    bs->setDefault(new BaselineSinglePoint);
    options.add("stability", bs);

    return options;
}

AcquireDMTCCN::AcquireDMTCCN(const ComponentOptions &options, const std::string &loggingContext)
        : FramedInstrument("dmtccn", loggingContext),
          autoprobeStatus(AcquisitionInterface::AutoprobeStatus::InProgress),
          responseState(RESP_WAIT),
          loggingMux(LogStream_TOTAL)
{
    DynamicTimeInterval::Variable oneMinute;
    oneMinute.set(Time::Minute, 1, true);

    if (options.isSet("model-interval")) {
        modelInterval = qobject_cast<TimeIntervalSelectionOption *>(
                options.get("model-interval"))->getInterval();
    } else {
        modelInterval = oneMinute.clone();
    }

    if (options.isSet("processing-interval")) {
        processingInterval = qobject_cast<TimeIntervalSelectionOption *>(
                options.get("processing-interval"))->getInterval();
    } else {
        processingInterval = oneMinute.clone();
    }

    dtStability = qobject_cast<BaselineSmootherOption *>(options.get("stability"))->getSmoother();

    setDefaultInvalid();
    Configuration base;
    base.maximumModelBacklog = -1;

    if (options.isSet("basetime")) {
        base.timeOrigin = qobject_cast<ComponentOptionSingleTime *>(options.get("basetime"))->get();
    }

    if (options.isSet("model-backlog")) {
        auto i = qobject_cast<ComponentOptionSingleInteger *>(options.get("model-backlog"))->get();
        if (INTEGER::defined(i) && i > 0)
            base.maximumModelBacklog = static_cast<std::size_t>(i);
        else
            base.maximumModelBacklog = static_cast<std::size_t>(-1);
    }

    config.append(base);
    configurationChanged();
}

AcquireDMTCCN::~AcquireDMTCCN()
{
    delete modelInterval;
    delete processingInterval;
    delete dtStability;
}


void AcquireDMTCCN::logValue(double startTime,
                             double endTime,
                             SequenceName::Component name,
                             Variant::Root &&value,
                             int streamID)
{
    if (!loggingEgress && !realtimeEgress)
        return;
    SequenceValue dv({{}, "raw", std::move(name)}, std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        loggingMux.incoming(streamID, std::move(dv), loggingEgress);
        return;
    }
    loggingMux.incoming(streamID, dv, loggingEgress);
    if (FP::defined(endTime)) {
        dv.setStart(endTime);
        dv.setEnd(endTime + 1.0);
    } else if (FP::defined(startTime)) {
        dv.setEnd(startTime + 1.0);
    }
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireDMTCCN::realtimeValue(double time, SequenceName::Component name, Variant::Root &&value)
{
    if (!realtimeEgress) return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

SequenceValue::Transfer AcquireDMTCCN::buildLogMeta(double time) const
{
    Q_ASSERT(!config.isEmpty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_dmt_ccn");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());

    result.emplace_back(SequenceName({}, "raw_meta", "T1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("TEC 1 temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("TEC 1"));

    result.emplace_back(SequenceName({}, "raw_meta", "T2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("TEC 2 temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("TEC 2"));

    result.emplace_back(SequenceName({}, "raw_meta", "T3"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("TEC 3 temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("TEC 3"));

    result.emplace_back(SequenceName({}, "raw_meta", "T4"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Sample temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));

    result.emplace_back(SequenceName({}, "raw_meta", "T5"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("OPC temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("OPC"));

    result.emplace_back(SequenceName({}, "raw_meta", "T6"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Nafion temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Nafion"));

    result.emplace_back(SequenceName({}, "raw_meta", "Tu"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Inlet temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Inlet"));

    result.emplace_back(SequenceName({}, "raw_meta", "DT"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("GroupUnits").setString("d\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Temperature gradient setpoint");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("dT setpoint"));

    result.emplace_back(SequenceName({}, "raw_meta", "DTg"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("GroupUnits").setString("d\xC2\xB0\x43");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Standard deviation of T3 - T1");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("dT stddev"));

    /* NOTE: Volume flows so no report T/P */
    result.emplace_back(SequenceName({}, "raw_meta", "Q1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Sample flow");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));

    result.emplace_back(SequenceName({}, "raw_meta", "Q2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Sheath flow");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sheath"));

    result.emplace_back(SequenceName({}, "raw_meta", "U"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0.000");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Supersaturation setting");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("SS Setting"));


    result.emplace_back(SequenceName({}, "raw_meta", "P"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Sample pressure");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));

    result.emplace_back(SequenceName({}, "raw_meta", "V1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("First stage monitor");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("1st stage monitor"));

    result.emplace_back(SequenceName({}, "raw_meta", "V2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Proportional valve");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Proportional valve"));

    result.emplace_back(SequenceName({}, "raw_meta", "A"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("mA");
    result.back().write().metadataReal("Description").setString("Laser current");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Laser current"));

    result.emplace_back(SequenceName({}, "raw_meta", "N"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0");
    result.back().write().metadataReal("Units").setString("cm\xE2\x81\xBB³");
    result.back().write().metadataReal("Description").setString("Total concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Concentration"));

    result.emplace_back(SequenceName({}, "raw_meta", "Np"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0");
    result.back().write().metadataReal("Units").setString("cm\xE2\x81\xBB³");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Total concentration without unstable data");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);

    result.emplace_back(SequenceName({}, "raw_meta", "ZBin"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataInteger("Format").setString("0");
    result.back()
          .write()
          .metadataInteger("Description")
          .setString("Instrument reported minimum bin");
    result.back().write().metadataInteger("Source").set(instrumentMeta);
    result.back().write().metadataInteger("Processing").toArray().after_back().set(processing);
    result.back().write().metadataInteger("Smoothing").hash("Mode").setString("None");

    result.emplace_back(SequenceName({}, "raw_meta", "Nb"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataArray("Count").setInt64(TOTAL_BINS);
    result.back().write().metadataArray("Children").metadataReal("Format").setString("00000.0");
    result.back()
          .write()
          .metadataArray("Children")
          .metadataReal("Units")
          .setString("cm\xE2\x81\xBB³");
    result.back().write().metadataArray("Description").setString("Bin concentration");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Realtime").hash("Page").setInt64(1);
    result.back()
          .write()
          .metadataArray("Realtime")
          .hash("Name")
          .setString(QObject::tr("Concentration"));
    result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInt64(1);


    if (config.first().enableModel) {
        result.emplace_back(SequenceName({}, "raw_meta", "Uc"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("00.000");
        result.back().write().metadataReal("Units").setString("%");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Super saturation calculated from the model");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("ModelParameters")
              .setString(config.first().modelParameters);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Name")
              .setString(QObject::tr("SS Calculated"));

#ifdef RECORD_MODEL_INPUTS
        result.emplace_back(SequenceName({}, "raw_meta", "ZMODEL"), Variant::Root(),time, FP::undefined());
        result.back().write().metadataHash("Description").
            setString("Super saturation model inputs");
        result.back().write().metadataHash("Source").set(instrumentMeta);
        result.back().write().metadataHash("Processing").
            arrayAppend(processing);
        result.back().write().metadataHashChild("ZQshaer").
            metadataDouble("Format").setString("0.000E0");
        result.back().write().metadataHashChild("ZQshaer").
            metadataDouble("Description").
            setString("Ratio of sheath to sample flow");
        result.back().write().metadataHashChild("Q").
            metadataDouble("Format").setString("0.000E0");
        result.back().write().metadataHashChild("Q").
            metadataDouble("Units").setString("m³/s");
        result.back().write().metadataHashChild("Q").
            metadataDouble("Description").setString("Total flow");
        result.back().write().metadataHashChild("P").
            metadataDouble("Format").setString("0.000E0");
        result.back().write().metadataHashChild("P").
            metadataDouble("Units").setString("Pa");
        result.back().write().metadataHashChild("P").
            metadataDouble("Description").setString("Pressure");
        result.back().write().metadataHashChild("Tu").
            metadataDouble("Format").setString("0.000E0");
        result.back().write().metadataHashChild("Tu").
            metadataDouble("Units").setString("K");
        result.back().write().metadataHashChild("Tu").
            metadataDouble("Description").setString("Inlet temperature");
        result.back().write().metadataHashChild("T1").
            metadataDouble("Format").setString("0.000E0");
        result.back().write().metadataHashChild("T1").
            metadataDouble("Units").setString("K");
        result.back().write().metadataHashChild("T1").
            metadataDouble("Description").setString("Cold temperature");
        result.back().write().metadataHashChild("T3").
            metadataDouble("Format").setString("0.000E0");
        result.back().write().metadataHashChild("T3").
            metadataDouble("Units").setString("K");
        result.back().write().metadataHashChild("T3").
            metadataDouble("Description").setString("Hot temperature");
#endif
    }


    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataSingleFlag("LaserOverCurrent")
          .hash("Description")
          .setString("Laser current too high");
    result.back().write().metadataSingleFlag("LaserOverCurrent").hash("Bits").setInt64(0x00010000);
    result.back()
          .write()
          .metadataSingleFlag("LaserOverCurrent")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_ccn");

    result.back()
          .write()
          .metadataSingleFlag("FirstStageMonitorOverVoltage")
          .hash("Description")
          .setString("First stage montor voltage greater than 4.7V");
    result.back()
          .write()
          .metadataSingleFlag("FirstStageMonitorOverVoltage")
          .hash("Bits")
          .setInt64(0x00020000);
    result.back()
          .write()
          .metadataSingleFlag("FirstStageMonitorOverVoltage")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_ccn");

    result.back()
          .write()
          .metadataSingleFlag("FlowOutOfRange")
          .hash("Description")
          .setString("Flow deviation greater than 20% or flow ratio outside of 5-15");
    result.back().write().metadataSingleFlag("FlowOutOfRange").hash("Bits").setInt64(0x00040000);
    result.back()
          .write()
          .metadataSingleFlag("FlowOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_ccn");

    result.back()
          .write()
          .metadataSingleFlag("TemperatureOutOfRange")
          .hash("Description")
          .setString("Any temperature setpoint more than 10C from the target");
    result.back()
          .write()
          .metadataSingleFlag("TemperatureOutOfRange")
          .hash("Bits")
          .setInt64(0x00080000);
    result.back()
          .write()
          .metadataSingleFlag("TemperatureOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_ccn");

    result.back()
          .write()
          .metadataSingleFlag("SampleTemperatureOutOfRange")
          .hash("Description")
          .setString("Sample temperature outside of 0-50C");
    result.back()
          .write()
          .metadataSingleFlag("SampleTemperatureOutOfRange")
          .hash("Bits")
          .setInt64(0x00100000);
    result.back()
          .write()
          .metadataSingleFlag("SampleTemperatureOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_ccn");

    result.back()
          .write()
          .metadataSingleFlag("OPCError")
          .hash("Description")
          .setString("First stage monitor greater than 4.7V and CCN counts too low");
    result.back().write().metadataSingleFlag("OPCError").hash("Bits").setInt64(0x00200000);
    result.back()
          .write()
          .metadataSingleFlag("OPCError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_ccn");

    result.back()
          .write()
          .metadataSingleFlag("CCNCountsLow")
          .hash("Description")
          .setString("CCN counts too low");
    result.back().write().metadataSingleFlag("CCNCountsLow").hash("Bits").setInt64(0x00400000);
    result.back()
          .write()
          .metadataSingleFlag("CCNCountsLow")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_ccn");

    result.back()
          .write()
          .metadataSingleFlag("ColumnTemperaturesUnstableAlarm")
          .hash("Description")
          .setString("Column temperature unstable alarm active");
    result.back()
          .write()
          .metadataSingleFlag("ColumnTemperaturesUnstableAlarm")
          .hash("Bits")
          .setInt64(0x00800000);
    result.back()
          .write()
          .metadataSingleFlag("ColumnTemperaturesUnstableAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_ccn");

    result.back()
          .write()
          .metadataSingleFlag("NoOPCCommunications")
          .hash("Description")
          .setString("OPC not communicating with CCN control computer");
    result.back()
          .write()
          .metadataSingleFlag("NoOPCCommunications")
          .hash("Bits")
          .setInt64(0x01000000);
    result.back()
          .write()
          .metadataSingleFlag("NoOPCCommunications")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_ccn");

    result.back()
          .write()
          .metadataSingleFlag("DuplicatedFile")
          .hash("Description")
          .setString("A duplicate file was detected on the CCN computer");
    result.back().write().metadataSingleFlag("DuplicatedFile").hash("Bits").setInt64(0x02000000);
    result.back()
          .write()
          .metadataSingleFlag("DuplicatedFile")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_ccn");

    result.back()
          .write()
          .metadataSingleFlag("ReportedTemperatureInstability")
          .hash("Description")
          .setString(
                  "The temperature difference setpoint is unstable as calculated by the instrument");
    result.back()
          .write()
          .metadataSingleFlag("ReportedTemperatureInstability")
          .hash("Bits")
          .setInt64(0x10000000);
    result.back()
          .write()
          .metadataSingleFlag("ReportedTemperatureInstability")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_ccn");

    result.back()
          .write()
          .metadataSingleFlag("CalculatedTemperatureInstability")
          .hash("Description")
          .setString(
                  "The temperature difference setpoint is unstable as calculated by the acquisition system");
    result.back()
          .write()
          .metadataSingleFlag("CalculatedTemperatureInstability")
          .hash("Bits")
          .setInt64(0x20000000);
    result.back()
          .write()
          .metadataSingleFlag("CalculatedTemperatureInstability")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_ccn");

    result.back()
          .write()
          .metadataSingleFlag("SafeModeActive")
          .hash("Description")
          .setString("The CCN control program has entered safe mode due to a serious alarm");
    result.back().write().metadataSingleFlag("SafeModeActive").hash("Bits").setInt64(0x80000000);
    result.back()
          .write()
          .metadataSingleFlag("SafeModeActive")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_ccn");

    return result;
}

SequenceValue::Transfer AcquireDMTCCN::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_dmt_ccn");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());

    result.emplace_back(SequenceName({}, "raw_meta", "ZNc"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataArray("Count").setInt64(TOTAL_BINS);
    result.back().write().metadataArray("Children").metadataReal("Format").setString("00000");
    result.back().write().metadataArray("Children").metadataReal("Units").setString("Hz");
    result.back().write().metadataArray("Description").setString("Bin counts");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Realtime").hash("Page").setInt64(1);
    result.back().write().metadataArray("Realtime").hash("Name").setString(QObject::tr("Counts"));
    result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInt64(1);

    return result;
}

void AcquireDMTCCN::configAdvance(double frameTime)
{
    Q_ASSERT(!config.isEmpty());
    int oldSize = config.size();
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.isEmpty());

    if (oldSize == config.size())
        return;
    configurationChanged(frameTime);
}

void AcquireDMTCCN::emptyModelProcessing(double frameTime)
{
    if (!model)
        return;
    model->finish();
    processModelPending(frameTime);
}

void AcquireDMTCCN::configurationChanged(double frameTime)
{
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;

    if (model) {
        emptyModelProcessing(frameTime);
        model.reset();
    }
    if (!config.first().enableModel)
        return;

    model.reset(new ModelRunner(config.first().modelProgram, config.first().modelParameters,
                                config.first().maximumModelBacklog));
}

static double calculateConcentration(double counts, double flow)
{
    if (!FP::defined(counts) || !FP::defined(flow))
        return FP::undefined();
    if (flow <= 0.0)
        return FP::undefined();
    return counts / (flow * (1000.0 / 60.0));
}

static double calculateDt(double T1, double T3)
{
    if (!FP::defined(T1) || !FP::defined(T3))
        return FP::undefined();
    return T3 - T1;
}

void AcquireDMTCCN::processModelPending(double frameTime)
{
    Q_ASSERT(model.get() != nullptr);
    for (const auto &result : model->consume()) {
        Q_ASSERT(FP::defined(result.input.start));
        Q_ASSERT(FP::defined(result.input.end));

        if (!result.valid || result.input.start == result.input.end) {
            loggingMux.advance(LogStream_Model, result.input.end, loggingEgress);
            continue;
        }

        SequenceValue dv
                ({{}, "raw", "Uc"}, Variant::Root(result.ss), result.input.start, result.input.end);
        if (realtimeEgress) {
            loggingMux.incoming(LogStream_Model, dv, loggingEgress);
            if (FP::defined(frameTime)) {
                dv.setStart(frameTime);
                dv.setEnd(frameTime + 1.0);
            }
            realtimeEgress->incomingData(std::move(dv));
        } else {
            loggingMux.incoming(LogStream_Model, std::move(dv), loggingEgress);
        }

#ifdef RECORD_MODEL_INPUTS
        Variant::Root parameters;
        parameters.write().hash("ZQshaer").setDouble(result.input.Qshaer);
        parameters.write().hash("Q").setDouble(result.input.Qtot);
        parameters.write().hash("P").setDouble(result.input.P);
        parameters.write().hash("Tu").setDouble(result.input.Tinlet);
        parameters.write().hash("T1").setDouble(result.input.Tcold);
        parameters.write().hash("T3").setDouble(result.input.Thot);
        loggingMux.incoming(LogStream_Model, SequenceValue(
            SequenceName({}, "raw_meta", "ZMODEL"), std::move(parameters),
            result.input.start, result.input.end), loggingEgress);
#endif

        loggingMux.advance(LogStream_Model, result.input.end, loggingEgress);
    }
}

void AcquireDMTCCN::invalidateLogValues(double frameTime)
{
    emptyModelProcessing(frameTime);
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
        streamAge[i] = 0;

        if (loggingEgress != NULL)
            loggingMux.advance(i, frameTime, loggingEgress);
    }
    loggingMux.clear();
    loggingLost(frameTime);
}

void AcquireDMTCCN::emitMetadata(double frameTime)
{
    if (!FP::defined(frameTime))
        return;

    if (!haveEmittedLogMeta && loggingEgress) {
        haveEmittedLogMeta = true;
        loggingMux.incoming(LogStream_Metadata, buildLogMeta(frameTime), loggingEgress);
    }

    if (!haveEmittedRealtimeMeta && realtimeEgress) {
        haveEmittedRealtimeMeta = true;

        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }
}

#define CALCULATE_WEIGHTED_AVERAGE(input, member, output) do { \
    double totalTime = 0.0; \
    for (const auto &p : input) { \
        Q_ASSERT(FP::defined(p.start) && FP::defined(p.end)); \
        double dT = p.end - p.start; \
        if (dT <= 0.0) \
            continue; \
        if (!FP::defined(p.member)) \
            continue; \
        totalTime += dT; \
    } \
    output = FP::undefined(); \
    for (const auto &p : input) { \
        double dT = p.end - p.start; \
        if (dT <= 0.0) \
            continue; \
        if (!FP::defined(p.member)) \
            continue; \
        double weighted = p.member * (dT / totalTime); \
        if (!FP::defined(output)) \
            output = weighted; \
        else \
            output += weighted; \
    } \
} while (false)

void AcquireDMTCCN::advanceStream(int streamID, double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));

    if (loggingEgress == NULL) {
        memset(streamAge, 0, sizeof(streamAge));
        for (int i = 0; i < LogStream_TOTAL; i++) {
            streamTime[i] = FP::undefined();
        }
        loggingMux.clear();
    }

    emitMetadata(frameTime);
    loggingMux.advance(LogStream_Metadata, frameTime, loggingEgress);

    quint32 streamBits = (1 << streamID);
    for (int i = LogStream_RecordBegin; i < LogStream_TOTAL; i++) {
        if (i == streamID)
            continue;
        if (!(streamAge[streamID] & streamBits)) {
            streamAge[streamID] |= streamBits;
            continue;
        }

        streamTime[i] = FP::undefined();
        streamAge[i] = 0;
        loggingMux.advance(i, frameTime, loggingEgress);
    }

    if (streamAge[LogStream_State] & streamBits) {
        double startTime = streamTime[LogStream_State];
        double endTime = frameTime;
        if (FP::defined(startTime) && FP::defined(endTime)) {
            logValue(startTime, endTime, "F1", Variant::Root(instrumentAlarmFlags),
                     LogStream_State);
        } else {
            loggingMux.advance(LogStream_State, endTime, loggingEgress);
        }
        streamTime[LogStream_State] = endTime;
        streamAge[LogStream_State] = 0;

        instrumentAlarmFlags.clear();
    } else {
        streamAge[LogStream_State] |= streamBits;
    }

    streamAge[streamID] = 0;
    streamTime[streamID] = frameTime;

    if (model && config.first().enableModel) {
        if (modelData.empty()) {
            ModelPoint add;
            add.start = frameTime;
            add.end = frameTime;
            add.Q1 = FP::undefined();
            add.Q2 = FP::undefined();
            add.P = FP::undefined();
            add.T1 = FP::undefined();
            add.T3 = FP::undefined();
            add.Tu = FP::undefined();
            modelData.emplace_back(add);
        }

        double intervalEnd = modelData.front().start;
        intervalEnd = modelInterval->apply(frameTime, intervalEnd, true);
        if (!FP::defined(intervalEnd) || modelData.back().end >= intervalEnd) {
            if (!FP::defined(intervalEnd))
                intervalEnd = frameTime;
            ModelInput input;
            input.start = modelData.front().start;

            double Q1;
            CALCULATE_WEIGHTED_AVERAGE(modelData, Q1, Q1);
            double Q2;
            CALCULATE_WEIGHTED_AVERAGE(modelData, Q2, Q2);
            if (FP::defined(Q1) && FP::defined(Q2)) {
                input.Qtot = (Q1 + Q2) * 1E-3 / 60.0;
                if (Q1 != 0.0) {
                    input.Qshaer = Q2 / Q1;
                } else {
                    input.Qshaer = FP::undefined();
                }
            } else {
                input.Qtot = FP::undefined();
                input.Qshaer = FP::undefined();
            }

            CALCULATE_WEIGHTED_AVERAGE(modelData, P, input.P);
            if (FP::defined(input.P))
                input.P *= 1E2;
            CALCULATE_WEIGHTED_AVERAGE(modelData, T1, input.Tcold);
            if (FP::defined(input.Tcold))
                input.Tcold += 273.15;
            CALCULATE_WEIGHTED_AVERAGE(modelData, T3, input.Thot);
            if (FP::defined(input.Thot))
                input.Thot += 273.15;
            CALCULATE_WEIGHTED_AVERAGE(modelData, Tu, input.Tinlet);
            if (FP::defined(input.Tinlet))
                input.Tinlet += 273.15;

            if (modelData.back().end <= intervalEnd || intervalEnd <= modelData.front().start) {
                input.end = modelData.back().end;
                modelData.clear();
            } else {
                input.end = intervalEnd;
                modelData.erase(modelData.begin(), modelData.end() - 1);
                modelData.back().start = intervalEnd;
            }

            model->submit(input);
        }
    } else {
        modelData.clear();
        loggingMux.advance(LogStream_Model, frameTime, loggingEgress);
    }


    if (processingData.empty()) {
        ProcessingPoint add;
        add.start = frameTime;
        add.end = frameTime;
        add.DT = FP::undefined();
        processingData.emplace_back(add);
    }

    double intervalEnd = processingData.front().start;
    intervalEnd = processingInterval->apply(frameTime, intervalEnd, true);
    if (!FP::defined(intervalEnd) || processingData.back().end >= intervalEnd) {
        if (!FP::defined(intervalEnd))
            intervalEnd = frameTime;
        double startTime = processingData.front().start;

        std::vector<double> dtData;
        for (const auto &p : processingData) {
            if (!FP::defined(p.DT))
                continue;
            dtData.emplace_back(p.DT);
        }

        double endTime;
        if (processingData.back().end <= intervalEnd ||
                intervalEnd <= processingData.front().start) {
            endTime = processingData.back().end;
            processingData.clear();
        } else {
            endTime = intervalEnd;
            processingData.erase(processingData.begin(), processingData.end() - 1);
            processingData.back().start = intervalEnd;
        }

        logValue(startTime, endTime, "DTg", Variant::Root(Statistics::sd(dtData)),
                 LogStream_Processing);
        logValue(startTime, endTime, "ZBin", Variant::Root(lastBinNumber), LogStream_Processing);
    }
}

double AcquireDMTCCN::remapModelInput(const SequenceName::Component &name,
                                      const Variant::Read &input)
{
    Variant::Root v(input);
    remap("ZMODEL" + name, v);
    return v.read().toDouble();
}

int AcquireDMTCCN::processH(std::deque<Util::ByteView> &fields, double &frameTime)
{
    bool ok = false;

    if (fields.empty()) return 1000;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    auto subfields = Util::as_deque(field.split(':'));

    if (subfields.empty()) return 1001;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    qint64 ihour = field.parse_i64(&ok);
    if (!ok) return 1002;
    if (ihour < 0) return 1003;
    if (ihour > 23) return 1004;
    Variant::Root hour(ihour);
    remap("HOUR", hour);
    ihour = hour.read().toInt64();

    if (subfields.empty()) return 1005;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    qint64 iminute = field.parse_i64(&ok);
    if (!ok) return 1006;
    if (iminute < 0) return 1007;
    if (iminute > 59) return 1008;
    Variant::Root minute(iminute);
    remap("MINUTE", minute);
    iminute = minute.read().toInt64();

    if (subfields.empty()) return 1009;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    qint64 isecond = field.parse_i64(&ok);
    if (!ok) return 1010;
    if (isecond < 0) return 1011;
    if (isecond > 60) return 1012;
    Variant::Root second(isecond);
    remap("SECOND", second);
    isecond = second.read().toInt64();

    if (!FP::defined(frameTime) &&
            FP::defined(config.first().timeOrigin) &&
            INTEGER::defined(ihour) &&
            ihour >= 0 &&
            ihour <= 23 &&
            INTEGER::defined(iminute) &&
            iminute >= 0 &&
            iminute <= 59 &&
            INTEGER::defined(isecond) && isecond >= 0 && isecond <= 60) {
        frameTime = config.first().timeOrigin +
                (double) (ihour * 3600) +
                (double) (iminute * 60) +
                (double) isecond;
        configAdvance(frameTime);
    }
    lastHRecordTime = frameTime;

    if (fields.empty()) return 1013;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root U(field.parse_real(&ok));
    if (!ok) return 1014;
    if (!FP::defined(U.read().toReal())) return 1015;
    remap("U", U);

    if (fields.empty()) return 1016;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root tempStable;
    tempStable.write().setBool(field.parse_real(&ok) != 0.0);
    if (!ok) return 1017;
    remap("ZSTABLE", tempStable);

    if (fields.empty()) return 1018;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T1(field.parse_real(&ok));
    if (!ok) return 1019;
    if (!FP::defined(T1.read().toReal())) return 1020;
    remap("T1", T1);

    if (fields.empty()) return 1021;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T2(field.parse_real(&ok));
    if (!ok) return 1022;
    if (!FP::defined(T2.read().toReal())) return 1023;
    remap("T2", T2);

    if (fields.empty()) return 1022;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T3(field.parse_real(&ok));
    if (!ok) return 1023;
    if (!FP::defined(T3.read().toReal())) return 1024;
    remap("T3", T3);

    if (fields.empty()) return 1025;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T4(field.parse_real(&ok));
    if (!ok) return 1026;
    if (!FP::defined(T4.read().toReal())) return 1027;
    remap("T4", T4);

    if (fields.empty()) return 1028;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Tu(field.parse_real(&ok));
    if (!ok) return 1029;
    if (!FP::defined(Tu.read().toReal())) return 1030;
    remap("Tu", Tu);

    if (fields.empty()) return 1031;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T5(field.parse_real(&ok));
    if (!ok) return 1032;
    if (!FP::defined(T5.read().toReal())) return 1033;
    remap("T5", T5);

    if (fields.empty()) return 1034;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T6(field.parse_real(&ok));
    if (!ok) return 1035;
    if (!FP::defined(T6.read().toReal())) return 1036;
    remap("T6", T6);

    if (fields.empty()) return 1037;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Q1(field.parse_real(&ok));
    if (!ok) return 1038;
    if (!FP::defined(Q1.read().toReal())) return 1039;
    Q1.write().setDouble(Q1.read().toDouble() * 0.001);
    remap("Q1", Q1);

    if (fields.empty()) return 1040;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Q2(field.parse_real(&ok));
    if (!ok) return 1041;
    if (!FP::defined(Q2.read().toReal())) return 1042;
    Q2.write().setDouble(Q2.read().toDouble() * 0.001);
    remap("Q2", Q2);

    if (fields.empty()) return 1043;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root P(field.parse_real(&ok));
    if (!ok) return 1044;
    if (!FP::defined(P.read().toReal())) return 1045;
    remap("P", P);

    if (fields.empty()) return 1046;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root A(field.parse_real(&ok));
    if (!ok) return 1047;
    if (!FP::defined(A.read().toReal())) return 1048;
    remap("A", A);

    if (fields.empty()) return 1049;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root V1(field.parse_real(&ok));
    if (!ok) return 1050;
    if (!FP::defined(V1.read().toReal())) return 1051;
    remap("V1", V1);

    if (fields.empty()) return 1052;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root DT(field.parse_real(&ok));
    if (!ok) return 1053;
    if (!FP::defined(DT.read().toReal())) return 1054;
    remap("DT", DT);

    if (fields.empty()) return 1055;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root V2(field.parse_real(&ok));
    if (!ok) return 1056;
    if (!FP::defined(V2.read().toReal())) return 1057;
    remap("V2", V2);

    Variant::Root alarmCode(0);
    if (!fields.empty()) {
        field = fields.front().string_trimmed();
        alarmCode.write().setInt64(field.parse_i64(&ok));
        if (!ok)
            alarmCode.write().setInt64((qint64) field.parse_real(&ok));
        if (!ok) {
            alarmCode.write().setInteger(0);
        } else {
            fields.pop_front();
        }
    }
    remap("ZALARM", alarmCode);

    if (!fields.empty()) {
        if (fields.front().toQByteArray().trimmed() == "C") {
            fields.pop_front();
            int code = processC(fields, frameTime);
            if (code != 0)
                return code;
        }
    }

    if (config.first().strictMode) {
        if (!fields.empty())
            return 1058;
    }

    emitMetadata(frameTime);

    if (INTEGER::defined(alarmCode.read().toInt64())) {
        qint64 flagsBits(alarmCode.read().toInt64());
        if (flagsBits < 0) {
            flagsBits = -flagsBits;
            instrumentAlarmFlags.insert("SafeModeActive");
        }
        for (int i = 0;
                i <
                        (int) (sizeof(instrumentAlarmTranslation) /
                                sizeof(instrumentAlarmTranslation[0]));
                i++) {
            if (flagsBits & (Q_INT64_C(1) << i)) {
                if (instrumentAlarmTranslation[i] != NULL) {
                    instrumentAlarmFlags.insert(instrumentAlarmTranslation[i]);
                } else {
                    qCDebug(log) << "Unrecognized bit" << hex << ((1 << i)) << "set in alarm code";
                }
            }
        }
    }

    reportedStable = tempStable.read().toBool();
    if (!reportedStable) {
        instrumentAlarmFlags.insert("ReportedTemperatureInstability");
    }

    lastSampleFlow = Q1.read().toDouble();

    double startTime = streamTime[LogStream_H];
    double endTime = frameTime;
    if (!FP::defined(startTime) || !FP::defined(endTime)) {
        streamTime[LogStream_H] = endTime;
        return 0;
    }

    if (config.first().enableModel) {
        ModelPoint add;
        add.start = startTime;
        add.end = endTime;
        add.Q1 = remapModelInput("Q1", Q1);
        add.Q2 = remapModelInput("Q2", Q2);
        add.P = remapModelInput("P", P);
        add.T1 = remapModelInput("T1", T1);
        add.T3 = remapModelInput("T3", T3);
        add.Tu = remapModelInput("Tu", Tu);
        modelData.emplace_back(add);
    }

    double calculatedDt = calculateDt(T1.read().toDouble(), T3.read().toDouble());
    {
        ProcessingPoint add;
        add.start = startTime;
        add.end = endTime;
        add.DT = calculatedDt;
        processingData.emplace_back(add);
    }

    dtStability->add(calculatedDt, startTime, endTime);
    if (!dtStability->stable()) {
        instrumentAlarmFlags.insert("CalculatedTemperatureInstability");
    }
    logValue(startTime, endTime, "U", std::move(U), LogStream_H);
    logValue(startTime, endTime, "T1", std::move(T1), LogStream_H);
    logValue(startTime, endTime, "T2", std::move(T2), LogStream_H);
    logValue(startTime, endTime, "T3", std::move(T3), LogStream_H);
    logValue(startTime, endTime, "T4", std::move(T4), LogStream_H);
    logValue(startTime, endTime, "T5", std::move(T5), LogStream_H);
    logValue(startTime, endTime, "T6", std::move(T6), LogStream_H);
    logValue(startTime, endTime, "Tu", std::move(Tu), LogStream_H);
    logValue(startTime, endTime, "Q1", std::move(Q1), LogStream_H);
    logValue(startTime, endTime, "Q2", std::move(Q2), LogStream_H);
    logValue(startTime, endTime, "P", std::move(P), LogStream_H);
    logValue(startTime, endTime, "A", std::move(A), LogStream_H);
    logValue(startTime, endTime, "V1", std::move(V1), LogStream_H);
    logValue(startTime, endTime, "V2", std::move(V2), LogStream_H);
    logValue(startTime, endTime, "DT", std::move(DT), LogStream_H);

    advanceStream(LogStream_H, frameTime);
    return 0;
}

int AcquireDMTCCN::processC(std::deque<Util::ByteView> &fields, double &frameTime)
{
    bool ok = false;

    if (fields.empty()) return 2000;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZBin;
    {
        double v = field.parse_real(&ok);
        if (!ok) return 2001;
        if (!FP::defined(v)) return 2002;
        ZBin.write().setInt64((qint64) v);
    }
    remap("ZBin", ZBin);

    if (fields.empty()) return 2003;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root N(field.parse_real(&ok));
    if (!ok) return 2004;
    if (!FP::defined(N.read().toReal())) return 2005;
    remap("N", N);

    Variant::Root counts;

    if (fields.empty()) return 2006;
    {
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root overflow(field.parse_real(&ok));
        if (!ok) return 2007;
        if (!FP::defined(overflow.read().toReal())) return 2008;
        remap("ZN" + std::to_string(TOTAL_BINS), overflow);
        counts.write().array(TOTAL_BINS - 1).set(overflow);
    }

    for (int i = 0; i < TOTAL_BINS - 1; i++) {
        if (fields.empty()) return 2100 + (i * 10);
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root bin(field.parse_real(&ok));
        if (!ok) return 2101 + (i * 10);
        if (!FP::defined(bin.read().toReal())) return 2102 + (i * 10);
        remap("ZN" + std::to_string(i + 1), bin);
        counts.write().array(i).set(bin);
    }
    remap("ZN", counts);

    if (!fields.empty()) {
        if (fields.front().toQByteArray().trimmed() == "H") {
            fields.pop_front();
            int code = processH(fields, frameTime);
            if (code != 0)
                return code;
        }
    }

    if (config.first().strictMode) {
        if (!fields.empty())
            return 2009;
    }

    if (!FP::defined(frameTime))
        frameTime = lastHRecordTime;
    else
        lastHRecordTime = frameTime;

    emitMetadata(frameTime);

    Variant::Root Nb;
    for (int i = 0, max = counts.read().toArray().size(); i < max; i++) {
        Variant::Root
                conc(calculateConcentration(counts.read().array(i).toDouble(), lastSampleFlow));
        remap("Nb" + std::to_string(i + 1), conc);
        Nb.write().array(i).set(conc);
    }
    remap("Nb", Nb);

    lastBinNumber = ZBin.read().toInt64();

    double startTime = streamTime[LogStream_C];
    double endTime = frameTime;
    if (!FP::defined(startTime) || !FP::defined(endTime)) {
        streamTime[LogStream_C] = endTime;
        return 0;
    }

    logValue(startTime, endTime, "Nb", std::move(Nb), LogStream_C);
    realtimeValue(endTime, "ZNc", std::move(counts));

    if ((config.first().useReportedStability && reportedStable) ||
            (config.first().useCalculatedStability && dtStability->stable())) {
        logValue(startTime, endTime, "Np", Variant::Root(N), LogStream_C);
    }
    logValue(startTime, endTime, "N", std::move(N), LogStream_C);

    advanceStream(LogStream_C, frameTime);
    return 0;
}

int AcquireDMTCCN::processRecord(const Util::ByteView &line, double &frameTime)
{
    Q_ASSERT(!config.isEmpty());

    if (line.size() < 3)
        return 1;

    auto fields = Util::as_deque(line.split(','));
    if (fields.size() < 2)
        return 2;
    auto code = fields.front().toQByteArray().trimmed();
    fields.pop_front();
    if (code.length() != 1)
        return 3;

    switch (code.at(0)) {
    case 'C':
        return processC(fields, frameTime);
    case 'H':
        return processH(fields, frameTime);
    default:
        return 4;
    }

    return -1;
}


void AcquireDMTCCN::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    configAdvance(frameTime);

    switch (responseState) {
    case RESP_AUTOPROBE_WAIT:
    case RESP_AUTOPROBE_INITIALIZE: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Autoprobe succeeded at" << Logging::time(frameTime);

            responseState = RESP_RUN;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            timeoutAt(frameTime + config.first().reportInterval * 2.0);

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("AutoprobeWait");
            event(frameTime, QObject::tr("Autoprobe succeeded."), false, info);
        } else {
            qCDebug(log) << "Autoprobe failed at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            responseState = RESP_WAIT;
            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_WAIT:
    case RESP_INITIALIZE: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Comms established at" << Logging::time(frameTime);

            responseState = RESP_RUN;
            generalStatusUpdated();

            timeoutAt(frameTime + config.first().reportInterval * 2.0);

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("Wait");
            event(frameTime, QObject::tr("Communications established."), false, info);
        } else if (code > 0) {
            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_RUN: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + config.first().reportInterval * 2.0);
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            responseState = RESP_WAIT;
            discardData(frameTime + 0.5, 1);
            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            info.hash("ResponseState").setString("Run");
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
        }
        break;
    }

    default:
        break;
    }

    if (!FP::defined(frameTime))
        frameTime = lastHRecordTime;
    else
        lastHRecordTime = frameTime;
    if (model)
        processModelPending(frameTime);
}

void AcquireDMTCCN::incomingInstrumentTimeout(double frameTime)
{
    if (!FP::defined(frameTime))
        return;

    switch (responseState) {
    case RESP_RUN: {
        qCDebug(log) << "Timeout in unpolled mode at" << Logging::time(frameTime);

        if (controlStream != NULL)
            controlStream->resetControl();
        responseState = RESP_WAIT;
        discardData(frameTime + 2.0, 1);
        generalStatusUpdated();

        Variant::Write info = Variant::Write::empty();
        info.hash("ResponseState").setString("Run");
        event(frameTime, QObject::tr("Timeout waiting for report.  Communications dropped."), true,
              info);

        invalidateLogValues(frameTime);
        break;
    }

    case RESP_AUTOPROBE_WAIT:
        qCDebug(log) << "Timeout waiting for autoprobe records at" << Logging::time(frameTime);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        if (controlStream != NULL)
            controlStream->resetControl();
        discardData(frameTime + 2.0, 1);
        responseState = RESP_WAIT;

        invalidateLogValues(frameTime);
        break;

    case RESP_INITIALIZE:
        discardData(frameTime + 0.5, 1);
        timeoutAt(frameTime + config.first().reportInterval * 3.0 + 1.0);
        responseState = RESP_WAIT;
        break;

    case RESP_AUTOPROBE_INITIALIZE:
        discardData(frameTime + 0.5, 1);
        timeoutAt(frameTime + config.first().reportInterval * 3.0 + 1.0);
        responseState = RESP_AUTOPROBE_WAIT;
        break;

    default:
        discardData(frameTime + 0.5, 1);
        invalidateLogValues(frameTime);
        break;
    }
}

void AcquireDMTCCN::discardDataCompleted(double frameTime)
{
    Q_UNUSED(frameTime);
}

void AcquireDMTCCN::shutdownInProgress(double frameTime)
{
    emptyModelProcessing(frameTime);

    loggingMux.finish(LogStream_Model, loggingEgress);
    loggingMux.finish(LogStream_Processing, loggingEgress);
    processingData.clear();
    modelData.clear();
}


void AcquireDMTCCN::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_UNUSED(frameTime);
    Q_UNUSED(frame);
}


Variant::Root AcquireDMTCCN::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireDMTCCN::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_RUN:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

void AcquireDMTCCN::command(const Variant::Read &command)
{
    if (command.hash("Setpoint").exists()) {
        double sp = Variant::Composite::toNumber(
                command.hash("Setpoint").hash("Parameters").hash("Value").hash("Value"));
        if (!FP::defined(sp)) {
            sp = Variant::Composite::toNumber(
                    command.hash("Setpoint").hash("Parameters").hash("Setpoint"));
        }
        if (!FP::defined(sp)) {
            sp = Variant::Composite::toNumber(command.hash("Setpoint").hash("Value"));
        }
        if (!FP::defined(sp)) {
            sp = Variant::Composite::toNumber(command.hash("Setpoint").hash("Setpoint"));
        }
        if (!FP::defined(sp)) {
            sp = Variant::Composite::toNumber(command.hash("Setpoint"));
        }

        if (FP::defined(sp) && sp >= 0.0 && sp <= 99.99) {
            if (controlStream != NULL) {
                controlStream->writeControl(QByteArray::number(sp, 'f', 2).rightJustified(5, '0'));
            }
        }
    }
}

Variant::Root AcquireDMTCCN::getCommands()
{
    Variant::Root result;

    result["Setpoint"].hash("DisplayName").setString("Change d&T Setpoint");
    result["Setpoint"].hash("ToolTip").setString("Change the T3-T1 temperature setpoint.");
    result["Setpoint"].hash("Parameters").hash("Value").hash("Variable").setString("DT");
    result["Setpoint"].hash("Parameters").hash("Value").hash("Name").setString("T3-T1 setpoint");
    result["Setpoint"].hash("Parameters").hash("Value").hash("Format").setString("00.00");
    result["Setpoint"].hash("Parameters").hash("Value").hash("Minimum").setDouble(0.0);
    result["Setpoint"].hash("Parameters").hash("Value").hash("Maximum").setDouble(99.99);
    result["Setpoint"].hash("Parameters").hash("Value").hash("Value").setDouble(0.0);

    return result;
}

AcquisitionInterface::AutoprobeStatus AcquireDMTCCN::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireDMTCCN::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_RUN:
        /* Already running, just notify that we succeeded. */
        qCDebug(log) << "Interactive autoprobe requested with communications established at"
                     << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    default:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        timeoutAt(time + config.first().reportInterval * 3.0 + 1.0);
        discardData(time + 0.5, 1);
        responseState = RESP_AUTOPROBE_WAIT;
        generalStatusUpdated();
        break;
    }
}

void AcquireDMTCCN::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    timeoutAt(time + config.first().reportInterval * 3.0 + 1.0);
    discardData(time + 0.5, 1);
    responseState = RESP_AUTOPROBE_WAIT;
    generalStatusUpdated();
}


void AcquireDMTCCN::autoprobePromote(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + config.first().reportInterval * 2.0);

    switch (responseState) {
    case RESP_RUN:
    case RESP_WAIT:
    case RESP_INITIALIZE:
        qCDebug(log) << "Promoted from state" << responseState << "to interactive acquisition at"
                     << Logging::time(time);
        break;

    case RESP_AUTOPROBE_WAIT:
    case RESP_AUTOPROBE_INITIALIZE:
        qCDebug(log) << "Promoted from passive autoprobe to interactive acquisition at"
                     << Logging::time(time);

        responseState = RESP_WAIT;
        break;
    }
}

void AcquireDMTCCN::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + config.first().reportInterval * 2.0);

    switch (responseState) {
    case RESP_RUN:
    case RESP_WAIT:
    case RESP_INITIALIZE:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);
        break;

    case RESP_AUTOPROBE_WAIT:
    case RESP_AUTOPROBE_INITIALIZE:
        qCDebug(log) << "Promoted from passive autoprobe to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_WAIT;
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireDMTCCN::getDefaults()
{
    AutomaticDefaults result;
    result.name = "N$1$2";
    result.setSerialN81(9600);
    return result;
}


ComponentOptions AcquireDMTCCNComponent::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options, true);

    options.add("basetime",
                new ComponentOptionSingleTime(tr("basetime", "name"), tr("File time origin"),
                                              tr("When dealing with files without a full timestamp on each "
                                                 "record, this time is used to reconstruct the full time from "
                                                 "the time of day on the H records."), ""));

    return options;
}

ComponentOptions AcquireDMTCCNComponent::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    ComponentOptionSingleInteger *i = new ComponentOptionSingleInteger(tr("model-backlog", "name"),
                                                                       tr("Maximum model backlog"),
                                                                       tr("This is the maximum number of model processing requests allowed "
                                                                          "before they start being dropped."),
                                                                       tr("None",
                                                                          "default backlog"));
    i->setMinimum(1);
    i->setAllowUndefined(true);
    options.add("model-backlog", i);

    return options;
}

QList<ComponentExample> AcquireDMTCCNComponent::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples());
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireDMTCCNComponent::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireDMTCCNComponent::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options, true);
}

QList<ComponentExample> AcquireDMTCCNComponent::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireDMTCCNComponent::createAcquisitionPassive(const ComponentOptions &options,
                                                                               const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireDMTCCN(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireDMTCCNComponent::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                               const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireDMTCCN(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireDMTCCNComponent::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                 const std::string &loggingContext)
{
    std::unique_ptr<AcquireDMTCCN> i(new AcquireDMTCCN(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<
        AcquisitionInterface> AcquireDMTCCNComponent::createAcquisitionInteractive(const ValueSegment::Transfer &config,
                                                                                   const std::string &loggingContext)
{
    std::unique_ptr<AcquireDMTCCN> i(new AcquireDMTCCN(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
