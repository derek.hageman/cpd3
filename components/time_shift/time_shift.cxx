/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/component.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "core/environment.hxx"

#include "time_shift.hxx"

using namespace CPD3;
using namespace CPD3::Data;


TimeShift::TimeShift(const ComponentOptions &options) : priorStart(FP::undefined()),
                                                        interval(NULL),
                                                        fromLocalTime(NULL)
{
    if (options.isSet("shift")) {
        interval = qobject_cast<TimeIntervalSelectionOption *>(options.get("shift"))->getInterval();
    } else {
        interval = new DynamicTimeInterval::None;
    }

    if (options.isSet("local")) {
        fromLocalTime = qobject_cast<DynamicBoolOption *>(options.get("local"))->getInput();
    } else {
        fromLocalTime = new DynamicPrimitive<bool>::Constant(false);
    }
}

TimeShift::TimeShift(double start,
                     double end,
                     const SequenceName::Component &,
                     const SequenceName::Component &,
                     const ValueSegment::Transfer &config) : priorStart(FP::undefined()),
                                                             interval(NULL),
                                                             fromLocalTime(NULL)
{
    DynamicTimeInterval::Variable *defaultTime = new DynamicTimeInterval::Variable;
    defaultTime->set(Time::Second, 0, false);
    interval =
            DynamicTimeInterval::fromConfiguration(config, "Shift", start, end, true, defaultTime);
    delete defaultTime;

    fromLocalTime = DynamicBoolOption::fromConfiguration(config, "Local", start, end);
}

TimeShift::~TimeShift()
{
    delete interval;
    delete fromLocalTime;
}

TimeShift::TimeShift(QDataStream &stream) : AsyncProcessingStage(stream)
{
    stream >> priorStart;
    stream >> interval;
    stream >> fromLocalTime;
}

void TimeShift::serialize(QDataStream &stream)
{
    AsyncProcessingStage::serialize(stream);
    stream << priorStart;
    stream << interval;
    stream << fromLocalTime;
}

double TimeShift::apply(double origin, double time, bool roundUp)
{
    if (!fromLocalTime->get(origin))
        return interval->apply(origin, time, roundUp);

    QDateTime dt = QDateTime::fromMSecsSinceEpoch(static_cast<qint64>(std::round(time * 1000.0)));

    /* The current time is actually "local" now, so we need to calculate the
     * offset in reverse. */
    dt = dt.toLocalTime();
    if (dt.toUTC().time() == dt.time())
        return time;

    int deltaTZ = dt.toUTC().time().secsTo(dt.time());
    if (deltaTZ > 86400 / 2)
        deltaTZ -= 86400;
    else if (deltaTZ < -86400 / 2)
        deltaTZ += 86400;

    if (deltaTZ >= 86400)
        deltaTZ = 86400 - 1;
    else if (deltaTZ <= -86400)
        deltaTZ = -(86400 - 1);

    return time - static_cast<double>(deltaTZ);
}

void TimeShift::process(SequenceValue::Transfer &&incoming)
{
    SequenceValue::Transfer result;
    for (auto &add : incoming) {
        double oldStart = add.getStart();
        if (FP::defined(oldStart)) {
            add.setStart(apply(oldStart, oldStart, false));
            /* Don't corrupt the data stream */
            if (FP::defined(priorStart) && add.getStart() < priorStart)
                continue;

            if (FP::defined(add.getEnd())) {
                add.setEnd(apply(oldStart, add.getEnd(), true));

                /* Screwy aligning made this a non-segment, so drop it */
                if (Range::compareStartEnd(add.getStart(), add.getEnd()) >= 0)
                    continue;
            }
        } else if (FP::defined(add.getEnd())) {
            add.setEnd(apply(oldStart, add.getEnd(), true));
        }

        if (add.getUnit().isMeta() &&
                add.read().isMetadata() &&
                add.read().metadata("Processing").exists()) {
            auto meta = add.write().metadata("Processing").toArray().after_back();
            meta["By"].setString("time_shift");
            meta["At"].setDouble(Time::time());
            meta["Environment"].setString(Environment::describe());
            meta["Revision"].setString(Environment::revision());
            if (fromLocalTime->get(oldStart)) {
                QDateTime dt(Time::toDateTime(oldStart).toLocalTime());
                meta["Parameters"].hash("FromLocal").setString(dt.toString("t"));
            } else {
                meta["Parameters"].hash("Offset").setString(interval->describe(oldStart));
            }
        }

        priorStart = add.getStart();
        result.emplace_back(std::move(add));
    }
    egress->incomingData(std::move(result));
}


QString TimeShiftComponent::getGeneralSerializationName() const
{ return QString::fromLatin1("time_shift"); }

ComponentOptions TimeShiftComponent::getOptions()
{
    ComponentOptions options;

    TimeIntervalSelectionOption *shift = new TimeIntervalSelectionOption(tr("shift", "name"),
                                                                         tr("The interval to shift data by"),
                                                                         tr("This if the offset to shift data by.  A positive offset shifts "
                                                                            "data forwards in time.  Alignment (if specified) causes the start "
                                                                            "to be rounded down and the end to be rounded up for all data "
                                                                            "values.  Invalid values (zero or data moved before the preceding "
                                                                            "values) are removed from the data stream."),
                                                                         tr("None",
                                                                            "default shift"), 1);
    shift->setAllowZero(true);
    shift->setAllowNegative(true);
    options.add("shift", shift);

    options.add("local", new DynamicBoolOption(tr("local", "name"), tr("Change from local time"),
                                               tr("When enabled this causes data to be shifted as if the time stamps "
                                                  "are in local time.  This can be used to correct data that was "
                                                  "mistakenly logged in local time.  The computer time zone must "
                                                  "match the logging time zone in this case.  On Unix this can "
                                                  "often be accomplished by setting the 'TZ' environment variable."),
                                               QString()));

    return options;
}

ProcessingStage *TimeShiftComponent::createGeneralFilterDynamic(const ComponentOptions &options)
{ return new TimeShift(options); }

ProcessingStage *TimeShiftComponent::createGeneralFilterEditing(double start,
                                                                double end,
                                                                const SequenceName::Component &station,
                                                                const SequenceName::Component &archive,
                                                                const ValueSegment::Transfer &config)
{ return new TimeShift(start, end, station, archive, config); }

ProcessingStage *TimeShiftComponent::deserializeGeneralFilter(QDataStream &stream)
{ return new TimeShift(stream); }