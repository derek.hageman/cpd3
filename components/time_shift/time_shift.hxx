/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef TIMESHIFT_H
#define TIMESHIFT_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "datacore/dynamicprimitive.hxx"

class TimeShift : public CPD3::Data::AsyncProcessingStage {
    double priorStart;
    CPD3::Data::DynamicTimeInterval *interval;
    CPD3::Data::DynamicBool *fromLocalTime;

    double apply(double origin, double time, bool roundUp);

public:
    TimeShift(const CPD3::ComponentOptions &options);

    TimeShift(double start,
              double end,
              const CPD3::Data::SequenceName::Component &station,
              const CPD3::Data::SequenceName::Component &archive,
              const CPD3::Data::ValueSegment::Transfer &config);

    TimeShift(QDataStream &stream);

    ~TimeShift();

    virtual void serialize(QDataStream &stream);

protected:
    virtual void process(CPD3::Data::SequenceValue::Transfer &&incoming);
};

class TimeShiftComponent : public QObject, public CPD3::Data::ProcessingStageComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ProcessingStageComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.time_shift"
                              FILE
                              "time_shift.json")

public:
    virtual QString getGeneralSerializationName() const;

    virtual CPD3::ComponentOptions getOptions();

    virtual CPD3::Data::ProcessingStage *createGeneralFilterDynamic
            (const CPD3::ComponentOptions &options = CPD3::ComponentOptions());

    virtual CPD3::Data::ProcessingStage *createGeneralFilterEditing(double start,
                                                                    double end,
                                                                    const CPD3::Data::SequenceName::Component &station,
                                                                    const CPD3::Data::SequenceName::Component &archive,
                                                                    const CPD3::Data::ValueSegment::Transfer &config);

    virtual CPD3::Data::ProcessingStage *deserializeGeneralFilter(QDataStream &stream);

};

#endif
