/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/processingstage.hxx"
#include "core/component.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "datacore/stream.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace CPD3 {
namespace Data {
bool operator==(const SequenceValue &a, const SequenceValue &b)
{ return SequenceValue::ValueEqual()(a, b); }
}
}

class TestComponent : public QObject {
Q_OBJECT

    ProcessingStageComponent *component;

private slots:

    void initTestCase()
    {
        component = qobject_cast<ProcessingStageComponent *>(ComponentLoader::create("time_shift"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<TimeIntervalSelectionOption *>(options.get("shift")));
        QVERIFY(qobject_cast<DynamicBoolOption *>(options.get("local")));
    }

    void applyShift()
    {
        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<TimeIntervalSelectionOption *>(options.get("shift"))->set(Time::Second, 10);

        ProcessingStage *filter = component->createGeneralFilterDynamic(options);
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName u1("brw", "raw", "T_S11");
        filter->incomingData(SequenceValue(u1, Variant::Root(1.0), 1.0, 2.0));
        filter->incomingData(SequenceValue(u1, Variant::Root(2.0), 2.0, 3.0));
        filter->incomingData(SequenceValue(u1, Variant::Root(3.0), 3.0, 4.0));
        filter->endData();

        QVERIFY(filter->wait());
        delete filter;

        QCOMPARE((int) e.values().size(), 3);
        QCOMPARE(e.values()[0], SequenceValue(u1, Variant::Root(1.0), 11.0, 12.0));
        QCOMPARE(e.values()[1], SequenceValue(u1, Variant::Root(2.0), 12.0, 13.0));
        QCOMPARE(e.values()[2], SequenceValue(u1, Variant::Root(3.0), 13.0, 14.0));
    }

    void removeInvalid()
    {
        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<TimeIntervalSelectionOption *>(options.get("shift"))->set(FP::undefined(), 2.0,
                                                                               Time::Second, 10);
        qobject_cast<TimeIntervalSelectionOption *>(options.get("shift"))->set(2.0, FP::undefined(),
                                                                               Time::Second, 8);

        ProcessingStage *filter = component->createGeneralFilterDynamic(options);
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName u1("brw", "raw", "T_S11");
        filter->incomingData(SequenceValue(u1, Variant::Root(1.0), 1.0, 2.0));
        filter->incomingData(SequenceValue(u1, Variant::Root(2.0), 2.0, 3.0));
        filter->incomingData(SequenceValue(u1, Variant::Root(3.0), 3.0, 4.0));
        filter->endData();

        QVERIFY(filter->wait());
        delete filter;

        QCOMPARE((int) e.values().size(), 2);
        QCOMPARE(e.values()[0], SequenceValue(u1, Variant::Root(1.0), 11.0, 12.0));
        QCOMPARE(e.values()[1], SequenceValue(u1, Variant::Root(3.0), 11.0, 12.0));
    }

    void serialize()
    {
        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<TimeIntervalSelectionOption *>(options.get("shift"))->set(Time::Second, 10);

        ProcessingStage *filter = component->createGeneralFilterDynamic(options);
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName u1("brw", "raw", "T_S11");
        for (int i = 0; i < 10000; i++) {
            filter->incomingData(SequenceValue(u1, Variant::Root(1.0 + i), 1.0 + i, 2.0 + i));
        }
        filter->setEgress(NULL);

        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            filter->serialize(stream);
        }
        filter->signalTerminate();
        QVERIFY(filter->wait());
        delete filter;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            filter = component->deserializeGeneralFilter(stream);
            QVERIFY(stream.atEnd());
        }
        QVERIFY(filter != NULL);
        filter->start();
        filter->setEgress(&e);

        for (int i = 10000; i < 20000; i++) {
            filter->incomingData(SequenceValue(u1, Variant::Root(1.0 + i), 1.0 + i, 2.0 + i));
        }
        filter->endData();

        QVERIFY(filter->wait());
        delete filter;

        QCOMPARE((int) e.values().size(), 20000);
        for (int i = 10000; i < 20000; i++) {
            QCOMPARE(e.values()[i], SequenceValue(u1, Variant::Root(1.0 + i), 11.0 + i, 12.0 + i));
        }
    }

#ifdef Q_OS_UNIX

    void localTime()
    {
        QVERIFY(qputenv("TZ", QByteArray("America/Denver")));

        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<DynamicBoolOption *>(options.get("local"))->set(true);

        ProcessingStage *filter = component->createGeneralFilterDynamic(options);
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName u1("brw", "raw", "T_S11");
        filter->incomingData(SequenceValue(u1, Variant::Root(1.0), 1422784800.0, 1422784801.0));
        filter->incomingData(SequenceValue(u1, Variant::Root(2.0), 1425895200.0, 1425895201.0));
        filter->incomingData(SequenceValue(u1, Variant::Root(3.0), 1425931200.0, 1425931201.0));
        filter->endData();

        QVERIFY(filter->wait());
        delete filter;

        QCOMPARE((int) e.values().size(), 3);
        QCOMPARE(e.values()[0], SequenceValue(u1, Variant::Root(1.0), 1422810000.0, 1422810001.0));
        QCOMPARE(e.values()[1], SequenceValue(u1, Variant::Root(2.0), 1425916800.0, 1425916801.0));
        QCOMPARE(e.values()[2], SequenceValue(u1, Variant::Root(3.0), 1425952800.0, 1425952801.0));
    }

#endif
};

QTEST_MAIN(TestComponent)

#include "test.moc"
