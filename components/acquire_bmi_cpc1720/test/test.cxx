/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    double unpolledRemaining;
    QDateTime currentTime;
    bool loggingMode;

    double tau;
    double counts;
    double sampleFlow;
    double saturatorFlow;

    double Toptics;
    double Poptics;
    double Tcondenser;
    double Pcondenser;
    double Tsattop;
    double Psattop;
    double Tsatbot;
    double Psatbot;
    double Psatflow;
    double Tinlet;
    double pressure;
    int fillCount;
    quint16 errorBits;

    double calSampleFlow[2];
    double calSaturatorFlow[2];
    double calPressure[2];

    double rawConcentration() const
    {
        return counts / (sampleFlow / 60.0);
    }

    double correctedConcentration() const
    {
        return (counts * exp(counts * tau)) / (sampleFlow / 60.0);
    }

    double sampleFlowLPM() const
    {
        return sampleFlow / 1000.0;
    }

    double saturatorFlowLPM() const
    {
        return saturatorFlow / 1000.0;
    }

    double opticsPower() const
    {
        return (Poptics / 200.0) * 100.0;
    }

    double condenserPower() const
    {
        return (Pcondenser / 250.0) * 100.0;
    }

    double saturatorTopPower() const
    {
        return (Psattop / 200.0) * 100.0;
    }

    double saturatorBottomPower() const
    {
        return (Psatbot / 200.0) * 100.0;
    }

    double saturatorFlowPower() const
    {
        return (Psatflow / 200.0) * 100.0;
    }

    double rawSampleFlow() const
    {
        double V = (sampleFlow + calSampleFlow[0]) / calSampleFlow[1];
        return floor(V + 0.5);
    }

    double rawSaturatorFlow() const
    {
        double V = (saturatorFlow + calSaturatorFlow[0]) / calSaturatorFlow[1];
        return floor(V + 0.5);
    }

    double rawPressure() const
    {
        double V = (pressure + calPressure[0]) / calPressure[1];
        return floor(V + 0.5);
    }

    void readCommand()
    {
        outgoing.append("concent=");
        outgoing.append(QByteArray::number(correctedConcentration(), 'f', 1));
        outgoing.append("\rrawconc=");
        outgoing.append(QByteArray::number(rawConcentration(), 'f', 0));
        outgoing.append("\rcnt_sec=");
        outgoing.append(QByteArray::number(counts, 'f', 0));
        outgoing.append("\rcondtmp=");
        outgoing.append(QByteArray::number(Tcondenser, 'f', 1));
        outgoing.append("\rsatttmp=");
        outgoing.append(QByteArray::number(Tsattop, 'f', 1));
        outgoing.append("\rsatbtmp=");
        outgoing.append(QByteArray::number(Tsatbot, 'f', 1));
        outgoing.append("\roptctmp=");
        outgoing.append(QByteArray::number(Toptics, 'f', 1));
        outgoing.append("\rinlttmp=");
        outgoing.append(QByteArray::number(Tinlet, 'f', 1));
        outgoing.append("\rsmpflow=");
        outgoing.append(QByteArray::number(sampleFlow, 'f', 0));
        outgoing.append("\rsatflow=");
        outgoing.append(QByteArray::number(saturatorFlow, 'f', 0));
        outgoing.append("\rpressur=");
        outgoing.append(QByteArray::number(pressure, 'f', 0));
        outgoing.append("\rcondpwr=");
        outgoing.append(QByteArray::number(Pcondenser, 'f', 0));
        outgoing.append("\rsattpwr=");
        outgoing.append(QByteArray::number(Psattop, 'f', 0));
        outgoing.append("\rsatbpwr=");
        outgoing.append(QByteArray::number(Psatbot, 'f', 0));
        outgoing.append("\roptcpwr=");
        outgoing.append(QByteArray::number(Poptics, 'f', 0));
        outgoing.append("\rsatfpwr=");
        outgoing.append(QByteArray::number(Psatflow, 'f', 0));
        outgoing.append("\rfillcnt=");
        outgoing.append(QByteArray::number(fillCount));
        outgoing.append("\rerr_num=");
        outgoing.append(QByteArray::number(errorBits));
        outgoing.append("\r\r");
    }

    void settingsCommand()
    {
        outgoing.append("autorpt=");
        outgoing.append(FP::defined(unpolledRemaining) ? '1' : '0');
        outgoing.append("\rrpt_lbl=1\r"
                                "sd_stat=0\r"
                                "sd_save=0\r"
                                "sd_intv=60\r"
                                "sd_file=N/A\r"
                                "sd_size=N/A\r"
                                "sd_used=N/A\r\r");
    }

    void clockCommand()
    {
        outgoing.append("clkhour=");
        outgoing.append(QByteArray::number(currentTime.time().hour()));
        outgoing.append("\rclk_min=");
        outgoing.append(QByteArray::number(currentTime.time().minute()));
        outgoing.append("\rclk_sec=");
        outgoing.append(QByteArray::number(currentTime.time().second()));
        outgoing.append("\rclkyear=");
        outgoing.append(QByteArray::number(currentTime.date().year() % 100));
        outgoing.append("\rclk_mon=");
        outgoing.append(QByteArray::number(currentTime.date().month()));
        outgoing.append("\rclk_day=");
        outgoing.append(QByteArray::number(currentTime.date().day()));
        outgoing.append("\r");
    }

    void calibrationCommand()
    {
        outgoing.append("smpslop=");
        outgoing.append(QByteArray::number(calSampleFlow[1], 'f', 0));
        outgoing.append("\rsmpoffs=");
        outgoing.append(QByteArray::number(calSampleFlow[0], 'f', 0));
        outgoing.append("\rsatslop=");
        outgoing.append(QByteArray::number(calSaturatorFlow[1], 'f', 0));
        outgoing.append("\rsatoffs=");
        outgoing.append(QByteArray::number(calSaturatorFlow[0], 'f', 0));
        outgoing.append("\r"
                                "cal_tmp=22.8\r"
                                "coincid=3250\r"
                                "cs_diff=25.0");
        outgoing.append("\rprsslop=");
        outgoing.append(QByteArray::number(calPressure[1], 'f', 0));
        outgoing.append("\rprsoffs=");
        outgoing.append(QByteArray::number(calPressure[0], 'f', 0));
        outgoing.append("\r"
                                "anlgcal=126\r\r");
    }

    void rawCommand()
    {
        outgoing.append("smp_raw=");
        outgoing.append(QByteArray::number(rawSampleFlow(), 'f', 0));
        outgoing.append("\rsat_raw=");
        outgoing.append(QByteArray::number(rawSaturatorFlow(), 'f', 0));
        outgoing.append("\rprs_raw=");
        outgoing.append(QByteArray::number(rawPressure(), 'f', 0));
        outgoing.append("\r\r");
    }

    void manufacturingCommand()
    {
        outgoing.append("ser_num=28    \r"
                                "mfgyear=15 \r"
                                "mfg_mon=7  \r"
                                "mfg_day=16 \r"
                                "firmwar=4.1 \r\r");
    }

    void logHeader()
    {
        outgoing.append("#firmware=4.1\r"
                                "#mfgdate=15-07-16\r");
        outgoing.append("#smpslop=");
        outgoing.append(QByteArray::number(calSampleFlow[1], 'f', 0));
        outgoing.append("\r#smpoffs=");
        outgoing.append(QByteArray::number(calSampleFlow[0], 'f', 0));
        outgoing.append("\r#satslop=");
        outgoing.append(QByteArray::number(calSaturatorFlow[1], 'f', 0));
        outgoing.append("\r#satoffs=");
        outgoing.append(QByteArray::number(calSaturatorFlow[0], 'f', 0));
        outgoing.append("\r"
                                "#cal_tmp=22.8\r"
                                "#coincid=3250\r"
                                "#cs_diff=25.0");
        outgoing.append("\r#prsslop=");
        outgoing.append(QByteArray::number(calPressure[1], 'f', 0));
        outgoing.append("\r#prsoffs=");
        outgoing.append(QByteArray::number(calPressure[0], 'f', 0));
        outgoing.append("\r"
                                "#anlgcal=126\r\r");
    }

    void logLine()
    {
        outgoing.append(QByteArray::number(currentTime.date().year() % 100));
        outgoing.append("/");
        outgoing.append(QByteArray::number(currentTime.date().month()));
        outgoing.append(":");
        outgoing.append(QByteArray::number(currentTime.date().day()));
        outgoing.append("\t");

        outgoing.append(QByteArray::number(currentTime.time().hour()));
        outgoing.append("\t");
        outgoing.append(QByteArray::number(currentTime.time().minute()));
        outgoing.append("\t");
        outgoing.append(QByteArray::number(currentTime.time().second()));
        outgoing.append("\t");

        outgoing.append(QByteArray::number(correctedConcentration(), 'f', 1));
        outgoing.append("\t");
        outgoing.append(QByteArray::number(correctedConcentration(), 'f', 1));
        outgoing.append("\t");
        outgoing.append(QByteArray::number(rawConcentration(), 'f', 1));
        outgoing.append("\t");
        outgoing.append(QByteArray::number(counts, 'f', 0));
        outgoing.append("\t");
        outgoing.append(QByteArray::number(Tcondenser, 'f', 1));
        outgoing.append("\t");
        outgoing.append(QByteArray::number(Tsattop, 'f', 1));
        outgoing.append("\t");
        outgoing.append(QByteArray::number(Tsatbot, 'f', 1));
        outgoing.append("\t");
        outgoing.append(QByteArray::number(Toptics, 'f', 1));
        outgoing.append("\t");
        outgoing.append(QByteArray::number(Tinlet, 'f', 1));
        outgoing.append("\t");
        outgoing.append(QByteArray::number(sampleFlow, 'f', 0));
        outgoing.append("\t");
        outgoing.append(QByteArray::number(saturatorFlow, 'f', 0));
        outgoing.append("\t");
        outgoing.append(QByteArray::number(pressure, 'f', 0));
        outgoing.append("\t");
        outgoing.append(QByteArray::number(Pcondenser, 'f', 0));
        outgoing.append("\t");
        outgoing.append(QByteArray::number(Psattop, 'f', 0));
        outgoing.append("\t");
        outgoing.append(QByteArray::number(Psatbot, 'f', 0));
        outgoing.append("\t");
        outgoing.append(QByteArray::number(Poptics, 'f', 0));
        outgoing.append("\t");
        outgoing.append(QByteArray::number(Psatflow, 'f', 0));
        outgoing.append("\t");
        outgoing.append(QByteArray::number(fillCount));
        outgoing.append("\t");
        outgoing.append(QByteArray::number(errorBits));

        outgoing.append("\n");
    }

    ModelInstrument()
            : incoming(),
              outgoing(),
              unpolledRemaining(FP::undefined()),
              currentTime(QDate(2015, 9, 5), QTime(10, 15, 14), Qt::UTC),
              loggingMode(false)
    {
        counts = 6000;
        sampleFlow = 300;
        saturatorFlow = 310;
        tau = 0.5E-6;

        Toptics = 23.1;
        Poptics = 20;
        Tcondenser = 15.0;
        Pcondenser = 10;
        Tsattop = 21.0;
        Psattop = 30;
        Tsatbot = 20.5;
        Psatbot = 29;
        Psatflow = 150;
        Tinlet = 10.0;
        fillCount = 0;
        pressure = 850;
        errorBits = 0;

        calSampleFlow[0] = 425;
        calSampleFlow[1] = 237;
        calSaturatorFlow[0] = 394;
        calSaturatorFlow[1] = 232;
        calPressure[0] = 138;
        calPressure[1] = 319;
    }

    void advance(double seconds)
    {

        int idxCR = -1;
        while ((idxCR = incoming.indexOf('\r')) >= 0) {
            QByteArray line(incoming.mid(0, idxCR));

            if (line == "read") {
                readCommand();
            } else if (line == "settings") {
                settingsCommand();
            } else if (line == "rtclck") {
                clockCommand();
            } else if (line == "calib") {
                calibrationCommand();
            } else if (line == "raw=2") {
                rawCommand();
            } else if (line == "mfginfo") {
                manufacturingCommand();
            } else if (line == "store") {
            } else if (line == "autorpt=0") {
                unpolledRemaining = FP::undefined();
            } else if (line == "autorpt=1") {
                unpolledRemaining = 0.5;
            } else if (line.startsWith("clkhour=")) {
                currentTime.setTime(QTime(line.mid(8).toInt(NULL, 16), currentTime.time().minute(),
                                          currentTime.time().second()));
            } else if (line.startsWith("clk_min=")) {
                currentTime.setTime(QTime(currentTime.time().hour(), line.mid(8).toInt(NULL, 16),
                                          currentTime.time().second()));
            } else if (line.startsWith("clk_sec=")) {
                currentTime.setTime(QTime(currentTime.time().hour(), currentTime.time().minute(),
                                          line.mid(8).toInt(NULL, 16)));
            } else if (line.startsWith("clkyear=")) {
                currentTime.setDate(
                        QDate(line.mid(8).toInt(NULL, 16) + 2000, currentTime.date().month(),
                              currentTime.date().day()));
            } else if (line.startsWith("clk_mon=")) {
                currentTime.setDate(QDate(currentTime.date().year(), line.mid(8).toInt(NULL, 16),
                                          currentTime.date().day()));
            } else if (line.startsWith("clk_day=")) {
                currentTime.setDate(QDate(currentTime.date().year(), currentTime.date().month(),
                                          line.mid(8).toInt(NULL, 16)));
            } else {
                outgoing.append("ERR\r");
            }
            if (idxCR >= incoming.size() - 1) {
                incoming.clear();
                break;
            }
            incoming = incoming.mid(idxCR + 1);
        }

        if (FP::defined(unpolledRemaining)) {
            unpolledRemaining -= seconds;
            currentTime = currentTime.addMSecs((qint64) floor(seconds * 1000.0 + 0.5));
            while (unpolledRemaining <= 0.0) {
                if (!loggingMode) {
                    unpolledRemaining += 0.5;
                    readCommand();
                } else {
                    unpolledRemaining += 1.0;
                    readCommand();
                }
            }
        }
    }
};

class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("N", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Tu", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T3", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T4", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Q1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Q2", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("C", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("PCT1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("PCT2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("PCT3", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("PCT4", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("PCT5", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkContiguous(StreamCapture &stream)
    {
        if (!stream.checkContiguous("N"))
            return false;
        if (!stream.checkContiguous("Tu"))
            return false;
        if (!stream.checkContiguous("T1"))
            return false;
        if (!stream.checkContiguous("T2"))
            return false;
        if (!stream.checkContiguous("T3"))
            return false;
        if (!stream.checkContiguous("T4"))
            return false;
        if (!stream.checkContiguous("Q1"))
            return false;
        if (!stream.checkContiguous("Q2"))
            return false;
        if (!stream.checkContiguous("P"))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream, const ModelInstrument &model,
                     double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("N", Variant::Root(model.correctedConcentration()), time,
                                        QString(),
                                        1E-4))
            return false;
        if (!stream.hasAnyMatchingValue("Tu", Variant::Root(model.Tinlet), time))
            return false;
        if (!stream.hasAnyMatchingValue("T1", Variant::Root(model.Tsatbot), time))
            return false;
        if (!stream.hasAnyMatchingValue("T2", Variant::Root(model.Tsattop), time))
            return false;
        if (!stream.hasAnyMatchingValue("T3", Variant::Root(model.Tcondenser), time))
            return false;
        if (!stream.hasAnyMatchingValue("T4", Variant::Root(model.Toptics), time))
            return false;
        if (!stream.hasAnyMatchingValue("Q1", Variant::Root(model.sampleFlowLPM()), time, QString(),
                                        1E-4))
            return false;
        if (!stream.hasAnyMatchingValue("Q2", Variant::Root(model.saturatorFlowLPM()), time,
                                        QString(),
                                        1E-4))
            return false;
        if (!stream.hasAnyMatchingValue("P", Variant::Root(model.pressure), time))
            return false;
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream, const ModelInstrument &model,
                             bool includeRaw = true,
                             double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("C", Variant::Root(model.counts), time))
            return false;
        if (includeRaw) {
            if (!stream.hasAnyMatchingValue("ZN", Variant::Root(model.rawConcentration()), time))
                return false;
            if (!stream.hasAnyMatchingValue("ZQ1", Variant::Root(model.rawSampleFlow()), time))
                return false;
            if (!stream.hasAnyMatchingValue("ZQ2", Variant::Root(model.rawSaturatorFlow()), time))
                return false;
        }
        if (!stream.hasAnyMatchingValue("PCT1", Variant::Root(model.saturatorBottomPower()), time))
            return false;
        if (!stream.hasAnyMatchingValue("PCT2", Variant::Root(model.saturatorTopPower()), time))
            return false;
        if (!stream.hasAnyMatchingValue("PCT3", Variant::Root(model.condenserPower()), time))
            return false;
        if (!stream.hasAnyMatchingValue("PCT4", Variant::Root(model.opticsPower()), time))
            return false;
        if (!stream.hasAnyMatchingValue("PCT5", Variant::Root(model.saturatorFlowPower()), time))
            return false;
        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_bmi_cpc1720"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_bmi_cpc1720"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = 0.1;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 50; i++) {
            control.advance(0.2);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(0.2);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument, false));
    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = FP::undefined();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 20; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(logging.hasAnyMatchingValue("F1",
                                            Variant::Root(Variant::Flags{"CoincidenceCorrected"})));
        QVERIFY(logging.hasMeta("F1", Variant::Root(28), "^Source/SerialNumber"));
        QVERIFY(persistent.hasAnyMatchingValue("ZPARAMETERS", Variant::Root(
                instrument.calSampleFlow[0] / (1000.0)),
                                               FP::undefined(), "SampleFlowOffset"));
    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["CalculateConcentration"].setBool(true);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = 0.1;

        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.5);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        control.advance(0.1);

        for (int i = 0; i < 20; i++) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(logging.hasAnyMatchingValue("F1"));

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument, false));
    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = FP::undefined();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);

        double checkTime = control.time();
        while (control.time() < 120.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        control.advance(0.125);
        checkTime = control.time();
        while (control.time() < 120.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);

        checkTime = control.time();
        while (control.time() < 120.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);

        checkTime = control.time();
        while (control.time() < 120.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void passiveLogging()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.loggingMode = true;
        instrument.unpolledRemaining = 0.1;

        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.5);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        control.advance(0.1);

        for (int i = 0; i < 20; i++) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(logging.hasAnyMatchingValue("F1"));

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument, false));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
