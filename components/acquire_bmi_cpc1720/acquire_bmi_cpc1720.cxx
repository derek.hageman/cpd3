/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/csv.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_bmi_cpc1720.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

static const char *instrumentFlagTranslation[16] = {"EEPROMError",              /* 0x0001 */
                                                    "ConfigurationError",       /* 0x0002 */
                                                    "RTCReset",                 /* 0x0004 */
                                                    "RTCError",                 /* 0x0008 */
                                                    "SDCardError",              /* 0x0010 */
                                                    "SDCardFormatError",        /* 0x0020 */
                                                    "SDCardFull",               /* 0x0040 */
                                                    "SaturatorPumpWarning",     /* 0x0080 */
                                                    "LiquidLow",                /* 0x0100 */
                                                    "TemperatureControlError",  /* 0x0200 */
                                                    "Overheating",              /* 0x0400 */
                                                    "OpticsThermistorError",    /* 0x0800 */
                                                    "CondenserThermistorError", /* 0x1000 */
                                                    "SaturatorTopThermistorError",/* 0x2000 */
                                                    "SaturatorBottomThermistorError",/* 0x4000 */
                                                    "InletThermistorError",     /* 0x8000 */
};

AcquireBMICPC1720::Configuration::Configuration() : start(FP::undefined()),
                                                    end(FP::undefined()),
                                                    calculateConcentration(false),
                                                    pollInterval(0.5)
{ }

AcquireBMICPC1720::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          calculateConcentration(other.calculateConcentration),
          pollInterval(other.pollInterval)
{ }

void AcquireBMICPC1720::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("BMI");
    instrumentMeta["Model"].setString("1720");

    autoprobeValidRecords = 0;
    remainingVariables.clear();
    latestVariables.clear();

    memset(streamAge, 0, sizeof(streamAge));
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
    }

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    haveEmittedParameters = false;
    forceRealtimeStateEmit = true;

    manufactureDate = QDateTime(QDate(), QTime(0, 0, 0), Qt::UTC);

    lastSampleFlow = FP::undefined();
}

AcquireBMICPC1720::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s), end(e), calculateConcentration(false), pollInterval(0.5)
{
    setFromSegment(other);
}

AcquireBMICPC1720::Configuration::Configuration(const Configuration &under,
                                                const ValueSegment &over,
                                                double s,
                                                double e) : start(s),
                                                            end(e),
                                                            calculateConcentration(
                                                                    under.calculateConcentration),
                                                            pollInterval(under.pollInterval)
{
    setFromSegment(over);
}

void AcquireBMICPC1720::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["CalculateConcentration"].exists())
        calculateConcentration = config["CalculateConcentration"].toBool();
    if (FP::defined(config["PollInterval"].toDouble()))
        pollInterval = config["PollInterval"].toDouble();
}

AcquireBMICPC1720::AcquireBMICPC1720(const ValueSegment::Transfer &configData,
                                     const std::string &loggingContext) : FramedInstrument(
        "bmi1720", loggingContext),
                                                                          autoprobeStatus(
                                                                                  AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                          responseState(
                                                                                  RESP_PASSIVE_WAIT),
                                                                          autoprobeValidRecords(0),
                                                                          remainingVariables(),
                                                                          latestVariables(),
                                                                          loggingMux(
                                                                                  LogStream_TOTAL)
{
    setDefaultInvalid();
    config.append(Configuration());

    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
}

void AcquireBMICPC1720::setToAutoprobe()
{
    responseState = RESP_AUTOPROBE_PASSIVE_INITIALIZE;
    remainingVariables.insert("N");
    remainingVariables.insert("P");
    remainingVariables.insert("Tu");
}

void AcquireBMICPC1720::setToInteractive()
{ responseState = RESP_INTERACTIVE_INITIALIZE; }


ComponentOptions AcquireBMICPC1720Component::getBaseOptions()
{
    ComponentOptions options;

    options.add("recalculate", new ComponentOptionBoolean(tr("recalculate", "name"),
                                                          tr("Recalculate concentration"),
                                                          tr("When enabled the concentration is calculated from the count rate "
                                                             "and reported flow."), QString()));

    return options;
}

AcquireBMICPC1720::AcquireBMICPC1720(const ComponentOptions &options,
                                     const std::string &loggingContext) : FramedInstrument(
        "bmi1720", loggingContext),
                                                                          autoprobeStatus(
                                                                                  AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                          responseState(
                                                                                  RESP_PASSIVE_WAIT),
                                                                          autoprobeValidRecords(0),
                                                                          remainingVariables(),
                                                                          latestVariables(),
                                                                          loggingMux(
                                                                                  LogStream_TOTAL)
{
    setDefaultInvalid();
    config.append(Configuration());

    if (options.isSet("recalculate")) {
        config.first().calculateConcentration =
                qobject_cast<ComponentOptionBoolean *>(options.get("recalculate"))->get();
    }
}

AcquireBMICPC1720::~AcquireBMICPC1720()
{
}


void AcquireBMICPC1720::logValue(double frameTime,
                                 SequenceName::Component name,
                                 Variant::Root &&value,
                                 int streamID)
{
    double startTime = streamTime[streamID];
    double endTime = frameTime;
    streamAge[streamID]++;
    streamTime[streamID] = endTime;
    Q_ASSERT(FP::defined(endTime));

    if (!FP::defined(startTime) || startTime == endTime || !loggingEgress) {
        if (loggingEgress)
            streamTime[streamID] = endTime;
        if (!realtimeEgress)
            return;
        realtimeEgress->emplaceData(SequenceName({}, "raw", std::move(name)), std::move(value),
                                    frameTime, frameTime + 0.5);
        return;
    }

    SequenceValue
            dv(SequenceName({}, "raw", std::move(name)), std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        loggingMux.incoming(streamID, std::move(dv), loggingEgress);
        return;
    }

    loggingMux.incoming(streamID, dv, loggingEgress);
    dv.setStart(endTime);
    dv.setEnd(endTime + 1.0);
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireBMICPC1720::realtimeValue(double time,
                                      SequenceName::Component name,
                                      Variant::Root &&value)
{
    if (!realtimeEgress) return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

void AcquireBMICPC1720::invalidateLogValues(double frameTime)
{
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
        streamAge[i] = 0;

        if (loggingEgress != NULL)
            loggingMux.advance(i, frameTime, loggingEgress);
    }
    loggingMux.clear();
    reportedFlags.clear();
    remainingVariables.clear();
    latestVariables.clear();
    autoprobeValidRecords = 0;
    loggingLost(frameTime);
}

SequenceValue::Transfer AcquireBMICPC1720::buildLogMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_bmi_cpc1720");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);
    processing["ManufacturedAt"].set(instrumentMeta["ManufacturedAt"]);

    result.emplace_back(SequenceName({}, "raw_meta", "N"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0");
    result.back().write().metadataReal("Units").setString("cm\xE2\x81\xBB³");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Condensation nuclei concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Concentration"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "Tu"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Inlet temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Inlet"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Box")
          .setString(QObject::tr("Temperatures"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("1");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "T1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Saturator bottom temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Saturator bottom"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Box")
          .setString(QObject::tr("Temperatures"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("1");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "T2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Saturator top temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Saturator top"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Box")
          .setString(QObject::tr("Temperatures"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("1");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "T3"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Condenser temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Condenser"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Box")
          .setString(QObject::tr("Temperatures"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("1");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "T4"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Optics temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Optics"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Box")
          .setString(QObject::tr("Temperatures"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("1");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(6);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "Q1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Sample flow");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Flows"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("2");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(7);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "Q2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Saturator flow");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Saturator"));
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Flows"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("2");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(8);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "P"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Sample pressure");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Pressure"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInt64(0);


    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataSingleFlag("CoincidenceCorrected")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1720");
    result.back()
          .write()
          .metadataSingleFlag("CoincidenceCorrected")
          .hash("Description")
          .setString("Particle concentration has a coincidence correction applied");

    result.back()
          .write()
          .metadataSingleFlag("EEPROMError")
          .hash("Description")
          .setString("Instrument unable to read or write to EEPROM");
    result.back().write().metadataSingleFlag("EEPROMError").hash("Bits").setInt64(0x00010000);
    result.back()
          .write()
          .metadataSingleFlag("EEPROMError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1720");

    result.back()
          .write()
          .metadataSingleFlag("ConfigurationError")
          .hash("Description")
          .setString("Instrument saved configuration value out of range");
    result.back()
          .write()
          .metadataSingleFlag("ConfigurationError")
          .hash("Bits")
          .setInt64(0x00020000);
    result.back()
          .write()
          .metadataSingleFlag("ConfigurationError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1720");

    result.back()
          .write()
          .metadataSingleFlag("RTCReset")
          .hash("Description")
          .setString("Instrument internal RTC time has been reset (check RTC battery)");
    result.back().write().metadataSingleFlag("RTCReset").hash("Bits").setInt64(0x00040000);
    result.back()
          .write()
          .metadataSingleFlag("RTCReset")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1720");

    result.back()
          .write()
          .metadataSingleFlag("RTCError")
          .hash("Description")
          .setString("Instrument unable to communicate with RTC chip");
    result.back().write().metadataSingleFlag("RTCError").hash("Bits").setInt64(0x00080000);
    result.back()
          .write()
          .metadataSingleFlag("RTCError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1720");

    result.back()
          .write()
          .metadataSingleFlag("SDCardError")
          .hash("Description")
          .setString("Instrument SD card error");
    result.back().write().metadataSingleFlag("SDCardError").hash("Bits").setInt64(0x00100000);
    result.back()
          .write()
          .metadataSingleFlag("SDCardError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1720");

    result.back()
          .write()
          .metadataSingleFlag("SDCardFormatError")
          .hash("Description")
          .setString("Instrument SD card filesystem is not FAT32");
    result.back().write().metadataSingleFlag("SDCardFormatError").hash("Bits").setInt64(0x00200000);
    result.back()
          .write()
          .metadataSingleFlag("SDCardFormatError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1720");

    result.back()
          .write()
          .metadataSingleFlag("SDCardFull")
          .hash("Description")
          .setString("Instrument SD card is full");
    result.back().write().metadataSingleFlag("SDCardFull").hash("Bits").setInt64(0x00400000);
    result.back()
          .write()
          .metadataSingleFlag("SDCardFull")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1720");

    result.back()
          .write()
          .metadataSingleFlag("SaturatorPumpWarning")
          .hash("Description")
          .setString("Saturator pump at maximum power");
    result.back()
          .write()
          .metadataSingleFlag("SaturatorPumpWarning")
          .hash("Bits")
          .setInt64(0x00800000);
    result.back()
          .write()
          .metadataSingleFlag("SaturatorPumpWarning")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1720");

    result.back()
          .write()
          .metadataSingleFlag("LiquidLow")
          .hash("Description")
          .setString("Instrument reporting low butanol level");
    result.back().write().metadataSingleFlag("LiquidLow").hash("Bits").setInt64(0x01000000);
    result.back()
          .write()
          .metadataSingleFlag("LiquidLow")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1720");

    result.back()
          .write()
          .metadataSingleFlag("TemperatureControlError")
          .hash("Description")
          .setString("Condenser temperature greater than inlet temperature (check condenser fan)");
    result.back()
          .write()
          .metadataSingleFlag("TemperatureControlError")
          .hash("Bits")
          .setInt64(0x02000000);
    result.back()
          .write()
          .metadataSingleFlag("TemperatureControlError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1720");

    result.back()
          .write()
          .metadataSingleFlag("Overheating")
          .hash("Description")
          .setString("Condenser temperature greater than 45\xC2\xB0\x43");
    result.back().write().metadataSingleFlag("Overheating").hash("Bits").setInt64(0x04000000);
    result.back()
          .write()
          .metadataSingleFlag("Overheating")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1720");

    result.back()
          .write()
          .metadataSingleFlag("OpticsThermistorError")
          .hash("Description")
          .setString("Optics thermistor malfunctioning or out of range");
    result.back()
          .write()
          .metadataSingleFlag("OpticsThermistorError")
          .hash("Bits")
          .setInt64(0x08000000);
    result.back()
          .write()
          .metadataSingleFlag("OpticsThermistorError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1720");

    result.back()
          .write()
          .metadataSingleFlag("CondenserThermistorError")
          .hash("Description")
          .setString("Condenser thermistor malfunctioning or out of range");
    result.back()
          .write()
          .metadataSingleFlag("CondenserThermistorError")
          .hash("Bits")
          .setInt64(0x10000000);
    result.back()
          .write()
          .metadataSingleFlag("CondenserThermistorError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1720");

    result.back()
          .write()
          .metadataSingleFlag("SaturatorTopThermistorError")
          .hash("Description")
          .setString("Saturator top thermistor malfunctioning or out of range");
    result.back()
          .write()
          .metadataSingleFlag("SaturatorTopThermistorError")
          .hash("Bits")
          .setInt64(0x20000000);
    result.back()
          .write()
          .metadataSingleFlag("SaturatorTopThermistorError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1720");

    result.back()
          .write()
          .metadataSingleFlag("SaturatorBottomThermistorError")
          .hash("Description")
          .setString("Saturator bottom thermistor malfunctioning or out of range");
    result.back()
          .write()
          .metadataSingleFlag("SaturatorBottomThermistorError")
          .hash("Bits")
          .setInt64(0x40000000);
    result.back()
          .write()
          .metadataSingleFlag("SaturatorBottomThermistorError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1720");

    result.back()
          .write()
          .metadataSingleFlag("InletThermistorError")
          .hash("Description")
          .setString("Inlet thermistor malfunctioning or out of range");
    result.back()
          .write()
          .metadataSingleFlag("InletThermistorError")
          .hash("Bits")
          .setInt64(0x80000000);
    result.back()
          .write()
          .metadataSingleFlag("InletThermistorError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_bmi_cpc1720");


    result.emplace_back(SequenceName({}, "raw_meta", "ZPARAMETERS"), Variant::Root(), time,
                        FP::undefined());
    result.back()
          .write()
          .metadataHash("Description")
          .setString("Instrument calibration parameters");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
    result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");
    result.back()
          .write()
          .metadataHashChild("SampleFlowSlope")
          .metadataReal("Description")
          .setString("Sample flow slope (Q = raw * slope - offset)");
    result.back()
          .write()
          .metadataHashChild("SampleFlowSlope")
          .metadataReal("Format")
          .setString("0.00E0");
    result.back()
          .write()
          .metadataHashChild("SampleFlowSlope")
          .metadataReal("Units")
          .setString("lpm/ADC");
    result.back()
          .write()
          .metadataHashChild("SampleFlowOffset")
          .metadataReal("Description")
          .setString("Sample flow offset (Q = raw * slope - offset)");
    result.back()
          .write()
          .metadataHashChild("SampleFlowOffset")
          .metadataReal("Format")
          .setString("0.000");
    result.back()
          .write()
          .metadataHashChild("SampleFlowOffset")
          .metadataReal("Units")
          .setString("lpm");
    result.back()
          .write()
          .metadataHashChild("SaturatorFlowSlope")
          .metadataReal("Description")
          .setString("Saturator flow slope (Q = raw * slope - offset)");
    result.back()
          .write()
          .metadataHashChild("SaturatorFlowSlope")
          .metadataReal("Format")
          .setString("0.00E0");
    result.back()
          .write()
          .metadataHashChild("SaturatorFlowSlope")
          .metadataReal("Units")
          .setString("lpm/ADC");
    result.back()
          .write()
          .metadataHashChild("SaturatorFlowOffset")
          .metadataReal("Description")
          .setString("Saturator flow offset (Q = raw * slope - offset)");
    result.back()
          .write()
          .metadataHashChild("SaturatorFlowOffset")
          .metadataReal("Format")
          .setString("0.000");
    result.back()
          .write()
          .metadataHashChild("SaturatorFlowOffset")
          .metadataReal("Units")
          .setString("lpm");
    result.back()
          .write()
          .metadataHashChild("TemperatureCalibration")
          .metadataReal("Description")
          .setString("LFE calibration temperature");
    result.back()
          .write()
          .metadataHashChild("TemperatureCalibration")
          .metadataReal("Format")
          .setString("00.0");
    result.back()
          .write()
          .metadataHashChild("TemperatureCalibration")
          .metadataReal("Units")
          .setString("\xC2\xB0\x43");
    result.back()
          .write()
          .metadataHashChild("CoincidenceCorrection")
          .metadataInteger("Description")
          .setString("Coincidence correction setting");
    result.back()
          .write()
          .metadataHashChild("CoincidenceCorrection")
          .metadataInteger("Format")
          .setString("00000");
    result.back()
          .write()
          .metadataHashChild("TemperatureDifference")
          .metadataReal("Description")
          .setString("Target difference between saturator and condenser temperatures");
    result.back()
          .write()
          .metadataHashChild("TemperatureDifference")
          .metadataReal("Format")
          .setString("00.0");
    result.back()
          .write()
          .metadataHashChild("TemperatureDifference")
          .metadataReal("Units")
          .setString("\xC2\xB0\x43");
    result.back()
          .write()
          .metadataHashChild("PressureSlope")
          .metadataReal("Description")
          .setString("Pressure slope (P = raw * slope - offset)");
    result.back()
          .write()
          .metadataHashChild("PressureSlope")
          .metadataReal("Format")
          .setString("0.000");
    result.back()
          .write()
          .metadataHashChild("PressureSlope")
          .metadataReal("Units")
          .setString("hPa/ADC");
    result.back()
          .write()
          .metadataHashChild("PressureSlope")
          .metadataReal("Description")
          .setString("Pressure offset (P = raw * slope - offset)");
    result.back()
          .write()
          .metadataHashChild("PressureSlope")
          .metadataReal("Format")
          .setString("000");
    result.back().write().metadataHashChild("PressureSlope").metadataReal("Units").setString("hPa");
    result.back()
          .write()
          .metadataHashChild("AnalogPotentiometer")
          .metadataInteger("Description")
          .setString("Analog output potentiometer setting");
    result.back()
          .write()
          .metadataHashChild("AnalogPotentiometer")
          .metadataInteger("Format")
          .setString("000");

    return result;
}

SequenceValue::Transfer AcquireBMICPC1720::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_bmi_cpc1720");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);

    result.emplace_back(SequenceName({}, "raw_meta", "ZN"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0");
    result.back().write().metadataReal("Units").setString("cm\xE2\x81\xBB³");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Uncorrected condensation nuclei concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Uncorrected"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "C"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Rate"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInt64(2);

    result.emplace_back(SequenceName({}, "raw_meta", "PCT1"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000.0");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Saturator bottom heater power");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Saturator bottom heater"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Box")
          .setString(QObject::tr("Temperatures"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("1");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "PCT2"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000.0");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Saturator top heater power");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Saturator top heater"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Box")
          .setString(QObject::tr("Temperatures"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("1");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "PCT3"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000.0");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Condenser heater power");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Condenser TEC power"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Box")
          .setString(QObject::tr("Temperatures"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("1");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "PCT4"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000.0");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Optics heater power");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Optics heater"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Box")
          .setString(QObject::tr("Temperatures"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("1");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(6);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "PCT5"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000.0");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Saturator pump power");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Saturator pump power"));
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Flows"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("2");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(8);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "ZQ1"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000");
    result.back().write().metadataReal("Description").setString("Raw sample flow value");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Sample ADC"));
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Flows"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("2");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(7);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "ZQ2"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000");
    result.back().write().metadataReal("Description").setString("Raw saturator flow value");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Saturator ADC"));
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Flows"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("2");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(8);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "ZP"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000");
    result.back().write().metadataReal("Description").setString("Raw sample pressure value");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Pressure ADC"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInt64(1);


    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    /*result.back().write().metadataString("Realtime").hash("Hide").
        setBool(true);*/
    result.back().write().metadataString("Realtime").hash("Name").setString(QString());
    result.back().write().metadataString("Realtime").hash("Units").setString(QString());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back().write().metadataString("Realtime").hash("RowOrder").setInt64(9);
    /* Just so it's at the bottom */
    result.back().write().metadataString("Realtime").hash("Box").setString(QObject::tr("Flows"));
    result.back().write().metadataString("Realtime").hash("BoxID").setString("2");
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Run")
          .setString(QObject::tr("", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidRecord")
          .setString(QObject::tr("NO COMMS: Invalid record"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidResponse")
          .setString(QObject::tr("NO COMMS: Invalid response"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Timeout")
          .setString(QObject::tr("NO COMMS: Timeout"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractive")
          .setString(QObject::tr("STARTING COMMS"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveEnableReportLabel")
          .setString(QObject::tr("STARTING COMMS: Enabling report labels"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadManufacturingInfo")
          .setString(QObject::tr("STARTING COMMS: Reading manufacturing info"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadCalibration")
          .setString(QObject::tr("STARTING COMMS: Reading calibration info"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadClock")
          .setString(QObject::tr("STARTING COMMS: Reading clock"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetClock")
          .setString(QObject::tr("STARTING COMMS: Setting clock"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveDiscardFirst")
          .setString(QObject::tr("STARTING COMMS: Flushing status report"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveStartReadFirstRecord")
          .setString(QObject::tr("STARTING COMMS: Waiting for first record"));

    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("EEPROMError")
          .setString(QObject::tr("EEPROM read/write error"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("ConfigurationError")
          .setString(QObject::tr("Invalid stored configuration value"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("RTCReset")
          .setString(QObject::tr("Internal RTC reset (check battery)"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("RTCError")
          .setString(QObject::tr("Internal RTC read/write error"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SDCardError")
          .setString(QObject::tr("Internal SD card error"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SDCardFormatError")
          .setString(QObject::tr("Internal SD is not FAT32"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SDCardFull")
          .setString(QObject::tr("Internal SD is full"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SaturatorPumpWarning")
          .setString(QObject::tr("Saturator pump at maximum"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("LiquidLow")
          .setString(QObject::tr("LOW BUTANOL"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("TemperatureControlError")
          .setString(QObject::tr("Condenser temperature control error (check fan)"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Overheating")
          .setString(QObject::tr("Condenser overheating"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("OpticsThermistorError")
          .setString(QObject::tr("Optics thermistor error"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("CondenserThermistorError")
          .setString(QObject::tr("Condenser thermistor error"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SaturatorTopThermistorError")
          .setString(QObject::tr("Saturator top thermistor error"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SaturatorBottomThermistorError")
          .setString(QObject::tr("Saturator bottom thermistor error"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InletThermistorError")
          .setString(QObject::tr("Inlet thermistor error"));

    return result;
}

SequenceMatch::Composite AcquireBMICPC1720::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "ZSTATE");
    sel.append({}, {}, "ZPARAMETERS");
    return sel;
}

static double calculateConcentration(double C, double Q, double tau = 0.5E-6)
{
    if (!FP::defined(C) || !FP::defined(Q) || !FP::defined(tau))
        return FP::undefined();
    if (Q <= 0.0)
        return 0.0;
    C *= exp(C * tau);
    return C / (Q * (1000.0 / 60.0));
}

static int extractNextKey(Util::ByteView &data, Util::ByteView &key, Util::ByteView &value)
{
    auto idxSpace = data.indexOf(' ');
    while (idxSpace == 0) {
        data = data.mid(1);
        idxSpace = data.indexOf(' ');
    }
    if (idxSpace == data.npos) {
        if (data.empty())
            return -1;
        key = data;
        data = Util::ByteView();
    } else {
        Q_ASSERT(idxSpace > 0);
        key = data.mid(0, idxSpace);
        Q_ASSERT(!key.empty());
        data = data.mid(idxSpace + 1);
    }

    auto idxEqual = key.indexOf('=');
    if (idxEqual == 0)
        return 1;
    if (idxEqual == key.npos)
        return 2;
    value = key.mid(idxEqual + 1);
    if (value.empty())
        return 3;
    key = key.mid(0, idxEqual);
    Q_ASSERT(!key.empty());

    return 0;
}

int AcquireBMICPC1720::processLabels(Util::ByteView line,
                                     std::unordered_map<std::string, Variant::Root> &updatedValues)
{
    int pairCount = 0;
    bool ok = false;
    for (; pairCount < 1000; ++pairCount) {
        if (line.empty())
            break;

        Util::ByteView key;
        Util::ByteView value;
        int result = extractNextKey(line, key, value);
        if (result < 0)
            break;
        if (result != 0)
            return 1000 * (pairCount + 1) + result;

        bool isHeader = false;
        if (key.front() == '#') {
            isHeader = true;
            key = key.mid(1);
        }

        if (key.string_equal_insensitive("concent")) {
            if (updatedValues.count("N"))
                return 1000 * (pairCount + 1) + 100;
            Variant::Root val(value.parse_real(&ok));
            if (!ok) return 1000 * (pairCount + 1) + 101;
            remap("N", val);
            Util::insert_or_assign(updatedValues, "N", std::move(val));
        } else if (key.string_equal_insensitive("rawconc")) {
            if (updatedValues.count("ZN"))
                return 1000 * (pairCount + 1) + 102;
            Variant::Root val(value.parse_real(&ok));
            if (!ok) return 1000 * (pairCount + 1) + 103;
            remap("ZN", val);
            Util::insert_or_assign(updatedValues, "ZN", std::move(val));
        } else if (key.string_equal_insensitive("cnt_sec")) {
            if (updatedValues.count("C"))
                return 1000 * (pairCount + 1) + 104;
            Variant::Root val(value.parse_real(&ok));
            if (!ok) return 1000 * (pairCount + 1) + 105;
            remap("C", val);
            Util::insert_or_assign(updatedValues, "C", std::move(val));
        } else if (key.string_equal_insensitive("condtmp")) {
            if (updatedValues.count("T3"))
                return 1000 * (pairCount + 1) + 106;
            Variant::Root val(value.parse_real(&ok));
            if (!ok) return 1000 * (pairCount + 1) + 107;
            remap("T3", val);
            Util::insert_or_assign(updatedValues, "T3", std::move(val));
        } else if (key.string_equal_insensitive("satttmp")) {
            if (updatedValues.count("T2"))
                return 1000 * (pairCount + 1) + 108;
            Variant::Root val(value.parse_real(&ok));
            if (!ok) return 1000 * (pairCount + 1) + 109;
            remap("T2", val);
            Util::insert_or_assign(updatedValues, "T2", std::move(val));
        } else if (key.string_equal_insensitive("satbtmp")) {
            if (updatedValues.count("T1"))
                return 1000 * (pairCount + 1) + 110;
            Variant::Root val(value.parse_real(&ok));
            if (!ok) return 1000 * (pairCount + 1) + 111;
            remap("T1", val);
            Util::insert_or_assign(updatedValues, "T1", std::move(val));
        } else if (key.string_equal_insensitive("optctmp")) {
            if (updatedValues.count("T4"))
                return 1000 * (pairCount + 1) + 112;
            Variant::Root val(value.parse_real(&ok));
            if (!ok) return 1000 * (pairCount + 1) + 113;
            remap("T4", val);
            Util::insert_or_assign(updatedValues, "T4", std::move(val));
        } else if (key.string_equal_insensitive("inlttmp")) {
            if (updatedValues.count("Tu"))
                return 1000 * (pairCount + 1) + 114;
            Variant::Root val(value.parse_real(&ok));
            if (!ok) return 1000 * (pairCount + 1) + 115;
            remap("Tu", val);
            Util::insert_or_assign(updatedValues, "Tu", std::move(val));
        } else if (key.string_equal_insensitive("smpflow")) {
            if (updatedValues.count("Q1"))
                return 1000 * (pairCount + 1) + 116;
            double d = value.parse_real(&ok);
            if (!ok) return 1000 * (pairCount + 1) + 117;
            if (FP::defined(d))
                d /= 1000.0;
            Variant::Root val(d);
            remap("Q1", val);
            Util::insert_or_assign(updatedValues, "Q1", std::move(val));
        } else if (key.string_equal_insensitive("satflow")) {
            if (updatedValues.count("Q2"))
                return 1000 * (pairCount + 1) + 118;
            double d = value.parse_real(&ok);
            if (!ok) return 1000 * (pairCount + 1) + 119;
            if (FP::defined(d))
                d /= 1000.0;
            Variant::Root val(d);
            remap("Q2", val);
            Util::insert_or_assign(updatedValues, "Q2", std::move(val));
        } else if (key.string_equal_insensitive("pressur")) {
            if (updatedValues.count("P"))
                return 1000 * (pairCount + 1) + 120;
            Variant::Root val(value.parse_real(&ok));
            if (!ok) return 1000 * (pairCount + 1) + 121;
            remap("P", val);
            Util::insert_or_assign(updatedValues, "P", std::move(val));
        } else if (key.string_equal_insensitive("condpwr")) {
            if (updatedValues.count("PCT3"))
                return 1000 * (pairCount + 1) + 122;
            double d = value.parse_real(&ok);
            if (!ok) return 1000 * (pairCount + 1) + 123;
            if (!FP::defined(d) || d < 0.0 || d > 250.0)
                return 1000 * (pairCount + 1) + 124;
            Variant::Root val((d / 250.0) * 100.0);
            remap("PCT3", val);
            Util::insert_or_assign(updatedValues, "PCT3", std::move(val));
        } else if (key.string_equal_insensitive("sattpwr")) {
            if (updatedValues.count("PCT2"))
                return 1000 * (pairCount + 1) + 125;
            double d = value.parse_real(&ok);
            if (!ok) return 1000 * (pairCount + 1) + 126;
            if (!FP::defined(d) || d < 0.0 || d > 200.0)
                return 1000 * (pairCount + 1) + 127;
            Variant::Root val((d / 200.0) * 100.0);
            remap("PCT2", val);
            Util::insert_or_assign(updatedValues, "PCT2", std::move(val));
        } else if (key.string_equal_insensitive("satbpwr")) {
            if (updatedValues.count("PCT1"))
                return 1000 * (pairCount + 1) + 127;
            double d = value.parse_real(&ok);
            if (!ok) return 1000 * (pairCount + 1) + 129;
            if (!FP::defined(d) || d < 0.0 || d > 200.0)
                return 1000 * (pairCount + 1) + 130;
            Variant::Root val((d / 200.0) * 100.0);
            remap("PCT1", val);
            Util::insert_or_assign(updatedValues, "PCT1", std::move(val));
        } else if (key.string_equal_insensitive("optcpwr")) {
            if (updatedValues.count("PCT4"))
                return 1000 * (pairCount + 1) + 131;
            double d = value.parse_real(&ok);
            if (!ok) return 1000 * (pairCount + 1) + 132;
            if (!FP::defined(d) || d < 0.0 || d > 200.0)
                return 1000 * (pairCount + 1) + 133;
            Variant::Root val((d / 200.0) * 100.0);
            remap("PCT4", val);
            Util::insert_or_assign(updatedValues, "PCT4", std::move(val));
        } else if (key.string_equal_insensitive("satfpwr")) {
            if (updatedValues.count("PCT5"))
                return 1000 * (pairCount + 1) + 134;
            double d = value.parse_real(&ok);
            if (!ok) return 1000 * (pairCount + 1) + 135;
            if (!FP::defined(d) || d < 0.0 || d > 200.0)
                return 1000 * (pairCount + 1) + 136;
            Variant::Root val((d / 200.0) * 100.0);
            remap("PCT5", val);
            Util::insert_or_assign(updatedValues, "PCT5", std::move(val));
        } else if (key.string_equal_insensitive("fillcnt")) {
        } else if (key.string_equal_insensitive("err_num")) {
            if (updatedValues.count("F1"))
                return 1000 * (pairCount + 1) + 137;
            Variant::Root val((qint64) value.parse_u16(&ok, 10));
            if (!ok) return 1000 * (pairCount + 1) + 138;
            remap("FRAW", val);

            if (INTEGER::defined(val.read().toInt64())) {
                Variant::Flags oldFlags = reportedFlags;
                reportedFlags.clear();
                qint64 flagsBits(val.read().toInt64());
                for (int i = 0;
                        i <
                                (int) (sizeof(instrumentFlagTranslation) /
                                        sizeof(instrumentFlagTranslation[0]));
                        i++) {
                    if (flagsBits & (Q_INT64_C(1) << i)) {
                        if (instrumentFlagTranslation[i] != NULL) {
                            reportedFlags.insert(instrumentFlagTranslation[i]);
                        } else {
                            qCDebug(log) << "Unrecognized bit" << hex << ((1 << i))
                                         << "set in instrument flags";
                        }
                    }
                }

                if (reportedFlags != oldFlags)
                    forceRealtimeStateEmit = true;
            }

            Util::insert_or_assign(updatedValues, "F1", std::move(val));
        } else if (key.string_equal_insensitive("autorpt")) {
        } else if (key.string_equal_insensitive("rpt_lbl")) {
        } else if (key.string_equal_insensitive("sd_stat")) {
        } else if (key.string_equal_insensitive("sd_save")) {
        } else if (key.string_equal_insensitive("sd_intv")) {
        } else if (key.string_equal_insensitive("sd_file")) {
        } else if (key.string_equal_insensitive("sd_size")) {
        } else if (key.string_equal_insensitive("sd_used")) {
        } else if (key.string_equal_insensitive("clkhour")) {
            if (updatedValues.count("HOUR"))
                return 1000 * (pairCount + 1) + 139;
            qint64 i = value.parse_i64(&ok);
            if (!ok) return 1000 * (pairCount + 1) + 140;
            if (!INTEGER::defined(i) || i < 0 || i > 23)
                return 1000 * (pairCount + 1) + 141;
            Variant::Root val(i);
            remap("HOUR", val);
            Util::insert_or_assign(updatedValues, "HOUR", std::move(val));
        } else if (key.string_equal_insensitive("clk_min")) {
            if (updatedValues.count("MINUTE"))
                return 1000 * (pairCount + 1) + 142;
            qint64 i = value.parse_i64(&ok);
            if (!ok) return 1000 * (pairCount + 1) + 143;
            if (!INTEGER::defined(i) || i < 0 || i > 59)
                return 1000 * (pairCount + 1) + 144;
            Variant::Root val(i);
            remap("MINUTE", val);
            Util::insert_or_assign(updatedValues, "MINUTE", std::move(val));
        } else if (key.string_equal_insensitive("clk_sec")) {
            if (updatedValues.count("SECOND"))
                return 1000 * (pairCount + 1) + 145;
            qint64 i = value.parse_i64(&ok);
            if (!ok) return 1000 * (pairCount + 1) + 146;
            if (!INTEGER::defined(i) || i < 0 || i > 60)
                return 1000 * (pairCount + 1) + 147;
            Variant::Root val(i);
            remap("SECOND", val);
            Util::insert_or_assign(updatedValues, "SECOND", std::move(val));
        } else if (key.string_equal_insensitive("clkyear")) {
            if (updatedValues.count("YEAR"))
                return 1000 * (pairCount + 1) + 148;
            qint64 i = value.parse_i64(&ok);
            if (!ok) return 1000 * (pairCount + 1) + 149;
            if (!INTEGER::defined(i))
                return 1000 * (pairCount + 1) + 150;
            if (i < 1970) {
                if (i <= 10 || i > 99)
                    return 1000 * (pairCount + 1) + 151;
                i += 2000;
            } else {
                if (i > 2999)
                    return 1000 * (pairCount + 1) + 151;
            }
            Variant::Root val(i);
            remap("YEAR", val);
            Util::insert_or_assign(updatedValues, "YEAR", std::move(val));
        } else if (key.string_equal_insensitive("clk_mon")) {
            if (updatedValues.count("MONTH"))
                return 1000 * (pairCount + 1) + 152;
            qint64 i = value.parse_i64(&ok);
            if (!ok) return 1000 * (pairCount + 1) + 153;
            if (!INTEGER::defined(i) || i < 1 || i > 12)
                return 1000 * (pairCount + 1) + 154;
            Variant::Root val(i);
            remap("MONTH", val);
            Util::insert_or_assign(updatedValues, "MONTH", std::move(val));
        } else if (key.string_equal_insensitive("clk_day")) {
            if (updatedValues.count("DAY"))
                return 1000 * (pairCount + 1) + 155;
            qint64 i = value.parse_i64(&ok);
            if (!ok) return 1000 * (pairCount + 1) + 156;
            if (!INTEGER::defined(i) || i < 1 || i > 31)
                return 1000 * (pairCount + 1) + 157;
            Variant::Root val(i);
            remap("DAY", val);
            Util::insert_or_assign(updatedValues, "DAY", std::move(val));
        } else if (key.string_equal_insensitive("smpslop")) {
            if (updatedValues.count("ZPARAMETERS/SampleFlowSlope"))
                return 1000 * (pairCount + 1) + 157;
            double v = value.parse_real(&ok);
            if (!ok || !FP::defined(v)) return 1000 * (pairCount + 1) + 158;
            v /= 1000.0 * 1000.0;
            Util::insert_or_assign(updatedValues, "ZPARAMETERS/SampleFlowSlope", Variant::Root(v));
            dataParameters["SampleFlowSlope"].setDouble(v);
            haveEmittedParameters = false;
        } else if (key.string_equal_insensitive("smpoffs")) {
            if (updatedValues.count("ZPARAMETERS/SampleFlowOffset"))
                return 1000 * (pairCount + 1) + 159;
            double v = value.parse_real(&ok);
            if (!ok || !FP::defined(v)) return 1000 * (pairCount + 1) + 160;
            v /= 1000.0;
            Util::insert_or_assign(updatedValues, "ZPARAMETERS/SampleFlowOffset", Variant::Root(v));
            dataParameters["SampleFlowOffset"].setDouble(v);
            haveEmittedParameters = false;
        } else if (key.string_equal_insensitive("satslop")) {
            if (updatedValues.count("ZPARAMETERS/SaturatorFlowSlope"))
                return 1000 * (pairCount + 1) + 161;
            double v = value.parse_real(&ok);
            if (!ok || !FP::defined(v)) return 1000 * (pairCount + 1) + 162;
            v /= 1000.0 * 1000.0;
            Util::insert_or_assign(updatedValues, "ZPARAMETERS/SaturatorFlowSlope",
                                   Variant::Root(v));
            dataParameters["SaturatorFlowSlope"].setDouble(v);
            haveEmittedParameters = false;
        } else if (key.string_equal_insensitive("satoffs")) {
            if (updatedValues.count("ZPARAMETERS/SaturatorFlowOffset"))
                return 1000 * (pairCount + 1) + 163;
            double v = value.parse_real(&ok);
            if (!ok || !FP::defined(v)) return 1000 * (pairCount + 1) + 164;
            v /= 1000.0;
            Util::insert_or_assign(updatedValues, "ZPARAMETERS/SaturatorFlowOffset",
                                   Variant::Root(v));
            dataParameters["SaturatorFlowOffset"].setDouble(v);
            haveEmittedParameters = false;
        } else if (key.string_equal_insensitive("cal_tmp")) {
            if (updatedValues.count("ZPARAMETERS/TemperatureCalibration"))
                return 1000 * (pairCount + 1) + 165;
            double v = value.parse_real(&ok);
            if (!ok || !FP::defined(v)) return 1000 * (pairCount + 1) + 166;
            Util::insert_or_assign(updatedValues, "ZPARAMETERS/TemperatureCalibration",
                                   Variant::Root(v));
            dataParameters["TemperatureCalibration"].setDouble(v);
            haveEmittedParameters = false;
        } else if (key.string_equal_insensitive("coincid")) {
            if (updatedValues.count("ZPARAMETERS/CoincidenceCorrection"))
                return 1000 * (pairCount + 1) + 167;
            qint64 v = value.parse_i64(&ok);
            if (!ok || !INTEGER::defined(v)) return 1000 * (pairCount + 1) + 168;
            Util::insert_or_assign(updatedValues, "ZPARAMETERS/CoincidenceCorrection",
                                   Variant::Root(v));
            dataParameters["CoincidenceCorrection"].setInt64(v);
            haveEmittedParameters = false;
        } else if (key.string_equal_insensitive("cs_diff")) {
            if (updatedValues.count("ZPARAMETERS/TemperatureDifference"))
                return 1000 * (pairCount + 1) + 169;
            double v = value.parse_real(&ok);
            if (!ok || !FP::defined(v)) return 1000 * (pairCount + 1) + 170;
            Util::insert_or_assign(updatedValues, "ZPARAMETERS/TemperatureDifference",
                                   Variant::Root(v));
            dataParameters["TemperatureDifference"].setDouble(v);
            haveEmittedParameters = false;
        } else if (key.string_equal_insensitive("prsslop")) {
            if (updatedValues.count("ZPARAMETERS/PressureSlope"))
                return 1000 * (pairCount + 1) + 171;
            double v = value.parse_real(&ok);
            if (!ok || !FP::defined(v)) return 1000 * (pairCount + 1) + 172;
            v /= 1000.0;
            Util::insert_or_assign(updatedValues, "ZPARAMETERS/PressureSlope", Variant::Root(v));
            dataParameters["PressureSlope"].setDouble(v);
            haveEmittedParameters = false;
        } else if (key.string_equal_insensitive("prsoffs")) {
            if (updatedValues.count("ZPARAMETERS/PressureOffset"))
                return 1000 * (pairCount + 1) + 173;
            double v = value.parse_real(&ok);
            if (!ok || !FP::defined(v)) return 1000 * (pairCount + 1) + 174;
            Util::insert_or_assign(updatedValues, "ZPARAMETERS/PressureOffset", Variant::Root(v));
            dataParameters["PressureOffset"].setDouble(v);
            haveEmittedParameters = false;
        } else if (key.string_equal_insensitive("anlgcal")) {
            if (updatedValues.count("ZPARAMETERS/AnalogPotentiometer"))
                return 1000 * (pairCount + 1) + 175;
            qint64 v = value.parse_i64(&ok);
            if (!ok || !INTEGER::defined(v)) return 1000 * (pairCount + 1) + 176;
            Util::insert_or_assign(updatedValues, "ZPARAMETERS/AnalogPotentiometer",
                                   Variant::Root(v));
            dataParameters["AnalogPotentiometer"].setInt64(v);
            haveEmittedParameters = false;
        } else if (key.string_equal_insensitive("smp_raw")) {
            if (updatedValues.count("ZQ1"))
                return 1000 * (pairCount + 1) + 177;
            Variant::Root val(value.parse_real(&ok));
            if (!ok) return 1000 * (pairCount + 1) + 178;
            remap("ZQ1", val);
            Util::insert_or_assign(updatedValues, "ZQ1", std::move(val));
        } else if (key.string_equal_insensitive("sat_raw")) {
            if (updatedValues.count("ZQ2"))
                return 1000 * (pairCount + 1) + 179;
            Variant::Root val(value.parse_real(&ok));
            if (!ok) return 1000 * (pairCount + 1) + 180;
            remap("ZQ2", val);
            Util::insert_or_assign(updatedValues, "ZQ2", std::move(val));
        } else if (key.string_equal_insensitive("prs_raw")) {
            if (updatedValues.count("ZP"))
                return 1000 * (pairCount + 1) + 181;
            Variant::Root val(value.parse_real(&ok));
            if (!ok) return 1000 * (pairCount + 1) + 182;
            remap("ZP", val);
            Util::insert_or_assign(updatedValues, "ZP", std::move(val));
        } else if (key.string_equal_insensitive("ser_num")) {
            if (updatedValues.count("MFGINFO/SerialNumber"))
                return 1000 * (pairCount + 1) + 183;
            QString str(value.toQString().trimmed());
            if (str.isEmpty()) return 1000 * (pairCount + 1) + 184;

            Variant::Root oldValue(instrumentMeta["SerialNumber"]);

            int sn = -1;
            ok = false;
            if ((sn = str.toInt(&ok, 10)) > 0 && ok) {
                instrumentMeta["SerialNumber"].setInt64(sn);
            } else {
                instrumentMeta["SerialNumber"].setString(str);
            }

            if (oldValue.read() != instrumentMeta["SerialNumber"]) {
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }

            Util::insert_or_assign(updatedValues, "MFGINFO/SerialNumber",
                                   Variant::Root(instrumentMeta["SerialNumber"]));
        } else if (key.string_equal_insensitive("mfgyear")) {
            if (updatedValues.count("MFGINFO/Year"))
                return 1000 * (pairCount + 1) + 185;

            qint64 i = value.parse_i64(&ok);
            if (!ok) return 1000 * (pairCount + 1) + 186;
            if (!INTEGER::defined(i))
                return 1000 * (pairCount + 1) + 187;
            if (i < 1970) {
                if (i <= 10 || i > 99)
                    return 1000 * (pairCount + 1) + 188;
                i += 2000;
            } else {
                if (i > 2999)
                    return 1000 * (pairCount + 1) + 189;
            }

            manufactureDate.setDate(
                    QDate((int) i, manufactureDate.date().month(), manufactureDate.date().day()));
            if (manufactureDate.isValid()) {
                Variant::Root oldValue(instrumentMeta["ManufacturedAt"]);
                instrumentMeta["ManufacturedAt"].setDouble(Time::fromDateTime(manufactureDate));
                if (oldValue.read() != instrumentMeta["ManufacturedAt"]) {
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;
                    sourceMetadataUpdated();
                }
            }

            Util::insert_or_assign(updatedValues, "MFGINFO/Year", Variant::Root(i));
        } else if (key.string_equal_insensitive("mfg_mon")) {
            if (updatedValues.count("MFGINFO/Month"))
                return 1000 * (pairCount + 1) + 190;
            qint64 i = value.parse_i64(&ok);
            if (!ok) return 1000 * (pairCount + 1) + 191;
            if (!INTEGER::defined(i) || i < 1 || i > 12)
                return 1000 * (pairCount + 1) + 192;

            manufactureDate.setDate(
                    QDate(manufactureDate.date().year(), (int) i, manufactureDate.date().day()));
            if (manufactureDate.isValid()) {
                Variant::Root oldValue(instrumentMeta["ManufacturedAt"]);
                instrumentMeta["ManufacturedAt"].setDouble(Time::fromDateTime(manufactureDate));
                if (oldValue.read() != instrumentMeta["ManufacturedAt"]) {
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;
                    sourceMetadataUpdated();
                }
            }

            Util::insert_or_assign(updatedValues, "MFGINFO/Month", Variant::Root(i));
        } else if (key.string_equal_insensitive("mfg_day")) {
            if (updatedValues.count("MFGINFO/Day"))
                return 1000 * (pairCount + 1) + 193;
            qint64 i = value.parse_i64(&ok);
            if (!ok) return 1000 * (pairCount + 1) + 194;
            if (!INTEGER::defined(i) || i < 1 || i > 31)
                return 1000 * (pairCount + 1) + 195;

            manufactureDate.setDate(
                    QDate(manufactureDate.date().year(), manufactureDate.date().month(), (int) i));
            if (manufactureDate.isValid()) {
                Variant::Root oldValue(instrumentMeta["ManufacturedAt"]);
                instrumentMeta["ManufacturedAt"].setDouble(Time::fromDateTime(manufactureDate));
                if (oldValue.read() != instrumentMeta["ManufacturedAt"]) {
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;
                    sourceMetadataUpdated();
                }
            }

            Util::insert_or_assign(updatedValues, "MFGINFO/Day", Variant::Root(i));
        } else if (key.string_equal_insensitive("firmwar")) {
            if (updatedValues.count("MFGINFO/FirmwareVersion"))
                return 1000 * (pairCount + 1) + 196;
            QString str(value.toQString());
            if (str.isEmpty()) return 1000 * (pairCount + 1) + 197;

            Variant::Root oldValue(instrumentMeta["FirmwareVersion"]);
            instrumentMeta["FirmwareVersion"].setString(str.trimmed());
            if (oldValue.read() != instrumentMeta["FirmwareVersion"]) {
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }

            Util::insert_or_assign(updatedValues, "MFGINFO/FirmwareVersion",
                                   Variant::Root(instrumentMeta["FirmwareVersion"]));
        } else if (key.string_equal_insensitive("mfgdate")) {
        } else if (!isHeader) {
            return 1000 * (pairCount + 1) + 999;
        }
    }

    if (pairCount < 1)
        return 99;

    return 0;
}

int AcquireBMICPC1720::processUnpolled(const Util::ByteArray &line,
                                       std::unordered_map<std::string,
                                                          Variant::Root> &updatedValues)
{
    auto fields = CSV::acquisitionSplit(line);
    bool ok = false;
    double d;

    if (fields.empty()) return 100;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    auto subfields = Util::as_deque(field.split('/'));

    if (subfields.empty()) return 101;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    Variant::Root year;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 0) return 102;
        if (field.size() == 2) {
            raw += 2000;
            year.write().setInt64(raw);
        } else if (field.size() == 4) {
            if (raw < 1900 || raw > 2100) return 103;
            year.write().setInt64(raw);
        } else {
            return 5;
        }
    }
    remap("YEAR", year);

    if (subfields.empty()) return 104;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    Variant::Root month;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 1 || raw > 12) return 105;
        month.write().setInt64(raw);
    }
    remap("MONTH", month);

    if (subfields.empty()) return 106;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    if (field.empty()) return 107;
    Variant::Root day;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 1 || raw > 31) return 108;
        day.write().setInt64(raw);
    }
    remap("DAY", day);

    if (!subfields.empty()) return 109;

    if (fields.empty()) return 110;
    field = fields.front().string_trimmed();
    fields.pop_front();
    subfields = Util::as_deque(field.split(':'));

    if (subfields.empty()) return 111;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    if (field.empty()) return 112;
    Variant::Root hour;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 0 || raw > 23) return 113;
        hour.write().setInt64(raw);
    }
    remap("HOUR", hour);

    if (subfields.empty()) return 114;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    if (field.empty()) return 115;
    Variant::Root minute;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 0 || raw > 59) return 116;
        minute.write().setInt64(raw);
    }
    remap("MINUTE", minute);

    if (subfields.empty()) return 117;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    if (field.empty()) return 118;
    Variant::Root second;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 0 || raw > 60) return 119;
        second.write().setInt64(raw);
    }
    remap("SECOND", second);

    if (!subfields.empty()) return 120;

    Util::insert_or_assign(updatedValues, "YEAR", std::move(year));
    Util::insert_or_assign(updatedValues, "MONTH", std::move(month));
    Util::insert_or_assign(updatedValues, "DAY", std::move(day));
    Util::insert_or_assign(updatedValues, "HOUR", std::move(hour));
    Util::insert_or_assign(updatedValues, "MINUTE", std::move(minute));
    Util::insert_or_assign(updatedValues, "SECOND", std::move(second));

    if (fields.empty()) return 121;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root N(field.parse_real(&ok));
    if (!ok) return 122;
    if (!FP::defined(N.read().toReal())) return 123;
    remap("N", N);
    Util::insert_or_assign(updatedValues, "N", std::move(N));

    /* Instantaneous concentration, ignored */
    if (fields.empty()) return 124;
    fields.pop_front();

    if (fields.empty()) return 121;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZN(field.parse_real(&ok));
    if (!ok) return 122;
    if (!FP::defined(ZN.read().toReal())) return 123;
    remap("ZN", ZN);
    Util::insert_or_assign(updatedValues, "ZN", std::move(ZN));

    if (fields.empty()) return 124;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root C(field.parse_real(&ok));
    if (!ok) return 125;
    if (!FP::defined(C.read().toReal())) return 126;
    remap("C", C);
    Util::insert_or_assign(updatedValues, "C", std::move(C));

    if (fields.empty()) return 127;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T3(field.parse_real(&ok));
    if (!ok) return 128;
    if (!FP::defined(T3.read().toReal())) return 129;
    remap("T3", T3);
    Util::insert_or_assign(updatedValues, "T3", std::move(T3));

    if (fields.empty()) return 130;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T2(field.parse_real(&ok));
    if (!ok) return 131;
    if (!FP::defined(T2.read().toReal())) return 132;
    remap("T2", T2);
    Util::insert_or_assign(updatedValues, "T2", std::move(T2));

    if (fields.empty()) return 133;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T1(field.parse_real(&ok));
    if (!ok) return 134;
    if (!FP::defined(T1.read().toReal())) return 135;
    remap("T1", T1);
    Util::insert_or_assign(updatedValues, "T1", std::move(T1));

    if (fields.empty()) return 136;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T4(field.parse_real(&ok));
    if (!ok) return 137;
    if (!FP::defined(T4.read().toReal())) return 138;
    remap("T4", T4);
    Util::insert_or_assign(updatedValues, "T4", std::move(T4));

    if (fields.empty()) return 139;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Tu(field.parse_real(&ok));
    if (!ok) return 140;
    if (!FP::defined(Tu.read().toReal())) return 141;
    remap("Tu", Tu);
    Util::insert_or_assign(updatedValues, "Tu", std::move(Tu));

    if (fields.empty()) return 142;
    field = fields.front().string_trimmed();
    fields.pop_front();
    d = field.parse_real(&ok);
    if (!ok) return 143;
    if (!FP::defined(d)) return 144;
    Variant::Root Q1(d / 1000.0);
    remap("Q1", Q1);
    Util::insert_or_assign(updatedValues, "Q1", std::move(Q1));

    if (fields.empty()) return 145;
    field = fields.front().string_trimmed();
    fields.pop_front();
    d = field.parse_real(&ok);
    if (!ok) return 146;
    if (!FP::defined(d)) return 147;
    Variant::Root Q2(d / 1000.0);
    remap("Q2", Q2);
    Util::insert_or_assign(updatedValues, "Q2", std::move(Q2));

    if (fields.empty()) return 148;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root P(field.parse_real(&ok));
    if (!ok) return 149;
    if (!FP::defined(P.read().toReal())) return 150;
    remap("P", P);
    Util::insert_or_assign(updatedValues, "P", std::move(P));

    if (fields.empty()) return 151;
    field = fields.front().string_trimmed();
    fields.pop_front();
    d = field.parse_real(&ok);
    if (!ok) return 152;
    if (!FP::defined(d)) return 153;
    if (d < 0.0 || d > 250.0) return 154;
    Variant::Root PCT3((d / 250.0) * 100.0);
    remap("PCT3", PCT3);
    Util::insert_or_assign(updatedValues, "PCT3", std::move(PCT3));

    if (fields.empty()) return 155;
    field = fields.front().string_trimmed();
    fields.pop_front();
    d = field.parse_real(&ok);
    if (!ok) return 156;
    if (!FP::defined(d)) return 157;
    if (d < 0.0 || d > 200.0) return 158;
    Variant::Root PCT2((d / 200.0) * 100.0);
    remap("PCT2", PCT2);
    Util::insert_or_assign(updatedValues, "PCT2", std::move(PCT2));

    if (fields.empty()) return 159;
    field = fields.front().string_trimmed();
    fields.pop_front();
    d = field.parse_real(&ok);
    if (!ok) return 156;
    if (!FP::defined(d)) return 160;
    if (d < 0.0 || d > 200.0) return 161;
    Variant::Root PCT1((d / 200.0) * 100.0);
    remap("PCT1", PCT1);
    Util::insert_or_assign(updatedValues, "PCT1", std::move(PCT1));

    if (fields.empty()) return 162;
    field = fields.front().string_trimmed();
    fields.pop_front();
    d = field.parse_real(&ok);
    if (!ok) return 163;
    if (!FP::defined(d)) return 164;
    if (d < 0.0 || d > 200.0) return 165;
    Variant::Root PCT4((d / 200.0) * 100.0);
    remap("PCT4", PCT4);
    Util::insert_or_assign(updatedValues, "PCT4", std::move(PCT4));

    if (fields.empty()) return 166;
    field = fields.front().string_trimmed();
    fields.pop_front();
    d = field.parse_real(&ok);
    if (!ok) return 167;
    if (!FP::defined(d)) return 168;
    if (d < 0.0 || d > 200.0) return 169;
    Variant::Root PCT5((d / 200.0) * 100.0);
    remap("PCT5", PCT5);
    Util::insert_or_assign(updatedValues, "PCT5", std::move(PCT5));

    /* Fill count, ignored */
    if (fields.empty()) return 170;
    fields.pop_front();

    if (fields.empty()) return 171;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root val((qint64) field.parse_u16(&ok, 10));
    if (!ok) return 172;
    remap("FRAW", val);
    if (INTEGER::defined(val.read().toInt64())) {
        Variant::Flags oldFlags = reportedFlags;
        reportedFlags.clear();
        qint64 flagsBits(val.read().toInt64());
        for (int i = 0; i < (int) (sizeof(instrumentFlagTranslation) /
                                sizeof(instrumentFlagTranslation[0]));
                i++) {
            if (flagsBits & (Q_INT64_C(1) << i)) {
                if (instrumentFlagTranslation[i] != NULL) {
                    reportedFlags.insert(instrumentFlagTranslation[i]);
                } else {
                    qCDebug(log) << "Unrecognized bit" << hex << ((1 << i))
                                 << "set in instrument flags";
                }
            }
        }

        if (reportedFlags != oldFlags)
            forceRealtimeStateEmit = true;
    }
    Util::insert_or_assign(updatedValues, "F1", std::move(val));

    if (!fields.empty())
        return 199;

    return 0;
}

int AcquireBMICPC1720::processRecord(const Util::ByteArray &line, double &frameTime)
{
    Q_ASSERT(!config.isEmpty());

    std::unordered_map<std::string, Variant::Root> updatedValues;
    int code = processLabels(line, updatedValues);
    if (code != 0 || updatedValues.size() < 1) {
        if (processUnpolled(line, updatedValues) != 0)
            return code;
    } else if (!remainingVariables.empty()) {
        for (const auto &check : updatedValues) {
            remainingVariables.erase(check.first);
        }
    }

    if (!FP::defined(frameTime)) {
        qint64 year = updatedValues["YEAR"].read().toInt64();
        qint64 month = updatedValues["MONTH"].read().toInt64();
        qint64 day = updatedValues["DAY"].read().toInt64();
        qint64 hour = updatedValues["HOUR"].read().toInt64();
        qint64 minute = updatedValues["MINUTE"].read().toInt64();
        qint64 second = updatedValues["SECOND"].read().toInt64();
        if (INTEGER::defined(year) &&
                year >= 1970 &&
                year < 2999 &&
                INTEGER::defined(month) &&
                month >= 1 &&
                month <= 12 &&
                INTEGER::defined(day) &&
                day >= 1 &&
                day <= 12) {
            QTime time(0, 0, 0);
            if (INTEGER::defined(hour) &&
                    hour >= 0 &&
                    hour <= 23 &&
                    INTEGER::defined(minute) &&
                    minute >= 0 &&
                    minute <= 59 &&
                    INTEGER::defined(second) &&
                    second >= 1 &&
                    second <= 60) {
                time = QTime((int) hour, (int) minute, (int) second);
            }
            double check = Time::fromDateTime(
                    QDateTime(QDate((int) year, (int) month, (int) day), time, Qt::UTC));
            if (FP::defined(check)) {
                if (FP::defined(lastReportTime) && check < lastReportTime)
                    return -1;
                frameTime = check;
                configurationAdvance(frameTime);
            }
        }
    }

    if (!FP::defined(frameTime))
        return -1;
    lastReportTime = frameTime;

    if (loggingEgress == NULL) {
        haveEmittedLogMeta = false;
        loggingMux.clear();
        memset(streamAge, 0, sizeof(streamAge));
        for (int i = 0; i < LogStream_TOTAL; i++) {
            streamTime[i] = FP::undefined();
        }
    } else {
        if (!haveEmittedLogMeta) {
            haveEmittedLogMeta = true;
            loggingMux.incoming(LogStream_Metadata, buildLogMeta(frameTime), loggingEgress);
        }
        loggingMux.advance(LogStream_Metadata, frameTime, loggingEgress);
    }
    if (!haveEmittedRealtimeMeta && realtimeEgress != NULL) {
        haveEmittedRealtimeMeta = true;
        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }
    if (!haveEmittedParameters && persistentEgress != NULL) {
        haveEmittedParameters = true;
        persistentEgress->emplaceData(SequenceName({}, "raw", "ZPARAMETERS"), dataParameters,
                                      frameTime, FP::undefined());
    }


    if (updatedValues.count("N") && !config.first().calculateConcentration) {
        logValue(frameTime, "N", std::move(updatedValues["N"]), LogStream_Concentration);
    }
    if (updatedValues.count("Q1")) {
        lastSampleFlow = updatedValues["Q1"].read().toDouble();
        logValue(frameTime, "Q1", std::move(updatedValues["Q1"]), LogStream_SampleFlow);
    }
    if (updatedValues.count("Q2")) {
        logValue(frameTime, "Q2", std::move(updatedValues["Q2"]), LogStream_SaturatorFlow);
    }
    if (updatedValues.count("C")) {
        if (config.first().calculateConcentration) {
            Variant::Root
                    N(calculateConcentration(updatedValues["C"].read().toDouble(), lastSampleFlow));
            remap("N", N);
            logValue(frameTime, "N", std::move(N), LogStream_Concentration);
        }
        realtimeValue(frameTime, "C", std::move(updatedValues["C"]));
    }
    if (updatedValues.count("T4")) {
        logValue(frameTime, "T4", std::move(updatedValues["T4"]), LogStream_OpticsTemperature);
    }
    if (updatedValues.count("T3")) {
        logValue(frameTime, "T3", std::move(updatedValues["T3"]), LogStream_CondenserTemperature);
    }
    if (updatedValues.count("T2")) {
        logValue(frameTime, "T2", std::move(updatedValues["T2"]),
                 LogStream_SaturatorTopTemperature);
    }
    if (updatedValues.count("T1")) {
        logValue(frameTime, "T1", std::move(updatedValues["T1"]),
                 LogStream_SaturatorBottomTemperature);
    }
    if (updatedValues.count("Tu")) {
        logValue(frameTime, "Tu", std::move(updatedValues["Tu"]), LogStream_InletTemperature);
    }
    if (updatedValues.count("P")) {
        logValue(frameTime, "P", std::move(updatedValues["P"]), LogStream_Pressure);
    }

    if (updatedValues.count("ZN")) {
        realtimeValue(frameTime, "ZN", std::move(updatedValues["ZN"]));
    }
    if (updatedValues.count("ZQ1")) {
        realtimeValue(frameTime, "ZQ1", std::move(updatedValues["ZQ1"]));
    }
    if (updatedValues.count("ZQ2")) {
        realtimeValue(frameTime, "ZQ2", std::move(updatedValues["ZQ2"]));
    }
    if (updatedValues.count("ZP")) {
        realtimeValue(frameTime, "ZP", std::move(updatedValues["ZP"]));
    }
    if (updatedValues.count("PCT1")) {
        realtimeValue(frameTime, "PCT1", std::move(updatedValues["PCT1"]));
    }
    if (updatedValues.count("PCT2")) {
        realtimeValue(frameTime, "PCT2", std::move(updatedValues["PCT2"]));
    }
    if (updatedValues.count("PCT3")) {
        realtimeValue(frameTime, "PCT3", std::move(updatedValues["PCT3"]));
    }
    if (updatedValues.count("PCT4")) {
        realtimeValue(frameTime, "PCT4", std::move(updatedValues["PCT4"]));
    }
    if (updatedValues.count("PCT5")) {
        realtimeValue(frameTime, "PCT5", std::move(updatedValues["PCT5"]));
    }

    Variant::Flags flags = reportedFlags;
    flags.insert("CoincidenceCorrected");
    realtimeValue(frameTime, "F1", Variant::Root(flags));

    for (int i = LogStream_RecordBaseStart; i < LogStream_TOTAL; i++) {
        if (streamAge[i] < 2)
            continue;
        if (loggingEgress == NULL)
            continue;

        if (FP::defined(streamTime[LogStream_State])) {
            double startTime = streamTime[LogStream_State];
            double endTime = frameTime;

            loggingMux.incoming(LogStream_State,
                                SequenceValue({{}, "raw", "F1"}, Variant::Root(flags), startTime,
                                              endTime), loggingEgress);
        } else {
            loggingMux.advance(LogStream_State, frameTime, loggingEgress);
        }
        streamTime[LogStream_State] = frameTime;

        for (i = LogStream_RecordBaseStart; i < LogStream_TOTAL; i++) {
            if (streamAge[i] == 0) {
                loggingMux.advance(i, frameTime, loggingEgress);
                streamTime[i] = FP::undefined();
            }
            streamAge[i] = 0;
        }
        break;
    }

    if (realtimeEgress != NULL && forceRealtimeStateEmit) {
        forceRealtimeStateEmit = false;

        if (reportedFlags.empty()) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                  FP::undefined()));
        } else {
            auto sorted = Util::to_qstringlist(reportedFlags);
            std::sort(sorted.begin(), sorted.end());
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(sorted.join(",")), frameTime,
                                  FP::undefined()));
        }
    }

    return 0;
}

void AcquireBMICPC1720::configurationAdvance(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    bool oldCalculate = config.first().calculateConcentration;
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.isEmpty());
    if (oldCalculate != config.first().calculateConcentration) {
        streamAge[LogStream_Concentration] = 0;
        streamTime[LogStream_Concentration] = FP::undefined();
    }
}

void AcquireBMICPC1720::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    configurationAdvance(frameTime);

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            if (++autoprobeValidRecords > 5 && remainingVariables.empty()) {
                qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

                responseState = RESP_PASSIVE_RUN;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                timeoutAt(frameTime + config.first().pollInterval + 2.0);

                forceRealtimeStateEmit = true;

                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("PassiveAutoprobeWait");
                event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);
            }
        } else if (code > 0) {
            invalidateLogValues(frameTime);
        }
        break;
    }
    case RESP_PASSIVE_WAIT: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);
            responseState = RESP_PASSIVE_RUN;
            generalStatusUpdated();

            ++autoprobeValidRecords;
            timeoutAt(frameTime + config.first().pollInterval + 2.0);

            forceRealtimeStateEmit = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveWait");
            event(frameTime, QObject::tr("Passive communications established."), false, info);
        } else if (code > 0) {
            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_INTERACTIVE_START_FIRSTVALID: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            if (++autoprobeValidRecords > 5 && remainingVariables.empty()) {
                qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

                timeoutAt(frameTime + config.first().pollInterval);
                responseState = RESP_INTERACTIVE_RUN_WAIT;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                forceRealtimeStateEmit = true;

                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("FirstValid");
                event(frameTime, QObject::tr("Communications established."), false, info);
            }
        } else if (code > 0) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "rejected with code" << code;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidRecord"),
                                      frameTime, FP::undefined()));
            }

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Communications start rejected the first line (code %1).").arg(code),
                  true, info);

            invalidateLogValues(frameTime);
        } /* Ignored line */
        break;
    }

    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RUN:
    case RESP_INTERACTIVE_RUN_WAIT: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            ++autoprobeValidRecords;

            if (responseState != RESP_PASSIVE_RUN) {
                responseState = RESP_INTERACTIVE_RUN_WAIT;
                timeoutAt(frameTime + config.first().pollInterval);
            } else {
                timeoutAt(frameTime + config.first().pollInterval + 2.0);
            }
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            Variant::Write info = Variant::Write::empty();

            if (responseState == RESP_PASSIVE_RUN) {
                timeoutAt(FP::undefined());
                responseState = RESP_PASSIVE_WAIT;
                info.hash("ResponseState").setString("PassiveRun");
            } else {
                if (controlStream != NULL) {
                    controlStream->writeControl("\r\rautorpt=0\r");
                }
                responseState = RESP_INTERACTIVE_START_STOPREPORTS;
                discardData(frameTime + 0.5);
                info.hash("ResponseState").setString("Run");
            }
            generalStatusUpdated();

            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
        } /* Ignored line */
        break;
    }

    case RESP_INTERACTIVE_START_READMFG:
    case RESP_INTERACTIVE_START_READMFG_WAIT:
    case RESP_INTERACTIVE_START_READCAL:
    case RESP_INTERACTIVE_START_READCAL_WAIT:
    case RESP_INTERACTIVE_START_READCLK:
    case RESP_INTERACTIVE_START_READCLK_WAIT: {
        std::unordered_map<std::string, Variant::Root> updated;
        int code = processLabels(frame, updated);
        if (code == 0) {
            for (auto &check : updated) {
                remainingVariables.erase(check.first);
                Util::insert_or_assign(latestVariables, check.first, std::move(check.second));
            }
            if (remainingVariables.empty()) {
                switch (responseState) {
                case RESP_INTERACTIVE_START_READMFG:
                    responseState = RESP_INTERACTIVE_START_READMFG_WAIT;
                    timeoutAt(frameTime + 0.5);
                    break;
                case RESP_INTERACTIVE_START_READCAL:
                    responseState = RESP_INTERACTIVE_START_READCAL_WAIT;
                    timeoutAt(frameTime + 0.5);
                    break;
                case RESP_INTERACTIVE_START_READCLK:
                    responseState = RESP_INTERACTIVE_START_READCLK_WAIT;
                    timeoutAt(frameTime + 0.5);
                    break;
                default:
                    break;
                }
            }
        } else if (code > 0) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "rejected with code" << code;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidRecord"),
                                      frameTime, FP::undefined()));
            }

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            invalidateLogValues(frameTime);
        } /* Ignored line */
        break;
    }

    default:
        break;
    }
}

void AcquireBMICPC1720::incomingInstrumentTimeout(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));

    switch (responseState) {
    case RESP_INTERACTIVE_RUN:
        qCDebug(log) << "Timeout in interactive mode at" << Logging::time(frameTime);
        timeoutAt(frameTime + 30.0);

        if (controlStream != NULL) {
            controlStream->writeControl("\r\rautorpt=0\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        discardData(frameTime + 0.5);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("InteractiveRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_RUN_WAIT:
        timeoutAt(frameTime + 2.0);
        responseState = RESP_INTERACTIVE_RUN;

        if (controlStream != NULL) {
            controlStream->writeControl("read\rraw=2\r");
        }
        break;

    case RESP_PASSIVE_RUN:
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        responseState = RESP_PASSIVE_WAIT;
        generalStatusUpdated();
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_START_READMFG_WAIT:
        if (controlStream != NULL) {
            controlStream->writeControl("calib\r");
        }
        responseState = RESP_INTERACTIVE_START_READCAL;
        timeoutAt(frameTime + 2.0);

        remainingVariables.insert("ZPARAMETERS/SampleFlowSlope");
        remainingVariables.insert("ZPARAMETERS/SampleFlowOffset");
        remainingVariables.insert("ZPARAMETERS/SaturatorFlowSlope");
        remainingVariables.insert("ZPARAMETERS/SaturatorFlowOffset");
        remainingVariables.insert("ZPARAMETERS/CoincidenceCorrection");
        remainingVariables.insert("ZPARAMETERS/PressureSlope");
        remainingVariables.insert("ZPARAMETERS/PressureOffset");

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadCalibration"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_READCAL_WAIT:
        if (controlStream != NULL) {
            controlStream->writeControl("rtclck\r");
        }
        responseState = RESP_INTERACTIVE_START_READCLK;
        timeoutAt(frameTime + 2.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveReadClock"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_READCLK_WAIT: {
        qint64 year = latestVariables["YEAR"].read().toInt64();
        qint64 month = latestVariables["MONTH"].read().toInt64();
        qint64 day = latestVariables["DAY"].read().toInt64();
        qint64 hour = latestVariables["HOUR"].read().toInt64();
        qint64 minute = latestVariables["MINUTE"].read().toInt64();
        qint64 second = latestVariables["SECOND"].read().toInt64();
        if (controlStream &&
                INTEGER::defined(year) &&
                year >= 1970 &&
                year < 2999 &&
                INTEGER::defined(month) &&
                month >= 1 &&
                month <= 12 &&
                INTEGER::defined(day) &&
                day >= 1 &&
                day <= 12 &&
                INTEGER::defined(hour) &&
                hour >= 0 &&
                hour <= 23 &&
                INTEGER::defined(minute) &&
                minute >= 0 &&
                minute <= 59 &&
                INTEGER::defined(second) &&
                second >= 1 &&
                second <= 60) {
            double check = Time::fromDateTime(QDateTime(QDate((int) year, (int) month, (int) day),
                                                        QTime((int) hour, (int) minute,
                                                              (int) second), Qt::UTC));
            if (FP::defined(check) && fabs(check - frameTime) > 3.0) {
                QDateTime dt(Time::toDateTime(frameTime));
                Util::ByteArray send;
                send += "clkyear=";
                send += QByteArray::number(dt.date().year() % 100, 16);
                send += "\rclk_mon=";
                send += QByteArray::number(dt.date().month(), 16);
                send += "\rclk_day=";
                send += QByteArray::number(dt.date().day(), 16);
                send += "\rclk_hour=";
                send += QByteArray::number(dt.time().hour(), 16);
                send += "\rclk_minute=";
                send += QByteArray::number(dt.time().minute(), 16);
                send += "\rclk_second=";
                send += QByteArray::number(dt.time().second(), 16);
                send += "\rstore\r";
                controlStream->writeControl(std::move(send));

                responseState = RESP_INTERACTIVE_START_SETCLK;
                discardData(frameTime + 0.75);
                timeoutAt(frameTime + 2.0);

                if (realtimeEgress) {
                    realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                            "StartInteractiveSetClock"), frameTime, FP::undefined()));
                }
                break;
            }
        }

        timeoutAt(frameTime + 2.0);
        discardData(frameTime + 0.75);
        responseState = RESP_INTERACTIVE_START_DISCARDFIRST;

        if (controlStream != NULL) {
            controlStream->writeControl("read\r");
        }
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveDiscardFirst"), frameTime, FP::undefined()));
        }
        break;
    }

    case RESP_INTERACTIVE_START_STOPREPORTS:
    case RESP_INTERACTIVE_START_ENABLEREPORTLABEL:
    case RESP_INTERACTIVE_START_READMFG:
    case RESP_INTERACTIVE_START_READCAL:
    case RESP_INTERACTIVE_START_READCLK:
    case RESP_INTERACTIVE_START_SETCLK:
    case RESP_INTERACTIVE_START_DISCARDFIRST:
    case RESP_INTERACTIVE_START_FIRSTVALID:
    case RESP_INTERACTIVE_RESTART_WAIT:
        qCDebug(log) << "Timeout in interactive start state" << responseState << "at"
                     << Logging::time(frameTime);

        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        timeoutAt(frameTime + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl("\r\rautorpt=0\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        discardData(frameTime + 0.5);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        invalidateLogValues(frameTime);
        break;

    default:
        invalidateLogValues(frameTime);
        break;
    }
}

void AcquireBMICPC1720::discardDataCompleted(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    switch (responseState) {
    case RESP_INTERACTIVE_START_STOPREPORTS:
        if (controlStream != NULL) {
            controlStream->writeControl("rpt_lbl=1\r");
        }
        responseState = RESP_INTERACTIVE_START_ENABLEREPORTLABEL;
        discardData(frameTime + 0.5);

        autoprobeValidRecords = 0;
        remainingVariables.clear();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveEnableReportLabel"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_ENABLEREPORTLABEL:
        if (controlStream != NULL) {
            controlStream->writeControl("mfginfo\r");
        }
        responseState = RESP_INTERACTIVE_START_READMFG;
        timeoutAt(frameTime + 2.0);

        autoprobeValidRecords = 0;
        remainingVariables.clear();
        remainingVariables.insert("MFGINFO/SerialNumber");

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadManufacturingInfo"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_DISCARDFIRST:
        if (controlStream != NULL) {
            controlStream->writeControl("read\rraw=2\r");
        }

        autoprobeValidRecords = 0;
        remainingVariables.clear();
        remainingVariables.insert("N");
        remainingVariables.insert("P");
        remainingVariables.insert("Tu");
        remainingVariables.insert("ZQ1");

        responseState = RESP_INTERACTIVE_START_FIRSTVALID;
        timeoutAt(frameTime + 3.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveStartReadFirstRecord"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_SETCLK:
        timeoutAt(frameTime + 2.0);
        discardData(frameTime + 0.75);
        responseState = RESP_INTERACTIVE_START_DISCARDFIRST;

        if (controlStream != NULL) {
            controlStream->writeControl("read\r");
        }
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveDiscardFirst"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_RESTART_WAIT:
        timeoutAt(frameTime + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl("\r\rautorpt=0\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        discardData(frameTime + 0.5);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }

        invalidateLogValues(frameTime);
        break;

    default:
        break;
    }
}


void AcquireBMICPC1720::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_UNUSED(frameTime);
    Q_UNUSED(frame);
    /* Maybe implement something to discard unknown command responses? */
}

Variant::Root AcquireBMICPC1720::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireBMICPC1720::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RUN:
    case RESP_INTERACTIVE_RUN_WAIT:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

AcquisitionInterface::AutoprobeStatus AcquireBMICPC1720::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireBMICPC1720::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN:
    case RESP_INTERACTIVE_RUN_WAIT:
        /* Already running/started, do nothing */
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << responseState << "at" << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS:
    case RESP_INTERACTIVE_START_ENABLEREPORTLABEL:
    case RESP_INTERACTIVE_START_READMFG:
    case RESP_INTERACTIVE_START_READMFG_WAIT:
    case RESP_INTERACTIVE_START_READCAL:
    case RESP_INTERACTIVE_START_READCAL_WAIT:
    case RESP_INTERACTIVE_START_READCLK:
    case RESP_INTERACTIVE_START_READCLK_WAIT:
    case RESP_INTERACTIVE_START_SETCLK:
    case RESP_INTERACTIVE_START_DISCARDFIRST:
    case RESP_INTERACTIVE_START_FIRSTVALID:
    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        timeoutAt(time + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl("\r\rautorpt=0\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        discardData(time + 0.5);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }

        invalidateLogValues(time);
        break;
    }
}

void AcquireBMICPC1720::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
    autoprobeValidRecords = 0;
    discardData(time + 0.5, 1);
    timeoutAt(time + config.first().pollInterval * 7.0 + 3.0);
    generalStatusUpdated();
}

void AcquireBMICPC1720::autoprobePromote(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_WAIT:
        qCDebug(log) << "Promoted from interactive wait to interactive acquisition at"
                     << Logging::time(time);

        timeoutAt(time);
        break;
    case RESP_INTERACTIVE_RUN:
    case RESP_INTERACTIVE_START_STOPREPORTS:
    case RESP_INTERACTIVE_START_ENABLEREPORTLABEL:
    case RESP_INTERACTIVE_START_READMFG:
    case RESP_INTERACTIVE_START_READMFG_WAIT:
    case RESP_INTERACTIVE_START_READCAL:
    case RESP_INTERACTIVE_START_READCAL_WAIT:
    case RESP_INTERACTIVE_START_READCLK:
    case RESP_INTERACTIVE_START_READCLK_WAIT:
    case RESP_INTERACTIVE_START_SETCLK:
    case RESP_INTERACTIVE_START_DISCARDFIRST:
    case RESP_INTERACTIVE_START_FIRSTVALID:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + config.first().pollInterval + 2.0);
        break;

    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl("\r\rautorpt=0\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        discardData(time + 0.5);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }

        invalidateLogValues(time);
        break;
    }
}

void AcquireBMICPC1720::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    Q_ASSERT(!config.isEmpty());

    timeoutAt(time + config.first().pollInterval + 2.0);

    switch (responseState) {
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case RESP_INTERACTIVE_RUN:
        responseState = RESP_PASSIVE_RUN;

        qCDebug(log) << "Promoted from interactive to passive acquisition at "
                     << Logging::time(time);
        break;

    default:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_PASSIVE_RUN;
        generalStatusUpdated();
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireBMICPC1720::getDefaults()
{
    AutomaticDefaults result;
    result.name = "N$1$2";
    result.setSerialN81(38400);
    return result;
}


ComponentOptions AcquireBMICPC1720Component::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options, true);
    return options;
}

ComponentOptions AcquireBMICPC1720Component::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireBMICPC1720Component::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples());
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireBMICPC1720Component::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireBMICPC1720Component::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options, true);
}

QList<ComponentExample> AcquireBMICPC1720Component::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options,
                                     tr("Convert data using the instrument reported concentration")));

    options = getBaseOptions();
    (qobject_cast<ComponentOptionBoolean *>(options.get("recalculate")))->set(true);
    examples.append(
            ComponentExample(options, tr("Recalculate the concentration from the count rate")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireBMICPC1720Component::createAcquisitionPassive(const ComponentOptions &options,
                                                                                   const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireBMICPC1720(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireBMICPC1720Component::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                   const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireBMICPC1720(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireBMICPC1720Component::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                     const std::string &loggingContext)
{
    std::unique_ptr<AcquireBMICPC1720> i(new AcquireBMICPC1720(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<
        AcquisitionInterface> AcquireBMICPC1720Component::createAcquisitionInteractive(const ValueSegment::Transfer &config,
                                                                                       const std::string &loggingContext)
{
    std::unique_ptr<AcquireBMICPC1720> i(new AcquireBMICPC1720(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
