/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QLoggingCategory>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/sequencefilter.hxx"
#include "datacore/variant/composite.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/acquisitionvariableset.hxx"
#include "acquisition/lineingress.hxx"
#include "algorithms/cryptography.hxx"

#include "process_incoming.hxx"

Q_DECLARE_LOGGING_CATEGORY(log_component_process_incoming)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Transfer;
using namespace CPD3::Acquisition;

namespace {

class ProcessingFilter {
    SequenceFilter filter;
    bool first;

    double timeLimitStart;
    double timeLimitEnd;
    bool forceDefinedEnd;

    qint64 acceptedCount;
    qint64 rejectedCount;
    double acceptedStart;
    double acceptedEnd;
    double acceptedLastStart;

    SequenceValue::Transfer finalValues;

    void processAccepted(SequenceValue &&v, StreamSink *egress)
    {
        ++acceptedCount;

        if (Range::compareStart(v.getStart(), timeLimitStart) < 0)
            v.setStart(timeLimitStart);
        if (Range::compareEnd(v.getEnd(), timeLimitEnd) > 0)
            v.setEnd(timeLimitEnd);

        if (first) {
            acceptedStart = v.getStart();
            first = false;
        }
        acceptedLastStart = v.getStart();

        if (!forceDefinedEnd) {
            if (Range::compareEnd(acceptedEnd, v.getEnd()) < 0) {
                acceptedEnd = v.getEnd();
            }
        } else if (!FP::defined(acceptedEnd) ||
                (FP::defined(v.getEnd()) && v.getEnd() > acceptedEnd)) {
            acceptedEnd = v.getEnd();
        }

        if (!forceDefinedEnd) {
            egress->incomingData(std::move(v));
        } else {
            finalValues.emplace_back(std::move(v));
        }
    }

public:
    ProcessingFilter(SequenceFilter &&f,
                     double limitStart = FP::undefined(),
                     double limitEnd = FP::undefined(),
                     bool definedEnd = false) : filter(std::move(f)),
                                                first(true),
                                                timeLimitStart(limitStart),
                                                timeLimitEnd(limitEnd),
                                                forceDefinedEnd(definedEnd),
                                                acceptedCount(0),
                                                rejectedCount(0),
                                                acceptedStart(FP::undefined()),
                                                acceptedEnd(FP::undefined()),
                                                acceptedLastStart(FP::undefined()),
                                                finalValues()
    { }

    void incomingData(const SequenceValue &add, StreamSink *egress)
    {
        if (!filter.acceptAdvancing(add)) {
            ++rejectedCount;
            return;
        }
        if (Range::compareStartEnd(add.getStart(), timeLimitEnd) >= 0 ||
                Range::compareStartEnd(timeLimitStart, add.getEnd()) >= 0) {
            ++rejectedCount;
            return;
        }

        SequenceValue v = add;
        processAccepted(std::move(v), egress);
    }

    void incomingData(SequenceValue &&add, StreamSink *egress)
    {
        if (!filter.acceptAdvancing(add)) {
            ++rejectedCount;
            return;
        }
        if (Range::compareStartEnd(add.getStart(), timeLimitEnd) >= 0 ||
                Range::compareStartEnd(timeLimitStart, add.getEnd()) >= 0) {
            ++rejectedCount;
            return;
        }
        processAccepted(std::move(add), egress);
    }

    void endData(StreamSink *egress)
    {
        if (forceDefinedEnd && FP::defined(acceptedEnd)) {
            for (auto &v : finalValues) {
                if (FP::defined(v.getEnd()))
                    continue;
                v.setEnd(acceptedEnd);
            }
        }

        egress->incomingData(std::move(finalValues));
        finalValues.clear();
        egress->endData();
    }

    void injectIntoEvent(Variant::Write &eventInformation) const
    {
        eventInformation["AcceptedValues"].setInt64(acceptedCount);
        eventInformation["RejectedValues"].setInt64(rejectedCount);
        eventInformation["Start"].setDouble(acceptedStart);
        eventInformation["End"].setDouble(acceptedEnd);
        eventInformation["LastStart"].setDouble(acceptedLastStart);
    }

    bool hadAnyData() const
    {
        return acceptedCount > 0 || rejectedCount > 0;
    }
};

class FilterShim : public ErasureSink {
    StreamSink *target;
    ProcessingFilter filter;
public:
    FilterShim(StreamSink *t, SequenceFilter &&f) : target(t), filter(std::move(f))
    { }

    void incomingValue(const ArchiveValue &value) override
    { filter.incomingData(value, target); }

    void incomingValue(ArchiveValue &&value) override
    { filter.incomingData(std::move(value), target); }

    void incomingErasure(const ArchiveErasure &) override
    { }

    void endStream() override
    { filter.endData(target); }

    void injectIntoEvent(Variant::Write &eventInformation) const
    { filter.injectIntoEvent(eventInformation); }
};
}

static void injectUnpackActions(TransferUnpack &unpacker, int depth, const Variant::Read &config)
{
    switch (config.getType()) {
    case Variant::Type::Empty:
        break;
    case Variant::Type::Array:
        for (auto child : config.toArray()) {
            unpacker.invokeAt(child, depth);
        }
        break;
    default:
        unpacker.invokeAt(config, depth);
        break;
    }
}

static ExternalSink *createArchiveTarget()
{
    ExternalSinkComponent
            *component = qobject_cast<ExternalSinkComponent *>(ComponentLoader::create("archive"));
    Q_ASSERT(component);

    ComponentOptions options = component->getOptions();
    qobject_cast<ComponentOptionEnum *>(options.get("remove-flavors"))->set("flatten");
    qobject_cast<ComponentOptionEnum *>(options.get("remove"))->set("variable");

    ExternalSink *archive = component->createDataEgress(nullptr, options);
    Q_ASSERT(archive);
    return archive;
}


ProcessIncoming::IncomingTransferUnpacker::IncomingTransferUnpacker(ProcessIncoming *p,
                                                                    const Variant::Read &fc,
                                                                    const ValueSegment::Transfer &a)
        : parent(p),
          fileConfiguration(fc),
          authorizations(a),
          signingCertificate(),
          signatureRejected(false)
{ }

ProcessIncoming::IncomingTransferUnpacker::~IncomingTransferUnpacker()
{ }

QString ProcessIncoming::IncomingTransferUnpacker::getSigningCertificateID() const
{ return signingCertificate; }

bool ProcessIncoming::IncomingTransferUnpacker::isUnauthorized() const
{ return signatureRejected; }

QString ProcessIncoming::IncomingTransferUnpacker::applySubstitutions(const QString &input) const
{ return parent->substitutions.apply(input); }

Variant::Read ProcessIncoming::IncomingTransferUnpacker::getCertificate(const QByteArray &id,
                                                                        bool decryption,
                                                                        int depth)
{
    if (decryption)
        return Variant::Root(false);
    if (depth != 1)
        return Variant::Read::empty();

    QString name(QString::fromLatin1(id.toHex().toLower()));

    Variant::Read result;
    for (const auto &check : authorizations) {
        auto auth = check.getValue().hash(name);
        if (!auth.exists())
            continue;
        if (!auth.hash("Certificate").exists())
            continue;
        result = auth.hash("Certificate");
    }
    return result;
}

Variant::Read ProcessIncoming::IncomingTransferUnpacker::getKeyForCertificate(const QByteArray &id,
                                                                              int depth)
{
    if (depth != 2)
        return Variant::Read::empty();

    QString name(QString::fromLatin1(id.toHex().toLower()));
    return fileConfiguration.hash("Decryption").hash(name);
}

bool ProcessIncoming::IncomingTransferUnpacker::signatureAuthorized(const QList<QByteArray> &ids,
                                                                    int depth)
{
    if (depth != 1)
        return false;
    if (ids.size() != 1)
        return false;

    QString name(QString::fromLatin1(ids.first().toHex().toLower()));
    signingCertificate = name;

    for (const auto &check : authorizations) {
        auto auth = check.getValue().hash(name);
        if (!auth.exists())
            continue;
        if (!auth.hash("Filter").exists())
            continue;
        signatureRejected = false;
        return true;
    }

    signatureRejected = true;
    return false;
}

bool ProcessIncoming::IncomingTransferUnpacker::signingCertificateValid(const QSslCertificate &certificate,
                                                                        int depth)
{
    if (depth != 1)
        return false;

    QString name
            (QString::fromLatin1(Algorithms::Cryptography::sha512(certificate).toHex().toLower()));

    for (const auto &check : authorizations) {
        auto auth = check.getValue().hash(name);
        if (!auth.exists())
            continue;
        if (!Algorithms::Cryptography::verifyCertificate(certificate, auth.hash("Verify"))) {
            signingCertificate = name;
            signatureRejected = true;
            return false;
        }
    }

    return true;
}


ProcessIncoming::ProcessingResult ProcessIncoming::processDataFile(DownloadFile *file,
                                                                   Variant::Write &eventInformation,
                                                                   const SequenceName::Component &station,
                                                                   const Variant::Read &fileConfiguration,
                                                                   const ValueSegment::Transfer &stationConfiguration,
                                                                   bool canTerminate)
{
    auto inputFile = IO::Access::file(file->fileInfo(), IO::File::Mode::readOnly());
    if (!inputFile) {
        qCDebug(log_component_process_incoming) << "Failed to open local file";
        return Processing_Error;
    }

    auto unpackedDataFile = IO::Access::temporaryFile();
    if (!unpackedDataFile) {
        qCDebug(log_component_process_incoming) << "Error opening temporary file";
        return Processing_Error;
    }

    ValueSegment::Transfer authorizationConfiguration
            (ValueSegment::withPath(stationConfiguration, "Authorization"));

    IncomingTransferUnpacker transferUnpacker(this, fileConfiguration, authorizationConfiguration);
    Threading::Receiver rxUnpack;
    transferUnpacker.feedback.forward(feedback);
    if (canTerminate) {
        terminateRequested.connect(rxUnpack, std::bind(&IncomingTransferUnpacker::signalTerminate,
                                                       &transferUnpacker));
    }
    injectUnpackActions(transferUnpacker, 0, fileConfiguration["Preprocessing"]);
    injectUnpackActions(transferUnpacker, -1, fileConfiguration["Postprocessing"]);

    transferUnpacker.requireChecksum();
    transferUnpacker.requireSignature(TransferUnpack::Signature_SingleOnly);
    transferUnpacker.requireEncryption(TransferUnpack::Encryption_Default, true);
    transferUnpacker.requireCompression();
    transferUnpacker.requireEnd();

    if (canTerminate && testTerminated()) {
        return Processing_Error;
    }
    if (!transferUnpacker.exec(inputFile, unpackedDataFile)) {
        if (transferUnpacker.isUnauthorized()) {
            qCDebug(log_component_process_incoming) << "File authorization"
                                                    << transferUnpacker.getSigningCertificateID()
                                                    << " denied";

            eventInformation["UnauthorizedDigest"].setString(
                    transferUnpacker.getSigningCertificateID());
            return Processing_Unauthorized;
        }

        qCDebug(log_component_process_incoming) << "File unpacking failed:"
                                                << transferUnpacker.errorString();
        eventInformation["CorruptReason"].setString(transferUnpacker.errorString());
        return Processing_Corrupted;
    }
    inputFile.reset();

    QString authorizationDigest(transferUnpacker.getSigningCertificateID());
    if (authorizationDigest.isEmpty()) {
        qCDebug(log_component_process_incoming) << "File has no signature";

        eventInformation["CorruptReason"].setString(tr("File has no signature"));
        return Processing_Corrupted;
    }
    eventInformation["AuthorizationDigest"].setString(authorizationDigest);

    std::unique_ptr<ExternalSink> archive(createArchiveTarget());
    archive->feedback.forward(feedback);

    SequenceFilter auth;
    auth.configure(ValueSegment::withPath(stationConfiguration,
                                          QString("Authorization/%1/Filter").arg(
                                                  transferUnpacker.getSigningCertificateID())),
                   {QString::fromStdString(station)});
    FilterShim filter(archive.get(), std::move(auth));
    DataUnpack dataUnpacker;
    Threading::Receiver rxData;
    if (canTerminate) {
        terminateRequested.connect(rxData, std::bind(&DataUnpack::signalTerminate, &dataUnpacker));
        terminateRequested.connect(rxData,
                                   std::bind(&ExternalSink::signalTerminate, archive.get()));
    }

    archive->start();
    if (canTerminate && testTerminated()) {
        archive->endData();
        archive->signalTerminate();
        return Processing_Error;
    }
    if (!dataUnpacker.exec(unpackedDataFile, &filter)) {
        qCDebug(log_component_process_incoming) << "Data unpacking failed";

        archive->wait();
        archive.reset();

        eventInformation["CorruptReason"].setString(dataUnpacker.errorString());
        return Processing_Corrupted;
    }
    unpackedDataFile.reset();

    archive->wait();
    archive.reset();

    filter.injectIntoEvent(eventInformation);
    return Processing_Accepted;
}

ProcessIncoming::ProcessingResult ProcessIncoming::acquireFile(DownloadFile *file,
                                                               Variant::Write &,
                                                               const Variant::Read &,
                                                               std::shared_ptr<
                                                                       IO::File::Backing> &localFile)
{
    localFile = IO::Access::file(file->fileInfo(), IO::File::Mode::readOnly());
    if (!localFile) {
        qCDebug(log_component_process_incoming) << "Failed to open local file";
        return Processing_Error;
    }
    return Processing_Accepted;
}

namespace {
class GlobalMetadataOverlay : public AsyncProcessingStage {
    std::deque<ValueSegment> overlay;
public:
    GlobalMetadataOverlay(const ValueSegment::Transfer &o) : overlay(o.begin(), o.end())
    { }

    virtual ~GlobalMetadataOverlay() = default;

protected:
    void process(SequenceValue::Transfer &&incoming) override
    {
        for (auto &value : incoming) {
            if (!value.getName().isMeta())
                continue;
            if (!Range::intersectShift(overlay, value))
                continue;

            AcquisitionVariableSet::applyValueOverlay(value, overlay.front().root());
        }
        egress->incomingData(std::move(incoming));
    }
};

class VariableMetadataOverlay : public AsyncProcessingStage {
    struct VariableMetadataConfiguration {
        SequenceMatch::Composite selection;
        ValueSegment overlay;
    };

    std::deque<VariableMetadataConfiguration> variableMetadata;

public:
    explicit VariableMetadataOverlay(const ValueSegment::Transfer &overlay)
    {
        for (const auto &segment : overlay) {
            switch (segment.value().getType()) {
            case Variant::Type::Hash:
                for (auto child : segment.read().toHash()) {
                    if (child.first.empty())
                        continue;
                    VariableMetadataConfiguration add;
                    add.selection.append(QString(), QString(), QString::fromStdString(child.first));
                    add.overlay = ValueSegment(segment.getStart(), segment.getEnd(),
                                               Variant::Root(child.second));
                    variableMetadata.emplace_back(std::move(add));
                }
                break;
            default:
                for (auto child : segment.read().toChildren()) {
                    VariableMetadataConfiguration add;
                    add.selection = SequenceMatch::Composite(child["Variable"]);
                    add.overlay = ValueSegment(segment.getStart(), segment.getEnd(),
                                               Variant::Root(child["Metadata"]));
                    variableMetadata.emplace_back(std::move(add));
                }
                break;
            }
        }
    }

    virtual ~VariableMetadataOverlay() = default;

protected:
    void process(SequenceValue::Transfer &&incoming) override
    {
        for (auto &value : incoming) {
            if (!value.getName().isMeta())
                continue;

            for (const auto &apply : variableMetadata) {
                if (!Range::intersects(value.getStart(), value.getEnd(), apply.overlay.getStart(),
                                       apply.overlay.getEnd()))
                    continue;
                if (!apply.selection.matches(value.getName()))
                    continue;

                AcquisitionVariableSet::applyValueOverlay(value, apply.overlay.root());
            }
        }
        egress->incomingData(std::move(incoming));
    }
};

class PipelineFilter : public AsyncProcessingStage {
    std::shared_ptr<ProcessingFilter> filter;
public:
    PipelineFilter(std::shared_ptr<ProcessingFilter> f) : filter(std::move(f))
    { }

    virtual ~PipelineFilter() = default;

protected:
    void process(SequenceValue::Transfer &&incoming) override
    {
        for (auto &value : incoming) {
            filter->incomingData(std::move(value), egress);
        }
    }

    void finish() override
    { filter->endData(egress); }
};

};

ProcessIncoming::ProcessingResult ProcessIncoming::configurePipeline(Variant::Write &eventInformation,
                                                                     const SequenceName::Component &station,
                                                                     const Variant::Read &converterConfiguration,
                                                                     const ValueSegment::Transfer &stationConfiguration,
                                                                     StreamPipeline &pipeline)
{
    Q_UNUSED(eventInformation);

    std::vector<Variant::Read> stages;
    switch (converterConfiguration["Processing"].getType()) {
    case Variant::Type::Hash:
        stages.emplace_back(converterConfiguration["Processing"]);
        break;
    case Variant::Type::String: {
        Variant::Root add;
        add.write().hash("Component").setString(converterConfiguration["Processing"].toString());
        stages.emplace_back(std::move(add));
        break;
    }
    default:
        Util::append(converterConfiguration["Processing"].toArray(), stages);
        break;
    }

    for (const auto &stage : stages) {
        QString componentName(stage["Component"].toQString());
        QObject *component = ComponentLoader::create(componentName);
        if (!component) {
            qCWarning(log_component_process_incoming) << "Can't load component" << componentName;
            return Processing_Error;
        }
        ProcessingStageComponent
                *processingStageComponent = qobject_cast<ProcessingStageComponent *>(component);
        if (!processingStageComponent) {
            qCWarning(log_component_process_incoming) << "Can't load filter component"
                                                      << componentName;
            return Processing_Error;
        }

        ComponentOptions o = processingStageComponent->getOptions();

        Variant::Root options(stage["Options"]);
        if (!options["profile"].exists()) {
            options["profile"].setString(profile);
        }
        if (!options["station"].exists()) {
            options["station"].setString(station);
        }
        ValueOptionParse::parse(o, options);
        std::unique_ptr<ProcessingStage>
                add(processingStageComponent->createGeneralFilterDynamic(o));
        if (!add) {
            qCWarning(log_component_process_incoming) << "Can't create component" << componentName;
            return Processing_Error;
        }

        if (!pipeline.addProcessingStage(std::move(add), processingStageComponent)) {
            qCWarning(log_component_process_incoming) << "Can't add component to pipeline"
                                                      << pipeline.getChainError();
            return Processing_Error;
        }
    }

    ValueSegment::Transfer overlayConfiguration;
    QString path
            (QString("Profiles/%1/Converter/GlobalMetadata").arg(QString::fromStdString(profile)));
    for (const auto &check : stationConfiguration) {
        auto cv = check[path];
        if (!cv.exists())
            continue;
        overlayConfiguration.emplace_back(check.getStart(), check.getEnd(), Variant::Root(cv));
    }
    if (!overlayConfiguration.empty()) {
        if (!pipeline.addProcessingStage(std::unique_ptr<ProcessingStage>(
                new GlobalMetadataOverlay(overlayConfiguration)))) {
            qCWarning(log_component_process_incoming) << "Can't add overlay to pipeline"
                                                      << pipeline.getChainError();
            return Processing_Error;
        }
    }

    overlayConfiguration.clear();
    path = QString("Profiles/%1/Converter/VariableMetadata").arg(QString::fromStdString(profile));
    for (const auto &check : stationConfiguration) {
        auto v = check[path];
        if (!v.exists())
            continue;
        overlayConfiguration.emplace_back(check.getStart(), check.getEnd(), Variant::Root(v));
    }
    if (!overlayConfiguration.empty()) {
        if (!pipeline.addProcessingStage(std::unique_ptr<ProcessingStage>(
                new VariableMetadataOverlay(overlayConfiguration)))) {
            qCWarning(log_component_process_incoming) << "Can't add overlay to pipeline"
                                                      << pipeline.getChainError();
            return Processing_Error;
        }
    }

    return Processing_Accepted;
}

ProcessIncoming::ProcessingResult ProcessIncoming::pipelineFileInput(DownloadFile *file,
                                                                     Variant::Write &eventInformation,
                                                                     const SequenceName::Component &station,
                                                                     const Variant::Read &converterConfiguration,
                                                                     const ValueSegment::Transfer &stationConfiguration,
                                                                     StreamPipeline &pipeline,
                                                                     std::shared_ptr<
                                                                             IO::File::Backing> &localFile)
{
    ProcessingResult
            result = acquireFile(file, eventInformation, converterConfiguration, localFile);
    if (result != Processing_Accepted) {
        localFile.reset();
        return result;
    }

    result = configurePipeline(eventInformation, station, converterConfiguration,
                               stationConfiguration, pipeline);
    if (result != Processing_Accepted) {
        localFile.reset();
        return result;
    }

    return Processing_Accepted;
}

ProcessIncoming::ProcessingResult ProcessIncoming::executePipeline(DownloadFile *file,
                                                                   Variant::Write &eventInformation,
                                                                   const SequenceName::Component &station,
                                                                   const Variant::Read &converterConfiguration,
                                                                   const ValueSegment::Transfer &stationConfiguration,
                                                                   StreamPipeline &pipeline,
                                                                   std::shared_ptr<
                                                                           IO::Process::Instance> &&process)
{
    bool limitEnd = !converterConfiguration["AllowUndefinedEnd"].toBool();

    double timeLimitStart = FP::undefined();
    double timeLimitEnd = FP::undefined();
    double fileTime = file->getFileTime();
    if (converterConfiguration["FileTimeLimit"].exists() && FP::defined(fileTime)) {
        timeLimitStart =
                Variant::Composite::offsetTimeInterval(converterConfiguration["FileTimeLimit"],
                                                       fileTime, false);
        timeLimitEnd =
                Variant::Composite::offsetTimeInterval(converterConfiguration["FileTimeLimit"],
                                                       fileTime, true);
        if (!FP::defined(timeLimitStart) ||
                !FP::defined(timeLimitEnd) ||
                timeLimitEnd <= timeLimitStart) {
            timeLimitStart = FP::undefined();
            timeLimitEnd = FP::undefined();
        }
    }

    std::shared_ptr<ProcessingFilter> outputFilter;
    {
        SequenceFilter filter;
        filter.configure(ValueSegment::withPath(stationConfiguration,
                                                QString("Profiles/%1/Converter/Filter").arg(
                                                        QString::fromStdString(profile))),
                         {QString::fromStdString(station)});
        filter.setDefaultAccept(true);
        outputFilter =
                std::make_shared<ProcessingFilter>(std::move(filter), timeLimitStart, timeLimitEnd,
                                                   limitEnd);
    }

    if (!pipeline.addProcessingStage(
            std::unique_ptr<ProcessingStage>(new PipelineFilter(outputFilter)))) {
        qCWarning(log_component_process_incoming) << "Can't add filter to pipeline"
                                                  << pipeline.getChainError();
        return Processing_Error;
    }

    if (!pipeline.setOutputGeneral(std::unique_ptr<ExternalSink>(createArchiveTarget()))) {
        qCWarning(log_component_process_incoming) << "Can't set archive pipeline target"
                                                  << pipeline.getOutputError();
        return Processing_Error;
    }

    Threading::Receiver receiver;
    QEventLoop loop;

    bool allowTerminate = !converterConfiguration["DisableTerminate"].toBool();
    if (allowTerminate) {
        terminateRequested.connect(receiver,
                                   std::bind(&StreamPipeline::signalTerminate, &pipeline));
        terminateRequested.connect(&loop, std::bind(&QEventLoop::quit, &loop), true);
        if (process) {
            terminateRequested.connect(receiver,
                                       std::bind(&IO::Process::Instance::kill, process.get()));
        }
    }
    pipeline.finished.connect(&loop, std::bind(&QEventLoop::quit, &loop), true);

    pipeline.feedback.forward(feedback);
    if (!pipeline.start()) {
        qCWarning(log_component_process_incoming) << "Error starting pipeline";
        return Processing_Error;
    }
    std::unique_ptr<IO::Process::Instance::ExitMonitor> monitor;
    if (process) {
        monitor.reset(new IO::Process::Instance::ExitMonitor(*process));
        process->start();
    }

    if (allowTerminate && testTerminated())
        return Processing_Error;

    loop.exec();

    if (!pipeline.wait()) {
        qCWarning(log_component_process_incoming) << "Error waiting for pipeline termination";
        return Processing_Error;
    }
    if (process) {
        process->wait();
        if (!monitor->success()) {
            qCWarning(log_component_process_incoming) << "Process did not complete successfully";
            if (!converterConfiguration["IgnoreExitStatus"].toBool()) {
                return Processing_Error;
            }
        }
        monitor.reset();
        /* Leave the process, since the receiver may still be connected */
    }

    if (!outputFilter->hadAnyData()) {
        if (!converterConfiguration["AcceptEmpty"].toBool()) {
            eventInformation["CorruptReason"].setString(tr("No values returned in the conversion"));
            return Processing_Corrupted;
        }
    }

    outputFilter->injectIntoEvent(eventInformation);

    return Processing_Accepted;
}

ProcessIncoming::ProcessingResult ProcessIncoming::pipelineFromComponent(Variant::Write &eventInformation,
                                                                         const SequenceName::Component &station,
                                                                         const Variant::Read &converterConfiguration,
                                                                         StreamPipeline &pipeline,
                                                                         const std::shared_ptr<
                                                                                 IO::File::Backing> &localFile)
{
    Q_UNUSED(eventInformation);

    QString componentName(converterConfiguration["Component"].toQString());
    if (componentName.isEmpty()) {
        qCWarning(log_component_process_incoming) << "Data component name is empty";
        return Processing_Error;
    }

    QObject *component = ComponentLoader::create(componentName);
    if (!component) {
        qCWarning(log_component_process_incoming) << "Can't load component" << componentName;
        return Processing_Error;
    }
    ExternalConverterComponent *ingress = qobject_cast<ExternalConverterComponent *>(component);
    if (!ingress) {
        qCWarning(log_component_process_incoming) << "Can't load ingress component"
                                                  << componentName;
        return Processing_Error;
    }

    ComponentOptions o = ingress->getOptions();

    Variant::Root options(converterConfiguration["Options"]);
    if (!options["profile"].exists()) {
        options["profile"].setString(profile);
    }
    if (!options["station"].exists()) {
        options["station"].setString(station);
    }
    ValueOptionParse::parse(o, options);
    std::unique_ptr<ExternalConverter> input(ingress->createDataIngress(o));
    if (!input) {
        qCWarning(log_component_process_incoming) << "Can't create component" << componentName;
        return Processing_Error;
    }

    auto fileStream = localFile->stream();
    if (!fileStream) {
        qCWarning(log_component_process_incoming) << "Can't get input file stream";
        return Processing_Error;
    }
    if (!pipeline.setInputGeneral(std::move(input), std::move(fileStream), false)) {
        qCWarning(log_component_process_incoming) << "Can't set pipeline input"
                                                  << pipeline.getInputError();
        return Processing_Error;
    }

    return Processing_Accepted;
}

namespace {
class AcquisitionStateWrapper : public ExternalConverter {
    std::unique_ptr<ExternalConverter> input;
    AcquisitionInterface *acquisition;
    std::shared_ptr<ProcessIncomingAcquisitionState> state;
public:
    AcquisitionStateWrapper(AcquisitionInterface *acq,
                            std::unique_ptr<ExternalConverter> &&in,
                            const std::shared_ptr<ProcessIncomingAcquisitionState> &st) : input(
            std::move(in)), acquisition(acq), state(st)
    {
        accelerationKey = input->accelerationKey;
        finished = input->finished;
    }

    virtual ~AcquisitionStateWrapper()
    {
        QByteArray state;
        {
            QDataStream stream(&state, QIODevice::WriteOnly);
            acquisition->serializeState(stream);
        }
        this->state->state = state;

        input.reset();
    }

    void setEgress(StreamSink *setEgress) override
    { input->setEgress(setEgress); }

    void incomingData(const Util::ByteView &data) override
    { input->incomingData(data); }

    void incomingData(const Util::ByteArray &data) override
    { input->incomingData(data); }

    void incomingData(Util::ByteArray &&data) override
    { input->incomingData(std::move(data)); }

    void incomingData(const QByteArray &data) override
    { input->incomingData(data); }

    void incomingData(QByteArray &&data) override
    { input->incomingData(std::move(data)); }

    void endData() override
    { input->endData(); }

    void signalTerminate() override
    { input->signalTerminate(); }

    bool isFinished() override
    { return input->isFinished(); }

    void start() override
    { input->start(); }

    bool wait(double timeout = FP::undefined()) override
    { return input->wait(timeout); }

    SequenceName::Set predictedOutputs() override
    { return input->predictedOutputs(); }
};
}

ProcessIncomingAcquisitionState::ProcessIncomingAcquisitionState()
        : state(), lastFileTime(FP::undefined()), referenced(false)
{ }

static const quint8 acquisitionStateVersion = 1;

QByteArray ProcessIncoming::saveAcquisitionState(bool pruneUnused) const
{
    QByteArray result;
    {
        QDataStream stream(&result, QIODevice::WriteOnly);
        stream << acquisitionStateVersion;

        quint32 n = (quint32) acquisitionState.size();
        if (pruneUnused) {
            for (const auto &add : acquisitionState) {
                if (!add.second->referenced)
                    n--;
            }
        }

        stream << n;
        for (const auto &add : acquisitionState) {
            if (pruneUnused && !add.second->referenced)
                continue;

            stream << add.first;

            stream << add.second->lastFileTime;
            stream << add.second->state;
        }
    }
    return result;
}

void ProcessIncoming::restoreAcquisitionState(const QByteArray &state)
{
    acquisitionState.clear();

    QDataStream stream(state);
    quint8 version = 0;
    stream >> version;
    if (version != acquisitionStateVersion)
        return;

    quint32 n = 0;
    stream >> n;
    for (quint32 i = 0; i < n; i++) {
        QString key;
        stream >> key;

        auto value = std::make_shared<ProcessIncomingAcquisitionState>();
        stream >> value->lastFileTime;
        stream >> value->state;

        acquisitionState.emplace(std::move(key), std::move(value));
    }
}


ProcessIncoming::ProcessingResult ProcessIncoming::pipelineFromAcquisition(DownloadFile *file,
                                                                           Variant::Write &eventInformation,
                                                                           const SequenceName::Component &station,
                                                                           const Variant::Read &converterConfiguration,
                                                                           const ValueSegment::Transfer &stationConfiguration,
                                                                           StreamPipeline &pipeline,
                                                                           const std::shared_ptr<
                                                                                   IO::File::Backing> &localFile)
{
    Q_UNUSED(eventInformation);

    QString componentName(converterConfiguration["Component"].toQString());
    if (componentName.isEmpty()) {
        qCWarning(log_component_process_incoming) << "Data component name is empty";
        return Processing_Error;
    }

    QObject *component = ComponentLoader::create(componentName);
    if (!component) {
        qCWarning(log_component_process_incoming) << "Can't load component" << componentName;
        return Processing_Error;
    }
    AcquisitionComponent *acquisitionComponent = qobject_cast<AcquisitionComponent *>(component);
    if (!acquisitionComponent) {
        qCWarning(log_component_process_incoming) << "Can't load acquisition component"
                                                  << componentName;
        return Processing_Error;
    }

    ValueSegment::Transfer componentConfig;
    double fileTime = file->getFileTime();
    double fileDiscardPoint = Variant::Composite::offsetTimeInterval(
            converterConfiguration["InitialConfigurationTimeOffset"], fileTime, false);
    for (const auto &add : stationConfiguration) {
        if (FP::defined(fileDiscardPoint) &&
                FP::defined(add.getEnd()) &&
                fileDiscardPoint >= add.getEnd())
            continue;

        Variant::Root cfg(add.getValue()
                             .hash("Profiles")
                             .hash(profile)
                             .hash("Converter")
                             .hash("Acquisition"));
        if (!cfg["BaseTime"].exists())
            cfg["BaseTime"].setDouble(fileTime);
        componentConfig.emplace_back(add.getStart(), add.getEnd(), std::move(cfg));
    }

    auto acquisition = acquisitionComponent->createAcquisitionPassive(componentConfig);
    if (!acquisition) {
        qCWarning(log_component_process_incoming) << "Can't create component " << componentName;
        return Processing_Error;
    }

    std::shared_ptr<ProcessIncomingAcquisitionState> state;
    {
        auto check = acquisitionState.find(componentName);
        if (check != acquisitionState.end())
            state = check->second;
    }
    if (!state) {
        state = std::make_shared<ProcessIncomingAcquisitionState>();
    }
    state->referenced = true;
    bool saveState = false;
    if (!FP::defined(state->lastFileTime) ||
            !FP::defined(fileTime) ||
            fileTime >= state->lastFileTime) {
        {
            QDataStream stream(&state->state, QIODevice::ReadOnly);
            acquisition->deserializeState(stream);
        }
        if (FP::defined(fileTime))
            state->lastFileTime = fileTime;
        saveState = true;
    }

    ComponentOptions o;
    LineIngressWrapper::addOptions(o, true);
    Variant::Root options(converterConfiguration["Options"]);
    if (!options["profile"].exists()) {
        options["profile"].setString(profile);
    }
    if (!options["station"].exists()) {
        options["station"].setString(station);
    }
    if (!options["basetime"].exists()) {
        options["basetime"].setDouble(fileTime);
    }
    ValueOptionParse::parse(o, options);
    auto acquisitionPtr = acquisition.get();
    std::unique_ptr<ExternalConverter>
            input(LineIngressWrapper::create(std::move(acquisition), o, true));
    if (!input) {
        qCWarning(log_component_process_incoming) << "Can't create wrapper" << componentName;
        return Processing_Error;
    }

    if (saveState) {
        input.reset(new AcquisitionStateWrapper(acquisitionPtr, std::move(input), state));
        Q_ASSERT(input);
    }

    auto fileStream = localFile->stream();
    if (!fileStream) {
        qCWarning(log_component_process_incoming) << "Can't get input file stream";
        return Processing_Error;
    }
    if (!pipeline.setInputGeneral(std::move(input), std::move(fileStream), false)) {
        qCWarning(log_component_process_incoming) << "Can't set pipeline input"
                                                  << pipeline.getInputError();
        return Processing_Error;
    }

    return Processing_Accepted;
}

IO::Process::Spawn ProcessIncoming::programSpawn(const Variant::Read &converterConfiguration)
{
    QString program;
    QStringList arguments;

    auto pcfg = converterConfiguration["Program"];
    bool wasSingleString = false;
    switch (pcfg.getType()) {
    case Variant::Type::String:
        program = substitutions.apply(pcfg.toQString());
        wasSingleString = true;
        break;
    case Variant::Type::Hash:
        program = substitutions.apply(pcfg["Program"].toQString());
        for (auto add : pcfg["Arguments"].toArray()) {
            arguments.append(substitutions.apply(add.toQString()));
        }
        break;
    default:
        for (auto add : pcfg.toChildren()) {
            arguments.append(substitutions.apply(add.toQString()));
        }
        if (!arguments.isEmpty())
            program = arguments.takeFirst();
        break;
    }

    for (auto add : converterConfiguration["Arguments"].toArray()) {
        arguments.append(substitutions.apply(add.toQString()));
    }

    if (wasSingleString && arguments.isEmpty()) {
        return IO::Process::Spawn::shell(program);
    }

    return IO::Process::Spawn(program, arguments);
}

ProcessIncoming::ProcessingResult ProcessIncoming::pipelineFromProgram(Variant::Write &eventInformation,
                                                                       const SequenceName::Component &station,
                                                                       const Variant::Read &converterConfiguration,
                                                                       StreamPipeline &pipeline,
                                                                       const std::shared_ptr<
                                                                               IO::File::Backing> &localFile,
                                                                       std::shared_ptr<
                                                                               CPD3::IO::Process::Instance> &process)
{
    bool useInputFile = converterConfiguration["InputFile"].toBool();

    std::unique_ptr<IO::Generic::Stream> stream;

    bool canAccelerate = false;
    if (converterConfiguration["OutputFile"].toBool()) {
        auto outputFile = IO::Access::temporaryFile();
        if (!outputFile) {
            qCWarning(log_component_process_incoming) << "Error opening process output file";
            return Processing_Error;
        }

        TextSubstitutionStack::Context subctx(substitutions);
        substitutions.setFile("output", QFileInfo(QString::fromStdString(outputFile->filename())));
        if (useInputFile) {
            substitutions.setFile("input",
                                  QFileInfo(QString::fromStdString(localFile->filename())));
        }

        auto spawn = programSpawn(converterConfiguration);
        spawn.outputStream = IO::Process::Spawn::StreamMode::Discard;
        spawn.errorStream = IO::Process::Spawn::StreamMode::Forward;
        if (useInputFile) {
            spawn.inputStream = IO::Process::Spawn::StreamMode::Discard;
        } else {
            spawn.inputStream = IO::Process::Spawn::StreamMode::File;
            spawn.inputFile = localFile->filename();
        }

        process = spawn.create();
        if (!process) {
            qCWarning(log_component_process_incoming) << "Error creating process";
            return Processing_Error;
        }
        {
            IO::Process::Instance::ExitMonitor monitor(*process);
            if (!process->start()) {
                qCWarning(log_component_process_incoming) << "Error starting conversion program";
                return Processing_Error;
            }

            if (!converterConfiguration["DisableTerminate"].toBool()) {
                monitor.connectTerminate(terminateRequested);
                if (testTerminated()) {
                    process->terminate();
                    return Processing_Error;
                }
            }

            process->wait();
            if (!monitor.success()) {
                qCWarning(log_component_process_incoming)
                    << "Process did not complete successfully";
                if (!converterConfiguration["IgnoreExitStatus"].toBool()) {
                    return Processing_Error;
                }
            }
        }
        process.reset();

        stream = outputFile->stream();
        if (!stream) {
            qCWarning(log_component_process_incoming) << "Can't get process output file stream";
            return Processing_Error;
        }
        canAccelerate = false;
    } else {
        TextSubstitutionStack::Context subctx(substitutions);
        if (useInputFile) {
            substitutions.setFile("input",
                                  QFileInfo(QString::fromStdString(localFile->filename())));
        }

        auto spawn = programSpawn(converterConfiguration);
        spawn.errorStream = IO::Process::Spawn::StreamMode::Forward;
        spawn.outputStream = IO::Process::Spawn::StreamMode::Capture;
        if (useInputFile) {
            spawn.inputStream = IO::Process::Spawn::StreamMode::Discard;
        } else {
            spawn.inputStream = IO::Process::Spawn::StreamMode::File;
            spawn.inputFile = localFile->filename();
        }

        process = spawn.create();
        if (!process) {
            qCWarning(log_component_process_incoming) << "Error creating process";
            return Processing_Error;
        }

        stream = process->outputStream();
        if (!stream) {
            qCWarning(log_component_process_incoming) << "Error getting process output stream";
            return Processing_Error;
        }
        canAccelerate = true;
    }

    Q_ASSERT(stream);
    if (!pipeline.setInputGeneral(std::unique_ptr<ExternalConverter>(new StandardDataInput),
                                  std::move(stream), canAccelerate)) {
        qCWarning(log_component_process_incoming) << "Can't set pipeline input"
                                                  << pipeline.getInputError();
        return Processing_Error;
    }
    return Processing_Accepted;
}

ProcessIncoming::ProcessingResult ProcessIncoming::executeStandalone(Variant::Write &eventInformation,
                                                                     const Variant::Read &converterConfiguration,
                                                                     const std::shared_ptr<
                                                                             IO::File::Backing> &localFile)
{
    Q_ASSERT(localFile);

    bool useInputFile = converterConfiguration["InputFile"].toBool();

    TextSubstitutionStack::Context subctx(substitutions);
    if (useInputFile) {
        substitutions.setFile("input", QFileInfo(QString::fromStdString(localFile->filename())));
    }

    auto spawn = programSpawn(converterConfiguration);
    spawn.errorStream = IO::Process::Spawn::StreamMode::Forward;
    spawn.outputStream = IO::Process::Spawn::StreamMode::Forward;
    if (useInputFile) {
        spawn.inputStream = IO::Process::Spawn::StreamMode::Discard;
    } else {
        spawn.inputStream = IO::Process::Spawn::StreamMode::File;
        spawn.inputFile = localFile->filename();
    }

    auto process = spawn.create();
    int code = -1;
    {
        IO::Process::Instance::ExitMonitor monitor(*process);
        if (!process->start()) {
            qCWarning(log_component_process_incoming) << "Error starting conversion program";
            return Processing_Error;
        }

        if (!converterConfiguration["DisableTerminate"].toBool()) {
            monitor.connectTerminate(terminateRequested);
            if (testTerminated()) {
                process->terminate();
                return Processing_Error;
            }
        }

        process->wait();
        if (!monitor.isNormal()) {
            qCWarning(log_component_process_incoming) << "Process did not exit normally";
            return Processing_Error;
        }
        code = monitor.getExitCode();
    }
    process.reset();

    if (converterConfiguration["ExitCode"].exists()) {
        auto mapping = converterConfiguration["ExitCode"].toHash();
        auto check = mapping[QString::number(code)];
        if (!check.exists()) {
            if (code != 0) {
                check = mapping["Error"];
            } else {
                check = mapping["Normal"];
            }
        }
        const auto &operation = check.toString();
        if (Util::equal_insensitive(operation, "error")) {
            qCDebug(log_component_process_incoming) << "Process exit code " << code
                                                    << "interpreted as an error";
            return Processing_Error;
        } else if (Util::equal_insensitive(operation, "corrupt", "corrupted")) {
            qCDebug(log_component_process_incoming) << "Process exit code" << code
                                                    << "interpreted as corruption";
            eventInformation["CorruptReason"].setString(
                    tr("Process exited with code %1").arg(code));
            return Processing_Corrupted;
        } else if (Util::equal_insensitive(operation, "unauthorized")) {
            qCDebug(log_component_process_incoming) << "Process exit code" << code
                                                    << "interpreted as an unauthorized file";
            return Processing_Unauthorized;
        }
        return Processing_Accepted;
    }

    if (code != 0) {
        qCDebug(log_component_process_incoming) << "Process exited with code" << code;
        return Processing_Error;
    }

    return Processing_Accepted;
}

ProcessIncoming::ProcessingResult ProcessIncoming::processFile(DownloadFile *file,
                                                               Variant::Write &eventInformation,
                                                               const SequenceName::Component &station,
                                                               const Variant::Read &currentConfiguration,
                                                               const ValueSegment::Transfer &stationConfiguration,
                                                               bool isFirstFile)
{
    QFileInfo local(file->fileInfo());
    qCDebug(log_component_process_incoming) << "Processing file" << file->fileName()
                                            << "from local file" << local.absoluteFilePath();

    feedback.emitStage(tr("Process %1").arg(file->fileName()),
                       tr("The system is processing %1.").arg(file->fileName()), true);

    substitutions.setString("name", file->fileName());
    substitutions.setFile("file", local);
    substitutions.setFile("local", local);
    substitutions.setTime("filetime", file->getFileTime());
    substitutions.setTime("modified", file->getModifiedTime());

    eventInformation.hash("Local").setString(local.absoluteFilePath());
    eventInformation.hash("BaseName").setString(file->fileName());
    eventInformation.hash("Matched").setString(file->getPattern());
    eventInformation.hash("Writable").setBool(file->writable());
    eventInformation.hash("ModifiedTime").setDouble(file->getModifiedTime());
    eventInformation.hash("FileTime").setDouble(file->getFileTime());

    if (!executeActions(currentConfiguration["Actions/File"], isFirstFile))
        return Processing_Error;

    Variant::Read fileConfiguration = Variant::Read::empty();
    {
        double fileTime = file->getFileTime();
        if (!FP::defined(fileTime))
            fileTime = file->getModifiedTime();
        if (!FP::defined(fileTime))
            fileTime = Time::time();
        auto active = Range::findIntersecting(stationConfiguration, fileTime);
        if (active == stationConfiguration.end())
            fileConfiguration = stationConfiguration.back().getValue();
        else
            fileConfiguration = active->getValue();
        fileConfiguration = fileConfiguration.hash("Profiles").hash(profile);
    }

    if (!executeActions(fileConfiguration["Actions/Local"], isFirstFile))
        return Processing_Error;
    local.refresh();

    eventInformation.hash("Size").setInt64(local.size());

    auto converterConfiguration = fileConfiguration.hash("Converter");

    const auto &type = converterConfiguration["Type"].toString();
    if (Util::equal_insensitive(type, "data", "ingress", "converter")) {
        std::shared_ptr<IO::File::Backing> localFile;
        StreamPipeline pipeline;
        ProcessingResult result =
                pipelineFileInput(file, eventInformation, station, converterConfiguration,
                                  stationConfiguration, pipeline, localFile);
        if (result != Processing_Accepted)
            return result;
        Q_ASSERT(localFile);

        result = pipelineFromComponent(eventInformation, station, converterConfiguration, pipeline,
                                       localFile);
        if (result != Processing_Accepted)
            return result;

        return executePipeline(file, eventInformation, station, converterConfiguration,
                               stationConfiguration, pipeline);
    } else if (Util::equal_insensitive(type, "program", "process", "external")) {
        std::shared_ptr<IO::File::Backing> localFile;
        StreamPipeline pipeline;
        std::shared_ptr<IO::Process::Instance> process;
        ProcessingResult result =
                pipelineFileInput(file, eventInformation, station, converterConfiguration,
                                  stationConfiguration, pipeline, localFile);
        if (result != Processing_Accepted)
            return result;
        Q_ASSERT(localFile);

        result = pipelineFromProgram(eventInformation, station, converterConfiguration, pipeline,
                                     localFile, process);
        if (result != Processing_Accepted)
            return result;

        return executePipeline(file, eventInformation, station, converterConfiguration,
                               stationConfiguration, pipeline, std::move(process));
    } else if (Util::equal_insensitive(type, "acquisition")) {
        std::shared_ptr<IO::File::Backing> localFile;
        StreamPipeline pipeline;
        ProcessingResult result =
                pipelineFileInput(file, eventInformation, station, converterConfiguration,
                                  stationConfiguration, pipeline, localFile);
        if (result != Processing_Accepted)
            return result;
        Q_ASSERT(localFile);

        result = pipelineFromAcquisition(file, eventInformation, station, converterConfiguration,
                                         stationConfiguration, pipeline, localFile);
        if (result != Processing_Accepted)
            return result;

        return executePipeline(file, eventInformation, station, converterConfiguration,
                               stationConfiguration, pipeline);
    } else if (Util::equal_insensitive(type, "standalone")) {
        std::shared_ptr<IO::File::Backing> localFile;
        ProcessingResult result = acquireFile(file, eventInformation, fileConfiguration, localFile);
        if (result != Processing_Accepted)
            return result;
        Q_ASSERT(localFile);

        return executeStandalone(eventInformation, converterConfiguration, localFile);
    } else if (Util::equal_insensitive(type, "ignore")) {
        return Processing_Accepted;
    }

    return processDataFile(file, eventInformation, station, fileConfiguration, stationConfiguration,
                           isFirstFile);
}