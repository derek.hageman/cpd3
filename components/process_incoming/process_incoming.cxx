/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <limits.h>
#include <QStringList>
#include <QDir>
#include <QLoggingCategory>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "core/range.hxx"
#include "core/environment.hxx"
#include "core/process.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "database/runlock.hxx"
#include "datacore/archive/access.hxx"
#include "io/process.hxx"

#include "process_incoming.hxx"


Q_LOGGING_CATEGORY(log_component_process_incoming, "cpd3.component.process.incoming", QtWarningMsg)


using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Transfer;

ProcessIncoming::ProcessIncoming() : profile(SequenceName::impliedProfile()),
                                     stations(),
                                     lockTimeout(30.0),
                                     afterTime(FP::undefined()),
                                     haveAfter(false),
                                     terminated(false),
                                     mutex(),
                                     substitutions()
{ }

ProcessIncoming::ProcessIncoming(const ComponentOptions &options,
                                 const std::vector<SequenceName::Component> &setStations) : profile(
        SequenceName::impliedProfile()),
                                                                                            stations(
                                                                                                    setStations),
                                                                                            lockTimeout(
                                                                                                    30.0),
                                                                                            afterTime(
                                                                                                    FP::undefined()),
                                                                                            haveAfter(
                                                                                                    false),
                                                                                            terminated(
                                                                                                    false),
                                                                                            mutex(),
                                                                                            substitutions()
{
    if (options.isSet("profile")) {
        profile = qobject_cast<ComponentOptionSingleString *>(options.get("profile"))->get()
                                                                                     .toLower()
                                                                                     .toStdString();
    }
    if (options.isSet("timeout")) {
        lockTimeout = qobject_cast<ComponentOptionSingleDouble *>(options.get("timeout"))->get();
    }
    if (options.isSet("after")) {
        haveAfter = true;
        afterTime = qobject_cast<ComponentOptionSingleTime *>(options.get("after"))->get();
    }
}

ProcessIncoming::~ProcessIncoming()
{
}

void ProcessIncoming::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    terminateRequested();
}

bool ProcessIncoming::testTerminated()
{
    std::lock_guard<std::mutex> lock(mutex);
    return terminated;
}


static bool fileSortCompare(const std::unique_ptr<DownloadFile> &a, const std::unique_ptr<DownloadFile> &b)
{
    double ftA = a->getFileTime();
    double ftB = b->getFileTime();
    if (FP::defined(ftA)) {
        if (FP::defined(ftB)) {
            if (ftA != ftB)
                return ftA < ftB;
        } else {
            return false;
        }
    } else if (FP::defined(ftB)) {
        return true;
    }

    double mtA = a->getModifiedTime();
    double mtB = b->getModifiedTime();
    if (FP::defined(mtA)) {
        if (FP::defined(mtB)) {
            if (mtA != mtB)
                return mtA < mtB;
        } else {
            return false;
        }
    } else if (FP::defined(mtB)) {
        return true;
    }

    return a->fileName() < b->fileName();
}

bool ProcessIncoming::invokeCommand(const QString &program,
                                    const QStringList &arguments,
                                    bool canTerminate)
{
    qCDebug(log_component_process_incoming) << "Invoking:" << program << arguments;

    auto proc = IO::Process::Spawn(program, arguments).forward().create();
    if (!proc)
        return false;

    IO::Process::Instance::ExitMonitor exitOk(*proc);
    exitOk.connectTerminate(terminateRequested);
    if (canTerminate && testTerminated())
        return false;

    if (!proc->start() || !proc->wait())
        return false;
    return exitOk.success();
}

bool ProcessIncoming::executeActions(const Variant::Read &config, bool canTerminate)
{
    switch (config.getType()) {
    case Variant::Type::String: {
        QStringList arguments(substitutions.apply(config.toQString())
                                           .split(QRegExp("\\s+"), QString::SkipEmptyParts));
        if (arguments.isEmpty()) {
            qCWarning(log_component_process_incoming) << "Empty command as the only action";
            return true;
        }
        QString program(arguments.takeFirst());

        feedback.emitStage(tr("Execute External Program"),
                           tr("An external program is currently being executed."), false);
        feedback.emitState(program);
        if (!invokeCommand(program, arguments, canTerminate)) {
            feedback.emitFailure();
            return false;
        }
        break;
    }

    case Variant::Type::Array: {
        auto children = config.toArray();
        if (children.empty())
            return true;

        feedback.emitStage(tr("Execute External Program"),
                           tr("An external program is currently being executed."), false);

        for (auto child : children) {
            QString program(substitutions.apply(child.hash("Program").toQString()));
            if (program.isEmpty()) {
                qCWarning(log_component_process_incoming) << "Empty command in action list";
                continue;
            }

            QStringList arguments;
            for (auto add : child.hash("Arguments").toArray()) {
                arguments.append(substitutions.apply(add.toQString()));
            }

            feedback.emitState(program);
            bool status = invokeCommand(program, arguments,
                                        canTerminate && child.hash("CanTerminate").toBool());
            if (!status && !child.hash("AllowFailure").toBool()) {
                qCDebug(log_component_process_incoming) << "Execution of" << program << "failed";
                feedback.emitFailure();
                return false;
            }
        }

        break;
    }

    case Variant::Type::Hash: {
        QString program(substitutions.apply(config["Program"].toQString()));
        if (program.isEmpty()) {
            qCWarning(log_component_process_incoming) << "Empty command in action list";
            return true;
        }

        feedback.emitStage(tr("Execute External Program"),
                           tr("An external program is currently being executed."), false);

        QStringList arguments;
        for (auto add : config["Arguments"].toArray()) {
            arguments.append(substitutions.apply(add.toQString()));
        }

        feedback.emitState(program);
        bool status = invokeCommand(program, arguments,
                                    canTerminate && config.hash("CanTerminate").toBool());
        if (!status && !config["AllowFailure"].toBool()) {
            qCDebug(log_component_process_incoming) << "Execution of" << program << "failed";
            feedback.emitFailure();
            return false;
        }
    }

    default:
        break;
    }

    return true;
}


ProcessIncoming::IncomingDownloader::IncomingDownloader(ProcessIncoming *p) : parent(p)
{ }

ProcessIncoming::IncomingDownloader::~IncomingDownloader() = default;

QString ProcessIncoming::IncomingDownloader::applySubstitutions(const QString &input) const
{ return parent->substitutions.apply(input); }

QString ProcessIncoming::IncomingDownloader::applyMatching(const QString &input,
                                                           QList<FileDownloader::TimeCapture> &captures) const
{
    MatchingStack subs(parent->substitutions, captures);
    return subs.apply(input);
}

void ProcessIncoming::IncomingDownloader::pushTemporaryOutput(const QFileInfo &fileName)
{
    parent->substitutions.push();
    parent->substitutions.setFile("file", fileName);
}

void ProcessIncoming::IncomingDownloader::popTemporaryOutput()
{ parent->substitutions.pop(); }

void ProcessIncoming::IncomingDownloader::pushFilterDirectory(const QDir &dir, const QDir &rootDir)
{
    parent->substitutions.push();
    parent->substitutions.setFile("directory", dir.absolutePath());
    parent->substitutions.setFile("rootdirectory", rootDir.absolutePath());
}

void ProcessIncoming::IncomingDownloader::pushFilterRemote(const std::string &path,
                                                           const std::string &rootPath)
{
    parent->substitutions.push();
    parent->substitutions.setString("directory", QString::fromStdString(path));
    parent->substitutions.setString("rootdirectory", QString::fromStdString(rootPath));
}

void ProcessIncoming::IncomingDownloader::popFilter()
{ parent->substitutions.pop(); }

QString ProcessIncoming::IncomingDownloader::applyInstanceMatching(ProcessIncoming *parent,
                                                                   const QString &input,
                                                                   int &groupIndex)
{
    QList<FileDownloader::TimeCapture> captures;
    MatchingStack subs(parent->substitutions, captures);
    subs.instanceCapture = &groupIndex;
    return subs.apply(input);
}


ProcessIncoming::IncomingDownloader::MatchingStack::MatchingStack(const CPD3::TextSubstitutionStack &base,
                                                                  QList<TimeCapture> &caps)
        : TextSubstitutionStack(base), captures(caps), instanceCapture(NULL)
{ }

ProcessIncoming::IncomingDownloader::MatchingStack::~MatchingStack()
{ }

QString ProcessIncoming::IncomingDownloader::MatchingStack::substitution(const QStringList &elements) const
{
    QString check(getTimeCapture(elements, captures));
    if (!check.isEmpty())
        return check;
    QString name(elements.first().toLower());
    if (name == "instance") {
        if (instanceCapture == NULL)
            return "\\d+";
        *instanceCapture = captures.size() + 1;
        captures.append(FileDownloader::TimeCapture::Ignore);
        return "(\\d+)";
    } else if (name == "uid") {
        return "[a-zA-Z0-9]{8}";
    }
    return QRegExp::escape(TextSubstitutionStack::substitution(elements));
}


void ProcessIncoming::run()
{
    if (testTerminated())
        return;

    profile = Util::to_lower(std::move(profile));
    if (profile.empty()) {
        qCDebug(log_component_process_incoming) << "Invalid profile";
        return;
    }

    double start = Time::time();

    SequenceSegment globalConfiguration;
    {
        Archive::Access access;
        Archive::Access::ReadLock lock(access);

        auto allStations = access.availableStations(stations);
        if (allStations.empty()) {
            qCDebug(log_component_process_incoming) << "No stations available";
            return;
        }
        stations.clear();
        std::copy(allStations.begin(), allStations.end(), Util::back_emplacer(stations));

        {
            auto tnow = Time::time();
            auto confList = SequenceSegment::Stream::read(
                    Archive::Selection(tnow - 1.0, tnow + 1.0, stations, {"configuration"},
                                       {"processing"}), &access);
            auto f = Range::findIntersecting(confList, tnow);
            if (f == confList.end()) {
                qCDebug(log_component_process_incoming) << "No configuration for" << stations;
                return;
            }
            globalConfiguration = *f;
        }
    }
    std::sort(stations.begin(), stations.end());

    if (testTerminated())
        return;

    qCDebug(log_component_process_incoming) << "Starting processing for" << stations.size()
                                            << "station(s) on profile" << profile;

    for (const auto &station : stations) {
        if (testTerminated())
            break;

        acquisitionState.clear();

        substitutions.clear();
        substitutions.setString("station", QString::fromStdString(station).toLower());
        substitutions.setString("profile", QString::fromStdString(profile));

        double stationStart = Time::time();

        Variant::Read currentConfiguration =
                globalConfiguration.takeValue(SequenceName(station, "configuration", "processing"))
                                   .read()
                                   .hash("Profiles")
                                   .hash(profile);
        if (!currentConfiguration.exists()) {
            qCDebug(log_component_process_incoming) << "No active configuration for" << station;
            continue;
        }

        ValueSegment::Transfer stationConfiguration = ValueSegment::Stream::read(
                Archive::Selection(FP::undefined(), FP::undefined(), {station}, {"configuration"},
                                   {"processing"}));
        if (stationConfiguration.empty()) {
            qCDebug(log_component_process_incoming) << "No configuration for" << station;
            continue;
        }

        feedback.emitStage(tr("Lock %1").arg(QString::fromStdString(station).toUpper()),
                           tr("The system is acquiring an exclusive lock for %1.").arg(
                                   QString::fromStdString(station).toUpper()), false);

        QString rcKey(QString("processincoming %1 %2").arg(QString::fromStdString(profile),
                                                           QString::fromStdString(station)));
        QString rcLog(QString("processlog %1").arg(QString::fromStdString(station)));

        qCDebug(log_component_process_incoming) << "Locking" << station << "/" << profile;

        Database::RunLock rc;
        bool ok = false;
        double lastRunTime = rc.acquire(rcKey, lockTimeout, &ok);
        if (ok) {
            rc.acquire(rcLog, lockTimeout, &ok);
            if (!ok) {
                qCDebug(log_component_process_incoming) << "Processing log lock failed for"
                                                        << station;
                rc.fail();
            } else {
                qCDebug(log_component_process_incoming) << "Starting processing for" << station
                                                        << " with last run"
                                                        << Logging::time(lastRunTime);
            }
        }
        if (!ok) {
            feedback.emitFailure();
            qCDebug(log_component_process_incoming) << "Processing skipped for" << station;
            continue;
        }

        IncomingDownloader downloader(this);
        downloader.feedback.forward(feedback);
        Threading::Receiver rx;
        terminateRequested.connect(rx,
                                   std::bind(&IncomingDownloader::signalTerminate, &downloader));

        if (testTerminated()) {
            rc.fail();
            break;
        }

        if (haveAfter)
            lastRunTime = afterTime;

        static const quint8 rcStateVersion = 1;
        {
            QByteArray data(rc.get(rcKey));
            if (!data.isEmpty() && !haveAfter) {
                QDataStream stream(&data, QIODevice::ReadOnly);
                quint8 version = 0;
                stream >> version;

                if (stream.status() != QDataStream::Ok || version != rcStateVersion) {
                    qCDebug(log_component_process_incoming) << "Invalid state data";
                } else {
                    {
                        QByteArray downloaderState;
                        stream >> downloaderState;
                        downloader.restoreState(downloaderState);
                    }
                    {
                        QByteArray acqState;
                        stream >> acqState;
                        restoreAcquisitionState(acqState);
                    }
                }
            }
        }

        static const quint8 rcLogVersion = 1;

        std::vector<Variant::Root> log;
        {
            QByteArray data(rc.get(rcLog));
            if (data.isEmpty()) {
                log.clear();
            } else {
                QDataStream stream(&data, QIODevice::ReadOnly);
                quint8 version = 0;
                stream >> version >> log;
                if (stream.status() != QDataStream::Ok || version != rcLogVersion) {
                    log.clear();
                    qCDebug(log_component_process_incoming) << "Invalid log data";
                }
            }
        }

        double processEndTime = rc.startTime();

        substitutions.setTime("time", processEndTime, false);
        substitutions.setTime("end", processEndTime);
        substitutions.setTime("lasttime", lastRunTime, false);
        substitutions.setTime("start", lastRunTime, false);

        if (haveAfter) {
            if (!executeActions(currentConfiguration["Actions/Reprocess"], true)) {
                rc.fail();
                break;
            }
        }

        if (!executeActions(currentConfiguration["Actions/Station"], true)) {
            rc.fail();
            break;
        }

        double downloadStart = Time::time();

        downloader.setupExecutionBounds(lastRunTime, rc.startTime());
        if (!downloader.exec(currentConfiguration["Sources"])) {
            rc.fail();
            qCDebug(log_component_process_incoming)
                << "Processing aborted due to failure in file location";
            continue;
        }

        double downloadEnd = Time::time();
        qCDebug(log_component_process_incoming) << "File location completed in"
                                                << Logging::elapsed(downloadEnd - downloadStart);

        auto fileToProcess = downloader.takeFiles();
        int totalFiles = fileToProcess.size();
        std::vector<Variant::Read> eventFiles;
        if (fileToProcess.empty()) {
            qCDebug(log_component_process_incoming) << "No new files to process";
        } else {
            std::stable_sort(fileToProcess.begin(), fileToProcess.end(), fileSortCompare);

            ok = true;
            bool first = true;
            for (auto &dl : fileToProcess) {
                double fileStart = Time::time();
                Variant::Write eventInfo = Variant::Write::empty();

                substitutions.push();
                ProcessingResult result =
                        processFile(dl.get(), eventInfo, station, currentConfiguration,
                                    stationConfiguration, first);
                substitutions.setDuration("elapsed", Time::time() - fileStart);

                executeActions(currentConfiguration["Actions/Complete"], true);
                substitutions.setDuration("elapsed", Time::time() - fileStart);

                switch (result) {
                case Processing_Error:
                    ok = false;
                    executeActions(currentConfiguration["Actions/Error"], true);
                    break;

                case Processing_Corrupted:
                    if (!corruptedFile(dl.get(), eventInfo, currentConfiguration))
                        ok = false;
                    substitutions.setDuration("elapsed", Time::time() - fileStart);
                    executeActions(currentConfiguration["Actions/Corrupted"], true);
                    break;
                case Processing_Unauthorized:
                    if (!unauthorizedFile(dl.get(), eventInfo, currentConfiguration))
                        ok = false;
                    substitutions.setDuration("elapsed", Time::time() - fileStart);
                    executeActions(currentConfiguration["Actions/Unauthorized"], true);
                    break;

                case Processing_Accepted:
                    if (!acceptFile(dl.get(), eventInfo, currentConfiguration)) {
                        ok = false;
                        substitutions.setDuration("elapsed", Time::time() - fileStart);
                        executeActions(currentConfiguration["Actions/Error"], true);
                    } else {
                        first = false;
                        substitutions.setDuration("elapsed", Time::time() - fileStart);
                        executeActions(currentConfiguration["Actions/Accepted"], true);
                    }
                    break;
                }

                substitutions.pop();

                double fileEnd = Time::time();

                qCDebug(log_component_process_incoming) << "File" << dl->fileName()
                                                        << "completed processing after"
                                                        << Logging::elapsed(fileEnd - fileStart);

                dl.reset();
                if (!ok) {
                    if (first)
                        break;
                    else
                        continue;
                }

                eventInfo["ProcessingTime"].setDouble(fileEnd - fileStart);

                if (!currentConfiguration["DisableLog"].toBool()) {
                    Variant::Root logEntry;
                    logEntry["Type"].setString("CompletedFile");
                    logEntry["Time"].setDouble(Time::time());
                    logEntry["File"].set(eventInfo);
                    log.emplace_back(std::move(logEntry));
                }

                eventFiles.emplace_back(std::move(eventInfo));
            }

            if (!ok) {
                feedback.emitFailure();
                rc.fail();
                if (!first) {
                    qCWarning(log_component_process_incoming)
                        << "Processing failure after the first file; this may result in incomplete or unpredictable processed data";
                }
                qCDebug(log_component_process_incoming) << "Processing for" << station
                                                        << "ended prematurely";
                continue;
            }
        }


        {
            QByteArray data;
            QDataStream stream(&data, QIODevice::WriteOnly);
            stream << rcLogVersion << log;
            rc.set(rcLog, data);
        }

        if (totalFiles > 0) {
            Variant::Root event;

            event["Text"].setString(
                    tr("Completed processing %n file(s)", "processing event text", totalFiles));
            event["Information"].hash("By").setString("process_incoming");
            event["Information"].hash("At").setDouble(Time::time());
            event["Information"].hash("Environment").setString(Environment::describe());
            event["Information"].hash("Revision").setString(Environment::revision());
            event["Information"].hash("TotalFiles").setInt64(totalFiles);
            event["Information"].hash("LastRunTime").setDouble(lastRunTime);
            event["Information"].hash("EndTime").setDouble(processEndTime);

            for (const auto &file : eventFiles) {
                event["Files"].toArray().after_back().set(file);
            }

            Archive::Access().writeSynchronous(SequenceValue::Transfer{
                    SequenceValue({station, "events", "processing"}, event, processEndTime,
                                  processEndTime)});
        }

        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << rcStateVersion;

                bool prune = currentConfiguration["PruneUnusedSourceState"].toBool();

                stream << downloader.saveState(prune);
                stream << saveAcquisitionState(prune);
            }
            rc.set(rcKey, data);
        }

        feedback.emitStage(tr("Finalize %1").arg(QString::fromStdString(station).toUpper()),
                           tr("Final cleanup is taking place for %1.").arg(
                                   QString::fromStdString(station).toUpper()), false);
        rc.release();

        double stationEnd = Time::time();
        qCDebug(log_component_process_incoming) << "Successfully completed processing for"
                                                << station << "after"
                                                << Logging::elapsed(stationEnd - stationStart);

        QCoreApplication::processEvents();
    }

    double end = Time::time();
    qCDebug(log_component_process_incoming) << "Finished processing after"
                                            << Logging::elapsed(end - start);

    QCoreApplication::processEvents();
}


ComponentOptions ProcessIncomingComponent::getOptions()
{
    ComponentOptions options;

    options.add("profile",
                new ComponentOptionSingleString(tr("profile", "name"), tr("Execution profile"),
                                                tr("This is the profile name to perform.  Multiple profiles can be "
                                                   "defined to perform different types of processing for a station.  "
                                                   "Consult the station configuration for available profiles."),
                                                tr("aerosol")));

    ComponentOptionSingleDouble *timeout =
            new ComponentOptionSingleDouble(tr("wait", "name"), tr("Maximum wait time"),
                                            tr("This is the maximum time to wait for another upload process to "
                                               "complete, in seconds.  If there are two concurrent processes for "
                                               "the same station and profile, the new one will wait at most this "
                                               "many seconds before giving up without uploading new data.  If "
                                               "this is set to undefined, it will wait forever for the other "
                                               "process to complete."), tr("30 seconds"));
    timeout->setMinimum(0.0);
    timeout->setAllowUndefined(true);
    options.add("timeout", timeout);

    ComponentOptionSingleTime *after =
            new ComponentOptionSingleTime(tr("after", "name"), tr("Override time"),
                                          tr("This sets the time to reprocess after.  When set, this will process "
                                             "all files modified after the time.  This also triggers a state reset "
                                             "when used.  When set to undefined, all data are reprocessed."),
                                          tr("Last run time"));
    after->setAllowUndefined(true);
    options.add("after", after);

    return options;
}

QString ProcessIncomingComponent::promptActionContinue(const ComponentOptions &options,
                                                       const std::vector<std::string> &stations)
{
    Q_UNUSED(stations);
    if (options.isSet("reprocess")) {
        if (qobject_cast<ComponentOptionBoolean *>(options.get("reprocess"))->get()) {
            return ProcessIncoming::tr("This operation will reprocess all available data.");
        }
    }
    return QString();
}

QList<ComponentExample> ProcessIncomingComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Process all data", "default example name"),
                                     tr("This will process all incoming data.")));

    return examples;
}

int ProcessIncomingComponent::actionAllowStations()
{ return INT_MAX; }

CPD3Action *ProcessIncomingComponent::createAction(const ComponentOptions &options,
                                                   const std::vector<std::string> &stations)
{ return new ProcessIncoming(options, stations); }
