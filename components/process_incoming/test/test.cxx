/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QDir>
#include <QCryptographicHash>
#include <QHostInfo>
#include <QFile>
#include <QBuffer>
#include <QTemporaryFile>
#include <QProcess>

#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "core/actioncomponent.hxx"
#include "core/number.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"
#include "transfer/package.hxx"
#include "transfer/datafile.hxx"
#include "algorithms/dewpoint.hxx"
#include "io/drivers/file.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Transfer;
using namespace CPD3::Algorithms;


static const char *cert1Data = "-----BEGIN CERTIFICATE-----\n"
        "MIICbTCCAdYCCQDZ6mj3BI+kCTANBgkqhkiG9w0BAQUFADB7MQswCQYDVQQGEwJV\n"
        "UzERMA8GA1UECAwIQ29sb3JhZG8xEDAOBgNVBAcMB0JvdWxkZXIxFjAUBgNVBAoM\n"
        "DU5PQUEvR01EL0VTUkwxFzAVBgNVBAsMDkFlcm9zb2xzIEdyb3VwMRYwFAYDVQQD\n"
        "DA1EZXJlayBIYWdlbWFuMB4XDTEyMDcyNzE1MzkwNloXDTM5MTIxMjE1MzkwNlow\n"
        "ezELMAkGA1UEBhMCVVMxETAPBgNVBAgMCENvbG9yYWRvMRAwDgYDVQQHDAdCb3Vs\n"
        "ZGVyMRYwFAYDVQQKDA1OT0FBL0dNRC9FU1JMMRcwFQYDVQQLDA5BZXJvc29scyBH\n"
        "cm91cDEWMBQGA1UEAwwNRGVyZWsgSGFnZW1hbjCBnzANBgkqhkiG9w0BAQEFAAOB\n"
        "jQAwgYkCgYEAn3nIKzjLLEY8KG6+hUvrkjDkl9VU8HFKnp8wA2RsMta/UvKG4ZIY\n"
        "bNVsKSiAGm1G0i6Mtftw8quyihoxuy/hElH/XJChU5lersdrhalocHXoipNx8BeW\n"
        "T8sXuJOrh+IoDKqVI4I6Sz42qAlqBU7bNm40TE8sBPAOefyTGavC3e0CAwEAATAN\n"
        "BgkqhkiG9w0BAQUFAAOBgQBU2hljZZpJJ+zK6eePUpdELQGPGHJEbzfReeSBhRau\n"
        "iDj4jEa7IE3yrMv8NqC3Q+6lEVuJg0NInvDYsNYLPjMSPS+jKldrmrJvg+TFrp1w\n"
        "Qr3JoznNxwcwuDyrKjOVux6jo2aype3Zayo5VUizJX9Zo+xLe2MBVR1D0MXmoq8L\n"
        "zA==\n"
        "-----END CERTIFICATE-----";
static const char *key1Data = "-----BEGIN RSA PRIVATE KEY-----\n"
        "MIICXAIBAAKBgQCfecgrOMssRjwobr6FS+uSMOSX1VTwcUqenzADZGwy1r9S8obh\n"
        "khhs1WwpKIAabUbSLoy1+3Dyq7KKGjG7L+ESUf9ckKFTmV6ux2uFqWhwdeiKk3Hw\n"
        "F5ZPyxe4k6uH4igMqpUjgjpLPjaoCWoFTts2bjRMTywE8A55/JMZq8Ld7QIDAQAB\n"
        "AoGBAIQOd0f7PpsKCfS9R7zfklG7dP+Z4z07wzu4vCyC8uniVAoe1LxjmyA8VtV6\n"
        "OSIpDTUs4M4tSWlZ7n1XlYjY6/lNt3xsy5BBQMlWVI8BV/yXnWRCxkQGBvXYTUKe\n"
        "7LRsUcqyGpAPeuStO/V8GEM5H272yv5S4413D09C9cAhROMtAkEA07j2lnnSl/WR\n"
        "0NFEOT9+nbh6Z+yRTzybC86TBhLCN3bY4VvqSWi1PdcU3oiz81dcaAPTksdx18/Y\n"
        "cSfz295ewwJBAMDTq22Dt2HMPQymxNUn9/IdIPU34/fEqSHS3e9hAgrp2gDVeNHR\n"
        "ZEzMkW00rLgdukkW51PV9hVIhYXdxYOjZY8CQFsf/cnwLurGf/b/SrzVDjr1/oEi\n"
        "Obx/2j+vrmnrwvm6Rkhglir4TSGLo+jPr5vpmtUN6I8BFoeLZp31UyjrwZ8CQC3W\n"
        "Y2ruI7qozV5jimjNToCMchg4yAVPB5GVydIsskqb2onWNRlTeE9VVcCrA9/kmTLk\n"
        "serY8t2OVsdCt8AaKHsCQFMEhJCpXaeLnrzToSv0uqRYjcT+r3ZjSghhSepN0ZA+\n"
        "PPj1CMmq9JrQaQAv3rT3hCbDF38TPhKX64OYzbuvdxA=\n"
        "-----END RSA PRIVATE KEY-----\n";
static const char *cert2Data = "-----BEGIN CERTIFICATE-----\n"
        "MIICbTCCAdYCCQCr9EVbER/M4TANBgkqhkiG9w0BAQUFADB7MQswCQYDVQQGEwJV\n"
        "UzERMA8GA1UECAwIQ29sb3JhZG8xEDAOBgNVBAcMB0JvdWxkZXIxFjAUBgNVBAoM\n"
        "DU5PQUEvRVNSTC9HTUQxFzAVBgNVBAsMDkFlcm9zb2xzIEdyb3VwMRYwFAYDVQQD\n"
        "DA1EZXJlayBIYWdlbWFuMB4XDTEyMDcyNzE1NDIxNloXDTM5MTIxMjE1NDIxNlow\n"
        "ezELMAkGA1UEBhMCVVMxETAPBgNVBAgMCENvbG9yYWRvMRAwDgYDVQQHDAdCb3Vs\n"
        "ZGVyMRYwFAYDVQQKDA1OT0FBL0VTUkwvR01EMRcwFQYDVQQLDA5BZXJvc29scyBH\n"
        "cm91cDEWMBQGA1UEAwwNRGVyZWsgSGFnZW1hbjCBnzANBgkqhkiG9w0BAQEFAAOB\n"
        "jQAwgYkCgYEAqa4sVcN1M4mWVqTqZuG2VikkOTpYrHYgnD4jFKDfi5FTFE9OQ5oW\n"
        "xhrBdoEtiWLEPmPSQCtJWCbD0d/YQ1ITxOF0OSaAa4c6PkdeiDmfull8EGmuaTFo\n"
        "ikcFS0r2SVtvIy3eFEAg9GBF4iCMp2AvUo2eqNCLt4CdRi6LQs2xkmUCAwEAATAN\n"
        "BgkqhkiG9w0BAQUFAAOBgQCEvlrqC1zChgkFMq8Oi7YRQOdx7ikzDodey/uu0r/W\n"
        "DxCnpA+F2f3okHiR5+LfYqQ5fvCYnmZd7w69tvdpui8swa15aXWaxLVD9/woyM2y\n"
        "Ofce/9pw6UgoYLkh9KsW5+2EP2Sn44lWcU5Jf8U/p8cDdjNIDzDG4vw7UFWiRakm\n"
        "Tw==\n"
        "-----END CERTIFICATE-----\n";
static const char *key2Data = "-----BEGIN RSA PRIVATE KEY-----\n"
        "MIICWwIBAAKBgQCprixVw3UziZZWpOpm4bZWKSQ5OlisdiCcPiMUoN+LkVMUT05D\n"
        "mhbGGsF2gS2JYsQ+Y9JAK0lYJsPR39hDUhPE4XQ5JoBrhzo+R16IOZ+6WXwQaa5p\n"
        "MWiKRwVLSvZJW28jLd4UQCD0YEXiIIynYC9SjZ6o0Iu3gJ1GLotCzbGSZQIDAQAB\n"
        "AoGAUzE2Q4ZlfDNFJo4M7wxTXcMmI3jb6RKxwmkkwgRuFfvWg+quMK7n45FSsUt8\n"
        "jBOErCI8/4E5oKLA97GMUtV3IxAXT/hxQRe6qZS7PEUW4k6Op7evX1hSCfI/IAB/\n"
        "AS6OqUR1U4+AFArhPS3CA+vuY4I01mpWpvI3dfvq29oUW4ECQQDQ4ItViA/CjWiq\n"
        "peW2+cptQ8QqkNGscjsUTZyp3aNtunC1mP/HRKlKLnuhY6G+LQWbzDv7BSJ9UNO4\n"
        "Ro1y90jJAkEAz/Xdjqx/xZntCleuxvMACWdhpMBqfdgs7+maLDXbtc9G43ygbqIz\n"
        "Wj99k9lu0L3iHdRlYicwdfFFBbvd4jsmvQJATOF5J4AvHNLjpXvuc0y5n0IEIA6x\n"
        "viFFcZGnijZUAv1OouivrG6vSOiXBK4hSFhV6iRgJ2KacTmg1ADT627tUQJAK4hl\n"
        "W9OCX8QMGekm/iCqNk284/cfk75oEcTN8ElJ9/Iu/bn9/4rWwyKdUBDpIKtPJT1s\n"
        "B7L6cwYRk9Sy6wPE5QJAFmjRUr3GypvuC4haazaOMyIt/FEbC+kUs9kq6JZsiBuM\n"
        "LbZ02ko9IkhfOiUIewV6+Xb2j+1u/Itc737M6RZ+3Q==\n"
        "-----END RSA PRIVATE KEY-----";

class TestComponent : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;
    QString tmpPath;
    QDir tmpDir;

    ActionComponent *component;

    void recursiveRemove(const QDir &base)
    {
        if (base.path().isEmpty() || base == QDir::current())
            return;
        QFileInfoList list(base.entryInfoList(QDir::Files | QDir::NoDotAndDotDot));
        for (QFileInfoList::const_iterator rm = list.constBegin(), endRm = list.constEnd();
                rm != endRm;
                ++rm) {
            if (!QFile::remove(rm->absoluteFilePath())) {
                qDebug() << "Failed to remove file" << rm->absoluteFilePath();
                continue;
            }
        }
        list = base.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot);
        for (QFileInfoList::const_iterator rm = list.constBegin(), endRm = list.constEnd();
                rm != endRm;
                ++rm) {
            QDir subDir(base);
            if (!subDir.cd(rm->fileName())) {
                qDebug() << "Failed to change directory" << rm->absoluteFilePath();
                continue;
            }
            recursiveRemove(subDir);
            base.rmdir(rm->fileName());
        }
    }

    bool writeData(const QString &target, const QByteArray &data)
    {
        QFile output(target);
        if (!output.open(QIODevice::WriteOnly))
            return false;

        const char *ptr = data.constData();
        qint64 remaining = data.size();
        while (remaining > 0) {
            qint64 n = output.write(ptr, remaining);
            if (n < 0)
                return false;
            Q_ASSERT(n <= remaining);
            ptr += n;
            remaining -= n;
        }

        output.flush();
        output.close();
        return true;
    }

    bool writeData(const QString &target,
                   const SequenceValue::Transfer &values, const Variant::Read &config)
    {
        auto buffer = IO::Access::buffer();
        DataPack data(buffer->stream());
        data.start();
        data.incomingData(values);
        data.endData();
        Threading::pollInEventLoop([&] { return !data.isFinished(); });
        data.wait();

        auto output = IO::Access::file(target, IO::File::Mode::writeOnly());
        if (!output) {
            qDebug() << "Unable to open output";
            return false;
        }

        TransferPack filePack;
        filePack.compressStage();
        filePack.signStage(config);
        filePack.checksumStage();
        if (!filePack.exec(buffer, output)) {
            qDebug() << "Error packaging file";
            return false;
        }

        return true;
    }

    bool checkArchiveContains(SequenceValue::Transfer expected, bool checkTimes = true)
    {
        auto result = Archive::Access(databaseFile).readSynchronous(Archive::Selection());
        for (auto rv = result.begin(); rv != result.end();) {
            auto check =
                    std::find_if(expected.begin(), expected.end(), [=](const SequenceValue &e) {
                        if (checkTimes) {
                            if (!FP::equal(e.getStart(), rv->getStart()))
                                return false;
                            if (!FP::equal(e.getEnd(), rv->getEnd()))
                                return false;
                        }
                        if (e.getUnit() != rv->getUnit())
                            return false;
                        if (e.getPriority() != rv->getPriority())
                            return false;
                        return e.getValue() == rv->getValue();
                    });
            if (check == expected.end()) {
                ++rv;
                continue;
            }

            rv = result.erase(rv);
            expected.erase(check);
        }

        if (!expected.empty()) {
            //qDebug() << "Unmatched expected values:" << expected;
            return false;
        }
        return true;
    }

    bool directoryHasContents(const QDir &dir,
                              const QStringList &contents = QStringList(),
                              bool retry = true)
    {
        dir.refresh();
        QFileInfoList fi(dir.entryInfoList(QDir::Files | QDir::NoDotAndDotDot));
        if (contents.size() != fi.size()) {
            for (QFileInfoList::const_iterator add = fi.constBegin(), end = fi.constEnd();
                    add != end;
                    ++add) {
                qDebug() << "Unexpected file list:" << add->fileName();
            }
            return false;
        }
        for (QStringList::const_iterator check = contents.constBegin(),
                endCheck = contents.constEnd(); check != endCheck; ++check) {
            bool hit = false;
            for (QFileInfoList::const_iterator info = fi.constBegin(), endInfo = fi.constEnd();
                    info != endInfo;
                    ++info) {
                if (info->fileName() != *check)
                    continue;
                hit = true;
                break;
            }
            if (!hit) {
                qDebug() << "Unmatched file:" << *check;
                return false;
            }
        }
        return true;
    }

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component = qobject_cast<ActionComponent *>(ComponentLoader::create("process_incoming"));
        QVERIFY(component);

        QVERIFY(databaseFile.open());
        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());

        tmpDir = QDir(QDir::tempPath());
        tmpPath = "CPD3Process-";
        tmpPath.append(Random::string());
        tmpDir.mkdir(tmpPath);
        QVERIFY(tmpDir.cd(tmpPath));
    }

    void cleanupTestCase()
    {
        recursiveRemove(tmpDir);
        tmpDir.cdUp();
        tmpDir.rmdir(tmpPath);
    }

    void init()
    {
        recursiveRemove(tmpDir);
        databaseFile.resize(0);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("timeout")));
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("profile")));
        QVERIFY(qobject_cast<ComponentOptionSingleTime *>(options.get("after")));
    }

    void basic()
    {
        tmpDir.mkdir("incoming");
        tmpDir.mkdir("incomingstatic");
        tmpDir.mkdir("completed");
        tmpDir.mkdir("corrupt");
        tmpDir.mkdir("unauthorized");

        Variant::Root config;
        config["/Profiles/aerosol/Sources/#0/Path"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "incoming"));
        config["/Profiles/aerosol/Sources/#0/Writable"].setBool(true);
        config["/Profiles/aerosol/Sources/#0/MinimumAge"].setInt64(0);
        config["/Profiles/aerosol/Sources/#0/Match/#0"].setString("process${INSTANCE}\\.cpd3data");
        config["/Profiles/aerosol/Sources/#0/Match/#1"].setString(".*");
        config["/Profiles/aerosol/Sources/#1/Path"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "incomingstatic"));
        config["/Profiles/aerosol/Sources/#1/Writable"].setBool(false);
        config["/Profiles/aerosol/Sources/#1/MinimumAge"].setInt64(0);
        config["/Profiles/aerosol/Sources/#1/Match"].setString(".*");
        config["/Profiles/aerosol/CompletedLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "completed"));
        config["/Profiles/aerosol/UnauthorizedLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "unauthorized"));
        config["/Profiles/aerosol/CorruptLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "corrupt"));
        config["/Authorization/57dfe3dfbb14c8e43a2f9e95ff5758686cb2f1d20a6afde77e2b577fd1a9fead972eaff5b438963a92ed7cd2a50fc044f6ddd02013e6766381783a03dd201891/Filter"]
                .setString("sfa:raw:.*");

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "configuration", "processing"}, config)});

        SequenceValue::Transfer transferData1;
        transferData1.emplace_back(SequenceName("sfa", "raw", "T_S11"), Variant::Root(1.0), 100,
                                   200, 0);
        transferData1.emplace_back(SequenceName("sfa", "raw", "P_S11"), Variant::Root(1.0), 100,
                                   200, 0);

        Variant::Root packConfig;
        packConfig["Certificate"].setString(cert1Data);
        packConfig["Key"].setString(key1Data);
        QVERIFY(writeData(tmpDir.absolutePath() + "/incoming/process1.cpd3data", transferData1,
                          packConfig));

        ComponentOptions options(component->getOptions());
        CPD3Action *action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        QVERIFY(checkArchiveContains(transferData1));

        QDir incomingDir(tmpDir.absolutePath() + "/incoming");
        QDir incomingStaticDir(tmpDir.absolutePath() + "/incomingstatic");
        QDir completedDir(tmpDir.absolutePath() + "/completed");
        QDir corruptDir(tmpDir.absolutePath() + "/corrupt");
        QDir unauthorizedDir(tmpDir.absolutePath() + "/unauthorized");

        QVERIFY(directoryHasContents(incomingDir));
        QVERIFY(directoryHasContents(incomingStaticDir));
        QVERIFY(directoryHasContents(completedDir, QStringList("process1.cpd3data")));
        QVERIFY(directoryHasContents(corruptDir));
        QVERIFY(directoryHasContents(unauthorizedDir));


        QVERIFY(QFile::copy(tmpDir.absolutePath() + "/completed/process1.cpd3data",
                            tmpDir.absolutePath() + "/incoming/process1.cpd3data"));

        options = component->getOptions();
        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        QVERIFY(checkArchiveContains(transferData1));

        QVERIFY(directoryHasContents(incomingDir));
        QVERIFY(directoryHasContents(incomingStaticDir));
        QVERIFY(directoryHasContents(completedDir, QStringList("process1.cpd3data")));
        QVERIFY(directoryHasContents(corruptDir));
        QVERIFY(directoryHasContents(unauthorizedDir));


        SequenceValue::Transfer transferData2;
        transferData2.emplace_back(SequenceName("sfa", "raw", "T_S11"), Variant::Root(2.0), 200,
                                   300, 0);
        transferData2.emplace_back(SequenceName("sfa", "raw", "P_S11"), Variant::Root(2.1), 200,
                                   300, 0);
        SequenceValue::Transfer transferData2Reject;
        transferData2Reject.emplace_back(SequenceName("sfa", "clean", "P_S11"), Variant::Root(2.2),
                                         200,
                                         300, 0);


        {
            auto combined = transferData2;
            Util::append(transferData2Reject, combined);
            QVERIFY(writeData(tmpDir.absolutePath() + "/incoming/process2.cpd3data", combined,
                              packConfig));
        }

        options = component->getOptions();
        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        QVERIFY(checkArchiveContains(transferData2));
        QVERIFY(!checkArchiveContains(transferData2Reject));

        QVERIFY(directoryHasContents(incomingDir));
        QVERIFY(directoryHasContents(incomingStaticDir));
        QVERIFY(directoryHasContents(completedDir,
                                     QStringList("process1.cpd3data") << "process2.cpd3data"));
        QVERIFY(directoryHasContents(corruptDir));
        QVERIFY(directoryHasContents(unauthorizedDir));


        SequenceValue::Transfer transferData3
                {SequenceValue(SequenceName("sfa", "raw", "T_S11"), Variant::Root(3.0), 300, 400,
                               0),
                 SequenceValue(SequenceName("sfa", "raw", "P_S11"), Variant::Root(3.1), 300, 400,
                               0)};
        QVERIFY(writeData(tmpDir.absolutePath() + "/incoming/process1.cpd3data", transferData3,
                          packConfig));

        /* This assumes 1-second modified resolution, which may need
         * changing */
        QTest::qSleep(1250);

        SequenceValue::Transfer transferData4
                {SequenceValue(SequenceName("sfa", "raw", "T_S11"), Variant::Root(3.0), 301, 401,
                               0),
                 SequenceValue(SequenceName("sfa", "raw", "P_S11"), Variant::Root(3.1), 301, 401,
                               0)};
        packConfig["Certificate"].setString(cert2Data);
        packConfig["Key"].setString(key2Data);
        QVERIFY(writeData(tmpDir.absolutePath() + "/incomingstatic/processUA.cpd3data",
                          transferData4, packConfig));

        {
            QFile wr(tmpDir.absolutePath() + "/incoming/processCR.cpd3data");
            QVERIFY(wr.open(QIODevice::WriteOnly));
            wr.write("Blarg corrupt");
            wr.flush();
            wr.close();
        }

        QTest::qSleep(250);

        options = component->getOptions();
        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        QVERIFY(checkArchiveContains(transferData3));
        QVERIFY(!checkArchiveContains(transferData4));

        QVERIFY(directoryHasContents(incomingDir));
        QVERIFY(directoryHasContents(incomingStaticDir, QStringList("processUA.cpd3data")));
        QVERIFY(directoryHasContents(completedDir,
                                     QStringList("process1.cpd3data") << "process2.cpd3data"
                                                                      << "process3.cpd3data"));
        QVERIFY(directoryHasContents(corruptDir, QStringList("processCR.cpd3data")));
        QVERIFY(directoryHasContents(unauthorizedDir, QStringList("processUA.cpd3data")));


        QTest::qSleep(250);

        options = component->getOptions();
        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        QVERIFY(checkArchiveContains(transferData1));
        QVERIFY(checkArchiveContains(transferData2));
        QVERIFY(checkArchiveContains(transferData3));
        QVERIFY(!checkArchiveContains(transferData2Reject));
        QVERIFY(!checkArchiveContains(transferData4));

        QVERIFY(directoryHasContents(incomingDir));
        QVERIFY(directoryHasContents(incomingStaticDir, QStringList("processUA.cpd3data")));
        QVERIFY(directoryHasContents(completedDir,
                                     QStringList("process1.cpd3data") << "process2.cpd3data"
                                                                      << "process3.cpd3data"));
        QVERIFY(directoryHasContents(corruptDir, QStringList("processCR.cpd3data")));
        QVERIFY(directoryHasContents(unauthorizedDir, QStringList("processUA.cpd3data")));
    }

#ifdef Q_OS_UNIX

    /* This is to simulate an rsync setting the timestamps backwards between
     * runs of processing */
    void minimumAgeSetBackwardsBetweenRuns()
    {
        tmpDir.mkdir("incoming");
        tmpDir.mkdir("incomingstatic");
        tmpDir.mkdir("completed");
        tmpDir.mkdir("corrupt");
        tmpDir.mkdir("unauthorized");

        Variant::Root config;
        config["/Profiles/aerosol/Sources/#0/Path"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "incoming"));
        config["/Profiles/aerosol/Sources/#0/Writable"].setBool(false);
        config["/Profiles/aerosol/Sources/#0/MinimumAge"].setInt64(300);
        config["/Profiles/aerosol/Sources/#0/Match"].setString(".*");
        config["/Profiles/aerosol/CompletedLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "completed"));
        config["/Profiles/aerosol/UnauthorizedLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "unauthorized"));
        config["/Profiles/aerosol/CorruptLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "corrupt"));
        config["/Authorization/57dfe3dfbb14c8e43a2f9e95ff5758686cb2f1d20a6afde77e2b577fd1a9fead972eaff5b438963a92ed7cd2a50fc044f6ddd02013e6766381783a03dd201891/Filter"]
                .setString("sfa:raw:.*");

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "configuration", "processing"}, config)});

        SequenceValue::Transfer transferData1
                {SequenceValue(SequenceName("sfa", "raw", "T_S11"), Variant::Root(1.0), 100, 200,
                               0),
                 SequenceValue(SequenceName("sfa", "raw", "P_S11"), Variant::Root(1.0), 100, 200,
                               0)};

        QString markerFile(tmpDir.absolutePath() + "/incoming/marker");
        {
            QProcess p;
            p.start("touch", QStringList() << "-t" << QDateTime::currentDateTime().addSecs(-3600)
                                                                                  .toString(
                                                                                          "yyyyMMddhhmm.ss")
                                           << markerFile);
            QVERIFY(p.waitForStarted(30000));
            QVERIFY(p.waitForFinished(30000));
        }
        QVERIFY(QFileInfo(markerFile).lastModified().
                        secsTo(QDateTime::currentDateTime()) >= 3599);

        Variant::Root packConfig;
        packConfig["Certificate"].setString(cert1Data);
        packConfig["Key"].setString(key1Data);
        QString fileName(tmpDir.absolutePath() + "/incoming/process1.cpd3data");
        QVERIFY(writeData(fileName, transferData1, packConfig));

        ComponentOptions options(component->getOptions());
        CPD3Action *action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        QVERIFY(!checkArchiveContains(transferData1));

        QDir incomingDir(tmpDir.absolutePath() + "/incoming");
        QDir completedDir(tmpDir.absolutePath() + "/completed");
        QDir corruptDir(tmpDir.absolutePath() + "/corrupt");
        QDir unauthorizedDir(tmpDir.absolutePath() + "/unauthorized");

        QVERIFY(directoryHasContents(incomingDir, QStringList("process1.cpd3data") << "marker"));
        QVERIFY(directoryHasContents(completedDir));
        QVERIFY(directoryHasContents(corruptDir, QStringList("marker")));
        QVERIFY(directoryHasContents(unauthorizedDir));

        {
            QProcess p;
            p.start("touch", QStringList() << "-t" << QDateTime::currentDateTime().addSecs(-1800)
                                                                                  .toString(
                                                                                          "yyyyMMddhhmm.ss")
                                           << fileName);
            QVERIFY(p.waitForStarted(30000));
            QVERIFY(p.waitForFinished(30000));
        }
        QVERIFY(QFileInfo(fileName).lastModified().
                        secsTo(QDateTime::currentDateTime()) >= 1799);

        options = component->getOptions();
        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        QVERIFY(checkArchiveContains(transferData1));

        QVERIFY(directoryHasContents(incomingDir, QStringList("process1.cpd3data") << "marker"));
        QVERIFY(directoryHasContents(completedDir, QStringList("process1.cpd3data")));
        QVERIFY(directoryHasContents(corruptDir, QStringList("marker")));
        QVERIFY(directoryHasContents(unauthorizedDir));

        /* This assumes 1-second modified resolution, which may need
         * changing */
        QTest::qSleep(1250);

        SequenceValue::Transfer transferData2
                {SequenceValue(SequenceName("sfa", "raw", "T_S11"), Variant::Root(2.0), 200, 300,
                               0),
                 SequenceValue(SequenceName("sfa", "raw", "P_S11"), Variant::Root(2.1), 200, 300,
                               0)};

        fileName = tmpDir.absolutePath() + "/incoming/process2.cpd3data";
        QVERIFY(writeData(fileName, transferData2, packConfig));

        options = component->getOptions();
        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        QTest::qSleep(1250);

        options = component->getOptions();
        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        QVERIFY(!checkArchiveContains(transferData2));

        QVERIFY(directoryHasContents(incomingDir,
                                     QStringList("process1.cpd3data") << "process2.cpd3data"
                                                                      << "marker"));
        QVERIFY(directoryHasContents(completedDir, QStringList("process1.cpd3data")));
        QVERIFY(directoryHasContents(corruptDir, QStringList("marker")));
        QVERIFY(directoryHasContents(unauthorizedDir));

        {
            QProcess p;
            p.start("touch", QStringList() << "-t" << QDateTime::currentDateTime().addSecs(-1800)
                                                                                  .toString(
                                                                                          "yyyyMMddhhmm.ss")
                                           << fileName);
            QVERIFY(p.waitForStarted(30000));
            QVERIFY(p.waitForFinished(30000));
        }
        QVERIFY(QFileInfo(fileName).lastModified().
                        secsTo(QDateTime::currentDateTime()) >= 1799);

        options = component->getOptions();
        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        QVERIFY(checkArchiveContains(transferData2));

        QVERIFY(directoryHasContents(incomingDir,
                                     QStringList("process1.cpd3data") << "process2.cpd3data"
                                                                      << "marker"));
        QVERIFY(directoryHasContents(completedDir,
                                     QStringList("process1.cpd3data") << "process2.cpd3data"));
        QVERIFY(directoryHasContents(corruptDir, QStringList("marker")));
        QVERIFY(directoryHasContents(unauthorizedDir));

        SequenceValue::Transfer transferData3
                {SequenceValue(SequenceName("sfa", "raw", "T_S11"), Variant::Root(3.0), 301, 401,
                               0),
                 SequenceValue(SequenceName("sfa", "raw", "P_S11"), Variant::Root(3.1), 301, 401,
                               0)};
        packConfig["Certificate"].setString(cert2Data);
        packConfig["Key"].setString(key2Data);
        fileName = tmpDir.absolutePath() + "/incoming/processUA.cpd3data";
        QVERIFY(writeData(fileName, transferData3, packConfig));

        options = component->getOptions();
        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        QVERIFY(!checkArchiveContains(transferData3));

        QVERIFY(directoryHasContents(incomingDir,
                                     QStringList("process1.cpd3data") << "process2.cpd3data"
                                                                      << "processUA.cpd3data"
                                                                      << "marker"));
        QVERIFY(directoryHasContents(completedDir,
                                     QStringList("process1.cpd3data") << "process2.cpd3data"));
        QVERIFY(directoryHasContents(corruptDir, QStringList("marker")));
        QVERIFY(directoryHasContents(unauthorizedDir));

        {
            QProcess p;
            p.start("touch", QStringList() << "-t" << QDateTime::currentDateTime().addSecs(-1800)
                                                                                  .toString(
                                                                                          "yyyyMMddhhmm.ss")
                                           << fileName);
            QVERIFY(p.waitForStarted(30000));
            QVERIFY(p.waitForFinished(30000));
        }
        QVERIFY(QFileInfo(fileName).lastModified().
                        secsTo(QDateTime::currentDateTime()) >= 1799);

        options = component->getOptions();
        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        QVERIFY(!checkArchiveContains(transferData3));

        QVERIFY(directoryHasContents(incomingDir,
                                     QStringList("process1.cpd3data") << "process2.cpd3data"
                                                                      << "processUA.cpd3data"
                                                                      << "marker"));
        QVERIFY(directoryHasContents(completedDir,
                                     QStringList("process1.cpd3data") << "process2.cpd3data"));
        QVERIFY(directoryHasContents(corruptDir, QStringList("marker")));
        QVERIFY(directoryHasContents(unauthorizedDir, QStringList("processUA.cpd3data")));

        QTest::qSleep(1250);

        options = component->getOptions();
        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        QVERIFY(!checkArchiveContains(transferData3));

        QVERIFY(directoryHasContents(incomingDir,
                                     QStringList("process1.cpd3data") << "process2.cpd3data"
                                                                      << "processUA.cpd3data"
                                                                      << "marker"));
        QVERIFY(directoryHasContents(completedDir,
                                     QStringList("process1.cpd3data") << "process2.cpd3data"));
        QVERIFY(directoryHasContents(corruptDir, QStringList("marker")));
        QVERIFY(directoryHasContents(unauthorizedDir, QStringList("processUA.cpd3data")));


        fileName = tmpDir.absolutePath() + "/incoming/processCR.cpd3data";
        {
            QFile wr(fileName);
            QVERIFY(wr.open(QIODevice::WriteOnly));
            wr.write("Blarg corrupt");
            wr.flush();
            wr.close();
        }

        options = component->getOptions();
        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        QVERIFY(directoryHasContents(incomingDir,
                                     QStringList("process1.cpd3data") << "process2.cpd3data"
                                                                      << "processUA.cpd3data"
                                                                      << "processCR.cpd3data"
                                                                      << "marker"));
        QVERIFY(directoryHasContents(completedDir,
                                     QStringList("process1.cpd3data") << "process2.cpd3data"));
        QVERIFY(directoryHasContents(corruptDir, QStringList("marker")));
        QVERIFY(directoryHasContents(unauthorizedDir, QStringList("processUA.cpd3data")));

        {
            QProcess p;
            p.start("touch", QStringList() << "-t" << QDateTime::currentDateTime().addSecs(-1800)
                                                                                  .toString(
                                                                                          "yyyyMMddhhmm.ss")
                                           << fileName);
            QVERIFY(p.waitForStarted(30000));
            QVERIFY(p.waitForFinished(30000));
        }
        QVERIFY(QFileInfo(fileName).lastModified().
                        secsTo(QDateTime::currentDateTime()) >= 1799);

        options = component->getOptions();
        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        QVERIFY(directoryHasContents(incomingDir,
                                     QStringList("process1.cpd3data") << "process2.cpd3data"
                                                                      << "processUA.cpd3data"
                                                                      << "processCR.cpd3data"
                                                                      << "marker"));
        QVERIFY(directoryHasContents(completedDir,
                                     QStringList("process1.cpd3data") << "process2.cpd3data"));
        QVERIFY(directoryHasContents(corruptDir, QStringList("processCR.cpd3data") << "marker"));
        QVERIFY(directoryHasContents(unauthorizedDir, QStringList("processUA.cpd3data")));

        options = component->getOptions();
        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        QVERIFY(directoryHasContents(incomingDir,
                                     QStringList("process1.cpd3data") << "process2.cpd3data"
                                                                      << "processUA.cpd3data"
                                                                      << "processCR.cpd3data"
                                                                      << "marker"));
        QVERIFY(directoryHasContents(completedDir,
                                     QStringList("process1.cpd3data") << "process2.cpd3data"));
        QVERIFY(directoryHasContents(corruptDir, QStringList("processCR.cpd3data") << "marker"));
        QVERIFY(directoryHasContents(unauthorizedDir, QStringList("processUA.cpd3data")));
    }

#endif

#if 0
    void ftpSource() {
        tmpDir.mkdir("completed");
        tmpDir.mkdir("corrupt");
        
        Variant::Root config;
        config["/Profiles/aerosol/Sources/#0/Type"].setString("FTP");
        config["/Profiles/aerosol/Sources/#0/Hostname"].setString("aftp.cmdl.noaa.gov");
        config["/Profiles/aerosol/Sources/#0/Directory"].setString("aerosol/smo");
        config["/Profiles/aerosol/Sources/#0/Recursive"].setBool(true);
        config["/Profiles/aerosol/Sources/#0/MinimumAge"].setInt64(300);
        config["/Profiles/aerosol/Sources/#0/Match"].setString("README(?:\\.txt)?");
        config["/Profiles/aerosol/CompletedLocation"].setString(QString("%1/%2").
            arg(tmpDir.absolutePath(), "completed"));
        config["/Profiles/aerosol/CorruptLocation"].setString(QString("%1/%2").
            arg(tmpDir.absolutePath(), "corrupt"));
        
        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "configuration", "processing"}, config)});
        
        ComponentOptions options(component->getOptions());
        CPD3Action *action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        QDir corruptDir(tmpDir.absolutePath() + "/corrupt");
        QVERIFY(directoryHasContents(corruptDir, QStringList("README") << "README_1" << "README.txt"));
    }
#endif

    void convertData()
    {
        tmpDir.mkdir("incoming");
        tmpDir.mkdir("completed");
        tmpDir.mkdir("corrupt");
        tmpDir.mkdir("unauthorized");

        Variant::Root config;
        config["/Profiles/aerosol/Sources/#0/Path"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "incoming"));
        config["/Profiles/aerosol/Sources/#0/Writable"].setBool(true);
        config["/Profiles/aerosol/Sources/#0/MinimumAge"].setInt64(0);
        config["/Profiles/aerosol/Sources/#0/Match/#0"].setString(".*");
        config["/Profiles/aerosol/CompletedLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "completed"));
        config["/Profiles/aerosol/UnauthorizedLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "unauthorized"));
        config["/Profiles/aerosol/CorruptLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "corrupt"));
        config["/Profiles/aerosol/Converter/Type"].setString("Data");
        config["/Profiles/aerosol/Converter/Component"].setString("import_cpd2");
        config["/Profiles/aerosol/Converter/Filter/#0/Variable"].setString("BsG_S11");
        config["/Profiles/aerosol/Converter/Filter/#1/Reject"].setBool(true);
        config["/Profiles/aerosol/Converter/Filter/#1/Variable"].setString(".*");

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "configuration", "processing"}, config)});

        QVERIFY(writeData(tmpDir.absolutePath() + "/incoming/process1.cpd2",
                          QByteArray("!row;colhdr;S11a,S11a;STN;EPOCH;DateTime;BsG_S11\n"
                                             "!row;mvc;S11a,S11a;ZZZ;0;9999-99-99T99:99:99Z;9999.99\n"
                                             "!row;varfmt;S11a,S11a;%s;%u;%04d-%02d-%02dT%02d:%02d:%02dZ;%07.2f\n"
                                             "!var;BsG_S11;FieldDesc,Some description (A Unit)\n"
                                             "!var;BsG_S11;Wavelength;0,450;WL1\n"
                                             "S11a,SFA,1262304000,2010-01-01T00:00:00Z,0001.00\n"
                                             "S11a,SFA,1262304060,2010-01-01T00:01:00Z,0002.00\n")));

        ComponentOptions options(component->getOptions());
        CPD3Action *action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        SequenceValue::Transfer expected;
        expected.emplace_back(SequenceName("sfa", "raw", "BsG_S11"), Variant::Root(1.0), 1262304000,
                              1262304060);
        expected.emplace_back(SequenceName("sfa", "raw", "BsG_S11"), Variant::Root(2.0), 1262304060,
                              1262304120);

        QVERIFY(checkArchiveContains(expected));

        QDir incomingDir(tmpDir.absolutePath() + "/incoming");
        QDir completedDir(tmpDir.absolutePath() + "/completed");
        QDir corruptDir(tmpDir.absolutePath() + "/corrupt");
        QDir unauthorizedDir(tmpDir.absolutePath() + "/unauthorized");

        QVERIFY(directoryHasContents(incomingDir));
        QVERIFY(directoryHasContents(completedDir, QStringList("process1.cpd2")));
        QVERIFY(directoryHasContents(corruptDir));
        QVERIFY(directoryHasContents(unauthorizedDir));
    }

#ifdef Q_OS_UNIX

    void convertProgram()
    {
        tmpDir.mkdir("incoming");
        tmpDir.mkdir("completed");
        tmpDir.mkdir("corrupt");
        tmpDir.mkdir("unauthorized");

        Variant::Root config;
        config["/Profiles/aerosol/Sources/#0/Path"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "incoming"));
        config["/Profiles/aerosol/Sources/#0/Writable"].setBool(true);
        config["/Profiles/aerosol/Sources/#0/MinimumAge"].setInt64(0);
        config["/Profiles/aerosol/Sources/#0/Match/#0"].setString(".*");
        config["/Profiles/aerosol/CompletedLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "completed"));
        config["/Profiles/aerosol/UnauthorizedLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "unauthorized"));
        config["/Profiles/aerosol/CorruptLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "corrupt"));
        config["/Profiles/aerosol/Converter/Type"].setString("External");
        config["/Profiles/aerosol/Converter/Program"].setString("cat");
        config["/Profiles/aerosol/Converter/Arguments/#0"].setString("-");

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "configuration", "processing"}, config)});

        QVERIFY(writeData(tmpDir.absolutePath() + "/incoming/process1.xml",
                          QByteArray("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                                             "<!DOCTYPE cpd3data>\n"
                                             "<cpd3data version=\"1.0\">\n"
                                             "<value station=\"sfa\" archive=\"raw\" variable=\"BsG_S11\" flavors=\"\" start=\"2014-05-01T00:01:00.000Z\" end=\"2014-05-01T00:02:00.000Z\" priority=\"0\" type=\"real\">1.0</value>\n"
                                             "<value station=\"sfa\" archive=\"raw\" variable=\"BsG_S11\" flavors=\"\" start=\"2014-05-01T00:02:00.000Z\" end=\"2014-05-01T00:03:00.000Z\" priority=\"0\" type=\"real\">2.0</value>\n"
                                             "</cpd3data>")));


        ComponentOptions options(component->getOptions());
        CPD3Action *action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        SequenceValue::Transfer expected;
        expected.emplace_back(SequenceName("sfa", "raw", "BsG_S11"), Variant::Root(1.0), 1398902460,
                              1398902520);
        expected.emplace_back(SequenceName("sfa", "raw", "BsG_S11"), Variant::Root(2.0), 1398902520,
                              1398902580);

        QVERIFY(checkArchiveContains(expected));

        QDir incomingDir(tmpDir.absolutePath() + "/incoming");
        QDir completedDir(tmpDir.absolutePath() + "/completed");
        QDir corruptDir(tmpDir.absolutePath() + "/corrupt");
        QDir unauthorizedDir(tmpDir.absolutePath() + "/unauthorized");

        QVERIFY(directoryHasContents(incomingDir));
        QVERIFY(directoryHasContents(completedDir, QStringList("process1.xml")));
        QVERIFY(directoryHasContents(corruptDir));
        QVERIFY(directoryHasContents(unauthorizedDir));
    }

    void standaloneProgram()
    {
        tmpDir.mkdir("incoming");
        tmpDir.mkdir("completed");
        tmpDir.mkdir("corrupt");
        tmpDir.mkdir("unauthorized");

        QTemporaryFile checkOutput;
        QVERIFY(checkOutput.open());

        Variant::Root config;
        config["/Profiles/aerosol/Sources/#0/Path"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "incoming"));
        config["/Profiles/aerosol/Sources/#0/Writable"].setBool(true);
        config["/Profiles/aerosol/Sources/#0/MinimumAge"].setInt64(0);
        config["/Profiles/aerosol/Sources/#0/Match/#0"].setString(".*");
        config["/Profiles/aerosol/CompletedLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "completed"));
        config["/Profiles/aerosol/UnauthorizedLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "unauthorized"));
        config["/Profiles/aerosol/CorruptLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "corrupt"));
        config["/Profiles/aerosol/Converter/Type"].setString("Standalone");
        config["/Profiles/aerosol/Converter/InputFile"].setBool(true);
        config["/Profiles/aerosol/Converter/Program"].setString("sh");
        config["/Profiles/aerosol/Converter/Arguments/#0"].setString("-c");
        config["/Profiles/aerosol/Converter/Arguments/#1"].setString(
                QString("cat '${INPUT}' > '%1'").arg(checkOutput.fileName()));

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "configuration", "processing"}, config)});

        QByteArray rawData("This file contains data");

        QVERIFY(writeData(tmpDir.absolutePath() + "/incoming/process1", rawData));


        ComponentOptions options(component->getOptions());
        CPD3Action *action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        QDir incomingDir(tmpDir.absolutePath() + "/incoming");
        QDir completedDir(tmpDir.absolutePath() + "/completed");
        QDir corruptDir(tmpDir.absolutePath() + "/corrupt");
        QDir unauthorizedDir(tmpDir.absolutePath() + "/unauthorized");

        QVERIFY(directoryHasContents(incomingDir));
        QVERIFY(directoryHasContents(completedDir, QStringList("process1")));
        QVERIFY(directoryHasContents(corruptDir));
        QVERIFY(directoryHasContents(unauthorizedDir));

        QCOMPARE(checkOutput.readAll(), rawData);
    }

#endif

    void convertAcquisition()
    {
        tmpDir.mkdir("incoming");
        tmpDir.mkdir("completed");
        tmpDir.mkdir("corrupt");
        tmpDir.mkdir("unauthorized");

        Variant::Root config;
        config["/Profiles/aerosol/Sources/#0/Path"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "incoming"));
        config["/Profiles/aerosol/Sources/#0/Writable"].setBool(true);
        config["/Profiles/aerosol/Sources/#0/MinimumAge"].setInt64(0);
        config["/Profiles/aerosol/Sources/#0/Match/#0"].setString(
                "BC${MONTH}${DAY}${SHORTYEAR}.CSV");
        config["/Profiles/aerosol/CompletedLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "completed"));
        config["/Profiles/aerosol/UnauthorizedLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "unauthorized"));
        config["/Profiles/aerosol/CorruptLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "corrupt"));
        config["/Profiles/aerosol/Converter/Type"].setString("Acquisition");
        config["/Profiles/aerosol/Converter/Component"].setString(
                "acquire_magee_aethalometer162131");
        config["/Profiles/aerosol/Converter/Options/suffix"].setString("A81");
        config["/Profiles/aerosol/Converter/Acquisition/BCUnits"].setString("ug");
        config["/Profiles/aerosol/Converter/Acquisition/SampleTemperature"].setDouble(0);
        config["/Profiles/aerosol/Converter/Acquisition/SamplePressure"].setDouble(1013.25);

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "configuration", "processing"}, config)});

        QVERIFY(writeData(tmpDir.absolutePath() + "/incoming/BC070214.CSV", QByteArray(
                "\"02-jul-14\",\"00:00\",  14.0,  19.0,  11.5,  12.7,   4.2,    30,    39,  4.8, 0.0211, 1.3202, 0.0212, 2.1401, 1.00,  .584, 0.0211, 2.0454, 0.0212, 2.2989, 1.00,  .412, 0.0211, 1.3748, 0.0212, 2.3600, 1.00,  .332, 0.0211, 1.8480, 0.0212, 1.1890, 1.00,  .250, 0.0211, 2.2155, 0.0212, 2.1224, 1.00,  .272, 0.0211, 2.2371, 0.0212, 3.3319, 1.00,  .231, 0.0211, 1.7546, 0.0212, 4.3526, 1.00,  .231\n"
                        "\"02-jul-14\",\"00:05\",   8.5,   5.7,  24.4,  27.3,  20.8,  21.5,  27.0,  4.8, 0.0211, 1.3201, 0.0212, 2.1402, 1.00,  .600, 0.0211, 2.0454, 0.0212, 2.2990, 1.00,  .421, 0.0211, 1.3745, 0.0212, 2.3604, 1.00,  .365, 0.0211, 1.8469, 0.0212, 1.1887, 1.00,  .283, 0.0211, 2.2148, 0.0212, 2.1223, 1.00,  .295, 0.0211, 2.2367, 0.0212, 3.3318, 1.00,  .249, 0.0211, 1.7542, 0.0212, 4.3525, 1.00,  .251\n"
                        "\"02-jul-14\",\"00:10\",  15.2,  22.4,  16.0,  18.5,    33,    67,    71,  4.8, 0.0211, 1.3199, 0.0212, 2.1405, 1.00,  .630, 0.0211, 2.0451, 0.0212, 2.2995, 1.00,  .455, 0.0211, 1.3746, 0.0212, 2.3610, 1.00,  .387, 0.0211, 1.8464, 0.0212, 1.1887, 1.00,  .305, 0.0211, 2.2143, 0.0212, 2.1225, 1.00,  .332, 0.0211, 2.2358, 0.0212, 3.3324, 1.00,  .303, 0.0211, 1.7536, 0.0212, 4.3532, 1.00,  .305\n"
                        "\"02-jul-14\",\"00:15\",  -4.1, -12.0, -10.5, -13.9, -11.3,   -47,   -77,  4.8, 0.0211, 1.3196, 0.0212, 2.1399, 1.00,  .622, 0.0211, 2.0446, 0.0212, 2.2986, 1.00,  .437, 0.0211, 1.3742, 0.0212, 2.3600, 1.00,  .373, 0.0211, 1.8455, 0.0212, 1.1879, 1.00,  .288, 0.0211, 2.2132, 0.0212, 2.1212, 1.00,  .319, 0.0211, 2.2349, 0.0212, 3.3297, 1.00,  .265, 0.0211, 1.7532, 0.0212, 4.3497, 1.00,  .246\n")));

        ComponentOptions options(component->getOptions());
        CPD3Action *action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        SequenceValue::Transfer expected;
        expected.emplace_back(SequenceName("sfa", "raw", "X1_A81"), Variant::Root(14.0), 1404259200,
                              1404259500);
        expected.emplace_back(SequenceName("sfa", "raw", "X1_A81"), Variant::Root(8.5), 1404259500,
                              1404259800);
        expected.emplace_back(SequenceName("sfa", "raw", "X1_A81"), Variant::Root(15.2), 1404259800,
                              1404260100);
        expected.emplace_back(SequenceName("sfa", "raw", "X1_A81"), Variant::Root(-4.1), 1404260100,
                              1404260400);

        QVERIFY(checkArchiveContains(expected));

        QDir incomingDir(tmpDir.absolutePath() + "/incoming");
        QDir completedDir(tmpDir.absolutePath() + "/completed");
        QDir corruptDir(tmpDir.absolutePath() + "/corrupt");
        QDir unauthorizedDir(tmpDir.absolutePath() + "/unauthorized");

        QVERIFY(directoryHasContents(incomingDir));
        QVERIFY(directoryHasContents(completedDir, QStringList("BC070214.CSV")));
        QVERIFY(directoryHasContents(corruptDir));
        QVERIFY(directoryHasContents(unauthorizedDir));
    }

    void convertAcquisitionTimeLimit()
    {
        tmpDir.mkdir("incoming");
        tmpDir.mkdir("completed");
        tmpDir.mkdir("corrupt");
        tmpDir.mkdir("unauthorized");

        Variant::Root config;
        config["/Profiles/aerosol/Sources/#0/Path"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "incoming"));
        config["/Profiles/aerosol/Sources/#0/Writable"].setBool(true);
        config["/Profiles/aerosol/Sources/#0/MinimumAge"].setInt64(0);
        config["/Profiles/aerosol/Sources/#0/Match/#0"].setString(
                "BC${MONTH}${DAY}${SHORTYEAR}.CSV");
        config["/Profiles/aerosol/CompletedLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "completed"));
        config["/Profiles/aerosol/UnauthorizedLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "unauthorized"));
        config["/Profiles/aerosol/CorruptLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "corrupt"));
        config["/Profiles/aerosol/Converter/Type"].setString("Acquisition");
        config["/Profiles/aerosol/Converter/Component"].setString(
                "acquire_magee_aethalometer162131");
        config["/Profiles/aerosol/Converter/Options/suffix"].setString("A81");
        config["/Profiles/aerosol/Converter/Acquisition/BCUnits"].setString("ug");
        config["/Profiles/aerosol/Converter/Acquisition/SampleTemperature"].setDouble(0);
        config["/Profiles/aerosol/Converter/Acquisition/SamplePressure"].setDouble(1013.25);
        config["/Profiles/aerosol/Converter/FileTimeLimit/Units"].setString("Day");
        config["/Profiles/aerosol/Converter/FileTimeLimit/Count"].setInt64(5);

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "configuration", "processing"}, config)});

        QVERIFY(writeData(tmpDir.absolutePath() + "/incoming/BC070215.CSV", QByteArray(
                "\"02-jul-14\",\"00:00\",  14.0,  19.0,  11.5,  12.7,   4.2,    30,    39,  4.8, 0.0211, 1.3202, 0.0212, 2.1401, 1.00,  .584, 0.0211, 2.0454, 0.0212, 2.2989, 1.00,  .412, 0.0211, 1.3748, 0.0212, 2.3600, 1.00,  .332, 0.0211, 1.8480, 0.0212, 1.1890, 1.00,  .250, 0.0211, 2.2155, 0.0212, 2.1224, 1.00,  .272, 0.0211, 2.2371, 0.0212, 3.3319, 1.00,  .231, 0.0211, 1.7546, 0.0212, 4.3526, 1.00,  .231\n"
                        "\"02-jul-14\",\"00:05\",   8.5,   5.7,  24.4,  27.3,  20.8,  21.5,  27.0,  4.8, 0.0211, 1.3201, 0.0212, 2.1402, 1.00,  .600, 0.0211, 2.0454, 0.0212, 2.2990, 1.00,  .421, 0.0211, 1.3745, 0.0212, 2.3604, 1.00,  .365, 0.0211, 1.8469, 0.0212, 1.1887, 1.00,  .283, 0.0211, 2.2148, 0.0212, 2.1223, 1.00,  .295, 0.0211, 2.2367, 0.0212, 3.3318, 1.00,  .249, 0.0211, 1.7542, 0.0212, 4.3525, 1.00,  .251\n"
                        "\"02-jul-14\",\"00:10\",  15.2,  22.4,  16.0,  18.5,    33,    67,    71,  4.8, 0.0211, 1.3199, 0.0212, 2.1405, 1.00,  .630, 0.0211, 2.0451, 0.0212, 2.2995, 1.00,  .455, 0.0211, 1.3746, 0.0212, 2.3610, 1.00,  .387, 0.0211, 1.8464, 0.0212, 1.1887, 1.00,  .305, 0.0211, 2.2143, 0.0212, 2.1225, 1.00,  .332, 0.0211, 2.2358, 0.0212, 3.3324, 1.00,  .303, 0.0211, 1.7536, 0.0212, 4.3532, 1.00,  .305\n"
                        "\"02-jul-14\",\"00:15\",  -4.1, -12.0, -10.5, -13.9, -11.3,   -47,   -77,  4.8, 0.0211, 1.3196, 0.0212, 2.1399, 1.00,  .622, 0.0211, 2.0446, 0.0212, 2.2986, 1.00,  .437, 0.0211, 1.3742, 0.0212, 2.3600, 1.00,  .373, 0.0211, 1.8455, 0.0212, 1.1879, 1.00,  .288, 0.0211, 2.2132, 0.0212, 2.1212, 1.00,  .319, 0.0211, 2.2349, 0.0212, 3.3297, 1.00,  .265, 0.0211, 1.7532, 0.0212, 4.3497, 1.00,  .246\n")));

        ComponentOptions options(component->getOptions());
        CPD3Action *action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        SequenceValue::Transfer expected;
        expected.emplace_back(SequenceName("sfa", "raw", "X1_A81"), Variant::Root(14.0), 1404259200,
                              1404259500);
        expected.emplace_back(SequenceName("sfa", "raw", "X1_A81"), Variant::Root(8.5), 1404259500,
                              1404259800);
        expected.emplace_back(SequenceName("sfa", "raw", "X1_A81"), Variant::Root(15.2), 1404259800,
                              1404260100);
        expected.emplace_back(SequenceName("sfa", "raw", "X1_A81"), Variant::Root(-4.1), 1404260100,
                              1404260400);

        QVERIFY(!checkArchiveContains(expected));

        QDir incomingDir(tmpDir.absolutePath() + "/incoming");
        QDir completedDir(tmpDir.absolutePath() + "/completed");
        QDir corruptDir(tmpDir.absolutePath() + "/corrupt");
        QDir unauthorizedDir(tmpDir.absolutePath() + "/unauthorized");

        QVERIFY(directoryHasContents(incomingDir));
        QVERIFY(directoryHasContents(completedDir, QStringList("BC070215.CSV")));
        QVERIFY(directoryHasContents(corruptDir));
        QVERIFY(directoryHasContents(unauthorizedDir));
    }

    void convertProcessing()
    {
        tmpDir.mkdir("incoming");
        tmpDir.mkdir("completed");
        tmpDir.mkdir("corrupt");
        tmpDir.mkdir("unauthorized");

        Variant::Root config;
        config["/Profiles/aerosol/Sources/#0/Path"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "incoming"));
        config["/Profiles/aerosol/Sources/#0/Writable"].setBool(true);
        config["/Profiles/aerosol/Sources/#0/MinimumAge"].setInt64(0);
        config["/Profiles/aerosol/Sources/#0/Match/#0"].setString(".*");
        config["/Profiles/aerosol/CompletedLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "completed"));
        config["/Profiles/aerosol/UnauthorizedLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "unauthorized"));
        config["/Profiles/aerosol/CorruptLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "corrupt"));
        config["/Profiles/aerosol/Converter/Type"].setString("Data");
        config["/Profiles/aerosol/Converter/Component"].setString("import_cpd2");
        config["/Profiles/aerosol/Converter/Processing/#0/Component"].setString("calc_dewpoint");

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "configuration", "processing"}, config)});

        QVERIFY(writeData(tmpDir.absolutePath() + "/incoming/process1.cpd2",
                          QByteArray("!row;colhdr;S11a,S11a;STN;EPOCH;DateTime;T_S11;U_S11\n"
                                             "!row;mvc;S11a,S11a;ZZZ;0;9999-99-99T99:99:99Z;9999.99;9999.99\n"
                                             "!row;varfmt;S11a,S11a;%s;%u;%04d-%02d-%02dT%02d:%02d:%02dZ;%07.2f;%07.2f\n"
                                             "S11a,SFA,1262304000,2010-01-01T00:00:00Z,0025.00,0050.00\n"
                                             "S11a,SFA,1262304060,2010-01-01T00:01:00Z,0026.00,0051.00\n")));

        ComponentOptions options(component->getOptions());
        CPD3Action *action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        SequenceValue::Transfer expected;
        expected.emplace_back(SequenceName("sfa", "raw", "T_S11"), Variant::Root(25.0), 1262304000,
                              1262304060);
        expected.emplace_back(SequenceName("sfa", "raw", "U_S11"), Variant::Root(50.0), 1262304000,
                              1262304060);
        expected.emplace_back(SequenceName("sfa", "raw", "TD_S11"),
                              Variant::Root(Dewpoint::dewpoint(25.0, 50.0)),
                              1262304000, 1262304060);
        expected.emplace_back(SequenceName("sfa", "raw", "T_S11"), Variant::Root(26.0), 1262304060,
                              1262304120);
        expected.emplace_back(SequenceName("sfa", "raw", "U_S11"), Variant::Root(51.0), 1262304060,
                              1262304120);
        expected.emplace_back(SequenceName("sfa", "raw", "TD_S11"),
                              Variant::Root(Dewpoint::dewpoint(26.0, 51.0)),
                              1262304060, 1262304120);

        QVERIFY(checkArchiveContains(expected));

        QDir incomingDir(tmpDir.absolutePath() + "/incoming");
        QDir completedDir(tmpDir.absolutePath() + "/completed");
        QDir corruptDir(tmpDir.absolutePath() + "/corrupt");
        QDir unauthorizedDir(tmpDir.absolutePath() + "/unauthorized");

        QVERIFY(directoryHasContents(incomingDir));
        QVERIFY(directoryHasContents(completedDir, QStringList("process1.cpd2")));
        QVERIFY(directoryHasContents(corruptDir));
        QVERIFY(directoryHasContents(unauthorizedDir));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
