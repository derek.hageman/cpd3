/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QLoggingCategory>

#include "transfer/upload.hxx"
#include "io/drivers/file.hxx"

#include "process_incoming.hxx"

Q_DECLARE_LOGGING_CATEGORY(log_component_process_incoming)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Transfer;

namespace {
enum InstanceNumberingResult {
    InstancedFailure, InstancedSuccess, InstancedDuplicated,
};
};

/* Do a byte for byte compare, since we're likely only reading each file
 * once anyway so a hash would be redundant (it would still inspect each byte
 * necessarily) */
static bool filesIdentical(const QFileInfo &a, const QFileInfo &b)
{
    if (a.size() != b.size())
        return false;

    auto readA = IO::Access::file(a, IO::File::Mode::readOnly())->block();
    if (!readA)
        return false;

    auto readB = IO::Access::file(b, IO::File::Mode::readOnly())->block();
    if (!readB)
        return false;

    Util::ByteArray dataA;
    Util::ByteArray dataB;
    while (!readA->readFinished()) {
        dataA.clear();
        dataB.clear();
        readA->read(dataA, 65536);
        readB->read(dataB, 65536);
        if (dataA != dataB)
            return false;
    }
    if (!readB->readFinished())
        return false;

    return !readA->inError() && !readB->inError();
}

static InstanceNumberingResult applyInstanceNumbering(const QFileInfo &info,
                                                      const QRegExp &re,
                                                      int captureIndex,
                                                      Variant::Write &eventInformation,
                                                      QFileInfo &targetInfo)
{
    QString baseName(info.fileName());
    if (!re.exactMatch(baseName))
        return InstancedFailure;

    QFileInfo check(QDir(targetInfo.absoluteFilePath()), baseName);
    if (!check.exists() && !check.isSymLink()) {
        targetInfo = check;
        return InstancedSuccess;
    }
    if (filesIdentical(info, check)) {
        qCDebug(log_component_process_incoming) << "Identical files detected"
                                                << info.absoluteFilePath() << "and"
                                                << check.absoluteFilePath() << ", output skipped";
        targetInfo = check;
        eventInformation["Existing"].setString(check.absoluteFilePath());
        return InstancedDuplicated;
    }

    for (int instanceID = 1; instanceID <= 9999; ++instanceID) {
        QString replaced(baseName);
        int p = re.pos(captureIndex);
        if (p >= 0) {
            int length = re.cap(captureIndex).length();
            if (length > 0) {
                QString nText(QString::number(instanceID));
                replaced.replace(p, length, nText);
            }
        }

        check = QFileInfo(QDir(targetInfo.absoluteFilePath()), replaced);
        if (!check.exists() && !check.isSymLink()) {
            targetInfo = check;
            return InstancedSuccess;
        }

        if (filesIdentical(info, check)) {
            qCDebug(log_component_process_incoming) << "Identical files detected"
                                                    << info.absoluteFilePath() << "and"
                                                    << check.absoluteFilePath()
                                                    << ", output skipped";
            targetInfo = check;
            eventInformation["Existing"].setString(check.absoluteFilePath());
            return InstancedDuplicated;
        }
    }
    return InstancedFailure;
}

ProcessIncoming::OutputResult ProcessIncoming::outputTo(DownloadFile *file,
                                                        Variant::Write &eventInformation,
                                                        const QString &target)
{
    QFileInfo info(file->fileInfo());
    QString baseName(info.fileName());
    auto destinationName = substitutions.apply(target);
    QFileInfo targetInfo(destinationName);

    if (destinationName.endsWith('/')) {
        destinationName.chop(1);
        targetInfo = QFileInfo(destinationName);
        if (!targetInfo.exists()) {
            targetInfo.dir().mkpath(targetInfo.fileName());
        }
    }

    if (targetInfo.isDir()) {
        int instanceMatch = -1;
        QString matchedPattern(IncomingDownloader::applyInstanceMatching(this, file->getPattern(),
                                                                         instanceMatch));
        if (instanceMatch > 0) {
            QRegExp re(matchedPattern, Qt::CaseInsensitive);
            switch (applyInstanceNumbering(info, re, instanceMatch, eventInformation, targetInfo)) {
            case InstancedFailure:
                targetInfo = QFileInfo(QDir(substitutions.apply(target)), baseName);
                break;
            case InstancedSuccess:
                break;
            case InstancedDuplicated:
                substitutions.setFile("local", targetInfo);
                return Output_Duplicated;
            }
        } else if (!baseName.isEmpty()) {
            targetInfo = QFileInfo(QDir(substitutions.apply(target)), baseName);
        } else {
            targetInfo = QFileInfo(QDir(substitutions.apply(target)), "output");
        }
    }
    if (targetInfo.exists() || targetInfo.isDir() || targetInfo.isSymLink()) {
        if (targetInfo.exists() && filesIdentical(info, targetInfo)) {
            eventInformation["Existing"].setString(targetInfo.absoluteFilePath());
            return Output_Duplicated;
        }
        for (int duplicate = 1; duplicate <= 99999; duplicate++) {
            targetInfo = QFileInfo(QDir(substitutions.apply(target)),
                                   QString("%2_%1").arg(duplicate).arg(baseName));
            if (!targetInfo.exists() && !targetInfo.isSymLink())
                break;
            if (filesIdentical(info, targetInfo)) {
                eventInformation["Existing"].setString(targetInfo.absoluteFilePath());
                substitutions.setFile("local", targetInfo);
                return Output_Duplicated;
            }
        }
    }

    if (file->writable()) {
        eventInformation["MovedTo"].setString(targetInfo.absoluteFilePath());
        qCDebug(log_component_process_incoming) << "Moving" << info.absoluteFilePath() << "to"
                                                << targetInfo.absoluteFilePath();
        auto result = FileUploader::moveLocalFile(info, targetInfo, false) ? Output_Completed
                                                                           : Output_Failed;
        targetInfo.refresh();
        substitutions.setFile("local", targetInfo);
        return result;
    } else {
        eventInformation["CopiedTo"].setString(targetInfo.absoluteFilePath());
        qCDebug(log_component_process_incoming) << "Copying" << info.absoluteFilePath() << "to"
                                                << targetInfo.absoluteFilePath();
        auto result = FileUploader::copyLocalFile(info, targetInfo, false) ? Output_Completed
                                                                           : Output_Failed;
        targetInfo.refresh();
        substitutions.setFile("local", targetInfo);
        return result;
    }
}

bool ProcessIncoming::corruptedFile(DownloadFile *file,
                                    Variant::Write &eventInformation,
                                    const Variant::Read &currentConfiguration)
{
    eventInformation["Corrupted"].setBool(true);

    if (file->writable() && currentConfiguration["CorruptRemove"].toBool()) {
        QFileInfo info(file->fileInfo());
        qCDebug(log_component_process_incoming) << "Removing corrupted file"
                                                << info.absoluteFilePath();
        if (!QFile::remove(info.absoluteFilePath())) {
            qCWarning(log_component_process_incoming) << "Failed to remove corrupted file";
            return false;
        }
        return true;
    }

    QString target(currentConfiguration["CorruptLocation"].toQString());
    if (target.isEmpty()) {
        if (file->writable()) {
            target = QDir(QDir::tempPath()).path();
            qCDebug(log_component_process_incoming)
                    << "No path specified for corrupted move on a writable source";
        } else {
            return true;
        }
    }

    switch (outputTo(file, eventInformation, target)) {
    case Output_Failed:
        return false;
    case Output_Completed:
        return true;
    case Output_Duplicated:
        if (file->writable()) {
            QFileInfo info(file->fileInfo());
            if (!QFile::remove(info.absoluteFilePath())) {
                qCWarning(log_component_process_incoming) << "Failed to remove duplicate file"
                                                          << info.absoluteFilePath();
                return false;
            }
        }
        return true;
    }

    return false;
}

bool ProcessIncoming::unauthorizedFile(DownloadFile *file,
                                       Variant::Write &eventInformation,
                                       const Variant::Read &currentConfiguration)
{
    eventInformation["Unauthorized"].setBool(true);

    if (file->writable() && currentConfiguration["UnauthorizedRemove"].toBool()) {
        QFileInfo info(file->fileInfo());
        qCDebug(log_component_process_incoming) << "Removing unauthorized file"
                                                << info.absoluteFilePath();
        if (!QFile::remove(info.absoluteFilePath())) {
            qCWarning(log_component_process_incoming) << "Failed to remove unauthorized file";
            return false;
        }
        return true;
    }

    QString target(currentConfiguration["UnauthorizedLocation"].toQString());
    if (target.isEmpty()) {
        if (file->writable()) {
            target = QDir(QDir::tempPath()).path();
            qCDebug(log_component_process_incoming)
                    << "No path specified for unauthorized move on a writable source";
        } else {
            return true;
        }
    }

    switch (outputTo(file, eventInformation, target)) {
    case Output_Failed:
        return false;
    case Output_Completed:
        return true;
    case Output_Duplicated:
        if (file->writable()) {
            QFileInfo info(file->fileInfo());
            if (!QFile::remove(info.absoluteFilePath())) {
                qCWarning(log_component_process_incoming) << "Failed to remove duplicate file"
                                                          << info.absoluteFilePath();
                return false;
            }
        }
        return true;
    }

    return true;
}

bool ProcessIncoming::acceptFile(DownloadFile *file,
                                 Variant::Write &eventInformation,
                                 const Variant::Read &currentConfiguration)
{
    if (currentConfiguration["CompletedIgnore"].toBool()) {
        QFileInfo info(file->fileInfo());
        qCDebug(log_component_process_incoming) << "Ignoring completed file"
                                                << info.absoluteFilePath();
        return true;
    }
    if (file->writable() && currentConfiguration["CompletedRemove"].toBool()) {
        QFileInfo info(file->fileInfo());
        qCDebug(log_component_process_incoming) << "Removing completed file"
                                                << info.absoluteFilePath();
        if (!QFile::remove(info.absoluteFilePath())) {
            qCWarning(log_component_process_incoming) << "Failed to remove completed file";
            return false;
        }
        return true;
    }

    QString target(currentConfiguration["CompletedLocation"].toQString());
    if (target.isEmpty()) {
        if (file->writable()) {
            target = QDir(QDir::tempPath()).path();
            qCDebug(log_component_process_incoming)
                    << "No path specified for completed move on a writable source";
        } else {
            return true;
        }
    }

    switch (outputTo(file, eventInformation, target)) {
    case Output_Failed:
        return false;
    case Output_Completed:
        return true;
    case Output_Duplicated:
        if (file->writable()) {
            QFileInfo info(file->fileInfo());
            if (!QFile::remove(info.absoluteFilePath())) {
                qCWarning(log_component_process_incoming) << "Failed to remove duplicate file"
                                                          << info.absoluteFilePath();
                return false;
            }
        }
        return true;
    }

    return true;
}