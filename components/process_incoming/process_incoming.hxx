/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef PROCESSINCOMING_H
#define PROCESSINCOMING_H

#include "core/first.hxx"

#include <mutex>
#include <memory>
#include <unordered_map>
#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QList>
#include <QStringList>
#include <QHash>
#include <QIODevice>
#include <QByteArray>
#include <QFileInfo>
#include <QPair>
#include <QProcess>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "core/threading.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/externalsink.hxx"
#include "datacore/segment.hxx"
#include "datacore/streampipeline.hxx"
#include "transfer/datafile.hxx"
#include "transfer/package.hxx"
#include "transfer/download.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "io/process.hxx"
#include "io/drivers/file.hxx"

struct ProcessIncomingAcquisitionState {
    QByteArray state;
    double lastFileTime;

    bool referenced;

    ProcessIncomingAcquisitionState();
};

class ProcessIncoming : public CPD3::CPD3Action {
Q_OBJECT

    std::string profile;
    std::vector<CPD3::Data::SequenceName::Component> stations;
    double lockTimeout;
    double afterTime;
    bool haveAfter;

    bool terminated;
    std::mutex mutex;

    CPD3::Threading::Signal<> terminateRequested;

    CPD3::TextSubstitutionStack substitutions;

    std::unordered_map<QString, std::shared_ptr<ProcessIncomingAcquisitionState> > acquisitionState;

    void restoreAcquisitionState(const QByteArray &state);

    QByteArray saveAcquisitionState(bool pruneUnused) const;

    class IncomingDownloader : public CPD3::Transfer::FileDownloader {
        ProcessIncoming *parent;

        friend class ProcessIncoming;

    public:
        IncomingDownloader(ProcessIncoming *parent);

        virtual ~IncomingDownloader();

        static QString applyInstanceMatching(ProcessIncoming *parent,
                                             const QString &input,
                                             int &groupIndex);

    protected:
        QString applySubstitutions(const QString &input) const override;

        class MatchingStack : public CPD3::TextSubstitutionStack {
            QList<TimeCapture> &captures;
        public:
            int *instanceCapture;

            MatchingStack(const CPD3::TextSubstitutionStack &base, QList<TimeCapture> &captures);

            virtual ~MatchingStack();

        protected:
            QString substitution(const QStringList &elements) const override;
        };

        QString applyMatching(const QString &input, QList<TimeCapture> &captures) const override;

        void pushTemporaryOutput(const QFileInfo &fileName) override;

        void popTemporaryOutput() override;

        void pushFilterDirectory(const QDir &dir, const QDir &rootDir) override;

        void pushFilterRemote(const std::string &path, const std::string &rootPath) override;

        void popFilter() override;
    };

    friend class IncomingDownloader;

    class IncomingTransferUnpacker : public CPD3::Transfer::TransferUnpack {
        ProcessIncoming *parent;
        CPD3::Data::Variant::Read fileConfiguration;
        CPD3::Data::ValueSegment::Transfer authorizations;

        QString signingCertificate;
        bool signatureRejected;
    public:
        IncomingTransferUnpacker(ProcessIncoming *parent,
                                 const CPD3::Data::Variant::Read &fileConfiguration,
                                 const CPD3::Data::ValueSegment::Transfer &authorizations);

        virtual ~IncomingTransferUnpacker();

        QString getSigningCertificateID() const;

        bool isUnauthorized() const;

    protected:
        QString applySubstitutions(const QString &input) const override;

        CPD3::Data::Variant::Read getCertificate(const QByteArray &id,
                                                 bool decryption,
                                                 int depth) override;

        CPD3::Data::Variant::Read getKeyForCertificate(const QByteArray &id, int depth) override;

        bool signatureAuthorized(const QList<QByteArray> &ids, int depth) override;

        bool signingCertificateValid(const QSslCertificate &certificate, int depth) override;
    };

    friend class IncomingTransferUnpacker;

    enum ProcessingResult {
        Processing_Accepted, Processing_Corrupted, Processing_Unauthorized, Processing_Error,
    };

    ProcessingResult processDataFile(CPD3::Transfer::DownloadFile *file,
                                     CPD3::Data::Variant::Write &eventInformation,
                                     const CPD3::Data::SequenceName::Component &station,
                                     const CPD3::Data::Variant::Read &fileConfiguration,
                                     const CPD3::Data::ValueSegment::Transfer &stationConfiguration,
                                     bool canTerminate);

    ProcessingResult acquireFile(CPD3::Transfer::DownloadFile *file,
                                 CPD3::Data::Variant::Write &eventInformation,
                                 const CPD3::Data::Variant::Read &fileConfiguration,
                                 std::shared_ptr<CPD3::IO::File::Backing> &localFile);

    ProcessingResult configurePipeline(CPD3::Data::Variant::Write &eventInformation,
                                       const CPD3::Data::SequenceName::Component &station,
                                       const CPD3::Data::Variant::Read &converterConfiguration,
                                       const CPD3::Data::ValueSegment::Transfer &stationConfiguration,
                                       CPD3::Data::StreamPipeline &pipeline);

    ProcessingResult pipelineFileInput(CPD3::Transfer::DownloadFile *file,
                                       CPD3::Data::Variant::Write &eventInformation,
                                       const CPD3::Data::SequenceName::Component &station,
                                       const CPD3::Data::Variant::Read &converterConfiguration,
                                       const CPD3::Data::ValueSegment::Transfer &stationConfiguration,
                                       CPD3::Data::StreamPipeline &pipeline,
                                       std::shared_ptr<CPD3::IO::File::Backing> &localFile);

    ProcessingResult executePipeline(CPD3::Transfer::DownloadFile *file,
                                     CPD3::Data::Variant::Write &eventInformation,
                                     const CPD3::Data::SequenceName::Component &station,
                                     const CPD3::Data::Variant::Read &converterConfiguration,
                                     const CPD3::Data::ValueSegment::Transfer &stationConfiguration,
                                     CPD3::Data::StreamPipeline &pipeline,
                                     std::shared_ptr<CPD3::IO::Process::Instance> &&process = {});

    CPD3::IO::Process::Spawn programSpawn(const CPD3::Data::Variant::Read &converterConfiguration);

    ProcessingResult pipelineFromProgram(CPD3::Data::Variant::Write &eventInformation,
                                         const CPD3::Data::SequenceName::Component &station,
                                         const CPD3::Data::Variant::Read &converterConfiguration,
                                         CPD3::Data::StreamPipeline &pipeline,
                                         const std::shared_ptr<CPD3::IO::File::Backing> &localFile,
                                         std::shared_ptr<CPD3::IO::Process::Instance> &process);

    ProcessingResult pipelineFromComponent(CPD3::Data::Variant::Write &eventInformation,
                                           const CPD3::Data::SequenceName::Component &station,
                                           const CPD3::Data::Variant::Read &converterConfiguration,
                                           CPD3::Data::StreamPipeline &pipeline,
                                           const std::shared_ptr<
                                                   CPD3::IO::File::Backing> &localFile);

    ProcessingResult pipelineFromAcquisition(CPD3::Transfer::DownloadFile *file,
                                             CPD3::Data::Variant::Write &eventInformation,
                                             const CPD3::Data::SequenceName::Component &station,
                                             const CPD3::Data::Variant::Read &converterConfiguration,
                                             const CPD3::Data::ValueSegment::Transfer &stationConfiguration,
                                             CPD3::Data::StreamPipeline &pipeline,
                                             const std::shared_ptr<
                                                     CPD3::IO::File::Backing> &localFile);

    ProcessingResult executeStandalone(CPD3::Data::Variant::Write &eventInformation,
                                       const CPD3::Data::Variant::Read &converterConfiguration,
                                       const std::shared_ptr<CPD3::IO::File::Backing> &localFile);

    ProcessingResult processFile(CPD3::Transfer::DownloadFile *file,
                                 CPD3::Data::Variant::Write &eventInformation,
                                 const CPD3::Data::SequenceName::Component &station,
                                 const CPD3::Data::Variant::Read &currentConfiguration,
                                 const CPD3::Data::ValueSegment::Transfer &stationConfiguration,
                                 bool isFirstFile);

    enum OutputResult {
        Output_Failed, Output_Completed, Output_Duplicated,
    };

    OutputResult outputTo(CPD3::Transfer::DownloadFile *file,
                          CPD3::Data::Variant::Write &eventInformation,
                          const QString &target);

    bool acceptFile(CPD3::Transfer::DownloadFile *file,
                    CPD3::Data::Variant::Write &eventInformation,
                    const CPD3::Data::Variant::Read &fileConfiguration);

    bool unauthorizedFile(CPD3::Transfer::DownloadFile *file,
                          CPD3::Data::Variant::Write &eventInformation,
                          const CPD3::Data::Variant::Read &fileConfiguration);

    bool corruptedFile(CPD3::Transfer::DownloadFile *file,
                       CPD3::Data::Variant::Write &eventInformation,
                       const CPD3::Data::Variant::Read &fileConfiguration);

    bool invokeCommand(const QString &program, const QStringList &arguments, bool canTerminate);

    bool executeActions(const CPD3::Data::Variant::Read &config, bool canTerminate);

public:
    ProcessIncoming();

    ProcessIncoming(const CPD3::ComponentOptions &options,
                    const std::vector<CPD3::Data::SequenceName::Component> &setStations = {});

    virtual ~ProcessIncoming();

    virtual void signalTerminate();

    /**
     * Test if the processor has been terminated.
     * 
     * @return true if the processor has been terminated
     */
    bool testTerminated();

protected:
    virtual void run();

};

class ProcessIncomingComponent : public QObject, virtual public CPD3::ActionComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::ActionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.process_incoming"
                              FILE
                              "process_incoming.json")

public:
    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual int actionAllowStations();

    CPD3::CPD3Action *createAction(const CPD3::ComponentOptions &options = {},
                                   const std::vector<std::string> &stations = {}) override;

    QString promptActionContinue(const CPD3::ComponentOptions &options = {},
                                 const std::vector<std::string> &stations = {}) override;
};

#endif
