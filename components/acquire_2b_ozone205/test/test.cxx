/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_capture.hxx"
#include "acquire_test_control.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    double unpolledRemaining;
    double unpolledInterval;
    QDateTime time;
    int recordIndex;

    bool enableRecordIndex;
    bool enableCellOzone;
    bool enableAnalogInputs;

    double ZX1;
    double ZX2;
    double T;
    double P;
    double Q;
    double V[3];

    ModelInstrument()
            : incoming(),
              outgoing(),
              unpolledRemaining(0),
              unpolledInterval(2.0),
              time(QDate(2016, 8, 30), QTime(1, 2, 0)),
              recordIndex(0),
              enableRecordIndex(false),
              enableCellOzone(false),
              enableAnalogInputs(false)
    {
        ZX1 = 12.0;
        ZX2 = 13.0;
        T = 25.0;
        P = 820.0;
        Q = 1.512;

        V[0] = 1.5;
        V[1] = 1.75;
        V[2] = 2.0;
    }

    double instrumentOzone() const
    {
        return (ZX1 + ZX2) * 0.5;
    }

    void advance(double seconds)
    {
        int idxCR = -1;
        while ((idxCR = incoming.indexOf('\r')) >= 0) {
            QByteArray line(incoming.mid(0, idxCR).trimmed());
            incoming = incoming.mid(idxCR + 1);

            if (line == "m") {
                unpolledRemaining = FP::undefined();
                outgoing.append("menu>");
                continue;
            } else if (line == "x") {
                outgoing.append("Exiting menu\r\n");
                unpolledRemaining = unpolledInterval;
                continue;
            } else if (line.startsWith("cd")) {
                outgoing.append("Set time\r\nDate\r\n");
                time.setDate(QDate(line.mid(6, 2).toInt() + 2000, line.mid(4, 2).toInt(),
                                   line.mid(2, 2).toInt()));
                continue;
            } else if (line.startsWith("ct")) {
                outgoing.append("Set time\r\nTime\r\n");
                time.setTime(QTime(line.mid(2, 2).toInt(), line.mid(4, 2).toInt(),
                                   line.mid(6, 2).toInt()));
                continue;
            } else if (line.startsWith("a") && line.size() > 1) {
                outgoing.append(
                        "Enter Average setting (0 = 2 second, 1 = 10 second, 2 = 1 minute, 3 = 5 minute,\n"
                                "        4 = 1 hour):menu>");
                switch (line.at(1)) {
                case '0':
                    unpolledInterval = 2.0;
                    continue;
                case '1':
                    unpolledInterval = 10.0;
                    continue;
                case '2':
                    unpolledInterval = 60.0;
                    continue;
                case '3':
                    unpolledInterval = 300.0;
                    continue;
                case '4':
                    unpolledInterval = 3600.0;
                    continue;
                default:
                    break;
                }
            } else if (line == "r") {
                outgoing.append("Set to raw mode\r\nmenu>");
                enableCellOzone = true;
                continue;
            } else if (line == "v") {
                outgoing.append("Set to normal mode\r\nmenu>");
                enableCellOzone = false;
                continue;
            } else if (line == "h") {
                if (enableCellOzone)
                    outgoing.append(
                            "Ozone_CellA,Ozone_CellB,,Ozone,Temperature,Pressure,Flow,Date,Time\r\nmenu>");
                else
                    outgoing.append("Ozone,Temperature,Pressure,Flow,Date,Time\r\nmenu>");
                continue;
            }

            qDebug() << "Unrecognized instrument command:" << line;
        }

        time = time.addMSecs((qint64) floor(seconds * 1000.0 + 0.5));
        if (FP::defined(unpolledRemaining)) {
            unpolledRemaining -= seconds;
            while (unpolledRemaining <= 0.0) {
                unpolledRemaining += unpolledInterval;

                QByteArray report;

                ++recordIndex;

                if (enableRecordIndex) {
                    outgoing.append(QByteArray::number(recordIndex));
                    outgoing.append(',');
                }

                if (enableCellOzone) {
                    outgoing.append(QByteArray::number(ZX1, 'f', 1));
                    outgoing.append(',');
                    outgoing.append(QByteArray::number(ZX2, 'f', 1));
                    outgoing.append(',');
                }

                outgoing.append(QByteArray::number(instrumentOzone(), 'f', 1));
                outgoing.append(',');
                outgoing.append(QByteArray::number(T, 'f', 1));
                outgoing.append(',');
                outgoing.append(QByteArray::number(P, 'f', 1));
                outgoing.append(',');
                outgoing.append(QByteArray::number(Q * 1000.0, 'f', 0));
                outgoing.append(',');

                if (enableAnalogInputs) {
                    outgoing.append(QByteArray::number(V[0], 'f', 4));
                    outgoing.append(',');
                    outgoing.append(QByteArray::number(V[1], 'f', 4));
                    outgoing.append(',');
                    outgoing.append(QByteArray::number(V[2], 'f', 4));
                    outgoing.append(',');
                }

                outgoing.append(QByteArray::number(time.date().day()).rightJustified(2, '0'));
                outgoing.append('/');
                outgoing.append(QByteArray::number(time.date().month()).rightJustified(2, '0'));
                outgoing.append('/');
                outgoing.append(
                        QByteArray::number(time.date().year() % 100).rightJustified(2, '0'));
                outgoing.append(',');
                outgoing.append(QByteArray::number(time.time().hour()).rightJustified(2, '0'));
                outgoing.append(':');
                outgoing.append(QByteArray::number(time.time().minute()).rightJustified(2, '0'));
                outgoing.append(':');
                outgoing.append(QByteArray::number(time.time().second()).rightJustified(2, '0'));

                outgoing.append("\r\n");
            }
        }
    }
};

class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("X", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("P", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Q", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, const ModelInstrument &model,
                           double time = FP::undefined())
    {
        if (model.enableCellOzone) {
            if (!stream.hasMeta("ZX1", Variant::Root(), QString(), time))
                return false;
            if (!stream.hasMeta("ZX2", Variant::Root(), QString(), time))
                return false;
        }
        if (model.enableAnalogInputs) {
            if (!stream.hasMeta("V1", Variant::Root(), QString(), time))
                return false;
            if (!stream.hasMeta("V2", Variant::Root(), QString(), time))
                return false;
            if (!stream.hasMeta("V3", Variant::Root(), QString(), time))
                return false;
        }
        return true;
    }

    bool checkContiguous(StreamCapture &stream)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("X"))
            return false;
        if (!stream.checkContiguous("T"))
            return false;
        if (!stream.checkContiguous("P"))
            return false;
        if (!stream.checkContiguous("Q"))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream, const ModelInstrument &model,
                     double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("X", Variant::Root(model.instrumentOzone()), time))
            return false;
        if (!stream.hasAnyMatchingValue("T", Variant::Root(model.T), time))
            return false;
        if (!stream.hasAnyMatchingValue("P", Variant::Root(model.P), time))
            return false;
        if (!stream.hasAnyMatchingValue("Q", Variant::Root(model.Q), time))
            return false;
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream, const ModelInstrument &model,
                             double time = FP::undefined())
    {
        if (model.enableCellOzone) {
            if (!stream.hasAnyMatchingValue("ZX1", Variant::Root(model.ZX1), time))
                return false;
            if (!stream.hasAnyMatchingValue("ZX2", Variant::Root(model.ZX2), time))
                return false;
        }
        if (model.enableAnalogInputs) {
            if (!stream.hasAnyMatchingValue("V1", Variant::Root(model.V[0]), time))
                return false;
            if (!stream.hasAnyMatchingValue("V2", Variant::Root(model.V[1]), time))
                return false;
            if (!stream.hasAnyMatchingValue("V3", Variant::Root(model.V[2]), time))
                return false;
        }
        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_2b_ozone205"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_2b_ozone205"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get(), 1451606400.0);
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 100; i++) {
            control.advance(2.0);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void passiveAutoprobeAnalog()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.enableAnalogInputs = true;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get(), 1451606400.0);
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 100; i++) {
            control.advance(2.0);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void passiveAutoprobeLogged()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.enableRecordIndex = true;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get(), 1451606400.0);
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 100; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get(), 1451606400.0);
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(control.time());

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.25);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get(), 1451606400.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 50; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get(), 1451606400.0);
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(control.time());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 200; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
        }

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
