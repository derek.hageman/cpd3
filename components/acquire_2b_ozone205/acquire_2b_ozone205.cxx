/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/csv.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/wavelength.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_2b_ozone205.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

Acquire2BOzone205::Configuration::Configuration() : start(FP::undefined()),
                                                    end(FP::undefined()),
                                                    strictMode(true),
                                                    reportInterval(2.0)
{ }

Acquire2BOzone205::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s), end(e), strictMode(other.strictMode), reportInterval(other.reportInterval)
{ }

Acquire2BOzone205::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s), end(e), strictMode(true), reportInterval(2.0)
{
    setFromSegment(other);
}

Acquire2BOzone205::Configuration::Configuration(const Configuration &under,
                                                const ValueSegment &over,
                                                double s,
                                                double e) : start(s),
                                                            end(e),
                                                            strictMode(under.strictMode),
                                                            reportInterval(under.reportInterval)
{
    setFromSegment(over);
}

void Acquire2BOzone205::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["StrictMode"].exists())
        strictMode = config["StrictMode"].toBool();

    {
        double v = config["ReportInterval"].toDouble();
        if (FP::defined(v) && v > 0.0)
            reportInterval = v;
    }
}


void Acquire2BOzone205::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("2B");
    instrumentMeta["Model"].setString("205");

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    haveCellOzone = false;
    haveAnalogInputs = false;
}

Acquire2BOzone205::Acquire2BOzone205(const ValueSegment::Transfer &configData,
                                     const std::string &loggingContext) : FramedInstrument(
        "ozone205", loggingContext),
                                                                          lastRecordTime(
                                                                                  FP::undefined()),
                                                                          autoprobeStatus(
                                                                                  AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                          responseState(
                                                                                  RESP_PASSIVE_WAIT)
{
    setDefaultInvalid();
    config.append(Configuration());
    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
    configurationChanged();
}

void Acquire2BOzone205::setToAutoprobe()
{
    responseState = RESP_AUTOPROBE_PASSIVE_INITIALIZE;
    autoprobeValidRecords = 0;
}

void Acquire2BOzone205::setToInteractive()
{ responseState = RESP_INTERACTIVE_INITIALIZE; }


ComponentOptions Acquire2BOzone205Component::getBaseOptions()
{
    ComponentOptions options;

    return options;
}

Acquire2BOzone205::Acquire2BOzone205(const ComponentOptions &options,
                                     const std::string &loggingContext) : FramedInstrument(
        "ozone205", loggingContext),
                                                                          lastRecordTime(
                                                                                  FP::undefined()),
                                                                          autoprobeStatus(
                                                                                  AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                          responseState(
                                                                                  RESP_PASSIVE_WAIT)
{
    Q_UNUSED(options);

    setDefaultInvalid();

    config.append(Configuration());
    configurationChanged();
}

Acquire2BOzone205::~Acquire2BOzone205()
{
}


void Acquire2BOzone205::logValue(double startTime,
                                 double endTime,
                                 SequenceName::Component name,
                                 Variant::Root &&value)
{
    if (!loggingEgress && !realtimeEgress) return;
    SequenceValue dv({{}, "raw", std::move(name)}, std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        if (FP::defined(startTime))
            loggingEgress->incomingData(std::move(dv));
        return;
    }
    if (loggingEgress && FP::defined(startTime))
        loggingEgress->incomingData(dv);
    if (FP::defined(endTime)) {
        dv.setStart(endTime);
        dv.setEnd(endTime + 1.0);
    } else if (FP::defined(startTime)) {
        dv.setEnd(startTime + 1.0);
    }
    realtimeEgress->incomingData(std::move(dv));
}

void Acquire2BOzone205::realtimeValue(double time,
                                      SequenceName::Component name,
                                      Variant::Root &&value)
{
    if (!realtimeEgress) return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

SequenceValue::Transfer Acquire2BOzone205::buildLogMeta(double time) const
{
    Q_ASSERT(!config.isEmpty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_2b_ozone205");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["Model"].set(instrumentMeta["Model"]);

    result.emplace_back(SequenceName({}, "raw_meta", "X"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000000.00");
    result.back().write().metadataReal("Units").setString("ppb");
    result.back().write().metadataReal("Description").setString("Ozone concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Mean Ozone"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "T"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Sample temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);

    result.emplace_back(SequenceName({}, "raw_meta", "P"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Sample pressure");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);

    result.emplace_back(SequenceName({}, "raw_meta", "Q"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Sample flow");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    return result;
}

SequenceValue::Transfer Acquire2BOzone205::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_2b_ozone205");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["InstrumentID"].set(instrumentMeta["InstrumentID"]);
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    processing["Model"].set(instrumentMeta["Model"]);

    if (haveCellOzone) {
        result.emplace_back(SequenceName({}, "raw_meta", "ZX1"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("000000.00");
        result.back().write().metadataReal("Units").setString("ppb");
        result.back().write().metadataReal("Description").setString("Cell A ozone concentration");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Name")
              .setString(QObject::tr("Cell A Ozone"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);

        result.emplace_back(SequenceName({}, "raw_meta", "ZX2"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("000000.00");
        result.back().write().metadataReal("Units").setString("ppb");
        result.back().write().metadataReal("Description").setString("Cell B ozone concentration");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Name")
              .setString(QObject::tr("Cell B Ozone"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);
    }


    if (haveAnalogInputs) {
        result.emplace_back(SequenceName({}, "raw_meta", "V1"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("00.0000");
        result.back().write().metadataReal("Units").setString("V");
        result.back().write().metadataReal("Description").setString("Analog input A");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);

        result.emplace_back(SequenceName({}, "raw_meta", "V2"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("00.0000");
        result.back().write().metadataReal("Units").setString("V");
        result.back().write().metadataReal("Description").setString("Analog input B");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);

        result.emplace_back(SequenceName({}, "raw_meta", "V3"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("00.0000");
        result.back().write().metadataReal("Units").setString("V");
        result.back().write().metadataReal("Description").setString("Analog input C");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    }

    return result;
}

int Acquire2BOzone205::processDateTime(const Util::ByteView &date,
                                       const Util::ByteView &time,
                                       double &frameTime)
{
    auto fields = Util::as_deque(date.split('/'));
    Util::ByteView field;
    bool ok = false;

    if (fields.empty()) return 1;
    field = fields.front().string_trimmed();
    fields.pop_front();
    qint64 iday = field.parse_i32(&ok);
    if (!ok) return 2;
    if (iday < 1) return 3;
    if (iday > 31) return 4;
    Variant::Root day(iday);
    remap("DAY", day);
    iday = day.read().toInt64();

    if (fields.empty()) return 5;
    field = fields.front().string_trimmed();
    fields.pop_front();
    qint64 imonth = field.parse_i32(&ok);
    if (!ok) return 6;
    if (imonth < 1) return 7;
    if (imonth > 12) return 8;
    Variant::Root month(imonth);
    remap("MONTH", month);
    imonth = month.read().toInt64();

    if (fields.empty()) return 9;
    field = fields.front().string_trimmed();
    fields.pop_front();
    qint64 iyear = field.parse_i32(&ok);
    if (!ok) return 10;
    if (iyear < 1900) {
        if (iyear < 0) return 11;
        if (iyear > 99) return 12;
        int century;
        if (FP::defined(frameTime)) {
            century = Time::toDateTime(frameTime).date().year() / 100;
        } else {
            /* Doesn't look like these existed before 2000, so just assume 2k */
            century = 20;
        }
        iyear += century * 100;
    } else {
        if (iyear < 1990) return 13;
        if (iyear > 2999) return 14;
    }
    Variant::Root year(iyear);
    remap("YEAR", year);
    iyear = year.read().toInt64();

    fields = Util::as_deque(time.split(':'));

    if (fields.empty()) return 15;
    field = fields.front().string_trimmed();
    fields.pop_front();
    qint64 ihour = field.parse_i32(&ok);
    if (!ok) return 16;
    if (ihour < 0) return 17;
    if (ihour > 24) return 18;
    Variant::Root hour(ihour);
    remap("HOUR", hour);
    ihour = hour.read().toInt64();

    if (fields.empty()) return 19;
    field = fields.front().string_trimmed();
    fields.pop_front();
    qint64 iminute = field.parse_i32(&ok);
    if (!ok) return 20;
    if (iminute < 0) return 21;
    if (iminute > 59) return 22;
    Variant::Root minute(iminute);
    remap("MINUTE", minute);
    iminute = minute.read().toInt64();

    if (fields.empty()) return 23;
    field = fields.front().string_trimmed();
    fields.pop_front();
    qint64 isecond = field.parse_i32(&ok);
    if (!ok) return 24;
    if (isecond < 0) return 25;
    if (isecond > 60) return 26;
    Variant::Root second(isecond);
    remap("SECOND", second);
    isecond = second.read().toInt64();

    if (!FP::defined(frameTime) && INTEGER::defined(iyear) &&
            iyear >= 1900 &&
            iyear <= 2999 &&
            INTEGER::defined(imonth) &&
            imonth >= 1 &&
            imonth <= 12 &&
            INTEGER::defined(iday) &&
            iday >= 1 &&
            iday <= 31 &&
            INTEGER::defined(ihour) &&
            ihour >= 0 &&
            ihour <= 23 &&
            INTEGER::defined(iminute) &&
            iminute >= 0 &&
            iminute <= 59 &&
            INTEGER::defined(isecond) &&
            isecond >= 0 &&
            isecond <= 60) {
        frameTime = Time::fromDateTime(QDateTime(QDate((int) iyear, (int) imonth, (int) iday),
                                                 QTime((int) ihour, (int) iminute, (int) isecond),
                                                 Qt::UTC));

        configAdvance(frameTime);
    }

    return 0;
}

static double convertTemperature(double T)
{
    if (!FP::defined(T))
        return T;
    if (T > 200.0)
        T -= 273.15;
    return T;
}

static double convertFlow(double Q)
{
    if (!FP::defined(Q))
        return Q;
    return Q / 1000.0;
}

int Acquire2BOzone205::processRecord(const Util::ByteView &line, double &frameTime)
{
    Q_ASSERT(!config.isEmpty());

    auto fields = CSV::acquisitionSplit(line);
    Util::ByteView field;
    bool ok = false;

    if (fields.size() < 6)
        return 1;

    std::size_t dateIndex = 4;
    int timeCode = -1;
    for (std::size_t max = std::min<std::size_t>(fields.size(), 11) - 1;
            dateIndex < max;
            ++dateIndex) {
        timeCode = processDateTime(fields[dateIndex], fields.at(dateIndex + 1), frameTime);
        if (timeCode == 0)
            break;
    }

    if (timeCode != 0) {
        if (timeCode < 0)
            return timeCode;
        return timeCode + 1000;
    }

    Q_ASSERT(dateIndex < fields.size());
    fields.erase(fields.begin() + dateIndex);
    Q_ASSERT(dateIndex < fields.size());
    fields.erase(fields.begin() + dateIndex);

    bool haveRecordNumber = false;

    switch (dateIndex) {
    case 4:
        /* Normal record, with no additional */
        haveRecordNumber = false;
        if (haveCellOzone || haveAnalogInputs)
            haveEmittedRealtimeMeta = false;
        haveCellOzone = false;
        haveAnalogInputs = false;
        break;

    case 7:
        /* Normal record with analog */
        haveRecordNumber = false;
        if (haveCellOzone || !haveAnalogInputs)
            haveEmittedRealtimeMeta = false;
        haveCellOzone = false;
        haveAnalogInputs = true;
        break;

    case 5:
        /* Logging record with no analog */
        haveRecordNumber = true;
        if (haveCellOzone || haveAnalogInputs)
            haveEmittedRealtimeMeta = false;
        haveCellOzone = false;
        haveAnalogInputs = false;
        break;

    case 8:
        /* Logging record with analog */
        haveRecordNumber = true;
        if (haveCellOzone || !haveAnalogInputs)
            haveEmittedRealtimeMeta = false;
        haveCellOzone = false;
        haveAnalogInputs = true;
        break;

    case 6:
        /* Cell ozone record with no analog */
        haveRecordNumber = false;
        if (!haveCellOzone || haveAnalogInputs)
            haveEmittedRealtimeMeta = false;
        haveCellOzone = true;
        haveAnalogInputs = false;
        break;
    case 9:
        /* Cell ozone record with analog */
        haveRecordNumber = false;
        if (!haveCellOzone || !haveAnalogInputs)
            haveEmittedRealtimeMeta = false;
        haveCellOzone = true;
        haveAnalogInputs = true;
        break;

    default:
        return 2;
    }

    if (haveRecordNumber) {
        if (fields.empty()) return 3;
        fields.pop_front();
    }

    Variant::Root ZX1;
    Variant::Root ZX2;
    if (haveCellOzone) {
        if (fields.empty()) return 4;
        field = fields.front().string_trimmed();
        fields.pop_front();
        ZX1.write().setReal(field.parse_real(&ok));
        if (!ok) return 5;
        if (!FP::defined(ZX1.read().toReal())) return 6;
        remap("ZX1", ZX1);

        if (fields.empty()) return 7;
        field = fields.front().string_trimmed();
        fields.pop_front();
        ZX2.write().setReal(field.parse_real(&ok));
        if (!ok) return 8;
        if (!FP::defined(ZX2.read().toReal())) return 9;
        remap("ZX2", ZX2);
    }

    if (fields.empty()) return 10;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root X(field.parse_real(&ok));
    if (!ok) return 11;
    if (!FP::defined(X.read().toReal())) return 12;
    remap("X", X);

    if (fields.empty()) return 13;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T;
    {
        double v = field.parse_real(&ok);
        if (!ok) return 14;
        if (!FP::defined(v)) return 15;
        T.write().setDouble(convertTemperature(v));
    }
    remap("T", T);

    /* There doesn't seem to be a way to query units on this (mbar or torr) and they overlap
     * valid ranges, so just assume mbar */
    if (fields.empty()) return 16;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root P(field.parse_real(&ok));
    if (!ok) return 17;
    if (!FP::defined(P.read().toReal())) return 18;
    remap("P", P);

    if (fields.empty()) return 19;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Q;
    {
        double v = field.parse_real(&ok);
        if (!ok) return 20;
        if (!FP::defined(v)) return 21;
        Q.write().setDouble(convertFlow(v));
    }
    remap("Q", Q);

    Variant::Root V1;
    Variant::Root V2;
    Variant::Root V3;
    if (haveAnalogInputs) {
        if (fields.empty()) return 22;
        field = fields.front().string_trimmed();
        fields.pop_front();
        V1.write().setReal(field.parse_real(&ok));
        if (!ok) return 23;
        if (!FP::defined(V1.read().toReal())) return 24;
        remap("V1", V1);

        if (fields.empty()) return 25;
        field = fields.front().string_trimmed();
        fields.pop_front();
        V2.write().setReal(field.parse_real(&ok));
        if (!ok) return 26;
        if (!FP::defined(V2.read().toReal())) return 27;
        remap("V2", V2);

        if (fields.empty()) return 28;
        field = fields.front().string_trimmed();
        fields.pop_front();
        V3.write().setReal(field.parse_real(&ok));
        if (!ok) return 29;
        if (!FP::defined(V3.read().toReal())) return 30;
        remap("V3", V3);
    }

    if (config.first().strictMode) {
        if (!fields.empty())
            return 31;
    }

    if (!FP::defined(frameTime))
        return -1;
    if (FP::defined(lastRecordTime) && frameTime < lastRecordTime)
        return -1;
    double startTime = lastRecordTime;
    double endTime = frameTime;
    lastRecordTime = frameTime;
    if (!haveEmittedLogMeta && FP::defined(startTime) && loggingEgress != NULL) {
        haveEmittedLogMeta = true;
        loggingEgress->incomingData(buildLogMeta(startTime));
    }

    if (!haveEmittedRealtimeMeta && FP::defined(frameTime) && realtimeEgress != NULL) {
        haveEmittedRealtimeMeta = true;
        SequenceValue::Transfer meta(buildLogMeta(frameTime));
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }

    logValue(startTime, endTime, "F1", Variant::Root(Variant::Type::Flags));
    logValue(startTime, endTime, "X", std::move(X));
    logValue(startTime, endTime, "T", std::move(T));
    logValue(startTime, endTime, "P", std::move(P));
    logValue(startTime, endTime, "Q", std::move(Q));

    if (haveCellOzone) {
        realtimeValue(endTime, "ZX1", std::move(ZX1));
        realtimeValue(endTime, "ZX2", std::move(ZX2));
    }

    if (haveAnalogInputs) {
        realtimeValue(endTime, "V1", std::move(V1));
        realtimeValue(endTime, "V2", std::move(V2));
        realtimeValue(endTime, "V3", std::move(V3));
    }

    return 0;
}

void Acquire2BOzone205::configAdvance(double frameTime)
{
    Q_ASSERT(!config.isEmpty());
    int oldSize = config.size();
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.isEmpty());

    if (oldSize == config.size())
        return;
    configurationChanged();
}

void Acquire2BOzone205::configurationChanged()
{
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
}

void Acquire2BOzone205::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (FP::defined(frameTime))
        configAdvance(frameTime);

    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 2.0;

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            if (++autoprobeValidRecords > 5) {
                qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

                responseState = RESP_PASSIVE_RUN;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                if (FP::defined(frameTime))
                    timeoutAt(frameTime + unpolledResponseTime * 2.0);

                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("PassiveAutoprobeWait");
                event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);
            }
        } else if (code > 0) {
            autoprobeValidRecords = 0;
            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;
    }
    case RESP_PASSIVE_WAIT: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);

            responseState = RESP_PASSIVE_RUN;
            ++autoprobeValidRecords;
            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveWait");
            event(frameTime, QObject::tr("Passive communications established."), false, info);
        } else if (code > 0) {
            autoprobeValidRecords = 0;
            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;
    }

    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

            if (FP::defined(frameTime))
                timeoutAt(frameTime + unpolledResponseTime * 2.0);
            responseState = RESP_UNPOLLED_RUN;
            ++autoprobeValidRecords;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            event(frameTime, QObject::tr("Communications established."), false, info);
        } else if (code > 0) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << " rejected with code" << code;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            autoprobeValidRecords = 0;

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            discardData(frameTime + 30.0);

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Communications start rejected the first line (code %1).").arg(code),
                  true, info);

            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;
    }

    case RESP_PASSIVE_RUN:
    case RESP_UNPOLLED_RUN: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            if (FP::defined(frameTime))
                timeoutAt(frameTime + unpolledResponseTime * 2.0);
            ++autoprobeValidRecords;
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            Variant::Write info = Variant::Write::empty();

            if (responseState == RESP_PASSIVE_RUN) {
                timeoutAt(FP::undefined());
                responseState = RESP_PASSIVE_WAIT;
                info.hash("ResponseState").setString("PassiveRun");
            } else {
                if (controlStream != NULL) {
                    controlStream->writeControl("m\r\n");
                }
                responseState = RESP_INTERACTIVE_START_MENUFIRST;
                if (FP::defined(frameTime)) {
                    discardData(frameTime + 1.0);
                    timeoutAt(frameTime + 30.0);
                }
                info.hash("ResponseState").setString("Run");
            }
            generalStatusUpdated();
            autoprobeValidRecords = 0;

            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;
    }

    case RESP_INTERACTIVE_START_READHEADER: {
        if (!frame.string_start("Ozone"))
            break;

        if (controlStream != NULL) {
            controlStream->writeControl("x\r\n");
        }
        responseState = RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID;
        discardData(frameTime + unpolledResponseTime * 1.5, 1);
        timeoutAt(frameTime + unpolledResponseTime * 4.0 + 1.0);
        break;
    }

    default:
        break;
    }
}

void Acquire2BOzone205::incomingInstrumentTimeout(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configAdvance(frameTime);

    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 6.0;

    switch (responseState) {
    case RESP_UNPOLLED_RUN:
        qCDebug(log) << "Timeout in unpolled state" << responseState << "at"
                     << Logging::time(frameTime);

        if (controlStream != NULL) {
            controlStream->writeControl("m\r\n");
        }
        responseState = RESP_INTERACTIVE_START_MENUFIRST;
        discardData(frameTime + 1.0);
        timeoutAt(frameTime + 30.0);
        generalStatusUpdated();

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("UnpolledRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;

    case RESP_PASSIVE_RUN:
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        responseState = RESP_PASSIVE_WAIT;
        generalStatusUpdated();

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;

    case RESP_INTERACTIVE_START_MENUFIRST:
    case RESP_INTERACTIVE_START_MENUSECOND:
    case RESP_INTERACTIVE_START_MENUTHIRD:
    case RESP_INTERACTIVE_START_SETDATE_CLOCK:
    case RESP_INTERACTIVE_START_SETDATE_SELECT:
    case RESP_INTERACTIVE_START_SETDATE_DATE:
    case RESP_INTERACTIVE_START_SETTIME_CLOCK:
    case RESP_INTERACTIVE_START_SETTIME_SELECT:
    case RESP_INTERACTIVE_START_SETTIME_TIME:
    case RESP_INTERACTIVE_START_SETAVERAGING_MENU:
    case RESP_INTERACTIVE_START_SETAVERAGING_SET:
    case RESP_INTERACTIVE_START_SETRAW:
    case RESP_INTERACTIVE_START_READHEADER:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
    case RESP_INTERACTIVE_RESTART_WAIT:
        qCDebug(log) << "Timeout in interactive start state" << responseState << "at"
                     << Logging::time(frameTime);

        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;

    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        if (controlStream != NULL) {
            controlStream->writeControl("m\r\n");
        }
        responseState = RESP_INTERACTIVE_START_MENUFIRST;
        discardData(frameTime + 1.0);
        timeoutAt(frameTime + 30.0);
        break;

    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */

    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;

    default:
        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;
    }
}

void Acquire2BOzone205::discardDataCompleted(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configAdvance(frameTime);

    Q_ASSERT(!config.isEmpty());
    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 2.0;

    switch (responseState) {
    case RESP_INTERACTIVE_START_MENUFIRST:
        if (controlStream != NULL) {
            controlStream->writeControl("m\r\n");
        }
        responseState = RESP_INTERACTIVE_START_MENUSECOND;
        discardData(frameTime + 1.0);
        break;

    case RESP_INTERACTIVE_START_MENUSECOND:
        if (controlStream != NULL) {
            controlStream->writeControl("m\r\n");
        }
        responseState = RESP_INTERACTIVE_START_MENUTHIRD;
        discardData(frameTime + 1.0);
        break;

    case RESP_INTERACTIVE_START_MENUTHIRD:
        instrumentSetTime = Time::toDateTime(frameTime + 0.5);
        if (controlStream != NULL) {
            controlStream->writeControl("c");
        }
        responseState = RESP_INTERACTIVE_START_SETDATE_CLOCK;
        discardData(frameTime + 0.5);
        break;

    case RESP_INTERACTIVE_START_SETDATE_CLOCK:
        instrumentSetTime = Time::toDateTime(frameTime + 0.5);
        if (controlStream != NULL) {
            controlStream->writeControl("d");
        }
        responseState = RESP_INTERACTIVE_START_SETDATE_SELECT;
        discardData(frameTime + 0.5);
        break;

    case RESP_INTERACTIVE_START_SETDATE_SELECT:
        instrumentSetTime = Time::toDateTime(frameTime + 0.5);
        if (controlStream != NULL) {
            QByteArray command;
            command.append(
                    QByteArray::number(instrumentSetTime.date().day()).rightJustified(2, '0'));
            command.append(
                    QByteArray::number(instrumentSetTime.date().month()).rightJustified(2, '0'));
            command.append(
                    QByteArray::number(instrumentSetTime.date().year() % 100).rightJustified(2,
                                                                                             '0'));
            command.append("\r\n");
            controlStream->writeControl(command);
        }
        responseState = RESP_INTERACTIVE_START_SETDATE_DATE;
        discardData(frameTime + 1.0);
        break;

    case RESP_INTERACTIVE_START_SETDATE_DATE:
        instrumentSetTime = Time::toDateTime(frameTime + 0.5);
        if (controlStream != NULL) {
            controlStream->writeControl("c");
        }
        responseState = RESP_INTERACTIVE_START_SETTIME_CLOCK;
        discardData(frameTime + 0.5);
        break;

    case RESP_INTERACTIVE_START_SETTIME_CLOCK:
        if (controlStream != NULL) {
            controlStream->writeControl("t");
        }
        responseState = RESP_INTERACTIVE_START_SETTIME_SELECT;
        discardData(frameTime + 0.5);
        break;

    case RESP_INTERACTIVE_START_SETTIME_SELECT:
        if (controlStream != NULL) {
            QByteArray command;
            command.append(
                    QByteArray::number(instrumentSetTime.time().hour()).rightJustified(2, '0'));
            command.append(
                    QByteArray::number(instrumentSetTime.time().minute()).rightJustified(2, '0'));
            command.append(
                    QByteArray::number(instrumentSetTime.time().second()).rightJustified(2, '0'));
            command.append("\r\n");
            controlStream->writeControl(command);
        }
        responseState = RESP_INTERACTIVE_START_SETTIME_TIME;
        discardData(frameTime + 1.0);
        break;

    case RESP_INTERACTIVE_START_SETTIME_TIME:
        if (controlStream != NULL) {
            controlStream->writeControl("a");
        }
        responseState = RESP_INTERACTIVE_START_SETAVERAGING_MENU;
        discardData(frameTime + 0.5);
        break;

    case RESP_INTERACTIVE_START_SETAVERAGING_MENU:
        if (controlStream != NULL) {
            QByteArray command;
            if (unpolledResponseTime < 10.0)
                command.append('0');
            else if (unpolledResponseTime < 60.0)
                command.append('1');
            else if (unpolledResponseTime < 300.0)
                command.append('2');
            else if (unpolledResponseTime < 3600.0)
                command.append('3');
            else
                command.append('4');
            command.append("\r\n");
            controlStream->writeControl(command);
        }
        responseState = RESP_INTERACTIVE_START_SETAVERAGING_SET;
        discardData(frameTime + 1.0);
        break;

    case RESP_INTERACTIVE_START_SETAVERAGING_SET:
        if (controlStream != NULL) {
            controlStream->writeControl("r\r\n");
        }
        responseState = RESP_INTERACTIVE_START_SETRAW;
        discardData(frameTime + 1.0);
        break;

    case RESP_INTERACTIVE_START_SETRAW:
        if (controlStream != NULL) {
            controlStream->writeControl("h\r\n");
        }
        responseState = RESP_INTERACTIVE_START_READHEADER;
        timeoutAt(frameTime + 5.0);
        break;

    case RESP_INTERACTIVE_START_READHEADER:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
        break;

    case RESP_INTERACTIVE_RESTART_WAIT:
        if (controlStream != NULL) {
            controlStream->writeControl("m\r\n");
        }
        responseState = RESP_INTERACTIVE_START_MENUFIRST;
        discardData(frameTime + 1.0);
        timeoutAt(frameTime + 30.0);
        break;

    default:
        break;
    }
}


void Acquire2BOzone205::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_UNUSED(frame);
    Q_UNUSED(frameTime);
}

Variant::Root Acquire2BOzone205::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus Acquire2BOzone205::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_UNPOLLED_RUN:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

AcquisitionInterface::AutoprobeStatus Acquire2BOzone205::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void Acquire2BOzone205::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    Q_ASSERT(!config.isEmpty());
    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 6.0;

    switch (responseState) {
    case RESP_UNPOLLED_RUN:
        /* Already running/started, do nothing */
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << responseState << "at" << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case RESP_INTERACTIVE_START_MENUFIRST:
    case RESP_INTERACTIVE_START_MENUSECOND:
    case RESP_INTERACTIVE_START_MENUTHIRD:
    case RESP_INTERACTIVE_START_SETDATE_CLOCK:
    case RESP_INTERACTIVE_START_SETDATE_SELECT:
    case RESP_INTERACTIVE_START_SETDATE_DATE:
    case RESP_INTERACTIVE_START_SETTIME_CLOCK:
    case RESP_INTERACTIVE_START_SETTIME_SELECT:
    case RESP_INTERACTIVE_START_SETTIME_TIME:
    case RESP_INTERACTIVE_START_SETAVERAGING_MENU:
    case RESP_INTERACTIVE_START_SETAVERAGING_SET:
    case RESP_INTERACTIVE_START_SETRAW:
    case RESP_INTERACTIVE_START_READHEADER:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        if (controlStream != NULL) {
            controlStream->writeControl("m\r\n");
        }
        responseState = RESP_INTERACTIVE_START_MENUFIRST;
        timeoutAt(time + 30.0);
        discardData(time + 1.0);

        generalStatusUpdated();
        break;
    }
}

void Acquire2BOzone205::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 6.0;

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
    autoprobeValidRecords = 0;
    discardData(time + 0.25, 1);
    timeoutAt(time + unpolledResponseTime * 7.0 + 3.0);
    generalStatusUpdated();
}

void Acquire2BOzone205::autoprobePromote(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    Q_ASSERT(!config.isEmpty());
    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 6.0;

    switch (responseState) {
    case RESP_UNPOLLED_RUN:
        qCDebug(log) << "Promoted from interactive run to interactive acquisition at"
                     << Logging::time(time);
        /* Reset timeout in case we didn't have a control stream */
        timeoutAt(time + unpolledResponseTime + 2.0);
        break;

    case RESP_INTERACTIVE_START_MENUFIRST:
    case RESP_INTERACTIVE_START_MENUSECOND:
    case RESP_INTERACTIVE_START_MENUTHIRD:
    case RESP_INTERACTIVE_START_SETDATE_CLOCK:
    case RESP_INTERACTIVE_START_SETDATE_SELECT:
    case RESP_INTERACTIVE_START_SETDATE_DATE:
    case RESP_INTERACTIVE_START_SETTIME_CLOCK:
    case RESP_INTERACTIVE_START_SETTIME_SELECT:
    case RESP_INTERACTIVE_START_SETTIME_TIME:
    case RESP_INTERACTIVE_START_SETAVERAGING_MENU:
    case RESP_INTERACTIVE_START_SETAVERAGING_SET:
    case RESP_INTERACTIVE_START_SETRAW:
    case RESP_INTERACTIVE_START_READHEADER:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);
        /* Reset timeout in case we didn't have a control stream */
        timeoutAt(time + unpolledResponseTime * 2.0 + 30.0);
        break;

    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        if (controlStream != NULL) {
            controlStream->writeControl("m\r\n");
        }
        responseState = RESP_INTERACTIVE_START_MENUFIRST;
        timeoutAt(time + 30.0);
        discardData(time + 1.0);

        generalStatusUpdated();
        break;
    }
}

void Acquire2BOzone205::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    Q_ASSERT(!config.isEmpty());
    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 6.0;

    /* Reset timeout in case we didn't have a control stream */
    timeoutAt(time + unpolledResponseTime + 2.0);

    switch (responseState) {
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);

        break;

    case RESP_UNPOLLED_RUN:
        responseState = RESP_PASSIVE_RUN;

        qCDebug(log) << "Promoted from unpolled interactive state to passive acquisition at"
                     << Logging::time(time);
        break;

    default:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_PASSIVE_RUN;

        discardData(FP::undefined());
        generalStatusUpdated();
        break;
    }
}

AcquisitionInterface::AutomaticDefaults Acquire2BOzone205::getDefaults()
{
    AutomaticDefaults result;
    result.name = "G$1$2";
    result.setSerialN81(4800);
    return result;
}


ComponentOptions Acquire2BOzone205Component::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options, true);

    return options;
}

ComponentOptions Acquire2BOzone205Component::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> Acquire2BOzone205Component::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples());
    examples.append(getPassiveExamples());
    return examples;
}

bool Acquire2BOzone205Component::requiresInputDevice()
{ return true; }

ExternalConverter *Acquire2BOzone205Component::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options, true);
}

QList<ComponentExample> Acquire2BOzone205Component::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data.")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> Acquire2BOzone205Component::createAcquisitionPassive(const ComponentOptions &options,
                                                                                   const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new Acquire2BOzone205(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> Acquire2BOzone205Component::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                   const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new Acquire2BOzone205(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> Acquire2BOzone205Component::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                     const std::string &loggingContext)
{
    std::unique_ptr<Acquire2BOzone205> i(new Acquire2BOzone205(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<
        AcquisitionInterface> Acquire2BOzone205Component::createAcquisitionInteractive(const ValueSegment::Transfer &config,
                                                                                       const std::string &loggingContext)
{
    std::unique_ptr<Acquire2BOzone205> i(new Acquire2BOzone205(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}