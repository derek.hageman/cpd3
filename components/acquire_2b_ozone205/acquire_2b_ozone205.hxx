/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIRE2BOZONE205_H
#define ACQUIRE2BOZONE205_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QDateTime>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"

class Acquire2BOzone205 : public CPD3::Acquisition::FramedInstrument {
    double lastRecordTime;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum ResponseState {
        /* Passive autoprobe initializing, ignoring the first timeout */
        RESP_AUTOPROBE_PASSIVE_INITIALIZE,
        /* Waiting for a valid record to confirm passive mode autoprobe */
        RESP_AUTOPROBE_PASSIVE_WAIT,

        /* Waiting for a valid record to start passive acquisition */
        RESP_PASSIVE_WAIT,

        /* Acquiring data in passive mode */
        RESP_PASSIVE_RUN,
        /* Acquiring data in interactive mode, but only reading unpolled data */
        RESP_UNPOLLED_RUN,

        /* Waiting for the completion of the initial "m" command */
        RESP_INTERACTIVE_START_MENUFIRST,
        /* Waiting for the completion of the second "m" command */
        RESP_INTERACTIVE_START_MENUSECOND,
        /* Waiting for the completion of the third "m" command */
        RESP_INTERACTIVE_START_MENUTHIRD,

        /* Waiting for the completion of the clock command to set the date */
        RESP_INTERACTIVE_START_SETDATE_CLOCK,
        /* Waiting for the completion of the date select subcommand to set the date */
        RESP_INTERACTIVE_START_SETDATE_SELECT,
        /* Waiting for the completion of the date subcommand to set the date */
        RESP_INTERACTIVE_START_SETDATE_DATE,
        /* Waiting for the completion of the clock command to set the date */
        RESP_INTERACTIVE_START_SETTIME_CLOCK,
        /* Waiting for the completion of the date select subcommand to set the date */
        RESP_INTERACTIVE_START_SETTIME_SELECT,
        /* Waiting for the completion of the date subcommand to set the date */
        RESP_INTERACTIVE_START_SETTIME_TIME,
        /* Waiting for the completion of the set averaging command to print its menu */
        RESP_INTERACTIVE_START_SETAVERAGING_MENU,
        /* Waiting for the completion of the set averaging command */
        RESP_INTERACTIVE_START_SETAVERAGING_SET,
        /* Waiting for the completion of the set raw command */
        RESP_INTERACTIVE_START_SETRAW,

        /* Waiting for the completion of the header read command */
        RESP_INTERACTIVE_START_READHEADER,

        /* Waiting for the first valid record */
        RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID,

        /* Waiting before attempting a communications restart */
        RESP_INTERACTIVE_RESTART_WAIT,
        /* Initial state for interactive start */
        RESP_INTERACTIVE_INITIALIZE,
    };
    ResponseState responseState;
    int autoprobeValidRecords;

    friend class Configuration;

    class Configuration {
        double start;
        double end;
    public:
        bool strictMode;
        double reportInterval;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under,
                      const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    void setDefaultInvalid();

    CPD3::Data::Variant::Root instrumentMeta;

    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;
    bool haveCellOzone;
    bool haveAnalogInputs;

    QDateTime instrumentSetTime;

    void logValue(double startTime,
                  double endTime,
                  CPD3::Data::SequenceName::Component name,
                  CPD3::Data::Variant::Root &&value);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root &&value);

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    void configAdvance(double frameTime);

    void configurationChanged();

    int processDateTime(const CPD3::Util::ByteView &date,
                        const CPD3::Util::ByteView &time,
                        double &frameTime);

    int processRecord(const CPD3::Util::ByteView &line, double &frameTime);

public:
    Acquire2BOzone205(const CPD3::Data::ValueSegment::Transfer &config,
                      const std::string &loggingContext);

    Acquire2BOzone205(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~Acquire2BOzone205();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus() override;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus() override;

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:

    void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingInstrumentTimeout(double time) override;

    void discardDataCompleted(double time) override;
};

class Acquire2BOzone205Component
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT

    Q_INTERFACES(CPD3::Data::ExternalConverterComponent CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_2b_ozone205"
                              FILE
                              "acquire_2b_ozone205.json")

    CPD3::ComponentOptions getBaseOptions();

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
