/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef SYNCAUTHORIZE_H
#define SYNCAUTHORIZE_H

#include "core/first.hxx"

#include <mutex>
#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QList>
#include <QStringList>
#include <QSslCertificate>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "core/textsubstitution.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/stream.hxx"

class SyncAuthorize : public CPD3::CPD3Action {
Q_OBJECT

    std::mutex mutex;
    bool terminated;

    std::vector<CPD3::Data::SequenceName::Component> stations;
    QString templateName;
    bool deauthorizeOld;

    QString digest;
    QString fileArgument;
    QString certificateArgument;

    struct Operation {
        QString digest;
        QSslCertificate certificate;
        QString email;
        QString name;
        QString note;
        QString profile;

        inline Operation(const QString &digest,
                         const QSslCertificate &certificate = QSslCertificate(),
                         const QString &email = {},
                         const QString &name = {},
                         const QString &note = {},
                         const QString &profile = {}) : digest(digest),
                                                         certificate(certificate),
                                                         email(email),
                                                         name(name),
                                                         note(note),
                                                         profile(profile)
        { }
    };

    QList<Operation> pending;

    CPD3::TextSubstitutionStack substitutions;

    CPD3::Threading::Signal<> terminateRequested;

    bool testTerminated();

    void addAuthorization();

    void sendEmails();

    void addCertificate(const QString &fileName);

    void afterAddActions();

    bool invokeCommand(const QString &program, const QStringList &arguments);

    bool executeAction(CPD3::CPD3Action *action);

    bool handleAction(const CPD3::Data::Variant::Read &action,
                      const CPD3::Data::SequenceName::Component &station);

    bool executeActions(const CPD3::Data::Variant::Read &config,
                        const CPD3::Data::SequenceName::Component &station = {});

public:
    SyncAuthorize(const CPD3::ComponentOptions &options,
                  const std::vector<CPD3::Data::SequenceName::Component> &setStations = {});

    virtual ~SyncAuthorize();

    virtual void signalTerminate();

protected:
    virtual void run();
};

class SyncAuthorizeComponent : public QObject, virtual public CPD3::ActionComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::ActionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.sync_authorize"
                              FILE
                              "sync_authorize.json")

public:
    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual int actionAllowStations();

    virtual int actionRequireStations();

    QString promptActionContinue(const CPD3::ComponentOptions &options = {},
                                 const std::vector<std::string> &stations = {}) override;

    CPD3::CPD3Action *createAction(const CPD3::ComponentOptions &options = {},
                                   const std::vector<std::string> &stations = {}) override;
};

#endif
