/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QDir>
#include <QCryptographicHash>
#include <QHostInfo>
#include <QFile>
#include <QSslCertificate>
#include <QTemporaryFile>

#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "core/actioncomponent.hxx"
#include "core/number.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"
#include "algorithms/cryptography.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Algorithms;


static const char *cert1Data = "-----BEGIN CERTIFICATE-----\n"
        "MIICbTCCAdYCCQDZ6mj3BI+kCTANBgkqhkiG9w0BAQUFADB7MQswCQYDVQQGEwJV\n"
        "UzERMA8GA1UECAwIQ29sb3JhZG8xEDAOBgNVBAcMB0JvdWxkZXIxFjAUBgNVBAoM\n"
        "DU5PQUEvR01EL0VTUkwxFzAVBgNVBAsMDkFlcm9zb2xzIEdyb3VwMRYwFAYDVQQD\n"
        "DA1EZXJlayBIYWdlbWFuMB4XDTEyMDcyNzE1MzkwNloXDTM5MTIxMjE1MzkwNlow\n"
        "ezELMAkGA1UEBhMCVVMxETAPBgNVBAgMCENvbG9yYWRvMRAwDgYDVQQHDAdCb3Vs\n"
        "ZGVyMRYwFAYDVQQKDA1OT0FBL0dNRC9FU1JMMRcwFQYDVQQLDA5BZXJvc29scyBH\n"
        "cm91cDEWMBQGA1UEAwwNRGVyZWsgSGFnZW1hbjCBnzANBgkqhkiG9w0BAQEFAAOB\n"
        "jQAwgYkCgYEAn3nIKzjLLEY8KG6+hUvrkjDkl9VU8HFKnp8wA2RsMta/UvKG4ZIY\n"
        "bNVsKSiAGm1G0i6Mtftw8quyihoxuy/hElH/XJChU5lersdrhalocHXoipNx8BeW\n"
        "T8sXuJOrh+IoDKqVI4I6Sz42qAlqBU7bNm40TE8sBPAOefyTGavC3e0CAwEAATAN\n"
        "BgkqhkiG9w0BAQUFAAOBgQBU2hljZZpJJ+zK6eePUpdELQGPGHJEbzfReeSBhRau\n"
        "iDj4jEa7IE3yrMv8NqC3Q+6lEVuJg0NInvDYsNYLPjMSPS+jKldrmrJvg+TFrp1w\n"
        "Qr3JoznNxwcwuDyrKjOVux6jo2aype3Zayo5VUizJX9Zo+xLe2MBVR1D0MXmoq8L\n"
        "zA==\n"
        "-----END CERTIFICATE-----";

class TestComponent : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;
    QString certAuth;

    ActionComponent *component;

    static bool prioritySort(const SequenceValue &a, const SequenceValue &b)
    {
        return a.getPriority() < b.getPriority();
    }

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component = qobject_cast<ActionComponent *>(ComponentLoader::create("sync_authorize"));
        QVERIFY(component);

        QVERIFY(databaseFile.open());
        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());

        certAuth = QString::fromLatin1(
                Cryptography::sha512(QSslCertificate(QByteArray(cert1Data))).leftJustified(64, 0,
                                                                                           true)
                                                                            .toHex()
                                                                            .toLower());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("template")));
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("digest")));;
        QVERIFY(qobject_cast<ComponentOptionFile *>(options.get("certificate")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("deauthorize")));
    }

    void singleFile()
    {
        Variant::Root config;
        config["/Processing/Authorize/DisableEmail"].setBool(true);

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "configuration", "synchronize"}, config, FP::undefined(),
                              FP::undefined(), -2),
                SequenceValue({"sfa", "configuration", "synchronize"}, config, FP::undefined(),
                              FP::undefined(), -1)});

        QTemporaryFile cert;
        QVERIFY(cert.open());
        cert.write(cert1Data);
        cert.close();

        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionFile *>(options.get("certificate"))->set(cert.fileName());
        CPD3Action *action = component->createAction(options, {"sfa"});
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        auto outputConfig = Archive::Access(databaseFile).readSynchronous(
                Archive::Selection(FP::undefined(), FP::undefined(), {"sfa"}, {"configuration"},
                                   {"synchronize"}));
        QCOMPARE((int) outputConfig.size(), 2);

        std::sort(outputConfig.begin(), outputConfig.end(), prioritySort);
        QVERIFY(outputConfig.at(0).getValue()["/Processing/Authorize/DisableEmail"].exists());
        QVERIFY(outputConfig.at(0).getValue()["/Authorization/" + certAuth].exists());
        QVERIFY(outputConfig.at(1).getValue()["/Processing/Authorize/DisableEmail"].exists());
        QVERIFY(outputConfig.at(1).getValue()["/Authorization/" +
                certAuth + "/Encryption"].exists());
    }

    void specificDigest()
    {
        Variant::Root config;
        config["/Processing/Authorize/DisableEmail"].setBool(true);

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "configuration", "synchronize"}, config, FP::undefined(),
                              FP::undefined(), 1)});

        QTemporaryFile cert;
        QVERIFY(cert.open());
        cert.write(cert1Data);
        cert.close();

        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionSingleString *>(options.get("digest"))->set(
                "7048b94963f3d6bf1dbc22b88a1a1d4a1ebeefd1");
        CPD3Action *action = component->createAction(options, {"sfa"});
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        auto outputConfig = Archive::Access(databaseFile).readSynchronous(
                Archive::Selection(FP::undefined(), FP::undefined(), {"sfa"}, {"configuration"},
                                   {"synchronize"}));
        QCOMPARE((int) outputConfig.size(), 3);
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
