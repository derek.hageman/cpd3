/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QStringList>
#include <QDir>
#include <QSslCertificate>
#include <QSettings>
#include <QLoggingCategory>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "core/range.hxx"
#include "core/environment.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/segment.hxx"
#include "datacore/valueoptionparse.hxx"
#include "transfer/smtp.hxx"
#include "transfer/email.hxx"
#include "algorithms/cryptography.hxx"
#include "io/process.hxx"

#include "sync_authorize.hxx"


Q_LOGGING_CATEGORY(log_component_sync_authorize, "cpd3.component.sync.authorize", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Transfer;
using namespace CPD3::Algorithms;


SyncAuthorize::SyncAuthorize(const ComponentOptions &options,
                             const std::vector<SequenceName::Component> &setStations)
        : mutex(),
          terminated(false),
          stations(setStations),
          templateName(),
          deauthorizeOld(false),
          digest(),
          fileArgument(),
          certificateArgument(),
          pending()
{
    if (options.isSet("digest")) {
        digest =
                qobject_cast<ComponentOptionSingleString *>(options.get("digest"))->get().trimmed();
    }
    if (options.isSet("template")) {
        templateName = qobject_cast<ComponentOptionSingleString *>(options.get("template"))->get();
    }
    if (options.isSet("certificate")) {
        certificateArgument =
                qobject_cast<ComponentOptionFile *>(options.get("certificate"))->get();
    }
    if (options.isSet("deauthorize")) {
        deauthorizeOld = qobject_cast<ComponentOptionBoolean *>(options.get("deauthorize"))->get();
    }
}

SyncAuthorize::~SyncAuthorize()
{
}

void SyncAuthorize::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    terminateRequested();
}

bool SyncAuthorize::testTerminated()
{
    std::lock_guard<std::mutex> lock(mutex);
    return terminated;
}

void SyncAuthorize::addAuthorization()
{
    feedback.emitStage(
            tr("Change %1 authorization").arg(Util::to_qstringlist(stations).join(",").toUpper()),
            tr("The authorization settings for %1 are being altered.").arg(
                    Util::to_qstringlist(stations).join(",").toUpper()), false);

    Archive::Access access;
    for (;;) {
        Archive::Access::WriteLock lock(access);

        std::unordered_map<std::string,std::string> templateLookup;
        {
            {
                double tnow = Time::time();
                auto confList = SequenceSegment::Stream::read(
                        Archive::Selection(tnow - 1.0, tnow + 1.0, stations, {"configuration"},
                                           {"synchronize"}), &access);
                if (Range::intersectShift(confList, tnow)) {
                    auto &active = confList.front();
                    for (const auto &station : stations) {
                        for (auto profile : active[SequenceName(station, "configuration", "synchronize")]["Processing/Authorize/Profiles"].toHash()) {
                            const auto &target = profile.second.hash("Template").toString();
                            if (target.empty())
                                continue;
                            templateLookup.emplace(profile.first, target);
                        }
                    }
                }
            }
        }

        Variant::Root authBase;
        Variant::Root authSpecific;
        QSet<QString> allDigests;
        for (const auto &op : pending) {
            allDigests.insert(op.digest);

            QString selectedTemplate = "Standard";
            {
                auto check = templateLookup.find(op.profile.toStdString());
                if (check != templateLookup.end()) {
                    selectedTemplate = QString::fromStdString(check->second);
                }
            }
            if (!templateName.isEmpty()) {
                selectedTemplate = templateName;
            }

            authBase.write()
                    .hash("Authorization")
                    .hash(op.digest)
                    .setOverlay(QString("/AuthorizationTemplates/%1").arg(selectedTemplate));
            if (!op.certificate.isNull()) {
                authSpecific.write()
                            .hash("Authorization")
                            .hash(op.digest)
                            .hash("Encryption")
                            .setString(op.certificate.toPem().toStdString());
            }
        }
        QStringList digests(allDigests.values());
        std::sort(digests.begin(), digests.end());

        qCDebug(log_component_sync_authorize) << "Adding authorization for" << digests << "to"
                                              << stations;

        SequenceValue::Transfer add;
        SequenceIdentity::Transfer remove;
        if (deauthorizeOld) {
            int totalDeauthorized = 0;

            StreamSink::Iterator data;
            access.readStream(Archive::Selection(FP::undefined(), FP::undefined(), stations,
                                                 {"configuration"},
                                                 {"synchronize"}).withDefaultStation(false)
                                                                 .withMetaArchive(false), &data)
                  ->detach();
            while (data.hasNext()) {
                auto check = data.next();
                if (!check.read().hash("Authorization").exists())
                    continue;

                ++totalDeauthorized;

                /* Will remove everything, so don't add a new one */
                auto oldChildren = check.read().toHash();
                if (oldChildren.size() == 0 ||
                        (oldChildren.size() == 1 && oldChildren.begin().key() == "Authorization")) {
                    remove.emplace_back(std::move(check));
                    continue;
                }

                check.write().hash("Authorization").remove(false);
                add.emplace_back(std::move(check));
            }

            if (totalDeauthorized > 0) {
                qCDebug(log_component_sync_authorize) << "Removing" << totalDeauthorized
                                                      << "old authorization specification(s)";
            }

            for (const auto &station : stations) {
                add.emplace_back(SequenceName(station, "configuration", "synchronize"), authBase,
                                 FP::undefined(), FP::undefined(), -2);
                if (authSpecific.read().exists()) {
                    add.emplace_back(SequenceName(station, "configuration", "synchronize"),
                                     authSpecific, FP::undefined(), FP::undefined(), -1);
                }
            }
        } else {
            SequenceName::ComponentSet addBaseStations(stations.begin(), stations.end());
            SequenceName::ComponentSet addSpecificStations = addBaseStations;

            StreamSink::Iterator data;
            access.readStream(Archive::Selection(FP::undefined(), FP::undefined(), stations,
                                                 {"configuration"},
                                                 {"synchronize"}).withDefaultStation(false)
                                                                 .withMetaArchive(false), &data)
                  ->detach();
            while (data.hasNext()) {
                auto check = data.next();
                if (check.getPriority() == -2) {
                    for (const auto &op : pending) {
                        QString selectedTemplate = "Standard";
                        {
                            auto check = templateLookup.find(op.profile.toStdString());
                            if (check != templateLookup.end()) {
                                selectedTemplate = QString::fromStdString(check->second);
                            }
                        }
                        if (!templateName.isEmpty()) {
                            selectedTemplate = templateName;
                        }

                        check.write()
                             .hash("Authorization")
                             .hash(op.digest)
                             .setOverlay(
                                     QString("/AuthorizationTemplates/%1").arg(selectedTemplate));
                    }
                    addBaseStations.erase(check.getStation());
                    add.emplace_back(std::move(check));
                } else if (check.getPriority() == -1) {
                    for (const auto &op : pending) {
                        check.write()
                             .hash("Authorization")
                             .hash(op.digest)
                             .set(authSpecific.read().hash("Authorization").hash(op.digest));
                    }
                    addSpecificStations.erase(check.getStation());
                    add.emplace_back(std::move(check));
                }
            }
            for (const auto &station : addBaseStations) {
                add.emplace_back(SequenceName(station, "configuration", "synchronize"), authBase,
                                 FP::undefined(), FP::undefined(), -2);
            }
            for (const auto &station : addSpecificStations) {
                add.emplace_back(SequenceName(station, "configuration", "synchronize"),
                                 authSpecific, FP::undefined(), FP::undefined(), -1);
            }
        }

        Variant::Root event;
        double tnow = Time::time();
        event["Text"].setString(
                tr("Added authorization to synchronize digest(s) %1 for %2").arg(
                        digests.join(","),
                        Util::to_qstringlist(stations).join(",").toUpper()));
        event["Information"].hash("By").setString("sync_authorize");
        event["Information"].hash("At").setDouble(tnow);
        event["Information"].hash("Environment").setString(Environment::describe());
        event["Information"].hash("Revision").setString(Environment::revision());
        for (const auto &stn : stations) {
            event["Information"].hash("Stations").applyFlag(stn);
        }
        event["Information"].hash("Deauthorizing").setBool(deauthorizeOld);
        for (const auto &op : pending) {
            auto av = event["Information"].hash("Authorization").toArray().after_back();
            av.hash("Digest").setString(op.digest);
            if (!op.certificate.isNull()) {
                av.hash("Certificate").setBinary(op.certificate.toDer());
            }
            if (!op.email.isEmpty()) {
                av.hash("Email").setString(op.email);
            }
            if (!op.name.isEmpty()) {
                av.hash("Name").setString(op.name);
            }
            if (!op.note.isEmpty()) {
                av.hash("Note").setString(op.note);
            }
            if (!op.profile.isEmpty()) {
                av.hash("Profile").setString(op.profile);
            }
        }
        for (const auto &station : stations) {
            add.push_back(SequenceValue({station, "events", "synchronize"}, event, tnow, tnow));
        }

        access.writeSynchronous(add, remove);

        if (lock.commit())
            break;
    }
}

void SyncAuthorize::sendEmails()
{
    double tnow = Time::time();

    substitutions.clear();
    substitutions.setTime("time", tnow);
    substitutions.setString("stations", Util::to_qstringlist(stations).join(",").toLower());
    if (!stations.empty())
        substitutions.setString("station", QString::fromStdString(stations.front()).toLower());

    QSet<QString> additionalEmails;
    Variant::Read relay = Variant::Read::empty();
    QString text(tr(R"(<html><body>
<h2>Synchronization Granted for ${STATIONS|UPPER} - ${PROFILENAME}</h2>
Granted to: <b><a href="mailto:${EMAIL}">${NAME}</a></b><br>
Note: ${NOTE}<br>
Authorization: ${DIGEST|LOWER}<br>
<br>
<br>
Please note that data may not be available for synchronization until at least one day has passed.
</body></html>)"));
    std::unordered_map<std::string,QString> profileNames;

    {
        SequenceSegment configSegment;
        {
            auto confList = SequenceSegment::Stream::read(
                    Archive::Selection(tnow - 1.0, tnow + 1.0, stations, {"configuration"},
                                       {"synchronize"}));
            if (!Range::intersectShift(confList, tnow)) {
                qCDebug(log_component_sync_authorize) << "No configuration for " << stations;
                return;
            }
            configSegment = std::move(confList.front());
        }

        for (const auto &station : stations) {
            auto config =
                    configSegment.takeValue(SequenceName(station, "configuration", "synchronize"));

            if (config["Processing/Authorize/DisableEmail"].toBool())
                return;

            switch (config["Processing/Authorize/AdditionalEmail"].getType()) {
            default:
                for (const auto &add : config["Processing/Authorize/AdditionalEmail"].toChildren()
                                                                                     .keys()) {
                    additionalEmails.insert(QString::fromStdString(add).toLower());
                }
                break;
            case Variant::Type::String:
                for (const auto &add : config["Processing/Authorize/AdditionalEmail"].toQString()
                                                                                     .split(",")) {
                    additionalEmails.insert(add.toLower());
                }
                break;
            }

            if (config["Processing/Authorize/Relay"].exists())
                relay = config["Processing/Authorize/Relay"];
            if (config["Processing/Authorize/EmailText"].exists())
                text = config["Processing/Authorize/EmailText"].toDisplayString();

            for (const auto &profile :  config["Processing/Authorize/Profiles"].toHash()) {
                auto name = profile.second["Name"].toDisplayString();
                if (name.isEmpty())
                    continue;
                profileNames.emplace(profile.first, name);
            }
        }
    }

    SMTPSend sender(relay);

    for (QList<Operation>::const_iterator op = pending.constBegin(), end = pending.constEnd();
            op != end;
            ++op) {
        TextSubstitutionStack::Context subCtx(substitutions);
        substitutions.setString("digest", op->digest);
        substitutions.setString("name", op->name);
        substitutions.setString("email", op->email);
        substitutions.setString("note", op->note);
        substitutions.setString("profile", op->profile);
        {
            auto name = profileNames.find(op->profile.toStdString());
            if (name != profileNames.end())
                substitutions.setString("profilename", name->second);
        }

        EmailBuilder builder;

        QStringList allRecipients;
        if (!op->email.isEmpty())
            allRecipients.append(op->email);
        allRecipients.append(additionalEmails.values());
        if (allRecipients.isEmpty())
            continue;

        builder.setEscapeMode(EmailBuilder::Escape_HTML);
        builder.setSubject(substitutions.apply(tr("CPD3 - Synchronization Grant - ${STATIONS|UPPER}")));
        builder.setHeader("Reply-To", allRecipients.join(", "));
        if (!op->email.isEmpty()) {
            builder.addTo(op->email);
            for (QSet<QString>::const_iterator cc = additionalEmails.constBegin(),
                    endCC = additionalEmails.constEnd(); cc != endCC; ++cc) {
                builder.addCC(*cc);
            }
        } else {
            for (QList<QString>::const_iterator to = allRecipients.constBegin(),
                    endTo = allRecipients.constEnd(); to != endTo; ++to) {
                builder.addTo(*to);
            }
        }

        builder.setBody(substitutions.apply(text), "text/html");

        builder.queue(&sender);
    }

    feedback.emitStage(tr("Send email"), tr("Sending notification emails."), false);

    Threading::Receiver rx;
    terminateRequested.connect(rx, std::bind(&SMTPSend::signalTerminate, &sender));
    sender.start();
    if (testTerminated())
        sender.signalTerminate();
    if (!sender.wait(60)) {
        qCInfo(log_component_sync_authorize) << "SMTP transmission failed";
        sender.signalTerminate();
        sender.wait();
    }
    if (!sender.anyMessageSent()) {
        feedback.emitFailure();
    }
}

static QString certInfo(const QStringList &input)
{
    if (input.isEmpty())
        return QString();
    return input.first();
}

void SyncAuthorize::addCertificate(const QString &fileName)
{
    QString originName;
    QString originEmail;
    QString requestNote;
    QString requestProfile;

    QSslCertificate cd(Cryptography::getCertificate(Variant::Root(fileName), true));
    QString addDigest(digest);
    if (!cd.isNull()) {
        if (addDigest.isEmpty()) {
            addDigest = Cryptography::sha512(cd).toHex().toLower();
        }
        originName = certInfo(cd.subjectInfo(QSslCertificate::CommonName));
        originEmail = certInfo(cd.subjectInfo(QSslCertificate::EmailAddress));
        requestNote = certInfo(cd.subjectInfo(QSslCertificate::DistinguishedNameQualifier));
        requestProfile = certInfo(cd.subjectInfo(QSslCertificate::OrganizationalUnitName));
    } else {
        qCDebug(log_component_sync_authorize) << "Unable to load certificate" << fileName;
    }

    if (addDigest.isEmpty())
        return;

    pending.append(Operation(addDigest, cd, originEmail, originName, requestNote, requestProfile));
}

bool SyncAuthorize::invokeCommand(const QString &program, const QStringList &arguments)
{
    qCDebug(log_component_sync_authorize) << "Invoking:" << program << arguments;

    auto proc = IO::Process::Spawn(program, arguments).forward().create();
    if (!proc)
        return false;

    IO::Process::Instance::ExitMonitor exitOk(*proc);
    exitOk.connectTerminate(terminateRequested);
    if (testTerminated())
        return false;

    if (!proc->start() || !proc->wait())
        return false;
    return exitOk.success();
}

bool SyncAuthorize::executeAction(CPD3Action *action)
{
    if (action == NULL) {
        qCWarning(log_component_sync_authorize) << "Error creating action";
        return false;
    }

    action->feedback.forward(feedback);
    terminateRequested.connect(action, SLOT(signalTerminate()), Qt::DirectConnection);

    action->start();
    action->wait();
    delete action;
    return true;
}

bool SyncAuthorize::handleAction(const Variant::Read &action,
                                 const SequenceName::Component &station)
{
    if (action.getType() == Variant::Type::String) {
        QStringList arguments
                (substitutions.apply(action.toQString()).split(' ', QString::SkipEmptyParts));
        if (arguments.isEmpty())
            return true;
        QString program(arguments.takeFirst());
        return invokeCommand(program, arguments);
    }

    const auto &type = action["Type"].toString();
    if (Util::equal_insensitive(type, "program")) {
        QString program(substitutions.apply(action["Program"].toQString()));
        QStringList arguments;
        if (action["Arguments"].getType() == Variant::Type::String) {
            arguments = substitutions.apply(action["Arguments"].toQString())
                                     .split(' ', QString::SkipEmptyParts);
        } else {
            for (auto add : action["Arguments"].toArray()) {
                arguments.append(substitutions.apply(add.toQString()));
            }
        }
        if (program.isEmpty() && !arguments.isEmpty())
            program = arguments.takeFirst();
        if (program.isEmpty())
            return true;
        return invokeCommand(program, arguments);
    }

    QString componentName(action["Name"].toQString());
    if (componentName.isEmpty()) {
        qCDebug(log_component_sync_authorize) << "Invalid action:" << action;
        return true;
    }

    QObject *component = ComponentLoader::create(componentName);
    if (component == NULL) {
        qCWarning(log_component_sync_authorize) << "Can't load component" << componentName;
        return false;
    }

    feedback.emitState(componentName);

    Variant::Root options(action["Options"]);
    if (!options["station"].exists()) {
        options["station"].setString(station);
    }

    ActionComponent *actionComponent;
    if ((actionComponent = qobject_cast<ActionComponent *>(component))) {
        std::vector<SequenceName::Component> runStations;
        if (!station.empty() &&
                actionComponent->actionAllowStations() > 0 &&
                actionComponent->actionRequireStations() <= 1) {
            runStations.emplace_back(station);
        } else if (actionComponent->actionRequireStations() > 0) {
            qCWarning(log_component_sync_authorize) << "Unable to provide required stations to"
                                                    << componentName;
            return false;
        }

        ComponentOptions o = actionComponent->getOptions();
        ValueOptionParse::parse(o, options);
        CPD3Action *actionThread = actionComponent->createAction(o, runStations);
        return executeAction(actionThread);
    }

    ActionComponentTime *actionComponentTime;
    if ((actionComponentTime = qobject_cast<ActionComponentTime *>(component))) {
        if (actionComponentTime->actionRequiresTime()) {
            qCWarning(log_component_sync_authorize) << "Refusing to run" << componentName
                                                    << "since it requires times and cannot be provided";
            return false;
        }

        std::vector<SequenceName::Component> runStations;
        if (!runStations.empty() &&
                actionComponentTime->actionAllowStations() > 0 &&
                actionComponentTime->actionRequireStations() <= 1) {
            stations.emplace_back(station);
        } else if (actionComponentTime->actionRequireStations() > 0) {
            qCWarning(log_component_sync_authorize) << "Unable to provide required stations to"
                                                    << componentName;
            return false;
        }

        ComponentOptions o = actionComponentTime->getOptions();
        ValueOptionParse::parse(o, options);
        CPD3Action *actionThread = actionComponentTime->createTimeAction(o, runStations);
        return executeAction(actionThread);
    }

    qCWarning(log_component_sync_authorize) << "Unable to execute component" << componentName
                                            << " because the type is not supported";
    return false;
}

bool SyncAuthorize::executeActions(const Variant::Read &config,
                                   const SequenceName::Component &station)
{
    if (testTerminated())
        return false;
    switch (config.getType()) {
    case Variant::Type::String:
        return handleAction(config, station);

    case Variant::Type::Hash: {
        if (!handleAction(config, station))
            return false;
        break;
    }

    default:
        for (auto child : config.toChildren()) {
            if (testTerminated())
                return false;
            if (!handleAction(child, station))
                return false;
        }
        break;
    }

    return true;
}

void SyncAuthorize::afterAddActions()
{
    substitutions.clear();
    substitutions.setString("stations", Util::to_qstringlist(stations).join(",").toLower());
    if (!stations.empty())
        substitutions.setString("station", QString::fromStdString(stations.front()).toLower());

    SequenceSegment configSegment;
    {
        double tnow = Time::time();
        auto confList = SequenceSegment::Stream::read(
                Archive::Selection(tnow - 1.0, tnow + 1.0, stations, {"configuration"},
                                   {"synchronize"}));
        if (!Range::intersectShift(confList, tnow)) {
            qCDebug(log_component_sync_authorize) << "No configuration for" << stations;
            return;
        }
        configSegment = std::move(confList.front());
    }

    bool haveRunGlobal = false;
    for (const auto &station : stations) {
        TextSubstitutionStack::Context subctx(substitutions);
        substitutions.setString("station", QString::fromStdString(station).toLower());

        SequenceName configUnit(station, "configuration", "synchronize");
        auto config = configSegment.takeValue(configUnit)["Processing/Authorize"];
        config.detachFromRoot();

        if (!haveRunGlobal && config["GlobalActions"].exists()) {
            if (executeActions(config["GlobalActions"])) {
                haveRunGlobal = true;
            }
        }

        if (!executeActions(config["StationActions"], station))
            continue;

        for (QList<Operation>::const_iterator op = pending.constBegin(), endP = pending.constEnd();
                op != endP;
                ++op) {
            TextSubstitutionStack::Context subop(substitutions);
            substitutions.setString("digest", op->digest);
            substitutions.setString("name", op->name);
            substitutions.setString("email", op->email);

            executeActions(config["DigestActions"], station);
        }
    }
}

void SyncAuthorize::run()
{
    if (testTerminated())
        return;
    if (!certificateArgument.isEmpty()) {
        feedback.emitStage(tr("Reading certificate"),
                           tr("The certificate %1 is being read.").arg(certificateArgument), false);
        addCertificate(certificateArgument);
    } else if (!digest.isEmpty()) {
        pending.append(Operation(digest));
    }

    if (certificateArgument.isEmpty() && digest.isEmpty()) {
        feedback.emitStage(tr("Reading certificates"),
                           tr("The certificates in the current directory are being read."), false);
        QDir current;
        QStringList files(current.entryList(QStringList("*.pem"), QDir::Files | QDir::Readable));
        for (QList<QString>::const_iterator cert = files.constBegin(), end = files.constEnd();
                cert != end;
                ++cert) {
            if (testTerminated())
                return;
            addCertificate(*cert);
        }
    }

    if (pending.isEmpty()) {
        qCDebug(log_component_sync_authorize) << "No authorizations to add";
        return;
    }

    addAuthorization();
    afterAddActions();
    sendEmails();
}

QString SyncAuthorizeComponent::promptActionContinue(const ComponentOptions &options,
                                                     const std::vector<std::string> &stations)
{
    if (stations.empty())
        return QString();

    if (options.isSet("certificate")) {
        QString file(qobject_cast<ComponentOptionFile *>(options.get("certificate"))->get());
        QSslCertificate cd(Cryptography::getCertificate(Variant::Root(file), true));
        if (!cd.isNull()) {
            auto digest = Cryptography::sha512(cd).toHex().toLower();
            auto originName = certInfo(cd.subjectInfo(QSslCertificate::CommonName));
            auto originEmail = certInfo(cd.subjectInfo(QSslCertificate::EmailAddress));
            auto requestNote =
                    certInfo(cd.subjectInfo(QSslCertificate::DistinguishedNameQualifier));
            auto requestProfile = certInfo(cd.subjectInfo(QSslCertificate::OrganizationalUnitName));

            if (!originName.isEmpty()) {
                if (!originEmail.isEmpty()) {
                    originName = tr("%1 <%2>", "email format").arg(originName, originEmail);
                }
            } else if (!originEmail.isEmpty()) {
                originName = originEmail;
            }
            if (originName.isEmpty()) {
                originName = requestNote;
            } else if (!requestNote.isEmpty()) {
                originName = tr("%1 (%2)", "note format").arg(originName, requestNote);
            }

            if (requestProfile.isEmpty()) {
                requestProfile = tr("all data", "default profile description");
            }

            if (originName.isEmpty()) {
                return tr("This will enable synchronization for %1 based on "
                          "the certificate %2 with digest %3 for %4.").arg(
                        Util::to_qstringlist(stations).join(",").toUpper(), file, digest,
                        requestProfile);
            }
            return tr("This will enable synchronization for %1 based on "
                      "the certificate %2 with digest %3 from %4 for %5.").arg(
                    Util::to_qstringlist(stations).join(",").toUpper(), file, digest, originName,
                    requestProfile);
        }
    }

    if (options.isSet("digest")) {
        QString digest(qobject_cast<ComponentOptionSingleString *>(options.get("digest"))->get());
        if (!digest.isEmpty()) {
            return tr("This will enable synchronization for %1 using the "
                      "certificate digest %2.").arg(
                    Util::to_qstringlist(stations).join(",").toUpper(), digest);
        }
    }

    QDir current;
    QStringList files(current.entryList(QStringList("*.pem"), QDir::Files | QDir::Readable));
    if (files.isEmpty())
        return tr("No files to add; this operation will have no effect.");
    return tr("This will add authorization to synchronized based on the certificate(s) %1.", "",
              files.size()).arg(files.join(","));
}


ComponentOptions SyncAuthorizeComponent::getOptions()
{
    ComponentOptions options;

    options.add("template", new ComponentOptionSingleString(tr("template", "name"),
                                                            tr("Authorization template name"),
                                                            tr("This specifies the name of the template configuration used to "
                                                               "configure the authorization."),
                                                            tr("Derived from the certificate")));

    options.add("digest", new ComponentOptionSingleString(tr("digest", "name"),
                                                          tr("Certificate digest to add"),
                                                          tr("This specifies the digest of the certificate to add authorization "
                                                             "for.  When absent this is calculated from any certificates "
                                                             "that are added."), QString()));

    options.add("certificate",
                new ComponentOptionFile(tr("certificate", "name"), tr("Certificate file to load"),
                                        tr("This is the certificate used for synchronization."),
                                        QString()));

    options.add("deauthorize", new ComponentOptionBoolean(tr("deauthorize", "name"),
                                                          tr("Remove any old authorization"),
                                                          tr("When enabled any existing authorization is removed and replaced "
                                                             "with the new authorization.  This ensures that the old "
                                                             "synchronization certificate can no longer be used."),
                                                          QString()));

    return options;
}

QList<ComponentExample> SyncAuthorizeComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options,
                                     tr("Authorize synchronization in the current directory",
                                        "default example name"),
                                     tr("This will add authorization for all certificates in the current "
                                        "directory to the given stations.")));

    options = getOptions();
    qobject_cast<ComponentOptionSingleString *>(options.get("digest"))->set(
            "7058b93963e3d6bf1dbc22b88a1a1d4a1ebfefd0");
    examples.append(ComponentExample(options, tr("Add authorization", "digest example name"),
                                     tr("This will add authorization for any stations specified to "
                                        "accept the given digest for synchronization.")));

    options = getOptions();
    qobject_cast<ComponentOptionFile *>(options.get("certificate"))->set("/tmp/sfa_sync.pem");
    qobject_cast<ComponentOptionBoolean *>(options.get("deauthorize"))->set(true);
    examples.append(ComponentExample(options, tr("File authorization", "digest example name"),
                                     tr("This will add authorization for any stations specified to  "
                                        "accept the given certificate for synchronization.")));

    return examples;
}

int SyncAuthorizeComponent::actionAllowStations()
{ return INT_MAX; }

int SyncAuthorizeComponent::actionRequireStations()
{ return 1; }

CPD3Action *SyncAuthorizeComponent::createAction(const ComponentOptions &options,
                                                 const std::vector<std::string> &stations)
{
    if (stations.empty())
        return NULL;
    return new SyncAuthorize(options, stations);
}
