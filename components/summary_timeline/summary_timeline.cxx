/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <QLoggingCategory>
#include <io/drivers/stdio.hxx>

#include "summary_timeline.hxx"

#include "core/csv.hxx"
#include "core/util.hxx"
#include "core/threadpool.hxx"
#include "datacore/variant/composite.hxx"
#include "clicore/terminaloutput.hxx"
#include "io/drivers/stdio.hxx"


Q_LOGGING_CATEGORY(log_component_summary_timeline, "cpd3.component.summary.timeline", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::CLI;

namespace {

class Timeline_DataChanges : public TimelineTarget {
    struct Segment : public Time::Bounds {
        Variant::Root value;

        Segment(const Variant::Read &v, double start, double end) : Time::Bounds(start, end),
                                                                    value(v)
        { }
    };

    std::vector<Segment> segments;
    SequenceSegment::Stream reader;
    QSet<SequenceName> units;

    DynamicTimeInterval *gap;

    bool skipGap(double priorEnd, double currentStart) const
    {
        if (!FP::defined(currentStart))
            return false;
        if (!FP::defined(priorEnd))
            return false;
        if (currentStart - priorEnd < 0.001)
            return true;
        double checkTime = gap->applyConst(priorEnd, priorEnd);
        if (!FP::defined(checkTime))
            return true;
        return currentStart - checkTime < 0.001;
    }

    void processSegments(SequenceSegment::Transfer &&toAdd)
    {
        for (auto &add : toAdd) {
            Variant::Read extracted = Variant::Read::empty();
            for (QSet<SequenceName>::const_iterator u = units.constBegin(), endU = units.constEnd();
                    u != endU;
                    ++u) {
                extracted = convertValue(add.getValue(*u));
                if (extracted.exists())
                    break;
            }

            if (!extracted.exists())
                continue;

            double start = add.getStart();
            if (!segments.empty()) {
                Q_ASSERT(FP::defined(add.getStart()));
                Q_ASSERT(FP::defined(segments.back().getEnd()));
                if (std::fabs(add.getStart() - segments.back().getEnd()) < 0.001) {
                    if (extracted == segments.back().value) {
                        segments.back().setEnd(add.getEnd());
                        continue;
                    }
                    start = segments.back().getEnd();
                } else if (skipGap(segments.back().getEnd(), add.getStart()) &&
                        extracted == segments.back().value) {
                    segments.back().setEnd(add.getEnd());
                    continue;
                }
            }

            segments.emplace_back(std::move(extracted), start, add.getEnd());
        }
    }

public:
    Timeline_DataChanges(DynamicTimeInterval *g) : segments(), reader(), units(), gap(g)
    { }

    virtual ~Timeline_DataChanges()
    {
        delete gap;
    }

    virtual bool registerInput(const SequenceName &unit)
    {
        if (unit.isMeta())
            return false;
        SequenceName add(unit);
        add.removeFlavor(SequenceName::flavor_cover);
        add.removeFlavor(SequenceName::flavor_stats);
        registerUnit(add);
        return true;
    }

    void incomingData(const SequenceValue &value) override
    { processSegments(reader.add(value)); }

    void endData() override
    { processSegments(reader.finish()); }


    virtual void fillPoint(TimelinePoint &point)
    {
        for (auto endS = segments.cend(), add =
                Range::findIntersecting(segments.cbegin(), endS, point.getStart(), point.getEnd());
                add != endS;
                ++add) {
            if (Range::compareStartEnd(add->getStart(), point.getEnd()) >= 0)
                break;

            TimelineAction *action = createPointAction(add->value);
            if (!action)
                continue;
            point.actions.push_back(TimelineActionPointer(action));
        }
    }

    virtual QList<TimelineEvent> getEvents(double dataStart, double dataEnd)
    {
        Q_UNUSED(dataStart);

        QList<TimelineEvent> result;
        if (segments.empty())
            return result;

        TimelineAction *action = createEventAction(segments.front().value);
        bool hadAction = false;
        if (action != NULL) {
            hadAction = true;
            result.append(
                    TimelineEvent(segments.front().getStart(), TimelineActionPointer(action)));
        }
        for (auto prior = segments.cbegin(), add = prior + 1, end = segments.cend();
                add != end;
                ++prior, ++add) {
            Q_ASSERT(FP::defined(add->getStart()));
            Q_ASSERT(FP::defined(prior->getEnd()));
            if (hadAction && !skipGap(prior->getEnd(), add->getStart())) {
                hadAction = false;
                action = createEventRemoveAction();
                if (action != NULL) {
                    result.append(TimelineEvent(prior->getEnd(), TimelineActionPointer(action)));
                }
            }

            action = createEventAction(add->value);
            if (action != NULL) {
                hadAction = true;
                result.append(TimelineEvent(add->getStart(), TimelineActionPointer(action)));
            } else {
                hadAction = false;
            }
        }
        if (hadAction &&
                FP::defined(dataEnd) &&
                FP::defined(segments.back().getEnd()) &&
                !skipGap(segments.back().getEnd(), dataEnd)) {
            action = createEventRemoveAction();
            if (action != NULL) {
                result.append(
                        TimelineEvent(segments.back().getEnd(), TimelineActionPointer(action)));
            }
        }

        return result;
    }

    virtual QList<TimelineEvent> getActive(double dataEnd)
    {
        QList<TimelineEvent> result;
        if (segments.empty())
            return result;
        if (!FP::defined(dataEnd) && FP::defined(segments.back().getEnd()))
            return result;
        if (!FP::defined(segments.back().getEnd()) || skipGap(segments.back().getEnd(), dataEnd)) {
            TimelineAction *action = createEventAction(segments.back().value);
            if (action != NULL) {
                result.append(
                        TimelineEvent(segments.back().getEnd(), TimelineActionPointer(action)));
            }
        }
        return result;
    }

protected:
    inline void registerUnit(const SequenceName &unit)
    { units.insert(unit); }

    virtual Variant::Read convertValue(const Variant::Read &input)
    { return input; }

    virtual TimelineAction *createAction(const Variant::Read &data)
    {
        Q_UNUSED(data);
        return NULL;
    }

    virtual TimelineAction *createPointAction(const Variant::Read &data)
    { return createAction(data); }

    virtual TimelineAction *createEventAction(const Variant::Read &data)
    { return createAction(data); }

    virtual TimelineAction *createEventRemoveAction()
    { return NULL; }
};

class Timeline_MetadataChanges : public Timeline_DataChanges {
public:
    Timeline_MetadataChanges(DynamicTimeInterval *gap) : Timeline_DataChanges(gap)
    { }

    virtual ~Timeline_MetadataChanges()
    { }

    virtual bool registerInput(const SequenceName &unit)
    {
        if (!unit.isMeta())
            return false;
        SequenceName add(unit);
        add.removeFlavor(SequenceName::flavor_cover);
        add.removeFlavor(SequenceName::flavor_stats);
        registerUnit(add);
        return true;
    }
};

class Timeline_Instrument : public Timeline_MetadataChanges {
    bool active;
    bool events;

    class Action : public TimelineAction {
        QString instrument;
    public:
        Action(const QString &i) : instrument(i)
        { }

        virtual ~Action()
        { }

        virtual QString displayData() const
        { return instrument; }

        virtual QString typeDescription() const
        {
            return QObject::tr("Instrument changed");
        }
    };

    class RemoveAction : public TimelineAction {
    public:
        RemoveAction()
        { }

        virtual ~RemoveAction()
        { }

        virtual QString displayData() const
        {
            return QObject::tr("REMOVED", "remove instrument");
        }

        virtual QString typeDescription() const
        {
            return QObject::tr("Instrument changed");
        }

        virtual bool isPresent() const
        { return false; }
    };

    static QString resultToString(const Variant::Read &value)
    {
        QString instrument;
        QString add(value.hash("Manufacturer").toDisplayString());
        if (!add.isEmpty()) {
            if (!instrument.isEmpty())
                instrument.append(' ');
            instrument.append(add);
        }
        add = value.hash("Model").toDisplayString();
        if (!add.isEmpty()) {
            if (!instrument.isEmpty())
                instrument.append(' ');
            instrument.append(add);
        }
        add = value.hash("SerialNumber").toDisplayString();
        if (!add.isEmpty()) {
            if (!instrument.isEmpty())
                instrument.append(' ');
            instrument.append(QObject::tr("#%1", "instrument serial number").arg(add));
        }
        if (instrument.isEmpty())
            instrument = QObject::tr("UNKNOWN", "unknown instrument");
        return instrument;
    }

public:
    Timeline_Instrument(DynamicTimeInterval *gap,
                        bool enableActive = true,
                        bool enableEvents = true) : Timeline_MetadataChanges(gap),
                                                    active(enableActive),
                                                    events(enableEvents)
    { }

    virtual ~Timeline_Instrument()
    { }

protected:
    virtual Variant::Read convertValue(const Variant::Read &meta)
    {
        Variant::Write output = Variant::Write::empty();

        auto v = meta.metadata("Source").hash("Manufacturer");
        if (v.exists())
            output.hash("Manufacturer").set(v);

        v = meta.metadata("Source").hash("Model");
        if (v.exists())
            output.hash("Model").set(v);

        v = meta.metadata("Source").hash("SerialNumber");
        if (v.exists())
            output.hash("SerialNumber").set(v);

        return output;
    }

    virtual TimelineAction *createAction(const Variant::Read &data)
    { return new Action(resultToString(data)); }

    virtual TimelineAction *createPointAction(const Variant::Read &data)
    {
        if (!active)
            return NULL;
        return Timeline_MetadataChanges::createPointAction(data);
    }

    virtual TimelineAction *createEventAction(const Variant::Read &data)
    {
        if (!events)
            return NULL;
        return Timeline_MetadataChanges::createEventAction(data);
    }

    virtual TimelineAction *createEventRemoveAction()
    {
        if (!events)
            return NULL;
        return new RemoveAction;
    }
};

class Timeline_Exists : public Timeline_DataChanges {
    bool active;
    bool events;

    class Action : public TimelineAction {
    public:
        Action()
        { }

        virtual ~Action()
        { }

        virtual QString displayData() const
        {
            return QObject::tr("PRESENT", "add exists");
        }

        virtual QString typeDescription() const
        {
            return QObject::tr("Presence changed");
        }
    };

    class RemoveAction : public TimelineAction {
    public:
        RemoveAction()
        { }

        virtual ~RemoveAction()
        { }

        virtual QString displayData() const
        {
            return QObject::tr("ABSENT", "remove exist");
        }

        virtual QString typeDescription() const
        {
            return QObject::tr("Presence changed");
        }

        virtual bool isPresent() const
        { return false; }
    };

public:
    Timeline_Exists(DynamicTimeInterval *gap, bool enableActive = true, bool enableEvents = true)
            : Timeline_DataChanges(gap), active(enableActive), events(enableEvents)
    { }

    virtual ~Timeline_Exists()
    { }

protected:
    virtual Variant::Read convertValue(const Variant::Read &value)
    {
        if (Variant::Composite::isDefined(value))
            return Variant::Root(true);
        return Variant::Read::empty();
    }

    virtual TimelineAction *createAction(const Variant::Read &data)
    {
        Q_UNUSED(data);
        return new Action;
    }

    virtual TimelineAction *createPointAction(const Variant::Read &data)
    {
        if (!active)
            return NULL;
        return Timeline_DataChanges::createPointAction(data);
    }

    virtual TimelineAction *createEventAction(const Variant::Read &data)
    {
        if (!events)
            return NULL;
        return Timeline_DataChanges::createEventAction(data);
    }

    virtual TimelineAction *createEventRemoveAction()
    {
        if (!events)
            return NULL;
        return new RemoveAction;
    }
};

class Timeline_DataPath : public Timeline_DataChanges {
    QString path;

protected:
    bool active;
    bool events;

    class Action : public TimelineAction {
        QString display;
    public:
        Action(const QString &d) : display(d)
        { }

        virtual ~Action()
        { }

        virtual QString displayData() const
        { return display; }

        virtual QString typeDescription() const
        {
            return QObject::tr("Value changed");
        }
    };

    class RemoveAction : public TimelineAction {
    public:
        RemoveAction()
        { }

        virtual ~RemoveAction()
        { }

        virtual QString displayData() const
        {
            return QObject::tr("NONE", "remove data path");
        }

        virtual QString typeDescription() const
        {
            return QObject::tr("Value changed");
        }

        virtual bool isPresent() const
        { return false; }
    };

public:
    Timeline_DataPath(DynamicTimeInterval *gap,
                      const QString &dataPath,
                      bool enableActive = true,
                      bool enableEvents = true) : Timeline_DataChanges(gap),
                                                  path(dataPath),
                                                  active(enableActive),
                                                  events(enableEvents)
    { }

    virtual ~Timeline_DataPath()
    { }

protected:
    virtual Variant::Read convertValue(const Variant::Read &input)
    { return input[path]; }

    virtual TimelineAction *createAction(const Variant::Read &data)
    { return new Action(formatValue(data)); }

    virtual TimelineAction *createPointAction(const Variant::Read &data)
    {
        if (!active)
            return NULL;
        return Timeline_DataChanges::createPointAction(data);
    }

    virtual TimelineAction *createEventAction(const Variant::Read &data)
    {
        if (!events)
            return NULL;
        return Timeline_DataChanges::createEventAction(data);
    }

    virtual TimelineAction *createEventRemoveAction()
    {
        if (!events)
            return NULL;
        return new RemoveAction;
    }

    QString formatValue(const Variant::Read &value) const
    {
        switch (value.getType()) {
        case Variant::Type::Array: {
            QStringList components;
            for (auto v : value.toArray()) {
                components.append(formatValue(v));
            }
            return QObject::tr("(%1)", "array format").arg(
                    components.join(QObject::tr(":", "array join")));
        }
        case Variant::Type::Matrix: {
            QStringList components;
            for (auto v : value.toMatrix()) {
                QStringList indices;
                for (auto i : v.first) {
                    indices.append(QString::number(i));
                }
                components.append(QObject::tr("[%1]=%2", "matrix key format").arg(indices.join(';'),
                                                                                  formatValue(
                                                                                        v.second)));
            }
            return QObject::tr("(%1)", "matrix format").arg(
                    components.join(QObject::tr(":", "matrix join")));
        }
        case Variant::Type::Hash: {
            QStringList components;
            typedef std::pair<Variant::PathElement::HashIndex, Variant::Read> ChildType;
            std::vector<ChildType> children;
            Util::append(value.toHash(), children);
            std::sort(children.begin(), children.end(), [](const ChildType &a, const ChildType &b) {
                return a.first < b.first;
            });
            for (auto add : children) {
                components.append(QObject::tr("%1=%2", "hash key format").arg(
                        QString::fromStdString(add.first), formatValue(add.second)));
            }
            return QObject::tr("(%1)", "hash format").arg(
                    components.join(QObject::tr(":", "hash join")));
        }
        case Variant::Type::Keyframe: {
            QStringList components;
            for (auto v : value.toKeyframe()) {
                components.append(QObject::tr("%1=%2", "keyframe key format").arg(v.first)
                                                                             .arg(formatValue(
                                                                                             v.second)));
            }
            return QObject::tr("(%1)", "keyframe format").arg(
                    components.join(QObject::tr(":", "array join")));
        }
        default:
            break;
        }
        return value.toDisplayString();
    }

};

class Timeline_MetadataPath : public Timeline_DataPath {
protected:
    class Action : public TimelineAction {
        QString display;
    public:
        Action(const QString &d) : display(d)
        { }

        virtual ~Action()
        { }

        virtual QString displayData() const
        { return display; }

        virtual QString typeDescription() const
        {
            return QObject::tr("Metadata changed");
        }
    };

    class RemoveAction : public TimelineAction {
    public:
        RemoveAction()
        { }

        virtual ~RemoveAction()
        { }

        virtual QString displayData() const
        {
            return QObject::tr("NONE", "remove metadata path");
        }

        virtual QString typeDescription() const
        {
            return QObject::tr("Metadata changed");
        }

        virtual bool isPresent() const
        { return false; }
    };

public:
    Timeline_MetadataPath(DynamicTimeInterval *gap,
                          const QString &path,
                          bool enableActive = true,
                          bool enableEvents = true) : Timeline_DataPath(gap, path, enableActive,
                                                                        enableEvents)
    { }

    virtual ~Timeline_MetadataPath()
    { }

    virtual bool registerInput(const SequenceName &unit)
    {
        if (!unit.isMeta())
            return false;
        SequenceName add(unit);
        add.removeFlavor(SequenceName::flavor_cover);
        add.removeFlavor(SequenceName::flavor_stats);
        registerUnit(add);
        return true;
    }

protected:
    virtual TimelineAction *createAction(const Variant::Read &data)
    { return new Action(formatValue(data)); }

    virtual TimelineAction *createPointAction(const Variant::Read &data)
    {
        if (!active)
            return NULL;
        return createAction(data);
    }

    virtual TimelineAction *createEventAction(const Variant::Read &data)
    {
        if (!events)
            return NULL;
        return createAction(data);
    }

    virtual TimelineAction *createEventRemoveAction()
    {
        if (!events)
            return NULL;
        return new RemoveAction;
    }
};

class Timeline_Wavelength : public Timeline_MetadataPath {
    class Action : public Timeline_MetadataPath::Action {
    public:
        Action(const QString &d) : Timeline_MetadataPath::Action(d)
        { }

        virtual ~Action()
        { }

        virtual QString typeDescription() const
        {
            return QObject::tr("Wavelength changed");
        }
    };

    class RemoveAction : public Timeline_MetadataPath::RemoveAction {
    public:
        RemoveAction()
        { }

        virtual ~RemoveAction()
        { }

        virtual QString typeDescription() const
        {
            return QObject::tr("Wavelength changed");
        }

        virtual bool isPresent() const
        { return false; }
    };

public:
    Timeline_Wavelength(DynamicTimeInterval *gap,
                        bool enableActive = true,
                        bool enableEvents = true) : Timeline_MetadataPath(gap, "^Wavelength",
                                                                          enableActive,
                                                                          enableEvents)
    { }

    virtual ~Timeline_Wavelength()
    { }

protected:
    virtual TimelineAction *createAction(const Variant::Read &data)
    { return new Action(formatValue(data)); }

    virtual TimelineAction *createEventRemoveAction()
    {
        if (!events)
            return NULL;
        return new RemoveAction;
    }
};


class Timeline_BinnedContinuous : public TimelineTarget {
protected:
    class BinData {
    public:
        BinData()
        { }

        virtual ~BinData()
        { }
    };

    struct BinPoint : public Time::Bounds {
        std::shared_ptr<BinData> data;

        BinPoint(BinData *d, double s, double e) : Time::Bounds(s, e), data(d)
        { }

        BinPoint() : Time::Bounds(), data()
        { }
    };

private:

    std::vector<BinPoint> points;
    SequenceSegment::Stream reader;
    DynamicTimeInterval *interval;

    void integrateSegment(SequenceSegment segment)
    {
        auto endP = points.end();
        auto p =
                Range::findIntersecting(points.begin(), endP, segment.getStart(), segment.getEnd());
        for (; p != endP; ++p) {
            Q_ASSERT(Range::intersects(p->getStart(), p->getEnd(), segment.getStart(),
                                       segment.getEnd()));
            integrate(p->data.get(), p->getStart(), p->getEnd(), segment);
        }
    }

    void processSegments(const SequenceSegment::Transfer &segments)
    {
        for (const auto &add : segments) {
            if (!FP::defined(add.getStart())) {
                Q_ASSERT(points.empty());

                double end = interval->round(add.getStart(), add.getEnd(), true);
                if (!FP::defined(end) || Range::compareEnd(add.getEnd(), end) > 0)
                    end = add.getEnd();

                points.emplace_back(create(), add.getStart(), end);

                BinPoint &target = points.back();
                SequenceSegment seg(add);
                integrate(target.data.get(), target.getStart(), target.getEnd(), seg);
                continue;
            }
            if (!FP::defined(add.getEnd())) {
                if (points.empty()) {
                    points.emplace_back(create(), add.getStart(), add.getEnd());
                    BinPoint &target = points.back();
                    SequenceSegment seg = add;
                    integrate(target.data.get(), target.getStart(), target.getEnd(), seg);
                    continue;
                } else {
                    points.emplace_back(create(), points.back().getEnd(), add.getEnd());
                }
                integrateSegment(add);
                continue;
            }

            if (add.getStart() >= add.getEnd())
                continue;

            if (points.empty()) {
                double start = interval->round(add.getStart(), add.getStart(), false);
                if (!FP::defined(start))
                    start = add.getStart();
                double end = interval->apply(add.getStart(), start, true);
                if (!FP::defined(end))
                    end = add.getEnd();
                Q_ASSERT(FP::defined(start));
                Q_ASSERT(FP::defined(end));
                if (start >= end) {
                    end = add.getEnd();
                    if (start >= end)
                        start = add.getStart();
                }
                Q_ASSERT(start < end);
                points.emplace_back(create(), start, end);
            }

            Q_ASSERT(!points.empty());

            double priorEnd = points.back().getEnd();
            Q_ASSERT(FP::defined(priorEnd));
            for (int i = 0; i < 1000; i++) {
                if (priorEnd >= add.getEnd())
                    break;

                double start = priorEnd;
                double end = interval->apply(add.getStart(), start, true);
                if (!FP::defined(end) || end <= start)
                    end = add.getEnd();

                Q_ASSERT(start < end);

                if (Range::intersects(start, end, add.getStart(), add.getEnd())) {
                    points.emplace_back(create(), start, end);
                }

                priorEnd = end;
            }
            if (priorEnd < add.getEnd()) {
                double start = priorEnd;
                double end = interval->round(add.getStart(), add.getEnd(), true);
                if (!FP::defined(end) || end <= start)
                    end = add.getEnd();
                Q_ASSERT(start < end);
                points.emplace_back(create(), start, end);
            }

            integrateSegment(add);
        }
    }

public:
    Timeline_BinnedContinuous(DynamicTimeInterval *i) : points(), reader(), interval(i)
    { }

    virtual ~Timeline_BinnedContinuous()
    {
        delete interval;
    }

    virtual void fillPoint(TimelinePoint &point)
    {
        auto finalP = points.end();
        auto begin =
                Range::findIntersecting(points.begin(), finalP, point.getStart(), point.getEnd());
        if (begin == finalP)
            return;
        auto end = begin;
        for (++end; end != finalP; ++end) {
            if (Range::compareStartEnd(end->getStart(), point.getEnd()) >= 0)
                break;
        }
        fillPoint(point, Util::RangeIterator<std::vector<BinPoint>::const_iterator>(begin, end));
    }

    void incomingData(const SequenceValue &value) override
    { processSegments(reader.add(value)); }

    void endData() override
    { processSegments(reader.finish()); }

protected:
    virtual BinData *create() = 0;

    virtual void integrate(BinData *data, double start, double end, SequenceSegment &segment) = 0;

    virtual void fillPoint(TimelinePoint &timeline,
                           const Util::RangeIterator<
                                   std::vector<BinPoint>::const_iterator> &points) = 0;
};

class Timeline_Coverage : public Timeline_BinnedContinuous {
    QSet<SequenceName> units;

    class Data : public BinData {
    public:
        double seconds;
        double cover;
        bool haveAnything;

        Data() : seconds(FP::undefined()), cover(FP::undefined()), haveAnything(false)
        { }

        virtual ~Data()
        { }
    };

public:
    Timeline_Coverage(DynamicTimeInterval *i) : Timeline_BinnedContinuous(i)
    { }

    virtual ~Timeline_Coverage()
    { }

    virtual bool registerInput(const SequenceName &unit)
    {
        SequenceName add(unit);
        add.removeFlavor(SequenceName::flavor_cover);
        add.removeFlavor(SequenceName::flavor_stats);
        add.clearMeta();
        units.insert(add);
        return true;
    }

protected:
    virtual BinData *create()
    { return new Data; }

    virtual void integrate(BinData *baseData, double start, double end, SequenceSegment &segment)
    {
        Data *data = static_cast<Data *>(baseData);
        if (!FP::defined(segment.getStart()) ||
                !FP::defined(segment.getEnd()) ||
                !FP::defined(start) ||
                !FP::defined(end)) {
            double bestCover = 0.0;
            for (QSet<SequenceName>::const_iterator u = units.constBegin(), endU = units.constEnd();
                    u != endU;
                    ++u) {
                auto check = segment[*u];
                if (!Variant::Composite::isDefined(check)) {
                    if (check.exists() || segment[u->toMeta()].exists())
                        data->haveAnything = true;
                    continue;
                }
                data->haveAnything = true;
                double c = segment[u->withFlavor(SequenceName::flavor_cover)].toDouble();
                if (!FP::defined(c))
                    c = 1.0;
                else if (c <= 0.0)
                    continue;
                else if (c > 1.0)
                    c = 1.0;
                bestCover = qMax(bestCover, c);
            }
            if (!FP::defined(data->cover) || bestCover > data->cover)
                data->cover = bestCover;
            return;
        }

        double segmentSeconds = qMin(end, segment.getEnd()) - qMax(start, segment.getStart());
        if (segmentSeconds <= 0.0)
            return;

        double bestSeconds = 0.0;
        double bestCover = 0.0;
        for (QSet<SequenceName>::const_iterator u = units.constBegin(), endU = units.constEnd();
                u != endU;
                ++u) {
            auto check = segment[*u];
            if (!Variant::Composite::isDefined(check)) {
                if (check.exists() || segment[u->toMeta()].exists())
                    data->haveAnything = true;
                continue;
            }
            data->haveAnything = true;
            double c = segment[u->withFlavor(SequenceName::flavor_cover)].toDouble();
            if (!FP::defined(c))
                c = 1.0;
            else if (c <= 0.0)
                continue;
            else if (c > 1.0)
                c = 1.0;

            bestSeconds = qMax(bestSeconds, segmentSeconds * c);
            bestCover = qMax(bestCover, c);
        }

        if (!FP::defined(data->seconds))
            data->seconds = bestSeconds;
        else
            data->seconds += bestSeconds;

        if (!FP::defined(data->cover) || bestCover > data->cover)
            data->cover = bestCover;
    }

    virtual void fillPoint(TimelinePoint &timeline,
                           const Util::RangeIterator<std::vector<BinPoint>::const_iterator> &points)
    {
        Q_ASSERT(!points.empty());

        double sumCover = 0.0;
        double sumSeconds = 0.0;
        bool anythingValid = false;
        for (const auto &p : points) {
            const Data *data = static_cast<const Data *>(p.data.get());

            if (data->haveAnything)
                anythingValid = true;
            if (!FP::defined(data->cover)) {
                sumCover = FP::undefined();
            } else if (FP::defined(sumCover)) {
                sumCover += data->cover;
            }

            if (!FP::defined(data->seconds)) {
                sumSeconds = FP::undefined();
            } else if (FP::defined(sumSeconds)) {
                if (!FP::defined(p.getStart()) || !FP::defined(p.getEnd()) ||
                        !FP::defined(timeline.getStart()) ||
                        !FP::defined(timeline.getEnd())) {
                    /* Merging bounds are undefined, so we can't weight this
                     * correctly */
                    sumSeconds += data->seconds;
                } else if (timeline.getStart() <= p.getStart() && timeline.getEnd() >= p.getEnd()) {
                    /* Entirely within the point, so don't need to weight */
                    sumSeconds += data->seconds;
                } else {
                    double coveredSeconds = data->seconds / (p.getEnd() - p.getStart());
                    coveredSeconds *= (qMin(p.getEnd(), timeline.getEnd()) -
                            qMax(p.getStart(), timeline.getStart()));
                    sumSeconds += coveredSeconds;
                }
            }
        }

        if (!anythingValid)
            return;
        if (FP::defined(sumSeconds) &&
                FP::defined(timeline.getStart()) &&
                FP::defined(timeline.getEnd())) {
            timeline.fraction = sumSeconds / (timeline.getEnd() - timeline.getStart());
        } else if (FP::defined(sumCover)) {
            timeline.fraction = sumCover / (double) points.size();
        } else if (!FP::defined(timeline.fraction)) {
            timeline.fraction = 0;
        }
    }
};

class Timeline_Interval : public Timeline_BinnedContinuous {
    QSet<SequenceName> units;

    class Data : public BinData {
    public:
        QHash<qint64, qint64> counts;

        Data() : counts()
        { }

        virtual ~Data()
        { }
    };

    class Action : public TimelineAction {
        double interval;
    public:
        Action(double i) : interval(i)
        { }

        virtual ~Action()
        { }

        virtual QString displayData() const
        {
            return QString::number(interval);
        }
    };

public:
    Timeline_Interval(DynamicTimeInterval *i) : Timeline_BinnedContinuous(i)
    { }

    virtual ~Timeline_Interval()
    { }

    virtual bool registerInput(const SequenceName &unit)
    {
        SequenceName add(unit);
        add.removeFlavor(SequenceName::flavor_cover);
        add.removeFlavor(SequenceName::flavor_stats);
        add.clearMeta();
        units.insert(add);
        return true;
    }

protected:
    virtual BinData *create()
    { return new Data; }

    virtual void integrate(BinData *baseData, double start, double end, SequenceSegment &segment)
    {
        Q_UNUSED(start);
        Q_UNUSED(end);

        Data *data = static_cast<Data *>(baseData);
        if (!FP::defined(segment.getStart()) || !FP::defined(segment.getEnd()))
            return;
        qint64 interval = floor((segment.getEnd() - segment.getStart()) * 1000.0 + 0.5);
        if (interval <= 0)
            return;
        for (QSet<SequenceName>::const_iterator u = units.constBegin(), endU = units.constEnd();
                u != endU;
                ++u) {
            if (!segment[*u].exists())
                continue;
            data->counts[interval]++;
        }
    }

    virtual void fillPoint(TimelinePoint &timeline,
                           const Util::RangeIterator<std::vector<BinPoint>::const_iterator> &points)
    {
        Q_ASSERT(!points.empty());

        QHash<qint64, qint64> totals(static_cast<const Data *>(points.front().data.get())->counts);
        for (const auto &p : points) {
            const Data *data = static_cast<const Data *>(p.data.get());
            for (QHash<qint64, qint64>::const_iterator add = data->counts.constBegin(),
                    endAdd = data->counts.constEnd(); add != endAdd; ++add) {
                totals[add.key()] += add.value();
            }
        }

        qint64 bestCount = 0;
        qint64 bestInterval = 0;
        for (QHash<qint64, qint64>::const_iterator add = totals.constBegin(),
                endAdd = totals.constEnd(); add != endAdd; ++add) {
            if (add.value() > bestCount) {
                bestCount = add.value();
                bestInterval = add.key();
            }
        }

        timeline.actions
                .push_back(TimelineActionPointer(new Action((double) bestInterval / 1000.0)));
    }
};

class Trace_Unit : public TimelineTrace {
protected:
    SequenceName unit;

    virtual SequenceName compareUnit(SequenceName input) const
    {
        input.clearMeta();
        input.removeFlavor(SequenceName::flavor_cover);
        input.removeFlavor(SequenceName::flavor_stats);
        return input;
    }

public:
    Trace_Unit(const SequenceName &u) : unit(u)
    { }

    virtual ~Trace_Unit()
    { }

    virtual bool sortLessThan(const TimelineTrace *other) const
    {
        const Trace_Unit *o = static_cast<const Trace_Unit *>(other);
        return SequenceName::OrderDisplay()(unit, o->unit);
    }

    virtual QStringList nameComponents() const
    {
        QStringList result;
        result.append(unit.getStationQString().toUpper());
        result.append(unit.getArchiveQString().toUpper());

        QStringList sorted = Util::to_qstringlist(unit.getFlavors());
        for (auto &f : sorted) {
            f = f.toUpper();
        }
        std::sort(sorted.begin(), sorted.end());
        result.append(sorted.join(";"));

        result.append(unit.getVariableQString());
        return result;
    }

    virtual bool primaryInput(const SequenceName &unit) const
    { return this->unit == compareUnit(unit); }
};

class Trace_NoFlavors : public Trace_Unit {
protected:
    virtual SequenceName compareUnit(SequenceName input) const
    { return Trace_Unit::compareUnit(input).withoutAllFlavors(); }

public:
    Trace_NoFlavors(const SequenceName &u) : Trace_Unit(u)
    { }

    virtual ~Trace_NoFlavors()
    { }
};

class Trace_Instrument : public Trace_Unit {
protected:
    virtual SequenceName compareUnit(SequenceName input) const
    {
        input.clearMeta();
        input.clearFlavors();
        input.setVariable(Util::suffix(input.getVariable(), '_'));
        return input;
    }

public:
    Trace_Instrument(const SequenceName &u) : Trace_Unit(u)
    { }

    virtual ~Trace_Instrument()
    { }
};


enum {
    Data_Active = 0x1, Data_Events = 0x2,
};

enum GlobalMode {
    Mode_Instrument, Mode_Presence, Mode_Variable, Mode_Coverage, Mode_Changes,
};

}

static void optionMask(const ComponentOptions &options, const QString &name, int bits, int &option)
{
    if (!options.isSet(name))
        return;
    if (qobject_cast<ComponentOptionBoolean *>(options.get(name))->get()) {
        option |= bits;
    } else {
        option &= ~bits;
    }
}

SummaryTimeline::SummaryTimeline(std::unique_ptr<IO::Generic::Stream> &&stream,
                                 const ComponentOptions &options) : stream(std::move(stream)),
                                                                    terminated(false),
                                                                    dataEnded(false),
                                                                    threadFinished(false),
                                                                    firstDataStart(FP::undefined()),
                                                                    lastDataEnd(FP::undefined()),
                                                                    firstMetadataStart(
                                                                            FP::undefined()),
                                                                    lastMetadataEnd(FP::undefined())
{
    if (options.isSet("display")) {
        displayInterval.reset(
                qobject_cast<TimeIntervalSelectionOption *>(options.get("display"))->getInterval());
    }
    if (options.isSet("interval")) {
        interval.reset(qobject_cast<TimeIntervalSelectionOption *>(
                options.get("interval"))->getInterval());
        if (!displayInterval)
            displayInterval.reset(interval->clone());
    } else {
        interval.reset(new DynamicTimeInterval::Constant(Time::Day, 1, true));
    }

    if (options.isSet("gap")) {
        gapInterval.reset(
                qobject_cast<TimeIntervalSelectionOption *>(options.get("gap"))->getInterval());
    } else {
        gapInterval.reset(new DynamicTimeInterval::Constant(Time::Day, 1, false));
    }

    traceMode = TraceMode_Instrument;
    timelineMode = TimelineMode_HorizontalTimeline;
    displayTimelineLegend = true;
    eventMode = EventMode_Standard;
    activeMode = ActiveMode_Standard;

    enableCoverage = true;
    enableInterval = false;
    enableInstrument = Data_Events;
    enableWavelength = 0;
    enableExists = 0;

    enableAdditionalData = Data_Events;
    enableAdditionalMetadata = Data_Events;


    if (options.isSet("mode")) {
        switch ((GlobalMode) (qobject_cast<ComponentOptionEnum *>(options.get("mode"))->get()
                                                                                      .getID())) {
        case Mode_Instrument:
            break;
        case Mode_Presence:
            traceMode = TraceMode_Variable;
            enableInstrument = 0;
            enableWavelength = 0;
            eventMode = EventMode_Presence;
            activeMode = ActiveMode_LatestTime;
            enableExists = Data_Events;
            break;
        case Mode_Variable:
            traceMode = TraceMode_Variable;
            enableInstrument = 0;
            enableWavelength = Data_Events;
            eventMode = EventMode_Disabled;
            activeMode = ActiveMode_Disabled;
            break;
        case Mode_Coverage:
            traceMode = TraceMode_Variable;
            timelineMode = TimelineMode_Report;
            enableInstrument = 0;
            enableWavelength = 0;
            eventMode = EventMode_Disabled;
            activeMode = ActiveMode_Disabled;
            enableCoverage = true;
            break;
        case Mode_Changes:
            traceMode = TraceMode_Variable;
            timelineMode = TimelineMode_Disabled;
            eventMode = EventMode_CSV;
            activeMode = ActiveMode_Disabled;
            break;
        }
    }

    if (options.isSet("breakdown")) {
        traceMode =
                (TraceMode) (qobject_cast<ComponentOptionEnum *>(options.get("breakdown"))->get()
                                                                                          .getID());
    }
    if (options.isSet("timeline")) {
        timelineMode = (TimelineMode) (qobject_cast<ComponentOptionEnum *>(options.get("timeline"))
                ->get()
                .getID());
    }
    if (options.isSet("legend")) {
        displayTimelineLegend =
                qobject_cast<ComponentOptionBoolean *>(options.get("legend"))->get();
    }
    if (options.isSet("events")) {
        eventMode = (EventMode) (qobject_cast<ComponentOptionEnum *>(options.get("events"))->get()
                                                                                           .getID());
    }
    if (options.isSet("active")) {
        activeMode = (ActiveMode) (qobject_cast<ComponentOptionEnum *>(options.get("active"))->get()
                                                                                             .getID());
    }

    if (options.isSet("cover")) {
        enableCoverage = qobject_cast<ComponentOptionBoolean *>(options.get("cover"))->get();
    }
    if (options.isSet("show-interval")) {
        enableInterval =
                qobject_cast<ComponentOptionBoolean *>(options.get("show-interval"))->get();
    }

    optionMask(options, "instrument-events", Data_Events, enableInstrument);
    optionMask(options, "instrument-active", Data_Active, enableInstrument);
    optionMask(options, "wavelength-events", Data_Events, enableWavelength);
    optionMask(options, "wavelength-active", Data_Active, enableWavelength);
    optionMask(options, "presence-events", Data_Events, enableExists);
    optionMask(options, "presence-active", Data_Active, enableExists);

    optionMask(options, "data-events", Data_Events, enableAdditionalData);
    optionMask(options, "data-active", Data_Active, enableAdditionalData);
    if (options.isSet("data")) {
        additionalData =
                qobject_cast<ComponentOptionStringSet *>(options.get("data"))->get().values();
        std::sort(additionalData.begin(), additionalData.end());
    }

    optionMask(options, "metadata-events", Data_Events, enableAdditionalMetadata);
    optionMask(options, "metadata-active", Data_Active, enableAdditionalMetadata);
    if (options.isSet("metadata")) {
        additionalMetadata =
                qobject_cast<ComponentOptionStringSet *>(options.get("metadata"))->get().values();
        std::sort(additionalMetadata.begin(), additionalMetadata.end());
    }
}

SummaryTimeline::~SummaryTimeline()
{
    signalTerminate();
    if (thread.joinable())
        thread.join();
}

void SummaryTimeline::incomingData(const CPD3::Data::SequenceValue::Transfer &values)
{
    if (values.empty())
        return;
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        Util::append(values, pendingProcessing);
    }
    notify.notify_all();
}

void SummaryTimeline::incomingData(CPD3::Data::SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        Util::append(std::move(values), pendingProcessing);
    }
    notify.notify_all();
}

void SummaryTimeline::incomingData(const CPD3::Data::SequenceValue &value)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        pendingProcessing.emplace_back(value);
    }
    notify.notify_all();
}

void SummaryTimeline::incomingData(CPD3::Data::SequenceValue &&value)
{
    {
        std::unique_lock<std::mutex> lock(mutex);
        stall(lock);
        pendingProcessing.emplace_back(std::move(value));
    }
    notify.notify_all();
}

void SummaryTimeline::endData()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        dataEnded = true;
    }
    notify.notify_all();
}

void SummaryTimeline::start()
{ thread = std::thread(&SummaryTimeline::run, this); }

void SummaryTimeline::run()
{
    for (;;) {
        SequenceValue::Transfer toProcess;
        bool processEnd = false;
        {
            std::unique_lock<std::mutex> lock(mutex);
            for (;;) {
                if (terminated) {
                    threadFinished = true;
                    lock.unlock();
                    stream.reset();
                    notify.notify_all();
                    finished();
                    return;
                }
                toProcess = std::move(pendingProcessing);
                pendingProcessing.clear();
                processEnd = dataEnded;
                if (!toProcess.empty() || processEnd)
                    break;
                notify.wait(lock);
            }
        }

        if (toProcess.size() > stallThreshold)
            notify.notify_all();

        dispatchData(toProcess);
        if (!processEnd)
            continue;

        generateOutput();

        {
            std::lock_guard<std::mutex> lock(mutex);
            threadFinished = true;
        }
        stream.reset();
        notify.notify_all();
        finished();
        return;
    }
}

void SummaryTimeline::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    notify.notify_all();
}

bool SummaryTimeline::isFinished()
{
    std::lock_guard<std::mutex> lock(mutex);
    return threadFinished;
}

bool SummaryTimeline::wait(double timeout)
{ return Threading::waitForTimeout(timeout, mutex, notify, [this] { return threadFinished; }); }

void SummaryTimeline::stall(std::unique_lock<std::mutex> &lock)
{
    while (pendingProcessing.size() > stallThreshold) {
        if (terminated)
            return;
        notify.wait(lock);
    }
}


template<typename T>
static void vectorAppend(std::vector<T> &target, const std::vector<T> &source)
{ target.insert(target.begin(), source.begin(), source.end()); }

static bool shouldCreateTrace(const SequenceName &unit)
{
    if (unit.isDefaultStation())
        return false;
    if (unit.getVariable() == "alias") {
        if (unit.getArchive() == "raw" ||
                unit.getArchive() == "clean" ||
                unit.getArchive() == "avgh")
            return false;
    }
    return true;
}

void SummaryTimeline::createTraceTargets(TimelineTrace *trace)
{
    if (enableCoverage) {
        trace->addTarget(new Timeline_Coverage(interval->clone()));
    }
    if (enableInterval) {
        trace->addTarget(new Timeline_Interval(interval->clone()));
    }
    if (enableInstrument) {
        trace->addTarget(
                new Timeline_Instrument(gapInterval->clone(), (enableInstrument & Data_Active),
                                        (enableInstrument & Data_Events)));
    }
    if (enableWavelength) {
        trace->addTarget(
                new Timeline_Wavelength(gapInterval->clone(), (enableWavelength & Data_Active),
                                        (enableWavelength & Data_Events)));
    }
    if (enableExists) {
        trace->addTarget(new Timeline_Exists(gapInterval->clone(), (enableExists & Data_Active),
                                             (enableExists & Data_Events)));
    }

    if (enableAdditionalData) {
        for (QList<QString>::const_iterator add = additionalData.constBegin(),
                end = additionalData.constEnd(); add != end; ++add) {
            trace->addTarget(new Timeline_DataPath(gapInterval->clone(), *add,
                                                   (enableAdditionalData & Data_Active),
                                                   (enableAdditionalData & Data_Events)));
        }
    }

    if (enableAdditionalMetadata) {
        for (QList<QString>::const_iterator add = additionalMetadata.constBegin(),
                end = additionalMetadata.constEnd(); add != end; ++add) {
            trace->addTarget(new Timeline_MetadataPath(gapInterval->clone(), *add,
                                                       (enableAdditionalMetadata & Data_Active),
                                                       (enableAdditionalMetadata & Data_Events)));
        }
    }
}

void SummaryTimeline::dispatchData(const SequenceValue::Transfer &values)
{
    for (const auto &v : values) {
        if (!v.getUnit().isMeta()) {
            if (FP::defined(v.getStart())) {
                if (!FP::defined(firstDataStart))
                    firstDataStart = v.getStart();
                if (!FP::defined(lastDataEnd) || v.getStart() > lastDataEnd)
                    lastDataEnd = v.getStart();
            }
            if (FP::defined(v.getEnd())) {
                if (!FP::defined(lastDataEnd) || v.getEnd() > lastDataEnd)
                    lastDataEnd = v.getEnd();
            }
        } else {
            if (FP::defined(v.getStart())) {
                if (!FP::defined(firstMetadataStart))
                    firstMetadataStart = v.getStart();
                if (!FP::defined(lastMetadataEnd) || v.getStart() > lastMetadataEnd)
                    lastMetadataEnd = v.getStart();
            }
            if (FP::defined(v.getEnd())) {
                if (!FP::defined(lastMetadataEnd) || v.getEnd() > lastMetadataEnd)
                    lastMetadataEnd = v.getEnd();
            }
        }

        auto target = dispatch.find(v.getName());
        if (target == dispatch.end()) {
            auto created = dispatch.emplace(v.getName(), std::vector<TimelineTarget *>()).first;

            bool havePrimary = false;
            for (const auto &t : traces) {
                if (t->primaryInput(v.getUnit())) {
                    havePrimary = true;
                    vectorAppend(created->second, t->registerInput(v.getUnit()));
                } else {
                    vectorAppend(created->second, t->registerInput(v.getUnit(), true));
                }
            }
            if (!havePrimary && shouldCreateTrace(v.getUnit())) {
                std::unique_ptr<TimelineTrace> trace;
                switch (traceMode) {
                case TraceMode_Variable: {
                    SequenceName unit(v.getUnit());
                    unit.clearMeta();
                    unit.removeFlavor(SequenceName::flavor_cover);
                    unit.removeFlavor(SequenceName::flavor_stats);
                    trace.reset(new Trace_Unit(unit));
                    break;
                }
                case TraceMode_FlattenFlavors: {
                    SequenceName unit(v.getUnit());
                    unit.clearMeta();
                    unit.clearFlavors();
                    trace.reset(new Trace_NoFlavors(unit));
                    break;
                }
                case TraceMode_Instrument: {
                    SequenceName unit(v.getUnit());
                    unit.clearMeta();
                    unit.clearFlavors();
                    unit.setVariable(Util::suffix(unit.getVariable(), '_'));
                    if (unit.getVariable().empty())
                        break;
                    trace.reset(new Trace_Instrument(unit));
                    break;
                }
                }

                if (trace) {
                    createTraceTargets(trace.get());
                    vectorAppend(created->second, trace->registerInput(v.getUnit()));
                    traces.emplace_back(std::move(trace));
                }
            }

            target = created;
        }


        for (auto c : target->second) {
            c->incomingData(v);
        }
    }
}

QStringList SummaryTimeline::buildTraceTitles()
{
    QList<bool> titleComponentMultiple;
    QList<QStringList> traceTitles;
    for (const auto &trace : traces) {
        QStringList titleComponents(trace->nameComponents());
        if (!traceTitles.isEmpty()) {
            while (titleComponentMultiple.size() < traceTitles.last().size()) {
                titleComponentMultiple.append(false);
            }
            while (titleComponentMultiple.size() < titleComponents.size()) {
                titleComponentMultiple.append(true);
            }
            for (int i = titleComponents.size(), max = titleComponentMultiple.size();
                    i < max;
                    i++) {
                titleComponentMultiple[i] = true;
            }
            for (int i = 0, max = qMin(titleComponents.size(), traceTitles.last().size());
                    i < max;
                    i++) {
                if (traceTitles.last().at(i) == titleComponents.at(i))
                    continue;
                titleComponentMultiple[i] = true;
            }
        } else {
            while (titleComponentMultiple.size() < titleComponents.size()) {
                titleComponentMultiple.append(false);
            }
        }
        traceTitles.append(titleComponents);
    }

    QStringList result;
    for (QList<QStringList>::const_iterator title = traceTitles.constBegin(),
            end = traceTitles.constEnd(); title != end; ++title) {
        QString add;
        for (int i = 0, max = title->size() - 1; i < max; i++) {
            if (!titleComponentMultiple[i])
                continue;
            if (!add.isEmpty())
                add.append(':');
            add.append(title->at(i));
        }
        if (!add.isEmpty())
            add.append(':');
        add.append(title->last());
        result.append(add);
    }
    Q_ASSERT(static_cast<std::size_t>(result.size()) == traces.size());
    return result;
}

void SummaryTimeline::outputReportTimeline(double startTime, double endTime)
{
    {
        QStringList titles(buildTraceTitles());
        titles.prepend(QObject::tr("DateTime"));
        QString output(CSV::join(titles));
        output.append('\n');
        stream->write(output);
    }

    while (startTime < endTime) {
        double binStart = startTime;
        double binEnd;
        if (displayInterval) {
            binEnd = displayInterval->apply(startTime, startTime, true);
        } else {
            binEnd = interval->apply(startTime, startTime, true);
        }
        if (!FP::defined(binEnd) || binEnd <= binStart)
            binEnd = endTime;
        startTime = binEnd;

        QStringList values;
        values.append(Time::toISO8601(binStart));

        for (const auto &trace : traces) {
            TimelinePoint point;
            point.setStart(binStart);
            point.setEnd(binEnd);

            trace->fillPoint(point);

            QString add;
            if (FP::defined(point.fraction)) {
                if (!add.isEmpty())
                    add.append(';');
                add.append(NumberFormat(1, 3).apply(qBound(0.0, point.fraction, 1.0), QChar()));
            }
            for (const auto &action : point.actions) {
                if (!add.isEmpty())
                    add.append(';');
                add.append(action->displayData());
            }

            values.append(add);
        }

        QString output(CSV::join(values));
        output.append('\n');
        stream->write(output);
    }
}

static bool eventSortCompare(const TimelineEvent &a, const TimelineEvent &b)
{ return Range::compareStart(a.time, b.time) < 0; }

static QChar fractionSymbol(double fraction)
{
    if (!FP::defined(fraction))
        return QChar(' ');
    if (fraction > 0.9)
        return QChar('*');
    if (fraction > 0.5)
        return QChar('0');
    if (fraction > 0.25)
        return QChar('o');
    return QChar('.');
}

void SummaryTimeline::buildTimeline(double startTime, double endTime, int desiredBlocks)
{
    Q_ASSERT(timelineSymbols.isEmpty());
    Q_ASSERT(timelinePoints.isEmpty());
    Q_ASSERT(timelineLegend.isEmpty());

    QList<QList<TimelinePoint> > points;
    QList<QList<TimelineEvent> > events;
    QList<double> maximumFraction;
    QSet<QString> uniqueTypes;
    for (const auto &trace : traces) {
        points.append(QList<TimelinePoint>());

        timelineSymbols.append(QList<QChar>());

        maximumFraction.append(FP::undefined());

        events.append(trace->getEvents(startTime, endTime));
        std::stable_sort(events.last().begin(), events.last().end(), eventSortCompare);
        while (!events.last().isEmpty() && !FP::defined(events.last().first().time)) {
            events.last().removeFirst();
        }

        for (QList<TimelineEvent>::const_iterator event = events.last().constBegin(),
                endE = events.last().constEnd(); event != endE; ++event) {
            Q_ASSERT(event->action);
            QString type(event->action->typeDescription());
            if (type.isEmpty())
                continue;
            uniqueTypes.insert(type);
        }
    }

    double deltaBlock = 0;
    if (desiredBlocks > 0)
        deltaBlock = (endTime - startTime) / (double) desiredBlocks;

    while (startTime < endTime) {
        double binStart = startTime;
        double binEnd;
        if (displayInterval) {
            binEnd = displayInterval->apply(startTime, startTime, true);
        } else if (desiredBlocks > 1 && deltaBlock > 0.0) {
            binEnd = binStart + deltaBlock;
        } else {
            binEnd = interval->apply(startTime, startTime, true);
        }
        if (!FP::defined(binEnd))
            binEnd = endTime;
        startTime = binEnd;

        for (int i = 0, max = traces.size(); i < max; i++) {
            TimelinePoint point;
            point.setStart(binStart);
            point.setEnd(binEnd);

            traces[i]->fillPoint(point);

            if (FP::defined(point.fraction)) {
                if (!FP::defined(maximumFraction[i]) || point.fraction > maximumFraction[i])
                    maximumFraction[i] = point.fraction;
            }

            points[i].append(point);
        }

        timelinePoints.append(Time::Bounds(binStart, binEnd));
    }

    timelineLegend.append(TimelineLegend('*', QObject::tr("Coverage > 90%")));
    timelineLegend.append(TimelineLegend('0', QObject::tr("Coverage > 50%")));
    timelineLegend.append(TimelineLegend('o', QObject::tr("Coverage > 25%")));
    timelineLegend.append(TimelineLegend('.', QObject::tr("Data present")));

    /* .o0* are reserved for fraction */
    static const char pointSymbols[] = "@#%!123456789ABCDEFGHIJKLMNPRSTUVWXYZ";
    QStringList sorted(uniqueTypes.values());
    std::sort(sorted.begin(), sorted.end());
    QHash<QString, QChar> eventSymbols;
    for (int i = 0, max = sorted.size(); i < max; i++) {
        char symbol =
                pointSymbols[(size_t) i % (sizeof(pointSymbols) / sizeof(pointSymbols[0]) - 1)];
        eventSymbols.insert(sorted.at(i), QChar(symbol));

        timelineLegend.append(TimelineLegend(QChar(symbol), sorted.at(i)));
    }

    for (int i = 0, max = traces.size(); i < max; i++) {
        for (QList<TimelinePoint>::const_iterator point = points.at(i).constBegin(),
                endP = points.at(i).constEnd(); point != endP; ++point) {
            QChar symbol;
            while (!events.at(i).isEmpty() &&
                    Range::compareStart(events.at(i).first().time, point->getStart()) < 0) {
                events[i].removeFirst();
            }
            for (QList<TimelineEvent>::const_iterator event = events.at(i).constBegin(),
                    endE = events.at(i).constEnd(); event != endE; ++event) {
                if (Range::compareStartEnd(event->time, point->getEnd()) >= 0)
                    break;

                symbol = eventSymbols.value(event->action->typeDescription(), QChar());
                if (!symbol.isNull())
                    break;
            }

            if (symbol.isNull()) {
                if (!FP::defined(maximumFraction.at(i)) || !FP::defined(point->fraction)) {
                    symbol = fractionSymbol(FP::undefined());
                } else if (maximumFraction.at(i) == 0.0) {
                    symbol = fractionSymbol(0.0);
                } else {
                    symbol = fractionSymbol(point->fraction / maximumFraction.at(i));
                }
            }

            if (symbol.isNull())
                symbol = QChar(' ');

            timelineSymbols[i].append(symbol);
        }
    }
}

void SummaryTimeline::outputTimelineLegend()
{
    for (QList<TimelineLegend>::const_iterator item = timelineLegend.constBegin(),
            end = timelineLegend.constEnd(); item != end; ++item) {
        QString output(QObject::tr("%1 - %2", "legend format").arg(QString(item->symbol),
                                                                   item->description));
        output.append('\n');
        stream->write(output);
    }
}

namespace {
enum DateDisplayFormat {
    Date_Milliseconds,
    Date_Full,
    Date_Day,
    Date_YearOnly,
    Date_YearQuarter,
    Date_YearMonth,
    Date_YearWeek,
    Date_YearDOY,
};

static int formatWidth(DateDisplayFormat format)
{
    switch (format) {
    case Date_Milliseconds:
        return 24;
    case Date_Full:
        return 21;
    case Date_Day:
        return 10;
    case Date_YearOnly:
        return 4;
    case Date_YearQuarter:
        return 6;
    case Date_YearMonth:
        return 7;
    case Date_YearWeek:
        return 7;
    case Date_YearDOY:
        return 14;
    }
    return 1;
}

struct DateAlignment {
    bool onYears;
    bool onQuarters;
    bool onMonths;
    bool onWeeks;
    bool onDays;

    DateAlignment(const QList<Time::Bounds> &points) : onYears(true),
                                                       onQuarters(true),
                                                       onMonths(true),
                                                       onWeeks(true),
                                                       onDays(true)
    {
        for (QList<Time::Bounds>::const_iterator point = points.constBegin(),
                end = points.constEnd(); point != end; ++point) {
            double time = point->getStart();
            QDateTime dt(Time::toDateTime(time));

            if (dt.time().second() != 0 || dt.time().minute() != 0 || dt.time().hour() != 0) {
                onYears = false;
                onQuarters = false;
                onMonths = false;
                onWeeks = false;
                onDays = false;
            } else if (dt.date().day() != 1) {
                onYears = false;
                onMonths = false;
            } else if (dt.date().month() != 1) {
                onYears = false;
            }

            {
                int year = -1;
                int quarter = -1;
                Time::containingQuarter(time, &year, &quarter);
                if (fabs(Time::quarterStart(year, quarter) - time) >= 1.0)
                    onQuarters = false;
            }

            {
                int year = -1;
                int week = dt.date().weekNumber(&year);
                if (fabs(Time::weekStart(year, week) - time) >= 1.0)
                    onWeeks = false;
            }
        }
    }
};

static DateDisplayFormat calculateDateFormat(const QList<Time::Bounds> &points,
                                             double startTime,
                                             double endTime)
{
    DateAlignment align(points);
    double interval = endTime - startTime;
    if (interval < 5)
        return Date_Milliseconds;
    else if (interval < 86400 * 4)
        return Date_Full;
    else if (align.onYears)
        return Date_YearOnly;
    else if (align.onQuarters)
        return Date_YearQuarter;
    else if (align.onMonths)
        return Date_YearMonth;
    else if (align.onWeeks)
        return Date_YearWeek;
    return Date_Day;
}

static void positionDatesUnaligned(const QList<Time::Bounds> &points,
                                   QList<int> &positions,
                                   DateDisplayFormat format)
{
    int totalWidth = points.size();
    int labelWidth = formatWidth(format);

    positions.append(0);
    totalWidth -= labelWidth + 1;
    if (totalWidth >= labelWidth + 1) {
        positions.append(points.size() - 1);
        totalWidth -= labelWidth + 1;
    }

    int totalMiddleLabels = totalWidth / (labelWidth + 3);
    for (int i = 0; i < totalMiddleLabels; i++) {
        int position = (points.size() * i + totalMiddleLabels / 2) / totalMiddleLabels;
        positions.insert(positions.size() - 1, position);
    }
}

static DateDisplayFormat positionDates(const QList<Time::Bounds> &points,
                                       double startTime,
                                       double endTime,
                                       QList<int> &positions)
{
    DateAlignment align(points);
    double interval = endTime - startTime;

    if (interval < 5) {
        positionDatesUnaligned(points, positions, Date_Milliseconds);
        return Date_Milliseconds;
    } else if (interval < 86400 * (points.size() / 10 + 3)) {
        positionDatesUnaligned(points, positions, Date_Full);
        return Date_Full;
    } else if (align.onYears) {
        positionDatesUnaligned(points, positions, Date_YearOnly);
        return Date_YearOnly;
    } else if (align.onWeeks) {
        positionDatesUnaligned(points, positions, Date_YearWeek);
        return Date_YearWeek;
    } else if (align.onMonths) {
        positionDatesUnaligned(points, positions, Date_YearMonth);
        return Date_YearMonth;
    } else if (align.onQuarters) {
        positionDatesUnaligned(points, positions, Date_YearQuarter);
        return Date_YearQuarter;
    }

    /* NYI: Do something with positioning them for best alignment */

    positionDatesUnaligned(points, positions, Date_Day);
    return Date_Day;
}

static QString formatTime(double time, DateDisplayFormat format)
{
    switch (format) {
    case Date_Milliseconds:
        return Time::toISO8601(time, true);
    case Date_Full:
        return Time::toISO8601(time);
    case Date_Day:
        return Time::toDateTime(time).toString("yyyy-MM-dd");
    case Date_YearOnly:
        return QString::number(Time::toDateTime(time).date().year());
    case Date_YearQuarter: {
        int year = -1;
        int quarter = -1;
        Time::containingQuarter(time, &year, &quarter);
        QString result(QString::number(year));
        result.append('Q');
        result.append(QString::number(quarter));
        return result;
    }
    case Date_YearMonth:
        return Time::toDateTime(time).toString("yyyy-MM");
    case Date_YearWeek: {
        QDateTime dt(Time::toDateTime(time));
        int year = -1;
        int week = dt.date().weekNumber(&year);
        QString result(QString::number(year));
        result.append('W');
        result.append(QString::number(week));
        return result;
    }
    case Date_YearDOY:
        return Time::toYearDOY(time, QString(':'));
    }
    return QString();
}

}

void SummaryTimeline::outputVerticalTimeline(double startTime, double endTime)
{
    int termHeight = -1;
    if (auto s = dynamic_cast<IO::STDIO::Stream *>(stream.get())) {
        termHeight = s->terminalHeight();
    }
    if (termHeight <= 0)
        termHeight = 24;
    buildTimeline(startTime, endTime, termHeight);

    if (displayTimelineLegend) {
        outputTimelineLegend();
        stream->write("\n");
    }

    QStringList labels;
    int timeWidth = 0;
    DateDisplayFormat dateFormat = calculateDateFormat(timelinePoints, startTime, endTime);
    for (QList<Time::Bounds>::const_iterator point = timelinePoints.constBegin(),
            end = timelinePoints.constEnd(); point != end; ++point) {
        labels.append(formatTime(point->getStart(), dateFormat));
        timeWidth = qMax(timeWidth, labels.last().length());
    }

    QStringList padding;
    padding.append(QString());

    {
        QString output(timeWidth, QChar(' '));

        QStringList headers(buildTraceTitles());
        for (QList<QString>::const_iterator title = headers.constBegin(), end = headers.constEnd();
                title != end;
                ++title) {
            output.append(' ');
            output.append(*title);

            int totalPadding = title->length() - 1;
            int paddingBefore = totalPadding / 2;
            int paddingAfter = totalPadding - paddingBefore;

            paddingBefore++;

            padding.last().append(QString(paddingBefore, ' '));
            padding.append(QString(paddingAfter, ' '));
        }

        output.append('\n');
        stream->write(output);
    }

    for (int pIdx = 0, maxP = timelinePoints.size(); pIdx < maxP; pIdx++) {
        QString output(labels.at(pIdx).rightJustified(timeWidth, ' '));
        for (int tIdx = 0, maxT = timelineSymbols.size(); tIdx < maxT; tIdx++) {
            output.append(padding.at(tIdx));
            output.append(timelineSymbols.at(tIdx).at(pIdx));
        }
        output.append('\n');
        stream->write(output);
    }
}

void SummaryTimeline::outputHorizontalTimeline(double startTime, double endTime)
{
    int termWidth = -1;
    if (auto s = dynamic_cast<IO::STDIO::Stream *>(stream.get())) {
        termWidth = s->terminalWidth();
    }
    if (termWidth <= 0)
        termWidth = 80;

    QStringList headers(buildTraceTitles());
    int headerWidth = 0;
    for (QList<QString>::const_iterator header = headers.constBegin(), end = headers.constEnd();
            header != end;
            ++header) {
        headerWidth = qMax(headerWidth, header->length());
    }
    Q_ASSERT(static_cast<std::size_t>(headers.size()) == traces.size());

    buildTimeline(startTime, endTime, termWidth - headerWidth - 2);
    Q_ASSERT(timelineSymbols.size() == headers.size());

    if (displayTimelineLegend) {
        outputTimelineLegend();
        stream->write("\n");
    }

    {
        QList<int> datePositions;
        DateDisplayFormat
                dateFormat = positionDates(timelinePoints, startTime, endTime, datePositions);
        QString output;
        for (QList<int>::const_iterator centerIndex = datePositions.constBegin(),
                end = datePositions.constEnd(); centerIndex != end; ++centerIndex) {
            double time;
            if (*centerIndex == 0) {
                time = timelinePoints.first().getStart();
            } else if (*centerIndex == timelinePoints.size() - 1) {
                time = timelinePoints.last().getEnd();
            } else {
                time = (timelinePoints.at(*centerIndex).getEnd() +
                        timelinePoints.at(*centerIndex).getStart()) * 0.5;
            }

            QString display(formatTime(time, dateFormat));
            int displayIndex = *centerIndex - display.length() / 2;
            if (displayIndex + display.length() > timelinePoints.size())
                displayIndex = timelinePoints.size() - display.length();
            if (displayIndex < 0)
                displayIndex = 0;
            while (displayIndex >= output.length()) {
                output.append(' ');
            }
            output.replace(displayIndex, display.length(), display);
        }

        output.prepend(QString(headerWidth + 1, ' '));
        output.append('\n');
        stream->write(output);
    }

    for (int tIdx = 0, maxT = timelineSymbols.size(); tIdx < maxT; tIdx++) {
        QString output(headers.at(tIdx).rightJustified(headerWidth, ' '));
        output.append(' ');

        for (int pIdx = 0, maxP = timelinePoints.size(); pIdx < maxP; pIdx++) {
            output.append(timelineSymbols.at(tIdx).at(pIdx));
        }

        output.append('\n');
        stream->write(output);
    }
}

namespace {
struct GlobalDisplayEvent : public TimelineEvent {
    QString origin;
    int index;

    GlobalDisplayEvent(const TimelineEvent &event, const QString &o, int i) : TimelineEvent(event),
                                                                              origin(o),
                                                                              index(i)
    { }
};
}

void SummaryTimeline::outputEvents(double startTime, double endTime)
{
    std::vector<GlobalDisplayEvent> events;
    QStringList headers(buildTraceTitles());
    for (int i = 0, max = traces.size(); i < max; i++) {
        QList<TimelineEvent> toAdd(traces.at(i)->getEvents(startTime, endTime));
        for (QList<TimelineEvent>::const_iterator add = toAdd.constBegin(), end = toAdd.constEnd();
                add != end;
                ++add) {
            events.emplace_back(*add, headers.at(i), i);
        }
    }
    std::stable_sort(events.begin(), events.end(), eventSortCompare);

    QList<QStringList> rows;
    QList<int> columnWidths;
    for (const auto &event : events) {
        rows.append(QStringList() << Time::toISO8601(event.time) << event.origin
                                  << event.action->displayData());

        for (int i = 0, max = rows.last().size(); i < max; i++) {
            while (i >= columnWidths.size()) {
                columnWidths.append(0);
            }
            columnWidths[i] = qMax(columnWidths.at(i), rows.last().at(i).length());
        }
    }

    for (QList<QStringList>::const_iterator row = rows.constBegin(), end = rows.constEnd();
            row != end;
            ++row) {
        QString output;
        for (int i = 0, max = row->size(); i < max; i++) {
            if (output.isEmpty())
                output.append(QString(4, ' '));
            else
                output.append(' ');
            output.append(row->at(i).rightJustified(columnWidths.at(i), ' '));
        }
        output.append('\n');
        stream->write(output);
    }
}

void SummaryTimeline::outputEventsPresence(double startTime, double endTime)
{
    QList<GlobalDisplayEvent> events;
    QStringList headers(buildTraceTitles());
    for (int i = 0, max = traces.size(); i < max; i++) {
        QList<TimelineEvent> toAdd(traces.at(i)->getEvents(startTime, endTime));
        for (QList<TimelineEvent>::const_iterator add = toAdd.constBegin(), end = toAdd.constEnd();
                add != end;
                ++add) {
            events.append(GlobalDisplayEvent(*add, headers.at(i), i));
        }
    }
    std::stable_sort(events.begin(), events.end(), eventSortCompare);

    QList<QStringList> rows;
    QList<int> columnWidths;
    QVector<bool> wasPresent(traces.size(), false);
    for (QList<GlobalDisplayEvent>::const_iterator event = events.constBegin(),
            end = events.constEnd(); event != end; ++event) {
        bool isPresent = event->action->isPresent();
        if (wasPresent[event->index] == isPresent)
            continue;
        if (isPresent) {
            rows.append(QStringList() << Time::toISO8601(event->time) << event->origin
                                      << QObject::tr("BEGIN", "presence event"));
        } else {
            rows.append(QStringList() << Time::toISO8601(event->time) << event->origin
                                      << QObject::tr("END", "absence event"));
        }
        wasPresent[event->index] = isPresent;

        for (int i = 0, max = rows.last().size(); i < max; i++) {
            while (i >= columnWidths.size()) {
                columnWidths.append(0);
            }
            columnWidths[i] = qMax(columnWidths.at(i), rows.last().at(i).length());
        }
    }

    for (QList<QStringList>::const_iterator row = rows.constBegin(), end = rows.constEnd();
            row != end;
            ++row) {
        QString output;
        for (int i = 0, max = row->size(); i < max; i++) {
            if (output.isEmpty())
                output.append(QString(4, ' '));
            else
                output.append(' ');
            output.append(row->at(i).rightJustified(columnWidths.at(i), ' '));
        }
        output.append('\n');
        stream->write(output);
    }
}

void SummaryTimeline::outputEventsCSV(double startTime, double endTime)
{
    QStringList headers(buildTraceTitles());
    {
        QString output(CSV::join(QStringList() << QObject::tr("DateTime") << QObject::tr("Origin")
                                               << QObject::tr("Event")));
        output.append('\n');
        stream->write(output);
    }

    QList<GlobalDisplayEvent> events;
    for (int i = 0, max = traces.size(); i < max; i++) {
        QList<TimelineEvent> toAdd(traces.at(i)->getEvents(startTime, endTime));
        for (QList<TimelineEvent>::const_iterator add = toAdd.constBegin(), end = toAdd.constEnd();
                add != end;
                ++add) {
            events.append(GlobalDisplayEvent(*add, headers.at(i), i));
        }
    }
    std::stable_sort(events.begin(), events.end(), eventSortCompare);

    for (QList<GlobalDisplayEvent>::const_iterator event = events.constBegin(),
            end = events.constEnd(); event != end; ++event) {
        QString output(CSV::join(QStringList() << Time::toISO8601(event->time) << event->origin
                                               << event->action->displayData()));
        output.append('\n');
        stream->write(output);
    }
}

void SummaryTimeline::outputActive(double endTime)
{
    QList<QStringList> rows;
    QStringList headers(buildTraceTitles());
    double now = Time::time();
    for (int i = 0, max = traces.size(); i < max; i++) {
        QList<TimelineEvent> active(traces.at(i)->getActive(endTime));
        if (active.isEmpty())
            continue;

        QStringList row;
        bool haveNonEmpty = false;
        double latestTime = FP::undefined();
        for (QList<TimelineEvent>::const_iterator add = active.constBegin(),
                end = active.constEnd(); add != end; ++add) {
            QString str(add->action->displayData());
            if (!str.isEmpty())
                haveNonEmpty = true;

            switch (activeMode) {
            case ActiveMode_LatestTime:
                break;
            default:
                row.append(str);
                break;
            }

            if (FP::defined(add->time) && (!FP::defined(latestTime) || latestTime < add->time))
                latestTime = add->time;
        }
        if (!haveNonEmpty)
            continue;

        switch (activeMode) {
        case ActiveMode_LatestTime: {
            if (!FP::defined(latestTime))
                break;
            row.append(Time::toISO8601(latestTime));

            int year;
            int week = Time::toDateTime(latestTime).date().weekNumber(&year);
            double start = Time::weekStart(year, week);
            if (fabs(start - latestTime) < 1.0) {
                /* Output a week number that contains it, rather than 
                 * the actual end week */
                week = Time::toDateTime(latestTime - 1.0).date().weekNumber(&year);

                if (now - start >= 604800.0) {
                    int behind = (int) floor((now - start) / 604800.0);
                    row.append(
                            QObject::tr("Week %1 (%n week(s) behind)", "weeks behind", behind).arg(
                                    week));
                } else {
                    row.append(QObject::tr("Week %1").arg(week));
                }
            }
            break;
        }
        default:
            break;
        }

        row.prepend(headers.at(i));
        rows.append(row);
    }

    QList<int> columnWidths;
    for (QList<QStringList>::const_iterator row = rows.constBegin(), end = rows.constEnd();
            row != end;
            ++row) {
        for (int i = 0, max = row->size(); i < max; i++) {
            while (i >= columnWidths.size()) {
                columnWidths.append(0);
            }
            columnWidths[i] = qMax(columnWidths.at(i), row->at(i).length());
        }
    }

    for (QList<QStringList>::const_iterator row = rows.constBegin(), end = rows.constEnd();
            row != end;
            ++row) {
        QString output;
        for (int i = 0, max = row->size(); i < max; i++) {
            if (output.isEmpty())
                output.append(QString(4, ' '));
            else
                output.append(' ');
            output.append(row->at(i).rightJustified(columnWidths.at(i), ' '));
        }
        output.append('\n');
        stream->write(output);
    }
}

static bool traceSortCompare(const std::unique_ptr<TimelineTrace> &a,
                             const std::unique_ptr<TimelineTrace> &b)
{ return a->sortLessThan(b.get()); }

void SummaryTimeline::generateOutput()
{
    for (const auto &t : traces) {
        t->endData();
    }

    double startTime = firstDataStart;
    if (!FP::defined(startTime))
        startTime = firstMetadataStart;
    double endTime = lastDataEnd;
    if (!FP::defined(endTime))
        endTime = lastMetadataEnd;
    if (!FP::defined(startTime) || !FP::defined(endTime) || startTime >= endTime)
        return;
    std::sort(traces.begin(), traces.end(), traceSortCompare);

    bool haveGeneratedOutput = false;

    switch (timelineMode) {
    case TimelineMode_Disabled:
        break;
    case TimelineMode_HorizontalTimeline:
        outputHorizontalTimeline(startTime, endTime);
        haveGeneratedOutput = true;
        break;
    case TimelineMode_VerticalTimeline:
        outputVerticalTimeline(startTime, endTime);
        haveGeneratedOutput = true;
        break;
    case TimelineMode_Report:
        outputReportTimeline(startTime, endTime);
        haveGeneratedOutput = true;
        break;
    }

    switch (eventMode) {
    case EventMode_Disabled:
        break;
    case EventMode_Standard:
        if (haveGeneratedOutput) {
            stream->write(QObject::tr("\nChanges:\n"));
        }
        outputEvents(startTime, endTime);
        haveGeneratedOutput = true;
        break;
    case EventMode_Presence:
        if (haveGeneratedOutput) {
            stream->write(QObject::tr("\nPresence:\n"));
        }
        outputEventsPresence(startTime, endTime);
        haveGeneratedOutput = true;
        break;
    case EventMode_CSV:
        if (haveGeneratedOutput) {
            stream->write("\n");
        }
        outputEventsCSV(startTime, endTime);
        haveGeneratedOutput = true;
        break;
    }

    if (activeMode != ActiveMode_Disabled) {
        if (haveGeneratedOutput) {
            stream->write(QObject::tr("\nLatest:\n"));
        }
        outputActive(endTime);
        haveGeneratedOutput = true;
    }
}


TimelineAction::TimelineAction()
{ }

TimelineAction::~TimelineAction()
{ }

QString TimelineAction::typeDescription() const
{ return QString(); }

bool TimelineAction::isPresent() const
{ return true; }

TimelinePoint::TimelinePoint() : actions(), fraction(FP::undefined())
{ }

TimelineEvent::TimelineEvent() : time(FP::undefined()), action()
{ }

TimelineEvent::TimelineEvent(double t, const TimelineActionPointer &a) : time(t), action(a)
{ }

TimelineTarget::TimelineTarget()
{ }

TimelineTarget::~TimelineTarget()
{ }

void TimelineTarget::endData()
{ }

bool TimelineTarget::registerInput(const SequenceName &unit)
{
    Q_UNUSED(unit);
    return false;
}

bool TimelineTarget::registerExternal(const SequenceName &unit)
{
    Q_UNUSED(unit);
    return false;
}

void TimelineTarget::fillPoint(TimelinePoint &point)
{
    Q_UNUSED(point);
}

QList<TimelineEvent> TimelineTarget::getEvents(double dataStart, double dataEnd)
{
    Q_UNUSED(dataStart);
    Q_UNUSED(dataEnd);
    return QList<TimelineEvent>();
}

QList<TimelineEvent> TimelineTarget::getActive(double dataEnd)
{
    Q_UNUSED(dataEnd);
    return QList<TimelineEvent>();
}


TimelineTrace::TimelineTrace() : targets()
{ }

TimelineTrace::~TimelineTrace()
{
    qDeleteAll(targets);
}

std::vector<TimelineTarget *> TimelineTrace::registerInput(const SequenceName &unit, bool external)
{
    std::vector<TimelineTarget *> result;
    for (QList<TimelineTarget *>::const_iterator t = targets.constBegin(), end = targets.constEnd();
            t != end;
            ++t) {
        if (external) {
            if (!(*t)->registerExternal(unit))
                continue;
        } else {
            if (!(*t)->registerInput(unit))
                continue;
        }
        result.push_back(*t);
    }
    return result;
}

void TimelineTrace::endData()
{
    for (QList<TimelineTarget *>::const_iterator t = targets.constBegin(), end = targets.constEnd();
            t != end;
            ++t) {
        (*t)->endData();
    }
}

void TimelineTrace::addTarget(TimelineTarget *t)
{
    targets.append(t);
}

void TimelineTrace::fillPoint(TimelinePoint &point)
{
    for (QList<TimelineTarget *>::const_iterator t = targets.constBegin(),
            endT = targets.constEnd(); t != endT; ++t) {
        (*t)->fillPoint(point);
    }
}

QList<TimelineEvent> TimelineTrace::getEvents(double dataStart, double dataEnd)
{
    QList<TimelineEvent> result;
    for (QList<TimelineTarget *>::const_iterator t = targets.constBegin(),
            endT = targets.constEnd(); t != endT; ++t) {
        result.append((*t)->getEvents(dataStart, dataEnd));
    }
    return result;
}

QList<TimelineEvent> TimelineTrace::getActive(double dataEnd)
{
    QList<TimelineEvent> result;
    for (QList<TimelineTarget *>::const_iterator t = targets.constBegin(),
            endT = targets.constEnd(); t != endT; ++t) {
        result.append((*t)->getActive(dataEnd));
    }
    return result;
}


ComponentOptions SummaryTimelineComponent::getOptions()
{
    ComponentOptions options;

    ComponentOptionEnum *mode = new ComponentOptionEnum(tr("mode", "name"), tr("Base mode"),
                                                        tr("This is the base summary mode.  This determines the results "
                                                           "output as well as the method of displaying them."),
                                                        tr("Instrument coverage"), -1);
    mode->add(Mode_Instrument, "instrument", tr("instrument", "mode name"),
              tr("Instrument coverage summary"));
    mode->add(Mode_Presence, "presence", tr("presence", "mode name"),
              tr("Variable presence summary"));
    mode->add(Mode_Variable, "variable", tr("variable", "mode name"),
              tr("Variable coverage summary"));
    mode->add(Mode_Coverage, "cover", tr("cover", "mode name"), tr("Coverage fraction report"));
    mode->add(Mode_Changes, "changes", tr("changes", "mode name"), tr("Data change events"));
    options.add("mode", mode);


    ComponentOptionEnum *trace =
            new ComponentOptionEnum(tr("breakdown", "name"), tr("Input data breakdown"),
                                    tr("This is the method that input data is broken down into separate "
                                       "traces or columns.  For example in instrument mode all inputs "
                                       "belonging to the same instrument are combined into one output."),
                                    tr("Determined by base mode"));
    trace->add(SummaryTimeline::TraceMode_Variable, "variable", tr("variable", "mode name"),
               tr("Break down data into a separate output for each unique combination "
                  "of station, archive, variable and flavors."));
    trace->add(SummaryTimeline::TraceMode_FlattenFlavors, "flavorless",
               tr("flavorless", "mode name"),
               tr("Break down data into a separate output for each unique combination "
                  "of station, archive, and variable while combining flavors."));
    trace->add(SummaryTimeline::TraceMode_Instrument, "instrument", tr("instrument", "mode name"),
               tr("Combine data for instruments together (e.x. \"S11\")."));
    options.add("breakdown", trace);


    ComponentOptionEnum *timeline =
            new ComponentOptionEnum(tr("timeline", "name"), tr("Timeline display mode"),
                                    tr("This is the way the timeline is displayed."),
                                    tr("Determined by base mode"));
    timeline->add(SummaryTimeline::TimelineMode_HorizontalTimeline, "horizontal",
                  tr("horizontal", "mode name"),
                  tr("A timeline of symbols for each output is generated with the time "
                     "axis as columns and the outputs as rows."));
    timeline->add(SummaryTimeline::TimelineMode_VerticalTimeline, "vertical",
                  tr("vertical", "mode name"),
                  tr("A timeline of symbols for each output is generated with the time "
                     "axis as rows and the outputs as columns."));
    timeline->add(SummaryTimeline::TimelineMode_Report, "csv", tr("csv", "mode name"),
                  tr("A CSV style time series report is generated with the values at "
                     "each time step reported as rows and with the outputs as "
                     "columns."));
    timeline->add(SummaryTimeline::TimelineMode_Disabled, "none", tr("none", "mode name"),
                  tr("Timeline output disabled."));
    options.add("timeline", timeline);

    options.add("legend",
                new ComponentOptionBoolean(tr("legend", "name"), tr("Enable timeline legend"),
                                           tr("When set a legend is output when displaying a symbol based "
                                              "timeline."), tr("Enabled")));


    ComponentOptionEnum *events =
            new ComponentOptionEnum(tr("events", "name"), tr("Event display mode"),
                                    tr("This is the way change events are displayed."),
                                    tr("Determined by base mode"));
    events->add(SummaryTimeline::EventMode_Standard, "normal", tr("normal", "mode name"),
                tr("Space separated events are displayed."));
    events->add(SummaryTimeline::EventMode_Presence, "presence", tr("presence", "mode name"),
                tr("Space separated presence or absence only events are displayed."));
    events->add(SummaryTimeline::EventMode_CSV, "csv", tr("csv", "mode name"),
                tr("Comma separated events are displayed."));
    events->add(SummaryTimeline::EventMode_Disabled, "none", tr("none", "mode name"),
                tr("Event output disabled."));
    options.add("events", events);


    ComponentOptionEnum *active =
            new ComponentOptionEnum(tr("active", "name"), tr("Latest active display mode"),
                                    tr("When enabled a summary of the currently active parameters is "
                                       "output.  For example this would output the instruments active "
                                       "if instrument display is enabled."),
                                    tr("Determined by base mode"));
    active->add(SummaryTimeline::ActiveMode_Standard, "normal", tr("normal", "mode name"),
                tr("A summary of active parameters."));
    active->add(SummaryTimeline::ActiveMode_LatestTime, "latest", tr("latest", "mode name"),
                tr("A summary of active parameters relative to the current time."));
    active->add(SummaryTimeline::ActiveMode_Disabled, "none", tr("none", "mode name"),
                tr("Latest active display disabled."));
    options.add("active", active);


    options.add("cover",
                new ComponentOptionBoolean(tr("cover", "name"), tr("Enable coverage information"),
                                           tr("When enabled the timeline will incorporate coverage information "
                                              "into the display."), tr("Determined by base mode")));
    options.add("show-interval", new ComponentOptionBoolean(tr("show-interval", "name"),
                                                            tr("Enable data interval information"),
                                                            tr("When enabled data interval information will be incorporated into "
                                                               "the output report.  This has no effect on symbol based "
                                                               "timelines."), QString()));

    options.add("instrument-events", new ComponentOptionBoolean(tr("instrument-events", "name"),
                                                                tr("Enable instrument events"),
                                                                tr("When enabled instrument change events will incorporated into "
                                                                   "the output."),
                                                                tr("Determined by base mode")));
    options.add("instrument-active", new ComponentOptionBoolean(tr("instrument-active", "name"),
                                                                tr("Enable active instrument display"),
                                                                tr("When enabled active instrument information will be incorporated "
                                                                   "into the output report.  This has no effect on symbol based "
                                                                   "timelines."), QString()));

    options.add("wavelength-events", new ComponentOptionBoolean(tr("wavelength-events", "name"),
                                                                tr("Enable instrument events"),
                                                                tr("When enabled wavelength change events will incorporated into "
                                                                   "the output."),
                                                                tr("Determined by base mode")));
    options.add("wavelength-active", new ComponentOptionBoolean(tr("wavelength-active", "name"),
                                                                tr("Enable active instrument display"),
                                                                tr("When enabled current wavelength information will be incorporated "
                                                                   "into the output report.  This has no effect on symbol based "
                                                                   "timelines."), QString()));

    options.add("presence-events", new ComponentOptionBoolean(tr("presence-events", "name"),
                                                              tr("Enable instrument events"),
                                                              tr("When enabled presence change events will incorporated into "
                                                                 "the output."),
                                                              tr("Determined by base mode")));
    options.add("presence-active", new ComponentOptionBoolean(tr("presence-active", "name"),
                                                              tr("Enable active instrument display"),
                                                              tr("When enabled current presence information will be incorporated "
                                                                 "into the output report.  This has no effect on symbol based "
                                                                 "timelines."), QString()));


    options.add("data-events", new ComponentOptionBoolean(tr("data-events", "name"),
                                                          tr("Enable additional data events"),
                                                          tr("When enabled changes to the additional data specifications will "
                                                             "be incorporated into the output.  Unless one or more additional "
                                                             "data paths are set, this will have no effect."),
                                                          tr("Enabled")));
    options.add("data-active", new ComponentOptionBoolean(tr("data-active", "name"),
                                                          tr("Enable additional data current value display"),
                                                          tr("When enabled the current values of the additional data parameters "
                                                             "will be incorporated into the output report.  This has no effect "
                                                             "on symbol based timelines and no effect unless one or more "
                                                             "additional data paths are set."),
                                                          QString()));
    options.add("data",
                new ComponentOptionStringSet(tr("data", "name"), tr("Additional data paths"),
                                             tr("This sets any additional data paths to include in the final "
                                                "summary output."), QString()));

    options.add("metadata-events", new ComponentOptionBoolean(tr("metadata-events", "name"),
                                                              tr("Enable additional data events"),
                                                              tr("When enabled changes to the additional data specifications will "
                                                                 "be incorporated into the output.  Unless one or more additional "
                                                                 "data paths are set, this will have no effect."),
                                                              tr("Enabled")));
    options.add("metadata-active", new ComponentOptionBoolean(tr("metadata-active", "name"),
                                                              tr("Enable additional data current value display"),
                                                              tr("When enabled the current values of the additional data parameters "
                                                                 "will be incorporated into the output report.  This has no effect "
                                                                 "on symbol based timelines and no effect unless one or more "
                                                                 "additional data paths are set."),
                                                              QString()));
    options.add("metadata", new ComponentOptionStringSet(tr("metadata", "name"),
                                                         tr("Additional metadata paths"),
                                                         tr("This sets any additional data paths to include in the final "
                                                            "summary output.  Because these are looked up from metadata values "
                                                            "they will usually begin with a \"^\" indicating a metadata "
                                                            "path"), QString()));


    TimeIntervalSelectionOption *interval = new TimeIntervalSelectionOption(tr("interval", "name"),
                                                                            tr("The calculation granularity"),
                                                                            tr("This is the interval that continuous parameters are calculated "
                                                                               "at.  For example, all data are binned together at this interval "
                                                                               "when calculating coverage.  This will also set the display "
                                                                               "interval if none is explicitly set."),
                                                                            tr("One day",
                                                                               "default interval"));
    interval->setAllowZero(false);
    interval->setAllowNegative(false);
    interval->setAllowUndefined(false);
    interval->setDefaultAligned(true);
    options.add("interval", interval);

    TimeIntervalSelectionOption *displayInt =
            new TimeIntervalSelectionOption(tr("display", "name"), tr("The display interval"),
                                            tr("This is the interval that data are displayed at.  For example "
                                               "this sets the interval between lines in CSV report mode or "
                                               "the time in each symbol on a timeline.  When not set it defaults "
                                               "to the calculation interval for reports or a fixed amount of "
                                               "time determined by the timeline size."),
                                            tr("Automatic", "default display interval"));
    displayInt->setAllowZero(false);
    displayInt->setAllowNegative(false);
    displayInt->setAllowUndefined(false);
    displayInt->setDefaultAligned(true);
    options.add("display", displayInt);


    TimeIntervalSelectionOption *gapInterval = new TimeIntervalSelectionOption(tr("gap", "name"),
                                                                               tr("The minimum amount of time required for gaps"),
                                                                               tr("This is the interval between an event representing absence and "
                                                                                  "the next valid one before anything is displayed.  That is, this "
                                                                                  "is them minimum amount of time something must be absent for "
                                                                                  "before the absence is reported."),
                                                                               tr("One day",
                                                                                  "default interval"));
    gapInterval->setAllowZero(true);
    gapInterval->setAllowNegative(false);
    gapInterval->setAllowUndefined(true);
    gapInterval->setDefaultAligned(false);
    options.add("gap", gapInterval);


    return options;
}

QList<ComponentExample> SummaryTimelineComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Instrument and coverage information")));

    options = getOptions();
    qobject_cast<TimeIntervalSelectionOption *>(options.get("interval"))->set(Time::Quarter, 1,
                                                                              true);
    qobject_cast<ComponentOptionEnum *>(options.get("timeline"))->set("vertical");
    examples.append(
            ComponentExample(options, tr("Vertical instrument information at quarter resolution")));

    options = getOptions();
    qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("cover");
    qobject_cast<ComponentOptionBoolean *>(options.get("show-interval"))->set(true);
    examples.append(ComponentExample(options, tr("Coverage and data interval report")));

    options = getOptions();
    qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("changes");
    qobject_cast<ComponentOptionBoolean *>(options.get("instrument-events"))->set(false);
    qobject_cast<ComponentOptionBoolean *>(options.get("wavelength-events"))->set(true);
    examples.append(ComponentExample(options, tr("Wavelength change events")));

    return examples;
}

ExternalSink *SummaryTimelineComponent::createDataSink(std::unique_ptr<
        IO::Generic::Stream> &&stream, const ComponentOptions &options)
{ return new SummaryTimeline(std::move(stream), options); }
