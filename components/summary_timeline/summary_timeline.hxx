/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef SUMMARYTIMELINE_H
#define SUMMARYTIMELINE_H

#include "core/first.hxx"

#include <vector>
#include <mutex>
#include <memory>
#include <condition_variable>
#include <QtGlobal>
#include <QObject>
#include <QIODevice>
#include <QString>
#include <QList>
#include <QStringList>

#include "core/component.hxx"
#include "core/timeutils.hxx"
#include "datacore/externalsink.hxx"
#include "datacore/dynamictimeinterval.hxx"

class TimelineAction {
public:
    TimelineAction();

    virtual ~TimelineAction();

    virtual QString displayData() const = 0;

    virtual QString typeDescription() const;

    virtual bool isPresent() const;
};

typedef std::shared_ptr<TimelineAction> TimelineActionPointer;

struct TimelinePoint : public CPD3::Time::Bounds {
    TimelinePoint();

    std::vector<TimelineActionPointer> actions;
    double fraction;
};

struct TimelineEvent {
    TimelineEvent();

    TimelineEvent(double time, const TimelineActionPointer &action);

    double time;
    TimelineActionPointer action;
};


class TimelineTarget {
public:
    TimelineTarget();

    virtual ~TimelineTarget();

    virtual void incomingData(const CPD3::Data::SequenceValue &value) = 0;

    virtual void endData();

    virtual bool registerInput(const CPD3::Data::SequenceName &unit);

    virtual bool registerExternal(const CPD3::Data::SequenceName &unit);

    virtual void fillPoint(TimelinePoint &point);

    virtual QList<TimelineEvent> getEvents(double dataStart, double dataEnd);

    virtual QList<TimelineEvent> getActive(double dataEnd);
};

class TimelineTrace {
    QList<TimelineTarget *> targets;
public:
    TimelineTrace();

    virtual ~TimelineTrace();

    virtual bool sortLessThan(const TimelineTrace *other) const = 0;

    virtual QStringList nameComponents() const = 0;

    virtual bool primaryInput(const CPD3::Data::SequenceName &unit) const = 0;

    virtual std::vector<TimelineTarget *> registerInput(const CPD3::Data::SequenceName &unit,
                                                        bool external = false);

    virtual void endData();

    virtual void addTarget(TimelineTarget *t);

    virtual void fillPoint(TimelinePoint &point);

    virtual QList<TimelineEvent> getEvents(double dataStart, double dataEnd);

    virtual QList<TimelineEvent> getActive(double dataEnd);
};

class SummaryTimeline : public CPD3::Data::ExternalSink {
    friend class SummaryTimelineComponent;

    std::unique_ptr<CPD3::IO::Generic::Stream> stream;

    std::mutex mutex;
    std::condition_variable notify;
    CPD3::Data::SequenceValue::Transfer pendingProcessing;
    bool terminated;
    bool dataEnded;
    bool threadFinished;

    CPD3::Data::SequenceName::Map<std::vector<TimelineTarget *>> dispatch;
    std::vector<std::unique_ptr<TimelineTrace>> traces;

    double firstDataStart;
    double lastDataEnd;
    double firstMetadataStart;
    double lastMetadataEnd;

    std::unique_ptr<CPD3::Data::DynamicTimeInterval> interval;
    std::unique_ptr<CPD3::Data::DynamicTimeInterval> displayInterval;
    std::unique_ptr<CPD3::Data::DynamicTimeInterval> gapInterval;

    enum TraceMode {
        TraceMode_Variable, TraceMode_FlattenFlavors, TraceMode_Instrument,
    };
    TraceMode traceMode;

    enum TimelineMode {
        TimelineMode_Disabled,
        TimelineMode_HorizontalTimeline,
        TimelineMode_VerticalTimeline,
        TimelineMode_Report,
    };
    TimelineMode timelineMode;
    bool displayTimelineLegend;

    enum EventMode {
        EventMode_Disabled, EventMode_Standard, EventMode_Presence, EventMode_CSV,
    };
    EventMode eventMode;

    enum ActiveMode {
        ActiveMode_Disabled, ActiveMode_Standard, ActiveMode_LatestTime,
    };
    ActiveMode activeMode;

    bool enableCoverage;
    bool enableInterval;
    int enableInstrument;
    int enableWavelength;
    int enableExists;

    int enableAdditionalData;
    QStringList additionalData;
    int enableAdditionalMetadata;
    QStringList additionalMetadata;


    QList<CPD3::Time::Bounds> timelinePoints;
    QList<QList<QChar> > timelineSymbols;

    struct TimelineLegend {
        QChar symbol;
        QString description;

        inline TimelineLegend(const QChar &s, const QString &desc) : symbol(s), description(desc)
        { }
    };

    QList<TimelineLegend> timelineLegend;

    std::thread thread;

    void createTraceTargets(TimelineTrace *trace);

    void dispatchData(const CPD3::Data::SequenceValue::Transfer &values);

    void generateOutput();

    QStringList buildTraceTitles();

    void outputReportTimeline(double startTime, double endTime);

    void buildTimeline(double startTime, double endTime, int desiredBlocks);

    void outputTimelineLegend();

    void outputVerticalTimeline(double startTime, double endTime);

    void outputHorizontalTimeline(double startTime, double endTime);

    void outputEvents(double startTime, double endTime);

    void outputEventsPresence(double startTime, double endTime);

    void outputEventsCSV(double startTime, double endTime);

    void outputActive(double endTime);

    void stall(std::unique_lock<std::mutex> &lock);

    void run();

public:
    SummaryTimeline(std::unique_ptr<CPD3::IO::Generic::Stream> &&stream,
                    const CPD3::ComponentOptions &options);

    virtual ~SummaryTimeline();

    void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

    void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

    void incomingData(const CPD3::Data::SequenceValue &value) override;

    void incomingData(CPD3::Data::SequenceValue &&value) override;

    void endData() override;

    void signalTerminate() override;

    void start() override;

    bool isFinished() override;

    bool wait(double timeout = CPD3::FP::undefined()) override;
};

class SummaryTimelineComponent : public QObject, virtual public CPD3::Data::ExternalSinkComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalSinkComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.summary_timeline"
                              FILE
                              "summary_timeline.json")
public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    CPD3::Data::ExternalSink *createDataSink(std::unique_ptr<
            CPD3::IO::Generic::Stream> &&stream = {},
                                             const CPD3::ComponentOptions &options = {}) override;
};


#endif
