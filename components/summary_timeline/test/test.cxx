/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QBuffer>

#include "datacore/externalsink.hxx"
#include "core/component.hxx"
#include "core/csv.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamictimeinterval.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class TestComponent : public QObject {
Q_OBJECT

    ExternalSinkComponent *component;

    static QStringList convertLines(const Util::ByteView &input)
    {
        Util::ByteView::LineIterator it(input);
        QStringList result;
        while (auto line = it.next()) {
            result += QString::fromStdString(line.toString());
        }
        return result;
    }

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component =
                qobject_cast<ExternalSinkComponent *>(ComponentLoader::create("summary_timeline"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionEnum *>(options.get("mode")));
        QVERIFY(qobject_cast<ComponentOptionEnum *>(options.get("breakdown")));
        QVERIFY(qobject_cast<ComponentOptionEnum *>(options.get("timeline")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("legend")));
        QVERIFY(qobject_cast<ComponentOptionEnum *>(options.get("events")));
        QVERIFY(qobject_cast<ComponentOptionEnum *>(options.get("active")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("cover")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("show-interval")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("instrument-events")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("instrument-active")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("wavelength-events")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("wavelength-active")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("presence-events")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("presence-active")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("data-events")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("data-active")));
        QVERIFY(qobject_cast<ComponentOptionStringSet *>(options.get("data")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("metadata-events")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("metadata-active")));
        QVERIFY(qobject_cast<ComponentOptionStringSet *>(options.get("metadata")));
    }

    void summaryDefaults()
    {
        QByteArray data;
        QBuffer device(&data);
        device.open(QIODevice::WriteOnly);
        ComponentOptions options(component->getOptions());
        ExternalSink *exp = component->createDataEgress(&device, options);
        QVERIFY(exp != NULL);

        Variant::Root meta1;
        meta1.write().metadataReal("Source").hash("Manufacturer").setString("TSI");

        Variant::Root meta2;
        meta2.write().metadataReal("Source").hash("Manufacturer").setString("BMI");

        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw_meta", "BsG_S11"), meta1, 1388534400.0,
                              1420070400.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw_meta", "N_N11"), meta1, 1388534400.0,
                              1398902400.0));

        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(1.0),
                                        1388534400.0,
                                        1393632000.0));
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsG_S11"), Variant::Root(2.0),
                                        1393632000.0,
                                        1420070400.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw", "BsG_S11", {"cover"}), Variant::Root(0.8),
                              1393632000.0, 1420070400.0));

        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw_meta", "N_N11"), meta2, 1398902400.0,
                              1420070400.0));

        exp->endData();
        exp->start();
        Threading::pollInEventLoop([&] { return !exp->isFinished(); });
        exp->wait();
        delete exp;

        QStringList check(convertLines(data));

        QCOMPARE(check, QStringList() <<
                "* - Coverage > 90%" <<
                "0 - Coverage > 50%" <<
                "o - Coverage > 25%" <<
                ". - Data present" <<
                "@ - Instrument changed" <<
                "    2014-01-01    2014-04-05         2014-07-07        2014-10-02    2015-01-01" <<
                "N11 @.......................@.................................................." <<
                "S11 @***********000000000000000000000000000000000000000000000000000000000000000" <<
                "Changes:" <<
                "    2014-01-01T00:00:00Z N11 TSI" <<
                "    2014-01-01T00:00:00Z S11 TSI" <<
                "    2014-05-01T00:00:00Z N11 BMI" <<
                "Latest:" <<
                "    N11 BMI" <<
                "    S11 TSI");
    }

    void verticalWavelength()
    {
        QByteArray data;
        QBuffer device(&data);
        device.open(QIODevice::WriteOnly);
        ComponentOptions options(component->getOptions());
        qobject_cast<TimeIntervalSelectionOption *>(options.get("interval"))->set(Time::Month, 1,
                                                                                  true);
        qobject_cast<ComponentOptionEnum *>(options.get("timeline"))->set("vertical");
        qobject_cast<ComponentOptionEnum *>(options.get("breakdown"))->set("variable");
        qobject_cast<ComponentOptionBoolean *>(options.get("legend"))->set(false);
        qobject_cast<ComponentOptionBoolean *>(options.get("instrument-events"))->set(false);
        qobject_cast<ComponentOptionBoolean *>(options.get("wavelength-events"))->set(true);
        qobject_cast<ComponentOptionEnum *>(options.get("active"))->set("none");
        qobject_cast<ComponentOptionEnum *>(options.get("events"))->set("none");
        ExternalSink *exp = component->createDataEgress(&device, options);
        QVERIFY(exp != NULL);

        Variant::Root meta1;
        meta1.write().metadataReal("Wavelength").setDouble(550.0);

        Variant::Root meta2;
        meta2.write().metadataReal("Wavelength").setDouble(567.0);

        exp->start();
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw_meta", "BsG_S11"), meta1, 1388534400.0,
                              1414368000.0));
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsR_S11"), Variant::Root(3.0),
                                        1388534400.0,
                                        1420070400.0));
        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw_meta", "BsG_S11"), meta2, 1414368000.0,
                              1420070400.0));

        exp->endData();
        Threading::pollInEventLoop([&] { return !exp->isFinished(); });
        exp->wait();
        delete exp;

        QStringList check(convertLines(data));

        QCOMPARE(check, QStringList() <<
                "        BsG_S11 BsR_S11" <<
                "2014-01    @       *" <<
                "2014-02    .       *" <<
                "2014-03    .       *" <<
                "2014-04    .       *" <<
                "2014-05    .       *" <<
                "2014-06    .       *" <<
                "2014-07    .       *" <<
                "2014-08    .       *" <<
                "2014-09    .       *" <<
                "2014-10    @       *" <<
                "2014-11    .       *" <<
                "2014-12    .       *");
    }

    void reportExtended()
    {
        QByteArray data;
        QBuffer device(&data);
        device.open(QIODevice::WriteOnly);
        ComponentOptions options(component->getOptions());
        qobject_cast<TimeIntervalSelectionOption *>(options.get("interval"))->set(Time::Quarter, 1,
                                                                                  true);
        qobject_cast<ComponentOptionEnum *>(options.get("mode"))->set("cover");
        qobject_cast<ComponentOptionBoolean *>(options.get("show-interval"))->set(true);
        qobject_cast<ComponentOptionBoolean *>(options.get("instrument-active"))->set(true);
        qobject_cast<ComponentOptionBoolean *>(options.get("data-active"))->set(true);
        qobject_cast<ComponentOptionBoolean *>(options.get("metadata-active"))->set(true);
        qobject_cast<ComponentOptionStringSet *>(options.get("data"))->add("DataPath");
        qobject_cast<ComponentOptionStringSet *>(options.get("metadata"))->add("^MetadataPath");
        ExternalSink *exp = component->createDataEgress(&device, options);
        QVERIFY(exp != NULL);

        Variant::Root meta1;
        meta1.write().metadataReal("MetadataPath").setString("M1");
        meta1.write().metadataReal("Source").hash("Manufacturer").setString("TSI");
        Variant::Root meta2;
        meta2.write().metadataReal("MetadataPath").setString("M2");
        meta2.write().metadataReal("Source").hash("Manufacturer").setString("BMI");

        Variant::Root data1;
        data1.write().hash("DataPath").setString("D1");
        Variant::Root data2;
        data2.write().hash("DataPath").setString("D2");

        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw_meta", "BsG_S11"), meta1, 1356998400.0,
                              1420070400.0));

        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsG_S11"), data1, 1356998400.0,
                                        1356998460.0));
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsG_S11"), data1, 1356998460.0,
                                        1356998520.0));
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BsG_S11"), data1, 1356998520.0,
                                        1360886400));

        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw_meta", "BaG_A11"), meta1, 1364860800.0,
                              1388534400.0));

        exp->incomingData(
                SequenceValue(SequenceName("brw", "raw_meta", "BaG_A11"), meta2, 1388534400.0,
                              1420070400.0));
        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BaG_A11"), data2, 1388534400.0,
                                        1388534460.0));

        exp->incomingData(SequenceValue(SequenceName("brw", "raw", "BaG_A11"), data2, 1419984000.0,
                                        1419984060.0));


        exp->start();
        exp->endData();
        Threading::pollInEventLoop([&] { return !exp->isFinished(); });
        QVERIFY(exp->wait());
        delete exp;

        QStringList check(convertLines(data));

        QCOMPARE(check, QStringList() <<
                "DateTime,BsG_S11,BaG_A11" <<
                "2013-01-01T00:00:00Z,0.500;60;TSI;D1;M1," <<
                "2013-04-01T00:00:00Z,0.000;0;TSI;M1,0.000;0;TSI;M1" <<
                "2013-07-01T00:00:00Z,0.000;0;TSI;M1,0.000;0;TSI;M1" <<
                "2013-10-01T00:00:00Z,0.000;0;TSI;M1,0.000;0;TSI;M1" <<
                "2014-01-01T00:00:00Z,0.000;0;TSI;M1,0.000;60;BMI;D2;M2" <<
                "2014-04-01T00:00:00Z,0.000;0;TSI;M1,0.000;0;BMI;M2" <<
                "2014-07-01T00:00:00Z,0.000;0;TSI;M1,0.000;0;BMI;M2" <<
                "2014-10-01T00:00:00Z,0.000;0;TSI;M1,0.000;60;BMI;D2;M2");
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
