/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QTemporaryFile>

#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "core/number.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/externalsource.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace CPD3 {
namespace Data {
namespace Variant {
bool operator==(const Root &a, const Root &b)
{ return a.read() == b.read(); }
}
}
}


class TestComponent : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

    ExternalSourceComponent *component;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component = qobject_cast<ExternalSourceComponent *>(
                ComponentLoader::create("email_newvalues"));
        QVERIFY(component);

        QVERIFY(databaseFile.open());
        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("profile")));
    }

    void basic()
    {
        Variant::Root v;
        v["NewValues/aerosol/Spancheck/Selection/Variable"].setString("ZSPANCHECK_S11");
        v["NewValues/aerosol/Spancheck/Output"].setString(
                "Spancheck at ${TIME} with ${EVENT|NUMBER|B/Results/#0/PCT|0.0} percent error");

        SequenceValue::Transfer toAdd;
        toAdd.push_back(SequenceValue({"sfa", "configuration", "email"}, v, FP::undefined(),
                                      FP::undefined()));

        v.write().setEmpty();
        v["B/Results/#0/PCT"].setDouble(3.1);
        toAdd.push_back(
                SequenceValue({"sfa", "raw", "ZSPANCHECK_S11"}, v, FP::undefined(), 1414686489));

        v.write().setEmpty();
        v["B/Results/#0/PCT"].setDouble(4.1);
        toAdd.push_back(
                SequenceValue({"sfa", "raw", "ZSPANCHECK_S11"}, v, 1414686489, FP::undefined()));

        v.write().setEmpty();
        v["B/Results/#0/PCT"].setDouble(5.1);
        toAdd.push_back(
                SequenceValue({"sgp", "raw", "ZSPANCHECK_S11"}, v, 1414686489, FP::undefined()));

        Archive::Access(databaseFile).writeSynchronous(toAdd);

        ComponentOptions options(component->getOptions());
        ExternalConverter
                *input = component->createExternalIngress(options, 1414627200, 1414713600);
        QVERIFY(input != NULL);
        input->start();

        StreamSink::Buffer ingress;
        input->setEgress(&ingress);

        QVERIFY(input->wait());
        delete input;

        QVERIFY(ingress.ended());
        std::vector<Variant::Root> outputValues;
        for (const auto &dv : ingress.values()) {
            outputValues.emplace_back(dv.root());
        }

        std::vector<Variant::Root> values;
        v.write().setEmpty();
        v["Type"].setString("Break");
        values.emplace_back(v);

        v.write().setEmpty();
        v["Type"].setString("Text");
        v["Text"].setString("Spancheck at 2014-10-30T16:28:09Z with 4.1 percent error");
        values.emplace_back(v);

        QCOMPARE(outputValues, values);
    }

    void scripting()
    {
        Variant::Root v;
        v["NewValues/aerosol/FilterChange/Selection/Variable"].setString("ZFILTER_A11");
        v["NewValues/aerosol/FilterChange/Read/#0/Variable"].setString("ZFILTER_A11");
        v["NewValues/aerosol/FilterChange/Read/#1/Variable"].setString("ZWHITE_A11");
        v["NewValues/aerosol/FilterChange/Output"].setString(R"EOF(Maximum white filter ratio ${<
local max = 0;
for spot, value in ipairs(data.ZFILTER_A11.InB:toArray()) do
    local ratio = (tonumber(value) or CPD3.undefined) /
        (tonumber(data.ZWHITE_A11.InB[spot]) or CPD3.undefined);
    ratio = math.abs(1.0 - ratio);
    if ratio > max then
        max = ratio
    end
end
return string.format("%.2f", max);
>})EOF");

        SequenceValue::Transfer toAdd;
        toAdd.push_back(SequenceValue({"sfa", "configuration", "email"}, v, FP::undefined(),
                                      FP::undefined()));

        v.write().setEmpty();
        v["InB/#0"].setDouble(0.5);
        v["InB/#1"].setDouble(1.25);
        v["InB/#2"].setDouble(1.0);
        toAdd.push_back(SequenceValue({"sfa", "raw", "ZWHITE_A11"}, v, 1388534400, 1414686489));

        v.write().setEmpty();
        v["InB/#0"].setDouble(0.5);
        v["InB/#1"].setDouble(1.0);
        v["InB/#2"].setDouble(0.9);
        toAdd.push_back(SequenceValue({"sfa", "raw", "ZFILTER_A11"}, v, 1388534400, 1414686489));

        Archive::Access(databaseFile).writeSynchronous(toAdd);

        ComponentOptions options(component->getOptions());
        ExternalConverter
                *input = component->createExternalIngress(options, 1388534400, 1414686489);
        QVERIFY(input != NULL);
        input->start();

        StreamSink::Buffer ingress;
        input->setEgress(&ingress);

        QVERIFY(input->wait());
        delete input;

        QVERIFY(ingress.ended());
        std::vector<Variant::Root> outputValues;
        for (const auto &dv : ingress.values()) {
            outputValues.emplace_back(dv.root());
        }

        std::vector<Variant::Root> values;
        v.write().setEmpty();
        v["Type"].setString("Break");
        values.emplace_back(v);

        v.write().setEmpty();
        v["Type"].setString("Text");
        v["Text"].setString("Maximum white filter ratio 0.20");
        values.emplace_back(v);

        QCOMPARE(outputValues, values);
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
