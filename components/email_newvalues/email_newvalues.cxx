/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLoggingCategory>
#include <luascript/libs/variant.hxx>

#include "core/component.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/valueoptionparse.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/segment.hxx"
#include "datacore/sequencematch.hxx"
#include "luascript/libs/sequencesegment.hxx"

#include "email_newvalues.hxx"


Q_LOGGING_CATEGORY(log_email_newvalues, "cpd3.email.newvalues", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;

EmailNewValues::EmailNewValues() : profile(SequenceName::impliedProfile()),
                                   stations(),
                                   start(FP::undefined()),
                                   end(FP::undefined()),
                                   substitutions(this)
{ }

EmailNewValues::EmailNewValues(const ComponentOptions &options,
                               double start,
                               double end,
                               const std::vector<SequenceName::Component> &setStations) : profile(
        SequenceName::impliedProfile()),
                                                                                          stations(
                                                                                                  setStations),
                                                                                          start(start),
                                                                                          end(end),
                                                                                          substitutions(
                                                                                                  this)
{
    if (options.isSet("profile")) {
        profile = qobject_cast<ComponentOptionSingleString *>(options.get("profile"))->get()
                                                                                     .toLower()
                                                                                     .toStdString();
    }
}

EmailNewValues::~EmailNewValues() = default;

void EmailNewValues::incomingData(const CPD3::Util::ByteView &)
{ externalNotify(); }

void EmailNewValues::endData()
{
    externalNotify();
}

EmailNewValues::Substitutions::Substitutions(EmailNewValues *p) : parent(p),
                                                                  reference(Variant::Read::empty())
{ }

EmailNewValues::Substitutions::~Substitutions() = default;

QString EmailNewValues::Substitutions::literalCheck(QStringRef &key) const
{
    if (key.length() < 1)
        return QString();
    if (key.at(0) == '<') {
        key = QStringRef(key.string(), key.position() + 1, key.length() - 1);
        return QString('>');
    }
    return TextSubstitution::literalCheck(key);
}

QString EmailNewValues::Substitutions::literalSubstitution(const QStringRef &key,
                                                           const QString &) const
{
    if (key.isEmpty())
        return {};
    if (!parent->scriptEngine) {
        parent->scriptEngine.reset(new Lua::Engine);
        {
            Lua::Engine::Frame root(*(parent->scriptEngine));
            Lua::Engine::Assign assign(root, root.global(), "print");
            assign.push(std::function<void(Lua::Engine::Entry &)>([](Lua::Engine::Entry &entry) {
                for (std::size_t i = 0, max = entry.size(); i < max; i++) {
                    qCInfo(log_email_newvalues) << entry[i].toOutputString();
                }
            }));
        }
    }

    Lua::Engine::Frame root(*(parent->scriptEngine));

    {
        auto all = evalulateAll();
        for (auto var = all.constBegin(), endVar = all.constEnd(); var != endVar; ++var) {
            Lua::Engine::Assign assign(root, root.global(), var.key());
            assign.push(var.value());
        }
    }
    {
        Lua::Engine::Assign assign(root, root.global(), "data");
        assign.pushData<Lua::Libs::SequenceSegment>(segment);
    }
    {
        Lua::Engine::Assign assign(root, root.global(), "event");
        assign.pushData<Lua::Libs::Variant>(Variant::Root(reference).write());
    }

    Lua::Engine::Call call(root);
    if (!call.pushChunk(key.toString().toStdString(), false)) {
#ifndef NDEBUG
        auto error = parent->scriptEngine->errorDescription();
        parent->scriptEngine->clearError();
        return QString::fromStdString("_SCRIPT_PARSE_ERROR(" + error + ")");
#else
        parent->scriptEngine->clearError();
        return "_SCRIPT_PARSE_ERROR";
#endif
    }

    if (!call.execute(1)) {
#ifndef NDEBUG
        auto error = parent->scriptEngine->errorDescription();
        parent->scriptEngine->clearError();
        return QString::fromStdString("_SCRIPT_EXECUTE_ERROR(" + error + ")");
#else
        parent->scriptEngine->clearError();
        return "_SCRIPT_EXECUTE_ERROR";
#endif
    }

    return call.front().toQString();
}


void EmailNewValues::outputValues(const Variant::Read &config,
                                  const SequenceValue::Transfer &values,
                                  const SequenceSegment::Transfer &segments)
{
    bool isFirst = true;
    for (const auto &event : values) {
        QString output(config["Output"].toQString());
        if (output.isEmpty())
            continue;

        QHash<QString, QString> variables;
        variables.insert("station", event.getName().getStationQString().toLower());
        variables.insert("archive", event.getName().getArchiveQString().toLower());
        variables.insert("variable", event.getName().getVariableQString());

        substitutions.push();
        substitutions.setTime("time", event.getStart());
        substitutions.setTime("start", event.getStart());
        substitutions.setTime("end", event.getEnd());
        substitutions.setString("station", event.getName().getStationQString().toLower());
        substitutions.setString("archive", event.getName().getArchiveQString().toLower());
        substitutions.setString("variable", event.getName().getVariableQString());
        substitutions.setVariant("event", event.read());
        {
            QStringList flavors = Util::to_qstringlist(event.getFlavors());
            std::sort(flavors.begin(), flavors.end());
            substitutions.setString("flavors", flavors.join(",").toLower());
        }

        substitutions.reference = event.read();
        substitutions.reference.detachFromRoot();
        auto seg = Range::findIntersecting(segments, event);
        if (seg != segments.end())
            substitutions.segment = *seg;

        output = substitutions.apply(output);

        substitutions.pop();
        substitutions.reference = Variant::Read::empty();
        substitutions.segment = SequenceSegment();

        if (output.isEmpty())
            continue;

        if (isFirst) {
            isFirst = false;
            if (messages.empty() || config["ForceBreak"].toBool()) {
                Variant::Root addBreak(config["Message"]);
                addBreak["Type"].setString("Break");
                messages.emplace_back(std::move(addBreak));
            }
        } else if (!messages.empty() && config["AlwaysBreak"].toBool()) {
            Variant::Root addBreak(config["Message"]);
            addBreak["Type"].setString("Break");
            messages.emplace_back(std::move(addBreak));
        }

        Variant::Root addMessage(config["Message"]);
        addMessage["Type"].setString("Text");
        addMessage["Text"].setString(output);
        messages.emplace_back(std::move(addMessage));
    }
}

namespace {
struct ProcessingData {
    SequenceName::Component station;
    Variant::Read config;
    SequenceValue::Transfer values;
    SequenceSegment::Transfer segments;
};
}

bool EmailNewValues::begin()
{
    QList<ProcessingData> processing;

    do {
        Archive::Access access;
        Archive::Access::ReadLock lock(access, true);

        auto allStations = access.availableStations(stations);
        if (allStations.empty()) {
            qCDebug(log_email_newvalues) << "No stations available";
            return false;
        }
        stations.clear();
        std::copy(allStations.begin(), allStations.end(), Util::back_emplacer(stations));

        double tnow = Time::time();
        SequenceSegment configSegment;
        {
            auto confList = SequenceSegment::Stream::read(
                    Archive::Selection(tnow - 1.0, tnow + 1.0, stations, {"configuration"},
                                       {"email"}), &access);
            auto f = Range::findIntersecting(confList, tnow);
            if (f == confList.end())
                break;
            configSegment = *f;
        }

        int totalValues = 0;
        for (const auto &station : stations) {
            SequenceName stationUnit(station, "configuration", "email");
            auto base = configSegment.takeValue(stationUnit).read().hash("NewValues").hash(profile);
            if (!base.exists())
                continue;
            base.detachFromRoot();

            for (auto config : base.toChildren()) {
                if (isTerminated())
                    break;

                ProcessingData data;
                SequenceMatch::Composite selection(config["Selection"], {QString::fromStdString(station)}, {"raw"}, {});
                if (!config.hash("Read").exists()) {
                    auto selections = selection.toArchiveSelections();
                    for (auto &mod : selections) {
                        mod.start = start;
                        mod.end = end;
                        mod.includeDefaultStation = false;
                        mod.includeMetaArchive = false;
                    }
                    data.values = access.readSynchronous(selections);
                } else {
                    auto selections = SequenceMatch::Composite(config["Read"], {QString::fromStdString(station)}, {"raw"},
                                                               {}).toArchiveSelections();
                    for (auto &mod : selections) {
                        mod.start = start;
                        mod.end = end;
                        mod.includeDefaultStation = false;
                        mod.includeMetaArchive = false;
                    }
                    data.values = access.readSynchronous(selections);
                }

                SequenceSegment::Transfer segments;
                SequenceSegment::Stream segmentReader;
                for (auto check = data.values.begin(); check != data.values.end();) {
                    Util::append(segmentReader.add(*check), segments);
                    if (!selection.matches(check->getUnit())) {
                        check = data.values.erase(check);
                        continue;
                    }
                    if (FP::defined(start) &&
                            (!FP::defined(check->getStart()) || check->getStart() < start)) {
                        check = data.values.erase(check);
                        continue;
                    }
                    if (FP::defined(end) &&
                            FP::defined(check->getStart()) &&
                            check->getStart() >= end) {
                        check = data.values.erase(check);
                        continue;
                    }
                    ++check;
                }

                if (data.values.empty())
                    continue;
                Util::append(segmentReader.finish(), segments);

                data.station = station;
                data.config = config;
                data.segments = segments;

                if (config["LatestOnly"].toBool()) {
                    data.values = SequenceValue::Transfer{std::move(data.values.back())};
                }

                processing.append(data);
                totalValues += data.values.size();
            }
        }

        if (isTerminated())
            break;

        qCDebug(log_email_newvalues) << "Loaded" << totalValues << "values(s)";
    } while (false);

    if (isTerminated())
        return false;

    substitutions.clear();
    substitutions.setTime("globalstart", start);
    substitutions.setTime("globalend", end);

    for (QList<ProcessingData>::const_iterator data = processing.constBegin(),
            end = processing.constEnd(); data != end; ++data) {
        TextSubstitutionStack::Context subctx(substitutions);
        substitutions.setString("processstation", QString::fromStdString(data->station).toLower());

        outputValues(data->config, data->values, data->segments);
    }

    return true;
}

bool EmailNewValues::prepare()
{ return true; }

bool EmailNewValues::process()
{
    SequenceValue::Transfer toOutput;
    double tnow = Time::time();
    qCDebug(log_email_newvalues) << "Generated" << messages.size() << "message(s)";
    int priority = 0;
    for (auto &v : messages) {
        toOutput.emplace_back(
                SequenceIdentity({"message", "message", "message"}, tnow, tnow, priority++),
                std::move(v));
    }

    messages.clear();
    outputData(toOutput);
    return false;
}


ComponentOptions EmailNewValuesComponent::getOptions()
{
    ComponentOptions options;

    options.add("profile",
                new ComponentOptionSingleString(tr("profile", "name"), tr("Execution profile"),
                                                tr("This is the profile name to execute.  Multiple profiles can be "
                                                   "defined to allow for different output types for a single "
                                                   "station.  Consult the station configuration for available profiles."),
                                                tr("aerosol")));

    return options;
}

QList<ComponentExample> EmailNewValuesComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(
            ComponentExample(options, tr("Generate output for new values", "default example name"),
                             tr("This will generate the new value outputs for the \"aerosol\" "
                                "profile.")));

    return examples;
}

int EmailNewValuesComponent::ingressAllowStations()
{ return INT_MAX; }

bool EmailNewValuesComponent::ingressRequiresTime()
{ return true; }

ExternalConverter *EmailNewValuesComponent::createExternalIngress(const ComponentOptions &options,
                                                                  double start,
                                                                  double end,
                                                                  const std::vector<
                                                                          SequenceName::Component> &stations)
{ return new EmailNewValues(options, start, end, stations); }
