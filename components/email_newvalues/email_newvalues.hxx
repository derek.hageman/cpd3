/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef EMAILNEWVALUES_H
#define EMAILNEWVALUES_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QList>
#include <QStringList>

#include "core/component.hxx"
#include "core/textsubstitution.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/segment.hxx"
#include "datacore/variant/composite.hxx"
#include "luascript/engine.hxx"

class EmailNewValues : public CPD3::Data::ExternalSourceProcessor {
    std::string profile;
    std::vector<CPD3::Data::SequenceName::Component> stations;
    double start;
    double end;

    std::unique_ptr<CPD3::Lua::Engine> scriptEngine;

    class Substitutions : public CPD3::Data::Variant::Composite::SubstitutionStack {
        EmailNewValues *parent;
    public:
        CPD3::Data::SequenceSegment segment;
        CPD3::Data::Variant::Read reference;

        Substitutions(EmailNewValues *parent);

        virtual ~Substitutions();

    protected:
        virtual QString literalCheck(QStringRef &key) const;

        virtual QString literalSubstitution(const QStringRef &key, const QString &close) const;
    };

    friend class Substitutions;

    Substitutions substitutions;

    std::vector<CPD3::Data::Variant::Root> messages;

    void outputValues(const CPD3::Data::Variant::Read &config,
                      const CPD3::Data::SequenceValue::Transfer &values,
                      const CPD3::Data::SequenceSegment::Transfer &segments);

public:
    EmailNewValues();

    EmailNewValues(const CPD3::ComponentOptions &options,
                   double start,
                   double end,
                   const std::vector<CPD3::Data::SequenceName::Component> &setStations = {});

    virtual ~EmailNewValues();

    void incomingData(const CPD3::Util::ByteView &data) override;

    void endData() override;

protected:
    bool prepare() override;

    bool process() override;

    bool begin() override;
};

class EmailNewValuesComponent : public QObject, virtual public CPD3::Data::ExternalSourceComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalSourceComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.email_newvalues"
                              FILE
                              "email_newvalues.json")

public:
    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual int ingressAllowStations();

    virtual bool ingressRequiresTime();

    virtual CPD3::Data::ExternalConverter *createExternalIngress(const CPD3::ComponentOptions &options,
                                                                 double start,
                                                                 double end,
                                                                 const std::vector<CPD3::Data::SequenceName::Component> &stations = {});
};

#endif
