/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QTemporaryFile>

#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "core/actioncomponent.hxx"
#include "core/number.hxx"
#include "database/runlock.hxx"
#include "database/storage.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/archive/structure.hxx"
#include "datacore/archive/stringindex.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class TestComponent : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

    ActionComponent *component;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component = qobject_cast<ActionComponent *>(ComponentLoader::create("maintenance_archive"));
        QVERIFY(component);

        QVERIFY(databaseFile.open());
        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());
    }

    void init()
    {
        databaseFile.reset();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("cleanup-stale")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("remove-unused")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("optimize-database")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("recompress")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("split-station")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("split-archive")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("split-variable")));
        QVERIFY(qobject_cast<ComponentOptionTimeOffset *>(options.get("recompress-age")));
    }

    void basic()
    {

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0)),
                SequenceValue({"bnd", "raw", "BsB_S11"}, Variant::Root(1.0))});
        Archive::Access(databaseFile).writeSynchronous(ArchiveErasure::Transfer{
                ArchiveErasure({"bnd", "raw", "BsB_S11"}, FP::undefined())});

        {
            Database::RunLock rl(Database::Storage::sqlite(databaseFile.fileName().toStdString()));
            rl.acquire("foobar");
            rl.release();
        }

        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionBoolean *>(options.get("cleanup-stale"))->set(true);
        qobject_cast<ComponentOptionBoolean *>(options.get("remove-unused"))->set(true);
        qobject_cast<ComponentOptionBoolean *>(options.get("optimize-database"))->set(true);
        qobject_cast<ComponentOptionBoolean *>(options.get("recompress"))->set(true);
        CPD3Action *action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        {
            auto values = Archive::Access(databaseFile).readSynchronous(Archive::Selection());
            QCOMPARE((int) values.size(), 1);
            QCOMPARE(values.front().getUnit(), SequenceName("bnd", "raw", "BsG_S11"));
        }
        {
            Database::Connection
                    db(Database::Storage::sqlite(databaseFile.fileName().toStdString()));
            QVERIFY(db.start());
            auto trans = db.transaction();
            Archive::IdentityIndex index(&db);
            auto check = index.variable.lookup();
            QCOMPARE((int) check.size(), 1);
            QCOMPARE(check.front().second, std::string("BsG_S11"));
        }

        QTest::qSleep(500);

        options = component->getOptions();
        qobject_cast<ComponentOptionBoolean *>(options.get("cleanup-stale"))->set(true);
        qobject_cast<ComponentOptionBoolean *>(options.get("remove-unused"))->set(true);
        qobject_cast<ComponentOptionBoolean *>(options.get("optimize-database"))->set(true);
        qobject_cast<ComponentOptionBoolean *>(options.get("recompress"))->set(true);
        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;
    }

    void resplit()
    {
        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"bnd", "raw", "BsG_S11"}, Variant::Root(1.0)),
                SequenceValue({"bnd", "raw", "BsB_S11"}, Variant::Root(1.0))});

        {
            Database::Connection
                    db(Database::Storage::sqlite(databaseFile.fileName().toStdString()));
            QVERIFY(db.start());
            auto trans = db.transaction();
            QVERIFY(db.hasTable(Archive::Structure::Existing::Table_Prefix));
        }

        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionBoolean *>(options.get("split-station"))->set(true);
        qobject_cast<ComponentOptionBoolean *>(options.get("split-archive"))->set(true);
        qobject_cast<ComponentOptionBoolean *>(options.get("split-variable"))->set(true);
        CPD3Action *action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        {
            Database::Connection
                    db(Database::Storage::sqlite(databaseFile.fileName().toStdString()));
            QVERIFY(db.start());
            auto trans = db.transaction();
            QVERIFY(!db.hasTable(Archive::Structure::Existing::Table_Prefix));
            std::string table = Archive::Structure::Existing::Table_Prefix;
            table += "_s1_a1_v1";
            QVERIFY(db.hasTable(table));
        }
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
