/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <future>
#include <QLoggingCategory>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "database/runlock.hxx"
#include "database/storage.hxx"
#include "database/driver.hxx"
#include "database/util.hxx"
#include "database/connection.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/archive/globaloperation.hxx"
#include "datacore/archive/resplitoperation.hxx"
#include "datacore/archive/stringindex.hxx"
#include "datacore/archive/structure.hxx"

#include "maintenance_archive.hxx"


Q_LOGGING_CATEGORY(log_component_maintenance_archive, "cpd3.component.maintenance.archive",
                   QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;

MaintenanceArchive::MaintenanceArchive()
{ Q_ASSERT(false); }

MaintenanceArchive::MaintenanceArchive(const ComponentOptions &options)
        : mutex(),
          terminated(false),
          cleanupStale(true),
          purgeUnused(false),
          resplitAny(false),
          resplitStation(false),
          resplitArchive(false),
          resplitVariable(false),
          optimize(false),
          recompress(false),
          recompressUnit(Time::Month),
          recompressCount(6),
          recompressAlign(false)
{
    if (options.isSet("cleanup-stale")) {
        cleanupStale = qobject_cast<ComponentOptionBoolean *>(options.get("cleanup-stale"))->get();
    }
    if (options.isSet("remove-unused")) {
        purgeUnused = qobject_cast<ComponentOptionBoolean *>(options.get("remove-unused"))->get();
    }
    if (options.isSet("optimize-database")) {
        optimize = qobject_cast<ComponentOptionBoolean *>(options.get("optimize-database"))->get();
    }
    if (options.isSet("recompress")) {
        recompress = qobject_cast<ComponentOptionBoolean *>(options.get("recompress"))->get();
    }
    if (options.isSet("split-station")) {
        resplitAny = true;
        resplitStation =
                qobject_cast<ComponentOptionBoolean *>(options.get("split-station"))->get();
    }
    if (options.isSet("split-archive")) {
        resplitAny = true;
        resplitArchive =
                qobject_cast<ComponentOptionBoolean *>(options.get("split-archive"))->get();
    }
    if (options.isSet("split-variable")) {
        resplitAny = true;
        resplitVariable =
                qobject_cast<ComponentOptionBoolean *>(options.get("split-variable"))->get();
    }

    if (options.isSet("recompress-age")) {
        recompressUnit =
                qobject_cast<ComponentOptionTimeOffset *>(options.get("recompress-age"))->getUnit();
        recompressCount = qobject_cast<ComponentOptionTimeOffset *>(
                options.get("recompress-age"))->getCount();
        recompressAlign = qobject_cast<ComponentOptionTimeOffset *>(
                options.get("recompress-age"))->getAlign();
    }
}

MaintenanceArchive::~MaintenanceArchive()
{
}

void MaintenanceArchive::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    terminateRequested();
}

bool MaintenanceArchive::testTerminated()
{
    std::lock_guard<std::mutex> lock(mutex);
    return terminated;
}

void MaintenanceArchive::run()
{
    if (testTerminated())
        return;


    double startProcessing = Time::time();

    qCDebug(log_component_maintenance_archive) << "Starting maintenance";

    /* Create the archive and run lock tables */
    Archive::Access();
    Database::RunLock();

    if (cleanupStale) {
        qCDebug(log_component_maintenance_archive) << "Releasing stale locks";

        feedback.emitStage(tr("Releasing stale locks"),
                           tr("The system is removing all stale run locks."), false);

        Database::RunLock::cleanupStale();
    }

    if (testTerminated())
        return;

    if (purgeUnused) {
        qCDebug(log_component_maintenance_archive) << "Purging unused references";

        feedback.emitStage(tr("Removing unused references"),
                           tr("The system is removing all unused component references."), false);

        Database::Connection conn;
        if (!conn.start()) {
            qCDebug(log_component_maintenance_archive) << "Connection failed";
            feedback.emitFailure(tr("Connection failed"));
            return;
        }

        Threading::Receiver rxConn;
        terminateRequested.connect(rxConn,
                                   std::bind(&Database::Connection::signalTerminate, &conn));
        for (;;) {
            if (testTerminated())
                return;

            auto tx = conn.transaction(Database::Connection::Transaction_Serializable);

            Archive::Structure::Layout layout(&conn);

            Archive::FindUnusedIndexOperation::IndexContents unused;
            {
                Archive::IdentityIndex index(&conn);
                for (const auto &add : index.station.lookup()) {
                    unused.station.emplace(add.first);
                }
                for (const auto &add : index.archive.lookup()) {
                    unused.archive.emplace(add.first);
                }
                for (const auto &add : index.variable.lookup()) {
                    unused.variable.emplace(add.first);
                }
                for (const auto &add : index.flavor.lookup()) {
                    unused.flavor.emplace(add.first);
                }
            }

            Threading::Receiver rxOp;
            Archive::FindUnusedIndexOperation op(layout, unused);
            op.start();

            terminateRequested.connect(rxOp,
                                       std::bind(&Archive::RecompressOperation::signalTerminate,
                                                 &op));
            if (testTerminated())
                return;

            op.wait();
            if (testTerminated())
                return;

            for (const auto &rem : op.emptySequences()) {
                if (rem.station == 0 && rem.archive == 0 && rem.variable == 0)
                    continue;
                conn.removeTable(layout.sequenceTable(rem));
            }
            for (const auto &rem : op.emptyErasures()) {
                if (rem.station == 0 && rem.archive == 0 && rem.variable == 0)
                    continue;
                conn.removeTable(layout.erasureTable(rem));
            }

            unused = op.remainingContents();
            {
                Archive::IdentityIndex index(&conn);
                for (const auto &rem : unused.station) {
                    index.station.remove(rem);
                }
                for (const auto &rem : unused.archive) {
                    index.archive.remove(rem);
                }
                for (const auto &rem : unused.variable) {
                    index.variable.remove(rem);
                }
                for (const auto &rem : unused.flavor) {
                    index.flavor.remove(rem);
                }
            }

            if (tx.end())
                break;
        }

        conn.shutdown();
        conn.wait();
        rxConn.disconnect();
    }

    if (testTerminated())
        return;

    if (recompress) {
        Database::RunLock lock;

        static const quint8 rcVersion = 1;
        QString rcKey("maintenancearchive recompress");

        qCDebug(log_component_maintenance_archive) << "Locking for recompression";

        feedback.emitStage(tr("Locking for recompression"),
                           tr("The system is acquiring a lock for recompression of the archive."),
                           false);

        bool ok = false;
        lock.acquire(rcKey, FP::undefined(), &ok);
        if (!ok) {
            qCDebug(log_component_maintenance_archive) << "Recompression lock failed";
            feedback.emitFailure(tr("Lock failed"));
            return;
        }

        double start = FP::undefined();
        double end = lock.startTime();

        {
            QByteArray data(lock.get(rcKey));
            if (data.isEmpty()) {
                start = FP::undefined();
            } else {
                QDataStream stream(&data, QIODevice::ReadOnly);
                quint8 version = 0;
                stream >> version >> start;
                if (stream.status() != QDataStream::Ok || version != rcVersion) {
                    start = FP::undefined();
                    qCDebug(log_component_maintenance_archive) << "Invalid recompression data";
                }
            }
        }

        Q_ASSERT(FP::defined(end));
        end = Time::logical(end, recompressUnit, recompressCount, recompressAlign, false, -1);

        if (Range::compareStartEnd(start, end) >= 0) {
            qCDebug(log_component_maintenance_archive) << "Recompression skipped on"
                                                       << Logging::range(start, end)
                                                       << "because the interval is invalid";
            feedback.emitFailure(tr("Invalid time range"));
            lock.fail();
        } else {
            qCDebug(log_component_maintenance_archive) << "Starting recompression on"
                                                       << Logging::range(start, end);

            feedback.emitStage(tr("Recompressing data"),
                               tr("The system is recompressing data that has not been written to recently."),
                               false);

            Database::Connection conn;
            if (!conn.start()) {
                qCDebug(log_component_maintenance_archive) << "Connection failed";
                feedback.emitFailure(tr("Connection failed"));
                return;
            }
            Threading::Receiver rxConn;
            terminateRequested.connect(rxConn,
                                       std::bind(&Database::Connection::signalTerminate, &conn));
            for (;;) {
                if (testTerminated())
                    return;

                auto tx = conn.transaction(Database::Connection::Transaction_Serializable);

                Archive::Structure::Layout layout(&conn);
                Threading::Receiver rxOp;
                Archive::RecompressOperation op(layout, start, end);
                op.start();

                terminateRequested.connect(rxOp,
                                           std::bind(&Archive::RecompressOperation::signalTerminate,
                                                     &op));
                if (testTerminated())
                    return;

                op.wait();
                if (testTerminated())
                    return;

                if (tx.end())
                    break;
            }

            conn.shutdown();
            conn.wait();
            rxConn.disconnect();


            {
                QByteArray data;
                QDataStream stream(&data, QIODevice::WriteOnly);
                stream << rcVersion << end;
                lock.set(rcKey, data);
            }
            lock.release();
        }
    }

    if (testTerminated())
        return;

    if (resplitAny) {
        qCDebug(log_component_maintenance_archive).nospace() << "Starting resplit with station="
                                                             << resplitStation << " archive="
                                                             << resplitArchive << " variable="
                                                             << resplitVariable;

        feedback.emitStage(tr("Resplitting storage"),
                           tr("The system is reorganizing the data storage."), false);

        Database::Connection conn;
        if (!conn.start()) {
            qCDebug(log_component_maintenance_archive) << "Connection failed";
            feedback.emitFailure(tr("Connection failed"));
            return;
        }

        Threading::Receiver rxConn;
        terminateRequested.connect(rxConn,
                                   std::bind(&Database::Connection::signalTerminate, &conn));
        for (;;) {
            if (testTerminated())
                return;

            auto tx = conn.transaction(Database::Connection::Transaction_Serializable);

            Archive::Structure::Layout source(&conn);
            Archive::Structure::Layout
                    target(&conn, resplitStation, resplitArchive, resplitVariable);
            Threading::Receiver rxOp;
            Archive::ResplitOperation op(&conn, source, target);
            op.start();

            terminateRequested.connect(rxOp, std::bind(&Archive::ResplitOperation::signalTerminate,
                                                       &op));
            if (testTerminated())
                return;

            op.wait();
            if (testTerminated())
                return;
            if (!op.isOk()) {
                feedback.emitFailure(tr("Resplit failed"));
                return;
            }

            if (tx.end())
                break;
        }

        conn.shutdown();
        conn.wait();
        rxConn.disconnect();
    }

    if (testTerminated())
        return;

    if (optimize) {
        feedback.emitStage(tr("Optimizing database"),
                           tr("The system is starting the internal optimization process for the SQL database backend."),
                           false);

        auto db = Database::Util::getDefaultDatabase();
        std::promise<void> done;
        if (!db.open([&](std::unique_ptr<Database::Driver> &driver) {
            driver->optimizeDatabase();
            driver.reset();
            done.set_value();
        })) {
            qCDebug(log_component_maintenance_archive) << "Optimize open failed";
            feedback.emitFailure(tr("Database open failed"));
        }
        done.get_future().wait();
    }


    double endProcessing = Time::time();
    qCDebug(log_component_maintenance_archive) << "Finished maintenance after "
                                               << Logging::elapsed(endProcessing - startProcessing);

    QCoreApplication::processEvents();
}


ComponentOptions MaintenanceArchiveComponent::getOptions()
{
    ComponentOptions options;

    options.add("cleanup-stale",
                new ComponentOptionBoolean(tr("cleanup-stale", "name"), tr("Release stale locks"),
                                           tr("When enabled, the maintenance will release all stale locks "
                                                      "in the archive."), tr("Enabled")));

    options.add("remove-unused", new ComponentOptionBoolean(tr("remove-unused", "name"),
                                                            tr("Remove unused references"),
                                                            tr("When enabled, the maintenance will remove unused references in "
                                                                       "in the archive."),
                                                            QString()));

    options.add("optimize-database", new ComponentOptionBoolean(tr("optimize-database", "name"),
                                                                tr("Optimize and clean the database backend"),
                                                                tr("When enabled, the maintenance will perform automated optimization "
                                                                           "and cleaning of the SQL database backend."),
                                                                QString()));

    options.add("recompress",
                new ComponentOptionBoolean(tr("recompress", "name"), tr("Recompress older data"),
                                           tr("When enabled, the maintenance will recompress any data that has "
                                                      "not been modified for some time using a higher compression "
                                                      "algorithm.  This reduces the total size of the database and "
                                                      "speeds up access, but can take a very long time."),
                                           QString()));

    options.add("split-station", new ComponentOptionBoolean(tr("split-station", "name"),
                                                            tr("Restructure to split stations"),
                                                            tr("This option causes the storage to be restructured so each station "
                                                                       "is separated into a different table.  Under some database "
                                                                       "backends, this allows for better concurrency at the small "
                                                                       "cost of more complicated initialization."),
                                                            QString()));

    options.add("split-archive", new ComponentOptionBoolean(tr("split-archive", "name"),
                                                            tr("Restructure to split archives"),
                                                            tr("This option causes the storage to be restructured so each archive "
                                                                       "is separated into a different table.  Under some database "
                                                                       "backends, this allows for better concurrency at the small "
                                                                       "cost of more complicated initialization."),
                                                            QString()));

    options.add("split-variable", new ComponentOptionBoolean(tr("split-variable", "name"),
                                                             tr("Restructure to split variables"),
                                                             tr("This option causes the storage to be restructured so each variable "
                                                                        "is separated into a different table.  Under some database "
                                                                        "backends, this allows for better concurrency at the small "
                                                                        "cost of more complicated initialization."),
                                                             QString()));

    {
        ComponentOptionTimeBlock *o = new ComponentOptionTimeBlock(tr("recompress-age", "name"),
                                                                   tr("The minimum age of data to recompress"),
                                                                   tr("This sets the minimum age of data to recompress.  Data that "
                                                                              "has not been modified for this long will be recompressed."),
                                                                   tr("Six months"));
        o->setDefault(Time::Month, 6, false);
        options.add("recompress-age", o);
    }

    return options;
}

QList<ComponentExample> MaintenanceArchiveComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    qobject_cast<ComponentOptionBoolean *>(options.get("remove-unused"))->set(true);
    examples.append(
            ComponentExample(options, tr("Remove unused references", "default example name"),
                             tr("This will remove all unused references in the database backend.")));

    return examples;
}

int MaintenanceArchiveComponent::actionRequireStations()
{ return 0; }

int MaintenanceArchiveComponent::actionAllowStations()
{ return 0; }

CPD3Action *MaintenanceArchiveComponent::createAction(const ComponentOptions &options,
                                                      const std::vector<std::string> &)
{
    return new MaintenanceArchive(options);
}

QString MaintenanceArchiveComponent::promptActionContinue(const ComponentOptions &,
                                                          const std::vector<std::string> &)
{
    return tr("Archive maintenance can take a very long time and is not recommended to be "
                      "performed interactively.");
}
