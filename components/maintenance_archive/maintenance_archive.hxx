/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef MAINTENANCEARCHIVE_H
#define MAINTENANCEARCHIVE_H

#include "core/first.hxx"

#include <mutex>
#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QList>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "core/timeutils.hxx"
#include "core/threading.hxx"

class MaintenanceArchive : public CPD3::CPD3Action {
Q_OBJECT

    std::mutex mutex;
    bool terminated;

    bool cleanupStale;

    bool purgeUnused;

    bool resplitAny;
    bool resplitStation;
    bool resplitArchive;
    bool resplitVariable;

    bool optimize;

    bool recompress;
    CPD3::Time::LogicalTimeUnit recompressUnit;
    int recompressCount;
    bool recompressAlign;

    CPD3::Threading::Signal<> terminateRequested;

    bool testTerminated();

    MaintenanceArchive();

public:
    MaintenanceArchive(const CPD3::ComponentOptions &options);

    virtual ~MaintenanceArchive();

    virtual void signalTerminate();

protected:
    virtual void run();
};

class MaintenanceArchiveComponent : public QObject, virtual public CPD3::ActionComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::ActionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.maintenance_archive"
                              FILE
                              "maintenance_archive.json")

public:
    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual int actionAllowStations();

    virtual int actionRequireStations();

    QString promptActionContinue
            (const CPD3::ComponentOptions &options = CPD3::ComponentOptions(),
             const std::vector<std::string> &stations = {}) override;

    CPD3::CPD3Action *createAction
            (const CPD3::ComponentOptions &options = CPD3::ComponentOptions(),
             const std::vector<std::string> &stations = {}) override;
};

#endif
