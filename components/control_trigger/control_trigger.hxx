/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CONTROLTRIGGER_H
#define CONTROLTRIGGER_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QObject>
#include <QHash>
#include <QLoggingCategory>

#include "core/component.hxx"
#include "core/timeutils.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "smoothing/baseline.hxx"
#include "luascript/engine.hxx"

class ControlTrigger : public CPD3::Acquisition::AcquisitionInterface {
    std::string loggingName;
    QLoggingCategory log;

    bool terminated;
    CPD3::Acquisition::AcquisitionState *state;
    CPD3::Data::StreamSink *realtimeEgress;
    bool haveEmittedRealtimeMeta;
    CPD3::Data::StreamSink *loggingEgress;
    CPD3::Data::StreamSink *persistentEgress;

    bool enabled;
    bool logActive;
    bool realtimeStateUpdated;

    double lastExecutedTime;
    CPD3::Data::Variant::Root lastLoggingAction;

    CPD3::Data::Variant::Root instrumentMeta;

public:
    class TriggerInterface {
        ControlTrigger *control;
    public:
        TriggerInterface(ControlTrigger *ct);

        double time;

        double nextEvaluation;

        bool canTrigger(const std::unordered_set<std::string> &requireIDs,
                        const std::unordered_set<std::string> &excludeIDs);

        inline const QLoggingCategory &log()
        { return control->log; }
    };

private:
    friend class TriggerInterface;

    TriggerInterface triggerInterface;

    class RealtimeIngress : public CPD3::Data::StreamSink {
        ControlTrigger *control;
    public:
        RealtimeIngress(ControlTrigger *ct);

        virtual ~RealtimeIngress();

        void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

        void endData() override;
    };

    friend class RealtimeIngress;

    RealtimeIngress realtimeIngress;


    class Trigger {
        std::unordered_set<std::string> requiredIDs;
        std::unordered_set<std::string> excludedIDs;
        bool invert;
    protected:
        Trigger(const Trigger &other);

    public:
        Trigger(const CPD3::Data::Variant::Read &config);

        virtual ~Trigger();

        virtual bool evaluate(TriggerInterface *interface) = 0;

        virtual bool canTrigger(TriggerInterface *interface);

        virtual Trigger *clone() const = 0;

        inline bool inverted() const
        { return invert; }

        virtual bool incomingRealtime(TriggerInterface *interface,
                                      const CPD3::Data::SequenceValue::Transfer &values);
    };

    class TriggerAND : public Trigger {
        std::vector<Trigger *> components;
    protected:
        TriggerAND(const TriggerAND &other);

    public:
        TriggerAND(const CPD3::Data::Variant::Read &config);

        virtual ~TriggerAND();

        virtual bool evaluate(TriggerInterface *interface);

        virtual bool canTrigger(TriggerInterface *interface);

        virtual Trigger *clone() const;

        virtual bool incomingRealtime(TriggerInterface *interface,
                                      const CPD3::Data::SequenceValue::Transfer &values);
    };

    class TriggerOR : public Trigger {
        std::vector<Trigger *> components;
    protected:
        TriggerOR(const TriggerOR &other);

    public:
        TriggerOR(const CPD3::Data::Variant::Read &config);

        virtual ~TriggerOR();

        virtual bool evaluate(TriggerInterface *interface);

        virtual bool canTrigger(TriggerInterface *interface);

        virtual Trigger *clone() const;

        virtual bool incomingRealtime(TriggerInterface *interface,
                                      const CPD3::Data::SequenceValue::Transfer &values);
    };

    friend class TriggerAND;

    class TriggerAlways : public Trigger {
        bool polarity;

        TriggerAlways(const TriggerAlways &other);

    public:
        TriggerAlways(const CPD3::Data::Variant::Read &config, bool basePolarity = true);

        virtual ~TriggerAlways();

        virtual bool evaluate(TriggerInterface *interface);

        virtual Trigger *clone() const;
    };

    class TriggerSegmenting : public Trigger {
        struct Data {
            bool collect;
            std::vector<CPD3::Data::SequenceValue> values;

            explicit inline Data(bool collect) : collect(collect)
            { }
        };

        CPD3::Data::SequenceName::Map<Data> data;

        CPD3::Data::SequenceName::Map<
                std::vector<CPD3::Data::SequenceValue>> assembleValues() const;

    protected:
        TriggerSegmenting(const TriggerSegmenting &other);

        virtual bool collectUnit(const CPD3::Data::SequenceName &unit) = 0;

        CPD3::Data::SequenceSegment currentSegment;

    public:
        TriggerSegmenting(const CPD3::Data::Variant::Read &config);

        virtual ~TriggerSegmenting();

        virtual bool incomingRealtime(TriggerInterface *interface,
                                      const CPD3::Data::SequenceValue::Transfer &values);
    };

    class TriggerInput : public TriggerSegmenting {
        CPD3::Data::DynamicInput *input;
    protected:
        TriggerInput(const TriggerInput &other);

        double value(TriggerInterface *interface);

        virtual bool collectUnit(const CPD3::Data::SequenceName &unit);

    public:
        TriggerInput(const CPD3::Data::Variant::Read &config);

        virtual ~TriggerInput();
    };

    class TriggerThreshold : public TriggerInput {
        double threshold;
        enum {
            Op_LessThan,
            Op_LessThanEqual,
            Op_GreaterThan,
            Op_GreaterThanEqual,
            Op_Equal,
            Op_NotEqual,
        } operation;
    protected:
        TriggerThreshold(const TriggerThreshold &other);

    public:
        TriggerThreshold(const CPD3::Data::Variant::Read &config);

        virtual ~TriggerThreshold();

        virtual bool evaluate(TriggerInterface *interface);

        virtual Trigger *clone() const;
    };

    class TriggerRange : public TriggerInput {
        double min;
        double max;
    protected:
        TriggerRange(const TriggerRange &other);

    public:
        TriggerRange(const CPD3::Data::Variant::Read &config);

        virtual ~TriggerRange();

        virtual bool evaluate(TriggerInterface *interface);

        virtual Trigger *clone() const;
    };

    class TriggerAngle : public TriggerInput {
        double start;
        double end;
    protected:
        TriggerAngle(const TriggerAngle &other);

    public:
        TriggerAngle(const CPD3::Data::Variant::Read &config);

        virtual ~TriggerAngle();

        virtual bool evaluate(TriggerInterface *interface);

        virtual Trigger *clone() const;
    };

    class TriggerSpike : public TriggerInput {
        CPD3::Smoothing::BaselineSmoother *smoother;
    protected:
        TriggerSpike(const TriggerSpike &other);

    public:
        TriggerSpike(const CPD3::Data::Variant::Read &config);

        virtual ~TriggerSpike();

        virtual bool evaluate(TriggerInterface *interface);

        virtual Trigger *clone() const;

        virtual bool incomingRealtime(TriggerInterface *interface,
                                      const CPD3::Data::SequenceValue::Transfer &values);
    };

    class TriggerStable : public TriggerInput {
        CPD3::Smoothing::BaselineSmoother *smoother;
    protected:
        TriggerStable(const TriggerStable &other);

    public:
        TriggerStable(const CPD3::Data::Variant::Read &config);

        virtual ~TriggerStable();

        virtual bool evaluate(TriggerInterface *interface);

        virtual Trigger *clone() const;

        virtual bool incomingRealtime(TriggerInterface *interface,
                                      const CPD3::Data::SequenceValue::Transfer &values);
    };

    class TriggerHasFlags : public TriggerSegmenting {
        CPD3::Data::DynamicSequenceSelection *input;
        CPD3::Data::Variant::Flags flags;
    protected:
        TriggerHasFlags(const TriggerHasFlags &other);

        virtual bool collectUnit(const CPD3::Data::SequenceName &unit);

    public:
        TriggerHasFlags(const CPD3::Data::Variant::Read &config);

        virtual ~TriggerHasFlags();

        virtual bool evaluate(TriggerInterface *interface);

        virtual Trigger *clone() const;
    };

    class TriggerScript : public TriggerSegmenting {
        std::unique_ptr<CPD3::Data::DynamicSequenceSelection> input;
        std::string code;

        struct CallbackData {
            TriggerInterface *interface;
        };
        std::shared_ptr<CallbackData> callbackData;

        CPD3::Lua::Engine engine;
        CPD3::Lua::Engine::Frame root;
        CPD3::Lua::Engine::Reference invoke;

        void initialize();

    protected:
        TriggerScript(const TriggerScript &other);

        bool collectUnit(const CPD3::Data::SequenceName &name) override;

    public:
        TriggerScript(const CPD3::Data::Variant::Read &config);

        virtual ~TriggerScript();

        bool evaluate(TriggerInterface *interface) override;

        Trigger *clone() const override;
    };


    class Action {
    public:
        std::unordered_map<std::string, CPD3::Data::Variant::Root> commands;
        double flushTime;
        CPD3::Data::Variant::Flags setSystemFlags;
        CPD3::Data::Variant::Flags clearSystemFlags;
        CPD3::Data::Variant::Flags setBypassFlags;
        CPD3::Data::Variant::Flags clearBypassFlags;
        bool setAddFlavors;
        CPD3::Data::SequenceName::Flavors addFlavors;

        QString logEvent;
        bool logRealtime;

        QString description;
        CPD3::Data::Variant::Root identifier;

        std::unordered_set<std::string> acquireIDs;
        std::unordered_set<std::string> releaseIDs;
        double holdTime;
        bool continuous;

        double activationTime;
        bool hadJustActivated;

        Action();

        QString debugDescription() const;

        void loggingValue(CPD3::Data::Variant::Write &target, bool includeEvent = true) const;

        inline void loggingValue(CPD3::Data::Variant::Write &&target,
                                 bool includeEvent = true) const
        { return loggingValue(target, includeEvent); }

        Action *clone() const;
    };

    friend struct Operation;
    struct Operation {
        QList<Trigger *> triggers;
        Action *action;
    };

    friend class Configuration;

    class Configuration {
        double start;
        double end;

        void handleContaminate(const CPD3::Data::Variant::Read &config, Action *action);

        void handleUncontaminate(const CPD3::Data::Variant::Read &config, Action *action);

        void handleSetFlags(const CPD3::Data::Variant::Read &config, Action *action);

        void handleClearFlags(const CPD3::Data::Variant::Read &config, Action *action);

        void handleSetBypass(const CPD3::Data::Variant::Read &config, Action *action);

        void handleClearBypass(const CPD3::Data::Variant::Read &config, Action *action);

        Action *createAction(double defaultFlush,
                             const std::unordered_map<std::string, CPD3::Data::Variant::Root> &baselineCommands,
                             const CPD3::Data::Variant::Read &config);

        QList<Trigger *> createTriggers(const CPD3::Data::Variant::Read &config);

    public:
        QList<Operation> operations;

        std::unordered_map<CPD3::Data::Variant::Flag, CPD3::Data::Variant::Root> systemFlags;
        std::unordered_map<CPD3::Data::Variant::Flag, CPD3::Data::Variant::Root> bypassFlags;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        ~Configuration();

        Configuration(const Configuration &other);

        Configuration &operator=(const Configuration &other);

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under, const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);

        static Trigger *createTrigger(const CPD3::Data::Variant::Read &config);
    };

    QList<Configuration> config;

    void executeAction(double executeTime, Action *action);

    void advanceTime(double executeTime);

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time);

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time);

public:
    virtual ~ControlTrigger();

    ControlTrigger(const CPD3::Data::ValueSegment::Transfer &config,
                   const std::string &loggingContext);

    void setControlStream(CPD3::Acquisition::AcquisitionControlStream *controlStream) override;

    void setState(CPD3::Acquisition::AcquisitionState *state) override;

    void setRealtimeEgress(CPD3::Data::StreamSink *egress) override;

    void setLoggingEgress(CPD3::Data::StreamSink *egress) override;

    void setPersistentEgress(CPD3::Data::StreamSink *egress) override;

    CPD3::Data::SequenceValue::Transfer getPersistentValues() override;

    void incomingData(const CPD3::Util::ByteView &data,
                      double time,
                      AcquisitionInterface::IncomingDataType type = IncomingDataType::Stream) override;

    void incomingControl(const CPD3::Util::ByteView &data,
                         double time,
                         AcquisitionInterface::IncomingDataType type = IncomingDataType::Stream) override;

    void incomingTimeout(double time) override;

    void incomingCommand(const CPD3::Data::Variant::Read &command) override;

    void incomingAdvance(double time) override;

    CPD3::Data::StreamSink *getRealtimeIngress() override;

    CPD3::Data::Variant::Root getCommands() override;

    CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables() override;

    CPD3::Data::Variant::Root getSystemFlagsMetadata() override;

    CPD3::Data::Variant::Root getSourceMetadata() override;

    CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus() override;

    AutomaticDefaults getDefaults() override;

    bool isCompleted() override;

    void signalTerminate() override;
};

class ControlTriggerComponent
        : public QObject, virtual public CPD3::Acquisition::AcquisitionComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.control_trigger"
                              FILE
                              "control_trigger.json")
public:
    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config = {},
                                                                                const std::string &loggingContext = {}) override;
};

Q_DECLARE_METATYPE(ControlTrigger::TriggerInterface *)

#endif
