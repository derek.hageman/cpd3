/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "smoothing/baseline.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ControlTestState : public AcquisitionState {
public:
    Variant::Flags bypassFlags;
    Variant::Flags systemFlags;
    double flushTime;
    std::unordered_map<std::string, Variant::Read> commands;
    SequenceName::Flavors flavors;
    double wakeupTime;
    QString eventText;

    ControlTestState()
            : bypassFlags(),
              systemFlags(),
              flushTime(FP::undefined()),
              commands(),
              flavors(),
              wakeupTime(FP::undefined())
    { }

    virtual ~ControlTestState() = default;

    void setBypassFlag(const Variant::Flag &flag) override
    { bypassFlags.insert(flag); }

    void clearBypassFlag(const Variant::Flag &flag) override
    { bypassFlags.erase(flag); }

    void setSystemFlag(const Variant::Flag &flag) override
    { systemFlags.insert(flag); }

    void clearSystemFlag(const Variant::Flag &flag) override
    { systemFlags.erase(flag); }

    void requestFlush(double seconds) override
    { flushTime = seconds; }

    void sendCommand(const std::string &target, const Variant::Read &command) override
    { Util::insert_or_assign(commands, target, Variant::Root(command).read()); }

    void setSystemFlavors(const SequenceName::Flavors &flavors) override
    { this->flavors = flavors; }

    void setAveragingTime(Time::LogicalTimeUnit, int, bool setAligned) override
    { }

    void requestStateSave() override
    { }

    void requestGlobalStateSave() override
    { }

    void event(double, const QString &text, bool, const Variant::Read &) override
    { eventText = text; }

    void realtimeAdvanced(double) override
    { }

    void requestDataWakeup(double time) override
    {
        wakeupTime = time;
    }

    void requestControlWakeup(double time) override
    {
        wakeupTime = time;
    }
};

class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;

private slots:

    void initTestCase()
    {
        component =
                qobject_cast<AcquisitionComponent *>(ComponentLoader::create("control_trigger"));
        QVERIFY(component);

        Logging::suppressForTesting();
    }

    void always()
    {
        Variant::Root config;

        config["Actions/#0/Trigger/Type"].setString("Always");
        config["Actions/#0/Contaminate"].setBool(true);
        config["Actions/#0/Bypass"].setBool(true);
        config["Actions/#0/FlushTime"].setDouble(2.0);
        config["Actions/#0/SetCut"].setString("pm1");
        config["Actions/#0/Event"].setString("Sometext");
        Variant::Root cmd1;
        cmd1["Foobar"].setBool(true);
        config["Actions/#0/Commands"].set(cmd1);

        config["Actions/#1/Trigger/Type"].setString("Never");
        config["Actions/#1/Contaminate"].setBool(false);

        ControlTestState state;
        std::unique_ptr<AcquisitionInterface> interface(component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)}));
        QVERIFY(interface.get());
        interface->setState(&state);

        interface->incomingAdvance(0.0);
        QCOMPARE(state.systemFlags, Variant::Flags{"Contaminated"});
        QCOMPARE(state.bypassFlags, Variant::Flags{"Bypass"});
        QCOMPARE(state.flushTime, 2.0);
        QCOMPARE(state.flavors, SequenceName::Flavors{"pm1"});
        QCOMPARE(state.eventText, QString("Sometext"));
        std::unordered_map<std::string, Variant::Read> check;
        check.emplace(std::string(), cmd1.read());
        QCOMPARE(state.commands, check);
    }

    void threshold()
    {
        Variant::Root config;

        config["Actions/#0/Trigger/Type"].setString("Threshold");
        config["Actions/#0/Trigger/Threshold"].setDouble(5.0);
        config["Actions/#0/Trigger/Input"].setString("BsG_S11");
        config["Actions/#0/Contaminate"].setBool(true);

        ControlTestState state;
        std::unique_ptr<AcquisitionInterface> interface(component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)}));
        QVERIFY(interface.get());
        interface->setState(&state);

        StreamSink *rt = interface->getRealtimeIngress();
        QVERIFY(rt != NULL);

        interface->incomingAdvance(0.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QVERIFY(!FP::defined(state.wakeupTime));

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsR_S11"), Variant::Root(6.0), 1.0,
                              2.0)});
        QVERIFY(!FP::defined(state.wakeupTime));
        interface->incomingAdvance(1.0);
        QCOMPARE(state.systemFlags, Variant::Flags());

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsG_S11"), Variant::Root(6.0), 2.0,
                              3.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        interface->incomingAdvance(2.0);
        QCOMPARE(state.systemFlags, Variant::Flags{"Contaminated"});
    }

    void multiple()
    {
        Variant::Root config;

        config["Actions/#0/Triggers/#0/Type"].setString("Threshold");
        config["Actions/#0/Triggers/#0/Threshold"].setDouble(5.0);
        config["Actions/#0/Triggers/#0/Input"].setString("raw:BsG_S11");
        config["Actions/#0/Triggers/#1/Type"].setString("Threshold");
        config["Actions/#0/Triggers/#1/Threshold"].setDouble(10.0);
        config["Actions/#0/Triggers/#1/Input"].setString("raw:BsR_S11");
        config["Actions/#0/Contaminate"].setBool(true);

        config["Actions/#1/Trigger/Type"].setString("AND");
        config["Actions/#1/Trigger/Triggers/#0/Type"].setString("Threshold");
        config["Actions/#1/Trigger/Triggers/#0/Input"].setString("raw:BsB_S11");
        config["Actions/#1/Trigger/Triggers/#0/Threshold"].setDouble(5.0);
        config["Actions/#1/Trigger/Triggers/#1/Type"].setString("Threshold");
        config["Actions/#1/Trigger/Triggers/#1/Input"].setString("raw:BsG_S11");
        config["Actions/#1/Trigger/Triggers/#1/Operation"].setString("Less");
        config["Actions/#1/Trigger/Triggers/#1/Threshold"].setDouble(2.0);
        config["Actions/#1/Contaminate"].setBool(false);

        ControlTestState state;
        std::unique_ptr<AcquisitionInterface> interface(component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)}));
        QVERIFY(interface.get());
        interface->setState(&state);

        StreamSink *rt = interface->getRealtimeIngress();
        QVERIFY(rt != NULL);

        interface->incomingAdvance(0.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QVERIFY(!FP::defined(state.wakeupTime));

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "BsR_S11"), Variant::Root(6.0), 1.0,
                              10.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        state.wakeupTime = FP::undefined();
        interface->incomingAdvance(1.0);
        QCOMPARE(state.systemFlags, Variant::Flags());

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(6.0), 2.0,
                              10.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        state.wakeupTime = FP::undefined();
        interface->incomingAdvance(2.0);
        QCOMPARE(state.systemFlags, Variant::Flags{"Contaminated"});

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "BsB_S11"), Variant::Root(6.0), 3.0,
                              10.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        state.wakeupTime = FP::undefined();
        interface->incomingAdvance(3.0);
        QCOMPARE(state.systemFlags, Variant::Flags{"Contaminated"});

        rt->incomingData(
                SequenceValue(SequenceName("bnd", "raw", "BsG_S11"), Variant::Root(1.0), 4.0,
                              10.0));
        QVERIFY(FP::defined(state.wakeupTime));
        state.wakeupTime = FP::undefined();
        interface->incomingAdvance(4.0);
        QCOMPARE(state.systemFlags, Variant::Flags());

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "raw", "BsB_S11"), Variant::Root(4.0), 5.0, 10.0),
                SequenceValue(SequenceName("bnd", "raw", "BsR_S11"), Variant::Root(11.0), 5.0,
                              10.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        state.wakeupTime = FP::undefined();
        interface->incomingAdvance(5.0);
        QCOMPARE(state.systemFlags, Variant::Flags{"Contaminated"});
    }

    void dependencies()
    {
        Variant::Root config;

        config["Actions/#0/Trigger/Type"].setString("Threshold");
        config["Actions/#0/Trigger/Threshold"].setDouble(5.0);
        config["Actions/#0/Trigger/Input"].setString("BsG_S11");
        config["Actions/#0/Trigger/Exclude"].setString("I1");
        config["Actions/#0/Dependencies/Acquire"].setString("I2");
        config["Actions/#0/Contaminate"].setBool(true);

        config["Actions/#1/Trigger/Type"].setString("Threshold");
        config["Actions/#1/Trigger/Threshold"].setDouble(5.0);
        config["Actions/#1/Trigger/Input"].setString("BsB_S11");
        config["Actions/#1/Trigger/Require"].setString("I2");
        config["Actions/#1/Dependencies/Acquire"].setString("I1");
        config["Actions/#1/Dependencies/Release"].setString("I2");
        config["Actions/#1/Dependencies/Hold"].setDouble(3.0);
        config["Actions/#1/SetCut"].setString("pm1");

        config["Actions/#2/Trigger/Type"].setString("Threshold");
        config["Actions/#2/Trigger/Threshold"].setDouble(5.0);
        config["Actions/#2/Trigger/Input"].setString("BsR_S11");
        config["Actions/#2/Trigger/Require"].setString("I1");
        config["Actions/#2/Trigger/Invert"].setBool(true);
        config["Actions/#2/Bypass"].setBool(true);

        ControlTestState state;
        std::unique_ptr<AcquisitionInterface> interface(component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)}));
        QVERIFY(interface.get());
        interface->setState(&state);

        StreamSink *rt = interface->getRealtimeIngress();
        QVERIFY(rt != NULL);

        interface->incomingAdvance(0.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.bypassFlags, Variant::Flags());
        QCOMPARE(state.flavors, SequenceName::Flavors());
        QVERIFY(!FP::defined(state.wakeupTime));

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsB_S11"), Variant::Root(6.0), 1.0,
                              10.0),
                SequenceValue(SequenceName("bnd", "rt_instant", "BsR_S11"), Variant::Root(4.0), 1.0,
                              10.0)});
        interface->incomingAdvance(1.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.bypassFlags, Variant::Flags());
        QCOMPARE(state.flavors, SequenceName::Flavors());

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsB_S11"), Variant::Root(4.0), 2.0,
                              10.0),
                SequenceValue(SequenceName("bnd", "rt_instant", "BsR_S11"), Variant::Root(6.0), 2.0,
                              10.0),
                SequenceValue(SequenceName("bnd", "rt_instant", "BsG_S11"), Variant::Root(6.0), 2.0,
                              10.0)});
        interface->incomingAdvance(2.0);
        QCOMPARE(state.systemFlags, Variant::Flags{"Contaminated"});
        QCOMPARE(state.bypassFlags, Variant::Flags());
        QCOMPARE(state.flavors, SequenceName::Flavors());

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsR_S11"), Variant::Root(4.0), 3.0,
                              10.0)});
        interface->incomingAdvance(3.0);
        QCOMPARE(state.systemFlags, Variant::Flags{"Contaminated"});
        QCOMPARE(state.bypassFlags, Variant::Flags());
        QCOMPARE(state.flavors, SequenceName::Flavors());

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsB_S11"), Variant::Root(6.0), 4.0,
                              10.0),
                SequenceValue(SequenceName("bnd", "rt_instant", "BsR_S11"), Variant::Root(6.0), 4.0,
                              10.0)});
        interface->incomingAdvance(4.0);
        QCOMPARE(state.systemFlags, Variant::Flags{"Contaminated"});
        QCOMPARE(state.bypassFlags, Variant::Flags());
        QCOMPARE(state.flavors, SequenceName::Flavors{"pm1"});

        state.systemFlags.clear();
        interface->incomingAdvance(5.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.bypassFlags, Variant::Flags());
        QCOMPARE(state.flavors, SequenceName::Flavors{"pm1"});

        state.systemFlags.clear();
        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsR_S11"), Variant::Root(4.0), 6.0,
                              10.0)});
        interface->incomingAdvance(6.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.bypassFlags, Variant::Flags());
        QCOMPARE(state.flavors, SequenceName::Flavors{"pm1"});

        state.systemFlags.clear();
        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsR_S11"), Variant::Root(6.0), 8.0,
                              10.0)});
        interface->incomingAdvance(8.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.bypassFlags, Variant::Flags());
        QCOMPARE(state.flavors, SequenceName::Flavors{"pm1"});

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsR_S11"), Variant::Root(4.0), 9.0,
                              10.0)});
        interface->incomingAdvance(9.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.bypassFlags, Variant::Flags{"Bypass"});
        QCOMPARE(state.flavors, SequenceName::Flavors{"pm1"});
    }

    void once()
    {
        Variant::Root config;

        config["Actions/#0/Trigger/Type"].setString("Always");
        config["Actions/#0/Trigger/Exclude"].setString("FireOnce");
        config["Actions/#0/Contaminate"].setBool(true);
        config["Actions/#0/Dependencies/Acquire"].setString("FireOnce");

        ControlTestState state;
        std::unique_ptr<AcquisitionInterface> interface(component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)}));
        QVERIFY(interface.get());
        interface->setState(&state);

        interface->incomingAdvance(0.0);
        QCOMPARE(state.systemFlags, Variant::Flags{"Contaminated"});
        state.systemFlags.clear();

        interface->incomingAdvance(1.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        interface->incomingAdvance(1.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
    }

    void dependencyInputDifference()
    {
        Variant::Root config;

        config["Actions/#0/Trigger/Type"].setString("Threshold");
        config["Actions/#0/Trigger/Threshold"].setDouble(5.0);
        config["Actions/#0/Trigger/Input"].setString("BsG_S11");
        config["Actions/#0/Dependencies/Acquire"].setString("Upper");
        config["Actions/#0/Dependencies/Hold"].setDouble(1.1);
        config["Actions/#0/Contaminate"].setBool(true);

        config["Actions/#1/Trigger/Type"].setString("Threshold");
        config["Actions/#1/Trigger/Threshold"].setDouble(4.0);
        config["Actions/#1/Trigger/Operation"].setString("Less");
        config["Actions/#1/Trigger/Input"].setString("BsG_S11");
        config["Actions/#1/Trigger/Require"].setString("Upper");
        config["Actions/#1/Dependencies/Release"].setString("Upper");
        config["Actions/#1/Contaminate"].setBool(false);

        ControlTestState state;
        std::unique_ptr<AcquisitionInterface> interface(component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)}));
        QVERIFY(interface.get());
        interface->setState(&state);

        StreamSink *rt = interface->getRealtimeIngress();
        QVERIFY(rt != NULL);

        interface->incomingAdvance(0.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QVERIFY(!FP::defined(state.wakeupTime));

        SequenceName u1(SequenceName("bnd", "rt_instant", "BsG_S11", {"pm10"}));
        SequenceName u2(SequenceName("bnd", "rt_instant", "BsG_S11", {"pm1"}));
        SequenceName u3(SequenceName("bnd", "raw", "BsG_S11", {"pm10"}));

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(u2, Variant::Root(0.0), 0.5, FP::undefined())});
        interface->incomingAdvance(0.5);
        QCOMPARE(state.systemFlags, Variant::Flags());

        rt->incomingData(
                SequenceValue::Transfer{SequenceValue(u1, Variant::Root(0.0), 1.0, FP::undefined()),
                                        SequenceValue(u2, Variant::Root(), 1.0, FP::undefined())});
        interface->incomingAdvance(1.0);
        QCOMPARE(state.systemFlags, Variant::Flags());

        rt->incomingData(
                SequenceValue::Transfer{SequenceValue(u1, Variant::Root(6.0), 2.0, FP::undefined()),
                                        SequenceValue(u3, Variant::Root(6.0), 2.0,
                                                      FP::undefined())});
        interface->incomingAdvance(2.0);

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(u1, Variant::Root(6.0), 3.0, FP::undefined())});
        interface->incomingAdvance(3.0);
        QCOMPARE(state.systemFlags, Variant::Flags{"Contaminated"});

        rt->incomingData(
                SequenceValue::Transfer{SequenceValue(u2, Variant::Root(3.0), 4.0, FP::undefined()),
                                        SequenceValue(u1, Variant::Root(), 4.0, FP::undefined())});
        interface->incomingAdvance(4.0);
        QCOMPARE(state.systemFlags, Variant::Flags{"Contaminated"});

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(u2, Variant::Root(3.0), 5.0, FP::undefined())});
        interface->incomingAdvance(5.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
    }

    void flags()
    {
        Variant::Root config;

        config["Actions/#0/Trigger/Type"].setString("Flags");
        config["Actions/#0/Trigger/Flags"].setFlags({"Trigger"});
        config["Actions/#0/Trigger/Input"].setString("F1_S11");
        config["Actions/#0/Contaminate"].setBool(true);

        ControlTestState state;
        std::unique_ptr<AcquisitionInterface> interface(component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)}));
        QVERIFY(interface.get());
        interface->setState(&state);

        StreamSink *rt = interface->getRealtimeIngress();
        QVERIFY(rt != NULL);

        interface->incomingAdvance(0.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QVERIFY(!FP::defined(state.wakeupTime));

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "F2_S11"),
                              Variant::Root(Variant::Flags{"Trigger"}), 1.0, 2.0)});
        QVERIFY(!FP::defined(state.wakeupTime));
        interface->incomingAdvance(1.0);
        QCOMPARE(state.systemFlags, Variant::Flags());

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "F1_S11"),
                              Variant::Root(Variant::Flags{"Not"}),
                              2.0, 3.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        interface->incomingAdvance(2.0);
        QCOMPARE(state.systemFlags, Variant::Flags());

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "F1_S11"),
                              Variant::Root(Variant::Flags{"Trigger"}), 3.0, 4.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        interface->incomingAdvance(3.0);
        QCOMPARE(state.systemFlags, Variant::Flags{"Contaminated"});
    }

    void spike()
    {
        Variant::Root config;

        config["Actions/#0/Trigger/Type"].setString("Spike");
        config["Actions/#0/Trigger/Smoother/Type"].setString("FixedTime");
        config["Actions/#0/Trigger/Smoother/Time"].setDouble(2.0);
        config["Actions/#0/Trigger/Smoother/MinimumTime"].setDouble(2.0);
        config["Actions/#0/Trigger/Smoother/Band"].setDouble(-1.0);
        config["Actions/#0/Trigger/Input"].setString("BsG_S11");
        config["Actions/#0/Contaminate"].setBool(true);

        ControlTestState state;
        std::unique_ptr<AcquisitionInterface> interface(component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)}));
        QVERIFY(interface.get());
        interface->setState(&state);

        StreamSink *rt = interface->getRealtimeIngress();
        QVERIFY(rt != NULL);

        interface->incomingAdvance(0.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QVERIFY(!FP::defined(state.wakeupTime));

        interface->incomingAdvance(0.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QVERIFY(!FP::defined(state.wakeupTime));

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsR_S11"), Variant::Root(1.0), 1.0,
                              2.0)});
        QVERIFY(!FP::defined(state.wakeupTime));
        interface->incomingAdvance(1.0);
        QCOMPARE(state.systemFlags, Variant::Flags());

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsG_S11"), Variant::Root(1.0), 2.0,
                              3.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        state.wakeupTime = FP::undefined();
        interface->incomingAdvance(2.0);
        QCOMPARE(state.systemFlags, Variant::Flags());

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsG_S11"), Variant::Root(1.0), 3.0,
                              4.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        state.wakeupTime = FP::undefined();
        interface->incomingAdvance(3.0);
        QCOMPARE(state.systemFlags, Variant::Flags());

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsG_S11"), Variant::Root(1.0), 4.0,
                              5.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        state.wakeupTime = FP::undefined();
        interface->incomingAdvance(4.0);
        QCOMPARE(state.systemFlags, Variant::Flags());

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsG_S11"), Variant::Root(10.0),
                              5.0,
                              6.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        state.wakeupTime = FP::undefined();
        interface->incomingAdvance(5.0);
        QCOMPARE(state.systemFlags, Variant::Flags{"Contaminated"});
    }

    void spikeHold()
    {
        Variant::Root config;

        config["Actions/#0/Triggers/#0/Type"].setString("And");
        config["Actions/#0/Triggers/#0/Triggers/#0/Type"].setString("Spike");
        config["Actions/#0/Triggers/#0/Triggers/#0/Smoother/Type"].setString("FixedTime");
        config["Actions/#0/Triggers/#0/Triggers/#0/Smoother/Time"].setDouble(10.0);
        config["Actions/#0/Triggers/#0/Triggers/#0/Smoother/MinimumTime"].setDouble(5.0);
        config["Actions/#0/Triggers/#0/Triggers/#0/Input"].setString("N_N71");
        config["Actions/#0/Triggers/#0/Triggers/#1/Type"].setString("Threshold");
        config["Actions/#0/Triggers/#0/Triggers/#1/Threshold"].setDouble(500.0);
        config["Actions/#0/Triggers/#0/Triggers/#1/Input"].setString("N_N71");
        config["Actions/#0/Contaminate/ContaminateCPCSpike/Description"].setString(
                "Data contaminated due to high values");
        config["Actions/#0/Contaminate/ContaminateCPCSpike/Bits"].setInt64(0x0001);
        config["Actions/#0/Dependencies/Acquire"].setFlags(Variant::Flags{"ContaminateCPCSpike"});
        config["Actions/#0/Dependencies/Release"].setFlags(
                Variant::Flags{"ContaminateCPCSpikeClear"});
        config["Actions/#0/Dependencies/Hold"].setDouble(3.0);

        config["Actions/#1/Triggers/#0/Type"].setString("Always");
        config["Actions/#1/Triggers/#0/Require"].setFlags(Variant::Flags{"ContaminateCPCSpike"});
        config["Actions/#1/Triggers/#0/Exclude"].setFlags(
                Variant::Flags{"ContaminateCPCSpikeClear"});
        config["Actions/#1/Dependencies/Acquire"].setFlags(
                Variant::Flags{"ContaminateCPCSpikeClear"});
        config["Actions/#1/ClearContaminated"].setFlags(Variant::Flags{"ContaminateCPCSpike"});

        ControlTestState state;
        std::unique_ptr<AcquisitionInterface> interface(component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)}));
        QVERIFY(interface.get());
        interface->setState(&state);

        QCOMPARE(interface->getSystemFlagsMetadata().read()
                          .metadataSingleFlag("ContaminateCPCSpike")
                          .hash("Bits")
                          .toInt64(), Q_INT64_C(0x0001));

        StreamSink *rt = interface->getRealtimeIngress();
        QVERIFY(rt != NULL);

        interface->incomingAdvance(0.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QVERIFY(!FP::defined(state.wakeupTime));

        interface->incomingAdvance(0.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QVERIFY(!FP::defined(state.wakeupTime));

        double time = 1.0;
        for (double end = time + 6.0; time < end; time += 1.0) {
            rt->incomingData(SequenceValue::Transfer{
                    SequenceValue(SequenceName("bnd", "rt_instant", "N_N71"), Variant::Root(1.0),
                                  time,
                                  time + 1.0)});
            interface->incomingAdvance(time);
            QCOMPARE(state.systemFlags, Variant::Flags());
        }

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "N_N71"), Variant::Root(499.0),
                              time,
                              time + 1.0)});
        interface->incomingAdvance(time);
        QCOMPARE(state.systemFlags, Variant::Flags());
        time += 1.0;

        state.wakeupTime = FP::undefined();
        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "N_N71"), Variant::Root(50000.0),
                              time,
                              time + 1.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        interface->incomingAdvance(time);
        QCOMPARE(state.systemFlags, Variant::Flags{"ContaminateCPCSpike"});
        time += 1.0;

        state.wakeupTime = FP::undefined();
        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "N_N71"), Variant::Root(1.0), time,
                              time + 1.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        interface->incomingAdvance(time);
        QCOMPARE(state.systemFlags, Variant::Flags{"ContaminateCPCSpike"});
        time += 1.0;

        for (double end = time + 3.0; time < end; time += 1.0) {
            rt->incomingData(SequenceValue::Transfer{
                    SequenceValue(SequenceName("bnd", "rt_instant", "N_N71"), Variant::Root(1.0),
                                  time,
                                  time + 1.0)});
            interface->incomingAdvance(time);
        }

        QCOMPARE(state.systemFlags, Variant::Flags());
    }

    void stable()
    {
        Variant::Root config;

        config["Actions/#0/Trigger/Type"].setString("Stable");
        config["Actions/#0/Trigger/Smoother/Type"].setString("FixedTime");
        config["Actions/#0/Trigger/Smoother/Time"].setDouble(2.0);
        config["Actions/#0/Trigger/Smoother/MinimumTime"].setDouble(2.0);
        config["Actions/#0/Trigger/Smoother/RSD"].setDouble(1.0);
        config["Actions/#0/Trigger/Input"].setString("BsG_S11");
        config["Actions/#0/Contaminate"].setBool(true);

        ControlTestState state;
        std::unique_ptr<AcquisitionInterface> interface(component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)}));
        QVERIFY(interface.get());
        interface->setState(&state);

        StreamSink *rt = interface->getRealtimeIngress();
        QVERIFY(rt != NULL);

        interface->incomingAdvance(0.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QVERIFY(!FP::defined(state.wakeupTime));

        interface->incomingAdvance(0.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QVERIFY(!FP::defined(state.wakeupTime));

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsR_S11"), Variant::Root(1.0), 1.0,
                              2.0)});
        QVERIFY(!FP::defined(state.wakeupTime));
        interface->incomingAdvance(1.0);
        QCOMPARE(state.systemFlags, Variant::Flags());

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsG_S11"), Variant::Root(1.0), 2.0,
                              3.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        state.wakeupTime = FP::undefined();
        interface->incomingAdvance(2.0);
        QCOMPARE(state.systemFlags, Variant::Flags());

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsG_S11"), Variant::Root(10.0),
                              3.0,
                              4.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        state.wakeupTime = FP::undefined();
        interface->incomingAdvance(3.0);
        QCOMPARE(state.systemFlags, Variant::Flags());

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsG_S11"), Variant::Root(1.0), 4.0,
                              5.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        state.wakeupTime = FP::undefined();
        interface->incomingAdvance(4.0);
        QCOMPARE(state.systemFlags, Variant::Flags());

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsG_S11"), Variant::Root(1.0), 5.0,
                              6.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        state.wakeupTime = FP::undefined();
        interface->incomingAdvance(5.0);

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsG_S11"), Variant::Root(1.0), 6.0,
                              7.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        state.wakeupTime = FP::undefined();
        interface->incomingAdvance(6.0);

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsG_S11"), Variant::Root(1.0), 7.0,
                              8.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        state.wakeupTime = FP::undefined();
        interface->incomingAdvance(7.0);
        QCOMPARE(state.systemFlags, Variant::Flags{"Contaminated"});
    }

    void range()
    {
        Variant::Root config;

        config["Actions/#0/Trigger/Type"].setString("Range");
        config["Actions/#0/Trigger/Minimum"].setDouble(5.0);
        config["Actions/#0/Trigger/Maximum"].setDouble(7.0);
        config["Actions/#0/Trigger/Input"].setString("BsG_S11");
        config["Actions/#0/Contaminate"].setBool(true);

        ControlTestState state;
        std::unique_ptr<AcquisitionInterface> interface(component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)}));
        QVERIFY(interface.get());
        interface->setState(&state);

        StreamSink *rt = interface->getRealtimeIngress();
        QVERIFY(rt != NULL);

        interface->incomingAdvance(0.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QVERIFY(!FP::defined(state.wakeupTime));

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsR_S11"), Variant::Root(6.0), 1.0,
                              2.0)});
        QVERIFY(!FP::defined(state.wakeupTime));
        interface->incomingAdvance(1.0);
        QCOMPARE(state.systemFlags, Variant::Flags());

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsG_S11"), Variant::Root(8.0), 2.0,
                              3.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        state.wakeupTime = FP::undefined();
        interface->incomingAdvance(2.0);
        QCOMPARE(state.systemFlags, Variant::Flags());

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsG_S11"), Variant::Root(4.0), 3.0,
                              4.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        state.wakeupTime = FP::undefined();
        interface->incomingAdvance(3.0);
        QCOMPARE(state.systemFlags, Variant::Flags());

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsG_S11"), Variant::Root(6.0), 4.0,
                              5.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        state.wakeupTime = FP::undefined();
        interface->incomingAdvance(4.0);
        QCOMPARE(state.systemFlags, Variant::Flags{"Contaminated"});
    }

    void angle()
    {
        Variant::Root config;

        config["Actions/#0/Trigger/Type"].setString("Angle");
        config["Actions/#0/Trigger/Start"].setDouble(110.0);
        config["Actions/#0/Trigger/End"].setDouble(130.0);
        config["Actions/#0/Trigger/Input"].setString("WD_WX1");
        config["Actions/#0/Contaminate"].setBool(true);
        config["Actions/#0/Dependencies/Acquire"].setString("Sector");
        config["Actions/#0/Dependencies/Hold"].setDouble(2.0);

        config["Actions/#1/Trigger/Type"].setString("Winds");
        config["Actions/#1/Trigger/Start"].setDouble(350.0);
        config["Actions/#1/Trigger/End"].setDouble(10.0);
        config["Actions/#1/Trigger/Input"].setString("WD_WX1");
        config["Actions/#1/Bypass"].setBool(true);
        config["Actions/#1/Dependencies/Acquire"].setString("Sector");
        config["Actions/#1/Dependencies/Hold"].setDouble(1.0);

        config["Actions/#2/Trigger/Type"].setString("Always");
        config["Actions/#2/Trigger/Require"].setString("Sector");
        config["Actions/#2/Contaminate"].setBool(false);
        config["Actions/#2/Bypass"].setBool(false);


        ControlTestState state;
        std::unique_ptr<AcquisitionInterface> interface(component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)}));
        QVERIFY(interface.get());
        interface->setState(&state);

        StreamSink *rt = interface->getRealtimeIngress();
        QVERIFY(rt != NULL);

        interface->incomingAdvance(0.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.bypassFlags, Variant::Flags());
        QVERIFY(!FP::defined(state.wakeupTime));

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "WS_WX1"), Variant::Root(120.0),
                              1.0,
                              2.0)});
        QVERIFY(!FP::defined(state.wakeupTime));
        interface->incomingAdvance(1.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.bypassFlags, Variant::Flags());

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "WD_WX1"), Variant::Root(50.0), 2.0,
                              3.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        state.wakeupTime = FP::undefined();
        interface->incomingAdvance(2.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.bypassFlags, Variant::Flags());

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "WD_WX1"), Variant::Root(120.0),
                              3.0,
                              4.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        state.wakeupTime = FP::undefined();
        interface->incomingAdvance(3.0);
        QCOMPARE(state.systemFlags, Variant::Flags{"Contaminated"});
        QCOMPARE(state.bypassFlags, Variant::Flags());

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "WD_WX1"), Variant::Root(100.0),
                              4.0,
                              5.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        state.wakeupTime = FP::undefined();
        interface->incomingAdvance(4.0);
        QCOMPARE(state.systemFlags, Variant::Flags{"Contaminated"});
        QCOMPARE(state.bypassFlags, Variant::Flags());

        interface->incomingAdvance(6.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.bypassFlags, Variant::Flags());

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "WD_WX1"), Variant::Root(5.0), 7.0,
                              8.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        state.wakeupTime = FP::undefined();
        interface->incomingAdvance(7.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.bypassFlags, Variant::Flags{"Bypass"});

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "WD_WX1"), Variant::Root(355.0),
                              8.0,
                              9.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        state.wakeupTime = FP::undefined();
        interface->incomingAdvance(8.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.bypassFlags, Variant::Flags{"Bypass"});

        interface->incomingAdvance(12.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.bypassFlags, Variant::Flags{"Bypass"});

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "WD_WX1"), Variant::Root(15.0),
                              13.0,
                              14.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        state.wakeupTime = FP::undefined();
        interface->incomingAdvance(13.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.bypassFlags, Variant::Flags{"Bypass"});

        interface->incomingAdvance(14.1);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.bypassFlags, Variant::Flags());
    }

    void script()
    {
        Variant::Root config;

        config["Actions/#0/Trigger/Type"].setString("Script");
        config["Actions/#0/Trigger/Code"].setString("if (data.START < 2.0) then \n"
                                                    "   evaluateAt(data.START + 2.0); \n"
                                                    "else \n"
                                                            "   evaluateIn(2.0); \n"
                                                    "end \n"
                                                    "return data.BsG_S11 > 5.0;");
        config["Actions/#0/Trigger/Input"].setString("BsG_S11");
        config["Actions/#0/Contaminate"].setBool(true);

        ControlTestState state;
        std::unique_ptr<AcquisitionInterface> interface(component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)}));
        QVERIFY(interface.get());
        interface->setState(&state);

        StreamSink *rt = interface->getRealtimeIngress();
        QVERIFY(rt != NULL);

        interface->incomingAdvance(0.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.wakeupTime, 2.0);
        state.wakeupTime = FP::undefined();

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsR_S11"), Variant::Root(6.0), 1.0,
                              2.0)});
        QVERIFY(!FP::defined(state.wakeupTime));
        interface->incomingAdvance(1.0);
        QCOMPARE(state.wakeupTime, 3.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        state.wakeupTime = FP::undefined();

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsG_S11"), Variant::Root(4.0), 2.0,
                              3.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        state.wakeupTime = FP::undefined();
        interface->incomingAdvance(2.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.wakeupTime, 4.0);
        state.wakeupTime = FP::undefined();

        rt->incomingData(SequenceValue::Transfer{
                SequenceValue(SequenceName("bnd", "rt_instant", "BsG_S11"), Variant::Root(6.0), 3.0,
                              4.0)});
        QVERIFY(FP::defined(state.wakeupTime));
        state.wakeupTime = FP::undefined();
        interface->incomingAdvance(3.0);
        QCOMPARE(state.systemFlags, Variant::Flags{"Contaminated"});
        QCOMPARE(state.wakeupTime, 5.0);
        state.wakeupTime = FP::undefined();
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
