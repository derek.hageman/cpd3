/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/segment.hxx"
#include "datacore/variant/composite.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "luascript/libs/sequencesegment.hxx"
#include "luascript/libs/time.hxx"

#include "control_trigger.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;
using namespace CPD3::Smoothing;

static std::unordered_set<std::string> convertIDs(const Variant::Read &config)
{
    std::unordered_set<std::string> result;
    for (const auto &add : config.toChildren().keys()) {
        result.insert(add);
    }
    return result;
}

ControlTrigger::Trigger::Trigger(const Trigger &other) : requiredIDs(other.requiredIDs),
                                                         excludedIDs(other.excludedIDs),
                                                         invert(other.invert)
{ }

ControlTrigger::Trigger::Trigger(const Variant::Read &config) : requiredIDs(
        convertIDs(config["Require"])), excludedIDs(convertIDs(config["Exclude"])),
                                                                invert(config["Invert"].toBool())
{ }

ControlTrigger::Trigger::~Trigger()
{ }

bool ControlTrigger::Trigger::canTrigger(TriggerInterface *interface)
{ return interface->canTrigger(requiredIDs, excludedIDs); }

bool ControlTrigger::Trigger::incomingRealtime(TriggerInterface *interface,
                                               const SequenceValue::Transfer &values)
{
    Q_UNUSED(interface);
    Q_UNUSED(values);
    return false;
}

ControlTrigger::TriggerAND::TriggerAND(const TriggerAND &other) : Trigger(other), components()
{
    for (std::vector<Trigger *>::const_iterator it = other.components.begin(),
            end = other.components.end(); it != end; ++it) {
        components.push_back((*it)->clone());
    }
}

ControlTrigger::TriggerAND::TriggerAND(const Variant::Read &config) : Trigger(config), components()
{
    for (auto add : config["Triggers"].toArray()) {
        Trigger *t = ControlTrigger::Configuration::createTrigger(add);
        if (t == NULL)
            continue;
        components.push_back(t);
    }
}

ControlTrigger::TriggerAND::~TriggerAND()
{
    qDeleteAll(components);
}

bool ControlTrigger::TriggerAND::evaluate(TriggerInterface *interface)
{
    bool result = true;
    for (std::vector<Trigger *>::iterator it = components.begin(), end = components.end();
            it != end;
            ++it) {
        if ((*it)->evaluate(interface)) {
            if ((*it)->inverted())
                result = false;
        } else {
            if (!(*it)->inverted())
                result = false;
        }
    }
    return result;
}

bool ControlTrigger::TriggerAND::canTrigger(TriggerInterface *interface)
{
    bool result = true;
    for (std::vector<Trigger *>::iterator it = components.begin(), end = components.end();
            it != end;
            ++it) {
        if (!(*it)->canTrigger(interface))
            result = false;
    }
    return result;
}

ControlTrigger::Trigger *ControlTrigger::TriggerAND::clone() const
{ return new TriggerAND(*this); }

bool ControlTrigger::TriggerAND::incomingRealtime(TriggerInterface *interface,
                                                  const SequenceValue::Transfer &values)
{
    bool result = false;
    for (std::vector<Trigger *>::iterator it = components.begin(), end = components.end();
            it != end;
            ++it) {
        if ((*it)->incomingRealtime(interface, values))
            result = true;
    }
    return result;
}


ControlTrigger::TriggerOR::TriggerOR(const TriggerOR &other) : Trigger(other), components()
{
    for (std::vector<Trigger *>::const_iterator it = other.components.begin(),
            end = other.components.end(); it != end; ++it) {
        components.push_back((*it)->clone());
    }
}

ControlTrigger::TriggerOR::TriggerOR(const Variant::Read &config) : Trigger(config), components()
{
    for (auto add : config["Triggers"].toArray()) {
        Trigger *t = ControlTrigger::Configuration::createTrigger(add);
        if (t == NULL)
            continue;
        components.push_back(t);
    }
}

ControlTrigger::TriggerOR::~TriggerOR()
{
    qDeleteAll(components);
}

bool ControlTrigger::TriggerOR::evaluate(TriggerInterface *interface)
{
    bool result = false;
    for (std::vector<Trigger *>::iterator it = components.begin(), end = components.end();
            it != end;
            ++it) {
        if ((*it)->canTrigger(interface)) {
            if ((*it)->evaluate(interface)) {
                if (!(*it)->inverted())
                    result = true;
            } else {
                if ((*it)->inverted())
                    result = true;
            }
        }
    }
    return result;
}

bool ControlTrigger::TriggerOR::canTrigger(TriggerInterface *interface)
{
    bool result = false;
    for (std::vector<Trigger *>::iterator it = components.begin(), end = components.end();
            it != end;
            ++it) {
        if (!(*it)->canTrigger(interface))
            result = true;
    }
    return result;
}

ControlTrigger::Trigger *ControlTrigger::TriggerOR::clone() const
{ return new TriggerOR(*this); }

bool ControlTrigger::TriggerOR::incomingRealtime(TriggerInterface *interface,
                                                 const SequenceValue::Transfer &values)
{
    bool result = false;
    for (std::vector<Trigger *>::iterator it = components.begin(), end = components.end();
            it != end;
            ++it) {
        if ((*it)->incomingRealtime(interface, values))
            result = true;
    }
    return result;
}


ControlTrigger::TriggerAlways::TriggerAlways(const TriggerAlways &other) : Trigger(other),
                                                                           polarity(other.polarity)
{ }

ControlTrigger::TriggerAlways::TriggerAlways(const Variant::Read &config, bool basePolarity)
        : Trigger(config), polarity(basePolarity)
{
    if (config["Polarity"].exists())
        polarity = config["Polarity"].toBool();
}

ControlTrigger::TriggerAlways::~TriggerAlways()
{ }

bool ControlTrigger::TriggerAlways::evaluate(TriggerInterface *interface)
{
    Q_UNUSED(interface);
    return polarity;
}

ControlTrigger::Trigger *ControlTrigger::TriggerAlways::clone() const
{ return new TriggerAlways(*this); }


ControlTrigger::TriggerSegmenting::TriggerSegmenting(const TriggerSegmenting &other) : Trigger(
        other), data(other.data)
{ }

ControlTrigger::TriggerSegmenting::TriggerSegmenting(const Variant::Read &config) : Trigger(config),
                                                                                    data()
{ }

ControlTrigger::TriggerSegmenting::~TriggerSegmenting()
{ }

bool ControlTrigger::TriggerSegmenting::incomingRealtime(TriggerInterface *interface,
                                                         const SequenceValue::Transfer &values)
{
    bool updated = false;
    for (const auto &v : values) {
        auto check = data.find(v.getName());
        if (check != data.end()) {
            if (!check->second.collect)
                continue;
        } else {
            bool result = collectUnit(v.getName());
            check = data.emplace(v.getName(), Data(result)).first;
            if (!result)
                continue;
        }
        SequenceSegment::Stream::maintainValues(check->second.values, v);
        updated = true;
    }

    if (updated) {
        currentSegment = SequenceSegment(interface->time, FP::undefined(), assembleValues());
    }

    return updated;
}

SequenceName::Map<
        std::vector<SequenceValue>> ControlTrigger::TriggerSegmenting::assembleValues() const
{
    SequenceName::Map<std::vector<SequenceValue>> result;
    for (const auto &add : data) {
        if (!add.second.collect)
            continue;
        result.emplace(add.first, add.second.values);
    }
    return result;
}

ControlTrigger::TriggerInput::TriggerInput(const TriggerInput &other) : TriggerSegmenting(other),
                                                                        input(other.input->clone())
{ }

ControlTrigger::TriggerInput::TriggerInput(const Variant::Read &config) : TriggerSegmenting(config),
                                                                          input(NULL)
{
    input = DynamicInput::fromConfiguration((ValueSegment::Transfer{
                                                    ValueSegment(FP::undefined(), FP::undefined(), Variant::Root(config))}), "Input",
                                            FP::undefined(), FP::undefined(), {},
                                            "rt_instant");
    input->registerExpected({}, "rt_instant");
}

ControlTrigger::TriggerInput::~TriggerInput()
{
    delete input;
}

bool ControlTrigger::TriggerInput::collectUnit(const SequenceName &unit)
{ return input->registerInput(unit); }

double ControlTrigger::TriggerInput::value(TriggerInterface *interface)
{ return input->get(currentSegment); }


ControlTrigger::TriggerThreshold::TriggerThreshold(const TriggerThreshold &other) : TriggerInput(
        other), threshold(other.threshold), operation(other.operation)
{ }

ControlTrigger::TriggerThreshold::TriggerThreshold(const Variant::Read &config) : TriggerInput(
        config), threshold(config["Threshold"].toDouble()), operation(Op_GreaterThan)
{
    const auto &o = config["Operation"].toString();
    if (Util::equal_insensitive(o, "less", "lessthan"))
        operation = Op_LessThan;
    else if (Util::equal_insensitive(o, "lessequal", "lessthanorequal", "lessthanorequalto"))
        operation = Op_LessThanEqual;
    else if (Util::equal_insensitive(o, "greater", "greaterthan"))
        operation = Op_GreaterThan;
    else if (Util::equal_insensitive(o, "greaterequal", "greaterthanorequal",
                                     "greaterthanorequalto"))
        operation = Op_GreaterThanEqual;
    else if (Util::equal_insensitive(o, "equal", "exactly"))
        operation = Op_Equal;
    else if (Util::equal_insensitive(o, "notequal"))
        operation = Op_NotEqual;
}

ControlTrigger::TriggerThreshold::~TriggerThreshold()
{ }

bool ControlTrigger::TriggerThreshold::evaluate(TriggerInterface *interface)
{
    Q_UNUSED(interface);
    double v = value(interface);
    switch (operation) {
    case Op_LessThan:
        if (!FP::defined(threshold))
            return FP::defined(v);
        if (!FP::defined(v))
            return false;
        return v < threshold;
    case Op_LessThanEqual:
        if (!FP::defined(threshold))
            return FP::defined(v);
        if (!FP::defined(v))
            return false;
        return v <= threshold;
    case Op_GreaterThan:
        if (!FP::defined(threshold))
            return FP::defined(v);
        if (!FP::defined(v))
            return false;
        return v > threshold;
    case Op_GreaterThanEqual:
        if (!FP::defined(threshold))
            return FP::defined(v);
        if (!FP::defined(v))
            return false;
        return v >= threshold;
    case Op_Equal:
        if (!FP::defined(threshold))
            return FP::defined(v);
        if (!FP::defined(v))
            return false;
        return v == threshold;
    case Op_NotEqual:
        if (!FP::defined(threshold))
            return !FP::defined(v);
        if (!FP::defined(v))
            return false;
        return v != threshold;
    }
    Q_ASSERT(false);
    return false;
}

ControlTrigger::Trigger *ControlTrigger::TriggerThreshold::clone() const
{ return new TriggerThreshold(*this); }


ControlTrigger::TriggerRange::TriggerRange(const TriggerRange &other) : TriggerInput(other),
                                                                        min(other.min),
                                                                        max(other.max)
{ }

ControlTrigger::TriggerRange::TriggerRange(const Variant::Read &config) : TriggerInput(config),
                                                                          min(config["Minimum"].toDouble()),
                                                                          max(config["Maximum"].toDouble())
{ }

ControlTrigger::TriggerRange::~TriggerRange()
{ }

bool ControlTrigger::TriggerRange::evaluate(TriggerInterface *interface)
{
    Q_UNUSED(interface);
    double v = value(interface);
    if (!FP::defined(v))
        return false;
    if (FP::defined(min) && v < min)
        return false;
    if (FP::defined(max) && v > max)
        return false;
    return true;
}

ControlTrigger::Trigger *ControlTrigger::TriggerRange::clone() const
{ return new TriggerRange(*this); }


ControlTrigger::TriggerAngle::TriggerAngle(const TriggerAngle &other) : TriggerInput(other),
                                                                        start(other.start),
                                                                        end(other.end)
{ }

ControlTrigger::TriggerAngle::TriggerAngle(const Variant::Read &config) : TriggerInput(config),
                                                                          start(config["Start"].toDouble()),
                                                                          end(config["End"].toDouble())
{ }

ControlTrigger::TriggerAngle::~TriggerAngle()
{ }

bool ControlTrigger::TriggerAngle::evaluate(TriggerInterface *interface)
{
    Q_UNUSED(interface);
    double v = value(interface);
    if (!FP::defined(v))
        return false;
    while (v < 0.0) { v += 360.0; }
    while (v >= 360.0) { v -= 360.0; }

    if (!FP::defined(start)) {
        if (FP::defined(end))
            return v <= end;
        return false;
    } else if (!FP::defined(end)) {
        return v >= start;
    }

    if (start <= end) {
        return v >= start && v <= end;
    } else {
        return v <= end || v >= start;
    }
}

ControlTrigger::Trigger *ControlTrigger::TriggerAngle::clone() const
{ return new TriggerAngle(*this); }


ControlTrigger::TriggerSpike::TriggerSpike(const TriggerSpike &other) : TriggerInput(other),
                                                                        smoother(other.smoother
                                                                                      ->clone())
{ }

ControlTrigger::TriggerSpike::TriggerSpike(const Variant::Read &config) : TriggerInput(config),
                                                                          smoother(NULL)
{
    Variant::Write defaultSmoother = Variant::Write::empty();
    defaultSmoother["Type"].setString("FixedTime");
    defaultSmoother["Time"].setDouble(1800.0);
    defaultSmoother["MinimumTime"].setDouble(1800.0);
    defaultSmoother["Band"].setDouble(-4.0);
    smoother = BaselineSmoother::fromConfiguration(defaultSmoother, (ValueSegment::Transfer{
            ValueSegment(FP::undefined(), FP::undefined(), Variant::Root(config))}), "Smoother");
}

ControlTrigger::TriggerSpike::~TriggerSpike()
{
    delete smoother;
}

bool ControlTrigger::TriggerSpike::evaluate(TriggerInterface *interface)
{
    Q_UNUSED(interface);
    return smoother->spike();
}

ControlTrigger::Trigger *ControlTrigger::TriggerSpike::clone() const
{ return new TriggerSpike(*this); }

bool ControlTrigger::TriggerSpike::incomingRealtime(TriggerInterface *interface,
                                                    const SequenceValue::Transfer &values)
{
    if (!TriggerInput::incomingRealtime(interface, values))
        return false;
    smoother->add(value(interface), interface->time);
    return true;
}


ControlTrigger::TriggerStable::TriggerStable(const TriggerStable &other) : TriggerInput(other),
                                                                           smoother(other.smoother
                                                                                         ->clone())
{ }

ControlTrigger::TriggerStable::TriggerStable(const Variant::Read &config) : TriggerInput(config),
                                                                            smoother(NULL)
{
    Variant::Write defaultSmoother = Variant::Write::empty();
    defaultSmoother["Type"].setString("FixedTime");
    defaultSmoother["Time"].setDouble(90.0);
    defaultSmoother["MinimumTime"].setDouble(90.0);
    defaultSmoother["RSD"].setDouble(0.001);
    smoother = BaselineSmoother::fromConfiguration(defaultSmoother, (ValueSegment::Transfer{
            ValueSegment(FP::undefined(), FP::undefined(), Variant::Root(config))}), "Smoother");
}

ControlTrigger::TriggerStable::~TriggerStable()
{
    delete smoother;
}

bool ControlTrigger::TriggerStable::evaluate(TriggerInterface *interface)
{
    Q_UNUSED(interface);
    return smoother->stable();
}

ControlTrigger::Trigger *ControlTrigger::TriggerStable::clone() const
{ return new TriggerStable(*this); }

bool ControlTrigger::TriggerStable::incomingRealtime(TriggerInterface *interface,
                                                     const SequenceValue::Transfer &values)
{
    if (!TriggerInput::incomingRealtime(interface, values))
        return false;
    smoother->add(value(interface), interface->time);
    return true;
}


ControlTrigger::TriggerHasFlags::TriggerHasFlags(const TriggerHasFlags &other) : TriggerSegmenting(
        other), input(other.input->clone()), flags(other.flags)
{ }

ControlTrigger::TriggerHasFlags::TriggerHasFlags(const Variant::Read &config) : TriggerSegmenting(
        config), input(NULL), flags(config["Flags"].toFlags())
{
    input = DynamicSequenceSelection::fromConfiguration((ValueSegment::Transfer{
            ValueSegment(FP::undefined(), FP::undefined(), Variant::Root(config))}), "Input");
    input->registerExpected({}, "rt_instant");
}

ControlTrigger::TriggerHasFlags::~TriggerHasFlags()
{
    delete input;
}

bool ControlTrigger::TriggerHasFlags::evaluate(TriggerInterface *interface)
{
    for (const auto &n : input->get(currentSegment)) {
        auto v = currentSegment.getValue(n);
        if (v.getType() != Variant::Type::Flags)
            continue;
        if (v.testFlags(flags))
            return true;
    }
    return false;
}

ControlTrigger::Trigger *ControlTrigger::TriggerHasFlags::clone() const
{ return new TriggerHasFlags(*this); }

bool ControlTrigger::TriggerHasFlags::collectUnit(const SequenceName &unit)
{ return input->registerInput(unit); }

void ControlTrigger::TriggerScript::initialize()
{
    {
        Lua::Engine::Assign assign(root, engine.global(), "print");
        auto data = callbackData;
        assign.push(std::function<void(Lua::Engine::Entry &)>([data](Lua::Engine::Entry &entry) {
            for (std::size_t i = 0, max = entry.size(); i < max; i++) {
                qCInfo(data->interface->log()) << entry[i].toOutputString();
            }
        }));
    }
    {
        Lua::Engine::Assign assign(root, engine.global(), "evaluateAt");
        auto data = callbackData;
        assign.push(std::function<void(Lua::Engine::Entry &)>([data](Lua::Engine::Entry &entry) {
            if (entry.empty())
                return;
            double requestedTime = Lua::Libs::Time::extractSingle(entry, entry.front());
            if (!FP::defined(requestedTime))
                return;
            if (!FP::defined(data->interface->nextEvaluation) ||
                    data->interface->nextEvaluation > requestedTime) {
                data->interface->nextEvaluation = requestedTime;
            }
        }));
    }
    {
        Lua::Engine::Assign assign(root, engine.global(), "evaluateIn");
        auto data = callbackData;
        assign.push(std::function<void(Lua::Engine::Entry &)>([data](Lua::Engine::Entry &entry) {
            if (entry.empty())
                return;
            double requestedTime = entry.front().toNumber();
            if (!FP::defined(requestedTime))
                return;
            if (!FP::defined(data->interface->nextEvaluation) ||
                    data->interface->nextEvaluation - data->interface->time > requestedTime) {
                data->interface->nextEvaluation = data->interface->time + requestedTime;
            }
        }));
    }

    if (!root.pushChunk(code, false)) {
        engine.clearError();
        return;
    }
    invoke = root.back();
}

ControlTrigger::TriggerScript::TriggerScript(const TriggerScript &other) : TriggerSegmenting(other),
                                                                           input(other.input
                                                                                      ->clone()),
                                                                           code(other.code),
                                                                           callbackData(
                                                                                   std::make_shared<
                                                                                           CallbackData>()),
                                                                           engine(),
                                                                           root(engine)
{
    input->registerExpected({}, "rt_instant");
    initialize();
}

ControlTrigger::TriggerScript::TriggerScript(const Variant::Read &config) : TriggerSegmenting(
        config),
                                                                            input(DynamicSequenceSelection::fromConfiguration(
                                                                                    (ValueSegment::Transfer{
                                                                                            ValueSegment(
                                                                                                    FP::undefined(),
                                                                                                    FP::undefined(),
                                                                                                    Variant::Root(
                                                                                                            config))}),
                                                                                    "Input")),
                                                                            code(config["Code"].toString()),
                                                                            callbackData(
                                                                                    std::make_shared<
                                                                                            CallbackData>()),
                                                                            engine(),
                                                                            root(engine)
{
    input->registerExpected({}, "rt_instant");
    initialize();
}

ControlTrigger::TriggerScript::~TriggerScript() = default;

bool ControlTrigger::TriggerScript::collectUnit(const SequenceName &name)
{ return input->registerInput(name); }

ControlTrigger::Trigger *ControlTrigger::TriggerScript::clone() const
{ return new TriggerScript(*this); }

bool ControlTrigger::TriggerScript::evaluate(TriggerInterface *interface)
{
    if (!invoke.isAssigned())
        return false;

    callbackData->interface = interface;

    {
        Lua::Engine::Assign assign(root, engine.global(), "data");
        assign.pushData<Lua::Libs::SequenceSegment>(currentSegment);
    }

    Lua::Engine::Call call(root, invoke);
    if (!call.execute(1)) {
        engine.clearError();
        return false;
    }
    return call.back().toBoolean();
}


static std::string assembleLoggingCategory(const std::string &loggingContext)
{
    std::string result = "cpd3.acquisition.control.trigger";
    if (!loggingContext.empty()) {
        result += '.';
        result += loggingContext;
    }
    return result;
}

ControlTrigger::ControlTrigger(const ValueSegment::Transfer &configData,
                               const std::string &loggingContext) : loggingName(
        assembleLoggingCategory(loggingContext)), log(loggingName.data()),
                                                                    terminated(false),
                                                                    state(NULL),
                                                                    realtimeEgress(NULL),
                                                                    haveEmittedRealtimeMeta(false),
                                                                    loggingEgress(NULL),
                                                                    persistentEgress(NULL),
                                                                    enabled(true),
                                                                    realtimeStateUpdated(true),
                                                                    lastExecutedTime(
                                                                            FP::undefined()),
                                                                    lastLoggingAction(),
                                                                    instrumentMeta(),
                                                                    triggerInterface(this),
                                                                    realtimeIngress(this)
{
    instrumentMeta["InstrumentCode"].setString("controltrigger");

    config.append(Configuration());
    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
    auto hit = Range::findIntersecting(configData, Time::time());
    if (hit != configData.end()) {
        if (hit->value().hash("EnableTrigger").exists())
            enabled = hit->value().hash("EnableTrigger").toBool();
        logActive = hit->value().hash("LogActiveAction").toBool();
    }
}

ControlTrigger::~ControlTrigger()
{
}

void ControlTrigger::executeAction(double executeTime, Action *action)
{
    bool isFirst = false;

    if (!FP::defined(action->activationTime) ||
            (!action->hadJustActivated &&
                    (!FP::defined(action->holdTime) ||
                            action->activationTime + action->holdTime < executeTime))) {
        isFirst = true;

        qCDebug(log) << "Action" << action->debugDescription() << "executing at"
                     << Logging::time(executeTime);

        if (!action->logEvent.isEmpty()) {
            if (state) {
                Variant::Write data = Variant::Write::empty();
                action->loggingValue(data, false);
                state->event(executeTime, action->logEvent, action->logRealtime, data);
            }
        }

        if (logActive) {
            if (persistentEgress &&
                    FP::defined(lastExecutedTime) &&
                    executeTime > lastExecutedTime) {
                persistentEgress->incomingData(
                        SequenceValue({{}, "raw", "ZACTIVE"}, lastLoggingAction, lastExecutedTime,
                                      executeTime));
            }
        }

        auto wr = lastLoggingAction.write();
        action->loggingValue(wr);
        wr.hash("Time").setDouble(executeTime);
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZLAST"}, lastLoggingAction, executeTime,
                                  FP::undefined()));
        }

        lastExecutedTime = executeTime;
        if (logActive) {
            persistentValuesUpdated();
        }
    }
    action->hadJustActivated = true;
    action->activationTime = executeTime;

    if (!isFirst && !action->continuous)
        return;

    for (const auto &add : action->setBypassFlags) {
        if (state)
            state->setBypassFlag(add);
    }
    for (const auto &add : action->clearBypassFlags) {
        if (state)
            state->clearBypassFlag(add);
    }
    for (const auto &add : action->setSystemFlags) {
        if (state)
            state->setSystemFlag(add);
    }
    for (const auto &add : action->clearSystemFlags) {
        if (state)
            state->clearSystemFlag(add);
    }
    if (FP::defined(action->flushTime)) {
        if (state)
            state->requestFlush(action->flushTime);
    }
    for (const auto &send : action->commands) {
        if (state)
            state->sendCommand(send.first, send.second);
    }
    if (action->setAddFlavors) {
        if (state)
            state->setSystemFlavors(action->addFlavors);
    }
}

SequenceValue::Transfer ControlTrigger::getPersistentValues()
{
    SequenceValue::Transfer result;
    if (!logActive)
        return result;
    if (!FP::defined(lastExecutedTime))
        return result;

    result.emplace_back(SequenceName({}, "raw", "ZACTIVE"), lastLoggingAction, lastExecutedTime,
                        FP::undefined());

    return result;
}

ControlTrigger::Action::Action()
        : commands(),
          flushTime(FP::undefined()),
          setSystemFlags(),
          clearSystemFlags(),
          setBypassFlags(),
          clearBypassFlags(),
          setAddFlavors(false),
          addFlavors(),
          logEvent(),
          logRealtime(false),
          description(),
          identifier(),
          acquireIDs(),
          releaseIDs(),
          holdTime(FP::undefined()),
          continuous(false),
          activationTime(FP::undefined()),
          hadJustActivated(false)
{ }

static QString displayFlags(const Variant::Flags &flags)
{
    QStringList sorted;
    for (const auto &add : flags) {
        sorted.push_back(QString::fromStdString(add));
    }
    std::sort(sorted.begin(), sorted.end());
    return sorted.join(";");
}

QString ControlTrigger::Action::debugDescription() const
{
    QString result;
    if (setAddFlavors) {
        if (!result.isEmpty()) result.append(',');
        result.append("Set:");
        QStringList sort = Util::to_qstringlist(addFlavors);
        std::sort(sort.begin(), sort.end());
        result.append(sort.join(";"));
    }
    for (const auto &cmd : commands) {
        if (!result.isEmpty()) result.append(',');
        if (!cmd.first.empty()) {
            result.append(QString::fromStdString(cmd.first));
            result.append('=');
        }
        QStringList sorted;
        for (const auto &add : cmd.second.read().toHash().keys()) {
            sorted.push_back(QString::fromStdString(add));
        }
        std::sort(sorted.begin(), sorted.end());
        result.append(sorted.join(";"));
    }

    if (!setSystemFlags.empty()) {
        if (!result.isEmpty()) result.append(',');
        result.append("Flags:");
        result.append(displayFlags(setSystemFlags));
    }
    if (!clearSystemFlags.empty()) {
        if (!result.isEmpty()) result.append(',');
        result.append("ClearFlags:");
        result.append(displayFlags(clearSystemFlags));
    }
    if (!setBypassFlags.empty()) {
        if (!result.isEmpty()) result.append(',');
        result.append("Bypass:");
        result.append(displayFlags(setBypassFlags));
    }
    if (!clearBypassFlags.empty()) {
        if (!result.isEmpty()) result.append(',');
        result.append("ClearBypass:");
        result.append(displayFlags(clearBypassFlags));
    }
    if (FP::defined(flushTime)) {
        if (!result.isEmpty()) result.append(',');
        result.append("Flush:");
        result.append(QString::number(flushTime));
    }
    return result;
}

void ControlTrigger::Action::loggingValue(Variant::Write &target, bool includeEvent) const
{
    if (setAddFlavors) {
        target.hash("Flavors").setFlags(addFlavors);
    }
    if (includeEvent && !logEvent.isEmpty()) {
        target.hash("Event").setString(logEvent);
    }
    if (!description.isEmpty()) {
        target.hash("Description").setString(description);
    }
    if (identifier.read().exists()) {
        target.hash("Identifier").set(identifier);
    }
}

ControlTrigger::Action *ControlTrigger::Action::clone() const
{ return new Action(*this); }

ControlTrigger::Configuration::Configuration() : start(FP::undefined()),
                                                 end(FP::undefined()),
                                                 operations(),
                                                 systemFlags(),
                                                 bypassFlags()
{ }

ControlTrigger::Configuration::~Configuration()
{
    for (QList<Operation>::iterator op = operations.begin(), endOp = operations.end();
            op != endOp;
            ++op) {
        qDeleteAll(op->triggers);
        delete op->action;
    }
}

ControlTrigger::Configuration::Configuration(const Configuration &other) : start(other.start),
                                                                           end(other.end),
                                                                           operations(),
                                                                           systemFlags(
                                                                                   other.systemFlags),
                                                                           bypassFlags(
                                                                                   other.bypassFlags)
{
    for (QList<Operation>::const_iterator op = other.operations.constBegin(),
            endOp = other.operations.constEnd(); op != endOp; ++op) {
        Operation add;
        add.action = op->action->clone();
        for (QList<Trigger *>::const_iterator trigger = op->triggers.constBegin(),
                endTrigger = op->triggers.constEnd(); trigger != endTrigger; ++trigger) {
            add.triggers.append((*trigger)->clone());
        }
        operations.append(add);
    }
}

ControlTrigger::Configuration &ControlTrigger::Configuration::operator=(const Configuration &other)
{
    if (&other == this) return *this;
    start = other.start;
    end = other.end;
    systemFlags = other.systemFlags;
    bypassFlags = other.bypassFlags;

    for (QList<Operation>::iterator op = operations.begin(), endOp = operations.end();
            op != endOp;
            ++op) {
        qDeleteAll(op->triggers);
        delete op->action;
    }
    operations.clear();

    for (QList<Operation>::const_iterator op = other.operations.constBegin(),
            endOp = other.operations.constEnd(); op != endOp; ++op) {
        Operation add;
        add.action = op->action->clone();
        for (QList<Trigger *>::const_iterator trigger = op->triggers.constBegin(),
                endTrigger = op->triggers.constEnd(); trigger != endTrigger; ++trigger) {
            add.triggers.append((*trigger)->clone());
        }
        operations.append(add);
    }
    return *this;
}

ControlTrigger::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          operations(),
          systemFlags(other.systemFlags),
          bypassFlags(other.bypassFlags)
{
    for (QList<Operation>::const_iterator op = other.operations.constBegin(),
            endOp = other.operations.constEnd(); op != endOp; ++op) {
        Operation add;
        add.action = op->action->clone();
        for (QList<Trigger *>::const_iterator trigger = op->triggers.constBegin(),
                endTrigger = op->triggers.constEnd(); trigger != endTrigger; ++trigger) {
            add.triggers.append((*trigger)->clone());
        }
        operations.append(add);
    }
}

ControlTrigger::Configuration::Configuration(const ValueSegment &other, double s, double e) : start(
        s), end(e), operations(), systemFlags(), bypassFlags()
{
    setFromSegment(other);
}

ControlTrigger::Configuration::Configuration(const Configuration &under, const ValueSegment &over,
                                             double s,
                                             double e) : start(s),
                                                         end(e),
                                                         operations(),
                                                         systemFlags(under.systemFlags),
                                                         bypassFlags(under.bypassFlags)
{
    for (QList<Operation>::const_iterator op = under.operations.constBegin(),
            endOp = under.operations.constEnd(); op != endOp; ++op) {
        Operation add;
        add.action = op->action->clone();
        for (QList<Trigger *>::const_iterator trigger = op->triggers.constBegin(),
                endTrigger = op->triggers.constEnd(); trigger != endTrigger; ++trigger) {
            add.triggers.append((*trigger)->clone());
        }
        operations.append(add);
    }
    setFromSegment(over);
}

void ControlTrigger::Configuration::handleContaminate(const Variant::Read &config,
                                                      ControlTrigger::Action *action)
{
    switch (config.getType()) {
    case Variant::Type::String: {
        auto check = config.toString();
        if (!Variant::Composite::isContaminationFlag(check)) {
            check = "Contaminate" + check;
        }
        action->setSystemFlags.insert(check);

        Variant::Root meta;
        meta["Description"].setString("Data contaminated due to triggered event");
        meta["Origin"].toArray().after_back().setString("control_trigger");
        meta["Bits"].setInt64(0x000F);
        Util::insert_or_assign(systemFlags, std::move(check), std::move(meta));
        break;
    }
    case Variant::Type::Flags:
        for (auto check : config.toFlags()) {
            if (!Variant::Composite::isContaminationFlag(check)) {
                check = "Contaminate" + check;
            }
            action->setSystemFlags.insert(check);

            Variant::Root meta;
            meta["Description"].setString("Data contaminated due to triggered event");
            meta["Origin"].toArray().after_back().setString("control_trigger");
            meta["Bits"].setInt64(0x000F);
            Util::insert_or_assign(systemFlags, std::move(check), std::move(meta));
        }
        break;
    case Variant::Type::Hash: {
        for (auto add : config.toHash()) {
            if (add.first.empty())
                continue;
            Variant::Flag check = add.first;
            if (!Variant::Composite::isContaminationFlag(check)) {
                check = "Contaminate" + check;
            }
            action->setSystemFlags.insert(check);

            Variant::Root meta(add.second);
            meta["Origin"].toArray().after_back().setString("control_trigger");
            qint64 bits(meta["Bits"].toInt64());
            if (!INTEGER::defined(bits)) {
                meta["Bits"].setInt64(0x000F);
            } else if (!(bits & Q_INT64_C(0xF))) {
                bits |= Q_INT64_C(0xF);
                meta["Bits"].setInt64(bits);
            }
            if (!meta["Description"].exists()) {
                meta["Description"].setString("Data contaminated due to triggered event");
            }

            Util::insert_or_assign(systemFlags, std::move(check), std::move(meta));
        }
        break;
    }
    case Variant::Type::Boolean:
        if (config.toBool()) {
            action->setSystemFlags.insert("Contaminated");
        } else {
            action->clearSystemFlags.insert("Contaminated");
        }
        break;
    default:
        break;
    }
}

void ControlTrigger::Configuration::handleUncontaminate(const Variant::Read &config,
                                                        ControlTrigger::Action *action)
{
    switch (config.getType()) {
    case Variant::Type::Boolean:
        if (config.toBool()) {
            action->clearSystemFlags.insert("Contaminated");
        } else {
            action->setSystemFlags.insert("Contaminated");
        }
        break;
    default:
        for (const auto &add : config.toChildren().keys()) {
            if (add.empty())
                continue;
            Variant::Flag check = add;
            if (!Variant::Composite::isContaminationFlag(check)) {
                check = "Contaminate" + check;
            }
            action->clearSystemFlags.insert(std::move(check));
        }
        break;
    }
}

void ControlTrigger::Configuration::handleSetFlags(const Variant::Read &config,
                                                   ControlTrigger::Action *action)
{
    switch (config.getType()) {
    case Variant::Type::String: {
        const auto &add = config.toString();
        action->setSystemFlags.insert(add);

        Variant::Root meta;
        meta["Origin"].toArray().after_back().setString("control_cycle");
        Util::insert_or_assign(systemFlags, add, std::move(meta));
        break;
    }
    case Variant::Type::Flags: {
        for (const auto &add : config.toFlags()) {
            action->setSystemFlags.insert(add);

            Variant::Root meta;
            meta["Origin"].toArray().after_back().setString("control_cycle");
            Util::insert_or_assign(systemFlags, add, std::move(meta));
        }
        break;
    }
    case Variant::Type::Hash: {
        for (const auto &add : config.toHash()) {
            if (add.first.empty())
                continue;
            action->setSystemFlags.insert(add.first);

            Variant::Root meta(add.second);
            meta["Origin"].toArray().after_back().setString("control_cycle");
            Util::insert_or_assign(systemFlags, add.first, std::move(meta));
        }
        break;
    }
    default:
        break;
    }
}

void ControlTrigger::Configuration::handleClearFlags(const Variant::Read &config,
                                                     ControlTrigger::Action *action)
{
    Util::merge(config.toChildren().keys(), action->clearSystemFlags);
}

void ControlTrigger::Configuration::handleSetBypass(const Variant::Read &config,
                                                    ControlTrigger::Action *action)
{
    switch (config.getType()) {
    case Variant::Type::String: {
        const auto &check = config.toString();
        action->setBypassFlags.insert(check);

        Variant::Root meta(Variant::Type::Hash);
        Util::insert_or_assign(bypassFlags, check, std::move(meta));
        break;
    }
    case Variant::Type::Flags: {
        for (const auto &add : config.toFlags()) {
            action->setBypassFlags.insert(add);

            Variant::Root meta(Variant::Type::Hash);
            Util::insert_or_assign(bypassFlags, add, std::move(meta));
        }
        break;
    }
    case Variant::Type::Hash: {
        for (auto add : config.toHash()) {
            if (add.first.empty())
                continue;
            action->setBypassFlags.insert(add.first);

            Variant::Root meta(add.second);
            meta["Origin"].toArray().after_back().setString("control_cycle");
            Util::insert_or_assign(bypassFlags, add.first, std::move(meta));
        }
        break;
    }
    case Variant::Type::Boolean: {
        if (config.toBool()) {
            action->setBypassFlags.insert("Bypass");
            Variant::Root meta(Variant::Type::Hash);
            Util::insert_or_assign(bypassFlags, "Bypass", std::move(meta));
        } else {
            action->clearBypassFlags.insert("Bypass");
        }
    }
    default:
        break;
    }
}

void ControlTrigger::Configuration::handleClearBypass(const Variant::Read &config,
                                                      ControlTrigger::Action *action)
{
    switch (config.getType()) {
    case Variant::Type::Boolean:
        if (!config.toBool()) {
            action->setBypassFlags.insert("Bypass");
            Variant::Root meta(Variant::Type::Hash);
            Util::insert_or_assign(bypassFlags, "Bypass", std::move(meta));
        } else {
            action->clearBypassFlags.insert("Bypass");
        }
        break;
    default:
        Util::merge(config.toChildren().keys(), action->clearBypassFlags);
        break;
    }
}

ControlTrigger::Action *ControlTrigger::Configuration::createAction(double defaultFlush,
                                                                    const std::unordered_map<
                                                                            std::string,
                                                                            Variant::Root> &baselineCommands,
                                                                    const Variant::Read &config)
{
    ControlTrigger::Action *action = new ControlTrigger::Action;

    action->commands = baselineCommands;
    if (config["InstrumentCommands"].exists()) {
        for (auto add : config["InstrumentCommands"].toHash()) {
            if (add.first.empty())
                continue;

            auto merge = action->commands.find(add.first);
            if (merge == action->commands.end()) {
                action->commands.emplace(add.first, Variant::Root(add.second));
                continue;
            }

            merge->second = Variant::Root::overlay(merge->second, Variant::Root(add.second));
        }
    }
    if (config["Commands"].exists()) {
        auto merge = action->commands.find(std::string());
        if (merge == action->commands.end()) {
            action->commands.emplace(std::string(), Variant::Root(config["Commands"]));
        } else {
            merge->second =
                    Variant::Root::overlay(merge->second, Variant::Root(config["Commands"]));
        }
    }

    if (config["FlushTime"].exists()) {
        action->flushTime = config["FlushTime"].toDouble();
    } else {
        action->flushTime = defaultFlush;
    }

    if (config["Description"].exists()) {
        action->description = config["Description"].toDisplayString();
    }
    if (config["Identifier"].exists()) {
        action->identifier.write().set(config["Identifier"]);
    }

    if (config["Contaminate"].exists())
        handleContaminate(config["Contaminate"], action);
    if (config["Contaminated"].exists())
        handleContaminate(config["Contaminated"], action);
    if (config["Uncontaminate"].exists())
        handleUncontaminate(config["Uncontaminate"], action);
    if (config["UnContaminate"].exists())
        handleUncontaminate(config["UnContaminate"], action);
    if (config["Uncontaminated"].exists())
        handleUncontaminate(config["Uncontaminated"], action);
    if (config["UnContaminated"].exists())
        handleUncontaminate(config["UnContaminated"], action);
    if (config["ClearContaminate"].exists())
        handleUncontaminate(config["ClearContaminate"], action);
    if (config["ClearContamination"].exists())
        handleUncontaminate(config["ClearContamination"], action);
    if (config["ClearContaminated"].exists())
        handleUncontaminate(config["ClearContaminated"], action);

    if (config["SetFlags"].exists())
        handleSetFlags(config["SetFlags"], action);
    if (config["SetFlag"].exists())
        handleSetFlags(config["SetFlag"], action);
    if (config["ClearFlags"].exists())
        handleClearFlags(config["ClearFlags"], action);
    if (config["ClearFlag"].exists())
        handleClearFlags(config["ClearFlag"], action);

    if (config["Bypass"].exists())
        handleSetBypass(config["Bypass"], action);
    if (config["Bypassed"].exists())
        handleSetBypass(config["Bypassed"], action);
    if (config["Unbypass"].exists())
        handleClearBypass(config["Unbypass"], action);
    if (config["UnBypass"].exists())
        handleClearBypass(config["UnBypass"], action);
    if (config["ClearBypass"].exists())
        handleClearBypass(config["ClearBypass"], action);


    if (config["SetFlavors"].exists()) {
        action->setAddFlavors = true;
        action->addFlavors = SequenceName::toFlavors(config["SetFlavors"]);
    }
    if (config["SetCut"].exists()) {
        auto str = config["SetCut"].toString();
        if (Util::equal_insensitive(str, "pm1", "1", "fine")) {
            action->addFlavors.insert(SequenceName::flavor_pm1);
        } else if (Util::equal_insensitive(str, "pm10", "10", "coarse")) {
            action->addFlavors.insert(SequenceName::flavor_pm10);
        } else if (Util::equal_insensitive(str, "pm25", "25", "2.5", "pm2.5")) {
            action->addFlavors.insert(SequenceName::flavor_pm25);
        } else {
            action->addFlavors.erase(SequenceName::flavor_pm1);
            action->addFlavors.erase(SequenceName::flavor_pm10);
            action->addFlavors.erase(SequenceName::flavor_pm25);
        }
        action->setAddFlavors = true;
    }

    if (config["Event"].exists())
        action->logEvent = config["Event"].toQString();
    if (config["EventRealtime"].exists())
        action->logRealtime = config["EventRealtime"].toBool();

    if (config["Continuous"].exists())
        action->continuous = config["Continuous"].toBool();

    if (config["Dependencies/Acquire"].exists())
        action->acquireIDs = convertIDs(config["Dependencies/Acquire"]);
    if (config["Dependencies/Release"].exists())
        action->releaseIDs = convertIDs(config["Dependencies/Release"]);
    if (config["Dependencies/Hold"].exists())
        action->holdTime = config["Dependencies/Hold"].toDouble();

    return action;
}

ControlTrigger::Trigger *ControlTrigger::Configuration::createTrigger(const Variant::Read &config)
{
    switch (config.getType()) {
    case Variant::Type::Boolean:
        return new TriggerAlways(config);
    case Variant::Type::Empty:
        break;
    default: {
        const auto &t = config["Type"].toString();
        if (Util::equal_insensitive(t, "and", "composite"))
            return new TriggerAND(config);
        else if (Util::equal_insensitive(t, "or", "any"))
            return new TriggerAND(config);
        else if (Util::equal_insensitive(t, "always"))
            return new TriggerAlways(config);
        else if (Util::equal_insensitive(t, "never"))
            return new TriggerAlways(config, false);
        else if (Util::equal_insensitive(t, "threshold", "limit"))
            return new TriggerThreshold(config);
        else if (Util::equal_insensitive(t, "flags", "flag"))
            return new TriggerHasFlags(config);
        else if (Util::equal_insensitive(t, "risk", "spike"))
            return new TriggerSpike(config);
        else if (Util::equal_insensitive(t, "stable", "smoother"))
            return new TriggerStable(config);
        else if (Util::equal_insensitive(t, "range"))
            return new TriggerRange(config);
        else if (Util::equal_insensitive(t, "angle", "wind", "winds"))
            return new TriggerAngle(config);
        else if (Util::equal_insensitive(t, "script"))
            return new TriggerScript(config);
        return new TriggerThreshold(config);
    }
    }
    return NULL;
}

QList<ControlTrigger::Trigger *> ControlTrigger::Configuration::createTriggers(const Variant::Read &config)
{
    QList<ControlTrigger::Trigger *> result;
    for (auto add : config.toChildren()) {
        ControlTrigger::Trigger *t = createTrigger(add);
        if (t == NULL)
            continue;
        result.append(t);
    }
    return result;
}

void ControlTrigger::Configuration::setFromSegment(const ValueSegment &config)
{
    double defaultFlush = config["DefaultFlushTime"].toDouble();

    std::unordered_map<std::string, CPD3::Data::Variant::Root> baselineCommands;
    if (config["BaselineInstrumentCommands"].exists()) {
        for (auto add : config["BaselineInstrumentCommands"].toHash()) {
            if (add.first.empty())
                continue;
            baselineCommands.emplace(add.first, Variant::Root(add.second));
        }
    }
    if (config["BaselineCommands"].exists()) {
        baselineCommands.emplace(std::string(), Variant::Root(config["BaselineCommands"]));
    }

    if (config["Actions"].exists()) {
        for (QList<Operation>::iterator op = operations.begin(), endOp = operations.end();
                op != endOp;
                ++op) {
            qDeleteAll(op->triggers);
            delete op->action;
        }
        operations.clear();

        for (auto add : config["Actions"].toChildren()) {
            ControlTrigger::Action *action = createAction(defaultFlush, baselineCommands, add);
            if (!action)
                continue;
            ControlTrigger::Operation op;
            op.action = action;
            op.triggers = createTriggers(add.hash("Triggers"));
            if (op.triggers.isEmpty()) {
                ControlTrigger::Trigger *trigger = createTrigger(add.hash("Trigger"));
                if (!trigger) {
                    delete action;
                    continue;
                }
                op.triggers.append(trigger);
            }
            operations.append(op);
        }
    }
}


ControlTrigger::TriggerInterface::TriggerInterface(ControlTrigger *ct) : control(ct),
                                                                         time(FP::undefined())
{ }

template<typename T>
static bool activationTimeSort(const T &a, const T &b)
{ return a->activationTime < b->activationTime; }

bool ControlTrigger::TriggerInterface::canTrigger(const std::unordered_set<std::string> &requireIDs,
                                                  const std::unordered_set<std::string> &excludeIDs)
{
    if (requireIDs.empty() && excludeIDs.empty())
        return true;

    Q_ASSERT(!control->config.isEmpty());
    Q_ASSERT(FP::defined(time));
    std::vector<ControlTrigger::Action *> sortActions;
    for (const auto &op : control->config.first().operations) {
        if (!FP::defined(op.action->activationTime))
            continue;
        sortActions.emplace_back(op.action);
    }
    std::stable_sort(sortActions.begin(), sortActions.end(),
                activationTimeSort<ControlTrigger::Action *>);

    std::unordered_set<std::string> activeIDs;
    std::unordered_set<std::string> transitoryIDs;
    for (auto act : sortActions) {
        Util::merge(act->acquireIDs, activeIDs);
        for (const auto &rel : act->releaseIDs) {
            activeIDs.erase(rel);
        }

        if (FP::defined(act->holdTime) && (act->activationTime + act->holdTime) >= time) {
            Util::merge(act->acquireIDs, transitoryIDs);
            Util::merge(act->releaseIDs, transitoryIDs);
        }
    }

    for (const auto &check : requireIDs) {
        if (!activeIDs.count(check))
            return false;
        if (transitoryIDs.count(check))
            return false;
    }
    for (const auto &check : excludeIDs) {
        if (activeIDs.count(check))
            return false;
        if (transitoryIDs.count(check))
            return false;
    }

    return true;
}

void ControlTrigger::advanceTime(double executeTime)
{
    if (!FP::defined(executeTime))
        return;

    if (!haveEmittedRealtimeMeta && realtimeEgress != NULL) {
        haveEmittedRealtimeMeta = true;
        realtimeEgress->incomingData(buildRealtimeMeta(executeTime));
    }
    if (loggingEgress != NULL) {
        if (logActive) {
            loggingEgress->incomingData(buildLogMeta(executeTime));
        }
        loggingEgress->endData();
        loggingEgress = NULL;
    }

    if (realtimeStateUpdated && realtimeEgress) {
        realtimeStateUpdated = false;

        Variant::Root flags(Variant::Type::Flags);
        if (!enabled)
            flags.write().applyFlag("Disabled");
        realtimeEgress->incomingData(
                SequenceValue({{}, "raw", "F1"}, std::move(flags), executeTime, FP::undefined()));
    }

    if (!Range::intersectShift(config, executeTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    if (config.isEmpty())
        return;
    if (!enabled)
        return;

    triggerInterface.time = executeTime;
    triggerInterface.nextEvaluation = FP::undefined();
    for (QList<Operation>::iterator op = config.first().operations.begin(),
            endOp = config.first().operations.end(); op != endOp; ++op) {
        bool active = false;
        for (QList<Trigger *>::iterator tr = op->triggers.begin(), endTr = op->triggers.end();
                tr != endTr;
                ++tr) {
            if (!(*tr)->canTrigger(&triggerInterface))
                continue;
            if ((*tr)->evaluate(&triggerInterface)) {
                if (!(*tr)->inverted())
                    active = true;
            } else if ((*tr)->inverted()) {
                active = true;
            }
        }
        if (active) {
            executeAction(executeTime, op->action);
        } else {
            op->action->hadJustActivated = false;
        }
    }

    if (FP::defined(triggerInterface.nextEvaluation)) {
        if (state != NULL)
            state->requestDataWakeup(triggerInterface.nextEvaluation);
        triggerInterface.nextEvaluation = FP::undefined();
    }
}

SequenceValue::Transfer ControlTrigger::buildLogMeta(double time)
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("control_trigger");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());

    result.emplace_back(SequenceName({}, "raw_meta", "ZACTIVE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataHash("Description").setString("Currently active control point");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
    result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataHashChild("Flavors")
          .metadataFlags("Description")
          .setString("Flavors added by the control point");
    result.back().write().metadataHashChild("Description")
          .metadataString("Description")
          .setString("The description of the control point");
    result.back().write().metadataHashChild("Identifier")
          .metadataHash("Description")
          .setString("The control point identifier data");
    result.back().write().metadataHashChild("Identifier")
          .metadataHash("ArbitraryStructure")
          .setBool(true);
    result.back().write().metadataHashChild("Event")
          .metadataString("Description")
          .setString("The text of the acquisition system event logged on execution");
    return result;
}

SequenceValue::Transfer ControlTrigger::buildRealtimeMeta(double time)
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("control_trigger");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());


    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);
    result.back().write().metadataFlags("Realtime").hash("Hide").setBool(true);
    result.back().write().metadataFlags("Realtime").hash("Persistent").setBool(true);

    result.back().write()
          .metadataSingleFlag("Disabled")
          .hash("Description")
          .setString("Trigger execution disabled");
    result.back()
          .write()
          .metadataSingleFlag("Disabled")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("control_trigger");


    result.emplace_back(SequenceName({}, "raw_meta", "ZLAST"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataHash("Description").setString("Last executed control point");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
    result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataHash("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataHash("Realtime").hash("Hide").setBool(true);
    result.back().write().metadataHash("Realtime").hash("Name").setString("Last");
    result.back().write()
          .metadataHash("Realtime")
          .hash("HashFormat")
          .setString("${TIME|Time} ${FLAGS|Flavors|,|uc}");
    result.back().write().metadataHashChild("Flavors")
          .metadataFlags("Description")
          .setString("Flavors added by the control point");
    result.back().write().metadataHashChild("Description")
          .metadataString("Description")
          .setString("The description of the control point");
    result.back().write().metadataHashChild("Identifier")
          .metadataHash("Description")
          .setString("The control point identifier data");
    result.back().write().metadataHashChild("Identifier")
          .metadataHash("ArbitraryStructure")
          .setBool(true);
    result.back().write().metadataHashChild("Event")
          .metadataString("Description")
          .setString("The text of the acquisition system event logged on execution");

    return result;
}

SequenceMatch::Composite ControlTrigger::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "ZLAST");
    sel.append({}, {}, "ZACTIVE");
    return sel;
}

void ControlTrigger::incomingCommand(const Variant::Read &command)
{
    if (command.hash("EnableTrigger").exists()) {
        enabled = true;
        realtimeStateUpdated = true;
        qCDebug(log) << "Trigger enable command received";
        generalStatusUpdated();
        if (state != NULL)
            state->requestDataWakeup(0.0);
    }

    if (command.hash("DisableTrigger").exists()) {
        enabled = false;
        realtimeStateUpdated = false;
        qCDebug(log) << "Trigger disable command received";
        generalStatusUpdated();
        if (state != NULL)
            state->requestDataWakeup(0.0);
    }
}

Variant::Root ControlTrigger::getCommands()
{
    Variant::Root result;

    result["EnableTrigger"].hash("DisplayName").setString("Enable trigger execution");
    result["EnableTrigger"].hash("ToolTip")
                           .setString(
                                   "Enable the execution of this trigger set.  When enabled all triggers will execute normally.");
    result["EnableTrigger"].hash("Include").array(0).hash("Type").setString("Flags");
    result["EnableTrigger"].hash("Include").array(0).hash("Flags").setFlags({"Disabled"});
    result["EnableTrigger"].hash("Include").array(0).hash("Variable").setString("F1");

    result["DisableTrigger"].hash("DisplayName").setString("Disable trigger execution");
    result["DisableTrigger"].hash("ToolTip")
                            .setString(
                                    "Disable the execution of this trigger set.  While disabled no triggers will be executed.");
    result["DisableTrigger"].hash("Exclude").array(0).hash("Type").setString("Flags");
    result["DisableTrigger"].hash("Exclude").array(0).hash("Flags").setFlags({"Disabled"});
    result["DisableTrigger"].hash("Exclude").array(0).hash("Variable").setString("F1");

    return result;
}


void ControlTrigger::setControlStream(AcquisitionControlStream *controlStream)
{ Q_UNUSED(controlStream); }

void ControlTrigger::setState(AcquisitionState *state)
{ this->state = state; }

void ControlTrigger::setRealtimeEgress(StreamSink *egress)
{
    realtimeEgress = egress;
    haveEmittedRealtimeMeta = false;
}

void ControlTrigger::setLoggingEgress(StreamSink *egress)
{
    loggingEgress = egress;
}

void ControlTrigger::setPersistentEgress(Data::StreamSink *egress)
{
    persistentEgress = egress;
}

Variant::Root ControlTrigger::getSystemFlagsMetadata()
{
    Variant::Root result;
    for (const auto &cfg : config) {
        for (const auto &add : cfg.systemFlags) {
            result.write().metadataSingleFlag(add.first).set(add.second);
        }
    }
    return result;
}

Variant::Root ControlTrigger::getSourceMetadata()
{ return instrumentMeta; }

AcquisitionInterface::GeneralStatus ControlTrigger::getGeneralStatus()
{
    if (!enabled)
        return AcquisitionInterface::GeneralStatus::Disabled;
    return AcquisitionInterface::GeneralStatus::Normal;
}

void ControlTrigger::incomingData(const Util::ByteView &data,
                                  double time,
                                  AcquisitionInterface::IncomingDataType type)
{
    Q_UNUSED(data);
    Q_UNUSED(type);
    advanceTime(time);
    if (state != NULL)
        state->realtimeAdvanced(time);
}

void ControlTrigger::incomingControl(const Util::ByteView &data,
                                     double time,
                                     AcquisitionInterface::IncomingDataType type)
{
    Q_UNUSED(data);
    Q_UNUSED(type);
    advanceTime(time);
    if (state != NULL)
        state->realtimeAdvanced(time);
}

void ControlTrigger::incomingTimeout(double time)
{
    advanceTime(time);
    if (state != NULL)
        state->realtimeAdvanced(time);
}

void ControlTrigger::incomingAdvance(double time)
{
    advanceTime(time);
    if (state != NULL)
        state->realtimeAdvanced(time);
}

StreamSink *ControlTrigger::getRealtimeIngress()
{ return &realtimeIngress; }

ControlTrigger::RealtimeIngress::RealtimeIngress(ControlTrigger *ct) : control(ct)
{ }

ControlTrigger::RealtimeIngress::~RealtimeIngress()
{ }

void ControlTrigger::RealtimeIngress::incomingData(const SequenceValue::Transfer &values)
{
    if (values.empty())
        return;
    if (control->config.empty())
        return;

    control->triggerInterface.nextEvaluation = FP::undefined();
    if (Range::compareStart(control->triggerInterface.time, values.back().getStart()) < 0)
        control->triggerInterface.time = values.back().getStart();

    bool updated = false;
    for (auto &op : control->config.first().operations) {
        for (auto tr : op.triggers) {
            if (tr->incomingRealtime(&control->triggerInterface, values))
                updated = true;
        }
    }
    if (updated) {
        if (control->state)
            control->state->requestDataWakeup(0.0);
    }
}

void ControlTrigger::RealtimeIngress::endData()
{ }

AcquisitionInterface::AutomaticDefaults ControlTrigger::getDefaults()
{
    AutomaticDefaults result;
    result.name = "TRIGGER$2";
    return result;
}

bool ControlTrigger::isCompleted()
{ return terminated; }

void ControlTrigger::signalTerminate()
{
    if (realtimeEgress != NULL) {
        realtimeEgress->endData();
        realtimeEgress = NULL;
    }
    if (loggingEgress != NULL) {
        loggingEgress->endData();
        loggingEgress = NULL;
    }
    if (persistentEgress != NULL) {
        persistentEgress->endData();
        persistentEgress = NULL;
    }
    terminated = true;
    completed();
}


std::unique_ptr<
        AcquisitionInterface> ControlTriggerComponent::createAcquisitionPassive(const ComponentOptions &,
                                                                                const std::string &loggingContext)
{
    qWarning() << "Option based creation is not supported";
    return std::unique_ptr<AcquisitionInterface>(
            new ControlTrigger(ValueSegment::Transfer(), loggingContext));
}

std::unique_ptr<
        AcquisitionInterface> ControlTriggerComponent::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext)
{
    return std::unique_ptr<AcquisitionInterface>(new ControlTrigger(config, loggingContext));
}

std::unique_ptr<
        AcquisitionInterface> ControlTriggerComponent::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext)
{ return {}; }
