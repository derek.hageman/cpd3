/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLoggingCategory>

#include "sync_client.hxx"


Q_LOGGING_CATEGORY(log_component_sync_client, "cpd3.component.sync.client", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Sync;
using namespace CPD3::Data;

SyncClient::SyncClient(const SequenceName::Component &station,
                       const QString &profile,
                       const ComponentOptions &options)
        : Client(station, profile), station(station), profile(profile), runUpdate(true)
{
    if (options.isSet("update")) {
        runUpdate = qobject_cast<ComponentOptionBoolean *>(options.get("update"))->get();
    }
}

SyncClient::~SyncClient()
{ }

void SyncClient::run()
{
    Client::run();
    if (runUpdate)
        invokeUpdated();
}

void SyncClient::executeAction(CPD3Action *action)
{
    if (action == NULL) {
        qCWarning(log_component_sync_client) << "Unable to create action";
        return;
    }

    action->feedback.forward(feedback);
    connect(this, SIGNAL(terminateRequested()), action, SLOT(signalTerminate()),
            Qt::DirectConnection);

    action->start();
    action->wait();
    delete action;
}

void SyncClient::invokeUpdated()
{
    feedback.emitStage(tr("Checking for updated data"),
                       tr("The system is starting the passed data update."), false);

    QObject *component = ComponentLoader::create("update_passed");
    if (component == NULL) {
        qCWarning(log_component_sync_client) << "Can't load updated passed component";
        return;
    }

    ActionComponentTime *actionComponentTime;
    if (!(actionComponentTime = qobject_cast<ActionComponentTime *>(component))) {
        qCWarning(log_component_sync_client) << "Updated passed component is not a time action";
        return;
    }

    if (actionComponentTime->actionAllowStations() < 1 ||
            actionComponentTime->actionRequireStations() > 1) {
        qCWarning(log_component_sync_client) <<
                                             "Updated passed component does not accept the number of passed stations";
        return;
    }

    ComponentOptions o = actionComponentTime->getOptions();
    ComponentOptionStringSet
            *profileOption = qobject_cast<ComponentOptionStringSet *>(o.get("profile"));
    if (profileOption) {
        profileOption->set(QSet<QString>() << profile);
    } else {
        qCWarning(log_component_sync_client) << "Unable to restrict profiles to update passed";
    }

    if (auto detachedOption = qobject_cast<ComponentOptionBoolean *>(o.get("detached"))) {
        detachedOption->set(true);
    }

    executeAction(actionComponentTime->createTimeAction(o, {station}));
}

void SyncClient::signalTerminate()
{
    Client::signalTerminate();
    emit terminateRequested();
}


ComponentOptions SyncClientComponent::getOptions()
{
    ComponentOptions options;

    options.add("profile",
                new ComponentOptionSingleString(tr("profile", "name"), tr("Synchronize profile"),
                                                tr("This is the authorization profile to synchronize.  The profile "
                                                           "controls what data are synchronized with the remote server.  "
                                                           "Consult the station configuration and authorization for available profiles."),
                                                tr("aerosol")));

    options.add("update", new ComponentOptionBoolean(tr("update", "name"), tr("Enable data update"),
                                                     tr("When disabled, this causes the immediate data update to be "
                                                      "skipped.  The result is that the clean and averaged data are "
                                                      "not updated until the next time the station tasks are run at "
                                                      "the appropriate level.  This normally means that the data will "
                                                      "not be updated until the following night when the execution tasks "
                                                      "run automatically."), tr("Enabled")));

    return options;
}

QList<ComponentExample> SyncClientComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Run synchronization", "default example name"),
                                     tr("This will run synchronization using the default profile.")));

    return examples;
}

int SyncClientComponent::actionAllowStations()
{ return 1; }

int SyncClientComponent::actionRequireStations()
{ return 1; }

CPD3Action *SyncClientComponent::createAction(const ComponentOptions &options,
                                              const std::vector<std::string> &stations)
{
    if (stations.empty())
        return NULL;

    QString auth(QString::fromStdString(SequenceName::impliedProfile()));
    if (options.isSet("profile")) {
        auth = qobject_cast<ComponentOptionSingleString *>(options.get("profile"))->get().toLower();
    }

    return new SyncClient(stations.front(), auth, options);
}
