/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <cctype>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/csv.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/wavelength.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_rmy_wind86xxx.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;


AcquireRMYWind86xxx::Configuration::Configuration() : start(FP::undefined()),
                                                      end(FP::undefined()),
                                                      strictMode(true),
                                                      reportInterval(1.0),
                                                      address(0)
{ }

AcquireRMYWind86xxx::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          strictMode(other.strictMode),
          reportInterval(other.reportInterval),
          address(other.address)
{ }

AcquireRMYWind86xxx::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s), end(e), strictMode(true), reportInterval(1.0), address(0)
{
    setFromSegment(other);
}

AcquireRMYWind86xxx::Configuration::Configuration(const Configuration &under,
                                                  const ValueSegment &over,
                                                  double s,
                                                  double e) : start(s),
                                                              end(e),
                                                              strictMode(under.strictMode),
                                                              reportInterval(under.reportInterval),
                                                              address(under.address)
{
    setFromSegment(over);
}

void AcquireRMYWind86xxx::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["StrictMode"].exists())
        strictMode = config["StrictMode"].toBool();

    {
        double v = config["ReportInterval"].toDouble();
        if (FP::defined(v) && v > 0.0)
            reportInterval = v;
    }

    {
        const auto &addr = config["Address"].toString();
        if (!addr.empty()) {
            address = addr[0];
        }
    }
}


void AcquireRMYWind86xxx::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("RMY");
    instrumentMeta["Model"].setString("86xxx");

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    realtimeStateUpdated = true;
}

AcquireRMYWind86xxx::AcquireRMYWind86xxx(const ValueSegment::Transfer &configData,
                                         const std::string &loggingContext) : FramedInstrument(
        "rmy86xxx", loggingContext),
                                                                              lastRecordTime(
                                                                                      FP::undefined()),
                                                                              autoprobeStatus(
                                                                                      AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                              responseState(
                                                                                      RESP_PASSIVE_WAIT)
{
    setDefaultInvalid();
    config.emplace_back();
    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
}

void AcquireRMYWind86xxx::setToAutoprobe()
{
    responseState = RESP_AUTOPROBE_PASSIVE_INITIALIZE;
    autoprobeValidRecords = 0;
}

void AcquireRMYWind86xxx::setToInteractive()
{ responseState = RESP_INTERACTIVE_INITIALIZE; }


ComponentOptions AcquireRMYWind86xxxComponent::getBaseOptions()
{
    ComponentOptions options;

    return options;
}

AcquireRMYWind86xxx::AcquireRMYWind86xxx(const ComponentOptions &options,
                                         const std::string &loggingContext) : FramedInstrument(
        "wxt5xx", loggingContext),
                                                                              lastRecordTime(
                                                                                      FP::undefined()),
                                                                              autoprobeStatus(
                                                                                      AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                              responseState(
                                                                                      RESP_PASSIVE_WAIT)
{
    setDefaultInvalid();
    config.emplace_back();
}

AcquireRMYWind86xxx::~AcquireRMYWind86xxx() = default;

void AcquireRMYWind86xxx::logValue(double startTime,
                                   double endTime,
                                   SequenceName::Component name,
                                   Variant::Root &&value)
{
    if (!loggingEgress && !realtimeEgress) return;
    SequenceValue dv({{}, "raw", std::move(name)}, std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        if (FP::defined(startTime))
            loggingEgress->incomingData(std::move(dv));
        return;
    }
    if (loggingEgress && FP::defined(startTime))
        loggingEgress->incomingData(dv);
    if (FP::defined(endTime)) {
        dv.setStart(endTime);
        dv.setEnd(endTime + 1.0);
    } else if (FP::defined(startTime)) {
        dv.setEnd(startTime + 1.0);
    }
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireRMYWind86xxx::realtimeValue(double time,
                                        SequenceName::Component name,
                                        Variant::Root &&value)
{
    if (!realtimeEgress) return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

SequenceValue::Transfer AcquireRMYWind86xxx::buildLogMeta(double time) const
{
    Q_ASSERT(!config.empty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_rmy_wind86xxx");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());

    result.emplace_back(SequenceName({}, "raw_meta", "WS"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("m/s");
    result.back().write().metadataReal("Description").setString("Wind speed");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("Vector2D");
    result.back()
          .write()
          .metadataReal("Smoothing")
          .hash("Parameters")
          .hash("Direction")
          .setString("WD");
    result.back()
          .write()
          .metadataReal("Smoothing")
          .hash("Parameters")
          .hash("Magnitude")
          .setString("WS");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Wind Speed"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "WD"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000.0");
    result.back().write().metadataReal("Units").setString("degrees");
    result.back().write().metadataReal("Description").setString("Wind direction from true north");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("Vector2D");
    result.back()
          .write()
          .metadataReal("Smoothing")
          .hash("Parameters")
          .hash("Direction")
          .setString("WD");
    result.back()
          .write()
          .metadataReal("Smoothing")
          .hash("Parameters")
          .hash("Magnitude")
          .setString("WS");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Wind Direction"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataSingleFlag("AbnormalStatus")
          .hash("Description")
          .setString("None zero status code reported");

    return result;
}

SequenceValue::Transfer AcquireRMYWind86xxx::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_rmy_wind86xxx");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());

    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataString("Realtime").hash("Name").setString(QString());
    result.back().write().metadataString("Realtime").hash("Units").setString(QString());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataString("Realtime").hash("RowOrder").setInt64(99);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Run")
          .setString(QObject::tr("", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidRecord")
          .setString(QObject::tr("NO COMMS: Invalid record"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidResponse")
          .setString(QObject::tr("NO COMMS: Invalid response"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Timeout")
          .setString(QObject::tr("NO COMMS: Timeout"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractive")
          .setString(QObject::tr("STARTING COMMS"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetOutputFormat")
          .setString(QObject::tr("STARTING COMMS: Setting output format"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetUnits")
          .setString(QObject::tr("STARTING COMMS: Setting wind speed units"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetUnpolledInterval")
          .setString(QObject::tr("STARTING COMMS: Setting response rate"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetWindFormat")
          .setString(QObject::tr("STARTING COMMS: Setting wind format"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetResolution")
          .setString(QObject::tr("STARTING COMMS: Setting report resolution"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadFirstRecord")
          .setString(QObject::tr("STARTING COMMS: Waiting unpolled response"));

    return result;
}

SequenceMatch::Composite AcquireRMYWind86xxx::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "ZSTATE");
    return sel;
}


int AcquireRMYWind86xxx::processRecordASCII(Util::ByteView line, double startTime, double endTime)
{
    if (line.size() < 2)
        return 1;

    {
        auto csum = line.mid(line.size() - 3);
        if (csum[0] != '*') return 2;
        csum = csum.mid(1);
        bool ok = false;
        std::uint8_t expected = static_cast<std::uint8_t>(csum.string_trimmed()
                                                              .toQByteArrayRef()
                                                              .toUShort(&ok, 16));
        if (!ok || expected > 0xFF) return 3;

        line = line.mid(0, line.size() - 3);

        std::uint8_t calculated = 0;
        for (auto add = line.begin(), end = add + line.size(); add != end; ++add) {
            calculated ^= static_cast<std::uint8_t>(*add);
        }
        if (calculated != expected) return 4;
    }

    auto fields = Util::as_deque(CSV::acquisitionSplit(line));
    bool ok = false;

    if (fields.empty()) return 5;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.size() < 1) return 6;
    if (config.front().address) {
        if (field[0] != config.front().address)
            return -1;
    }

    if (fields.empty()) return 7;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root WS(field.parse_real(&ok));
    if (!ok) return 8;
    if (!FP::defined(WS.read().toReal())) return 9;
    remap("WS", WS);

    if (fields.empty()) return 10;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root WD(field.parse_real(&ok));
    if (!ok) return 11;
    if (!FP::defined(WD.read().toReal())) return 12;
    remap("WD", WD);

    if (fields.empty()) return 12;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZSTATUS(field.parse_i32(&ok));
    if (!ok) return 13;
    if (!FP::defined(WS.read().toReal())) return 14;
    remap("ZSTATUS", ZSTATUS);

    if (config.front().strictMode) {
        if (!fields.empty())
            return 99;
    }

    if (!haveEmittedLogMeta && loggingEgress && FP::defined(startTime)) {
        haveEmittedLogMeta = true;
        loggingEgress->incomingData(buildLogMeta(startTime));
    }
    if (!haveEmittedRealtimeMeta && realtimeEgress && FP::defined(endTime)) {
        haveEmittedRealtimeMeta = true;
        SequenceValue::Transfer meta = buildLogMeta(endTime);
        Util::append(buildRealtimeMeta(endTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }

    Variant::Flags flags;
    auto statusCode = ZSTATUS.read().toInteger();
    if (INTEGER::defined(statusCode) && statusCode != 0)
        flags.emplace("AbnormalStatus");

    logValue(startTime, endTime, "WS", std::move(WS));
    logValue(startTime, endTime, "WD", std::move(WD));
    logValue(startTime, endTime, "F1", Variant::Root(std::move(flags)));

    if (realtimeEgress && realtimeStateUpdated) {
        if (!INTEGER::defined(statusCode) || statusCode == 0) {
            realtimeStateUpdated = false;
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), endTime,
                                  FP::undefined()));
        } else {
            realtimeStateUpdated = true;
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    QObject::tr("Abnormal status: %1").arg(static_cast<int>(statusCode))), endTime,
                                                       FP::undefined()));
        }
    }

    return 0;
}

static void scaleValue(Variant::Root &v, double scale)
{
    double f = v.read().toDouble();
    if (!FP::defined(f))
        return;
    v.write().setDouble(f * scale);
}

static int convertWindDirection(Variant::Root &value, char unit)
{
    if (unit == 0)
        return 0;
    if (unit == 'D' || unit == 'd' || unit == 'R' || unit == 'r')
        return 0;
    return 5;
}

static int convertWindSpeed(Variant::Root &value, char unit)
{
    if (unit == 0)
        return 0;
    switch (unit) {
    case 'M':
    case 'm':
        break;
    case 'K':
    case 'k':
        scaleValue(value, 1000.0 / 3600.0);
        break;
    case 'S':
    case 's':
        scaleValue(value, 0.44704);
        break;
    case 'N':
    case 'n':
        scaleValue(value, 0.514444);
        break;
    default:
        return 5;
    }
    return 0;
}

int AcquireRMYWind86xxx::processRecordNMEA(Util::ByteView line, double startTime, double endTime)
{
    if (line.size() < 9) return 1;
    if (line[0] != '$') return 2;

    {
        auto csum = line.mid(line.size() - 3);
        if (csum[0] != '*') return 3;
        csum = csum.mid(1);
        bool ok = false;
        std::uint8_t expected = static_cast<std::uint8_t>(csum.string_trimmed()
                                                              .toQByteArrayRef()
                                                              .toUShort(&ok, 16));
        if (!ok || expected > 0xFF) return 4;

        line = line.mid(0, line.size() - 3);

        std::uint8_t calculated = 0;
        for (auto add = line.begin() + 1, end = add + line.size() - 1; add != end; ++add) {
            calculated ^= static_cast<std::uint8_t>(*add);
        }
        if (calculated != expected) return 5;
    }

    auto fields = CSV::acquisitionSplit(line);
    bool ok = false;

    if (fields.empty()) return 6;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    if (field != "$WIMWV") return 7;

    if (fields.size() < 2) return 8;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root WD(field.parse_real(&ok));
    if (!ok) { WD.write().setEmpty(); }
    field = fields.front().string_trimmed();
    fields.pop_front();
    char WDUnit = 0;
    if (!field.empty())
        WDUnit = field[0];

    if (fields.size() < 2) return 9;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root WS(field.parse_real(&ok));
    if (!ok) { WS.write().setEmpty(); }
    field = fields.front().string_trimmed();
    fields.pop_front();
    char WSUnit = 0;
    if (!field.empty())
        WSUnit = field[0];

    if (fields.empty()) return 10;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.empty()) return 11;
    if (field[0] != 'A') {
        if (field[0] != 'V') {
            if (config.front().strictMode)
                return 12;
        }
        WS.write().setDouble(FP::undefined());
        WD.write().setDouble(FP::undefined());
    } else {
        if (!FP::defined(WD.read().toReal()) && WDUnit != '#')
            return 13;
        if (!FP::defined(WS.read().toReal()) && WSUnit != '#')
            return 14;
        int code = convertWindDirection(WD, WDUnit);
        if (code != 0)
            return 100 + code;
        code = convertWindSpeed(WS, WSUnit);
        if (code != 0)
            return 100 + code;
    }

    if (config.front().strictMode) {
        if (!fields.empty())
            return 99;
    }

    if (!haveEmittedLogMeta && loggingEgress && FP::defined(startTime)) {
        haveEmittedLogMeta = true;
        loggingEgress->incomingData(buildLogMeta(startTime));
    }
    if (!haveEmittedRealtimeMeta && realtimeEgress && FP::defined(endTime)) {
        haveEmittedRealtimeMeta = true;
        SequenceValue::Transfer meta = buildLogMeta(endTime);
        Util::append(buildRealtimeMeta(endTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }

    logValue(startTime, endTime, "WS", std::move(WS));
    logValue(startTime, endTime, "WD", std::move(WD));
    logValue(startTime, endTime, "F1", Variant::Root(Variant::Type::Flags));

    if (realtimeEgress && realtimeStateUpdated) {
        realtimeStateUpdated = false;
        realtimeEgress->incomingData(
                SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), endTime,
                              FP::undefined()));
    }

    return 0;
}

int AcquireRMYWind86xxx::processRecord(const Util::ByteView &line, double startTime, double endTime)
{
    Q_ASSERT(!config.empty());

    if (line.size() == 1)
        return -1;
    if (line.size() < 4)
        return 1;

    /* Require manufacturer specific in passive autoprobe */
    if (line[0] == '$' &&
            responseState != RESP_AUTOPROBE_PASSIVE_WAIT &&
            responseState != RESP_AUTOPROBE_PASSIVE_INITIALIZE) {
        int code = processRecordNMEA(line, startTime, endTime);
        if (code < 0)
            return code;
        if (code > 0)
            return code + 1000;
    } else {
        int code = processRecordASCII(line, startTime, endTime);
        if (code < 0)
            return code;
        if (code > 0)
            return code + 2000;
    }

    return 0;
}

void AcquireRMYWind86xxx::invalidateLogValues(double frameTime)
{
    loggingLost(frameTime);
    lastRecordTime = FP::undefined();
    autoprobeValidRecords = 0;
}

void AcquireRMYWind86xxx::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (!FP::defined(frameTime))
        return;

    {
        Q_ASSERT(!config.empty());
        if (!Range::intersectShift(config, frameTime)) {
            qCWarning(log) << "Can't find active configuration (list corrupted)";
        }
        Q_ASSERT(!config.empty());
    }

    double unpolledResponseTime = config.front().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        double startTime;
        if (!FP::defined(lastRecordTime)) {
            startTime = FP::undefined();
            lastRecordTime = frameTime;
        } else {
            startTime = lastRecordTime;
            if (lastRecordTime != frameTime)
                lastRecordTime = frameTime;
        }

        int code = processRecord(frame, startTime, frameTime);
        if (code == 0) {
            if (++autoprobeValidRecords > 5) {
                qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

                responseState = RESP_PASSIVE_RUN;
                realtimeStateUpdated = true;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                timeoutAt(frameTime + unpolledResponseTime + 1.0);

                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("PassiveAutoprobeWait");
                event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);
            }
        } else if (code > 0) {
            autoprobeValidRecords = 0;
            invalidateLogValues(frameTime);
        } else {
            invalidateLogValues(frameTime);
        }
        break;
    }
    case RESP_PASSIVE_WAIT: {
        double startTime;
        if (!FP::defined(lastRecordTime)) {
            startTime = FP::undefined();
            lastRecordTime = frameTime;
        } else {
            startTime = lastRecordTime;
            if (lastRecordTime != frameTime)
                lastRecordTime = frameTime;
        }

        int code = processRecord(frame, startTime, frameTime);
        if (code == 0) {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);

            responseState = RESP_PASSIVE_RUN;
            realtimeStateUpdated = true;
            ++autoprobeValidRecords;
            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveWait");
            event(frameTime, QObject::tr("Passive communications established."), false, info);
        } else if (code > 0) {
            autoprobeValidRecords = 0;
            invalidateLogValues(frameTime);
        } else {
            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID: {
        double startTime;
        if (!FP::defined(lastRecordTime)) {
            startTime = FP::undefined();
            lastRecordTime = frameTime;
        } else {
            startTime = lastRecordTime;
            if (lastRecordTime != frameTime)
                lastRecordTime = frameTime;
        }

        int code = processRecord(frame, startTime, frameTime);
        if (code == 0) {
            qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

            timeoutAt(frameTime + unpolledResponseTime + 1.0);
            responseState = RESP_UNPOLLED_RUN;
            realtimeStateUpdated = true;
            ++autoprobeValidRecords;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            event(frameTime, QObject::tr("Communications established."), false, info);
        } else if (code > 0) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "rejected with code" << code;

            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidRecord"),
                                      frameTime, FP::undefined()));
            }

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            autoprobeValidRecords = 0;

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream)
                controlStream->resetControl();
            discardData(frameTime + 30.0);

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Communications start rejected the first line (code %1).").arg(code),
                  true, info);

            invalidateLogValues(frameTime);
        } else {
            invalidateLogValues(frameTime);
        }
    }
        break;

    case RESP_PASSIVE_RUN:
    case RESP_UNPOLLED_RUN: {
        double startTime;
        if (!FP::defined(lastRecordTime)) {
            startTime = FP::undefined();
            lastRecordTime = frameTime;
        } else {
            startTime = lastRecordTime;
            if (lastRecordTime != frameTime)
                lastRecordTime = frameTime;
        }

        int code = processRecord(frame, startTime, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + unpolledResponseTime + 1.0);
            ++autoprobeValidRecords;
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidRecord"),
                                      frameTime, FP::undefined()));
            }

            Variant::Write info = Variant::Write::empty();

            if (responseState == RESP_PASSIVE_RUN) {
                timeoutAt(FP::undefined());
                responseState = RESP_PASSIVE_WAIT;
                info.hash("ResponseState").setString("PassiveRun");
            } else {
                if (controlStream) {
                    controlStream->writeControl("\x1B\x1B\x1B\r");
                }
                responseState = RESP_INTERACTIVE_START_FLUSH;
                discardData(frameTime + unpolledResponseTime + 1.0);
                timeoutAt(frameTime + unpolledResponseTime + 6.0);
                info.hash("ResponseState").setString("Run");
            }
            generalStatusUpdated();
            autoprobeValidRecords = 0;

            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
        } else {
            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_INTERACTIVE_START_SET_OUTPUT_FORMAT: {
        if (Util::contains(frame.toString(), "CMD ERR") ||
                !Util::contains(frame.toString(), "SET")) {
            qCDebug(log) << "Invalid response" << frame
                         << "in interactive start set output format at" << Logging::time(frameTime);

            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            invalidateLogValues(frameTime);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            break;
        }

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveSetUnits"),
                                  frameTime, FP::undefined()));
        }

        responseState = RESP_INTERACTIVE_START_SET_UNITS;
        timeoutAt(frameTime + 2.0);
        if (controlStream) {
            controlStream->writeControl("SET044\r");
        }

        break;
    }

    case RESP_INTERACTIVE_START_SET_UNITS: {
        if (Util::contains(frame.toString(), "CMD ERR") ||
                !Util::contains(frame.toString(), "SET")) {
            qCDebug(log) << "Invalid response" << frame << "in interactive start set units at"
                         << Logging::time(frameTime);

            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            invalidateLogValues(frameTime);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            break;
        }

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveSetUnpolledInterval"), frameTime, FP::undefined()));
        }

        responseState = RESP_INTERACTIVE_START_SET_UNPOLLED_INTERVAL;
        timeoutAt(frameTime + 2.0);
        if (controlStream) {
            QByteArray command = "SET10";
            int ms = qRound(unpolledResponseTime * 1000.0);
            if (ms < 100)
                ms = 100;
            else if (ms > 9999)
                ms = 9999;
            command += QByteArray::number(ms);
            command += "\r";
            controlStream->writeControl(command);
        }

        break;
    }

    case RESP_INTERACTIVE_START_SET_UNPOLLED_INTERVAL: {
        if (Util::contains(frame.toString(), "CMD ERR") ||
                !Util::contains(frame.toString(), "SET")) {
            qCDebug(log) << "Invalid response" << frame
                         << "in interactive start set unpolled interval at"
                         << Logging::time(frameTime);

            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            invalidateLogValues(frameTime);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            break;
        }

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveSetWindFormat"), frameTime, FP::undefined()));
        }

        responseState = RESP_INTERACTIVE_START_SET_WIND_FORMAT;
        timeoutAt(frameTime + 2.0);
        if (controlStream) {
            controlStream->writeControl("SET130\r");
        }

        break;
    }

    case RESP_INTERACTIVE_START_SET_WIND_FORMAT: {
        if (Util::contains(frame.toString(), "CMD ERR") ||
                !Util::contains(frame.toString(), "SET")) {
            qCDebug(log) << "Invalid response" << frame << "in interactive start set wind format at"
                         << Logging::time(frameTime);

            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            invalidateLogValues(frameTime);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            break;
        }

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveSetResolution"), frameTime, FP::undefined()));
        }

        responseState = RESP_INTERACTIVE_START_SET_RESOLUTION;
        timeoutAt(frameTime + 2.0);
        if (controlStream) {
            controlStream->writeControl("SET151\r");
        }

        break;
    }

    case RESP_INTERACTIVE_START_SET_RESOLUTION: {
        if (Util::contains(frame.toString(), "CMD ERR") ||
                !Util::contains(frame.toString(), "SET")) {
            qCDebug(log) << "Invalid response" << frame << "in interactive start set resolution at"
                         << Logging::time(frameTime);

            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            invalidateLogValues(frameTime);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            break;
        }

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadFirstRecord"), frameTime, FP::undefined()));
        }

        responseState = RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID;
        discardData(frameTime + unpolledResponseTime + 1.0);
        timeoutAt(frameTime + unpolledResponseTime * 2.0 + 10.0);
        if (controlStream) {
            controlStream->writeControl("XX\r");
        }

        break;
    }

    default:
        break;
    }

}

void AcquireRMYWind86xxx::incomingInstrumentTimeout(double frameTime)
{
    if (!FP::defined(frameTime))
        return;

    {
        Q_ASSERT(!config.empty());
        if (!Range::intersectShift(config, frameTime)) {
            qCWarning(log) << "Can't find active configuration (list corrupted)";
        }
        Q_ASSERT(!config.empty());
    }

    double unpolledResponseTime = config.front().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    switch (responseState) {
    case RESP_UNPOLLED_RUN:
        qCDebug(log) << "Timeout in unpolled state" << responseState << "at"
                     << Logging::time(frameTime);
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        realtimeStateUpdated = true;

        timeoutAt(frameTime + unpolledResponseTime + 30.0);

        if (controlStream) {
            controlStream->writeControl("\x1B\x1B\x1B\r");
        }
        responseState = RESP_INTERACTIVE_START_FLUSH;
        discardData(frameTime + unpolledResponseTime + 2.0);
        generalStatusUpdated();

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("UnpolledRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_PASSIVE_RUN:
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        realtimeStateUpdated = true;

        timeoutAt(FP::undefined());

        responseState = RESP_PASSIVE_WAIT;
        generalStatusUpdated();

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_START_FLUSH:
    case RESP_INTERACTIVE_START_SET_OUTPUT_FORMAT:
    case RESP_INTERACTIVE_START_SET_UNITS:
    case RESP_INTERACTIVE_START_SET_UNPOLLED_INTERVAL:
    case RESP_INTERACTIVE_START_SET_WIND_FORMAT:
    case RESP_INTERACTIVE_START_SET_RESOLUTION:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
    case RESP_INTERACTIVE_RESTART_WAIT:
        qCDebug(log) << "Timeout in interactive start state" << responseState << "at"
                     << Logging::time(frameTime);
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        realtimeStateUpdated = true;

        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        timeoutAt(FP::undefined());
        if (controlStream)
            controlStream->resetControl();
        discardData(frameTime + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        realtimeStateUpdated = true;

        if (controlStream) {
            controlStream->writeControl("\x1B\x1B\x1B\r");
        }
        timeoutAt(frameTime + unpolledResponseTime + 30.0);
        responseState = RESP_INTERACTIVE_START_FLUSH;
        discardData(frameTime + unpolledResponseTime + 2.0);
        break;

    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
        invalidateLogValues(frameTime);
        break;

    default:
        invalidateLogValues(frameTime);
        break;
    }
}

void AcquireRMYWind86xxx::discardDataCompleted(double frameTime)
{
    if (!FP::defined(frameTime))
        return;

    {
        Q_ASSERT(!config.empty());
        if (!Range::intersectShift(config, frameTime)) {
            qCWarning(log) << "Can't find active configuration (list corrupted)";
        }
        Q_ASSERT(!config.empty());
    }

    Q_ASSERT(!config.empty());
    double unpolledResponseTime = config.front().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    switch (responseState) {
    case RESP_INTERACTIVE_START_FLUSH:
        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveSetOutputFormat"), frameTime, FP::undefined()));
        }
        realtimeStateUpdated = true;

        responseState = RESP_INTERACTIVE_START_SET_OUTPUT_FORMAT;
        timeoutAt(frameTime + 2.0);
        if (controlStream) {
            controlStream->writeControl("SET022\r");
        }
        break;

    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
        break;

    case RESP_INTERACTIVE_RESTART_WAIT:
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        realtimeStateUpdated = true;

        if (controlStream) {
            controlStream->writeControl("\x1B\x1B\x1B\r");
        }
        timeoutAt(frameTime + unpolledResponseTime + 30.0);
        responseState = RESP_INTERACTIVE_START_FLUSH;
        discardData(frameTime + unpolledResponseTime + 2.0);
        break;

    default:
        break;
    }
}


void AcquireRMYWind86xxx::incomingControlFrame(const Util::ByteArray &, double)
{ }

Variant::Root AcquireRMYWind86xxx::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireRMYWind86xxx::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_UNPOLLED_RUN:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}


AcquisitionInterface::AutoprobeStatus AcquireRMYWind86xxx::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireRMYWind86xxx::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    {
        Q_ASSERT(!config.empty());
        if (!Range::intersectShift(config, time)) {
            qCWarning(log) << "Can't find active configuration (list corrupted)";
        }
        Q_ASSERT(!config.empty());
    }

    double unpolledResponseTime = config.front().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    switch (responseState) {
    case RESP_UNPOLLED_RUN:
        /* Already running/started, do nothing */
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << responseState << " at " << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case RESP_INTERACTIVE_START_FLUSH:
    case RESP_INTERACTIVE_START_SET_OUTPUT_FORMAT:
    case RESP_INTERACTIVE_START_SET_UNITS:
    case RESP_INTERACTIVE_START_SET_UNPOLLED_INTERVAL:
    case RESP_INTERACTIVE_START_SET_WIND_FORMAT:
    case RESP_INTERACTIVE_START_SET_RESOLUTION:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        if (controlStream) {
            controlStream->writeControl("\x1B\x1B\x1B\r");
        }
        timeoutAt(time + unpolledResponseTime + 30.0);
        responseState = RESP_INTERACTIVE_START_FLUSH;
        discardData(time + unpolledResponseTime + 2.0);

        generalStatusUpdated();
        break;
    }
}

void AcquireRMYWind86xxx::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    {
        Q_ASSERT(!config.empty());
        if (!Range::intersectShift(config, time)) {
            qCWarning(log) << "Can't find active configuration (list corrupted)";
        }
        Q_ASSERT(!config.empty());
    }

    double unpolledResponseTime = config.front().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
    autoprobeValidRecords = 0;
    discardData(time + 0.5, 1);
    timeoutAt(time + unpolledResponseTime * 7.0 + 3.0);
    generalStatusUpdated();
}

void AcquireRMYWind86xxx::autoprobePromote(double time)
{
    PauseLock paused(*this);

    {
        Q_ASSERT(!config.empty());
        if (!Range::intersectShift(config, time)) {
            qCWarning(log) << "Can't find active configuration (list corrupted)";
        }
        Q_ASSERT(!config.empty());
    }

    double unpolledResponseTime = config.front().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    switch (responseState) {
    case RESP_UNPOLLED_RUN:
        qCDebug(log) << "Promoted from interactive run to interactive acquisition at"
                     << Logging::time(time);
        /* Reset timeout in case we didn't have a control stream */
        timeoutAt(time + unpolledResponseTime + 2.0);
        break;

    case RESP_INTERACTIVE_START_FLUSH:
    case RESP_INTERACTIVE_START_SET_OUTPUT_FORMAT:
    case RESP_INTERACTIVE_START_SET_UNITS:
    case RESP_INTERACTIVE_START_SET_UNPOLLED_INTERVAL:
    case RESP_INTERACTIVE_START_SET_WIND_FORMAT:
    case RESP_INTERACTIVE_START_SET_RESOLUTION:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);
        break;

    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        if (controlStream) {
            controlStream->writeControl("\x1B\x1B\x1B\r");
        }
        timeoutAt(time + unpolledResponseTime + 30.0);
        responseState = RESP_INTERACTIVE_START_FLUSH;
        discardData(time + unpolledResponseTime + 2.0);

        generalStatusUpdated();
        break;
    }
}

void AcquireRMYWind86xxx::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    {
        Q_ASSERT(!config.empty());
        if (!Range::intersectShift(config, time)) {
            qCWarning(log) << "Can't find active configuration (list corrupted)";
        }
        Q_ASSERT(!config.empty());
    }

    double unpolledResponseTime = config.front().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    /* Reset timeout in case we didn't have a control stream */
    timeoutAt(time + unpolledResponseTime + 2.0);

    switch (responseState) {
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);

        break;

    case RESP_UNPOLLED_RUN:
        responseState = RESP_PASSIVE_RUN;

        qCDebug(log) << "Promoted from unpolled interactive state to passive acquisition at"
                     << Logging::time(time);
        break;

    default:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_PASSIVE_RUN;

        discardData(FP::undefined());
        generalStatusUpdated();
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireRMYWind86xxx::getDefaults()
{
    AutomaticDefaults result;
    result.name = "XM$2";
    result.setSerialN81(9600);
    return result;
}


ComponentOptions AcquireRMYWind86xxxComponent::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options);

    return options;
}

ComponentOptions AcquireRMYWind86xxxComponent::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireRMYWind86xxxComponent::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples());
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireRMYWind86xxxComponent::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireRMYWind86xxxComponent::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options);
}

QList<ComponentExample> AcquireRMYWind86xxxComponent::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data.")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireRMYWind86xxxComponent::createAcquisitionPassive(const ComponentOptions &options,
                                                                                     const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireRMYWind86xxx(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireRMYWind86xxxComponent::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                     const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireRMYWind86xxx(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireRMYWind86xxxComponent::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                       const std::string &loggingContext)
{
    std::unique_ptr<AcquireRMYWind86xxx> i(new AcquireRMYWind86xxx(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<AcquisitionInterface> AcquireRMYWind86xxxComponent::createAcquisitionInteractive(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireRMYWind86xxx> i(new AcquireRMYWind86xxx(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
