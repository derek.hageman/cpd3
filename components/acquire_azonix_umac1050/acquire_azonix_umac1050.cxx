/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <algorithm>
#include <QtEndian>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/composite.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_azonix_umac1050.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

AcquireAzonixUMAC1050::Configuration::Configuration() : start(FP::undefined()),
                                                        end(FP::undefined()),
                                                        address(0),
                                                        pollInterval(1.0),
                                                        logOutputs(false),
                                                        analogChannelStart(0),
                                                        analogChannelCount(24),
                                                        analogNames(),
                                                        digitalNames(),
                                                        initializeAnalog(),
                                                        exitAnalog(),
                                                        initializeDigital(),
                                                        exitDigital(),
                                                        bypassAnalog(),
                                                        unbypassAnalog(),
                                                        bypassDigital(),
                                                        unbypassDigital(),
                                                        variables()
{ }

AcquireAzonixUMAC1050::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          address(other.address),
          pollInterval(other.pollInterval),
          logOutputs(other.logOutputs),
          analogChannelStart(other.analogChannelStart),
          analogChannelCount(other.analogChannelCount),
          analogNames(other.analogNames),
          digitalNames(other.digitalNames),
          initializeAnalog(other.initializeAnalog),
          exitAnalog(other.exitAnalog),
          initializeDigital(other.initializeDigital),
          exitDigital(other.exitDigital),
          bypassAnalog(other.bypassAnalog),
          unbypassAnalog(other.unbypassAnalog),
          bypassDigital(other.bypassDigital),
          unbypassDigital(other.unbypassDigital),
          variables(other.variables)
{ }

void AcquireAzonixUMAC1050::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("Azonix");
    instrumentMeta["Model"].setString("uMAC1050");

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;

    analogOutputValues.setUnit(SequenceName({}, "raw", "ZOUTPUTS"));
    digitalOutputValues.setUnit(SequenceName({}, "raw", "F2"));
    digitalOutputBits.setUnit(SequenceName({}, "raw", "ZDIGITAL"));

    maximumDisplayChannel = -1;

    memset(streamAge, 0, sizeof(streamAge));
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
    }
    lastInputs.write().setEmpty();
}

AcquireAzonixUMAC1050::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s),
          end(e),
          address(0),
          pollInterval(1.0),
          logOutputs(false),
          analogChannelStart(0),
          analogChannelCount(24),
          analogNames(),
          digitalNames(),
          initializeAnalog(),
          exitAnalog(),
          initializeDigital(),
          exitDigital(),
          bypassAnalog(),
          unbypassAnalog(),
          bypassDigital(),
          unbypassDigital(),
          variables()
{
    setFromSegment(other);
}

AcquireAzonixUMAC1050::Configuration::Configuration(const Configuration &under,
                                                    const ValueSegment &over,
                                                    double s,
                                                    double e) : start(s),
                                                                end(e),
                                                                address(under.address),
                                                                pollInterval(under.pollInterval),
                                                                logOutputs(under.logOutputs),
                                                                analogChannelStart(
                                                                        under.analogChannelStart),
                                                                analogChannelCount(
                                                                        under.analogChannelCount),
                                                                analogNames(under.analogNames),
                                                                digitalNames(under.digitalNames),
                                                                initializeAnalog(
                                                                        under.initializeAnalog),
                                                                exitAnalog(under.exitAnalog),
                                                                initializeDigital(
                                                                        under.initializeDigital),
                                                                exitDigital(under.exitDigital),
                                                                bypassAnalog(under.bypassAnalog),
                                                                unbypassAnalog(
                                                                        under.unbypassAnalog),
                                                                bypassDigital(under.bypassDigital),
                                                                unbypassDigital(
                                                                        under.unbypassDigital),
                                                                variables(under.variables)
{
    setFromSegment(over);
}

void AcquireAzonixUMAC1050::Configuration::addVariable(const Variant::Read &config,
                                                       qint64 defaultChannel,
                                                       const SequenceName::Component &defaultName)
{
    qint64 channel = config["Channel"].toInt64();
    if (!INTEGER::defined(channel)) {
        const auto &check = config["Channel"].toString();
        if (Util::equal_insensitive(check, "t")) {
            channel = 60;
        } else if (Util::equal_insensitive(check, "v")) {
            channel = 61;
        } else {
            channel = defaultChannel;
        }
    }
    if (channel < 0 || channel > 61)
        return;

    auto name = config["Name"].toString();
    if (name.empty())
        name = defaultName;
    if (name.empty())
        return;

    OutputVariable variable;
    variable.channel = (int) channel;
    variable.name = name;
    variable.metadata.write().set(config["Metadata"]);
    variable.calibration = Variant::Composite::toCalibration(config["Calibration"]);

    variables[variable.channel].append(variable);
}

void AcquireAzonixUMAC1050::Configuration::setAnalogValues(const Variant::Read &config,
                                                           QMap<int, double> &target)
{
    switch (config.getType()) {
    case Variant::Type::String: {
        QStringList channels(config.toQString().split(QRegExp("[:;,]+")));
        for (int idx = 0, max = qMin(channels.size(), 10); idx < max; ++idx) {
            bool ok = false;
            double value = channels.at(idx).toDouble(&ok);
            if (!ok)
                continue;
            if (!FP::defined(value))
                target.remove(idx);
            else
                target.insert(idx, value);
        }
        break;
    }
    default: {
        auto children = config.toChildren();
        for (auto add = children.begin(), endAdd = children.end(); add != endAdd; ++add) {
            int index = add.integerKey();
            auto check = add.stringKey();
            if (!check.empty()) {
                index = -1;
                auto lookup = analogNames.find(check);
                if (lookup != analogNames.end())
                    index = lookup->second;
                if (index == -1) {
                    bool ok = false;
                    index = QString::fromStdString(check).toInt(&ok);
                    if (!ok)
                        index = -1;
                }
            }
            if (index < 0 || index > 9)
                continue;

            double value = add.value().toDouble();
            if (!FP::defined(value))
                target.remove(index);
            else
                target.insert(index, value);
        }
        break;
    }
    }
}

void AcquireAzonixUMAC1050::Configuration::setDigitalValues(const Variant::Read &config,
                                                            QMap<int, bool> &target)
{
    switch (config.getType()) {
    case Variant::Type::String: {
        QStringList channels(config.toQString().split(QRegExp("[:;,]+")));
        if (channels.size() == 1) {
            bool ok = false;
            quint64 outputs = channels.at(0).toULongLong(&ok, 16);
            for (int idx = 0; idx < 64; idx++) {
                target.insert(idx, (outputs & ((quint64) 1 << idx)) != 0);
            }
        } else {
            for (int idx = 0, max = qMin(channels.size(), 9 * 8 + 1); idx < max; ++idx) {
                bool value = false;
                if (channels.at(idx).toLower() == "on" || channels.at(idx).toLower() == "enabled") {
                    value = true;
                } else {
                    bool ok = false;
                    if (channels.at(idx).toInt(&ok) && ok)
                        value = true;
                    else if (!ok)
                        continue;
                }
                target.insert(idx, value);
            }
        }
        break;
    }
    case Variant::Type::Integer: {
        qint64 outputs = config.toInt64();
        if (INTEGER::defined(outputs)) {
            for (int idx = 0; idx < 63; idx++) {
                target.insert(idx, (outputs & ((qint64) 1 << idx)) != 0);
            }
        }
        break;
    }
    default: {
        auto children = config.toChildren();
        for (auto add = children.begin(), endAdd = children.end(); add != endAdd; ++add) {
            int index = add.integerKey();
            auto check = add.stringKey();
            if (!check.empty()) {
                index = -1;
                auto lookup = digitalNames.find(check);
                if (lookup != digitalNames.end())
                    index = lookup->second;
                if (index == -1) {
                    bool ok = false;
                    index = QString::fromStdString(check).toInt(&ok);
                    if (!ok)
                        index = -1;
                }
            }
            if (index < 0 || index >= 9 * 8)
                continue;

            if (add.value().getType() != Variant::Type::Boolean)
                target.remove(index);
            else
                target.insert(index, add.value().toBool());
        }
        break;
    }
    }
}

void AcquireAzonixUMAC1050::Configuration::setFromSegment(const ValueSegment &config)
{
    if (INTEGER::defined(config["Address"].toInt64())) {
        address = (quint8) config["Address"].toInt64();
        if (address > 63)
            address = 0;
    }
    if (config["PollInterval"].exists()) {
        pollInterval = config["PollInterval"].toDouble();
        if (FP::defined(pollInterval) && pollInterval <= 0.0)
            pollInterval = FP::undefined();
    }
    if (config["LogOutputs"].exists()) {
        logOutputs = config["LogOutputs"].toBool();
    }
    if (INTEGER::defined(config["AnalogChannelStart"].toInt64())) {
        analogChannelStart = config["AnalogChannelStart"].toInt64();
        if (analogChannelStart < 0 || analogChannelStart > 61)
            analogChannelStart = 0;
    }
    if (INTEGER::defined(config["AnalogChannelCount"].toInt64())) {
        analogChannelCount = config["AnalogChannelCount"].toInt64();
        if (analogChannelCount < 0 || analogChannelStart + analogChannelCount > 62) {
            analogChannelCount = qMax(24, 62 - analogChannelStart);
        }
    }


    if (config["Variables"].exists()) {
        variables.clear();
        switch (config["Variables"].getType()) {
        case Variant::Type::String: {
            QStringList names(config["Variables"].toQString()
                                                 .split(QRegExp("[\\s+;:,]"),
                                                        QString::SkipEmptyParts));
            for (int idx = 0, max = names.size(); idx < max; ++idx) {
                if (idx < 10) {
                    addVariable(Variant::Read::empty(), idx + 1, names.at(idx).toStdString());
                } else {
                    addVariable(Variant::Read::empty(), INTEGER::undefined(),
                                names.at(idx).toStdString());
                }
            }
            break;
        }
        default: {
            auto children = config["Variables"].toChildren();
            for (auto add = children.begin(), endAdd = children.end(); add != endAdd; ++add) {
                bool ok = false;
                qint64 channel = add.integerKey();
                {
                    auto check = add.stringKey();
                    if (!check.empty()) {
                        channel = QString::fromStdString(add.stringKey()).toLongLong(&ok);
                        if (!ok)
                            channel = INTEGER::undefined();
                    }
                }
                addVariable(add.value(), channel, add.stringKey());
            }
            break;
        }
        }
    }

    if (config["AnalogOutputs"].exists()) {
        analogNames.clear();
        for (auto add : config["AnalogOutputs"].toHash()) {
            if (add.first.empty())
                continue;
            qint64 channel = INTEGER::undefined();
            switch (add.second.getType()) {
            case Variant::Type::String: {
                bool ok = false;
                channel = add.second.toQString().toLongLong(&ok);
                if (!ok)
                    channel = INTEGER::undefined();
                break;
            }
            default:
                channel = add.second.toInt64();
                break;
            }
            if (channel < 0 || channel > 9)
                continue;

            analogNames[add.first] = (int) channel;
        }
    }

    if (config["DigitalOutputs"].exists()) {
        digitalNames.clear();
        for (auto add : config["DigitalOutputs"].toHash()) {
            if (add.first.empty())
                continue;
            qint64 channel = INTEGER::undefined();
            switch (add.second.getType()) {
            case Variant::Type::String: {
                bool ok = false;
                channel = add.second.toQString().toLongLong(&ok);
                if (!ok)
                    channel = INTEGER::undefined();
                break;
            }
            default:
                channel = add.second.toInt64();
                break;
            }
            if (channel < 0 || channel >= 9 * 8)
                continue;

            digitalNames[add.first] = (int) channel;
        }
    }

    setAnalogValues(config["Initialize/Analog"], initializeAnalog);
    setAnalogValues(config["Exit/Analog"], exitAnalog);
    setDigitalValues(config["Initialize/Digital"], initializeDigital);
    setDigitalValues(config["Exit/Digital"], exitDigital);
    setAnalogValues(config["Bypass/Analog"], bypassAnalog);
    setAnalogValues(config["UnBypass/Analog"], unbypassAnalog);
    setDigitalValues(config["Bypass/Digital"], bypassDigital);
    setDigitalValues(config["UnBypass/Digital"], unbypassDigital);
}

AcquireAzonixUMAC1050::AcquireAzonixUMAC1050(const ValueSegment::Transfer &configData,
                                             const std::string &loggingContext) : FramedInstrument(
        "umac", loggingContext),
                                                                                  autoprobeStatus(
                                                                                          AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                                  responseState(
                                                                                          RESP_PASSIVE_WAIT),
                                                                                  commandQueue(),
                                                                                  issueSequenceNumber(
                                                                                          0),
                                                                                  loggingMux(
                                                                                          LogStream_TOTAL)
{
    setDefaultInvalid();
    config.append(Configuration());

    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }

    configurationChanged();
}

void AcquireAzonixUMAC1050::setToAutoprobe()
{ responseState = RESP_AUTOPROBE_PASSIVE_INITIALIZE; }

void AcquireAzonixUMAC1050::setToInteractive()
{ responseState = RESP_INTERACTIVE_INITIALIZE; }


ComponentOptions AcquireAzonixUMAC1050Component::getBaseOptions()
{
    ComponentOptions options;

    return options;
}

AcquireAzonixUMAC1050::AcquireAzonixUMAC1050(const ComponentOptions &options,
                                             const std::string &loggingContext) : FramedInstrument(
        "umac", loggingContext),
                                                                                  autoprobeStatus(
                                                                                          AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                                  responseState(
                                                                                          RESP_PASSIVE_WAIT),
                                                                                  commandQueue(),
                                                                                  issueSequenceNumber(
                                                                                          0),
                                                                                  loggingMux(
                                                                                          LogStream_TOTAL)
{
    Q_UNUSED(options);
    setDefaultInvalid();
    config.append(Configuration());
    configurationChanged();
}

AcquireAzonixUMAC1050::~AcquireAzonixUMAC1050()
{
}

SequenceValue::Transfer AcquireAzonixUMAC1050::buildLogMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_azonix_umac1050");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);

    for (QHash<int, QList<OutputVariable> >::const_iterator
            channelVariables = config.first().variables.constBegin(),
            endCV = config.first().variables.constEnd();
            channelVariables != endCV;
            ++channelVariables) {
        for (QList<OutputVariable>::const_iterator variable = channelVariables.value().constBegin(),
                endV = channelVariables.value().constEnd(); variable != endV; ++variable) {
            result.emplace_back(SequenceName({}, "raw_meta", variable->name),
                                Variant::Root(variable->metadata), time, FP::undefined());
            if (result.back().write().metadataReal("Format").toString().empty()) {
                result.back().write().metadataReal("Format").setString("00.000");
            }
            result.back().write().metadataReal("Source").set(instrumentMeta);
            result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
            Variant::Composite::fromCalibration(result.back().write().metadataReal("Calibration"),
                                                variable->calibration);
            result.back().write().metadataReal("Channel").setInt64(variable->channel);
            result.back()
                  .write()
                  .metadataReal("PhysicalChannel")
                  .setInt64(config.first().analogChannelStart + variable->channel);
            result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
            result.back().write().metadataReal("Realtime").hash("Name").setString(variable->name);
        }
    }

    result.emplace_back(SequenceName({}, "raw_meta", "T"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Internal board temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Board"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "V"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0.000");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Supply voltage");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Supply"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "ZINPUTS"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back()
          .write()
          .metadataArray("Description")
          .setString("Raw input values from all analog channels");
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Count").setInt64(config.first().analogChannelCount);
    result.back()
          .write()
          .metadataArray("ChannelOffset")
          .setInt64(config.first().analogChannelStart);
    result.back().write().metadataArray("Children").metadataReal("Format").setString("0.000");
    result.back().write().metadataArray("Children").metadataReal("Units").setString("V");
    result.back().write().metadataArray("Realtime").hash("Page").setInt64(1);
    result.back().write().metadataArray("Realtime").hash("ColumnOrder").setInt64(1);
    result.back().write().metadataArray("Realtime").hash("Name").setString("Ain");

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    /* Always output this metadata, even if we're not logging the
     * actual values, so it gets updated as needed */

    result.emplace_back(SequenceName({}, "raw_meta", "ZOUTPUTS"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Description").setString("Analog output channel values");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Children").metadataReal("Format").setString("0.000");
    result.back().write().metadataArray("Realtime").hash("Page").setInt64(1);
    result.back().write().metadataArray("Realtime").hash("ColumnOrder").setInt64(3);
    result.back().write().metadataArray("Realtime").hash("Name").setString("Aout");
    result.back().write().metadataArray("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "F2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Description").setString("Digital output channel values");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Children").setType(Variant::Type::MetadataBoolean);
    result.back().write().metadataArray("Realtime").hash("Page").setInt64(1);
    result.back().write().metadataArray("Realtime").hash("ColumnOrder").setInt64(2);
    result.back().write().metadataArray("Realtime").hash("Name").setString("Dout");
    result.back()
          .write()
          .metadataArray("Realtime")
          .hash("Translation")
          .hash("TRUE")
          .setString("ON");
    result.back()
          .write()
          .metadataArray("Realtime")
          .hash("Translation")
          .hash("FALSE")
          .setString("OFF");

    return result;
}

SequenceValue::Transfer AcquireAzonixUMAC1050::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_azonix_umac1050");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);


    result.emplace_back(SequenceName({}, "raw_meta", "ZDIGITAL"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataInteger("Format").setString("FFFFFFFF");
    result.back().write().metadataInteger("Source").set(instrumentMeta);
    result.back().write().metadataInteger("Processing").toArray().after_back().set(processing);
    result.back().write().metadataInteger("Realtime").hash("Page").setInt64(0);
    result.back().write().metadataInteger("Realtime").hash("Name").setString("Digital");
    result.back().write().metadataInteger("Realtime").hash("RowOrder").setInt64(1);
    result.back().write().metadataInteger("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "ZINDEX"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Description").setString("Output index display");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Children").metadataInteger("Format").setString("000");
    result.back().write().metadataArray("Realtime").hash("Page").setInt64(1);
    result.back().write().metadataArray("Realtime").hash("ColumnOrder").setInt64(0);
    result.back().write().metadataArray("Realtime").hash("Name").setString("Channel");
    result.back().write().metadataArray("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataString("Realtime").hash("Hide").setBool(true);
    /*result.back().write().metadataString("Realtime").hash("Name").
        setString(std::string());
    result.back().write().metadataString("Realtime").hash("Units").
        setString(std::string());
    result.back().write().metadataString("Realtime").hash("FullLine").
        setBool(true);        
    result.back().write().metadataString("Realtime").hash("Page").
        setInt64(0);
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("Run").setString(QObject::tr("", "run state string"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("MalformedCommand").setString(QObject::tr("NO COMMS: Malformed command"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("CommandSequencingError").setString(QObject::tr("NO COMMS: Command sequencing error"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("InvalidResponse").setString(QObject::tr("NO COMMS: Invalid response"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("Timeout").setString(QObject::tr("NO COMMS: Timeout"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractive").setString(QObject::tr("STARTING COMMS"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveReset").setString(QObject::tr("STARTING COMMS: Flushing data"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveResetWait").setString(QObject::tr("STARTING COMMS: Waiting for reset completion"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveReadRevision").setString(QObject::tr("STARTING COMMS: Reading firmware version"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveReadAnalog").setString(QObject::tr("STARTING COMMS: Reading analog values"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveLoadConfiguration").setString(QObject::tr("STARTING COMMS: Loading configuration"));*/

    return result;
}

SequenceMatch::Composite AcquireAzonixUMAC1050::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "F2");
    sel.append({}, {}, "ZDIGITAL");
    sel.append({}, {}, "ZINDEX");
    sel.append({}, {}, "ZSTATE");
    return sel;
}

void AcquireAzonixUMAC1050::invalidateLogValues(double frameTime)
{
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
        streamAge[i] = 0;
        if (loggingEgress != NULL)
            loggingMux.advance(i, frameTime, loggingEgress);
    }
    loggingMux.clear();
    lastInputs.write().setEmpty();
    loggingLost(frameTime);
}

void AcquireAzonixUMAC1050::logValues(double frameTime,
                                      int streamID,
                                      SequenceValue::Transfer &&values)
{
    double startTime = streamTime[streamID];
    double endTime = frameTime;
    Q_ASSERT(FP::defined(endTime));
    if (!FP::defined(startTime) || startTime == endTime || !loggingEgress) {
        if (loggingEgress)
            streamTime[streamID] = endTime;
        if (!realtimeEgress)
            return;
        realtimeEgress->incomingData(std::move(values));
        return;
    }

    if (!realtimeEgress) {
        for (auto &output : values) {
            output.setStart(startTime);
            output.setEnd(endTime);
        }
        loggingMux.incoming(streamID, std::move(values), loggingEgress);
        if (values.empty()) {
            loggingMux.advance(streamID, startTime, loggingEgress);
        }

        streamAge[streamID]++;
        streamTime[streamID] = endTime;
        return;
    }

    {
        auto copy = values;
        for (auto &output : copy) {
            output.setStart(startTime);
            output.setEnd(endTime);
        }
        loggingMux.incoming(streamID, std::move(copy), loggingEgress);
        if (values.empty()) {
            loggingMux.advance(streamID, startTime, loggingEgress);
        }
    }
    streamAge[streamID]++;
    streamTime[streamID] = endTime;

    Q_ASSERT(realtimeEgress);
    realtimeEgress->incomingData(std::move(values));
}

std::size_t AcquireAzonixUMAC1050::dataFrameStart(std::size_t offset,
                                                  const Util::ByteArray &input) const
{
    auto addr = config.first().address;
    for (auto max = input.size(); offset < max; ++offset) {
        offset = input.indexOf(addr, offset);
        if (offset == input.npos)
            break;
        if (offset + 4 >= max)
            break;
        if (input[offset + 1] != 0x01)
            continue;
        quint16 bcs = qFromLittleEndian<quint16>(input.data<const uchar *>(offset + 2));
        int len = bcs & 0x3FF;
        if (len < 4 || len > 1023)
            continue;
        if (offset + 4 + len > max)
            break;
        if (crc.calculate(input.data(offset), len + 4) != 0)
            continue;
        return offset;
    }
    return input.npos;
}

std::size_t AcquireAzonixUMAC1050::dataFrameEnd(std::size_t start,
                                                const Util::ByteArray &input) const
{
    if (input.size() - start < 8)
        return input.npos;

    auto data = input.data<const uchar *>(start);
    Q_ASSERT(data[0] == config.first().address);
    Q_ASSERT(data[1] == 0x01);
    quint16 bcs = qFromLittleEndian<quint16>(data + 2);
    std::size_t len = bcs & 0x3FF;
    Q_ASSERT(len >= 4 && len <= 1023);
    len += 4;
    if (input.size() - start < len)
        return input.npos;
    return start + len;
}

std::size_t AcquireAzonixUMAC1050::controlFrameStart(std::size_t offset,
                                                     const Util::ByteArray &input) const
{
    auto addr = config.first().address;
    for (auto max = input.size(); offset < max; ++offset) {
        offset = input.indexOf(addr, offset);
        if (offset == input.npos)
            break;
        if (offset + 4 >= max)
            break;
        if (input[offset + 1] != 0x01)
            continue;
        quint16 bcs = qFromLittleEndian<quint16>(input.data<const uchar *>(offset + 2));
        std::size_t len = bcs & 0x3FF;
        if (len < 4 || len > 1023)
            continue;
        if (offset + 4 + len > max)
            break;
        if (crc.calculate(input.data(offset), len + 4) != 0)
            continue;
        return offset;
    }
    return input.npos;
}

std::size_t AcquireAzonixUMAC1050::controlFrameEnd(std::size_t start,
                                                   const Util::ByteArray &input) const
{
    if (input.size() - start < 8)
        return input.npos;

    auto data = input.data<const uchar *>(start);
    Q_ASSERT(data[0] == config.first().address);
    Q_ASSERT(data[1] == 0x01);
    quint16 bcs = qFromLittleEndian<quint16>(data + 2);
    std::size_t len = bcs & 0x3FF;
    Q_ASSERT(len >= 4 && len <= 1023);
    len += 4;
    if (input.size() - start < len)
        return input.npos;
    return start + len;
}

void AcquireAzonixUMAC1050::limitDataBuffer(Util::ByteArray &buffer)
{
    static constexpr std::size_t maxmimumSize = 4096;
    if (buffer.size() <= maxmimumSize)
        return;
    auto firstPossible = buffer.indexOf(config.first().address);
    if (firstPossible == buffer.npos) {
        buffer.clear();
        return;
    }
    auto targetSize = buffer.size() - firstPossible;
    if (targetSize > maxmimumSize)
        targetSize = maxmimumSize;
    buffer.pop_front(buffer.size() - targetSize);
}

int AcquireAzonixUMAC1050::verifyPacket(const uchar *data, int length, int sequenceNumber) const
{
    if (length < 8)
        return 1;
    if (data[0] != config.first().address)
        return -1;
    if (data[1] != 0x01)
        return -2;

    quint16 bcs = qFromLittleEndian<quint16>(data + 2);
    int len = bcs & 0x3FF;
    if (len < 3)
        return 2;
    if (len > 1023)
        return 3;
    if (len + 4 != length)
        return 4;
    if (sequenceNumber != -1 && sequenceNumber != (int) (bcs >> 10))
        return 5;
    if (crc.calculate(data, length) != 0)
        return 6;

    return 0;
}

int AcquireAzonixUMAC1050::verifyDataFrame(const uchar *data, int length) const
{
    switch (data[4] & 0xF) {
    case 0x01:
    case 0x03:
        break;
    default:
        return 100;
    }

    if (data[5] & 0x80 || (data[4] & 0x0F) == 0x03) {
        if (length < 10)
            return 101;
    }

    return 0;
}

void AcquireAzonixUMAC1050::describeState(Variant::Write &info) const
{
    if (!commandQueue.isEmpty()) {
        info.hash("Command").set(commandQueue.first().stateDescription());
    }

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        info.hash("ResponseState").setString("AutoprobePassiveInitialize");
        break;
    case RESP_AUTOPROBE_PASSIVE_WAIT:
        info.hash("ResponseState").setString("AutoprobePassiveWait");
        break;
    case RESP_PASSIVE_WAIT:
        info.hash("ResponseState").setString("PassiveWait");
        break;
    case RESP_PASSIVE_RUN:
        info.hash("ResponseState").setString("PassiveRun");
        break;
    case RESP_INTERACTIVE_RUN_READ_ANALOG:
        info.hash("ResponseState").setString("InteractiveRunReadAnalog");
        break;
    case RESP_INTERACTIVE_RUN_READ_SENSORS:
        info.hash("ResponseState").setString("InteractiveRunReadSensors");
        break;
    case RESP_INTERACTIVE_RUN_WRITE_ANALOG:
        info.hash("ResponseState").setString("InteractiveRunWriteAnalog");
        info.hash("UpdateChannel").setInt64(updateChannel);
        break;
    case RESP_INTERACTIVE_RUN_WRITE_DIGITAL:
        info.hash("ResponseState").setString("InteractiveRunWriteDigital");
        info.hash("UpdateChannel").setInt64(updateChannel);
        break;
    case RESP_INTERACTIVE_RUN_WAIT:
        info.hash("ResponseState").setString("InteractiveRunWait");
        break;
    case RESP_INTERACTIVE_INITIALIZE:
        info.hash("ResponseState").setString("InteractiveInitialize");
        break;
    case RESP_INTERACTIVE_START_FLUSH:
        info.hash("ResponseState").setString("InteractiveStartFlush");
        break;
    case RESP_INTERACTIVE_START_RESET:
        info.hash("ResponseState").setString("InteractiveStartReset");
        break;
    case RESP_INTERACTIVE_START_CONFIG_LOAD:
        info.hash("ResponseState").setString("InteractiveStartConfigLoad");
        break;
    case RESP_INTERACTIVE_START_READ_REVISION:
        info.hash("ResponseState").setString("InteractiveStartReadRevision");
        break;
    case RESP_INTERACTIVE_START_READ_ANALOG:
        info.hash("ResponseState").setString("InteractiveStartReadAnalog");
        break;
    case RESP_INTERACTIVE_RESTART_WAIT:
        info.hash("ResponseState").setString("InteractiveRestartWait");
        break;
    }
}

AcquireAzonixUMAC1050::Command::Command() : type(COMMAND_INVALID), packet()
{ }

AcquireAzonixUMAC1050::Command::Command(CommandType t, Util::ByteArray p) : type(t),
                                                                            packet(std::move(p))
{ }

AcquireAzonixUMAC1050::Command::Command(const Command &) = default;

AcquireAzonixUMAC1050::Command &AcquireAzonixUMAC1050::Command::operator=(const Command &) = default;

AcquireAzonixUMAC1050::Command::Command(Command &&) = default;

AcquireAzonixUMAC1050::Command &AcquireAzonixUMAC1050::Command::operator=(Command &&) = default;


/* No separate way in Qt to determine floating point endianness, so
 * just use the integer one. */
static double unpackFloat(const uchar *data)
{
    quint32 i = qFromLittleEndian<quint32>(data);
    if ((i & 0x7F800000) == 0x7F800000)
        return FP::undefined();
    float f;
    memcpy(&f, &i, 4);
    double d = (double) f;
    if (!FP::defined(d))
        return FP::undefined();
    return d;
}

static void packFloat(double v, uchar *data)
{
    if (!FP::defined(v)) {
        memset(data, 0, 4);
        return;
    }
    quint32 i;
    float f = (float) v;
    memcpy(&i, &f, 4);
    qToLittleEndian<quint32>(i, data);
}

Variant::Root AcquireAzonixUMAC1050::Command::stateDescription() const
{
    Variant::Root result;
    result["Packet"].setBinary(packet);

    const uchar *data = packet.data<const uchar *>();
    std::size_t length = packet.size();

    if (length >= 6) {
        result["CommandNumber"].setInt64(data[5]);
    }

    switch (type) {
    default:
        result["Type"].setString("Unknown");
        break;
    case COMMAND_AIN:
        result["Type"].setString("AIN");
        if (length >= 8) {
            result["StartChannel"].setInt64(data[6]);
            result["EndChannel"].setInt64(data[7]);
        }
        break;
    case COMMAND_AOT:
        result["Type"].setString("AOT");
        if (length >= 12) {
            result["StartChannel"].setInt64(data[6]);
            result["EndChannel"].setInt64(data[7]);
            result["Value"].setDouble(unpackFloat(&data[8]));
        }
        break;
    case COMMAND_DOT:
        result["Type"].setString("DOT");
        if (length >= 10) {
            result["StartPort"].setInt64(data[6]);
            result["EndPort"].setInt64(data[7]);
            result["Mask"].setInt64(data[8]);
            result["Value"].setInt64(data[9]);
        }
        break;
    case COMMAND_CNFGLD:
        result["Type"].setString("CNFGLD");
        if (length >= 7) {
            result["LoadEEPROM"].setBool(data[6] != 0);
        }
        break;
    case COMMAND_RESET:
        result["Type"].setString("RESET");
        break;
    case COMMAND_REV:
        result["Type"].setString("REV");
        break;
    }
    return result;
}

int AcquireAzonixUMAC1050::Command::sequenceNumber() const
{
    if (packet.size() < 4)
        return 0;
    quint16 bcs = qFromLittleEndian<quint16>(packet.data<const uchar *>(2));
    return (int) (bcs >> 10);
}

void AcquireAzonixUMAC1050::queueCommand(CommandType command, const Util::ByteArray &payload)
{
    Q_ASSERT(payload.size() < 1023 - 4);
    Q_ASSERT(command != COMMAND_INVALID);
    Q_ASSERT(command != COMMAND_IGNORED);

    ++issueSequenceNumber;

    Util::ByteArray frame;
    frame.resize(6);
    uchar *data = (uchar *) frame.data();
    data[0] = config.first().address;
    data[1] = 0x01;

    quint16 bcs = (quint16) (payload.size() + 4);
    bcs |= (quint16) (issueSequenceNumber & 0x3F) << 10;
    qToLittleEndian(bcs, &data[2]);
    data[4] = 0;
    data[5] = (quint8) command;

    frame += payload;

    quint16 v = crc.calculate(frame.toQByteArrayRef());
    int offset = frame.size();
    frame.resize(offset + 2);
    qToLittleEndian(v, (uchar *) frame.data() + offset);

    if (controlStream != NULL) {
        controlStream->writeControl(frame);
    }
    commandQueue.append(Command(command, frame));
}

void AcquireAzonixUMAC1050::malformedCommand(const Util::ByteArray &frame,
                                             double frameTime,
                                             int code)
{
    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
        break;

    case RESP_INTERACTIVE_RESTART_WAIT:
        break;

    case RESP_PASSIVE_RUN:
        break;

    case RESP_INTERACTIVE_RUN_READ_ANALOG:
    case RESP_INTERACTIVE_RUN_READ_SENSORS:
    case RESP_INTERACTIVE_RUN_WRITE_ANALOG:
    case RESP_INTERACTIVE_RUN_WRITE_DIGITAL:
    case RESP_INTERACTIVE_RUN_WAIT:
        qCDebug(log) << "Malformed command of code" << code << "in interactive run state"
                     << responseState << "at" << Logging::time(frameTime);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("MalformedCommand"),
                                  frameTime, FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            describeState(info);
            info.hash("Packet").setBinary(frame);
            info.hash("Code").setInt64(code);
            event(frameTime,
                  QObject::tr("Malformed command (code %1).  Communications dropped.").arg(code),
                  true, info);
        }

        commandQueue.clear();
        responseState = RESP_INTERACTIVE_START_FLUSH;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 1.0);
        if (controlStream != NULL) {
            controlStream->writeControl(Util::ByteArray::filled(0, 64));
        }
        generalStatusUpdated();
        break;

    case RESP_INTERACTIVE_INITIALIZE:
    case RESP_INTERACTIVE_START_FLUSH:
    case RESP_INTERACTIVE_START_RESET:
    case RESP_INTERACTIVE_START_CONFIG_LOAD:
    case RESP_INTERACTIVE_START_READ_REVISION:
    case RESP_INTERACTIVE_START_READ_ANALOG:
        qCDebug(log) << "Malformed command of code " << code << " in interactive start state "
                     << responseState << " at " << Logging::time(frameTime);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("MalformedCommand"),
                                  frameTime, FP::undefined()));
        }

        commandQueue.clear();
        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        break;
    }

    invalidateLogValues(frameTime);
}

void AcquireAzonixUMAC1050::invalidResponse(const Util::ByteArray &frame,
                                            double frameTime,
                                            int code)
{
    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
        break;

    case RESP_INTERACTIVE_RESTART_WAIT:
        break;

    case RESP_PASSIVE_RUN:
        break;

    case RESP_INTERACTIVE_RUN_READ_ANALOG:
    case RESP_INTERACTIVE_RUN_READ_SENSORS:
    case RESP_INTERACTIVE_RUN_WRITE_ANALOG:
    case RESP_INTERACTIVE_RUN_WRITE_DIGITAL:
    case RESP_INTERACTIVE_RUN_WAIT:
        qCDebug(log) << "Invalid response payload code" << code << "in interactive run state"
                     << responseState << "at" << Logging::time(frameTime);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                  frameTime, FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            describeState(info);
            info.hash("Packet").setBinary(frame);
            info.hash("Code").setInt64(code);
            event(frameTime,
                  QObject::tr("Invalid response (code %1).  Communications dropped.").arg(code),
                  true, info);
        }

        commandQueue.clear();
        responseState = RESP_INTERACTIVE_START_FLUSH;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 1.0);
        if (controlStream != NULL) {
            controlStream->writeControl(Util::ByteArray::filled(0, 64));
        }
        generalStatusUpdated();
        break;

    case RESP_INTERACTIVE_INITIALIZE:
    case RESP_INTERACTIVE_START_FLUSH:
    case RESP_INTERACTIVE_START_RESET:
    case RESP_INTERACTIVE_START_CONFIG_LOAD:
    case RESP_INTERACTIVE_START_READ_REVISION:
    case RESP_INTERACTIVE_START_READ_ANALOG:
        qCDebug(log) << "Invalid response payload code" << code << "in interactive start state"
                     << responseState << "at" << Logging::time(frameTime);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                  frameTime, FP::undefined()));
        }

        commandQueue.clear();
        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        break;
    }

    invalidateLogValues(frameTime);
}

void AcquireAzonixUMAC1050::commandSequencingError(const Util::ByteArray &frame,
                                                   double frameTime,
                                                   int code)
{
    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
        break;

    case RESP_INTERACTIVE_RESTART_WAIT:
        break;

    case RESP_PASSIVE_RUN:
        break;

    case RESP_INTERACTIVE_RUN_READ_ANALOG:
    case RESP_INTERACTIVE_RUN_READ_SENSORS:
    case RESP_INTERACTIVE_RUN_WRITE_ANALOG:
    case RESP_INTERACTIVE_RUN_WRITE_DIGITAL:
    case RESP_INTERACTIVE_RUN_WAIT:
        qCDebug(log) << "Command sequencing error code" << code << "in interactive run state"
                     << responseState << "at" << Logging::time(frameTime);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("CommandSequencingError"),
                                  frameTime, FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            describeState(info);
            info.hash("Packet").setBinary(frame);
            info.hash("Code").setInt64(code);
            event(frameTime,
                  QObject::tr("Command sequencing error (code %1).  Communications dropped.").arg(
                          code), true, info);
        }

        commandQueue.clear();
        responseState = RESP_INTERACTIVE_START_FLUSH;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 1.0);
        if (controlStream != NULL) {
            controlStream->writeControl(Util::ByteArray::filled(0, 64));
        }
        generalStatusUpdated();
        break;

    case RESP_INTERACTIVE_INITIALIZE:
    case RESP_INTERACTIVE_START_FLUSH:
    case RESP_INTERACTIVE_START_RESET:
    case RESP_INTERACTIVE_START_CONFIG_LOAD:
    case RESP_INTERACTIVE_START_READ_REVISION:
    case RESP_INTERACTIVE_START_READ_ANALOG:
        qCDebug(log) << "Command sequencing error code" << code << "in interactive start state"
                     << responseState << "at" << Logging::time(frameTime);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("CommandSequencingError"),
                                  frameTime, FP::undefined()));
        }

        commandQueue.clear();
        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        break;
    }
    invalidateLogValues(frameTime);
}


void AcquireAzonixUMAC1050::configurationChanged()
{
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;

    for (QMap<int, double>::const_iterator add = config.first().initializeAnalog.constBegin(),
            endAdd = config.first().initializeAnalog.constEnd(); add != endAdd; ++add) {
        if (!FP::defined(add.value()))
            continue;
        QMap<int, double>::const_iterator check = analogUpdate.constFind(add.key());
        if (check == analogUpdate.constEnd() || !FP::defined(check.value())) {
            analogUpdate.insert(add.key(), add.value());
        }

        check = analogTarget.constFind(add.key());
        if (check == analogTarget.constEnd() || !FP::defined(check.value())) {
            analogTarget.insert(add.key(), add.value());
        }
    }

    for (QMap<int, bool>::const_iterator add = config.first().initializeDigital.constBegin(),
            endAdd = config.first().initializeDigital.constEnd(); add != endAdd; ++add) {
        QMap<int, bool>::const_iterator check = digitalUpdate.constFind(add.key());
        if (check == digitalUpdate.constEnd()) {
            digitalUpdate.insert(add.key(), add.value());
        }

        check = digitalTarget.constFind(add.key());
        if (check == digitalTarget.constEnd()) {
            digitalTarget.insert(add.key(), add.value());
        }
    }

    auto variables = instrumentMeta["VariableIndex"];
    variables.remove();
    for (auto channelVariables = config.first().variables.constBegin(),
            endCV = config.first().variables.constEnd();
            channelVariables != endCV;
            ++channelVariables) {
        if (channelVariables.value().empty())
            continue;

        auto index = channelVariables.key();
        if (index < 0 || index >= config.first().analogChannelCount)
            continue;

        const auto &variable = channelVariables.value().front();
        variables.array(static_cast<std::size_t>(index)).setString(variable.name);
    }

    auto output = instrumentMeta["OutputIndex"];
    output.remove();
    for (const auto &channel: config.first().analogNames) {
        output.array(static_cast<std::size_t>(channel.second)).setString(channel.first);
    }

    auto digital = instrumentMeta["DigitalIndex"];
    digital.remove();
    for (const auto &channel: config.first().digitalNames) {
        digital.array(static_cast<std::size_t>(channel.second)).setString(channel.first);
    }

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    sourceMetadataUpdated();
}

void AcquireAzonixUMAC1050::configurationAdvance(double frameTime)
{
    Q_ASSERT(!config.isEmpty());
    int oldSize = config.size();
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.isEmpty());

    if (oldSize == config.size())
        return;
    configurationChanged();
}

void AcquireAzonixUMAC1050::readAllAnalogInputs(double frameTime)
{
    Util::ByteArray payload;
    payload.push_back(config.first().analogChannelStart);
    payload.push_back(config.first().analogChannelStart + config.first().analogChannelCount - 1);
    queueCommand(COMMAND_AIN, payload);
    responseState = RESP_INTERACTIVE_RUN_READ_ANALOG;
    timeoutAt(frameTime + 2.0);
}

void AcquireAzonixUMAC1050::readAnalogSensors(double frameTime)
{
    Util::ByteArray payload;
    payload.push_back(60);
    payload.push_back(61);
    queueCommand(COMMAND_AIN, payload);
    responseState = RESP_INTERACTIVE_RUN_READ_SENSORS;
    timeoutAt(frameTime + 2.0);
}


bool AcquireAzonixUMAC1050::issueNextAnalogUpdate()
{
    QMap<int, double>::const_iterator next = analogUpdate.lowerBound(updateChannel);
    for (; next != analogUpdate.constEnd() && !FP::defined(next.value()); ++next) { }
    if (next == analogUpdate.constEnd())
        return false;
    int channelIndex = next.key();
    if (channelIndex < 0 || channelIndex > 9)
        return false;
    updateChannel = next.key() + 1;

    Util::ByteArray payload;
    payload.push_back(channelIndex);
    payload.push_back(channelIndex);

    int offset = payload.size();
    payload.resize(offset + 4);
    packFloat(next.value(), (uchar *) payload.data() + offset);

    queueCommand(COMMAND_AOT, payload);
    return true;
}

bool AcquireAzonixUMAC1050::issueNextDigitalUpdate()
{
    QMap<int, bool>::const_iterator next = digitalUpdate.lowerBound(updateChannel);
    if (next == digitalUpdate.constEnd())
        return false;
    int channelIndex = next.key();
    if (channelIndex < 0 || channelIndex >= 9 * 8)
        return false;
    int port = channelIndex / 8;
    int bit = channelIndex % 8;
    updateChannel = next.key() + 1;

    quint8 mask = 1 << bit;
    quint8 value = next.value() ? mask : 0;
    for (++next; next != digitalUpdate.constEnd(); ++next) {
        int addIndex = next.key();
        int addPort = addIndex / 8;
        if (addPort != port)
            break;
        bit = addIndex % 8;
        updateChannel = next.key() + 1;

        quint8 b = 1 << bit;
        mask |= b;
        if (next.value())
            value |= b;
    }


    Util::ByteArray payload;
    payload.push_back(port);
    payload.push_back(port);
    payload.push_back(mask);
    payload.push_back(value);
    queueCommand(COMMAND_DOT, payload);
    return true;
}

void AcquireAzonixUMAC1050::updateMaximumDisplay(double frameTime, int max)
{
    if (max <= maximumDisplayChannel)
        return;
    if (realtimeEgress == NULL)
        return;

    maximumDisplayChannel = max;

    Variant::Root output;
    for (int i = 0; i < maximumDisplayChannel; i++) {
        output.write().array(i).setInt64(i);
    }
    realtimeEgress->incomingData(
            SequenceValue({{}, "raw", "ZINDEX"}, std::move(output), frameTime, FP::undefined()));
}

void AcquireAzonixUMAC1050::updateLogMeta(double frameTime)
{
    if (loggingEgress == NULL) {
        haveEmittedLogMeta = false;
        loggingMux.clear();
        memset(streamAge, 0, sizeof(streamAge));
        for (int i = 0; i < LogStream_TOTAL; i++) {
            streamTime[i] = FP::undefined();
        }
        lastInputs.write().setEmpty();
    } else {
        if (!haveEmittedLogMeta) {
            haveEmittedLogMeta = true;
            loggingMux.incoming(LogStream_Metadata, buildLogMeta(frameTime), loggingEgress);
        }
        loggingMux.advance(LogStream_Metadata, frameTime, loggingEgress);
    }
}

void AcquireAzonixUMAC1050::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configurationAdvance(frameTime);

    if (!haveEmittedRealtimeMeta && realtimeEgress != NULL) {
        haveEmittedRealtimeMeta = true;
        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));

        /* Output all these now, since this is likely the first time and
         * we want them to show even if they're not updated for a while */
        digitalOutputValues.setStart(frameTime);
        digitalOutputValues.setEnd(FP::undefined());
        digitalOutputBits.setStart(frameTime);
        digitalOutputBits.setEnd(FP::undefined());
        analogOutputValues.setStart(frameTime);
        analogOutputValues.setEnd(FP::undefined());
        realtimeEgress->incomingData(digitalOutputValues);
        realtimeEgress->incomingData(digitalOutputBits);
        realtimeEgress->incomingData(analogOutputValues);

        maximumDisplayChannel = -1;
    }

    const uchar *data = frame.data<const uchar *>();
    {
        int code;
        if (commandQueue.isEmpty()) {
            code = verifyPacket(data, frame.size());
        } else {
            code = verifyPacket(data, frame.size(), commandQueue.first().sequenceNumber());
        }
        if (code == 0) {
            code = verifyDataFrame(data, frame.size());
        }

        if (code > 0) {
            switch (responseState) {
            case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
            case RESP_AUTOPROBE_PASSIVE_WAIT:
            case RESP_PASSIVE_WAIT:
                break;

            case RESP_INTERACTIVE_RESTART_WAIT:
                break;

            case RESP_PASSIVE_RUN:
                qCDebug(log) << "Data frame rejected with code" << code << "in passive mode at"
                             << Logging::time(frameTime);

                {
                    Variant::Write info = Variant::Write::empty();
                    describeState(info);
                    info.hash("Packet").setBinary(frame);
                    info.hash("Code").setInt64(code);
                    event(frameTime, QObject::tr(
                            "Invalid response (code %1) in passive mode.  Communications dropped.").arg(
                            code), true, info);
                }

                responseState = RESP_PASSIVE_WAIT;
                timeoutAt(FP::undefined());
                generalStatusUpdated();
                break;

            case RESP_INTERACTIVE_RUN_READ_ANALOG:
            case RESP_INTERACTIVE_RUN_READ_SENSORS:
            case RESP_INTERACTIVE_RUN_WRITE_ANALOG:
            case RESP_INTERACTIVE_RUN_WRITE_DIGITAL:
            case RESP_INTERACTIVE_RUN_WAIT:
                qCDebug(log) << "Data frame rejected with code" << code << "in interactive state"
                             << responseState << "at" << Logging::time(frameTime);

                if (realtimeEgress != NULL) {
                    realtimeEgress->incomingData(
                            SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                          frameTime, FP::undefined()));
                }

                {
                    Variant::Write info = Variant::Write::empty();
                    describeState(info);
                    info.hash("Packet").setBinary(frame);
                    info.hash("Code").setInt64(code);
                    event(frameTime,
                          QObject::tr("Invalid response (code %1).  Communications dropped.").arg(
                                  code), true, info);
                }

                commandQueue.clear();
                responseState = RESP_INTERACTIVE_START_FLUSH;
                timeoutAt(frameTime + 10.0);
                discardData(frameTime + 1.0);
                if (controlStream != NULL) {
                    controlStream->writeControl(Util::ByteArray::filled(0, 64));
                }
                generalStatusUpdated();
                break;

            case RESP_INTERACTIVE_INITIALIZE:
            case RESP_INTERACTIVE_START_FLUSH:
            case RESP_INTERACTIVE_START_RESET:
            case RESP_INTERACTIVE_START_CONFIG_LOAD:
            case RESP_INTERACTIVE_START_READ_REVISION:
            case RESP_INTERACTIVE_START_READ_ANALOG:
                qCDebug(log) << "Interactive start comms state" << responseState << "failed at"
                             << Logging::time(frameTime) << "with code" << code;

                if (realtimeEgress != NULL) {
                    realtimeEgress->incomingData(
                            SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                          frameTime, FP::undefined()));
                }

                commandQueue.clear();
                responseState = RESP_INTERACTIVE_RESTART_WAIT;
                if (controlStream != NULL)
                    controlStream->resetControl();
                timeoutAt(FP::undefined());
                discardData(frameTime + 10.0);

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
                }
                autoprobeStatusUpdated();

                /* Don't output this verbosity if we're getting failures
                 * at the very start */
                if (responseState != RESP_INTERACTIVE_START_RESET &&
                        responseState != RESP_INTERACTIVE_START_FLUSH) {
                    Variant::Write info = Variant::Write::empty();
                    describeState(info);
                    info.hash("Packet").setBinary(frame);
                    info.hash("Code").setInt64(code);
                    event(frameTime,
                          QObject::tr("Communications start failed (code %1).").arg(code), true,
                          info);
                }
                break;
            }

            invalidateLogValues(frameTime);
            return;
        } else if (code < 0) {
            /* Not intended for us */
            return;
        }
    }
    if (data[5] & 0x80 || (data[4] & 0x0F) == 0x03) {
        /* Error packet */
        Q_ASSERT(frame.size() >= 10);
        quint16 errorCode = qFromLittleEndian<quint16>(data + 6);

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            /* Require a non-erroring command before accepting these */
            if (!commandQueue.isEmpty()) {
                commandQueue.takeFirst();
            }
            break;

        case RESP_INTERACTIVE_RESTART_WAIT:
            commandQueue.clear();
            break;

        case RESP_PASSIVE_RUN:
            /* This is still communications, so ignore the failure */
            if (!commandQueue.isEmpty()) {
                qCDebug(log) << "Instrument error" << errorCode << "received in passive mode at"
                             << Logging::time(frameTime) << "in response to command"
                             << commandQueue.first().getType();
                commandQueue.takeFirst();
            } else {
                qCDebug(log) << "Instrument error" << errorCode << "received in passive mode at"
                             << Logging::time(frameTime);
            }
            break;

        case RESP_INTERACTIVE_RUN_READ_ANALOG:
        case RESP_INTERACTIVE_RUN_READ_SENSORS:
        case RESP_INTERACTIVE_RUN_WRITE_ANALOG:
        case RESP_INTERACTIVE_RUN_WRITE_DIGITAL:
        case RESP_INTERACTIVE_RUN_WAIT:
            qCDebug(log) << "Instrument error" << errorCode << "received in interactive state"
                         << responseState << "at" << Logging::time(frameTime);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        QObject::tr("Error: %1").arg(errorCode)), frameTime, FP::undefined()));
            }

            {
                Variant::Write info = Variant::Write::empty();
                describeState(info);
                info.hash("Packet").setBinary(frame);
                info.hash("Error").setInt64(errorCode);
                event(frameTime,
                      QObject::tr("Instrument error %1 received.  Communications dropped.").arg(
                              errorCode), true, info);
            }

            commandQueue.clear();
            responseState = RESP_INTERACTIVE_START_FLUSH;
            timeoutAt(frameTime + 10.0);
            discardData(frameTime + 1.0);
            if (controlStream != NULL) {
                controlStream->writeControl(Util::ByteArray::filled(0, 64));
            }
            generalStatusUpdated();
            break;

        case RESP_INTERACTIVE_INITIALIZE:
        case RESP_INTERACTIVE_START_FLUSH:
        case RESP_INTERACTIVE_START_RESET:
        case RESP_INTERACTIVE_START_CONFIG_LOAD:
        case RESP_INTERACTIVE_START_READ_REVISION:
        case RESP_INTERACTIVE_START_READ_ANALOG:
            qCDebug(log) << "Interactive start comms state" << responseState << "failed at"
                         << Logging::time(frameTime) << "with instrument error" << errorCode;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        QObject::tr("Error: %1").arg(errorCode)), frameTime, FP::undefined()));
            }

            /* Don't output this verbosity if we're getting failures
             * at the very start */
            if (responseState != RESP_INTERACTIVE_START_RESET ||
                    responseState == RESP_INTERACTIVE_START_FLUSH) {
                Variant::Write info = Variant::Write::empty();
                describeState(info);
                info.hash("Packet").setBinary(frame);
                info.hash("Error").setInt64(errorCode);
                event(frameTime,
                      QObject::tr("Communications start failed (instrument error %1).").arg(
                              errorCode), true, info);
            }

            commandQueue.clear();
            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            break;
        }

        invalidateLogValues(frameTime);
        return;
    }

    /* Don't know what do do with this response, so ignore it */
    if (commandQueue.isEmpty())
        return;
    Command command(commandQueue.takeFirst());

    data += 6;
    int length = frame.size() - 8;

    Util::ByteArray commandPacket(command.getPacket());
    const uchar *commandData = commandPacket.data<const uchar *>(6);
    int commandLength = commandPacket.size() - 8;

    switch (command.getType()) {
    case COMMAND_IGNORED:
    default:
        break;

    case COMMAND_INVALID:
        break;

    case COMMAND_AIN: {
        if (commandLength != 2) {
            malformedCommand(frame, frameTime, 1000);
            return;
        }
        int startChannel = commandData[0];
        int endChannel = commandData[1];
        if (startChannel < 0 || startChannel > 61) {
            malformedCommand(frame, frameTime, 1001);
            return;
        }
        if (endChannel < 0 || endChannel > 61) {
            malformedCommand(frame, frameTime, 1002);
            return;
        }
        if (endChannel < startChannel) {
            malformedCommand(frame, frameTime, 1003);
            return;
        }
        if (length != 4 * ((endChannel + 1) - startChannel)) {
            invalidResponse(frame, frameTime, 1004);
            return;
        }

        updateLogMeta(frameTime);

        int channelBegin = config.first().analogChannelStart;
        int channelCount = config.first().analogChannelCount;

        Variant::Root T;
        Variant::Root V;
        bool haveConventionalValues = false;
        for (int channel = startChannel; channel <= endChannel; ++channel, data += 4) {
            if (channel == 60) {
                T.write().setDouble(unpackFloat(data));
                remap("T", T);
                continue;
            } else if (channel == 61) {
                V.write().setDouble(unpackFloat(data));
                remap("V", V);
                continue;
            }

            int effectiveChannel = channel - channelBegin;
            if (effectiveChannel < 0 || effectiveChannel >= channelCount)
                continue;

            Variant::Root v(unpackFloat(data));
            remap("ZIN" + std::to_string(effectiveChannel), v);
            lastInputs.write().array(effectiveChannel).set(v);
            haveConventionalValues = true;
        }
        if (haveConventionalValues) {
            remap("ZINPUTS", lastInputs);
            updateMaximumDisplay(frameTime, (endChannel - channelBegin) + 1);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZINPUTS"}, lastInputs, frameTime,
                                      frameTime + 1.0));
            }
        }
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "F1"}, Variant::Root(Variant::Type::Flags), frameTime,
                                  frameTime + 1.0));
        }

        for (int channel = startChannel; channel <= endChannel; ++channel) {
            int effectiveChannel;
            int streamID;
            Variant::Read inputValue;

            SequenceValue::Transfer outputData;

            if (channel == 60) {
                effectiveChannel = channel;
                inputValue = T;
                streamID = LogStream_T;

                outputData.emplace_back(
                        SequenceIdentity({{}, "raw", "T"}, frameTime, frameTime + 1.0), T);
            } else if (channel == 61) {
                effectiveChannel = channel;
                inputValue = V;
                streamID = LogStream_V;

                outputData.emplace_back(
                        SequenceIdentity({{}, "raw", "V"}, frameTime, frameTime + 1.0), V);
            } else {
                effectiveChannel = channel - channelBegin;
                if (effectiveChannel < 0 || effectiveChannel >= channelCount)
                    continue;
                inputValue = lastInputs.write().array(effectiveChannel);
                streamID = LogStream_AIN_Begin + channel;
            }

            QList<OutputVariable> channelVariables(config.first().variables[effectiveChannel]);
            for (QList<OutputVariable>::const_iterator var = channelVariables.constBegin(),
                    endVar = channelVariables.constEnd(); var != endVar; ++var) {

                Variant::Root outputValue(var->calibration.apply(inputValue.toDouble()));
                remap(var->name, outputValue);

                outputData.emplace_back(
                        SequenceIdentity({{}, "raw", var->name}, frameTime, frameTime + 1.0),
                        std::move(outputValue));
            }

            logValues(frameTime, streamID, std::move(outputData));
        }

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
            responseState = RESP_PASSIVE_RUN;
            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            qCDebug(log) << "Autoprobe succeeded at" << Logging::time(frameTime);

            if (commandQueue.isEmpty())
                timeoutAt(frameTime + config.first().pollInterval + 2.0);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                      FP::undefined()));
            }

            {
                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("AutoprobePassiveWait");
                event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);
            }
            break;

        case RESP_PASSIVE_WAIT:
            responseState = RESP_PASSIVE_RUN;
            generalStatusUpdated();

            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);

            if (commandQueue.isEmpty())
                timeoutAt(frameTime + config.first().pollInterval + 2.0);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                      FP::undefined()));
            }

            {
                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("PassiveWait");
                event(frameTime, QObject::tr("Passive communications established."), false, info);
            }
            break;

        case RESP_PASSIVE_RUN:
            break;
        case RESP_INTERACTIVE_RESTART_WAIT:
            break;

        case RESP_INTERACTIVE_START_READ_ANALOG:
            responseState = RESP_INTERACTIVE_RUN_READ_ANALOG;

            qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                      FP::undefined()));
            }

            {
                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("InteractiveStartReadAnalog");
                event(frameTime, QObject::tr("Communications established."), false, info);
            }

            analogUpdate = analogTarget;
            digitalUpdate = digitalTarget;

            /* Fall through */
        case RESP_INTERACTIVE_RUN_READ_ANALOG:
        case RESP_INTERACTIVE_RUN_READ_SENSORS:
            updateChannel = 0;
            if (!analogUpdate.isEmpty() && issueNextAnalogUpdate()) {
                responseState = RESP_INTERACTIVE_RUN_WRITE_ANALOG;
                timeoutAt(frameTime + 2.0);
            } else if (!digitalUpdate.isEmpty() && issueNextDigitalUpdate()) {
                responseState = RESP_INTERACTIVE_RUN_WRITE_DIGITAL;
                timeoutAt(frameTime + 2.0);
            } else if (responseState == RESP_INTERACTIVE_RUN_READ_ANALOG) {
                readAnalogSensors(frameTime);
            } else {
                double poll = config.first().pollInterval;
                if (!FP::defined(poll) || poll <= 0.0) {
                    readAllAnalogInputs(frameTime);
                } else {
                    responseState = RESP_INTERACTIVE_RUN_WAIT;
                    timeoutAt(frameTime + poll);
                }
            }
            break;

        default:
            commandSequencingError(frame, frameTime, 1005);
            break;
        }

        break;
    }

    case COMMAND_AOT: {
        if (commandLength != 6) {
            malformedCommand(frame, frameTime, 2000);
            return;
        }
        int startChannel = commandData[0];
        int endChannel = commandData[1];
        if (startChannel < 0 || startChannel > 9) {
            malformedCommand(frame, frameTime, 2001);
            return;
        }
        if (endChannel < 0 || endChannel > 9) {
            malformedCommand(frame, frameTime, 2002);
            return;
        }
        if (endChannel < startChannel) {
            malformedCommand(frame, frameTime, 2003);
            return;
        }
        updateChannel = startChannel + 1;

        double v = unpackFloat(commandData + 2);

        if (persistentEgress != NULL &&
                config.first().logOutputs &&
                FP::defined(analogOutputValues.getStart())) {
            analogOutputValues.setEnd(frameTime);
            persistentEgress->incomingData(analogOutputValues);
        }

        for (; startChannel <= endChannel; ++startChannel) {
            analogOutputValues.write().array(startChannel).setDouble(v);

            QMap<int, double>::iterator check = analogUpdate.find(startChannel);
            if (check == analogUpdate.end())
                continue;
            if (!qFuzzyCompare(check.value(), v))
                continue;
            analogUpdate.erase(check);
        }

        analogOutputValues.setStart(frameTime);
        analogOutputValues.setEnd(FP::undefined());
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(analogOutputValues);
        }

        if (config.first().logOutputs) {
            persistentValuesUpdated();
        }

        updateMaximumDisplay(frameTime, endChannel);

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_RUN:
        case RESP_INTERACTIVE_RESTART_WAIT:
            break;

        case RESP_INTERACTIVE_RUN_WRITE_ANALOG:
            if (!analogUpdate.isEmpty() && issueNextAnalogUpdate()) {
                responseState = RESP_INTERACTIVE_RUN_WRITE_ANALOG;
                timeoutAt(frameTime + 2.0);
            } else {
                updateChannel = 0;
                if (!digitalUpdate.isEmpty() && issueNextDigitalUpdate()) {
                    responseState = RESP_INTERACTIVE_RUN_WRITE_DIGITAL;
                    timeoutAt(frameTime + 2.0);
                } else {
                    readAnalogSensors(frameTime);
                }
            }
            break;

        default:
            commandSequencingError(frame, frameTime, 2004);
            break;
        }

        break;
    }

    case COMMAND_DOT: {
        if (commandLength != 4) {
            malformedCommand(frame, frameTime, 3000);
            return;
        }
        int startPort = commandData[0];
        int endPort = commandData[1];
        if (startPort < 0 || startPort > 9) {
            malformedCommand(frame, frameTime, 3001);
            return;
        }
        if (endPort < 0 || endPort > 9) {
            malformedCommand(frame, frameTime, 3002);
            return;
        }
        if (endPort < startPort) {
            malformedCommand(frame, frameTime, 3003);
            return;
        }
        updateChannel = (startPort + 1) * 8;

        quint8 mask = (quint8) commandData[2];
        quint8 value = (quint8) commandData[3];

        if (persistentEgress != NULL &&
                config.first().logOutputs &&
                FP::defined(digitalOutputValues.getStart())) {
            digitalOutputValues.setEnd(frameTime);
            persistentEgress->incomingData(digitalOutputValues);
        }

        for (; startPort <= endPort; ++startPort) {
            for (int bit = 0; bit < 8; ++bit) {
                quint8 b = 1 << bit;
                if (!(mask & b))
                    continue;

                int globalBit = startPort * 8 + bit;
                if (globalBit < 63) {
                    qint64 g = (qint64) 1 << globalBit;

                    qint64 in = digitalOutputBits.write().toInt64();
                    if (!INTEGER::defined(in))
                        in = 0;
                    if (value & b)
                        in |= g;
                    else
                        in &= ~g;
                    digitalOutputBits.write().setInt64(in);
                }
                digitalOutputValues.write().array(globalBit).setBool(value & b);

                QMap<int, bool>::iterator check = digitalUpdate.find(globalBit);
                if (check == digitalUpdate.end())
                    continue;
                if (value & b) {
                    if (!check.value())
                        continue;
                } else {
                    if (check.value())
                        continue;
                }
                digitalUpdate.erase(check);
            }
        }

        digitalOutputValues.setStart(frameTime);
        digitalOutputValues.setEnd(FP::undefined());
        digitalOutputBits.setStart(frameTime);
        digitalOutputBits.setEnd(FP::undefined());
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(digitalOutputValues);
            realtimeEgress->incomingData(digitalOutputBits);
        }

        if (config.first().logOutputs) {
            persistentValuesUpdated();
        }

        updateMaximumDisplay(frameTime, endPort * 8);

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_RUN:
        case RESP_INTERACTIVE_RESTART_WAIT:
            break;

        case RESP_INTERACTIVE_RUN_WRITE_DIGITAL:
            if (!digitalUpdate.isEmpty() && issueNextDigitalUpdate()) {
                responseState = RESP_INTERACTIVE_RUN_WRITE_DIGITAL;
                timeoutAt(frameTime + 2.0);
            } else {
                readAnalogSensors(frameTime);
            }
            break;

        default:
            commandSequencingError(frame, frameTime, 2004);
            break;
        }

        break;
    }

    case COMMAND_CNFGLD:
        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_RUN:
        case RESP_INTERACTIVE_RESTART_WAIT:
            break;

        case RESP_INTERACTIVE_START_CONFIG_LOAD:
            queueCommand(COMMAND_REV);
            responseState = RESP_INTERACTIVE_START_READ_REVISION;
            timeoutAt(frameTime + 2.0);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadRevision"), frameTime, FP::undefined()));
            }
            break;

        default:
            commandSequencingError(frame, frameTime, 3000);
            break;
        }
        break;

    case COMMAND_RESET:
        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_RUN:
        case RESP_INTERACTIVE_RESTART_WAIT:
            break;

        case RESP_INTERACTIVE_START_RESET:
            discardData(frameTime + 2.0);
            timeoutAt(frameTime + 4.0);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveResetWait"), frameTime, FP::undefined()));
            }
            break;

        default:
            commandSequencingError(frame, frameTime, 4000);
            break;
        }
        break;

    case COMMAND_REV: {
        if (length <= 0) {
            invalidResponse(frame, frameTime, 5000);
            return;
        }

        QString str(QString::fromLatin1((const char *) data, length));
        QRegExp reVersion("Rev\\.?\\s*(\\d+\\.\\d+)", Qt::CaseInsensitive);

        Variant::Root oldValue(instrumentMeta["FirmwareVersion"]);
        if (reVersion.indexIn(str) != -1 && !reVersion.cap(1).isEmpty()) {
            instrumentMeta["FirmwareVersion"].setString(reVersion.cap(1));
        } else {
            instrumentMeta["FirmwareVersion"].setString(str);
        }
        if (oldValue.read() != instrumentMeta["FirmwareVersion"]) {
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_RUN:
        case RESP_INTERACTIVE_RESTART_WAIT:
            break;

        case RESP_INTERACTIVE_START_READ_REVISION:
            readAllAnalogInputs(frameTime);
            responseState = RESP_INTERACTIVE_START_READ_ANALOG;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadAnalog"), frameTime, FP::undefined()));
            }
            break;

        default:
            commandSequencingError(frame, frameTime, 5001);
            break;
        }
        break;
    }
    }

    for (int i = LogStream_RecordBaseStart; i < LogStream_TOTAL; i++) {
        if (streamAge[i] < 2)
            continue;
        if (loggingEgress == NULL)
            continue;

        updateLogMeta(frameTime);

        if (FP::defined(streamTime[LogStream_State])) {
            double startTime = streamTime[LogStream_State];
            double endTime = frameTime;

            loggingMux.incoming(LogStream_State, SequenceValue({{}, "raw", "F1"},
                                                               Variant::Root(Variant::Type::Flags),
                                                               startTime, endTime), loggingEgress);
            loggingMux.incoming(LogStream_State,
                                SequenceValue({{}, "raw", "ZINPUTS"}, lastInputs, startTime,
                                              endTime), loggingEgress);
        } else {
            loggingMux.advance(LogStream_State, frameTime, loggingEgress);
        }
        streamTime[LogStream_State] = frameTime;

        for (i = LogStream_RecordBaseStart; i < LogStream_TOTAL; i++) {
            if (streamAge[i] == 0) {
                loggingMux.advance(i, frameTime, loggingEgress);
                streamTime[i] = FP::undefined();
            }
            streamAge[i] = 0;
        }
        break;
    }
}

void AcquireAzonixUMAC1050::incomingInstrumentTimeout(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
        commandQueue.clear();
        invalidateLogValues(frameTime);
        break;

    case RESP_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
        commandQueue.clear();
        break;

    case RESP_PASSIVE_RUN:
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        {
            Variant::Write info = Variant::Write::empty();
            describeState(info);
            event(frameTime, QObject::tr("Timeout waiting for response.  Communications Dropped."),
                  true, info);
        }

        commandQueue.clear();
        responseState = RESP_PASSIVE_WAIT;
        generalStatusUpdated();
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_RUN_READ_ANALOG:
    case RESP_INTERACTIVE_RUN_READ_SENSORS:
    case RESP_INTERACTIVE_RUN_WRITE_ANALOG:
    case RESP_INTERACTIVE_RUN_WRITE_DIGITAL:
        qCDebug(log) << "Timeout in interactive state" << responseState << "at"
                     << Logging::time(frameTime);

        {
            Variant::Write info = Variant::Write::empty();
            describeState(info);
            event(frameTime, QObject::tr("Timeout waiting for response.  Communications Dropped."),
                  true, info);
        }

        commandQueue.clear();
        responseState = RESP_INTERACTIVE_START_FLUSH;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 1.0);
        if (controlStream != NULL) {
            controlStream->writeControl(Util::ByteArray::filled(0, 64));
        }
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_RUN_WAIT:
        readAllAnalogInputs(frameTime);
        break;

    case RESP_INTERACTIVE_INITIALIZE:
        commandQueue.clear();
        responseState = RESP_INTERACTIVE_START_FLUSH;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 1.0);
        if (controlStream != NULL) {
            controlStream->writeControl(Util::ByteArray::filled(0, 64));
        }

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_FLUSH:
    case RESP_INTERACTIVE_START_RESET:
    case RESP_INTERACTIVE_START_CONFIG_LOAD:
    case RESP_INTERACTIVE_START_READ_REVISION:
    case RESP_INTERACTIVE_START_READ_ANALOG:
        qCDebug(log) << "Timeout in interactive start state" << responseState << "at"
                     << Logging::time(frameTime);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        commandQueue.clear();
        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        invalidateLogValues(frameTime);
        break;
    }
}

void AcquireAzonixUMAC1050::discardDataCompleted(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);

    switch (responseState) {
    case RESP_INTERACTIVE_START_RESET:
        queueCommand(COMMAND_CNFGLD, Util::ByteArray::filled(1, 1));
        responseState = RESP_INTERACTIVE_START_CONFIG_LOAD;
        timeoutAt(frameTime + 2.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveLoadConfiguration"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_RESTART_WAIT:
        commandQueue.clear();
        responseState = RESP_INTERACTIVE_START_FLUSH;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 1.0);
        if (controlStream != NULL) {
            controlStream->writeControl(Util::ByteArray::filled(0, 64));
        }
        break;

    case RESP_INTERACTIVE_START_FLUSH:
        commandQueue.clear();
        queueCommand(COMMAND_RESET);

        responseState = RESP_INTERACTIVE_START_RESET;
        timeoutAt(frameTime + 2.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveReset"),
                                  frameTime, FP::undefined()));
        }
        break;

    default:
        break;
    }
}

void AcquireAzonixUMAC1050::shutdownInProgress(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_WAIT:
    case RESP_INTERACTIVE_RUN_READ_ANALOG:
    case RESP_INTERACTIVE_RUN_READ_SENSORS:
    case RESP_INTERACTIVE_RUN_WRITE_ANALOG:
    case RESP_INTERACTIVE_RUN_WRITE_DIGITAL:
        /* Just issue all the commands needed, without waiting for
         * acknowledgments */

        analogUpdate = config.first().exitAnalog;
        digitalUpdate = config.first().exitDigital;

        updateChannel = 0;
        while (issueNextAnalogUpdate()) { }
        updateChannel = 0;
        while (issueNextDigitalUpdate()) { }
        break;

    default:
        /* No interactive communications, so no action needed */
        break;
    }
}


void AcquireAzonixUMAC1050::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);

    if (verifyPacket(frame.data<const uchar *>(), frame.size()) != 0)
        return;
    if (frame.size() < 8)
        return;

    bool isFirstCommand = commandQueue.isEmpty();
    commandQueue.append(Command(static_cast<CommandType>(frame[5]), frame));

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
    case RESP_AUTOPROBE_PASSIVE_WAIT:
        if (isFirstCommand)
            timeoutAt(frameTime + 10.0);
        break;
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
        if (isFirstCommand)
            timeoutAt(frameTime + 2.0);
        break;

    default:
        break;
    }
}

Variant::Root AcquireAzonixUMAC1050::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireAzonixUMAC1050::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RUN_READ_ANALOG:
    case RESP_INTERACTIVE_RUN_READ_SENSORS:
    case RESP_INTERACTIVE_RUN_WRITE_ANALOG:
    case RESP_INTERACTIVE_RUN_WRITE_DIGITAL:
    case RESP_INTERACTIVE_RUN_WAIT:
        return AcquisitionInterface::GeneralStatus::Normal;
        break;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
        break;
    }
}

int AcquireAzonixUMAC1050::convertAnalogChannel(const Variant::Read &value)
{
    {
        auto i = Variant::Composite::toInteger(value, true);
        if (INTEGER::defined(i) && i >= 0 && i <= 9)
            return (int) i;
    }

    Q_ASSERT(!config.isEmpty());

    const auto &check = value.toString();
    if (!check.empty()) {
        auto name = config.front().analogNames.find(check);
        if (name != config.front().analogNames.end()) {
            if (name->second >= 0 && name->second <= 9)
                return name->second;
        }
    }

    return -1;
}

int AcquireAzonixUMAC1050::convertDigitalChannel(const Variant::Read &value)
{
    {
        auto i = Variant::Composite::toInteger(value, true);
        if (INTEGER::defined(i) && i >= 0 && i < 9 * 8)
            return (int) i;
    }

    Q_ASSERT(!config.isEmpty());

    const auto &check = value.toString();
    if (!check.empty()) {
        auto name = config.front().digitalNames.find(check);
        if (name != config.front().digitalNames.end()) {
            if (name->second >= 0 && name->second < 9 * 8)
                return name->second;
        }
    }

    return -1;
}

QHash<int, double> AcquireAzonixUMAC1050::convertAnalogMultiple(const Variant::Read &value)
{
    switch (value.getType()) {
    case Variant::Type::Hash: {
        QHash<int, double> result;
        for (auto add : value.toHash()) {
            double v = add.second.toDouble();
            if (!FP::defined(v))
                v = add.second.hash("Value").toDouble();
            if (!FP::defined(v))
                continue;

            int index = -1;
            auto check = config.front().analogNames.find(add.first);
            if (check != config.front().analogNames.end()) {
                index = check->second;
            }
            if (index == -1) {
                bool ok = false;
                index = QString::fromStdString(add.first).toInt(&ok);
                if (!ok)
                    index = -1;
            }
            if (index < 0 || index > 9)
                continue;

            result.insert(index, v);
        }
        return result;
    }
    case Variant::Type::Array: {
        auto children = value.toArray();
        QHash<int, double> result;
        for (int idx = 0, max = std::min<int>(children.size(), 10); idx < max; ++idx) {
            double v = children[idx].toDouble();
            if (!FP::defined(v))
                v = children[idx].hash("Value").toDouble();
            if (!FP::defined(v))
                continue;
            result.insert(idx, v);
        }
        return result;
    }
    case Variant::Type::String: {
        QStringList channels(value.toQString().split(QRegExp("[:;,]+")));
        QHash<int, double> result;
        for (int idx = 0, max = qMin(channels.size(), 10); idx < max; ++idx) {
            bool ok = false;
            double v = channels.at(idx).toDouble(&ok);
            if (!ok || FP::defined(v))
                continue;
            result.insert(idx, v);
        }
        return result;
    }

    default:
        break;
    }

    return QHash<int, double>();
}


static bool getValidBoolean(const Variant::Read &v, bool &ok)
{
    if (v.getType() == Variant::Type::Boolean) {
        ok = true;
        return v.toBool();
    } else if (v.getType() == Variant::Type::String) {
        const auto &check = v.toString();
        if (!check.empty()) {
            ok = true;
            if (Util::equal_insensitive(check, "on", "true") ||
                    (QString::fromStdString(check).toInt(&ok) && ok))
                return true;
            return false;
        }
    }

    {
        qint64 i = v.toInt64();
        if (INTEGER::defined(i)) {
            ok = true;
            return i != 0;
        }
    }

    {
        double d = v.toDouble();
        if (FP::defined(d)) {
            ok = true;
            return d != 0.0;
        }
    }

    return false;
}

static qint64 getValidMask(const Variant::Read &v)
{
    if (v.getType() != Variant::Type::Integer)
        return INTEGER::undefined();
    return v.toInt64();
}


QHash<int, bool> AcquireAzonixUMAC1050::convertDigitalMultiple(const Variant::Read &value)
{
    switch (value.getType()) {
    case Variant::Type::Hash: {
        QHash<int, bool> result;
        for (auto add : value.toHash()) {
            bool ok = false;
            bool v = getValidBoolean(add.second, ok);
            if (!ok)
                v = getValidBoolean(add.second.hash("Value"), ok);
            if (!ok)
                continue;

            int index = -1;
            auto lookup = config.front().digitalNames.find(add.first);
            if (lookup != config.front().digitalNames.end())
                index = lookup->second;
            if (index == -1) {
                index = QString::fromStdString(add.first).toInt(&ok);
                if (!ok)
                    index = -1;
            }
            if (index < 0 || index >= 9 * 8)
                continue;

            result.insert(index, v);
        }
        return result;
    }
    case Variant::Type::Array: {
        auto children = value.toArray();
        QHash<int, bool> result;
        for (int idx = 0, max = std::min<int>(children.size(), 9 * 8); idx < max; ++idx) {
            bool ok = false;
            bool v = getValidBoolean(children[idx], ok);
            if (!ok)
                v = getValidBoolean(children[idx].hash("Value"), ok);
            if (!ok)
                continue;
            result.insert(idx, v);
        }
        return result;
    }
    case Variant::Type::String: {
        QStringList channels(value.toQString().split(QRegExp("[:;,]+")));
        QHash<int, bool> result;
        for (int idx = 0, max = std::min<int>(channels.size(), 9 * 8); idx < max; ++idx) {
            QString check(channels.at(idx).toLower());
            if (check.isEmpty())
                continue;
            bool v = false;
            bool ok = false;
            if (check == "on" || check == "true" || (check.toInt(&ok) && ok))
                v = true;
            result.insert(idx, v);
        }
        return result;
    }

    default:
        break;
    }

    return QHash<int, bool>();
}

void AcquireAzonixUMAC1050::genericOutputCommand(const Variant::Read &command)
{
    if (command.getType() != Variant::Type::Hash) {
        switch (command.getType()) {
        case Variant::Type::Real: {
            double v = command.toDouble();
            auto index = config.front().analogNames.find(SequenceName::Component());
            if (FP::defined(v) &&
                    index != config.front().analogNames.end() &&
                    index->second >= 0 &&
                    index->second <= 9) {
                analogUpdate.insert(index->second, v);
                analogTarget.insert(index->second, v);
            }
            break;
        }

        case Variant::Type::Integer: {
            qint64 bits = command.toInt64();
            if (INTEGER::defined(bits)) {
                for (int i = 0; i < 63; i++) {
                    qint64 c = (qint64) 1 << i;
                    bool value = (bits & c) != 0;
                    digitalUpdate.insert(i, value);
                    digitalTarget.insert(i, value);
                }
            }
            break;
        }

        default: {
            bool v = command.toBool();
            auto index = config.front().digitalNames.find(SequenceName::Component());
            if (index != config.front().analogNames.end() &&
                    index->second >= 0 &&
                    index->second < 9 * 8) {
                digitalUpdate.insert(index->second, v);
                digitalTarget.insert(index->second, v);
            }
            break;
        }
        }
        return;
    }
    int index = convertAnalogChannel(command.hash("Parameters").hash("Channel").hash("Value"));
    if (index == -1) {
        index = convertAnalogChannel(command.hash("Parameters").hash("Channel"));
    }
    if (index == -1) {
        index = convertAnalogChannel(command.hash("Channel"));
    }
    if (index == -1) {
        index = convertAnalogChannel(command.hash("Parameters").hash("Index").hash("Value"));
    }
    if (index == -1) {
        index = convertAnalogChannel(command.hash("Parameters").hash("Index"));
    }
    if (index == -1) {
        index = convertAnalogChannel(command.hash("Index"));
    }
    if (index != -1) {
        double value = FP::undefined();
        if (command.hash("Parameters").hash("Value").hash("Value").getType() ==
                Variant::Type::Real) {
            value = command.hash("Parameters").hash("Value").hash("Value").toDouble();
        }
        if (!FP::defined(value) &&
                command.hash("Parameters").hash("Value").getType() == Variant::Type::Real) {
            value = command.hash("Parameters").hash("Value").toDouble();
        }
        if (!FP::defined(value) && command.hash("Value").getType() == Variant::Type::Real) {
            value = command.hash("Value").toDouble();
        }
        if (FP::defined(value)) {
            analogUpdate.insert(index, value);
            analogTarget.insert(index, value);
            return;
        }
    }

    index = convertDigitalChannel(command.hash("Parameters").hash("Channel").hash("Value"));
    if (index == -1) {
        index = convertDigitalChannel(command.hash("Parameters").hash("Channel"));
    }
    if (index == -1) {
        index = convertDigitalChannel(command.hash("Channel"));
    }
    if (index == -1) {
        index = convertDigitalChannel(command.hash("Parameters").hash("Index").hash("Value"));
    }
    if (index == -1) {
        index = convertDigitalChannel(command.hash("Parameters").hash("Index"));
    }
    if (index == -1) {
        index = convertDigitalChannel(command.hash("Index"));
    }
    if (index != -1) {
        if (command.hash("Parameters").hash("Value").hash("Value").getType() ==
                Variant::Type::Boolean) {
            bool value = command.hash("Parameters").hash("Value").hash("Value").toBool();
            digitalUpdate.insert(index, value);
            digitalTarget.insert(index, value);
            return;
        }
        if (command.hash("Parameters").hash("Value").getType() == Variant::Type::Boolean) {
            bool value = command.hash("Parameters").hash("Value").toBool();
            digitalUpdate.insert(index, value);
            digitalTarget.insert(index, value);
            return;
        }
        if (command.hash("Value").getType() == Variant::Type::Boolean) {
            bool value = command.hash("Value").toBool();
            digitalUpdate.insert(index, value);
            digitalTarget.insert(index, value);
            return;
        }
    }

    auto name = command.hash("Parameters").hash("Name").hash("Value").toString();
    if (name.empty()) {
        name = command.hash("Parameters").hash("Name").toString();
    }
    if (name.empty()) {
        name = command.hash("Name").toString();
    }

    {
        double value = FP::undefined();
        if (command.hash("Parameters").hash("Value").hash("Value").getType() ==
                Variant::Type::Real) {
            value = command.hash("Parameters").hash("Value").hash("Value").toDouble();
        }
        if (!FP::defined(value) &&
                command.hash("Parameters").hash("Value").getType() == Variant::Type::Real) {
            value = command.hash("Parameters").hash("Value").toDouble();
        }
        if (!FP::defined(value) && command.hash("Value").getType() == Variant::Type::Real) {
            value = command.hash("Value").toDouble();
        }
        if (!FP::defined(value) &&
                command.hash("Parameters").hash("Output").hash("Value").getType() ==
                        Variant::Type::Real) {
            value = command.hash("Parameters").hash("Value").hash("Value").toDouble();
        }
        if (!FP::defined(value) &&
                command.hash("Parameters").hash("Output").getType() == Variant::Type::Real) {
            value = command.hash("Parameters").hash("Value").toDouble();
        }
        if (!FP::defined(value) && command.hash("Value").getType() == Variant::Type::Real) {
            value = command.hash("Output").toDouble();
        }
        if (FP::defined(value)) {
            auto i = config.front().analogNames.find(name);
            if (i != config.front().analogNames.end() && i->second >= 0 && i->second <= 9) {
                analogUpdate.insert(i->second, value);
                analogTarget.insert(i->second, value);
                return;
            }
        }
    }

    index = -1;
    auto lookup = config.front().digitalNames.find(name);
    if (lookup != config.front().digitalNames.end())
        index = lookup->second;
    if (index >= 0 && index < 9 * 8) {
        if (command.hash("Parameters").hash("Value").hash("Value").getType() ==
                Variant::Type::Boolean) {
            bool value = command.hash("Parameters").hash("Value").hash("Value").toBool();
            digitalUpdate.insert(index, value);
            digitalTarget.insert(index, value);
            return;
        }
        if (command.hash("Parameters").hash("Value").getType() == Variant::Type::Boolean) {
            bool value = command.hash("Parameters").hash("Value").toBool();
            digitalUpdate.insert(index, value);
            digitalTarget.insert(index, value);
            return;
        }
        if (command.hash("Value").getType() == Variant::Type::Boolean) {
            bool value = command.hash("Value").toBool();
            digitalUpdate.insert(index, value);
            digitalTarget.insert(index, value);
            return;
        }
        if (command.hash("Parameters").hash("Output").hash("Value").getType() ==
                Variant::Type::Boolean) {
            bool value = command.hash("Parameters").hash("Output").hash("Value").toBool();
            digitalUpdate.insert(index, value);
            digitalTarget.insert(index, value);
            return;
        }
        if (command.hash("Parameters").hash("Output").getType() == Variant::Type::Boolean) {
            bool value = command.hash("Parameters").hash("Output").toBool();
            digitalUpdate.insert(index, value);
            digitalTarget.insert(index, value);
            return;
        }
        if (command.hash("Output").getType() == Variant::Type::Boolean) {
            bool value = command.hash("Output").toBool();
            digitalUpdate.insert(index, value);
            digitalTarget.insert(index, value);
            return;
        }
    }

    for (auto check : command.toHash()) {
        index = -1;
        if ((lookup = config.front().analogNames.find(check.first)) !=
                config.front().analogNames.end()) {
            index = lookup->second;
        }
        if (index != -1) {
            double value = Variant::Composite::toNumber(check.second);
            if (!FP::defined(value))
                value = Variant::Composite::toNumber(check.second.hash("Value"));
            if (FP::defined(value)) {
                analogUpdate.insert(index, value);
                analogTarget.insert(index, value);
                continue;
            }
        }

        index = -1;
        if ((lookup = config.front().digitalNames.find(check.first)) !=
                config.front().digitalNames.end()) {
            index = lookup->second;
        }
        if (index != -1) {
            bool value = check.second.toBool();
            digitalUpdate.insert(index, value);
            digitalTarget.insert(index, value);
            continue;
        }

        bool ok = false;
        index = QString::fromStdString(check.first).toInt(&ok);
        if (!ok)
            continue;

        auto base = check.second;
        if (base.getType() == Variant::Type::Hash)
            base = base.hash("Value");

        switch (base.getType()) {
        case Variant::Type::Real: {
            if (index < 0 || index > 9)
                break;
            double value = base.toDouble();
            if (FP::defined(value)) {
                analogUpdate.insert(index, value);
                analogTarget.insert(index, value);
            }
            break;
        }
        case Variant::Type::Boolean: {
            if (index < 0 || index >= 9 * 8)
                break;
            bool value = base.toBool();
            digitalUpdate.insert(index, value);
            digitalTarget.insert(index, value);
            break;
        }
        default:
            break;
        }
    }
}

void AcquireAzonixUMAC1050::command(const Variant::Read &command)
{
    if (command.hash("SetAnalog").exists()) {
        int index = convertAnalogChannel(
                command.hash("SetAnalog").hash("Parameters").hash("Channel").hash("Value"));
        if (index == -1) {
            index = convertAnalogChannel(
                    command.hash("SetAnalog").hash("Parameters").hash("Channel"));
        }
        if (index == -1) {
            index = convertAnalogChannel(command.hash("SetAnalog").hash("Channel"));
        }
        if (index != -1) {
            double value = Variant::Composite::toNumber(
                    command.hash("SetAnalog").hash("Parameters").hash("Value").hash("Value"), true);
            if (!FP::defined(value)) {
                value = Variant::Composite::toNumber(
                        command.hash("SetAnalog").hash("Parameters").hash("Value"), true);
            }
            if (!FP::defined(value)) {
                value = Variant::Composite::toNumber(command.hash("SetAnalog").hash("Value"), true);
            }

            if (FP::defined(value)) {
                analogUpdate.insert(index, value);
                analogTarget.insert(index, value);
            }
        } else {
            QHash<int, double>
                    values(convertAnalogMultiple(command.hash("SetAnalog").hash("Parameters")));
            if (values.isEmpty()) {
                values = convertAnalogMultiple(command.hash("SetAnalog"));
            }
            if (!values.isEmpty()) {
                for (QHash<int, double>::const_iterator add = values.constBegin(),
                        endAdd = values.constEnd(); add != endAdd; ++add) {
                    analogUpdate.insert(add.key(), add.value());
                    analogTarget.insert(add.key(), add.value());
                }
            }
        }
    }

    if (command.hash("SetDigital").exists()) {
        int index = convertDigitalChannel(
                command.hash("SetDigital").hash("Parameters").hash("Channel").hash("Value"));
        if (index == -1) {
            index = convertDigitalChannel(
                    command.hash("SetDigital").hash("Parameters").hash("Channel"));
        }
        if (index == -1) {
            index = convertDigitalChannel(command.hash("SetDigital").hash("Channel"));
        }
        if (index != -1) {
            bool ok = false;
            bool value = getValidBoolean(
                    command.hash("SetDigital").hash("Parameters").hash("Value").hash("Value"), ok);
            if (!ok) {
                value = getValidBoolean(command.hash("SetDigital").hash("Parameters").hash("Value"),
                                        ok);
            }
            if (!ok) {
                value = getValidBoolean(command.hash("SetDigital").hash("Value"), ok);
            }

            if (ok) {
                digitalUpdate.insert(index, value);
                digitalTarget.insert(index, value);
            }
        } else {
            qint64 mask = command.hash("SetDigital")
                                 .hash("Parameters")
                                 .hash("Mask")
                                 .hash("Value")
                                 .toInt64();
            if (!INTEGER::defined(mask)) {
                mask = command.hash("SetDigital").hash("Parameters").hash("Mask").toInt64();
            }
            if (!INTEGER::defined(mask)) {
                mask = command.hash("SetDigital").hash("Mask").toInt64();
            }
            if (INTEGER::defined(mask)) {
                qint64 bits = getValidMask(
                        command.hash("SetDigital").hash("Parameters").hash("Value").hash("Value"));
                if (!INTEGER::defined(bits)) {
                    bool ok = false;
                    bool value = getValidBoolean(command.hash("SetDigital")
                                                        .hash("Parameters")
                                                        .hash("Value")
                                                        .hash("Value"), ok);
                    if (ok) {
                        if (value)
                            bits = mask;
                        else
                            bits = 0;
                    }
                }
                if (!INTEGER::defined(bits)) {
                    bits = getValidMask(
                            command.hash("SetDigital").hash("Parameters").hash("Value"));
                }
                if (!INTEGER::defined(bits)) {
                    bool ok = false;
                    bool value = getValidBoolean(
                            command.hash("SetDigital").hash("Parameters").hash("Value"), ok);
                    if (ok) {
                        if (value)
                            bits = mask;
                        else
                            bits = 0;
                    }
                }
                if (!INTEGER::defined(bits)) {
                    bits = getValidMask(command.hash("SetDigital").hash("Value"));
                }
                if (!INTEGER::defined(bits)) {
                    bool ok = false;
                    bool value = getValidBoolean(command.hash("SetDigital").hash("Value"), ok);
                    if (ok) {
                        if (value)
                            bits = mask;
                        else
                            bits = 0;
                    }
                }

                if (INTEGER::defined(bits)) {
                    for (int i = 0; i < 63; i++) {
                        qint64 c = (qint64) 1 << i;
                        if (!(mask & c))
                            continue;
                        bool value = (bits & c) != 0;
                        digitalUpdate.insert(i, value);
                        digitalTarget.insert(i, value);
                    }
                }
            } else {
                QHash<int, bool> values
                        (convertDigitalMultiple(command.hash("SetDigital").hash("Parameters")));
                if (values.isEmpty()) {
                    values = convertDigitalMultiple(command.hash("SetDigital"));
                }
                if (!values.isEmpty()) {
                    for (QHash<int, bool>::const_iterator add = values.constBegin(),
                            endAdd = values.constEnd(); add != endAdd; ++add) {
                        digitalUpdate.insert(add.key(), add.value());
                        digitalTarget.insert(add.key(), add.value());
                    }
                }
            }
        }
    }

    if (command.hash("Output").exists()) {
        genericOutputCommand(command.hash("Output"));
    }

    if (command.hash("Bypass").toBool()) {
        Q_ASSERT(!config.isEmpty());
        for (QMap<int, double>::const_iterator add = config.first().bypassAnalog.constBegin(),
                endAdd = config.first().bypassAnalog.constEnd(); add != endAdd; ++add) {
            analogUpdate.insert(add.key(), add.value());
            analogTarget.insert(add.key(), add.value());
        }
        for (QMap<int, bool>::const_iterator add = config.first().bypassDigital.constBegin(),
                endAdd = config.first().bypassDigital.constEnd(); add != endAdd; ++add) {
            digitalUpdate.insert(add.key(), add.value());
            digitalTarget.insert(add.key(), add.value());
        }
    }
    if (command.hash("UnBypass").toBool()) {
        Q_ASSERT(!config.isEmpty());
        for (QMap<int, double>::const_iterator add = config.first().unbypassAnalog.constBegin(),
                endAdd = config.first().unbypassAnalog.constEnd(); add != endAdd; ++add) {
            analogUpdate.insert(add.key(), add.value());
            analogTarget.insert(add.key(), add.value());
        }
        for (QMap<int, bool>::const_iterator add = config.first().unbypassDigital.constBegin(),
                endAdd = config.first().unbypassDigital.constEnd(); add != endAdd; ++add) {
            digitalUpdate.insert(add.key(), add.value());
            digitalTarget.insert(add.key(), add.value());
        }
    }
}

Variant::Root AcquireAzonixUMAC1050::getCommands()
{
    Variant::Root result;

    result["SetAnalog"].hash("DisplayName").setString("Set an &Analog Output");
    result["SetAnalog"].hash("ToolTip").setString("Change the current output of an analog output.");
    result["SetAnalog"].hash("Parameters").hash("Channel").hash("Name").setString("Channel");
    result["SetAnalog"].hash("Parameters").hash("Channel").hash("Format").setString("0");
    result["SetAnalog"].hash("Parameters").hash("Channel").hash("Minimum").setInt64(0);
    result["SetAnalog"].hash("Parameters").hash("Channel").hash("Maximum").setInt64(9);
    result["SetAnalog"].hash("Parameters").hash("Channel").hash("Default").setInt64(0);
    result["SetAnalog"].hash("Parameters").hash("Value").hash("Name").setString("Output");
    result["SetAnalog"].hash("Parameters").hash("Value").hash("Format").setString("0.000");
    result["SetAnalog"].hash("Parameters").hash("Value").hash("Default").setReal(0.0);

    result["SetDigital"].hash("DisplayName").setString("Set a &Digital Output");
    result["SetDigital"].hash("ToolTip").setString("Change the current state of a digital output.");
    result["SetDigital"].hash("Parameters").hash("Channel").hash("Name").setString("Channel");
    result["SetDigital"].hash("Parameters").hash("Channel").hash("Format").setString("0");
    result["SetDigital"].hash("Parameters").hash("Channel").hash("Minimum").setInt64(0);
    result["SetDigital"].hash("Parameters").hash("Channel").hash("Maximum").setInt64(9 * 8 - 1);
    result["SetDigital"].hash("Parameters").hash("Channel").hash("Default").setInt64(0);
    result["SetDigital"].hash("Parameters").hash("Value").hash("Name").setString("Output high");
    result["SetDigital"].hash("Parameters").hash("Value").hash("Type").setString("Boolean");
    result["SetDigital"].hash("Parameters").hash("Value").hash("Default").setBoolean(false);

    return result;
}

AcquisitionInterface::AutoprobeStatus AcquireAzonixUMAC1050::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

SequenceValue::Transfer AcquireAzonixUMAC1050::getPersistentValues()
{
    PauseLock paused(*this);
    SequenceValue::Transfer result;

    Q_ASSERT(!config.isEmpty());

    if (config.first().logOutputs) {
        if (FP::defined(analogOutputValues.getStart())) {
            result.emplace_back(analogOutputValues);
        }
        if (FP::defined(digitalOutputValues.getStart())) {
            result.emplace_back(digitalOutputValues);
        }
    }

    return result;
}

void AcquireAzonixUMAC1050::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_WAIT:
    case RESP_INTERACTIVE_RUN_READ_ANALOG:
    case RESP_INTERACTIVE_RUN_READ_SENSORS:
    case RESP_INTERACTIVE_RUN_WRITE_ANALOG:
    case RESP_INTERACTIVE_RUN_WRITE_DIGITAL:
        /* Already running/started, do nothing */
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << responseState << "at" << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case RESP_INTERACTIVE_START_FLUSH:
    case RESP_INTERACTIVE_START_RESET:
    case RESP_INTERACTIVE_START_CONFIG_LOAD:
    case RESP_INTERACTIVE_START_READ_REVISION:
    case RESP_INTERACTIVE_START_READ_ANALOG:
    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        commandQueue.clear();
        responseState = RESP_INTERACTIVE_START_FLUSH;
        timeoutAt(time + 10.0);
        discardData(time + 1.0);
        if (controlStream != NULL) {
            controlStream->writeControl(Util::ByteArray::filled(0, 64));
        }
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireAzonixUMAC1050::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
    commandQueue.clear();
    discardData(time + 0.5, 1);
    timeoutAt(time + 10.0);
    generalStatusUpdated();
}

void AcquireAzonixUMAC1050::autoprobePromote(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_WAIT:
        qCDebug(log) << "Promoted from interactive wait to interactive acquisition at"
                     << Logging::time(time);
        timeoutAt(time + 2.0);
        break;

    case RESP_INTERACTIVE_RUN_READ_ANALOG:
    case RESP_INTERACTIVE_RUN_READ_SENSORS:
    case RESP_INTERACTIVE_RUN_WRITE_ANALOG:
    case RESP_INTERACTIVE_RUN_WRITE_DIGITAL:
    case RESP_INTERACTIVE_START_RESET:
    case RESP_INTERACTIVE_START_CONFIG_LOAD:
    case RESP_INTERACTIVE_START_READ_REVISION:
    case RESP_INTERACTIVE_START_READ_ANALOG:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + 5.0);
        break;

    case RESP_INTERACTIVE_START_FLUSH:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + 10.0);
        break;

    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        commandQueue.clear();
        responseState = RESP_INTERACTIVE_START_FLUSH;
        timeoutAt(time + 10.0);
        discardData(time + 1.0);
        if (controlStream != NULL) {
            controlStream->writeControl(Util::ByteArray::filled(0, 64));
        }
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireAzonixUMAC1050::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + 2.0);

    switch (responseState) {
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case RESP_INTERACTIVE_RUN_WAIT:
    case RESP_INTERACTIVE_RUN_READ_ANALOG:
    case RESP_INTERACTIVE_RUN_READ_SENSORS:
    case RESP_INTERACTIVE_RUN_WRITE_ANALOG:
    case RESP_INTERACTIVE_RUN_WRITE_DIGITAL:
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);

        responseState = RESP_PASSIVE_RUN;
        break;

    default:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_PASSIVE_WAIT;
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireAzonixUMAC1050::getDefaults()
{
    AutomaticDefaults result;
    result.name = "X$2";
    result.setSerialN81(9600);
    return result;
}


ComponentOptions AcquireAzonixUMAC1050Component::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireAzonixUMAC1050Component::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data using all defaults.")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireAzonixUMAC1050Component::createAcquisitionPassive(const ComponentOptions &options,
                                                                                       const std::string &loggingContext)
{
    return std::unique_ptr<AcquisitionInterface>(
            new AcquireAzonixUMAC1050(options, loggingContext));
}

std::unique_ptr<
        AcquisitionInterface> AcquireAzonixUMAC1050Component::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                       const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireAzonixUMAC1050(config, loggingContext)); }

std::unique_ptr<AcquisitionInterface> AcquireAzonixUMAC1050Component::createAcquisitionAutoprobe(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireAzonixUMAC1050> i(new AcquireAzonixUMAC1050(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<AcquisitionInterface> AcquireAzonixUMAC1050Component::createAcquisitionInteractive(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireAzonixUMAC1050> i(new AcquireAzonixUMAC1050(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
