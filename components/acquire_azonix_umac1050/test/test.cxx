/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <map>
#include <cstdint>
#include <cstring>
#include <QTest>
#include <QtGlobal>
#include <QtEndian>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "algorithms/crc.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;
using namespace CPD3::Algorithms;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;

    double ain[62];
    std::uint8_t dot[10];
    double aot[10];

    static const quint8 address = 0x00;

    static QByteArray createPacket(const QByteArray &payload, std::uint8_t sequence = 0)
    {
        QByteArray result;
        result.resize(4);
        uchar *data = reinterpret_cast<uchar *>(result.data());
        data[0] = address;
        data[1] = 0x01;
        quint16 bcs = static_cast<quint16>(payload.size() + 2);
        bcs |= static_cast<quint16>(sequence & 0x3F) << 10;
        qToLittleEndian<quint16>(bcs, &data[2]);
        result.append(payload);
        int offset = result.size();
        result.resize(offset + 2);
        qToLittleEndian<quint16>(CRC16().calculate(result.constData(), offset),
                                 reinterpret_cast<uchar *>(result.data()) + offset);
        return result;
    }

    static QByteArray createCommand(std::uint8_t command,
                                    const QByteArray &payload = QByteArray(),
                                    std::uint8_t sequence = 0)
    {
        QByteArray data;
        data.append(static_cast<char>(address));
        data.append(static_cast<char>(command));
        data.append(payload);
        return createPacket(data, sequence);
    }

    static QByteArray createResponse(const QByteArray &payload, std::uint8_t sequence)
    {
        QByteArray data;
        data.append(static_cast<char>(0x01));
        data.append(static_cast<char>(0x00));
        data.append(payload);
        return createPacket(data, sequence);
    }

    static QByteArray createError(quint16 error, quint8 sequence)
    {
        QByteArray data;
        data.resize(4);
        uchar *p = reinterpret_cast<uchar *>(data.data());
        p[0] = 0x03;
        p[1] = 0x80;
        qToLittleEndian<quint16>(error, &p[2]);
        return createPacket(data, sequence);
    }

    ModelInstrument() : incoming(), outgoing()
    {
        for (int i = 0; i < 60; i++) {
            ain[i] = 0.5 + 0.125 * i;
        }
        ain[60] = 23.0;
        ain[61] = 5.0;

        std::memset(dot, 0, sizeof(dot));
        for (int i = 0; i < 10; i++) {
            aot[i] = FP::undefined();
        }
    }

    void advance(double seconds)
    {
        Q_UNUSED(seconds);

        for (;;) {
            if (incoming.size() < 8)
                break;
            {
                int startIdx = incoming.indexOf((char) address);
                if (startIdx == -1)
                    break;
                if (startIdx > 0) {
                    incoming.remove(0, startIdx);
                    continue;
                }
            }
            if (incoming.at(1) != (char) 0x01) {
                incoming.remove(0, 1);
                continue;
            }

            const uchar *data = reinterpret_cast<const uchar *>(incoming.constData());
            const uchar *crcBegin = data;
            int length = incoming.length();

            data += 2;
            length -= 2;

            std::uint16_t bcs = qFromLittleEndian<quint16>(data);
            int packetLength = bcs & 0x3FF;
            int crcLength = packetLength + 4;
            std::uint8_t sequence = static_cast<std::uint8_t>(bcs >> 10);
            if (packetLength < 4 || data[2] != 0x00) {
                incoming.remove(0, 1);
                continue;
            }

            data += 2;
            length -= 2;
            if (length < packetLength)
                break;

            if (CRC16().calculate(crcBegin, crcLength) != 0) {
                incoming.remove(0, 1);
                continue;
            }
            std::uint8_t command = data[1];
            data += 2;
            length -= 4;
            packetLength -= 4;
            Q_ASSERT(length >= 0);
            Q_ASSERT(packetLength >= 0);

            switch (command) {
            case 9:     /* AIN */
                if (packetLength != 2) {
                    outgoing.append(createError(83, sequence));
                    break;
                } else {
                    int begin = data[0];
                    if (begin < 0 || begin > 61) {
                        outgoing.append(createError(81, sequence));
                        break;
                    }
                    int end = data[1];
                    if (end < 0 || end > 61 || end < begin) {
                        outgoing.append(createError(82, sequence));
                        break;
                    }
                    QByteArray payload;
                    payload.resize(((end - begin) + 1) * 4);
                    uchar *target = reinterpret_cast<uchar *>(payload.data());
                    for (; begin <= end; ++begin, target += 4) {
                        std::uint32_t i;
                        float f = static_cast<float>(ain[begin]);
                        std::memcpy(&i, &f, 4);
                        qToLittleEndian<quint32>(i, target);
                    }
                    outgoing.append(createResponse(payload, sequence));
                }
                break;

            case 34:    /* AOT */
                if (packetLength != 6) {
                    outgoing.append(createError(276, sequence));
                    break;
                } else {
                    int begin = data[0];
                    if (begin < 0 || begin > 9) {
                        outgoing.append(createError(273, sequence));
                        break;
                    }
                    int end = data[1];
                    if (end < 0 || end > 9 || end < begin) {
                        outgoing.append(createError(274, sequence));
                        break;
                    }
                    std::uint32_t i = qFromLittleEndian<quint32>(&data[2]);
                    float f;
                    std::memcpy(&f, &i, 4);
                    for (; begin <= end; ++begin) {
                        aot[begin] = static_cast<double>(f);
                    }
                    outgoing.append(createResponse(QByteArray(), sequence));
                }
                break;

            case 50:    /* DOT */
                if (length != 4) {
                    outgoing.append(createError(403, sequence));
                    break;
                } else {
                    int begin = data[0];
                    if (begin < 0 || begin > 9) {
                        outgoing.append(createError(401, sequence));
                        break;
                    }
                    int end = data[1];
                    if (end < 0 || end > 9 || end < begin) {
                        outgoing.append(createError(402, sequence));
                        break;
                    }
                    std::uint8_t mask = data[2];
                    std::uint8_t value = data[3];
                    for (; begin <= end; ++begin) {
                        dot[begin] &= ~mask;
                        dot[begin] |= (value & mask);
                    }
                    outgoing.append(createResponse(QByteArray(), sequence));
                }
                break;

            case 161:   /* CNFGLD */
                if (packetLength != 1) {
                    outgoing.append(createError(1289, sequence));
                    break;
                } else {
                    outgoing.append(createResponse(QByteArray(), sequence));
                }
                break;

            case 177:   /* RESET */
                if (packetLength != 0) {
                    outgoing.append(createError(1, sequence));
                    break;
                } else {
                    outgoing.append(createResponse(QByteArray(), sequence));
                }
                break;

            case 180:   /* REV */
                if (packetLength != 0) {
                    outgoing.append(createError(1, sequence));
                    break;
                } else {
                    outgoing.append(
                            createResponse(QByteArray("uMAC-1050 Firmware Rev. 1.00 (c)1989"),
                                           sequence));
                }
                break;

            default:
                outgoing.append(createError(1, sequence));
                break;
            }

            incoming.remove(0, packetLength + 6);
        }
    }


};

class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;

    bool checkMeta(StreamCapture &stream, const ModelInstrument &model,
                   double time = FP::undefined())
    {
        Q_UNUSED(model);
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZINPUTS", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("V", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZOUTPUTS", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("F2", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkVariableMeta(StreamCapture &stream,
                           const SequenceName::Component &variable,
                           int channel,
                           double time = FP::undefined())
    {
        if (!stream.hasMeta(variable, Variant::Root(channel), "^PhysicalChannel", time))
            return false;
        return true;
    }

    bool checkVariableMeta(StreamCapture &stream, const std::map<SequenceName::Component, int> &variables,
                           double time = FP::undefined())
    {
        for (const auto &check : variables) {
            if (!checkVariableMeta(stream, check.first, check.second, time))
                return false;
        }
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, const ModelInstrument &model,
                           double time = FP::undefined())
    {
        Q_UNUSED(model);
        if (!stream.hasMeta("ZINDEX", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZDIGITAL", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZSTATE", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkContiguous(StreamCapture &stream)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("ZINPUTS"))
            return false;
        if (!stream.checkContiguous("T"))
            return false;
        if (!stream.checkContiguous("V"))
            return false;
        return true;
    }

    bool checkVariableContiguous(StreamCapture &stream, const SequenceName::Component &variable)
    {
        if (!stream.checkContiguous(variable))
            return false;
        return true;
    }

    bool checkVariableContiguous(StreamCapture &stream, const std::map<SequenceName::Component, int> &variables)
    {
        for (const auto &check : variables) {
            if (!checkVariableContiguous(stream, check.first))
                return false;
        }
        return true;
    }

    bool checkValues(StreamCapture &stream, const ModelInstrument &model,
                     int channelsBegin = 0,
                     int channelCount = 24,
                     double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("T", Variant::Root(model.ain[60]), time))
            return false;
        if (!stream.hasAnyMatchingValue("V", Variant::Root(model.ain[61]), time))
            return false;
        {
            Variant::Root inputs;
            for (int end = channelsBegin + channelCount; channelsBegin < end; ++channelsBegin) {
                inputs.write().toArray().after_back().setDouble(model.ain[channelsBegin]);
            }
            if (!stream.hasAnyMatchingValue("ZINPUTS", inputs, time))
                return false;
        }
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream, const ModelInstrument &model,
                             int aotChannels = 4,
                             int dotChannels = 16,
                             int ainChannels = 24,
                             int ainOffset = 0,
                             double time = FP::undefined())
    {
        Q_UNUSED(ainOffset);

        if (dotChannels > 0) {
            qint64 digital = 0;
            for (int i = 0; i < qMax(63, dotChannels); i++) {
                digital |= (static_cast<qint64>(model.dot[i / 8]) & (Q_INT64_C(1) << (i % 8)))
                        << ((i / 8) * 8);
            }
            if (!stream.hasAnyMatchingValue("ZDIGITAL", Variant::Root(digital), time))
                return false;
        }

        Variant::Write indices = Variant::Write::empty();
        for (int i = 0; i < qMax(aotChannels, qMax(dotChannels, ainChannels)); i++) {
            indices.array(i).setInt64(i);
        }
        if (!stream.hasAnyMatchingValue("ZINDEX", indices, time))
            return false;
        return true;
    }

    bool checkOutputValues(StreamCapture &stream, const ModelInstrument &model,
                           int aotChannels = 4,
                           int dotChannels = 16,
                           double time = FP::undefined())
    {
        if (dotChannels > 0) {
            Variant::Write F2 = Variant::Write::empty();
            for (int i = 0; i < dotChannels; i++) {
                F2.array(i).setBool((model.dot[i / 8] & (1 << (i % 8))) != 0);
            }
            if (!stream.hasAnyMatchingValue("F2", F2, time))
                return false;
        }

        if (aotChannels > 0) {
            Variant::Write outputs = Variant::Write::empty();
            for (int i = 0; i < aotChannels; i++) {
                outputs.array(i).setDouble(model.aot[i]);
            }
            if (!stream.hasAnyMatchingValue("ZOUTPUTS", outputs, time))
                return false;
        }
        return true;
    }

    bool checkVariableValues(StreamCapture &stream,
                             const ModelInstrument &model,
                             const std::map<SequenceName::Component, int> &variables,
                             double time = FP::undefined())
    {
        for (const auto &check : variables) {
            if (!stream.hasAnyMatchingValue(check.first, Variant::Root(model.ain[check.second]),
                                            time))
                return false;
        }
        return true;
    }

    void configureVariables(Variant::Root &cv,
                            const std::map<SequenceName::Component, int> &variables,
                            int offset = 0)
    {
        for (const auto &check : variables) {
            cv["Variables"].hash(check.first).hash("Channel").setInt64(check.second - offset);
        }
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_azonix_umac1050"));
        QVERIFY(component);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        std::map<SequenceName::Component, int> variables{{"T_V11", 0},
                                         {"U_V11", 1}};
        configureVariables(cv, variables);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        QByteArray queryAIN(2, 0);
        queryAIN[0] = 0;
        queryAIN[1] = 23;
        QByteArray querySensors(2, 0);
        querySensors[0] = 60;
        querySensors[1] = 61;

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        StreamCapture realtime;
        interface->setRealtimeEgress(&realtime);

        quint8 s = 1;
        control.externalControl(ModelInstrument::createCommand(9, queryAIN, s++));
        control.externalControl(ModelInstrument::createCommand(9, querySensors, s++));
        control.externalControl(ModelInstrument::createCommand(9, queryAIN, s++));
        control.externalControl(ModelInstrument::createCommand(9, querySensors, s++));
        control.externalControl(ModelInstrument::createCommand(9, queryAIN, s++));
        control.externalControl(ModelInstrument::createCommand(9, querySensors, s++));
        control.externalControl(ModelInstrument::createCommand(9, queryAIN, s++));
        control.externalControl(ModelInstrument::createCommand(9, querySensors, s++));
        control.externalControl(ModelInstrument::createCommand(9, queryAIN, s++));
        control.externalControl(ModelInstrument::createCommand(9, querySensors, s++));
        control.externalControl(ModelInstrument::createCommand(9, queryAIN, s++));
        control.externalControl(ModelInstrument::createCommand(9, querySensors, s++));
        control.externalControl(ModelInstrument::createCommand(9, queryAIN, s++));
        control.externalControl(ModelInstrument::createCommand(9, querySensors, s++));
        while (control.time() < 10.0) {
            control.advance(0.125);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 10.0);

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        control.externalControl(ModelInstrument::createCommand(9, queryAIN, s++));
        control.advance(0.125);
        control.externalControl(ModelInstrument::createCommand(9, querySensors, s++));
        control.advance(0.125);
        control.externalControl(ModelInstrument::createCommand(9, queryAIN, s++));
        control.advance(0.125);
        control.externalControl(ModelInstrument::createCommand(9, querySensors, s++));
        control.advance(0.125);
        control.externalControl(ModelInstrument::createCommand(9, queryAIN, s++));
        control.advance(0.125);
        control.externalControl(ModelInstrument::createCommand(9, querySensors, s++));
        control.advance(0.125);
        control.externalControl(ModelInstrument::createCommand(9, queryAIN, s++));
        control.advance(0.125);
        control.externalControl(ModelInstrument::createCommand(9, querySensors, s++));
        control.advance(0.125);
        control.externalControl(ModelInstrument::createCommand(9, queryAIN, s++));
        control.advance(0.125);
        control.externalControl(ModelInstrument::createCommand(9, querySensors, s++));
        control.advance(0.125);
        while (control.time() < 60.0) {
            control.advance(0.125);
            if (logging.hasAnyMatchingValue("F1", Variant::Root(), FP::undefined()))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);

        for (int i = 0; i < 30; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkVariableMeta(logging, variables));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));
        QVERIFY(checkVariableMeta(realtime, variables));

        QVERIFY(checkContiguous(logging));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));

        QVERIFY(checkVariableValues(logging, instrument, variables));
        QVERIFY(checkVariableValues(realtime, instrument, variables));
    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        std::map<SequenceName::Component, int> variables{{"T_V11", 1},
                                         {"U_V11", 2}};
        configureVariables(cv, variables, 1);
        cv["AnalogChannelStart"].setInt64(1);
        cv["AnalogChannelCount"].setInt64(10);
        cv["PollInterval"].setDouble(0.0);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            if (logging.hasAnyMatchingValue("F1", Variant::Root(), FP::undefined()))
                break;
            control.advance(0.5);
            QTest::qSleep(50);
        }
        QVERIFY(logging.hasAnyMatchingValue("F1", Variant::Root(), FP::undefined()));

        for (int i = 0; i < 30; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkVariableMeta(logging, variables));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));
        QVERIFY(checkVariableMeta(realtime, variables));

        QVERIFY(checkContiguous(logging));

        QVERIFY(checkValues(logging, instrument, 1, 10));
        QVERIFY(checkValues(realtime, instrument, 1, 10));
        QVERIFY(checkRealtimeValues(realtime, instrument, 0, 0, 10, 1));

        QVERIFY(checkVariableValues(logging, instrument, variables));
        QVERIFY(checkVariableValues(realtime, instrument, variables));
    }

    void interactiveRun()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        std::map<SequenceName::Component, int> variables{{"T_V11", 1},
                                         {"U_V11", 2}};
        configureVariables(cv, variables);
        cv["AnalogOutputs/AOut1"].setInt64(0);
        cv["DigitalOutputs/DOut1"].setInt64(1);
        cv["DigitalOutputs/DOut2"].setInt64(2);
        cv["Initialize/Analog"].setString("0,1,2,3");
        cv["Initialize/Digital"].setInt64(0x0123);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")));

        instrument.ain[1] = 2.25;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("T_V11", Variant::Root(2.25)))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("T_V11", Variant::Root(2.25)));

        Variant::Write aout = Variant::Write::empty();
        aout.array(0).setDouble(0);
        aout.array(1).setDouble(1);
        aout.array(2).setDouble(2);
        aout.array(3).setDouble(3);
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZOUTPUTS", aout) &&
                    realtime.hasAnyMatchingValue("ZDIGITAL", Variant::Root(0x0123)))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZOUTPUTS", aout));
        QVERIFY(realtime.hasAnyMatchingValue("ZDIGITAL", Variant::Root(0x0123)));

        Variant::Write cmd = Variant::Write::empty();
        cmd["SetAnalog/1"].setDouble(4.0);
        cmd["SetAnalog/AOut1"].setDouble(4.5);
        aout.array(0).setDouble(4.5);
        aout.array(1).setDouble(4.0);
        interface->incomingCommand(cmd);
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZOUTPUTS", aout))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZOUTPUTS", aout));
        QCOMPARE(instrument.aot[0], 4.5);
        QCOMPARE(instrument.aot[1], 4.0);

        cmd.setEmpty();
        cmd["SetAnalog/Parameters/Channel/Value"].setInt64(2);
        cmd["SetAnalog/Parameters/Value/Value"].setDouble(0.5);
        aout.array(2).setDouble(0.5);
        interface->incomingCommand(cmd);
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZOUTPUTS", aout))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZOUTPUTS", aout));
        QCOMPARE(instrument.aot[2], 0.5);

        cmd.setEmpty();
        cmd["Output/AOut1"].setDouble(1.5);
        aout.array(0).setDouble(1.5);
        interface->incomingCommand(cmd);
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZOUTPUTS", aout))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZOUTPUTS", aout));
        QCOMPARE(instrument.aot[0], 1.5);

        cmd.setEmpty();
        cmd["SetDigital/Parameters/Channel/Value"].setInt64(15);
        cmd["SetDigital/Parameters/Value/Value"].setBool(true);
        interface->incomingCommand(cmd);
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZDIGITAL", Variant::Root(0x8123)))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZDIGITAL", Variant::Root(0x8123)));
        QCOMPARE(instrument.dot[1], (quint8) 0x81);

        cmd.setEmpty();
        cmd["Output/Parameters/Name"].setString("DOut1");
        cmd["Output/Parameters/Value"].setBool(false);
        interface->incomingCommand(cmd);
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZDIGITAL", Variant::Root(0x8121)))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZDIGITAL", Variant::Root(0x8121)));
        QCOMPARE(instrument.dot[0], (quint8) 0x21);

        for (int i = 0; i < 30; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        cmd.setEmpty();
        cmd["SetDigital/DOut2"].setBool(true);
        interface->incomingCommand(cmd);
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZDIGITAL", Variant::Root(0x8125)))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZDIGITAL", Variant::Root(0x8125)));
        QCOMPARE(instrument.dot[0], (quint8) 0x25);

        for (int i = 0; i < 30; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkVariableMeta(logging, variables));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));
        QVERIFY(checkVariableMeta(realtime, variables));

        QVERIFY(checkContiguous(logging));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
        QVERIFY(checkOutputValues(realtime, instrument, 4, 63));

        QVERIFY(checkVariableValues(logging, instrument, variables));
        QVERIFY(checkVariableValues(realtime, instrument, variables));
    }


};

QTEST_MAIN(TestComponent)

#include "test.moc"
