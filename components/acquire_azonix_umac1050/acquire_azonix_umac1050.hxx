/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIREAZONIXUMAC1050_H
#define ACQUIREAZONIXUMAC1050_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QObject>
#include <QHash>
#include <QSet>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "algorithms/crc.hxx"
#include "core/number.hxx"

class AcquireAzonixUMAC1050 : public CPD3::Acquisition::FramedInstrument {
    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum {
        /* Passive autoprobe initialization, ignoring the first timeout */
        RESP_AUTOPROBE_PASSIVE_INITIALIZE,
        /* Waiting for a valid record to confirm passive mode autoprobe */
        RESP_AUTOPROBE_PASSIVE_WAIT,

        /* Waiting for a valid record to start passive acquisition */
        RESP_PASSIVE_WAIT,

        /* Acquiring data in passive mode */
        RESP_PASSIVE_RUN,
        /* Acquiring data in interactive mode, waiting for the analog read
         * response */
        RESP_INTERACTIVE_RUN_READ_ANALOG,
        /* Acquiring data in interactive mode, waiting for the sensor
         * read response */
        RESP_INTERACTIVE_RUN_READ_SENSORS,
        /* Acquiring data in interactive mode, waiting for the analog output
         * write response */
        RESP_INTERACTIVE_RUN_WRITE_ANALOG,
        /* Acquiring data in interactive mode, waiting for the digital output
         * write response */
        RESP_INTERACTIVE_RUN_WRITE_DIGITAL,
        /* Acquiring data in interactive mode, sleeping until the next
         * time to query. */
        RESP_INTERACTIVE_RUN_WAIT,

        /* Starting communications, performing an initial flush */
        RESP_INTERACTIVE_START_FLUSH,
        /* Starting communications, waiting for the reset to complete */
        RESP_INTERACTIVE_START_RESET,
        /* Starting communications, loading the configuration */
        RESP_INTERACTIVE_START_CONFIG_LOAD,
        /* Starting communications, reading the firmware revision string */
        RESP_INTERACTIVE_START_READ_REVISION,
        /* Starting communications, reading the analog values for the first
         * time */
        RESP_INTERACTIVE_START_READ_ANALOG,

        /* Waiting before attempting a communications restart */
        RESP_INTERACTIVE_RESTART_WAIT,
        /* Initial state for interactive start */
        RESP_INTERACTIVE_INITIALIZE,
    } responseState;

    enum CommandType {
        /* Unknown command, so all responses until another command is
         * received are discarded. */
        COMMAND_IGNORED = 0x200,

        /* Read analog inputs */
        COMMAND_AIN = 0x09, /* Set analog outputs */
        COMMAND_AOT = 0x22,

        /* Set digital outputs */
        COMMAND_DOT = 0x32,

        /* Load configuration */
        COMMAND_CNFGLD = 0xA1, /* Reset */
        COMMAND_RESET = 0xB1, /* Firmware revision string */
        COMMAND_REV = 0xB4,

        /* A specifically invalid command */
        COMMAND_INVALID = 0x100,
    };

    class Command {
        CommandType type;
        CPD3::Util::ByteArray packet;
    public:
        Command();

        Command(CommandType t, CPD3::Util::ByteArray p = {});

        Command(const Command &);

        Command &operator=(const Command &);

        Command(Command &&);

        Command &operator=(Command &&);

        inline CommandType getType() const
        { return type; }

        inline const CPD3::Util::ByteArray &getPacket() const
        { return packet; }

        CPD3::Data::Variant::Root stateDescription() const;

        int sequenceNumber() const;
    };

    QList<Command> commandQueue;
    quint8 issueSequenceNumber;

    enum LogStreamID {
        LogStream_Metadata = 0, LogStream_State,

        LogStream_T, LogStream_V, LogStream_AIN_Begin, LogStream_AIN_End = LogStream_AIN_Begin + 60,

        LogStream_TOTAL, LogStream_RecordBaseStart = LogStream_T,
    };
    CPD3::Data::StaticMultiplexer loggingMux;
    quint32 streamAge[LogStream_TOTAL];
    double streamTime[LogStream_TOTAL];
    CPD3::Data::Variant::Root lastInputs;

    struct OutputVariable {
        CPD3::Data::SequenceName::Component name;
        CPD3::Data::Variant::Root metadata;

        int channel;
        CPD3::Calibration calibration;
    };

    class Configuration {
        double start;
        double end;

        void addVariable(const CPD3::Data::Variant::Read &config,
                         qint64 defaultChannel = CPD3::INTEGER::undefined(),
                         const CPD3::Data::SequenceName::Component &defaultName = {});

        void setAnalogValues(const CPD3::Data::Variant::Read &config, QMap<int, double> &target);

        void setDigitalValues(const CPD3::Data::Variant::Read &config, QMap<int, bool> &target);

    public:
        quint8 address;
        double pollInterval;

        bool logOutputs;

        int analogChannelStart;
        int analogChannelCount;

        std::unordered_map<CPD3::Data::SequenceName::Component, int> analogNames;
        std::unordered_map<CPD3::Data::SequenceName::Component, int> digitalNames;

        QMap<int, double> initializeAnalog;
        QMap<int, double> exitAnalog;

        QMap<int, bool> initializeDigital;
        QMap<int, bool> exitDigital;

        QMap<int, double> bypassAnalog;
        QMap<int, double> unbypassAnalog;

        QMap<int, bool> bypassDigital;
        QMap<int, bool> unbypassDigital;

        QHash<int, QList<OutputVariable> > variables;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under,
                      const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    CPD3::Algorithms::CRC16 crc;

    QMap<int, double> analogUpdate;
    QMap<int, bool> digitalUpdate;
    int updateChannel;

    QMap<int, double> analogTarget;
    QMap<int, bool> digitalTarget;

    CPD3::Data::SequenceValue analogOutputValues;
    CPD3::Data::SequenceValue digitalOutputValues;
    CPD3::Data::SequenceValue digitalOutputBits;

    CPD3::Data::Variant::Root instrumentMeta;
    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;

    int maximumDisplayChannel;

    void setDefaultInvalid();

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    void invalidateLogValues(double frameTime);

    void logValues(double frameTime, int streamID, CPD3::Data::SequenceValue::Transfer &&values);

    int verifyPacket(const uchar *data, int length, int sequenceNumber = -1) const;

    int verifyDataFrame(const uchar *data, int length) const;

    void describeState(CPD3::Data::Variant::Write &info) const;

    void updateLogMeta(double frameTime);

    void queueCommand(CommandType command, const CPD3::Util::ByteArray &payload = {});

    void malformedCommand(const CPD3::Util::ByteArray &frame, double frameTime, int code);

    void invalidResponse(const CPD3::Util::ByteArray &frame, double frameTime, int code);

    void commandSequencingError(const CPD3::Util::ByteArray &frame, double frameTime, int code);

    bool issueNextAnalogUpdate();

    bool issueNextDigitalUpdate();

    void configurationChanged();

    void configurationAdvance(double frameTime);

    void readAllAnalogInputs(double frameTime);

    void readAnalogSensors(double frameTime);

    void updateMaximumDisplay(double frameTime, int max);

    int convertAnalogChannel(const CPD3::Data::Variant::Read &value);

    int convertDigitalChannel(const CPD3::Data::Variant::Read &value);

    QHash<int, double> convertAnalogMultiple(const CPD3::Data::Variant::Read &value);

    QHash<int, bool> convertDigitalMultiple(const CPD3::Data::Variant::Read &value);

    void genericOutputCommand(const CPD3::Data::Variant::Read &command);

public:
    AcquireAzonixUMAC1050(const CPD3::Data::ValueSegment::Transfer &config,
                          const std::string &loggingContext);

    AcquireAzonixUMAC1050(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireAzonixUMAC1050();

    virtual CPD3::Data::SequenceValue::Transfer getPersistentValues();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    virtual CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus();

    virtual CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus();

    CPD3::Data::Variant::Root getCommands() override;

    virtual CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables();

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    virtual void command(const CPD3::Data::Variant::Read &value);

    std::size_t dataFrameStart(std::size_t offset,
                               const CPD3::Util::ByteArray &input) const override;

    std::size_t dataFrameEnd(std::size_t start, const CPD3::Util::ByteArray &input) const override;

    std::size_t controlFrameStart(std::size_t offset,
                                  const CPD3::Util::ByteArray &input) const override;

    std::size_t controlFrameEnd(std::size_t start,
                                const CPD3::Util::ByteArray &input) const override;

    void limitDataBuffer(CPD3::Util::ByteArray &buffer) override;

    void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingInstrumentTimeout(double time) override;

    void discardDataCompleted(double time) override;

    void shutdownInProgress(double time) override;
};

class AcquireAzonixUMAC1050Component
        : public QObject, virtual public CPD3::Acquisition::AcquisitionComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_azonix_umac1050"
                              FILE
                              "acquire_azonix_umac1050.json")

    CPD3::ComponentOptions getBaseOptions();

public:
    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
