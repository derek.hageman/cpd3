/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <array>
#include <cstdint>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    double unpolledRemaining;
    double unpolledInterval;

    QDateTime time;

    double N;
    double TDinlet;
    double Tinlet;
    double Uinlet;
    double Tcon;
    double TDgrowth;
    double Tini;
    double Tmod;
    double Topt;
    double Thsk;
    double Tcase;

    double PCT;
    double P;
    double Q;
    double modSet;
    double pulseHeight;
    double pulseFraction;

    std::uint32_t flags;

    ModelInstrument()
            : incoming(),
              outgoing(),
              unpolledRemaining(FP::undefined()),
              unpolledInterval(1.0),
              time(QDateTime(QDate(2013, 01, 01), QTime(0, 0, 0)))
    {
        N = 1234.0;
        TDinlet = 85.0;
        Tinlet = 21.0;
        Uinlet = 45.0;
        Tcon = 25.0;
        TDgrowth = 35.0;
        Tini = 30.0;
        Tmod = 23.0;
        Topt = 22.0;
        Thsk = 55.0;
        Tcase = 27.0;

        PCT = 85.0;
        P = 980.0;
        Q = 0.2;
        modSet = 35.0;
        pulseHeight = 2312.0;
        pulseFraction = 50;

        flags = 0;
    }

    double counts() const
    {
        return N * Q * 1000.0;
    }

    void unpolledResponse()
    {
        outgoing += QByteArray::number(time.date().year()).rightJustified(4, '0');
        outgoing += "/";
        outgoing += QByteArray::number(time.date().month()).rightJustified(2, '0');
        outgoing += "/";
        outgoing += QByteArray::number(time.date().day()).rightJustified(2, '0');
        outgoing += " ";
        outgoing += QByteArray::number(time.time().hour()).rightJustified(2, '0');
        outgoing += ":";
        outgoing += QByteArray::number(time.time().minute()).rightJustified(2, '0');
        outgoing += ":";
        outgoing += QByteArray::number(time.time().second()).rightJustified(2, '0');
        outgoing += ",";

        outgoing += QByteArray::number(N, 'f', 0).rightJustified(7);
        outgoing += ",";
        outgoing += QByteArray::number(TDinlet, 'f', 1).rightJustified(5);
        outgoing += ",";
        outgoing += QByteArray::number(Tinlet, 'f', 1).rightJustified(4);
        outgoing += ",";
        outgoing += QByteArray::number(Uinlet, 'f', 1).rightJustified(4);
        outgoing += ",";
        outgoing += QByteArray::number(Tcon, 'f', 1).rightJustified(5);
        outgoing += ",";
        outgoing += QByteArray::number(Tini, 'f', 1).rightJustified(4);
        outgoing += ",";
        outgoing += QByteArray::number(Tmod, 'f', 1).rightJustified(4);
        outgoing += ",";
        outgoing += QByteArray::number(Topt, 'f', 1).rightJustified(4);
        outgoing += ",";
        outgoing += QByteArray::number(Thsk, 'f', 1).rightJustified(7);
        outgoing += ",";
        outgoing += QByteArray::number(Tcase, 'f', 1).rightJustified(5);
        outgoing += ",";
        outgoing += QByteArray::number(PCT, 'f', 0).rightJustified(6);
        outgoing += ",";
        outgoing += QByteArray::number(modSet, 'f', 1).rightJustified(5);
        outgoing += ",";
        outgoing += QByteArray::number(TDgrowth, 'f', 1).rightJustified(5);
        outgoing += ",";
        outgoing += QByteArray::number(P, 'f', 0).rightJustified(5);
        outgoing += ",";
        outgoing += QByteArray::number(Q * 1000.0, 'f', 0).rightJustified(3);
        outgoing += ",  1,  10000,      0,";
        outgoing += QByteArray::number(counts(), 'f', 0).rightJustified(6);
        outgoing += ",";
        outgoing += QByteArray::number(pulseHeight, 'f', 0).rightJustified(5);
        outgoing += ".";
        outgoing += QByteArray::number(pulseFraction, 'f', 0).rightJustified(2, '0');
        outgoing += ",";
        outgoing += QByteArray::number((int) flags, 16).rightJustified(6);
        outgoing += ", ...., 141\r";
    }

    void advance(double seconds)
    {
        time = time.addMSecs(static_cast<qint64>(std::ceil(seconds * 1000.0)));

        if (FP::defined(unpolledRemaining)) {
            unpolledRemaining -= seconds;
            while (unpolledRemaining < 0.0) {
                unpolledRemaining += unpolledInterval;
                unpolledResponse();
            }
        }

        int idxCR = -1;
        while ((idxCR = incoming.indexOf('\r')) >= 0) {
            QByteArray line(incoming.mid(0, idxCR).trimmed());

            incoming = incoming.mid(idxCR + 1);

            outgoing += line + "\n\r";
            if (line == "ver" || line == "rv") {
                outgoing += " Serial Number: 141\r\n"
                            " FW Ver: 3.03a\r\n"
                            " 2022 Aug 31\r\n\r\n";
            } else if (line == "hdr") {
                outgoing += "year time, Concentration, DewPoint,Input T, Input RH \r\n"
                            "Cond T, Init T,Mod T, Opt T, HeatSink T,  Case T,\r\n"
                            "wickSensor, ModSet, Humidifer Exit DP,\r\n"
                            "Abs. Press., flow (cc/min)\r\n"
                            "log interval, corrected live time, measured dead time, raw counts, PulseHeight.Thres2\r\n"
                            "Status(hex code), Status(ascii), Serial Number\r\n";
            } else if (line.startsWith("rtc,")) {
                outgoing += " \r\nrtc,00: OK\r\n";
            } else if (line.startsWith("Log,")) {
                bool ok = false;
                unpolledInterval = line.mid(4).toInt(&ok);
                if (ok) {
                    if (unpolledInterval > 0.0) {
                        unpolledRemaining = unpolledInterval;
                    } else {
                        unpolledRemaining = FP::undefined();
                    }
                    outgoing += "OK\r\n";
                } else {
                    outgoing += "ERROR\r\n";
                }
            } else {
                outgoing += "ERROR in cmd\r\n";
            }
        }
    }
};


class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;

    bool checkMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("P", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("Q", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("Tu", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("T1", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("T2", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("T3", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("T4", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("T5", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("T6", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("Uu", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("TDu", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("TD1", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("PCT", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("V", Variant::Root(), {}, time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("ZSTATE", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("C", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("ZQ", Variant::Root(), {}, time))
            return false;
        return true;
    }

    bool checkContiguous(StreamCapture &stream)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("P"))
            return false;
        if (!stream.checkContiguous("Pd"))
            return false;
        if (!stream.checkContiguous("Q"))
            return false;
        if (!stream.checkContiguous("Tu"))
            return false;
        if (!stream.checkContiguous("T1"))
            return false;
        if (!stream.checkContiguous("T2"))
            return false;
        if (!stream.checkContiguous("T3"))
            return false;
        if (!stream.checkContiguous("T4"))
            return false;
        if (!stream.checkContiguous("T5"))
            return false;
        if (!stream.checkContiguous("T6"))
            return false;
        if (!stream.checkContiguous("Uu"))
            return false;
        if (!stream.checkContiguous("TDu"))
            return false;
        if (!stream.checkContiguous("TD1"))
            return false;
        if (!stream.checkContiguous("PCT"))
            return false;
        if (!stream.checkContiguous("V"))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream,
                     const ModelInstrument &model,
                     double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("P", Variant::Root(model.P), time))
            return false;
        if (!stream.hasAnyMatchingValue("Q", Variant::Root(model.Q), time))
            return false;
        if (!stream.hasAnyMatchingValue("Tu", Variant::Root(model.Tinlet), time))
            return false;
        if (!stream.hasAnyMatchingValue("T1", Variant::Root(model.Tcon), time))
            return false;
        if (!stream.hasAnyMatchingValue("T2", Variant::Root(model.Tini), time))
            return false;
        if (!stream.hasAnyMatchingValue("T3", Variant::Root(model.Tmod), time))
            return false;
        if (!stream.hasAnyMatchingValue("T4", Variant::Root(model.Topt), time))
            return false;
        if (!stream.hasAnyMatchingValue("T5", Variant::Root(model.Thsk), time))
            return false;
        if (!stream.hasAnyMatchingValue("T6", Variant::Root(model.Tcase), time))
            return false;
        if (!stream.hasAnyMatchingValue("Uu", Variant::Root(model.Uinlet), time))
            return false;
        if (!stream.hasAnyMatchingValue("TDu", Variant::Root(model.TDinlet), time))
            return false;
        if (!stream.hasAnyMatchingValue("TD1", Variant::Root(model.TDgrowth), time))
            return false;
        if (!stream.hasAnyMatchingValue("PCT", Variant::Root(model.PCT), time))
            return false;
        if (!stream.hasAnyMatchingValue("V", Variant::Root(model.pulseHeight), time))
            return false;
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream,
                             const ModelInstrument &model,
                             double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("C", Variant::Root(model.counts()), time))
            return false;
        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_ad_cpcmagic250"));
        QVERIFY(component);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = 0.0;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress) {
            control.advance(0.25);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(0.5);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(0.2);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void interactiveAutoprobeAlreadyActive()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = 0.0;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 100; i++) {
            control.advance(0.2);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated.wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(control.time());

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(0.2);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = 0.0;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.5);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")));
        control.advance(0.1);

        for (int i = 0; i < 50; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = 0.0;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")));

        for (int i = 0; i < 50; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

};

QTEST_MAIN(TestComponent)

#include "test.moc"
