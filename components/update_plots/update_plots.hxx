/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef UPDATEPLOTS_H
#define UPDATEPLOTS_H

#include "core/first.hxx"

#include <mutex>
#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QList>
#include <QStringList>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "core/textsubstitution.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/processingtapchain.hxx"
#include "graphing/displaylayout.hxx"

class UpdatePlots;

class PlotOutputTarget : public QObject {
Q_OBJECT

    UpdatePlots *parent;
    QString id;
    CPD3::Graphing::DisplayDynamicContext context;

    CPD3::Graphing::DisplayLayout *display;
    QSize size;
    QFileInfo file;
    bool createDirectory;
    bool opaque;

    CPD3::Data::Variant::Root postProcessingActions;

public:
    PlotOutputTarget(UpdatePlots *parent,
                     const QString &id,
                     const CPD3::Data::ValueSegment::Transfer &config,
                     const CPD3::Graphing::DisplayDefaults &defaults,
                     const CPD3::Graphing::DisplayDynamicContext &contex,
                     double start,
                     double end);

    virtual ~PlotOutputTarget();

    void registerChain(CPD3::Data::ProcessingTapChain *chain);

    bool finish();

    inline QFileInfo getFile() const
    { return file; }

    inline QString getID() const
    { return id; }

    CPD3::ActionFeedback::Source feedback;

private slots:

    void writeOutput();

signals:

    void finished();
};

class UpdatePlots : public CPD3::CPD3Action {
Q_OBJECT

    friend class PlotOutputTarget;

    std::vector<std::string> profiles;
    std::vector<CPD3::Data::SequenceName::Component> stations;
    bool reprocess;
    bool recalculateData;
    double lockTimeout;

    bool terminated;
    std::mutex mutex;

    CPD3::TextSubstitutionStack substitutions;

    bool invokeCommand(const QString &program, const QStringList &arguments);

    bool executeActions(const CPD3::Data::Variant::Read &config);

    bool testTerminated();

    struct ProfileState {
        double lastRunTime;
        double dataModified;
        double dataStartTime;
        double dataEndTime;

        ProfileState() : lastRunTime(CPD3::FP::undefined()),
                         dataModified(CPD3::FP::undefined()),
                         dataStartTime(CPD3::FP::undefined()),
                         dataEndTime(CPD3::FP::undefined())
        { }
    };

    bool processStation(const CPD3::Data::SequenceName::Component &station,
                        const std::string &profile,
                        const CPD3::Data::Variant::Read &config,
                        const CPD3::Data::ValueSegment::Transfer &outputConfig,
                        ProfileState &state);

    CPD3::Threading::Signal<> terminateRequested;

public:
    UpdatePlots();

    UpdatePlots(const CPD3::ComponentOptions &options,
                const std::vector<CPD3::Data::SequenceName::Component> &setStations = {});

    virtual ~UpdatePlots();

    virtual void signalTerminate();

protected:
    virtual void run();
};

class UpdatePlotsComponent : public QObject, virtual public CPD3::ActionComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::ActionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.update_plots"
                              FILE
                              "update_plots.json")

public:
    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual int actionAllowStations();

    CPD3::CPD3Action *createAction(const CPD3::ComponentOptions &options = CPD3::ComponentOptions(),
                                   const std::vector<std::string> &stations = {}) override;
};

#endif
