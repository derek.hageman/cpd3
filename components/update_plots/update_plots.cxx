/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QSvgGenerator>
#include <QImage>
#include <QDir>
#include <QLoggingCategory>

#include "datacore/sequencefilter.hxx"
#include "datacore/variant/composite.hxx"
#include "database/runlock.hxx"
#include "core/qtcompat.hxx"
#include "core/memory.hxx"
#include "graphing/displayenable.hxx"
#include "io/process.hxx"

#include "update_plots.hxx"


Q_LOGGING_CATEGORY(log_component_update_plots, "cpd3.component.update.plots", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Graphing;

UpdatePlots::UpdatePlots()
        : profiles(),
          stations(),
          reprocess(false),
          recalculateData(false),
          lockTimeout(30.0),
          terminated(false),
          mutex()
{ }

UpdatePlots::UpdatePlots(const ComponentOptions &options,
                         const std::vector<SequenceName::Component> &setStations)
        : profiles(),
          stations(setStations),
          reprocess(false),
          recalculateData(false),
          lockTimeout(30.0),
          terminated(false),
          mutex()
{
    if (options.isSet("profile")) {
        std::unordered_set<std::string> unique;
        for (const auto &add : qobject_cast<ComponentOptionStringSet *>(
                options.get("profile"))->get()) {
            if (add.isEmpty())
                continue;
            unique.insert(add.toLower().toStdString());
        }
        Util::append(unique, profiles);
    }
    if (options.isSet("timeout")) {
        lockTimeout = qobject_cast<ComponentOptionSingleDouble *>(options.get("timeout"))->get();
    }
    if (options.isSet("reprocess")) {
        reprocess = qobject_cast<ComponentOptionBoolean *>(options.get("reprocess"))->get();
    }
    if (options.isSet("recalculate")) {
        recalculateData = qobject_cast<ComponentOptionBoolean *>(options.get("recalculate"))->get();
    }
}

UpdatePlots::~UpdatePlots() = default;

void UpdatePlots::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    terminateRequested();
}

bool UpdatePlots::testTerminated()
{
    std::lock_guard<std::mutex> lock(mutex);
    return terminated;
}

PlotOutputTarget::PlotOutputTarget(UpdatePlots *p,
                                   const QString &i,
                                   const ValueSegment::Transfer &config,
                                   const DisplayDefaults &defaults,
                                   const DisplayDynamicContext &ctx,
                                   double start,
                                   double end) : parent(p),
                                                 id(i),
                                                 context(ctx),
                                                 display(NULL),
                                                 size(),
                                                 file(parent->substitutions
                                                            .apply(config.back()["Filename"].toQString())),
                                                 createDirectory(true),
                                                 opaque(config.back()["Opaque"].toBool()),
                                                 postProcessingActions(config.back()["Actions"])
{
    if (config.back()["CreateDirectory"].exists())
        createDirectory = config.back()["CreateDirectory"].toBool();

    if (file.fileName().isEmpty())
        return;

    qint64 width = config.back()["Width"].toInt64();
    if (!INTEGER::defined(width) || width <= 0)
        width = 1024;
    qint64 height = config.back()["Height"].toInt64();
    if (!INTEGER::defined(height) || height <= 0)
        height = 768;
    size = QSize((int) width, (int) height);
    if (!size.isValid())
        return;

    display = new DisplayLayout(defaults, this);
    connect(display, SIGNAL(readyForFinalPaint()), this, SLOT(writeOutput()), Qt::QueuedConnection);
    display->initialize(ValueSegment::withPath(config, "Layout"), defaults);
    display->setVisibleTimeRange(start, end);
}

PlotOutputTarget::~PlotOutputTarget()
{
    if (display != NULL)
        delete display;
}

void PlotOutputTarget::registerChain(ProcessingTapChain *chain)
{
    {
        std::lock_guard<std::mutex> lock(parent->mutex);
        if (display == NULL)
            return;
    }
    display->registerChain(chain);
}

void PlotOutputTarget::writeOutput()
{
    if (parent->testTerminated()) {
        emit finished();
        return;
    }
    {
        std::lock_guard<std::mutex> lock(parent->mutex);
        if (display == NULL) {
            emit finished();
            return;
        }
    }

    if (createDirectory) {
        QDir dir(QFileInfo(file).absoluteDir());
        if (!dir.exists()) {
            qCDebug(log_component_update_plots) << "Creating target directory:"
                                                << dir.absolutePath();
            dir.mkpath(dir.absolutePath());
        }
    }

    qCDebug(log_component_update_plots) << "Writing output plot" << file.absoluteFilePath();

    if (file.fileName().endsWith(".svg", Qt::CaseInsensitive)) {
        feedback.emitStage(tr("Writing %1").arg(file.fileName()),
                           tr("Writing to final output file."), false);

        QFile::remove(file.absoluteFilePath());
        QSvgGenerator svg;
        svg.setFileName(file.fileName());
        svg.setSize(size);
        svg.setViewBox(QRect(0, 0, size.width(), size.height()));
        QPainter painter;
        painter.begin(&svg);
        display->paint(&painter, QRectF(0, 0, size.width(), size.height()), context);
        painter.end();
    } else {
        feedback.emitStage(tr("Drawing output"), tr("The display is being drawn."), false);
        QImage img(size, QImage::Format_ARGB32);
        if (opaque)
            img.fill(0xFFFFFFFF);
        else
            img.fill(0x00FFFFFF);
        QPainter painter;
        painter.begin(&img);
        display->paint(&painter, QRectF(0, 0, size.width(), size.height()), context);
        painter.end();

        feedback.emitStage(tr("Writing image to %1").arg(file.fileName()),
                           tr("Writing to final output file."), false);

        QFile::remove(file.absoluteFilePath());
        img.save(file.absoluteFilePath());
    }

    qCDebug(log_component_update_plots) << "Final size for plot" << file.absoluteFilePath() << "is"
                                        << file.size() << "byte(s)";

    DisplayLayout *toDelete = display;
    {
        std::lock_guard<std::mutex> lock(parent->mutex);
        display = NULL;
    }
    delete toDelete;

    emit finished();
}

bool PlotOutputTarget::finish()
{
    for (;;) {
        QEventLoop el;

        QTimer::singleShot(500, &el, SLOT(quit()));
        connect(this, SIGNAL(finished()), &el, SLOT(quit()), Qt::QueuedConnection);
        parent->terminateRequested.connect(&el, "quit");
        el.processEvents();

        {
            std::lock_guard<std::mutex> lock(parent->mutex);
            if (parent->terminated)
                return false;
            if (display == NULL)
                break;
        }

        el.exec();
    }

    TextSubstitutionStack::Context subctx(parent->substitutions);
    parent->substitutions.setFile("file", file);
    parent->substitutions.setString("name", id);
    return parent->executeActions(postProcessingActions);
}

bool UpdatePlots::invokeCommand(const QString &program, const QStringList &arguments)
{
    qCDebug(log_component_update_plots) << "Invoking:" << program << arguments;

    auto proc = IO::Process::Spawn(program, arguments).forward().create();
    if (!proc)
        return false;

    IO::Process::Instance::ExitMonitor exitOk(*proc);
    exitOk.connectTerminate(terminateRequested);
    if (testTerminated())
        return false;

    if (!proc->start() || !proc->wait())
        return false;
    return exitOk.success();
}

bool UpdatePlots::executeActions(const Variant::Read &config)
{
    switch (config.getType()) {
    case Variant::Type::String: {
        QStringList arguments(substitutions.apply(config.toQString())
                                           .split(QRegExp("\\s+"), QString::SkipEmptyParts));
        if (arguments.isEmpty()) {
            qCWarning(log_component_update_plots) << "Empty command as the only action";
            return true;
        }
        QString program(arguments.takeFirst());

        feedback.emitStage(tr("Execute External Program"),
                           tr("An external program is currently being executed."), false);
        feedback.emitState(program);
        return invokeCommand(program, arguments);
    }

    case Variant::Type::Array: {
        auto children = config.toArray();
        if (children.empty())
            return true;

        feedback.emitStage(tr("Execute External Program"),
                           tr("An external program is currently being executed."), false);

        for (auto child : children) {
            QString program(substitutions.apply(child.hash("Program").toQString()));
            if (program.isEmpty()) {
                qCWarning(log_component_update_plots) << "Empty command in action list";
                continue;
            }

            QStringList arguments;
            for (auto add : child.hash("Arguments").toArray()) {
                arguments.append(substitutions.apply(add.toQString()));
            }

            feedback.emitState(program);
            bool status = invokeCommand(program, arguments);
            if (!status && !child.hash("AllowFailure").toBool()) {
                qCDebug(log_component_update_plots) << "Execution of" << program << "failed";
                return false;
            }
        }

        break;
    }

    case Variant::Type::Hash: {
        QString program(substitutions.apply(config["Program"].toQString()));
        if (program.isEmpty()) {
            qCWarning(log_component_update_plots) << "Empty command in action list";
            return true;
        }

        feedback.emitStage(tr("Execute External Program"),
                           tr("An external program is currently being executed."), false);

        QStringList arguments;
        for (auto add : config["Arguments"].toArray()) {
            arguments.append(substitutions.apply(add.toQString()));
        }

        feedback.emitState(program);
        bool status = invokeCommand(program, arguments);
        if (!status && !config["AllowFailure"].toBool()) {
            qCDebug(log_component_update_plots) << "Execution of" << program << "failed";
            return false;
        }
    }

    default:
        break;
    }

    return true;
}

bool UpdatePlots::processStation(const SequenceName::Component &station,
                                 const std::string &profile,
                                 const Variant::Read &config,
                                 const ValueSegment::Transfer &outputConfig,
                                 ProfileState &state)
{
    Q_UNUSED(profile);

    double effectiveStart = FP::undefined();
    if (FP::defined(state.dataEndTime)) {
        if (config["Start"].exists()) {
            effectiveStart =
                    Variant::Composite::offsetTimeInterval(config["Start"], state.dataEndTime,
                                                           false);
        }
    }
    if (config["MaximumAge"].exists()) {
        double begin =
                Variant::Composite::offsetTimeInterval(config["MaximumAge"], Time::time(), false);
        if (FP::defined(begin) && (!FP::defined(effectiveStart) || effectiveStart < begin))
            effectiveStart = begin;
    }

    auto archive = config["Archive"].toString();
    if (archive.empty())
        archive = "raw";
    substitutions.setString("archive", QString::fromStdString(archive).toLower());

    feedback.emitState(tr("Reading data"));

    SequenceFilter filter;
    filter.configure(config["Trigger"], {QString::fromStdString(station)});
    auto selections = filter.toArchiveSelection({station}, {archive});
    for (auto &sel : selections) {
        sel.modifiedAfter = state.lastRunTime;
        sel.includeMetaArchive = false;
        sel.includeDefaultStation = false;
        sel.start = effectiveStart;
        sel.end = FP::undefined();
    }

    double latestDataEnd = FP::undefined();
    double latestDataStart = FP::undefined();

    std::unique_ptr<Archive::Access> access(new Archive::Access);
    Archive::Access::ReadLock lock(*access, true);

    Threading::Receiver accessReceiver;
    terminateRequested.connect(accessReceiver,
                               std::bind(&Archive::Access::signalTerminate, access.get()));

    if (testTerminated())
        return false;

    if (!selections.empty()) {
        qCDebug(log_component_update_plots) << "Issuing archive read for" << station << ':'
                                            << selections;

        ArchiveSink::Iterator data;
        auto reader = access->readArchive(selections, &data);

        Threading::Receiver readerReceiver;

        terminateRequested.connect(accessReceiver, std::bind(&ArchiveSink::Iterator::abort, &data));
        terminateRequested.connect(accessReceiver,
                                   std::bind(&Archive::Access::ArchiveContext::signalTerminate,
                                             reader.get()));
        if (testTerminated())
            return false;

        while (data.hasNext()) {
            auto value = data.next();
            if (!filter.acceptAdvancing(value))
                continue;

            Q_ASSERT(FP::defined(value.getModified()));
            if (!FP::defined(state.dataModified) || state.dataModified < value.getModified())
                state.dataModified = value.getModified();

            if (FP::defined(value.getStart()) && !FP::defined(latestDataStart))
                latestDataStart = value.getStart();

            if (FP::defined(value.getEnd()) &&
                    (!FP::defined(latestDataEnd) || latestDataEnd < value.getEnd())) {
                latestDataEnd = value.getEnd();
            }
        }

        terminateRequested.disconnect();

        reader->wait();
        data.wait();
    } else if (config["Start"].exists() || config["End"].exists()) {
        qCDebug(log_component_update_plots) << "No trigger defined, plotting skipped";
        return false;
    }

    double plotBegin = FP::undefined();
    double plotEnd = FP::undefined();

    if (!FP::defined(latestDataEnd)) {
        if (reprocess || config["AlwaysPlot"].toBool()) {
            if (FP::defined(state.dataEndTime)) {
                plotEnd = state.dataEndTime;
            } else if (config["Start"].exists() || config["End"].exists()) {
                qCDebug(log_component_update_plots) << "No data bounds available, plotting skipped";
                return false;
            }
        } else {
            qCDebug(log_component_update_plots) << "No trigger data updated, plotting skipped";
            return false;
        }
    } else {
        if (!FP::defined(state.dataEndTime) || latestDataEnd > state.dataEndTime) {
            state.dataEndTime = latestDataEnd;
        } else {
            latestDataEnd = state.dataEndTime;
        }
        plotEnd = latestDataEnd;
    }

    if (config["Start"].exists()) {
        plotBegin = Variant::Composite::offsetTimeInterval(config["Start"], plotEnd, false);
    } else {
        plotBegin = FP::undefined();
    }
    if (FP::defined(plotBegin)) {
        state.dataStartTime = plotBegin;
    } else if (!FP::defined(state.dataStartTime) ||
            (FP::defined(latestDataStart) && latestDataStart < state.dataStartTime)) {
        state.dataStartTime = latestDataStart;
    }

    if (config["End"].exists()) {
        double check = Variant::Composite::offsetTimeInterval(config["End"], plotBegin, true);
        if (Range::compareStartEnd(plotBegin, plotEnd) < 0)
            plotEnd = check;
    } else {
        plotEnd = FP::undefined();
    }

    feedback.emitState(tr("Creating outputs"));

    substitutions.setTime("time", plotEnd, false);
    substitutions.setTime("end", state.dataEndTime, false);
    substitutions.setTime("lasttime", plotBegin, false);
    substitutions.setTime("start", state.dataStartTime, false);


    qCDebug(log_component_update_plots) << "Checking for plot availability in"
                                        << Logging::range(plotBegin, plotEnd);

    if (!executeActions(config["StartActions"]))
        return false;
    if (testTerminated())
        return false;


    DisplayDefaults defaults;
    DisplayDynamicContext displayContext;

    defaults.setArchive(archive);
    displayContext.setStation(station);
    displayContext.setInteractive(false);

    CPD3::Data::ProcessingTapChain *chain = new ProcessingTapChain;
    terminateRequested.connect(chain, "signalTerminate", Qt::DirectConnection);

    chain->setDefaultSelection(plotBegin, plotEnd, {station}, {archive});

    QList<PlotOutputTarget *> targets;
    std::unordered_map<Variant::PathElement::HashIndex, ValueSegment::Transfer>
            displayConfiguration;
    {
        DisplayEnable displayEnable(plotBegin, plotEnd, station, archive, {}, access.get());
        for (const auto &seg : outputConfig) {
            for (auto child : seg.value().toHash()) {
                if (testTerminated())
                    return false;

                if (child.first.empty())
                    continue;
                if (!child.second.hash("Layout").exists())
                    continue;
                if (!displayEnable.enabled(child.second.hash("Enable")))
                    continue;

                displayConfiguration[child.first].emplace_back(seg.getStart(), seg.getEnd(),
                                                               Variant::Root(child.second));
            }
        }
    }

    accessReceiver.disconnect();
    lock.release();
    access.reset();

    for (const auto &displayData : displayConfiguration) {
        ValueSegment::Transfer activeConfig;
        for (const auto &add : displayData.second) {
            if (!activeConfig.empty() &&
                    !DisplayLayout::mergable(activeConfig.back().value().hash("Layout"),
                                             add.value().hash("Layout"))) {
                activeConfig.clear();
            }
            activeConfig.emplace_back(add);
        }
        if (activeConfig.empty())
            continue;

        TextSubstitutionStack::Context subctx(substitutions);
        substitutions.setString("name", QString::fromStdString(displayData.first));

        PlotOutputTarget *target =
                new PlotOutputTarget(this, QString::fromStdString(displayData.first), activeConfig,
                                     defaults, displayContext, plotBegin, plotEnd);
        targets.append(target);
        target->registerChain(chain);

        target->feedback.forward(feedback);
    }

    if (testTerminated()) {
        qDeleteAll(targets);
        delete chain;
        return false;
    }

    if (targets.isEmpty()) {
        delete chain;
        qCDebug(log_component_update_plots) << "No plots to update";
        return true;
    }
    feedback.emitState(tr("Starting output"));

    double chainProcessingBegin = Time::time();

    qCDebug(log_component_update_plots) << "Starting update for" << targets.size() << "plot(s) in"
                                        << Logging::range(plotBegin, plotEnd);

    chain->feedback.forward(feedback);
    chain->start();
    chain->wait();

    if (testTerminated()) {
        qDeleteAll(targets);
        delete chain;
        return false;
    }

    double chainProcessingEnd = Time::time();
    qCDebug(log_component_update_plots) << "Processing chain completed in" << Logging::elapsed(
                chainProcessingEnd - chainProcessingBegin);

    bool result = true;

    for (QList<PlotOutputTarget *>::const_iterator plot = targets.constBegin(),
            end = targets.constEnd(); plot != end; ++plot) {
        if (!(*plot)->finish()) {
            result = false;
        } else {
            TextSubstitutionStack::Context subctx(substitutions);
            substitutions.setFile("file", (*plot)->getFile());
            substitutions.setFile("name", (*plot)->getID());

            if (!executeActions(config["FileActions"])) {
                result = false;
            }

            qCDebug(log_component_update_plots) << "Processing completed for"
                                                << (*plot)->getFile().absoluteFilePath();
        }

        delete *plot;
    }

    if (result && !executeActions(config["EndActions"]))
        result = false;

    double updateCompletedTime = Time::time();
    qCDebug(log_component_update_plots) << "Plot generation for" << station << "completed in"
                                        << Logging::elapsed(
                                                updateCompletedTime - chainProcessingBegin);

    delete chain;
    return result;
}

void UpdatePlots::run()
{
    if (testTerminated())
        return;
    double startProcessing = Time::time();

    SequenceSegment::Transfer globalConfiguration;
    {
        Archive::Access access;
        Archive::Access::ReadLock lock(access);

        auto allStations = access.availableStations(stations);
        if (allStations.empty()) {
            qCDebug(log_component_update_plots) << "No stations available";
            return;
        }
        stations.clear();
        std::copy(allStations.begin(), allStations.end(), Util::back_emplacer(stations));
        std::sort(stations.begin(), stations.end());
        globalConfiguration = SequenceSegment::Stream::read(
                Archive::Selection(FP::undefined(), FP::undefined(), stations, {"configuration"},
                                   {"update_plots"}), &access);
    }

    if (testTerminated())
        return;

    if (stations.empty()) {
        qCDebug(log_component_update_plots) << "No stations to update";
        return;
    }

    auto activeConfig =
            Range::findIntersecting(globalConfiguration.begin(), globalConfiguration.end(),
                                    Time::time());
    if (activeConfig == globalConfiguration.end()) {
        qCDebug(log_component_update_plots) << "No configuration for" << stations;
        return;
    }

    Memory::release_unused();

    qCDebug(log_component_update_plots) << "Starting plots update for" << stations.size()
                                        << "stations(s)";

    for (const auto &station : stations) {
        SequenceName stationUnit(station, "configuration", "update_plots");
        auto stationActiveConfig = activeConfig->getValue(stationUnit).hash("Profiles");
        if (!stationActiveConfig.exists()) {
            for (auto &s : globalConfiguration) {
                s.clearValue(stationUnit);
            }
            continue;
        }

        auto activeProfiles = profiles;
        if (activeProfiles.empty()) {
            for (const auto &add : stationActiveConfig.toHash().keys()) {
                activeProfiles.push_back(add);
            }
        }

        for (const auto &profile : activeProfiles) {
            if (profile.empty())
                continue;
            auto activeProfileConfig = stationActiveConfig.hash(profile);
            if (!activeProfileConfig.exists())
                continue;
            bool plotEmpty = activeProfileConfig.hash("AllowEmpty").toBoolean();
            if (!plotEmpty && !activeProfileConfig.hash("Displays").exists())
                continue;

            Memory::release_unused();

            ValueSegment::Transfer profileDisplays;
            for (const auto &s : globalConfiguration) {
                auto add = s.getValue(stationUnit).hash("Profiles").hash(profile).hash("Displays");
                if (!add.exists())
                    continue;
                profileDisplays.emplace_back(s.getStart(), s.getEnd(), Variant::Root(add));
            }
            if (!plotEmpty && profileDisplays.empty())
                continue;

            substitutions.clear();
            substitutions.setString("station", QString::fromStdString(station).toLower());
            substitutions.setString("profile", QString::fromStdString(profile).toLower());

            QString rcKey(QString("updateplots %1 %2").arg(QString::fromStdString(profile),
                                                           QString::fromStdString(station)));

            qCDebug(log_component_update_plots) << "Updating profile" << profile << "for station"
                                                << station;

            Database::RunLock rc;
            static const quint8 rcVersion = 1;

            feedback.emitStage(tr("Lock %1").arg(QString::fromStdString(station).toUpper()),
                               tr("The system is acquiring an exclusive lock for %1 on profile %2.")
                                       .arg(QString::fromStdString(station).toUpper(),
                                            QString::fromStdString(profile).toLower()), false);
            bool ok = false;
            qCDebug(log_component_update_plots) << "Locking " << station << profile;

            ProfileState state;
            state.lastRunTime = rc.acquire(rcKey, lockTimeout, &ok);
            if (!ok) {
                feedback.emitFailure();
                qCDebug(log_component_update_plots) << "Update skipped for profile" << profile
                                                    << "and station" << station;
                continue;
            }

            QByteArray data(rc.get(rcKey));
            if (!data.isEmpty()) {
                QDataStream stream(&data, QIODevice::ReadOnly);
                quint8 version = 0;
                stream >> version >> state.dataStartTime >> state.dataEndTime;
                if (stream.status() != QDataStream::Ok || version != rcVersion) {
                    state.dataStartTime = FP::undefined();
                    state.dataEndTime = FP::undefined();
                    qCDebug(log_component_update_plots) << "Invalid key data";
                }
            }

            if (testTerminated()) {
                rc.fail();
                break;
            }

            if (recalculateData) {
                state.lastRunTime = FP::undefined();
                state.dataStartTime = FP::undefined();
                state.dataEndTime = FP::undefined();
            }

            feedback.emitStage(tr("Process %1").arg(QString::fromStdString(station).toUpper()),
                               tr("The system is starting processing for %1 on profile %2.").arg(
                                       QString::fromStdString(station).toUpper(),
                                       QString::fromStdString(profile).toLower()), false);

            if (FP::defined(state.lastRunTime)) {
                substitutions.setTime("modified", state.lastRunTime, false);
                substitutions.setTime("modifiedafter", state.lastRunTime, false);
            }

            if (processStation(station, profile, activeProfileConfig, profileDisplays, state)) {
                if (FP::defined(state.dataModified)) {
                    QByteArray wr;
                    QDataStream stream(&wr, QIODevice::WriteOnly);
                    stream << rcVersion << state.dataStartTime << state.dataEndTime;
                    rc.set(rcKey, wr);

                    rc.seenTime(state.dataModified);
                    rc.release();
                } else {
                    rc.fail();
                }
                qCDebug(log_component_update_plots) << "Updated completed for profile" << profile
                                                    << "and station" << station;
            } else {
                feedback.emitFailure();
                rc.fail();
            }
        }

        for (auto &s : globalConfiguration) {
            s.clearValue(stationUnit);
        }
    }

    double endProcessing = Time::time();
    qCDebug(log_component_update_plots) << "Finished update after "
                                        << Logging::elapsed(endProcessing - startProcessing);

    QCoreApplication::processEvents();
}


ComponentOptions UpdatePlotsComponent::getOptions()
{
    ComponentOptions options;

    options.add("profile", new ComponentOptionStringSet(tr("profile", "name"), tr("Plot profile"),
                                                        tr("This sets the profiles to update.  If set then only the profiles "
                                                           "listed here will be considered for updating.  "
                                                           "Consult the station configuration for available profiles."),
                                                        tr("All available profiles")));

    ComponentOptionSingleDouble *timeout =
            new ComponentOptionSingleDouble(tr("wait", "name"), tr("Maximum wait time"),
                                            tr("This is the maximum time to wait for another process to "
                                               "complete, in seconds.  If there are two concurrent processes for "
                                               "the same station and profile, the new one will wait at most this "
                                               "many seconds before giving up without performing an update."),
                                            tr("Thirty seconds"));
    timeout->setMinimum(0.0);
    timeout->setAllowUndefined(true);
    options.add("timeout", timeout);

    options.add("reprocess",
                new ComponentOptionBoolean(tr("reprocess", "name"), tr("Regenerate all plots"),
                                           tr("If set then all plots are regenerated, not just those that have "
                                              "been updated since the last run."), ""));

    options.add("recalculate",
                new ComponentOptionBoolean(tr("recalculate", "name"), tr("Recalculate data bounds"),
                                           tr("If set then the plot data bounds are recalculated from scratch.  "
                                              "This can be used if the plots need to be regenerated after"
                                              "corrupted data has been removed."), ""));

    return options;
}

QList<ComponentExample> UpdatePlotsComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Update all stations", "default example name"),
                                     tr("This will update plots for all profiles and all stations.")));

    return examples;
}

int UpdatePlotsComponent::actionAllowStations()
{ return INT_MAX; }


CPD3Action *UpdatePlotsComponent::createAction(const ComponentOptions &options,
                                               const std::vector<std::string> &stations)
{ return new UpdatePlots(options, stations); }
