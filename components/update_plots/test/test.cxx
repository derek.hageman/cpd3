/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QDir>
#include <QFile>
#include <QHostInfo>
#include <QFileInfo>
#include <QTemporaryFile>

#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "core/actioncomponent.hxx"
#include "core/number.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class TestComponent : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;
    QString tmpPath;
    QDir tmpDir;

    ActionComponent *component;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component = qobject_cast<ActionComponent *>(ComponentLoader::create("update_plots"));
        QVERIFY(component);

        QVERIFY(databaseFile.open());
        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());

        tmpDir = QDir(QDir::tempPath());
        tmpPath = "CPD3UpdatePlots-";
        tmpPath.append(Random::string());
        tmpDir.mkdir(tmpPath);
        QVERIFY(tmpDir.cd(tmpPath));
    }

    void cleanupTestCase()
    {
        tmpDir.cdUp();
        tmpDir.rmdir(tmpPath);
    }

    void init()
    {
        databaseFile.resize(0);

        QFileInfoList list(tmpDir.entryInfoList(QDir::Files | QDir::NoDotAndDotDot));
        for (QFileInfoList::const_iterator rm = list.constBegin(), endRm = list.constEnd();
                rm != endRm;
                ++rm) {
            if (!QFile::remove(rm->absoluteFilePath())) {
                qDebug() << "Failed to remove file " << rm->absoluteFilePath();
                continue;
            }
        }
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionStringSet *>(options.get("profile")));
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("timeout")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("reprocess")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("recalculate")));
    }

    void latestPeriod()
    {
        QFileInfo outputPlot1(tmpDir.absolutePath() + "/O1.png");
        QFileInfo outputPlot2(tmpDir.absolutePath() + "/O2.png");
        QFileInfo outputPlot3(tmpDir.absolutePath() + "/O3.png");

        Variant::Root config;
        config["Profiles/aerosol/Trigger/Variable"] = "F1_S11";
        config["Profiles/aerosol/Start"] = 4000;
        config["Profiles/aerosol/End"] = 4000;
        config["Profiles/aerosol/Displays/Display1/Filename"] = outputPlot1.absoluteFilePath();
        config["Profiles/aerosol/Displays/Display1/Layout/1/Type"] = "Timeseries";
        config["Profiles/aerosol/Displays/Display1/Layout/1/Traces/All/Data/Dimensions/#0/Input/#0/Variable"] =
                "T1_S11";
        config["Profiles/aerosol/Displays/Display2/Filename"] = outputPlot2.absoluteFilePath();
        config["Profiles/aerosol/Displays/Display2/Enable/Any"] = "T2_S11";
        config["Profiles/aerosol/Displays/Display2/Layout/1/Type"] = "Timeseries";
        config["Profiles/aerosol/Displays/Display2/Layout/1/Traces/All/Data/Dimensions/#0/Input/#0/Variable"] =
                "T2_S11";
        config["Profiles/aerosol/Displays/Display3/Filename"] = outputPlot2.absoluteFilePath();
        config["Profiles/aerosol/Displays/Display3/Enable/All"] = "T3_S11";
        config["Profiles/aerosol/Displays/Display3/Layout/1/Type"] = "Timeseries";
        config["Profiles/aerosol/Displays/Display3/Layout/1/Traces/All/Data/Dimensions/#0/Input/#0/Variable"] =
                "T2_S11";


#ifdef Q_OS_UNIX
        QTemporaryFile processingFile;
        QVERIFY(processingFile.open());
        config["Profiles/aerosol/Displays/Display1/Actions/Program"] = "sh";
        config["Profiles/aerosol/Displays/Display1/Actions/Arguments/#0"] = "-c";
        config["Profiles/aerosol/Displays/Display1/Actions/Arguments/#1"] =
                QString("echo P1 ${FILE} >> %1").arg(processingFile.fileName());
        config["Profiles/aerosol/Displays/Display2/Actions/Program"] = "sh";
        config["Profiles/aerosol/Displays/Display2/Actions/Arguments/#0"] = "-c";
        config["Profiles/aerosol/Displays/Display2/Actions/Arguments/#1"] =
                QString("echo P2 ${NAME} >> %1").arg(processingFile.fileName());
        config["Profiles/aerosol/FileActions/Program"] = "sh";
        config["Profiles/aerosol/FileActions/Arguments/#0"] = "-c";
        config["Profiles/aerosol/FileActions/Arguments/#1"] =
                QString("echo F ${file} >> %1").arg(processingFile.fileName());
        config["Profiles/aerosol/EndActions/Program"] = "sh";
        config["Profiles/aerosol/EndActions/Arguments/#0"] = "-c";
        config["Profiles/aerosol/EndActions/Arguments/#1"] =
                QString("echo E ${profile} >> %1").arg(processingFile.fileName());
#endif

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"bnd", "configuration", "update_plots"}, config),
                SequenceValue({"bnd", "raw", "T1_S11"}, Variant::Root(1.0), 1000, 2000),
                SequenceValue({"bnd", "raw", "T2_S11"}, Variant::Root(2.0), 1000, 2000),
                SequenceValue({"bnd", "raw", "T1_S11"}, Variant::Root(3.0), 2000, 3000),
                SequenceValue({"bnd", "raw", "T2_S11"}, Variant::Root(4.0), 2000, 3000),
                SequenceValue({"bnd", "raw", "T1_S11"}, Variant::Root(5.0), 3000, 4000),
                SequenceValue({"bnd", "raw", "T2_S11"}, Variant::Root(6.0), 3000, 4000),
                SequenceValue({"bnd", "raw", "T1_S11"}, Variant::Root(6.0), 4000, 5000),
                SequenceValue({"bnd", "raw", "T2_S11"}, Variant::Root(7.0), 4000, 5000)});

        ComponentOptions options(component->getOptions());
        CPD3Action *action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        QCOMPARE((int) outputPlot1.size(), 0);
        QCOMPARE((int) outputPlot2.size(), 0);
        QCOMPARE((int) outputPlot3.size(), 0);
#ifdef Q_OS_UNIX
        QCOMPARE((int) QFileInfo(processingFile).size(), 0);
#endif

        QTest::qSleep(50);

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"bnd", "raw", "F1_S11"}, Variant::Root(), 0.0, 4000.0)});
        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        outputPlot1.refresh();
        outputPlot2.refresh();
        outputPlot3.refresh();
        QVERIFY(outputPlot1.size() > 0);
        QVERIFY(outputPlot2.size() > 0);
        QCOMPARE((int) outputPlot3.size(), 0);
#ifdef Q_OS_UNIX
        QVERIFY(QFileInfo(processingFile).size() > 0);
        {
            QString str(processingFile.readAll());
            QVERIFY(str.contains("P1 " + outputPlot1.absolutePath()));
            QVERIFY(str.contains("P2 Display2"));
            QVERIFY(str.contains("F " + outputPlot1.absolutePath()));
            QVERIFY(str.contains("F " + outputPlot2.absolutePath()));
            QVERIFY(str.contains("E aerosol"));
        }
#endif

        int priorPlot1Size = (int) outputPlot1.size();
        QDateTime priorPlot1Modified(outputPlot1.lastModified());
        int priorPlot2Size = (int) outputPlot2.size();
        QDateTime priorPlot2Modified(outputPlot2.lastModified());

        QTest::qSleep(2000);

        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        outputPlot1.refresh();
        outputPlot2.refresh();
        outputPlot3.refresh();
        QCOMPARE((int) outputPlot1.size(), priorPlot1Size);
        QCOMPARE(outputPlot1.lastModified(), priorPlot1Modified);
        QCOMPARE((int) outputPlot2.size(), priorPlot2Size);
        QCOMPARE(outputPlot2.lastModified(), priorPlot2Modified);


        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"bnd", "raw", "F1_S11"}, Variant::Root(), 1000.0, 2000.0)});

        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        outputPlot1.refresh();
        outputPlot2.refresh();
        QCOMPARE((int) outputPlot1.size(), priorPlot1Size);
        QVERIFY(outputPlot1.lastModified() > priorPlot1Modified);
        QCOMPARE((int) outputPlot2.size(), priorPlot2Size);
        QVERIFY(outputPlot2.lastModified() > priorPlot2Modified);

        priorPlot1Modified = outputPlot1.lastModified();
        priorPlot2Modified = outputPlot2.lastModified();


        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"bnd", "raw", "F1_S11"}, Variant::Root(), 1000.0, 5000.0)});

        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        outputPlot1.refresh();
        outputPlot2.refresh();
        outputPlot3.refresh();
        QVERIFY(outputPlot1.size() > 0);
        QVERIFY(outputPlot2.size() > 0);
        QVERIFY((int) outputPlot1.size() != priorPlot1Size);
        QVERIFY((int) outputPlot2.size() != priorPlot2Size);

        priorPlot1Size = (int) outputPlot1.size();
        priorPlot2Size = (int) outputPlot2.size();
        priorPlot1Modified = outputPlot1.lastModified();
        priorPlot2Modified = outputPlot2.lastModified();

        QTest::qSleep(2000);

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"bnd", "raw", "F1_S11"}, Variant::Root(), 500.0, 600.0)});

        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        outputPlot1.refresh();
        outputPlot2.refresh();
        outputPlot3.refresh();
        QCOMPARE((int) outputPlot1.size(), priorPlot1Size);
        QCOMPARE(outputPlot1.lastModified(), priorPlot1Modified);
        QCOMPARE((int) outputPlot2.size(), priorPlot2Size);
        QCOMPARE(outputPlot2.lastModified(), priorPlot2Modified);

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"bnd", "raw", "F1_S11"}, Variant::Root(), 1500.0, 1600.0)});

        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        outputPlot1.refresh();
        outputPlot2.refresh();
        QCOMPARE((int) outputPlot1.size(), priorPlot1Size);
        QVERIFY(outputPlot1.lastModified() > priorPlot1Modified);
        QCOMPARE((int) outputPlot2.size(), priorPlot2Size);
        QVERIFY(outputPlot2.lastModified() > priorPlot2Modified);

        priorPlot1Modified = outputPlot1.lastModified();
        priorPlot2Modified = outputPlot2.lastModified();

        QTest::qSleep(2000);

        qobject_cast<ComponentOptionBoolean *>(options.get("reprocess"))->set(true);
        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        outputPlot1.refresh();
        outputPlot2.refresh();
        QCOMPARE((int) outputPlot1.size(), priorPlot1Size);
        QVERIFY(outputPlot1.lastModified() > priorPlot1Modified);
        QCOMPARE((int) outputPlot2.size(), priorPlot2Size);
        QVERIFY(outputPlot2.lastModified() > priorPlot2Modified);

        priorPlot1Modified = outputPlot1.lastModified();
        priorPlot2Modified = outputPlot2.lastModified();
    }

    void globalData()
    {
        QFileInfo outputPlot1(tmpDir.absolutePath() + "/O1.png");

        Variant::Root config;
        config["Profiles/aerosol/Trigger/Variable"] = "F1_S11";
        config["Profiles/aerosol/Displays/Display1/Layout/Type"] = "Timeseries";
        config["Profiles/aerosol/Displays/Display1/Layout/Traces/All/Data/Dimensions/#0/Input/#0/Variable"] =
                "T1_S11";
        config["Profiles/aerosol/Displays/Display1/Filename"] = outputPlot1.absoluteFilePath();

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"bnd", "configuration", "update_plots"}, config),
                SequenceValue({"bnd", "raw", "T1_S11"}, Variant::Root(1.0), 1000, 2000),
                SequenceValue({"bnd", "raw", "T2_S11"}, Variant::Root(2.0), 1000, 2000),
                SequenceValue({"bnd", "raw", "T1_S11"}, Variant::Root(3.0), 2000, 3000),
                SequenceValue({"bnd", "raw", "T2_S11"}, Variant::Root(4.0), 2000, 3000),
                SequenceValue({"bnd", "raw", "T1_S11"}, Variant::Root(5.0), 3000, 4000),
                SequenceValue({"bnd", "raw", "T2_S11"}, Variant::Root(6.0), 3000, 4000),
                SequenceValue({"bnd", "raw", "T1_S11"}, Variant::Root(6.0), 4000, 5000),
                SequenceValue({"bnd", "raw", "T2_S11"}, Variant::Root(7.0), 4000, 5000)});

        ComponentOptions options(component->getOptions());
        CPD3Action *action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        QCOMPARE((int) outputPlot1.size(), 0);

        QTest::qSleep(50);

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"bnd", "raw", "F1_S11"}, Variant::Root(), 0.0, 4000.0)});

        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        outputPlot1.refresh();
        QVERIFY(outputPlot1.size() > 0);

        int priorPlot1Size = (int) outputPlot1.size();
        QDateTime priorPlot1Modified(outputPlot1.lastModified());

        QTest::qSleep(2000);

        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        outputPlot1.refresh();
        QCOMPARE((int) outputPlot1.size(), priorPlot1Size);
        QCOMPARE(outputPlot1.lastModified(), priorPlot1Modified);


        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"bnd", "raw", "F1_S11"}, Variant::Root(), 1000.0, 2000.0)});

        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        outputPlot1.refresh();
        QCOMPARE((int) outputPlot1.size(), priorPlot1Size);
        QVERIFY(outputPlot1.lastModified() > priorPlot1Modified);

        priorPlot1Modified = outputPlot1.lastModified();

        QTest::qSleep(2000);

        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        outputPlot1.refresh();
        QCOMPARE((int) outputPlot1.size(), priorPlot1Size);
        QCOMPARE(outputPlot1.lastModified(), priorPlot1Modified);


        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"bnd", "raw", "F1_S11"}, Variant::Root(), 9000.0, 10000.0)});

        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        outputPlot1.refresh();
        QCOMPARE((int) outputPlot1.size(), priorPlot1Size);
        QVERIFY(outputPlot1.lastModified() > priorPlot1Modified);

        priorPlot1Modified = outputPlot1.lastModified();

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"bnd", "raw", "T1_S11"}, Variant::Root(), 9000.0, 10000.0)});

        qobject_cast<ComponentOptionBoolean *>(options.get("reprocess"))->set(true);
        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        outputPlot1.refresh();
        QVERIFY(outputPlot1.size() > 0);
        QVERIFY((int) outputPlot1.size() != priorPlot1Size);

        priorPlot1Modified = outputPlot1.lastModified();
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
