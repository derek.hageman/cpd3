/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "core/environment.hxx"

#include "corr_weiss.hxx"

using namespace CPD3;
using namespace CPD3::Data;

void CorrWeiss::handleOptions(const ComponentOptions &options)
{
    if (options.isSet("transmittance-start")) {
        defaultTransmittanceStart =
                qobject_cast<DynamicInputOption *>(options.get("transmittance-start"))->getInput();
    } else {
        defaultTransmittanceStart = NULL;
    }

    if (options.isSet("transmittance-end")) {
        defaultTransmittanceEnd =
                qobject_cast<DynamicInputOption *>(options.get("transmittance-end"))->getInput();
    } else {
        defaultTransmittanceEnd = NULL;
    }

    if (options.isSet("reverse")) {
        defaultReverse = qobject_cast<DynamicBoolOption *>(options.get("reverse"))->getInput();
    } else {
        defaultReverse = NULL;
    }

    if (options.isSet("constants")) {
        defaultConstants =
                qobject_cast<DynamicCalibrationOption *>(options.get("constants"))->getInput();
    } else {
        defaultConstants = new DynamicPrimitive<Calibration>::Constant(
                Calibration(QList<double>() << 0.814 << 1.237));
    }

    restrictedInputs = false;

    if (options.isSet("absorption")) {
        restrictedInputs = true;

        Processing p;

        SequenceName::Component station;
        SequenceName::Component archive;
        SequenceName::Component suffix;
        SequenceName::Component trSuffix;
        SequenceName::Flavors flavors;

        CorrectionSet s;
        s.operateAbsorption = qobject_cast<DynamicSequenceSelectionOption *>(
                options.get("absorption"))->getOperator();

        for (const auto &name : s.operateAbsorption->getAllUnits()) {
            station = name.getStation();
            archive = name.getArchive();
            flavors = name.getFlavors();

            if (suffix.empty())
                suffix = Util::suffix(name.getVariable(), '_');

            if (Util::starts_with(name.getVariable(), "Ba"))
                trSuffix = name.getVariable().substr(2);
        }

        if (defaultReverse == NULL) {
            p.reverse = new DynamicPrimitive<bool>::Constant(false);
        } else {
            p.reverse = defaultReverse->clone();
        }
        p.operateFlags = new DynamicSequenceSelection::Single(
                SequenceName(station, archive, "F1_" + suffix, flavors));

        if (defaultTransmittanceStart == NULL) {
            s.transmittanceStart = new DynamicInput::Basic(
                    SequenceName(station, archive, "Ir" + trSuffix, flavors));
        } else {
            s.transmittanceStart = defaultTransmittanceStart->clone();
        }

        if (defaultTransmittanceEnd == NULL) {
            s.transmittanceEnd = new DynamicInput::Basic(
                    SequenceName(station, archive, "Ir" + trSuffix, {SequenceName::flavor_end}));
        } else {
            s.transmittanceEnd = defaultTransmittanceEnd->clone();
        }

        s.constants = defaultConstants->clone();

        p.operations.push_back(s);
        processing.push_back(p);
    }

    if (options.isSet("suffix")) {
        filterSuffixes = Util::set_from_qstring(
                qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->get());
    }
}


CorrWeiss::CorrWeiss()
{ Q_ASSERT(false); }

CorrWeiss::CorrWeiss(const ComponentOptions &options)
{
    handleOptions(options);
}

CorrWeiss::CorrWeiss(const ComponentOptions &options,
                     double start,
                     double end,
                     const QList<SequenceName> &inputs)
{
    Q_UNUSED(start);
    Q_UNUSED(end);
    handleOptions(options);

    for (QList<SequenceName>::const_iterator unit = inputs.constBegin(), endC = inputs.constEnd();
            unit != endC;
            ++unit) {
        CorrWeiss::unhandled(*unit, NULL);
    }
}

CorrWeiss::CorrWeiss(double start,
                     double end,
                     const SequenceName::Component &station,
                     const SequenceName::Component &archive,
                     const ValueSegment::Transfer &config)
{
    defaultReverse = NULL;
    defaultConstants = NULL;
    defaultTransmittanceStart = NULL;
    defaultTransmittanceEnd = NULL;
    restrictedInputs = true;

    std::unordered_set<Variant::PathElement::HashIndex> children;
    for (const auto &add : config) {
        Util::merge(add.read().toHash().keys(), children);
    }
    children.erase(Variant::PathElement::HashIndex());

    for (const auto &child : children) {
        Processing p;

        p.reverse = DynamicBoolOption::fromConfiguration(config, QString("%1/Reverse").arg(
                QString::fromStdString(child)), start, end);
        p.operateFlags = DynamicSequenceSelection::fromConfiguration(config,
                                                                     QString("%1/Flags").arg(
                                                                             QString::fromStdString(
                                                                                     child)), start,
                                                                     end);

        std::unordered_set<Variant::PathElement::HashIndex> operateChildren;
        for (const auto &add : config) {
            Util::merge(
                    add[QString("%1/Correct").arg(QString::fromStdString(child))].toHash().keys(),
                    operateChildren);
        }
        operateChildren.erase(Variant::PathElement::HashIndex());

        for (const auto &operate : operateChildren) {
            CorrectionSet s;

            s.operateAbsorption = DynamicSequenceSelection::fromConfiguration(config,
                                                                              QString("%1/Correct/%2/Absorption")
                                                                                      .arg(QString::fromStdString(
                                                                                              child),
                                                                                           QString::fromStdString(
                                                                                                   operate)),
                                                                              start, end);

            s.transmittanceStart = DynamicInput::fromConfiguration(config,
                                                                   QString("%1/Correct/%2/Transmittance/Start")
                                                                           .arg(QString::fromStdString(
                                                                                   child),
                                                                                QString::fromStdString(
                                                                                        operate)),
                                                                   start, end);
            s.transmittanceEnd = DynamicInput::fromConfiguration(config,
                                                                 QString("%1/Correct/%2/Transmittance/End")
                                                                         .arg(QString::fromStdString(
                                                                                 child),
                                                                              QString::fromStdString(
                                                                                      operate)),
                                                                 start, end);

            s.constants = DynamicCalibrationOption::fromConfiguration(config,
                                                                      QString("%1/Correct/%2/Constants")
                                                                              .arg(QString::fromStdString(
                                                                                      child),
                                                                                   QString::fromStdString(
                                                                                           operate)),
                                                                      start, end);

            s.operateAbsorption->registerExpected(station, archive);
            s.transmittanceStart->registerExpected(station, archive);
            s.transmittanceEnd->registerExpected(station, archive);

            p.operations.push_back(s);
        }

        p.operateFlags->registerExpected(station, archive);

        processing.push_back(p);
    }
}


CorrWeiss::~CorrWeiss()
{
    if (defaultConstants != NULL)
        delete defaultConstants;
    if (defaultReverse != NULL)
        delete defaultReverse;
    if (defaultTransmittanceStart != NULL)
        delete defaultTransmittanceStart;
    if (defaultTransmittanceEnd != NULL)
        delete defaultTransmittanceEnd;

    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        delete p->reverse;
        delete p->operateFlags;

        for (std::vector<CorrectionSet>::const_iterator s = p->operations.begin(),
                end = p->operations.end(); s != end; ++s) {
            delete s->operateAbsorption;
            delete s->transmittanceStart;
            delete s->transmittanceEnd;
            delete s->constants;
        }
    }
}

void CorrWeiss::unhandled(const SequenceName &unit,
                          SegmentProcessingStage::SequenceHandlerControl *control)
{
    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        for (int s = 0, max = (int) processing[id].operations.size(); s < max; s++) {
            if (processing[id].operations[s].transmittanceStart->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
            if (processing[id].operations[s].transmittanceEnd->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
        }
    }
    if (defaultTransmittanceStart != NULL &&
            defaultTransmittanceStart->registerInput(unit) &&
            control != NULL)
        control->deferHandling(unit);
    if (defaultTransmittanceEnd != NULL &&
            defaultTransmittanceEnd->registerInput(unit) &&
            control != NULL)
        control->deferHandling(unit);

    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        if (processing[id].operateFlags->registerInput(unit)) {
            if (control != NULL)
                control->filterUnit(unit, id);
            return;
        }
        for (int s = 0, max = (int) processing[id].operations.size(); s < max; s++) {
            if (processing[id].operations[s].operateAbsorption->registerInput(unit)) {
                if (control != NULL)
                    control->filterUnit(unit, id);
                return;
            }
        }
    }

    if (restrictedInputs)
        return;

    if (unit.hasFlavor(SequenceName::flavor_cover) || unit.hasFlavor(SequenceName::flavor_stats))
        return;

    if (!Util::starts_with(unit.getVariable(), "Ba")) {
        if (control) {
            if (Util::starts_with(unit.getVariable(), "Ir") ||
                    Util::starts_with(unit.getVariable(), "F1_")) {
                control->deferHandling(unit);
            }
        }
        return;
    }

    auto suffix = Util::suffix(unit.getVariable(), '_');
    if (suffix.empty())
        return;
    if (!filterSuffixes.empty() && !filterSuffixes.count(suffix))
        return;
    SequenceName suffixUnit(unit.getStation(), unit.getArchive(), suffix, unit.getFlavors());

    SequenceName transmittanceStartUnit
            (unit.getStation(), unit.getArchive(), "Ir" + unit.getVariable().substr(2),
             unit.getFlavors());
    SequenceName transmittanceEndUnit(transmittanceStartUnit.withFlavor(SequenceName::flavor_end));

    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        if (processing[id].suffix != suffixUnit)
            continue;

        CorrectionSet s;

        s.operateAbsorption = new DynamicSequenceSelection::Single(unit);

        if (defaultTransmittanceStart == NULL) {
            s.transmittanceStart = new DynamicInput::Basic(transmittanceStartUnit);
        } else {
            s.transmittanceStart = defaultTransmittanceStart->clone();
        }
        s.transmittanceStart->registerInput(unit);
        s.transmittanceStart->registerInput(transmittanceStartUnit);
        s.transmittanceStart->registerInput(transmittanceEndUnit);

        if (defaultTransmittanceEnd == NULL) {
            s.transmittanceEnd = new DynamicInput::Basic(transmittanceEndUnit);
        } else {
            s.transmittanceEnd = defaultTransmittanceEnd->clone();
        }
        s.transmittanceEnd->registerInput(unit);
        s.transmittanceEnd->registerInput(transmittanceStartUnit);
        s.transmittanceEnd->registerInput(transmittanceEndUnit);

        if (defaultConstants == NULL) {
            s.constants = new DynamicPrimitive<Calibration>::Constant(
                    Calibration(QList<double>() << 0.814 << 1.237));
        } else {
            s.constants = defaultConstants->clone();
        }

        processing[id].operations.push_back(s);

        if (control) {
            for (const auto &n : s.transmittanceStart->getUsedInputs()) {
                control->inputUnit(n, id);
            }
            for (const auto &n : s.transmittanceEnd->getUsedInputs()) {
                control->inputUnit(n, id);
            }

            control->filterUnit(unit, id);
        }
        return;
    }

    Processing p;
    p.suffix = suffixUnit;

    if (defaultReverse == NULL) {
        p.reverse = new DynamicPrimitive<bool>::Constant(false);
    } else {
        p.reverse = defaultReverse->clone();
    }

    SequenceName flagUnit
            (unit.getStation(), unit.getArchive(), "F1_" + suffix, unit.getFlavors());
    p.operateFlags = new DynamicSequenceSelection::Single(flagUnit);
    p.operateFlags->registerInput(unit);
    p.operateFlags->registerInput(flagUnit);
    p.operateFlags->registerInput(transmittanceStartUnit);
    p.operateFlags->registerInput(transmittanceEndUnit);

    CorrectionSet s;

    s.operateAbsorption = new DynamicSequenceSelection::Single(unit);

    if (defaultTransmittanceStart == NULL) {
        s.transmittanceStart = new DynamicInput::Basic(transmittanceStartUnit);
    } else {
        s.transmittanceStart = defaultTransmittanceStart->clone();
    }
    s.transmittanceStart->registerInput(unit);
    s.transmittanceStart->registerInput(flagUnit);
    s.transmittanceStart->registerInput(transmittanceStartUnit);
    s.transmittanceStart->registerInput(transmittanceEndUnit);

    if (defaultTransmittanceEnd == NULL) {
        s.transmittanceEnd = new DynamicInput::Basic(transmittanceEndUnit);
    } else {
        s.transmittanceEnd = defaultTransmittanceEnd->clone();
    }
    s.transmittanceEnd->registerInput(unit);
    s.transmittanceEnd->registerInput(flagUnit);
    s.transmittanceEnd->registerInput(transmittanceStartUnit);
    s.transmittanceEnd->registerInput(transmittanceEndUnit);

    if (defaultConstants == NULL) {
        s.constants = new DynamicPrimitive<Calibration>::Constant(
                Calibration(QList<double>() << 0.814 << 1.237));
    } else {
        s.constants = defaultConstants->clone();
    }

    p.operations.push_back(s);

    int id = (int) processing.size();
    processing.push_back(p);

    if (control) {
        for (const auto &n : s.transmittanceStart->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : s.transmittanceEnd->getUsedInputs()) {
            control->inputUnit(n, id);
        }

        control->filterUnit(unit, id);
        if (unit != flagUnit)
            control->filterUnit(flagUnit, id);
    }
}

static const Variant::Flag FLAG_NAME = "Weiss";

void CorrWeiss::process(int id, SequenceSegment &data)
{
    Processing *proc = &processing[id];

    bool reverse = proc->reverse->get(data);

    for (const auto &i : proc->operateFlags->get(data)) {
        if (!data.exists(i))
            continue;
        auto d = data[i];
        if (!d.exists())
            continue;
        if (reverse) {
            d.clearFlag(FLAG_NAME);
        } else {
            d.applyFlag(FLAG_NAME);
        }
    }

    for (std::vector<CorrectionSet>::const_iterator set = proc->operations.begin(),
            endSet = proc->operations.end(); set != endSet; ++set) {
        double startIr = set->transmittanceStart->get(data);
        double endIr = set->transmittanceEnd->get(data);
        double factor = FP::undefined();
        if (FP::defined(startIr)) {
            if (FP::defined(endIr)) {
                factor = (startIr + endIr) / 2.0;
            } else {
                factor = startIr;
            }
        } else if (FP::defined(endIr)) {
            factor = endIr;
        }

        factor = set->constants->get(data).apply(factor);
        if (!FP::defined(factor) || factor == 0.0) {
            for (const auto &i : set->operateAbsorption->get(data)) {
                if (!data.exists(i))
                    continue;
                data[i].setEmpty();
            }
            continue;
        }

        if (reverse)
            factor = 1.0 / factor;

        for (const auto &i : set->operateAbsorption->get(data)) {
            if (!data.exists(i))
                continue;
            Variant::Composite::applyInplace(data[i], [=](Variant::Write &d) {
                double v = d.toReal();
                if (FP::defined(v))
                    d.setReal(v / factor);
                else
                    Variant::Composite::invalidate(d);
            });
        }
    }
}

void CorrWeiss::processMeta(int id, SequenceSegment &data)
{
    Processing *p = &processing[id];

    Variant::Root meta;

    meta["By"].setString("corr_weiss");
    meta["At"].setDouble(Time::time());
    meta["Environment"].setString(Environment::describe());
    meta["Revision"].setString(Environment::revision());

    for (std::vector<CorrectionSet>::const_iterator set = p->operations.begin(),
            endSet = p->operations.end(); set != endSet; ++set) {
        for (auto i : set->operateAbsorption->get(data)) {
            i.setMeta();
            if (!data.exists(i))
                continue;

            Variant::Composite::fromCalibration(meta["Parameters"].hash("Constants"),
                                                set->constants->get(data));
            meta["Parameters"].hash("Reverse").setBool(p->reverse->get(data));
            data[i].metadata("Processing").toArray().after_back().set(meta);
            clearPropagatedSmoothing(data[i].metadata("Smoothing"));
        }
    }

    for (auto i : p->operateFlags->get(data)) {
        i.setMeta();
        if (!data.exists(i))
            continue;
        auto flagMeta = data[i].metadataSingleFlag(FLAG_NAME);
        flagMeta.hash("Origin").toArray().after_back().setString("corr_weiss");
        flagMeta.hash("Bits").setInt64(0x0080);
        flagMeta.hash("Description").setString("Weiss loading correction applied");
    }
}


SequenceName::Set CorrWeiss::requestedInputs()
{
    SequenceName::Set out;
    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        Util::merge(p->operateFlags->getAllUnits(), out);
        for (std::vector<CorrectionSet>::const_iterator s = p->operations.begin(),
                end = p->operations.end(); s != end; ++s) {
            Util::merge(s->transmittanceStart->getUsedInputs(), out);
            Util::merge(s->transmittanceEnd->getUsedInputs(), out);
            Util::merge(s->operateAbsorption->getAllUnits(), out);
        }
    }
    return out;
}

QSet<double> CorrWeiss::metadataBreaks(int id)
{
    Processing *p = &processing[id];
    QSet<double> result;
    Util::merge(p->operateFlags->getChangedPoints(), result);
    Util::merge(p->reverse->getChangedPoints(), result);
    for (std::vector<CorrectionSet>::const_iterator s = p->operations.begin(),
            end = p->operations.end(); s != end; ++s) {
        Util::merge(s->transmittanceStart->getChangedPoints(), result);
        Util::merge(s->transmittanceEnd->getChangedPoints(), result);
        Util::merge(s->constants->getChangedPoints(), result);
        Util::merge(s->operateAbsorption->getChangedPoints(), result);
    }
    return result;
}

CorrWeiss::CorrWeiss(QDataStream &stream)
{
    stream >> defaultReverse;
    stream >> defaultConstants;
    stream >> defaultTransmittanceStart;
    stream >> defaultTransmittanceEnd;
    stream >> filterSuffixes;
    stream >> restrictedInputs;

    quint32 n;
    stream >> n;
    for (int i = 0; i < (int) n; i++) {
        Processing p;
        stream >> p.reverse;
        stream >> p.operateFlags;
        stream >> p.suffix;

        quint32 m;
        stream >> m;
        for (int j = 0; j < (int) m; j++) {
            CorrectionSet s;
            stream >> s.operateAbsorption;
            stream >> s.transmittanceStart;
            stream >> s.transmittanceEnd;
            stream >> s.constants;

            p.operations.push_back(s);
        }

        processing.push_back(p);
    }
}

void CorrWeiss::serialize(QDataStream &stream)
{
    stream << defaultReverse;
    stream << defaultConstants;
    stream << defaultTransmittanceStart;
    stream << defaultTransmittanceEnd;
    stream << filterSuffixes;
    stream << restrictedInputs;

    quint32 n = (quint32) processing.size();
    stream << n;
    for (int i = 0; i < (int) n; i++) {
        stream << processing[i].reverse;
        stream << processing[i].operateFlags;
        stream << processing[i].suffix;

        quint32 m = (quint32) processing[i].operations.size();
        stream << m;
        for (int j = 0; j < (int) m; j++) {
            stream << processing[i].operations[j].operateAbsorption;
            stream << processing[i].operations[j].transmittanceStart;
            stream << processing[i].operations[j].transmittanceEnd;
            stream << processing[i].operations[j].constants;
        }
    }
}


QString CorrWeissComponent::getBasicSerializationName() const
{ return QString::fromLatin1("corr_weiss"); }

ComponentOptions CorrWeissComponent::getOptions()
{
    ComponentOptions options;

    options.add("transmittance-start", new DynamicInputOption(tr("transmittance-start", "name"),
                                                              tr("Input start transmittance"),
                                                              tr("This is the input start transmittance used in the correction.  "
                                                                 "When this option is set, this transmittance is used for all "
                                                                 "absorption channels.  Therefor, normally only a single absorption "
                                                                 "can be corrected while using this option."),
                                                              tr("Resolved automatically by instrument suffix")));

    options.add("transmittance-end", new DynamicInputOption(tr("transmittance-end", "name"),
                                                            tr("Input end transmittance"),
                                                            tr("This is the input end transmittance used in the correction.  "
                                                               "When this option is set, this transmittance is used for all "
                                                               "absorption channels.  Therefor, normally only a single absorption "
                                                               "can be corrected while using this option."),
                                                            tr("Resolved automatically by instrument suffix")));

    DynamicCalibrationOption *cal = new DynamicCalibrationOption(tr("constants", "name"),
                                                                 tr("Transmittance correction constants"),
                                                                 tr("This calibration set the transformation applied to the "
                                                                    "transmittances to generate the correction factor."),
                                                                 tr("0.814,1.237"));
    options.add("constants", cal);

    options.add("reverse",
                new DynamicBoolOption(tr("reverse", "name"), tr("Reverse the correction"),
                                      tr("When enabled, this causes the correction to be run in reverse.  "
                                         "This is used to back out the correction as applied by the "
                                         "PSAPs in their hardware."), QString()));

    options.add("absorption", new DynamicSequenceSelectionOption(tr("correct-absorption", "name"),
                                                                 tr("Absorption variables to correct"),
                                                                 tr("These are the variables that the correction is applied to.  "
                                                                    "Correcting using more than one transmittance for all absorptions "
                                                                    "is only supported using the suffixes option or by leaving this "
                                                                    "option unset (using all suffixes)."),
                                                                 tr("All absorptions")));

    options.add("suffix", new ComponentOptionInstrumentSuffixSet(tr("instruments", "name"),
                                                                 tr("Instrument suffixes to correct"),
                                                                 tr("These are the instrument suffixes to correct.  "
                                                                    "For example A11 would usually specifies the main absorption "
                                                                    "instrument.  This option is mutually exclusive with manual "
                                                                    "variable specification."),
                                                                 tr("All instrument suffixes")));
    options.exclude("transmittance-start", "suffix");
    options.exclude("transmittance-end", "suffix");
    options.exclude("absorption", "suffix");
    options.exclude("suffix", "transmittance-start");
    options.exclude("suffix", "transmittance-end");
    options.exclude("suffix", "absorption");

    return options;
}

QList<ComponentExample> CorrWeissComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Defaults", "default example name"),
                                     tr("This will correct all absorptions present in the input data.  "
                                        "The transmittance for each absorption is resolved by looking for "
                                        "one with the same end suffix as the absorption.  For example, "
                                        "BaG_A11 uses IrG_A11 as its transmittance.")));
    examples.back().setInputTypes(ComponentExample::Input_Absorption);

    options = getOptions();
    (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")))->add("A11");
    (qobject_cast<DynamicCalibrationOption *>(options.get("constants")))->set(
            Calibration(QList<double>() << 0.866 << 1.317));
    examples.append(ComponentExample(options, tr("Single instrument with alternate constants"),
                                     tr("This will correct all absorptions from the A11 instrument using "
                                        "the alternate constants A=0.866 and B=1.317.")));
    examples.back().setInputTypes(ComponentExample::Input_Absorption);

    options = getOptions();
    (qobject_cast<DynamicSequenceSelectionOption *>(options.get("absorption")))->set("", "",
                                                                                     "BaG_A11");
    (qobject_cast<DynamicInputOption *>(options.get("transmittance-start")))->set(
            SequenceMatch::OrderedLookup(SequenceMatch::Element::variable("IrR_A11")),
            Calibration());
    examples.append(ComponentExample(options, tr("Single variable with alternate transmittance"),
                                     tr("This will correct only the variable BaG_A11 and specifies an "
                                        "alternate transmittance (IrR_A11) to correct it with.")));
    examples.back().setInputTypes(ComponentExample::Input_Absorption);

    return examples;
}

SegmentProcessingStage *CorrWeissComponent::createBasicFilterDynamic(const ComponentOptions &options)
{ return new CorrWeiss(options); }

SegmentProcessingStage *CorrWeissComponent::createBasicFilterPredefined(const ComponentOptions &options,
                                                                        double start,
                                                                        double end,
                                                                        const QList<
                                                                                SequenceName> &inputs)
{ return new CorrWeiss(options, start, end, inputs); }

SegmentProcessingStage *CorrWeissComponent::createBasicFilterEditing(double start,
                                                                     double end,
                                                                     const SequenceName::Component &station,
                                                                     const SequenceName::Component &archive,
                                                                     const ValueSegment::Transfer &config)
{ return new CorrWeiss(start, end, station, archive, config); }

SegmentProcessingStage *CorrWeissComponent::deserializeBasicFilter(QDataStream &stream)
{ return new CorrWeiss(stream); }