/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CORRWEISS_H
#define CORRWEISS_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QObject>
#include <QList>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"

class CorrWeiss : public CPD3::Data::SegmentProcessingStage {
    class CorrectionSet {
    public:
        CPD3::Data::DynamicSequenceSelection *operateAbsorption;

        CPD3::Data::DynamicInput *transmittanceStart;
        CPD3::Data::DynamicInput *transmittanceEnd;
        CPD3::Data::DynamicCalibration *constants;

        CorrectionSet() : operateAbsorption(NULL), transmittanceStart(NULL),
                          transmittanceEnd(NULL),
                          constants(NULL)
        { }
    };

    class Processing {
    public:
        std::vector<CorrectionSet> operations;

        CPD3::Data::DynamicBool *reverse;
        CPD3::Data::DynamicSequenceSelection *operateFlags;

        CPD3::Data::SequenceName suffix;

        Processing() : operations(), reverse(NULL), operateFlags(NULL), suffix()
        { }
    };

    CPD3::Data::DynamicBool *defaultReverse;
    CPD3::Data::DynamicCalibration *defaultConstants;
    CPD3::Data::DynamicInput *defaultTransmittanceStart;
    CPD3::Data::DynamicInput *defaultTransmittanceEnd;
    CPD3::Data::SequenceName::ComponentSet filterSuffixes;

    bool restrictedInputs;

    void handleOptions(const CPD3::ComponentOptions &options);

    std::vector<Processing> processing;

public:
    CorrWeiss();

    CorrWeiss(const CPD3::ComponentOptions &options);

    CorrWeiss(const CPD3::ComponentOptions &options,
              double start, double end, const QList<CPD3::Data::SequenceName> &inputs);

    CorrWeiss(double start,
              double end,
              const CPD3::Data::SequenceName::Component &station,
              const CPD3::Data::SequenceName::Component &archive,
              const CPD3::Data::ValueSegment::Transfer &config);

    virtual ~CorrWeiss();

    void unhandled(const CPD3::Data::SequenceName &unit,
                   CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control) override;

    void process(int id, CPD3::Data::SequenceSegment &data) override;

    void processMeta(int id, CPD3::Data::SequenceSegment &data) override;

    CPD3::Data::SequenceName::Set requestedInputs() override;

    QSet<double> metadataBreaks(int id) override;

    CorrWeiss(QDataStream &stream);

    void serialize(QDataStream &stream) override;
};

class CorrWeissComponent
        : public QObject, virtual public CPD3::Data::SegmentProcessingStageComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ProcessingStageComponent
                         CPD3::Data::SegmentProcessingStageComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.corr_weiss"
                              FILE
                              "corr_weiss.json")

public:
    virtual QString getBasicSerializationName() const;

    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual CPD3::Data::SegmentProcessingStage
            *createBasicFilterDynamic(const CPD3::ComponentOptions &options);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterPredefined
            (const CPD3::ComponentOptions &options,
             double start,
             double end, const QList<CPD3::Data::SequenceName> &inputs);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterEditing(double start,
                                                                         double end,
                                                                         const CPD3::Data::SequenceName::Component &station,
                                                                         const CPD3::Data::SequenceName::Component &archive,
                                                                         const CPD3::Data::ValueSegment::Transfer &config);

    virtual CPD3::Data::SegmentProcessingStage *deserializeBasicFilter(QDataStream &stream);
};

#endif
