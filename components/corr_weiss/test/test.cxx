/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/segmentprocessingstage.hxx"
#include "core/component.hxx"

#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace QTest {

template<>
char *toString(const QSet<double> &set)
{
    QByteArray ba = "QSet<double>(";
    for (QSet<double>::const_iterator i = set.constBegin(); i != set.constEnd(); ++i) {
        if (i != set.constBegin()) ba += ", ";
        ba += QByteArray::number(*i);
    }
    ba += ")";
    return qstrdup(ba.data());
}
}

class TestController : public SegmentProcessingStage::SequenceHandlerControl {
public:
    class ID {
    public:
        ID() : filter(), input()
        { }

        ID(const QSet<SequenceName> &setFilter, const QSet<SequenceName> &setInput) : filter(
                setFilter), input(setInput)
        { }

        bool operator==(const ID &other) const
        { return filter == other.filter && input == other.input; }

        QSet<SequenceName> filter;
        QSet<SequenceName> input;
    };

    QHash<int, ID> contents;

    virtual void filterUnit(const SequenceName &unit, int targetID)
    {
        contents[targetID].input.remove(unit);
        contents[targetID].filter.insert(unit);
    }

    virtual void inputUnit(const SequenceName &unit, int targetID)
    {
        if (contents[targetID].filter.contains(unit))
            return;
        contents[targetID].input.insert(unit);
    }

    virtual bool isHandled(const SequenceName &unit)
    {
        for (QHash<int, ID>::const_iterator it = contents.constBegin();
                it != contents.constEnd();
                ++it) {
            if (it.value().filter.contains(unit))
                return true;
            if (it.value().input.contains(unit))
                return true;
        }
        return false;
    }
};

class TestComponent : public QObject {
Q_OBJECT

    SegmentProcessingStageComponent *component;

    static double correct(double Ba,
                          double Ir,
                          const Calibration &factor = Calibration(
                                  QVector<double>() << 0.814 << 1.237),
                          bool reverse = false)
    {
        if (!FP::defined(Ba) || !FP::defined(Ir))
            return FP::undefined();
        Ir = factor.apply(Ir);
        if (!FP::defined(Ir) || Ir == 0.0)
            return FP::undefined();
        if (reverse)
            return Ba * Ir;
        return Ba / Ir;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<SegmentProcessingStageComponent *>(
                ComponentLoader::create("corr_weiss"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("transmittance-start")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("transmittance-end")));
        QVERIFY(qobject_cast<DynamicBoolOption *>(options.get("reverse")));
        QVERIFY(qobject_cast<DynamicCalibrationOption *>(options.get("constants")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("absorption")));
        QVERIFY(qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")));

        QVERIFY(options.excluded().value("transmittance-start").contains("suffix"));
        QVERIFY(options.excluded().value("transmittance-end").contains("suffix"));
        QVERIFY(options.excluded().value("absorption").contains("suffix"));
        QVERIFY(options.excluded().value("suffix").contains("transmittance-start"));
        QVERIFY(options.excluded().value("suffix").contains("transmittance-end"));
        QVERIFY(options.excluded().value("suffix").contains("absorption"));
    }

    void dynamicDefaults()
    {
        ComponentOptions options(component->getOptions());
        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        SegmentProcessingStage *filter2;
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "Q_A11q"), &controller);
        QVERIFY(controller.contents.isEmpty());
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                filter2 = component->deserializeBasicFilter(stream);
                QVERIFY(stream.atEnd());
            }
            QVERIFY(filter2 != NULL);
            filter2->unhandled(SequenceName("brw", "raw", "Q_A11q"), &controller);
            QVERIFY(controller.contents.isEmpty());
            delete filter2;
        }

        SequenceName F1_A11_1("brw", "raw", "F1_A11");
        SequenceName IrB_A11_1("brw", "raw", "IrB_A11");
        SequenceName BaB_A11_1("brw", "raw", "BaB_A11");
        SequenceName IrG_A11_1("brw", "raw", "IrG_A11");
        SequenceName BaG_A11_1("brw", "raw", "BaG_A11");
        SequenceName F1_A11_2("mlo", "raw", "F1_A11");
        SequenceName IrB_A11_2("mlo", "raw", "IrB_A11");
        SequenceName BaB_A11_2("mlo", "raw", "BaB_A11");
        SequenceName Q_A11_2("mlo", "raw", "Q_A11");
        SequenceName F1_A12_1("brw", "raw", "F1_A12");
        SequenceName IrG_A12_1("brw", "raw", "IrG_A12");
        SequenceName BaG_A12_1("brw", "raw", "BaG_A12");

        filter->unhandled(IrB_A11_1, &controller);
        filter->unhandled(BaB_A11_1, &controller);
        filter->unhandled(F1_A11_1, &controller);
        filter->unhandled(BaG_A11_1, &controller);
        filter->unhandled(IrG_A11_1, &controller);
        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(
                                QSet<SequenceName>() << BaB_A11_1 << BaG_A11_1 << F1_A11_1,
                                QSet<SequenceName>() <<
                                        IrB_A11_1 <<
                                        IrB_A11_1.withFlavor("end") <<
                                        IrG_A11_1 <<
                                        IrG_A11_1.withFlavor("end")));
        QCOMPARE(idL.size(), 1);
        int A11_1 = idL.at(0);

        filter->unhandled(Q_A11_2, &controller);
        QCOMPARE(controller.contents.size(), 1);

        filter->unhandled(BaB_A11_2, &controller);
        QCOMPARE(controller.contents.size(), 2);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << BaB_A11_2 << F1_A11_2,
                                                 QSet<SequenceName>() <<
                                                         IrB_A11_2 <<
                                                         IrB_A11_2.withFlavor("end")));
        QCOMPARE(idL.size(), 1);
        int A11_2 = idL.at(0);

        filter->unhandled(F1_A12_1, &controller);
        filter->unhandled(BaG_A12_1, &controller);
        filter->unhandled(IrG_A12_1, &controller);
        QCOMPARE(controller.contents.size(), 3);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << BaG_A12_1 << F1_A12_1,
                                                 QSet<SequenceName>() <<
                                                         IrG_A12_1 <<
                                                         IrG_A12_1.withFlavor("end")));
        QCOMPARE(idL.size(), 1);
        int A12_1 = idL.at(0);

        data.setStart(15.0);
        data.setEnd(16.0);

        data.setValue(F1_A11_1, Variant::Root(Variant::Type::Flags));
        data.setValue(IrB_A11_1, Variant::Root(1.0));
        data.setValue(BaB_A11_1, Variant::Root(2.0));
        data.setValue(IrG_A11_1, Variant::Root(0.8));
        data.setValue(BaG_A11_1, Variant::Root(3.0));
        data.setValue(F1_A11_1, Variant::Root(Variant::Type::Flags));
        filter->process(A11_1, data);
        QCOMPARE(correct(2.0, 1.0), 0.97513408093612863);
        QCOMPARE(data.value(BaB_A11_1).toDouble(), correct(2.0, 1.0));
        QCOMPARE(data.value(BaG_A11_1).toDouble(), correct(3.0, 0.8));
        QVERIFY(data.value(F1_A11_1).testFlag("Weiss"));

        {
            QByteArray sd;
            {
                QDataStream stream(&sd, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&sd, QIODevice::ReadOnly);
                filter2 = component->deserializeBasicFilter(stream);
            }
            QVERIFY(filter2 != NULL);
            data.setValue(F1_A11_1, Variant::Root(Variant::Type::Flags));
            data.setValue(IrB_A11_1, Variant::Root(1.0));
            data.setValue(BaB_A11_1, Variant::Root(2.0));
            data.setValue(IrG_A11_1, Variant::Root(0.8));
            data.setValue(BaG_A11_1, Variant::Root(3.0));
            filter->process(A11_1, data);
            QCOMPARE(data.value(BaB_A11_1).toDouble(), correct(2.0, 1.0));
            QCOMPARE(data.value(BaG_A11_1).toDouble(), correct(3.0, 0.8));
            QVERIFY(data.value(F1_A11_1).testFlag("Weiss"));
            delete filter2;
        }

        data.setValue(F1_A11_1, Variant::Root(Variant::Type::Flags));
        data.setValue(IrB_A11_1, Variant::Root(0.9));
        data.setValue(BaB_A11_1, Variant::Root(2.5));
        data.setValue(IrG_A11_1, Variant::Root(0.95));
        data.setValue(BaG_A11_1, Variant::Root(3.5));
        data.setValue(F1_A11_1, Variant::Root(Variant::Type::Flags));
        filter->process(A11_1, data);
        QCOMPARE(data.value(BaB_A11_1).toDouble(), correct(2.5, 0.9));
        QCOMPARE(data.value(BaG_A11_1).toDouble(), correct(3.5, 0.95));
        QVERIFY(data.value(F1_A11_1).testFlag("Weiss"));

        data.setValue(IrB_A11_2, Variant::Root(0.5));
        data.setValue(BaB_A11_2, Variant::Root(100.0));
        data.setValue(Q_A11_2, Variant::Root(0.75));
        filter->process(A11_2, data);
        QCOMPARE(data.value(BaB_A11_2).toDouble(), correct(100.0, 0.5));
        QCOMPARE(data.value(Q_A11_2).toDouble(), 0.75);

        data.setValue(IrG_A12_1, Variant::Root(0.75));
        data.setValue(BaG_A12_1, Variant::Root(12.0));
        filter->process(A12_1, data);
        QCOMPARE(data.value(BaG_A12_1).toDouble(), correct(12.0, 0.75));

        data.setValue(F1_A11_1, Variant::Root(Variant::Type::Flags));
        data.setValue(IrB_A11_1, Variant::Root(0.9));
        data.setValue(BaB_A11_1, Variant::Root());
        data.setValue(IrG_A11_1, Variant::Root(0.95));
        data.setValue(BaG_A11_1, Variant::Root(3.5));
        data.setValue(F1_A11_1, Variant::Root(Variant::Type::Flags));
        filter->process(A11_1, data);
        QVERIFY(!FP::defined(data.value(BaB_A11_1).toDouble()));
        QCOMPARE(data.value(BaG_A11_1).toDouble(), correct(3.5, 0.95));
        QVERIFY(data.value(F1_A11_1).testFlag("Weiss"));

        data.setValue(F1_A11_1, Variant::Root(Variant::Type::Flags));
        data.setValue(IrB_A11_1, Variant::Root());
        data.setValue(BaB_A11_1, Variant::Root(0.5));
        data.setValue(IrG_A11_1, Variant::Root(0.95));
        data.setValue(BaG_A11_1, Variant::Root(3.5));
        data.setValue(F1_A11_1, Variant::Root(Variant::Type::Flags));
        filter->process(A11_1, data);
        QVERIFY(!FP::defined(data.value(BaB_A11_1).toDouble()));
        QCOMPARE(data.value(BaG_A11_1).toDouble(), correct(3.5, 0.95));
        QVERIFY(data.value(F1_A11_1).testFlag("Weiss"));

        data.setValue(F1_A11_1, Variant::Root(Variant::Type::Flags));
        data.setValue(IrB_A11_1, Variant::Root());
        data.setValue(BaB_A11_1, Variant::Root());
        data.setValue(IrG_A11_1, Variant::Root(0.95));
        data.setValue(BaG_A11_1, Variant::Root(3.5));
        data.setValue(F1_A11_1, Variant::Root(Variant::Type::Flags));
        filter->process(A11_1, data);
        QVERIFY(!FP::defined(data.value(BaB_A11_1).toDouble()));
        QCOMPARE(data.value(BaG_A11_1).toDouble(), correct(3.5, 0.95));
        QVERIFY(data.value(F1_A11_1).testFlag("Weiss"));

        data.setValue(F1_A11_1, Variant::Root(Variant::Type::Flags));
        data.setValue(IrB_A11_1, Variant::Root());
        data.setValue(BaB_A11_1, Variant::Root());
        data.setValue(IrG_A11_1, Variant::Root());
        data.setValue(BaG_A11_1, Variant::Root());
        data.setValue(F1_A11_1, Variant::Root(Variant::Type::Flags));
        filter->process(A11_1, data);
        QVERIFY(!FP::defined(data.value(BaB_A11_1).toDouble()));
        QVERIFY(!FP::defined(data.value(BaG_A11_1).toDouble()));
        QVERIFY(data.value(F1_A11_1).testFlag("Weiss"));

        data.setValue(BaB_A11_1.toMeta(), Variant::Root(Variant::Type::MetadataReal));
        data.setValue(BaG_A11_1.toMeta(), Variant::Root(Variant::Type::MetadataReal));
        data.setValue(F1_A11_1.toMeta(), Variant::Root(Variant::Type::MetadataFlags));

        filter->processMeta(A11_1, data);
        QVERIFY(data.value(F1_A11_1.toMeta()).metadataSingleFlag("Weiss").exists());
        QCOMPARE(data.value(BaB_A11_1.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QCOMPARE(data.value(BaG_A11_1.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);

        delete filter;
    }

    void dynamicOptions()
    {
        ComponentOptions options(component->getOptions());

        qobject_cast<DynamicInputOption *>(options.get("transmittance-start"))->set(0.7);
        qobject_cast<DynamicCalibrationOption *>(options.get("constants"))->set(
                Calibration(QList<double>() << 0.5 << 1.0));
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("absorption"))->set("", "",
                                                                                       "A.*");

        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "IrG_A11"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BaG_A11"), &controller);
        QVERIFY(controller.contents.isEmpty());

        SequenceName A_A11("brw", "raw", "A_A11");
        SequenceName A_A12("brw", "raw", "A_A12");
        filter->unhandled(A_A11, &controller);
        filter->unhandled(A_A12, &controller);

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << A_A11 << A_A12,
                                                 QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        int id = idL.at(0);

        data.setValue(A_A11, Variant::Root(1.0));
        data.setValue(A_A12, Variant::Root(2.0));
        filter->process(id, data);
        QCOMPARE(data.value(A_A11).toDouble(),
                 correct(1.0, 0.7, Calibration(QList<double>() << 0.5 << 1.0)));
        QCOMPARE(data.value(A_A12).toDouble(),
                 correct(2.0, 0.7, Calibration(QList<double>() << 0.5 << 1.0)));

        delete filter;


        options = component->getOptions();
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->add("A11");
        filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        controller.contents.clear();

        filter->unhandled(SequenceName("brw", "raw", "BaG_A12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BaR_A12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "IrG_A12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "F1_A12"), &controller);
        QVERIFY(controller.contents.isEmpty());

        SequenceName F1_A11("brw", "raw", "F1_A11");
        SequenceName BaG_A11("brw", "raw", "BaG_A11");
        SequenceName IrG_S11("brw", "raw", "IrG_A11");
        filter->unhandled(F1_A11, &controller);
        filter->unhandled(BaG_A11, &controller);
        filter->unhandled(IrG_S11, &controller);
        filter->unhandled(SequenceName("brw", "raw", "BsG_S12"), &controller);

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << F1_A11 << BaG_A11,
                                                 QSet<SequenceName>() <<
                                                         IrG_S11 <<
                                                         IrG_S11.withFlavor("end")));
        QCOMPARE(idL.size(), 1);
        id = idL.at(0);

        data.setValue(F1_A11, Variant::Root(Variant::Type::Flags));
        data.setValue(BaG_A11, Variant::Root(30.0));
        data.setValue(IrG_S11, Variant::Root(0.9));
        data.setValue(IrG_S11.withFlavor("end"), Variant::Root(0.7));
        filter->process(id, data);
        QCOMPARE(data.value(BaG_A11).toDouble(), correct(30.0, 0.8));
        QVERIFY(data.value(F1_A11).testFlag("Weiss"));

        delete filter;


        options = component->getOptions();
        qobject_cast<DynamicBoolOption *>(options.get("reverse"))->set(true);
        filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        controller.contents.clear();

        filter->unhandled(F1_A11, &controller);
        filter->unhandled(BaG_A11, &controller);
        filter->unhandled(IrG_S11, &controller);
        filter->unhandled(SequenceName("brw", "raw", "BsG_S12"), &controller);

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << F1_A11 << BaG_A11,
                                                 QSet<SequenceName>() <<
                                                         IrG_S11 <<
                                                         IrG_S11.withFlavor("end")));
        QCOMPARE(idL.size(), 1);
        id = idL.at(0);

        data.setValue(BaG_A11, Variant::Root(30.0));
        data.setValue(IrG_S11, Variant::Root(0.9));
        data.setValue(IrG_S11.withFlavor("end"), Variant::Root(0.7));
        data.setValue(F1_A11, Variant::Root(Variant::Flags{"Weiss"}));
        filter->process(id, data);
        QCOMPARE(data.value(BaG_A11).toDouble(),
                 correct(30.0, 0.8, Calibration(QVector<double>() << 0.814 << 1.237), true));
        QVERIFY(!data.value(F1_A11).testFlag("Weiss"));

        delete filter;
    }

    void predefined()
    {
        ComponentOptions options(component->getOptions());

        SequenceName F1_A11("brw", "raw", "F1_A11");
        SequenceName IrG_A11("brw", "raw", "IrG_A11");
        SequenceName BaG_A11("brw", "raw", "BaG_A11");
        QList<SequenceName> input;
        input << BaG_A11;

        SegmentProcessingStage *filter =
                component->createBasicFilterPredefined(options, FP::undefined(), FP::undefined(),
                                                       input);
        QVERIFY(filter != NULL);
        TestController controller;
        QList<int> idL;

        QCOMPARE(filter->requestedInputs(),
                 (SequenceName::Set{IrG_A11, IrG_A11.withFlavor("end"), BaG_A11, F1_A11}));

        delete filter;
    }

    void editing()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["A11/Flags"] = "::F1_A11:=";
        cv["A11/Correct/Blue/Absorption"] = "::BaB_A11:=";
        cv["A11/Correct/Blue/Transmittance/Start/Input"] = "::IrB_A11:=";
        cv["A11/Correct/Blue/Constants/#0"] = 0.814;
        cv["A11/Correct/Blue/Constants/#1"] = 1.237;
        cv["A11/Correct/Red/Absorption"] = "::BaR_A11:=";
        cv["A11/Correct/Red/Transmittance/End/Input"] = "::IrR_A11:=";
        cv["A11/Correct/Red/Constants/#0"] = 0.814;
        cv["A11/Correct/Red/Constants/#1"] = 1.237;
        cv["A12/Correct/Blue/Absorption"] = "::BaB_A12:=";
        cv["A12/Correct/Blue/Transmittance/Start/Input"] = "::IrB_A12:=";
        cv["A12/Correct/Blue/Constants/#0"] = 0.8;
        cv["A12/Correct/Blue/Constants/#1"] = 1.4;
        config.emplace_back(FP::undefined(), 13.0, cv);
        cv.write().setEmpty();
        cv["A11/Flags"] = "::F1_A11:=";
        cv["A11/Correct/Blue/Absorption"] = "::BaB_A11:=";
        cv["A11/Correct/Blue/Transmittance/End/Input"] = "::IrB_A11:=";
        cv["A11/Correct/Blue/Constants/#0"] = 0.5;
        cv["A11/Correct/Blue/Constants/#1"] = 1.5;
        config.emplace_back(13.0, FP::undefined(), cv);

        SegmentProcessingStage
                *filter = component->createBasicFilterEditing(12.0, 15.0, "brw", "raw", config);
        QVERIFY(filter != NULL);
        TestController controller;

        SequenceName F1_A11("brw", "raw", "F1_A11");
        SequenceName BaB_A11("brw", "raw", "BaB_A11");
        SequenceName BaR_A11("brw", "raw", "BaR_A11");
        SequenceName IrB_A11("brw", "raw", "IrB_A11");
        SequenceName IrR_A11("brw", "raw", "IrR_A11");
        SequenceName BaB_A12("brw", "raw", "BaB_A12");
        SequenceName IrB_A12("brw", "raw", "IrB_A12");

        QCOMPARE(filter->requestedInputs(),
                 (SequenceName::Set{F1_A11, BaB_A11, BaR_A11, IrB_A11, IrR_A11, BaB_A12, IrB_A12}));

        filter->unhandled(F1_A11, &controller);
        filter->unhandled(BaB_A11, &controller);
        filter->unhandled(BaR_A11, &controller);
        filter->unhandled(IrB_A11, &controller);
        filter->unhandled(IrR_A11, &controller);
        filter->unhandled(BaB_A12, &controller);
        filter->unhandled(IrB_A12, &controller);

        QList<int> idL = controller.contents
                                   .keys(TestController::ID(
                                           QSet<SequenceName>() << BaB_A11 << BaR_A11 << F1_A11,
                                           QSet<SequenceName>() << IrB_A11 << IrR_A11));
        QCOMPARE(idL.size(), 1);
        int A11 = idL.at(0);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << BaB_A12,
                                                 QSet<SequenceName>() << IrB_A12));
        QCOMPARE(idL.size(), 1);
        int A12 = idL.at(0);

        QCOMPARE(filter->metadataBreaks(A11), QSet<double>() << 13.0);
        QCOMPARE(filter->metadataBreaks(A12), QSet<double>() << 13.0);

        SequenceSegment data1;
        data1.setStart(12.0);
        data1.setEnd(13.0);

        data1.setValue(F1_A11, Variant::Root(Variant::Type::Flags));
        data1.setValue(BaB_A11, Variant::Root(1.0));
        data1.setValue(BaR_A11, Variant::Root(2.0));
        data1.setValue(IrB_A11, Variant::Root(0.9));
        data1.setValue(IrR_A11, Variant::Root(0.8));
        data1.setValue(F1_A11, Variant::Root(Variant::Type::Flags));
        filter->process(A11, data1);
        QCOMPARE(data1.value(BaB_A11).toDouble(), correct(1.0, 0.9));
        QCOMPARE(data1.value(BaR_A11).toDouble(), correct(2.0, 0.8));
        QVERIFY(data1.value(F1_A11).testFlag("Weiss"));

        data1.setValue(BaB_A12, Variant::Root(3.0));
        data1.setValue(IrB_A12, Variant::Root(0.7));
        filter->process(A12, data1);
        QCOMPARE(data1.value(BaB_A12).toDouble(),
                 correct(3.0, 0.7, Calibration(QList<double>() << 0.8 << 1.4)));

        data1.setValue(BaB_A11.toMeta(), Variant::Root(Variant::Type::MetadataReal));
        data1.setValue(BaR_A11.toMeta(), Variant::Root(Variant::Type::MetadataReal));
        data1.setValue(F1_A11.toMeta(), Variant::Root(Variant::Type::MetadataFlags));

        filter->processMeta(A11, data1);
        QCOMPARE(data1.value(BaB_A11.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QCOMPARE(data1.value(BaR_A11.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QVERIFY(data1.value(F1_A11.toMeta()).metadataSingleFlag("Weiss").exists());

        data1.setValue(BaB_A12.toMeta(), Variant::Root(Variant::Type::MetadataReal));

        filter->processMeta(A12, data1);
        QCOMPARE(data1.value(BaB_A12.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);


        SequenceSegment data2;
        data2.setStart(13.0);
        data2.setEnd(15.0);

        data2.setValue(BaB_A11, Variant::Root(4.0));
        data2.setValue(BaR_A11, Variant::Root(5.0));
        data2.setValue(IrB_A11, Variant::Root(0.6));
        data2.setValue(IrR_A11, Variant::Root(0.5));
        filter->process(A11, data2);
        QCOMPARE(data2.value(BaB_A11).toDouble(),
                 correct(4.0, 0.6, Calibration(QList<double>() << 0.5 << 1.5)));
        QCOMPARE(data2.value(BaR_A11).toDouble(), 5.0);
        QVERIFY(!data2.value(F1_A11).exists());

        data2.setValue(BaB_A12, Variant::Root(3.0));
        data2.setValue(IrB_A12, Variant::Root(0.75));
        filter->process(A12, data2);
        QCOMPARE(data2.value(BaB_A12).toDouble(), 3.0);

        data2.setValue(BaB_A11.toMeta(), Variant::Root(Variant::Type::MetadataReal));
        data2.setValue(F1_A11.toMeta(), Variant::Root(Variant::Type::MetadataFlags));

        filter->processMeta(A11, data2);
        QVERIFY(!data2.value(BaR_A11.toMeta()).exists());
        QCOMPARE(data2.value(BaB_A11.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
        QVERIFY(data2.value(F1_A11.toMeta()).metadataSingleFlag("Weiss").exists());

        filter->processMeta(A12, data2);
        QVERIFY(!data2.value(BaB_A12.toMeta()).exists());

        delete filter;
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
