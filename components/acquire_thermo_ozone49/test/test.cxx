/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;

    enum {
        Type_49c, Type_49_legacy, Type_49i,
    } type;
    int address;

    enum {
        Gas_Ambient, Gas_Zero, Gas_Level1,
    };
    int gas;

    bool legacy49c_noAverage;
    bool legacy49c_noLamp;
    bool legacy49c_noPressure;
    bool legacy49c_noTime;
    bool legacy49c_noO3Coeff;
    bool legacy49c_alternateProgramNo;
    bool legacy49c_slowProcessing;
    double legacy49c_slowDiscard;

    quint32 flags;

    QByteArray sumDelimiter;

    double concentration;
    double sampleT;
    double sampleP;

    double lampT;
    double lampV;
    double lampPCT;

    double cellAFlow;
    double cellBFlow;
    double cellAInt;
    double cellBInt;

    double ozonatorFlow;
    double ozonatorVoltage;
    double ozonatorTemperature;
    double ozonatorLevels[5];

    double ozoneBackground;
    double ozoneCoefficient;

    double mbVP24;
    double mbVP15;
    double mbVP5;
    double mbVP3;
    double mbVN3;

    double meVP24;
    double meVP15;
    double meVP5;
    double meVP3;
    double meVN15;

    double ioVP24;
    double ioVP5;
    double ioVP3;
    double ioVN3;

    ModelInstrument() : incoming(), outgoing(), type(Type_49i), address(0), gas(Gas_Ambient)
    {
        legacy49c_noAverage = false;
        legacy49c_noLamp = false;
        legacy49c_noPressure = false;
        legacy49c_noTime = false;
        legacy49c_noO3Coeff = false;
        legacy49c_alternateProgramNo = false;
        legacy49c_slowProcessing = false;
        legacy49c_slowDiscard = 0.0;

        flags = 0x10000000;
        sumDelimiter.clear();

        concentration = 52.0;
        sampleT = 23.5;
        sampleP = 740.0 * 1.333224;

        lampT = 45.0;
        lampV = 11.4;
        lampPCT = 91.0;

        cellAFlow = 0.8;
        cellBFlow = 0.9;
        cellAInt = 95231.0;
        cellBInt = 95236.0;

        ozonatorFlow = 0.1;
        ozonatorVoltage = 12.1;
        ozonatorTemperature = 68.7;
        ozonatorLevels[0] = 0.2;
        ozonatorLevels[1] = 0.4;
        ozonatorLevels[2] = 0.8;
        ozonatorLevels[3] = 0.8;
        ozonatorLevels[4] = 1.0;

        ozoneBackground = 52.0;
        ozoneCoefficient = 1.05;

        mbVP24 = 24.1;
        mbVP15 = 14.9;
        mbVP5 = 4.9;
        mbVP3 = 3.2;
        mbVN3 = -3.3;

        meVP24 = 23.9;
        meVP15 = 15.1;
        meVP5 = 5.1;
        meVP3 = 3.3;
        meVN15 = -15.1;

        ioVP24 = 24.0;
        ioVP5 = 5.0;
        ioVP3 = 3.4;
        ioVN3 = -3.2;
    }

    void outputFrame(const QByteArray &data)
    {
        outgoing.append(data);
        if (sumDelimiter.length() == 0) {
            outgoing.append('\r');
            return;
        }
        quint16 sum = 0;
        for (const char *add = data.constData(), *end = add + data.length(); add != end; ++add) {
            sum += (quint8) *add;
        }
        outgoing.append(sumDelimiter);
        outgoing.append("sum ");
        outgoing.append(QByteArray::number(sum, 16).rightJustified(4, '0'));
        outgoing.append('\r');
    }

    void writeSingleValue(const QByteArray &name,
                          double v,
                          int digits = 2,
                          const QByteArray &units = QByteArray(),
                          bool unitSpace = true)
    {
        QByteArray frame(name);
        frame.append(' ');
        frame.append(QByteArray::number(v, 'f', digits));
        if (!units.isEmpty()) {
            if (unitSpace)
                frame.append(' ');
            frame.append(units);
        }
        outputFrame(frame);
    }

    void advance(double seconds)
    {
        Q_UNUSED(seconds);

        legacy49c_slowDiscard -= seconds;
        if (legacy49c_slowDiscard > 0.0)
            incoming.clear();

        int idxCR = -1;
        while ((idxCR = incoming.indexOf('\r')) >= 0) {
            QByteArray line(incoming.mid(0, idxCR));
            incoming = incoming.mid(idxCR + 1);
            if (legacy49c_slowProcessing) {
                incoming.clear();
                legacy49c_slowDiscard = 0.2;
            }

            if (line.isEmpty())
                continue;

            if (address != 0) {
                if (line.at(0) != (char) (address + 128))
                    continue;
                line = line.mid(1);
            }
            line = line.trimmed().simplified().toLower();

            if (line == "o3") {
                writeSingleValue(line, concentration, 2, "ppb");
            } else if (line == "o3 bkg") {
                writeSingleValue(line, ozoneBackground, 2, "ppb");
            } else if (line == "o3 coef") {
                if (type != Type_49_legacy || !legacy49c_noO3Coeff) {
                    writeSingleValue(line, ozoneCoefficient, 3);
                }
            } else if (line == "lamp temp") {
                writeSingleValue(line, lampT, 1, "deg C");
            } else if (line == "cell a int") {
                writeSingleValue(line, cellAInt, 0, "Hz");
            } else if (line == "cell b int") {
                writeSingleValue(line, cellBInt, 0, "Hz");
            } else if (line == "lamp setting") {
                if (type != Type_49_legacy) {
                    writeSingleValue(line, lampPCT, 1, "%");
                } else if (!legacy49c_noLamp) {
                    writeSingleValue(line, lampPCT, 1, "%", false);
                }
            } else if (line == "flow a") {
                writeSingleValue(line, cellAFlow, 1, "l/m");
            } else if (line == "flow b") {
                writeSingleValue(line, cellBFlow, 1, "l/m");
            } else if (line == "flags") {
                QByteArray frame("flags ");
                frame.append(QByteArray::number(flags, 16).rightJustified(8, '0'));
                outputFrame(frame);
            } else if (type == Type_49c && line == "oz flow") {
                writeSingleValue(line, ozonatorFlow, 1, "l/m");
            } else if (type == Type_49_legacy && line == "oz flow") {
                outputFrame("oz flow bad cmd");
            } else if (type == Type_49i && line == "o3 lamp temp") {
                writeSingleValue(line, ozonatorTemperature, 1, "deg C");
            } else if (type == Type_49i && line == "lamp voltage bench") {
                writeSingleValue(line, lampV, 1, "V");
            } else if (type == Type_49i && line == "lamp voltage oz") {
                writeSingleValue(line, ozonatorVoltage, 1, "V");
            } else if (type == Type_49i && line == "diag volt mb") {
                QByteArray frame("diag volt mb ");
                frame.append(QByteArray::number(mbVP24, 'f', 1));
                frame.append(' ');
                frame.append(QByteArray::number(mbVP15, 'f', 1));
                frame.append(' ');
                frame.append(QByteArray::number(mbVP5, 'f', 1));
                frame.append(' ');
                frame.append(QByteArray::number(mbVP3, 'f', 1));
                frame.append(' ');
                frame.append(QByteArray::number(mbVN3, 'f', 1));
                outputFrame(frame);
            } else if (type == Type_49i && line == "diag volt mib") {
                QByteArray frame("diag volt mib ");
                frame.append(QByteArray::number(meVP24, 'f', 1));
                frame.append(' ');
                frame.append(QByteArray::number(meVP15, 'f', 1));
                frame.append(' ');
                frame.append(QByteArray::number(meVN15, 'f', 1));
                frame.append(' ');
                frame.append(QByteArray::number(meVP5, 'f', 1));
                frame.append(' ');
                frame.append(QByteArray::number(meVP3, 'f', 1));
                frame.append(' ');
                frame.append(QByteArray::number(meVP15, 'f', 1));
                outputFrame(frame);
            } else if (type == Type_49i && line == "diag volt iob") {
                QByteArray frame("diag volt iob ");
                frame.append(QByteArray::number(ioVP24, 'f', 1));
                frame.append(' ');
                frame.append(QByteArray::number(ioVP5, 'f', 1));
                frame.append(' ');
                frame.append(QByteArray::number(ioVP3, 'f', 1));
                frame.append(' ');
                frame.append(QByteArray::number(ioVN3, 'f', 1));
                outputFrame(frame);
            } else if (type == Type_49i && line == "l1") {
                writeSingleValue(line, ozonatorLevels[0], 1, "%");
            } else if (type == Type_49i && line == "l2") {
                writeSingleValue(line, ozonatorLevels[1], 1, "%");
            } else if (type == Type_49i && line == "l3") {
                writeSingleValue(line, ozonatorLevels[2], 1, "%");
            } else if (type == Type_49i && line == "l4") {
                writeSingleValue(line, ozonatorLevels[3], 1, "%");
            } else if (type == Type_49i && line == "l5") {
                writeSingleValue(line, ozonatorLevels[4], 1, "%");
            } else if (type == Type_49i && line == "host name") {
                outputFrame("host name ISERIES");
            } else if (line == "program no") {
                switch (type) {
                case Type_49c:
                    outputFrame("program no processor 49 00000100 link 49L 00000100");
                    break;
                case Type_49i:
                    outputFrame("program no iSeries 49i 01.00.01.074");
                    break;
                case Type_49_legacy:
                    if (legacy49c_alternateProgramNo) {
                        outputFrame("program no processor 49 000009 00 clink 49L000009 00");
                    }
                    break;
                }
            } else if (line == "bench temp") {
                QByteArray frame(line);
                frame.append(' ');
                if (!(flags & 0x08000000)) {
                    frame.append("000.0");
                } else {
                    frame.append(QByteArray::number(sampleT, 'f', 1));
                }
                frame.append(" deg C, actual ");
                frame.append(QByteArray::number(sampleT, 'f', 1));
                outputFrame(frame);
            } else if (line == "pres") {
                QByteArray frame(line);
                frame.append(' ');
                if (!(flags & 0x04000000)) {
                    frame.append("760.0");
                } else {
                    frame.append(QByteArray::number(sampleP / 1.333224, 'f', 1));
                }
                frame.append(" mm Hg, actual ");
                frame.append(QByteArray::number(sampleP / 1.333224, 'f', 1));
                outputFrame(frame);
            } else if (line == "set sample") {
                outputFrame("set sample ok");
                gas = Gas_Ambient;
            } else if (line == "set zero") {
                outputFrame("set zero ok");
                gas = Gas_Zero;
            } else if (line.startsWith("set level ")) {
                bool ok = false;
                int level = line.mid(10).toInt(&ok);
                if (ok && level >= 1 && level <= 5) {
                    QByteArray frame("set level ");
                    frame.append(QByteArray::number(level));
                    frame.append(" ok");
                    outputFrame(frame);
                    gas = Gas_Level1 + (level - 1);
                } else {
                    outputFrame("ERROR");
                }
            } else if (line == "set mode remote") {
                outputFrame("set mode remote ok");
                flags &= ~0x30000000;
                flags |= 0x10000000;
            } else if (line == "set mode local") {
                outputFrame("set mode local ok");
                flags &= ~0x30000000;
            } else if (line.startsWith("set gas unit ")) {
                outputFrame(line + " ok");
            } else if (line.startsWith("set avg time ")) {
                if (type != Type_49_legacy || !legacy49c_noAverage) {
                    bool ok = false;
                    int mode = line.mid(13).toInt(&ok);
                    if (ok && mode >= 0 && mode <= 8) {
                        QByteArray frame("set avg time ");
                        frame.append(QByteArray::number(mode));
                        frame.append(" ok");
                        outputFrame(frame);
                    } else {
                        outputFrame("ERROR");
                    }
                }
            } else if (line == "set temp comp on") {
                outputFrame("set temp comp on ok");
                flags |= 0x08000000;
            } else if (line == "set temp comp off") {
                outputFrame("set temp comp off ok");
                flags |= ~0x08000000;
            } else if (line == "set pres comp on") {
                if (type != Type_49_legacy || !legacy49c_noPressure) {
                    outputFrame("set pres comp on ok");
                    flags |= 0x04000000;
                }
            } else if (line == "set pres comp off") {
                if (type != Type_49_legacy || !legacy49c_noPressure) {
                    outputFrame("set pres comp off ok");
                    flags |= ~0x04000000;
                }
            } else if (line.startsWith("set time ")) {
                if (type != Type_49_legacy || !legacy49c_noTime) {
                    bool hourOk = false;
                    int hour = line.mid(9, 2).toInt(&hourOk);
                    bool minOk = false;
                    int min = line.mid(12, 2).toInt(&minOk);
                    bool secOk = false;
                    int sec = line.mid(15, 2).toInt(&secOk);
                    if (hourOk &&
                            minOk &&
                            secOk &&
                            hour >= 0 &&
                            hour <= 23 &&
                            min >= 0 &&
                            min <= 59 &&
                            sec >= 0 &&
                            sec <= 60 &&
                            line.at(11) == ':' &&
                            line.at(14) == ':') {
                        outputFrame(line + " ok");
                    } else {
                        outputFrame("ERROR");
                    }
                }
            } else if (line.startsWith("set date ")) {
                if (type != Type_49_legacy || !legacy49c_noTime) {
                    bool monOk = false;
                    int mon = line.mid(9, 2).toInt(&monOk);
                    bool dayOk = false;
                    int day = line.mid(12, 2).toInt(&dayOk);
                    bool yearOk = false;
                    int year = line.mid(15, 2).toInt(&yearOk);
                    if (monOk &&
                            dayOk &&
                            yearOk &&
                            mon >= 1 &&
                            mon <= 12 &&
                            day >= 1 &&
                            day <= 31 &&
                            year >= 0 &&
                            year <= 99 &&
                            line.at(11) == '-' &&
                            line.at(14) == '-') {
                        outputFrame(line + " ok");
                    } else {
                        outputFrame("ERROR");
                    }
                }
            } else if (type == Type_49i && line == "set pump on") {
                outputFrame("set pump on ok");
            } else if (type == Type_49i && line == "set pump off") {
                outputFrame("set pump off ok");
            } else if (line.startsWith("set format ")) {
                if (line.mid(11).toInt()) {
                    switch (type) {
                    case Type_49c:
                    case Type_49_legacy:

                        sumDelimiter = QByteArray(1, (char) 0x80);
                        break;
                    case Type_49i:
                        sumDelimiter = QByteArray(1, '\n');
                        break;
                    }
                    outputFrame("set format 01 ok");
                } else {
                    sumDelimiter.clear();
                    outputFrame("set format 00 ok");
                }
            } else {
                outputFrame("ERROR");
            }
        }
    }
};

class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;

    bool checkCommonMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("X", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("P", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Q1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Q2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("C1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("C2", Variant::Root(), QString(), time))
            return false;

        if (!stream.hasMeta("Tz", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Pz", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Xz", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZZERO", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZSPANCHECK", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkCommonRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("ZX", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Xzd", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZINSTFLAGS", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("PCT", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZSTATE", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool check49CMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("Q3", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool check49CRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("ZX", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool check49IMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("T3", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("V1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("V2", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool check49IRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("ZMBP24V", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZMBP15V", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZMBP5V", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZMBP3V", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZMBN3V", Variant::Root(), QString(), time))
            return false;

        if (!stream.hasMeta("ZMEP24V", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZMEP15V", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZMEP5V", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZMEP3V", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZMEN15V", Variant::Root(), QString(), time))
            return false;

        if (!stream.hasMeta("ZIOP24V", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZIOP5V", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZIOP3V", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZION3V", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkCommonContiguous(StreamCapture &stream)
    {

        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("X"))
            return false;
        if (!stream.checkContiguous("T1"))
            return false;
        if (!stream.checkContiguous("T2"))
            return false;
        if (!stream.checkContiguous("P"))
            return false;
        if (!stream.checkContiguous("Q1"))
            return false;
        if (!stream.checkContiguous("Q2"))
            return false;
        if (!stream.checkContiguous("C1"))
            return false;
        if (!stream.checkContiguous("C2"))
            return false;
        return true;
    }

    bool check49CContiguous(StreamCapture &stream)
    {
        if (!stream.checkContiguous("Q3"))
            return false;
        return true;
    }

    bool check49IContiguous(StreamCapture &stream)
    {
        if (!stream.checkContiguous("T3"))
            return false;
        if (!stream.checkContiguous("V1"))
            return false;
        if (!stream.checkContiguous("V2"))
            return false;
        return true;
    }

    bool checkCommonValues(StreamCapture &stream, const ModelInstrument &model,
                           double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("X", Variant::Root(model.concentration), time))
            return false;
        if (!stream.hasAnyMatchingValue("T1", Variant::Root(model.sampleT), time))
            return false;
        if (!stream.hasAnyMatchingValue("T2", Variant::Root(model.lampT), time))
            return false;
        if (!stream.hasAnyMatchingValue("P", Variant::Root(model.sampleP), time))
            return false;
        if (!stream.hasAnyMatchingValue("Q1", Variant::Root(model.cellAFlow), time))
            return false;
        if (!stream.hasAnyMatchingValue("Q2", Variant::Root(model.cellBFlow), time))
            return false;
        if (!stream.hasAnyMatchingValue("C1", Variant::Root(model.cellAInt), time))
            return false;
        if (!stream.hasAnyMatchingValue("C2", Variant::Root(model.cellBInt), time))
            return false;
        return true;
    }

    bool checkCommonRealtimeValues(StreamCapture &stream, const ModelInstrument &model,
                                   double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("ZX", Variant::Root(model.concentration), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZINSTFLAGS", Variant::Root((qint64) model.flags), time))
            return false;
        if (model.type != ModelInstrument::Type_49_legacy &&
                !stream.hasAnyMatchingValue("PCT", Variant::Root(model.lampPCT), time))
            return false;
        return true;
    }

    bool check49CValues(StreamCapture &stream, const ModelInstrument &model,
                        double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("Q3", Variant::Root(model.ozonatorFlow), time))
            return false;
        return true;
    }

    bool check49CRealtimeValues(StreamCapture &stream, const ModelInstrument &model,
                                double time = FP::undefined())
    {
        Q_UNUSED(stream);
        Q_UNUSED(model);
        Q_UNUSED(time);
        return true;
    }

    bool check49IValues(StreamCapture &stream, const ModelInstrument &model,
                        double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("T3", Variant::Root(model.ozonatorTemperature), time))
            return false;
        if (!stream.hasAnyMatchingValue("V1", Variant::Root(model.lampV), time))
            return false;
        if (!stream.hasAnyMatchingValue("V2", Variant::Root(model.ozonatorVoltage), time))
            return false;
        return true;
    }

    bool check49IRealtimeValues(StreamCapture &stream, const ModelInstrument &model,
                                double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("ZMBP24V", Variant::Root(model.mbVP24), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZMBP15V", Variant::Root(model.mbVP15), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZMBP5V", Variant::Root(model.mbVP5), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZMBP3V", Variant::Root(model.mbVP3), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZMBN3V", Variant::Root(model.mbVN3), time))
            return false;

        if (!stream.hasAnyMatchingValue("ZMEP24V", Variant::Root(model.meVP24), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZMEP15V", Variant::Root(model.meVP15), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZMEP5V", Variant::Root(model.meVP5), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZMEP3V", Variant::Root(model.meVP3), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZMEN15V", Variant::Root(model.meVN15), time))
            return false;

        if (!stream.hasAnyMatchingValue("ZIOP24V", Variant::Root(model.ioVP24), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZIOP5V", Variant::Root(model.ioVP5), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZIOP3V", Variant::Root(model.ioVP3), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZION3V", Variant::Root(model.ioVN3), time))
            return false;
        return true;
    }


    bool checkZero(const SequenceValue::Transfer &values, const ModelInstrument &model)
    {
        if (!StreamCapture::findValue(values, "raw", "ZZERO", Variant::Root()))
            return false;
        if (!StreamCapture::findValue(values, "raw", "Xz", Variant::Root(model.concentration)))
            return false;
        if (!StreamCapture::findValue(values, "raw", "Tz", Variant::Root(model.sampleT)))
            return false;
        if (!StreamCapture::findValue(values, "raw", "Pz", Variant::Root(model.sampleP)))
            return false;
        return true;
    }

    bool checkSpancheck(const SequenceValue::Transfer &values, const ModelInstrument &model)
    {
        Q_UNUSED(model);
        if (!StreamCapture::findValue(values, "raw", "ZSPANCHECK", Variant::Root()))
            return false;
        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_thermo_ozone49"));
        QVERIFY(component);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 20; i++) {
            control.externalControl("o3\r");
            control.externalControl("pres\r");
            control.externalControl("lamp temp\r");
            control.externalControl("bench temp\r");
            control.externalControl("cell a int\r");
            control.externalControl("cell b int\r");
            control.externalControl("flow b\r");
            control.externalControl("flow a\r");
            control.externalControl("flags\r");
            control.advance(1.0);
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        control.externalControl("lamp setting\r");
        control.advance(0.1);
        QTest::qSleep(50);

        for (int i = 0; i < 10; i++) {
            control.externalControl("o3\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("pres\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("lamp temp\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("bench temp\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("cell a int\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("cell b int\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("flow b\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("flow a\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("flags\r");
            control.advance(0.1);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkCommonContiguous(logging));

        QVERIFY(checkCommonMeta(logging));
        QVERIFY(checkCommonMeta(realtime));
        QVERIFY(checkCommonRealtimeMeta(realtime));

        QVERIFY(checkCommonValues(logging, instrument));
        QVERIFY(checkCommonValues(realtime, instrument));
        QVERIFY(checkCommonRealtimeValues(realtime, instrument));

    }

    void passiveAutoprobe49C()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.type = ModelInstrument::Type_49c;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 20; i++) {
            control.externalControl("o3\r");
            control.externalControl("pres\r");
            control.externalControl("lamp temp\r");
            control.externalControl("bench temp\r");
            control.externalControl("cell a int\r");
            control.externalControl("cell b int\r");
            control.externalControl("flow b\r");
            control.externalControl("flow a\r");
            control.externalControl("flags\r");
            control.externalControl("oz flow\r");
            control.advance(1.0);
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        control.externalControl("lamp setting\r");
        control.advance(0.1);
        QTest::qSleep(50);

        for (int i = 0; i < 10; i++) {
            control.externalControl("o3\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("pres\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("lamp temp\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("bench temp\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("cell a int\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("cell b int\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("flow b\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("flow a\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("flags\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("oz flow\r");
            control.advance(0.1);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkCommonContiguous(logging));
        QVERIFY(check49CContiguous(logging));

        QVERIFY(checkCommonMeta(logging));
        QVERIFY(checkCommonMeta(realtime));
        QVERIFY(checkCommonRealtimeMeta(realtime));
        QVERIFY(check49CMeta(logging));
        QVERIFY(check49CMeta(realtime));
        QVERIFY(check49CRealtimeMeta(realtime));

        QVERIFY(checkCommonValues(logging, instrument));
        QVERIFY(checkCommonValues(realtime, instrument));
        QVERIFY(checkCommonRealtimeValues(realtime, instrument));
        QVERIFY(check49CValues(logging, instrument));
        QVERIFY(check49CValues(realtime, instrument));
        QVERIFY(check49CRealtimeValues(realtime, instrument));

    }

    void passiveAutoprobe49I()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.type = ModelInstrument::Type_49i;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 20; i++) {
            control.externalControl("o3\r");
            control.externalControl("pres\r");
            control.externalControl("lamp temp\r");
            control.externalControl("bench temp\r");
            control.externalControl("cell a int\r");
            control.externalControl("cell b int\r");
            control.externalControl("flow b\r");
            control.externalControl("flow a\r");
            control.externalControl("flags\r");
            control.externalControl("o3 lamp temp\r");
            control.externalControl("lamp voltage bench\r");
            control.externalControl("lamp voltage oz\r");
            control.externalControl("diag volt iob\r");
            control.externalControl("diag volt mib\r");
            control.externalControl("diag volt mb\r");
            control.advance(1.0);
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        control.externalControl("lamp setting\r");
        control.advance(0.1);
        QTest::qSleep(50);

        for (int i = 0; i < 10; i++) {
            control.externalControl("o3\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("pres\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("lamp temp\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("bench temp\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("cell a int\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("cell b int\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("flow b\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("flow a\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("flags\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("o3 lamp temp\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("lamp voltage bench\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("lamp voltage oz\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("diag volt iob\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("diag volt mib\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("diag volt mb\r");
            control.advance(0.1);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkCommonContiguous(logging));
        QVERIFY(check49IContiguous(logging));

        QVERIFY(checkCommonMeta(logging));
        QVERIFY(checkCommonMeta(realtime));
        QVERIFY(checkCommonRealtimeMeta(realtime));
        QVERIFY(check49IMeta(logging));
        QVERIFY(check49IMeta(realtime));
        QVERIFY(check49IRealtimeMeta(realtime));

        QVERIFY(checkCommonValues(logging, instrument));
        QVERIFY(checkCommonValues(realtime, instrument));
        QVERIFY(checkCommonRealtimeValues(realtime, instrument));
        QVERIFY(check49IValues(logging, instrument));
        QVERIFY(check49IValues(realtime, instrument));
        QVERIFY(check49IRealtimeValues(realtime, instrument));

    }

    void passiveAutoprobe49ISums1()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.type = ModelInstrument::Type_49i;
        instrument.sumDelimiter = QByteArray(1, '\n');
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 20; i++) {
            control.externalControl("o3\r");
            control.externalControl("pres\r");
            control.externalControl("lamp temp\r");
            control.externalControl("bench temp\r");
            control.externalControl("cell a int\r");
            control.externalControl("cell b int\r");
            control.externalControl("flow b\r");
            control.externalControl("flow a\r");
            control.externalControl("flags\r");
            control.externalControl("o3 lamp temp\r");
            control.externalControl("lamp voltage bench\r");
            control.externalControl("lamp voltage oz\r");
            control.externalControl("diag volt iob\r");
            control.externalControl("diag volt mib\r");
            control.externalControl("diag volt mb\r");
            control.advance(1.0);
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        control.externalControl("lamp setting\r");
        control.advance(0.1);
        QTest::qSleep(50);

        for (int i = 0; i < 10; i++) {
            control.externalControl("o3\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("pres\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("lamp temp\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("bench temp\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("cell a int\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("cell b int\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("flow b\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("flow a\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("flags\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("o3 lamp temp\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("lamp voltage bench\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("lamp voltage oz\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("diag volt iob\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("diag volt mib\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("diag volt mb\r");
            control.advance(0.1);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkCommonContiguous(logging));
        QVERIFY(check49IContiguous(logging));

        QVERIFY(checkCommonMeta(logging));
        QVERIFY(checkCommonMeta(realtime));
        QVERIFY(checkCommonRealtimeMeta(realtime));
        QVERIFY(check49IMeta(logging));
        QVERIFY(check49IMeta(realtime));
        QVERIFY(check49IRealtimeMeta(realtime));

        QVERIFY(checkCommonValues(logging, instrument));
        QVERIFY(checkCommonValues(realtime, instrument));
        QVERIFY(checkCommonRealtimeValues(realtime, instrument));
        QVERIFY(check49IValues(logging, instrument));
        QVERIFY(check49IValues(realtime, instrument));
        QVERIFY(check49IRealtimeValues(realtime, instrument));

    }

    void passiveAutoprobe49ISums2()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.type = ModelInstrument::Type_49i;
        instrument.sumDelimiter = "*\n";
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 20; i++) {
            control.externalControl("o3\r");
            control.externalControl("pres\r");
            control.externalControl("lamp temp\r");
            control.externalControl("bench temp\r");
            control.externalControl("cell a int\r");
            control.externalControl("cell b int\r");
            control.externalControl("flow b\r");
            control.externalControl("flow a\r");
            control.externalControl("flags\r");
            control.externalControl("o3 lamp temp\r");
            control.externalControl("lamp voltage bench\r");
            control.externalControl("lamp voltage oz\r");
            control.externalControl("diag volt iob\r");
            control.externalControl("diag volt mib\r");
            control.externalControl("diag volt mb\r");
            control.advance(1.0);
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        control.externalControl("lamp setting\r");
        control.advance(0.1);
        QTest::qSleep(50);

        for (int i = 0; i < 10; i++) {
            control.externalControl("o3\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("pres\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("lamp temp\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("bench temp\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("cell a int\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("cell b int\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("flow b\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("flow a\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("flags\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("o3 lamp temp\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("lamp voltage bench\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("lamp voltage oz\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("diag volt iob\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("diag volt mib\r");
            control.advance(0.1);
            QTest::qSleep(50);

            control.externalControl("diag volt mb\r");
            control.advance(0.1);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkCommonContiguous(logging));
        QVERIFY(check49IContiguous(logging));

        QVERIFY(checkCommonMeta(logging));
        QVERIFY(checkCommonMeta(realtime));
        QVERIFY(checkCommonRealtimeMeta(realtime));
        QVERIFY(check49IMeta(logging));
        QVERIFY(check49IMeta(realtime));
        QVERIFY(check49IRealtimeMeta(realtime));

        QVERIFY(checkCommonValues(logging, instrument));
        QVERIFY(checkCommonValues(realtime, instrument));
        QVERIFY(checkCommonRealtimeValues(realtime, instrument));
        QVERIFY(check49IValues(logging, instrument));
        QVERIFY(check49IValues(realtime, instrument));
        QVERIFY(check49IRealtimeValues(realtime, instrument));

    }

    void interactiveAutoprobe49C()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["PollInterval"].setDouble(3.0);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.type = ModelInstrument::Type_49c;
        instrument.flags |= 0x1;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 200; i++) {
            control.advance(0.5);
            QTest::qSleep(25);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkCommonContiguous(logging));
        QVERIFY(check49CContiguous(logging));

        QVERIFY(checkCommonMeta(logging));
        QVERIFY(checkCommonMeta(realtime));
        QVERIFY(checkCommonRealtimeMeta(realtime));
        QVERIFY(check49CMeta(logging));
        QVERIFY(check49CMeta(realtime));
        QVERIFY(check49CRealtimeMeta(realtime));

        QVERIFY(checkCommonValues(logging, instrument));
        QVERIFY(checkCommonValues(realtime, instrument));
        QVERIFY(checkCommonRealtimeValues(realtime, instrument));
        QVERIFY(check49CValues(logging, instrument));
        QVERIFY(check49CValues(realtime, instrument));
        QVERIFY(check49CRealtimeValues(realtime, instrument));

    }

    void interactiveAutoprobe49CLegacy1()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["PollInterval"].setDouble(3.0);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.type = ModelInstrument::Type_49_legacy;
        instrument.legacy49c_noLamp = true;
        instrument.legacy49c_noAverage = true;
        instrument.sumDelimiter = QByteArray(1, (char) 0x80);
        instrument.flags |= 0x1;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 200; i++) {
            control.advance(0.5);
            QTest::qSleep(25);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkCommonContiguous(logging));
        QVERIFY(check49CContiguous(logging));

        QVERIFY(checkCommonMeta(logging));
        QVERIFY(checkCommonMeta(realtime));
        QVERIFY(checkCommonRealtimeMeta(realtime));
        QVERIFY(check49CRealtimeMeta(realtime));

        QVERIFY(checkCommonValues(logging, instrument));
        QVERIFY(checkCommonValues(realtime, instrument));
        QVERIFY(checkCommonRealtimeValues(realtime, instrument));
        QVERIFY(check49CRealtimeValues(realtime, instrument));

    }

    void interactiveAutoprobe49CLegacy2()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["PollInterval"].setDouble(3.0);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.type = ModelInstrument::Type_49_legacy;
        instrument.legacy49c_alternateProgramNo = true;
        instrument.legacy49c_slowProcessing = true;
        instrument.sumDelimiter = QByteArray(1, (char) 0x80);
        instrument.flags |= 0x1;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 200; i++) {
            control.advance(0.5);
            QTest::qSleep(25);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkCommonContiguous(logging));
        QVERIFY(check49CContiguous(logging));

        QVERIFY(checkCommonMeta(logging));
        QVERIFY(checkCommonMeta(realtime));
        QVERIFY(checkCommonRealtimeMeta(realtime));
        QVERIFY(check49CRealtimeMeta(realtime));

        QVERIFY(checkCommonValues(logging, instrument));
        QVERIFY(checkCommonValues(realtime, instrument));
        QVERIFY(checkCommonRealtimeValues(realtime, instrument));
        QVERIFY(check49CRealtimeValues(realtime, instrument));

    }

    void interactiveAutoprobe49I()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.type = ModelInstrument::Type_49i;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 300; i++) {
            control.advance(0.5);
            QTest::qSleep(10);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkCommonContiguous(logging));
        QVERIFY(check49IContiguous(logging));

        QVERIFY(checkCommonMeta(logging));
        QVERIFY(checkCommonMeta(realtime));
        QVERIFY(checkCommonRealtimeMeta(realtime));
        QVERIFY(check49IMeta(logging));
        QVERIFY(check49IMeta(realtime));
        QVERIFY(check49IRealtimeMeta(realtime));

        QVERIFY(checkCommonValues(logging, instrument));
        QVERIFY(checkCommonValues(realtime, instrument));
        QVERIFY(checkCommonRealtimeValues(realtime, instrument));
        QVERIFY(check49IValues(logging, instrument));
        QVERIFY(check49IValues(realtime, instrument));
        QVERIFY(check49IRealtimeValues(realtime, instrument));

    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.type = ModelInstrument::Type_49i;

        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        control.externalControl("lamp setting\r");
        control.advance(0.1);
        QTest::qSleep(50);

        for (int i = 0; i < 20; i++) {
            control.externalControl("o3\r");
            control.externalControl("pres\r");
            control.externalControl("lamp temp\r");
            control.externalControl("bench temp\r");
            control.externalControl("cell a int\r");
            control.externalControl("cell b int\r");
            control.externalControl("flow b\r");
            control.externalControl("flow a\r");
            control.externalControl("flags\r");
            control.externalControl("o3 lamp temp\r");
            control.externalControl("lamp voltage bench\r");
            control.externalControl("lamp voltage oz\r");
            control.externalControl("diag volt iob\r");
            control.externalControl("diag volt mib\r");
            control.externalControl("diag volt mb\r");
            control.advance(1.0);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkCommonContiguous(logging));
        QVERIFY(check49IContiguous(logging));

        QVERIFY(checkCommonMeta(logging));
        QVERIFY(checkCommonMeta(realtime));
        QVERIFY(checkCommonRealtimeMeta(realtime));
        QVERIFY(check49IMeta(logging));
        QVERIFY(check49IMeta(realtime));
        QVERIFY(check49IRealtimeMeta(realtime));

        QVERIFY(checkCommonValues(logging, instrument));
        QVERIFY(checkCommonValues(realtime, instrument));
        QVERIFY(checkCommonRealtimeValues(realtime, instrument));
        QVERIFY(check49IValues(logging, instrument));
        QVERIFY(check49IValues(realtime, instrument));
        QVERIFY(check49IRealtimeValues(realtime, instrument));

    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["PollInterval"].setDouble(0.0);
        cv["Address"].setInt64(49);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.type = ModelInstrument::Type_49c;
        instrument.address = 49;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);

        double checkTime = control.time() + 0.05;
        while (control.time() < 120.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        control.advance(0.1);
        checkTime = control.time() + 0.05;
        while (control.time() < 120.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);

        checkTime = control.time() + 0.05;
        while (control.time() < 120.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);

        checkTime = control.time() + 0.05;
        while (control.time() < 120.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);

        checkTime = control.time() + 0.05;
        while (control.time() < 120.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkCommonContiguous(logging));
        QVERIFY(check49CContiguous(logging));

        QVERIFY(checkCommonMeta(logging));
        QVERIFY(checkCommonMeta(realtime));
        QVERIFY(checkCommonRealtimeMeta(realtime));
        QVERIFY(check49CMeta(logging));
        QVERIFY(check49CMeta(realtime));
        QVERIFY(check49CRealtimeMeta(realtime));

        QVERIFY(checkCommonValues(logging, instrument));
        QVERIFY(checkCommonValues(realtime, instrument));
        QVERIFY(checkCommonRealtimeValues(realtime, instrument));
        QVERIFY(check49CValues(logging, instrument));
        QVERIFY(check49CValues(realtime, instrument));
        QVERIFY(check49CRealtimeValues(realtime, instrument));

    }

    void issueZero49I()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["PollInterval"].setDouble(0.0);
        cv["Zero/Flush"].setDouble(5.0);
        cv["Zero/MinimumSample"].setDouble(15.0);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);

        double checkTime = control.time();
        while (control.time() < 60.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        checkTime = control.time();
        control.advance(0.1);

        Variant::Write cmd = Variant::Write::empty();
        cmd["StartZero"].setBool(true);
        interface->incomingCommand(cmd);

        while (control.time() < 120.0) {
            control.advance(0.2);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Zero"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);
        QVERIFY(instrument.gas == ModelInstrument::Gas_Zero);

        checkTime = control.time();
        control.advance(0.1);
        while (control.time() < 180.0) {
            control.advance(0.2);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 180.0);
        QVERIFY(instrument.gas == ModelInstrument::Gas_Ambient);

        control.advance(0.1);
        checkTime = control.time();
        while (control.time() < 240.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("X", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);

        checkTime = control.time();
        while (control.time() < 240.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("X", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);

        checkTime = control.time();
        while (control.time() < 120.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("X", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkCommonMeta(logging));
        QVERIFY(checkCommonMeta(realtime));
        QVERIFY(checkCommonRealtimeMeta(realtime));

        QVERIFY(checkCommonValues(logging, instrument));
        QVERIFY(checkCommonValues(realtime, instrument));
        QVERIFY(checkCommonRealtimeValues(realtime, instrument));

        Util::append(persistent.values(), persistentValues);
        QVERIFY(checkZero(persistentValues, instrument));
        QVERIFY(checkZero(realtime.values(), instrument));

    }

    void issueZero49C()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["PollInterval"].setDouble(0.0);
        cv["Zero/Flush"].setDouble(5.0);
        cv["Zero/MinimumSample"].setDouble(15.0);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.type = ModelInstrument::Type_49c;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);

        double checkTime = control.time();
        while (control.time() < 60.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        checkTime = control.time();
        control.advance(0.1);

        Variant::Write cmd = Variant::Write::empty();
        cmd["StartZero"].setBool(true);
        interface->incomingCommand(cmd);

        while (control.time() < 120.0) {
            control.advance(0.2);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Zero"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);
        QVERIFY(instrument.gas == ModelInstrument::Gas_Zero);

        checkTime = control.time();
        control.advance(0.1);
        while (control.time() < 180.0) {
            control.advance(0.2);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 180.0);
        QVERIFY(instrument.gas == ModelInstrument::Gas_Ambient);

        control.advance(0.1);
        checkTime = control.time();
        while (control.time() < 240.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("X", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);

        checkTime = control.time();
        while (control.time() < 240.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("X", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);

        checkTime = control.time();
        while (control.time() < 120.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("X", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkCommonMeta(logging));
        QVERIFY(checkCommonMeta(realtime));
        QVERIFY(checkCommonRealtimeMeta(realtime));

        QVERIFY(checkCommonValues(logging, instrument));
        QVERIFY(checkCommonValues(realtime, instrument));
        QVERIFY(checkCommonRealtimeValues(realtime, instrument));

        Util::append(persistent.values(), persistentValues);
        QVERIFY(checkZero(persistentValues, instrument));
        QVERIFY(checkZero(realtime.values(), instrument));

    }

    void issueSpancheck49I()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["PollInterval"].setDouble(0.0);
        cv["Zero/Flush"].setDouble(5.0);
        cv["Zero/MinimumSample"].setDouble(15.0);
        cv["Spancheck/Flush"].setDouble(5.0);
        cv["Spancheck/MinimumSample"].setDouble(15.0);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);

        double checkTime = control.time();
        while (control.time() < 60.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        checkTime = control.time() + 0.05;
        control.advance(0.1);

        Variant::Write cmd = Variant::Write::empty();
        cmd["StartOzoneSpancheck"].setBool(true);
        interface->incomingCommand(cmd);

        while (control.time() < 120.0) {
            control.advance(0.2);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Spancheck"), checkTime))
                break;
            QTest::qSleep(50);
        }
        while (control.time() < 120.0) {
            if (instrument.gas == ModelInstrument::Gas_Zero)
                break;
            control.advance(0.2);
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);
        QVERIFY(instrument.gas == ModelInstrument::Gas_Zero);

        while (control.time() < 180.0) {
            control.advance(0.2);
            if (instrument.gas >= ModelInstrument::Gas_Level1)
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 180.0);
        QVERIFY(instrument.gas >= ModelInstrument::Gas_Level1);

        checkTime = control.time() + 0.05;
        control.advance(0.1);
        while (control.time() < 240.0) {
            control.advance(0.2);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);
        QVERIFY(instrument.gas == ModelInstrument::Gas_Ambient);

        control.advance(0.1);
        checkTime = control.time();
        while (control.time() < 240.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("X", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);

        checkTime = control.time() + 0.05;
        while (control.time() < 240.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("X", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);

        checkTime = control.time() + 0.05;
        while (control.time() < 240.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("X", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);

        checkTime = control.time() + 0.05;
        while (control.time() < 240.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("X", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkCommonMeta(logging));
        QVERIFY(checkCommonMeta(realtime));
        QVERIFY(checkCommonRealtimeMeta(realtime));

        QVERIFY(checkCommonValues(logging, instrument));
        QVERIFY(checkCommonValues(realtime, instrument));
        QVERIFY(checkCommonRealtimeValues(realtime, instrument));

        Util::append(persistent.values(), persistentValues);
        QVERIFY(checkZero(persistentValues, instrument));
        QVERIFY(checkZero(realtime.values(), instrument));
        QVERIFY(checkSpancheck(persistentValues, instrument));
        QVERIFY(checkSpancheck(realtime.values(), instrument));

    }

    void issueSpancheck49C()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["PollInterval"].setDouble(0.0);
        cv["Zero/Flush"].setDouble(5.0);
        cv["Zero/MinimumSample"].setDouble(15.0);
        cv["Spancheck/Flush"].setDouble(5.0);
        cv["Spancheck/MinimumSample"].setDouble(15.0);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.type = ModelInstrument::Type_49c;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);

        double checkTime = control.time();
        while (control.time() < 60.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        checkTime = control.time() + 0.05;
        control.advance(0.1);

        Variant::Write cmd = Variant::Write::empty();
        cmd["StartOzoneSpancheck"].setBool(true);
        interface->incomingCommand(cmd);

        while (control.time() < 120.0) {
            control.advance(0.2);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Spancheck"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);
        QVERIFY(instrument.gas == ModelInstrument::Gas_Zero);

        while (control.time() < 180.0) {
            control.advance(0.2);
            if (instrument.gas >= ModelInstrument::Gas_Level1)
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 180.0);
        QVERIFY(instrument.gas >= ModelInstrument::Gas_Level1);

        checkTime = control.time() + 0.05;
        control.advance(0.1);
        while (control.time() < 240.0) {
            control.advance(0.2);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);
        QVERIFY(instrument.gas == ModelInstrument::Gas_Ambient);

        control.advance(0.1);
        checkTime = control.time();
        while (control.time() < 240.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("X", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);

        checkTime = control.time() + 0.05;
        while (control.time() < 240.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("X", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);

        checkTime = control.time() + 0.05;
        while (control.time() < 240.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("X", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);

        checkTime = control.time() + 0.05;
        while (control.time() < 240.0) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("X", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkCommonMeta(logging));
        QVERIFY(checkCommonMeta(realtime));
        QVERIFY(checkCommonRealtimeMeta(realtime));

        QVERIFY(checkCommonValues(logging, instrument));
        QVERIFY(checkCommonValues(realtime, instrument));
        QVERIFY(checkCommonRealtimeValues(realtime, instrument));

        Util::append(persistent.values(), persistentValues);
        QVERIFY(checkZero(persistentValues, instrument));
        QVERIFY(checkZero(realtime.values(), instrument));
        QVERIFY(checkSpancheck(persistentValues, instrument));
        QVERIFY(checkSpancheck(realtime.values(), instrument));

    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
