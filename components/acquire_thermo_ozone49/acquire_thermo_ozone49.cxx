/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <cctype>
#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"
#include "algorithms/rayleigh.hxx"

#include "acquire_thermo_ozone49.hxx"


using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;
using namespace CPD3::Smoothing;

enum {
    FLAGS_ALARM_MASK = 0xC000FFFF,
    FLAGS_PRESSURE_COMPENSATION = 0x04000000,
    FLAGS_TEMPERATURE_COMPENSATION = 0x08000000,
};
static const char *instrumentFlagTranslation[32] = {"SampleTemperatureLowAlarm",    /* 0x00000001 */
                                                    "SampleTemperatureHighAlarm",   /* 0x00000002 */
                                                    "LampTemperatureLowAlarm",      /* 0x00000004 */
                                                    "LampTemperatureHighAlarm",     /* 0x00000008 */


                                                    "OzonatorTemperatureLowAlarm",  /* 0x00000010 */
                                                    "OzonatorTemperatureHighAlarm", /* 0x00000020 */
                                                    "PressureLowAlarm",             /* 0x00000040 */
                                                    "PressureHighAlarm",            /* 0x00000080 */

                                                    "FlowALowAlarm",                /* 0x00000100 */
                                                    "FlowAHighAlarm",               /* 0x00000200 */
                                                    "FlowBLowAlarm",                /* 0x00000400 */
                                                    "FlowBHighAlarm",               /* 0x00000800 */

                                                    "IntensityALowAlarm",           /* 0x00001000 */
                                                    "IntensityAHighAlarm",          /* 0x00002000 */
                                                    "IntensityBLowAlarm",           /* 0x00004000 */
                                                    "IntensityBHighAlarm",          /* 0x00008000 */

                                                    NULL,                           /* 0x00010000 - Unused */
                                                    NULL,                           /* 0x00020000 - Unused */
                                                    NULL,                           /* 0x00040000 - Gas units */
                                                    NULL,                           /* 0x00080000 - Gas units */

                                                    "OzonatorOn",                   /* 0x00100000 - Ozonator status */
                                                    NULL,                           /* 0x00200000 - Sample mode */
                                                    NULL,                           /* 0x00400000 - Sample mode */
                                                    NULL,                           /* 0x00800000 - Sample mode */

                                                    NULL,                           /* 0x01000000 - Unused */
                                                    NULL,                           /* 0x02000000 - Unused */
                                                    "PressureCompensation",         /* 0x04000000 - Pressure compensation */
                                                    "TemperatureCompensation",      /* 0x08000000 - Temperature compensation */

                                                    NULL,                           /* 0x10000000 - Access mode */
                                                    "ServiceMode",                  /* 0x20000000 - Access mode */
                                                    "OzoneLowAlarm",                /* 0x40000000 */
                                                    "OzoneHighAlarm",               /* 0x80000000 */
};

AcquireThermoOzone49::Configuration::Configuration() : start(FP::undefined()),
                                                       end(FP::undefined()),
                                                       address(0),
                                                       pollInterval(10.0),
                                                       commandDelay(FP::undefined()),
                                                       spancheckLevels()
{
    spancheckLevels.append(1);
    spancheckLevels.append(2);
}

AcquireThermoOzone49::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          address(other.address),
          pollInterval(other.pollInterval),
          commandDelay(other.commandDelay),
          spancheckLevels(other.spancheckLevels)
{ }

AcquireThermoOzone49::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s), end(e), address(0), pollInterval(10.0), spancheckLevels()
{
    spancheckLevels.append(1);
    spancheckLevels.append(2);

    setFromSegment(other);
}

AcquireThermoOzone49::Configuration::Configuration(const Configuration &under,
                                                   const ValueSegment &over,
                                                   double s,
                                                   double e) : start(s),
                                                               end(e),
                                                               address(under.address),
                                                               pollInterval(under.pollInterval),
                                                               commandDelay(under.commandDelay),
                                                               spancheckLevels(
                                                                       under.spancheckLevels)
{
    setFromSegment(over);
}

void AcquireThermoOzone49::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["Address"].exists()) {
        qint64 check = config["Address"].toInt64();
        if (INTEGER::defined(check) && check > 0 && check <= 126)
            address = (int) check;
        else
            address = 0;
    }
    {
        double check = config["PollInterval"].toDouble();
        if (FP::defined(check) && check >= 0.0)
            pollInterval = check;
    }

    if (config["CommandDelay"].exists())
        commandDelay = config["CommandDelay"].toDouble();

    switch (config["Spancheck/Levels"].getType()) {
    case Variant::Type::Empty:
        break;

    case Variant::Type::Array: {
        spancheckLevels.clear();
        for (auto add : config["Spancheck/Levels"].toArray()) {
            qint64 i = add.toInt64();
            if (!INTEGER::defined(i) || i <= 0)
                continue;
            spancheckLevels.append((int) i);
        }
        break;
    }

    default: {
        spancheckLevels.clear();

        qint64 i = config["Spancheck/Levels"].toInt64();
        if (INTEGER::defined(i) && i > 0) {
            for (int idx = 0; idx < (int) i; idx++) {
                spancheckLevels.append(idx + 1);
            }
        }
        break;
    }
    }
}

AcquireThermoOzone49::MeasurementData::MeasurementData(const ValueSegment::Transfer &config)
{
    Variant::Write defaultSmoothing = Variant::Write::empty();
    defaultSmoothing["Type"].setString("Forever");

    concentration = BaselineSmoother::fromConfiguration(defaultSmoothing, config,
                                                        "Smoothing/Concentration");
    sampleTemperature =
            BaselineSmoother::fromConfiguration(defaultSmoothing, config, "Smoothing/Temperature");
    samplePressure =
            BaselineSmoother::fromConfiguration(defaultSmoothing, config, "Smoothing/Pressure");
}

AcquireThermoOzone49::MeasurementData::MeasurementData(const MeasurementData &other)
        : concentration(other.concentration->clone()),
          sampleTemperature(other.sampleTemperature->clone()),
          samplePressure(other.samplePressure->clone())
{ }

AcquireThermoOzone49::MeasurementData::~MeasurementData()
{
    delete concentration;
    delete sampleTemperature;
    delete samplePressure;
}

void AcquireThermoOzone49::MeasurementData::reset()
{
    concentration->reset();
    sampleTemperature->reset();
    samplePressure->reset();
}

bool AcquireThermoOzone49::MeasurementData::stable() const
{
    return concentration->stable() && sampleTemperature->stable() && samplePressure->stable();
}

void AcquireThermoOzone49::MeasurementData::setResult(Variant::Write &target) const
{
    target.hash("X").setDouble(concentration->value());
    target.hash("Xs").setDouble(concentration->stabilityFactor());

    target.hash("T").setDouble(sampleTemperature->value());
    target.hash("Ts").setDouble(sampleTemperature->stabilityFactor());

    target.hash("P").setDouble(samplePressure->value());
    target.hash("Ps").setDouble(samplePressure->stabilityFactor());
}

void AcquireThermoOzone49::setMeasurementMetadata(Variant::Write &target) const
{
    target.metadataHashChild("X").metadataReal("Format").setString("000000.00");
    target.metadataHashChild("X").metadataReal("Units").setString("ppb");
    target.metadataHashChild("X").metadataReal("Description").setString("Ozone concentration");
    target.metadataHashChild("X").metadataReal("Background").setDouble(ozoneBackground);
    target.metadataHashChild("X").metadataReal("Coefficient").setDouble(ozoneCoefficient);
    target.metadataHashChild("Xs")
          .metadataReal("Description")
          .setString("Ozone concentration stability indicator");

    target.metadataHashChild("T").metadataReal("Format").setString("00.0");
    target.metadataHashChild("T").metadataReal("Units").setString("\xC2\xB0\x43");
    target.metadataHashChild("T").metadataReal("Description").setString("Sample temperature");
    target.metadataHashChild("Ts")
          .metadataReal("Description")
          .setString("Sample temperature stability indicator");

    target.metadataHashChild("P").metadataReal("Format").setString("0000.0");
    target.metadataHashChild("P").metadataReal("Units").setString("hPa");
    target.metadataHashChild("P").metadataReal("Description").setString("Sample pressure");
    target.metadataHashChild("Ps")
          .metadataReal("Description")
          .setString("Sample pressure stability indicator");
}

void AcquireThermoOzone49::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("Thermo");
    instrumentMeta["Model"].setString("49");

    zeroDetails.setUnit(SequenceName({}, "raw", "ZZERO"));
    spancheckDetails.setUnit(SequenceName({}, "raw", "ZSPANCHECK"));

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    realtimeStateUpdated = false;
    zeroRealtimeUpdated = false;
    spancheckRealtimeUpdated = false;

    zeroUpdated = false;
    spanUpdated = false;

    loggingMux.clear();
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
        streamAge[i] = 0;
    }

    ozoneBackground = FP::undefined();
    ozoneCoefficient = FP::undefined();

    temperatureCompensation = Compensation_Unknown;
    pressureCompensation = Compensation_Unknown;
    reportingT = FP::undefined();
    reportingP = FP::undefined();

    interfaceMode = Mode_Undetermined;
}

void AcquireThermoOzone49::createMeasurementData(const ValueSegment::Transfer &configData)
{
    DynamicTimeInterval::Variable *defaultTime = new DynamicTimeInterval::Variable;

    defaultTime->set(Variant::Root(2 * 60));
    zeroFlushTime =
            DynamicTimeInterval::fromConfiguration(configData, "Zero/Flush", FP::undefined(),
                                                   FP::undefined(), true, defaultTime);

    defaultTime->set(Variant::Root(12 * 60));
    zeroMinimumMeasureTime =
            DynamicTimeInterval::fromConfiguration(configData, "Zero/MinimumSample",
                                                   FP::undefined(), FP::undefined(), true,
                                                   defaultTime);

    defaultTime->set(Variant::Root("Undefined"));
    zeroMaximumMeasureTime =
            DynamicTimeInterval::fromConfiguration(configData, "Zero/MaximumSample",
                                                   FP::undefined(), FP::undefined(), true,
                                                   defaultTime);

    zeroMeasurement = new MeasurementData(ValueSegment::withPath(configData, "Zero"));


    defaultTime->set(Variant::Root(2 * 60));
    levelFlushTime =
            DynamicTimeInterval::fromConfiguration(configData, "Spancheck/Flush", FP::undefined(),
                                                   FP::undefined(), true, defaultTime);

    defaultTime->set(Variant::Root(12 * 60));
    levelMinimumMeasureTime =
            DynamicTimeInterval::fromConfiguration(configData, "Spancheck/MinimumSample",
                                                   FP::undefined(), FP::undefined(), true,
                                                   defaultTime);

    defaultTime->set(Variant::Root("Undefined"));
    levelMaximumMeasureTime =
            DynamicTimeInterval::fromConfiguration(configData, "Spancheck/MaximumSample",
                                                   FP::undefined(), FP::undefined(), true,
                                                   defaultTime);

    levelMeasurement.append(new MeasurementData(ValueSegment::withPath(configData, "Spancheck")));
    int max = 0;
    for (QList<Configuration>::const_iterator add = config.constBegin(), endAdd = config.constEnd();
            add != endAdd;
            ++add) {
        max = qMax(max, add->spancheckLevels.size());
    }
    for (int i = levelMeasurement.size(); i < max; i++) {
        levelMeasurement.append(new MeasurementData(*levelMeasurement.last()));
    }

    delete defaultTime;
}

AcquireThermoOzone49::AcquireThermoOzone49(const ValueSegment::Transfer &configData,
                                           const std::string &loggingContext) : FramedInstrument(
        "ozone49", loggingContext),
                                                                                autoprobeStatus(
                                                                                        AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                                autoprobeValidRecords(
                                                                                        0),
                                                                                responseState(
                                                                                        RESP_PASSIVE_INITIALIZE),
                                                                                responseStateIndex(
                                                                                        0),
                                                                                issuedCommand(),
                                                                                commandBacklog(),
                                                                                sampleState(
                                                                                        SAMPLE_RUN),
                                                                                sampleStateIndex(0),
                                                                                sampleStateBeginTime(
                                                                                        FP::undefined()),
                                                                                modeChangeRequest(
                                                                                        REQUEST_NONE),
                                                                                loggingMux(
                                                                                        LogStream_TOTAL)
{
    setDefaultInvalid();
    config.append(Configuration());

    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }

    createMeasurementData(configData);

    configurationChanged();
}

void AcquireThermoOzone49::setToAutoprobe()
{
    responseState = RESP_AUTOPROBE_PASSIVE_INITIALIZE;
    autoprobeValidRecords = 0;
}

void AcquireThermoOzone49::setToInteractive()
{ responseState = RESP_INTERACTIVE_INITIALIZE; }


ComponentOptions AcquireThermoOzone49Component::getBaseOptions()
{
    ComponentOptions options;

    return options;
}

AcquireThermoOzone49::AcquireThermoOzone49(const ComponentOptions &options,
                                           const std::string &loggingContext) : FramedInstrument(
        "ozone49", loggingContext),
                                                                                autoprobeStatus(
                                                                                        AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                                autoprobeValidRecords(
                                                                                        0),
                                                                                responseState(
                                                                                        RESP_PASSIVE_INITIALIZE),
                                                                                responseStateIndex(
                                                                                        0),
                                                                                issuedCommand(),
                                                                                commandBacklog(),
                                                                                sampleState(
                                                                                        SAMPLE_RUN),
                                                                                sampleStateIndex(0),
                                                                                sampleStateBeginTime(
                                                                                        FP::undefined()),
                                                                                modeChangeRequest(
                                                                                        REQUEST_NONE),
                                                                                loggingMux(
                                                                                        LogStream_TOTAL)
{
    Q_UNUSED(options);

    setDefaultInvalid();
    config.append(Configuration());
    createMeasurementData();

    configurationChanged();
}

AcquireThermoOzone49::~AcquireThermoOzone49()
{
    delete zeroMeasurement;
    qDeleteAll(levelMeasurement);

    delete zeroFlushTime;
    delete zeroMinimumMeasureTime;
    delete zeroMaximumMeasureTime;
    delete levelFlushTime;
    delete levelMinimumMeasureTime;
    delete levelMaximumMeasureTime;
}

void AcquireThermoOzone49::logValue(double frameTime,
                                    SequenceName::Component name,
                                    Variant::Root value,
                                    int streamID)
{
    double startTime = streamTime[streamID];
    double endTime = frameTime;
    streamTime[streamID] = endTime;
    streamAge[streamID]++;

    Q_ASSERT(FP::defined(endTime));
    if (!FP::defined(startTime) || startTime == endTime || !loggingEgress) {
        if (loggingEgress)
            streamTime[streamID] = endTime;
        if (!realtimeEgress)
            return;
        realtimeEgress->emplaceData(SequenceName({}, "raw", std::move(name)), std::move(value),
                                    frameTime, frameTime + 0.5);
        return;
    }

    SequenceValue
            dv(SequenceName({}, "raw", std::move(name)), std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        loggingMux.incoming(streamID, std::move(dv), loggingEgress);
        return;
    }

    loggingMux.incoming(streamID, dv, loggingEgress);
    dv.setStart(endTime);
    dv.setEnd(endTime + 1.0);
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireThermoOzone49::realtimeValue(double time,
                                         SequenceName::Component name,
                                         Variant::Root value)
{
    if (!realtimeEgress) return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

SequenceValue::Transfer AcquireThermoOzone49::buildLogMeta(double time) const
{
    Q_ASSERT(!config.isEmpty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_thermo_ozone49");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["Model"].set(instrumentMeta["Model"]);
    switch (interfaceMode) {
    case Mode_49c_legacy:
        if (instrumentMeta["FirmwareVersion"].exists())
            processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
        if (instrumentMeta["LinkVersion"].exists())
            processing["LinkVersion"].set(instrumentMeta["LinkVersion"]);
        break;
    case Mode_49c:

        processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
        processing["LinkVersion"].set(instrumentMeta["LinkVersion"]);
        break;
    case Mode_49i:

        processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
        processing["HostName"].set(instrumentMeta["HostName"]);
        break;
    default:
        break;
    }

    result.emplace_back(SequenceName({}, "raw_meta", "X"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000000.00");
    result.back().write().metadataReal("Units").setString("ppb");
    result.back().write().metadataReal("Description").setString("Ozone concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Background").setDouble(ozoneBackground);
    result.back().write().metadataReal("Coefficient").setDouble(ozoneCoefficient);
    if (FP::defined(reportingT) && !compensationStateOn(temperatureCompensation)) {
        result.back().write().metadataReal("ReportT").setDouble(reportingT);
    }
    if (FP::defined(reportingP) && !compensationStateOn(pressureCompensation)) {
        result.back().write().metadataReal("ReportP").setDouble(reportingP);
    }
    /* No realtime since because there's a duplicate for that so it shows
     * values during zeros/spanchecks */


    result.emplace_back(SequenceName({}, "raw_meta", "T1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Sample temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);

    result.emplace_back(SequenceName({}, "raw_meta", "T2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Lamp temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Lamp"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);

    if (interfaceMode == Mode_49i) {
        result.emplace_back(SequenceName({}, "raw_meta", "T3"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("00.0");
        result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
        result.back().write().metadataReal("Description").setString("Ozonator lamp temperature");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Name")
              .setString(QObject::tr("Ozonator"));
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);
    }

    result.emplace_back(SequenceName({}, "raw_meta", "P"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Sample pressure");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);

    result.emplace_back(SequenceName({}, "raw_meta", "Q1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Cell A flow");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Cell A"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);

    result.emplace_back(SequenceName({}, "raw_meta", "Q2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Cell B flow");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Cell B"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);

    if (interfaceMode == Mode_49c) {
        result.emplace_back(SequenceName({}, "raw_meta", "Q3"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("00.000");
        result.back().write().metadataReal("Units").setString("lpm");
        result.back().write().metadataReal("Description").setString("Ozonator flow");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Name")
              .setString(QObject::tr("Ozonator"));
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);
    }


    result.emplace_back(SequenceName({}, "raw_meta", "C1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Cell A lamp intensity");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Cell A"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);

    result.emplace_back(SequenceName({}, "raw_meta", "C2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Cell B lamp intensity");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Cell B"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);

    if (interfaceMode == Mode_49i) {
        result.emplace_back(SequenceName({}, "raw_meta", "V1"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("00.0");
        result.back().write().metadataReal("Units").setString("V");
        result.back().write().metadataReal("Description").setString("Lamp voltage");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Lamp"));
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(10);

        result.emplace_back(SequenceName({}, "raw_meta", "V2"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("00.0");
        result.back().write().metadataReal("Units").setString("V");
        result.back().write().metadataReal("Description").setString("Ozonator lamp voltage");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Name")
              .setString(QObject::tr("Ozonator"));
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(10);
    }


    result.emplace_back(SequenceName({}, "raw_meta", "Tz"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Sample temperature during zero");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataReal("Realtime").hash("Hide").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "Pz"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Sample pressure during zero");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataReal("Realtime").hash("Hide").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "Xz"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000000.00");
    result.back().write().metadataReal("Units").setString("ppb");
    result.back().write().metadataReal("Description").setString("Zero ozone concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Background").setDouble(ozoneBackground);
    result.back().write().metadataReal("Coefficient").setDouble(ozoneCoefficient);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Zero"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "ZZERO"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataHash("Description").setString("Zero check detailed results");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
    result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataHash("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataHash("Realtime").hash("Hide").setBool(true);
    setMeasurementMetadata(result.back().write());

    result.emplace_back(SequenceName({}, "raw_meta", "ZSPANCHECK"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataHash("Description").setString("Span check detailed results");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
    result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataHash("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataHash("Realtime").hash("Hide").setBool(true);

    result.back()
          .write()
          .metadataHashChild("Zero")
          .metadataHash("Description")
          .setString("Zero results performed as part of the spancheck");
    setMeasurementMetadata(result.back().write().metadataHashChild("Zero"));

    result.back()
          .write()
          .metadataHashChild("Levels")
          .metadataArray("Description")
          .setString("Individual ozonator level results");
    result.back()
          .write()
          .metadataHashChild("Levels")
          .metadataArray("Count")
          .setInt64(config.first().spancheckLevels.size());
    setMeasurementMetadata(
            result.back().write().metadataHashChild("Levels").metadataArray("Children"));
    for (auto level : config.first().spancheckLevels) {
        result.back()
              .write()
              .metadataHashChild("Levels")
              .metadataArray("Levels")
              .toArray()
              .after_back()
              .setInteger(level);
        result.back()
              .write()
              .metadataHashChild("Levels")
              .metadataArray("Drive")
              .toArray()
              .after_back()
              .setReal(ozonatorDrive.value(level, FP::undefined()));
    }


    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataSingleFlag("Blank")
          .hash("Description")
          .setString("Data removed in blank period");
    result.back()
          .write()
          .metadataSingleFlag("Blank")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x000100000000));
    result.back()
          .write()
          .metadataSingleFlag("Blank")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49");

    result.back()
          .write()
          .metadataSingleFlag("Zero")
          .hash("Description")
          .setString("Zero in progress");
    result.back()
          .write()
          .metadataSingleFlag("Zero")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x000200000000));
    result.back()
          .write()
          .metadataSingleFlag("Zero")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49");

    result.back()
          .write()
          .metadataSingleFlag("Spancheck")
          .hash("Description")
          .setString("Spancheck in progress");
    result.back()
          .write()
          .metadataSingleFlag("Spancheck")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49");


    result.back()
          .write()
          .metadataSingleFlag("STP")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49");
    result.back().write().metadataSingleFlag("STP").hash("Bits").setInt64(0x0200);
    result.back()
          .write()
          .metadataSingleFlag("STP")
          .hash("Description")
          .setString("STP correction applied");

    result.back()
          .write()
          .metadataSingleFlag("TemperatureCompensation")
          .hash("Description")
          .setString("Temperature compensation enabled");
    result.back()
          .write()
          .metadataSingleFlag("TemperatureCompensation")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x080000000000));
    result.back()
          .write()
          .metadataSingleFlag("TemperatureCompensation")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49");

    result.back()
          .write()
          .metadataSingleFlag("PressureCompensation")
          .hash("Description")
          .setString("Pressure compensation enabled");
    result.back()
          .write()
          .metadataSingleFlag("PressureCompensation")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x040000000000));
    result.back()
          .write()
          .metadataSingleFlag("PressureCompensation")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49");

    result.back()
          .write()
          .metadataSingleFlag("OzonatorOn")
          .hash("Description")
          .setString("Ozonator lamp turned on");
    result.back()
          .write()
          .metadataSingleFlag("OzonatorOn")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x001000000000));
    result.back()
          .write()
          .metadataSingleFlag("OzonatorOn")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49");


    result.back()
          .write()
          .metadataSingleFlag("SampleTemperatureLowAlarm")
          .hash("Description")
          .setString("Sample temperature exceeded lower bound alarm threshold");
    result.back()
          .write()
          .metadataSingleFlag("SampleTemperatureLowAlarm")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x000000010000));
    result.back()
          .write()
          .metadataSingleFlag("SampleTemperatureLowAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49");

    result.back()
          .write()
          .metadataSingleFlag("SampleTemperatureHighAlarm")
          .hash("Description")
          .setString("Sample temperature exceeded upper bound alarm threshold");
    result.back()
          .write()
          .metadataSingleFlag("SampleTemperatureHighAlarm")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x000000020000));
    result.back()
          .write()
          .metadataSingleFlag("SampleTemperatureHighAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49");

    result.back()
          .write()
          .metadataSingleFlag("LampTemperatureLowAlarm")
          .hash("Description")
          .setString("Lamp temperature exceeded lower bound alarm threshold");
    result.back()
          .write()
          .metadataSingleFlag("LampTemperatureLowAlarm")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x000000040000));
    result.back()
          .write()
          .metadataSingleFlag("LampTemperatureLowAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49");

    result.back()
          .write()
          .metadataSingleFlag("LampTemperatureHighAlarm")
          .hash("Description")
          .setString("Lamp temperature exceeded upper bound alarm threshold");
    result.back()
          .write()
          .metadataSingleFlag("LampTemperatureHighAlarm")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x000000080000));
    result.back()
          .write()
          .metadataSingleFlag("LampTemperatureHighAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49");


    result.back()
          .write()
          .metadataSingleFlag("OzonatorTemperatureLowAlarm")
          .hash("Description")
          .setString("Ozonator lamp temperature exceeded lower bound alarm threshold");
    result.back()
          .write()
          .metadataSingleFlag("OzonatorTemperatureLowAlarm")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x000000100000));
    result.back()
          .write()
          .metadataSingleFlag("OzonatorTemperatureLowAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49");

    result.back()
          .write()
          .metadataSingleFlag("OzonatorTemperatureHighAlarm")
          .hash("Description")
          .setString("Ozonator lamp temperature exceeded upper bound alarm threshold");
    result.back()
          .write()
          .metadataSingleFlag("OzonatorTemperatureHighAlarm")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x000000200000));
    result.back()
          .write()
          .metadataSingleFlag("OzonatorTemperatureHighAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49");

    result.back()
          .write()
          .metadataSingleFlag("PressureLowAlarm")
          .hash("Description")
          .setString("Sample pressure exceeded lower bound alarm threshold");
    result.back()
          .write()
          .metadataSingleFlag("PressureLowAlarm")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x000000400000));
    result.back()
          .write()
          .metadataSingleFlag("PressureLowAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49");

    result.back()
          .write()
          .metadataSingleFlag("PressureHighAlarm")
          .hash("Description")
          .setString("Sample pressure exceeded upper bound alarm threshold");
    result.back()
          .write()
          .metadataSingleFlag("PressureHighAlarm")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x000000800000));
    result.back()
          .write()
          .metadataSingleFlag("PressureHighAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49");


    result.back()
          .write()
          .metadataSingleFlag("FlowALowAlarm")
          .hash("Description")
          .setString("Cell A flow rate exceeded lower bound alarm threshold");
    result.back()
          .write()
          .metadataSingleFlag("FlowALowAlarm")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x000001000000));
    result.back()
          .write()
          .metadataSingleFlag("FlowALowAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49");

    result.back()
          .write()
          .metadataSingleFlag("FlowAHighAlarm")
          .hash("Description")
          .setString("Cell A flow rate exceeded upper bound alarm threshold");
    result.back()
          .write()
          .metadataSingleFlag("FlowAHighAlarm")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x000002000000));
    result.back()
          .write()
          .metadataSingleFlag("FlowAHighAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49");

    result.back()
          .write()
          .metadataSingleFlag("FlowBLowAlarm")
          .hash("Description")
          .setString("Cell B flow rate exceeded lower bound alarm threshold");
    result.back()
          .write()
          .metadataSingleFlag("FlowBLowAlarm")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x000004000000));
    result.back()
          .write()
          .metadataSingleFlag("FlowBLowAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49");

    result.back()
          .write()
          .metadataSingleFlag("FlowBHighAlarm")
          .hash("Description")
          .setString("Cell B flow rate exceeded upper bound alarm threshold");
    result.back()
          .write()
          .metadataSingleFlag("FlowBHighAlarm")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x000008000000));
    result.back()
          .write()
          .metadataSingleFlag("FlowBHighAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49");


    result.back()
          .write()
          .metadataSingleFlag("IntensityALowAlarm")
          .hash("Description")
          .setString("Cell A intensity exceeded lower bound alarm threshold");
    result.back()
          .write()
          .metadataSingleFlag("IntensityALowAlarm")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x000010000000));
    result.back()
          .write()
          .metadataSingleFlag("IntensityALowAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49");

    result.back()
          .write()
          .metadataSingleFlag("IntensityAHighAlarm")
          .hash("Description")
          .setString("Cell A intensity exceeded upper bound alarm threshold");
    result.back()
          .write()
          .metadataSingleFlag("IntensityAHighAlarm")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x000020000000));
    result.back()
          .write()
          .metadataSingleFlag("IntensityAHighAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49");

    result.back()
          .write()
          .metadataSingleFlag("IntensityBLowAlarm")
          .hash("Description")
          .setString("Cell B intensity exceeded lower bound alarm threshold");
    result.back()
          .write()
          .metadataSingleFlag("IntensityBLowAlarm")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x000040000000));
    result.back()
          .write()
          .metadataSingleFlag("IntensityBLowAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49");

    result.back()
          .write()
          .metadataSingleFlag("IntensityBHighAlarm")
          .hash("Description")
          .setString("Cell B intensity exceeded upper bound alarm threshold");
    result.back()
          .write()
          .metadataSingleFlag("IntensityBHighAlarm")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x000080000000));
    result.back()
          .write()
          .metadataSingleFlag("IntensityBHighAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49");


    result.back()
          .write()
          .metadataSingleFlag("OzoneLowAlarm")
          .hash("Description")
          .setString("Ozone concentration exceeded lower bound alarm threshold");
    result.back()
          .write()
          .metadataSingleFlag("OzoneLowAlarm")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x400000000000));
    result.back()
          .write()
          .metadataSingleFlag("OzoneLowAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49");

    result.back()
          .write()
          .metadataSingleFlag("OzoneHighAlarm")
          .hash("Description")
          .setString("Ozone concentration exceeded upper bound alarm threshold");
    result.back()
          .write()
          .metadataSingleFlag("OzoneHighAlarm")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x800000000000));
    result.back()
          .write()
          .metadataSingleFlag("OzoneHighAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49");


    result.back()
          .write()
          .metadataSingleFlag("ServiceMode")
          .hash("Description")
          .setString("Service mode enabled");
    result.back()
          .write()
          .metadataSingleFlag("ServiceMode")
          .hash("Bits")
          .setInt64(Q_INT64_C(0x200000000000));
    result.back()
          .write()
          .metadataSingleFlag("ServiceMode")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49");

    result.back()
          .write()
          .metadataSingleFlag("LocalMode")
          .hash("Description")
          .setString("Local mode enabled");
    result.back()
          .write()
          .metadataSingleFlag("LocalMode")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49");


    return result;
}

SequenceValue::Transfer AcquireThermoOzone49::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_thermo_ozone49");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    processing["Model"].set(instrumentMeta["Model"]);
    switch (interfaceMode) {
    case Mode_49c_legacy:
        if (instrumentMeta["FirmwareVersion"].exists())
            processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
        if (instrumentMeta["LinkVersion"].exists())
            processing["LinkVersion"].set(instrumentMeta["LinkVersion"]);
        break;
    case Mode_49c:
        processing["LinkVersion"].set(instrumentMeta["LinkVersion"]);
        break;
    case Mode_49i:
        processing["HostName"].set(instrumentMeta["HostName"]);
        break;
    default:
        break;
    }


    /* This exists so that the text mode clients show values during spanchecks
     * and zeros, but the plots and logged values do not. */
    result.emplace_back(SequenceName({}, "raw_meta", "ZX"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000000.00");
    result.back().write().metadataReal("Units").setString("ppb");
    result.back().write().metadataReal("Description").setString("Ozone concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Background").setDouble(ozoneBackground);
    result.back().write().metadataReal("Coefficient").setDouble(ozoneCoefficient);
    if (FP::defined(reportingT) && !temperatureCompensation) {
        result.back().write().metadataReal("ReportT").setDouble(reportingT);
    }
    if (FP::defined(reportingP) && !pressureCompensation) {
        result.back().write().metadataReal("ReportP").setDouble(reportingP);
    }
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Ozone"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);


    result.emplace_back(SequenceName({}, "raw_meta", "Xzd"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000000.00");
    result.back().write().metadataReal("Units").setString("ppb");
    result.back().write().metadataReal("Description").setString("Zero ozone concentration change");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Background").setDouble(ozoneBackground);
    result.back().write().metadataReal("Coefficient").setDouble(ozoneCoefficient);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Zero change"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);


    result.emplace_back(SequenceName({}, "raw_meta", "ZINSTFLAGS"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataInteger("Format").setString("FFFFFFFF");
    result.back().write().metadataInteger("Description").setString("Raw instrument flags bits");
    result.back().write().metadataInteger("Source").set(instrumentMeta);
    result.back().write().metadataInteger("Processing").toArray().after_back().set(processing);
    result.back().write().metadataInteger("Realtime").hash("Name").setString(QObject::tr("Flags"));
    result.back().write().metadataInteger("Realtime").hash("PageMask").setInt64(0x03);
    result.back().write().metadataInteger("Realtime").hash("RowOrder").setInt64(20);


    if (interfaceMode == Mode_49i) {
        result.emplace_back(SequenceName({}, "raw_meta", "ZMBP24V"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("000.0");
        result.back().write().metadataReal("Units").setString("V");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Motherboard positive 24V supply");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Motherboard"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("+24V"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(0);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);

        result.emplace_back(SequenceName({}, "raw_meta", "ZMBP15V"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("000.0");
        result.back().write().metadataReal("Units").setString("V");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Motherboard positive 15V supply");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Motherboard"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("+15V"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);

        result.emplace_back(SequenceName({}, "raw_meta", "ZMBP5V"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("000.0");
        result.back().write().metadataReal("Units").setString("V");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Motherboard positive 5V supply");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Motherboard"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("+5V"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(2);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);

        result.emplace_back(SequenceName({}, "raw_meta", "ZMBP3V"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("000.0");
        result.back().write().metadataReal("Units").setString("V");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Motherboard positive 3.3V supply");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Motherboard"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("+3.3V"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(3);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);

        result.emplace_back(SequenceName({}, "raw_meta", "ZMBN3V"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("000.0");
        result.back().write().metadataReal("Units").setString("V");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Motherboard negative 3.3V supply");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Motherboard"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("-3.3V"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(4);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);


        result.emplace_back(SequenceName({}, "raw_meta", "ZMEP24V"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("000.0");
        result.back().write().metadataReal("Units").setString("V");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Measurement board positive 24V supply");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Measurement"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("+24V"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(0);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);

        result.emplace_back(SequenceName({}, "raw_meta", "ZMEP15V"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("000.0");
        result.back().write().metadataReal("Units").setString("V");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Measurement board positive 15V supply");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Measurement"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("+15V"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);

        result.emplace_back(SequenceName({}, "raw_meta", "ZMEP5V"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("000.0");
        result.back().write().metadataReal("Units").setString("V");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Measurement board positive 5V supply");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Measurement"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("+5V"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(2);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);

        result.emplace_back(SequenceName({}, "raw_meta", "ZMEP3V"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("000.0");
        result.back().write().metadataReal("Units").setString("V");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Measurement board positive 3.3V supply");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Measurement"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("+3.3V"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(3);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);

        result.emplace_back(SequenceName({}, "raw_meta", "ZMEN15V"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("000.0");
        result.back().write().metadataReal("Units").setString("V");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Measurement board negative 15V supply");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Measurement"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("-15V"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(5);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);


        result.emplace_back(SequenceName({}, "raw_meta", "ZIOP24V"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("000.0");
        result.back().write().metadataReal("Units").setString("V");
        result.back().write().metadataReal("Description").setString("IO board positive 24V supply");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("IO"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("+24V"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(0);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);

        result.emplace_back(SequenceName({}, "raw_meta", "ZIOP5V"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("000.0");
        result.back().write().metadataReal("Units").setString("V");
        result.back().write().metadataReal("Description").setString("IO board positive 5V supply");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("IO"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("+5V"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(2);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);

        result.emplace_back(SequenceName({}, "raw_meta", "ZIOP3V"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("000.0");
        result.back().write().metadataReal("Units").setString("V");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("IO board positive 3.3V supply");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("IO"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("+3.3V"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(3);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);

        result.emplace_back(SequenceName({}, "raw_meta", "ZION3V"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("000.0");
        result.back().write().metadataReal("Units").setString("V");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("IO board negative 3.3V supply");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("IO"));
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("-3.3V"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(4);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);
    }


    result.emplace_back(SequenceName({}, "raw_meta", "PCT"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000.00");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Sample lamp setting");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Lamp"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);


    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Realtime").hash("Name").setString(QString());
    result.back().write().metadataString("Realtime").hash("Units").setString(QString());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back().write().metadataString("Realtime").hash("PageMask").setInt64(0x03);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Run")
          .setString(QObject::tr("", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Alarm")
          .setString(QObject::tr("ALARM ACTIVE", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Zero")
          .setString(QObject::tr("Zero in progress", "zero state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Spancheck")
          .setString(QObject::tr("Spancheck in progress", "spancheck state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Blank")
          .setString(QObject::tr("Blank in progress", "spancheck state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidRecord")
          .setString(QObject::tr("NO COMMS: Invalid record"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidResponse")
          .setString(QObject::tr("NO COMMS: Invalid response"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Timeout")
          .setString(QObject::tr("NO COMMS: Timeout"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractive")
          .setString(QObject::tr("STARTING COMMS"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetRemote")
          .setString(QObject::tr("STARTING COMMS: Setting remote mode"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetFormat")
          .setString(QObject::tr("STARTING COMMS: Setting output format"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetDate")
          .setString(QObject::tr("STARTING COMMS: Setting date"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetTime")
          .setString(QObject::tr("STARTING COMMS: Setting time"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadProgramNumber")
          .setString(QObject::tr("STARTING COMMS: Reading program number"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveEnableTemperatureCompensation")
          .setString(QObject::tr("STARTING COMMS: Enabling temperature compensation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveEnablePressureCompensation")
          .setString(QObject::tr("STARTING COMMS: Enabling pressure compensation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetUnits")
          .setString(QObject::tr("STARTING COMMS: Setting ppb units"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetMode")
          .setString(QObject::tr("STARTING COMMS: Setting sample mode"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadBackground")
          .setString(QObject::tr("STARTING COMMS: Reading ozone background setting"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadCoefficient")
          .setString(QObject::tr("STARTING COMMS: Reading ozone coefficient setting"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadFlags")
          .setString(QObject::tr("STARTING COMMS: Reading flags setting"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetAveraging")
          .setString(QObject::tr("STARTING COMMS: Setting averaging time"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadLampSetting")
          .setString(QObject::tr("STARTING COMMS: Reading lamp setting"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadHostName")
          .setString(QObject::tr("STARTING COMMS: Reading host name"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadOzonatorFlow")
          .setString(QObject::tr("STARTING COMMS: Reading ozonator flow"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadOzone")
          .setString(QObject::tr("STARTING COMMS: Reading ozone concentration"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadOzonatorLevels")
          .setString(QObject::tr("STARTING COMMS: Reading ozonator drive levels"));

    /* Not really metadata, but not updated except at startup either */
    if (FP::defined(values.PCT.read().toReal())) {
        result.emplace_back(SequenceName({}, "raw", "PCT"), values.PCT, time, FP::undefined());
    }

    return result;
}

SequenceMatch::Composite AcquireThermoOzone49::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "Pz");
    sel.append({}, {}, "Tz");
    sel.append({}, {}, "Xz");
    sel.append({}, {}, "ZZERO");
    sel.append({}, {}, "ZSPANCHECK.*");
    sel.append({}, {}, "Xzd");
    sel.append({}, {}, "PCT");
    sel.append({}, {}, "ZSTATE");
    return sel;
}

AcquireThermoOzone49::Command::Command() : parent(NULL), command(COMMAND_IGNORED), dt()
{ }

AcquireThermoOzone49::Command::Command(AcquireThermoOzone49 *p, CommandType t, Util::ByteArray d)
        : parent(p), command(t), dt(std::move(d))
{ }

AcquireThermoOzone49::Command::Command(const Command &other) : parent(other.parent),
                                                               command(other.command),
                                                               dt(other.dt)
{ }

AcquireThermoOzone49::Command &AcquireThermoOzone49::Command::operator=(const Command &other)
{
    if (&other == this)
        return *this;
    parent = other.parent;
    command = other.command;
    dt = other.dt;
    return *this;
}

bool AcquireThermoOzone49::Command::operator==(const Command &other) const
{
    return command == other.command && dt == other.dt;
}

Variant::Root AcquireThermoOzone49::Command::stateDescription() const
{
    Variant::Root result;
    result["Payload"].setString(dt.toString());

    switch (command) {
    case COMMAND_IGNORED:
        result["Type"].setString("Ignored");
        break;
    case COMMAND_READ_OZONE:
        result["Type"].setString("ReadOzone");
        break;
    case COMMAND_READ_SAMPLE_P:
        result["Type"].setString("ReadSamplePressure");
        break;
    case COMMAND_READ_SAMPLE_T:
        result["Type"].setString("ReadSampleTemperature");
        break;
    case COMMAND_READ_LAMP_T:
        result["Type"].setString("ReadLampTemperature");
        break;
    case COMMAND_READ_INTENSITY_A:
        result["Type"].setString("ReadCellAIntensity");
        break;
    case COMMAND_READ_INTENSITY_B:
        result["Type"].setString("ReadCellBIntensity");
        break;
    case COMMAND_READ_FLOW_A:
        result["Type"].setString("ReadCellAFlow");
        break;
    case COMMAND_READ_FLOW_B:
        result["Type"].setString("ReadCellBFlow");
        break;
    case COMMAND_READ_FLAGS:
        result["Type"].setString("ReadFlags");
        break;
    case COMMAND_READ_FLOW_OZONATOR:
        result["Type"].setString("ReadOzonatorFlow");
        break;
    case COMMAND_READ_OZONATOR_T:
        result["Type"].setString("ReadOzonatorTemperature");
        break;
    case COMMAND_READ_LAMP_V:
        result["Type"].setString("ReadLampVoltage");
        break;
    case COMMAND_READ_OZONATOR_V:
        result["Type"].setString("ReadOzonatorVoltage");
        break;
    case COMMAND_READ_MOTHERBOARD_V:
        result["Type"].setString("ReadMotherboardVoltages");
        break;
    case COMMAND_READ_INTERFACE_V:
        result["Type"].setString("ReadMeasurementVoltages");
        break;
    case COMMAND_READ_IO_V:
        result["Type"].setString("ReadIOVoltages");
        break;
    case COMMAND_SAMPLE_AMBIENT:
        result["Type"].setString("SetAmbientSampling");
        break;
    case COMMAND_SAMPLE_ZERO:
        result["Type"].setString("SetZeroSampling");
        break;
    case COMMAND_SAMPLE_LEVEL:
        result["Type"].setString("SetOzonatorLevelSampling");
        break;
    case COMMAND_SET_PUMP_ON:
        result["Type"].setString("SetPumpOn");
        break;
    case COMMAND_SET_PUMP_OFF:
        result["Type"].setString("SetPumpOff");
        break;
    case COMMAND_SET_REMOTE:
        result["Type"].setString("SetRemoteMode");
        break;
    case COMMAND_SET_FORMAT:
        result["Type"].setString("SetFormat");
        break;
    case COMMAND_SET_GAS_UNITS:
        result["Type"].setString("SetPPBUnits");
        break;
    case COMMAND_SET_AVERAGE_TIME:
        result["Type"].setString("SetAveragingTime");
        break;
    case COMMAND_SET_ENABLE_T_COMP:
        result["Type"].setString("EnableTemperatureCompensation");
        break;
    case COMMAND_SET_ENABLE_P_COMP:
        result["Type"].setString("EnablePressureCompensation");
        break;
    case COMMAND_SET_DATE:
        result["Type"].setString("SetDate");
        break;
    case COMMAND_SET_TIME:
        result["Type"].setString("SetTime");
        break;
    case COMMAND_READ_PROGRAM_NUMBER:
        result["Type"].setString("ReadProgramNumber");
        break;
    case COMMAND_READ_OZONEBACKGROUND:
        result["Type"].setString("ReadOzoneBackground");
        break;
    case COMMAND_READ_OZONECOEFFICIENT:
        result["Type"].setString("ReadOzoneCoefficient");
        break;
    case COMMAND_READ_LAMP_SETTTING:
        result["Type"].setString("ReadLampSetting");
        break;
    case COMMAND_READ_HOST_NAME:
        result["Type"].setString("ReadHostName");
        break;
    case COMMAND_READ_OZONATOR_LEVEL:
        result["Type"].setString("ReadOzonatorLevel");
        break;
    }

    return result;
}

void AcquireThermoOzone49::Command::invalidate()
{
    parent = NULL;
    command = COMMAND_IGNORED;
    dt = QByteArray();
}

void AcquireThermoOzone49::invalidateLogValues(double frameTime)
{
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
        streamAge[i] = 0;

        if (loggingEgress != NULL)
            loggingMux.advance(i, frameTime, loggingEgress);
    }
    loggingMux.clear();
    loggingLost(frameTime);

    zeroUpdated = false;
    spanUpdated = false;

    autoprobeValidRecords = 0;
}

QString AcquireThermoOzone49::describeResponseState() const
{
    switch (responseState) {
    case RESP_PASSIVE_RUN:
        return "PassiveRun";
    case RESP_PASSIVE_WAIT:
        return "PassiveWait";
    case RESP_PASSIVE_INITIALIZE:
        return "PassiveInitialize";
    case RESP_AUTOPROBE_PASSIVE_WAIT:
        return "AutoprobePassiveWait";
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        return "AutoprobePassiveInitialize";
    case RESP_INTERACTIVE_RUN:
        return "Interactive";
    case RESP_INTERACTIVE_RUN_DELAY:
        return "InteractiveDelay";
    case RESP_INTERACTIVE_RUN_WAIT:
        return "InteractiveWait";
    case RESP_INTERACTIVE_START_SET_REMOTE_INITIAL:
        return "InteractiveStartSetRemoteInitial";
    case RESP_INTERACTIVE_START_SET_REMOTE_CONFIRM:
        return "InteractiveStartSetRemoteConfirm";
    case RESP_INTERACTIVE_START_READ_PROGRAM_NUMBER:
        return "InteractiveStartReadProgramNumber";
    case RESP_INTERACTIVE_START_SET_FORMAT:
        return "InteractiveStartSetFormat";
    case RESP_INTERACTIVE_START_SET_AVERAGING_TIME:
        return "InteractiveStartSetAveraging";
    case RESP_INTERACTIVE_START_SET_GAS_UNITS:
        return "InteractiveStartSetGasUnits";
    case RESP_INTERACTIVE_START_SET_TEMPERATURE_COMPENSATION:
        return "InteractiveStartEnableTemperatureCompensation";
    case RESP_INTERACTIVE_START_SET_PRESSURE_COMPENSATION:
        return "InteractiveStartEnablePressureCompensation";
    case RESP_INTERACTIVE_START_SET_MODE:
        return "InteractiveStartSetSamplingMode";
    case RESP_INTERACTIVE_START_SET_DATE:
        return "InteractiveStartSetDate";
    case RESP_INTERACTIVE_START_SET_TIME:
        return "InteractiveStartSetTime";
    case RESP_INTERACTIVE_START_READ_OZONE_BACKGROUND:
        return "InteractiveStartReadBackground";
    case RESP_INTERACTIVE_START_READ_OZONE_COEFFICIENT:
        return "InteractiveStartReadCoefficient";
    case RESP_INTERACTIVE_START_READ_FLAGS:
        return "InteractiveStartReadFlags";
    case RESP_INTERACTIVE_START_READ_LAMP_SETTING:
        return "InteractiveStartReadLampSetting";
    case RESP_INTERACTIVE_START_READ_HOST_NAME:
        return "InteractiveStartReadHostName";
    case RESP_INTERACTIVE_START_READ_OZONATOR_LEVELS:
        return QString("InteractiveStartReadOzonatorLevels(%1)").arg(responseStateIndex);
    case RESP_INTERACTIVE_START_READ_OZONATOR_FLOW:
        return "InterattiveStartReadOzonatorFlow";
    case RESP_INTERACTIVE_START_READ_OZONE_INITIAL:
        return "InteractiveStartReadOzone";
    case RESP_INTERACTIVE_RESTART_WAIT:
        return "InteractiveRestartWait";
    case RESP_INTERACTIVE_INITIALIZE:
        return "InteractiveInitialize";
    }
    Q_ASSERT(false);
    return QString();
}

Variant::Root AcquireThermoOzone49::describeSampleState() const
{
    Variant::Root result;

    switch (sampleState) {
    case SAMPLE_RUN:
        result["State"].setString("Run");
        break;
    case SAMPLE_ZERO_BEGIN_FLUSH:
        result["State"].setString("ZeroBeginFlush");
        result["BeginTime"].setDouble(sampleStateBeginTime);
        break;
    case SAMPLE_ZERO_MEASURE:
        result["State"].setString("Zero");
        result["BeginTime"].setDouble(sampleStateBeginTime);
        break;
    case SAMPLE_ZERO_END_FLUSH:
        result["State"].setString("ZeroEndFlush");
        result["BeginTime"].setDouble(sampleStateBeginTime);
        break;

    case SAMPLE_SPANCHECK_BEGINZERO_FLUSH:
        result["State"].setString("SpancheckBeginZeroFlush");
        result["BeginTime"].setDouble(sampleStateBeginTime);
        break;
    case SAMPLE_SPANCHECK_BEGINZERO_MEASURE:
        result["State"].setString("SpancheckBeginZero");
        result["BeginTime"].setDouble(sampleStateBeginTime);
        break;
    case SAMPLE_SPANCHECK_LEVEL_FLUSH:
        result["State"].setString("SpancheckLevelFlush");
        result["BeginTime"].setDouble(sampleStateBeginTime);
        result["Index"].setInt64(sampleStateIndex);
        break;
    case SAMPLE_SPANCHECK_LEVEL_MEASURE:
        result["State"].setString("SpancheckLevel");
        result["BeginTime"].setDouble(sampleStateBeginTime);
        result["Index"].setInt64(sampleStateIndex);
        break;
    case SAMPLE_SPANCHECK_ENDZERO_BEGIN_FLUSH:
        result["State"].setString("SpancheckEndZeroFlush");
        result["BeginTime"].setDouble(sampleStateBeginTime);
        break;
    case SAMPLE_SPANCHECK_ENDZERO_MEASURE:
        result["State"].setString("SpancheckEndZero");
        result["BeginTime"].setDouble(sampleStateBeginTime);
        break;
    case SAMPLE_SPANCHECK_ENDZERO_END_FLUSH:
        result["State"].setString("SpancheckEndFlush");
        result["BeginTime"].setDouble(sampleStateBeginTime);
        break;
    }

    switch (modeChangeRequest) {
    case REQUEST_NONE:
        result["Request"].setString("None");
        break;
    case REQUEST_ZERO:
        result["Request"].setString("Zero");
        break;
    case REQUEST_SPANCHECK:
        result["Request"].setString("Spancheck");
        break;
    case REQUEST_SPANCHECK_ABORT:
        result["Request"].setString("SpancheckAbort");
        break;
    }

    return result;
}

void AcquireThermoOzone49::invalidCommandResponse(const Util::ByteView &frame,
                                                  double frameTime,
                                                  int code)
{
    Variant::Write info = Variant::Write::empty();

    info.hash("Response").setString(frame.toString());
    info.hash("Code").setInt64(code);
    info.hash("ResponseState").setString(describeResponseState());
    info.hash("SampleState").set(describeSampleState());
    info.hash("IssuedCommand").set(issuedCommand.stateDescription());

    invalidateLogValues(frameTime);

    switch (responseState) {
    case RESP_PASSIVE_RUN:

        qCDebug(log) << "Passive command response at" << Logging::time(frameTime) << frame
                     << "rejected in response to" << issuedCommand.data() << "with code" << code;

        timeoutAt(FP::undefined());
        responseState = RESP_PASSIVE_WAIT;
        autoprobeValidRecords = 0;

        event(frameTime, QObject::tr(
                "Invalid command response received (code %1).  Communications dropped.").arg(code),
              true, info);
        break;

    case RESP_INTERACTIVE_RUN:
    case RESP_INTERACTIVE_RUN_WAIT:
    case RESP_INTERACTIVE_RUN_DELAY:
        qCDebug(log) << "Interactive command response at" << Logging::time(frameTime) << frame
                     << "rejected in response to" << issuedCommand.data() << "with code" << code;

        event(frameTime, QObject::tr(
                "Invalid command response received (code %1).  Communications dropped.").arg(code),
              true, info);

        autoprobeValidRecords = 0;
        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        timeoutAt(frameTime + 2.0);
        discardData(frameTime + 0.5);
        generalStatusUpdated();
        break;

    case RESP_PASSIVE_INITIALIZE:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
        break;

    case RESP_INTERACTIVE_START_SET_REMOTE_INITIAL:
    case RESP_INTERACTIVE_START_SET_REMOTE_CONFIRM:
    case RESP_INTERACTIVE_START_READ_PROGRAM_NUMBER:
    case RESP_INTERACTIVE_START_SET_FORMAT:
    case RESP_INTERACTIVE_START_SET_AVERAGING_TIME:
    case RESP_INTERACTIVE_START_SET_GAS_UNITS:
    case RESP_INTERACTIVE_START_SET_TEMPERATURE_COMPENSATION:
    case RESP_INTERACTIVE_START_SET_PRESSURE_COMPENSATION:
    case RESP_INTERACTIVE_START_SET_MODE:
    case RESP_INTERACTIVE_START_SET_DATE:
    case RESP_INTERACTIVE_START_SET_TIME:
    case RESP_INTERACTIVE_START_READ_OZONE_BACKGROUND:
    case RESP_INTERACTIVE_START_READ_OZONE_COEFFICIENT:
    case RESP_INTERACTIVE_START_READ_FLAGS:
    case RESP_INTERACTIVE_START_READ_LAMP_SETTING:
    case RESP_INTERACTIVE_START_READ_HOST_NAME:
    case RESP_INTERACTIVE_START_READ_OZONATOR_LEVELS:
    case RESP_INTERACTIVE_START_READ_OZONE_INITIAL:
    case RESP_INTERACTIVE_START_READ_OZONATOR_FLOW:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive start response at" << Logging::time(frameTime) << frame
                     << "rejected in response to" << issuedCommand.data() << "with code" << code;

        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);
        autoprobeValidRecords = 0;

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                  frameTime, FP::undefined()));
        }
        break;
    }

    if (!commandBacklog.isEmpty()) {
        issuedCommand = commandBacklog.takeFirst();
    } else {
        issuedCommand.invalidate();
    }
}

void AcquireThermoOzone49::resetSpancheckLevels()
{
    for (QList<MeasurementData *>::iterator mod = levelMeasurement.begin(),
            endMod = levelMeasurement.end(); mod != endMod; ++mod) {
        (*mod)->reset();
    }
}

void AcquireThermoOzone49::sendCommand(const Command &command, double frameTime)
{
    double delay = config.first().commandDelay;
    if (!FP::defined(delay)) {
        switch (interfaceMode) {
        case Mode_Undetermined:
            break;
        case Mode_49c_legacy:
        case Mode_49c:
            delay = 0.25;
            break;
        case Mode_49i:
            break;
        }
    }

    commandBacklog.clear();
    issuedCommand = command;

    if (FP::defined(frameTime)) {
        if (FP::defined(delay) && delay > 0.0 && responseState == RESP_INTERACTIVE_RUN) {
            responseState = RESP_INTERACTIVE_RUN_DELAY;
            timeoutAt(frameTime + delay);
            return;
        }

        discardData(FP::undefined());
        timeoutAt(frameTime + 2.0);
    }

    if (controlStream != NULL) {
        Util::ByteArray send = command.data();
        if (config.first().address != 0) {
            send.push_front((char) (config.first().address + 128));
        }
        send.push_back('\r');
        controlStream->writeControl(std::move(send));
    }
}

void AcquireThermoOzone49::restartSequence(double frameTime)
{
    if (!FP::defined(config.first().pollInterval) ||
            !FP::defined(streamTime[LogStream_Ozone]) ||
            config.first().pollInterval <= 0.0 ||
            frameTime - streamTime[LogStream_Ozone] >= config.first().pollInterval) {
        sendCommand(Command(this, COMMAND_READ_OZONE, "o3"), frameTime);
        return;
    }

    responseState = RESP_INTERACTIVE_RUN_WAIT;
    timeoutAt(streamTime[LogStream_Ozone] + config.first().pollInterval);
}

bool AcquireThermoOzone49::compensationStateOn(CompensationState state)
{
    switch (state) {
    case Compensation_Unknown:
        return true;
    case Compensation_SetInFlags:
        return true;
    case Compensation_ClearInFlags:
        return false;
    case Compensation_SetInReading:
        return true;
    case Compensation_ClearInReading:
        return true;
    }
    Q_ASSERT(false);
    return true;
}

Variant::Flags AcquireThermoOzone49::buildFlags() const
{
    Variant::Flags flags;
    if (!compensationStateOn(temperatureCompensation) &&
            !compensationStateOn(pressureCompensation)) {
        flags.insert("STP");
    }

    switch (sampleState) {
    case SAMPLE_RUN:
        break;

    case SAMPLE_ZERO_BEGIN_FLUSH:
    case SAMPLE_ZERO_END_FLUSH:
        flags.insert("Blank");
        break;

    case SAMPLE_ZERO_MEASURE:
        flags.insert("Zero");
        break;

    case SAMPLE_SPANCHECK_BEGINZERO_FLUSH:
    case SAMPLE_SPANCHECK_LEVEL_FLUSH:
    case SAMPLE_SPANCHECK_ENDZERO_BEGIN_FLUSH:
    case SAMPLE_SPANCHECK_ENDZERO_END_FLUSH:
        flags.insert("Spancheck");
        flags.insert("Blank");
        break;

    case SAMPLE_SPANCHECK_BEGINZERO_MEASURE:
    case SAMPLE_SPANCHECK_ENDZERO_MEASURE:
        flags.insert("Spancheck");
        flags.insert("Zero");
        break;

    case SAMPLE_SPANCHECK_LEVEL_MEASURE:
        flags.insert("Spancheck");
        break;
    }

    qint64 bits = values.flags.read().toInt64();
    if (INTEGER::defined(bits)) {
        for (int i = 0;
                i <
                        (int) (sizeof(instrumentFlagTranslation) /
                                sizeof(instrumentFlagTranslation[0]));
                i++) {
            if (instrumentFlagTranslation[i] == NULL)
                continue;
            if (!(bits & Q_INT64_C(1) << (qint64) i))
                continue;
            flags.insert(instrumentFlagTranslation[i]);
        }

        if ((bits & Q_INT64_C(0x30000000)) == 0) {
            flags.insert("LocalMode");
        }
    }

    return flags;
}

void AcquireThermoOzone49::configAdvance(double frameTime)
{
    Q_ASSERT(!config.isEmpty());
    int oldSize = config.size();
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.isEmpty());

    if (oldSize == config.size())
        return;
    configurationChanged();
}

void AcquireThermoOzone49::configurationChanged()
{
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
}

static double applyUnitScaling(double input, double scale)
{
    if (!FP::defined(input) || !FP::defined(scale))
        return FP::undefined();
    return input * scale;
}

static double convertOzoneUnits(double X, const Util::ByteView &units, bool &ok)
{
    ok = true;
    if (units == "ppb")
        return X;
    if (units == "ppm")
        return applyUnitScaling(X, 1E3);

    /* My reading of the manual is that this uses a fixed temperature
     * defaulting to 20C and that there's no way to read the actual
     * temperature.  So we use the air density at 20C for lack of
     * anything better */
    static const double airWeightkgm3 = 1.2041;

    if (units == "mg/m3" || units == "mg/m³")
        return applyUnitScaling(X, 1E9 / (airWeightkgm3 * 1E-6));
    if (units == "ug/m3" || units == "ug/m³")
        return applyUnitScaling(X, 1E9 / (airWeightkgm3 * 1E-9));

    ok = false;
    return FP::undefined();
}

/*
 * Frames look like:
 *
 *    response\r
 *    response*\r
 *    \xFFresponse\r
 *    response\nsum hhhh\r
 *    response\x80sum hhhh\r
 *    response*\nsum hhhh\r
 *
 * But we just ignore the checksum (4-hex sum of the response bytes).
 */
std::size_t AcquireThermoOzone49::dataFrameStart(std::size_t offset,
                                                 const Util::ByteArray &input) const
{
    auto max = input.size();
    while (offset < max) {
        auto ch = input[offset];
        if (ch == '\r' || ch == '\n' || ch == 0x80 || ch == 0xFF) {
            offset++;
            continue;
        }
        if (ch == '*') {
            if (offset + 1 >= max)
                return input.npos;
            auto next = input[offset + 1];
            if (next == '\n' || next == '\r') {
                offset += 2;
                continue;
            }
        }
        return offset;
    }
    return input.npos;
}

std::size_t AcquireThermoOzone49::dataFrameEnd(std::size_t start,
                                               const Util::ByteArray &input) const
{
    for (auto max = input.size(); start < max; start++) {
        auto ch = input[start];
        if (ch == '\r' || ch == '\n' || ch == 0x80 || ch == 0xFF)
            return start;
        if (ch == '*') {
            if (start < max + 1) {
                char next = input[start + 1];
                if (next == '\n' || next == '\r')
                    return start;
            }
        }
    }
    return input.npos;
}

void AcquireThermoOzone49::handleStartupAdvance(double frameTime, bool delayable)
{
    if (delayable) {
        double delay = config.first().commandDelay;
        if (!FP::defined(delay)) {
            switch (interfaceMode) {
            case Mode_Undetermined:
                break;
            case Mode_49c_legacy:
            case Mode_49c:
                delay = 0.25;
                break;
            case Mode_49i:
                break;
            }
        }
        if (FP::defined(delay) && delay > 0.0) {
            discardData(frameTime + delay);
            return;
        }
    }
    switch (responseState) {
    case RESP_INTERACTIVE_START_SET_REMOTE_INITIAL:
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveSetRemote"),
                                  frameTime, FP::undefined()));
        }

        sendCommand(Command(this, COMMAND_SET_REMOTE, "set mode remote"), frameTime);
        responseState = RESP_INTERACTIVE_START_SET_REMOTE_CONFIRM;
        invalidateLogValues(frameTime);
        return;

    case RESP_INTERACTIVE_START_READ_PROGRAM_NUMBER:

        autoprobeValidRecords = 0;

        sendCommand(Command(this, COMMAND_SET_FORMAT, "set format 00"), frameTime);
        responseState = RESP_INTERACTIVE_START_SET_FORMAT;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveSetFormat"),
                                  frameTime, FP::undefined()));
        }
        return;

    case RESP_INTERACTIVE_START_SET_FORMAT: {

        int avgInterval = 0;
        double poll = config.first().pollInterval;
        if (!FP::defined(poll) || poll <= 15.0) {
            avgInterval = 0;
        } else if (!FP::defined(poll) || poll <= 25.0) {
            avgInterval = 1;
        } else if (!FP::defined(poll) || poll <= 45.0) {
            avgInterval = 2;
        } else if (!FP::defined(poll) || poll <= 75.0) {
            avgInterval = 3;
        } else if (!FP::defined(poll) || poll <= 105.0) {
            avgInterval = 4;
        } else if (!FP::defined(poll) || poll <= 150.0) {
            avgInterval = 5;
        } else if (!FP::defined(poll) || poll <= 210.0) {
            avgInterval = 6;
        } else if (!FP::defined(poll) || poll <= 270.0) {
            avgInterval = 7;
        } else {
            avgInterval = 8;
        }
        Util::ByteArray command("set avg time ");
        command += QByteArray::number(avgInterval);

        sendCommand(Command(this, COMMAND_SET_AVERAGE_TIME, std::move(command)), frameTime);
        responseState = RESP_INTERACTIVE_START_SET_AVERAGING_TIME;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveSetAveraging"), frameTime, FP::undefined()));
        }
        return;
    }

    case RESP_INTERACTIVE_START_SET_GAS_UNITS:

        sendCommand(Command(this, COMMAND_SET_ENABLE_T_COMP, "set temp comp on"), frameTime);
        responseState = RESP_INTERACTIVE_START_SET_TEMPERATURE_COMPENSATION;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveEnableTemperatureCompensation"), frameTime, FP::undefined()));
        }
        return;

    case RESP_INTERACTIVE_START_SET_AVERAGING_TIME:

        sendCommand(Command(this, COMMAND_SET_GAS_UNITS, "set gas unit ppb"), frameTime);
        responseState = RESP_INTERACTIVE_START_SET_GAS_UNITS;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveSetUnits"),
                                  frameTime, FP::undefined()));
        }
        return;

    case RESP_INTERACTIVE_START_SET_TEMPERATURE_COMPENSATION:

        sendCommand(Command(this, COMMAND_SET_ENABLE_P_COMP, "set pres comp on"), frameTime);
        responseState = RESP_INTERACTIVE_START_SET_PRESSURE_COMPENSATION;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveEnablePressureCompensation"), frameTime, FP::undefined()));
        }
        return;

    case RESP_INTERACTIVE_START_SET_PRESSURE_COMPENSATION:

        switch (sampleState) {
        case SAMPLE_RUN:
        case SAMPLE_ZERO_END_FLUSH:
        case SAMPLE_SPANCHECK_ENDZERO_END_FLUSH:

            sendCommand(Command(this, COMMAND_SAMPLE_AMBIENT, "set sample"), frameTime);
            break;

        case SAMPLE_ZERO_BEGIN_FLUSH:
        case SAMPLE_ZERO_MEASURE:
        case SAMPLE_SPANCHECK_BEGINZERO_FLUSH:
        case SAMPLE_SPANCHECK_BEGINZERO_MEASURE:
        case SAMPLE_SPANCHECK_ENDZERO_BEGIN_FLUSH:
        case SAMPLE_SPANCHECK_ENDZERO_MEASURE:

            sendCommand(Command(this, COMMAND_SAMPLE_ZERO, "set zero"), frameTime);
            break;

        case SAMPLE_SPANCHECK_LEVEL_FLUSH:
        case SAMPLE_SPANCHECK_LEVEL_MEASURE: {
            if (sampleStateIndex < 0 || sampleStateIndex >= config.first().spancheckLevels.size()) {
                sendCommand(Command(this, COMMAND_SAMPLE_ZERO, "set zero"), frameTime);
                break;
            } else {
                Util::ByteArray command("set level ");
                command += QByteArray::number(config.first().spancheckLevels.at(sampleStateIndex));
                sendCommand(Command(this, COMMAND_SAMPLE_LEVEL, std::move(command)), frameTime);
                break;
            }
            break;
        }
        }
        responseState = RESP_INTERACTIVE_START_SET_MODE;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveSetMode"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_SET_MODE: {
        setInstrumentTime = Time::toDateTime(frameTime + 0.5);
        Util::ByteArray command("set date ");
        command += QByteArray::number(setInstrumentTime.date().month()).rightJustified(2, '0');
        command.push_back('-');
        command += QByteArray::number(setInstrumentTime.date().day()).rightJustified(2, '0');
        command.push_back('-');
        command += QByteArray::number(setInstrumentTime.date().year() % 100).rightJustified(2, '0');

        sendCommand(Command(this, COMMAND_SET_DATE, command), frameTime);
        responseState = RESP_INTERACTIVE_START_SET_DATE;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveSetDate"),
                                  frameTime, FP::undefined()));
        }
        return;
    }

    case RESP_INTERACTIVE_START_SET_DATE: {
        Util::ByteArray packet("set time ");
        packet += QByteArray::number(setInstrumentTime.time().hour()).rightJustified(2, '0');
        packet.push_back(':');
        packet += QByteArray::number(setInstrumentTime.time().minute()).rightJustified(2, '0');
        packet.push_back(':');
        packet += QByteArray::number(setInstrumentTime.time().second()).rightJustified(2, '0');

        sendCommand(Command(this, COMMAND_SET_TIME, std::move(packet)), frameTime);
        responseState = RESP_INTERACTIVE_START_SET_TIME;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveSetTime"),
                                  frameTime, FP::undefined()));
        }
        return;
    }

    case RESP_INTERACTIVE_START_SET_TIME:

        sendCommand(Command(this, COMMAND_READ_OZONEBACKGROUND, "o3 bkg"), frameTime);
        responseState = RESP_INTERACTIVE_START_READ_OZONE_BACKGROUND;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadBackground"), frameTime, FP::undefined()));
        }
        return;

    case RESP_INTERACTIVE_START_READ_OZONE_BACKGROUND:

        sendCommand(Command(this, COMMAND_READ_OZONECOEFFICIENT, "o3 coef"), frameTime);
        responseState = RESP_INTERACTIVE_START_READ_OZONE_COEFFICIENT;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadCoefficient"), frameTime, FP::undefined()));
        }
        return;

    case RESP_INTERACTIVE_START_READ_OZONE_COEFFICIENT:

        sendCommand(Command(this, COMMAND_READ_FLAGS, "flags"), frameTime);
        responseState = RESP_INTERACTIVE_START_READ_FLAGS;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveReadFlags"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_READ_FLAGS:

        sendCommand(Command(this, COMMAND_READ_LAMP_SETTTING, "lamp setting"), frameTime);
        responseState = RESP_INTERACTIVE_START_READ_LAMP_SETTING;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadLampSetting"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_READ_LAMP_SETTING:
        switch (interfaceMode) {
        case Mode_49i:

            sendCommand(Command(this, COMMAND_READ_HOST_NAME, "host name"), frameTime);
            responseState = RESP_INTERACTIVE_START_READ_HOST_NAME;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadHostName"), frameTime, FP::undefined()));
            }
            break;

        case Mode_49c:
        case Mode_Undetermined:

            sendCommand(Command(this, COMMAND_READ_FLOW_OZONATOR, "oz flow"), frameTime);
            responseState = RESP_INTERACTIVE_START_READ_OZONATOR_FLOW;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadOzonatorFlow"), frameTime, FP::undefined()));
            }
            break;

        case Mode_49c_legacy:

            sendCommand(Command(this, COMMAND_READ_OZONE, "o3"), frameTime);
            responseState = RESP_INTERACTIVE_START_READ_OZONE_INITIAL;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadOzone"), frameTime, FP::undefined()));
            }
            break;
        }
        return;

    case RESP_INTERACTIVE_START_READ_HOST_NAME: {
        if (config.first().spancheckLevels.isEmpty()) {
            sendCommand(Command(this, COMMAND_READ_OZONE, "o3"), frameTime);
            responseState = RESP_INTERACTIVE_START_READ_OZONE_INITIAL;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadOzone"), frameTime, FP::undefined()));
            }
            return;
        }

        Util::ByteArray command("l");
        command += QByteArray::number(config.first().spancheckLevels.at(0));

        responseStateIndex = 0;
        sendCommand(Command(this, COMMAND_READ_OZONATOR_LEVEL, std::move(command)), frameTime);
        responseState = RESP_INTERACTIVE_START_READ_OZONATOR_LEVELS;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadOzonatorLevels"), frameTime, FP::undefined()));
        }
        return;
    }

    case RESP_INTERACTIVE_START_READ_OZONATOR_LEVELS: {
        ++responseStateIndex;
        if (responseStateIndex >= config.first().spancheckLevels.size()) {
            sendCommand(Command(this, COMMAND_READ_OZONE, "o3"), frameTime);
            responseState = RESP_INTERACTIVE_START_READ_OZONE_INITIAL;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadOzone"), frameTime, FP::undefined()));
            }
            return;
        }

        Util::ByteArray command("l");
        command += QByteArray::number(config.first().spancheckLevels.at(responseStateIndex));

        sendCommand(Command(this, COMMAND_READ_OZONATOR_LEVEL, std::move(command)), frameTime);
        return;
    }

    case RESP_INTERACTIVE_START_READ_OZONATOR_FLOW:

        sendCommand(Command(this, COMMAND_READ_OZONE, "o3"), frameTime);
        responseState = RESP_INTERACTIVE_START_READ_OZONE_INITIAL;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveReadOzone"),
                                  frameTime, FP::undefined()));
        }
        break;

    default:
        break;
    }
}

void AcquireThermoOzone49::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configAdvance(frameTime);

    Util::ByteArray process = frame;
    process.string_to_lower();
    process.string_trimmed();
    if (!process.empty()) {
        for (auto prior = process.begin(), next = prior + 1; next != process.end();) {
            if (std::isspace(*prior) && std::isspace(*next)) {
                prior = process.erase(prior);
                if (prior == process.end())
                    break;
                next = prior + 1;
                continue;
            }
            prior = next;
            ++next;
        }
    }

    /* Ignore checksum frames */
    if (process.size() == 8 && process.string_start("sum "))
        return;

    std::deque<Util::ByteView> fields;
    Util::ByteView field;
    bool ok = false;

    /* First stage: process the actual response to the command and update
     * the associated values and intermediate state.  During interactive
     * start comms, this also issues the next command for some of the initial
     * sequence (to prevent value output before we know the serial number,
     * etc). */
    switch (issuedCommand.type()) {
    case COMMAND_IGNORED:
        break;

    case COMMAND_READ_OZONE: {
        if (!process.string_start("o3 ")) {
            invalidCommandResponse(frame, frameTime, 1000);
            return;
        }
        fields = Util::as_deque(process.split(' '));
        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 1001);
            return;
        }
        fields.pop_front();

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 1002);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        values.X.write().setDouble(field.parse_real(&ok));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 1003);
            return;
        }
        if (!FP::defined(values.X.read().toReal())) {
            invalidCommandResponse(frame, frameTime, 1004);
            return;
        }
        remap("ZXRaw", values.X);

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 1005);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        values.X.write().setReal(convertOzoneUnits(values.X.write().toReal(), field, ok));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 1006);
            return;
        }
        remap("ZX", values.X);


        ++autoprobeValidRecords;

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE: {
            if (autoprobeValidRecords <= 10)
                break;

            qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

            responseState = RESP_PASSIVE_RUN;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            realtimeStateUpdated = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString(describeResponseState());
            info.hash("SampleState").set(describeSampleState());
            info.hash("IssuedCommand").set(issuedCommand.stateDescription());
            event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);

            /* Fall through to normal handling */
            break;
        }

        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE: {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);

            responseState = RESP_PASSIVE_RUN;
            generalStatusUpdated();

            realtimeStateUpdated = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString(describeResponseState());
            info.hash("SampleState").set(describeSampleState());
            info.hash("IssuedCommand").set(issuedCommand.stateDescription());
            event(frameTime, QObject::tr("Passive communications established."), false, info);

            /* Fall through to normal handling */
            break;
        }

        case RESP_INTERACTIVE_START_READ_OZONE_INITIAL: {
            switch (interfaceMode) {
            case Mode_49c_legacy:
            case Mode_Undetermined:
                /* Require at least one response to the optional commands */
                if (autoprobeValidRecords < 2) {
                    qCDebug(log) << "Insufficient disambiguation for interactive startup at"
                                 << Logging::time(frameTime);

                    responseState = RESP_INTERACTIVE_RESTART_WAIT;
                    if (controlStream != NULL)
                        controlStream->resetControl();
                    timeoutAt(FP::undefined());
                    discardData(frameTime + 10.0);
                    issuedCommand.invalidate();
                    commandBacklog.clear();
                    autoprobeValidRecords = 0;

                    {
                        std::lock_guard<std::mutex> lock(mutex);
                        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
                    }
                    autoprobeStatusUpdated();

                    if (realtimeEgress != NULL) {
                        realtimeEgress->incomingData(
                                SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidRecord"),
                                              frameTime, FP::undefined()));
                    }
                    invalidateLogValues(frameTime);
                    return;
                }
                break;
            default:
                break;
            }

            qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

            responseState = RESP_INTERACTIVE_RUN_WAIT;
            timeoutAt(frameTime + 1.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            realtimeStateUpdated = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString(describeResponseState());
            info.hash("SampleState").set(describeSampleState());
            info.hash("IssuedCommand").set(issuedCommand.stateDescription());
            event(frameTime, QObject::tr("Communications established."), false, info);

            return;
        }

        default:
            invalidCommandResponse(frame, frameTime, 1900);
            return;
        }
        break;
    }

    case COMMAND_READ_SAMPLE_P: {
        if (!process.string_start("pres ")) {
            invalidCommandResponse(frame, frameTime, 2000);
            return;
        }
        fields = Util::as_deque(process.split(' '));
        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 2001);
            return;
        }
        fields.pop_front();

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 2002);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root Preport(applyUnitScaling(field.parse_real(&ok), 1.333224));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 2003);
            return;
        }
        if (!FP::defined(Preport.read().toReal())) {
            invalidCommandResponse(frame, frameTime, 2004);
            return;
        }
        remap("ZPreport", Preport);

        bool hit = false;
        Variant::Root Pactual;
        while (!fields.empty()) {
            field = fields.front();
            fields.pop_front();
            if (field == "actual") {
                if (fields.empty()) {
                    invalidCommandResponse(frame, frameTime, 3005);
                    return;
                }
                field = fields.front();
                fields.pop_front();
                Pactual.write().setReal(applyUnitScaling(field.parse_real(&ok), 1.333224));
                if (!ok) {
                    invalidCommandResponse(frame, frameTime, 3006);
                    return;
                }
                if (!FP::defined(Pactual.read().toReal())) {
                    invalidCommandResponse(frame, frameTime, 3007);
                    return;
                }
                remap("ZPactual", Pactual);
                hit = true;
                break;
            }
        }

        switch (pressureCompensation) {
        case Compensation_Unknown:
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            if (hit && Preport.read() != Pactual.read()) {
                pressureCompensation = Compensation_SetInReading;
            } else {
                pressureCompensation = Compensation_ClearInReading;
            }
            break;
        case Compensation_SetInReading:
            if (!hit || (Preport.read() == Pactual.read())) {
                pressureCompensation = Compensation_ClearInReading;
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
            }
            break;
        case Compensation_ClearInReading:
            if (hit && Preport.read() != Pactual.read()) {
                pressureCompensation = Compensation_SetInReading;
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
            }
            break;
        case Compensation_SetInFlags:
        case Compensation_ClearInFlags:
            break;
        }

        if (!compensationStateOn(pressureCompensation)) {
            double check = Preport.read().toDouble();
            if (FP::defined(check) && (!FP::defined(reportingP) || check != reportingP)) {
                reportingP = check;
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
            }
        }

        if (hit) {
            values.P = Pactual;
        } else {
            values.P = Preport;
        }
        remap("P", values.P);


        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        default:
            invalidCommandResponse(frame, frameTime, 2900);
            return;
        }
        break;
    }

    case COMMAND_READ_SAMPLE_T: {
        if (!process.string_start("bench temp ")) {
            invalidCommandResponse(frame, frameTime, 3000);
            return;
        }
        fields = Util::as_deque(process.split(' '));
        if (fields.size() < 2) {
            invalidCommandResponse(frame, frameTime, 3001);
            return;
        }
        fields.pop_front();
        fields.pop_front();

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 3002);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root Treport(field.parse_real(&ok));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 3003);
            return;
        }
        if (!FP::defined(Treport.read().toReal())) {
            invalidCommandResponse(frame, frameTime, 3004);
            return;
        }
        remap("ZTreport", Treport);

        bool hit = false;
        Variant::Root Tactual;
        while (!fields.empty()) {
            field = fields.front();
            fields.pop_front();
            if (field == "actual") {
                if (fields.empty()) {
                    invalidCommandResponse(frame, frameTime, 3005);
                    return;
                }
                field = fields.front();
                fields.pop_front();
                Tactual.write().setReal(field.parse_real(&ok));
                if (!ok) {
                    invalidCommandResponse(frame, frameTime, 3006);
                    return;
                }
                if (!FP::defined(Tactual.read().toReal())) {
                    invalidCommandResponse(frame, frameTime, 3007);
                    return;
                }
                remap("ZTactual", Tactual);
                hit = true;
                break;
            }
        }

        switch (temperatureCompensation) {
        case Compensation_Unknown:
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            if (hit && Treport.read() != Tactual.read()) {
                temperatureCompensation = Compensation_SetInReading;
            } else {
                temperatureCompensation = Compensation_ClearInReading;
            }
            break;
        case Compensation_SetInReading:
            if (!hit || (Treport.read() == Tactual.read())) {
                temperatureCompensation = Compensation_ClearInReading;
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
            }
            break;
        case Compensation_ClearInReading:
            if (hit && Treport.read() != Tactual.read()) {
                temperatureCompensation = Compensation_SetInReading;
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
            }
            break;
        case Compensation_SetInFlags:
        case Compensation_ClearInFlags:
            break;
        }

        if (!compensationStateOn(temperatureCompensation)) {
            double check = Treport.read().toDouble();
            if (FP::defined(check) && (!FP::defined(reportingT) || check != reportingT)) {
                reportingT = check;
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
            }
        }

        if (hit) {
            values.T1 = Tactual;
        } else {
            values.T1 = Treport;
        }
        remap("T1", values.T1);


        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        default:
            invalidCommandResponse(frame, frameTime, 3900);
            return;
        }
        break;
    }

    case COMMAND_READ_LAMP_T: {
        if (!process.string_start("lamp temp ")) {
            invalidCommandResponse(frame, frameTime, 4000);
            return;
        }
        fields = Util::as_deque(process.split(' '));
        if (fields.size() < 2) {
            invalidCommandResponse(frame, frameTime, 4001);
            return;
        }
        fields.pop_front();
        fields.pop_front();

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 4002);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        values.T2.write().setReal(field.parse_real(&ok));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 4003);
            return;
        }
        if (!FP::defined(values.T2.read().toReal())) {
            invalidCommandResponse(frame, frameTime, 4004);
            return;
        }
        remap("T2", values.T2);


        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        default:
            invalidCommandResponse(frame, frameTime, 4900);
            return;
        }
        break;
    }

    case COMMAND_READ_INTENSITY_A: {
        if (!process.string_start("cell a int ")) {
            invalidCommandResponse(frame, frameTime, 5000);
            return;
        }
        fields = Util::as_deque(process.split(' '));
        if (fields.size() < 3) {
            invalidCommandResponse(frame, frameTime, 5001);
            return;
        }
        fields.pop_front();
        fields.pop_front();
        fields.pop_front();

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 5002);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        values.C1.write().setReal(field.parse_real(&ok));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 5003);
            return;
        }
        if (!FP::defined(values.C1.read().toReal())) {
            invalidCommandResponse(frame, frameTime, 5004);
            return;
        }
        remap("C1", values.C1);


        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        default:
            invalidCommandResponse(frame, frameTime, 5900);
            return;
        }
        break;
    }

    case COMMAND_READ_INTENSITY_B: {
        if (!process.string_start("cell b int ")) {
            invalidCommandResponse(frame, frameTime, 6000);
            return;
        }
        fields = Util::as_deque(process.split(' '));
        if (fields.size() < 3) {
            invalidCommandResponse(frame, frameTime, 6001);
            return;
        }
        fields.pop_front();
        fields.pop_front();
        fields.pop_front();

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 6002);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        values.C2.write().setReal(field.parse_real(&ok));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 6003);
            return;
        }
        if (!FP::defined(values.C2.read().toReal())) {
            invalidCommandResponse(frame, frameTime, 6004);
            return;
        }
        remap("C2", values.C2);


        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        default:
            invalidCommandResponse(frame, frameTime, 6900);
            return;
        }
        break;
    }

    case COMMAND_READ_FLOW_A: {
        if (!process.string_start("flow a ")) {
            invalidCommandResponse(frame, frameTime, 7000);
            return;
        }
        fields = Util::as_deque(process.split(' '));
        if (fields.size() < 2) {
            invalidCommandResponse(frame, frameTime, 7001);
            return;
        }
        fields.pop_front();
        fields.pop_front();

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 7002);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        values.Q1.write().setReal(field.parse_real(&ok));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 7003);
            return;
        }
        if (!FP::defined(values.Q1.read().toReal())) {
            invalidCommandResponse(frame, frameTime, 7004);
            return;
        }
        remap("Q1", values.Q1);


        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        default:
            invalidCommandResponse(frame, frameTime, 7900);
            return;
        }
        break;
    }

    case COMMAND_READ_FLOW_B: {
        if (!process.string_start("flow b ")) {
            invalidCommandResponse(frame, frameTime, 8000);
            return;
        }
        fields = Util::as_deque(process.split(' '));
        if (fields.size() < 2) {
            invalidCommandResponse(frame, frameTime, 8001);
            return;
        }
        fields.pop_front();
        fields.pop_front();

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 8002);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        values.Q2.write().setReal(field.parse_real(&ok));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 8003);
            return;
        }
        if (!FP::defined(values.Q2.read().toReal())) {
            invalidCommandResponse(frame, frameTime, 8004);
            return;
        }
        remap("Q2", values.Q2);


        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        default:
            invalidCommandResponse(frame, frameTime, 8900);
            return;
        }
        break;
    }

    case COMMAND_READ_FLAGS: {
        if (!process.string_start("flags ")) {
            invalidCommandResponse(frame, frameTime, 9000);
            return;
        }
        fields = Util::as_deque(process.split(' '));
        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 9001);
            return;
        }
        fields.pop_front();

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 9002);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        qint64 oldFlags = values.flags.read().toInt64();
        values.flags.write().setInt64(field.parse_i64(&ok, 16));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 9003);
            return;
        }
        if (!INTEGER::defined(values.flags.read().toInteger())) {
            invalidCommandResponse(frame, frameTime, 9004);
            return;
        }
        remap("ZFLAGS", values.flags);

        qint64 currentFlags = values.flags.read().toInt64();
        if (INTEGER::defined(oldFlags) &&
                INTEGER::defined(currentFlags) &&
                oldFlags != currentFlags) {
            realtimeStateUpdated = true;
        }

        if (INTEGER::defined(currentFlags)) {
            switch (pressureCompensation) {
            case Compensation_Unknown:
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                if (currentFlags & FLAGS_PRESSURE_COMPENSATION) {
                    pressureCompensation = Compensation_SetInFlags;
                } else {
                    pressureCompensation = Compensation_ClearInFlags;
                }
                break;
            case Compensation_SetInFlags:
            case Compensation_SetInReading:
                if (!(currentFlags & FLAGS_PRESSURE_COMPENSATION)) {
                    pressureCompensation = Compensation_ClearInFlags;
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;
                } else {
                    pressureCompensation = Compensation_SetInFlags;
                }
                break;
            case Compensation_ClearInReading:
            case Compensation_ClearInFlags:
                if (currentFlags & FLAGS_PRESSURE_COMPENSATION) {
                    pressureCompensation = Compensation_SetInFlags;
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;
                } else {
                    pressureCompensation = Compensation_ClearInFlags;
                }
                break;
            }

            switch (temperatureCompensation) {
            case Compensation_Unknown:
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                if (currentFlags & FLAGS_TEMPERATURE_COMPENSATION) {
                    temperatureCompensation = Compensation_SetInFlags;
                } else {
                    temperatureCompensation = Compensation_ClearInFlags;
                }
                break;
            case Compensation_SetInFlags:
            case Compensation_SetInReading:
                if (!(currentFlags & FLAGS_TEMPERATURE_COMPENSATION)) {
                    temperatureCompensation = Compensation_ClearInFlags;
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;
                } else {
                    temperatureCompensation = Compensation_SetInFlags;
                }
                break;
            case Compensation_ClearInReading:
            case Compensation_ClearInFlags:
                if (currentFlags & FLAGS_TEMPERATURE_COMPENSATION) {
                    temperatureCompensation = Compensation_SetInFlags;
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;
                } else {
                    temperatureCompensation = Compensation_ClearInFlags;
                }
                break;
            }
        }

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        case RESP_INTERACTIVE_START_READ_FLAGS:

            handleStartupAdvance(frameTime);
            return;

        default:
            invalidCommandResponse(frame, frameTime, 9900);
            return;
        }
        break;
    }

    case COMMAND_READ_FLOW_OZONATOR: {
        if (Util::contains(process.toString(), "bad cmd")) {
            if (interfaceMode == Mode_Undetermined || interfaceMode == Mode_49c) {
                interfaceMode = Mode_49c_legacy;
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
            }

            switch (responseState) {
            case RESP_PASSIVE_RUN:
            case RESP_PASSIVE_WAIT:
            case RESP_PASSIVE_INITIALIZE:
            case RESP_AUTOPROBE_PASSIVE_WAIT:
            case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
            case RESP_INTERACTIVE_RUN:
            case RESP_INTERACTIVE_RUN_WAIT:
            case RESP_INTERACTIVE_RUN_DELAY:
            case RESP_INTERACTIVE_RESTART_WAIT:
            case RESP_INTERACTIVE_INITIALIZE:
                break;

            case RESP_INTERACTIVE_START_READ_OZONATOR_FLOW:

                handleStartupAdvance(frameTime);
                ++autoprobeValidRecords;
                return;

            default:
                invalidCommandResponse(frame, frameTime, 10901);
                return;
            }

            break;
        }

        if (!process.string_start("oz flow ")) {
            invalidCommandResponse(frame, frameTime, 10000);
            return;
        }
        fields = Util::as_deque(process.split(' '));
        if (fields.size() < 2) {
            invalidCommandResponse(frame, frameTime, 10001);
            return;
        }
        fields.pop_front();
        fields.pop_front();

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 10002);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        values.Q3.write().setReal(field.parse_real(&ok));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 10003);
            return;
        }
        if (!FP::defined(values.Q3.read().toReal())) {
            invalidCommandResponse(frame, frameTime, 10004);
            return;
        }
        remap("Q3", values.Q3);

        if (instrumentMeta["Model"].toString() != "49c") {
            instrumentMeta["Model"].setString("49c");
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }
        if (interfaceMode != Mode_49c) {
            interfaceMode = Mode_49c;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        case RESP_INTERACTIVE_START_READ_OZONATOR_FLOW:

            handleStartupAdvance(frameTime);
            ++autoprobeValidRecords;
            return;

        default:
            invalidCommandResponse(frame, frameTime, 10900);
            return;
        }
        break;
    }

    case COMMAND_READ_OZONATOR_T: {
        if (!process.string_start("o3 lamp temp ")) {
            invalidCommandResponse(frame, frameTime, 11000);
            return;
        }
        fields = Util::as_deque(process.split(' '));
        if (fields.size() < 3) {
            invalidCommandResponse(frame, frameTime, 11001);
            return;
        }
        fields.pop_front();
        fields.pop_front();
        fields.pop_front();

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 11002);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        values.T3.write().setReal(field.parse_real(&ok));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 11003);
            return;
        }
        if (!FP::defined(values.T3.read().toReal())) {
            invalidCommandResponse(frame, frameTime, 11004);
            return;
        }
        remap("T3", values.T3);

        if (instrumentMeta["Model"].toString() != "49i") {
            instrumentMeta["Model"].setString("49i");
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }
        if (interfaceMode != Mode_49i) {
            interfaceMode = Mode_49i;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        default:
            invalidCommandResponse(frame, frameTime, 11900);
            return;
        }
        break;
    }

    case COMMAND_READ_LAMP_V: {
        if (!process.string_start("lamp voltage bench ")) {
            invalidCommandResponse(frame, frameTime, 12000);
            return;
        }
        fields = Util::as_deque(process.split(' '));
        if (fields.size() < 3) {
            invalidCommandResponse(frame, frameTime, 12001);
            return;
        }
        fields.pop_front();
        fields.pop_front();
        fields.pop_front();

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 12002);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        values.V1.write().setReal(field.parse_real(&ok));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 12003);
            return;
        }
        if (!FP::defined(values.V1.read().toReal())) {
            invalidCommandResponse(frame, frameTime, 12004);
            return;
        }
        remap("V1", values.V1);

        if (instrumentMeta["Model"].toString() != "49i") {
            instrumentMeta["Model"].setString("49i");
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }
        if (interfaceMode != Mode_49i) {
            interfaceMode = Mode_49i;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        default:
            invalidCommandResponse(frame, frameTime, 12900);
            return;
        }
        break;
    }

    case COMMAND_READ_OZONATOR_V: {
        if (!process.string_start("lamp voltage oz ")) {
            invalidCommandResponse(frame, frameTime, 13000);
            return;
        }
        fields = Util::as_deque(process.split(' '));
        if (fields.size() < 3) {
            invalidCommandResponse(frame, frameTime, 13001);
            return;
        }
        fields.pop_front();
        fields.pop_front();
        fields.pop_front();

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 13002);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        values.V2.write().setReal(field.parse_real(&ok));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 13003);
            return;
        }
        if (!FP::defined(values.V2.read().toReal())) {
            invalidCommandResponse(frame, frameTime, 13004);
            return;
        }
        remap("V2", values.V2);

        if (instrumentMeta["Model"].toString() != "49i") {
            instrumentMeta["Model"].setString("49i");
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }
        if (interfaceMode != Mode_49i) {
            interfaceMode = Mode_49i;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        default:
            invalidCommandResponse(frame, frameTime, 13900);
            return;
        }
        break;
    }

    case COMMAND_READ_MOTHERBOARD_V: {
        if (!process.string_start("diag volt mb ")) {
            invalidCommandResponse(frame, frameTime, 14000);
            return;
        }
        fields = Util::as_deque(process.split(' '));
        if (fields.size() < 3) {
            invalidCommandResponse(frame, frameTime, 14001);
            return;
        }
        fields.pop_front();
        fields.pop_front();
        fields.pop_front();

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 14002);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        values.ZMBP24V.write().setReal(field.parse_real(&ok));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 14003);
            return;
        }
        if (!FP::defined(values.ZMBP24V.read().toReal())) {
            invalidCommandResponse(frame, frameTime, 14004);
            return;
        }
        remap("ZMBP24V", values.ZMBP24V);

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 14005);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        values.ZMBP15V.write().setReal(field.parse_real(&ok));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 14006);
            return;
        }
        if (!FP::defined(values.ZMBP15V.read().toReal())) {
            invalidCommandResponse(frame, frameTime, 14007);
            return;
        }
        remap("ZMBP15V", values.ZMBP15V);

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 14008);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        values.ZMBP5V.write().setReal(field.parse_real(&ok));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 14009);
            return;
        }
        if (!FP::defined(values.ZMBP5V.read().toReal())) {
            invalidCommandResponse(frame, frameTime, 14010);
            return;
        }
        remap("ZMBP5V", values.ZMBP5V);

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 14011);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        values.ZMBP3V.write().setReal(field.parse_real(&ok));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 14012);
            return;
        }
        if (!FP::defined(values.ZMBP3V.read().toReal())) {
            invalidCommandResponse(frame, frameTime, 14013);
            return;
        }
        remap("ZMBP3V", values.ZMBP3V);

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 14014);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        values.ZMBN3V.write().setReal(field.parse_real(&ok));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 14015);
            return;
        }
        if (!FP::defined(values.ZMBN3V.read().toReal())) {
            invalidCommandResponse(frame, frameTime, 14016);
            return;
        }
        remap("ZMBN3V", values.ZMBN3V);

        if (instrumentMeta["Model"].toString() != "49i") {
            instrumentMeta["Model"].setString("49i");
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }
        if (interfaceMode != Mode_49i) {
            interfaceMode = Mode_49i;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        default:
            invalidCommandResponse(frame, frameTime, 14900);
            return;
        }
        break;
    }

    case COMMAND_READ_INTERFACE_V: {
        if (!process.string_start("diag volt mib ")) {
            invalidCommandResponse(frame, frameTime, 15000);
            return;
        }
        fields = Util::as_deque(process.split(' '));
        if (fields.size() < 3) {
            invalidCommandResponse(frame, frameTime, 15001);
            return;
        }
        fields.pop_front();
        fields.pop_front();
        fields.pop_front();

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 15002);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        values.ZMEP24V.write().setReal(field.parse_real(&ok));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 15003);
            return;
        }
        if (!FP::defined(values.ZMEP24V.read().toReal())) {
            invalidCommandResponse(frame, frameTime, 15004);
            return;
        }
        remap("ZMEP24V", values.ZMEP24V);

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 15005);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        values.ZMEP15V.write().setReal(field.parse_real(&ok));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 15006);
            return;
        }
        if (!FP::defined(values.ZMEP15V.read().toReal())) {
            invalidCommandResponse(frame, frameTime, 15007);
            return;
        }
        remap("ZMEP15V", values.ZMEP15V);

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 15008);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        values.ZMEN15V.write().setReal(field.parse_real(&ok));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 15009);
            return;
        }
        if (!FP::defined(values.ZMEN15V.read().toReal())) {
            invalidCommandResponse(frame, frameTime, 15010);
            return;
        }
        remap("ZMEN15V", values.ZMEN15V);

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 15011);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        values.ZMEP5V.write().setReal(field.parse_real(&ok));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 15012);
            return;
        }
        if (!FP::defined(values.ZMEP5V.read().toReal())) {
            invalidCommandResponse(frame, frameTime, 15013);
            return;
        }
        remap("ZMEP5V", values.ZMEP5V);

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 15014);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        values.ZMEP3V.write().setReal(field.parse_real(&ok));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 15015);
            return;
        }
        if (!FP::defined(values.ZMEP3V.read().toReal())) {
            invalidCommandResponse(frame, frameTime, 15016);
            return;
        }
        remap("ZMEP3V", values.ZMEP3V);

        /* Another +15V, ignored */
        if (!fields.empty())
            fields.pop_front();

        if (instrumentMeta["Model"].toString() != "49i") {
            instrumentMeta["Model"].setString("49i");
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }
        if (interfaceMode != Mode_49i) {
            interfaceMode = Mode_49i;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        default:
            invalidCommandResponse(frame, frameTime, 15900);
            return;
        }
        break;
    }

    case COMMAND_READ_IO_V: {
        if (!process.string_start("diag volt iob ")) {
            invalidCommandResponse(frame, frameTime, 16000);
            return;
        }
        fields = Util::as_deque(process.split(' '));
        if (fields.size() < 3) {
            invalidCommandResponse(frame, frameTime, 16001);
            return;
        }
        fields.pop_front();
        fields.pop_front();
        fields.pop_front();

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 16002);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        values.ZIOP24V.write().setReal(field.parse_real(&ok));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 16003);
            return;
        }
        if (!FP::defined(values.ZIOP24V.read().toReal())) {
            invalidCommandResponse(frame, frameTime, 16004);
            return;
        }
        remap("ZIOP24V", values.ZIOP24V);

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 16005);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        values.ZIOP5V.write().setReal(field.parse_real(&ok));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 16006);
            return;
        }
        if (!FP::defined(values.ZIOP5V.read().toReal())) {
            invalidCommandResponse(frame, frameTime, 16007);
            return;
        }
        remap("ZIOP5V", values.ZIOP5V);

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 16008);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        values.ZIOP3V.write().setReal(field.parse_real(&ok));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 16009);
            return;
        }
        if (!FP::defined(values.ZIOP3V.read().toReal())) {
            invalidCommandResponse(frame, frameTime, 16010);
            return;
        }
        remap("ZIOP3V", values.ZIOP3V);

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 16011);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        values.ZION3V.write().setReal(field.parse_real(&ok));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 16012);
            return;
        }
        if (!FP::defined(values.ZION3V.read().toReal())) {
            invalidCommandResponse(frame, frameTime, 16013);
            return;
        }
        remap("ZION3V", values.ZION3V);

        if (instrumentMeta["Model"].toString() != "49i") {
            instrumentMeta["Model"].setString("49i");
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }
        if (interfaceMode != Mode_49i) {
            interfaceMode = Mode_49i;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        default:
            invalidCommandResponse(frame, frameTime, 15900);
            return;
        }
        break;
    }

    case COMMAND_SAMPLE_AMBIENT:
        if (process != "set sample ok") {
            invalidCommandResponse(frame, frameTime, 17000);
            return;
        }

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
            switch (sampleState) {
            case SAMPLE_RUN:
            case SAMPLE_ZERO_END_FLUSH:
            case SAMPLE_SPANCHECK_ENDZERO_END_FLUSH:
                break;

            case SAMPLE_ZERO_BEGIN_FLUSH:
            case SAMPLE_ZERO_MEASURE:
                qCDebug(log) << "Ending zero sampling at" << Logging::time(frameTime);

                sampleState = SAMPLE_SPANCHECK_ENDZERO_END_FLUSH;
                sampleStateBeginTime = frameTime;
                break;

            case SAMPLE_SPANCHECK_BEGINZERO_FLUSH:
            case SAMPLE_SPANCHECK_BEGINZERO_MEASURE:
            case SAMPLE_SPANCHECK_LEVEL_FLUSH:
            case SAMPLE_SPANCHECK_LEVEL_MEASURE:
            case SAMPLE_SPANCHECK_ENDZERO_BEGIN_FLUSH:
            case SAMPLE_SPANCHECK_ENDZERO_MEASURE:
                qCDebug(log) << "Ending spancheck zeroing at" << Logging::time(frameTime);

                sampleState = SAMPLE_SPANCHECK_ENDZERO_END_FLUSH;
                sampleStateBeginTime = frameTime;
                break;
            }
            break;

        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        case RESP_INTERACTIVE_START_SET_MODE:

            handleStartupAdvance(frameTime);
            return;

        default:
            invalidCommandResponse(frame, frameTime, 17900);
            return;
        }
        break;

    case COMMAND_SAMPLE_ZERO:
        if (process != "set zero ok") {
            invalidCommandResponse(frame, frameTime, 18000);
            return;
        }

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
            switch (sampleState) {
            case SAMPLE_RUN:
                qCDebug(log) << "Starting zero at" << Logging::time(frameTime);

                sampleState = SAMPLE_ZERO_BEGIN_FLUSH;
                sampleStateBeginTime = frameTime;
                break;

            case SAMPLE_ZERO_END_FLUSH:
                qCDebug(log) << "Restarting zero at" << Logging::time(frameTime);
                sampleState = SAMPLE_ZERO_BEGIN_FLUSH;
                sampleStateBeginTime = frameTime;
                break;

            case SAMPLE_SPANCHECK_ENDZERO_END_FLUSH:
                qCDebug(log) << "Restarting spancheck zeroing at" << Logging::time(frameTime);
                sampleState = SAMPLE_SPANCHECK_ENDZERO_BEGIN_FLUSH;
                sampleStateBeginTime = frameTime;
                break;

            case SAMPLE_ZERO_BEGIN_FLUSH:
            case SAMPLE_ZERO_MEASURE:
                break;

            case SAMPLE_SPANCHECK_BEGINZERO_FLUSH:
            case SAMPLE_SPANCHECK_BEGINZERO_MEASURE:
                break;

            case SAMPLE_SPANCHECK_LEVEL_FLUSH:
            case SAMPLE_SPANCHECK_LEVEL_MEASURE:
                qCDebug(log) << "Ending spancheck levels at" << Logging::time(frameTime);

                sampleState = SAMPLE_SPANCHECK_BEGINZERO_FLUSH;
                sampleStateBeginTime = frameTime;
                break;

            case SAMPLE_SPANCHECK_ENDZERO_BEGIN_FLUSH:
            case SAMPLE_SPANCHECK_ENDZERO_MEASURE:
                break;
            }
            break;

        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        case RESP_INTERACTIVE_START_SET_MODE:

            handleStartupAdvance(frameTime);
            return;

        default:
            invalidCommandResponse(frame, frameTime, 18900);
            return;
        }
        break;

    case COMMAND_SAMPLE_LEVEL:
        if (process != issuedCommand.data().toQByteArray().simplified() + " ok") {
            invalidCommandResponse(frame, frameTime, 19000);
            return;
        }

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE: {
            int level = -1;
            if (process.string_start("set level ")) {
                fields = Util::as_deque(process.split(' '));
                if (fields.size() > 3) {
                    level = fields[2].parse_i32(&ok);
                    if (!ok)
                        level = -1;
                }
            }

            switch (sampleState) {
            case SAMPLE_RUN:
            case SAMPLE_ZERO_BEGIN_FLUSH:
            case SAMPLE_ZERO_MEASURE:
            case SAMPLE_ZERO_END_FLUSH:
            case SAMPLE_SPANCHECK_BEGINZERO_FLUSH:
            case SAMPLE_SPANCHECK_BEGINZERO_MEASURE:
            case SAMPLE_SPANCHECK_ENDZERO_BEGIN_FLUSH:
            case SAMPLE_SPANCHECK_ENDZERO_MEASURE:
            case SAMPLE_SPANCHECK_ENDZERO_END_FLUSH:
                sampleStateIndex = config.first().spancheckLevels.indexOf(level);
                resetSpancheckLevels();

                qCDebug(log) << "Switching to spancheck level index" << sampleStateIndex << "at"
                             << Logging::time(frameTime) << "from sample state" << sampleState;

                sampleState = SAMPLE_SPANCHECK_LEVEL_FLUSH;
                sampleStateBeginTime = frameTime;
                break;

            case SAMPLE_SPANCHECK_LEVEL_FLUSH:
            case SAMPLE_SPANCHECK_LEVEL_MEASURE:
                if (sampleStateIndex >= 0) {
                    sampleStateIndex =
                            config.first().spancheckLevels.indexOf(level, sampleStateIndex + 1);
                } else {
                    sampleStateIndex = config.first().spancheckLevels.indexOf(level);
                }

                qCDebug(log) << "Changing to spancheck level index" << sampleStateIndex << "at"
                             << Logging::time(frameTime);

                sampleState = SAMPLE_SPANCHECK_BEGINZERO_FLUSH;
                sampleStateBeginTime = frameTime;
                break;
            }
            break;
        }

        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        case RESP_INTERACTIVE_START_SET_MODE:

            handleStartupAdvance(frameTime);
            return;

        default:
            invalidCommandResponse(frame, frameTime, 19900);
            return;
        }
        break;

    case COMMAND_SET_REMOTE:
        if (process != "set mode remote ok") {
            invalidCommandResponse(frame, frameTime, 20000);
            return;
        }

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        case RESP_INTERACTIVE_START_SET_REMOTE_INITIAL:
        case RESP_INTERACTIVE_START_SET_REMOTE_CONFIRM:
            responseState = RESP_INTERACTIVE_START_SET_REMOTE_CONFIRM;

            timeoutAt(frameTime + 10.0);
            discardData(frameTime + 0.5);
            issuedCommand.invalidate();
            commandBacklog.clear();
            autoprobeValidRecords = 0;
            return;

        default:
            invalidCommandResponse(frame, frameTime, 20900);
            return;
        }
        break;

    case COMMAND_SET_FORMAT:
        if (process != issuedCommand.data().toQByteArray().simplified() + " ok") {
            invalidCommandResponse(frame, frameTime, 21000);
            return;
        }

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        case RESP_INTERACTIVE_START_SET_FORMAT:

            handleStartupAdvance(frameTime);
            return;

        default:
            invalidCommandResponse(frame, frameTime, 21900);
            return;
        }
        break;

    case COMMAND_SET_GAS_UNITS:
        if (process != "set gas unit ppb ok") {
            invalidCommandResponse(frame, frameTime, 22000);
            return;
        }

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        case RESP_INTERACTIVE_START_SET_GAS_UNITS:

            handleStartupAdvance(frameTime);
            return;

        default:
            invalidCommandResponse(frame, frameTime, 22900);
            return;
        }
        break;

    case COMMAND_SET_AVERAGE_TIME:
        if (process != issuedCommand.data().toQByteArray().simplified() + " ok") {
            invalidCommandResponse(frame, frameTime, 23000);
            return;
        }

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        case RESP_INTERACTIVE_START_SET_AVERAGING_TIME:

            handleStartupAdvance(frameTime);
            ++autoprobeValidRecords;
            return;

        default:
            invalidCommandResponse(frame, frameTime, 23900);
            return;
        }
        break;

    case COMMAND_SET_ENABLE_T_COMP:
        if (process != "set temp comp on ok") {
            invalidCommandResponse(frame, frameTime, 24000);
            return;
        }

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        case RESP_INTERACTIVE_START_SET_TEMPERATURE_COMPENSATION:

            handleStartupAdvance(frameTime);
            ++autoprobeValidRecords;
            return;

        default:
            invalidCommandResponse(frame, frameTime, 24900);
            return;
        }
        break;

    case COMMAND_SET_ENABLE_P_COMP:
        if (process != "set pres comp on ok") {
            invalidCommandResponse(frame, frameTime, 25000);
            return;
        }

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        case RESP_INTERACTIVE_START_SET_PRESSURE_COMPENSATION:

            handleStartupAdvance(frameTime);
            ++autoprobeValidRecords;
            return;

        default:
            invalidCommandResponse(frame, frameTime, 25900);
            return;
        }
        break;

    case COMMAND_SET_DATE:
        if (process != issuedCommand.data().toQByteArray().simplified() + " ok") {
            invalidCommandResponse(frame, frameTime, 26000);
            return;
        }

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        case RESP_INTERACTIVE_START_SET_DATE:

            handleStartupAdvance(frameTime);
            ++autoprobeValidRecords;
            return;

        default:
            invalidCommandResponse(frame, frameTime, 26900);
            return;
        }
        break;

    case COMMAND_SET_TIME:
        if (process != issuedCommand.data().toQByteArray().simplified() + " ok") {
            invalidCommandResponse(frame, frameTime, 27000);
            return;
        }

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        case RESP_INTERACTIVE_START_SET_TIME:

            handleStartupAdvance(frameTime);
            ++autoprobeValidRecords;
            return;

        default:
            invalidCommandResponse(frame, frameTime, 27900);
            return;
        }
        break;

    case COMMAND_READ_PROGRAM_NUMBER: {
        if (!process.string_start("program no")) {
            invalidCommandResponse(frame, frameTime, 50000);
            return;
        }

        QString toMatch(frame.toQString(false));

        QRegExp reI("iSeries\\s+49\\S*\\s+(\\S+)", Qt::CaseInsensitive);
        QRegExp reC("processor\\s+49\\S*\\s+(\\S+)\\s+[\\s\\d]*c?link\\s+49\\D*\\s*(\\S+)",
                    Qt::CaseInsensitive);
        if (reI.indexIn(toMatch) != -1) {
            if (instrumentMeta["Model"].toString() != "49i") {
                instrumentMeta["Model"].setString("49i");
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }

            if (instrumentMeta["FirmwareVersion"].toQString() != reI.cap(1)) {
                instrumentMeta["FirmwareVersion"].setString(reI.cap(1));
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }

            if (interfaceMode != Mode_49i) {
                interfaceMode = Mode_49i;
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
            }
        } else if (reC.indexIn(toMatch) != -1) {
            if (instrumentMeta["Model"].toString() != "49c") {
                instrumentMeta["Model"].setString("49c");
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }

            if (instrumentMeta["FirmwareVersion"].toQString() != reC.cap(1)) {
                instrumentMeta["FirmwareVersion"].setString(reC.cap(1));
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }

            if (instrumentMeta["LinkVersion"].toQString() != reC.cap(2)) {
                instrumentMeta["LinkVersion"].setString(reC.cap(2));
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }

            if (interfaceMode != Mode_49c) {
                interfaceMode = Mode_49c;
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
            }
        } else {
            invalidCommandResponse(frame, frameTime, 50001);
            return;
        }

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        case RESP_INTERACTIVE_START_READ_PROGRAM_NUMBER:

            handleStartupAdvance(frameTime);
            ++autoprobeValidRecords;
            return;

        default:
            invalidCommandResponse(frame, frameTime, 50900);
            return;
        }
        break;
    }

    case COMMAND_READ_OZONEBACKGROUND: {
        if (!process.string_start("o3 bkg ")) {
            invalidCommandResponse(frame, frameTime, 51000);
            return;
        }
        fields = Util::as_deque(process.split(' '));
        if (fields.size() < 2) {
            invalidCommandResponse(frame, frameTime, 51001);
            return;
        }
        fields.pop_front();
        fields.pop_front();

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 51002);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        double v = field.parse_real(&ok);
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 51003);
            return;
        }
        if (!FP::defined(v)) {
            invalidCommandResponse(frame, frameTime, 51004);
            return;
        }

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 51005);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        v = convertOzoneUnits(v, field, ok);
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 51006);
            return;
        }

        if (!FP::equal(v, ozoneBackground)) {
            ozoneBackground = v;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        case RESP_INTERACTIVE_START_READ_OZONE_BACKGROUND:

            handleStartupAdvance(frameTime);
            ++autoprobeValidRecords;
            return;

        default:
            invalidCommandResponse(frame, frameTime, 51900);
            return;
        }
        break;
    }

    case COMMAND_READ_OZONECOEFFICIENT: {
        if (!process.string_start("o3 coef ")) {
            invalidCommandResponse(frame, frameTime, 52000);
            return;
        }
        fields = Util::as_deque(process.split(' '));
        if (fields.size() < 2) {
            invalidCommandResponse(frame, frameTime, 52001);
            return;
        }
        fields.pop_front();
        fields.pop_front();

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 52002);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        double v = field.parse_real(&ok);
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 52003);
            return;
        }
        if (!FP::defined(v)) {
            invalidCommandResponse(frame, frameTime, 52004);
            return;
        }

        if (!FP::equal(v, ozoneCoefficient)) {
            ozoneCoefficient = v;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        case RESP_INTERACTIVE_START_READ_OZONE_COEFFICIENT:

            handleStartupAdvance(frameTime);
            ++autoprobeValidRecords;
            return;

        default:
            invalidCommandResponse(frame, frameTime, 52900);
            return;
        }
        break;
    }

    case COMMAND_READ_LAMP_SETTTING: {
        if (!process.string_start("lamp setting ")) {
            invalidCommandResponse(frame, frameTime, 53000);
            return;
        }
        fields = Util::as_deque(process.split(' '));
        if (fields.size() < 2) {
            invalidCommandResponse(frame, frameTime, 53001);
            return;
        }
        fields.pop_front();
        fields.pop_front();

        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 53002);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (field.string_end("%"))
            field = field.mid(0, field.size() - 1);
        values.PCT.write().setReal(field.parse_real(&ok));
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 53003);
            return;
        }
        if (!FP::defined(values.PCT.read().toReal())) {
            invalidCommandResponse(frame, frameTime, 53004);
            return;
        }
        remap("PCT", values.PCT);

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        case RESP_INTERACTIVE_START_READ_LAMP_SETTING:

            handleStartupAdvance(frameTime);
            ++autoprobeValidRecords;
            return;

        default:
            invalidCommandResponse(frame, frameTime, 53900);
            return;
        }
        break;
    }

    case COMMAND_READ_HOST_NAME: {
        if (!process.string_start("host name")) {
            invalidCommandResponse(frame, frameTime, 54000);
            return;
        }

        auto check = Util::ByteView(frame).string_trimmed();
        if (check.size() > 10) {
            QString host(QString::fromLatin1(check.mid(10).toQByteArrayRef()).trimmed());
            if (!host.isEmpty()) {
                if (instrumentMeta["HostName"].toQString() != host) {
                    instrumentMeta["HostName"].setString(host);
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;
                    sourceMetadataUpdated();
                }
            }
        }

        if (instrumentMeta["Model"].toString() != "49i") {
            instrumentMeta["Model"].setString("49i");
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }
        if (interfaceMode != Mode_49i) {
            interfaceMode = Mode_49i;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        case RESP_INTERACTIVE_START_READ_HOST_NAME:

            handleStartupAdvance(frameTime);
            return;

        default:
            invalidCommandResponse(frame, frameTime, 54900);
            return;
        }
        break;
    }

    case COMMAND_READ_OZONATOR_LEVEL: {
        if (!process.string_start("l")) {
            invalidCommandResponse(frame, frameTime, 55000);
            return;
        }
        int level = process.mid(1, 2).string_trimmed().parse_i32(&ok);
        if (!ok || level < 1) {
            invalidCommandResponse(frame, frameTime, 55001);
            return;
        }

        field = process.mid(2).string_trimmed();
        fields = Util::as_deque(field.split(' '));
        if (fields.empty()) {
            invalidCommandResponse(frame, frameTime, 55002);
            return;
        }
        field = fields.front().string_trimmed();
        fields.pop_front();
        if (field.string_end("%")) {
            field = field.mid(0, field.size() - 1).string_trimmed();
        }
        double v = field.parse_real(&ok);
        if (!ok) {
            invalidCommandResponse(frame, frameTime, 55003);
            return;
        }
        if (!FP::defined(v)) {
            invalidCommandResponse(frame, frameTime, 55004);
            return;
        }

        if (!fields.empty()) {
            fields.pop_front();
        }

        if (!FP::equal(ozonatorDrive.value(level, FP::undefined()), v)) {
            ozonatorDrive.insert(level, v);
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        case RESP_INTERACTIVE_START_READ_OZONATOR_LEVELS:

            handleStartupAdvance(frameTime);
            return;

        default:
            invalidCommandResponse(frame, frameTime, 55900);
            return;
        }
        break;
    }

    case COMMAND_SET_PUMP_ON:
        if (process != "set pump on ok") {
            invalidCommandResponse(frame, frameTime, 80000);
            return;
        }

        if (instrumentMeta["Model"].toString() != "49i") {
            instrumentMeta["Model"].setString("49i");
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }
        if (interfaceMode != Mode_49i) {
            interfaceMode = Mode_49i;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        default:
            invalidCommandResponse(frame, frameTime, 80900);
            return;
        }
        break;

    case COMMAND_SET_PUMP_OFF:
        if (process != "set pump off ok") {
            invalidCommandResponse(frame, frameTime, 80000);
            return;
        }

        if (instrumentMeta["Model"].toString() != "49i") {
            instrumentMeta["Model"].setString("49i");
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }
        if (interfaceMode != Mode_49i) {
            interfaceMode = Mode_49i;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }

        switch (responseState) {
        case RESP_PASSIVE_RUN:
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_INTERACTIVE_RUN:
        case RESP_INTERACTIVE_RUN_WAIT:
        case RESP_INTERACTIVE_RUN_DELAY:
        case RESP_INTERACTIVE_RESTART_WAIT:
        case RESP_INTERACTIVE_INITIALIZE:
            break;

        default:
            invalidCommandResponse(frame, frameTime, 80900);
            return;
        }
        break;

    }


    /* Second stage: handle the response state/operating mode and transitions
     * based on the response. */
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RUN:
        /* Handling continues below */
        break;

    case RESP_INTERACTIVE_RUN_DELAY:
    case RESP_INTERACTIVE_RUN_WAIT:
        responseState = RESP_INTERACTIVE_RUN;
        /* Handling continues below */
        break;

    case RESP_PASSIVE_INITIALIZE:
        responseState = RESP_PASSIVE_WAIT;
        /* Handling continues below */
        break;

    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
        /* Handling continues below */
        break;

    case RESP_INTERACTIVE_START_SET_REMOTE_INITIAL:
    case RESP_INTERACTIVE_START_SET_REMOTE_CONFIRM:
    case RESP_INTERACTIVE_START_READ_PROGRAM_NUMBER:
    case RESP_INTERACTIVE_START_SET_FORMAT:
    case RESP_INTERACTIVE_START_SET_AVERAGING_TIME:
    case RESP_INTERACTIVE_START_SET_GAS_UNITS:
    case RESP_INTERACTIVE_START_SET_TEMPERATURE_COMPENSATION:
    case RESP_INTERACTIVE_START_SET_PRESSURE_COMPENSATION:
    case RESP_INTERACTIVE_START_SET_MODE:
    case RESP_INTERACTIVE_START_SET_DATE:
    case RESP_INTERACTIVE_START_SET_TIME:
    case RESP_INTERACTIVE_START_READ_OZONE_BACKGROUND:
    case RESP_INTERACTIVE_START_READ_OZONE_COEFFICIENT:
    case RESP_INTERACTIVE_START_READ_FLAGS:
    case RESP_INTERACTIVE_START_READ_LAMP_SETTING:
    case RESP_INTERACTIVE_START_READ_HOST_NAME:
    case RESP_INTERACTIVE_START_READ_OZONATOR_LEVELS:
    case RESP_INTERACTIVE_START_READ_OZONATOR_FLOW:
    case RESP_INTERACTIVE_START_READ_OZONE_INITIAL:
        /* These should all be caught above. */
        invalidCommandResponse(frame, frameTime, 1);
        return;

    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        /* If we get here it's because something else overrode our control,
         * so just retry the startup in a moment. */
        issuedCommand.invalidate();
        commandBacklog.clear();
        timeoutAt(frameTime + 1.0);
        return;
    }

    /* Now we know we're in a normal operating mode. */

    if (loggingEgress == NULL) {
        haveEmittedLogMeta = false;
        loggingMux.clear();
        memset(streamAge, 0, sizeof(streamAge));
        for (int i = 0; i < LogStream_TOTAL; i++) {
            streamTime[i] = FP::undefined();
        }
    } else {
        if (!haveEmittedLogMeta) {
            haveEmittedLogMeta = true;
            loggingMux.incoming(LogStream_Metadata, buildLogMeta(frameTime), loggingEgress);
        }
        loggingMux.advance(LogStream_Metadata, frameTime, loggingEgress);
    }
    if (!haveEmittedRealtimeMeta && realtimeEgress != NULL) {
        haveEmittedRealtimeMeta = true;
        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }

    if (realtimeStateUpdated && realtimeEgress != NULL) {
        realtimeStateUpdated = false;

        Variant::Root state;
        switch (sampleState) {
        case SAMPLE_RUN:
            if (INTEGER::defined(values.flags.read().toInt64()) &&
                    (values.flags.read().toInt64() & FLAGS_ALARM_MASK) != 0) {
                state.write().setString("Alarm");
            } else {
                state.write().setString("Run");
            }
            break;

        case SAMPLE_ZERO_BEGIN_FLUSH:
        case SAMPLE_ZERO_END_FLUSH:
            state.write().setString("Blank");
            break;

        case SAMPLE_ZERO_MEASURE:
            state.write().setString("Zero");
            break;

        case SAMPLE_SPANCHECK_BEGINZERO_FLUSH:
        case SAMPLE_SPANCHECK_BEGINZERO_MEASURE:
        case SAMPLE_SPANCHECK_ENDZERO_BEGIN_FLUSH:
        case SAMPLE_SPANCHECK_ENDZERO_MEASURE:
        case SAMPLE_SPANCHECK_ENDZERO_END_FLUSH:
        case SAMPLE_SPANCHECK_LEVEL_FLUSH:
        case SAMPLE_SPANCHECK_LEVEL_MEASURE:
            state.write().setString("Spancheck");
            break;
        }

        realtimeEgress->emplaceData(SequenceName({}, "raw", "ZSTATE"), std::move(state), frameTime,
                                    FP::undefined());
    }

    /* Third stage: update values */
    switch (issuedCommand.type()) {
    case COMMAND_IGNORED:
        break;

    case COMMAND_SAMPLE_AMBIENT:
    case COMMAND_SAMPLE_ZERO:
    case COMMAND_SAMPLE_LEVEL:
    case COMMAND_SET_FORMAT:
    case COMMAND_SET_PUMP_ON:
    case COMMAND_SET_PUMP_OFF:
    case COMMAND_SET_REMOTE:
    case COMMAND_SET_GAS_UNITS:
    case COMMAND_SET_AVERAGE_TIME:
    case COMMAND_SET_ENABLE_T_COMP:
    case COMMAND_SET_ENABLE_P_COMP:
    case COMMAND_SET_DATE:
    case COMMAND_SET_TIME:
        /* Just ACK responses */
        break;

    case COMMAND_READ_PROGRAM_NUMBER:
    case COMMAND_READ_OZONEBACKGROUND:
    case COMMAND_READ_OZONECOEFFICIENT:
    case COMMAND_READ_HOST_NAME:
    case COMMAND_READ_OZONATOR_LEVEL:
        /* Real responses, but handled above (metadata mostly) */
        break;

    case COMMAND_READ_LAMP_SETTTING:
        realtimeValue(frameTime, "PCT", values.PCT);
        break;

    case COMMAND_READ_OZONE:
        realtimeValue(frameTime, "ZX", values.X);
        remap("X", values.X);

        switch (sampleState) {
        case SAMPLE_RUN:

            logValue(frameTime, "X", values.X, LogStream_Ozone);
            break;

        case SAMPLE_ZERO_BEGIN_FLUSH:
        case SAMPLE_ZERO_END_FLUSH:
        case SAMPLE_SPANCHECK_BEGINZERO_FLUSH:
        case SAMPLE_SPANCHECK_LEVEL_FLUSH:
        case SAMPLE_SPANCHECK_ENDZERO_BEGIN_FLUSH:
        case SAMPLE_SPANCHECK_ENDZERO_END_FLUSH:

            realtimeValue(frameTime, "X", Variant::Root(FP::undefined()));

            loggingMux.advance(LogStream_Ozone, frameTime, loggingEgress);
            streamTime[LogStream_Ozone] = frameTime;
            streamAge[LogStream_Ozone] = 1;
            break;

        case SAMPLE_ZERO_MEASURE:
        case SAMPLE_SPANCHECK_BEGINZERO_MEASURE:
        case SAMPLE_SPANCHECK_ENDZERO_MEASURE:

            realtimeValue(frameTime, "X", Variant::Root(FP::undefined()));

            zeroMeasurement->concentration
                           ->add(values.X.write().toReal(), streamTime[LogStream_Ozone], frameTime);
            zeroUpdated = true;

            loggingMux.advance(LogStream_Ozone, frameTime, loggingEgress);
            streamTime[LogStream_Ozone] = frameTime;
            streamAge[LogStream_Ozone] = 1;
            break;

        case SAMPLE_SPANCHECK_LEVEL_MEASURE:

            realtimeValue(frameTime, "X", Variant::Root(FP::undefined()));

            if (sampleStateIndex >= 0 && sampleStateIndex < levelMeasurement.size()) {
                levelMeasurement[sampleStateIndex]->concentration
                                                  ->add(values.X.write().toReal(),
                                                        streamTime[LogStream_Ozone], frameTime);
                spanUpdated = true;
            }

            loggingMux.advance(LogStream_Ozone, frameTime, loggingEgress);
            streamTime[LogStream_Ozone] = frameTime;
            streamAge[LogStream_Ozone] = 1;
            break;
        }
        break;

    case COMMAND_READ_SAMPLE_P:
        switch (sampleState) {
        case SAMPLE_RUN:
            break;

        case SAMPLE_ZERO_BEGIN_FLUSH:
        case SAMPLE_ZERO_END_FLUSH:
        case SAMPLE_SPANCHECK_BEGINZERO_FLUSH:
        case SAMPLE_SPANCHECK_LEVEL_FLUSH:
        case SAMPLE_SPANCHECK_ENDZERO_BEGIN_FLUSH:
        case SAMPLE_SPANCHECK_ENDZERO_END_FLUSH:
            break;

        case SAMPLE_ZERO_MEASURE:
        case SAMPLE_SPANCHECK_BEGINZERO_MEASURE:
        case SAMPLE_SPANCHECK_ENDZERO_MEASURE:
            zeroMeasurement->samplePressure
                           ->add(values.P.write().toReal(), streamTime[LogStream_SampleP],
                                 frameTime);
            break;

        case SAMPLE_SPANCHECK_LEVEL_MEASURE:
            if (sampleStateIndex >= 0 && sampleStateIndex < levelMeasurement.size()) {
                levelMeasurement[sampleStateIndex]->samplePressure
                                                  ->add(values.P.write().toReal(),
                                                        streamTime[LogStream_SampleP], frameTime);
            }
            break;
        }

        logValue(frameTime, "P", values.P, LogStream_SampleP);
        break;

    case COMMAND_READ_SAMPLE_T:
        switch (sampleState) {
        case SAMPLE_RUN:
            break;

        case SAMPLE_ZERO_BEGIN_FLUSH:
        case SAMPLE_ZERO_END_FLUSH:
        case SAMPLE_SPANCHECK_BEGINZERO_FLUSH:
        case SAMPLE_SPANCHECK_LEVEL_FLUSH:
        case SAMPLE_SPANCHECK_ENDZERO_BEGIN_FLUSH:
        case SAMPLE_SPANCHECK_ENDZERO_END_FLUSH:
            break;

        case SAMPLE_ZERO_MEASURE:
        case SAMPLE_SPANCHECK_BEGINZERO_MEASURE:
        case SAMPLE_SPANCHECK_ENDZERO_MEASURE:
            zeroMeasurement->sampleTemperature
                           ->add(values.T1.write().toReal(), streamTime[LogStream_SampleT],
                                 frameTime);
            break;

        case SAMPLE_SPANCHECK_LEVEL_MEASURE:
            if (sampleStateIndex >= 0 && sampleStateIndex < levelMeasurement.size()) {
                levelMeasurement[sampleStateIndex]->sampleTemperature
                                                  ->add(values.T1.write().toReal(),
                                                        streamTime[LogStream_SampleT], frameTime);
            }
            break;
        }

        logValue(frameTime, "T1", values.T1, LogStream_SampleT);
        break;

    case COMMAND_READ_LAMP_T:
        logValue(frameTime, "T2", values.T2, LogStream_LampT);
        break;
    case COMMAND_READ_INTENSITY_A:
        logValue(frameTime, "C1", values.C1, LogStream_IntensityA);
        break;
    case COMMAND_READ_INTENSITY_B:
        logValue(frameTime, "C2", values.C2, LogStream_IntensityB);
        break;
    case COMMAND_READ_FLOW_A:
        logValue(frameTime, "Q1", values.Q1, LogStream_FlowA);
        break;
    case COMMAND_READ_FLOW_B:
        logValue(frameTime, "Q2", values.Q2, LogStream_FlowB);
        break;

    case COMMAND_READ_FLAGS:
        /* Log value handled below */
        realtimeValue(frameTime, "ZINSTFLAGS", values.flags);

        if (realtimeEgress) {
            realtimeEgress->emplaceData(SequenceName({}, "raw", "F1"), Variant::Root(buildFlags()),
                                        frameTime, FP::undefined());
        }
        break;

    case COMMAND_READ_FLOW_OZONATOR:
        logValue(frameTime, "Q3", values.Q3, LogStream_FlowOzonator);
        break;

    case COMMAND_READ_OZONATOR_T:
        logValue(frameTime, "T3", values.T3, LogStream_OzonatorT);
        break;
    case COMMAND_READ_LAMP_V:
        logValue(frameTime, "V1", values.V1, LogStream_LampV);
        break;
    case COMMAND_READ_OZONATOR_V:
        logValue(frameTime, "V2", values.V2, LogStream_OzonatorV);
        break;

    case COMMAND_READ_MOTHERBOARD_V:
        realtimeValue(frameTime, "ZMBP24V", values.ZMBP24V);
        realtimeValue(frameTime, "ZMBP15V", values.ZMBP15V);
        realtimeValue(frameTime, "ZMBP5V", values.ZMBP5V);
        realtimeValue(frameTime, "ZMBP3V", values.ZMBP3V);
        realtimeValue(frameTime, "ZMBN3V", values.ZMBN3V);
        break;
    case COMMAND_READ_INTERFACE_V:
        realtimeValue(frameTime, "ZMEP24V", values.ZMEP24V);
        realtimeValue(frameTime, "ZMEP15V", values.ZMEP15V);
        realtimeValue(frameTime, "ZMEP5V", values.ZMEP5V);
        realtimeValue(frameTime, "ZMEP3V", values.ZMEP3V);
        realtimeValue(frameTime, "ZMEN15V", values.ZMEN15V);
        break;
    case COMMAND_READ_IO_V:
        realtimeValue(frameTime, "ZIOP24V", values.ZIOP24V);
        realtimeValue(frameTime, "ZIOP5V", values.ZIOP5V);
        realtimeValue(frameTime, "ZIOP3V", values.ZIOP3V);
        realtimeValue(frameTime, "ZION3V", values.ZION3V);
        break;
    }

    if (loggingEgress != NULL) {
        switch (interfaceMode) {
        case Mode_Undetermined:
            break;
        case Mode_49c_legacy:
            for (int i = LogStream_49i_ExclusiveBegin; i <= LogStream_49i_ExclusiveEnd; i++) {
                loggingMux.advance(i, frameTime, loggingEgress);
                streamTime[i] = FP::undefined();
                streamAge[i] = 0;
            }
            for (int i = LogStream_49c_ExclusiveBegin; i <= LogStream_49c_ExclusiveEnd; i++) {
                loggingMux.advance(i, frameTime, loggingEgress);
                streamTime[i] = FP::undefined();
                streamAge[i] = 0;
            }
            break;
        case Mode_49c:
            for (int i = LogStream_49i_ExclusiveBegin; i <= LogStream_49i_ExclusiveEnd; i++) {
                loggingMux.advance(i, frameTime, loggingEgress);
                streamTime[i] = FP::undefined();
                streamAge[i] = 0;
            }
            break;
        case Mode_49i:
            for (int i = LogStream_49c_ExclusiveBegin; i <= LogStream_49c_ExclusiveEnd; i++) {
                loggingMux.advance(i, frameTime, loggingEgress);
                streamTime[i] = FP::undefined();
                streamAge[i] = 0;
            }
            break;
        }
        for (int i = LogStream_RecordBaseStart; i < LogStream_TOTAL; i++) {
            if (streamAge[i] < 2)
                continue;

            if (FP::defined(streamTime[LogStream_State])) {
                double startTime = streamTime[LogStream_State];
                double endTime = frameTime;

                loggingMux.incoming(LogStream_State,
                                    SequenceValue({{}, "raw", "F1"}, Variant::Root(buildFlags()),
                                                  startTime, endTime), loggingEgress);
            } else {
                loggingMux.advance(LogStream_State, frameTime, loggingEgress);
            }
            streamTime[LogStream_State] = frameTime;

            for (i = LogStream_RecordBaseStart; i < LogStream_TOTAL; i++) {
                if (streamAge[i] == 0) {
                    loggingMux.advance(i, frameTime, loggingEgress);
                    streamTime[i] = FP::undefined();
                }
                streamAge[i] = 0;
            }
            break;
        }
    }

    /* Fourth stage: handle sample state control and transitions */
    switch (sampleState) {
    case SAMPLE_RUN:
        switch (modeChangeRequest) {
        case REQUEST_NONE:
            break;

        case REQUEST_SPANCHECK_ABORT:

            modeChangeRequest = REQUEST_NONE;
            break;

        case REQUEST_ZERO:

            qCDebug(log) << "Starting zero at" << Logging::time(frameTime);

            realtimeStateUpdated = true;
            sampleState = SAMPLE_ZERO_BEGIN_FLUSH;
            sampleStateBeginTime = frameTime;

            if (responseState == RESP_INTERACTIVE_RUN ||
                    responseState == RESP_INTERACTIVE_RUN_WAIT ||
                    responseState == RESP_INTERACTIVE_RUN_DELAY) {
                switch (interfaceMode) {
                case Mode_49c_legacy:
                case Mode_49c:
                    sendCommand(Command(this, COMMAND_SAMPLE_ZERO, "set zero"), frameTime);
                    return;
                case Mode_Undetermined:
                case Mode_49i:
                    sendCommand(Command(this, COMMAND_SET_PUMP_ON, "set pump on"), frameTime);
                    return;
                }
            }
            break;

        case REQUEST_SPANCHECK:

            qCDebug(log) << "Starting spancheck at" << Logging::time(frameTime);

            realtimeStateUpdated = true;
            sampleState = SAMPLE_SPANCHECK_BEGINZERO_FLUSH;
            sampleStateBeginTime = frameTime;

            if (responseState == RESP_INTERACTIVE_RUN ||
                    responseState == RESP_INTERACTIVE_RUN_WAIT ||
                    responseState == RESP_INTERACTIVE_RUN_DELAY) {
                switch (interfaceMode) {
                case Mode_49c_legacy:
                case Mode_49c:
                    sendCommand(Command(this, COMMAND_SAMPLE_ZERO, "set zero"), frameTime);
                    return;
                case Mode_Undetermined:
                case Mode_49i:
                    sendCommand(Command(this, COMMAND_SET_PUMP_ON, "set pump on"), frameTime);
                    return;
                }
            }
            break;
        }
        break;

    case SAMPLE_ZERO_BEGIN_FLUSH: {
        modeChangeRequest = REQUEST_NONE;

        double endTime = zeroFlushTime->apply(frameTime, sampleStateBeginTime, true);
        if (FP::defined(endTime) && endTime > frameTime)
            break;

        realtimeStateUpdated = true;
        sampleState = SAMPLE_ZERO_MEASURE;
        sampleStateBeginTime = frameTime;
        /* Fall through */
    }
    case SAMPLE_ZERO_MEASURE: {
        modeChangeRequest = REQUEST_NONE;

        double endTime = zeroMinimumMeasureTime->apply(frameTime, sampleStateBeginTime, true);
        if (FP::defined(endTime) && endTime > frameTime)
            break;
        if (!zeroMeasurement->stable()) {
            endTime = zeroMaximumMeasureTime->apply(frameTime, sampleStateBeginTime, true);
            if (!FP::defined(endTime) || endTime > frameTime)
                break;
        }

        realtimeStateUpdated = true;
        sampleState = SAMPLE_ZERO_END_FLUSH;
        sampleStateBeginTime = frameTime;

        if (responseState == RESP_INTERACTIVE_RUN ||
                responseState == RESP_INTERACTIVE_RUN_WAIT ||
                responseState == RESP_INTERACTIVE_RUN_DELAY) {
            switch (interfaceMode) {
            case Mode_49c_legacy:
            case Mode_49c:
                sendCommand(Command(this, COMMAND_SAMPLE_AMBIENT, "set sample"), frameTime);
                return;
            case Mode_Undetermined:
            case Mode_49i:
                sendCommand(Command(this, COMMAND_SET_PUMP_OFF, "set pump off"), frameTime);
                return;
            }
        }
        /* Fall through */
    }
    case SAMPLE_ZERO_END_FLUSH: {
        modeChangeRequest = REQUEST_NONE;

        double endTime = zeroFlushTime->apply(frameTime, sampleStateBeginTime, true);
        if (FP::defined(endTime) && endTime > frameTime)
            break;

        qCDebug(log) << "Zero completed at" << Logging::time(frameTime);

        realtimeStateUpdated = true;
        sampleState = SAMPLE_RUN;
        break;
    }


    case SAMPLE_SPANCHECK_BEGINZERO_FLUSH: {
        if (modeChangeRequest == REQUEST_SPANCHECK_ABORT) {
            modeChangeRequest = REQUEST_NONE;

            qCDebug(log) << "Aborting spacheck at" << Logging::time(frameTime);
            sampleState = SAMPLE_ZERO_END_FLUSH;
            sampleStateBeginTime = frameTime;
            zeroUpdated = false;
            spanUpdated = false;
            break;
        }
        modeChangeRequest = REQUEST_NONE;

        double endTime = zeroFlushTime->apply(frameTime, sampleStateBeginTime, true);
        if (FP::defined(endTime) && endTime > frameTime)
            break;

        sampleState = SAMPLE_SPANCHECK_BEGINZERO_MEASURE;
        sampleStateBeginTime = frameTime;
        /* Fall through */
    }
    case SAMPLE_SPANCHECK_BEGINZERO_MEASURE: {
        if (modeChangeRequest == REQUEST_SPANCHECK_ABORT) {
            modeChangeRequest = REQUEST_NONE;

            qCDebug(log) << "Aborting spacheck at" << Logging::time(frameTime);
            sampleState = SAMPLE_ZERO_END_FLUSH;
            sampleStateBeginTime = frameTime;
            zeroUpdated = false;
            spanUpdated = false;
            break;
        }
        modeChangeRequest = REQUEST_NONE;

        double endTime = zeroMinimumMeasureTime->apply(frameTime, sampleStateBeginTime, true);
        if (FP::defined(endTime) && endTime > frameTime)
            break;
        if (!zeroMeasurement->stable()) {
            endTime = zeroMaximumMeasureTime->apply(frameTime, sampleStateBeginTime, true);
            if (!FP::defined(endTime) || endTime > frameTime)
                break;
        }

        int level = -1;
        if (!config.first().spancheckLevels.isEmpty()) {
            level = config.first().spancheckLevels.first();
        }

        if (level >= 1) {
            sampleState = SAMPLE_SPANCHECK_LEVEL_FLUSH;
            sampleStateBeginTime = frameTime;
            sampleStateIndex = 0;

            qCDebug(log) << "Starting first spancheck level" << level << "at"
                         << Logging::time(frameTime);

            if (responseState == RESP_INTERACTIVE_RUN ||
                    responseState == RESP_INTERACTIVE_RUN_WAIT ||
                    responseState == RESP_INTERACTIVE_RUN_DELAY) {
                Util::ByteArray command("set level ");
                command += QByteArray::number(level);
                sendCommand(Command(this, COMMAND_SAMPLE_LEVEL, std::move(command)), frameTime);
                return;
            }
        } else {
            qCDebug(log) << "Continuing to spancheck final zero due to no available levels at"
                         << Logging::time(frameTime);

            sampleState = SAMPLE_SPANCHECK_ENDZERO_MEASURE;
            sampleStateBeginTime = frameTime;
            break;
        }
        /* Fall through */
    }
    case SAMPLE_SPANCHECK_LEVEL_FLUSH: {
        if (modeChangeRequest == REQUEST_SPANCHECK_ABORT) {
            modeChangeRequest = REQUEST_NONE;

            qCDebug(log) << "Aborting spacheck at" << Logging::time(frameTime);
            sampleState = SAMPLE_ZERO_END_FLUSH;
            sampleStateBeginTime = frameTime;
            zeroUpdated = false;
            spanUpdated = false;
            if (responseState == RESP_INTERACTIVE_RUN ||
                    responseState == RESP_INTERACTIVE_RUN_WAIT ||
                    responseState == RESP_INTERACTIVE_RUN_DELAY) {
                sendCommand(Command(this, COMMAND_SAMPLE_ZERO, "set zero"), frameTime);
                return;
            }
            break;
        }
        modeChangeRequest = REQUEST_NONE;

        double endTime = levelFlushTime->apply(frameTime, sampleStateBeginTime, true);
        if (FP::defined(endTime) && endTime > frameTime)
            break;

        sampleState = SAMPLE_SPANCHECK_LEVEL_MEASURE;
        sampleStateBeginTime = frameTime;
        /* Fall through */
    }
    case SAMPLE_SPANCHECK_LEVEL_MEASURE: {
        if (modeChangeRequest == REQUEST_SPANCHECK_ABORT) {
            modeChangeRequest = REQUEST_NONE;

            qCDebug(log) << "Aborting spacheck at" << Logging::time(frameTime);
            sampleState = SAMPLE_ZERO_END_FLUSH;
            sampleStateBeginTime = frameTime;
            zeroUpdated = false;
            spanUpdated = false;
            if (responseState == RESP_INTERACTIVE_RUN ||
                    responseState == RESP_INTERACTIVE_RUN_WAIT ||
                    responseState == RESP_INTERACTIVE_RUN_DELAY) {
                sendCommand(Command(this, COMMAND_SAMPLE_ZERO, "set zero"), frameTime);
                return;
            }
            break;
        }
        modeChangeRequest = REQUEST_NONE;

        double endTime = levelMinimumMeasureTime->apply(frameTime, sampleStateBeginTime, true);
        if (FP::defined(endTime) && endTime > frameTime)
            break;
        if (sampleStateIndex < 0 ||
                sampleStateIndex >= config.first().spancheckLevels.size() ||
                !levelMeasurement.at(sampleStateIndex)->stable()) {
            endTime = levelMaximumMeasureTime->apply(frameTime, sampleStateBeginTime, true);
            if (!FP::defined(endTime) || endTime > frameTime)
                break;
        }

        if (sampleStateIndex >= 0 && sampleStateIndex + 1 < config.first().spancheckLevels.size()) {
            sampleState = SAMPLE_SPANCHECK_LEVEL_FLUSH;
            sampleStateBeginTime = frameTime;
            sampleStateIndex++;

            int level = config.first().spancheckLevels.at(sampleStateIndex);
            qCDebug(log) << "Starting spancheck level" << level << "index" << sampleStateIndex
                         << "at" << Logging::time(frameTime);

            if (responseState == RESP_INTERACTIVE_RUN ||
                    responseState == RESP_INTERACTIVE_RUN_WAIT ||
                    responseState == RESP_INTERACTIVE_RUN_DELAY) {
                Util::ByteArray command("set level ");
                command += QByteArray::number(level);
                sendCommand(Command(this, COMMAND_SAMPLE_LEVEL, std::move(command)), frameTime);
                return;
            }
        } else {
            qCDebug(log) << "Starting final spancheck zero at" << Logging::time(frameTime);

            sampleState = SAMPLE_SPANCHECK_ENDZERO_MEASURE;
            sampleStateBeginTime = frameTime;

            if (responseState == RESP_INTERACTIVE_RUN ||
                    responseState == RESP_INTERACTIVE_RUN_WAIT ||
                    responseState == RESP_INTERACTIVE_RUN_DELAY) {
                sendCommand(Command(this, COMMAND_SAMPLE_ZERO, "set zero"), frameTime);
                return;
            }
        }
        /* Fall through */
    }
    case SAMPLE_SPANCHECK_ENDZERO_BEGIN_FLUSH: {
        if (modeChangeRequest == REQUEST_SPANCHECK_ABORT) {
            modeChangeRequest = REQUEST_NONE;

            qCDebug(log) << "Aborting spacheck at" << Logging::time(frameTime);
            sampleState = SAMPLE_ZERO_END_FLUSH;
            sampleStateBeginTime = frameTime;
            zeroUpdated = false;
            spanUpdated = false;
            break;
        }
        modeChangeRequest = REQUEST_NONE;

        double endTime = zeroFlushTime->apply(frameTime, sampleStateBeginTime, true);
        if (FP::defined(endTime) && endTime > frameTime)
            break;

        sampleState = SAMPLE_SPANCHECK_ENDZERO_MEASURE;
        sampleStateBeginTime = frameTime;
        /* Fall through */
    }
    case SAMPLE_SPANCHECK_ENDZERO_MEASURE: {
        if (modeChangeRequest == REQUEST_SPANCHECK_ABORT) {
            modeChangeRequest = REQUEST_NONE;

            qCDebug(log) << "Aborting spacheck at" << Logging::time(frameTime);
            sampleState = SAMPLE_ZERO_END_FLUSH;
            sampleStateBeginTime = frameTime;
            zeroUpdated = false;
            spanUpdated = false;
            break;
        }
        modeChangeRequest = REQUEST_NONE;

        double endTime = zeroMinimumMeasureTime->apply(frameTime, sampleStateBeginTime, true);
        if (FP::defined(endTime) && endTime > frameTime)
            break;
        if (!zeroMeasurement->stable()) {
            endTime = zeroMaximumMeasureTime->apply(frameTime, sampleStateBeginTime, true);
            if (!FP::defined(endTime) || endTime > frameTime)
                break;
        }

        sampleState = SAMPLE_SPANCHECK_ENDZERO_END_FLUSH;
        sampleStateBeginTime = frameTime;

        if (responseState == RESP_INTERACTIVE_RUN ||
                responseState == RESP_INTERACTIVE_RUN_WAIT ||
                responseState == RESP_INTERACTIVE_RUN_DELAY) {
            switch (interfaceMode) {
            case Mode_49c_legacy:
            case Mode_49c:
                sendCommand(Command(this, COMMAND_SAMPLE_AMBIENT, "set sample"), frameTime);
                return;
            case Mode_Undetermined:
            case Mode_49i:
                sendCommand(Command(this, COMMAND_SET_PUMP_OFF, "set pump off"), frameTime);
                return;
            }
        }
        /* Fall through */
    }
    case SAMPLE_SPANCHECK_ENDZERO_END_FLUSH: {
        modeChangeRequest = REQUEST_NONE;

        double endTime = zeroFlushTime->apply(frameTime, sampleStateBeginTime, true);
        if (FP::defined(endTime) && endTime > frameTime)
            break;

        qCDebug(log) << "Spancheck completed at" << Logging::time(frameTime);

        realtimeStateUpdated = true;
        sampleState = SAMPLE_RUN;
        break;
    }
    }

    /* Output result values once the actual measurements are completed */
    switch (sampleState) {
    case SAMPLE_RUN:
    case SAMPLE_ZERO_END_FLUSH:
    case SAMPLE_SPANCHECK_ENDZERO_END_FLUSH:
        if (spanUpdated) {
            spanUpdated = false;

            if (persistentEgress != NULL &&
                    FP::defined(spancheckDetails.getStart()) &&
                    spancheckDetails.read().exists()) {
                spancheckDetails.setEnd(frameTime);
                persistentEgress->incomingData(spancheckDetails);
            }
            spancheckDetails.write().setEmpty();

            if (zeroUpdated) {
                zeroMeasurement->setResult(spancheckDetails.write().hash("Zero"));
            }
            for (int i = 0,
                    max = qMin(config.first().spancheckLevels.size(), levelMeasurement.size());
                    i < max;
                    i++) {
                levelMeasurement[i]->setResult(spancheckDetails.write().hash("Levels").array(i));
            }
            resetSpancheckLevels();

            spancheckDetails.setStart(frameTime);
            spancheckDetails.setEnd(FP::undefined());
            persistentValuesUpdated();
            spancheckRealtimeUpdated = true;
        }
        if (zeroUpdated) {
            zeroUpdated = false;

            double vOld = zeroDetails.read().hash("X").toDouble();

            if (persistentEgress != NULL &&
                    FP::defined(zeroDetails.getStart()) &&
                    zeroDetails.read().exists()) {
                zeroDetails.setEnd(frameTime);
                persistentEgress->incomingData(zeroDetails);
            }
            zeroDetails.write().setEmpty();
            zeroMeasurement->setResult(zeroDetails.write());
            zeroMeasurement->reset();

            Variant::Root v(zeroDetails.read().hash("X"));
            remap("Xz", v);
            zeroDetails.write().hash("X").set(v);

            v.write().set(zeroDetails.read().hash("T"));
            remap("Tz", v);
            zeroDetails.write().hash("T").set(v);

            v.write().set(zeroDetails.read().hash("P"));
            remap("Pz", v);
            zeroDetails.write().hash("P").set(v);

            double vNew = zeroDetails.read().hash("X").toDouble();
            if (FP::defined(vOld) && FP::defined(vNew)) {
                Xzd.write().setDouble(vNew - vOld);
            } else {
                Xzd.write().setDouble(FP::undefined());
            }
            remap("Xzd", Xzd);

            zeroDetails.setStart(frameTime);
            zeroDetails.setEnd(FP::undefined());
            persistentValuesUpdated();
            zeroRealtimeUpdated = true;
        }

    case SAMPLE_ZERO_BEGIN_FLUSH:
    case SAMPLE_ZERO_MEASURE:
    case SAMPLE_SPANCHECK_BEGINZERO_FLUSH:
    case SAMPLE_SPANCHECK_BEGINZERO_MEASURE:
    case SAMPLE_SPANCHECK_LEVEL_FLUSH:
    case SAMPLE_SPANCHECK_LEVEL_MEASURE:
    case SAMPLE_SPANCHECK_ENDZERO_BEGIN_FLUSH:
    case SAMPLE_SPANCHECK_ENDZERO_MEASURE:
        break;
    }

    if (spancheckRealtimeUpdated && realtimeEgress != NULL) {
        spancheckRealtimeUpdated = false;

        realtimeValue(frameTime, "ZSPANCHECK", spancheckDetails.root());
    }
    if (zeroRealtimeUpdated && realtimeEgress != NULL) {
        zeroRealtimeUpdated = false;

        realtimeValue(frameTime, "ZZERO", zeroDetails.root());
        realtimeValue(frameTime, "Xzd", Xzd);
        realtimeValue(frameTime, "Xz", Variant::Root(zeroDetails.read().hash("X")));
        realtimeValue(frameTime, "Tz", Variant::Root(zeroDetails.read().hash("T")));
        realtimeValue(frameTime, "Pz", Variant::Root(zeroDetails.read().hash("P")));
    }


    /* Fifth stage: handle interactive control */
    if ((responseState != RESP_INTERACTIVE_RUN &&
            responseState != RESP_INTERACTIVE_RUN_WAIT &&
            responseState == RESP_INTERACTIVE_RUN_DELAY) || controlStream == NULL) {
        if (!commandBacklog.isEmpty()) {
            issuedCommand = commandBacklog.takeFirst();
        } else {
            issuedCommand.invalidate();
        }
        return;
    }

    /* We're going to issue something, so no backlog handling */
    commandBacklog.clear();

    switch (issuedCommand.type()) {
    case COMMAND_IGNORED:
    case COMMAND_SAMPLE_AMBIENT:
    case COMMAND_SAMPLE_ZERO:
    case COMMAND_SAMPLE_LEVEL:
    case COMMAND_SET_REMOTE:
    case COMMAND_SET_FORMAT:
    case COMMAND_SET_GAS_UNITS:
    case COMMAND_SET_AVERAGE_TIME:
    case COMMAND_SET_ENABLE_T_COMP:
    case COMMAND_SET_ENABLE_P_COMP:
    case COMMAND_SET_DATE:
    case COMMAND_SET_TIME:
    case COMMAND_READ_PROGRAM_NUMBER:
    case COMMAND_READ_OZONEBACKGROUND:
    case COMMAND_READ_OZONECOEFFICIENT:
    case COMMAND_READ_LAMP_SETTTING:
    case COMMAND_READ_HOST_NAME:
    case COMMAND_READ_OZONATOR_LEVEL:

        sendCommand(Command(this, COMMAND_READ_OZONE, "o3"), frameTime);
        break;

    case COMMAND_SET_PUMP_ON:
        switch (sampleState) {
        case SAMPLE_RUN:
        case SAMPLE_SPANCHECK_ENDZERO_END_FLUSH:
        case SAMPLE_ZERO_END_FLUSH:
        case SAMPLE_SPANCHECK_ENDZERO_BEGIN_FLUSH:
        case SAMPLE_SPANCHECK_ENDZERO_MEASURE:
        case SAMPLE_SPANCHECK_LEVEL_FLUSH:
        case SAMPLE_SPANCHECK_LEVEL_MEASURE:
            /* Anomalous or racing, so just continue */
            sendCommand(Command(this, COMMAND_READ_OZONE, "o3"), frameTime);
            break;

        case SAMPLE_ZERO_BEGIN_FLUSH:
        case SAMPLE_ZERO_MEASURE:
        case SAMPLE_SPANCHECK_BEGINZERO_FLUSH:
        case SAMPLE_SPANCHECK_BEGINZERO_MEASURE:
            /* Initiate zero sampling */
            sendCommand(Command(this, COMMAND_SAMPLE_ZERO, "set zero"), frameTime);
            break;
        }
        break;

    case COMMAND_SET_PUMP_OFF:
        /* Pump off always goes back to ambient sampling */
        sendCommand(Command(this, COMMAND_SAMPLE_AMBIENT, "set sample"), frameTime);
        break;

        /* Shared sequence */
    case COMMAND_READ_OZONE:

        sendCommand(Command(this, COMMAND_READ_SAMPLE_P, "pres"), frameTime);
        break;
    case COMMAND_READ_SAMPLE_P:

        sendCommand(Command(this, COMMAND_READ_SAMPLE_T, "bench temp"), frameTime);
        break;
    case COMMAND_READ_SAMPLE_T:

        sendCommand(Command(this, COMMAND_READ_LAMP_T, "lamp temp"), frameTime);
        break;
    case COMMAND_READ_LAMP_T:

        sendCommand(Command(this, COMMAND_READ_INTENSITY_A, "cell a int"), frameTime);
        break;
    case COMMAND_READ_INTENSITY_A:

        sendCommand(Command(this, COMMAND_READ_INTENSITY_B, "cell b int"), frameTime);
        break;
    case COMMAND_READ_INTENSITY_B:

        sendCommand(Command(this, COMMAND_READ_FLOW_A, "flow a"), frameTime);
        break;
    case COMMAND_READ_FLOW_A:

        sendCommand(Command(this, COMMAND_READ_FLOW_B, "flow b"), frameTime);
        break;
    case COMMAND_READ_FLOW_B:

        sendCommand(Command(this, COMMAND_READ_FLAGS, "flags"), frameTime);
        break;
    case COMMAND_READ_FLAGS:
        switch (interfaceMode) {
        case Mode_Undetermined:
        case Mode_49c_legacy:
            restartSequence(frameTime);
            break;
        case Mode_49c:
            sendCommand(Command(this, COMMAND_READ_FLOW_OZONATOR, "oz flow"), frameTime);
            break;
        case Mode_49i:
            sendCommand(Command(this, COMMAND_READ_OZONATOR_T, "o3 lamp temp"), frameTime);
            break;
        }
        break;

        /* 49c sequence */
    case COMMAND_READ_FLOW_OZONATOR:

        restartSequence(frameTime);
        break;

        /* 49i sequence */
    case COMMAND_READ_OZONATOR_T:

        sendCommand(Command(this, COMMAND_READ_LAMP_V, "lamp voltage bench"), frameTime);
        break;
    case COMMAND_READ_LAMP_V:

        sendCommand(Command(this, COMMAND_READ_OZONATOR_V, "lamp voltage oz"), frameTime);
        break;
    case COMMAND_READ_OZONATOR_V:

        sendCommand(Command(this, COMMAND_READ_MOTHERBOARD_V, "diag volt mb"), frameTime);
        break;
    case COMMAND_READ_MOTHERBOARD_V:

        sendCommand(Command(this, COMMAND_READ_INTERFACE_V, "diag volt mib"), frameTime);
        break;
    case COMMAND_READ_INTERFACE_V:

        sendCommand(Command(this, COMMAND_READ_IO_V, "diag volt iob"), frameTime);
        break;
    case COMMAND_READ_IO_V:

        restartSequence(frameTime);
        break;
    }
}

void AcquireThermoOzone49::incomingInstrumentTimeout(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configAdvance(frameTime);

    switch (responseState) {
    case RESP_INTERACTIVE_INITIALIZE:
    case RESP_INTERACTIVE_RESTART_WAIT:

        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveSetRemote"),
                                  frameTime, FP::undefined()));
        }

        sendCommand(Command(this, COMMAND_SET_REMOTE, "set mode remote"), frameTime);
        responseState = RESP_INTERACTIVE_START_SET_REMOTE_INITIAL;
        invalidateLogValues(frameTime);
        generalStatusUpdated();
        break;

    case RESP_INTERACTIVE_RUN:

        qCDebug(log) << "Timeout in interactive mode at" << Logging::time(frameTime)
                     << "waiting for command" << issuedCommand.type();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString(describeResponseState());
            info.hash("SampleState").set(describeSampleState());
            info.hash("IssuedCommand").set(issuedCommand.stateDescription());
            event(frameTime, QObject::tr("Timeout waiting for response.  Communications Dropped."),
                  true, info);
        }

        sendCommand(Command(this, COMMAND_SET_REMOTE, "set mode remote"), frameTime);
        responseState = RESP_INTERACTIVE_START_SET_REMOTE_INITIAL;
        sampleState = SAMPLE_RUN;
        invalidateLogValues(frameTime);
        generalStatusUpdated();
        break;

    case RESP_INTERACTIVE_RUN_WAIT:

        sendCommand(Command(this, COMMAND_READ_OZONE, "o3"), frameTime);
        responseState = RESP_INTERACTIVE_RUN;
        break;

    case RESP_INTERACTIVE_RUN_DELAY:

        responseState = RESP_INTERACTIVE_RUN;

        discardData(FP::undefined());
        timeoutAt(frameTime + 2.0);

        if (controlStream != NULL) {
            Util::ByteArray send = issuedCommand.data();
            if (config.first().address != 0) {
                send.push_front(config.first().address + 128);
            }
            send.push_back('\r');
            controlStream->writeControl(send);
        }
        break;

    case RESP_PASSIVE_RUN:

        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime)
                     << "waiting for command" << issuedCommand.type();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString(describeResponseState());
            info.hash("SampleState").set(describeSampleState());
            info.hash("IssuedCommand").set(issuedCommand.stateDescription());
            event(frameTime, QObject::tr("Timeout waiting for response.  Communications Dropped."),
                  true, info);
        }

        timeoutAt(FP::undefined());
        responseState = RESP_PASSIVE_WAIT;
        issuedCommand.invalidate();
        commandBacklog.clear();
        generalStatusUpdated();

        sampleState = SAMPLE_RUN;
        invalidateLogValues(frameTime);
        break;

    case RESP_PASSIVE_INITIALIZE:
        /* No action taken on the first timeout. */
        timeoutAt(FP::undefined());
        responseState = RESP_PASSIVE_WAIT;
        issuedCommand.invalidate();
        sampleState = SAMPLE_RUN;
        invalidateLogValues(frameTime);
        break;

    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        /* No action taken on the first timeout. */
        timeoutAt(FP::undefined());
        responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
        issuedCommand.invalidate();
        commandBacklog.clear();
        sampleState = SAMPLE_RUN;
        invalidateLogValues(frameTime);
        break;

    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case RESP_PASSIVE_WAIT:
        timeoutAt(FP::undefined());
        issuedCommand.invalidate();
        commandBacklog.clear();
        sampleState = SAMPLE_RUN;
        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_START_READ_PROGRAM_NUMBER:
        /* This doesn't always exist on 49C's, so allow a silent failure
         * timeout */
        if (instrumentMeta["Model"].toString() != "49c") {
            instrumentMeta["Model"].setString("49c");
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }
        if (instrumentMeta["FirmwareVersion"].exists()) {
            instrumentMeta["FirmwareVersion"].setEmpty();
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }
        if (instrumentMeta["LinkVersion"].exists()) {
            instrumentMeta["LinkVersion"].setEmpty();
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }

        if (interfaceMode != Mode_49c_legacy) {
            interfaceMode = Mode_49c_legacy;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }

        handleStartupAdvance(frameTime, false);
        break;

    case RESP_INTERACTIVE_START_SET_REMOTE_INITIAL:
        /* Allow no response to remote set once, but require it on the second one */
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 1.0);
        issuedCommand.invalidate();
        commandBacklog.clear();
        autoprobeValidRecords = 0;
        break;

    case RESP_INTERACTIVE_START_SET_AVERAGING_TIME:
    case RESP_INTERACTIVE_START_SET_GAS_UNITS:
    case RESP_INTERACTIVE_START_SET_TEMPERATURE_COMPENSATION:
    case RESP_INTERACTIVE_START_SET_PRESSURE_COMPENSATION:
    case RESP_INTERACTIVE_START_SET_DATE:
    case RESP_INTERACTIVE_START_SET_TIME:
    case RESP_INTERACTIVE_START_READ_OZONE_COEFFICIENT:
    case RESP_INTERACTIVE_START_READ_OZONE_BACKGROUND:
    case RESP_INTERACTIVE_START_READ_LAMP_SETTING:
    case RESP_INTERACTIVE_START_READ_OZONATOR_FLOW:
        if (interfaceMode == Mode_49c || interfaceMode == Mode_Undetermined) {
            interfaceMode = Mode_49c_legacy;
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
        }
        handleStartupAdvance(frameTime, false);
        break;


    case RESP_INTERACTIVE_START_SET_REMOTE_CONFIRM:
    case RESP_INTERACTIVE_START_SET_FORMAT:
    case RESP_INTERACTIVE_START_SET_MODE:
    case RESP_INTERACTIVE_START_READ_HOST_NAME:
    case RESP_INTERACTIVE_START_READ_OZONATOR_LEVELS:
    case RESP_INTERACTIVE_START_READ_OZONE_INITIAL:
    case RESP_INTERACTIVE_START_READ_FLAGS:

        qCDebug(log) << "Timeout in interactive start state" << responseState << "at"
                     << Logging::time(frameTime);

        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);
        issuedCommand.invalidate();
        commandBacklog.clear();
        autoprobeValidRecords = 0;

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        invalidateLogValues(frameTime);
        break;
    }
}

void AcquireThermoOzone49::discardDataCompleted(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));

    switch (responseState) {
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveSetRemote"),
                                  frameTime, FP::undefined()));
        }

        sendCommand(Command(this, COMMAND_SET_REMOTE, "set mode remote"), frameTime);
        responseState = RESP_INTERACTIVE_START_SET_REMOTE_INITIAL;
        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_START_SET_REMOTE_INITIAL:

        sendCommand(Command(this, COMMAND_SET_REMOTE, "set mode remote"), frameTime);
        responseState = RESP_INTERACTIVE_START_SET_REMOTE_CONFIRM;
        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_START_SET_REMOTE_CONFIRM:
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadProgramNumber"), frameTime, FP::undefined()));
        }

        sendCommand(Command(this, COMMAND_READ_PROGRAM_NUMBER, "program no"), frameTime);
        responseState = RESP_INTERACTIVE_START_READ_PROGRAM_NUMBER;
        break;

    case RESP_INTERACTIVE_START_READ_PROGRAM_NUMBER:
    case RESP_INTERACTIVE_START_SET_FORMAT:
    case RESP_INTERACTIVE_START_SET_AVERAGING_TIME:
    case RESP_INTERACTIVE_START_SET_GAS_UNITS:
    case RESP_INTERACTIVE_START_SET_TEMPERATURE_COMPENSATION:
    case RESP_INTERACTIVE_START_SET_PRESSURE_COMPENSATION:
    case RESP_INTERACTIVE_START_SET_MODE:
    case RESP_INTERACTIVE_START_SET_DATE:
    case RESP_INTERACTIVE_START_SET_TIME:
    case RESP_INTERACTIVE_START_READ_OZONE_BACKGROUND:
    case RESP_INTERACTIVE_START_READ_OZONE_COEFFICIENT:
    case RESP_INTERACTIVE_START_READ_FLAGS:
    case RESP_INTERACTIVE_START_READ_LAMP_SETTING:
    case RESP_INTERACTIVE_START_READ_HOST_NAME:
    case RESP_INTERACTIVE_START_READ_OZONATOR_LEVELS:
    case RESP_INTERACTIVE_START_READ_OZONATOR_FLOW:
    case RESP_INTERACTIVE_START_READ_OZONE_INITIAL:

        handleStartupAdvance(frameTime, false);
        break;

    default:
        break;
    }
}

void AcquireThermoOzone49::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configAdvance(frameTime);

    Util::ByteArray check = frame;
    check.string_trimmed();
    if (!check.empty()) {
        for (auto prior = check.begin(), next = prior + 1; next != check.end();) {
            if (std::isspace(*prior) && std::isspace(*next)) {
                prior = check.erase(prior);
                if (prior == check.end())
                    break;
                next = prior + 1;
                continue;
            }
            prior = next;
            ++next;
        }
    }
    check.string_to_lower();

    Command command(this, COMMAND_IGNORED, frame);
    if (check == "o3") {
        command = Command(this, COMMAND_READ_OZONE, frame);
    } else if (check == "pres") {
        command = Command(this, COMMAND_READ_SAMPLE_P, frame);
    } else if (check == "bench temp") {
        command = Command(this, COMMAND_READ_SAMPLE_T, frame);
    } else if (check == "lamp temp") {
        command = Command(this, COMMAND_READ_LAMP_T, frame);
    } else if (check == "cell a int") {
        command = Command(this, COMMAND_READ_INTENSITY_A, frame);
    } else if (check == "cell b int") {
        command = Command(this, COMMAND_READ_INTENSITY_B, frame);
    } else if (check == "flow a") {
        command = Command(this, COMMAND_READ_FLOW_A, frame);
    } else if (check == "flow b") {
        command = Command(this, COMMAND_READ_FLOW_B, frame);
    } else if (check == "flags") {
        command = Command(this, COMMAND_READ_FLAGS, frame);
    } else if (check == "oz flow") {
        command = Command(this, COMMAND_READ_FLOW_OZONATOR, frame);
    } else if (check == "o3 lamp temp") {
        command = Command(this, COMMAND_READ_OZONATOR_T, frame);
    } else if (check == "lamp voltage bench") {
        command = Command(this, COMMAND_READ_LAMP_V, frame);
    } else if (check == "lamp voltage oz") {
        command = Command(this, COMMAND_READ_OZONATOR_V, frame);
    } else if (check == "diag volt mb") {
        command = Command(this, COMMAND_READ_MOTHERBOARD_V, frame);
    } else if (check == "diag volt mib") {
        command = Command(this, COMMAND_READ_INTERFACE_V, frame);
    } else if (check == "diag volt iob") {
        command = Command(this, COMMAND_READ_IO_V, frame);
    } else if (check == "set sample") {
        command = Command(this, COMMAND_SAMPLE_AMBIENT, frame);
    } else if (check == "set zero") {
        command = Command(this, COMMAND_SAMPLE_ZERO, frame);
    } else if (check == "mode remote") {
        command = Command(this, COMMAND_SET_REMOTE, frame);
    } else if (check == "set gas unit ppb") {
        command = Command(this, COMMAND_SET_GAS_UNITS, frame);
    } else if (check == "set temp comp on") {
        command = Command(this, COMMAND_SET_ENABLE_T_COMP, frame);
    } else if (check == "set pres comp on") {
        command = Command(this, COMMAND_SET_ENABLE_P_COMP, frame);
    } else if (check == "program no") {
        command = Command(this, COMMAND_READ_PROGRAM_NUMBER, frame);
    } else if (check == "o3 bkg") {
        command = Command(this, COMMAND_READ_OZONEBACKGROUND, frame);
    } else if (check == "o3 coef") {
        command = Command(this, COMMAND_READ_OZONECOEFFICIENT, frame);
    } else if (check == "lamp setting") {
        command = Command(this, COMMAND_READ_LAMP_SETTTING, frame);
    } else if (check == "host name") {
        command = Command(this, COMMAND_READ_HOST_NAME, frame);
    } else if (check == "set pump on") {
        command = Command(this, COMMAND_SET_PUMP_ON, frame);
    } else if (check == "set pump off") {
        command = Command(this, COMMAND_SET_PUMP_OFF, frame);
    } else if (check.string_start("set level ")) {
        command = Command(this, COMMAND_SAMPLE_LEVEL, frame);
    } else if (check.string_start("set avg time ")) {
        command = Command(this, COMMAND_SET_AVERAGE_TIME, frame);
    } else if (check.string_start("set date ")) {
        command = Command(this, COMMAND_SET_DATE, frame);
    } else if (check.string_start("set time ")) {
        command = Command(this, COMMAND_SET_TIME, frame);
    } else if (check.string_start("l") && check.size() >= 2 && check[1] >= '0' && check[1] <= '9') {
        command = Command(this, COMMAND_READ_OZONATOR_LEVEL, frame);
    }

    if (!issuedCommand.valid()) {
        issuedCommand = command;
    } else {
        commandBacklog.append(command);
    }
}

SequenceValue::Transfer AcquireThermoOzone49::getPersistentValues()
{
    PauseLock paused(*this);
    SequenceValue::Transfer result;

    if (FP::defined(spancheckDetails.getStart()) && spancheckDetails.read().exists()) {
        result.emplace_back(spancheckDetails);
    }

    if (FP::defined(zeroDetails.getStart()) && zeroDetails.read().exists()) {
        result.emplace_back(zeroDetails);

        result.emplace_back(SequenceName({}, "raw", "Xz"),
                            Variant::Root(zeroDetails.read().hash("X")), zeroDetails.getStart(),
                            zeroDetails.getEnd());

        result.emplace_back(SequenceName({}, "raw", "Tz"),
                            Variant::Root(zeroDetails.read().hash("T")), zeroDetails.getStart(),
                            zeroDetails.getEnd());

        result.emplace_back(SequenceName({}, "raw", "Pz"),
                            Variant::Root(zeroDetails.read().hash("P")), zeroDetails.getStart(),
                            zeroDetails.getEnd());
    }

    return result;
}

Variant::Root AcquireThermoOzone49::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireThermoOzone49::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RUN:
    case RESP_INTERACTIVE_RUN_WAIT:
    case RESP_INTERACTIVE_RUN_DELAY:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}


void AcquireThermoOzone49::command(const Variant::Read &command)
{
    Q_ASSERT(!config.isEmpty());

    if (command.hash("StartZero").exists()) {
        if (sampleState != SAMPLE_RUN) {
            qCDebug(log) << "Discarding zero request, sample state:" << sampleState
                         << ", response state:" << responseState;
        } else if (modeChangeRequest == REQUEST_NONE) {
            qCDebug(log) << "Zero requested in response state" << responseState;
            modeChangeRequest = REQUEST_ZERO;
        } else {
            qCDebug(log) << "Zero request ignored because request" << modeChangeRequest
                         << "is already pending";
        }
    }

    if (command.hash("StartOzoneSpancheck").exists()) {
        if (sampleState != SAMPLE_RUN) {
            qCDebug(log) << "Discarding spancheck request, sample state:" << sampleState
                         << ", response state:" << responseState;
        } else if (modeChangeRequest == REQUEST_NONE) {
            qCDebug(log) << "Spancheck requested in response state" << responseState;
            modeChangeRequest = REQUEST_SPANCHECK;
        } else {
            qCDebug(log) << "Spancheck request ignored because request" << modeChangeRequest
                         << "is already pending";
        }
    }

    if (command.hash("StopOzoneSpancheck").exists()) {
        switch (sampleState) {
        case SAMPLE_SPANCHECK_BEGINZERO_FLUSH:
        case SAMPLE_SPANCHECK_BEGINZERO_MEASURE:
        case SAMPLE_SPANCHECK_LEVEL_FLUSH:
        case SAMPLE_SPANCHECK_LEVEL_MEASURE:
        case SAMPLE_SPANCHECK_ENDZERO_BEGIN_FLUSH:
        case SAMPLE_SPANCHECK_ENDZERO_MEASURE:
            if (modeChangeRequest == REQUEST_SPANCHECK_ABORT) {
                qCDebug(log) << "Spancheck abort request ignored because request"
                             << modeChangeRequest << "is already pending";
                break;
            }

            qCDebug(log) << "Spancheck abort requested in response state" << responseState;
            modeChangeRequest = REQUEST_SPANCHECK_ABORT;
            break;
        default:
            qCDebug(log) << "Discarding spancheck abort request, sample state:" << sampleState
                         << ", response state:" << responseState;
            break;
        }
    }
}

Variant::Root AcquireThermoOzone49::getCommands()
{
    Variant::Root result;

    result["StartZero"].hash("DisplayName").setString("Start &Zero");
    result["StartZero"].hash("ToolTip").setString("Start a zero level check.");
    result["StartZero"].hash("Include").array(0).hash("Type").setString("Equal");
    result["StartZero"].hash("Include").array(0).hash("Value").setString("Run");
    result["StartZero"].hash("Include").array(0).hash("Variable").setString("ZSTATE");

    result["StartOzoneSpancheck"].hash("DisplayName").setString("Start &Spancheck");
    result["StartOzoneSpancheck"].hash("ToolTip").setString("Start a ozonator level span check.");
    result["StartOzoneSpancheck"].hash("Include").array(0).hash("Type").setString("Equal");
    result["StartOzoneSpancheck"].hash("Include").array(0).hash("Value").setString("Run");
    result["StartOzoneSpancheck"].hash("Include").array(0).hash("Variable").setString("ZSTATE");
    result["StartOzoneSpancheck"].hash("Aggregate").setBool(true);

    result["StopOzoneSpancheck"].hash("DisplayName").setString("Stop &Spancheck");
    result["StopOzoneSpancheck"].hash("ToolTip")
                                .setString("Abort the current ozonator level span check.");
    result["StopOzoneSpancheck"].hash("Include").array(0).hash("Type").setString("Equal");
    result["StopOzoneSpancheck"].hash("Include").array(0).hash("Value").setString("Spancheck");
    result["StopOzoneSpancheck"].hash("Include").array(0).hash("Variable").setString("ZSTATE");
    result["StopOzoneSpancheck"].hash("Aggregate").setBool(true);

    return result;
}

AcquisitionInterface::AutoprobeStatus AcquireThermoOzone49::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireThermoOzone49::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN:
    case RESP_INTERACTIVE_RUN_WAIT:
    case RESP_INTERACTIVE_RUN_DELAY:
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << responseState << "at" << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case RESP_INTERACTIVE_START_SET_REMOTE_INITIAL:
    case RESP_INTERACTIVE_START_SET_REMOTE_CONFIRM:
    case RESP_INTERACTIVE_START_READ_PROGRAM_NUMBER:
    case RESP_INTERACTIVE_START_SET_FORMAT:
    case RESP_INTERACTIVE_START_SET_AVERAGING_TIME:
    case RESP_INTERACTIVE_START_SET_GAS_UNITS:
    case RESP_INTERACTIVE_START_SET_TEMPERATURE_COMPENSATION:
    case RESP_INTERACTIVE_START_SET_PRESSURE_COMPENSATION:
    case RESP_INTERACTIVE_START_SET_MODE:
    case RESP_INTERACTIVE_START_SET_DATE:
    case RESP_INTERACTIVE_START_SET_TIME:
    case RESP_INTERACTIVE_START_READ_OZONE_BACKGROUND:
    case RESP_INTERACTIVE_START_READ_OZONE_COEFFICIENT:
    case RESP_INTERACTIVE_START_READ_FLAGS:
    case RESP_INTERACTIVE_START_READ_LAMP_SETTING:
    case RESP_INTERACTIVE_START_READ_HOST_NAME:
    case RESP_INTERACTIVE_START_READ_OZONATOR_LEVELS:
    case RESP_INTERACTIVE_START_READ_OZONATOR_FLOW:
    case RESP_INTERACTIVE_START_READ_OZONE_INITIAL:
    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        responseState = RESP_INTERACTIVE_INITIALIZE;
        discardData(time + 0.5);
        timeoutAt(time + 10.0);
        break;
    }
}

void AcquireThermoOzone49::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
    autoprobeValidRecords = 0;
    issuedCommand.invalidate();
    commandBacklog.clear();
    discardData(time + 0.5, 1);
    timeoutAt(time + 15.0);
    generalStatusUpdated();
}

void AcquireThermoOzone49::autoprobePromote(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN:
    case RESP_INTERACTIVE_START_SET_REMOTE_INITIAL:
    case RESP_INTERACTIVE_START_SET_REMOTE_CONFIRM:
    case RESP_INTERACTIVE_START_READ_PROGRAM_NUMBER:
    case RESP_INTERACTIVE_START_SET_FORMAT:
    case RESP_INTERACTIVE_START_SET_AVERAGING_TIME:
    case RESP_INTERACTIVE_START_SET_GAS_UNITS:
    case RESP_INTERACTIVE_START_SET_TEMPERATURE_COMPENSATION:
    case RESP_INTERACTIVE_START_SET_PRESSURE_COMPENSATION:
    case RESP_INTERACTIVE_START_SET_MODE:
    case RESP_INTERACTIVE_START_SET_DATE:
    case RESP_INTERACTIVE_START_SET_TIME:
    case RESP_INTERACTIVE_START_READ_OZONE_BACKGROUND:
    case RESP_INTERACTIVE_START_READ_OZONE_COEFFICIENT:
    case RESP_INTERACTIVE_START_READ_FLAGS:
    case RESP_INTERACTIVE_START_READ_LAMP_SETTING:
    case RESP_INTERACTIVE_START_READ_HOST_NAME:
    case RESP_INTERACTIVE_START_READ_OZONATOR_LEVELS:
    case RESP_INTERACTIVE_START_READ_OZONE_INITIAL:
    case RESP_INTERACTIVE_START_READ_OZONATOR_FLOW:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        /* Reset incase the controller was changed */
        timeoutAt(time + 2.0);
        break;

    case RESP_INTERACTIVE_RUN_WAIT:
    case RESP_INTERACTIVE_RUN_DELAY: {

        qCDebug(log) << "Promoted from interactive wait state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        double delay = config.first().commandDelay;
        if (!FP::defined(delay))
            delay = 1.0;

        /* Reset timeout for the query */
        responseState = RESP_INTERACTIVE_RUN_WAIT;
        timeoutAt(time + delay);
        break;
    }

    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        sendCommand(Command(this, COMMAND_SET_REMOTE, "set mode remote"), time);
        responseState = RESP_INTERACTIVE_START_SET_REMOTE_INITIAL;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveSetRemote"),
                                  time, FP::undefined()));
        }
        break;
    }
}

void AcquireThermoOzone49::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + 2.0);

    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_INITIALIZE:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from autoprobe passive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);
        responseState = RESP_PASSIVE_INITIALIZE;
        break;

    case RESP_INTERACTIVE_RUN:
    case RESP_INTERACTIVE_RUN_WAIT:
    case RESP_INTERACTIVE_RUN_DELAY:
        /* Have communications, so don't discard anything we've done so far,
         * and just assume something else is transparently assuming control. */
        responseState = RESP_PASSIVE_RUN;

        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case RESP_INTERACTIVE_START_SET_REMOTE_INITIAL:
    case RESP_INTERACTIVE_START_SET_REMOTE_CONFIRM:
    case RESP_INTERACTIVE_START_READ_PROGRAM_NUMBER:
    case RESP_INTERACTIVE_START_SET_FORMAT:
    case RESP_INTERACTIVE_START_SET_AVERAGING_TIME:
    case RESP_INTERACTIVE_START_SET_GAS_UNITS:
    case RESP_INTERACTIVE_START_SET_TEMPERATURE_COMPENSATION:
    case RESP_INTERACTIVE_START_SET_PRESSURE_COMPENSATION:
    case RESP_INTERACTIVE_START_SET_MODE:
    case RESP_INTERACTIVE_START_SET_DATE:
    case RESP_INTERACTIVE_START_SET_TIME:
    case RESP_INTERACTIVE_START_READ_OZONE_BACKGROUND:
    case RESP_INTERACTIVE_START_READ_OZONE_COEFFICIENT:
    case RESP_INTERACTIVE_START_READ_FLAGS:
    case RESP_INTERACTIVE_START_READ_LAMP_SETTING:
    case RESP_INTERACTIVE_START_READ_HOST_NAME:
    case RESP_INTERACTIVE_START_READ_OZONATOR_LEVELS:
    case RESP_INTERACTIVE_START_READ_OZONE_INITIAL:
    case RESP_INTERACTIVE_START_READ_OZONATOR_FLOW:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        /* We didn't have full communications, so just drop it and start
         * over. */
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);

        discardData(FP::undefined());

        responseState = RESP_PASSIVE_INITIALIZE;
        issuedCommand.invalidate();
        commandBacklog.clear();
        generalStatusUpdated();
        break;
    }
}

static const quint16 serializedStateVersion = 1;

void AcquireThermoOzone49::serializeState(QDataStream &stream)
{
    PauseLock paused(*this);

    stream << serializedStateVersion;

    stream << zeroDetails.getStart();
    stream << zeroDetails.read();

    stream << spancheckDetails.getStart();
    stream << spancheckDetails.read();
}

void AcquireThermoOzone49::deserializeState(QDataStream &stream)
{
    quint16 version = 0;
    stream >> version;
    if (version != serializedStateVersion)
        return;

    PauseLock paused(*this);

    double start = FP::undefined();
    stream >> start >> zeroDetails.root();
    zeroDetails.setStart(start);
    zeroDetails.setEnd(FP::undefined());
    zeroRealtimeUpdated = true;
    Xzd.write().setEmpty();

    stream >> start >> spancheckDetails.root();
    spancheckDetails.setStart(start);
    spancheckDetails.setEnd(FP::undefined());
    spancheckRealtimeUpdated = true;
}

AcquisitionInterface::AutomaticDefaults AcquireThermoOzone49::getDefaults()
{
    AutomaticDefaults result;
    result.name = "G$1$2";
    result.setSerialN81(9600);
    return result;
}


ComponentOptions AcquireThermoOzone49Component::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireThermoOzone49Component::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireThermoOzone49Component::createAcquisitionPassive(const ComponentOptions &options,
                                                                                      const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireThermoOzone49(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireThermoOzone49Component::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                      const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireThermoOzone49(config, loggingContext)); }

std::unique_ptr<AcquisitionInterface> AcquireThermoOzone49Component::createAcquisitionAutoprobe(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireThermoOzone49> i(new AcquireThermoOzone49(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<AcquisitionInterface> AcquireThermoOzone49Component::createAcquisitionInteractive(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireThermoOzone49> i(new AcquireThermoOzone49(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
