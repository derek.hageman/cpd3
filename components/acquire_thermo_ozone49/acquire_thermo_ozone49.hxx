/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIREECOTECHNEPHAURORA3000_H
#define ACQUIREECOTECHNEPHAURORA3000_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "acquisition/spancheck.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"
#include "smoothing/baseline.hxx"

class AcquireThermoOzone49 : public CPD3::Acquisition::FramedInstrument {
    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;
    int autoprobeValidRecords;

    enum {
        /* Waiting a response to a command in passive mode */
                RESP_PASSIVE_RUN,

        /* Waiting for enough data to confirm communications */
                RESP_PASSIVE_WAIT,
        /* Passive initialization (same as passive wait, but discard the
         * first timeout). */
                RESP_PASSIVE_INITIALIZE,

        /* Same as passive wait but currently autoprobing */
                RESP_AUTOPROBE_PASSIVE_WAIT,
        /* Same as passive initialize but autoprobing */
                RESP_AUTOPROBE_PASSIVE_INITIALIZE,

        /* Setting remote mode */
                RESP_INTERACTIVE_START_SET_REMOTE_INITIAL,
        /* Setting remote mode */
                RESP_INTERACTIVE_START_SET_REMOTE_CONFIRM,
        /* Reading the program number during start up */
                RESP_INTERACTIVE_START_READ_PROGRAM_NUMBER,
        /* Setting the the format to disable checksum */
                RESP_INTERACTIVE_START_SET_FORMAT,
        /* Setting the averaging time */
                RESP_INTERACTIVE_START_SET_AVERAGING_TIME,
        /* Setting gas units */
                RESP_INTERACTIVE_START_SET_GAS_UNITS,
        /* Enabling temperature compensation */
                RESP_INTERACTIVE_START_SET_TEMPERATURE_COMPENSATION,
        /* Enabling pressure compensation */
                RESP_INTERACTIVE_START_SET_PRESSURE_COMPENSATION,
        /* Change sampling mode */
                RESP_INTERACTIVE_START_SET_MODE,
        /* Setting date */
                RESP_INTERACTIVE_START_SET_DATE,
        /* Setting time */
                RESP_INTERACTIVE_START_SET_TIME,
        /* Reading ozone background setting */
                RESP_INTERACTIVE_START_READ_OZONE_BACKGROUND,
        /* Reading ozone coefficient */
                RESP_INTERACTIVE_START_READ_OZONE_COEFFICIENT, /* Reading the current flags */
                RESP_INTERACTIVE_START_READ_FLAGS,
        /* Reading lamp settings */
                RESP_INTERACTIVE_START_READ_LAMP_SETTING,

        /* Reading ozonantor flow (49c only, disambiguation from legacy mode) */
                RESP_INTERACTIVE_START_READ_OZONATOR_FLOW,

        /* Reading instrument name (49i only) */
                RESP_INTERACTIVE_START_READ_HOST_NAME,
        /* Reading ozonator levels (49i only) */
                RESP_INTERACTIVE_START_READ_OZONATOR_LEVELS,

        /* Initial ozone read */
                RESP_INTERACTIVE_START_READ_OZONE_INITIAL,

        /* Waiting before attempting a communications restart */
                RESP_INTERACTIVE_RESTART_WAIT,
        /* Initial state for interactive start */
                RESP_INTERACTIVE_INITIALIZE,

        /* In interactive mode interrogating the instrument. */
                RESP_INTERACTIVE_RUN,
        /* In interative run mode, waiting before querying again. */
                RESP_INTERACTIVE_RUN_WAIT,
        /* In interative run mode, delaying between commands. */
                RESP_INTERACTIVE_RUN_DELAY,
    } responseState;
    int responseStateIndex;

    enum CommandType {
        /* Unknown command, so all response until another command is
         * received is discarded. */
                COMMAND_IGNORED,

        /* Read measured parameters */
                COMMAND_READ_OZONE, /* o3 */
                COMMAND_READ_SAMPLE_P, /* pres */
                COMMAND_READ_SAMPLE_T, /* bench temp */
                COMMAND_READ_LAMP_T, /* lamp temp */
                COMMAND_READ_INTENSITY_A, /* cell a int */
                COMMAND_READ_INTENSITY_B, /* cell b int */
                COMMAND_READ_FLOW_A, /* flow a */
                COMMAND_READ_FLOW_B, /* flow b */
                COMMAND_READ_FLAGS,             /* flags */
        /* 49c (non legacy) only */
                COMMAND_READ_FLOW_OZONATOR,     /* oz flow */
        /* 49i only */
                COMMAND_READ_OZONATOR_T, /* o3 lamp temp */
                COMMAND_READ_LAMP_V, /* lamp voltage bench */
                COMMAND_READ_OZONATOR_V, /* lamp voltage oz */
                COMMAND_READ_MOTHERBOARD_V, /* diag volt mb */
                COMMAND_READ_INTERFACE_V, /* diag volt mib */
                COMMAND_READ_IO_V,              /* diag volt iob */

        /* Change sampling mode */
                COMMAND_SAMPLE_AMBIENT, /* set sample */
                COMMAND_SAMPLE_ZERO, /* set zero */
                COMMAND_SAMPLE_LEVEL,           /* set level X */
        /* 49i only */
                COMMAND_SET_PUMP_ON, /* set pump on */
                COMMAND_SET_PUMP_OFF,           /* set pump off */

        /* Setup commands */
                COMMAND_SET_REMOTE, /* mode remote */
                COMMAND_SET_FORMAT, /* set format */
                COMMAND_SET_GAS_UNITS, /* set gas unit ppb */
                COMMAND_SET_AVERAGE_TIME, /* set avg time X  (not available on 49C legacy) */
                COMMAND_SET_ENABLE_T_COMP, /* set temp comp on */
                COMMAND_SET_ENABLE_P_COMP, /* set pres comp on */
                COMMAND_SET_DATE, /* set date MM-DD-YY */
                COMMAND_SET_TIME,               /* set time HH:MM:SS */

        /* System parameters */
                COMMAND_READ_PROGRAM_NUMBER, /* program no */
                COMMAND_READ_OZONEBACKGROUND, /* o3 bkg */
                COMMAND_READ_OZONECOEFFICIENT,  /* o3 coef */
        /* Not available on 49c legacy */
                COMMAND_READ_LAMP_SETTTING,     /* lamp setting */
        /* 49i only */
                COMMAND_READ_HOST_NAME, /* host name */
                COMMAND_READ_OZONATOR_LEVEL,    /* lX */
    };

    friend struct Command;

    class Command {
        AcquireThermoOzone49 *parent;
        CommandType command;
        CPD3::Util::ByteArray dt;

    public:
        Command();

        Command(AcquireThermoOzone49 *p, CommandType t, CPD3::Util::ByteArray d);

        inline Command(AcquireThermoOzone49 *p, CommandType t, const char *d) : Command(p, t,
                                                                                        CPD3::Util::ByteArray(
                                                                                                d))
        { }

        Command(const Command &other);

        Command &operator=(const Command &other);

        bool operator==(const Command &other) const;

        inline bool valid() const
        { return parent != NULL; }

        inline CommandType type() const
        { return command; }

        inline const CPD3::Util::ByteArray &data() const
        { return dt; }

        void invalidate();

        CPD3::Data::Variant::Root stateDescription() const;
    };

    Command issuedCommand;
    QList<Command> commandBacklog;

    enum {
        /* Normal operation. */
                SAMPLE_RUN,

        /* Zero begin blanking */
                SAMPLE_ZERO_BEGIN_FLUSH,
        /* Zero measurement */
                SAMPLE_ZERO_MEASURE,
        /* Zero end blanking */
                SAMPLE_ZERO_END_FLUSH,

        /* Spancheck initial zero blanking */
                SAMPLE_SPANCHECK_BEGINZERO_FLUSH,
        /* Spancheck initial zero measuring */
                SAMPLE_SPANCHECK_BEGINZERO_MEASURE,
        /* Spancheck level begin blanking */
                SAMPLE_SPANCHECK_LEVEL_FLUSH,
        /* Spancheck level measurement */
                SAMPLE_SPANCHECK_LEVEL_MEASURE,
        /* Spancheck final zero initial blanking */
                SAMPLE_SPANCHECK_ENDZERO_BEGIN_FLUSH,
        /* Spancheck final zero measuring */
                SAMPLE_SPANCHECK_ENDZERO_MEASURE,
        /* Spancheck final zero blanking before returning to measurement */
                SAMPLE_SPANCHECK_ENDZERO_END_FLUSH,
    } sampleState;
    int sampleStateIndex;
    double sampleStateBeginTime;

    enum {
        REQUEST_NONE, REQUEST_ZERO, REQUEST_SPANCHECK, REQUEST_SPANCHECK_ABORT,
    } modeChangeRequest;

    enum LogStreamID {
        LogStream_Metadata = 0,
        LogStream_State,

        /* Common */
                LogStream_Ozone,
        LogStream_SampleP,
        LogStream_SampleT,
        LogStream_LampT,
        LogStream_IntensityA,
        LogStream_IntensityB,
        LogStream_FlowA,
        LogStream_FlowB,

        /* 49c only */
                LogStream_FlowOzonator,

        /* 49i only */
                LogStream_OzonatorT,
        LogStream_OzonatorV,
        LogStream_LampV,


        LogStream_TOTAL,
        LogStream_RecordBaseStart = LogStream_Ozone,

        LogStream_49c_ExclusiveBegin = LogStream_FlowOzonator,
        LogStream_49c_ExclusiveEnd = LogStream_FlowOzonator,

        LogStream_49i_ExclusiveBegin = LogStream_OzonatorT,
        LogStream_49i_ExclusiveEnd = LogStream_LampV,
    };
    CPD3::Data::StaticMultiplexer loggingMux;
    quint32 streamAge[LogStream_TOTAL];
    double streamTime[LogStream_TOTAL];

    struct {
        CPD3::Data::Variant::Root X;
        CPD3::Data::Variant::Root P;
        CPD3::Data::Variant::Root T1;
        CPD3::Data::Variant::Root T2;
        CPD3::Data::Variant::Root C1;
        CPD3::Data::Variant::Root C2;
        CPD3::Data::Variant::Root Q1;
        CPD3::Data::Variant::Root Q2;

        CPD3::Data::Variant::Root Q3;

        CPD3::Data::Variant::Root T3;
        CPD3::Data::Variant::Root V1;
        CPD3::Data::Variant::Root V2;

        CPD3::Data::Variant::Root flags;

        CPD3::Data::Variant::Root ZMBP24V;
        CPD3::Data::Variant::Root ZMBP15V;
        CPD3::Data::Variant::Root ZMBP5V;
        CPD3::Data::Variant::Root ZMBP3V;
        CPD3::Data::Variant::Root ZMBN3V;

        CPD3::Data::Variant::Root ZMEP24V;
        CPD3::Data::Variant::Root ZMEP15V;
        CPD3::Data::Variant::Root ZMEP5V;
        CPD3::Data::Variant::Root ZMEP3V;
        CPD3::Data::Variant::Root ZMEN15V;

        CPD3::Data::Variant::Root ZIOP24V;
        CPD3::Data::Variant::Root ZIOP5V;
        CPD3::Data::Variant::Root ZIOP3V;
        CPD3::Data::Variant::Root ZION3V;

        CPD3::Data::Variant::Root PCT;
    } values;

    class Configuration {
        double start;
        double end;

    public:
        int address;
        double pollInterval;
        double commandDelay;

        QList<int> spancheckLevels;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under, const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    CPD3::Data::DynamicTimeInterval *zeroFlushTime;
    CPD3::Data::DynamicTimeInterval *zeroMinimumMeasureTime;
    CPD3::Data::DynamicTimeInterval *zeroMaximumMeasureTime;

    CPD3::Data::DynamicTimeInterval *levelFlushTime;
    CPD3::Data::DynamicTimeInterval *levelMinimumMeasureTime;
    CPD3::Data::DynamicTimeInterval *levelMaximumMeasureTime;

    struct MeasurementData {
        CPD3::Smoothing::BaselineSmoother *concentration;
        CPD3::Smoothing::BaselineSmoother *sampleTemperature;
        CPD3::Smoothing::BaselineSmoother *samplePressure;

        MeasurementData(const CPD3::Data::ValueSegment::Transfer &config);

        MeasurementData(const MeasurementData &other);

        ~MeasurementData();

        void reset();

        bool stable() const;

        void setResult(CPD3::Data::Variant::Write &target) const;

        inline void setResult(CPD3::Data::Variant::Write &&target) const
        { return setResult(target); }
    };

    MeasurementData *zeroMeasurement;
    QList<MeasurementData *> levelMeasurement;

    void setMeasurementMetadata(CPD3::Data::Variant::Write &target) const;

    inline void setMeasurementMetadata(CPD3::Data::Variant::Write &&target) const
    { return setMeasurementMetadata(target); }

    enum {
        Mode_Undetermined, Mode_49c_legacy, Mode_49c, Mode_49i,
    } interfaceMode;

    /* State that needs to be saved */
    CPD3::Data::SequenceValue zeroDetails;
    CPD3::Data::SequenceValue spancheckDetails;

    CPD3::Data::Variant::Root instrumentMeta;
    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;
    bool realtimeStateUpdated;
    bool zeroRealtimeUpdated;
    bool spancheckRealtimeUpdated;
    CPD3::Data::Variant::Root Xzd;

    bool zeroUpdated;
    bool spanUpdated;

    double ozoneBackground;
    double ozoneCoefficient;
    QHash<int, double> ozonatorDrive;
    QDateTime setInstrumentTime;

    /* T/P compensation is actually "reverse" STP, when enabled the instrument is reporting
     * at ambient */
    enum CompensationState {
        Compensation_Unknown,
        Compensation_SetInFlags,
        Compensation_ClearInFlags,
        Compensation_SetInReading,
        Compensation_ClearInReading,
    };
    CompensationState temperatureCompensation;
    CompensationState pressureCompensation;

    static bool compensationStateOn(CompensationState state);

    double reportingT;
    double reportingP;

    void setDefaultInvalid();

    void createMeasurementData(const CPD3::Data::ValueSegment::Transfer &configData = CPD3::Data::ValueSegment::Transfer());

    void logValue(double frameTime,
                  CPD3::Data::SequenceName::Component name,
                  CPD3::Data::Variant::Root value,
                  int streamID);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root value);

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    void invalidateLogValues(double frameTime);

    QString describeResponseState() const;

    CPD3::Data::Variant::Root describeSampleState() const;

    void invalidCommandResponse(const CPD3::Util::ByteView &frame, double frameTime, int code);

    void resetSpancheckLevels();

    void sendCommand(const Command &command, double frameTime);

    void restartSequence(double frameTime);

    CPD3::Data::Variant::Flags buildFlags() const;

    void configAdvance(double frameTime);

    void configurationChanged();

    void handleStartupAdvance(double frameTime, bool delayable = true);

public:
    AcquireThermoOzone49(const CPD3::Data::ValueSegment::Transfer &config,
                         const std::string &loggingContext);

    AcquireThermoOzone49(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireThermoOzone49();

    CPD3::Data::SequenceValue::Transfer getPersistentValues() override;

    CPD3::Data::Variant::Root getSourceMetadata() override;

    CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus() override;

    CPD3::Data::Variant::Root getCommands() override;

    CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables() override;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus() override;

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    void serializeState(QDataStream &stream) override;

    void deserializeState(QDataStream &stream) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    std::size_t dataFrameStart(std::size_t offset,
                               const CPD3::Util::ByteArray &input) const override;

    std::size_t dataFrameEnd(std::size_t start, const CPD3::Util::ByteArray &input) const override;

    void command(const CPD3::Data::Variant::Read &value) override;

    void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingInstrumentTimeout(double time) override;

    void discardDataCompleted(double time) override;
};

class AcquireThermoOzone49Component
        : public QObject, virtual public CPD3::Acquisition::AcquisitionComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_thermo_ozone49"
                              FILE
                              "acquire_thermo_ozone49.json")

    CPD3::ComponentOptions getBaseOptions();

public:
    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
