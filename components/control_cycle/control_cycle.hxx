/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CONTROLCYCLE_H
#define CONTROLCYCLE_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QLoggingCategory>

#include "core/component.hxx"
#include "core/timeutils.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/dynamictimeinterval.hxx"

class ControlCycle : public CPD3::Acquisition::AcquisitionInterface {
    std::string loggingName;
    QLoggingCategory log;

    bool terminated;
    CPD3::Acquisition::AcquisitionState *state;
    CPD3::Data::StreamSink *realtimeEgress;
    bool haveEmittedRealtimeMeta;
    CPD3::Data::StreamSink *loggingEgress;
    CPD3::Data::StreamSink *persistentEgress;

    double nextTime;
    bool updatePending;
    bool enabled;
    bool logActive;
    bool isBypassed;
    double spinupEndTime;

    double cycleStartTime;
    double cycleEndTime;

    QSet<int> requestActionExecute;

    double lastExecutedTime;
    CPD3::Data::Variant::Root lastLoggingAction;

    CPD3::Data::Variant::Root instrumentMeta;

    class Action {
        bool executed;
    public:
        std::unordered_map<std::string, CPD3::Data::Variant::Root> commands;
        double flushTime;
        CPD3::Data::Variant::Flags setSystemFlags;
        CPD3::Data::Variant::Flags clearSystemFlags;
        CPD3::Data::Variant::Flags setBypassFlags;
        CPD3::Data::Variant::Flags clearBypassFlags;
        bool setAddFlavors;
        CPD3::Data::SequenceName::Flavors addFlavors;
        QString description;
        CPD3::Data::Variant::Root identifier;

        Action();

        void reset();

        bool haveExecuted() const;

        void flagExecuted();

        QString debugDescription() const;

        void loggingValue(CPD3::Data::Variant::Write &target) const;

        inline void loggingValue(CPD3::Data::Variant::Write &&target) const
        { return loggingValue(target); }

        void command(CPD3::Data::Variant::Write &target, const QString &timeDesc) const;

        Action *clone() const;
    };

    friend class Configuration;

    class Configuration {
        double start;
        double end;

        Action *createAction(double defaultFlush,
                             const std::unordered_map<std::string, CPD3::Data::Variant::Root> &baselineCommands,
                             const CPD3::Data::Variant::Read &config);

        void handleContaminate(const CPD3::Data::Variant::Read &config,
                               ControlCycle::Action *action);

        void handleUncontaminate(const CPD3::Data::Variant::Read &config,
                                 ControlCycle::Action *action);

        void handleSetFlags(const CPD3::Data::Variant::Read &config, ControlCycle::Action *action);

        void handleClearFlags(const CPD3::Data::Variant::Read &config,
                              ControlCycle::Action *action);

        void handleSetBypass(const CPD3::Data::Variant::Read &config, ControlCycle::Action *action);

        void handleClearBypass(const CPD3::Data::Variant::Read &config,
                               ControlCycle::Action *action);

    public:
        CPD3::Data::DynamicTimeInterval *cycle;
        CPD3::Data::DynamicTimeInterval *minimumDuration;
        CPD3::Data::DynamicTimeInterval *spinupDuration;
        ControlCycle::Action *idleAction;
        ControlCycle::Action *spinupActiveAction;
        ControlCycle::Action *spinupEndAction;
        QList<QPair<CPD3::Data::DynamicTimeInterval *, ControlCycle::Action *> > actions;

        std::unordered_map<CPD3::Data::Variant::Flag, CPD3::Data::Variant::Root> systemFlags;
        std::unordered_map<CPD3::Data::Variant::Flag, CPD3::Data::Variant::Root> bypassFlags;

        bool initializeInactive;
        bool suspendOnBypass;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        ~Configuration();

        Configuration(const Configuration &other);

        Configuration &operator=(const Configuration &other);

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under, const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    void executeAction(double executeTime, Action *action, bool logAction = true);

    void setNextAction(double executeTime, double nextTime, Action *action);

    void setNoNextAction(double executeTime);

    void updateNext(double executeTime);

    void advanceTime(double executeTime);

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time);

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time);

public:
    virtual ~ControlCycle();

    ControlCycle(const CPD3::Data::ValueSegment::Transfer &config,
                 const std::string &loggingContext);

    void setControlStream(CPD3::Acquisition::AcquisitionControlStream *controlStream) override;

    void setState(CPD3::Acquisition::AcquisitionState *state) override;

    void setRealtimeEgress(CPD3::Data::StreamSink *egress) override;

    void setLoggingEgress(CPD3::Data::StreamSink *egress) override;

    void setPersistentEgress(CPD3::Data::StreamSink *egress) override;

    CPD3::Data::SequenceValue::Transfer getPersistentValues() override;

    void incomingData(const CPD3::Util::ByteView &data,
                      double time,
                      AcquisitionInterface::IncomingDataType type = IncomingDataType::Stream) override;

    void incomingControl(const CPD3::Util::ByteView &data,
                         double time,
                         AcquisitionInterface::IncomingDataType type = AcquisitionInterface::IncomingDataType::Stream) override;

    void incomingTimeout(double time) override;

    void incomingCommand(const CPD3::Data::Variant::Read &command) override;

    void incomingAdvance(double time) override;

    CPD3::Data::Variant::Root getCommands() override;

    CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables() override;

    CPD3::Data::Variant::Root getSystemFlagsMetadata() override;

    CPD3::Data::Variant::Root getSourceMetadata() override;

    CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus() override;

    AutomaticDefaults getDefaults() override;

    bool isCompleted() override;

    void signalTerminate() override;
};

class ControlCycleComponent
        : public QObject, virtual public CPD3::Acquisition::AcquisitionComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.control_cycle"
                              FILE
                              "control_cycle.json")
public:
    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config = {},
                                                                                const std::string &loggingContext = {}) override;
};


#endif
