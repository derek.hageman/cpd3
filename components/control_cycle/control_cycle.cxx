/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/composite.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "control_cycle.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

static std::string assembleLoggingCategory(const std::string &loggingContext)
{
    std::string result = "cpd3.acquisition.control.cycle";
    if (!loggingContext.empty()) {
        result += '.';
        result += loggingContext;
    }
    return result;
}

ControlCycle::ControlCycle(const ValueSegment::Transfer &configData,
                           const std::string &loggingContext) : loggingName(
        assembleLoggingCategory(loggingContext)),
                                                                log(loggingName.data()),
                                                                terminated(false),
                                                                state(NULL),
                                                                realtimeEgress(NULL),
                                                                haveEmittedRealtimeMeta(false),
                                                                loggingEgress(NULL),
                                                                nextTime(FP::undefined()),
                                                                updatePending(true),
                                                                enabled(true),
                                                                logActive(false),
                                                                isBypassed(false),
                                                                spinupEndTime(FP::undefined()),
                                                                cycleStartTime(FP::undefined()),
                                                                cycleEndTime(FP::undefined()),
                                                                requestActionExecute(),
                                                                lastExecutedTime(FP::undefined()),
                                                                lastLoggingAction(),
                                                                config()
{
    instrumentMeta["InstrumentCode"].setString("controlcycle");

    config.append(Configuration());
    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
    auto hit = Range::findIntersecting(configData, Time::time());
    if (hit != configData.end()) {
        if (hit->value().hash("EnableCycle").exists())
            enabled = hit->value().hash("EnableCycle").toBool();
        logActive = hit->value().hash("LogActiveAction").toBool();
    }
}

ControlCycle::~ControlCycle()
{
}

void ControlCycle::executeAction(double executeTime, Action *action, bool logAction)
{
    for (const auto &add : action->setBypassFlags) {
        if (state)
            state->setBypassFlag(add);
    }
    for (const auto &add : action->clearBypassFlags) {
        if (state)
            state->clearBypassFlag(add);
    }
    for (const auto &add : action->setSystemFlags) {
        if (state)
            state->setSystemFlag(add);
    }
    for (const auto &add : action->clearSystemFlags) {
        if (state)
            state->clearSystemFlag(add);
    }
    if (FP::defined(action->flushTime)) {
        if (state)
            state->requestFlush(action->flushTime);
    }
    for (const auto &send : action->commands) {
        if (state)
            state->sendCommand(send.first, send.second);
    }
    if (action->setAddFlavors) {
        if (state)
            state->setSystemFlavors(action->addFlavors);
    }

    action->flagExecuted();

    if (!logAction)
        return;

    if (logActive) {
        if (persistentEgress && FP::defined(lastExecutedTime) && executeTime > lastExecutedTime) {
            persistentEgress->incomingData(
                    SequenceValue({{}, "raw", "ZACTIVE"}, lastLoggingAction, lastExecutedTime,
                                  executeTime));
        }
    }

    action->loggingValue(lastLoggingAction.write());
    lastLoggingAction.write().hash("Time").setDouble(executeTime);
    if (realtimeEgress) {
        realtimeEgress->incomingData(
                SequenceValue({{}, "raw", "ZLAST"}, lastLoggingAction, executeTime,
                              FP::undefined()));
    }

    lastExecutedTime = executeTime;
    if (logActive) {
        persistentValuesUpdated();
    }
}

SequenceValue::Transfer ControlCycle::getPersistentValues()
{
    SequenceValue::Transfer result;
    if (!logActive)
        return result;
    if (!FP::defined(lastExecutedTime))
        return result;

    result.emplace_back(SequenceName({}, "raw", "ZACTIVE"), lastLoggingAction, lastExecutedTime,
                        FP::undefined());

    return result;
}

void ControlCycle::setNextAction(double executeTime, double nextActionTime, Action *action)
{
    nextTime = nextActionTime;
    if (FP::defined(cycleEndTime) && cycleEndTime < nextTime)
        nextTime = cycleEndTime;
    if (!config.isEmpty() &&
            FP::defined(config.first().getEnd()) &&
            config.first().getEnd() < nextTime)
        nextTime = config.first().getEnd();
    if (state != NULL)
        state->requestDataWakeup(nextTime);

    if (realtimeEgress != NULL) {
        Variant::Root act;
        action->loggingValue(act.write());
        act["Time"].setDouble(nextActionTime);
        act["Current"].set(lastLoggingAction);
        realtimeEgress->incomingData(
                SequenceValue({{}, "raw", "ZNEXT"}, std::move(act), executeTime, FP::undefined()));
    }
}

void ControlCycle::setNoNextAction(double executeTime)
{
    nextTime = cycleEndTime;
    if (!config.isEmpty() &&
            FP::defined(config.first().getEnd()) &&
            (!FP::defined(nextTime) || config.first().getEnd() < nextTime))
        nextTime = config.first().getEnd();
    if (state != NULL && FP::defined(nextTime))
        state->requestDataWakeup(nextTime);

    if (realtimeEgress != NULL) {
        Variant::Root act;
        act["Current"].set(lastLoggingAction);
        realtimeEgress->incomingData(
                SequenceValue({{}, "raw", "ZNEXT"}, std::move(act), executeTime, FP::undefined()));
    }
}

ControlCycle::Action::Action() : executed(false),
                                 commands(),
                                 flushTime(FP::undefined()),
                                 setSystemFlags(),
                                 clearSystemFlags(),
                                 setBypassFlags(),
                                 clearBypassFlags(),
                                 setAddFlavors(false),
                                 addFlavors(),
                                 description(),
                                 identifier()
{ }

void ControlCycle::Action::reset()
{ executed = false; }

bool ControlCycle::Action::haveExecuted() const
{ return executed; }

void ControlCycle::Action::flagExecuted()
{ executed = true; }

static QString displayFlags(const Variant::Flags &flags)
{
    QStringList sorted;
    for (const auto &add : flags) {
        sorted.push_back(QString::fromStdString(add));
    }
    std::sort(sorted.begin(), sorted.end());
    return sorted.join(";");
}

QString ControlCycle::Action::debugDescription() const
{
    QString result;
    if (setAddFlavors) {
        if (!result.isEmpty()) result.append(',');
        result.append("Set:");
        QStringList sort = Util::to_qstringlist(addFlavors);
        std::sort(sort.begin(), sort.end());
        result.append(sort.join(";"));
    }
    for (const auto &cmd : commands) {
        if (!result.isEmpty()) result.append(',');
        if (!cmd.first.empty()) {
            result.append(QString::fromStdString(cmd.first));
            result.append('=');
        }
        QStringList sorted;
        for (const auto &add : cmd.second.read().toHash().keys()) {
            sorted.push_back(QString::fromStdString(add));
        }
        std::sort(sorted.begin(), sorted.end());
        result.append(sorted.join(";"));
    }

    if (!setSystemFlags.empty()) {
        if (!result.isEmpty()) result.append(',');
        result.append("Flags:");
        result.append(displayFlags(setSystemFlags));
    }
    if (!clearSystemFlags.empty()) {
        if (!result.isEmpty()) result.append(',');
        result.append("ClearFlags:");
        result.append(displayFlags(clearSystemFlags));
    }
    if (!setBypassFlags.empty()) {
        if (!result.isEmpty()) result.append(',');
        result.append("Bypass:");
        result.append(displayFlags(setBypassFlags));
    }
    if (!clearBypassFlags.empty()) {
        if (!result.isEmpty()) result.append(',');
        result.append("ClearBypass:");
        result.append(displayFlags(clearBypassFlags));
    }
    if (FP::defined(flushTime)) {
        if (!result.isEmpty()) result.append(',');
        result.append("Flush:");
        result.append(QString::number(flushTime));
    }
    return result;
}

void ControlCycle::Action::loggingValue(Variant::Write &target) const
{
    if (setAddFlavors) {
        Variant::Flags flags;
        for (const auto &add : addFlavors) {
            flags.insert(add);
        }
        target.hash("Flavors").setFlags(std::move(flags));
    }
    if (!description.isEmpty()) {
        target.hash("Description").setString(description);
    }
    if (identifier.read().exists()) {
        target.hash("Identifier").set(identifier);
    }
}

void ControlCycle::Action::command(Variant::Write &target, const QString &timeDesc) const
{
    if (!description.isEmpty()) {
        target.hash("DisplayName").setString(description);
    } else {
        QString desc;
        if (setAddFlavors) {
            QStringList sorted = Util::to_qstringlist(addFlavors);
            std::sort(sorted.begin(), sorted.end());
            desc.append(sorted.join(",").toUpper());
        }
        if (desc.isEmpty()) {
            desc = QString("Execute event from %1").arg(timeDesc);
        } else {
            desc = QString("Execute %1 from %2").arg(desc, timeDesc);
        }
        target.hash("DisplayName").setString(desc);
    }

    target.hash("ToolTip")
          .setString(QString("This will execute the action scheduled at %1 into the cycle.").arg(
                  timeDesc));
}

ControlCycle::Action *ControlCycle::Action::clone() const
{ return new Action(*this); }

ControlCycle::Configuration::Configuration() : start(FP::undefined()),
                                               end(FP::undefined()),
                                               cycle(new DynamicTimeInterval::Constant(Time::Hour,
                                                                                       1, true)),
                                               minimumDuration(new DynamicTimeInterval::Undefined),
                                               spinupDuration(new DynamicTimeInterval::Undefined),
                                               idleAction(new ControlCycle::Action),
                                               spinupActiveAction(new ControlCycle::Action),
                                               spinupEndAction(new ControlCycle::Action),
                                               actions(),
                                               systemFlags(),
                                               bypassFlags(),
                                               initializeInactive(false),
                                               suspendOnBypass(false)
{ }

ControlCycle::Configuration::~Configuration()
{
    delete cycle;
    delete minimumDuration;
    delete spinupDuration;
    delete idleAction;
    delete spinupActiveAction;
    delete spinupEndAction;
    for (QList<QPair<DynamicTimeInterval *, ControlCycle::Action *> >::iterator
            action = actions.begin(), endAction = actions.end(); action != endAction; ++action) {
        delete action->first;
        delete action->second;
    }
}

ControlCycle::Configuration::Configuration(const Configuration &other) : start(other.start),
                                                                         end(other.end),
                                                                         cycle(other.cycle
                                                                                    ->clone()),
                                                                         minimumDuration(
                                                                                 other.minimumDuration
                                                                                      ->clone()),
                                                                         spinupDuration(
                                                                                 other.spinupDuration
                                                                                      ->clone()),
                                                                         idleAction(other.idleAction
                                                                                         ->clone()),
                                                                         spinupActiveAction(
                                                                                 other.spinupActiveAction
                                                                                      ->clone()),
                                                                         spinupEndAction(
                                                                                 other.spinupEndAction
                                                                                      ->clone()),
                                                                         actions(),
                                                                         systemFlags(
                                                                                 other.systemFlags),
                                                                         bypassFlags(
                                                                                 other.bypassFlags),
                                                                         initializeInactive(
                                                                                 other.initializeInactive),
                                                                         suspendOnBypass(
                                                                                 other.suspendOnBypass)
{
    for (QList<QPair<DynamicTimeInterval *, Action *> >::const_iterator
            action = other.actions.constBegin(), endAction = other.actions.constEnd();
            action != endAction;
            ++action) {
        actions.append(QPair<DynamicTimeInterval *, ControlCycle::Action *>(action->first->clone(),
                                                                            action->second
                                                                                  ->clone()));
    }
}

ControlCycle::Configuration &ControlCycle::Configuration::operator=(const Configuration &other)
{
    if (&other == this) return *this;
    start = other.start;
    end = other.end;
    delete cycle;
    cycle = other.cycle->clone();
    delete minimumDuration;
    minimumDuration = other.minimumDuration->clone();
    delete spinupDuration;
    spinupDuration = other.spinupDuration->clone();
    delete idleAction;
    idleAction = other.idleAction->clone();
    delete spinupActiveAction;
    spinupActiveAction = other.spinupActiveAction->clone();
    delete spinupEndAction;
    spinupEndAction = other.spinupEndAction->clone();
    systemFlags = other.systemFlags;
    bypassFlags = other.bypassFlags;
    initializeInactive = other.initializeInactive;
    suspendOnBypass = other.suspendOnBypass;

    for (QList<QPair<DynamicTimeInterval *, ControlCycle::Action *> >::iterator
            action = actions.begin(), endAction = actions.end(); action != endAction; ++action) {
        delete action->first;
        delete action->second;
    }
    actions.clear();
    for (QList<QPair<DynamicTimeInterval *, Action *> >::const_iterator
            action = other.actions.constBegin(), endAction = other.actions.constEnd();
            action != endAction;
            ++action) {
        actions.append(QPair<DynamicTimeInterval *, ControlCycle::Action *>(action->first->clone(),
                                                                            action->second
                                                                                  ->clone()));
    }
    return *this;
}

ControlCycle::Configuration::Configuration(const Configuration &other, double s, double e) : start(
        s),
                                                                                             end(e),
                                                                                             cycle(other.cycle
                                                                                                        ->clone()),
                                                                                             minimumDuration(
                                                                                                     other.minimumDuration
                                                                                                          ->clone()),
                                                                                             spinupDuration(
                                                                                                     other.spinupDuration
                                                                                                          ->clone()),
                                                                                             idleAction(
                                                                                                     other.idleAction
                                                                                                          ->clone()),
                                                                                             spinupActiveAction(
                                                                                                     other.spinupActiveAction
                                                                                                          ->clone()),
                                                                                             spinupEndAction(
                                                                                                     other.spinupEndAction
                                                                                                          ->clone()),
                                                                                             actions(),
                                                                                             systemFlags(
                                                                                                     other.systemFlags),
                                                                                             bypassFlags(
                                                                                                     other.bypassFlags),
                                                                                             initializeInactive(
                                                                                                     other.initializeInactive),
                                                                                             suspendOnBypass(
                                                                                                     other.suspendOnBypass)
{
    for (QList<QPair<DynamicTimeInterval *, Action *> >::const_iterator
            action = other.actions.constBegin(), endAction = other.actions.constEnd();
            action != endAction;
            ++action) {
        actions.append(QPair<DynamicTimeInterval *, ControlCycle::Action *>(action->first->clone(),
                                                                            action->second
                                                                                  ->clone()));
    }
}

ControlCycle::Configuration::Configuration(const ValueSegment &other, double s, double e) : start(
        s),
                                                                                            end(e),
                                                                                            cycle(new DynamicTimeInterval::Constant(
                                                                                                    Time::Hour,
                                                                                                    1,
                                                                                                    true)),
                                                                                            minimumDuration(
                                                                                                    new DynamicTimeInterval::Undefined),
                                                                                            spinupDuration(
                                                                                                    new DynamicTimeInterval::Undefined),
                                                                                            idleAction(
                                                                                                    new ControlCycle::Action),
                                                                                            spinupActiveAction(
                                                                                                    new ControlCycle::Action),
                                                                                            spinupEndAction(
                                                                                                    new ControlCycle::Action),
                                                                                            actions(),
                                                                                            systemFlags(),
                                                                                            bypassFlags(),
                                                                                            initializeInactive(
                                                                                                    false),
                                                                                            suspendOnBypass(
                                                                                                    false)
{
    setFromSegment(other);
}

ControlCycle::Configuration::Configuration(const Configuration &under, const ValueSegment &over,
                                           double s,
                                           double e) : start(s),
                                                       end(e),
                                                       cycle(under.cycle->clone()),
                                                       minimumDuration(
                                                               under.minimumDuration->clone()),
                                                       spinupDuration(
                                                               under.spinupDuration->clone()),
                                                       idleAction(under.idleAction->clone()),
                                                       spinupActiveAction(
                                                               under.spinupActiveAction->clone()),
                                                       spinupEndAction(
                                                               under.spinupEndAction->clone()),
                                                       actions(),
                                                       systemFlags(under.systemFlags),
                                                       bypassFlags(under.bypassFlags),
                                                       initializeInactive(under.initializeInactive),
                                                       suspendOnBypass(under.suspendOnBypass)
{
    for (QList<QPair<DynamicTimeInterval *, ControlCycle::Action *> >::const_iterator
            action = under.actions.constBegin(), endAction = under.actions.constEnd();
            action != endAction;
            ++action) {
        actions.append(QPair<DynamicTimeInterval *, ControlCycle::Action *>(action->first->clone(),
                                                                            action->second
                                                                                  ->clone()));
    }
    setFromSegment(over);
}

static DynamicTimeInterval *parseTimeInterval(const Variant::Read &config)
{
    return DynamicTimeInterval::fromConfiguration(ValueSegment::Transfer{
            ValueSegment(FP::undefined(), FP::undefined(), Variant::Root(config))});
}

void ControlCycle::Configuration::handleContaminate(const Variant::Read &config,
                                                    ControlCycle::Action *action)
{
    switch (config.getType()) {
    case Variant::Type::String: {
        auto check = config.toString();
        if (!Variant::Composite::isContaminationFlag(check)) {
            check = "Contaminate" + check;
        }
        action->setSystemFlags.insert(check);

        Variant::Root meta;
        meta["Description"].setString("Data contaminated due to periodic cycle scheduling");
        meta["Origin"].toArray().after_back().setString("control_cycle");
        meta["Bits"].setInt64(0x000F);
        Util::insert_or_assign(systemFlags, std::move(check), std::move(meta));
        break;
    }
    case Variant::Type::Flags:
        for (auto check : config.toFlags()) {
            if (!Variant::Composite::isContaminationFlag(check)) {
                check = "Contaminate" + check;
            }
            action->setSystemFlags.insert(check);

            Variant::Root meta;
            meta["Description"].setString("Data contaminated due to periodic cycle scheduling");
            meta["Origin"].toArray().after_back().setString("control_cycle");
            meta["Bits"].setInt64(0x000F);
            Util::insert_or_assign(systemFlags, std::move(check), std::move(meta));
        }
        break;
    case Variant::Type::Hash: {
        for (auto add : config.toHash()) {
            if (add.first.empty())
                continue;
            Variant::Flag check = add.first;
            if (!Variant::Composite::isContaminationFlag(check)) {
                check = "Contaminate" + check;
            }
            action->setSystemFlags.insert(check);

            Variant::Root meta(add.second);
            meta["Origin"].toArray().after_back().setString("control_cycle");
            qint64 bits(meta["Bits"].toInt64());
            if (!INTEGER::defined(bits)) {
                meta["Bits"].setInt64(0x000F);
            } else if (!(bits & Q_INT64_C(0xF))) {
                bits |= Q_INT64_C(0xF);
                meta["Bits"].setInt64(bits);
            }
            if (!meta["Description"].exists()) {
                meta["Description"].setString("Data contaminated due to periodic cycle scheduling");
            }

            Util::insert_or_assign(systemFlags, std::move(check), std::move(meta));
        }
        break;
    }
    case Variant::Type::Boolean:
        if (config.toBool()) {
            action->setSystemFlags.insert("Contaminated");
        } else {
            action->clearSystemFlags.insert("Contaminated");
        }
        break;
    default:
        break;
    }
}

void ControlCycle::Configuration::handleUncontaminate(const Variant::Read &config,
                                                      ControlCycle::Action *action)
{
    switch (config.getType()) {
    case Variant::Type::Boolean:
        if (config.toBool()) {
            action->clearSystemFlags.insert("Contaminated");
        } else {
            action->setSystemFlags.insert("Contaminated");
        }
        break;
    default:
        for (const auto &add : config.toChildren().keys()) {
            if (add.empty())
                continue;
            Variant::Flag check = add;
            if (!Variant::Composite::isContaminationFlag(check)) {
                check = "Contaminate" + check;
            }
            action->clearSystemFlags.insert(std::move(check));
        }
        break;
    }
}

void ControlCycle::Configuration::handleSetFlags(const Variant::Read &config,
                                                 ControlCycle::Action *action)
{
    switch (config.getType()) {
    case Variant::Type::String: {
        const auto &add = config.toString();
        action->setSystemFlags.insert(add);

        Variant::Root meta;
        meta["Origin"].toArray().after_back().setString("control_cycle");
        Util::insert_or_assign(systemFlags, add, std::move(meta));
        break;
    }
    case Variant::Type::Flags: {
        for (const auto &add : config.toFlags()) {
            action->setSystemFlags.insert(add);

            Variant::Root meta;
            meta["Origin"].toArray().after_back().setString("control_cycle");
            Util::insert_or_assign(systemFlags, add, std::move(meta));
        }
        break;
    }
    case Variant::Type::Hash: {
        for (const auto &add : config.toHash()) {
            if (add.first.empty())
                continue;
            action->setSystemFlags.insert(add.first);

            Variant::Root meta(add.second);
            meta["Origin"].toArray().after_back().setString("control_cycle");
            Util::insert_or_assign(systemFlags, add.first, std::move(meta));
        }
        break;
    }
    default:
        break;
    }
}

void ControlCycle::Configuration::handleClearFlags(const Variant::Read &config,
                                                   ControlCycle::Action *action)
{
    Util::merge(config.toChildren().keys(), action->clearSystemFlags);
}

void ControlCycle::Configuration::handleSetBypass(const Variant::Read &config,
                                                  ControlCycle::Action *action)
{
    switch (config.getType()) {
    case Variant::Type::String: {
        const auto &check = config.toString();
        action->setBypassFlags.insert(check);

        Variant::Root meta(Variant::Type::Hash);
        Util::insert_or_assign(bypassFlags, check, std::move(meta));
        break;
    }
    case Variant::Type::Flags: {
        for (const auto &add : config.toFlags()) {
            action->setBypassFlags.insert(add);

            Variant::Root meta(Variant::Type::Hash);
            Util::insert_or_assign(bypassFlags, add, std::move(meta));
        }
        break;
    }
    case Variant::Type::Hash: {
        for (auto add : config.toHash()) {
            if (add.first.empty())
                continue;
            action->setBypassFlags.insert(add.first);

            Variant::Root meta(add.second);
            meta["Origin"].toArray().after_back().setString("control_cycle");
            Util::insert_or_assign(bypassFlags, add.first, std::move(meta));
        }
        break;
    }
    case Variant::Type::Boolean: {
        if (config.toBool()) {
            action->setBypassFlags.insert("Bypass");
            Variant::Root meta(Variant::Type::Hash);
            Util::insert_or_assign(bypassFlags, "Bypass", std::move(meta));
        } else {
            action->clearBypassFlags.insert("Bypass");
        }
    }
    default:
        break;
    }
}

void ControlCycle::Configuration::handleClearBypass(const Variant::Read &config,
                                                    ControlCycle::Action *action)
{
    switch (config.getType()) {
    case Variant::Type::Boolean:
        if (!config.toBool()) {
            action->setBypassFlags.insert("Bypass");
            Variant::Root meta(Variant::Type::Hash);
            Util::insert_or_assign(bypassFlags, "Bypass", std::move(meta));
        } else {
            action->clearBypassFlags.insert("Bypass");
        }
        break;
    default:
        Util::merge(config.toChildren().keys(), action->clearBypassFlags);
        break;
    }
}

ControlCycle::Action *ControlCycle::Configuration::createAction(double defaultFlush,
                                                                const std::unordered_map<
                                                                        std::string, Variant::Root> &baselineCommands,
                                                                const Variant::Read &config)
{
    ControlCycle::Action *action = new ControlCycle::Action;

    action->commands = baselineCommands;
    if (config["InstrumentCommands"].exists()) {
        for (auto add : config["InstrumentCommands"].toHash()) {
            if (add.first.empty())
                continue;

            auto merge = action->commands.find(add.first);
            if (merge == action->commands.end()) {
                action->commands.emplace(add.first, Variant::Root(add.second));
                continue;
            }

            merge->second = Variant::Root::overlay(merge->second, Variant::Root(add.second));
        }
    }
    if (config["Commands"].exists()) {
        auto merge = action->commands.find(std::string());
        if (merge == action->commands.end()) {
            action->commands.emplace(std::string(), Variant::Root(config["Commands"]));
        } else {
            merge->second =
                    Variant::Root::overlay(merge->second, Variant::Root(config["Commands"]));
        }
    }

    if (config["FlushTime"].exists()) {
        action->flushTime = config["FlushTime"].toDouble();
    } else {
        action->flushTime = defaultFlush;
    }

    if (config["Description"].exists()) {
        action->description = config["Description"].toDisplayString();
    }
    if (config["Identifier"].exists()) {
        action->identifier.write().set(config["Identifier"]);
    }

    if (config["Contaminate"].exists())
        handleContaminate(config["Contaminate"], action);
    if (config["Contaminated"].exists())
        handleContaminate(config["Contaminated"], action);
    if (config["Uncontaminate"].exists())
        handleUncontaminate(config["Uncontaminate"], action);
    if (config["UnContaminate"].exists())
        handleUncontaminate(config["UnContaminate"], action);
    if (config["Uncontaminated"].exists())
        handleUncontaminate(config["Uncontaminated"], action);
    if (config["UnContaminated"].exists())
        handleUncontaminate(config["UnContaminated"], action);
    if (config["ClearContaminate"].exists())
        handleUncontaminate(config["ClearContaminate"], action);
    if (config["ClearContamination"].exists())
        handleUncontaminate(config["ClearContamination"], action);
    if (config["ClearContaminated"].exists())
        handleUncontaminate(config["ClearContaminated"], action);

    if (config["SetFlags"].exists())
        handleSetFlags(config["SetFlags"], action);
    if (config["SetFlag"].exists())
        handleSetFlags(config["SetFlag"], action);
    if (config["ClearFlags"].exists())
        handleClearFlags(config["ClearFlags"], action);
    if (config["ClearFlag"].exists())
        handleClearFlags(config["ClearFlag"], action);

    if (config["Bypass"].exists())
        handleSetBypass(config["Bypass"], action);
    if (config["Bypassed"].exists())
        handleSetBypass(config["Bypassed"], action);
    if (config["Unbypass"].exists())
        handleClearBypass(config["Unbypass"], action);
    if (config["UnBypass"].exists())
        handleClearBypass(config["UnBypass"], action);
    if (config["ClearBypass"].exists())
        handleClearBypass(config["ClearBypass"], action);


    if (config["SetFlavors"].exists()) {
        action->setAddFlavors = true;
        action->addFlavors = SequenceName::toFlavors(config["SetFlavors"]);
    }
    if (config["SetCut"].exists()) {
        auto str = config["SetCut"].toString();
        if (Util::equal_insensitive(str, "pm1", "1", "fine")) {
            action->addFlavors.insert(SequenceName::flavor_pm1);
        } else if (Util::equal_insensitive(str, "pm10", "10", "coarse")) {
            action->addFlavors.insert(SequenceName::flavor_pm10);
        } else if (Util::equal_insensitive(str, "pm25", "25", "2.5", "pm2.5")) {
            action->addFlavors.insert(SequenceName::flavor_pm25);
        } else {
            action->addFlavors.erase(SequenceName::flavor_pm1);
            action->addFlavors.erase(SequenceName::flavor_pm10);
            action->addFlavors.erase(SequenceName::flavor_pm25);
        }
        action->setAddFlavors = true;
    }
    action->addFlavors.erase(SequenceName::Component());

    return action;
}

void ControlCycle::Configuration::setFromSegment(const ValueSegment &config)
{
    double defaultFlush = config["DefaultFlushTime"].toDouble();

    if (config["Period"].exists()) {
        if (cycle != NULL)
            delete cycle;
        cycle = parseTimeInterval(config["Period"]);
        Q_ASSERT(cycle != NULL);
    }

    if (config["MinimumTime"].exists()) {
        if (minimumDuration != NULL)
            delete minimumDuration;
        minimumDuration = parseTimeInterval(config["MinimumTime"]);
        Q_ASSERT(minimumDuration != NULL);
    }

    if (config["Spinup/Duration"].exists()) {
        if (spinupDuration != NULL)
            delete spinupDuration;
        spinupDuration = parseTimeInterval(config["Spinup/Duration"]);
        Q_ASSERT(spinupDuration != NULL);
    }

    if (config["InitializeInactive"].exists()) {
        initializeInactive = config["InitializeInactive"].toBool();
    }

    if (config["SuspendOnBypass"].exists()) {
        suspendOnBypass = config["SuspendOnBypass"].toBool();
    }

    std::unordered_map<std::string, CPD3::Data::Variant::Root> baselineCommands;
    if (config["BaselineInstrumentCommands"].exists()) {
        for (auto add : config["BaselineInstrumentCommands"].toHash()) {
            if (add.first.empty())
                continue;
            baselineCommands.emplace(add.first, Variant::Root(add.second));
        }
    }
    if (config["BaselineCommands"].exists()) {
        baselineCommands.emplace(std::string(), Variant::Root(config["BaselineCommands"]));
    }

    if (config["Actions"].exists()) {
        for (QList<QPair<DynamicTimeInterval *, ControlCycle::Action *> >::iterator
                action = actions.begin(), endAction = actions.end();
                action != endAction;
                ++action) {
            delete action->first;
            delete action->second;
        }
        actions.clear();
        for (auto add : config["Actions"].toChildren()) {
            ControlCycle::Action *result = createAction(defaultFlush, baselineCommands, add);
            if (!result)
                continue;
            QPair<DynamicTimeInterval *, ControlCycle::Action *>
                    action(parseTimeInterval(add), result);
            Q_ASSERT(action.first != NULL);
            actions.append(action);
        }
    }

    if (config["IdleAction"].exists()) {
        ControlCycle::Action
                *check = createAction(defaultFlush, baselineCommands, config["IdleAction"]);
        if (check != NULL) {
            delete idleAction;
            idleAction = check;
        }
    }

    if (config["Spinup/Active"].exists()) {
        ControlCycle::Action
                *check = createAction(defaultFlush, baselineCommands, config["Spinup/Active"]);
        if (check != NULL) {
            delete spinupActiveAction;
            spinupActiveAction = check;
        }
    }

    if (config["Spinup/End"].exists()) {
        ControlCycle::Action
                *check = createAction(defaultFlush, baselineCommands, config["Spinup/End"]);
        if (check != NULL) {
            delete spinupEndAction;
            spinupEndAction = check;
        }
    }
}

static double applyEndRounding(DynamicTimeInterval *range,
                               double executeTime,
                               double cycleStartTime)
{
    double cycleEndTime = cycleStartTime;
    for (int t = 0;; t++) {
        cycleEndTime = range->apply(executeTime, cycleEndTime);
        if (!FP::defined(cycleEndTime)) {
            cycleEndTime = cycleStartTime + 1.0;
            break;
        }
        if (cycleEndTime > executeTime)
            break;
        if (t > 1000000) {
            cycleEndTime = range->apply(executeTime, executeTime, true);
            if (!FP::defined(cycleEndTime) || cycleEndTime < executeTime)
                cycleEndTime = executeTime + 1.0;
            break;
        }
    }
    return cycleEndTime;
}

void ControlCycle::updateNext(double executeTime)
{
    Q_ASSERT(FP::defined(executeTime));
    {
        int oldSize = config.size();
        if (!Range::intersectShift(config, executeTime)) {
            qCWarning(log) << "Can't find active configuration (list corrupted)";
        }
        if (oldSize != config.size())
                generalStatusUpdated();
        if (config.isEmpty())
            return;
    }

    if (!requestActionExecute.isEmpty()) {
        for (QSet<int>::const_iterator req = requestActionExecute.constBegin(),
                endReq = requestActionExecute.constEnd(); req != endReq; ++req) {
            int idx = *req;
            if (idx < 0 || idx >= config.first().actions.size())
                continue;

            Action *act = config.first().actions.at(idx).second;

            executeAction(executeTime, act);
            qCDebug(log) << "Action" << act->debugDescription() << "executing at"
                         << Logging::time(executeTime) << "from a manual request";

        }
        requestActionExecute.clear();
        if (!enabled)
            return;
    }


    if (!enabled) {
        cycleStartTime = FP::undefined();
        cycleEndTime = FP::undefined();
        qCDebug(log) << "Executing idle action at" << Logging::time(executeTime) << ":"
                     << config.first().idleAction->debugDescription();
        executeAction(executeTime, config.first().idleAction);
        setNoNextAction(executeTime);

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "F1"}, Variant::Root(Variant::Flags{"Disabled"}),
                                  executeTime, FP::undefined()));
        }
        return;
    }

    if (!FP::defined(cycleStartTime) || !FP::defined(cycleEndTime) || cycleEndTime <= executeTime) {
        Q_ASSERT(config.first().cycle != NULL);

        bool isFirst = false;
        if (!FP::defined(cycleStartTime)) {
            cycleStartTime = config.first().cycle->round(executeTime, executeTime);
            if (!FP::defined(cycleStartTime))
                cycleStartTime = executeTime;
            cycleEndTime = applyEndRounding(config.first().cycle, executeTime, cycleStartTime);

            double minimumTime =
                    config.first().minimumDuration->apply(executeTime, executeTime, true);
            double originalEnd = cycleEndTime;
            for (int t = 1;
                    FP::defined(minimumTime) &&
                            FP::defined(cycleEndTime) &&
                            minimumTime > cycleEndTime;
                    t++) {
                cycleEndTime = config.first().cycle->apply(executeTime, originalEnd, false, t);
                if (!FP::defined(cycleEndTime)) {
                    cycleEndTime = applyEndRounding(config.first().cycle, executeTime, minimumTime);
                    break;
                }

                /* Give up */
                if (t > 10000) {
                    qCDebug(log) << "Cycle initialize overflow, using simple rounding.  Original "
                                 << Logging::range(cycleStartTime, originalEnd);
                    cycleEndTime = applyEndRounding(config.first().cycle, executeTime, minimumTime);
                    break;
                }
            }

            spinupEndTime = config.first().spinupDuration->apply(executeTime, executeTime, true);

            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "F1"}, Variant::Root(Variant::Type::Flags),
                                      executeTime, FP::undefined()));
            }

            isFirst = true;
            config.first().spinupActiveAction->reset();
            config.first().spinupEndAction->reset();
        } else {
            double originalStart = cycleStartTime;
            double originalEnd = cycleEndTime;
            cycleStartTime = cycleEndTime;
            for (int t = 1;; t++) {
                cycleEndTime = config.first().cycle->apply(executeTime, originalEnd, false, t);
                if (!FP::defined(cycleEndTime)) {
                    cycleEndTime =
                            applyEndRounding(config.first().cycle, executeTime, cycleStartTime);
                    break;
                }
                if (cycleEndTime > executeTime && cycleEndTime > originalEnd)
                    break;
                cycleStartTime = cycleEndTime;

                /* Give up */
                if (t > 10000) {
                    qCDebug(log) << "Cycle advance overflow, using simple rounding.  Original"
                                 << Logging::range(originalStart, originalEnd);
                    cycleEndTime =
                            applyEndRounding(config.first().cycle, executeTime, cycleStartTime);
                    break;
                }
            }
        }

        for (QList<QPair<DynamicTimeInterval *, Action *> >::iterator
                action = config.first().actions.begin(), endAction = config.first().actions.end();
                action != endAction;
                ++action) {
            Q_ASSERT(action->second != NULL);
            action->second->reset();
            if (isFirst && config.first().initializeInactive)
                action->second->flagExecuted();
        }

        qCDebug(log) << "Cycle bounds set to" << Logging::range(cycleStartTime, cycleEndTime)
                     << "at" << Logging::time(executeTime);
    }

    double timeFirstNextCycle = FP::undefined();
    Action *actionFirstNextCycle = NULL;
    double timeFirstThisCycle = FP::undefined();
    Action *actionFirstThisCycle = NULL;
    double timeFirstExecute = FP::undefined();
    Action *actionFirstExecute = NULL;

    for (QList<QPair<DynamicTimeInterval *, Action *> >::iterator
            action = config.first().actions.begin(), endAction = config.first().actions.end();
            action != endAction;
            ++action) {
        Q_ASSERT(action->first != NULL);
        Q_ASSERT(action->second != NULL);
        double timeCycle = action->first->apply(executeTime, cycleEndTime);
        if (FP::defined(timeCycle) &&
                (!FP::defined(timeFirstNextCycle) || timeCycle < timeFirstNextCycle)) {
            timeFirstNextCycle = timeCycle;
            actionFirstNextCycle = action->second;
        }

        if (action->second->haveExecuted())
            continue;

        timeCycle = action->first->apply(executeTime, cycleStartTime);
        if (!FP::defined(timeCycle))
            continue;
        if (timeCycle <= executeTime) {
            if (!FP::defined(timeFirstExecute)) {
                timeFirstExecute = timeCycle;
                actionFirstExecute = action->second;
            } else if (timeCycle > timeFirstExecute) {
                Q_ASSERT(actionFirstExecute != NULL);
                actionFirstExecute->flagExecuted();

                timeFirstExecute = timeCycle;
                actionFirstExecute = action->second;
            }
            continue;
        }

        if (!FP::defined(timeFirstThisCycle) || timeCycle < timeFirstThisCycle) {
            timeFirstThisCycle = timeCycle;
            actionFirstThisCycle = action->second;
        }
    }

    if (FP::defined(spinupEndTime) && spinupEndTime > executeTime) {
        if (!config.first().spinupActiveAction->haveExecuted() &&
                (!isBypassed || !config.first().suspendOnBypass)) {
            qCDebug(log) << "Executing spinup action at" << Logging::time(executeTime) << ":"
                         << config.first().spinupActiveAction->debugDescription();
            executeAction(executeTime, config.first().spinupActiveAction);
        }

        /* If there's something pending to execute, then flag it as the next as soon as the
         * spinup would end and don't do the normal next action selection. */
        if (FP::defined(timeFirstExecute)) {
            Q_ASSERT(actionFirstExecute != NULL);
            setNextAction(executeTime, spinupEndTime, actionFirstExecute);

            qCDebug(log) << "Next action selected after spinup:"
                         << actionFirstExecute->debugDescription() << "with a target time of"
                         << Logging::time(spinupEndTime) << "at" << Logging::time(executeTime);
            return;
        }
    } else if (!isBypassed || !config.first().suspendOnBypass) {
        if (!config.first().spinupEndAction->haveExecuted()) {
            qCDebug(log) << "Executing spinup end action at" << Logging::time(executeTime) << ":"
                         << config.first().spinupEndAction->debugDescription();
            executeAction(executeTime, config.first().spinupEndAction,
                          !FP::defined(timeFirstExecute));
        }

        if (FP::defined(timeFirstExecute)) {
            Q_ASSERT(actionFirstExecute != NULL);
            executeAction(executeTime, actionFirstExecute);
            qCDebug(log) << "Action" << actionFirstExecute->debugDescription() << "executing at"
                         << Logging::time(executeTime) << "from scheduled time"
                         << Logging::time(timeFirstExecute);
        }
    }

    if (FP::defined(timeFirstNextCycle) && FP::defined(timeFirstThisCycle)) {
        if (timeFirstThisCycle < timeFirstNextCycle) {
            setNextAction(executeTime, timeFirstThisCycle, actionFirstThisCycle);
            qCDebug(log) << "Next action selected in current cycle:"
                         << actionFirstThisCycle->debugDescription() << "with a scheduled time of"
                         << Logging::time(timeFirstThisCycle) << "at" << Logging::time(executeTime);
        } else {
            setNextAction(executeTime, timeFirstNextCycle, actionFirstNextCycle);
            qCDebug(log) << "Next action selected in next cycle:"
                         << actionFirstNextCycle->debugDescription() << "with a scheduled time of"
                         << Logging::time(timeFirstNextCycle) << "at" << Logging::time(executeTime);
        }
    } else if (FP::defined(timeFirstThisCycle)) {
        setNextAction(executeTime, timeFirstThisCycle, actionFirstThisCycle);
        qCDebug(log) << "Next action selected in current cycle with no next cycle:"
                     << actionFirstThisCycle->debugDescription() << "with a scheduled time of"
                     << Logging::time(timeFirstThisCycle) << "at" << Logging::time(executeTime);
    } else if (FP::defined(timeFirstNextCycle)) {
        setNextAction(executeTime, timeFirstNextCycle, actionFirstNextCycle);
        qCDebug(log) << "Next action selected in next cycle with current cycle completed:"
                     << actionFirstNextCycle->debugDescription() << "with a scheduled time of"
                     << Logging::time(timeFirstNextCycle) << " at " << Logging::time(executeTime);
    } else {
        setNoNextAction(executeTime);
        qCDebug(log) << "No possible next action available at" << Logging::time(executeTime);
    }
}

void ControlCycle::advanceTime(double executeTime)
{
    if (!FP::defined(executeTime))
        return;

    if (!haveEmittedRealtimeMeta && realtimeEgress != NULL) {
        haveEmittedRealtimeMeta = true;
        realtimeEgress->incomingData(buildRealtimeMeta(executeTime));
    }

    if (loggingEgress != NULL) {
        if (logActive) {
            loggingEgress->incomingData(buildLogMeta(executeTime));
        }
        loggingEgress->endData();
        loggingEgress = NULL;
    }

    if (updatePending) {
        updateNext(executeTime);
        updatePending = false;
    }

    if (!enabled)
        return;
    if (!FP::defined(nextTime))
        return;
    if (executeTime < nextTime)
        return;

    updateNext(executeTime);
}

SequenceValue::Transfer ControlCycle::buildLogMeta(double time)
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("control_cycle");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());

    result.emplace_back(SequenceName({}, "raw_meta", "ZACTIVE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataHash("Description").setString("Currently active control point");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
    result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataHashChild("Flavors")
          .metadataFlags("Description")
          .setString("Flavors added by the control point");
    result.back().write().metadataHashChild("Description")
          .metadataString("Description")
          .setString("The description of the control point");
    result.back().write().metadataHashChild("Identifier")
          .metadataHash("Description")
          .setString("The control point identifier data");
    result.back().write().metadataHashChild("Identifier")
          .metadataHash("ArbitraryStructure")
          .setBool(true);

    return result;
}

SequenceValue::Transfer ControlCycle::buildRealtimeMeta(double time)
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("control_cycle");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());


    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);
    result.back().write().metadataFlags("Realtime").hash("Hide").setBool(true);
    result.back().write().metadataFlags("Realtime").hash("Persistent").setBool(true);

    result.back().write()
          .metadataSingleFlag("Disabled")
          .hash("Description")
          .setString("Cycle execution disabled");
    result.back().write()
          .metadataSingleFlag("Disabled")
          .hash("Origin").toArray().after_back().setString("control_cycle");


    result.emplace_back(SequenceName({}, "raw_meta", "ZLAST"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataHash("Description").setString("Last executed control point");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
    result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataHash("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataHash("Realtime").hash("Hide").setBool(true);
    result.back().write().metadataHash("Realtime").hash("Name").setString("Last");
    result.back().write()
          .metadataHash("Realtime")
          .hash("HashFormat")
          .setString("${TIME|Time} ${FLAGS|Flavors|,|uc}");
    result.back().write().metadataHashChild("Flavors")
          .metadataFlags("Description")
          .setString("Flavors added by the control point");
    result.back().write().metadataHashChild("Description")
          .metadataString("Description")
          .setString("The description of the control point");
    result.back().write().metadataHashChild("Identifier")
          .metadataHash("Description")
          .setString("The control point identifier data");
    result.back().write().metadataHashChild("Identifier")
          .metadataHash("ArbitraryStructure")
          .setBool(true);


    result.emplace_back(SequenceName({}, "raw_meta", "ZNEXT"), Variant::Root(), time,
                        FP::undefined());
    result.back().write()
          .metadataHash("Description")
          .setString("The next in order control point that will be executed");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
    result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataHash("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataHash("Realtime").hash("Hide").setBool(true);
    result.back().write().metadataHash("Realtime").hash("Name").setString("Next");
    result.back().write()
          .metadataHash("Realtime")
          .hash("HashFormat")
          .setString("${TIME|Time} ${FLAGS|Flavors|,|uc}");
    result.back().write().metadataHashChild("Flavors")
          .metadataFlags("Description")
          .setString("Flavors added by the control point");
    result.back().write().metadataHashChild("Description")
          .metadataString("Description")
          .setString("The description of the control point");
    result.back().write().metadataHashChild("Identifier")
          .metadataHash("Description")
          .setString("The control point identifier data");
    result.back().write().metadataHashChild("Identifier")
          .metadataHash("ArbitraryStructure")
          .setBool(true);

    return result;
}

SequenceMatch::Composite ControlCycle::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "ZLAST");
    sel.append({}, {}, "ZNEXT");
    sel.append({}, {}, "ZACTIVE");
    return sel;
}

void ControlCycle::incomingCommand(const Variant::Read &command)
{
    if (command.hash("EnableCycle").exists()) {
        enabled = true;
        updatePending = true;
        qCDebug(log) << "Cycle scan enable command received";
        generalStatusUpdated();
    }

    if (command.hash("DisableCycle").exists()) {
        enabled = false;
        updatePending = true;
        qCDebug(log) << "Cycle scan disable command received";
        generalStatusUpdated();
    }

    if (command.hash("RestartCycle").exists()) {
        cycleStartTime = FP::undefined();
        cycleEndTime = FP::undefined();
        updatePending = true;
        qCDebug(log) << "Cycle scan restart command received";
        generalStatusUpdated();
    }

    if (command.hash("Bypass").exists() && !isBypassed) {
        isBypassed = true;
        /* Don't need to trigger an update, since we don't actually do anything
         * on a bypass, we just stop doing things (potentially). */
    }

    if (command.hash("UnBypass").exists() && isBypassed) {
        isBypassed = false;
        updatePending = true;
    }

    if (!config.isEmpty()) {
        for (int i = 0, max = config.first().actions.size(); i < max; i++) {
            if (!command.hash(QString("Execute_%1").arg(i)).exists())
                continue;
            requestActionExecute.insert(i);
            updatePending = true;
            qCDebug(log) << "Manual execution of action" << (i + 1) << "requested";
        }
    }
}

Variant::Root ControlCycle::getCommands()
{
    Variant::Root result;

    result["EnableCycle"].hash("DisplayName").setString("Enable cycle execution");
    result["EnableCycle"].hash("ToolTip")
                         .setString(
                                 "Enable the execution of this cycle schedule.  When enabled the cycle will execute actions periodically.");
    result["EnableCycle"].hash("Include").array(0).hash("Type").setString("Flags");
    result["EnableCycle"].hash("Include").array(0).hash("Flags").setFlags({"Disabled"});
    result["EnableCycle"].hash("Include").array(0).hash("Variable").setString("F1");

    result["DisableCycle"].hash("DisplayName").setString("Disable cycle execution");
    result["DisableCycle"].hash("ToolTip")
                          .setString(
                                  "Disable the execution of this cycle schedule.  While disabled the schedule will not execute any actions.");
    result["DisableCycle"].hash("Exclude").array(0).hash("Type").setString("Flags");
    result["DisableCycle"].hash("Exclude").array(0).hash("Flags").setFlags({"Disabled"});
    result["DisableCycle"].hash("Exclude").array(0).hash("Variable").setString("F1");

    result["RestartCycle"].hash("DisplayName").setString("Restart cycle execution");
    result["RestartCycle"].hash("ToolTip")
                          .setString(
                                  "Restart execution of the cycle.  This will resend all cycle execution commands and reset the time interval.");
    result["RestartCycle"].hash("Exclude").array(0).hash("Type").setString("Flags");
    result["RestartCycle"].hash("Exclude").array(0).hash("Flags").setFlags({"Disabled"});
    result["RestartCycle"].hash("Exclude").array(0).hash("Variable").setString("F1");

    for (QList<Configuration>::const_iterator c = config.constBegin(), endC = config.constEnd();
            c != endC;
            ++c) {
        for (int i = 0, max = c->actions.size(); i < max; i++) {
            auto target = result[QString("Execute_%1").arg(i)];
            if (target.exists())
                continue;
            target.hash("SortPriority").setInt64(i + 1);
            c->actions
             .at(i)
             .second
             ->command(target, c->actions.at(i).first->describe(c->getStart()));
        }
    }

    return result;
}


void ControlCycle::setControlStream(AcquisitionControlStream *controlStream)
{ Q_UNUSED(controlStream); }

void ControlCycle::setState(AcquisitionState *state)
{ this->state = state; }

void ControlCycle::setRealtimeEgress(StreamSink *egress)
{
    realtimeEgress = egress;
    haveEmittedRealtimeMeta = false;
}

void ControlCycle::setLoggingEgress(StreamSink *egress)
{
    loggingEgress = egress;
}

void ControlCycle::setPersistentEgress(Data::StreamSink *egress)
{
    persistentEgress = egress;
}

Variant::Root ControlCycle::getSystemFlagsMetadata()
{
    Variant::Root result;
    for (const auto &cfg : config) {
        for (const auto &add : cfg.systemFlags) {
            result.write().metadataSingleFlag(add.first).set(add.second);
        }
    }
    return result;
}

Variant::Root ControlCycle::getSourceMetadata()
{ return instrumentMeta; }

AcquisitionInterface::GeneralStatus ControlCycle::getGeneralStatus()
{
    if (!enabled)
        return AcquisitionInterface::GeneralStatus::Disabled;
    return AcquisitionInterface::GeneralStatus::Normal;
}

void ControlCycle::incomingData(const Util::ByteView &data,
                                double time,
                                AcquisitionInterface::IncomingDataType type)
{
    Q_UNUSED(data);
    Q_UNUSED(type);
    advanceTime(time);
    if (state != NULL)
        state->realtimeAdvanced(time);
}

void ControlCycle::incomingControl(const Util::ByteView &data,
                                   double time,
                                   AcquisitionInterface::IncomingDataType type)
{
    Q_UNUSED(data);
    Q_UNUSED(type);
    advanceTime(time);
    if (state != NULL)
        state->realtimeAdvanced(time);
}

void ControlCycle::incomingTimeout(double time)
{
    advanceTime(time);
    if (state != NULL)
        state->realtimeAdvanced(time);
}

void ControlCycle::incomingAdvance(double time)
{
    advanceTime(time);
    if (state != NULL)
        state->realtimeAdvanced(time);
}

AcquisitionInterface::AutomaticDefaults ControlCycle::getDefaults()
{
    AutomaticDefaults result;
    result.name = "CYCLE$2";
    return result;
}

bool ControlCycle::isCompleted()
{ return terminated; }

void ControlCycle::signalTerminate()
{
    if (realtimeEgress != NULL) {
        realtimeEgress->endData();
        realtimeEgress = NULL;
    }
    if (loggingEgress != NULL) {
        loggingEgress->endData();
        loggingEgress = NULL;
    }
    if (persistentEgress != NULL) {
        persistentEgress->endData();
        persistentEgress = NULL;
    }
    terminated = true;
    completed();
}


std::unique_ptr<
        AcquisitionInterface> ControlCycleComponent::createAcquisitionPassive(const ComponentOptions &,
                                                                              const std::string &loggingContext)
{
    qWarning() << "Option based creation is not supported";
    return std::unique_ptr<AcquisitionInterface>(
            new ControlCycle(ValueSegment::Transfer(), loggingContext));
}

std::unique_ptr<
        AcquisitionInterface> ControlCycleComponent::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext)
{
    return std::unique_ptr<AcquisitionInterface>(new ControlCycle(config, loggingContext));
}

std::unique_ptr<
        AcquisitionInterface> ControlCycleComponent::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext)
{ return {}; }
