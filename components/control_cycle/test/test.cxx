/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "smoothing/baseline.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ControlTestState : public AcquisitionState {
public:
    Variant::Flags bypassFlags;
    Variant::Flags systemFlags;
    double flushTime;
    std::unordered_map<std::string, Variant::Read> commands;
    SequenceName::Flavors flavors;
    double wakeupTime;

    ControlTestState()
            : bypassFlags(),
              systemFlags(),
              flushTime(FP::undefined()),
              commands(),
              flavors(),
              wakeupTime(FP::undefined())
    { }

    virtual ~ControlTestState() = default;

    void setBypassFlag(const Variant::Flag &flag) override
    { bypassFlags.insert(flag); }

    void clearBypassFlag(const Variant::Flag &flag) override
    { bypassFlags.erase(flag); }

    void setSystemFlag(const Variant::Flag &flag) override
    { systemFlags.insert(flag); }

    void clearSystemFlag(const Variant::Flag &flag) override
    { systemFlags.erase(flag); }

    void requestFlush(double seconds) override
    { flushTime = seconds; }

    void sendCommand(const std::string &target, const Variant::Read &command) override
    { Util::insert_or_assign(commands, target, Variant::Root(command).read()); }

    void setSystemFlavors(const SequenceName::Flavors &flavors) override
    { this->flavors = flavors; }

    void setAveragingTime(Time::LogicalTimeUnit, int, bool setAligned) override
    { }

    void requestStateSave() override
    { }

    void requestGlobalStateSave() override
    { }

    void event(double, const QString &, bool, const Variant::Read &) override
    { }

    void realtimeAdvanced(double) override
    { }

    void requestDataWakeup(double time) override
    {
        wakeupTime = time;
    }

    void requestControlWakeup(double time) override
    {
        wakeupTime = time;
    }
};

class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(ComponentLoader::create("control_cycle"));
        QVERIFY(component);

        Logging::suppressForTesting();
    }

    void basic()
    {
        Variant::Root config;
        config["Period/Unit"].setString("Hour");
        config["Period/Count"].setInt64(1);
        config["Period/Align"].setBool(true);

        config["Actions/#0/Unit"].setString("Minute");
        config["Actions/#0/Count"].setInt64(0);
        config["Actions/#0/SetFlags"].setFlags({"A"});

        config["Actions/#1/Unit"].setString("Minute");
        config["Actions/#1/Count"].setInt64(30);
        config["Actions/#1/ClearFlags"].setFlags({"A"});

        config["IdleAction/SetFlags"].setFlags({"C"});

        ControlTestState state;
        std::unique_ptr<AcquisitionInterface> interface(component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)}));
        QVERIFY(interface.get());
        interface->setState(&state);

        interface->incomingAdvance(3600.0);
        QCOMPARE(state.systemFlags, (Variant::Flags{"A"}));
        QCOMPARE(state.wakeupTime, 5400.0);
        interface->incomingAdvance(3601.0);
        QCOMPARE(state.systemFlags, (Variant::Flags{"A"}));
        QCOMPARE(state.wakeupTime, 5400.0);
        interface->incomingAdvance(5399.0);
        QCOMPARE(state.systemFlags, (Variant::Flags{"A"}));
        QCOMPARE(state.wakeupTime, 5400.0);
        interface->incomingAdvance(5400.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.wakeupTime, 7200.0);
        interface->incomingAdvance(7199.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.wakeupTime, 7200.0);
        interface->incomingAdvance(7200.0);
        QCOMPARE(state.systemFlags, (Variant::Flags{"A"}));
        QCOMPARE(state.wakeupTime, 9000.0);
        interface->incomingAdvance(9001.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.wakeupTime, 10800.0);
        interface->incomingAdvance(10801.0);
        QCOMPARE(state.systemFlags, (Variant::Flags{"A"}));
        QCOMPARE(state.wakeupTime, 12600.0);

        Variant::Write command = Variant::Write::empty();
        command["DisableCycle"].setBool(true);
        state.wakeupTime = FP::undefined();
        interface->incomingCommand(command);
        interface->incomingAdvance(10802.0);
        QCOMPARE(state.systemFlags, (Variant::Flags{"A", "C"}));
        QVERIFY(!FP::defined(state.wakeupTime));
        interface->incomingAdvance(12601.0);
        QCOMPARE(state.systemFlags, (Variant::Flags{"A", "C"}));
        QVERIFY(!FP::defined(state.wakeupTime));

        command.setEmpty();
        command["EnableCycle"].setBool(true);
        interface->incomingCommand(command);
        interface->incomingAdvance(14399.0);
        QCOMPARE(state.systemFlags, (Variant::Flags{"C"}));
        QCOMPARE(state.wakeupTime, 14400.0);
        interface->incomingAdvance(14400.0);
        QCOMPARE(state.systemFlags, (Variant::Flags{"A", "C"}));
        QCOMPARE(state.wakeupTime, 16200.0);

        interface = component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)});
        QVERIFY(interface.get());
        state.systemFlags.clear();
        state.wakeupTime = FP::undefined();
        interface->setState(&state);

        interface->incomingAdvance(5400.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.wakeupTime, 7200.0);
        interface->incomingAdvance(7199.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.wakeupTime, 7200.0);
        interface->incomingAdvance(7200.0);
        QCOMPARE(state.systemFlags, (Variant::Flags{"A"}));
        QCOMPARE(state.wakeupTime, 9000.0);
        interface->incomingAdvance(9001.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.wakeupTime, 10800.0);
        interface->incomingAdvance(10801.0);
        QCOMPARE(state.systemFlags, (Variant::Flags{"A"}));
        QCOMPARE(state.wakeupTime, 12600.0);

        interface = component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)});
        QVERIFY(interface.get());
        state.systemFlags.clear();
        state.wakeupTime = FP::undefined();
        interface->setState(&state);

        auto check = interface->getCommands();
        QVERIFY(check["Execute_0"].hash("DisplayName").toQString().startsWith("Exec"));
        QVERIFY(check["Execute_1"].hash("DisplayName").toQString().startsWith("Exec"));

        interface->incomingAdvance(5400.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.wakeupTime, 7200.0);

        command.setEmpty();
        command["Execute_0"].setBool(true);
        interface->incomingCommand(command);

        interface->incomingAdvance(7199.0);
        QCOMPARE(state.systemFlags, (Variant::Flags{"A"}));
        QCOMPARE(state.wakeupTime, 7200.0);
        interface->incomingAdvance(7200.0);
        QCOMPARE(state.systemFlags, (Variant::Flags{"A"}));
        QCOMPARE(state.wakeupTime, 9000.0);
        interface->incomingAdvance(9001.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.wakeupTime, 10800.0);
        interface->incomingAdvance(10801.0);
        QCOMPARE(state.systemFlags, (Variant::Flags{"A"}));
        QCOMPARE(state.wakeupTime, 12600.0);

        command.setEmpty();
        command["Execute_1"].setBool(true);
        interface->incomingCommand(command);
        interface->incomingAdvance(10802.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.wakeupTime, 14400.0);
        interface->incomingAdvance(14399.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.wakeupTime, 14400.0);
        interface->incomingAdvance(14401.0);
        QCOMPARE(state.systemFlags, (Variant::Flags{"A"}));
    }

    void actions()
    {
        Variant::Root config;
        config["Period/Unit"].setString("Hour");
        config["Period/Count"].setInt64(1);
        config["Period/Align"].setBool(true);

        config["Actions/#0/Unit"].setString("Minute");
        config["Actions/#0/Count"].setInt64(0);
        config["Actions/#0/Contaminate"].setBool(true);
        config["Actions/#0/Bypass"].setBool(true);
        config["Actions/#0/FlushTime"].setDouble(2.0);
        config["Actions/#0/SetCut"].setString("pm1");
        Variant::Root cmd1;
        cmd1["Foobar"].setBool(true);
        config["Actions/#0/Commands"].set(cmd1);

        config["Actions/#1/Unit"].setString("Minute");
        config["Actions/#1/Count"].setInt64(30);
        config["Actions/#1/Contaminate"].setBool(false);
        config["Actions/#1/Bypass"].setBool(false);
        config["Actions/#1/FlushTime"].setDouble(3.0);
        config["Actions/#1/SetCut"].setString("pm10");
        Variant::Root cmd2;
        cmd2["Foobaz"].setBool(false);
        config["Actions/#1/Commands"].set(cmd2);

        ControlTestState state;
        std::unique_ptr<AcquisitionInterface> interface(component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)}));
        QVERIFY(interface.get());
        interface->setState(&state);

        interface->incomingAdvance(3600.0);
        QCOMPARE(state.wakeupTime, 5400.0);
        QCOMPARE(state.systemFlags, (Variant::Flags{"Contaminated"}));
        QCOMPARE(state.bypassFlags, (Variant::Flags{"Bypass"}));
        QCOMPARE(state.flushTime, 2.0);
        QCOMPARE(state.flavors, (SequenceName::Flavors{"pm1"}));
        std::unordered_map<std::string, Variant::Read> check;
        Util::insert_or_assign(check, std::string(), cmd1.read());
        QCOMPARE(state.commands, check);

        interface->incomingAdvance(5400.0);
        QCOMPARE(state.wakeupTime, 7200.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.bypassFlags, Variant::Flags());
        QCOMPARE(state.flushTime, 3.0);
        QCOMPARE(state.flavors, (SequenceName::Flavors{"pm10"}));
        Util::insert_or_assign(check, std::string(), cmd2.read());
        QCOMPARE(state.commands, check);
    }

    void minimumTime()
    {
        Variant::Root config;
        config["Period/Unit"].setString("Hour");
        config["Period/Count"].setInt64(1);
        config["Period/Align"].setBool(true);
        config["MinimumTime/Unit"].setString("Minute");
        config["MinimumTime/Count"].setInt64(45);
        config["InitializeInactive"].setBool(true);

        config["Actions/#0/Unit"].setString("Minute");
        config["Actions/#0/Count"].setInt64(0);
        config["Actions/#0/Contaminate"].setBool(true);
        config["Actions/#0/SetCut"].setString("pm1");

        ControlTestState state;
        std::unique_ptr<AcquisitionInterface> interface(component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)}));
        QVERIFY(interface.get());
        interface->setState(&state);

        interface->incomingAdvance(6300.0);
        QCOMPARE(state.wakeupTime, 10800.0);
        QCOMPARE(state.flavors, SequenceName::Flavors());

        interface->incomingAdvance(10800.0);
        QCOMPARE(state.wakeupTime, 14400.0);
        QCOMPARE(state.flavors, (SequenceName::Flavors{"pm1"}));
    }

    void spinup()
    {
        Variant::Root config;
        config["Period/Unit"].setString("Hour");
        config["Period/Count"].setInt64(1);
        config["Period/Align"].setBool(true);
        config["Spinup/Duration/Unit"].setString("Minute");
        config["Spinup/Duration/Count"].setInt64(5);
        config["Spinup/Duration/Align"].setBool(true);
        config["Spinup/Active/SetCut"].setString("pm25");
        config["Spinup/End/Contaminate"].setBool(true);

        config["Actions/#0/Unit"].setString("Minute");
        config["Actions/#0/Count"].setInt64(0);
        config["Actions/#0/SetCut"].setString("pm1");

        config["Actions/#1/Unit"].setString("Minute");
        config["Actions/#1/Count"].setInt64(30);
        config["Actions/#1/SetCut"].setString("pm10");

        ControlTestState state;
        std::unique_ptr<AcquisitionInterface> interface(component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)}));
        QVERIFY(interface.get());
        interface->setState(&state);

        interface->incomingAdvance(3600.0);
        QCOMPARE(state.wakeupTime, 3900.0);
        QCOMPARE(state.systemFlags, Variant::Flags());
        QCOMPARE(state.flavors, (SequenceName::Flavors{"pm25"}));

        interface->incomingAdvance(3900.0);
        QCOMPARE(state.wakeupTime, 5400.0);
        QCOMPARE(state.systemFlags, (Variant::Flags{"Contaminated"}));
        QCOMPARE(state.flavors, (SequenceName::Flavors{"pm1"}));

        interface->incomingAdvance(5400.0);
        QCOMPARE(state.wakeupTime, 7200.0);
        QCOMPARE(state.systemFlags, (Variant::Flags{"Contaminated"}));
        QCOMPARE(state.flavors, (SequenceName::Flavors{"pm10"}));

        interface->incomingAdvance(7200.0);
        QCOMPARE(state.wakeupTime, 9000.0);
        QCOMPARE(state.systemFlags, (Variant::Flags{"Contaminated"}));
        QCOMPARE(state.flavors, (SequenceName::Flavors{"pm1"}));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
