/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */
#ifndef CONTROLLER_H
#define CONTROLLER_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QLoggingCategory>

#include "core/util.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/busmanager.hxx"
#include "datacore/stream.hxx"

class AcquireLovePID;

class LovePIDController : public CPD3::Acquisition::BusManager::Device {
    AcquireLovePID *pid;
    quint16 address;
    std::string loggingName;
    QLoggingCategory log;

    enum CommandType {
        /* Unknown command, so all responses until another command is
         * received are discarded. */
                COMMAND_IGNORED,

        /* 00 - Controller status */
                COMMAND_READ_STATUS,

        /* 05 - Error Status */
                COMMAND_READ_ERRORSTATUS,

        /* 0101 - Read setpoint 1 */
                COMMAND_READ_SP1,

        /* 0200 - Write setpoint 1 */
                COMMAND_WRITE_SP1,

        /* 0153 - Read manual setpoint 1 */
                COMMAND_READ_MANUAL_SP1,

        /* 0266 - Write manual setpoint 1 */
                COMMAND_WRITE_MANAUL_SP1,

        /* 0156 - Read output percentage */
                COMMAND_READ_OUTPUT_PERCENTAGE,

        /* 031A - Read decimal point setting */
                COMMAND_READ_DECIMAL_POINT,

        /* 025C - Write decimal point setting */
                COMMAND_WRITE_DECIMAL_POINT,

        /* 032C - Read EEPROM write status */
                COMMAND_READ_EEPROM_WRITE,

        /* 0441 - Disable EEPROM writes */
                COMMAND_DISABLE_EEPROM_WRITE,

        /* 0400 - Set to remote mode */
                COMMAND_REMOTE_MODE,

        /* 0408 - Disable manual mode */
                COMMAND_DISABLE_MANUAL,

        /* 0409 - Enable manual mode */
                COMMAND_ENABLE_MANUAL,

        /* 0700 - Controller hardware production date */
                COMMAND_READ_CONTROLLER_DATA,

        /* 0702 - Firmware date */
                COMMAND_READ_FIRMWARE_DATA,

        /* A specifically invalid command */
                COMMAND_INVALID,
    };

    class Command {
        CommandType type;
        CPD3::Util::ByteArray packet;
    public:
        Command();

        explicit Command(CommandType t);

        Command(CommandType t, CPD3::Util::ByteArray p);

        inline Command(CommandType t, const CPD3::Util::ByteView &p) : Command(t,
                                                                               CPD3::Util::ByteArray(
                                                                                       p))
        { }

        inline Command(CommandType t, const char *p) : Command(t, CPD3::Util::ByteArray(p))
        { }

        Command(const Command &other);

        Command &operator=(const Command &other);

        inline CommandType getType() const
        { return type; }

        inline const CPD3::Util::ByteArray &getPacket() const
        { return packet; }

        CPD3::Data::Variant::Root stateDescription() const;
    };

    QList<Command> commandQueue;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum {
        /* Waiting a response to a command in passive mode */
                RESP_PASSIVE_RUN,

        /* Waiting for enough data to confirm communications */
                RESP_PASSIVE_WAIT,

        /* Starting comms, with no command issued */
                RESP_INTERACTIVE_START,
        /* Starting comms, waiting for the initial status 
         * command response */
                RESP_INTERACTIVE_START_STATUS,
        /* Starting comms, waiting for the remote mode response */
                RESP_INTERACTIVE_START_REMOTEMODE,
        /* Starting comms, waiting for the manual mode set/clear response */
                RESP_INTERACTIVE_START_SETMANUAL,
        /* Starting comms, waiting for the response to the EEPROM
         * write status */
                RESP_INTERACTIVE_START_READ_EEPROM,
        /* Starting comms, waiting for the response to the EEPROM
         * write disable command */
                RESP_INTERACTIVE_START_CLEAR_EEPROM,
        /* Starting comms, reading controller data */
                RESP_INTERACTIVE_START_READ_CONTROLLERDATA,
        /* Starting comms, reading firmware data */
                RESP_INTERACTIVE_START_READ_FIRMWAREDATA,
        /* Starting comms, reading the number of decimal digits */
                RESP_INTERACTIVE_START_READ_DECIMALS,
        /* Starting comms, writing the number of decimal digits */
                RESP_INTERACTIVE_START_WRITE_DECIMALS,

        /* In interactive mode, reading status */
                RESP_INTERACTIVE_RUN_STATUS,
        /* In interactive mode, reading the error register */
                RESP_INTERACTIVE_RUN_READ_ERROR,
        /* In interactive mode, reading the current setpoint */
                RESP_INTERACTIVE_RUN_READ_SP,
        /* In interactive mode, updating the current setpoint */
                RESP_INTERACTIVE_RUN_WRITE_SP,
        /* In interactive mode, reading the output percentage */
                RESP_INTERACTIVE_RUN_READ_OUTPUT_PERCENTAGE,
        /* Finished this interrogation run, and waiting for the next one */
                RESP_INTERACTIVE_RUN_WAIT,

        /* Waiting before retrying interactive initialize */
                RESP_INTERACTIVE_RESTART_WAIT,
    } responseState;
    int commandFailedCount;
    double responseTime;

    enum {
        Exception_FailTest = 0x8000,
        Exception_CheckCal = 0x2000,
        Exception_Overflow = 0x1000,
        Exception_Underflow = 0x0800,
        Exception_BadInput = 0x0400,
        Exception_OpenInput = 0x0200,
        Exception_Area = 0x0100,
        Exception_LoopBreak = 0x0080,
        Exception_SensorRate = 0x0040,
        Exception_ErrorMask = 0xFFFF,

        Exception_Alarm1 = 0x10000,
        Exception_Alarm2 = 0x20000,
        Exception_ActivityTimeout = 0x40000,
    };
    quint32 exceptions;

    enum {
        Failure_Timeout, Failure_ErrorPacket, Failure_ParseError,
    } failureReason;
    CPD3::Data::Variant::Root failureData;

    CPD3::Data::Variant::Root metadata;
    double targetSetpoint;
    int decimalDigits;
    bool manualMode;
    bool interfaceResetPending;

    bool atTargetSetpoint(double sp) const;

    void responseError(const CPD3::Util::ByteView &payload,
                       double frameTime,
                       int code,
                       const Command &command);

    void outOfSequenceResponse(const CPD3::Util::ByteView &payload,
                               double frameTime,
                               const Command &command);

    void commandRetryExceeded(double currentTime);

    void issueCommand(const Command &command, double currentTime);

public:
    LovePIDController(CPD3::Acquisition::BusManager::DeviceInterface *interface,
                      AcquireLovePID *parent,
                      quint16 addr);

    virtual ~LovePIDController();

    inline CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus() const
    { return autoprobeStatus; };

    inline bool getInterfaceResetPending() const
    { return interfaceResetPending; }

    inline void interfaceResetIssued()
    { interfaceResetPending = false; }

    inline const CPD3::Data::Variant::Root &getMetadata() const
    { return metadata; }

    QString describeResponseState() const;

    QString realtimeState() const;

    void incomingDataFrame(const CPD3::Util::ByteView &payload, double frameTime);

    void incomingControlFrame(const CPD3::Util::ByteView &payload, double frameTime);

    void incomingCorruptDataFrame(const CPD3::Util::ByteView &payload, int code, double frameTime);

    void incomingErrorFrame(int errorCode, double frameTime);

    virtual void incomingTimeout(double time);

    virtual void busControlAcquired(double time);

    virtual void lockAcquired(double time);

    virtual void discardDataCompleted(double time);

    inline quint16 getAddress() const
    { return address; }

    inline void setTargetSetpoint(double sp)
    { targetSetpoint = sp; }

    inline double getTargetSetpoint() const
    { return targetSetpoint; }

    void changeDecimalDigits(int digits);

    void changeManualMode(bool mode);

    inline int getDecimalDigits() const
    { return decimalDigits; }

    void autoprobeTryInteractive(double time);

    void autoprobeResetPassive(double time);

    void autoprobePromote(double time);

    void autoprobePromotePassive(double time);

    void setToAutoprobe();

    void setToInteractive();

    void applyExitSetpoint(double sp);

    bool hasCommunications() const;
};

#endif
