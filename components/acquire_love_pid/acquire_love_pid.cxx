/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <sstream>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/composite.hxx"
#include "acquisition/acquisitioncomponent.hxx"

#include "acquire_love_pid.hxx"
#include "controller.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

AcquireLovePID::Configuration::Configuration() : start(FP::undefined()),
                                                 end(FP::undefined()),
                                                 controllers(),
                                                 variables(),
                                                 setpointNames(),
                                                 retryTimes(3),
                                                 provisionalTimeout(120.0),
                                                 pollInterval(1.0)
{ }

AcquireLovePID::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          controllers(other.controllers),
          variables(other.variables),
          setpointNames(other.setpointNames),
          retryTimes(other.retryTimes),
          provisionalTimeout(other.provisionalTimeout),
          pollInterval(other.pollInterval)
{ }

void AcquireLovePID::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("Love");
    instrumentMeta["Model"].setString("PID");

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
}

AcquireLovePID::Configuration::Configuration(const ValueSegment &other, double s, double e) : start(
        s),
                                                                                              end(e),
                                                                                              controllers(),
                                                                                              variables(),
                                                                                              setpointNames(),
                                                                                              retryTimes(
                                                                                                      3),
                                                                                              provisionalTimeout(
                                                                                                      120.0),
                                                                                              pollInterval(
                                                                                                      1.0)
{
    setFromSegment(other);
}

AcquireLovePID::Configuration::Configuration(const Configuration &under,
                                             const ValueSegment &over,
                                             double s,
                                             double e) : start(s),
                                                         end(e),
                                                         controllers(under.controllers),
                                                         variables(under.variables),
                                                         setpointNames(under.setpointNames),
                                                         retryTimes(under.retryTimes),
                                                         provisionalTimeout(
                                                                 under.provisionalTimeout),
                                                         pollInterval(under.pollInterval)
{
    setFromSegment(over);
}

static bool isValidAddress(qint64 address)
{
    if (!INTEGER::defined(address))
        return false;
    if (address <= 0 || address > 0x3FF)
        return false;
    if (address == 0x100 || address == 0x200 || address == 0x300)
        return false;
    return true;
}

AcquireLovePID::ActiveController::ActiveController() : manualMode(false),
                                                       manualModeSet(false),
                                                       initialSetpoint(FP::undefined()),
                                                       exitSetpoint(FP::undefined()),
                                                       bypassSetpoint(FP::undefined()),
                                                       unbypassSetpoint(FP::undefined()),
                                                       decimalDigits(INTEGER::undefined())
{ }

void AcquireLovePID::Configuration::addController(qint64 address, const Variant::Read &config)
{
    if (!INTEGER::defined(address))
        address = config.toInt64();
    if (!isValidAddress(address))
        return;

    ActiveController controller;
    controller.manualMode = config["ManualMode"].toBool();
    controller.manualModeSet = config["ManualMode"].exists();
    controller.initialSetpoint = config["Setpoint"].toDouble();
    controller.exitSetpoint = config["Shutdown"].toDouble();
    controller.bypassSetpoint = config["Bypass"].toDouble();
    controller.unbypassSetpoint = config["UnBypass"].toDouble();
    controller.decimalDigits = config["DecimalDigits"].toInt64();
    if (INTEGER::defined(controller.decimalDigits) &&
            (controller.decimalDigits < 0 || controller.decimalDigits > 3))
        controller.decimalDigits = INTEGER::undefined();

    controllers.insert((quint16) address, controller);
}

void AcquireLovePID::Configuration::addVariable(const Variant::Read &config,
                                                qint64 defaultAddress,
                                                const SequenceName::Component &defaultName)
{
    qint64 address = config["Address"].toInt64();
    if (!INTEGER::defined(address))
        address = defaultAddress;
    if (!isValidAddress(address))
        return;

    auto name = config["Name"].toString();
    if (name.empty())
        name = defaultName;
    if (name.empty())
        return;

    OutputVariable variable;
    variable.address = (quint16) address;
    variable.name = std::move(name);
    variable.metadata.write().set(config["Metadata"]);
    variable.calibration = Variant::Composite::toCalibration(config["Calibration"]);

    variables.append(variable);
}

void AcquireLovePID::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["Controllers"].exists()) {
        controllers.clear();
        switch (config["Controllers"].getType()) {
        case Variant::Type::String: {
            QStringList addresses(config["Controllers"].toQString()
                                                       .split(QRegExp("[\\s+;:,]"),
                                                              QString::SkipEmptyParts));
            for (QStringList::const_iterator addrRaw = addresses.constBegin(),
                    end = addresses.constEnd(); addrRaw != end; ++addrRaw) {
                bool ok = false;
                qint64 addr = addrRaw->toLongLong(&ok, 16);
                if (!ok)
                    continue;
                addController(addr, Variant::Root());
            }
            break;
        }
        default: {
            auto children = config["Controllers"].toChildren();
            for (auto add = children.begin(), endAdd = children.end(); add != endAdd; ++add) {
                qint64 addr = add.value().hash("Address").toInt64();
                if (!INTEGER::defined(addr)) {
                    bool ok = false;
                    addr = QString::fromStdString(add.stringKey()).toLongLong(&ok, 16);
                    if (!ok)
                        continue;
                }
                addController(addr, add.value());
            }
            break;
        }
        }
    }

    if (config["Variables"].exists()) {
        variables.clear();
        switch (config["Variables"].getType()) {
        case Variant::Type::Array: {
            auto children = config["Variables"].toArray();
            for (std::size_t idx = 0, max = children.size(); idx < max; ++idx) {
                if (static_cast<int>(idx) < controllers.size()) {
                    addVariable(children[idx], (controllers.begin() + idx).key());
                } else {
                    addVariable(children[idx]);
                }
            }
            break;
        }
        case Variant::Type::String: {
            QStringList names(config["Variables"].toQString()
                                                 .split(QRegExp("[\\s+;:,]"),
                                                        QString::SkipEmptyParts));
            for (int idx = 0, max = names.size(); idx < max; ++idx) {
                if (idx < controllers.size()) {
                    addVariable(Variant::Read::empty(), (controllers.begin() + idx).key(),
                                names.at(idx).toStdString());
                } else {
                    addVariable(Variant::Read::empty(), INTEGER::undefined(),
                                names.at(idx).toStdString());
                }
            }
            break;
        }
        default: {
            auto children = config["Variables"].toChildren();
            for (auto add = children.begin(), endAdd = children.end(); add != endAdd; ++add) {
                qint64 addr = add.value().hash("Address").toInt64();
                if (!INTEGER::defined(addr)) {
                    bool ok = false;
                    addr = QString::fromStdString(add.stringKey()).toLongLong(&ok, 16);
                    if (!ok)
                        addr = INTEGER::undefined();
                }
                addVariable(add.value(), addr, add.stringKey());
            }
            break;
        }
        }
    }

    auto check = config["SetpointOutputs"];
    if (!check.exists())
        check = config["SetpointNames"];
    if (check.exists()) {
        setpointNames.clear();
        for (auto add : check.toHash()) {
            if (add.first.empty())
                continue;
            qint64 address = INTEGER::undefined();
            switch (add.second.getType()) {
            case Variant::Type::String: {
                bool ok = false;
                address = add.second.toQString().toLongLong(&ok, 16);
                if (!ok)
                    address = INTEGER::undefined();
                break;
            }
            default:
                address = add.second.toInt64();
                break;
            }
            if (!isValidAddress(address))
                continue;

            setpointNames.insert(QString::fromStdString(add.first), (quint16) address);
        }
    }

    if (INTEGER::defined(config["RetryTimes"].toInt64())) {
        retryTimes = (int) config["RetryTimes"].toInt64();
        if (retryTimes < 0)
            retryTimes = 0;
    }

    if (config["ProvisionalTimeout"].exists())
        provisionalTimeout = config["ProvisionalTimeout"].toDouble();

    if (FP::defined(config["PollInterval"].toDouble()))
        pollInterval = config["PollInterval"].toDouble();
}

AcquireLovePID::~AcquireLovePID()
{
    backend.clear();
}

AcquireLovePID::AcquireLovePID(const ValueSegment::Transfer &configData,
                               const std::string &loggingContext) : FramedInstrument("lovepid",
                                                                                     loggingContext),
                                                                    autoprobeStatus(
                                                                            AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                    globalState(
                                                                            GLOBAL_PASSIVE_INITIALIZE),
                                                                    backend(this),
                                                                    bus(&backend),
                                                                    loggingMux(LogStream_ALLOCATE),
                                                                    availableStreams(),
                                                                    lastStateTime(FP::undefined())
{
    setDefaultInvalid();
    config.append(Configuration());

    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }

    configurationChanged();
}

void AcquireLovePID::setToAutoprobe()
{
    globalState = GLOBAL_AUTOPROBE_PASSIVE_INITIALIZE;

    for (QMap<quint16, LovePIDController *>::const_iterator controller = backend.begin(),
            endController = backend.end(); controller != endController; ++controller) {
        controller.value()->setToAutoprobe();
    }
}

void AcquireLovePID::setToInteractive()
{
    globalState = GLOBAL_INTERACTIVE_INITIALIZE;

    for (QMap<quint16, LovePIDController *>::const_iterator controller = backend.begin(),
            endController = backend.end(); controller != endController; ++controller) {
        controller.value()->setToInteractive();
    }
}


ComponentOptions AcquireLovePIDComponent::getBaseOptions()
{
    ComponentOptions options;

    return options;
}

AcquireLovePID::AcquireLovePID(const ComponentOptions &options, const std::string &loggingContext)
        : FramedInstrument("lovepid", loggingContext),
          autoprobeStatus(AcquisitionInterface::AutoprobeStatus::InProgress),
          globalState(GLOBAL_PASSIVE_INITIALIZE),
          backend(this),
          bus(&backend),
          loggingMux(LogStream_ALLOCATE),
          availableStreams(),
          lastStateTime(FP::undefined())
{
    Q_UNUSED(options);

    setDefaultInvalid();
    config.append(Configuration());

    configurationChanged();
}

SequenceValue::Transfer AcquireLovePID::buildLogMeta(double time)
{
    Q_ASSERT(!config.isEmpty());

    SequenceValue::Transfer result;

    for (QVector<OutputVariable>::const_iterator variable = config.first().variables.constBegin(),
            end = config.first().variables.constEnd(); variable != end; ++variable) {
        LovePIDController *controller = backend.lookup(variable->address);

        Variant::Root processing = controller->getMetadata();
        processing["By"].setString("acquire_love_pid");
        processing["At"].setDouble(Time::time());
        processing["Environment"].setString(Environment::describe());
        processing["Revision"].setString(Environment::revision());

        result.emplace_back(SequenceName({}, "raw_meta", variable->name),
                            Variant::Root(variable->metadata), time, FP::undefined());
        if (result.back().write().metadataReal("Format").toString().empty()) {
            int decimals = controller->getDecimalDigits();
            if (decimals > 4)
                decimals = 4;
            result.back()
                  .write()
                  .metadataReal("Format")
                  .setString(NumberFormat(5 - decimals, decimals).getDigitFilled());
        }
        result.back().write().metadataReal("Source").set(controller->getMetadata());
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        Variant::Composite::fromCalibration(result.back().write().metadataReal("Calibration"),
                                            variable->calibration);
        result.back().write().metadataReal("Address").setInt64(variable->address);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
        result.back().write().metadataReal("Realtime").hash("GroupName").setString(variable->name);
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(0);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Value"));
    }


    Variant::Root processing;
    processing["By"].setString("acquire_love_pid");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);


    result.emplace_back(SequenceName({}, "raw_meta", "ZINPUTS"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataArray("Count").setInt64(backend.size());
    result.back()
          .write()
          .metadataArray("Description")
          .setString("Raw input process values from all active controllers");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);

    int maximumDecimals = 0;
    for (QMap<quint16, LovePIDController *>::const_iterator add = backend.begin(),
            endAdd = backend.end(); add != endAdd; ++add) {
        LovePIDController *controller = add.value();

        maximumDecimals = qMax(maximumDecimals, controller->getDecimalDigits());

        result.back()
              .write()
              .metadataArray("Controllers")
              .toArray()
              .after_back()
              .set(controller->getMetadata());
    }

    if (maximumDecimals > 4)
        maximumDecimals = 4;

    result.back()
          .write()
          .metadataArray("Children")
          .metadataReal("Format")
          .setString(NumberFormat(5 - maximumDecimals, maximumDecimals).getDigitFilled());

    return result;
}

static SequenceName::Component toControllerNumber(std::size_t number)
{
    std::stringstream stream;
    stream << std::hex << number;
    return stream.str();
}

SequenceValue::Transfer AcquireLovePID::buildRealtimeMeta(double time)
{
    SequenceValue::Transfer result;

    Variant::Write controllerStateTranslation = Variant::Write::empty();
    controllerStateTranslation.hash("Run").setString(QObject::tr("", "run state string"));
    controllerStateTranslation.hash("Timeout").setString(QObject::tr("NO COMMS: Timeout"));
    controllerStateTranslation.hash("InteractiveStart").setString(QObject::tr("NO COMMS"));
    controllerStateTranslation.hash("InteractiveRunRestartWait").setString(QObject::tr("NO COMMS"));
    controllerStateTranslation.hash("PassiveWait").setString(QObject::tr("NO COMMS"));
    controllerStateTranslation.hash("InteractiveStartStatus")
                              .setString(QObject::tr("STARTING COMMS: Reading status"));
    controllerStateTranslation.hash("InteractiveStartSetRemote")
                              .setString(QObject::tr("STARTING COMMS: Setting remote mode"));
    controllerStateTranslation.hash("InteractiveStartSetManual")
                              .setString(QObject::tr("STARTING COMMS: Setting manual mode"));
    controllerStateTranslation.hash("InteractiveStartReadEEPROM")
                              .setString(QObject::tr("STARTING COMMS: Reading EEPROM status"));
    controllerStateTranslation.hash("InteractiveStartClearEEPROM")
                              .setString(QObject::tr("STARTING COMMS: Disabling EEPROM writes"));
    controllerStateTranslation.hash("InteractiveStartReadControllerData")
                              .setString(QObject::tr("STARTING COMMS: Reading controller data"));
    controllerStateTranslation.hash("InteractiveStartReadFirmwareData")
                              .setString(QObject::tr("STARTING COMMS: Reading firmware data"));
    controllerStateTranslation.hash("InteractiveStartReadDecimalDigits")
                              .setString(QObject::tr("STARTING COMMS: Reading decimal count"));
    controllerStateTranslation.hash("InteractiveStartWriteDecimalDigits")
                              .setString(QObject::tr("STARTING COMMS: Setting decimal count"));

    for (QVector<OutputVariable>::const_iterator variable = config.first().variables.constBegin(),
            end = config.first().variables.constEnd(); variable != end; ++variable) {
        LovePIDController *controller = backend.lookup(variable->address);

        Variant::Root processing = controller->getMetadata();
        processing["By"].setString("acquire_love_pid");
        processing["At"].setDouble(Time::time());
        processing["Environment"].setString(Environment::describe());
        processing["Revision"].setString(Environment::revision());

        Variant::Root baseRoot = variable->metadata;
        auto baseMeta = baseRoot.write();
        if (baseMeta.metadataReal("Format").toString().empty()) {
            int decimals = controller->getDecimalDigits();
            if (decimals > 4)
                decimals = 4;
            baseMeta.metadataReal("Format")
                    .setString(NumberFormat(5 - decimals, decimals).getDigitFilled());
        }
        baseMeta.metadataReal("Source").set(controller->getMetadata());
        baseMeta.metadataReal("Processing").toArray().after_back().set(processing);
        baseMeta.metadataReal("Address").setInt64(variable->address);
        baseMeta.metadataReal("Realtime").hash("Page").setInt64(0);
        baseMeta.metadataReal("Realtime").hash("GroupName").setString(variable->name);

        result.emplace_back(SequenceName({}, "raw_meta", "ZIN" + variable->name), baseRoot, time,
                            FP::undefined());
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Raw input value from the controller");
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(1);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Input"));

        result.emplace_back(SequenceName({}, "raw_meta", "ZSP" + variable->name), baseRoot, time,
                            FP::undefined());
        result.back().write().metadataReal("Description").setString("Current controller setpoint");
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(2);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Setpoint"));

        result.emplace_back(SequenceName({}, "raw_meta", "ZPCT" + variable->name), baseRoot, time,
                            FP::undefined());
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Current controller output percentage");
        result.back().write().metadataReal("Format").setString("000");
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(3);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Output %"));

        result.emplace_back(SequenceName({}, "raw_meta", "ZST" + variable->name), Variant::Root(),
                            time, FP::undefined());
        result.back().write().metadataString("Description").setString("Current controller state");
        result.back().write().metadataString("Source").set(controller->getMetadata());
        result.back().write().metadataString("Processing").toArray().after_back().set(processing);
        result.back().write().metadataString("Address").setInt64(controller->getAddress());
        result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
        result.back().write().metadataString("Realtime").hash("Page").setInt64(0);
        result.back()
              .write()
              .metadataString("Realtime")
              .hash("GroupName")
              .setString(variable->name);
        result.back().write().metadataString("Realtime").hash("ColumnOrder").setInt64(4);
        result.back()
              .write()
              .metadataString("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Status"));
        result.back()
              .write()
              .metadataString("Realtime")
              .hash("Translation")
              .set(controllerStateTranslation);
    }

    for (QMap<quint16, LovePIDController *>::const_iterator add = backend.begin(),
            endAdd = backend.end(); add != endAdd; ++add) {
        LovePIDController *controller = add.value();

        Variant::Root processing = controller->getMetadata();
        processing["By"].setString("acquire_love_pid");
        processing["At"].setDouble(Time::time());
        processing["Environment"].setString(Environment::describe());
        processing["Revision"].setString(Environment::revision());

        auto idx = toControllerNumber(add.key());
        QString group(QString::number(add.key(), 16).toUpper().rightJustified(3, '0'));

        Variant::Root baseRoot;
        auto baseMeta = baseRoot.write();
        {
            int decimals = controller->getDecimalDigits();
            if (decimals > 4)
                decimals = 4;
            baseMeta.metadataReal("Format")
                    .setString(NumberFormat(5 - decimals, decimals).getDigitFilled());
        }
        baseMeta.metadataReal("Source").set(controller->getMetadata());
        baseMeta.metadataReal("Processing").toArray().after_back().set(processing);
        baseMeta.metadataReal("Address").setInt64(controller->getAddress());
        baseMeta.metadataReal("Realtime").hash("Page").setInt64(1);
        baseMeta.metadataReal("Realtime").hash("GroupName").setString(group);

        result.emplace_back(SequenceName({}, "raw_meta", "ZIN" + idx), baseRoot, time,
                            FP::undefined());
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Raw input value from the controller");
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(0);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Input"));
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);

        result.emplace_back(SequenceName({}, "raw_meta", "ZSP" + idx), baseRoot, time,
                            FP::undefined());
        result.back().write().metadataReal("Description").setString("Current controller setpoint");
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(1);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Setpoint"));
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);

        result.emplace_back(SequenceName({}, "raw_meta", "ZPCT" + idx), baseRoot, time,
                            FP::undefined());
        result.back().write().metadataReal("Description").setString("Current controller output");
        result.back().write().metadataReal("Format").setString("000");
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(2);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Output %"));
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInt64(2);

        result.emplace_back(SequenceName({}, "raw_meta", "ZST" + idx), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataString("Description").setString("Current controller state");
        result.back().write().metadataString("Source").set(controller->getMetadata());
        result.back().write().metadataString("Processing").toArray().after_back().set(processing);
        result.back().write().metadataString("Address").setInt64(controller->getAddress());
        result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
        result.back().write().metadataString("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataString("Realtime").hash("GroupName").setString(group);
        result.back()
              .write()
              .metadataString("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Status"));
        result.back().write().metadataString("Realtime").hash("ColumnOrder").setInt64(3);
        result.back()
              .write()
              .metadataString("Realtime")
              .hash("Translation")
              .set(controllerStateTranslation);
        result.back().write().metadataString("Realtime").hash("NetworkPriority").setInt64(2);
    }

    return result;
}

SequenceMatch::Composite AcquireLovePID::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "ZST.*");
    return sel;
}

std::size_t AcquireLovePID::dataFrameStart(std::size_t offset, const Util::ByteArray &input) const
{
    auto max = input.size();
    while (offset < max) {
        auto ch = input[offset];
        if (ch == 0x02) {
            if (offset + 1 >= max)
                return input.npos;
            return offset + 1;
        }
        offset++;
    }
    return input.npos;
}

std::size_t AcquireLovePID::dataFrameEnd(std::size_t start, const Util::ByteArray &input) const
{
    for (auto max = input.size(); start < max; start++) {
        auto ch = input[start];
        if (ch == 0x06)
            return start;
    }
    return input.npos;
}

std::size_t AcquireLovePID::controlFrameStart(std::size_t offset,
                                              const Util::ByteArray &input) const
{
    auto max = input.size();
    while (offset < max) {
        auto ch = input[offset];
        if (ch == 0x02) {
            if (offset + 1 >= max)
                return -1;
            return offset + 1;
        }
        offset++;
    }
    return input.npos;
}

std::size_t AcquireLovePID::controlFrameEnd(std::size_t start, const Util::ByteArray &input) const
{
    for (auto max = input.size(); start < max; start++) {
        auto ch = input[start];
        if (ch == 0x03)
            return start;
    }
    return input.npos;
}

void AcquireLovePID::limitDataBuffer(Util::ByteArray &buffer)
{
    static constexpr std::size_t maxmimumSize = 256;
    if (buffer.size() <= maxmimumSize)
        return;
    buffer.pop_front(buffer.size() - maxmimumSize);
}

static char toHexDigit(quint8 b)
{
    if (b < 10)
        return b + '0';
    if (b < 16)
        return (b - 0xA) + 'A';
    return 'X';
}

static void addHexNumber(Util::ByteArray &output, quint8 n)
{
    output.push_back(toHexDigit((n >> 4) & 0xF));
    output.push_back(toHexDigit(n & 0xF));
}

static bool parseHexDigit(char c, quint8 &output)
{
    if (c >= '0' && c <= '9')
        output = (quint8) (c - '0');
    else if (c >= 'A' && c <= 'F')
        output = (quint8) (c - 'A') + 0xA;
    else if (c >= 'a' && c <= 'f')
        output = (quint8) (c - 'a') + 0xA;
    else
        return false;
    return true;
}

static bool parseHexNumber(const char *begin, quint8 &output)
{
    if (!parseHexDigit(*begin, output))
        return false;
    quint8 add;
    ++begin;
    if (!parseHexDigit(*begin, add))
        return false;
    output <<= 4;
    output |= add;
    return true;
}

static bool parseDecimalNumber(const char *begin, int digits, int &output)
{
    output = 0;
    for (const char *end = begin + digits; begin != end; ++begin) {
        char c = *begin;
        if (c >= '0' && c <= '9') {
            output *= 10;
            output += c - '0';
        } else {
            return false;
        }
    }
    return true;
}

static quint8 calculateChecksum(const char *begin, int n)
{
    quint32 sum = 0;
    for (const quint8 *add = (const quint8 *) begin, *end = (const quint8 *) begin + n;
            add != end;
            ++add) {
        sum += *add;
    }
    return sum & 0xFF;
}

Util::ByteArray AcquireLovePID::prepareCommandData(quint16 address, const Util::ByteView &payload)
{
    Util::ByteArray result;

    result.push_back(0x02);
    if (address > 0x01 && address <= 0xFF) {
        result.push_back('L');
    } else if (address > 0x101 && address <= 0x1FF) {
        result.push_back('O');
    } else if (address > 0x201 && address <= 0x2FF) {
        result.push_back('V');
    } else if (address > 0x301 && address <= 0x3FF) {
        result.push_back('E');
    } else {
        qCDebug(log) << "Address" << hex << address << "is out of range";
        result.push_back(0);
    }
    addHexNumber(result, address & 0xFF);
    result += payload;
    addHexNumber(result, calculateChecksum(result.data<const char *>(2), result.size() - 2));
    result.push_back(0x03);

    return result;
}

void AcquireLovePID::writeControllerCommand(LovePIDController *controller,
                                            const Util::ByteView &payload)
{
    if (controlStream == NULL)
        return;

    controlStream->writeControl(prepareCommandData(controller->getAddress(), payload));
}

void AcquireLovePID::emitRealtimeMetadataIfNeeded(double frameTime)
{
    if (realtimeEgress == NULL)
        return;
    if (haveEmittedRealtimeMeta)
        return;
    haveEmittedRealtimeMeta = true;

    SequenceValue::Transfer meta = buildLogMeta(frameTime);
    Util::append(buildRealtimeMeta(frameTime), meta);
    realtimeEgress->incomingData(std::move(meta));

    /* If we output the metadata, then also output the states of all controllers,
     * since they'll likely need persistent updating */
    for (QMap<quint16, LovePIDController *>::const_iterator add = backend.begin(),
            endAdd = backend.end(); add != endAdd; ++add) {
        controllerStatusChanged(frameTime, add.value());
    }
}

void AcquireLovePID::advanceGlobalLogging(double frameTime)
{
    if (loggingEgress == NULL)
        return;
    if (!haveEmittedLogMeta) {
        haveEmittedLogMeta = true;
        loggingMux.incoming(LogStream_Metadata, buildLogMeta(frameTime), loggingEgress);
    }
    loggingMux.advance(LogStream_Metadata, frameTime, loggingEgress);
}

AcquireLovePID::StreamData::StreamData() : processOutOfDate(1),
                                           processValueTime(FP::undefined()),
                                           processIndex(-1)
{ }

void AcquireLovePID::controllerProcessValue(double frameTime,
                                            LovePIDController *controller,
                                            double value)
{
    if (realtimeEgress == NULL && loggingEgress == NULL)
        return;
    advanceGlobalLogging(frameTime);
    emitRealtimeMetadataIfNeeded(frameTime);

    Variant::Root baseValue(value);
    remap("ZIN" + toControllerNumber(controller->getAddress()), baseValue);
    value = baseValue.read().toDouble();
    lastInputValues.insert(controller->getAddress(), value);

    double processBegin;
    double processEnd;

    QMap<quint16, StreamData>::iterator sd = loggingStreams.find(controller->getAddress());
    if (sd == loggingStreams.end()) {
        sd = loggingStreams.insert(controller->getAddress(), StreamData());
        QSet<int>::iterator id = availableStreams.begin();
        if (id != availableStreams.end()) {
            sd.value().processIndex = *id;
            availableStreams.erase(id);
        } else {
            int selected = LogStream_FirstController;
            for (QMap<quint16, StreamData>::const_iterator check = loggingStreams.constBegin(),
                    endCheck = loggingStreams.constEnd(); check != endCheck; ++check) {
                selected = qMax(selected, check.value().processIndex + 1);
            }
            loggingMux.setStreams(selected + 1);
            sd.value().processIndex = selected;
        }
        processBegin = FP::undefined();
        processEnd = frameTime;
    } else {
        processBegin = sd.value().processValueTime;
        processEnd = frameTime;
    }

    for (QVector<OutputVariable>::const_iterator var = config.first().variables.constBegin(),
            endVar = config.first().variables.constEnd(); var != endVar; ++var) {
        if (var->address != controller->getAddress())
            continue;

        Variant::Root outputValue(var->calibration.apply(value));
        remap(var->name, outputValue);

        SequenceValue dv(SequenceName({}, "raw", var->name), std::move(outputValue), processBegin,
                         processEnd);

        if (loggingEgress && FP::defined(processBegin)) {
            loggingMux.incoming(sd.value().processIndex, dv, loggingEgress);
        }

        if (realtimeEgress) {
            dv.setStart(frameTime);
            dv.setEnd(frameTime + 1.0);
            realtimeEgress->incomingData(std::move(dv));
        }
    }

    if (loggingEgress == NULL) {
        loggingMux.clear();
        sd.value().processValueTime = FP::undefined();
    } else {
        if (sd.value().processOutOfDate == 0) {
            if (FP::defined(lastStateTime)) {
                loggingMux.incoming(LogStream_State, SequenceValue({{}, "raw", "F1"}, Variant::Root(
                        Variant::Type::Flags), lastStateTime, frameTime), loggingEgress);
                if (realtimeEgress != NULL) {
                    realtimeEgress->incomingData(
                            SequenceValue({{}, "raw", "F1"}, Variant::Root(Variant::Type::Flags),
                                          frameTime, frameTime + 1.0));
                }

                Variant::Root allInputs;
                for (QMap<quint16, double>::const_iterator add = lastInputValues.constBegin(),
                        endAdd = lastInputValues.constEnd(); add != endAdd; ++add) {
                    allInputs.write().toArray().after_back().setReal(add.value());
                }
                if (realtimeEgress) {
                    realtimeEgress->incomingData(
                            SequenceValue({{}, "raw", "ZINPUTS"}, allInputs, frameTime,
                                          frameTime + 1.0));
                }
                loggingMux.incoming(LogStream_State,
                                    SequenceValue({{}, "raw", "ZINPUTS"}, std::move(allInputs),
                                                  lastStateTime, frameTime), loggingEgress);
            }
            loggingMux.advance(LogStream_State, frameTime, loggingEgress);
            lastStateTime = frameTime;

            for (QMap<quint16, StreamData>::iterator adv = loggingStreams.begin(),
                    endAdv = loggingStreams.end(); adv != endAdv; ++adv) {
                if (adv == sd)
                    continue;

                if (adv.value().processOutOfDate < 10) {
                    adv.value().processOutOfDate++;
                    continue;
                }

                adv.value().processValueTime = FP::undefined();
                loggingMux.advance(adv.value().processIndex, frameTime, loggingEgress);

                QMap<quint16, double>::iterator check = lastInputValues.find(adv.key());
                if (check != lastInputValues.end())
                    check.value() = FP::undefined();
            }
        }

        loggingMux.advance(sd.value().processIndex, processEnd, loggingEgress);
        sd.value().processValueTime = processEnd;
        sd.value().processOutOfDate = 0;

        for (QSet<int>::const_iterator adv = availableStreams.constBegin(),
                endAdv = availableStreams.constEnd(); adv != endAdv; ++adv) {
            loggingMux.advance(*adv, frameTime, loggingEgress);
        }
    }

    if (!realtimeEgress)
        return;

    for (QVector<OutputVariable>::const_iterator var = config.first().variables.constBegin(),
            endVar = config.first().variables.constEnd(); var != endVar; ++var) {
        if (var->address != controller->getAddress())
            continue;
        realtimeEgress->incomingData(
                SequenceValue(SequenceName({}, "raw", "ZIN" + var->name), baseValue, frameTime,
                              frameTime + 1.0));
    }

    realtimeEgress->incomingData(SequenceValue(
            SequenceName({}, "raw", "ZIN" + toControllerNumber(controller->getAddress())),
            std::move(baseValue), frameTime, frameTime + 1.0));
}

void AcquireLovePID::controllerSetpointValue(double frameTime,
                                             LovePIDController *controller,
                                             double value)
{
    if (realtimeEgress == NULL)
        return;
    emitRealtimeMetadataIfNeeded(frameTime);

    Variant::Root baseValue(value);
    remap("ZSP" + toControllerNumber(controller->getAddress()), baseValue);

    for (QVector<OutputVariable>::const_iterator var = config.first().variables.constBegin(),
            endVar = config.first().variables.constEnd(); var != endVar; ++var) {
        if (var->address != controller->getAddress())
            continue;
        realtimeEgress->incomingData(
                SequenceValue(SequenceName({}, "raw", "ZSP" + var->name), baseValue, frameTime,
                              frameTime + 1.0));
    }

    realtimeEgress->incomingData(SequenceValue(
            SequenceName({}, "raw", "ZSP" + toControllerNumber(controller->getAddress())),
            std::move(baseValue), frameTime, frameTime + 1.0));
}

void AcquireLovePID::controllerOutputPercentage(double frameTime,
                                                LovePIDController *controller,
                                                double value)
{
    if (realtimeEgress == NULL)
        return;
    emitRealtimeMetadataIfNeeded(frameTime);

    Variant::Root baseValue(value);
    remap("ZPCT" + toControllerNumber(controller->getAddress()), baseValue);

    for (QVector<OutputVariable>::const_iterator var = config.first().variables.constBegin(),
            endVar = config.first().variables.constEnd(); var != endVar; ++var) {
        if (var->address != controller->getAddress())
            continue;

        realtimeEgress->incomingData(
                SequenceValue(SequenceName({}, "raw", "ZPCT" + var->name), baseValue, frameTime,
                              frameTime + 1.0));
    }

    realtimeEgress->incomingData(SequenceValue(
            SequenceName({}, "raw", "ZPCT" + toControllerNumber(controller->getAddress())),
            std::move(baseValue), frameTime, frameTime + 1.0));
}

void AcquireLovePID::controllerStatusChanged(double frameTime, LovePIDController *controller)
{
    if (realtimeEgress == NULL)
        return;
    emitRealtimeMetadataIfNeeded(frameTime);

    QString state(controller->realtimeState());
    realtimeEgress->incomingData(SequenceValue(
            SequenceName({}, "raw", "ZST" + toControllerNumber(controller->getAddress())),
            Variant::Root(state), frameTime, FP::undefined()));

    for (QVector<OutputVariable>::const_iterator var = config.first().variables.constBegin(),
            endVar = config.first().variables.constEnd(); var != endVar; ++var) {
        if (var->address != controller->getAddress())
            continue;

        realtimeEgress->incomingData(
                SequenceValue(SequenceName({}, "raw", "ZST" + (var->name)),
                              Variant::Root(state), frameTime, FP::undefined()));
    }
}

bool AcquireLovePID::controllerResetInterface(LovePIDController *controller)
{
    Q_UNUSED(controller);
    return false;
    if (state == NULL)
        return false;
    for (QMap<quint16, LovePIDController *>::const_iterator check = backend.begin(),
            endCheck = backend.end(); check != endCheck; ++check) {
        if (check.value()->getInterfaceResetPending()) {
            return false;
        }
    }
    if (controlStream != NULL)
        controlStream->resetControl();
    for (QMap<quint16, LovePIDController *>::const_iterator check = backend.begin(),
            endCheck = backend.end(); check != endCheck; ++check) {
        check.value()->interfaceResetIssued();
    }
    return true;
}

void AcquireLovePID::handleLostLogging(double frameTime)
{
    for (QSet<int>::const_iterator adv = availableStreams.constBegin(),
            endAdv = availableStreams.constEnd(); adv != endAdv; ++adv) {
        loggingMux.advance(*adv, frameTime, loggingEgress);
    }

    switch (globalState) {
    case GLOBAL_PASSIVE_RUN:
    case GLOBAL_INTERACTIVE_RUN:
        return;
    case GLOBAL_PASSIVE_WAIT:
    case GLOBAL_PASSIVE_INITIALIZE:
    case GLOBAL_AUTOPROBE_PASSIVE_WAIT:
    case GLOBAL_AUTOPROBE_PASSIVE_INITIALIZE:
    case GLOBAL_INTERACTIVE_WAIT:
    case GLOBAL_INTERACTIVE_INITIALIZE:
        break;
    }

    for (QMap<quint16, StreamData>::const_iterator release = loggingStreams.constBegin(),
            endRelease = loggingStreams.constEnd(); release != endRelease; ++release) {
        int idx = release.value().processIndex;
        availableStreams.insert(idx);
        loggingMux.advance(idx, frameTime, loggingEgress);
    }
    loggingStreams.clear();

    loggingMux.advance(LogStream_Metadata, frameTime, loggingEgress);
    loggingMux.advance(LogStream_State, frameTime, loggingEgress);
    lastStateTime = FP::undefined();

    loggingMux.clear();
    loggingLost(frameTime);
}

void AcquireLovePID::updateControllerCommunications(LovePIDController *controller)
{
    switch (globalState) {
    case GLOBAL_PASSIVE_RUN:
        if (controller != NULL && controller->hasCommunications())
            break;
        globalState = GLOBAL_PASSIVE_WAIT;
        for (QMap<quint16, LovePIDController *>::const_iterator check = backend.begin(),
                endCheck = backend.end(); check != endCheck; ++check) {
            if (check.value()->hasCommunications()) {
                globalState = GLOBAL_PASSIVE_RUN;
                break;
            }
        }
        if (globalState != GLOBAL_PASSIVE_RUN) {
            generalStatusUpdated();
        }
        break;
    case GLOBAL_PASSIVE_WAIT:
    case GLOBAL_PASSIVE_INITIALIZE:
        if (controller != NULL && !controller->hasCommunications())
            break;
        globalState = GLOBAL_PASSIVE_WAIT;
        for (QMap<quint16, LovePIDController *>::const_iterator check = backend.begin(),
                endCheck = backend.end(); check != endCheck; ++check) {
            if (check.value()->hasCommunications()) {
                globalState = GLOBAL_PASSIVE_RUN;
                generalStatusUpdated();
                break;
            }
        }
        break;

    case GLOBAL_AUTOPROBE_PASSIVE_WAIT:
    case GLOBAL_AUTOPROBE_PASSIVE_INITIALIZE:
        if (controller != NULL && !controller->hasCommunications())
            break;
        globalState = GLOBAL_AUTOPROBE_PASSIVE_WAIT;
        for (QMap<quint16, LovePIDController *>::const_iterator check = backend.begin(),
                endCheck = backend.end(); check != endCheck; ++check) {
            if (check.value()->hasCommunications()) {
                globalState = GLOBAL_PASSIVE_RUN;
                generalStatusUpdated();
                break;
            }
        }
        break;

    case GLOBAL_INTERACTIVE_RUN:
        if (controller != NULL && controller->hasCommunications())
            break;
        globalState = GLOBAL_INTERACTIVE_WAIT;
        for (QMap<quint16, LovePIDController *>::const_iterator check = backend.begin(),
                endCheck = backend.end(); check != endCheck; ++check) {
            if (check.value()->hasCommunications()) {
                globalState = GLOBAL_INTERACTIVE_RUN;
                break;
            }
        }
        if (globalState != GLOBAL_INTERACTIVE_RUN) {
            generalStatusUpdated();
        }
        break;

    case GLOBAL_INTERACTIVE_WAIT:
    case GLOBAL_INTERACTIVE_INITIALIZE:
        if (controller != NULL && !controller->hasCommunications())
            break;
        globalState = GLOBAL_INTERACTIVE_WAIT;
        for (QMap<quint16, LovePIDController *>::const_iterator check = backend.begin(),
                endCheck = backend.end(); check != endCheck; ++check) {
            if (check.value()->hasCommunications()) {
                globalState = GLOBAL_INTERACTIVE_RUN;
                generalStatusUpdated();
                break;
            }
        }
        break;
    }
}

void AcquireLovePID::controllerCommunicationsLost(double frameTime,
                                                  LovePIDController *controller,
                                                  const QString &message,
                                                  Variant::Write info)
{
    info["Address"].setInt64(controller->getAddress());
    info["GlobalState"].setString(describeGlobalState());
    event(frameTime, message, true, info);

    QMap<quint16, StreamData>::iterator sd = loggingStreams.find(controller->getAddress());
    if (sd != loggingStreams.end()) {
        sd.value().processOutOfDate = 10;
        sd.value().processValueTime = FP::undefined();
    }

    updateControllerCommunications(controller);

    controllerStatusChanged(frameTime, controller);
}

void AcquireLovePID::controllerCommunicationsGained(double frameTime, LovePIDController *controller)
{
    updateControllerCommunications(controller);
    controllerStatusChanged(frameTime, controller);
}

void AcquireLovePID::controllerAutoprobeStatusChanged(LovePIDController *controller)
{
    if (controller != NULL &&
            controller->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::InProgress)
        return;
    {
        std::lock_guard<std::mutex> lock(mutex);
        if (autoprobeStatus == AcquisitionInterface::AutoprobeStatus::Success)
            return;
    }

    bool anyInProgress = false;
    for (QMap<quint16, LovePIDController *>::const_iterator check = backend.begin(),
            endCheck = backend.end(); check != endCheck; ++check) {
        switch (check.value()->getAutoprobeStatus()) {
        case AcquisitionInterface::AutoprobeStatus::Success: {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
            autoprobeStatusUpdated();
            return;
        case AcquisitionInterface::AutoprobeStatus::Failure:
            break;
        case AcquisitionInterface::AutoprobeStatus::InProgress:
            anyInProgress = true;
            break;
        }
    }

    if (!anyInProgress) {
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        return;
    }
}

void AcquireLovePID::controllerMetaChanged(LovePIDController *controller)
{
    Q_UNUSED(controller);
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    sourceMetadataUpdated();
}

void AcquireLovePID::configurationChanged()
{
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;

    for (QVector<OutputVariable>::const_iterator c = config.first().variables.constBegin(),
            endC = config.first().variables.constEnd(); c != endC; ++c) {
        backend.lookup(c->address);
    }
    for (QMap<quint16, ActiveController>::const_iterator
            c = config.first().controllers.constBegin(),
            endC = config.first().controllers.constEnd(); c != endC; ++c) {
        LovePIDController *controller = backend.lookup(c.key());
        if (FP::defined(c.value().initialSetpoint) &&
                !FP::defined(controller->getTargetSetpoint())) {
            controller->setTargetSetpoint(c.value().initialSetpoint);
        }
        if (c.value().manualModeSet) {
            controller->changeManualMode(c.value().manualMode);
        }
        if (INTEGER::defined(c.value().decimalDigits)) {
            controller->changeDecimalDigits((int) c.value().decimalDigits);
        }
    }
}

void AcquireLovePID::configurationAdvance(double frameTime)
{
    Q_ASSERT(!config.isEmpty());
    int oldSize = config.size();
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.isEmpty());

    if (oldSize == config.size())
        return;
    configurationChanged();
    /* So we advance the bus if something got changed */
    timeoutAt(frameTime);
}

bool AcquireLovePID::controllerManualMode(LovePIDController *controller, bool &mode) const
{
    QMap<quint16, ActiveController>::const_iterator
            check = config.first().controllers.find(controller->getAddress());
    if (check == config.first().controllers.constEnd())
        return false;
    if (!check.value().manualModeSet)
        return false;
    if (check.value().manualMode == mode)
        return false;
    mode = check.value().manualMode;
    return true;
}

bool AcquireLovePID::controllerDecimalDigits(LovePIDController *controller, int &digits) const
{
    QMap<quint16, ActiveController>::const_iterator
            check = config.first().controllers.find(controller->getAddress());
    if (check == config.first().controllers.constEnd())
        return false;
    if (!INTEGER::defined(check.value().decimalDigits))
        return false;
    if (check.value().decimalDigits == digits)
        return false;
    digits = (int) check.value().decimalDigits;
    return true;
}

void AcquireLovePID::corruptedDataFrame(const Util::ByteView &frame, double frameTime, int code)
{
    LovePIDController *controller = static_cast<LovePIDController *>(
            bus.nextResponder());
    if (controller == NULL) {
        switch (globalState) {
        case GLOBAL_PASSIVE_RUN:
        case GLOBAL_PASSIVE_WAIT:
        case GLOBAL_PASSIVE_INITIALIZE:
        case GLOBAL_AUTOPROBE_PASSIVE_WAIT:
        case GLOBAL_AUTOPROBE_PASSIVE_INITIALIZE:
            /* Silence in passive mode (assume we just didn't see the
             * initiator */
            return;
        default:
            break;
        }
        qCDebug(log) << "Corrupted data frame with code" << code
                     << "received with no expected response";
        return;
    }
    controller->incomingCorruptDataFrame(frame, code, frameTime);
}

void AcquireLovePID::processDataFrame(const Util::ByteView &frame, double frameTime)
{
    if (frame.size() < 5) {
        corruptedDataFrame(frame, frameTime, 1);
        return;
    }

    quint16 address = 0;
    switch (frame[0]) {
    case 'L':
        address = 0;
        break;
    case 'O':
        address = 0x100;
        break;
    case 'V':
        address = 0x200;
        break;
    case 'E':
        address = 0x300;
        break;
    default:
        corruptedDataFrame(frame, frameTime, 1);
        return;
    }

    {
        quint8 low = 0;
        if (!parseHexNumber(frame.data<const char *>(1), low)) {
            corruptedDataFrame(frame, frameTime, 2);
            return;
        }
        address |= low;
    }

    if (frame[3] == 'N') {
        if (frame.size() < 6) {
            corruptedDataFrame(frame, frameTime, 3);
            return;
        }

        int errorCode = 0;
        if (!parseDecimalNumber(frame.data<const char *>(4), 2, errorCode)) {
            corruptedDataFrame(frame, frameTime, 4);
            return;
        }

        backend.lookupProvisional(address)->incomingErrorFrame(errorCode, frameTime);
        return;
    }

    {
        quint8 rx = 0;
        if (!parseHexNumber(frame.data<const char *>(frame.size() - 2), rx)) {
            corruptedDataFrame(frame, frameTime, 5);
            return;
        }
        quint8 cs = calculateChecksum(frame.data<const char *>(), frame.size() - 2);
        if (cs != rx) {
            corruptedDataFrame(frame, frameTime, 6);
            return;
        }
    }

    backend.lookup(address)->incomingDataFrame(frame.mid(3, frame.size() - 5), frameTime);
}

void AcquireLovePID::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configurationAdvance(frameTime);

    processDataFrame(frame, frameTime);

    bus.advance(frameTime);
    handleLostLogging(frameTime);
}

void AcquireLovePID::processControlFrame(const Util::ByteView &frame, double frameTime)
{
    if (frame.size() < 5)
        return;

    quint16 address = 0;
    switch (frame[0]) {
    case 'L':
        address = 0;
        break;
    case 'O':
        address = 0x100;
        break;
    case 'V':
        address = 0x200;
        break;
    case 'E':
        address = 0x300;
        break;
    default:
        return;
    }

    {
        quint8 low = 0;
        if (!parseHexNumber(frame.data<const char *>(1), low)) {
            return;
        }
        address |= low;
    }

    {
        quint8 rx = 0;
        if (!parseHexNumber(frame.data<const char *>(frame.size() - 2), rx)) {
            return;
        }
        quint8 cs = calculateChecksum(frame.data<const char *>(1), frame.size() - 3);
        if (cs != rx) {
            return;
        }
    }

    backend.lookupProvisional(address)
           ->incomingControlFrame(frame.mid(3, frame.size() - 5), frameTime);
}

void AcquireLovePID::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configurationAdvance(frameTime);

    processControlFrame(frame, frameTime);

    bus.advance(frameTime);
    handleLostLogging(frameTime);
}

void AcquireLovePID::incomingInstrumentTimeout(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);

    switch (globalState) {
    case GLOBAL_AUTOPROBE_PASSIVE_INITIALIZE:
        globalState = GLOBAL_AUTOPROBE_PASSIVE_WAIT;
        /* Insert the defaults if there's no configuration, so we attempt
         * something anyway */
        if (backend.begin() == backend.end()) {
            backend.lookupProvisional(0x31);
            backend.lookupProvisional(0x32);
            backend.lookupProvisional(0x33);
            backend.lookupProvisional(0x34);
        }
        break;

    case GLOBAL_INTERACTIVE_INITIALIZE:
        globalState = GLOBAL_INTERACTIVE_WAIT;
        /* Insert the defaults if there's no configuration, so we attempt
         * something anyway */
        if (backend.begin() == backend.end()) {
            backend.lookupProvisional(0x31);
            backend.lookupProvisional(0x32);
            backend.lookupProvisional(0x33);
            backend.lookupProvisional(0x34);
        }
        break;

    case GLOBAL_PASSIVE_INITIALIZE:
        globalState = GLOBAL_PASSIVE_WAIT;
        /* Insert the defaults if there's no configuration, so we attempt
         * something anyway */
        if (backend.begin() == backend.end()) {
            backend.lookupProvisional(0x31);
            backend.lookupProvisional(0x32);
            backend.lookupProvisional(0x33);
            backend.lookupProvisional(0x34);
        }
        break;

    default:
        break;
    }

    bus.advance(frameTime);
    handleLostLogging(frameTime);
}

void AcquireLovePID::discardDataCompleted(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);
    for (QSet<int>::const_iterator adv = availableStreams.constBegin(),
            endAdv = availableStreams.constEnd(); adv != endAdv; ++adv) {
        loggingMux.advance(*adv, frameTime, loggingEgress);
    }

    LovePIDController *controller = static_cast<LovePIDController *>(
            bus.busLocker());
    if (controller != NULL) {
        controller->discardDataCompleted(frameTime);
    }

    bus.advance(frameTime);
}

void AcquireLovePID::shutdownInProgress(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);

    for (QMap<quint16, ActiveController>::const_iterator
            c = config.first().controllers.constBegin(),
            endC = config.first().controllers.constEnd(); c != endC; ++c) {
        if (!FP::defined(c.value().exitSetpoint))
            continue;
        LovePIDController *controller = backend.lookup(c.key());
        if (!controller->hasCommunications())
            continue;
        controller->applyExitSetpoint(c.value().exitSetpoint);
    }
}

AcquireLovePID::Backend::Backend(AcquireLovePID *parent) : pid(parent),
                                                           controllers(),
                                                           controlIterator(controllers.constEnd()),
                                                           simpleIterator(controllers.constEnd())
{ }

AcquireLovePID::Backend::~Backend()
{ }

LovePIDController *AcquireLovePID::Backend::addController(quint16 address)
{
    LovePIDController
            *controller = new LovePIDController(pid->bus.createDeviceInterface(), pid, address);
    switch (pid->globalState) {
    case GLOBAL_INTERACTIVE_INITIALIZE:
    case GLOBAL_INTERACTIVE_WAIT:
    case GLOBAL_INTERACTIVE_RUN:
        controller->setToInteractive();
        break;
    case GLOBAL_AUTOPROBE_PASSIVE_INITIALIZE:
    case GLOBAL_AUTOPROBE_PASSIVE_WAIT:
        controller->setToAutoprobe();
        break;
    case GLOBAL_PASSIVE_INITIALIZE:
    case GLOBAL_PASSIVE_WAIT:
    case GLOBAL_PASSIVE_RUN:
        break;
    }

    Q_ASSERT(!pid->config.isEmpty());
    controller->setTargetSetpoint(pid->config.first().controllers.value(address).initialSetpoint);

    return controller;
}

LovePIDController *AcquireLovePID::Backend::lookup(quint16 address)
{
    {
        QMap<quint16, LovePIDController *>::const_iterator
                existing = controllers.constFind(address);
        if (existing != controllers.constEnd())
            return existing.value();
    }

    LovePIDController *controller = addController(address);

    controllers.insert(address, controller);
    controlIterator = controllers.constEnd();
    simpleIterator = controllers.constEnd();

    pid->updateControllerCommunications(controller);
    pid->controllerAutoprobeStatusChanged(controller);
    return controller;
}

LovePIDController *AcquireLovePID::Backend::lookupProvisional(quint16 address)
{
    {
        QMap<quint16, LovePIDController *>::const_iterator
                existing = controllers.constFind(address);
        if (existing != controllers.constEnd())
            return existing.value();
    }

    LovePIDController *controller = addController(address);
    controller->setProvisional(pid->config.first().provisionalTimeout);

    controllers.insert(address, controller);
    controlIterator = controllers.constEnd();
    simpleIterator = controllers.constEnd();

    pid->updateControllerCommunications(controller);
    pid->controllerAutoprobeStatusChanged(controller);
    return controller;
}

BusManager::Device *AcquireLovePID::Backend::advanceControl()
{
    if (controlIterator == controllers.constEnd())
        controlIterator = controllers.constBegin();
    if (controlIterator == controllers.constEnd())
        return NULL;
    BusManager::Device *result = controlIterator.value();
    ++controlIterator;
    return result;
}

void AcquireLovePID::Backend::removeDevice(BusManager::Device *device)
{
    LovePIDController *controller = static_cast<LovePIDController *>(device);
    QMap<quint16, LovePIDController *>::iterator del = controllers.find(controller->getAddress());
    if (del == controllers.end())
        return;

    QMap<quint16, StreamData>::iterator sd = pid->loggingStreams.find(controller->getAddress());
    if (sd != pid->loggingStreams.end()) {
        pid->availableStreams.insert(sd.value().processIndex);
        pid->loggingStreams.erase(sd);
    }
    pid->lastInputValues.remove(controller->getAddress());

    delete controller;
    controllers.erase(del);

    controlIterator = controllers.constEnd();
    simpleIterator = controllers.constEnd();

    pid->haveEmittedLogMeta = false;
    pid->haveEmittedRealtimeMeta = false;
    pid->updateControllerCommunications(NULL);
    pid->controllerAutoprobeStatusChanged(NULL);
}

void AcquireLovePID::Backend::timeoutAt(double time)
{ pid->timeoutAt(time); }

void AcquireLovePID::Backend::beginSimpleIteration()
{ simpleIterator = controllers.constBegin(); }

BusManager::Device *AcquireLovePID::Backend::nextSimpleIteration()
{
    if (simpleIterator == controllers.constEnd())
        return NULL;
    BusManager::Device *result = simpleIterator.value();
    ++simpleIterator;
    return result;
}

void AcquireLovePID::Backend::clear()
{
    for (QMap<quint16, LovePIDController *>::iterator del = controllers.begin(),
            endDel = controllers.end(); del != endDel; ++del) {
        delete del.value();
    }
    controllers.clear();
    controlIterator = controllers.constEnd();
    simpleIterator = controllers.constEnd();
}

QString AcquireLovePID::describeGlobalState() const
{
    switch (globalState) {
    case GLOBAL_AUTOPROBE_PASSIVE_INITIALIZE:
        return "AutoprobePassiveInitialize";
    case GLOBAL_AUTOPROBE_PASSIVE_WAIT:
        return "AutoprobePassiveWait";
    case GLOBAL_PASSIVE_INITIALIZE:
        return "PassiveInitialize";
    case GLOBAL_PASSIVE_WAIT:
        return "PassiveWait";
    case GLOBAL_PASSIVE_RUN:
        return "PassiveRun";
    case GLOBAL_INTERACTIVE_INITIALIZE:
        return "InteractiveInitialize";
    case GLOBAL_INTERACTIVE_WAIT:
        return "InteractiveWait";
    case GLOBAL_INTERACTIVE_RUN:
        return "InteractiveRun";
    }
    return "Invalid";
}

Variant::Root AcquireLovePID::getSourceMetadata()
{
    Variant::Root result;
    QSet<QString> models;
    QSet<QString> controllerIDs;
    QSet<QString> serialNumbers;
    QSet<QString> firmwareVersions;
    QSet<quint16> addresses;
    {
        PauseLock paused(*this);
        result = instrumentMeta;
        auto controllerAddresses = result.write()["AddressIndex"];
        auto controllerVariables = result.write()["VariableIndex"];
        for (QMap<quint16, LovePIDController *>::const_iterator controller = backend.begin(),
                endControllers = backend.end(); controller != endControllers; ++controller) {
            auto meta = controller.value()->getMetadata();
            QString add(meta["Model"].toQString());
            if (!add.isEmpty())
                models |= add;

            add = meta["ControllerID"].toQString();
            if (!add.isEmpty())
                controllerIDs |= add;

            add = meta["SerialNumber"].toQString();
            if (!add.isEmpty())
                serialNumbers |= add;

            add = meta["FirmwareVersion"].toQString();
            if (!add.isEmpty())
                firmwareVersions |= add;

            auto addr = controller.value()->getAddress();
            addresses |= addr;
            controllerAddresses.toArray().after_back().setInteger(addr);

            auto targetVariable = controllerVariables.toArray().after_back();
            targetVariable.setEmpty();
            for (auto variable = config.first().variables.constBegin(),
                    end = config.first().variables.constEnd(); variable != end; ++variable) {
                if (variable->address != addr) {
                    continue;
                }

                targetVariable.setString(variable->name);
                break;
            }
        }
    }

    if (!models.isEmpty()) {
        QStringList sorted(models.values());
        std::sort(sorted.begin(), sorted.end());
        result["Model"].setString(sorted.join(","));
    }
    if (!controllerIDs.isEmpty()) {
        QStringList sorted(controllerIDs.values());
        std::sort(sorted.begin(), sorted.end());
        result["ControllerID"].setString(sorted.join(","));
    }
    if (!serialNumbers.isEmpty()) {
        QStringList sorted(serialNumbers.values());
        std::sort(sorted.begin(), sorted.end());
        result["SerialNumber"].setString(sorted.join(","));
    }
    if (!firmwareVersions.isEmpty()) {
        QStringList sorted(firmwareVersions.values());
        std::sort(sorted.begin(), sorted.end());
        result["FirmwareVersion"].setString(sorted.join(","));
    }
    if (!addresses.isEmpty()) {
        QList<quint16> sorted(addresses.values());
        std::sort(sorted.begin(), sorted.end());
        QString combined;
        for (QList<quint16>::const_iterator add = sorted.constBegin(), endAdd = sorted.constEnd();
                add != endAdd;
                ++add) {
            if (!combined.isEmpty())
                combined.append(',');
            combined.append(QString::number(*add, 16).toUpper());
        }
        result["Address"].setString(combined);
    }

    return result;
}

AcquisitionInterface::GeneralStatus AcquireLovePID::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (globalState) {
    case GLOBAL_PASSIVE_RUN:
    case GLOBAL_INTERACTIVE_RUN:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

void AcquireLovePID::command(const Variant::Read &command)
{
    Q_ASSERT(!config.isEmpty());

    if (command.hash("ChangeSetpoints").exists()) {
        for (QMap<quint16, LovePIDController *>::const_iterator check = backend.begin(),
                endCheck = backend.end(); check != endCheck; ++check) {
            double sp = Variant::Composite::toNumber(command.hash("ChangeSetpoints")
                                                            .hash("Parameters")
                                                            .hash(QString::number(check.key(), 16))
                                                            .hash("Value"));
            if (!FP::defined(sp))
                continue;
            check.value()->setTargetSetpoint(sp);
        }
    }

    if (command.hash("Setpoint").exists()) {
        qint64 addr = Variant::Composite::toInteger(
                command.hash("Setpoint").hash("Parameters").hash("Address").hash("Value"));
        if (!INTEGER::defined(addr))
            addr = Variant::Composite::toInteger(command.hash("Setpoint").hash("Address"));
        if (!INTEGER::defined(addr)) {
            qint64 check = Variant::Composite::toInteger(
                    command.hash("Setpoint").hash("Parameters").hash("Index").hash("Value"));
            if (!INTEGER::defined(check))
                check = Variant::Composite::toInteger(command.hash("Setpoint").hash("Index"));
            if (INTEGER::defined(check) && check >= 0 && check < backend.size()) {
                QMap<quint16, LovePIDController *>::const_iterator controller = backend.begin();
                controller += (int) (check);
                addr = controller.key();
            }
        }
        if (!INTEGER::defined(addr)) {
            QString check(command.hash("Setpoint")
                                 .hash("Parameters")
                                 .hash("Name")
                                 .hash("Value")
                                 .toQString());
            if (check.isEmpty())
                check = command.hash("Setpoint").hash("Name").toQString();
            addr = config.first().setpointNames.value(check, 0);
        }

        double sp = Variant::Composite::toNumber(
                command.hash("Setpoint").hash("Parameters").hash("Value").hash("Value"));
        if (!FP::defined(sp)) {
            sp = Variant::Composite::toNumber(
                    command.hash("Setpoint").hash("Parameters").hash("Setpoint"));
        }
        if (!FP::defined(sp)) {
            sp = Variant::Composite::toNumber(command.hash("Setpoint").hash("Value"));
        }
        if (!FP::defined(sp)) {
            sp = Variant::Composite::toNumber(command.hash("Setpoint").hash("Setpoint"));
        }

        if (FP::defined(sp) && isValidAddress(addr)) {
            backend.lookupProvisional(addr)->setTargetSetpoint(sp);
        }
    }

    if (command.hash("Output").exists()) {
        if (command.hash("Output").getType() != Variant::Type::Hash) {
            double sp = Variant::Composite::toNumber(command.hash("Output"));
            qint64 addr = config.first().setpointNames.value(QString(), 0);
            if (FP::defined(sp) && isValidAddress(addr)) {
                backend.lookupProvisional(addr)->setTargetSetpoint(sp);
            }
        } else {
            qint64 addr = Variant::Composite::toInteger(
                    command.hash("Output").hash("Parameters").hash("Address").hash("Value"));
            if (!INTEGER::defined(addr))
                addr = Variant::Composite::toInteger(command.hash("Output").hash("Address"));
            if (!INTEGER::defined(addr)) {
                qint64 check = Variant::Composite::toInteger(
                        command.hash("Output").hash("Parameters").hash("Index").hash("Value"));
                if (!INTEGER::defined(check))
                    check = Variant::Composite::toInteger(command.hash("Output").hash("Index"));
                if (INTEGER::defined(check) && check >= 0 && check < backend.size()) {
                    QMap<quint16, LovePIDController *>::const_iterator controller = backend.begin();
                    controller += (int) (check);
                    addr = controller.key();
                }
            }
            if (!INTEGER::defined(addr)) {
                QString check(command.hash("Output")
                                     .hash("Parameters")
                                     .hash("Name")
                                     .hash("Value")
                                     .toQString());
                if (check.isEmpty())
                    check = command.hash("Output").hash("Name").toQString();
                addr = config.first().setpointNames.value(check, 0);
            }

            double sp = Variant::Composite::toNumber(
                    command.hash("Output").hash("Parameters").hash("Value").hash("Value"));
            if (!FP::defined(sp)) {
                sp = Variant::Composite::toNumber(
                        command.hash("Setpoint").hash("Parameters").hash("Setpoint"));
            }
            if (!FP::defined(sp)) {
                sp = Variant::Composite::toNumber(command.hash("Output").hash("Value"));
            }
            if (!FP::defined(sp)) {
                sp = Variant::Composite::toNumber(command.hash("Output").hash("Setpoint"));
            }

            if (!isValidAddress(addr) || !FP::defined(sp)) {
                for (auto check : command.hash("Output").toHash()) {
                    addr = config.first()
                                 .setpointNames
                                 .value(QString::fromStdString(check.first), 0);
                    if (addr == 0) {
                        bool ok = false;
                        addr = QString::fromStdString(check.first).toUShort(&ok, false);
                        if (!ok)
                            addr = 0;
                    }
                    if (!isValidAddress(addr))
                        continue;

                    sp = Variant::Composite::toNumber(check.second);
                    if (!FP::defined(sp))
                        sp = Variant::Composite::toNumber(check.second.hash("Value"));
                    if (!FP::defined(sp))
                        continue;

                    backend.lookupProvisional(addr)->setTargetSetpoint(sp);
                }
            } else {
                backend.lookupProvisional(addr)->setTargetSetpoint(sp);
            }
        }
    }

    if (command.hash("Bypass").toBool()) {
        for (QMap<quint16, ActiveController>::const_iterator
                c = config.first().controllers.constBegin(),
                endC = config.first().controllers.constEnd(); c != endC; ++c) {
            LovePIDController *controller = backend.lookup(c.key());
            if (FP::defined(c.value().bypassSetpoint)) {
                controller->setTargetSetpoint(c.value().bypassSetpoint);
            }
        }
    }
    if (command.hash("UnBypass").toBool()) {
        for (QMap<quint16, ActiveController>::const_iterator
                c = config.first().controllers.constBegin(),
                endC = config.first().controllers.constEnd(); c != endC; ++c) {
            LovePIDController *controller = backend.lookup(c.key());
            if (FP::defined(c.value().unbypassSetpoint)) {
                controller->setTargetSetpoint(c.value().unbypassSetpoint);
            }
        }
    }
}

Variant::Root AcquireLovePID::getCommands()
{
    Variant::Root result;

    result["ChangeSetpoints"].hash("DisplayName").setString("&Change Setpoints");
    result["ChangeSetpoints"].hash("ToolTip").setString("Change controller setpoints.");
    for (QMap<quint16, LovePIDController *>::const_iterator check = backend.begin(),
            endCheck = backend.end(); check != endCheck; ++check) {
        QString index(QString::number(check.key(), 16));
        auto target = result["ChangeSetpoints"].hash("Parameters").hash(index);

        target.hash("Variable").setString(QString("ZSP%1").arg(index));
        target.hash("Name").setString(QString("Controller %1 Setpoint").arg(index));
        switch (check.value()->getDecimalDigits()) {
        default:
        case 0:
            target.hash("Format").setString("0000");
            target.hash("Minimum").setInt64(-9999);
            target.hash("Maximum").setInt64(9999);
            break;
        case 1:
            target.hash("Format").setString("000.0");
            target.hash("Minimum").setDouble(-999.9);
            target.hash("Maximum").setDouble(999.9);
            break;
        case 2:
            target.hash("Format").setString("00.00");
            target.hash("Minimum").setDouble(-99.99);
            target.hash("Maximum").setDouble(99.99);
            break;
        case 3:
            target.hash("Format").setString("0.000");
            target.hash("Minimum").setDouble(-9.999);
            target.hash("Maximum").setDouble(9.999);
            break;
        }
    }

    return result;
}

AcquisitionInterface::AutoprobeStatus AcquireLovePID::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireLovePID::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    configurationAdvance(time);

    switch (globalState) {
    case GLOBAL_INTERACTIVE_INITIALIZE:
    case GLOBAL_INTERACTIVE_WAIT:
    case GLOBAL_INTERACTIVE_RUN:
        /* Already running/started, do nothing */
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << globalState << "at" << Logging::time(time);
        break;

    case GLOBAL_AUTOPROBE_PASSIVE_INITIALIZE:
    case GLOBAL_AUTOPROBE_PASSIVE_WAIT:
    case GLOBAL_PASSIVE_INITIALIZE:
    case GLOBAL_PASSIVE_WAIT:
    case GLOBAL_PASSIVE_RUN:
        if (backend.size() == 0) {
            qCDebug(log) << "Interactive autoprobe failed from passive state" << globalState << "at"
                         << Logging::time(time) << "because there are no available controllers";

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
        } else {
            qCDebug(log) << "Interactive autoprobe started from passive state" << globalState
                         << "at" << Logging::time(time) << "with" << backend.size()
                         << "controller(s)";

            /* Reset this if needed, so we don't report success until we can
             * finish interrogating the instrument. */
            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
            }
        }
        autoprobeStatusUpdated();

        globalState = GLOBAL_INTERACTIVE_WAIT;
        for (QMap<quint16, LovePIDController *>::const_iterator controller = backend.begin(),
                endController = backend.end(); controller != endController; ++controller) {
            controller.value()->autoprobeTryInteractive(time);
        }
        bus.advance(time);

        generalStatusUpdated();
        break;
    }
}

void AcquireLovePID::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    configurationAdvance(time);

    qCDebug(log) << "Reset to passive autoprobe from state" << globalState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    globalState = GLOBAL_PASSIVE_WAIT;
    for (QMap<quint16, LovePIDController *>::const_iterator controller = backend.begin(),
            endController = backend.end(); controller != endController; ++controller) {
        controller.value()->autoprobeTryInteractive(time);
    }
    bus.advance(time);

    generalStatusUpdated();
}

void AcquireLovePID::autoprobePromote(double time)
{
    PauseLock paused(*this);

    configurationAdvance(time);

    switch (globalState) {
    case GLOBAL_INTERACTIVE_INITIALIZE:
    case GLOBAL_INTERACTIVE_WAIT:
    case GLOBAL_INTERACTIVE_RUN:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << globalState
                     << "to interactive acquisition at" << Logging::time(time);

        bus.advance(time);
        break;

    case GLOBAL_AUTOPROBE_PASSIVE_INITIALIZE:
    case GLOBAL_AUTOPROBE_PASSIVE_WAIT:
    case GLOBAL_PASSIVE_INITIALIZE:
    case GLOBAL_PASSIVE_WAIT:
    case GLOBAL_PASSIVE_RUN:
        qCDebug(log) << "Promoted from passive state" << globalState
                     << "to interactive acquisition at" << Logging::time(time) << "with"
                     << backend.size() << "controller(s)";

        globalState = GLOBAL_INTERACTIVE_WAIT;
        for (QMap<quint16, LovePIDController *>::const_iterator controller = backend.begin(),
                endController = backend.end(); controller != endController; ++controller) {
            controller.value()->autoprobePromote(time);
        }
        bus.advance(time);

        generalStatusUpdated();
        break;
    }
}

void AcquireLovePID::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    configurationAdvance(time);

    switch (globalState) {
    case GLOBAL_INTERACTIVE_INITIALIZE:
    case GLOBAL_INTERACTIVE_WAIT:
    case GLOBAL_INTERACTIVE_RUN:
        qCDebug(log) << "Switching to passive acquisition from interactive state" << globalState
                     << "at" << Logging::time(time);

        globalState = GLOBAL_PASSIVE_WAIT;

        for (QMap<quint16, LovePIDController *>::const_iterator controller = backend.begin(),
                endController = backend.end(); controller != endController; ++controller) {
            controller.value()->autoprobePromotePassive(time);
        }
        bus.advance(time);

        generalStatusUpdated();
        break;

    case GLOBAL_AUTOPROBE_PASSIVE_INITIALIZE:
        qCDebug(log) << "Promoted to passive acquisition from passive autoprobe initialization"
                     << Logging::time(time);
        globalState = GLOBAL_PASSIVE_INITIALIZE;

        bus.advance(time);
        break;
    case GLOBAL_AUTOPROBE_PASSIVE_WAIT:
        qCDebug(log) << "Promoted to passive acquisition from passive autoprobe waiting"
                     << Logging::time(time);
        globalState = GLOBAL_PASSIVE_WAIT;

        bus.advance(time);
        break;

    case GLOBAL_PASSIVE_INITIALIZE:
    case GLOBAL_PASSIVE_WAIT:
    case GLOBAL_PASSIVE_RUN:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted to passive acquisition from passive state" << globalState << "at"
                     << Logging::time(time);

        bus.advance(time);
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireLovePID::getDefaults()
{
    AutomaticDefaults result;
    result.name = "X$2";
    result.setSerialN81(9600);
    result.autoprobeInitialOrder = 10;
    return result;
}


ComponentOptions AcquireLovePIDComponent::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireLovePIDComponent::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireLovePIDComponent::createAcquisitionPassive(const ComponentOptions &options,
                                                                                const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireLovePID(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireLovePIDComponent::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireLovePID(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireLovePIDComponent::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext)
{
    std::unique_ptr<AcquireLovePID> i(new AcquireLovePID(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<
        AcquisitionInterface> AcquireLovePIDComponent::createAcquisitionInteractive(const ValueSegment::Transfer &config,
                                                                                    const std::string &loggingContext)
{
    std::unique_ptr<AcquireLovePID> i(new AcquireLovePID(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
