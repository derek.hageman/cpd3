/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIRELOVEPID_H
#define ACQUIRELOVEPID_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "acquisition/busmanager.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"

class LovePIDController;

class AcquireLovePID : public CPD3::Acquisition::FramedInstrument {
    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum {
        /* Waiting a response to a command in passive mode */
                GLOBAL_PASSIVE_RUN,

        /* Waiting for enough data to confirm communications */
                GLOBAL_PASSIVE_WAIT,
        /* Passive initialization (same as passive wait, but discard the
         * first timeout). */
                GLOBAL_PASSIVE_INITIALIZE,

        /* Same as passive wait but currently autoprobing */
                GLOBAL_AUTOPROBE_PASSIVE_WAIT,
        /* Same as passive initialize but autoprobing */
                GLOBAL_AUTOPROBE_PASSIVE_INITIALIZE,

        /* Operating in interactive mode */
                GLOBAL_INTERACTIVE_RUN,
        /* Waiting for any controller to initialize in interactive mode */
                GLOBAL_INTERACTIVE_WAIT,

        /* Initial state for interactive start */
                GLOBAL_INTERACTIVE_INITIALIZE,
    } globalState;

    QString describeGlobalState() const;

    friend class LovePIDController;

    friend class Backend;

    class Backend : public CPD3::Acquisition::BusManager::Backend {
        AcquireLovePID *pid;
        QMap<quint16, LovePIDController *> controllers;
        QMap<quint16, LovePIDController *>::const_iterator controlIterator;
        QMap<quint16, LovePIDController *>::const_iterator simpleIterator;

        LovePIDController *addController(quint16 address);

    public:
        Backend(AcquireLovePID *parent);

        virtual ~Backend();

        inline QMap<quint16, LovePIDController *>::const_iterator begin() const
        { return controllers.constBegin(); }

        inline QMap<quint16, LovePIDController *>::const_iterator end() const
        { return controllers.constEnd(); }

        inline int size() const
        { return controllers.size(); }

        LovePIDController *lookup(quint16 address);

        LovePIDController *lookupProvisional(quint16 address);

        virtual CPD3::Acquisition::BusManager::Device *advanceControl();

        virtual void removeDevice(CPD3::Acquisition::BusManager::Device *device);

        virtual void timeoutAt(double time);

        virtual void beginSimpleIteration();

        virtual CPD3::Acquisition::BusManager::Device *nextSimpleIteration();

        void clear();
    };

    Backend backend;
    CPD3::Acquisition::BusManager bus;

    enum {
        LogStream_Metadata = 0, LogStream_State,

        LogStream_FirstController,

        LogStream_ALLOCATE = LogStream_State + 1,
    };
    CPD3::Data::StaticMultiplexer loggingMux;

    struct StreamData {
        int processOutOfDate;
        double processValueTime;
        int processIndex;

        StreamData();
    };

    QMap<quint16, StreamData> loggingStreams;
    QSet<int> availableStreams;
    double lastStateTime;

    QMap<quint16, double> lastInputValues;

    struct OutputVariable {
        CPD3::Data::SequenceName::Component name;
        CPD3::Data::Variant::Root metadata;

        quint16 address;
        CPD3::Calibration calibration;
    };

    struct ActiveController {
        bool manualMode;
        bool manualModeSet;
        double initialSetpoint;
        double exitSetpoint;
        double bypassSetpoint;
        double unbypassSetpoint;
        qint64 decimalDigits;

        ActiveController();
    };

    class Configuration {
        double start;
        double end;

        void addController(qint64 address, const CPD3::Data::Variant::Read &config);

        void addVariable(const CPD3::Data::Variant::Read &config,
                         qint64 defaultAddress = CPD3::INTEGER::undefined(),
                         const CPD3::Data::SequenceName::Component &defaultName = {});

    public:
        QMap<quint16, ActiveController> controllers;
        QVector<OutputVariable> variables;
        QHash<QString, quint16> setpointNames;

        int retryTimes;
        double provisionalTimeout;
        double pollInterval;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under, const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    CPD3::Data::Variant::Root instrumentMeta;
    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;

    void setDefaultInvalid();

    void configurationChanged();

    void configurationAdvance(double frameTime);

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time);

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time);

    void emitRealtimeMetadataIfNeeded(double frameTime);

    void advanceGlobalLogging(double frameTime);

    void handleLostLogging(double frameTime);

    void corruptedDataFrame(const CPD3::Util::ByteView &frame, double frameTime, int code);

    void processDataFrame(const CPD3::Util::ByteView &frame, double frameTime);

    void processControlFrame(const CPD3::Util::ByteView &frame, double frameTime);

    CPD3::Util::ByteArray prepareCommandData(quint16 address, const CPD3::Util::ByteView &payload);

public:
    AcquireLovePID(const CPD3::Data::ValueSegment::Transfer &config,
                   const std::string &loggingContext);

    AcquireLovePID(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireLovePID();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus() override;

    CPD3::Data::Variant::Root getCommands() override;

    CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables() override;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus() override;

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

    void controllerProcessValue(double frameTime, LovePIDController *controller, double value);

    void controllerSetpointValue(double frameTime, LovePIDController *controller, double value);

    void controllerOutputPercentage(double frameTime, LovePIDController *controller, double value);

    void updateControllerCommunications(LovePIDController *controller);

    void controllerStatusChanged(double frameTime, LovePIDController *controller);

    void controllerCommunicationsLost(double frameTime,
                                      LovePIDController *controller,
                                      const QString &message,
                                      CPD3::Data::Variant::Write info = CPD3::Data::Variant::Write::empty());

    void controllerCommunicationsGained(double frameTime, LovePIDController *controller);

    void controllerAutoprobeStatusChanged(LovePIDController *controller);

    void controllerMetaChanged(LovePIDController *controller);

    void writeControllerCommand(LovePIDController *controller, const CPD3::Util::ByteView &payload);

    bool controllerManualMode(LovePIDController *controller, bool &mode) const;

    bool controllerDecimalDigits(LovePIDController *controller, int &digits) const;

    bool controllerResetInterface(LovePIDController *controller);

protected:
    void command(const CPD3::Data::Variant::Read &value) override;

    std::size_t dataFrameStart(std::size_t offset,
                               const CPD3::Util::ByteArray &input) const override;

    std::size_t dataFrameEnd(std::size_t start, const CPD3::Util::ByteArray &input) const override;

    std::size_t controlFrameStart(std::size_t offset,
                                  const CPD3::Util::ByteArray &input) const override;

    std::size_t controlFrameEnd(std::size_t start,
                                const CPD3::Util::ByteArray &input) const override;

    void limitDataBuffer(CPD3::Util::ByteArray &buffer) override;

    void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingInstrumentTimeout(double time) override;

    void discardDataCompleted(double time) override;

    void shutdownInProgress(double time) override;
};

class AcquireLovePIDComponent
        : public QObject, virtual public CPD3::Acquisition::AcquisitionComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_love_pid"
                              FILE
                              "acquire_love_pid.json")

    CPD3::ComponentOptions getBaseOptions();

public:
    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
