/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <sstream>

#include "core/environment.hxx"
#include "core/qtcompat.hxx"
#include "acquisition/acquisitioncomponent.hxx"

#include "controller.hxx"
#include "acquire_love_pid.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

LovePIDController::Command::Command() : type(COMMAND_IGNORED)
{ }

LovePIDController::Command::Command(CommandType t) : type(t)
{ }

LovePIDController::Command::Command(CommandType t, Util::ByteArray p) : type(t),
                                                                        packet(std::move(p))
{ }

LovePIDController::Command::Command(const Command &other) : type(other.type), packet(other.packet)
{ }

LovePIDController::Command &LovePIDController::Command::operator=(const Command &other)
{
    if (&other == this)
        return *this;
    type = other.type;
    packet = other.packet;
    return *this;
}

Variant::Root LovePIDController::Command::stateDescription() const
{
    Variant::Root result;
    result["Payload"].setBinary(packet);
    switch (type) {
    case COMMAND_IGNORED:
        result["Type"].setString("Ignored");
        break;
    case COMMAND_READ_STATUS:
        result["Type"].setString("ReadStatus");
        break;
    case COMMAND_READ_ERRORSTATUS:
        result["Type"].setString("ReadErrorStatus");
        break;
    case COMMAND_READ_SP1:
        result["Type"].setString("ReadSetpoint");
        break;
    case COMMAND_WRITE_SP1:
        result["Type"].setString("WriteSetpoint");
        break;
    case COMMAND_READ_MANUAL_SP1:
        result["Type"].setString("ReadManualSetpoint");
        break;
    case COMMAND_WRITE_MANAUL_SP1:
        result["Type"].setString("WriteManualSetpoint");
        break;
    case COMMAND_READ_OUTPUT_PERCENTAGE:
        result["Type"].setString("ReadOutputPercentage");
        break;
    case COMMAND_READ_DECIMAL_POINT:
        result["Type"].setString("ReadDecimalPoint");
        break;
    case COMMAND_WRITE_DECIMAL_POINT:
        result["Type"].setString("WriteDecimalPoint");
        break;
    case COMMAND_READ_EEPROM_WRITE:
        result["Type"].setString("ReadEEPROMWriteState");
        break;
    case COMMAND_DISABLE_EEPROM_WRITE:
        result["Type"].setString("DisableEEPROMWrites");
        break;
    case COMMAND_REMOTE_MODE:
        result["Type"].setString("SetRemoteMode");
        break;
    case COMMAND_DISABLE_MANUAL:
        result["Type"].setString("DisableManualMode");
        break;
    case COMMAND_ENABLE_MANUAL:
        result["Type"].setString("EnableManualMode");
        break;
    case COMMAND_READ_CONTROLLER_DATA:
        result["Type"].setString("ReadControllerData");
        break;
    case COMMAND_READ_FIRMWARE_DATA:
        result["Type"].setString("ReadFirmwareData");
        break;
    case COMMAND_INVALID:
        result["Type"].setString("Invalid");
        break;
    }
    return result;
}

static std::string assembleLoggingCategory(const QLoggingCategory &parent, quint16 addr)
{
    std::stringstream stream;
    stream << parent.categoryName() << '.' << std::hex << addr;
    return stream.str();
}

LovePIDController::LovePIDController(BusManager::DeviceInterface *interface,
                                     AcquireLovePID *parent,
                                     quint16 addr) : BusManager::Device(interface),
                                                     pid(parent),
                                                     address(addr),
                                                     loggingName(
                                                             assembleLoggingCategory(parent->log,
                                                                                     addr)),
                                                     log(loggingName.data()),
                                                     commandQueue(),
                                                     autoprobeStatus(
                                                             AcquisitionInterface::AutoprobeStatus::InProgress),
                                                     responseState(RESP_PASSIVE_WAIT),
                                                     commandFailedCount(0),
                                                     responseTime(FP::undefined()),
                                                     exceptions(0),
                                                     failureReason(Failure_Timeout),
                                                     failureData(),
                                                     metadata(),
                                                     targetSetpoint(FP::undefined()),
                                                     decimalDigits(1),
                                                     manualMode(false),
                                                     interfaceResetPending(false)
{
    metadata["Manufacturer"].setString("Love");
    metadata["Model"].setString("PID");
    metadata["Revision"].setString(Environment::revision());
    metadata["Address"].setInteger(address);
}

LovePIDController::~LovePIDController() = default;

QString LovePIDController::describeResponseState() const
{
    switch (responseState) {
    case RESP_PASSIVE_RUN:
        return "PassiveRun";
    case RESP_PASSIVE_WAIT:
        return "PassiveWait";
    case RESP_INTERACTIVE_START:
        return "InteractiveStart";
    case RESP_INTERACTIVE_START_STATUS:
        return "InteractiveStartStatus";
    case RESP_INTERACTIVE_START_REMOTEMODE:
        return "InteractiveStartSetRemote";
    case RESP_INTERACTIVE_START_SETMANUAL:
        return "InteractiveStartSetManual";
    case RESP_INTERACTIVE_START_READ_EEPROM:
        return "InteractiveStartReadEEPROM";
    case RESP_INTERACTIVE_START_CLEAR_EEPROM:
        return "InteractiveStartClearEEPROM";
    case RESP_INTERACTIVE_START_READ_CONTROLLERDATA:
        return "InteractiveStartReadControllerData";
    case RESP_INTERACTIVE_START_READ_FIRMWAREDATA:
        return "InteractiveStartReadFirmwareData";
    case RESP_INTERACTIVE_START_READ_DECIMALS:
        return "InteractiveStartReadDecimalDigits";
    case RESP_INTERACTIVE_START_WRITE_DECIMALS:
        return "InteractiveStartWriteDecimalDigits";
    case RESP_INTERACTIVE_RUN_STATUS:
        return "InteractiveRunStatus";
    case RESP_INTERACTIVE_RUN_READ_ERROR:
        return "InteractiveRunReadError";
    case RESP_INTERACTIVE_RUN_READ_SP:
        return "InteractiveRunReadSetpoint";
    case RESP_INTERACTIVE_RUN_WRITE_SP:
        return "InteractiveRunWriteSetpoint";
    case RESP_INTERACTIVE_RUN_READ_OUTPUT_PERCENTAGE:
        return "InteractiveRunReadOutputPercentage";
    case RESP_INTERACTIVE_RUN_WAIT:
        return "InteractiveRunWait";
    case RESP_INTERACTIVE_RESTART_WAIT:
        return "InteractiveRunRestartWait";
    }
    return "Unknown";
}

QString LovePIDController::realtimeState() const
{
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RUN_STATUS:
    case RESP_INTERACTIVE_RUN_READ_ERROR:
    case RESP_INTERACTIVE_RUN_READ_SP:
    case RESP_INTERACTIVE_RUN_WRITE_SP:
    case RESP_INTERACTIVE_RUN_READ_OUTPUT_PERCENTAGE:
    case RESP_INTERACTIVE_RUN_WAIT:
        if (exceptions == 0)
            return "Run";
        return QString("Exception: %1").arg(QString::number(exceptions, 16).rightJustified(5, '0'));
    case RESP_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
        switch (failureReason) {
        case Failure_Timeout:
            return "Timeout";
        case Failure_ErrorPacket:
            return QString("NO COMMS: Controller error %1").arg(
                    QString::number(failureData["ErrorCode"].toInt64(), 16).rightJustified(2, '0'));
        case Failure_ParseError:
            return QString("NO COMMS: Parse error %1").arg(
                    QString::number(failureData["Code"].toInt64()));
        }
        return QString();
    default:
        return describeResponseState();
    }
    return QString();
}

bool LovePIDController::atTargetSetpoint(double sp) const
{
    if (!FP::defined(targetSetpoint) || !FP::defined(sp))
        return true;

    Q_ASSERT(decimalDigits >= 0 && decimalDigits <= 3);
    double effectiveTarget = targetSetpoint;
    if (sp < 0.0) {
        if (targetSetpoint > 0.0)
            return false;
        sp = -sp;
        effectiveTarget = -effectiveTarget;
    } else if (effectiveTarget < 0.0) {
        return false;
    }

    static const double scale[4] = {1, 10, 100, 1000};
    int ti = qRound(effectiveTarget * scale[decimalDigits]);
    int si = qRound(sp * scale[decimalDigits]);
    return ti == si;
}

static bool atManualSetpoint(double sp, double targetSetpoint)
{
    if (!FP::defined(targetSetpoint) || !FP::defined(sp))
        return true;
    if (targetSetpoint <= 0.0 && sp <= 0.0)
        return true;
    if (targetSetpoint >= 100.0 && sp >= 100.0)
        return true;
    int ti = qRound(targetSetpoint * 10.0);
    int si = qRound(sp * 10.0);
    return ti == si;
}

void LovePIDController::responseError(const Util::ByteView &payload,
                                      double frameTime,
                                      int code,
                                      const Command &command)
{
    qCDebug(log) << "Error code" << code << "parsing response to command" << command.getType()
                 << "received by controller in state" << responseState << "at"
                 << Logging::time(frameTime);

    failureReason = Failure_ParseError;

    failureData.write().setEmpty();
    failureData["Code"].setInt64(code);
    failureData["ResponseTime"].setDouble(frameTime);
    failureData["ResponseState"].setString(describeResponseState());
    failureData["ResponseData"].setBinary(payload);
    failureData["Command"].set(command.stateDescription());
}

void LovePIDController::incomingTimeout(double currentTime)
{
    interface()->clearResponseQueue();
    interface()->unlockBus();

    failureReason = Failure_Timeout;
    failureData.write().setEmpty();
    failureData["TimeoutTime"].setDouble(currentTime);
    failureData["ResponseState"].setString(describeResponseState());

    if (!commandQueue.isEmpty()) {
        failureData["Command"].set(commandQueue.takeFirst().stateDescription());
    }
    commandQueue.clear();

    switch (responseState) {
    case RESP_PASSIVE_RUN:
        responseState = RESP_PASSIVE_WAIT;
        pid->controllerCommunicationsLost(currentTime, this, QObject::tr(
                "Timeout waiting for passive response on controller %1.  Communications Dropped.").arg(
                QString::number(address, 16)), Variant::Root(failureData).write());

        qCDebug(log) << "Controller timeout waiting for response in passive run mode at"
                     << Logging::time(currentTime);
        return;

    case RESP_INTERACTIVE_START:
    case RESP_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_RUN_WAIT:
        return;

    default:
        qCDebug(log) << "Controller timeout waiting for response in state" << responseState << "at"
                     << Logging::time(currentTime);
        break;
    }
}

void LovePIDController::incomingErrorFrame(int errorCode, double frameTime)
{
    interface()->receivedResponse();
    interface()->timeoutAt(FP::undefined());

    failureReason = Failure_ErrorPacket;
    failureData.write().setEmpty();
    failureData["ErrorCode"].setInt64(errorCode);
    failureData["ErrorTime"].setDouble(frameTime);
    failureData["ResponseState"].setString(describeResponseState());

    if (!commandQueue.isEmpty()) {
        failureData["Command"].set(commandQueue.takeFirst().stateDescription());
    }

    switch (errorCode) {
    case 0x01:
    case 0x06:
    case 0x10:
        failureData["ErrorDescription"].setString(QObject::tr("Undefined command"));
        break;
    case 0x02:
        failureData["ErrorDescription"].setString(QObject::tr("Checksum error"));
        break;
    case 0x03:
        failureData["ErrorDescription"].setString(QObject::tr("Command not performed"));
        break;
    case 0x04:
        failureData["ErrorDescription"].setString(QObject::tr("Illegal character"));
        break;
    case 0x05:
        failureData["ErrorDescription"].setString(QObject::tr("Data field error"));
        break;
    case 0x08:
    case 0x09:
        failureData["ErrorDescription"].setString(QObject::tr("Hardware fault"));
        break;
    }

    /*switch (responseState) {
    case RESP_PASSIVE_RUN: {
        responseState = RESP_PASSIVE_WAIT;
        pid->controllerCommunicationsLost(frameTime, this, QObject::tr(
                "Error code %2 received in passive state by controller %1.  Communications Dropped.")
                .arg(QString::number(address, 16))
                .arg(errorCode), Variant::Root(failureData).write());

        qCDebug(log) << "Controller timeout waiting for response in passive run mode at "
                     << Logging::time(frameTime);
        return;
    }
    case RESP_INTERACTIVE_START:
    case RESP_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
        return;

    default:
        qCDebug(log) << "Controller received error" << errorCode << "in state" << responseState
                     << "at" << Logging::time(frameTime);
        break;
    }*/
}

void LovePIDController::outOfSequenceResponse(const Util::ByteView &payload,
                                              double frameTime,
                                              const Command &command)
{

    switch (responseState) {
    case RESP_PASSIVE_RUN:
        responseState = RESP_PASSIVE_WAIT;
        break;
    case RESP_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_START:
        return;

    case RESP_INTERACTIVE_START_STATUS:
    case RESP_INTERACTIVE_START_REMOTEMODE:
    case RESP_INTERACTIVE_START_SETMANUAL:
    case RESP_INTERACTIVE_START_READ_EEPROM:
    case RESP_INTERACTIVE_START_CLEAR_EEPROM:
    case RESP_INTERACTIVE_START_READ_CONTROLLERDATA:
    case RESP_INTERACTIVE_START_READ_FIRMWAREDATA:
    case RESP_INTERACTIVE_START_READ_DECIMALS:
    case RESP_INTERACTIVE_START_WRITE_DECIMALS:
        break;

    case RESP_INTERACTIVE_RUN_STATUS:
    case RESP_INTERACTIVE_RUN_READ_ERROR:
    case RESP_INTERACTIVE_RUN_READ_SP:
    case RESP_INTERACTIVE_RUN_WRITE_SP:
    case RESP_INTERACTIVE_RUN_READ_OUTPUT_PERCENTAGE:
    case RESP_INTERACTIVE_RUN_WAIT: {
        qCDebug(log) << "Out of sequence response in state" << responseState << "to command"
                     << command.getType() << "on controller, communications dropped";

        Variant::Write info = Variant::Write::empty();
        info.hash("ResponseState").setString(describeResponseState());
        info.hash("Command").set(command.stateDescription());
        info.hash("Response").setBinary(payload);

        responseState = RESP_INTERACTIVE_START;

        pid->controllerCommunicationsLost(frameTime, this, QObject::tr(
                "Out of sequence response on controller %1.  Communications Dropped.").arg(
                QString::number(address, 16)), std::move(info));

        interface()->timeoutAt(frameTime);
        break;
    }
    }

}

void LovePIDController::commandRetryExceeded(double currentTime)
{
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_START:
    case RESP_INTERACTIVE_RUN_WAIT:
        return;

    case RESP_INTERACTIVE_START_STATUS:
    case RESP_INTERACTIVE_START_REMOTEMODE:
    case RESP_INTERACTIVE_START_SETMANUAL:
    case RESP_INTERACTIVE_START_READ_EEPROM:
    case RESP_INTERACTIVE_START_CLEAR_EEPROM:
    case RESP_INTERACTIVE_START_READ_CONTROLLERDATA:
    case RESP_INTERACTIVE_START_READ_FIRMWAREDATA:
    case RESP_INTERACTIVE_START_READ_DECIMALS:
    case RESP_INTERACTIVE_START_WRITE_DECIMALS:
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        pid->controllerAutoprobeStatusChanged(this);

        switch (failureReason) {
        case Failure_ErrorPacket:
            qCDebug(log) << "Start communications failed on controller with error code"
                         << failureData.read().hash("ErrorCode").toInt64() << "at"
                         << Logging::time(currentTime) << "in response state" << responseState;
            break;
        case Failure_Timeout:
            qCDebug(log) << "Timeout during start communications on controller at"
                         << Logging::time(currentTime) << "in response state" << responseState;
            break;
        case Failure_ParseError:
            qCDebug(log) << "Parsing error code" << failureData.read().hash("Code").toInt64()
                         << "during start communications on controller at"
                         << Logging::time(currentTime) << " in response state " << responseState;
            break;
        }

        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        responseTime = currentTime + 10.0;

        interface()->timeoutAt(responseTime);
        pid->controllerStatusChanged(currentTime, this);
        break;

    case RESP_INTERACTIVE_RUN_STATUS:
    case RESP_INTERACTIVE_RUN_READ_ERROR:
    case RESP_INTERACTIVE_RUN_READ_SP:
    case RESP_INTERACTIVE_RUN_WRITE_SP:
    case RESP_INTERACTIVE_RUN_READ_OUTPUT_PERCENTAGE:

        switch (failureReason) {
        case Failure_ErrorPacket:
            qCDebug(log) << "Controller received error "
                         << failureData.read().hash("ErrorCode").toInt64()
                         << "at" << Logging::time(currentTime) << "in state" << responseState
                         << ", communications dropped";

            responseState = RESP_INTERACTIVE_START;
            pid->controllerCommunicationsLost(currentTime, this, QObject::tr(
                    "Error code %2 received on controller %1.  Communications Dropped.").arg(QString::number(
                                                                                                address,
                                                                                                16))
                                                                                                                          .arg(QString::number(
                                                                                                                                  failureData
                                                                                                                                          .read()
                                                                                                                                          .hash("ErrorCode")
                                                                                                                                          .toInt64(),
                                                                                                                                  16)),
                                              Variant::Root(failureData).write());
            break;
        case Failure_Timeout:
            qCDebug(log) << "Timeout waiting for response on controller at"
                         << Logging::time(currentTime) << "in state" << responseState
                         << ", communications dropped";

            responseState = RESP_INTERACTIVE_START;
            pid->controllerCommunicationsLost(currentTime, this, QObject::tr(
                    "Timeout on controller %1.  Communications Dropped.").arg(
                    QString::number(address, 16)), Variant::Root(failureData).write());
            break;
        case Failure_ParseError:
            qCDebug(log) << "Parsing error code" << failureData.read().hash("Code").toInt64()
                         << " on controller at" << Logging::time(currentTime) << "in state"
                         << responseState << ", communications dropped";

            responseState = RESP_INTERACTIVE_START;
            pid->controllerCommunicationsLost(currentTime, this, QObject::tr(
                    "Parsing error code %2 received on controller %1.  Communications Dropped.").arg(QString::number(
                                                                                                        address,
                                                                                                        16))
                                                                                                                                  .arg(QString::number(
                                                                                                                                          failureData
                                                                                                                                                  .read()
                                                                                                                                                  .hash("Code")
                                                                                                                                                  .toInt64(),
                                                                                                                                          16)),
                                              Variant::Root(failureData).write());
            break;
        }

        responseState = RESP_INTERACTIVE_START;
        interface()->timeoutAt(currentTime);
        break;
    }
}

void LovePIDController::incomingCorruptDataFrame(const Util::ByteView &payload,
                                                 int code,
                                                 double frameTime)
{
    interface()->receivedResponse();
    interface()->timeoutAt(FP::undefined());

    if (commandQueue.isEmpty()) {
        outOfSequenceResponse(payload, frameTime, Command(COMMAND_INVALID));
        return;
    }

    responseError(payload, frameTime, code, commandQueue.takeFirst());
}

static bool parseHexDigit(char c, quint8 &output)
{
    if (c >= '0' && c <= '9')
        output = (quint8) (c - '0');
    else if (c >= 'A' && c <= 'F')
        output = (quint8) (c - 'A') + 0xA;
    else if (c >= 'a' && c <= 'f')
        output = (quint8) (c - 'a') + 0xA;
    else
        return false;
    return true;
}

static bool parseHexNumber(const char *begin, quint8 &output)
{
    if (!parseHexDigit(*begin, output))
        return false;
    quint8 add;
    ++begin;
    if (!parseHexDigit(*begin, add))
        return false;
    output <<= 4;
    output |= add;
    return true;
}

static bool parseDecimalNumber(const char *begin, int digits, int &output)
{
    output = 0;
    for (const char *end = begin + digits; begin != end; ++begin) {
        char c = *begin;
        if (c >= '0' && c <= '9') {
            output *= 10;
            output += c - '0';
        } else {
            return false;
        }
    }
    return true;
}

void LovePIDController::incomingDataFrame(const Util::ByteView &payload, double frameTime)
{
    interface()->receivedResponse();
    interface()->timeoutAt(FP::undefined());

    if (commandQueue.isEmpty()) {
        outOfSequenceResponse(payload, frameTime, Command(COMMAND_INVALID));
        return;
    }
    Command command(commandQueue.takeFirst());

    switch (command.getType()) {
    case COMMAND_IGNORED:
        switch (responseState) {
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_RUN:
            break;

        default:
            outOfSequenceResponse(payload, frameTime, command);
            break;
        }
        break;

    case COMMAND_INVALID:
        break;

    case COMMAND_READ_STATUS: {
        if (payload.size() != 8) {
            responseError(payload, frameTime, 1000, command);
            return;
        }
        quint8 status1 = 0;
        if (!parseHexNumber(payload.data<const char *>(), status1)) {
            responseError(payload, frameTime, 1001, command);
            return;
        }
        quint8 status2 = 0;
        if (!parseHexNumber(payload.data<const char *>(2), status2)) {
            responseError(payload, frameTime, 1002, command);
            return;
        }
        int raw = -1;
        if (!parseDecimalNumber(payload.data<const char *>(4), 4, raw)) {
            responseError(payload, frameTime, 1003, command);
            return;
        }

        manualMode = status1 & 0x80;
        bool remoteMode = status1 & 0x40;
        bool hasError = status1 & 0x10;
        bool alarm1Active = status1 & 0x08;
        bool alarm2Active = status1 & 0x04;
        //int selectedSetpoint = status1 & 0x3;
        bool activityTimeout = status2 & 0x80;
        int decimalPoint = (status2 >> 4) & 0x3;
        //int engineeringUnits = (status2 >> 1) & 0x3;
        bool valueSign = status2 & 0x1;

        static const double digitScale[4] = {1.0, 0.1, 0.01, 0.001};
        double value = (double) raw * digitScale[decimalPoint];
        if (valueSign)
            value = -value;

        quint32 oldExceptions = exceptions;
        if (alarm1Active)
            exceptions |= Exception_Alarm1;
        else
            exceptions &= ~Exception_Alarm1;
        if (alarm2Active)
            exceptions |= Exception_Alarm2;
        else
            exceptions &= ~Exception_Alarm2;
        if (activityTimeout)
            exceptions |= Exception_ActivityTimeout;
        else
            exceptions &= ~Exception_ActivityTimeout;

        pid->controllerProcessValue(frameTime, this, value);

        if (oldExceptions != exceptions)
            pid->controllerStatusChanged(frameTime, this);

        switch (responseState) {
        case RESP_PASSIVE_WAIT:
            responseState = RESP_PASSIVE_RUN;
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            pid->controllerAutoprobeStatusChanged(this);
            pid->controllerCommunicationsGained(frameTime, this);
            decimalDigits = decimalPoint;
            interfaceResetPending = false;

            qCDebug(log) << "Passive comms established with controller at"
                         << Logging::time(frameTime);
            break;

        case RESP_PASSIVE_RUN:
            if (decimalDigits != decimalPoint) {
                qCDebug(log) << "Decimal point changed from" << decimalDigits << "to"
                             << decimalPoint << "at" << Logging::time(frameTime);
                decimalDigits = decimalPoint;
            }
            break;

        case RESP_INTERACTIVE_START_STATUS:
            commandFailedCount = 0;
            if (!remoteMode) {
                responseState = RESP_INTERACTIVE_START_REMOTEMODE;
            } else {
                if (pid->controllerManualMode(this, manualMode)) {
                    responseState = RESP_INTERACTIVE_START_SETMANUAL;
                } else {
                    responseState = RESP_INTERACTIVE_START_READ_EEPROM;
                }
            }
            pid->controllerStatusChanged(frameTime, this);
            break;

        case RESP_INTERACTIVE_RUN_STATUS:
            commandFailedCount = 0;
            if (hasError)
                responseState = RESP_INTERACTIVE_RUN_READ_ERROR;
            else
                responseState = RESP_INTERACTIVE_RUN_READ_SP;

            if (decimalDigits != decimalPoint) {
                qCDebug(log) << "Decimal point changed from" << decimalDigits << "to"
                             << decimalPoint << "at" << Logging::time(frameTime);
                decimalDigits = decimalPoint;
            }
            break;

        default:
            outOfSequenceResponse(payload, frameTime, command);
            break;
        }
        break;
    }

    case COMMAND_READ_ERRORSTATUS: {
        if (payload.size() != 4) {
            responseError(payload, frameTime, 2000, command);
        }
        quint8 error1 = 0;
        if (!parseHexNumber(payload.data<const char *>(), error1)) {
            responseError(payload, frameTime, 2001, command);
            return;
        }
        quint8 error2 = 0;
        if (!parseHexNumber(payload.data<const char *>(2), error2)) {
            responseError(payload, frameTime, 2002, command);
            return;
        }

        quint32 oldExceptions = exceptions;

        exceptions &= ~Exception_ErrorMask;
        exceptions |= error2;
        exceptions |= (quint32) (error1) << 8;

        if (oldExceptions != exceptions)
            pid->controllerStatusChanged(frameTime, this);

        switch (responseState) {
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_RUN:
            break;

        case RESP_INTERACTIVE_RUN_READ_ERROR:
            commandFailedCount = 0;
            responseState = RESP_INTERACTIVE_RUN_READ_SP;
            break;

        default:
            outOfSequenceResponse(payload, frameTime, command);
            break;
        }

        break;
    }

    case COMMAND_READ_SP1:
    case COMMAND_READ_MANUAL_SP1: {
        if (payload.size() != 6) {
            responseError(payload, frameTime, 3000, command);
            return;
        }
        quint8 status = 0;
        if (!parseHexNumber(payload.data<const char *>(), status)) {
            responseError(payload, frameTime, 3001, command);
            return;
        }
        int raw = -1;
        if (!parseDecimalNumber(payload.data<const char *>(2), 4, raw)) {
            responseError(payload, frameTime, 3002, command);
            return;
        }

        int decimalPoint = (status >> 4) & 0x3;
        //int engineeringUnits = (status >> 1) & 0x3;
        bool valueSign = status & 0x1;

        static const double digitScale[4] = {1.0, 0.1, 0.01, 0.001};
        double value = (double) raw * digitScale[decimalPoint];
        if (!FP::defined(value)) {
            responseError(payload, frameTime, 3003, command);
            return;
        }
        if (valueSign)
            value = -value;

        pid->controllerSetpointValue(frameTime, this, value);

        switch (responseState) {
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_RUN:
            break;

        case RESP_INTERACTIVE_RUN_READ_SP:
            commandFailedCount = 0;
            if (manualMode) {
                if (command.getType() == COMMAND_READ_SP1)
                    responseState = RESP_INTERACTIVE_START;
                else if (!atManualSetpoint(value, targetSetpoint))
                    responseState = RESP_INTERACTIVE_RUN_WRITE_SP;
                else
                    responseState = RESP_INTERACTIVE_RUN_READ_OUTPUT_PERCENTAGE;
            } else {
                if (command.getType() == COMMAND_READ_MANUAL_SP1)
                    responseState = RESP_INTERACTIVE_START;
                else if (!atTargetSetpoint(value))
                    responseState = RESP_INTERACTIVE_RUN_WRITE_SP;
                else
                    responseState = RESP_INTERACTIVE_RUN_READ_OUTPUT_PERCENTAGE;
            }
            break;

        default:
            outOfSequenceResponse(payload, frameTime, command);
            break;
        }

        break;
    }

    case COMMAND_WRITE_SP1:
    case COMMAND_WRITE_MANAUL_SP1: {
        if (payload.size() != 2) {
            responseError(payload, frameTime, 4000, command);
            return;
        }
        if (payload != "00") {
            responseError(payload, frameTime, 4001, command);
            return;
        }

        switch (responseState) {
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_RUN:
            break;

        case RESP_INTERACTIVE_RUN_WRITE_SP:
            commandFailedCount = 0;
            if ((manualMode && command.getType() == COMMAND_WRITE_SP1) ||
                    (!manualMode && command.getType() == COMMAND_WRITE_MANAUL_SP1))
                responseState = RESP_INTERACTIVE_START;
            else
                responseState = RESP_INTERACTIVE_RUN_READ_OUTPUT_PERCENTAGE;
            break;

        default:
            outOfSequenceResponse(payload, frameTime, command);
            break;
        }
        break;
    }


    case COMMAND_READ_OUTPUT_PERCENTAGE: {
        if (payload.size() != 6) {
            responseError(payload, frameTime, 5000, command);
            return;
        }
        quint8 status = 0;
        if (!parseHexNumber(payload.data<const char *>(), status)) {
            responseError(payload, frameTime, 5001, command);
            return;
        }
        int raw = -1;
        if (!parseDecimalNumber(payload.data<const char *>(2), 4, raw)) {
            responseError(payload, frameTime, 5002, command);
            return;
        }

        int decimalPoint = (status >> 4) & 0x3;
        //int engineeringUnits = (status >> 1) & 0x3;
        bool valueSign = status & 0x1;

        static const double digitScale[4] = {1.0, 0.1, 0.01, 0.001};
        double value = (double) raw * digitScale[decimalPoint];
        if (!FP::defined(value)) {
            responseError(payload, frameTime, 5003, command);
            return;
        }
        if (valueSign)
            value = -value;

        pid->controllerOutputPercentage(frameTime, this, value);

        switch (responseState) {
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_RUN:
            break;

        case RESP_INTERACTIVE_RUN_READ_OUTPUT_PERCENTAGE: {
            commandFailedCount = 0;
            double interval = pid->config.first().pollInterval;
            if (FP::defined(interval) && interval > 0.0) {
                responseTime = interval + frameTime;
                responseState = RESP_INTERACTIVE_RUN_WAIT;
                interface()->timeoutAt(responseTime);
            } else {
                responseState = RESP_INTERACTIVE_RUN_STATUS;
            }
            break;
        }

        default:
            outOfSequenceResponse(payload, frameTime, command);
            break;
        }

        break;
    }

    case COMMAND_READ_DECIMAL_POINT: {
        if (payload.size() != 2) {
            responseError(payload, frameTime, 6000, command);
            return;
        }
        quint8 digits = 0;
        if (!parseHexNumber(payload.data<const char *>(), digits)) {
            responseError(payload, frameTime, 6001, command);
            return;
        }
        if (digits > 3) {
            responseError(payload, frameTime, 6002, command);
            return;
        }

        decimalDigits = digits;

        switch (responseState) {
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_RUN:
            break;

        case RESP_INTERACTIVE_START_READ_DECIMALS: {
            commandFailedCount = 0;
            if (pid->controllerDecimalDigits(this, decimalDigits)) {
                responseState = RESP_INTERACTIVE_START_WRITE_DECIMALS;
                pid->controllerStatusChanged(frameTime, this);
            } else {
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                responseState = RESP_INTERACTIVE_RUN_STATUS;
                interfaceResetPending = false;

                pid->controllerAutoprobeStatusChanged(this);
                pid->controllerCommunicationsGained(frameTime, this);

                qCDebug(log) << "Interactive comms succeeded on controller at"
                             << Logging::time(frameTime);
            }
            break;
        }

        default:
            outOfSequenceResponse(payload, frameTime, command);
            break;
        }
        break;
    }

    case COMMAND_WRITE_DECIMAL_POINT: {
        if (payload.size() != 2) {
            responseError(payload, frameTime, 7000, command);
            return;
        }
        if (payload != "00") {
            responseError(payload, frameTime, 7001, command);
            return;
        }

        switch (responseState) {
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_RUN:
            break;

        case RESP_INTERACTIVE_START_WRITE_DECIMALS:
            commandFailedCount = 0;
            responseState = RESP_INTERACTIVE_RUN_STATUS;
            interfaceResetPending = false;

            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            pid->controllerAutoprobeStatusChanged(this);
            pid->controllerCommunicationsGained(frameTime, this);

            qCDebug(log) << "Interactive comms succeeded on controller at"
                         << Logging::time(frameTime);
            break;

        default:
            outOfSequenceResponse(payload, frameTime, command);
            break;
        }
        break;
    }

    case COMMAND_READ_EEPROM_WRITE: {
        if (payload.size() < 1) {
            responseError(payload, frameTime, 8000, command);
            return;
        }

        if (payload[0] == '0' && (payload.size() == 1 || payload[1] == '0')) {
            switch (responseState) {
            case RESP_PASSIVE_WAIT:
            case RESP_PASSIVE_RUN:
                break;

            case RESP_INTERACTIVE_START_READ_EEPROM:
                responseState = RESP_INTERACTIVE_START_CLEAR_EEPROM;
                pid->controllerStatusChanged(frameTime, this);
                break;

            default:
                outOfSequenceResponse(payload, frameTime, command);
                break;
            }
        } else {
            switch (responseState) {
            case RESP_PASSIVE_WAIT:
            case RESP_PASSIVE_RUN:
                break;

            case RESP_INTERACTIVE_START_READ_EEPROM:
                commandFailedCount = 0;
                responseState = RESP_INTERACTIVE_START_READ_CONTROLLERDATA;
                pid->controllerStatusChanged(frameTime, this);
                break;

            default:
                outOfSequenceResponse(payload, frameTime, command);
                break;
            }
        }
        break;
    }

    case COMMAND_DISABLE_EEPROM_WRITE: {
        if (payload.size() != 2) {
            responseError(payload, frameTime, 9000, command);
            return;
        }
        if (payload != "00") {
            responseError(payload, frameTime, 9001, command);
            return;
        }

        switch (responseState) {
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_RUN:
            break;

        case RESP_INTERACTIVE_START_CLEAR_EEPROM:
            commandFailedCount = 0;
            responseState = RESP_INTERACTIVE_START_READ_CONTROLLERDATA;
            pid->controllerStatusChanged(frameTime, this);
            break;

        default:
            outOfSequenceResponse(payload, frameTime, command);
            break;
        }
        break;
    }

    case COMMAND_REMOTE_MODE: {
        if (payload.size() != 2) {
            responseError(payload, frameTime, 10000, command);
            return;
        }
        if (payload != "00") {
            responseError(payload, frameTime, 10001, command);
            return;
        }

        switch (responseState) {
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_RUN:
            break;

        case RESP_INTERACTIVE_START_REMOTEMODE:
            commandFailedCount = 0;
            if (pid->controllerManualMode(this, manualMode)) {
                responseState = RESP_INTERACTIVE_START_SETMANUAL;
            } else {
                responseState = RESP_INTERACTIVE_START_READ_EEPROM;
            }
            pid->controllerStatusChanged(frameTime, this);
            break;

        default:
            outOfSequenceResponse(payload, frameTime, command);
            break;
        }
        break;
    }

    case COMMAND_DISABLE_MANUAL:
    case COMMAND_ENABLE_MANUAL: {
        if (payload.size() != 2) {
            responseError(payload, frameTime, 11000, command);
            return;
        }
        if (payload != "00") {
            responseError(payload, frameTime, 11001, command);
            return;
        }

        switch (responseState) {
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_RUN:
            manualMode = (command.getType() == COMMAND_ENABLE_MANUAL);
            break;

        case RESP_INTERACTIVE_START_SETMANUAL:
            commandFailedCount = 0;
            responseState = RESP_INTERACTIVE_START_READ_EEPROM;
            pid->controllerStatusChanged(frameTime, this);
            break;

        default:
            outOfSequenceResponse(payload, frameTime, command);
            break;
        }
        break;
    }

    case COMMAND_READ_CONTROLLER_DATA: {
        std::size_t offset = 0;
        if (payload.string_start("LOVE"))
            offset += 4;
        if (payload.size() < offset + 4) {
            responseError(payload, frameTime, 12000, command);
            return;
        }

        int week = 0;
        if (!parseDecimalNumber(payload.data<const char *>(offset), 2, week)) {
            responseError(payload, frameTime, 12001, command);
            return;
        }
        int rawYear = 0;
        if (!parseDecimalNumber(payload.data<const char *>(offset + 2), 2, rawYear)) {
            responseError(payload, frameTime, 12002, command);
            return;
        }

        QDate currentDate(QDate::currentDate());
        int year;
        if (rawYear > (currentDate.year() % 100))
            year = (currentDate.year() - 100) / 100;
        else
            year = (currentDate.year() / 100) / 100;
        year = year * 100 + rawYear;

        QString model;
        if (payload.size() - offset - 4 > 2) {
            model = QString::fromLatin1(
                    payload.mid(offset + 5, std::max<std::size_t>(4, payload.size() - (offset + 5)))
                           .toQByteArrayRef());
            model = model.trimmed();
            for (;;) {
                if (model.isEmpty())
                    break;
                char c = model.at(model.size() - 1).toLatin1();
                if (c >= '0' && c <= '9')
                    break;
                if (c >= 'A' && c <= 'Z')
                    break;
                if (c >= 'a' && c <= 'z')
                    break;
                model.chop(1);
            }
            while (model.endsWith('0')) {
                model.chop(1);
            }
        }

        QString controllerID(payload.toQString(false).trimmed());
        for (;;) {
            if (controllerID.isEmpty())
                break;
            char c = controllerID.at(model.size() - 1).toLatin1();
            if (c >= '0' && c <= '9')
                break;
            if (c >= 'A' && c <= 'Z')
                break;
            if (c >= 'a' && c <= 'z')
                break;
            controllerID.chop(1);
        }

        Variant::Root oldControllerID(metadata["ControllerID"]);
        if (!controllerID.isEmpty())
            metadata["ControllerID"].setString(controllerID);

        Variant::Root oldModel(metadata["Model"]);
        if (!model.isEmpty())
            metadata["Model"].setString(model);

        Variant::Root oldSerial(metadata["SerialNumber"]);
        if (year > 1970 && week > 0 && week < 53) {
            QString str(QString::number(year).rightJustified(4, '0'));
            str.append('w');
            str.append(QString::number(week).rightJustified(2, '0'));
            metadata["SerialNumber"].setString(str);
        }

        if (oldModel.read() != metadata["Model"] ||
                oldSerial.read() != metadata["SerialNumber"] ||
                oldControllerID.read() != metadata["ControllerID"]) {
            pid->controllerMetaChanged(this);
        }

        switch (responseState) {
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_RUN:
            break;

        case RESP_INTERACTIVE_START_READ_CONTROLLERDATA:
            commandFailedCount = 0;
            responseState = RESP_INTERACTIVE_START_READ_FIRMWAREDATA;
            pid->controllerStatusChanged(frameTime, this);
            break;

        default:
            outOfSequenceResponse(payload, frameTime, command);
            break;
        }
        break;
    }

    case COMMAND_READ_FIRMWARE_DATA: {
        if (payload.size() < 2) {
            responseError(payload, frameTime, 13000, command);
            return;
        }

        QString firmware(payload.toQString(false).trimmed());
        for (;;) {
            if (firmware.isEmpty())
                break;
            char c = firmware.at(firmware.size() - 1).toLatin1();
            if (c >= '0' && c <= '9')
                break;
            if (c >= 'A' && c <= 'Z')
                break;
            if (c >= 'a' && c <= 'z')
                break;
            firmware.chop(1);
        }

        Variant::Root oldFirmware(metadata["FirmwareVersion"]);
        if (!firmware.isEmpty()) {
            metadata["FirmwareVersion"].setString(firmware);
        }

        if (oldFirmware.read() != metadata["FirmwareVersion"]) {
            pid->controllerMetaChanged(this);
        }

        switch (responseState) {
        case RESP_PASSIVE_WAIT:
        case RESP_PASSIVE_RUN:
            break;

        case RESP_INTERACTIVE_START_READ_FIRMWAREDATA:
            commandFailedCount = 0;
            responseState = RESP_INTERACTIVE_START_READ_DECIMALS;
            break;

        default:
            outOfSequenceResponse(payload, frameTime, command);
            break;
        }
        break;
    }
    }

    setAccepted();
}

void LovePIDController::incomingControlFrame(const Util::ByteView &payload, double frameTime)
{
    switch (responseState) {
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_START:
        return;

    default:
        break;
    }

    bool hadCommand = !commandQueue.isEmpty();
    if (payload == "00") {
        commandQueue.append(Command(COMMAND_READ_STATUS, payload));
    } else if (payload == "05") {
        commandQueue.append(Command(COMMAND_READ_ERRORSTATUS, payload));
    } else if (payload == "0101" || payload == "0100") {
        commandQueue.append(Command(COMMAND_READ_SP1, payload));
    } else if (payload.string_start("0200")) {
        commandQueue.append(Command(COMMAND_WRITE_SP1, payload));
    } else if (payload == "0153") {
        commandQueue.append(Command(COMMAND_READ_MANUAL_SP1, payload));
    } else if (payload.string_start("0266")) {
        commandQueue.append(Command(COMMAND_WRITE_MANAUL_SP1, payload));
    } else if (payload == "0156") {
        commandQueue.append(Command(COMMAND_READ_OUTPUT_PERCENTAGE, payload));
    } else if (payload == "031A") {
        commandQueue.append(Command(COMMAND_READ_DECIMAL_POINT, payload));
    } else if (payload.string_start("025C")) {
        commandQueue.append(Command(COMMAND_WRITE_DECIMAL_POINT, payload));
    } else if (payload == "032C") {
        commandQueue.append(Command(COMMAND_READ_EEPROM_WRITE, payload));
    } else if (payload == "0441") {
        commandQueue.append(Command(COMMAND_DISABLE_EEPROM_WRITE, payload));
    } else if (payload == "0400") {
        commandQueue.append(Command(COMMAND_REMOTE_MODE, payload));
    } else if (payload == "0408") {
        commandQueue.append(Command(COMMAND_DISABLE_MANUAL, payload));
    } else if (payload == "0409") {
        commandQueue.append(Command(COMMAND_ENABLE_MANUAL, payload));
    } else if (payload == "0700") {
        commandQueue.append(Command(COMMAND_READ_CONTROLLER_DATA, payload));
    } else if (payload == "0702") {
        commandQueue.append(Command(COMMAND_READ_FIRMWARE_DATA, payload));
    } else {
        commandQueue.append(Command(COMMAND_IGNORED, payload));
    }

    interface()->queueResponse();
    if (!hadCommand) {
        interface()->timeoutAt(frameTime + 4.0);
    }
}

void LovePIDController::issueCommand(const Command &command, double currentTime)
{
    bool hadCommand = !commandQueue.isEmpty();

    commandQueue.append(command);
    interface()->queueResponse();

    pid->writeControllerCommand(this, command.getPacket());

    if (!hadCommand)
        interface()->timeoutAt(currentTime + 4.0);
}

void LovePIDController::busControlAcquired(double currentTime)
{
    int retryMaximum = pid->config.first().retryTimes;

    switch (responseState) {
    case RESP_PASSIVE_RUN:
        break;
    case RESP_PASSIVE_WAIT:
        break;

    case RESP_INTERACTIVE_RESTART_WAIT:
        if (FP::defined(responseTime) && responseTime > currentTime) {
            break;
        }
        responseState = RESP_INTERACTIVE_START;
        pid->controllerStatusChanged(currentTime, this);
        /* Fall through */
    case RESP_INTERACTIVE_START:
        interface()->lockBus();
        interface()->timeoutAt(currentTime + 20.0);
        break;

    case RESP_INTERACTIVE_START_STATUS:
        if (commandFailedCount++ > retryMaximum) {
            commandRetryExceeded(currentTime);
            break;
        }
        issueCommand(Command(COMMAND_READ_STATUS, "00"), currentTime);
        break;

    case RESP_INTERACTIVE_START_REMOTEMODE:
        if (commandFailedCount++ > retryMaximum) {
            commandRetryExceeded(currentTime);
            break;
        }
        issueCommand(Command(COMMAND_REMOTE_MODE, "0400"), currentTime);
        break;

    case RESP_INTERACTIVE_START_SETMANUAL:
        if (commandFailedCount++ > retryMaximum) {
            commandRetryExceeded(currentTime);
            break;
        }
        if (manualMode) {
            issueCommand(Command(COMMAND_ENABLE_MANUAL, "0409"), currentTime);
        } else {
            issueCommand(Command(COMMAND_DISABLE_MANUAL, "0408"), currentTime);
        }
        break;

    case RESP_INTERACTIVE_START_READ_EEPROM:
        if (commandFailedCount++ > retryMaximum) {
            /* Failure here isn't actually fatal, since some controllers
             * just don't support it */
            responseState = RESP_INTERACTIVE_START_READ_CONTROLLERDATA;
            commandFailedCount = 1;
            issueCommand(Command(COMMAND_READ_CONTROLLER_DATA, "0700"), currentTime);
            pid->controllerStatusChanged(currentTime, this);
            break;
        }
        issueCommand(Command(COMMAND_READ_EEPROM_WRITE, "032C"), currentTime);
        break;

    case RESP_INTERACTIVE_START_CLEAR_EEPROM:
        if (commandFailedCount++ > retryMaximum) {
            /* Failure here isn't actually fatal, since some controllers
             * just don't support it */
            responseState = RESP_INTERACTIVE_START_READ_CONTROLLERDATA;
            commandFailedCount = 1;
            issueCommand(Command(COMMAND_READ_CONTROLLER_DATA, "0700"), currentTime);
            pid->controllerStatusChanged(currentTime, this);
            break;
        }
        issueCommand(Command(COMMAND_DISABLE_EEPROM_WRITE, "0441"), currentTime);
        break;

    case RESP_INTERACTIVE_START_READ_CONTROLLERDATA:
        if (commandFailedCount++ > retryMaximum) {
            commandRetryExceeded(currentTime);
            break;
        }
        issueCommand(Command(COMMAND_READ_CONTROLLER_DATA, "0700"), currentTime);
        break;

    case RESP_INTERACTIVE_START_READ_FIRMWAREDATA:
        if (commandFailedCount++ > retryMaximum) {
            commandRetryExceeded(currentTime);
            break;
        }
        issueCommand(Command(COMMAND_READ_FIRMWARE_DATA, "0702"), currentTime);
        break;

    case RESP_INTERACTIVE_START_READ_DECIMALS:
        if (commandFailedCount++ > retryMaximum) {
            commandRetryExceeded(currentTime);
            break;
        }
        issueCommand(Command(COMMAND_READ_DECIMAL_POINT, "031A"), currentTime);
        break;

    case RESP_INTERACTIVE_START_WRITE_DECIMALS:
        if (commandFailedCount++ > retryMaximum) {
            commandRetryExceeded(currentTime);
            break;
        }
        {
            Q_ASSERT(decimalDigits >= 0 && decimalDigits <= 4);
            Util::ByteArray data("025C000");
            data.push_back('0' + decimalDigits);
            data += "00";
            issueCommand(Command(COMMAND_WRITE_DECIMAL_POINT, std::move(data)), currentTime);
        }
        break;

    case RESP_INTERACTIVE_RUN_WAIT:
        if (FP::defined(responseTime) && responseTime > currentTime) {
            break;
        }
        commandFailedCount = 0;
        responseState = RESP_INTERACTIVE_RUN_STATUS;
        /* Fall through */
    case RESP_INTERACTIVE_RUN_STATUS:
        if (commandFailedCount++ > retryMaximum) {
            commandRetryExceeded(currentTime);
            break;
        }
        issueCommand(Command(COMMAND_READ_STATUS, "00"), currentTime);
        break;

    case RESP_INTERACTIVE_RUN_READ_ERROR:
        if (commandFailedCount++ > retryMaximum) {
            commandRetryExceeded(currentTime);
            break;
        }
        issueCommand(Command(COMMAND_READ_ERRORSTATUS, "05"), currentTime);
        break;

    case RESP_INTERACTIVE_RUN_READ_SP:
        if (commandFailedCount++ > retryMaximum) {
            commandRetryExceeded(currentTime);
            break;
        }
        if (manualMode) {
            issueCommand(Command(COMMAND_READ_MANUAL_SP1, "0153"), currentTime);
        } else {
            issueCommand(Command(COMMAND_READ_SP1, "0101"), currentTime);
        }
        break;

    case RESP_INTERACTIVE_RUN_WRITE_SP:
        if (commandFailedCount++ > retryMaximum) {
            commandRetryExceeded(currentTime);
            break;
        }
        if (!FP::defined(targetSetpoint)) {
            commandFailedCount = 1;
            responseState = RESP_INTERACTIVE_RUN_READ_OUTPUT_PERCENTAGE;
            issueCommand(Command(COMMAND_READ_OUTPUT_PERCENTAGE, "0156"), currentTime);
            break;
        }
        if (manualMode) {
            /* Manual mode write always uses 1 decimal digit */
            Util::ByteArray data("0266");

            int v = qRound(targetSetpoint * 10.0);
            if (v <= 0) {
                data += "0000";
            } else if (v >= 1000) {
                data += "1000";
            } else {
                data += QByteArray::number(v).rightJustified(4, '0');
            }
            data += "00";

            issueCommand(Command(COMMAND_WRITE_MANAUL_SP1, std::move(data)), currentTime);
        } else {
            Util::ByteArray data("0200");

            Q_ASSERT(decimalDigits >= 0 && decimalDigits <= 3);
            static const double scale[4] = {1, 10, 100, 1000};
            Util::ByteArray sign;
            double sp = targetSetpoint;
            if (sp < 0.0) {
                sign = "11";
                sp = -sp;
            } else {
                sign = "00";
            }
            int v = qRound(sp * scale[decimalDigits]);
            if (v <= 0) {
                data += "0000";
            } else if (v >= 9999) {
                data += "9999";
            } else {
                data += QByteArray::number(v).rightJustified(4, '0');
            }
            data += std::move(sign);

            issueCommand(Command(COMMAND_WRITE_SP1, std::move(data)), currentTime);
        }
        break;

    case RESP_INTERACTIVE_RUN_READ_OUTPUT_PERCENTAGE:
        if (commandFailedCount++ > retryMaximum) {
            commandRetryExceeded(currentTime);
            break;
        }
        issueCommand(Command(COMMAND_READ_OUTPUT_PERCENTAGE, "0156"), currentTime);
        break;
    }
}

void LovePIDController::applyExitSetpoint(double sp)
{
    Q_ASSERT(FP::defined(sp));

    if (manualMode) {
        /* Manual mode write always uses 1 decimal digit */
        Util::ByteArray data("0266");

        int v = qRound(sp * 10.0);
        if (v <= 0) {
            data += "0000";
        } else if (v >= 1000) {
            data += "1000";
        } else {
            data += QByteArray::number(v).rightJustified(4, '0');
        }
        data += "00";

        pid->writeControllerCommand(this, std::move(data));
    } else {
        Util::ByteArray data("0200");

        Q_ASSERT(decimalDigits >= 0 && decimalDigits <= 3);
        static const double scale[4] = {1, 10, 100, 1000};
        Util::ByteArray sign;
        if (sp < 0.0) {
            sign = "11";
            sp = -sp;
        } else {
            sign = "00";
        }
        int v = qRound(sp * scale[decimalDigits]);
        if (v <= 0) {
            data += "0000";
        } else if (v >= 9999) {
            data += "9999";
        } else {
            data += QByteArray::number(v).rightJustified(4, '0');
        }
        data += std::move(sign);

        pid->writeControllerCommand(this, std::move(data));
    }
}

void LovePIDController::lockAcquired(double currentTime)
{
    switch (responseState) {
    case RESP_INTERACTIVE_START:
        if (pid->controllerResetInterface(this)) {
            pid->discardData(currentTime + 2.0);
        } else {
            interfaceResetPending = true;
            pid->discardData(currentTime + 0.5);
        }
        pid->controllerStatusChanged(currentTime, this);
        break;
    default:
        qCDebug(log) << "Received lock acquire in unknown state" << responseState;
        interface()->unlockBus();
        break;
    }
}

void LovePIDController::discardDataCompleted(double time)
{
    switch (responseState) {
    case RESP_INTERACTIVE_START:
        interface()->clearResponseQueue();
        commandQueue.clear();
        commandFailedCount = 0;
        issueCommand(Command(COMMAND_READ_STATUS, "00"), time);
        responseState = RESP_INTERACTIVE_START_STATUS;
        interface()->unlockBus();
        pid->controllerStatusChanged(time, this);
        break;
    default:
        break;
    }
}

void LovePIDController::changeDecimalDigits(int digits)
{
    if (digits == decimalDigits)
        return;

    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_INTERACTIVE_START:
    case RESP_INTERACTIVE_START_STATUS:
    case RESP_INTERACTIVE_START_REMOTEMODE:
    case RESP_INTERACTIVE_START_SETMANUAL:
    case RESP_INTERACTIVE_START_READ_EEPROM:
    case RESP_INTERACTIVE_START_CLEAR_EEPROM:
    case RESP_INTERACTIVE_START_READ_CONTROLLERDATA:
    case RESP_INTERACTIVE_START_READ_FIRMWAREDATA:
    case RESP_INTERACTIVE_START_READ_DECIMALS:
    case RESP_INTERACTIVE_RESTART_WAIT:
        break;
    case RESP_INTERACTIVE_START_WRITE_DECIMALS:
    case RESP_INTERACTIVE_RUN_STATUS:
    case RESP_INTERACTIVE_RUN_READ_ERROR:
    case RESP_INTERACTIVE_RUN_READ_SP:
    case RESP_INTERACTIVE_RUN_WRITE_SP:
    case RESP_INTERACTIVE_RUN_READ_OUTPUT_PERCENTAGE:
    case RESP_INTERACTIVE_RUN_WAIT:
        qCDebug(log) << "Restarting communications from state" << responseState
                     << "due to decimal digits change to" << digits;
        responseState = RESP_INTERACTIVE_START;
        break;
    }
}

void LovePIDController::changeManualMode(bool mode)
{
    if (mode == manualMode)
        return;

    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_INTERACTIVE_START:
    case RESP_INTERACTIVE_START_STATUS:
    case RESP_INTERACTIVE_START_REMOTEMODE:
    case RESP_INTERACTIVE_START_SETMANUAL:
        break;
    case RESP_INTERACTIVE_START_READ_EEPROM:
    case RESP_INTERACTIVE_START_CLEAR_EEPROM:
    case RESP_INTERACTIVE_START_READ_CONTROLLERDATA:
    case RESP_INTERACTIVE_START_READ_FIRMWAREDATA:
    case RESP_INTERACTIVE_START_READ_DECIMALS:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_START_WRITE_DECIMALS:
    case RESP_INTERACTIVE_RUN_STATUS:
    case RESP_INTERACTIVE_RUN_READ_ERROR:
    case RESP_INTERACTIVE_RUN_READ_SP:
    case RESP_INTERACTIVE_RUN_WRITE_SP:
    case RESP_INTERACTIVE_RUN_READ_OUTPUT_PERCENTAGE:
    case RESP_INTERACTIVE_RUN_WAIT:
        qCDebug(log) << "Restarting communications from state" << responseState
                     << "due to manual mode update to" << mode;
        responseState = RESP_INTERACTIVE_START;
        break;
    }
}

void LovePIDController::autoprobeTryInteractive(double time)
{
    Q_UNUSED(time);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
        qCDebug(log) << "Controller starting interactive autoprobe from state" << responseState;
        responseState = RESP_INTERACTIVE_START;
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        break;
    case RESP_INTERACTIVE_START:
    case RESP_INTERACTIVE_START_STATUS:
    case RESP_INTERACTIVE_START_REMOTEMODE:
    case RESP_INTERACTIVE_START_SETMANUAL:
    case RESP_INTERACTIVE_START_READ_EEPROM:
    case RESP_INTERACTIVE_START_CLEAR_EEPROM:
    case RESP_INTERACTIVE_START_READ_CONTROLLERDATA:
    case RESP_INTERACTIVE_START_READ_FIRMWAREDATA:
    case RESP_INTERACTIVE_START_READ_DECIMALS:
    case RESP_INTERACTIVE_START_WRITE_DECIMALS:
    case RESP_INTERACTIVE_RESTART_WAIT:
        qCDebug(log) << "Controller interactive autoprobe requested from start communications state"
                     << responseState;
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        responseState = RESP_INTERACTIVE_START;
        break;
    case RESP_INTERACTIVE_RUN_STATUS:
    case RESP_INTERACTIVE_RUN_READ_ERROR:
    case RESP_INTERACTIVE_RUN_READ_SP:
    case RESP_INTERACTIVE_RUN_WRITE_SP:
    case RESP_INTERACTIVE_RUN_READ_OUTPUT_PERCENTAGE:
    case RESP_INTERACTIVE_RUN_WAIT:
        qCDebug(log) << "Controller interactive autoprobe requested from interactive state"
                     << responseState;
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        pid->controllerAutoprobeStatusChanged(this);
        break;
    }
}

void LovePIDController::autoprobeResetPassive(double time)
{
    qCDebug(log) << "Controller reset to passive autoprobe from state" << responseState;
    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    pid->controllerAutoprobeStatusChanged(this);

    responseState = RESP_PASSIVE_WAIT;
    responseTime = time + 10.0;
    commandQueue.clear();

    interface()->timeoutAt(responseTime);
    pid->controllerStatusChanged(time, this);
}

void LovePIDController::autoprobePromote(double time)
{
    Q_UNUSED(time);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
        qCDebug(log) << "Controller initiating interactive startup from passive state"
                     << responseState;
        responseState = RESP_INTERACTIVE_START;
        pid->updateControllerCommunications(this);
        break;

    case RESP_INTERACTIVE_START:
    case RESP_INTERACTIVE_START_STATUS:
    case RESP_INTERACTIVE_START_REMOTEMODE:
    case RESP_INTERACTIVE_START_SETMANUAL:
    case RESP_INTERACTIVE_START_READ_EEPROM:
    case RESP_INTERACTIVE_START_CLEAR_EEPROM:
    case RESP_INTERACTIVE_START_READ_CONTROLLERDATA:
    case RESP_INTERACTIVE_START_READ_FIRMWAREDATA:
    case RESP_INTERACTIVE_START_READ_DECIMALS:
    case RESP_INTERACTIVE_START_WRITE_DECIMALS:
        /* Already in progress, so no action needed */
        qCDebug(log)
            << "Controller promoted to interactive acquisition while in interactive startup state"
            << responseState;
        break;

    case RESP_INTERACTIVE_RESTART_WAIT:
        qCDebug(log) << "Controller initiating interactive startup from restart waiting state";
        responseState = RESP_INTERACTIVE_START;
        break;

    case RESP_INTERACTIVE_RUN_STATUS:
    case RESP_INTERACTIVE_RUN_READ_ERROR:
    case RESP_INTERACTIVE_RUN_READ_SP:
    case RESP_INTERACTIVE_RUN_WRITE_SP:
    case RESP_INTERACTIVE_RUN_READ_OUTPUT_PERCENTAGE:
    case RESP_INTERACTIVE_RUN_WAIT:
        qCDebug(log) << "Controller promoted to interactive acquisition while in interactive state"
                     << responseState;
        /* Let the PID master know we already have communications */
        pid->updateControllerCommunications(this);
        break;
    }
}

void LovePIDController::autoprobePromotePassive(double time)
{
    Q_UNUSED(time);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
        qCDebug(log) << "Controller promoted to passive acquisition from passive state"
                     << responseState;
        break;

    case RESP_INTERACTIVE_START:
    case RESP_INTERACTIVE_START_STATUS:
    case RESP_INTERACTIVE_START_REMOTEMODE:
    case RESP_INTERACTIVE_START_SETMANUAL:
    case RESP_INTERACTIVE_START_READ_EEPROM:
    case RESP_INTERACTIVE_START_CLEAR_EEPROM:
    case RESP_INTERACTIVE_START_READ_CONTROLLERDATA:
    case RESP_INTERACTIVE_START_READ_FIRMWAREDATA:
    case RESP_INTERACTIVE_START_READ_DECIMALS:
    case RESP_INTERACTIVE_START_WRITE_DECIMALS:
    case RESP_INTERACTIVE_RESTART_WAIT:
        qCDebug(log) << "Controller promoted to passive acquisition from interactive start state"
                     << responseState;
        responseState = RESP_PASSIVE_WAIT;
        break;

    case RESP_INTERACTIVE_RUN_STATUS:
    case RESP_INTERACTIVE_RUN_READ_ERROR:
    case RESP_INTERACTIVE_RUN_READ_SP:
    case RESP_INTERACTIVE_RUN_WRITE_SP:
    case RESP_INTERACTIVE_RUN_READ_OUTPUT_PERCENTAGE:
    case RESP_INTERACTIVE_RUN_WAIT:
        qCDebug(log) << "Controller promoted to passive acquisition from interactive state"
                     << responseState;
        responseState = RESP_PASSIVE_RUN;
        break;
    }
}


void LovePIDController::setToAutoprobe()
{
    responseState = RESP_PASSIVE_WAIT;
    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
}

void LovePIDController::setToInteractive()
{ responseState = RESP_INTERACTIVE_START; }

bool LovePIDController::hasCommunications() const
{
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RUN_STATUS:
    case RESP_INTERACTIVE_RUN_READ_ERROR:
    case RESP_INTERACTIVE_RUN_READ_SP:
    case RESP_INTERACTIVE_RUN_WRITE_SP:
    case RESP_INTERACTIVE_RUN_READ_OUTPUT_PERCENTAGE:
    case RESP_INTERACTIVE_RUN_WAIT:
        return true;

    default:
        return false;
    }
    return false;
}
