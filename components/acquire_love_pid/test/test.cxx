/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <map>
#include <cstdint>
#include <cmath>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

static const char idCharacters[4] = {'L', 'O', 'V', 'E'};

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;

    struct Controller {
        std::uint_fast16_t address;
        double setpoint;
        double process;
        double percent;
        bool manualMode;
        int decimals;
    };
    std::vector<Controller> controllers;

    static std::uint_fast8_t calculateChecksum(const char *begin, int n)
    {
        std::uint_fast32_t sum = 0;
        for (const std::uint8_t *add = reinterpret_cast<const std::uint8_t *>(begin),
                *end = reinterpret_cast<const std::uint8_t *>(begin) + n; add != end; ++add) {
            sum += *add;
        }
        return static_cast<std::uint_fast8_t>(sum & 0xFF);
    }

    static char toHexDigit(std::uint_fast8_t b)
    {
        if (b < 10)
            return b + '0';
        if (b < 16)
            return (b - 0xA) + 'A';
        return 'X';
    }

    static void addHexNumber(QByteArray &output, std::uint_fast8_t n)
    {
        output.append(toHexDigit((n >> 4) & 0xF));
        output.append(toHexDigit(n & 0xF));
    }

    static bool parseHexDigit(char c, std::uint_fast8_t &output)
    {
        if (c >= '0' && c <= '9')
            output = static_cast<std::uint_fast8_t>(c - '0');
        else if (c >= 'A' && c <= 'F')
            output = static_cast<std::uint_fast8_t>(c - 'A') + 0xA;
        else if (c >= 'a' && c <= 'f')
            output = static_cast<std::uint_fast8_t>(c - 'a') + 0xA;
        else
            return false;
        return true;
    }

    void sendResponse(std::uint_fast16_t addr, const QByteArray &payload)
    {
        Q_ASSERT(addr > 0x000 && addr <= 0x3FF);
        outgoing.append(static_cast<char>(0x02));
        int checksumBegin = outgoing.size();
        outgoing.append(idCharacters[(addr >> 8)]);
        addHexNumber(outgoing, addr & 0xFF);
        outgoing.append(payload);
        std::uint_fast8_t cs = calculateChecksum(outgoing.constBegin() + checksumBegin,
                                                 outgoing.size() - checksumBegin);
        addHexNumber(outgoing, cs);
        outgoing.append(static_cast<char>(0x06));
    }

    void sendError(std::uint_fast16_t addr, std::uint_fast8_t errorCode)
    {
        Q_ASSERT(addr > 0x000 && addr <= 0x3FF);
        outgoing.append(static_cast<char>(0x02));
        outgoing.append(idCharacters[(addr >> 8)]);
        addHexNumber(outgoing, addr & 0xFF);
        outgoing.append('N');
        addHexNumber(outgoing, errorCode);
        outgoing.append(static_cast<char>(0x06));
    }

    static QByteArray decimalPayload(int digits, double value)
    {
        Q_ASSERT(digits >= 0 && digits <= 3);
        QByteArray result;
        std::uint8_t bits;
        if (value < 0) {
            bits = 0x01;
            value = -value;
        } else {
            bits = 0;
        }
        bits |= static_cast<std::uint8_t>(digits << 4);
        addHexNumber(result, bits);

        double multiplier = std::pow(10.0, digits);
        int number = static_cast<int>(std::round(value * multiplier));
        Q_ASSERT(number >= 0 && number <= 9999);
        result.append(QByteArray::number(number).rightJustified(4, '0'));
        return result;
    }

    static QByteArray packageCommand(std::uint_fast16_t addr, const QByteArray &payload)
    {
        Q_ASSERT(addr > 0x000 && addr <= 0x3FF);
        QByteArray result;
        addHexNumber(result, addr & 0xFF);
        result.append(payload);
        quint8 cs = calculateChecksum(result.constBegin(), result.size());
        result.prepend(idCharacters[(addr >> 8)]);
        result.prepend(static_cast<char>(0x02));
        addHexNumber(result, cs);
        result.append(static_cast<char>(0x03));
        return result;
    }

    ModelInstrument() : incoming(), outgoing(), controllers()
    {
        Controller add;

        add.address = 0x31;
        add.setpoint = 50.0;
        add.process = 45.0;
        add.percent = 75.0;
        add.manualMode = false;
        add.decimals = 1;
        controllers.push_back(add);

        add.address = 0x32;
        add.setpoint = 51.0;
        add.process = 46.0;
        add.percent = 76.0;
        controllers.push_back(add);

        add.address = 0x33;
        add.setpoint = 52.0;
        add.process = 47.0;
        add.percent = 77.0;
        controllers.push_back(add);

        add.address = 0x34;
        add.setpoint = 53.0;
        add.process = 48.0;
        add.percent = 78.0;
        controllers.push_back(add);
    }

    void advance(double seconds)
    {
        Q_UNUSED(seconds);

        for (;;) {
            int idxCheck = incoming.indexOf((char) 0x02);
            if (idxCheck == -1) {
                if (incoming.size() > 256) {
                    incoming.remove(0, incoming.size() - 256);
                    break;
                }
            } else if (idxCheck != 0) {
                incoming.remove(0, idxCheck);
            }

            idxCheck = incoming.indexOf((char) 0x03, 1);
            if (idxCheck == -1)
                break;

            QByteArray packet(incoming.mid(1, idxCheck - 1));
            incoming.remove(0, 1);

            if (packet.size() < 5)
                continue;

            std::uint_fast16_t addr = 0xFFFF;
            switch (packet.at(0)) {
            case 'L':
                addr = 0;
                break;
            case 'O':
                addr = 0x100;
                break;
            case 'V':
                addr = 0x200;
                break;
            case 'E':
                addr = 0x300;
                break;
            default:
                break;
            }
            if (addr == 0xFFFF)
                continue;
            std::uint_fast8_t addrA = 0;
            if (!parseHexDigit(packet.at(1), addrA))
                continue;
            addr |= addrA << 4;
            if (!parseHexDigit(packet.at(2), addrA))
                continue;
            addr |= addrA;

            if (addr == 0x000 || addr == 0x100 || addr == 0x200 || addr == 0x300 || addr > 0x3FFF) {
                continue;
            }

            incoming.remove(0, idxCheck);

            auto controller = controllers.begin();
            for (auto end = controllers.end(); controller != end; ++controller) {
                if (controller->address == addr)
                    break;
            }
            if (controller == controllers.end())
                continue;

            std::uint_fast8_t cs = calculateChecksum(packet.constBegin() + 1, packet.size() - 3);
            if (toHexDigit((cs >> 4) & 0xF) != packet.at(packet.size() - 2) ||
                    toHexDigit(cs & 0xF) != packet.at(packet.size() - 1)) {
                sendError(addr, 0x03);
                continue;
            }

            QByteArray payload(packet.mid(3, packet.size() - 5));

            if (payload == "00") {
                QByteArray send;

                send.append(toHexDigit(controller->manualMode ? 0xC : 0x4));
                send.append('0');
                send.append(toHexDigit(controller->decimals));

                double value = controller->process;
                if (value < 0) {
                    send.append('1');
                    value = -value;
                } else {
                    send.append('0');
                }

                double multiplier = std::pow(10.0, controller->decimals);
                int number = static_cast<int>(std::round(value * multiplier));
                Q_ASSERT(number >= 0 && number <= 9999);
                send.append(QByteArray::number(number).rightJustified(4, '0'));

                sendResponse(addr, send);
            } else if (payload == "05") {
                sendResponse(addr, "0000000000");
            } else if (payload == "0101" || payload == "0153") {
                sendResponse(addr, decimalPayload(controller->decimals, controller->setpoint));
            } else if (payload.startsWith("0200")) {
                payload.remove(0, 4);
                if (payload.size() != 6) {
                    sendError(addr, 0x05);
                    continue;
                }

                bool ok = false;
                int number = payload.mid(0, 4).toInt(&ok, 10);
                if (!ok) {
                    sendError(addr, 0x05);
                    continue;
                }

                double value = number / pow(10.0, controller->decimals);
                if (payload.at(4) != '0' || payload.at(5) != '0') {
                    value = -value;
                }

                controller->setpoint = value;
                sendResponse(addr, "00");
            } else if (payload.startsWith("0266")) {
                payload.remove(0, 4);
                if (payload.size() != 6) {
                    sendError(addr, 0x05);
                    continue;
                }

                bool ok = false;
                int number = payload.mid(0, 4).toInt(&ok, 10);
                if (!ok) {
                    sendError(addr, 0x05);
                    continue;
                }

                double value = number / 10.0;
                if (payload.at(4) != '0' || payload.at(5) != '0') {
                    sendError(addr, 0x03);
                    continue;
                }
                if (value < 0.0 || value > 100.0) {
                    sendError(addr, 0x03);
                    continue;
                }

                controller->setpoint = value;
                sendResponse(addr, "00");
            } else if (payload == "0156") {
                sendResponse(addr, decimalPayload(controller->decimals, controller->percent));
            } else if (payload == "031A") {
                QByteArray send;
                addHexNumber(send, controller->decimals);
                sendResponse(addr, send);
            } else if (payload.startsWith("025C")) {
                if (payload.size() != 10) {
                    sendError(addr, 0x05);
                    continue;
                }
                if (payload.at(4) != '0' ||
                        payload.at(5) != '0' ||
                        payload.at(6) != '0' ||
                        payload.at(8) != '0' ||
                        payload.at(9) != '0') {
                    sendError(addr, 0x05);
                    continue;
                }
                std::uint_fast8_t n = 0;
                if (!parseHexDigit(payload.at(7), n)) {
                    sendError(addr, 0x04);
                    continue;
                }
                if (n > 4) {
                    sendError(addr, 0x05);
                    continue;
                }
                controller->decimals = n;
                sendResponse(addr, "00");
            } else if (payload == "0441" || payload == "0400" || payload == "032C") {
                sendResponse(addr, "00");
            } else if (payload == "0408") {
                controller->manualMode = false;
                sendResponse(addr, "00");
            } else if (payload == "0409") {
                controller->manualMode = true;
                sendResponse(addr, "00");
            } else if (payload == "0700") {
                sendResponse(addr, "LOVE4213M32A");
            } else if (payload == "0702") {
                sendResponse(addr, "501300");
            } else {
                sendError(addr, 0x01);
            }
        }
    }
};


class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream,
                   const ModelInstrument &model,
                   double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZINPUTS", Variant::Root((qint64) model.controllers.size()), "^Count",
                            time))
            return false;
        return true;
    }

    bool checkVariableMeta(StreamCapture &stream,
                           const SequenceName::Component &variable,
                           quint16 address,
                           double time = FP::undefined())
    {
        if (!stream.hasMeta(variable, Variant::Root((qint64) address), "^Address", time))
            return false;
        return true;
    }

    bool checkVariableMeta(StreamCapture &stream,
                           const std::map<SequenceName::Component, quint16> &variables,
                           double time = FP::undefined())
    {
        for (const auto &check : variables) {
            if (!checkVariableMeta(stream, check.first, check.second, time))
                return false;
        }
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream,
                           const ModelInstrument &model,
                           double time = FP::undefined())
    {
        for (const auto &controller : model.controllers) {
            auto idx = QString::number(controller.address, 16).toStdString();

            if (!stream.hasMeta("ZIN" + idx, Variant::Root(), QString(), time))
                return false;
            if (!stream.hasMeta("ZSP" + idx, Variant::Root(), QString(), time))
                return false;
            if (!stream.hasMeta("ZPCT" + idx, Variant::Root(), QString(), time))
                return false;
            if (!stream.hasMeta("ZST" + idx, Variant::Root(), QString(), time))
                return false;
        }
        return true;
    }

    bool checkRealtimeVariableMeta(StreamCapture &stream,
                                   const SequenceName::Component &variable,
                                   quint16 address,
                                   double time = FP::undefined())
    {
        if (!stream.hasMeta("ZIN" + variable, Variant::Root((qint64) address), "^Address", time))
            return false;
        if (!stream.hasMeta("ZSP" + variable, Variant::Root((qint64) address), "^Address", time))
            return false;
        if (!stream.hasMeta("ZPCT" + variable, Variant::Root((qint64) address), "^Address", time))
            return false;
        if (!stream.hasMeta("ZST" + variable, Variant::Root((qint64) address), "^Address", time))
            return false;
        return true;
    }

    bool checkRealtimeVariableMeta(StreamCapture &stream,
                                   const std::map<SequenceName::Component, quint16> &variables,
                                   double time = FP::undefined())
    {
        for (const auto &check : variables) {
            if (!checkRealtimeVariableMeta(stream, check.first, check.second, time))
                return false;
        }
        return true;
    }

    bool checkContiguous(StreamCapture &stream)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("ZINPUTS"))
            return false;
        return true;
    }

    bool checkVariableContiguous(StreamCapture &stream, const SequenceName::Component &variable)
    {
        if (!stream.checkContiguous(variable))
            return false;
        return true;
    }

    bool checkVariableContiguous(StreamCapture &stream,
                                 const std::map<SequenceName::Component, quint16> &variables)
    {
        for (const auto &check : variables) {
            if (!checkVariableContiguous(stream, check.first))
                return false;
        }
        return true;
    }

    bool checkValues(StreamCapture &stream,
                     const ModelInstrument &model,
                     double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        {
            std::map<quint16, double> values;
            for (const auto &controller : model.controllers) {
                values.emplace(controller.address, controller.process);
            }
            Variant::Write inputs = Variant::Write::empty();
            for (const auto &add : values) {
                inputs.toArray().after_back().setDouble(add.second);
            }
            if (!stream.hasAnyMatchingValue("ZINPUTS", inputs, time))
                return false;
        }
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream,
                             const ModelInstrument &model,
                             double time = FP::undefined(),
                             bool includeAuxiliary = true)
    {
        for (const auto &controller : model.controllers) {
            auto idx = QString::number(controller.address, 16).toStdString();

            if (!stream.hasAnyMatchingValue("ZIN" + idx, Variant::Root(controller.process), time))
                return false;
            if (!includeAuxiliary)
                continue;
            if (!stream.hasAnyMatchingValue("ZPCT" + idx, Variant::Root(controller.percent), time))
                return false;
            if (!stream.hasAnyMatchingValue("ZSP" + idx, Variant::Root(controller.setpoint), time))
                return false;
        }
        return true;
    }

    bool checkVariableValues(StreamCapture &stream,
                             const ModelInstrument &model,
                             const std::map<SequenceName::Component, quint16> &variables,
                             double time = FP::undefined())
    {
        for (const auto &check : variables) {
            auto controller = model.controllers.cbegin();
            for (auto end = model.controllers.cend(); controller != end; ++controller) {
                if (controller->address == check.second)
                    break;
            }
            if (controller == model.controllers.cend())
                return false;
            if (!stream.hasAnyMatchingValue(check.first, Variant::Root(controller->process), time))
                return false;
        }
        return true;
    }

    bool checkRealtimeVariableValues(StreamCapture &stream,
                                     const ModelInstrument &model,
                                     const std::map<SequenceName::Component, quint16> &variables,
                                     double time = FP::undefined(),
                                     bool includeAuxiliary = true)
    {
        for (const auto &check : variables) {
            auto controller = model.controllers.cbegin();
            for (auto end = model.controllers.cend(); controller != end; ++controller) {
                if (controller->address == check.second)
                    break;
            }
            if (controller == model.controllers.cend())
                return false;
            if (!stream.hasAnyMatchingValue("ZIN" + check.first, Variant::Root(controller->process),
                                            time))
                return false;
            if (!includeAuxiliary)
                continue;
            if (!stream.hasAnyMatchingValue("ZPCT" + check.first,
                                            Variant::Root(controller->percent), time))
                return false;
            if (!stream.hasAnyMatchingValue("ZSP" + check.first,
                                            Variant::Root(controller->setpoint), time))
                return false;
        }
        return true;
    }

    void configureVariables(Variant::Root &cv,
                            const std::map<SequenceName::Component, quint16> &variables)
    {
        for (const auto &check : variables) {
            cv["Variables"].hash(check.first).hash("Address").setInt64(check.second);
        }
    }

private slots:

    void initTestCase()
    {
        component =
                qobject_cast<AcquisitionComponent *>(ComponentLoader::create("acquire_love_pid"));
        QVERIFY(component);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        std::map<SequenceName::Component, quint16> variables{{"T_V11", 0x31},
                                                             {"U_V11", 0x32},};
        configureVariables(cv, variables);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        StreamCapture realtime;
        interface->setRealtimeEgress(&realtime);

        control.externalControl(ModelInstrument::packageCommand(0x31, "00"));
        control.externalControl(ModelInstrument::packageCommand(0x32, "00"));
        control.externalControl(ModelInstrument::packageCommand(0x33, "00"));
        control.externalControl(ModelInstrument::packageCommand(0x34, "00"));
        while (control.time() < 10.0) {
            control.advance(0.125);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 10.0);

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        control.externalControl(ModelInstrument::packageCommand(0x31, "00"));
        control.advance(0.125);
        control.externalControl(ModelInstrument::packageCommand(0x32, "00"));
        control.advance(0.125);
        control.externalControl(ModelInstrument::packageCommand(0x33, "00"));
        control.advance(0.125);
        control.externalControl(ModelInstrument::packageCommand(0x34, "00"));
        control.advance(0.125);
        control.externalControl(ModelInstrument::packageCommand(0x31, "00"));
        control.advance(0.125);
        control.externalControl(ModelInstrument::packageCommand(0x32, "00"));
        control.advance(0.125);
        control.externalControl(ModelInstrument::packageCommand(0x33, "00"));
        control.advance(0.125);
        control.externalControl(ModelInstrument::packageCommand(0x34, "00"));
        control.advance(0.125);
        control.externalControl(ModelInstrument::packageCommand(0x31, "00"));
        control.advance(0.125);
        control.externalControl(ModelInstrument::packageCommand(0x32, "00"));
        control.advance(0.125);
        control.externalControl(ModelInstrument::packageCommand(0x33, "00"));
        control.advance(0.125);
        control.externalControl(ModelInstrument::packageCommand(0x34, "00"));
        while (control.time() < 60.0) {
            control.advance(0.125);
            if (logging.hasAnyMatchingValue("F1", Variant::Root(), FP::undefined()))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);

        for (int i = 0; i < 30; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkVariableMeta(logging, variables));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));
        QVERIFY(checkVariableMeta(realtime, variables));
        QVERIFY(checkRealtimeVariableMeta(realtime, variables));

        QVERIFY(checkContiguous(logging));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument, FP::undefined(), false));

        QVERIFY(checkVariableValues(logging, instrument, variables));
        QVERIFY(checkVariableValues(realtime, instrument, variables));
        QVERIFY(checkRealtimeVariableValues(realtime, instrument, variables, FP::undefined(),
                                            false));


    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        std::map<SequenceName::Component, quint16> variables{{"T_V11", 0x31},
                                             {"U_V11", 0x32},};
        configureVariables(cv, variables);
        cv["PollInterval"].setDouble(0.0);
        cv["Controllers/#0/Address"].setInt64(0x31);
        cv["Controllers/#1/Address"].setInt64(0x32);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.controllers.pop_back();
        instrument.controllers.pop_back();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            if (logging.hasAnyMatchingValue("F1", Variant::Root(), FP::undefined()))
                break;
            control.advance(0.5);
            QTest::qSleep(50);
        }
        QVERIFY(logging.hasAnyMatchingValue("F1", Variant::Root(), FP::undefined()));
        while (elapsedTime.elapsed() < 30000) {
            if (realtime.hasValue("ZIN31", Variant::Root(), QString()) &&
                    realtime.hasValue("ZIN32", Variant::Root(), QString()) &&
                    realtime.hasValue("ZPCT31", Variant::Root(), QString()) &&
                    realtime.hasValue("ZPCT32", Variant::Root(), QString()))
                break;
            control.advance(0.5);
            QTest::qSleep(50);
        }

        for (int i = 0; i < 30; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkVariableMeta(logging, variables));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));
        QVERIFY(checkVariableMeta(realtime, variables));
        QVERIFY(checkRealtimeVariableMeta(realtime, variables));

        QVERIFY(checkContiguous(logging));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(checkVariableValues(logging, instrument, variables));
        QVERIFY(checkVariableValues(realtime, instrument, variables));
        QVERIFY(checkRealtimeVariableValues(realtime, instrument, variables));

    }

    void interactiveAutoprobeFailure()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["PollInterval"].setDouble(0.0);
        cv["Controllers/#0/Address"].setInt64(0x41);
        cv["Controllers/#1/Address"].setInt64(0x42);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.controllers.pop_back();
        instrument.controllers.pop_back();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Failure);
    }

    void interactiveRun()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        std::map<SequenceName::Component, quint16> variables{{"T_V11", 0x131},
                                             {"U_V11", 0x132},};
        configureVariables(cv, variables);
        cv["Controllers/#0/Address"].setInt64(0x131);
        cv["Controllers/#1/Address"].setInt64(0x132);
        cv["Controllers/#2/Address"].setInt64(0x33);
        cv["SetpointNames/OutputNameIDFoo"].setInt64(0x131);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.controllers.pop_back();
        instrument.controllers[0].address = 0x131;
        instrument.controllers[1].address = 0x132;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZST131", Variant::Root("Run")) &&
                    realtime.hasAnyMatchingValue("ZST132", Variant::Root("Run")) &&
                    realtime.hasAnyMatchingValue("ZST33", Variant::Root("Run")) &&
                    realtime.hasAnyMatchingValue("ZSTT_V11", Variant::Root("Run")) &&
                    realtime.hasAnyMatchingValue("ZSTU_V11", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZST131", Variant::Root("Run")));

        instrument.controllers[0].process = 20.0;
        instrument.controllers[1].percent = 99.0;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZIN131", Variant::Root(20.0)) &&
                    realtime.hasAnyMatchingValue("ZINT_V11", Variant::Root(20.0)) &&
                    realtime.hasAnyMatchingValue("ZPCT132", Variant::Root(99.0)) &&
                    realtime.hasAnyMatchingValue("ZPCTU_V11", Variant::Root(99.0)))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZIN131", Variant::Root(20.0)));
        QVERIFY(realtime.hasAnyMatchingValue("ZINT_V11", Variant::Root(20.0)));
        QVERIFY(realtime.hasAnyMatchingValue("ZPCT132", Variant::Root(99.0)));
        QVERIFY(realtime.hasAnyMatchingValue("ZPCTU_V11", Variant::Root(99.0)));

        Variant::Write cmd = Variant::Write::empty();
        cmd["ChangeSetpoints/Parameters/131/Value"].setDouble(10.0);
        cmd["ChangeSetpoints/Parameters/132/Value"].setDouble(11.0);
        interface->incomingCommand(cmd);
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZSP131", Variant::Root(10.0)) &&
                    realtime.hasAnyMatchingValue("ZSPT_V11", Variant::Root(10.0)) &&
                    realtime.hasAnyMatchingValue("ZSP132", Variant::Root(11.0)) &&
                    realtime.hasAnyMatchingValue("ZSPU_V11", Variant::Root(11.0)) &&
                    instrument.controllers[0].setpoint == 10.0 &&
                    instrument.controllers[1].setpoint == 11.0)
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZSP131", Variant::Root(10.0)));
        QVERIFY(realtime.hasAnyMatchingValue("ZSPT_V11", Variant::Root(10.0)));
        QVERIFY(realtime.hasAnyMatchingValue("ZSP132", Variant::Root(11.0)));
        QVERIFY(realtime.hasAnyMatchingValue("ZSPU_V11", Variant::Root(11.0)));
        QCOMPARE(instrument.controllers[0].setpoint, 10.0);
        QCOMPARE(instrument.controllers[1].setpoint, 11.0);

        cmd.setEmpty();
        cmd["Setpoint/Parameters/Address/Value"].setInt64(0x33);
        cmd["Setpoint/Parameters/Value/Value"].setDouble(12.0);
        interface->incomingCommand(cmd);
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZSP33", Variant::Root(12.0)) &&
                    instrument.controllers[2].setpoint == 12.0)
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZSP33", Variant::Root(12.0)));
        QCOMPARE(instrument.controllers[2].setpoint, 12.0);

        cmd.setEmpty();
        cmd["Output/Parameters/Name/Value"].setString("OutputNameIDFoo");
        cmd["Output/Parameters/Value/Value"].setDouble(13.0);
        interface->incomingCommand(cmd);
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZSP131", Variant::Root(13.0)) &&
                    instrument.controllers[0].setpoint == 13.0)
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZSP131", Variant::Root(13.0)));
        QCOMPARE(instrument.controllers[0].setpoint, 13.0);

        for (int i = 0; i < 30; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkVariableMeta(logging, variables));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));
        QVERIFY(checkVariableMeta(realtime, variables));
        QVERIFY(checkRealtimeVariableMeta(realtime, variables));

        QVERIFY(checkContiguous(logging));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(checkVariableValues(logging, instrument, variables));
        QVERIFY(checkVariableValues(realtime, instrument, variables));
        QVERIFY(checkRealtimeVariableValues(realtime, instrument, variables));

    }

    void passiveProvisional()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["PollInterval"].setDouble(0.0);
        cv["Controllers"].setType(Variant::Type::Array);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        StreamCapture realtime;
        StreamCapture logging;
        StreamCapture persistent;

        interface->setRealtimeEgress(&realtime);
        interface->setLoggingEgress(&logging);
        interface->setPersistentEgress(&persistent);

        control.externalControl(ModelInstrument::packageCommand(0x31, "00"));
        control.externalControl(ModelInstrument::packageCommand(0x32, "00"));
        control.externalControl(ModelInstrument::packageCommand(0x33, "00"));
        control.externalControl(ModelInstrument::packageCommand(0x34, "00"));

        control.externalControl(ModelInstrument::packageCommand(0x31, "00"));
        control.advance(0.125);
        control.externalControl(ModelInstrument::packageCommand(0x32, "00"));
        control.advance(0.125);
        control.externalControl(ModelInstrument::packageCommand(0x33, "00"));
        control.advance(0.125);
        control.externalControl(ModelInstrument::packageCommand(0x34, "00"));
        control.advance(0.125);
        control.externalControl(ModelInstrument::packageCommand(0x31, "00"));
        control.advance(0.125);
        control.externalControl(ModelInstrument::packageCommand(0x32, "00"));
        control.advance(0.125);
        control.externalControl(ModelInstrument::packageCommand(0x33, "00"));
        control.advance(0.125);
        control.externalControl(ModelInstrument::packageCommand(0x34, "00"));
        control.advance(0.125);
        control.externalControl(ModelInstrument::packageCommand(0x31, "00"));
        control.advance(0.125);
        control.externalControl(ModelInstrument::packageCommand(0x32, "00"));
        control.advance(0.125);
        control.externalControl(ModelInstrument::packageCommand(0x33, "00"));
        control.advance(0.125);
        control.externalControl(ModelInstrument::packageCommand(0x34, "00"));
        while (control.time() < 60.0) {
            control.advance(0.125);
            if (logging.hasAnyMatchingValue("F1", Variant::Root(), FP::undefined()))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);

        for (int i = 0; i < 30; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkContiguous(logging));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument, FP::undefined(), false));


    }

    void interactiveProvisional()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["PollInterval"].setDouble(0.0);
        cv["Controllers"].setType(Variant::Type::Array);
        cv["ProvisionalTimeout"].setDouble(10.0);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        ElapsedTimer elapsedTime;

        control.advance(0.125);
        QTest::qSleep(50);
        control.advance(0.125);
        QTest::qSleep(50);
        control.advance(0.125);
        QTest::qSleep(50);
        control.advance(0.125);

        Variant::Write cmd = Variant::Write::empty();
        cmd["Setpoint/Address"].setInt64(0x31);
        cmd["Setpoint/Value"].setDouble(10.0);
        interface->incomingCommand(cmd);

        cmd.setEmpty();
        cmd["Setpoint/Address"].setInt64(0x32);
        cmd["Setpoint/Value"].setDouble(11.0);
        interface->incomingCommand(cmd);

        cmd.setEmpty();
        cmd["Setpoint/Address"].setInt64(0x33);
        cmd["Setpoint/Value"].setDouble(12.0);
        interface->incomingCommand(cmd);

        cmd.setEmpty();
        cmd["Setpoint/Address"].setInt64(0x34);
        cmd["Setpoint/Value"].setDouble(13.0);
        interface->incomingCommand(cmd);

        cmd.setEmpty();
        cmd["Setpoint/Address"].setInt64(0x131);
        cmd["Setpoint/Value"].setDouble(30.0);
        interface->incomingCommand(cmd);

        double tExpire = control.time() + 11.0;

        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZST31", Variant::Root("Run")) &&
                    realtime.hasAnyMatchingValue("ZST32", Variant::Root("Run")) &&
                    realtime.hasAnyMatchingValue("ZST33", Variant::Root("Run")) &&
                    realtime.hasAnyMatchingValue("ZST34", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZST31", Variant::Root("Run")));

        while (control.time() < tExpire) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        for (int i = 0; i < 30; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkContiguous(logging));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

    void interactiveManualMode()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Controllers/#0/Address"].setInt64(0x31);
        cv["Controllers/#1/Address"].setInt64(0x32);
        cv["Controllers/#1/ManualMode"].setBool(true);
        cv["Controllers/#1/Setpoint"].setDouble(32.1);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.controllers.pop_back();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZST31", Variant::Root("Run")) &&
                    realtime.hasAnyMatchingValue("ZST32", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZST31", Variant::Root("Run")));

        for (int i = 0; i < 30; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        QVERIFY(!instrument.controllers[0].manualMode);
        QVERIFY(instrument.controllers[1].manualMode);
        QCOMPARE(instrument.controllers[1].setpoint, 32.1);

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
