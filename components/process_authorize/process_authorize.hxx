/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef PROCESSAUTHORIZE_H
#define PROCESSAUTHORIZE_H

#include "core/first.hxx"

#include <mutex>
#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QList>
#include <QStringList>
#include <QFile>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/stream.hxx"

class ProcessAuthorize : public CPD3::CPD3Action {
Q_OBJECT

    std::mutex mutex;
    bool terminated;

    std::string profile;
    std::vector<CPD3::Data::SequenceName::Component> stations;
    double authorizationStart;
    double authorizationEnd;
    CPD3::Data::Variant::Root certificate;
    QString templateName;
    bool moveAfterAuthorize;
    bool deauthorizeOld;

    QString digest;
    QString fileArgument;
    QString certificateArgument;

    bool testTerminated();

    void addAuthorization();

    void moveAuthorizedFile(const QString &fileName);

    struct FileInformation {
        double start;
        double end;
        QString digest;
        CPD3::Data::SequenceName::ComponentSet stations;
        std::vector<QString> files;

        void merge(FileInformation &&info);
    };

    bool inspectFile(const std::string &file, FileInformation &info);

    CPD3::Threading::Signal<> terminateRequested;

public:
    ProcessAuthorize(const CPD3::ComponentOptions &options,
                     const std::vector<CPD3::Data::SequenceName::Component> &setStations = {});

    virtual ~ProcessAuthorize();

    virtual void signalTerminate();

protected:
    virtual void run();
};

class ProcessAuthorizeComponent : public QObject, virtual public CPD3::ActionComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::ActionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.process_authorize"
                              FILE
                              "process_authorize.json")

public:
    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual int actionAllowStations();

    QString promptActionContinue
            (const CPD3::ComponentOptions &options = {},
             const std::vector<std::string> &stations = {}) override;

    CPD3::CPD3Action *createAction(const CPD3::ComponentOptions &options = {},
                                           const std::vector<std::string> &stations = {}) override;
};

#endif
