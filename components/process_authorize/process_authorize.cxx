/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QStringList>
#include <QFile>
#include <QDir>
#include <QSslCertificate>
#include <QSettings>
#include <QLoggingCategory>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "core/range.hxx"
#include "core/environment.hxx"
#include "core/textsubstitution.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/segment.hxx"
#include "transfer/package.hxx"
#include "transfer/datafile.hxx"
#include "transfer/upload.hxx"
#include "transfer/download.hxx"
#include "algorithms/cryptography.hxx"

#include "process_authorize.hxx"


Q_LOGGING_CATEGORY(log_component_process_authorize, "cpd3.component.process.authorize",
                   QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Transfer;
using namespace CPD3::Algorithms;


ProcessAuthorize::ProcessAuthorize(const ComponentOptions &options,
                                   const std::vector<SequenceName::Component> &setStations)
        : mutex(),
          terminated(false),
          profile(SequenceName::impliedProfile()),
          stations(setStations),
          authorizationStart(FP::undefined()),
          authorizationEnd(FP::undefined()),
          certificate(),
          templateName("Standard"),
          moveAfterAuthorize(true),
          deauthorizeOld(true)
{
    if (options.isSet("profile")) {
        profile = qobject_cast<ComponentOptionSingleString *>(options.get("profile"))->get()
                                                                                     .toLower()
                                                                                     .toStdString();
    }
    if (options.isSet("file")) {
        fileArgument = qobject_cast<ComponentOptionFile *>(options.get("file"))->get();
    }
    if (options.isSet("digest")) {
        digest =
                qobject_cast<ComponentOptionSingleString *>(options.get("digest"))->get().trimmed();
    }

    if (options.isSet("template")) {
        templateName = qobject_cast<ComponentOptionSingleString *>(options.get("template"))->get();
    }
    if (options.isSet("certificate")) {
        certificateArgument =
                qobject_cast<ComponentOptionFile *>(options.get("certificate"))->get();
    }

    if (options.isSet("move")) {
        moveAfterAuthorize = qobject_cast<ComponentOptionBoolean *>(options.get("move"))->get();
    }
    if (options.isSet("deauthorize")) {
        deauthorizeOld = qobject_cast<ComponentOptionBoolean *>(options.get("deauthorize"))->get();
    }
}

ProcessAuthorize::~ProcessAuthorize()
{
}

void ProcessAuthorize::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
    }
    terminateRequested();
}

bool ProcessAuthorize::testTerminated()
{
    std::lock_guard<std::mutex> lock(mutex);
    return terminated;
}

static std::string getImpliedStation()
{
    auto check = SequenceName::impliedStation();
    if (!check.empty())
        return check;
    return "nil";
}

static QStringList getUnauthorizedFiles(const SequenceName::Component &station,
                                        const std::string &profile,
                                        QDir *relativeDirectory = NULL)
{
    double tnow = Time::time();
    auto confList = ValueSegment::Stream::read(
            Archive::Selection(tnow - 1.0, tnow + 1.0, {station}, {"configuration"},
                               {"processing"}));
    auto f = Range::findIntersecting(confList, tnow);
    if (f == confList.end()) {
        return QStringList();
    }
    QString unauthorizedLocation(f->getValue()[QString("Profiles/%1/UnauthorizedLocation").arg(
            QString::fromStdString(profile))].toQString());
    if (unauthorizedLocation.isEmpty())
        return QStringList();


    {
        TextSubstitutionStack subs;
        subs.setString("station", QString::fromStdString(station).toLower());
        unauthorizedLocation = subs.apply(unauthorizedLocation);
    }

    QDir dir(unauthorizedLocation);
    if (relativeDirectory != NULL)
        *relativeDirectory = dir;
    return dir.entryList(QDir::Files | QDir::Readable | QDir::NoDotAndDotDot | QDir::NoSymLinks);
}

void ProcessAuthorize::addAuthorization()
{
    if (!FP::defined(authorizationStart))
        authorizationStart = Time::time();
    if (!FP::defined(authorizationEnd) ||
            Range::compareStartEnd(authorizationStart, authorizationEnd) > 0)
        authorizationEnd = authorizationStart;
    double graceEnd = authorizationEnd + 86400.0;

    qCDebug(log_component_process_authorize) << "Adding authorization for" << digest << "to"
                                             << stations << "starting at"
                                             << Logging::time(authorizationStart);

    QStringList displayStations = Util::to_qstringlist(stations);
    std::sort(displayStations.begin(), displayStations.end());

    feedback.emitStage(tr("Change %1 authorization").arg(displayStations.join(",").toUpper()),
                       tr("The authorization settings for %1 are being altered.").arg(
                               displayStations.join(",").toUpper()), false);

    Archive::Access access;
    for (;;) {
        Archive::Access::WriteLock lock(access);

        SequenceValue::Transfer add;
        SequenceIdentity::Transfer remove;
        Variant::Root v;

        v.write()
         .hash("Authorization")
         .hash(digest)
         .setOverlay(QString("/AuthorizationTemplates/%1").arg(templateName));
        for (const auto &station : stations) {
            add.push_back(
                    SequenceValue({station, "configuration", "processing"}, v, authorizationStart,
                                  FP::undefined()));
        }
        if (certificate.read().exists()) {
            v.write().setEmpty();
            v.write().hash("Authorization").hash(digest).hash("Certificate").set(certificate);
            for (const auto &station : stations) {
                add.push_back(SequenceValue({station, "configuration", "processing"}, v,
                                            authorizationStart, FP::undefined(), -1));
            }
        }

        if (deauthorizeOld) {
            int totalDeauthorized = 0;

            StreamSink::Iterator data;
            access.readStream(Archive::Selection(authorizationStart, FP::undefined(), stations,
                                                 {"configuration"},
                                                 {"processing"}).withDefaultStation(false)
                                                                .withMetaArchive(false), &data)
                  ->detach();

            while (data.hasNext()) {
                auto check = data.next();
                if (Range::compareStart(check.getStart(), authorizationStart) > 0)
                    continue;
                if (Range::compareEnd(check.getEnd(), graceEnd) <= 0)
                    continue;
                if (!check.read().hash("Authorization").exists())
                    continue;

                ++totalDeauthorized;

                remove.push_back(SequenceIdentity(check));

                SequenceValue replacement = check;
                replacement.setEnd(graceEnd);
                add.emplace_back(replacement);

                /* Will remove everything, so don't add a new one */
                auto oldChildren = check.read().toHash();
                if (oldChildren.size() == 0 ||
                        (oldChildren.size() == 1 && oldChildren.begin().key() == "Authorization")) {
                    continue;
                }

                replacement = check;
                replacement.setStart(graceEnd);
                if (Range::compareStartEnd(replacement.getStart(), replacement.getEnd()) >= 0)
                    continue;
                replacement.write().hash("Authorization").remove(false);
                add.emplace_back(replacement);
            }

            if (totalDeauthorized > 0) {
                qCDebug(log_component_process_authorize) << "Removing" << totalDeauthorized
                                                         << "old authorization specification(s)";
            }
        }

        Variant::Root event;
        double tnow = Time::time();
        event["Text"].setString(
                tr("Added authorization to process digest %1 using template %2 for %3").arg(digest,
                                                                                            templateName,
                                                                                            displayStations
                                                                                                    .join(",")
                                                                                                    .toUpper()));
        event["Information"].hash("By").setString("process_authorize");
        event["Information"].hash("At").setDouble(tnow);
        event["Information"].hash("Environment").setString(Environment::describe());
        event["Information"].hash("Revision").setString(Environment::revision());
        event["Information"].hash("Digest").setString(digest);
        event["Information"].hash("CertificateDefined").setBool(certificate.read().exists());
        for (const auto &s : stations) {
            event["Information"].hash("Stations").applyFlag(s);
        }
        event["Information"].hash("Deauthorizing").setBool(deauthorizeOld);
        event["Information"].hash("AuthorizationStart").setDouble(authorizationStart);
        event["Information"].hash("AuthorizationEnd").setDouble(authorizationEnd);
        event["Information"].hash("GraceEnd").setDouble(graceEnd);
        for (const auto &station : stations) {
            add.push_back(SequenceValue({station, "events", "processing"}, event, tnow, tnow));
        }

        access.writeSynchronous(add, remove);

        if (lock.commit())
            break;
    };
}

namespace {
class AuthorizeDownloader : public FileDownloader {
public:
    TextSubstitutionStack replace;

    AuthorizeDownloader()
    { }

    virtual ~AuthorizeDownloader()
    { }

protected:
    QString applySubstitutions(const QString &input) const override
    { return replace.apply(input); }

    class MatchingStack : public TextSubstitutionStack {
        QList<TimeCapture> &captures;
    public:
        MatchingStack(const TextSubstitutionStack &base, QList<TimeCapture> &caps)
                : TextSubstitutionStack(base), captures(caps)
        { }

        virtual ~MatchingStack()
        { }

    protected:
        QString substitution(const QStringList &elements) const override
        {
            QString check(FileDownloader::getTimeCapture(elements, captures));
            if (!check.isEmpty())
                return check;
            return QRegExp::escape(TextSubstitutionStack::substitution(elements));
        }
    };

    QString applyMatching(const QString &input, QList<TimeCapture> &captures) const override
    {
        MatchingStack subs(replace, captures);
        return subs.apply(input);
    }
};
}

void ProcessAuthorize::moveAuthorizedFile(const QString &fileName)
{
    if (!moveAfterAuthorize)
        return;
    if (stations.size() != 1) {
        qCDebug(log_component_process_authorize) << "Multiple stations specified, file not moved";
        return;
    }

    double tnow = Time::time();
    auto confList = ValueSegment::Stream::read(
            Archive::Selection(tnow - 1.0, tnow + 1.0, stations, {"configuration"},
                               {"processing"}));
    auto f = Range::findIntersecting(confList, tnow);
    if (f == confList.end()) {
        return;
    }
    auto config =
            f->value().getPath(QString("Profiles/%1/Sources").arg(QString::fromStdString(profile)));

    AuthorizeDownloader downloader;
    Threading::Receiver rx;
    terminateRequested.connect(rx, std::bind(&AuthorizeDownloader::signalTerminate, &downloader));
    downloader.setFindLocalDirectories(true);

    downloader.replace.setString("station", QString::fromStdString(stations.front()).toLower());
    if (!downloader.exec(config))
        return;

    QString targetDirectory;
    auto files = downloader.takeFiles();
    for (const auto &check : files) {
        if (!check->writable())
            continue;
        auto fi = check->fileInfo();
        if (!fi.isDir())
            continue;
        targetDirectory = fi.absoluteFilePath();
    }
    files.clear();

    if (targetDirectory.isEmpty()) {
        qCDebug(log_component_process_authorize) << "No writable local directory available";
        return;
    }

    QDir dir(targetDirectory);
    if (!dir.exists())
        return;

    QFileInfo file(fileName);
    QFileInfo targetInfo(dir, file.fileName());
    qCDebug(log_component_process_authorize) << "Moving file" << file.absoluteFilePath() << "to"
                                             << targetInfo.absoluteFilePath();
    if (!FileUploader::moveLocalFile(file, targetInfo)) {
        qCDebug(log_component_process_authorize) << "Failed to move file for reprocessing";
    }
}


namespace {
class SignatureExtractor : public TransferUnpack {
public:
    QByteArray signature;

    SignatureExtractor()
    { }

    virtual ~SignatureExtractor()
    { }

protected:
    virtual bool signatureAuthorized(const QList<QByteArray> &ids, int depth)
    {
        Q_UNUSED(depth);
        if (ids.size() != 1)
            return false;
        signature = ids.first();
        return true;
    }
};

class DataExtractor : public ErasureSink {
public:
    SequenceName::ComponentSet stations;
    double firstStart;
    double lastStart;

    DataExtractor() : stations(), firstStart(FP::undefined()), lastStart(FP::undefined())
    { }

    virtual ~DataExtractor() = default;

    virtual void incomingValue(const ArchiveValue &value)
    {
        lastStart = value.getStart();

        if (!FP::defined(firstStart)) {
            if (FP::defined(value.getStart()))
                firstStart = value.getStart();
        }

        if (!value.getStation().empty() && value.getStation() != "_")
            stations.insert(value.getStation());
    }

    virtual void incomingErasure(const ArchiveErasure &)
    { }

    virtual void endStream()
    { }

protected:
    virtual void handleData(const SequenceValue::Transfer &values)
    {
        if (values.empty())
            return;

    }
};
}

bool ProcessAuthorize::inspectFile(const std::string &file, FileInformation &info)
{
    auto tempFile = IO::Access::temporaryFile();
    if (!tempFile) {
        qCCritical(log_component_process_authorize) << "Error opening temporary file";
        return false;
    }

    SignatureExtractor unpacker;
    {
        Threading::Receiver rx;
        unpacker.feedback.forward(feedback);
        terminateRequested.connect(rx, std::bind(&SignatureExtractor::signalTerminate, &unpacker));

        unpacker.requireChecksum();
        unpacker.requireSignature(TransferUnpack::Signature_SingleOnly);
        unpacker.requireEncryption(TransferUnpack::Encryption_Default, true);
        unpacker.requireCompression();
        unpacker.requireEnd();

        auto inputFile = IO::Access::file(file, IO::File::Mode::readOnly());
        if (!inputFile) {
            qCCritical(log_component_process_authorize) << "Error opening input file";
            return false;
        }

        if (testTerminated())
            return false;
        if (!unpacker.exec(inputFile, tempFile)) {
            qCDebug(log_component_process_authorize) << "Unpacking failed on" << fileArgument << ':'
                                                     << unpacker.errorString();
            return false;
        }
    }
    if (unpacker.signature.isEmpty()) {
        qCCritical(log_component_process_authorize) << "No signature present in unpacked file";
        feedback.emitFailure(tr("Missing signature"));
        return false;
    }

    DataUnpack dataUnpack;
    DataExtractor data;
    Threading::Receiver rx;
    dataUnpack.feedback.forward(feedback);
    terminateRequested.connect(rx, std::bind(&DataUnpack::signalTerminate, &dataUnpack));
    if (!dataUnpack.exec(tempFile, &data))
        return false;

    info.digest = unpacker.signature.toHex().toLower();
    info.stations = std::move(data.stations);
    info.start = data.firstStart;
    info.end = data.lastStart;
    info.files.emplace_back(QString::fromStdString(file));

    return true;
}

void ProcessAuthorize::FileInformation::merge(FileInformation &&info)
{
    Util::merge(std::move(info.stations), stations);
    Util::append(std::move(info.files), files);

    if (FP::defined(info.start)) {
        if (!FP::defined(start)) {
            start = info.start;
        } else if (start > info.start) {
            start = info.start;
        }
    }
    if (FP::defined(info.end)) {
        if (!FP::defined(end)) {
            end = info.end;
        } else if (end < info.end) {
            end = info.end;
        }
    }
}

void ProcessAuthorize::run()
{
    if (!certificateArgument.isEmpty()) {
        QSslCertificate cd(Cryptography::getCertificate(Variant::Root(certificateArgument), true));
        if (cd.isNull()) {
            certificate = Variant::Root(certificateArgument);
        } else {
            certificate.write().setBinary(cd.toDer());
            if (digest.isEmpty()) {
                digest = Cryptography::sha512(cd).toHex().toLower();
            }
        }
    }

    if (!fileArgument.isEmpty()) {
        FileInformation info;
        if (!inspectFile(fileArgument.toStdString(), info))
            return;

        if (!digest.isEmpty()) {
            if (info.digest != digest.toLower()) {
                qCCritical(log_component_process_authorize)
                    << "Digest in file does not match digest given on the command line";
                return;
            }
        } else {
            digest = info.digest;
        }
        if (stations.empty())
            Util::append(info.stations, stations);
        if (stations.empty()) {
            qCCritical(log_component_process_authorize)
                << "No stations defined to add authorization to";
            return;
        }

        if (testTerminated())
            return;

        if (!FP::defined(authorizationStart))
            authorizationStart = info.start;
        if (FP::defined(info.end))
            authorizationEnd = info.end;

        addAuthorization();
        moveAuthorizedFile(fileArgument);
        return;
    }

    if (!digest.isEmpty()) {
        if (stations.empty()) {
            auto station = getImpliedStation();
            if (!station.empty())
                stations.emplace_back(station);
        }
        if (stations.empty()) {
            qCCritical(log_component_process_authorize)
                << "No stations defined to add authorization to";
            return;
        }

        authorizationEnd = FP::undefined();
        addAuthorization();
        return;
    }

    if (stations.empty()) {
        QDir current;
        QFileInfoList
                files = current.entryInfoList(QStringList("*.c3i"), QDir::Files | QDir::Readable);
        qCDebug(log_component_process_authorize) << "Adding authorization for" << files.size()
                                                 << "total unauthorized file(s)";

        double latestEnd = FP::undefined();
        std::unordered_map<QString, FileInformation> working;
        for (const auto &fileInfo : files) {
            if (testTerminated())
                return;

            FileInformation info;
            if (!inspectFile(fileInfo.absoluteFilePath().toStdString(), info))
                continue;

            if (info.digest.isEmpty()) {
                qCCritical(log_component_process_authorize)
                    << "No signature present in unpacked file";
                feedback.emitFailure(tr("Missing signature"));
                continue;
            }

            if (stations.empty()) {
                qCWarning(log_component_process_authorize)
                    << "No stations defined to add authorization to in file" << fileInfo.fileName();
                continue;
            }

            if (FP::defined(info.end)) {
                if (!FP::defined(latestEnd) || info.end > latestEnd)
                    latestEnd = info.end;
            }

            auto check = working.find(info.digest);
            if (check == working.end()) {
                working.emplace(info.digest, std::move(info));
            } else {
                check->second.merge(std::move(info));
            }
        }

        std::vector<FileInformation> sorted;
        for (auto &add : working) {
            sorted.emplace_back(std::move(add.second));
        }
        std::sort(sorted.begin(), sorted.end(),
                  [](const FileInformation &a, const FileInformation &b) {
                      return Range::compareStart(a.start, b.start) < 0;
                  });

        double restoreAuthStart = authorizationStart;
        if (FP::defined(latestEnd))
            authorizationEnd = latestEnd;
        for (const auto &add : sorted) {
            if (testTerminated())
                return;

            authorizationStart = restoreAuthStart;

            digest = add.digest;
            Q_ASSERT(!digest.isEmpty());
            stations.clear();
            Util::append(add.stations, stations);
            if (!FP::defined(authorizationStart))
                authorizationStart = add.start;

            addAuthorization();
            for (const auto &name : add.files) {
                moveAuthorizedFile(name);
            }
        }

        return;
    }

    auto addStations = stations;
    double latestEnd = FP::undefined();
    double restoreAuthEnd = authorizationEnd;
    std::unordered_map<QString, FileInformation> working;
    for (const auto &station : addStations) {
        if (testTerminated())
            return;

        stations = std::vector<SequenceName::Component>{station};
        authorizationEnd = restoreAuthEnd;

        QDir dir;
        auto files = getUnauthorizedFiles(station, profile, &dir);
        if (files.isEmpty())
            continue;

        qCDebug(log_component_process_authorize) << "Adding authorization for " << files.size()
                                                 << " total unauthorized file(s) for " << station;

        for (const auto &fileName : files) {
            if (testTerminated())
                return;

            FileInformation info;
            if (!inspectFile(dir.filePath(fileName).toStdString(), info))
                continue;

            if (info.digest.isEmpty()) {
                qCCritical(log_component_process_authorize)
                    << "No signature present in unpacked file";
                feedback.emitFailure(tr("Missing signature"));
                continue;
            }

            if (FP::defined(info.end)) {
                if (!FP::defined(latestEnd) || info.end > latestEnd)
                    latestEnd = info.end;
            }

            auto check = working.find(info.digest);
            if (check == working.end()) {
                working.emplace(info.digest, std::move(info));
            } else {
                check->second.merge(std::move(info));
            }
        }

        std::vector<FileInformation> sorted;
        for (auto &add : working) {
            sorted.emplace_back(std::move(add.second));
        }
        std::sort(sorted.begin(), sorted.end(),
                  [](const FileInformation &a, const FileInformation &b) {
                      return Range::compareStart(a.start, b.start) < 0;
                  });

        double restoreAuthStart = authorizationStart;
        if (FP::defined(latestEnd))
            authorizationEnd = latestEnd;
        for (const auto &add : sorted) {
            if (testTerminated())
                return;

            authorizationStart = restoreAuthStart;

            digest = add.digest;
            Q_ASSERT(!digest.isEmpty());
            if (!FP::defined(authorizationStart))
                authorizationStart = add.start;

            addAuthorization();
            for (const auto &name : add.files) {
                moveAuthorizedFile(name);
            }
        }
    }
}

QString ProcessAuthorizeComponent::promptActionContinue(const ComponentOptions &options,
                                                        const std::vector<std::string> &stations)
{
    auto profile = SequenceName::impliedProfile();
    if (options.isSet("profile")) {
        profile = qobject_cast<ComponentOptionSingleString *>(options.get("profile"))->get()
                                                                                     .toLower()
                                                                                     .toStdString();
    }

    if (options.isSet("file")) {
        QString file(qobject_cast<ComponentOptionFile *>(options.get("file"))->get());
        if (!file.isEmpty()) {
            if (stations.empty()) {
                return tr(
                        "This will add the necessary authorization to process data for all stations "
                        "contained in %1.").arg(file);
            } else {
                QStringList sorted = Util::to_qstringlist(stations);
                std::sort(sorted.begin(), sorted.end());
                return tr(
                        "This will add the necessary authorization to process data for %1 with the "
                        "certificate used in %2").arg(sorted.join(",").toUpper(), file);
            }
        }
    }

    if (options.isSet("digest")) {
        QString digest(qobject_cast<ComponentOptionSingleString *>(options.get("digest"))->get());
        if (!digest.isEmpty()) {
            if (stations.empty()) {
                auto station = getImpliedStation();
                return tr(
                        "This will add authorization for the digest %1 to process data for %2.").arg(
                        digest, QString::fromStdString(station).toUpper());
            } else {
                QStringList sorted = Util::to_qstringlist(stations);
                std::sort(sorted.begin(), sorted.end());
                return tr(
                        "This will add authorization for the digest %1 to process data for %2.").arg(
                        digest, sorted.join(",").toUpper());
            }
        }
    }

    if (options.isSet("certificate")) {
        QString cert(qobject_cast<ComponentOptionFile *>(options.get("certificate"))->get());
        if (!cert.isEmpty()) {
            if (stations.empty()) {
                auto station = getImpliedStation();
                return tr(
                        "This will add authorization for the certificate %1 to process data for %2.")
                        .arg(cert, QString::fromStdString(station).toUpper());
            } else {
                QStringList sorted = Util::to_qstringlist(stations);
                std::sort(sorted.begin(), sorted.end());
                return tr(
                        "This will add authorization for the certificate %1 to process data for %2.")
                        .arg(cert, sorted.join(",").toUpper());
            }
        }
    }

    if (stations.empty()) {
        QDir current;
        QStringList
                files(current.entryList(QStringList("*.cpd3upload"), QDir::Files | QDir::Readable));
        if (files.isEmpty())
            return tr("No files to add; this operation will have no effect.");
        return tr("This will add authorization to process the data contained in the file(s) %1.",
                  "", files.size()).arg(files.join(","));
    }

    QStringList files;
    for (const auto &station : stations) {
        files.append(getUnauthorizedFiles(station, profile));
    }
    if (files.isEmpty())
        return tr("No unauthorized files; this operation will have no effect.");

    return tr("This will add authorization to process the file(s) %1.", "", files.size()).arg(
            files.join(","));
}


ComponentOptions ProcessAuthorizeComponent::getOptions()
{
    ComponentOptions options;

    options.add("profile",
                new ComponentOptionSingleString(tr("profile", "name"), tr("Execution profile"),
                                                tr("This is the profile name inspect.  This is used when loading "
                                                   "existing unauthorized files. Multiple profiles can be "
                                                   "defined to perform different types of processing for a station.  "
                                                   "Consult the station processing configuration for available profiles."),
                                                tr("aerosol")));

    options.add("template", new ComponentOptionSingleString(tr("template", "name"),
                                                            tr("Authorization template name"),
                                                            tr("This specifies the name of the template configuration used to "
                                                               "configure the authorization."),
                                                            tr("\"Standard\"")));

    options.add("digest", new ComponentOptionSingleString(tr("digest", "name"),
                                                          tr("Certificate digest to add"),
                                                          tr("This specifies the digest of the certificate to add authorization "
                                                             "for.  When absent, this is calculated from the unauthorized "
                                                             "files.  Use \"Unsigned\" for data without an authorization "
                                                             "certificate or \"Implied\" to set the authorization for "
                                                             "already exchanged certificates."),
                                                          QString()));

    options.add("file",
                new ComponentOptionFile(tr("file", "name"), tr("Unauthorized file to allow"),
                                        tr("This will add the authorization needed to process the file."),
                                        QString()));

    options.add("certificate",
                new ComponentOptionFile(tr("certificate", "name"), tr("Certificate file to load"),
                                        tr("This is the decryption certificate used with the data."),
                                        QString()));

    options.add("move",
                new ComponentOptionBoolean(tr("move", "name"), tr("Move files for reprocessing"),
                                           tr("When enabled any files with that have their authorization added "
                                              "are automatically moved for reprocessing."),
                                           tr("Enabled")));

    options.add("deauthorize", new ComponentOptionBoolean(tr("deauthorize", "name"),
                                                          tr("Remove any old authorization"),
                                                          tr("When enabled any existing authorization is removed and replaced "
                                                             "with the new authorization.  This ensures that old data is not "
                                                             "allowed to be accepted after a short grace period."),
                                                          tr("Enabled")));

    return options;
}

QList<ComponentExample> ProcessAuthorizeComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Authorize data in the current directory",
                                                 "default example name"),
                                     tr("This will change the authorization to allow for processing of all "
                                        "files in the current directory.")));

    options = getOptions();
    qobject_cast<ComponentOptionSingleString *>(options.get("digest"))->set(
            "57dfe3dfbb14c8e43a2f9e95ff5758686cb2f1d20a6afde77e2b577fd1a9fead972eaff5b438963a92ed7cd2a50fc044f6ddd02013e6766381783a03dd201891");
    examples.append(ComponentExample(options, tr("Change authorization", "digest example name"),
                                     tr("This will add authorization for any stations specified to only "
                                        "accept the given certificate digest.")));

    options = getOptions();
    qobject_cast<ComponentOptionFile *>(options.get("file"))->set("/tmp/sfa_authorized.cpd3");
    examples.append(ComponentExample(options, tr("File authorization", "digest example name"),
                                     tr("This will add authorization for the given file.")));

    return examples;
}

int ProcessAuthorizeComponent::actionAllowStations()
{ return INT_MAX; }

CPD3Action *ProcessAuthorizeComponent::createAction(const ComponentOptions &options,
                                                    const std::vector<std::string> &stations)
{ return new ProcessAuthorize(options, stations); }
