/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QDir>
#include <QCryptographicHash>
#include <QHostInfo>
#include <QFile>
#include <QBuffer>
#include <QTemporaryFile>

#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "core/actioncomponent.hxx"
#include "core/number.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"
#include "transfer/datafile.hxx"
#include "transfer/package.hxx"
#include "io/drivers/file.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Transfer;


static const char *cert1Data = "-----BEGIN CERTIFICATE-----\n"
        "MIICbTCCAdYCCQDZ6mj3BI+kCTANBgkqhkiG9w0BAQUFADB7MQswCQYDVQQGEwJV\n"
        "UzERMA8GA1UECAwIQ29sb3JhZG8xEDAOBgNVBAcMB0JvdWxkZXIxFjAUBgNVBAoM\n"
        "DU5PQUEvR01EL0VTUkwxFzAVBgNVBAsMDkFlcm9zb2xzIEdyb3VwMRYwFAYDVQQD\n"
        "DA1EZXJlayBIYWdlbWFuMB4XDTEyMDcyNzE1MzkwNloXDTM5MTIxMjE1MzkwNlow\n"
        "ezELMAkGA1UEBhMCVVMxETAPBgNVBAgMCENvbG9yYWRvMRAwDgYDVQQHDAdCb3Vs\n"
        "ZGVyMRYwFAYDVQQKDA1OT0FBL0dNRC9FU1JMMRcwFQYDVQQLDA5BZXJvc29scyBH\n"
        "cm91cDEWMBQGA1UEAwwNRGVyZWsgSGFnZW1hbjCBnzANBgkqhkiG9w0BAQEFAAOB\n"
        "jQAwgYkCgYEAn3nIKzjLLEY8KG6+hUvrkjDkl9VU8HFKnp8wA2RsMta/UvKG4ZIY\n"
        "bNVsKSiAGm1G0i6Mtftw8quyihoxuy/hElH/XJChU5lersdrhalocHXoipNx8BeW\n"
        "T8sXuJOrh+IoDKqVI4I6Sz42qAlqBU7bNm40TE8sBPAOefyTGavC3e0CAwEAATAN\n"
        "BgkqhkiG9w0BAQUFAAOBgQBU2hljZZpJJ+zK6eePUpdELQGPGHJEbzfReeSBhRau\n"
        "iDj4jEa7IE3yrMv8NqC3Q+6lEVuJg0NInvDYsNYLPjMSPS+jKldrmrJvg+TFrp1w\n"
        "Qr3JoznNxwcwuDyrKjOVux6jo2aype3Zayo5VUizJX9Zo+xLe2MBVR1D0MXmoq8L\n"
        "zA==\n"
        "-----END CERTIFICATE-----";
static const char *key1Data = "-----BEGIN RSA PRIVATE KEY-----\n"
        "MIICXAIBAAKBgQCfecgrOMssRjwobr6FS+uSMOSX1VTwcUqenzADZGwy1r9S8obh\n"
        "khhs1WwpKIAabUbSLoy1+3Dyq7KKGjG7L+ESUf9ckKFTmV6ux2uFqWhwdeiKk3Hw\n"
        "F5ZPyxe4k6uH4igMqpUjgjpLPjaoCWoFTts2bjRMTywE8A55/JMZq8Ld7QIDAQAB\n"
        "AoGBAIQOd0f7PpsKCfS9R7zfklG7dP+Z4z07wzu4vCyC8uniVAoe1LxjmyA8VtV6\n"
        "OSIpDTUs4M4tSWlZ7n1XlYjY6/lNt3xsy5BBQMlWVI8BV/yXnWRCxkQGBvXYTUKe\n"
        "7LRsUcqyGpAPeuStO/V8GEM5H272yv5S4413D09C9cAhROMtAkEA07j2lnnSl/WR\n"
        "0NFEOT9+nbh6Z+yRTzybC86TBhLCN3bY4VvqSWi1PdcU3oiz81dcaAPTksdx18/Y\n"
        "cSfz295ewwJBAMDTq22Dt2HMPQymxNUn9/IdIPU34/fEqSHS3e9hAgrp2gDVeNHR\n"
        "ZEzMkW00rLgdukkW51PV9hVIhYXdxYOjZY8CQFsf/cnwLurGf/b/SrzVDjr1/oEi\n"
        "Obx/2j+vrmnrwvm6Rkhglir4TSGLo+jPr5vpmtUN6I8BFoeLZp31UyjrwZ8CQC3W\n"
        "Y2ruI7qozV5jimjNToCMchg4yAVPB5GVydIsskqb2onWNRlTeE9VVcCrA9/kmTLk\n"
        "serY8t2OVsdCt8AaKHsCQFMEhJCpXaeLnrzToSv0uqRYjcT+r3ZjSghhSepN0ZA+\n"
        "PPj1CMmq9JrQaQAv3rT3hCbDF38TPhKX64OYzbuvdxA=\n"
        "-----END RSA PRIVATE KEY-----\n";
static const char *cert2Data = "-----BEGIN CERTIFICATE-----\n"
        "MIICbTCCAdYCCQCr9EVbER/M4TANBgkqhkiG9w0BAQUFADB7MQswCQYDVQQGEwJV\n"
        "UzERMA8GA1UECAwIQ29sb3JhZG8xEDAOBgNVBAcMB0JvdWxkZXIxFjAUBgNVBAoM\n"
        "DU5PQUEvRVNSTC9HTUQxFzAVBgNVBAsMDkFlcm9zb2xzIEdyb3VwMRYwFAYDVQQD\n"
        "DA1EZXJlayBIYWdlbWFuMB4XDTEyMDcyNzE1NDIxNloXDTM5MTIxMjE1NDIxNlow\n"
        "ezELMAkGA1UEBhMCVVMxETAPBgNVBAgMCENvbG9yYWRvMRAwDgYDVQQHDAdCb3Vs\n"
        "ZGVyMRYwFAYDVQQKDA1OT0FBL0VTUkwvR01EMRcwFQYDVQQLDA5BZXJvc29scyBH\n"
        "cm91cDEWMBQGA1UEAwwNRGVyZWsgSGFnZW1hbjCBnzANBgkqhkiG9w0BAQEFAAOB\n"
        "jQAwgYkCgYEAqa4sVcN1M4mWVqTqZuG2VikkOTpYrHYgnD4jFKDfi5FTFE9OQ5oW\n"
        "xhrBdoEtiWLEPmPSQCtJWCbD0d/YQ1ITxOF0OSaAa4c6PkdeiDmfull8EGmuaTFo\n"
        "ikcFS0r2SVtvIy3eFEAg9GBF4iCMp2AvUo2eqNCLt4CdRi6LQs2xkmUCAwEAATAN\n"
        "BgkqhkiG9w0BAQUFAAOBgQCEvlrqC1zChgkFMq8Oi7YRQOdx7ikzDodey/uu0r/W\n"
        "DxCnpA+F2f3okHiR5+LfYqQ5fvCYnmZd7w69tvdpui8swa15aXWaxLVD9/woyM2y\n"
        "Ofce/9pw6UgoYLkh9KsW5+2EP2Sn44lWcU5Jf8U/p8cDdjNIDzDG4vw7UFWiRakm\n"
        "Tw==\n"
        "-----END CERTIFICATE-----\n";
static const char *key2Data = "-----BEGIN RSA PRIVATE KEY-----\n"
        "MIICWwIBAAKBgQCprixVw3UziZZWpOpm4bZWKSQ5OlisdiCcPiMUoN+LkVMUT05D\n"
        "mhbGGsF2gS2JYsQ+Y9JAK0lYJsPR39hDUhPE4XQ5JoBrhzo+R16IOZ+6WXwQaa5p\n"
        "MWiKRwVLSvZJW28jLd4UQCD0YEXiIIynYC9SjZ6o0Iu3gJ1GLotCzbGSZQIDAQAB\n"
        "AoGAUzE2Q4ZlfDNFJo4M7wxTXcMmI3jb6RKxwmkkwgRuFfvWg+quMK7n45FSsUt8\n"
        "jBOErCI8/4E5oKLA97GMUtV3IxAXT/hxQRe6qZS7PEUW4k6Op7evX1hSCfI/IAB/\n"
        "AS6OqUR1U4+AFArhPS3CA+vuY4I01mpWpvI3dfvq29oUW4ECQQDQ4ItViA/CjWiq\n"
        "peW2+cptQ8QqkNGscjsUTZyp3aNtunC1mP/HRKlKLnuhY6G+LQWbzDv7BSJ9UNO4\n"
        "Ro1y90jJAkEAz/Xdjqx/xZntCleuxvMACWdhpMBqfdgs7+maLDXbtc9G43ygbqIz\n"
        "Wj99k9lu0L3iHdRlYicwdfFFBbvd4jsmvQJATOF5J4AvHNLjpXvuc0y5n0IEIA6x\n"
        "viFFcZGnijZUAv1OouivrG6vSOiXBK4hSFhV6iRgJ2KacTmg1ADT627tUQJAK4hl\n"
        "W9OCX8QMGekm/iCqNk284/cfk75oEcTN8ElJ9/Iu/bn9/4rWwyKdUBDpIKtPJT1s\n"
        "B7L6cwYRk9Sy6wPE5QJAFmjRUr3GypvuC4haazaOMyIt/FEbC+kUs9kq6JZsiBuM\n"
        "LbZ02ko9IkhfOiUIewV6+Xb2j+1u/Itc737M6RZ+3Q==\n"
        "-----END RSA PRIVATE KEY-----";

class TestComponent : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;
    QString tmpPath;
    QDir tmpDir;

    ActionComponent *component;

    void recursiveRemove(const QDir &base)
    {
        QFileInfoList list(base.entryInfoList(QDir::Files | QDir::NoDotAndDotDot));
        for (QFileInfoList::const_iterator rm = list.constBegin(), endRm = list.constEnd();
                rm != endRm;
                ++rm) {
            if (!QFile::remove(rm->absoluteFilePath())) {
                qDebug() << "Failed to remove file" << rm->absoluteFilePath();
                continue;
            }
        }
        list = base.entryInfoList(QDir::Dirs | QDir::NoDotAndDotDot);
        for (QFileInfoList::const_iterator rm = list.constBegin(), endRm = list.constEnd();
                rm != endRm;
                ++rm) {
            QDir subDir(base);
            if (!subDir.cd(rm->fileName())) {
                qDebug() << "Failed to change directory" << rm->absoluteFilePath();
                continue;
            }
            recursiveRemove(subDir);
            base.rmdir(rm->fileName());
        }
    }

    bool writeData(const QString &target,
                   const SequenceValue::Transfer &values, const Variant::Read &config)
    {
        auto buffer = IO::Access::buffer();
        DataPack data(buffer->stream());
        data.start();
        data.incomingData(values);
        data.endData();
        Threading::pollInEventLoop([&] { return !data.isFinished(); });
        data.wait();

        auto output = IO::Access::file(target, IO::File::Mode::writeOnly());
        if (!output) {
            qDebug() << "Unable to open output";
            return false;
        }

        TransferPack filePack;
        filePack.compressStage();
        filePack.signStage(config);
        filePack.checksumStage();
        if (!filePack.exec(buffer, output)) {
            qDebug() << "Error packaging file";
            return false;
        }

        return true;
    }

    bool directoryHasContents(const QDir &dir, const QStringList &contents = QStringList())
    {
        dir.refresh();
        QFileInfoList fi(dir.entryInfoList(QDir::Files | QDir::NoDotAndDotDot));
        if (contents.size() != fi.size())
            return false;
        for (QStringList::const_iterator check = contents.constBegin(),
                endCheck = contents.constEnd(); check != endCheck; ++check) {
            bool hit = false;
            for (QFileInfoList::const_iterator info = fi.constBegin(), endInfo = fi.constEnd();
                    info != endInfo;
                    ++info) {
                if (info->fileName() != *check)
                    continue;
                hit = true;
                break;
            }
            if (!hit)
                return false;
        }
        return true;
    }

    bool checkArchiveContains(SequenceValue::Transfer expected, bool checkTimes = true)
    {
        auto result = Archive::Access(databaseFile).readSynchronous(Archive::Selection());
        for (auto rv = result.begin(); rv != result.end();) {
            auto check =
                    std::find_if(expected.begin(), expected.end(), [=](const SequenceValue &e) {
                        if (checkTimes) {
                            if (!FP::equal(e.getStart(), rv->getStart()))
                                return false;
                            if (!FP::equal(e.getEnd(), rv->getEnd()))
                                return false;
                        }
                        if (e.getUnit() != rv->getUnit())
                            return false;
                        if (e.getPriority() != rv->getPriority())
                            return false;
                        return e.getValue() == rv->getValue();
                    });
            if (check == expected.end()) {
                ++rv;
                continue;
            }

            rv = result.erase(rv);
            expected.erase(check);
        }

        if (!expected.empty()) {
            qDebug() << "Unmatched expected values:" << expected;
            return false;
        }
        return true;
    }

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component = qobject_cast<ActionComponent *>(ComponentLoader::create("process_authorize"));
        QVERIFY(component);

        QVERIFY(databaseFile.open());
        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());

        tmpDir = QDir(QDir::tempPath());
        tmpPath = "CPD3Process-";
        tmpPath.append(Random::string());
        tmpDir.mkdir(tmpPath);
        QVERIFY(tmpDir.cd(tmpPath));
    }

    void cleanupTestCase()
    {
        recursiveRemove(tmpDir);
        tmpDir.cdUp();
        tmpDir.rmdir(tmpPath);
    }

    void init()
    {
        recursiveRemove(tmpDir);
        databaseFile.resize(0);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("template")));
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("digest")));
        QVERIFY(qobject_cast<ComponentOptionFile *>(options.get("file")));
        QVERIFY(qobject_cast<ComponentOptionFile *>(options.get("certificate")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("move")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("deauthorize")));
    }

    void singleFile()
    {
        tmpDir.mkdir("unauthorized");
        tmpDir.mkdir("incoming");

        Variant::Root config;
        config["/Profiles/aerosol/Sources/#0/Path"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "incoming"));
        config["/Profiles/aerosol/Sources/#0/Writable"].setBool(true);
        config["/Profiles/aerosol/UnauthorizedLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "unauthorized"));

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "configuration", "processing"}, config)});

        SequenceValue::Transfer transferData1;
        transferData1.emplace_back(SequenceName("sfa", "raw", "T_S11"), Variant::Root(1.0), 100,
                                   200, 0);
        transferData1.emplace_back(SequenceName("sfa", "raw", "P_S11"), Variant::Root(1.0), 100,
                                   200, 0);

        Variant::Root packConfig;
        packConfig["Certificate"].setString(cert1Data);
        packConfig["Key"].setString(key1Data);
        writeData(tmpDir.absolutePath() + "/unauthorized/process1.c3i", transferData1,
                  packConfig);

        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionFile *>(options.get("file"))->set(
                tmpDir.absolutePath() + "/unauthorized/process1.c3i");
        CPD3Action *action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        QDir incomingDir(tmpDir.absolutePath() + "/incoming");
        QDir unauthorizedDir(tmpDir.absolutePath() + "/unauthorized");

        QTest::qSleep(50);
        QVERIFY(directoryHasContents(incomingDir, QStringList("process1.c3i")));
        QVERIFY(directoryHasContents(unauthorizedDir));

        Variant::Root configMod1;
        configMod1["/Authorization/57dfe3dfbb14c8e43a2f9e95ff5758686cb2f1d20a6afde77e2b577fd1a9fead972eaff5b438963a92ed7cd2a50fc044f6ddd02013e6766381783a03dd201891"]
                .setOverlay("/AuthorizationTemplates/Standard");

        QVERIFY(checkArchiveContains(SequenceValue::Transfer{
                SequenceValue(SequenceName("sfa", "configuration", "processing"), config,
                              FP::undefined(), FP::undefined()),
                SequenceValue(SequenceName("sfa", "configuration", "processing"), configMod1, 100.0,
                              FP::undefined())}));


        SequenceValue::Transfer transferData2;
        transferData2.emplace_back(SequenceName("sfa", "raw", "T_S11"), Variant::Root(1.1), 1000,
                                   2000, 0);
        transferData2.emplace_back(SequenceName("sfa", "raw", "P_S11"), Variant::Root(1.1), 2000,
                                   3000, 0);

        packConfig.write().setEmpty();
        packConfig["Certificate"].setString(cert2Data);
        packConfig["Key"].setString(key2Data);
        writeData(tmpDir.absolutePath() + "/unauthorized/process2.c3i", transferData2,
                  packConfig);

        options = component->getOptions();
        qobject_cast<ComponentOptionFile *>(options.get("file"))->set(
                tmpDir.absolutePath() + "/unauthorized/process2.c3i");
        qobject_cast<ComponentOptionBoolean *>(options.get("move"))->set(false);
        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;


        QTest::qSleep(50);
        QVERIFY(directoryHasContents(incomingDir, QStringList("process1.c3i")));
        QVERIFY(directoryHasContents(unauthorizedDir, QStringList("process2.c3i")));

        Variant::Root configMod2;
        configMod2["/Authorization/f46c0269714eb972238d3cccc51461485b538cf449d26dd27eb0d5eff34ecd87d14909ca8096d3359751a47e6826fea15def581fd131cf55d629db1064d159ab"]
                .setOverlay("/AuthorizationTemplates/Standard");

        QVERIFY(checkArchiveContains(SequenceValue::Transfer{
                SequenceValue(SequenceName("sfa", "configuration", "processing"), config,
                              FP::undefined(), FP::undefined()),
                SequenceValue(SequenceName("sfa", "configuration", "processing"), configMod1, 100.0,
                              88400.0),
                SequenceValue(SequenceName("sfa", "configuration", "processing"), configMod2,
                              1000.0, FP::undefined())}));
    }

    void allPending()
    {
        tmpDir.mkdir("unauthorized");
        tmpDir.mkdir("incoming");

        Variant::Root config;
        config["/Profiles/aerosol/Sources/#0/Path"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "incoming"));
        config["/Profiles/aerosol/Sources/#0/Writable"].setBool(true);
        config["/Profiles/aerosol/UnauthorizedLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "unauthorized"));
        config["/Authorization/Unsigned"].setBool(true);

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "configuration", "processing"}, config)});

        SequenceValue::Transfer transferData1;
        transferData1.emplace_back(SequenceName("sfa", "raw", "T_S11"), Variant::Root(1.0), 100,
                                   200, 0);
        transferData1.emplace_back(SequenceName("sfa", "raw", "P_S11"), Variant::Root(1.0), 200,
                                   300, 0);

        Variant::Root packConfig;
        packConfig["Certificate"].setString(cert1Data);
        packConfig["Key"].setString(key1Data);
        writeData(tmpDir.absolutePath() + "/unauthorized/process1.c3i", transferData1,
                  packConfig);

        ComponentOptions options(component->getOptions());
        CPD3Action *action = component->createAction(options, {"sfa"});
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        QDir incomingDir(tmpDir.absolutePath() + "/incoming");
        QDir unauthorizedDir(tmpDir.absolutePath() + "/unauthorized");

        QTest::qSleep(50);
        QVERIFY(directoryHasContents(incomingDir, QStringList("process1.c3i")));
        QVERIFY(directoryHasContents(unauthorizedDir));

        Variant::Root configMod1 = config;
        configMod1["/Authorization"].remove();
        Variant::Root configMod2;
        configMod2["/Authorization/57dfe3dfbb14c8e43a2f9e95ff5758686cb2f1d20a6afde77e2b577fd1a9fead972eaff5b438963a92ed7cd2a50fc044f6ddd02013e6766381783a03dd201891"]
                .setOverlay("/AuthorizationTemplates/Standard");

        QVERIFY(checkArchiveContains(SequenceValue::Transfer{
                SequenceValue(SequenceName("sfa", "configuration", "processing"), config,
                              FP::undefined(), 86600.0),
                SequenceValue(SequenceName("sfa", "configuration", "processing"), configMod1, 86600,
                              FP::undefined()),
                SequenceValue(SequenceName("sfa", "configuration", "processing"), configMod2, 100.0,
                              FP::undefined())}));


        SequenceValue::Transfer transferData2;
        transferData2.emplace_back(SequenceName("sfa", "raw", "T_S11"), Variant::Root(1.1), 1000,
                                   2000, 0);
        transferData2.emplace_back(SequenceName("sfa", "raw", "P_S11"), Variant::Root(1.1), 2000,
                                   3000, 0);

        packConfig.write().setEmpty();
        packConfig["Certificate"].setString(cert2Data);
        packConfig["Key"].setString(key2Data);
        writeData(tmpDir.absolutePath() + "/unauthorized/process2.c3i", transferData2,
                  packConfig);

        options = component->getOptions();
        action = component->createAction(options, {"sfa"});
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;


        QTest::qSleep(50);
        QVERIFY(directoryHasContents(incomingDir, QStringList("process1.c3i") << "process2.c3i"));
        QVERIFY(directoryHasContents(unauthorizedDir));

        Variant::Root configMod3;
        configMod3["/Authorization/f46c0269714eb972238d3cccc51461485b538cf449d26dd27eb0d5eff34ecd87d14909ca8096d3359751a47e6826fea15def581fd131cf55d629db1064d159ab"]
                .setOverlay("/AuthorizationTemplates/Standard");

        QVERIFY(checkArchiveContains(SequenceValue::Transfer{
                SequenceValue(SequenceName("sfa", "configuration", "processing"), config,
                              FP::undefined(), 86600.0),
                SequenceValue(SequenceName("sfa", "configuration", "processing"), configMod1, 86600,
                              FP::undefined()),
                SequenceValue(SequenceName("sfa", "configuration", "processing"), configMod2, 100.0,
                              88400.0),
                SequenceValue(SequenceName("sfa", "configuration", "processing"), configMod3,
                              1000.0, FP::undefined())}));
    }

    void multipleSameDigest()
    {
        tmpDir.mkdir("unauthorized");
        tmpDir.mkdir("incoming");

        Variant::Root config;
        config["/Profiles/aerosol/Sources/#0/Path"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "incoming"));
        config["/Profiles/aerosol/Sources/#0/Writable"].setBool(true);
        config["/Profiles/aerosol/UnauthorizedLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "unauthorized"));
        config["/Authorization/Unsigned"].setBool(true);

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"_", "configuration", "processing"}, config)});

        SequenceValue::Transfer transferData1;
        transferData1.emplace_back(SequenceName("sfa", "raw", "T_S11"), Variant::Root(1.0), 100,
                                   200, 0);
        transferData1.emplace_back(SequenceName("sfa", "raw", "P_S11"), Variant::Root(1.0), 200,
                                   300, 0);

        Variant::Root packConfig;
        packConfig["Certificate"].setString(cert1Data);
        packConfig["Key"].setString(key1Data);
        writeData(tmpDir.absolutePath() + "/unauthorized/process1.c3i", transferData1, packConfig);

        SequenceValue::Transfer transferData2;
        transferData2.emplace_back(SequenceName("sfa", "raw", "T_S11"), Variant::Root(1.1), 1000,
                                   2000, 0);
        transferData2.emplace_back(SequenceName("sfa", "raw", "P_S11"), Variant::Root(1.1), 2000,
                                   3000, 0);

        writeData(tmpDir.absolutePath() + "/unauthorized/process2.c3i", transferData2, packConfig);

        ComponentOptions options(component->getOptions());
        CPD3Action *action = component->createAction(options, {"sfa"});
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        QDir incomingDir(tmpDir.absolutePath() + "/incoming");
        QDir unauthorizedDir(tmpDir.absolutePath() + "/unauthorized");

        QTest::qSleep(50);
        QVERIFY(directoryHasContents(incomingDir, QStringList("process1.c3i") << "process2.c3i"));
        QVERIFY(directoryHasContents(unauthorizedDir));

        Variant::Root configMod;
        configMod["/Authorization/57dfe3dfbb14c8e43a2f9e95ff5758686cb2f1d20a6afde77e2b577fd1a9fead972eaff5b438963a92ed7cd2a50fc044f6ddd02013e6766381783a03dd201891"]
                .setOverlay("/AuthorizationTemplates/Standard");

        QVERIFY(checkArchiveContains(SequenceValue::Transfer{
                SequenceValue(SequenceName("_", "configuration", "processing"), config,
                              FP::undefined(), FP::undefined()),
                SequenceValue(SequenceName("sfa", "configuration", "processing"), configMod, 100,
                              FP::undefined())}));
    }

    void specificDigest()
    {
        Variant::Root config;
        config["/Profiles/aerosol/Sources/#0/Path"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "incoming"));
        config["/Profiles/aerosol/Sources/#0/Writable"].setBool(true);
        config["/Profiles/aerosol/UnauthorizedLocation"].setString(
                QString("%1/%2").arg(tmpDir.absolutePath(), "unauthorized"));

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "configuration", "processing"}, config)});

        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionSingleString *>(options.get("digest"))->set(
                "57dfe3dfbb14c8e43a2f9e95ff5758686cb2f1d20a6afde77e2b577fd1a9fead972eaff5b438963a92ed7cd2a50fc044f6ddd02013e6766381783a03dd201891");
        CPD3Action *action = component->createAction(options, {"sfa"});
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;

        Variant::Root configMod1;
        configMod1["/Authorization/57dfe3dfbb14c8e43a2f9e95ff5758686cb2f1d20a6afde77e2b577fd1a9fead972eaff5b438963a92ed7cd2a50fc044f6ddd02013e6766381783a03dd201891"]
                .setOverlay("/AuthorizationTemplates/Standard");

        QVERIFY(checkArchiveContains(SequenceValue::Transfer{
                SequenceValue(SequenceName("sfa", "configuration", "processing"), config,
                              FP::undefined(), FP::undefined()),
                SequenceValue(SequenceName("sfa", "configuration", "processing"), configMod1, 100.0,
                              FP::undefined())}, false));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
