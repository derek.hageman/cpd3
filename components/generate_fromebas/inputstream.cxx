/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QUuid>
#include <QXmlStreamWriter>
#include <QDomDocument>
#include <QLoggingCategory>

#include "fileinput/ebas.hxx"

#include "inputstream.hxx"
#include "generate_fromebas.hxx"

Q_DECLARE_LOGGING_CATEGORY(log_component_generate_fromebas)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Input;

static const int maximumActiveStreams = 32;

EBASInputStream::EBASInputStream(GenerateFromEBAS *parent,
                                 std::unique_ptr<CPD3::Transfer::DownloadFile> &&file,
                                 const SequenceSegment::Transfer &globalConfig,
                                 const SequenceName::Component &station,
                                 const std::string &profile) : parent(parent),
                                                               downloadFile(std::move(file)),
                                                               converter(NULL),
                                                               input(NULL),
                                                               buffer()
{
    EBASEngine *engine =
            new EBASEngine(globalConfig, SequenceName(station, "configuration", "import_ebas"),
                           QString("Profiles/%1").arg(QString::fromStdString(profile)));
    Q_ASSERT(engine);

    engine->setStation(QString::fromStdString(station));

    converter = engine;

    converter->start();
    converter->setEgress(parent->mux.createSink());
}

EBASInputStream::~EBASInputStream()
{
    delete converter;
    if (input != NULL)
        delete input;
}

SequenceName::Set EBASInputStream::predictedOutputs()
{ return converter->predictedOutputs(); }

void EBASInputStream::signalTerminate()
{
    converter->signalTerminate();
    emit streamIdentified(this);
}

static QByteArray extractResponseData(const QByteArray &response)
{
    QDomDocument doc;
    QString errorMessage;
    int errorLine = -1;
    int errorColumn = -1;
    bool parseResult = doc.setContent(response, true, &errorMessage, &errorLine, &errorColumn);
    if (!parseResult) {
        qCInfo(log_component_generate_fromebas) << "Error parsing SOAP XML response:"
                                                << errorMessage << "at line" << errorLine
                                                << "column" << errorColumn;
        return QByteArray();
    }

    QDomNodeList resultList
            (doc.elementsByTagNameNS("http://ebas.nilu.no/services", "GetPublicDatasetsResult"));
    if (resultList.isEmpty()) {
        resultList = doc.elementsByTagNameNS("http://ebas.nilu.no/services",
                                             "GetPublicOrPrivateDatasetsResult");
    }
    if (resultList.isEmpty()) {
        qCInfo(log_component_generate_fromebas) << "Unable to locate EBAS response";
        return QByteArray();
    }

    QDomElement result(resultList.item(0).firstChildElement("ArrayOfstring"));
    if (result.isNull()) {
        qCInfo(log_component_generate_fromebas) << "Response has no data";
        return QByteArray();
    }

    QDomElement ebasFile(result.firstChildElement("string"));
    if (ebasFile.isNull()) {
        qCInfo(log_component_generate_fromebas) << "Response has no contents";
        return QByteArray();
    }

    QByteArray ebasData(ebasFile.text().toUtf8());

    qCDebug(log_component_generate_fromebas) << "Extracted" << ebasData.size()
                                             << "byte(s) of EBAS data";
    return ebasData;
}

void EBASInputStream::start()
{
    Q_ASSERT(downloadFile != NULL);
    Q_ASSERT(input == NULL);

    input = new QFile(downloadFile->fileInfo().absoluteFilePath());
    if (!input->open(QIODevice::ReadOnly)) {
        qCWarning(log_component_generate_fromebas) << "Failed to open stream file:"
                                                   << input->errorString();
        delete input;
        input = NULL;
        downloadFile.reset();
        return;
    }

    connect(input, SIGNAL(readyRead()), this, SLOT(readInput()));
    connect(input, SIGNAL(readChannelFinished()), this, SLOT(readInput()));

    QMetaObject::invokeMethod(this, "readInput", Qt::QueuedConnection);

    qCDebug(log_component_generate_fromebas) << "Starting read of response with" << input->size()
                                             << "raw byte(s)";
}

void EBASInputStream::inputFinished()
{
    input->disconnect(this);
    input->close();
    delete input;
    input = NULL;
    downloadFile.reset();

    converter->incomingData(extractResponseData(buffer));
    buffer = QByteArray();
    converter->endData();
}


void EBASInputStream::readInput()
{
    if (input == NULL)
        return;

    qint64 n = qMin(input->bytesAvailable(), Q_INT64_C(65536));
    if (n <= 0) {
        if (!input->atEnd())
            return;
        inputFinished();
        return;
    }

    int original = buffer.size();
    buffer.resize(original + (int) n);
    n = input->read(buffer.data() + original, n);
    if (n <= 0) {
        buffer.resize(original);
        inputFinished();
        return;
    }

    buffer.resize(original + (int) n);

    if (input->bytesAvailable() > 0 || input->atEnd())
        QMetaObject::invokeMethod(this, "readInput", Qt::QueuedConnection);
}



