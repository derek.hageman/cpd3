/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <memory>
#include <QLoggingCategory>

#define DISABLE_VALUE_AUTOCAST

#include "datacore/variant/root.hxx"

#include "datacore/segment.hxx"
#include "transfer/download.hxx"

#include "generate_fromebas.hxx"
#include "ebasdownload.hxx"
#include "stationlookup.hxx"
#include "inputstream.hxx"


Q_LOGGING_CATEGORY(log_component_generate_fromebas, "cpd3.component.generate.fromebas",
                   QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Transfer;


GenerateFromEBAS::GenerateFromEBAS(const CPD3::ComponentOptions &options,
                                   double start,
                                   double end,
                                   const std::vector<SequenceName::Component> &stations) : dataStart(start),
                                                                                 dataEnd(end),
                                                                                 stations(stations),
                                                                                 profile(SequenceName::impliedProfile()),
                                                                                 mux(),
                                                                                 thread(this)
{
    if (options.isSet("profile")) {
        profile = qobject_cast<ComponentOptionSingleString *>(options.get("profile"))->get()
                                                                                     .toLower()
                                                                                     .toStdString();
    }

    mux.finished.connect([this] { finished(); });
}

GenerateFromEBAS::~GenerateFromEBAS()
{
    thread.signalTerminate();
    thread.wait();
    mux.signalTerminate();
}

SequenceName::Set GenerateFromEBAS::predictedOutputs()
{ return thread.predictedOutputs(); }

GenerateFromEBAS::WorkerThread::WorkerThread(GenerateFromEBAS *p) : parent(p)
{ }

GenerateFromEBAS::WorkerThread::~WorkerThread()
{
    for (QList<EBASInputStream *>::const_iterator stream = streams.constBegin(),
            end = streams.constEnd(); stream != end; ++stream) {
        (*stream)->signalTerminate();
    }
    qDeleteAll(streams);
}

SequenceName::Set GenerateFromEBAS::WorkerThread::predictedOutputs()
{
    SequenceName::Set output;
    std::lock_guard<std::mutex> lock(mutex);
    for (auto add : streams) {
        Util::merge(add->predictedOutputs(), output);
    }
    return output;
}

void GenerateFromEBAS::WorkerThread::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
        for (QList<EBASInputStream *>::const_iterator stream = streams.constBegin(),
                end = streams.constEnd(); stream != end; ++stream) {
            (*stream)->signalTerminate();
        }
    }
    quit();
}

void GenerateFromEBAS::WorkerThread::run()
{
    auto globalConfig = SequenceSegment::Stream::read(
            Archive::Selection(parent->dataStart, parent->dataEnd, parent->stations,
                               {"configuration"}, {"import_ebas", "site"}));

    EBASDownload download;
    Threading::Receiver rx;
    parent->terminateRequested.connect(rx, std::bind(&EBASDownload::signalTerminate, &download));

    if (!globalConfig.empty()) {
        std::unordered_map<Variant::Read, std::shared_ptr<EBASStationLookup>,
                           CPD3::Data::Variant::Read::ValueHash> stationLookupCache;
        std::unordered_map<Variant::Read, QList<EBASDownloadSegment>,
                           CPD3::Data::Variant::Read::ValueHash> sourceCache;
        for (const auto &station : parent->stations) {
            auto config = globalConfig.back()
                                      .getValue(SequenceName(station, "configuration",
                                                             "import_ebas"))
                                      .hash("Profiles")
                                      .hash(parent->profile);

            std::shared_ptr<EBASStationLookup> stationLookup;
            switch (config["Station"].getType()) {
            case Variant::Type::String: {
                QString ebasCode(config["Station"].toQString());
                if (!ebasCode.isEmpty()) {
                    stationLookup = std::shared_ptr<EBASStationLookup>(
                            new EBASStationLookupConstant(ebasCode));
                    break;
                }
            }
                /* Fall through */
            default: {
                QString ebasCode(globalConfig.back()
                                             .getValue(SequenceName(station, "configuration",
                                                                    "site"))
                                             .getPath("EBAS/Station")
                                             .toQString());
                if (ebasCode.isEmpty()) {
                    qCDebug(log_component_generate_fromebas)
                        << "Unable to resolve EBAS station code for" << station;
                    continue;
                }
                stationLookup =
                        std::shared_ptr<EBASStationLookup>(new EBASStationLookupConstant(ebasCode));
                break;
            }
            case Variant::Type::Hash: {
                auto c = config["Station"];
                stationLookup = stationLookupCache[c];
                if (!stationLookup) {
                    stationLookup =
                            std::shared_ptr<EBASStationLookup>(new EBASStationLookupDownload(c));
                    stationLookupCache.emplace(c, stationLookup);
                }
                break;
            }
            }

            Q_ASSERT(stationLookup);
            stationLookup->acquire(&download);
            QString ebasStation(stationLookup->toEBASStation(station));

            qCDebug(log_component_generate_fromebas) << "Using" << ebasStation << "for" << station;

            for (auto c : config["Fetch"].toChildren()) {
                Variant::Root f(c["Origin"]);
                if (!f.read().exists())
                    f.write().setBoolean(true);
                sourceCache[f].append(
                        EBASDownloadSegment(parent->dataStart, parent->dataEnd, station,
                                            parent->profile, c, globalConfig, ebasStation));
            }
        }

        for (const auto &f : sourceCache) {
            download.setSegments(f.second);
            if (!download.exec(f.first))
                continue;

            QList<EBASInputStream *> add(download.createStreams(parent));

            std::lock_guard<std::mutex> lock(mutex);
            streams.append(add);
        }
    }

    parent->mux.sinkCreationComplete();

    {
        QList<EBASInputStream *> es;
        {
            std::lock_guard<std::mutex> lock(mutex);
            es = streams;
        }
        for (QList<EBASInputStream *>::const_iterator stream = es.constBegin(), end = es.constEnd();
                stream != end;
                ++stream) {
            (*stream)->start();
        }
    }

    exec();
}

void GenerateFromEBAS::start()
{
    mux.start();
    thread.start();
}

void GenerateFromEBAS::signalTerminate()
{
    thread.signalTerminate();
    mux.signalTerminate(true);
    terminateRequested();
}

bool GenerateFromEBAS::isFinished()
{ return mux.isFinished(); }

bool GenerateFromEBAS::wait(double timeout)
{ return mux.wait(timeout); }

void GenerateFromEBAS::setEgress(StreamSink *egress)
{ mux.setEgress(egress); }


void GenerateFromEBAS::incomingData(const Util::ByteView &)
{ }

void GenerateFromEBAS::endData()
{ }


ComponentOptions GenerateFromEBASComponent::getOptions()
{
    ComponentOptions options;

    options.add("profile",
                new ComponentOptionSingleString(tr("profile", "name"), tr("Execution profile"),
                                                tr("This is the profile name to generate data for.  Different "
                                                   "profiles can generate different sets of data.  "
                                                   "Consult the station configuration for available profiles."),
                                                tr("aerosol")));

    return options;
}

QList<ComponentExample> GenerateFromEBASComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Generate fromebas data", "default example name"),
                                     tr("This will generate fromebas data for the \"aerosol\" profile.")));

    return examples;
}

int GenerateFromEBASComponent::ingressAllowStations()
{ return INT_MAX; }

int GenerateFromEBASComponent::ingressRequireStations()
{ return 1; }

bool GenerateFromEBASComponent::ingressRequiresTime()
{ return true; }

Time::LogicalTimeUnit GenerateFromEBASComponent::ingressDefaultTimeUnit()
{ return Time::Week; }

ExternalConverter *GenerateFromEBASComponent::createExternalIngress(const ComponentOptions &options,
                                                                    double start,
                                                                    double end,
                                                                    const std::vector<SequenceName::Component> &stations)
{
    if (stations.empty())
        return nullptr;
    return new GenerateFromEBAS(options, start, end, stations);
}
