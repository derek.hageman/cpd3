/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef STATIONLOOKUP_H
#define STATIONLOOKUP_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "datacore/segment.hxx"
#include "transfer/download.hxx"

class GenerateFromEBAS;

class FetchFile;

class EBASStationLookup : public QObject {
Q_OBJECT

public:
    EBASStationLookup();

    virtual ~EBASStationLookup();

    virtual QString toEBASStation(const CPD3::Data::SequenceName::Component &gawStation) = 0;

    virtual void acquire(CPD3::Transfer::FileDownloader *downloader) = 0;
};

class EBASStationLookupConstant : public EBASStationLookup {
Q_OBJECT

    QString station;
public:
    EBASStationLookupConstant(const QString &station);

    virtual ~EBASStationLookupConstant();

    virtual QString toEBASStation(const CPD3::Data::SequenceName::Component &gawStation);

    virtual void acquire(CPD3::Transfer::FileDownloader *downloader);
};

class EBASStationLookupDownload : public EBASStationLookup {
Q_OBJECT

    bool ready;
    QHash<QString, QString> lookup;

    CPD3::Data::Variant::Root config;
    int idxGAW;
    int idxEBAS;
public:
    EBASStationLookupDownload(const CPD3::Data::Variant::Read &config);

    virtual ~EBASStationLookupDownload();

    virtual QString toEBASStation(const CPD3::Data::SequenceName::Component &gawStation);

    virtual void acquire(CPD3::Transfer::FileDownloader *downloader);
};

#endif
