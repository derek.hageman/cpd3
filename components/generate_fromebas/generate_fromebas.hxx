/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef GENERATEFROMEBAS_H
#define GENERATEFROMEBAS_H

#include "core/first.hxx"

#include <mutex>
#include <QtGlobal>
#include <QObject>
#include <QThread>

#include "core/component.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/sinkmultiplexer.hxx"

class EBASInputStream;

class GenerateFromEBAS : public CPD3::Data::ExternalConverter {
    double dataStart;
    double dataEnd;
    std::vector<CPD3::Data::SequenceName::Component> stations;
    std::string profile;

    CPD3::Data::SinkMultiplexer mux;

    friend class EBASInputStream;

    class WorkerThread : public QThread {
        GenerateFromEBAS *parent;
        QList<EBASInputStream *> streams;
        QSet<EBASInputStream *> ready;
        std::mutex mutex;
        bool terminated;
    public:
        WorkerThread(GenerateFromEBAS *parent);

        virtual ~WorkerThread();

        void signalTerminate();

        CPD3::Data::SequenceName::Set predictedOutputs();

    protected:
        virtual void run();
    };

    WorkerThread thread;

    CPD3::Threading::Signal<> terminateRequested;

public:
    GenerateFromEBAS(const CPD3::ComponentOptions &options,
                     double start,
                     double end,
                     const std::vector<CPD3::Data::SequenceName::Component> &stations);

    virtual ~GenerateFromEBAS();

    void incomingData(const CPD3::Util::ByteView &data) override;

    void endData() override;

    void signalTerminate() override;

    bool isFinished() override;

    void start() override;

    bool wait(double timeout = CPD3::FP::undefined()) override;

    CPD3::Data::SequenceName::Set predictedOutputs() override;

    void setEgress(CPD3::Data::StreamSink *setEgress) override;
};

class GenerateFromEBASComponent
        : public QObject, virtual public CPD3::Data::ExternalSourceComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalSourceComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.generate_fromebas"
                              FILE
                              "generate_fromebas.json")

public:
    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual int ingressAllowStations();

    virtual int ingressRequireStations();

    virtual bool ingressRequiresTime();

    virtual CPD3::Time::LogicalTimeUnit ingressDefaultTimeUnit();

    CPD3::Data::ExternalConverter *createExternalIngress(const CPD3::ComponentOptions &options,
                                                         double start,
                                                         double end,
                                                         const std::vector<
                                                                 CPD3::Data::SequenceName::Component> &stations = {}) override;
};

#endif
