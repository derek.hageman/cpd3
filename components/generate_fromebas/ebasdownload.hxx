/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */


#ifndef CPD3_EBASDOWNLOAD_HXX
#define CPD3_EBASDOWNLOAD_HXX

#include "core/first.hxx"

#include "datacore/segment.hxx"
#include "transfer/download.hxx"

class EBASInputStream;

class GenerateFromEBAS;

class EBASDownloadSegment {
    double start;
    double end;
    CPD3::Data::SequenceName::Component station;
    std::string profile;

    CPD3::Data::Variant::Read config;
    CPD3::Data::SequenceSegment::Transfer globalConfig;

    QString ebasStation;

    QSet<QString> acceptParameters;
    QSet<QString> acceptMatrices;
    QSet<QString> acceptInstruments;

    QSet<QString> auxiliaryParameters;
    QSet<QString> auxiliaryMatrices;
    QSet<QString> auxiliaryInstruments;
public:

    inline double getStart() const
    { return start; }

    inline double getEnd() const
    { return end; }

    enum MatchResult {
        NoMatch, Primary, Auxiliary,
    };

    EBASDownloadSegment(double start,
                        double end,
                        const CPD3::Data::SequenceName::Component &station,
                        const std::string &profile,
                        const CPD3::Data::Variant::Read &config,
                        const CPD3::Data::SequenceSegment::Transfer &globalConfig,
                        const QString &ebasStation);

    MatchResult matches(const CPD3::Time::Bounds &bounds,
                        const QString &station,
                        const QString &matrix,
                        const QString &instrument,
                        const QString &parameter) const;

    EBASInputStream *create(std::unique_ptr<CPD3::Transfer::DownloadFile> &&file, GenerateFromEBAS *parent) const;
};

class EBASDownload : public CPD3::Transfer::FileDownloader {
    QList<EBASDownloadSegment> candidates;
    QHash<CPD3::Transfer::DownloadFile *, int> resultLookup;

    std::unique_ptr<CPD3::Transfer::DownloadFile> createParameterFetch(const CPD3::Data::Variant::Read &base,
                                                       const QSet<qint64> &parameters,
                                                       double start,
                                                       double end);

public:
    EBASDownload();

    virtual ~EBASDownload();

    void setSegments(const QList<EBASDownloadSegment> &set);

    QList<EBASInputStream *> createStreams(GenerateFromEBAS *parent);

public:
    std::unique_ptr<CPD3::Transfer::DownloadFile> createSingleFetch(const CPD3::Data::Variant::Read &config) override;

    bool convertListResponse(const CPD3::Data::Variant::Read &config,
                             CPD3::Transfer::DownloadFile &response,
                             std::vector<std::unique_ptr<CPD3::Transfer::DownloadFile>> &files) override;
};

#endif //CPD3_EBASDOWNLOAD_HXX
