/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef INPUTSTREAM_H
#define INPUTSTREAM_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QTimer>

#include "transfer/download.hxx"
#include "datacore/segment.hxx"
#include "datacore/externalsource.hxx"

#include "ebasdownload.hxx"

class GenerateFromEBAS;

class EBASInputStream : public QObject {
Q_OBJECT

    GenerateFromEBAS *parent;
    std::unique_ptr<CPD3::Transfer::DownloadFile> downloadFile;
    CPD3::Data::ExternalConverter *converter;

    QFile *input;
    QByteArray buffer;

    void inputFinished();

public:
    EBASInputStream(GenerateFromEBAS *parent,
                    std::unique_ptr<CPD3::Transfer::DownloadFile> &&file,
                    const CPD3::Data::SequenceSegment::Transfer &globalConfig,
                    const CPD3::Data::SequenceName::Component &station,
                    const std::string &profile);

    virtual ~EBASInputStream();

    void signalTerminate();

    CPD3::Data::SequenceName::Set predictedOutputs();

    void start();

signals:

    void streamIdentified(EBASInputStream *stream);

private slots:

    void readInput();
};


#endif
