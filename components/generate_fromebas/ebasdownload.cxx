/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include <QXmlStreamWriter>
#include <QUuid>
#include <QLoggingCategory>

#include "core/csv.hxx"
#include "core/timeparse.hxx"
#include "io/drivers/file.hxx"

#include "ebasdownload.hxx"
#include "inputstream.hxx"

Q_DECLARE_LOGGING_CATEGORY(log_component_generate_fromebas)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Transfer;

EBASDownload::EBASDownload() = default;

EBASDownload::~EBASDownload() = default;

static void setIfColumnDefined(const Variant::Read &config, QPair<int, int> &target)
{
    qint64 i = config["Begin"].toInt64();
    if (!INTEGER::defined(i) || i <= 0)
        return;
    qint64 j = config["End"].toInt64();
    if (!INTEGER::defined(j) || j <= 0)
        return;
    if (i > j)
        return;
    target.first = (int) i - 1;
    target.second = (int) j - 1;
}

static QSet<qint64> parseParameterSet(const Variant::Read &config)
{
    switch (config.getType()) {
    case Variant::Type::Integer:
        return QSet<qint64>() << config.toInt64();
    default: {
        QSet<qint64> result;
        for (auto v : config.toChildren()) {
            result |= v.toInt64();
        }
        return result;
    }
    }

    return QSet<qint64>();
}

static QString extractField(const QString &input, const QPair<int, int> &def)
{
    return input.mid(def.first, def.second - def.first + 1).trimmed().toLower();
}

static bool parseTimes(const QString &start,
                       const QString &end,
                       const QString &line,
                       bool disableTimeFallback,
                       QRegExp &re,
                       Time::Bounds &bounds)
{
    try {
        bounds = TimeParse::parseTimeBounds(start, end, true);
        return true;
    } catch (TimeParsingException tpe) {
        if (disableTimeFallback || re.indexIn(line) < 0) {
            qCDebug(log_component_generate_fromebas) << "Error parsing time bounds (" << start
                                                     << end << ") from" << line << ':'
                                                     << tpe.getDescription();
            return false;
        }
    }

    /* Try a fallback location if the bounds parse fails (due to overly
     * long columns, for example */
    try {
        bounds = TimeParse::parseTimeBounds(re.cap(1), re.cap(2), true);
        return true;
    } catch (TimeParsingException tpe) {
        qCDebug(log_component_generate_fromebas) << "Error parsing time bounds (" << re.cap(1)
                                                 << re.cap(2) << ") from" << line << ':'
                                                 << tpe.getDescription();
    }
    return false;
}

bool EBASDownload::convertListResponse(const Variant::Read &config,
                                       Transfer::DownloadFile &response,
                                       std::vector<std::unique_ptr<Transfer::DownloadFile>> &files)
{
    const auto &type = config["Convert"].toString();
    if (!type.empty() && !Util::equal_insensitive(type, "parameters", "parameter"))
        return FileDownloader::convertListResponse(config, response, files);

    auto inputFile = IO::Access::file(response.fileInfo(), IO::File::Mode::readOnly())->block();
    if (!inputFile) {
        qCWarning(log_component_generate_fromebas) << "Failed to open parameter list file";
        return false;
    }

    QPair<int, int> columnID(0, 9);
    QPair<int, int> columnStation(10, 17);
    QPair<int, int> columnStart(330, 348);
    QPair<int, int> columnEnd(351, 369);
    QPair<int, int> columnParameter(60, 200);
    QPair<int, int> columnMatrix(201, 241);
    QPair<int, int> columnInstrument(19, 58);

    setIfColumnDefined(config["Parse/ID"], columnID);
    setIfColumnDefined(config["Parse/Station"], columnStation);
    setIfColumnDefined(config["Parse/Start"], columnStart);
    setIfColumnDefined(config["Parse/End"], columnEnd);
    setIfColumnDefined(config["Parse/Parameter"], columnParameter);
    setIfColumnDefined(config["Parse/Matrix"], columnMatrix);
    setIfColumnDefined(config["Parse/Instrument"], columnInstrument);

    bool disableTimeFallback = config["Parse/DisableTimeFallback"].toBool();
    QRegExp reTimeFallback
            (R"((\d{4}-?\d{2}-?\d{2}T?\d{2}:?\d{2}:?\d{2}Z?)\s+(\d{4}-?\d{2}-?\d{2}T?\d{2}:?\d{2}:?\d{2}Z?))",
             Qt::CaseInsensitive);

    QByteArray data;

    typedef QPair<bool, QSet<qint64> > CandidateInfo;
    QList<CandidateInfo> candidateFiles;
    qint64 nLines = 0;
    while (auto rawLine = inputFile->readLine()) {
        if (testTerminated())
            return false;
        auto line = rawLine.toQString(false);

        ++nLines;
        bool ok = false;
        qint64 id = extractField(line, columnID).toLongLong(&ok);
        if (!ok || id <= 0)
            continue;

        Time::Bounds bounds;
        if (!parseTimes(extractField(line, columnStart), extractField(line, columnEnd), line,
                        disableTimeFallback, reTimeFallback, bounds))
            continue;

        QString station(extractField(line, columnStation).toUpper());
        QString matrix(extractField(line, columnMatrix));
        QString instrument(extractField(line, columnInstrument));
        QString parameter(extractField(line, columnParameter));
        if (!parameter.isEmpty()) {
            QStringList parameterComponents(parameter.split(',', QString::SkipEmptyParts));
            if (!parameterComponents.isEmpty()) {
                parameter = parameterComponents.at(0).toLower();
            }
        }

        int index = 0;
        for (QList<EBASDownloadSegment>::const_iterator check = candidates.constBegin(),
                endCheck = candidates.constEnd(); check != endCheck; ++check, ++index) {
            switch (check->matches(bounds, station, matrix, instrument, parameter)) {
            case EBASDownloadSegment::NoMatch:
                break;
            case EBASDownloadSegment::Primary:
                while (index >= candidateFiles.size()) {
                    candidateFiles.append(CandidateInfo(false, QSet<qint64>()));
                }
                candidateFiles[index].first = true;
                /* Fall through */
            case EBASDownloadSegment::Auxiliary:
                while (index >= candidateFiles.size()) {
                    candidateFiles.append(CandidateInfo(false, QSet<qint64>()));
                }
                candidateFiles[index].second |= id;
                break;
            }
        }
    }

    int index = 0;
    for (QList<EBASDownloadSegment>::const_iterator check = candidates.constBegin(),
            endCheck = candidates.constEnd(); check != endCheck; ++check, ++index) {
        if (index >= candidateFiles.size())
            break;
        if (!candidateFiles[index].first)
            continue;
        if (candidateFiles[index].second.isEmpty())
            continue;

        auto file = createParameterFetch(config["Fetch"], candidateFiles[index].second,
                                    check->getStart(), check->getEnd());
        if (!file)
            continue;

        resultLookup.insert(file.get(), index);
        files.emplace_back(std::move(file));
    }

    qCDebug(log_component_generate_fromebas) << "Parsed" << nLines << "lines into" << files.size()
                                             << "fetch request(s)";

    return true;
}

std::unique_ptr<
        DownloadFile> EBASDownload::createSingleFetch(const CPD3::Data::Variant::Read &config)
{
    const auto &type = config["Source"].toString();
    if (!type.empty() && !Util::equal_insensitive(type, "parameters", "parameter"))
        return FileDownloader::createSingleFetch(config);

    return createParameterFetch(config, parseParameterSet(config["Parameters"]), FP::undefined(),
                                FP::undefined());
}

static QString passwordOrPasswordFile(const Variant::Read &config, const QString &def = QString())
{
    QString password(config.hash("Password").toQString());
    if (password.isEmpty()) {
        QString fileName(config.hash("PasswordFile").toQString());
        if (!fileName.isEmpty() && QFile::exists(fileName)) {
            QFile file(fileName);
            if (file.open(QIODevice::ReadOnly)) {
                password = QString::fromUtf8(file.readAll());
                file.close();
            } else {
                qCDebug(log_component_generate_fromebas) << "Failed to open password file"
                                                         << fileName << ':' << file.errorString();
            }
        }
    }
    if (password.isEmpty())
        return def;
    return password;
}

std::unique_ptr<DownloadFile> EBASDownload::createParameterFetch(const Variant::Read &base,
                                                                 const QSet<qint64> &parameters,
                                                                 double start,
                                                                 double end)
{
    if (parameters.isEmpty())
        return {};

    QString username;
    if (base["EBAS/Username"].exists())
        username = applySubstitutions(base["Username"].toQString());
    QString password(passwordOrPasswordFile(base["EBAS"]));

    QByteArray postBody;
    QXmlStreamWriter xml(&postBody);
    xml.writeStartDocument();
    xml.writeCharacters(QString('\n'));
    xml.writeStartElement("S:Envelope");
    xml.writeAttribute("xmlns:S", "http://www.w3.org/2003/05/soap-envelope");

    QByteArray requestType;
    if (!username.isEmpty()) {
        requestType = "GetPublicOrPrivateDatasets";
    } else {
        requestType = "GetPublicDatasets";
    }

    xml.writeStartElement("S:Header");
    xml.writeStartElement("To");
    xml.writeAttribute("xmlns", "http://www.w3.org/2005/08/addressing");
    xml.writeCharacters("http://prod-web-actris.nilu.no/ebas_ws/DataSetService.svc");
    xml.writeEndElement();
    xml.writeStartElement("Action");
    xml.writeAttribute("xmlns", "http://www.w3.org/2005/08/addressing");
    xml.writeCharacters("http://ebas.nilu.no/services/IEbasDataSetService/GetPublicDatasets");
    xml.writeEndElement();
    xml.writeStartElement("ReplyTo");
    xml.writeAttribute("xmlns", "http://www.w3.org/2005/08/addressing");
    xml.writeStartElement("Address");
    xml.writeCharacters("http://www.w3.org/2005/08/addressing/anonymous");
    xml.writeEndElement();
    xml.writeEndElement();
    xml.writeStartElement("MessageID");
    xml.writeAttribute("xmlns", "http://www.w3.org/2005/08/addressing");
    {
        QString uid(QUuid::createUuid().toString());
        uid = uid.mid(1);
        uid.chop(1);
        xml.writeCharacters(uid);
    }
    xml.writeEndElement();
    xml.writeEndElement();

    xml.writeStartElement("S:Body");
    xml.writeStartElement("ns2:" + requestType);
    xml.writeAttribute("xmlns", "http://schemas.microsoft.com/2003/10/Serialization/Arrays");
    xml.writeAttribute("xmlns:ns2", "http://ebas.nilu.no/services");
    xml.writeAttribute("xmlns:ns3", "http://schemas.microsoft.com/2003/10/Serialization/");

    if (!username.isEmpty()) {
        xml.writeStartElement("ns2:userName");
        xml.writeCharacters(username);
        xml.writeEndElement();

        if (!password.isEmpty()) {
            xml.writeStartElement("ns2:password");
            xml.writeCharacters(username);
            xml.writeEndElement();
        }
    }

    xml.writeStartElement("ns2:keys");
    for (QSet<qint64>::const_iterator key = parameters.constBegin(), endK = parameters.constEnd();
            key != endK;
            ++key) {
        xml.writeStartElement("string");
        xml.writeCharacters(QString::number(*key));
        xml.writeEndElement();
    }
    xml.writeEndElement();

    if (FP::defined(start)) {
        xml.writeStartElement("ns2:dateFrom");
        xml.writeCharacters(Time::toISO8601(start, true));
        xml.writeEndElement();
    }

    if (FP::defined(end)) {
        xml.writeStartElement("ns2:dateTo");
        xml.writeCharacters(Time::toISO8601(end, true));
        xml.writeEndElement();
    }

    /* Request (Get...Datasets) */
    xml.writeEndElement();

    /* Body */
    xml.writeEndElement();

    /* Envelope */
    xml.writeEndElement();
    xml.writeEndDocument();

    qCDebug(log_component_generate_fromebas) << "Issuing request for parameter(s):" << parameters;

    Variant::Root config(base);
    if (!config["Source"].exists())
        config["Source"].setString("POST");
    if (!config["File"].exists())
        config["File"].setString("http://prod-web-actris.nilu.no/ebas_ws/DataSetService.svc");
    if (!config["Data"].exists())
        config["Data"].setBinary(postBody);

    auto file = createSingleFetch(config);
    if (auto url = file->getURL()) {
        url->header(QNetworkRequest::ContentTypeHeader,
                    "application/soap+xml;charset=utf-8;action=\"http://ebas.nilu.no/services/IEbasDataSetService/" +
                            requestType +
                            "\"");
    }
    return std::move(file);
}

QList<EBASInputStream *> EBASDownload::createStreams(GenerateFromEBAS *parent)
{
    auto files = takeFiles();
    QList<EBASInputStream *> result;
    for (auto &f : files) {
        int idx = resultLookup.value(f.get(), -1);
        if (idx < 0 || idx >= candidates.size()) {
            continue;
        }

        EBASInputStream *stream = candidates.at(idx).create(std::move(f), parent);
        if (!stream) {
            continue;
        }
        result.append(stream);
    }
    return result;
}

void EBASDownload::setSegments(const QList<EBASDownloadSegment> &set)
{
    candidates = set;
    resultLookup.clear();
}

static QSet<QString> parseMatchers(const Variant::Read &config)
{
    QSet<QString> result;
    for (const auto &add : config.toChildren().keys()) {
        result.insert(QString::fromStdString(add).toLower());
    }
    return result;
}


EBASDownloadSegment::EBASDownloadSegment(double start,
                                         double end,
                                         const SequenceName::Component &station,
                                         const std::string &profile,
                                         const Variant::Read &config,
                                         const SequenceSegment::Transfer &globalConfig,
                                         const QString &ebasStation) : start(start),
                                                                       end(end),
                                                                       station(station),
                                                                       profile(profile),
                                                                       config(config),
                                                                       globalConfig(globalConfig),
                                                                       ebasStation(ebasStation),
                                                                       acceptParameters(
                                                                               parseMatchers(
                                                                                       config["Parameter"])),
                                                                       acceptMatrices(parseMatchers(
                                                                               config["Matrix"])),
                                                                       acceptInstruments(
                                                                               parseMatchers(
                                                                                       config["Instrument"])),
                                                                       auxiliaryParameters(
                                                                               parseMatchers(
                                                                                       config["Auxiliary/Parameter"])),
                                                                       auxiliaryMatrices(
                                                                               parseMatchers(
                                                                                       config["Auxiliary/Matrix"])),
                                                                       auxiliaryInstruments(
                                                                               parseMatchers(
                                                                                       config["Auxiliary/Instrument"]))
{

}

EBASDownloadSegment::MatchResult EBASDownloadSegment::matches(const Time::Bounds &bounds,
                                                              const QString &station,
                                                              const QString &matrix,
                                                              const QString &instrument,
                                                              const QString &parameter) const
{
    if (!Range::intersects(bounds.getStart(), bounds.getEnd(), start, end))
        return NoMatch;
    if (!this->ebasStation.isEmpty() && this->ebasStation != station)
        return NoMatch;

    bool isAux = false;

    if (!auxiliaryMatrices.isEmpty() && !acceptMatrices.contains(matrix)) {
        if (auxiliaryMatrices.contains(matrix)) {
            isAux = true;
        } else {
            return NoMatch;
        }
    }

    if (!acceptInstruments.isEmpty() && !acceptInstruments.contains(instrument)) {
        if (auxiliaryInstruments.contains(instrument)) {
            isAux = true;
        } else {
            return NoMatch;
        }
    }

    if (!acceptParameters.isEmpty() && !acceptParameters.contains(parameter)) {
        if (auxiliaryParameters.contains(parameter)) {
            isAux = true;
        } else {
            return NoMatch;
        }
    }

    return isAux ? Auxiliary : Primary;
}

EBASInputStream *EBASDownloadSegment::create(std::unique_ptr<CPD3::Transfer::DownloadFile> &&file,
                                             GenerateFromEBAS *parent) const
{
    return new EBASInputStream(parent, std::move(file), globalConfig, station, profile);
}





