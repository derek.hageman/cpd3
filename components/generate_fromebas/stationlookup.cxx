/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QStringList>
#include <QLoggingCategory>

#include "core/csv.hxx"
#include "io/drivers/file.hxx"

#include "stationlookup.hxx"
#include "generate_fromebas.hxx"

Q_DECLARE_LOGGING_CATEGORY(log_component_generate_fromebas)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Transfer;

EBASStationLookup::EBASStationLookup()
{ }

EBASStationLookup::~EBASStationLookup()
{ }

EBASStationLookupConstant::EBASStationLookupConstant(const QString &stn) : station(stn.toUpper())
{ }

EBASStationLookupConstant::~EBASStationLookupConstant()
{ }

QString EBASStationLookupConstant::toEBASStation(const SequenceName::Component &gawStation)
{
    Q_UNUSED(gawStation);
    return station;
}

void EBASStationLookupConstant::acquire(CPD3::Transfer::FileDownloader *downloader)
{ }


EBASStationLookupDownload::EBASStationLookupDownload(const Variant::Read &cfg) : ready(false),
                                                                                 lookup(),
                                                                                 config(cfg),
                                                                                 idxGAW(0),
                                                                                 idxEBAS(1)
{
    qint64 i = config["GAW"].toInt64();
    if (INTEGER::defined(i) && i > 0)
        idxGAW = (int) i;

    i = config["EBAS"].toInt64();
    if (INTEGER::defined(i) && i > 0)
        idxEBAS = (int) i;
}

EBASStationLookupDownload::~EBASStationLookupDownload()
{
}

QString EBASStationLookupDownload::toEBASStation(const SequenceName::Component &gawStation)
{
    return lookup.value(QString::fromStdString(gawStation).toLower());
}

void EBASStationLookupDownload::acquire(CPD3::Transfer::FileDownloader *downloader)
{
    if (ready)
        return;
    ready = true;

    if (!downloader->exec(config["Origin"]))
        return;

    auto files = downloader->takeFiles();
    for (const auto &dl : files) {
        auto file = IO::Access::file(dl->fileInfo())->block();
        if (!file) {
            qCWarning(log_component_generate_fromebas) << "Failed to open station list file";
            continue;
        }

        while (auto line = file->readLine()) {
            QStringList fields(line.toQString().split(QRegExp("[\\s,]+"), QString::SkipEmptyParts));

            if (fields.size() <= idxGAW)
                continue;
            QString gawStation(fields.at(idxGAW));

            if (fields.size() <= idxEBAS)
                continue;
            QString ebasStation(fields.at(idxEBAS));

            lookup.insert(gawStation.toLower(), ebasStation.toUpper());
        }
    }
}
