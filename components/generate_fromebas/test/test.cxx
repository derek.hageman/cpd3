/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QFile>
#include <QTemporaryFile>

#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"

using namespace CPD3;
using namespace CPD3::Data;

//#define TEST_RETRIEVE

class TestComponent : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;
    QTemporaryFile stationFile;
    QTemporaryFile parameterFile;

    ExternalSourceComponent *component;

    static bool hasUnit(const SequenceValue::Transfer &values, const SequenceName &unit)
    {
        for (const auto &v : values) {
            if (v.getUnit() == unit)
                return true;
        }
        return false;
    }

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component = qobject_cast<ExternalSourceComponent *>(
                ComponentLoader::create("generate_fromebas"));
        QVERIFY(component);

        QVERIFY(databaseFile.open());
        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());

        QVERIFY(stationFile.open());
        QVERIFY(parameterFile.open());
    }

    void init()
    {
        databaseFile.resize(0);
        stationFile.resize(0);
        parameterFile.resize(0);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("profile")));
    }

#ifdef TEST_RETRIEVE

    void retrieve()
    {
        Variant::Root config;

        config["Profiles/aerosol/Station/Origin/Type"] = "Single";
        config["Profiles/aerosol/Station/Origin/Source"] = "Local";
        config["Profiles/aerosol/Station/Origin/File"] = stationFile.fileName();
        config["Profiles/aerosol/Fetch/#0/Origin/Type"] = "List";
        config["Profiles/aerosol/Fetch/#0/Origin/Source"] = "Local";
        config["Profiles/aerosol/Fetch/#0/Origin/File"] = parameterFile.fileName();
        config["Profiles/aerosol/Fetch/#0/Parameter"] = "aerosol_light_scattering_coefficient";
        config["Profiles/aerosol/Fetch/#0/Matrix"] = "pm1";

        config["Profiles/aerosol/Groups/Bs/Match/Type"] = "And";
        config["Profiles/aerosol/Groups/Bs/Match/Components/#0/Type"] = "Text";
        config["Profiles/aerosol/Groups/Bs/Match/Components/#0/Left/Key"] = "Component";
        config["Profiles/aerosol/Groups/Bs/Match/Components/#0/Right/Constant"] =
                "aerosol_light_scattering_coefficient";
        config["Profiles/aerosol/Groups/Bs/Match/Components/#1/Type"] = "Defined";
        config["Profiles/aerosol/Groups/Bs/Match/Components/#1/Value/Key"] = "Quantile";
        config["Profiles/aerosol/Groups/Bs/Match/Components/#1/Invert"] = true;
        config["Profiles/aerosol/Groups/Bs/Data/Type"] = "Statistics";
        config["Profiles/aerosol/Groups/Bs/Data/Variable/Convert"] =
                "Bs${WAVELENGTHCODE}_${SUFFIX}";
        config["Profiles/aerosol/Groups/Bs/Data/Flavors/Key"] = "Matrix";
        config["Profiles/aerosol/Groups/Bs/Data/Flavors/Convert/Type"] = "MatrixFlavors";
        config["Profiles/aerosol/Groups/Bs/Data/Metadata/Base/*dDescription"] =
                "Aerosol light scattering coefficient";
        config["Profiles/aerosol/Groups/Bs/Data/Metadata/Overlay/Wavelength/Path"] = "^Wavelength";
        config["Profiles/aerosol/Groups/Bs/Data/Metadata/Overlay/Wavelength/Key"] = "Wavelength";
        config["Profiles/aerosol/Groups/Bs/Data/Metadata/Overlay/ReportT/Path"] = "^ReportT";
        config["Profiles/aerosol/Groups/Bs/Data/Metadata/Overlay/ReportT/Key"] = "ReportT";
        config["Profiles/aerosol/Groups/Bs/Data/Metadata/Overlay/Units/Path"] = "^Units";
        config["Profiles/aerosol/Groups/Bs/Data/Metadata/Overlay/Units/Key"] = "Units";
        config["Profiles/aerosol/Groups/Bs/Auxiliary/Quantile/Match/Type"] = "And";
        config["Profiles/aerosol/Groups/Bs/Auxiliary/Quantile/Match/Components/#0/Type"] = "Text";
        config["Profiles/aerosol/Groups/Bs/Auxiliary/Quantile/Match/Components/#0/Left/Key"] =
                "Component";
        config["Profiles/aerosol/Groups/Bs/Auxiliary/Quantile/Match/Components/#0/Right/Constant"] =
                "aerosol_light_scattering_coefficient";
        config["Profiles/aerosol/Groups/Bs/Auxiliary/Quantile/Match/Components/#1/Type"] =
                "Defined";
        config["Profiles/aerosol/Groups/Bs/Auxiliary/Quantile/Match/Components/#1/Value/Key"] =
                "Quantile";
        config["Profiles/aerosol/Groups/Bs/Auxiliary/Quantile/Match/Components/#2/Type"] = "Exact";
        config["Profiles/aerosol/Groups/Bs/Auxiliary/Quantile/Match/Components/#2/Left/Key/Reference/Key"] =
                "Wavelength";
        config["Profiles/aerosol/Groups/Bs/Auxiliary/Quantile/Match/Components/#2/Right/Key"] =
                "Wavelength";
        config["Profiles/aerosol/Groups/Bs/Auxiliary/Quantile/Name/Key"] = "Quantile";
        config["Profiles/aerosol/Groups/Bs/Auxiliary/Quantile/Name/Convert/Type"] = "Format";
        config["Profiles/aerosol/Groups/Bs/Auxiliary/Quantile/Name/Convert/Format"] = "000.00000";

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"_", "configuration", "import_ebas"}, config)});
        {
            QFile file(stationFile.fileName());
            QVERIFY(file.open(QIODevice::WriteOnly | QIODevice::Truncate));
            file.write("BND US0035R");
            file.close();
        }
        {
            QFile file(parameterFile.fileName());
            QVERIFY(file.open(QIODevice::WriteOnly | QIODevice::Truncate));
            file.write(
                    " 215140975 US0035R nephelometer                             aerosol_light_scattering_coefficient,Wavelength=450.0 nm                                                                                     pm1                                      US06L_TSI_3563_BND_pm1                   US06L_scat_coef                          1h    2014-01-01T00:00:00  2015-01-01T00:00:00 GAW-WDCA,NOAA-ESRL\n"
                    " 215140976 US0035R nephelometer                             aerosol_light_scattering_coefficient,Wavelength=550.0 nm                                                                                     pm1                                      US06L_TSI_3563_BND_pm1                   US06L_scat_coef                          1h    2014-01-01T00:00:00  2015-01-01T00:00:00 GAW-WDCA,NOAA-ESRL\n"
                    " 215140977 US0035R nephelometer                             aerosol_light_scattering_coefficient,Wavelength=700.0 nm                                                                                     pm1                                      US06L_TSI_3563_BND_pm1                   US06L_scat_coef                          1h    2014-01-01T00:00:00  2015-01-01T00:00:00 GAW-WDCA,NOAA-ESRL\n"
                    " 215140981 US0035R nephelometer                             aerosol_light_scattering_coefficient,Statistics=percentile:15.87,Wavelength=450.0 nm                                                         pm1                                      US06L_TSI_3563_BND_pm1                   US06L_scat_coef                          1h    2014-01-01T00:00:00  2015-01-01T00:00:00 GAW-WDCA,NOAA-ESRL\n"
                    " 215140982 US0035R nephelometer                             aerosol_light_scattering_coefficient,Statistics=percentile:15.87,Wavelength=550.0 nm                                                         pm1                                      US06L_TSI_3563_BND_pm1                   US06L_scat_coef                          1h    2014-01-01T00:00:00  2015-01-01T00:00:00 GAW-WDCA,NOAA-ESRL\n"
                    " 215140983 US0035R nephelometer                             aerosol_light_scattering_coefficient,Statistics=percentile:15.87,Wavelength=700.0 nm                                                         pm1                                      US06L_TSI_3563_BND_pm1                   US06L_scat_coef                          1h    2014-01-01T00:00:00  2015-01-01T00:00:00 GAW-WDCA,NOAA-ESRL\n"
                    " 215140987 US0035R nephelometer                             aerosol_light_scattering_coefficient,Statistics=percentile:84.13,Wavelength=450.0 nm                                                         pm1                                      US06L_TSI_3563_BND_pm1                   US06L_scat_coef                          1h    2014-01-01T00:00:00  2015-01-01T00:00:00 GAW-WDCA,NOAA-ESRL\n"
                    " 215140988 US0035R nephelometer                             aerosol_light_scattering_coefficient,Statistics=percentile:84.13,Wavelength=550.0 nm                                                         pm1                                      US06L_TSI_3563_BND_pm1                   US06L_scat_coef                          1h    2014-01-01T00:00:00  2015-01-01T00:00:00 GAW-WDCA,NOAA-ESRL\n"
                    " 215140989 US0035R nephelometer                             aerosol_light_scattering_coefficient,Statistics=percentile:84.13,Wavelength=700.0 nm                                                         pm1                                      US06L_TSI_3563_BND_pm1                   US06L_scat_coef                          1h    2014-01-01T00:00:00  2015-01-01T00:00:00 GAW-WDCA,NOAA-ESRL\n"
                    " 215140993 US0035R nephelometer                             pressure,Location=instrument internal                                                                                                        instrument                               US06L_TSI_3563_BND_pm10                  US06L_scat_coef                          1h    2014-01-01T00:00:00  2015-01-01T00:00:00 GAW-WDCA,NOAA-ESRL\n"
                    " 215140995 US0035R nephelometer                             relative_humidity,Location=instrument internal                                                                                               instrument                               US06L_TSI_3563_BND_pm10                  US06L_scat_coef                          1h    2014-01-01T00:00:00  2015-01-01T00:00:00 GAW-WDCA,NOAA-ESRL\n"
                    " 215140994 US0035R nephelometer                             temperature,Location=instrument internal                                                                                                     instrument                               US06L_TSI_3563_BND_pm10                  US06L_scat_coef                          1h    2014-01-01T00:00:00  2015-01-01T00:00:00 GAW-WDCA,NOAA-ESRL\n"
                    " 215140996 US0035R nephelometer                             aerosol_light_scattering_coefficient,Wavelength=450.0 nm                                                                                     pm10                                     US06L_TSI_3563_BND_pm10                  US06L_scat_coef                          1h    2014-01-01T00:00:00  2015-01-01T00:00:00 GAW-WDCA,NOAA-ESRL\n"
                    " 215140997 US0035R nephelometer                             aerosol_light_scattering_coefficient,Wavelength=550.0 nm                                                                                     pm10                                     US06L_TSI_3563_BND_pm10                  US06L_scat_coef                          1h    2014-01-01T00:00:00  2015-01-01T00:00:00 GAW-WDCA,NOAA-ESRL\n"
                    " 215140998 US0035R nephelometer                             aerosol_light_scattering_coefficient,Wavelength=700.0 nm                                                                                     pm10                                     US06L_TSI_3563_BND_pm10                  US06L_scat_coef                          1h    2014-01-01T00:00:00  2015-01-01T00:00:00 GAW-WDCA,NOAA-ESRL\n");
            file.close();
        }

        ComponentOptions options(component->getOptions());
        ExternalConverter *input = component->createExternalIngress(options, 1398902400, 1398988800,
                                                                    {"bnd"});

        input->start();
        StreamSink::Buffer output;
        input->setEgress(&output);
        QVERIFY(input->wait(300000));
        delete input;

        QVERIFY(output.ended());

        SequenceName::Flavors pm1{SequenceName::flavor_pm1};
        SequenceName::Flavors pm10{SequenceName::flavor_pm10};

        QVERIFY(hasUnit(output.values(), SequenceName("bnd", "raw_meta", "BsB_X1", pm1)));
        QVERIFY(hasUnit(output.values(), SequenceName("bnd", "raw_meta", "BsG_X1", pm1)));
        QVERIFY(hasUnit(output.values(), SequenceName("bnd", "raw_meta", "BsR_X1", pm1)));
        QVERIFY(hasUnit(output.values(), SequenceName("bnd", "raw", "BsB_X1", pm1)));
        QVERIFY(hasUnit(output.values(), SequenceName("bnd", "raw", "BsG_X1", pm1)));
        QVERIFY(hasUnit(output.values(), SequenceName("bnd", "raw", "BsR_X1", pm1)));

        QVERIFY(!hasUnit(output.values(), SequenceName("bnd", "raw", "T_X1", pm1)));
        QVERIFY(!hasUnit(output.values(), SequenceName("bnd", "raw", "BsB_X1", pm10)));
        QVERIFY(!hasUnit(output.values(), SequenceName("bnd", "raw", "BsG_X1", pm10)));
        QVERIFY(!hasUnit(output.values(), SequenceName("bnd", "raw", "BsR_X1", pm10)));
    }

#endif
};

QTEST_MAIN(TestComponent)

#include "test.moc"
