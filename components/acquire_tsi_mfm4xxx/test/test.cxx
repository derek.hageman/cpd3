/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <cstdint>
#include <cmath>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>
#include <QtEndian>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    double unpolledRemaining;
    double polledInterval;
    int pollCount;
    bool outputBinary;
    enum {
        Field_Q = 0x01, Field_T = 0x02, Field_P = 0x04,
    };
    std::uint_fast32_t outputFields;

    double Q;
    double T;
    double P;

    ModelInstrument()
            : incoming(),
              outgoing(), unpolledRemaining(FP::undefined()),
              polledInterval(1.0),
              pollCount(0),
              outputBinary(false),
              outputFields(0xFFFFFFFF)
    {
        Q = 12.0 / (((273.15 + 21.11) / 273.15) * (1013.25 / 1013.0));
        T = 23.1;
        P = 850.2;
    }

    bool pollCommand(QByteArray line)
    {
        if (line.at(0) != 'D') return false;
        line = line.mid(1);

        switch (line.at(0)) {
        case 'A':
        case 'C':
            outputBinary = false;
            break;
        case 'B':
            outputBinary = true;
            break;
        default:
            return false;
        }
        line = line.mid(1);

        switch (line.at(0)) {
        case 'F':
            outputFields |= Field_Q;
            break;
        case 'x':
            outputFields &= ~static_cast<std::uint_fast32_t>(Field_Q);
            break;
        default:
            return false;
        }
        line = line.mid(1);

        switch (line.at(0)) {
        case 'T':
            outputFields |= Field_T;
            break;
        case 'x':
            outputFields &= ~static_cast<std::uint_fast32_t>(Field_T);
            break;
        default:
            return false;
        }
        line = line.mid(1);

        switch (line.at(0)) {
        case 'P':
            outputFields |= Field_P;
            break;
        case 'x':
            outputFields &= ~static_cast<std::uint_fast32_t>(Field_P);
            break;
        default:
            return false;
        }
        line = line.mid(1);

        bool ok = false;
        int n = line.toInt(&ok);
        if (!ok || n <= 0 || n > 1000) return false;

        pollCount = n;
        unpolledRemaining = polledInterval;
        return true;
    }

    void appendBinary(std::uint_fast16_t value)
    {
        int oldSize = outgoing.size();
        outgoing.resize(oldSize + 2);
        uchar *target = reinterpret_cast<uchar *>(outgoing.data()) + oldSize;
        qToBigEndian<quint16>(static_cast<quint16>(value), target);
    }

    void appendBinarySigned(std::int_fast16_t value)
    {
        appendBinary(static_cast<std::uint_fast16_t>(value));
    }

    double outputFlow() const
    {
        return Q * ((273.15 + 21.11) / 273.15) * (1013.25 / 1013.0);
    }

    void advance(double seconds)
    {
        int idxCR = -1;
        while ((idxCR = incoming.indexOf('\r')) >= 0) {
            QByteArray line(incoming.mid(0, idxCR));

            if (line == "CBT") {
                outgoing.append("OK\r\n");
            } else if (line == "CET") {
                outgoing.append("OK\r\n");
            } else if (line == "SUS" || line == "SUV") {
                outgoing.append("OK\r\n");
            } else if (line == "SN") {
                outgoing.append("OK\r\n40409806004\r\n");
            } else if (line == "MN") {
                outgoing.append("OK\r\n4040\r\n");
            } else if (line == "REV") {
                outgoing.append("OK\r\n1.3\r\n");
            } else if (line == "DATE") {
                outgoing.append("OK\r\n12/24/98\r\n");
            } else if (line.startsWith("SSR")) {
                bool ok = false;
                int n = line.mid(3).toInt(&ok);
                if (ok && n >= 1 && n <= 1000) {
                    polledInterval = static_cast<double>(n) / 1000.0;
                    outgoing.append("OK\r\n");
                } else {
                    outgoing.append("ERR1\r\n");
                }
            } else if (line.startsWith("D") && line.length() >= 9) {
                if (!pollCommand(line)) {
                    if (outputBinary) {
                        outgoing.append(0x01);
                    } else {
                        outgoing.append("ERR1\r\n");
                    }
                } else {
                    if (!outputBinary) {
                        outgoing.append("OK\r\n");
                    }
                }
            }

            if (idxCR >= incoming.size() - 1) {
                incoming.clear();
                break;
            }
            incoming = incoming.mid(idxCR + 1);
        }

        if (FP::defined(unpolledRemaining)) {
            unpolledRemaining -= seconds;
            while (unpolledRemaining <= 0.0) {
                if (outputBinary) {
                    outgoing.append((char) 0x00);
                    if (outputFields & Field_Q)
                        appendBinary(
                                static_cast<std::uint_fast16_t>(std::round(outputFlow() * 100.0)));
                    if (outputFields & Field_T)
                        appendBinarySigned(static_cast<std::int_fast16_t>(std::round(T * 100.0)));
                    if (outputFields & Field_P)
                        appendBinary(static_cast<std::uint_fast16_t>(std::round(P * 10.0)));
                    outgoing.append((char) 0xFF);
                    outgoing.append((char) 0xFF);
                } else {
                    outgoing.append(QByteArray::number(outputFlow(), 'f', 2));
                    outgoing.append(',');
                    outgoing.append(QByteArray::number(T, 'f', 2));
                    outgoing.append(',');
                    outgoing.append(QByteArray::number(P / 10.0, 'f', 3));
                    outgoing.append("\r\n");
                }

                if (pollCount > 0) {
                    --pollCount;
                    if (pollCount <= 0) {
                        unpolledRemaining = FP::undefined();
                        break;
                    }
                }
                unpolledRemaining += polledInterval;
            }
        }
    }
};


class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Q", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("P", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("ZSTATE", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkContiguous(StreamCapture &stream)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("Q"))
            return false;
        if (!stream.checkContiguous("T"))
            return false;
        if (!stream.checkContiguous("P"))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream, const ModelInstrument &model,
                     double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("Q", Variant::Root(model.Q), time))
            return false;
        if (!stream.hasAnyMatchingValue("T", Variant::Root(model.T), time))
            return false;
        if (!stream.hasAnyMatchingValue("P", Variant::Root(model.P), time))
            return false;
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream, const ModelInstrument &model,
                             double time = FP::undefined())
    {
        Q_UNUSED(stream);
        Q_UNUSED(model);
        Q_UNUSED(time);
        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_tsi_mfm4xxx"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_tsi_mfm4xxx"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = 0;
        instrument.pollCount = -1;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 50; i++) {
            control.advance(0.25);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = FP::undefined();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 20; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = 0;
        instrument.pollCount = -1;
        instrument.outputBinary = true;

        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.5);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        control.advance(0.1);

        for (int i = 0; i < 20; i++) {
            control.advance(0.25);
            QTest::qSleep(50);
        }

        QVERIFY(logging.hasAnyMatchingValue("F1"));

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = FP::undefined();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);

        double checkTime = control.time();
        while (control.time() < 120.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        control.advance(0.125);
        checkTime = control.time();
        while (control.time() < 120.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);

        checkTime = control.time();
        while (control.time() < 120.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);

        checkTime = control.time();
        while (control.time() < 120.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

};

QTEST_MAIN(TestComponent)

#include "test.moc"
