/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIRETSIMFM4XXX_H
#define ACQUIRETSIMFM4XXX_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"

class AcquireTSIMFM4xxx : public CPD3::Acquisition::FramedInstrument {
    double lastRecordTime;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum {
        /* Passive autoprobe initialization, ignorging the first timeout */
        RESP_AUTOPROBE_PASSIVE_INITIALIZE,
        /* Waiting for a valid record to confirm passive mode autoprobe */
        RESP_AUTOPROBE_PASSIVE_WAIT,

        /* Waiting for a valid record to start passive acquisition */
        RESP_PASSIVE_WAIT,

        /* Acquiring data in passive mode */
        RESP_PASSIVE_RUN,

        /* Acquiring data in interactive mode, waiting for the "OK" response
         * to a query. */
        RESP_INTERACTIVE_RUN_OK,
        /* Acquiring data in interactive mode, reading the response to a
         * query. */
        RESP_INTERACTIVE_RUN_RESPONSE,

        /* Waiting for the completion of the "DAFxx0001" command during start
         * communications. */
        RESP_INTERACTIVE_START_STOPREPORTS_INITIAL,
        /* And again, because it doesn't always respond right away. */
        RESP_INTERACTIVE_START_STOPREPORTS_AGAIN,
        /* Waiting for the result of the "MN" command to read the model
         * number. */
        RESP_INTERACTIVE_START_READMODEL,
        /* Waiting for the result of the "SN" command to read the serial
         * number. */
        RESP_INTERACTIVE_START_READSERIAL,
        /* Waiting for the result of the "REV" command to read the firmware
         * version. */
        RESP_INTERACTIVE_START_READFIRMWARE,
        /* Waiting for the result of the "DATE" command to read the calibration
         * date. */
        RESP_INTERACTIVE_START_READCAL,
        /* Waiting for the completion of the "SUS" command during start
         * communications to change to mass flow mode. */
        RESP_INTERACTIVE_START_SETMASS,
        /* Waiting for the completion of the "CBT" command during start
         * communications to clear the begin trigger. */
        RESP_INTERACTIVE_START_DISABLEBEGINTRIGGER,
        /* Waiting for the completion of the "CET" command during start
         * communications to clear the end trigger. */
        RESP_INTERACTIVE_START_DISABLEENDTRIGGER,
        /* Waiting for the completion of the "SSRnnnn" command during start
         * communications to set the sample rate. */
        RESP_INTERACTIVE_START_SETSAMPLERATE,


        /* Waiting for the first valid record */
        RESP_INTERACTIVE_START_FIRSTVALID,

        /* Waiting before attempting a communications restart */
        RESP_INTERACTIVE_RESTART_WAIT,
        /* Initial state for interactive start */
        RESP_INTERACTIVE_INITIALIZE,
    } responseState;
    int autoprobeValidRecords;

    class Configuration {
        double start;
        double end;

    public:
        double sampleTime;
        bool strictMode;
        double binaryFlowScale;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under,
                      const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    CPD3::Data::Variant::Root instrumentMeta;
    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;

    double binaryFlowConversion;
    bool extendedQuery;

    bool realtimeStateUpdated;


    void setDefaultInvalid();

    void logValue(double startTime,
                  double endTime,
                  CPD3::Data::SequenceName::Component name,
                  CPD3::Data::Variant::Root &&value);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root &&value);

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    void invalidateLogValues(double frameTime);

    int processRecord(const CPD3::Util::ByteView &frame, double startTime, double endTime);

public:
    AcquireTSIMFM4xxx(const CPD3::Data::ValueSegment::Transfer &config,
                      const std::string &loggingContext);

    AcquireTSIMFM4xxx(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireTSIMFM4xxx();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus() override;

    CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables() override;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus() override;

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    std::size_t dataFrameStart(std::size_t offset,
                               const CPD3::Util::ByteArray &input) const override;

    std::size_t dataFrameEnd(std::size_t start, const CPD3::Util::ByteArray &input) const override;

    void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingInstrumentTimeout(double time) override;

    void discardDataCompleted(double time) override;
};

class AcquireTSIMFM4xxxComponent
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_tsi_mfm4xxx"
                              FILE
                              "acquire_tsi_mfm4xxx.json")

    CPD3::ComponentOptions getBaseOptions();

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
