/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QtEndian>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_tsi_mfm4xxx.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

AcquireTSIMFM4xxx::Configuration::Configuration() : start(FP::undefined()),
                                                    end(FP::undefined()),
                                                    sampleTime(1.0),
                                                    strictMode(true),
                                                    binaryFlowScale(FP::undefined())
{ }

AcquireTSIMFM4xxx::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          sampleTime(other.sampleTime),
          strictMode(other.strictMode),
          binaryFlowScale(other.binaryFlowScale)
{ }

void AcquireTSIMFM4xxx::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("TSI");
    instrumentMeta["Model"].setString("4xxx");

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    realtimeStateUpdated = true;

    binaryFlowConversion = 100.0;
    extendedQuery = false;
}

AcquireTSIMFM4xxx::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s), end(e), sampleTime(1.0), strictMode(true), binaryFlowScale(FP::undefined())
{
    setFromSegment(other);
}

AcquireTSIMFM4xxx::Configuration::Configuration(const Configuration &under,
                                                const ValueSegment &over,
                                                double s,
                                                double e) : start(s),
                                                            end(e),
                                                            sampleTime(under.sampleTime),
                                                            strictMode(under.strictMode),
                                                            binaryFlowScale(under.binaryFlowScale)
{
    setFromSegment(over);
}

void AcquireTSIMFM4xxx::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["StrictMode"].exists())
        strictMode = config["StrictMode"].toBool();

    {
        double d = config["SampleTime"].toDouble();
        if (FP::defined(d) && d > 0.0)
            sampleTime = d;
    }

    if (FP::defined(config["BinaryFlowScale"].toDouble()))
        binaryFlowScale = config["BinaryFlowScale"].toDouble();
}

AcquireTSIMFM4xxx::AcquireTSIMFM4xxx(const ValueSegment::Transfer &configData,
                                     const std::string &loggingContext) : FramedInstrument(
        "tsi4xxx", loggingContext),
                                                                          lastRecordTime(
                                                                                  FP::undefined()),
                                                                          autoprobeStatus(
                                                                                  AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                          responseState(
                                                                                  RESP_PASSIVE_WAIT),
                                                                          autoprobeValidRecords(0)
{
    setDefaultInvalid();
    config.append(Configuration());

    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
}

void AcquireTSIMFM4xxx::setToAutoprobe()
{
    responseState = RESP_AUTOPROBE_PASSIVE_INITIALIZE;
    autoprobeValidRecords = 0;
}

void AcquireTSIMFM4xxx::setToInteractive()
{ responseState = RESP_INTERACTIVE_INITIALIZE; }


ComponentOptions AcquireTSIMFM4xxxComponent::getBaseOptions()
{
    ComponentOptions options;

    ComponentOptionSingleDouble *d = new ComponentOptionSingleDouble(tr("binary-flow", "name"),
                                                                     tr("Binary data flow scalar"),
                                                                     tr("This is the constant scalar used to convert binary flow "
                                                                        "measurements to lpm units.  For 4000 series meters this "
                                                                        "should be 100 and for 4100 series meters it should be 1000."),
                                                                     tr("100.0",
                                                                        "default flow scale"));
    d->setMinimum(0.0, false);
    options.add("binary-flow", d);

    return options;
}

AcquireTSIMFM4xxx::AcquireTSIMFM4xxx(const ComponentOptions &options,
                                     const std::string &loggingContext) : FramedInstrument(
        "tsi4xxx", loggingContext),
                                                                          lastRecordTime(
                                                                                  FP::undefined()),
                                                                          autoprobeStatus(
                                                                                  AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                          responseState(
                                                                                  RESP_PASSIVE_WAIT),
                                                                          autoprobeValidRecords(0)
{
    setDefaultInvalid();
    config.append(Configuration());
    config.first().strictMode = false;

    if (options.isSet("binary-flow")) {
        config.first().binaryFlowScale =
                qobject_cast<ComponentOptionSingleDouble *>(options.get("binary-flow"))->get();
    }
}

AcquireTSIMFM4xxx::~AcquireTSIMFM4xxx()
{
}

void AcquireTSIMFM4xxx::logValue(double startTime,
                                 double endTime,
                                 SequenceName::Component name,
                                 Variant::Root &&value)
{
    if (!loggingEgress && !realtimeEgress)
        return;
    SequenceValue dv({{}, "raw", std::move(name)}, std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        if (FP::defined(startTime))
            loggingEgress->incomingData(std::move(dv));
        return;
    }
    if (loggingEgress && FP::defined(startTime))
        loggingEgress->incomingData(dv);
    if (FP::defined(endTime)) {
        dv.setStart(endTime);
        dv.setEnd(endTime + 1.0);
    } else if (FP::defined(startTime)) {
        dv.setEnd(startTime + 1.0);
    }
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireTSIMFM4xxx::realtimeValue(double time,
                                      SequenceName::Component name,
                                      Variant::Root &&value)
{
    if (!realtimeEgress) return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

void AcquireTSIMFM4xxx::invalidateLogValues(double frameTime)
{
    autoprobeValidRecords = 0;
    lastRecordTime = FP::undefined();
    loggingLost(frameTime);
}

SequenceValue::Transfer AcquireTSIMFM4xxx::buildLogMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_tsi_mfm4xxx");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);
    processing["CalibrationDate"].set(instrumentMeta["CalibrationDate"]);
    processing["Model"].set(instrumentMeta["Model"]);

    result.emplace_back(SequenceName({}, "raw_meta", "Q"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Sample flow");
    result.back().write().metadataReal("ReportT").setDouble(0);
    result.back().write().metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Flow"));

    result.emplace_back(SequenceName({}, "raw_meta", "T"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Sample temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime").hash("Name").setString(QObject::tr("Temperature"));

    result.emplace_back(SequenceName({}, "raw_meta", "P"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Sample pressure");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Pressure"));

    if (extendedQuery) {
        result.emplace_back(SequenceName({}, "raw_meta", "U"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("00.0");
        result.back().write().metadataReal("Units").setString("%");
        result.back().write().metadataReal("Description").setString("Sample RH");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Name")
              .setString(QObject::tr("Humidity"));
    }

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    return result;
}

SequenceValue::Transfer AcquireTSIMFM4xxx::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_tsi_mfm4xxx");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);
    processing["CalibrationDate"].set(instrumentMeta["CalibrationDate"]);
    processing["Model"].set(instrumentMeta["Model"]);

    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataString("Realtime").hash("Hide").setBool(true);
    /*result.back().write().metadataString("Realtime").hash("Name").
        setString(QString());
    result.back().write().metadataString("Realtime").hash("Units").
        setString(QString());
    result.back().write().metadataString("Realtime").hash("FullLine").
        setBool(true);        
    result.back().write().metadataString("Realtime").hash("RowOrder").
        setInt64(8);
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("Run").setString(QObject::tr("", "run state string"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("InvalidRecord").setString(QObject::tr("NO COMMS: Invalid record"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("InvalidResponse").setString(QObject::tr("NO COMMS: Invalid response"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("Timeout").setString(QObject::tr("NO COMMS: Timeout"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractive").setString(QObject::tr("STARTING COMMS"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveReadModel").setString(QObject::tr("STARTING COMMS: Reading model number"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveReadSerial").setString(QObject::tr("STARTING COMMS: Reading serial number"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveReadFirmware").setString(QObject::tr("STARTING COMMS: Reading firmware version"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveReadCalibration").setString(QObject::tr("STARTING COMMS: Reading calibration date"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveSetMassFlow").setString(QObject::tr("STARTING COMMS: Setting mass flow mode"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveDisableBeginTrigger").setString(QObject::tr("STARTING COMMS: Disabling begin trigger"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveDisableEndTrigger").setString(QObject::tr("STARTING COMMS: Disabling end trigger"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveSetSampleRate").setString(QObject::tr("STARTING COMMS: Setting sample rate"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveStartReadFirstRecord").setString(QObject::tr("STARTING COMMS: Waiting for first record"));*/

    return result;
}

SequenceMatch::Composite AcquireTSIMFM4xxx::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "ZSTATE");
    return sel;
}

/* Allow for binary framing, where the data frame looks like:
 * 
 *  0x00        (ACK)
 *  0xXX 0xXX   (flow, big endian, *100 for 4000, *1000 for 4100)
 *  0xXX 0xXX   (temperature, big endian signed, *100)
 *  0xXX 0xXX   (pressure, big endian, *100)
 *  0xFF 0xFF   (END)
 */
std::size_t AcquireTSIMFM4xxx::dataFrameStart(std::size_t offset,
                                              const Util::ByteArray &input) const
{
    auto max = input.size();
    while (offset < max) {
        auto ch = input[offset];
        if (ch == 0x00) {
            if (offset + 9 >= max)
                return input.npos;
            if (input[offset + 7] != 0xFF || input[offset + 8] != 0xFF) {
                offset++;
                continue;
            }
            return offset;
        }
        if (ch == 0xFF || ch == '\r' || ch == '\n') {
            offset++;
            continue;
        }
        return offset;
    }
    return input.npos;
}

std::size_t AcquireTSIMFM4xxx::dataFrameEnd(std::size_t start, const Util::ByteArray &input) const
{
    for (auto max = input.size(); start < max; start++) {
        auto ch = input[start];
        if (ch == 0x00) {
            if (start + 9 >= max)
                return input.npos;
            if (input[start + 7] != 0xFF || input[start + 8] != 0xFF)
                continue;
            return start + 9;
        }
        if (ch == '\r' || ch == '\n')
            return start;
    }
    return input.npos;
}

/* TSI uses 70F as standard temperature */
static double correctFlow(double Q)
{
    if (!FP::defined(Q))
        return FP::undefined();
    return Q * (273.15 / (273.15 + 21.11)) * (1013.0 / 1013.25);
}

int AcquireTSIMFM4xxx::processRecord(const Util::ByteView &frame, double startTime, double endTime)
{
    Q_ASSERT(!config.isEmpty());

    if (Util::ByteView(frame).string_trimmed() == "OK")
        return -1;

    if (frame.size() < 3)
        return 1;

    if (frame[0] == 0x00) {
        if (frame.size() != 9 + (extendedQuery ? 2 : 0))
            return 1000;
        if (frame[7] != 0xFF)
            return 1001;
        if (frame[8] != 0xFF)
            return 1002;
        const uchar *data = frame.data<const uchar *>();
        data += 1;
        quint16 u16 = qFromBigEndian<quint16>(data);
        data += 2;
        if (u16 == 0xFFFF)
            return 1003;
        double scale = config.first().binaryFlowScale;
        if (!FP::defined(scale))
            scale = binaryFlowConversion;
        if (!FP::defined(scale) || scale <= 0.0)
            scale = 100.0;
        Variant::Root Q((double) u16 / scale);
        remap("ZQ", Q);
        Q.write().setDouble(correctFlow(Q.read().toDouble()));
        remap("Q", Q);

        qint16 i16 = (qint16) qFromBigEndian<quint16>(data);
        data += 2;
        if (i16 == -32768 || i16 == 32767)
            return 1004;
        Variant::Root T((double) i16 / 100.0);
        remap("T", T);

        u16 = qFromBigEndian<quint16>(data);
        data += 2;
        if (u16 == 0xFFFF)
            return 1005;
        Variant::Root P((double) u16 / 10.0);
        remap("P", P);

        Variant::Root U;
        if (extendedQuery) {
            u16 = qFromBigEndian<quint16>(data);
            data += 2;
            if (u16 == 0xFFFF)
                return 1006;
            U.write().setReal(static_cast<double>(u16) / 100.0);
            remap("U", U);
        }

        if (!haveEmittedLogMeta && loggingEgress != NULL && FP::defined(startTime)) {
            haveEmittedLogMeta = true;
            loggingEgress->incomingData(buildLogMeta(startTime));
        }
        if (!haveEmittedRealtimeMeta && realtimeEgress != NULL && FP::defined(endTime)) {
            haveEmittedRealtimeMeta = true;
            SequenceValue::Transfer meta = buildLogMeta(endTime);
            Util::append(buildRealtimeMeta(endTime), meta);
            realtimeEgress->incomingData(std::move(meta));
        }

        logValue(startTime, endTime, "Q", std::move(Q));
        logValue(startTime, endTime, "P", std::move(P));
        logValue(startTime, endTime, "T", std::move(T));
        if (extendedQuery) {
            logValue(startTime, endTime, "U", std::move(U));
        }
        logValue(startTime, endTime, "F1", Variant::Root(Variant::Type::Flags));

        if (realtimeStateUpdated && realtimeEgress != NULL) {
            realtimeStateUpdated = false;
            realtimeValue(endTime, "ZSTATE", Variant::Root("Run"));
        }

        return 0;
    }

    auto fields = Util::as_deque(frame.split(','));
    bool ok = false;

    if (fields.empty()) return 2;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Q(field.parse_real(&ok));
    if (!ok) return 3;
    if (!FP::defined(Q.read().toReal())) return 4;
    remap("ZQ", Q);
    Q.write().setDouble(correctFlow(Q.read().toDouble()));
    remap("Q", Q);

    if (fields.empty()) return 5;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T(field.parse_real(&ok));
    if (!ok) return 6;
    if (!FP::defined(T.read().toReal())) return 7;
    remap("T", T);

    if (fields.empty()) return 8;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root P(field.parse_real(&ok));
    if (!ok) return 9;
    if (!FP::defined(P.read().toReal())) return 10;
    P.write().setDouble(P.read().toDouble() * 10.0);
    remap("P", P);

    Variant::Root U;
    if (extendedQuery) {
        if (fields.empty()) return 11;
        field = fields.front().string_trimmed();
        fields.pop_front();
        U.write().setReal(field.parse_real(&ok));
        if (!ok) return 12;
        if (!FP::defined(P.read().toReal())) return 13;
        remap("U", U);
    }

    if (config.first().strictMode) {
        if (!fields.empty())
            return 100;
    }

    if (!haveEmittedLogMeta && loggingEgress != NULL && FP::defined(startTime)) {
        haveEmittedLogMeta = true;
        loggingEgress->incomingData(buildLogMeta(startTime));
    }
    if (!haveEmittedRealtimeMeta && realtimeEgress != NULL && FP::defined(endTime)) {
        haveEmittedRealtimeMeta = true;
        SequenceValue::Transfer meta = buildLogMeta(endTime);
        Util::append(buildRealtimeMeta(endTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }

    logValue(startTime, endTime, "Q", std::move(Q));
    logValue(startTime, endTime, "P", std::move(P));
    logValue(startTime, endTime, "T", std::move(T));
    if (extendedQuery) {
        logValue(startTime, endTime, "U", std::move(U));
    }
    logValue(startTime, endTime, "F1", Variant::Root(Variant::Type::Flags));

    if (realtimeStateUpdated && realtimeEgress != NULL) {
        realtimeStateUpdated = false;
        realtimeValue(endTime, "ZSTATE", Variant::Root("Run"));
    }

    return 0;
}

static double convertCalibrationDate(const Util::ByteView &trimmed)
{
    if (trimmed.size() != 8) return FP::undefined();
    auto fields = trimmed.split('/');
    if (fields.size() != 3) return FP::undefined();

    bool ok = false;
    int month = fields[0].string_trimmed().parse_i32(&ok);
    if (!ok) return FP::undefined();
    if (month < 1 || month > 12) return FP::undefined();

    int day = fields[1].string_trimmed().parse_i32(&ok);
    if (!ok) return FP::undefined();
    if (day < 1 || day > 31) return FP::undefined();

    int year = fields[2].string_trimmed().parse_i32(&ok);
    if (!ok) return FP::undefined();
    if (year < 0 || year > 99) return FP::undefined();
    if (year > 95)
        year += 1900;
    else
        year += 2000;

    QDateTime dt(QDate(year, month, day), QTime(0, 0, 0), Qt::UTC);
    if (!dt.isValid()) return FP::undefined();
    return Time::fromDateTime(dt);
}

static bool isDelayedReportRead(const Util::ByteView &trimmed)
{
    if (trimmed.size() < 5)
        return false;
    auto idx1 = trimmed.indexOf(',');
    if (idx1 == trimmed.npos)
        return false;
    auto idx2 = trimmed.indexOf(',', idx1 + 1);
    if (idx2 == trimmed.npos)
        return false;
    if (trimmed.indexOf(',', idx2 + 1) != trimmed.npos)
        return false;
    switch (trimmed[0]) {
    case '-':
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
        break;
    default:
        return false;
    }
    return true;
}

void AcquireTSIMFM4xxx::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (!FP::defined(frameTime))
        return;

    {
        Q_ASSERT(!config.isEmpty());
        if (!Range::intersectShift(config, frameTime)) {
            qCWarning(log) << "Can't find active configuration (list corrupted)";
        }
        Q_ASSERT(!config.isEmpty());
    }

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        double startTime;
        if (!FP::defined(lastRecordTime)) {
            startTime = FP::undefined();
            lastRecordTime = frameTime;
        } else {
            startTime = lastRecordTime;
            if (lastRecordTime != frameTime)
                lastRecordTime = frameTime;
        }

        int code = processRecord(frame, startTime, frameTime);
        if (code == 0) {
            if (++autoprobeValidRecords > 10) {
                qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

                responseState = RESP_PASSIVE_RUN;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();
                realtimeStateUpdated = true;

                timeoutAt(frameTime + config.first().sampleTime + 4.0);

                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("PassiveAutoprobeWait");
                event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);
            }
        } else if (code > 0) {
            invalidateLogValues(frameTime);
        }
        break;
    }
    case RESP_PASSIVE_WAIT: {
        double startTime;
        if (!FP::defined(lastRecordTime)) {
            startTime = FP::undefined();
            lastRecordTime = frameTime;
        } else {
            startTime = lastRecordTime;
            if (lastRecordTime != frameTime)
                lastRecordTime = frameTime;
        }

        int code = processRecord(frame, startTime, frameTime);
        if (code == 0) {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);
            responseState = RESP_PASSIVE_RUN;
            generalStatusUpdated();
            realtimeStateUpdated = true;
            ++autoprobeValidRecords;

            timeoutAt(frameTime + config.first().sampleTime + 4.0);

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveWait");
            event(frameTime, QObject::tr("Passive communications established."), false, info);
        } else if (code > 0) {
            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_INTERACTIVE_START_FIRSTVALID: {
        double startTime;
        if (!FP::defined(lastRecordTime)) {
            startTime = FP::undefined();
            lastRecordTime = frameTime;
        } else {
            startTime = lastRecordTime;
            if (lastRecordTime != frameTime)
                lastRecordTime = frameTime;
        }

        int code = processRecord(frame, startTime, frameTime);
        if (code == 0) {
            qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

            if (controlStream) {
                if (extendedQuery) {
                    controlStream->writeControl("DAFTPHxx0001\r");
                } else {
                    controlStream->writeControl("DAFTP0001\r");
                }
            }
            responseState = RESP_INTERACTIVE_RUN_OK;
            timeoutAt(frameTime + config.first().sampleTime + 4.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();
            realtimeStateUpdated = true;
            ++autoprobeValidRecords;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            event(frameTime, QObject::tr("Communications established."), false, info);
        } else if (code > 0) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "rejected with code" << code;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidRecord"),
                                      frameTime, FP::undefined()));
            }

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Communications start rejected the first line (code %1).").arg(code),
                  true, info);

            invalidateLogValues(frameTime);
        } /* Ignored line */
        break;
    }

    case RESP_INTERACTIVE_RUN_RESPONSE: {
        double startTime;
        if (!FP::defined(lastRecordTime)) {
            startTime = FP::undefined();
            lastRecordTime = frameTime;
        } else {
            startTime = lastRecordTime;
            if (lastRecordTime != frameTime)
                lastRecordTime = frameTime;
        }

        int code = processRecord(frame, startTime, frameTime);
        if (code == 0) {
            if (controlStream) {
                if (extendedQuery) {
                    controlStream->writeControl("DAFTPHxx0001\r");
                } else {
                    controlStream->writeControl("DAFTP0001\r");
                }
            }
            responseState = RESP_INTERACTIVE_RUN_OK;
            timeoutAt(frameTime + config.first().sampleTime + 4.0);
            ++autoprobeValidRecords;
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;


            if (controlStream) {
                controlStream->writeControl(Util::ByteArray::filled('\r', 512));
                controlStream->writeControl("DAFTP0001\r");
            }
            responseState = RESP_INTERACTIVE_START_STOPREPORTS_INITIAL;
            discardData(frameTime + config.first().sampleTime + 4.0);

            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("RunResponse");
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
        } /* Ignored line */
        break;
    }

    case RESP_INTERACTIVE_RUN_OK:
        if (frame.empty())
            break;
        if (frame != "OK") {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "is not an acknowledgment";


            if (controlStream != NULL) {
                controlStream->writeControl(Util::ByteArray::filled('\r', 512));
                controlStream->writeControl("DAFTP0001\r");
            }
            responseState = RESP_INTERACTIVE_START_STOPREPORTS_INITIAL;
            discardData(frameTime + config.first().sampleTime + 4.0);

            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("RunOK");
            info.hash("Line").setString(frame.toString());
            event(frameTime, QObject::tr("Non-acknowledgment received.  Communications dropped."),
                  true, info);

            invalidateLogValues(frameTime);
        } else {
            responseState = RESP_INTERACTIVE_RUN_RESPONSE;
        }
        break;

    case RESP_PASSIVE_RUN: {
        double startTime;
        if (!FP::defined(lastRecordTime)) {
            startTime = FP::undefined();
            lastRecordTime = frameTime;
        } else {
            startTime = lastRecordTime;
            if (lastRecordTime != frameTime)
                lastRecordTime = frameTime;
        }

        int code = processRecord(frame, startTime, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + config.first().sampleTime + 4.0);
            ++autoprobeValidRecords;
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            responseState = RESP_PASSIVE_WAIT;
            timeoutAt(FP::undefined());
            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveRun");
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
        } /* Ignored line */
        break;
    }

    case RESP_INTERACTIVE_START_READMODEL: {
        Util::ByteView tf(frame);
        tf.string_trimmed();
        if (tf.string_start("ERR")) {
            qCDebug(log) << "Interactive start comms read model number failed at"
                         << Logging::time(frameTime) << "because" << frame
                         << "is an error response";

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
        } else if (tf == "OK") {
            /* Ignored */
        } else if (isDelayedReportRead(tf)) {
            /* Trailing response to the initial report stop, ignore it. */
        } else {
            Variant::Root oldValue(instrumentMeta["Model"]);
            instrumentMeta["Model"].setString(tf.toString());
            if (tf.string_start("41")) {
                binaryFlowConversion = 1000.0;
            } else {
                binaryFlowConversion = 100.0;
            }
            if (tf.string_start("5")) {
                if (!extendedQuery) {
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;
                }
                extendedQuery = true;
            } else {
                if (extendedQuery) {
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;
                }
                extendedQuery = false;
            }

            if (oldValue.read() != instrumentMeta["Model"]) {
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }

            timeoutAt(frameTime + 4.0);
            responseState = RESP_INTERACTIVE_START_READSERIAL;

            if (controlStream != NULL) {
                controlStream->writeControl("SN\r");
            }
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadSerial"), frameTime, FP::undefined()));
            }
        }
        break;
    }

    case RESP_INTERACTIVE_START_READSERIAL: {
        Util::ByteView tf(frame);
        tf.string_trimmed();
        if (tf.string_start("ERR")) {
            qCDebug(log) << "Interactive start comms read serial number failed at"
                         << Logging::time(frameTime) << "because" << frame
                         << "is an error response";

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            invalidateLogValues(frameTime);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
        } else if (tf == "OK") {
            /* Ignored */
        } else {
            Variant::Root oldValue(instrumentMeta["SerialNumber"]);

            bool ok = false;
            qint64 i64 = tf.parse_i64(&ok, 10);
            if (ok && INTEGER::defined(i64) && i64 > 0) {
                instrumentMeta["SerialNumber"].setInt64(i64);
            } else {
                instrumentMeta["SerialNumber"].setString(tf.toString());
            }

            if (oldValue.read() != instrumentMeta["SerialNumber"]) {
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }

            timeoutAt(frameTime + 4.0);
            responseState = RESP_INTERACTIVE_START_READFIRMWARE;

            if (controlStream != NULL) {
                controlStream->writeControl("REV\r");
            }
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadFirmware"), frameTime, FP::undefined()));
            }
        }
        break;
    }

    case RESP_INTERACTIVE_START_READFIRMWARE: {
        Util::ByteView tf(frame);
        tf.string_trimmed();
        if (tf.string_start("ERR")) {
            qCDebug(log) << "Interactive start comms read firmware version failed at"
                         << Logging::time(frameTime) << "because" << frame
                         << "is an error response";

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            invalidateLogValues(frameTime);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
        } else if (tf == "OK") {
            /* Ignored */
        } else {
            Variant::Root oldValue(instrumentMeta["FirmwareVersion"]);
            instrumentMeta["FirmwareVersion"].setString(tf.toString());

            if (oldValue.read() != instrumentMeta["FirmwareVersion"]) {
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }

            timeoutAt(frameTime + 4.0);
            responseState = RESP_INTERACTIVE_START_READCAL;

            if (controlStream != NULL) {
                controlStream->writeControl("DATE\r");
            }
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadCalibration"), frameTime, FP::undefined()));
            }
        }
        break;
    }

    case RESP_INTERACTIVE_START_READCAL: {
        Util::ByteView tf(frame);
        tf.string_trimmed();
        if (tf.string_start("ERR")) {
            qCDebug(log) << "Interactive start comms read calibration failed at"
                         << Logging::time(frameTime) << "because" << frame
                         << "is an error response";

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            invalidateLogValues(frameTime);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
        } else if (tf == "OK") {
            /* Ignored */
        } else {
            Variant::Root oldValue(instrumentMeta["CalibrationDate"]);

            double cd = convertCalibrationDate(tf);
            if (FP::defined(cd)) {
                instrumentMeta["CalibrationDate"].setDouble(cd);
            } else {
                instrumentMeta["CalibrationDate"].setString(tf.toString());
            }

            if (oldValue.read() != instrumentMeta["CalibrationDate"]) {
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }

            timeoutAt(frameTime + 4.0);
            responseState = RESP_INTERACTIVE_START_SETMASS;

            if (controlStream != NULL) {
                controlStream->writeControl("SUS\r");
            }
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveSetMassFlow"), frameTime, FP::undefined()));
            }
        }
        break;
    }

    case RESP_INTERACTIVE_START_SETMASS:
    case RESP_INTERACTIVE_START_DISABLEBEGINTRIGGER:
    case RESP_INTERACTIVE_START_DISABLEENDTRIGGER:
    case RESP_INTERACTIVE_START_SETSAMPLERATE:
        if (Util::ByteView(frame).string_trimmed() != "OK") {
            qCDebug(log) << "Interactive start comms state" << responseState << "failed at"
                         << Logging::time(frameTime) << ":" << frame << "when expecting OK";

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            invalidateLogValues(frameTime);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
        } else {
            switch (responseState) {
            case RESP_INTERACTIVE_START_SETMASS:
                timeoutAt(frameTime + 4.0);
                responseState = RESP_INTERACTIVE_START_DISABLEBEGINTRIGGER;

                if (controlStream != NULL) {
                    controlStream->writeControl("CBT\r");
                }
                if (realtimeEgress != NULL) {
                    realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                            "StartInteractiveDisableBeginTrigger"), frameTime, FP::undefined()));
                }
                break;

            case RESP_INTERACTIVE_START_DISABLEBEGINTRIGGER:
                timeoutAt(frameTime + 4.0);
                responseState = RESP_INTERACTIVE_START_DISABLEENDTRIGGER;

                if (controlStream != NULL) {
                    controlStream->writeControl("CET\r");
                }
                if (realtimeEgress != NULL) {
                    realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                            "StartInteractiveDisableEndTrigger"), frameTime, FP::undefined()));
                }
                break;

            case RESP_INTERACTIVE_START_DISABLEENDTRIGGER:
                timeoutAt(frameTime + 4.0);
                responseState = RESP_INTERACTIVE_START_SETSAMPLERATE;

                if (controlStream) {
                    Util::ByteArray data("SSR");
                    int n = qRound(config.first().sampleTime * 1000.0);
                    if (n < 1)
                        n = 1;
                    if (n > 1000)
                        n = 1000;
                    data += QByteArray::number(n).rightJustified(4, '0');
                    data.push_back('\r');
                    controlStream->writeControl(std::move(data));
                }
                if (realtimeEgress != NULL) {
                    realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                            "StartInteractiveSetSampleRate"), frameTime, FP::undefined()));
                }
                break;

            case RESP_INTERACTIVE_START_SETSAMPLERATE:
                if (controlStream) {
                    if (extendedQuery) {
                        controlStream->writeControl("DAFTPHxx0001\r");
                    } else {
                        controlStream->writeControl("DAFTP0001\r");
                    }
                }
                lastRecordTime = frameTime;
                responseState = RESP_INTERACTIVE_START_FIRSTVALID;
                timeoutAt(frameTime + config.first().sampleTime + 4.0);
                if (realtimeEgress != NULL) {
                    realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                            "StartInteractiveStartReadFirstRecord"), frameTime, FP::undefined()));
                }
                break;

            default:
                Q_ASSERT(false);
                break;
            }
        }
        break;

    default:
        break;
    }
}

void AcquireTSIMFM4xxx::incomingInstrumentTimeout(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_OK:
    case RESP_INTERACTIVE_RUN_RESPONSE:
        qCDebug(log) << "Timeout in interactive state" << responseState << "at"
                     << Logging::time(frameTime);

        timeoutAt(frameTime + config.first().sampleTime + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl(Util::ByteArray::filled('\r', 512));
            controlStream->writeControl("DAFTP0001\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS_INITIAL;
        discardData(frameTime + config.first().sampleTime + 4.0);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        {
            Variant::Write info = Variant::Write::empty();
            if (responseState == RESP_INTERACTIVE_RUN_RESPONSE) {
                info.hash("ResponseState").setString("InteractiveRun");
            } else {
                info.hash("ResponseState").setString("InteractiveRunOK");
            }
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_PASSIVE_RUN:
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        responseState = RESP_PASSIVE_WAIT;
        generalStatusUpdated();
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS_INITIAL:
    case RESP_INTERACTIVE_START_STOPREPORTS_AGAIN:
    case RESP_INTERACTIVE_START_READMODEL:
    case RESP_INTERACTIVE_START_READSERIAL:
    case RESP_INTERACTIVE_START_READFIRMWARE:
    case RESP_INTERACTIVE_START_READCAL:
    case RESP_INTERACTIVE_START_SETMASS:
    case RESP_INTERACTIVE_START_DISABLEBEGINTRIGGER:
    case RESP_INTERACTIVE_START_DISABLEENDTRIGGER:
    case RESP_INTERACTIVE_START_SETSAMPLERATE:
    case RESP_INTERACTIVE_RESTART_WAIT:
        qCDebug(log) << "Timeout in interactive start state" << responseState << "at"
                     << Logging::time(frameTime);

        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        timeoutAt(frameTime + config.first().sampleTime + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl(Util::ByteArray::filled('\r', 512));
            controlStream->writeControl("DAFTP0001\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS_INITIAL;
        discardData(frameTime + config.first().sampleTime + 4.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
        invalidateLogValues(frameTime);
        break;

    default:
        invalidateLogValues(frameTime);
        break;
    }
}

void AcquireTSIMFM4xxx::discardDataCompleted(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    switch (responseState) {
    case RESP_INTERACTIVE_START_STOPREPORTS_AGAIN:
        if (controlStream != NULL) {
            controlStream->writeControl("MN\r");
        }
        responseState = RESP_INTERACTIVE_START_READMODEL;
        timeoutAt(frameTime + 4.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveReadModel"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS_INITIAL:
        timeoutAt(frameTime + config.first().sampleTime * 2 + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl("DAFTP0001\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS_AGAIN;
        discardData(frameTime + config.first().sampleTime * 2 + 4.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_RESTART_WAIT:
        timeoutAt(frameTime + config.first().sampleTime + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl(Util::ByteArray::filled('\r', 512));
            controlStream->writeControl("DAFTP0001\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS_INITIAL;
        discardData(frameTime + config.first().sampleTime + 4.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    default:
        break;
    }
}


void AcquireTSIMFM4xxx::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_UNUSED(frameTime);
    Q_UNUSED(frame);
}

Variant::Root AcquireTSIMFM4xxx::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireTSIMFM4xxx::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RUN_OK:
    case RESP_INTERACTIVE_RUN_RESPONSE:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

AcquisitionInterface::AutoprobeStatus AcquireTSIMFM4xxx::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireTSIMFM4xxx::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_OK:
    case RESP_INTERACTIVE_RUN_RESPONSE:
        /* Already running/started, do nothing */
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << responseState << "at" << Logging::time(time);
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS_INITIAL:
    case RESP_INTERACTIVE_START_STOPREPORTS_AGAIN:
    case RESP_INTERACTIVE_START_READMODEL:
    case RESP_INTERACTIVE_START_READSERIAL:
    case RESP_INTERACTIVE_START_READFIRMWARE:
    case RESP_INTERACTIVE_START_READCAL:
    case RESP_INTERACTIVE_START_SETMASS:
    case RESP_INTERACTIVE_START_DISABLEBEGINTRIGGER:
    case RESP_INTERACTIVE_START_DISABLEENDTRIGGER:
    case RESP_INTERACTIVE_START_SETSAMPLERATE:
    case RESP_INTERACTIVE_START_FIRSTVALID:
    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        timeoutAt(time + config.first().sampleTime + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl(Util::ByteArray::filled('\r', 512));
            controlStream->writeControl("DAFTP0001\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS_INITIAL;
        discardData(time + config.first().sampleTime + 4.0);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireTSIMFM4xxx::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    Q_ASSERT(!config.isEmpty());

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
    autoprobeValidRecords = 0;
    discardData(time + 0.5, 1);
    timeoutAt(time + config.first().sampleTime * 12.0 + 3.0);
    generalStatusUpdated();
}

void AcquireTSIMFM4xxx::autoprobePromote(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_OK:
    case RESP_INTERACTIVE_RUN_RESPONSE:
    case RESP_INTERACTIVE_START_STOPREPORTS_INITIAL:
    case RESP_INTERACTIVE_START_STOPREPORTS_AGAIN:
    case RESP_INTERACTIVE_START_READMODEL:
    case RESP_INTERACTIVE_START_READSERIAL:
    case RESP_INTERACTIVE_START_READFIRMWARE:
    case RESP_INTERACTIVE_START_READCAL:
    case RESP_INTERACTIVE_START_SETMASS:
    case RESP_INTERACTIVE_START_DISABLEBEGINTRIGGER:
    case RESP_INTERACTIVE_START_DISABLEENDTRIGGER:
    case RESP_INTERACTIVE_START_SETSAMPLERATE:
    case RESP_INTERACTIVE_START_FIRSTVALID:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + config.first().sampleTime + 4.0);
        break;

    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + config.first().sampleTime + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl(Util::ByteArray::filled('\r', 512));
            controlStream->writeControl("DAFTP0001\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS_INITIAL;
        discardData(time + config.first().sampleTime + 4.0);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireTSIMFM4xxx::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    Q_ASSERT(!config.isEmpty());

    timeoutAt(time + config.first().sampleTime + 4.0);

    switch (responseState) {
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case RESP_INTERACTIVE_RUN_OK:
    case RESP_INTERACTIVE_RUN_RESPONSE:
        responseState = RESP_PASSIVE_RUN;

        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);
        break;

    default:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_PASSIVE_RUN;
        generalStatusUpdated();
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireTSIMFM4xxx::getDefaults()
{
    AutomaticDefaults result;
    result.name = "Q$1$2";
    result.setSerialN81(38400);
    return result;
}


ComponentOptions AcquireTSIMFM4xxxComponent::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options);
    return options;
}

ComponentOptions AcquireTSIMFM4xxxComponent::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireTSIMFM4xxxComponent::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples());
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireTSIMFM4xxxComponent::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireTSIMFM4xxxComponent::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options);
}

QList<ComponentExample> AcquireTSIMFM4xxxComponent::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data from the instrument")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireTSIMFM4xxxComponent::createAcquisitionPassive(const ComponentOptions &options,
                                                                                   const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireTSIMFM4xxx(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireTSIMFM4xxxComponent::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                   const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireTSIMFM4xxx(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireTSIMFM4xxxComponent::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                     const std::string &loggingContext)
{
    std::unique_ptr<AcquireTSIMFM4xxx> i(new AcquireTSIMFM4xxx(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<
        AcquisitionInterface> AcquireTSIMFM4xxxComponent::createAcquisitionInteractive(const ValueSegment::Transfer &config,
                                                                                       const std::string &loggingContext)
{
    std::unique_ptr<AcquireTSIMFM4xxx> i(new AcquireTSIMFM4xxx(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
