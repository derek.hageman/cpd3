/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <vector>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray outgoing;
    QByteArray incoming;
    double unpolledRemaining;

    QDateTime time;
    int status;
    double C;
    double Csum;
    double N;
    double P;
    double baseline;
    double baselineThreshold;
    double baselineSD;
    double baselineMaxSD;
    double Tsample;
    double pumpHours;
    double meanWidth;
    double widthSD;
    double Q;
    double pumpFB;
    double Tlaser;
    double laserFB;
    double laserMon;
    double Tboard;
    double vBat;
    double laserCurrent;
    double Qset;
    int BLStart;
    int thresholdMultiplier;
    double binLogMin;
    double binLogMax;
    int skipSave;
    int minPkPts;
    int maxPkPts;
    int rawPts;
    std::vector<double> counts;


    ModelInstrument() : unpolledRemaining(0), time(QDate(2013, 2, 3), QTime(1, 2, 0))
    {
        status = 1;
        C = 1000.0;
        Csum = 1001.0;
        N = 3000.0;
        baseline = 2083;
        baselineThreshold = 2115;
        baselineSD = 10.34;
        baselineMaxSD = 10.75;
        vBat = 12.0;
        Tboard = 25.0;
        Tsample = 49.21;
        pumpHours = 948.14;
        widthSD = 0.0;
        meanWidth = 0.0;
        P = 830.0;
        Q = 0.15;
        pumpFB = 487;
        Tlaser = 35.0;
        laserFB = 420.0;
        laserMon = 1300.0;
        laserCurrent = 2.34;
        Qset = 2.98;
        BLStart = 30000;
        thresholdMultiplier = 3;
        binLogMin = 1.75;
        binLogMax = 4.81;
        skipSave = 0;
        minPkPts = 8;
        maxPkPts = 255;
        rawPts = 512;
        for (int i = 0; i < 24; i++) {
            counts.push_back(i * 100.0 + 200.0);
        }
    }

    void advance(double seconds)
    {
        unpolledRemaining -= seconds;
        while (unpolledRemaining <= 0.0) {
            unpolledRemaining += 1.0;
            time = time.addSecs(1);

            outgoing.append("POPS,POPS_H0086,/media/uSD/Data/F20200220/Peak_20200220x003.b,");
            outgoing.append(time.toString("yyyyMMdd'T'hhmmss").toUtf8());
            // Time SSM, Aircraft Status
            outgoing.append(",56936.4312,3,");
            outgoing.append(QString::number(status));
            outgoing.append(',');
            outgoing.append(QString::number(C));
            outgoing.append(',');
            outgoing.append(QString::number(Csum));
            outgoing.append(',');
            outgoing.append(QString::number(N));
            outgoing.append(',');
            outgoing.append(QString::number(baseline));
            outgoing.append(',');
            outgoing.append(QString::number(baselineThreshold));
            outgoing.append(',');
            outgoing.append(QString::number(baselineSD));
            outgoing.append(',');
            outgoing.append(QString::number(baselineMaxSD));
            outgoing.append(',');
            outgoing.append(QString::number(P));
            outgoing.append(',');
            outgoing.append(QString::number(Tsample));
            outgoing.append(',');
            outgoing.append(QString::number(pumpHours));
            outgoing.append(',');
            outgoing.append(QString::number(widthSD));
            outgoing.append(',');
            outgoing.append(QString::number(meanWidth));
            outgoing.append(',');
            outgoing.append(QString::number(Q * 1000.0 / 60.0));
            outgoing.append(',');
            outgoing.append(QString::number(pumpFB));
            outgoing.append(',');
            outgoing.append(QString::number(Tlaser));
            outgoing.append(',');
            outgoing.append(QString::number(laserFB));
            outgoing.append(',');
            outgoing.append(QString::number(laserMon));
            outgoing.append(',');
            outgoing.append(QString::number(Tboard));
            outgoing.append(',');
            outgoing.append(QString::number(vBat));
            outgoing.append(',');
            outgoing.append(QString::number(laserCurrent));
            outgoing.append(',');
            outgoing.append(QString::number(Qset));
            outgoing.append(',');
            outgoing.append(QString::number(BLStart));
            outgoing.append(',');
            outgoing.append(QString::number(thresholdMultiplier));
            outgoing.append(',');
            outgoing.append(QString::number(counts.size()));
            outgoing.append(',');
            outgoing.append(QString::number(binLogMin));
            outgoing.append(',');
            outgoing.append(QString::number(binLogMax));
            outgoing.append(',');
            outgoing.append(QString::number(skipSave));
            outgoing.append(',');
            outgoing.append(QString::number(minPkPts));
            outgoing.append(',');
            outgoing.append(QString::number(maxPkPts));
            outgoing.append(',');
            outgoing.append(QString::number(rawPts));

            for (const auto &v : counts) {
                outgoing.append(',');
                outgoing.append(QString::number(v));
            }
            outgoing.append('\r');
            outgoing.append('\n');
        }
    }
};

class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream, double time = FP::undefined(), bool checkSizes = true)
    {
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("N", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Q", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("P", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T3", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("I", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Ig", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZMEANWIDTH", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZPUMPFB", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZLASERMON", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Nb", Variant::Root(), QString(), time))
            return false;
        if (checkSizes && !stream.hasMeta("Ns", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("C", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("V", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("A", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Im", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Igm", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZPUMPTIME", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZLASERFB", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Cb", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkContiguous(StreamCapture &stream, bool checkSizes = true)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("N"))
            return false;
        if (!stream.checkContiguous("Q"))
            return false;
        if (!stream.checkContiguous("P"))
            return false;
        if (!stream.checkContiguous("T1"))
            return false;
        if (!stream.checkContiguous("T2"))
            return false;
        if (!stream.checkContiguous("T3"))
            return false;
        if (!stream.checkContiguous("I"))
            return false;
        if (!stream.checkContiguous("Ig"))
            return false;
        if (!stream.checkContiguous("ZMEANWIDTH"))
            return false;
        if (!stream.checkContiguous("ZPUMPFB"))
            return false;
        if (!stream.checkContiguous("ZLASERMON"))
            return false;
        if (checkSizes && !stream.checkContiguous("Ns"))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream,
                     const ModelInstrument &model,
                     double time = FP::undefined(),
                     bool checkSizes = true)
    {
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        double Qcc = (model.Q * 1000.0 / 60.0);
        if (!stream.hasAnyMatchingValue("N", Variant::Root(model.C / Qcc), time))
            return false;
        if (!stream.hasAnyMatchingValue("Q", Variant::Root(model.Q), time))
            return false;
        if (!stream.hasAnyMatchingValue("P", Variant::Root(model.P), time))
            return false;
        if (!stream.hasAnyMatchingValue("T1", Variant::Root(model.Tsample), time))
            return false;
        if (!stream.hasAnyMatchingValue("T2", Variant::Root(model.Tlaser), time))
            return false;
        if (!stream.hasAnyMatchingValue("T3", Variant::Root(model.Tboard), time))
            return false;
        if (!stream.hasAnyMatchingValue("I", Variant::Root(model.baseline), time))
            return false;
        if (!stream.hasAnyMatchingValue("Ig", Variant::Root(model.baselineSD), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZMEANWIDTH", Variant::Root(model.meanWidth), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZPUMPFB", Variant::Root(model.pumpFB), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZLASERMON", Variant::Root(model.laserMon), time))
            return false;

        Variant::Write Nb = Variant::Write::empty();
        for (std::size_t i = 0; i < model.counts.size(); i++) {
            Nb.array(i).setDouble(model.counts[i] / Qcc);
        }
        if (!stream.hasAnyMatchingValue("Nb", Nb, time))
            return false;

        if (checkSizes && !stream.hasAnyMatchingValue("Ns", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream,
                             const ModelInstrument &model,
                             double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("C", Variant::Root(model.C), time))
            return false;
        if (!stream.hasAnyMatchingValue("V", Variant::Root(model.vBat), time))
            return false;
        if (!stream.hasAnyMatchingValue("A", Variant::Root(model.laserCurrent), time))
            return false;
        if (!stream.hasAnyMatchingValue("Im", Variant::Root(model.baselineThreshold), time))
            return false;
        if (!stream.hasAnyMatchingValue("Igm", Variant::Root(model.baselineMaxSD), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZPUMPTIME", Variant::Root(model.pumpHours), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZLASERFB", Variant::Root(model.laserFB), time))
            return false;

        if (!stream.hasAnyMatchingValue("ZSTATE", Variant::Root(), time))
            return false;

        Variant::Write ZN = Variant::Write::empty();
        double sum = 0;
        for (std::size_t i = 0; i < model.counts.size(); i++) {
            sum += model.counts[i];
            ZN.array(i).setDouble(sum);
        }
        if (!stream.hasAnyMatchingValue("Cb", ZN, time))
            return false;

        return true;
    }


private slots:

    void initTestCase()
    {
        component =
                qobject_cast<AcquisitionComponent *>(ComponentLoader::create("acquire_csd_pops"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_csd_pops"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
        QVERIFY(qobject_cast<ComponentOptionDoubleList *>(options.get("diameter")));
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        for (int i = 0; i < 24; i++) {
            cv["Diameter"].array(i).setDouble(i + 1);
        }
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 50; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 20; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(5.0);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 20; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging, false));

        QVERIFY(checkMeta(logging, FP::undefined(), false));
        QVERIFY(checkMeta(realtime, FP::undefined(), false));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, FP::undefined(), false));
        QVERIFY(checkValues(realtime, instrument, FP::undefined(), false));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        for (int i = 0; i < 24; i++) {
            cv["Diameter"].array(i).setDouble(i + 1);
        }
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 50; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 50; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
