/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <array>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "datacore/stream.hxx"
#include "datacore/wavelength.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_csd_pops.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;


/*
 * Spreadsheet from an email on 2020-02-13:
 *
 * "Aerosol Sizing bin edges are as follows for bins 0-23 and assume a refreactive index of 1.53 tyoical of background continental aerosol:"
 */
static std::array<double, 24> defaultBinDiameters =
        {0.148932684284402, 0.167937950689274, 0.189368474873229, 0.213533743436962,
         0.240782736496803, 0.271508967443379, 0.306156165822747, 0.345224685408714,
         0.389278730007829, 0.438954501347729, 0.494969386715678, 0.55813231902961,
         0.629355458955506, 0.709667367777143, 0.800227861252798, 0.902344758969278,
         1.01749277107724, 1.14733479515859, 1.29374593078232, 1.45884055855244, 1.64500287470715,
         1.85492132223134, 2.09162741571559, 2.35853952064684,};


AcquireCSDPOPS::Configuration::Configuration() : start(FP::undefined()),
                                                 end(FP::undefined()),
                                                 binDiameter(defaultBinDiameters.begin(),
                                                             defaultBinDiameters.end()),
                                                 useMeasuredTime(false),
                                                 strictMode(false)
{ }

AcquireCSDPOPS::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          binDiameter(other.binDiameter),
          useMeasuredTime(other.useMeasuredTime),
          strictMode(other.strictMode)
{ }

AcquireCSDPOPS::Configuration::Configuration(const ValueSegment &other, double s, double e) : start(
        s),
                                                                                              end(e),
                                                                                              binDiameter(
                                                                                                      defaultBinDiameters
                                                                                                              .begin(),
                                                                                                      defaultBinDiameters
                                                                                                              .end()),
                                                                                              useMeasuredTime(
                                                                                                      false),
                                                                                              strictMode(
                                                                                                      false)
{
    setFromSegment(other);
}

AcquireCSDPOPS::Configuration::Configuration(const Configuration &under,
                                             const ValueSegment &over,
                                             double s,
                                             double e) : start(s),
                                                         end(e),
                                                         binDiameter(under.binDiameter),
                                                         useMeasuredTime(under.useMeasuredTime),
                                                         strictMode(under.strictMode)
{
    setFromSegment(over);
}

void AcquireCSDPOPS::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["StrictMode"].exists())
        strictMode = config["StrictMode"].toBool();

    if (config["UseMeasuredTime"].exists())
        useMeasuredTime = config["UseMeasuredTime"].toBool();

    if (config["Diameter"].exists()) {
        binDiameter.clear();
        for (auto add : config["Diameter"].toArray()) {
            binDiameter.push_back(add.toDouble());
        }
    }
}


void AcquireCSDPOPS::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("CSD");
    instrumentMeta["Model"].setString("POPS");

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    realtimeStateUpdated = true;
    lastBinCount = static_cast<std::size_t>(-1);
}

AcquireCSDPOPS::AcquireCSDPOPS(const ValueSegment::Transfer &configData,
                               const std::string &loggingContext) : FramedInstrument("pops",
                                                                                     loggingContext),
                                                                    lastRecordTime(FP::undefined()),
                                                                    autoprobeStatus(
                                                                            AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                    responseState(RESP_WAIT),
                                                                    autoprobeValidRecords(0)
{

    setDefaultInvalid();
    config.append(Configuration());
    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
}

void AcquireCSDPOPS::setToAutoprobe()
{
    responseState = RESP_AUTOPROBE_INITIALIZE;
    autoprobeValidRecords = 0;
}

void AcquireCSDPOPS::setToInteractive()
{ responseState = RESP_INITIALIZE; }


ComponentOptions AcquireCSDPOPSComponent::getBaseOptions()
{
    ComponentOptions options;

    ComponentOptionDoubleList *dl =
            new ComponentOptionDoubleList(tr("diameter", "name"), tr("Bin center diameters"),
                                          tr("This sets the center diameter of the bins in \xCE\xBCm.  When not set "
                                                 "no information about bin sizes is generated."),
                                          QString(), 1);
    dl->setMinimumComponents(1);
    options.add("diameter", dl);

    return options;
}

AcquireCSDPOPS::AcquireCSDPOPS(const ComponentOptions &options, const std::string &loggingContext)
        : FramedInstrument("pops", loggingContext),
          lastRecordTime(FP::undefined()),
          autoprobeStatus(AcquisitionInterface::AutoprobeStatus::InProgress),
          responseState(RESP_WAIT),
          autoprobeValidRecords(0)
{
    setDefaultInvalid();

    Configuration base;

    if (options.isSet("diameter")) {
        base.binDiameter.clear();
        for (auto add : qobject_cast<ComponentOptionDoubleList *>(options.get("diameter"))->get()) {
            base.binDiameter.push_back(add);
        }
    }

    config.append(base);
}

AcquireCSDPOPS::~AcquireCSDPOPS()
{
}


void AcquireCSDPOPS::logValue(double startTime,
                              double endTime,
                              SequenceName::Component name,
                              Variant::Root &&value)
{
    if (!loggingEgress && !realtimeEgress) return;
    SequenceValue dv({{}, "raw", std::move(name)}, std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        if (FP::defined(startTime))
            loggingEgress->incomingData(std::move(dv));
        return;
    }
    if (loggingEgress && FP::defined(startTime))
        loggingEgress->incomingData(dv);
    if (FP::defined(endTime)) {
        dv.setStart(endTime);
        dv.setEnd(endTime + 1.0);
    } else if (FP::defined(startTime)) {
        dv.setEnd(startTime + 1.0);
    }
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireCSDPOPS::realtimeValue(double time, SequenceName::Component name, Variant::Root &&value)
{
    if (!realtimeEgress) return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

SequenceValue::Transfer AcquireCSDPOPS::buildLogMeta(double time) const
{
    Q_ASSERT(!config.isEmpty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_csd_pops");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    if (instrumentMeta["SerialNumber"].exists())
        processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);

    result.emplace_back(SequenceName({}, "raw_meta", "N"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0");
    result.back().write().metadataReal("Units").setString("cm\xE2\x81\xBB³");
    result.back().write().metadataReal("Description").setString("Total concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Concentration"));
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");

    result.emplace_back(SequenceName({}, "raw_meta", "ZMEANWIDTH"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000.00");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Mean width of the detection peak in sampling cycles");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Peak width"));
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(2);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");

    result.emplace_back(SequenceName({}, "raw_meta", "Q"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Sample flow");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Flow"));
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(3);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");

    result.emplace_back(SequenceName({}, "raw_meta", "P"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Board pressure sensor");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Board"));
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(4);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");

    result.emplace_back(SequenceName({}, "raw_meta", "T1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Temperature of pressure sensor");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Board"));
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(5);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");

    result.emplace_back(SequenceName({}, "raw_meta", "T2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Laser temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Temperature"));
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(6);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Laser"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("1");

    result.emplace_back(SequenceName({}, "raw_meta", "T3"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Internal temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Internal"));
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(16);

    result.emplace_back(SequenceName({}, "raw_meta", "I"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000");
    result.back().write().metadataReal("Description").setString("Baseline value");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Value"));
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(10);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(2);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Baseline"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("2");

    result.emplace_back(SequenceName({}, "raw_meta", "Ig"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000");
    result.back().write().metadataReal("Description").setString("Baseline standard deviation");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("StdDev"));
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(12);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(2);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Baseline"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("2");

    result.emplace_back(SequenceName({}, "raw_meta", "ZLASERMON"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000");
    result.back().write().metadataReal("Description").setString("Laser monitor");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Monitor"));
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(4);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Laser"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("1");

    result.emplace_back(SequenceName({}, "raw_meta", "ZPUMPFB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000");
    result.back().write().metadataReal("Description").setString("Pump feedback");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Feedback"));
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(15);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(3);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Pump"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("3");

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);
    result.back().write().metadataSingleFlag("TooManyParticles").hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_csd_pops");
    result.back()
          .write()
          .metadataSingleFlag("TooManyParticles")
          .hash("Description")
          .setString("Too many particles present for accurate counting due to coincidence");
    result.back()
          .write()
          .metadataSingleFlag("TimingUncertainty")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_csd_pops");
    result.back()
          .write()
          .metadataSingleFlag("TimingUncertainty")
          .hash("Description")
          .setString("High uncertainty in timing (10-20 microseconds)");


    result.emplace_back(SequenceName({}, "raw_meta", "Nb"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataArray("Count").setInteger(lastBinCount);
    result.back().write().metadataArray("Children").metadataReal("Format").setString("00000.0");
    result.back()
          .write()
          .metadataArray("Children")
          .metadataReal("Units")
          .setString("cm\xE2\x81\xBB³");
    result.back().write().metadataArray("Description").setString("Bin concentration distribution");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Realtime").hash("Page").setInteger(1);
    result.back()
          .write()
          .metadataArray("Realtime")
          .hash("Name")
          .setString(QObject::tr("Concentration"));
    result.back().write().metadataArray("Realtime").hash("RowOrder").setInteger(1);
    result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInteger(1);

    if (!config.first().binDiameter.empty()) {
        result.emplace_back(SequenceName({}, "raw_meta", "Ns"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataArray("Count").setInteger(lastBinCount);
        result.back().write().metadataArray("Children").metadataReal("Format").setString("00.0000");
        result.back().write().metadataArray("Units").setString("\xCE\xBCm");
        result.back().write().metadataArray("Description").setString("Bin center diameter");
        result.back().write().metadataArray("Source").set(instrumentMeta);
        result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
        result.back().write().metadataArray("Realtime").hash("Page").setInteger(1);
        result.back()
              .write()
              .metadataArray("Realtime")
              .hash("Name")
              .setString(QObject::tr("Diameter"));
        result.back().write().metadataArray("Realtime").hash("RowOrder").setInteger(0);
        result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInteger(1);
    }

    return result;
}

SequenceValue::Transfer AcquireCSDPOPS::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_csd_pops");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    if (instrumentMeta["SerialNumber"].exists())
        processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);

    result.emplace_back(SequenceName({}, "raw_meta", "C"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Count rate"));
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");

    result.emplace_back(SequenceName({}, "raw_meta", "A"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000.00");
    result.back().write().metadataReal("Units").setString("mA");
    result.back().write().metadataReal("Description").setString("Laser current");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Current"));
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(7);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Laser"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("1");

    result.emplace_back(SequenceName({}, "raw_meta", "V"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Supply voltage");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Supply"));
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(17);

    result.emplace_back(SequenceName({}, "raw_meta", "Im"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000");
    result.back().write().metadataReal("Description").setString("Baseline threshold level");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Threshold"));
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(11);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(2);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Baseline"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("2");

    result.emplace_back(SequenceName({}, "raw_meta", "Igm"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Baseline maximum standard deviation");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("StdDevMax"));
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(13);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(2);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Baseline"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("2");

    result.emplace_back(SequenceName({}, "raw_meta", "ZPUMPTIME"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.00");
    result.back().write().metadataReal("Units").setString("h");
    result.back().write().metadataReal("Description").setString("Pump total on time");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("On time"));
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(14);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(3);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Pump"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("3");

    result.emplace_back(SequenceName({}, "raw_meta", "ZLASERFB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000");
    result.back().write().metadataReal("Description").setString("Laser feedback");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Feedback"));
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(9);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Laser"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("1");


    result.emplace_back(SequenceName({}, "raw_meta", "Cb"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataArray("Count").setInteger(lastBinCount);
    result.back().write().metadataArray("Children").metadataReal("Format").setString("000000");
    result.back().write().metadataArray("Children").metadataReal("Units").setString("Hz");
    result.back().write().metadataArray("Description").setString("Bin counts");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Realtime").hash("Page").setInteger(1);
    result.back()
          .write()
          .metadataArray("Realtime")
          .hash("Name")
          .setString(QObject::tr("Cumulative"));
    result.back().write().metadataArray("Realtime").hash("RowOrder").setInteger(2);
    result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInteger(1);


    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataString("Realtime").hash("Name").setString(QString());
    result.back().write().metadataString("Realtime").hash("Units").setString(QString());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("PageMask")
          .setInteger((1 << 0) | (1 << 1));
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Run")
          .setString(QObject::tr("", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidResponse")
          .setString(QObject::tr("NO COMMS: Invalid response"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Timeout")
          .setString(QObject::tr("NO COMMS: Timeout"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Wait")
          .setString(QObject::tr("STARTING COMMS"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("TooManyParticles")
          .setString(QObject::tr("Too many particles present"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("TimingUncertainty")
          .setString(QObject::tr("Timing uncertainty present"));

    return result;
}

SequenceMatch::Composite AcquireCSDPOPS::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "ZSTATE");
    return sel;
}

static double convertRate(double C, double dT)
{
    if (!FP::defined(C) || !FP::defined(dT))
        return FP::undefined();
    if (dT <= 0.0)
        return FP::undefined();
    return C / dT;
}

static double calculateConcentration(double N, double Q)
{
    if (!FP::defined(N) || !FP::defined(Q))
        return FP::undefined();
    if (Q <= 0.0)
        return FP::undefined();
    return N / (Q * 1000.0 / 60.0);
}

int AcquireCSDPOPS::processRecord(const Util::ByteArray &line, double &frameTime)
{
    Q_ASSERT(!config.isEmpty());

    if (line.empty())
        return -1;
    if (line.size() < 3)
        return 1;

    auto fields = Util::as_deque(line.split(','));
    bool ok = false;

    if (fields.empty()) return 2;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    if (field != "POPS") return 3;

    if (fields.empty()) return 4;
    auto rawSN = fields.front().toQByteArray().trimmed();
    fields.pop_front();

    /* Peak File Name */
    if (fields.empty()) return 5;
    fields.pop_front();

    if (fields.empty()) return 6;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.size() != 15) return 7;

    std::int_fast64_t iyear = field.mid(0, 4).parse_i32(&ok);
    if (!ok) return 8;
    if (iyear < 1900) return 9;
    if (iyear > 2999) return 10;
    Variant::Root year(iyear);
    remap("YEAR", year);
    iyear = year.read().toInteger();

    std::int_fast64_t imonth = field.mid(4, 2).parse_i32(&ok);
    if (!ok) return 11;
    if (imonth < 1) return 12;
    if (imonth > 12) return 13;
    Variant::Root month(imonth);
    remap("MONTH", month);
    imonth = month.read().toInteger();

    std::int_fast64_t iday = field.mid(6, 2).parse_i32(&ok);
    if (!ok) return 14;
    if (iday < 1) return 15;
    if (iday > 31) return 16;
    Variant::Root day(iday);
    remap("DAY", day);
    iday = day.read().toInteger();

    if (field.mid(8, 1) != "T") return 17;

    std::int_fast64_t ihour = field.mid(9, 2).parse_i32(&ok);
    if (!ok) return 18;
    if (ihour < 0) return 19;
    if (ihour > 23) return 20;
    Variant::Root hour(ihour);
    remap("HOUR", hour);
    ihour = hour.read().toInteger();

    std::int_fast64_t iminute = field.mid(11, 2).parse_i32(&ok);
    if (!ok) return 21;
    if (iminute < 0) return 22;
    if (iminute > 59) return 23;
    Variant::Root minute(iminute);
    remap("MINUTE", minute);
    iminute = minute.read().toInteger();

    std::int_fast64_t isecond = field.mid(13, 2).parse_i32(&ok);
    if (!ok) return 24;
    if (isecond < 0) return 25;
    if (isecond > 60) return 26;
    Variant::Root second(isecond);
    remap("SECOND", second);
    isecond = second.read().toInteger();

    /* Time SSM */
    if (fields.empty()) return 27;
    fields.pop_front();

    /* Aircraft Status */
    if (fields.empty()) return 28;
    fields.pop_front();

    if (fields.empty()) return 29;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZSTATUS(field.parse_i32(&ok));
    if (!ok) return 30;
    if (!INTEGER::defined(ZSTATUS.read().toInteger())) return 31;
    remap("ZSTATUS", ZSTATUS);

    if (fields.empty()) return 32;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root C(field.parse_real(&ok));
    if (!ok) return 33;
    if (!FP::defined(C.read().toReal())) return 34;
    remap("C", C);

    /* Particle Number sum of histogram */
    if (fields.empty()) return 35;
    fields.pop_front();

    if (fields.empty()) return 36;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZN(field.parse_real(&ok));
    if (!ok) {
        ZN.write().setReal(FP::undefined());
    }
    remap("ZN", ZN);

    if (fields.empty()) return 37;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root I(field.parse_real(&ok));
    if (!ok) return 38;
    if (!FP::defined(I.read().toReal())) return 39;
    remap("I", I);

    if (fields.empty()) return 40;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Im(field.parse_real(&ok));
    if (!ok) return 41;
    if (!FP::defined(Im.read().toReal())) return 42;
    remap("Im", Im);

    if (fields.empty()) return 43;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Ig(field.parse_real(&ok));
    if (!ok) return 44;
    if (!FP::defined(Ig.read().toReal())) return 45;
    remap("Ig", Ig);

    if (fields.empty()) return 46;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Igm(field.parse_real(&ok));
    if (!ok) return 47;
    if (!FP::defined(Igm.read().toReal())) return 48;
    remap("Igm", Igm);

    if (fields.empty()) return 49;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root P(field.parse_real(&ok));
    if (!ok) return 50;
    if (!FP::defined(P.read().toReal())) return 51;
    remap("P", P);

    if (fields.empty()) return 52;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T1(field.parse_real(&ok));
    if (!ok) return 53;
    if (!FP::defined(T1.read().toReal())) return 54;
    remap("T1", T1);

    if (fields.empty()) return 55;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZPUMPTIME(field.parse_real(&ok));
    if (!ok) return 56;
    if (!FP::defined(ZPUMPTIME.read().toReal())) return 57;
    remap("ZPUMPTIME", ZPUMPTIME);

    /* Width SD (always zero?) */
    if (fields.empty()) return 58;
    fields.pop_front();

    if (fields.empty()) return 59;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZMEANWIDTH(field.parse_real(&ok));
    if (!ok) return 60;
    if (!FP::defined(ZMEANWIDTH.read().toReal())) return 61;
    remap("ZMEANWIDTH", ZMEANWIDTH);

    if (fields.empty()) return 62;
    Variant::Root Q;
    {
        field = fields.front().string_trimmed();
        fields.pop_front();
        double value = field.parse_real(&ok);
        if (!ok) return 63;
        if (!FP::defined(value)) return 64;
        value *= 60.0 / 1000.0;
        Q.write().setReal(value);
    }
    remap("Q", Q);

    if (fields.empty()) return 65;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZPUMPFB(field.parse_real(&ok));
    if (!ok) return 66;
    if (!FP::defined(ZPUMPFB.read().toReal())) return 67;
    remap("ZPUMPFB", ZPUMPFB);

    if (fields.empty()) return 68;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T2(field.parse_real(&ok));
    if (!ok) return 69;
    if (!FP::defined(T2.read().toReal())) return 70;
    remap("T2", T2);

    if (fields.empty()) return 71;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZLASERFB(field.parse_real(&ok));
    if (!ok) return 72;
    if (!FP::defined(ZLASERFB.read().toReal())) return 73;
    remap("ZLASERFB", ZLASERFB);

    if (fields.empty()) return 74;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZLASERMON(field.parse_real(&ok));
    if (!ok) return 75;
    if (!FP::defined(ZLASERMON.read().toReal())) return 76;
    remap("ZLASERMON", ZLASERMON);

    if (fields.empty()) return 77;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T3(field.parse_real(&ok));
    if (!ok) return 78;
    if (!FP::defined(T3.read().toReal())) return 79;
    remap("T3", T3);

    if (fields.empty()) return 80;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root V(field.parse_real(&ok));
    if (!ok) return 81;
    if (!FP::defined(V.read().toReal())) return 82;
    remap("V", V);

    if (fields.empty()) return 83;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root A(field.parse_real(&ok));
    if (!ok) return 84;
    if (!FP::defined(A.read().toReal())) return 85;
    remap("A", A);

    /* Flow_Set */
    if (fields.empty()) return 86;
    fields.pop_front();

    /* BL Start */
    if (fields.empty()) return 87;
    fields.pop_front();

    /* Threshold multiplier */
    if (fields.empty()) return 88;
    fields.pop_front();

    if (fields.empty()) return 89;
    field = fields.front().string_trimmed();
    fields.pop_front();
    auto instrumentBins = field.parse_i64(&ok);
    if (!ok || instrumentBins <= 0) return 90;

    /* Bin LogMin */
    if (fields.empty()) return 91;
    fields.pop_front();

    /* Bin LogMax */
    if (fields.empty()) return 92;
    fields.pop_front();

    /* Skip Save */
    if (fields.empty()) return 93;
    fields.pop_front();

    /* Min Pk Pts */
    if (fields.empty()) return 94;
    fields.pop_front();

    /* Max Pk Pts */
    if (fields.empty()) return 95;
    fields.pop_front();

    /* Raw Pts */
    if (fields.empty()) return 96;
    fields.pop_front();

    if (!FP::defined(frameTime) &&
            INTEGER::defined(iyear) &&
            iyear >= 1900 &&
            iyear <= 2999 &&
            INTEGER::defined(imonth) &&
            imonth >= 1 &&
            imonth <= 12 &&
            INTEGER::defined(iday) &&
            iday >= 1 &&
            iday <= 31 &&
            INTEGER::defined(ihour) &&
            ihour >= 0 &&
            ihour <= 23 &&
            INTEGER::defined(iminute) &&
            iminute >= 0 &&
            iminute <= 59 &&
            INTEGER::defined(isecond) &&
            isecond >= 0 &&
            isecond <= 60) {
        frameTime = Time::fromDateTime(QDateTime(QDate((int) iyear, (int) imonth, (int) iday),
                                                 QTime((int) ihour, (int) iminute, (int) isecond),
                                                 Qt::UTC));
        configAdvance(frameTime);
    }

    if (!FP::defined(frameTime))
        return -1;
    if (FP::defined(lastRecordTime) && frameTime < lastRecordTime)
        return -1;

    double startTime = lastRecordTime;
    double endTime = frameTime;
    lastRecordTime = frameTime;

    if (config.first().strictMode) {
        if (static_cast<std::size_t>(instrumentBins) != fields.size())
            return 97;
        if (!config.first().binDiameter.empty() &&
                static_cast<std::size_t>(instrumentBins) != config.first().binDiameter.size())
            return 98;
    }


    double dT = 1.0;
    if (config.first().useMeasuredTime && FP::defined(startTime) && FP::defined(endTime))
        dT = endTime - startTime;

    C.write().setDouble(convertRate(C.read().toDouble(), dT));
    remap("C", C);

    Variant::Root N = ZN;
    if (FP::defined(C.read().toReal())) {
        N.write().setReal(calculateConcentration(C.read().toDouble(), Q.read().toDouble()));
    }
    remap("N", N);

    std::size_t nBins = fields.size();
    nBins = std::min<std::size_t>(nBins, instrumentBins);
    Variant::Root Nb;
    Variant::Root Ns;
    Variant::Root Cb;
    double total = FP::undefined();
    for (std::size_t i = 0; i < nBins; i++) {
        if (fields.empty()) return 100 * (i + 1) + 0;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root rate(convertRate(field.parse_real(&ok), dT));
        if (!ok) return 100 * (i + 1) + 1;
        if (!FP::defined(rate.read().toDouble())) return 100 * (i + 1) + 2;
        remap("ZRate" + std::to_string(i + 1), rate);
        double add = rate.read().toReal();
        if (FP::defined(add)) {
            if (!FP::defined(total))
                total = add;
            else
                total += add;
        }
        Variant::Root bin(total);
        remap("Cb" + std::to_string(i + 1), rate);
        Cb.write().array(i).set(bin);

        Variant::Root ZNb(calculateConcentration(add, Q.read().toDouble()));
        remap("Nb" + std::to_string(i + 1), ZNb);
        Nb.write().array(i).set(ZNb);

        if (i < config.first().binDiameter.size()) {
            Variant::Root ZNs(config.first().binDiameter[i]);
            remap("Ns" + std::to_string(i + 1), ZNs);
            Ns.write().array(i).set(ZNs);
        }
    }
    remap("Nb", Nb);
    remap("Ns", Ns);
    remap("Cb", Cb);

    if (config.first().strictMode) {
        if (!fields.empty())
            return 99;
    }

    if (rawSN.startsWith("POPS_"))
        rawSN = rawSN.mid(5);
    if (rawSN.startsWith("POPS-"))
        rawSN = rawSN.mid(5);
    if (!rawSN.isEmpty()) {
        auto sn = rawSN.toLongLong(&ok);
        if (ok && INTEGER::defined(sn) && sn > 0) {
            if (sn != instrumentMeta["SerialNumber"].toInteger()) {
                haveEmittedRealtimeMeta = false;
                haveEmittedLogMeta = false;
                instrumentMeta["SerialNumber"].setInteger(sn);
            }
        } else {
            auto strSN = rawSN.toStdString();
            if (strSN != instrumentMeta["SerialNumber"].toString()) {
                haveEmittedRealtimeMeta = false;
                haveEmittedLogMeta = false;
                instrumentMeta["SerialNumber"].setString(strSN);
            }
        }
    }

    if (Nb.read().getType() == Variant::Type::Array && lastBinCount != Nb.read().toArray().size()) {
        haveEmittedLogMeta = false;
        haveEmittedRealtimeMeta = false;
        lastBinCount = Nb.read().toArray().size();
    }


    if (!haveEmittedLogMeta && FP::defined(startTime) && loggingEgress) {
        haveEmittedLogMeta = true;
        loggingEgress->incomingData(buildLogMeta(startTime));
    }

    if (!haveEmittedRealtimeMeta && FP::defined(frameTime) && realtimeEgress) {
        haveEmittedRealtimeMeta = true;
        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }


    Variant::Flags flags;
    {
        auto statusBits = ZSTATUS.read().toInteger();
        if (INTEGER::defined(statusBits)) {
            if (statusBits & 0x1U) {
                flags.insert("TooManyParticles");
            } else if (statusBits & 0x2U) {
                flags.insert("TimingUncertainty");
            }
        }
    }
    if (flags != priorInstrumentFlags) {
        realtimeStateUpdated = true;
        priorInstrumentFlags = flags;
    }

    if (!FP::defined(startTime) || !FP::defined(endTime))
        return 0;

    logValue(startTime, endTime, "F1", Variant::Root(std::move(flags)));
    logValue(startTime, endTime, "N", std::move(N));
    logValue(startTime, endTime, "ZMEANWIDTH", std::move(ZMEANWIDTH));
    logValue(startTime, endTime, "T1", std::move(T1));
    logValue(startTime, endTime, "T2", std::move(T2));
    logValue(startTime, endTime, "T3", std::move(T3));
    logValue(startTime, endTime, "P", std::move(P));
    logValue(startTime, endTime, "Q", std::move(Q));
    logValue(startTime, endTime, "Nb", std::move(Nb));
    logValue(startTime, endTime, "I", std::move(I));
    logValue(startTime, endTime, "Ig", std::move(Ig));
    logValue(startTime, endTime, "ZPUMPFB", std::move(ZPUMPFB));
    logValue(startTime, endTime, "ZLASERMON", std::move(ZLASERMON));
    if (Ns.read().exists())
        logValue(startTime, endTime, "Ns", std::move(Ns));

    realtimeValue(frameTime, "C", std::move(C));
    realtimeValue(frameTime, "A", std::move(A));
    realtimeValue(frameTime, "V", std::move(V));
    realtimeValue(frameTime, "Im", std::move(Im));
    realtimeValue(frameTime, "Igm", std::move(Igm));
    realtimeValue(frameTime, "ZPUMPTIME", std::move(ZPUMPTIME));
    realtimeValue(frameTime, "ZLASERFB", std::move(ZLASERFB));
    realtimeValue(frameTime, "Cb", std::move(Cb));

    if (realtimeStateUpdated && realtimeEgress && FP::defined(frameTime)) {
        realtimeStateUpdated = false;

        std::string state;
        if (priorInstrumentFlags.size() == 1) {
            state = *priorInstrumentFlags.begin();
        } else if (!priorInstrumentFlags.empty()) {
            std::vector<std::string>
                    sorted(priorInstrumentFlags.begin(), priorInstrumentFlags.end());
            std::sort(sorted.begin(), sorted.end());
            for (const auto &f : sorted) {
                if (!state.empty())
                    state += ",";
                state += f;
            }
            state = "STATE: " + state;
        } else {
            state = "Run";
        }

        realtimeEgress->incomingData(
                SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(state), frameTime,
                              FP::undefined()));
    }

    return 0;
}

void AcquireCSDPOPS::configAdvance(double frameTime)
{
    Q_ASSERT(!config.isEmpty());
    int oldSize = config.size();
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.isEmpty());

    if (oldSize == config.size())
        return;
    configurationChanged();
}

void AcquireCSDPOPS::configurationChanged()
{
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
}

void AcquireCSDPOPS::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (FP::defined(frameTime))
        configAdvance(frameTime);

    switch (responseState) {
    case RESP_AUTOPROBE_WAIT:
    case RESP_AUTOPROBE_INITIALIZE: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            if (++autoprobeValidRecords > 3) {
                qCDebug(log) << "Autoprobe succeeded at" << Logging::time(frameTime);

                responseState = RESP_RUN;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                timeoutAt(frameTime + 4.0);

                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("AutoprobeWait");
                event(frameTime, QObject::tr("Autoprobe succeeded."), false, info);

                realtimeStateUpdated = true;
            }
        } else if (code > 0) {
            qCDebug(log) << "Autoprobe failed at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            responseState = RESP_WAIT;
            autoprobeValidRecords = 0;

            if (realtimeEgress != NULL && FP::defined(frameTime)) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        } else {
            autoprobeValidRecords = 0;
            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;
    }

    case RESP_WAIT:
    case RESP_INITIALIZE: {
        if (processRecord(frame, frameTime) == 0) {
            qCDebug(log) << "Comms established at" << Logging::time(frameTime);

            responseState = RESP_RUN;
            ++autoprobeValidRecords;
            generalStatusUpdated();

            timeoutAt(frameTime + 4.0);

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("Wait");
            event(frameTime, QObject::tr("Communications established."), false, info);

            realtimeStateUpdated = true;
        } else {
            autoprobeValidRecords = 0;
            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;
    }

    case RESP_RUN: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + 4.0);
            ++autoprobeValidRecords;
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            responseState = RESP_WAIT;
            discardData(frameTime + 0.5, 1);
            generalStatusUpdated();
            autoprobeValidRecords = 0;

            Variant::Write info = Variant::Write::empty();
            info.hash("Code").setInteger(code);
            info.hash("Line").setString(frame.toString());
            info.hash("ResponseState").setString("Run");
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            if (realtimeEgress != NULL && FP::defined(frameTime)) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;
    }

    default:
        break;
    }
}

void AcquireCSDPOPS::incomingInstrumentTimeout(double frameTime)
{
    if (!FP::defined(frameTime))
        return;

    switch (responseState) {
    case RESP_RUN: {
        qCDebug(log) << "Timeout in unpolled mode at" << Logging::time(frameTime);

        if (controlStream != NULL)
            controlStream->resetControl();

        responseState = RESP_WAIT;
        discardData(frameTime + 2.0, 1);
        generalStatusUpdated();
        autoprobeValidRecords = 0;

        Variant::Write info = Variant::Write::empty();
        info.hash("ResponseState").setString("Run");
        event(frameTime, QObject::tr("Timeout waiting for report.  Communications dropped."), true,
              info);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;
    }

    case RESP_AUTOPROBE_WAIT:
        qCDebug(log) << "Timeout waiting for autoprobe records" << Logging::time(frameTime);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        if (controlStream != NULL)
            controlStream->resetControl();

        discardData(frameTime + 2.0, 1);
        responseState = RESP_WAIT;
        autoprobeValidRecords = 0;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;

    case RESP_INITIALIZE:
        discardData(frameTime + 2.0, 1);
        timeoutAt(frameTime + 10.0);
        responseState = RESP_WAIT;
        break;

    case RESP_AUTOPROBE_INITIALIZE:
        discardData(frameTime + 2.0, 1);
        timeoutAt(frameTime + 10.0);
        responseState = RESP_AUTOPROBE_WAIT;
        break;

    default:
        discardData(frameTime + 0.5, 1);
        loggingLost(frameTime);
        break;
    }
}

void AcquireCSDPOPS::discardDataCompleted(double frameTime)
{
    Q_UNUSED(frameTime);
}

void AcquireCSDPOPS::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_UNUSED(frameTime);
    Q_UNUSED(frame);
}

AcquisitionInterface::AutoprobeStatus AcquireCSDPOPS::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

Variant::Root AcquireCSDPOPS::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireCSDPOPS::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_RUN:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

void AcquireCSDPOPS::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_RUN:
        if (autoprobeValidRecords > 3) {
            /* Already running, just notify that we succeeded. */
            qCDebug(log) << "Interactive autoprobe requested with communications established at"
                         << Logging::time(time);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            break;
        }
        /* Fall through */

    default:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        timeoutAt(time + 10.0);
        discardData(time + 2.0, 1);
        responseState = RESP_AUTOPROBE_WAIT;
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Wait"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireCSDPOPS::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    timeoutAt(time + 10.0);
    discardData(time + 2.0, 1);
    responseState = RESP_AUTOPROBE_WAIT;
    generalStatusUpdated();
}

void AcquireCSDPOPS::autoprobePromote(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + 4.0);

    switch (responseState) {
    case RESP_RUN:
    case RESP_WAIT:
    case RESP_INITIALIZE:
        qCDebug(log) << "Promoted from state" << responseState << "to interactive acquisition at"
                     << Logging::time(time);
        break;

    case RESP_AUTOPROBE_WAIT:
    case RESP_AUTOPROBE_INITIALIZE:
        qCDebug(log) << "Promoted from passive autoprobe to interactive acquisition at"
                     << Logging::time(time);

        responseState = RESP_WAIT;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Wait"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireCSDPOPS::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + 4.0);

    switch (responseState) {
    case RESP_RUN:
    case RESP_WAIT:
    case RESP_INITIALIZE:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);
        break;

    case RESP_AUTOPROBE_WAIT:
    case RESP_AUTOPROBE_INITIALIZE:
        qCDebug(log) << "Promoted from passive autoprobe to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_WAIT;
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireCSDPOPS::getDefaults()
{
    AutomaticDefaults result;
    result.name = "N$1$2";
    result.setSerialN81(115200);
    return result;
}


ComponentOptions AcquireCSDPOPSComponent::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options);
    return options;
}

ComponentOptions AcquireCSDPOPSComponent::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireCSDPOPSComponent::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples());
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireCSDPOPSComponent::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireCSDPOPSComponent::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options, true);
}

QList<ComponentExample> AcquireCSDPOPSComponent::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data.")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireCSDPOPSComponent::createAcquisitionPassive(const ComponentOptions &options,
                                                                                const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireCSDPOPS(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireCSDPOPSComponent::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireCSDPOPS(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireCSDPOPSComponent::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext)
{
    std::unique_ptr<AcquireCSDPOPS> i(new AcquireCSDPOPS(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<
        AcquisitionInterface> AcquireCSDPOPSComponent::createAcquisitionInteractive(const ValueSegment::Transfer &config,
                                                                                    const std::string &loggingContext)
{
    std::unique_ptr<AcquireCSDPOPS> i(new AcquireCSDPOPS(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
