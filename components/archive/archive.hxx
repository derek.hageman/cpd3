/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ARCHIVE_H
#define ARCHIVE_H

#include "core/first.hxx"

#include <memory>
#include <QtGlobal>
#include <QObject>
#include <QIODevice>
#include <QString>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "datacore/externalsink.hxx"
#include "datacore/stream.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"

class ArchiveComponent;

class Archive : public CPD3::Data::ExternalSink {
    friend class ArchiveComponent;

    bool shouldRunTasks;

    enum class RemoveMode : int {
        RemoveNone,
        RemoveVariablePriorityZero,
        RemoveVariableAllPriorities,
        RemoveInstrumentPriorityZero,
        RemoveInstrumentAllPriorities,
        PurgeExact,
        PurgeVariableAllPriorities,
        PurgeInstrumentAllPriorities,
    };
    RemoveMode removeMode;

    enum class FlavorsMode : int {
        FlavorsExact, FlavorsAny, FlavorsFlatten,
    };
    FlavorsMode flavorsMode;

    CPD3::Data::StreamSink *target;
    CPD3::Threading::Signal<> terminateRequested;

    std::mutex mutex;
    bool threadComplete;
    bool receivedTerminate;
    bool firstProgress;

    CPD3::Threading::Signal<double> progress;
    std::unique_ptr<CPD3::Data::Archive::Access> access;
    std::shared_ptr<CPD3::Data::Archive::Access::BackgroundWrite> write;
    std::thread thread;

    bool isTerminated();

    void threadExit();

    void run();

public:
    explicit Archive(const CPD3::ComponentOptions &options);

    virtual ~Archive();

    void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

    void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

    void incomingData(const CPD3::Data::SequenceValue &value) override;

    void incomingData(CPD3::Data::SequenceValue &&value) override;

    void endData() override;

    void signalTerminate() override;

    void start() override;

    bool isFinished() override;
};

class ArchiveComponent : public QObject, virtual public CPD3::Data::ExternalSinkComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalSinkComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.archive"
                              FILE
                              "archive.json")

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresOutputDevice() override;

    CPD3::Data::ExternalSink *createDataSink(std::unique_ptr<
            CPD3::IO::Generic::Stream> &&stream = {},
                                             const CPD3::ComponentOptions &options = {}) override;
};


#endif
