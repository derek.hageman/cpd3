/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QBuffer>
#include <QTemporaryFile>

#include "datacore/externalsink.hxx"
#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "core/waitutils.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/archive/access.hxx"
#include "database/connection.hxx"

using namespace CPD3;
using namespace CPD3::Data;


class TestComponent : public QObject {
Q_OBJECT

    ExternalSinkComponent *component;

    QTemporaryFile databaseFile;

    void syncDatabase()
    {
        //QTest::qSleep(250);

        Database::Connection connection(Database::Storage::sqlite(databaseFile.fileName().toUtf8().constData()));
        if (!connection.start()) {
            Q_ASSERT(false);
            return;
        }

        for (;;) {
            auto tx = connection.transaction(Database::Connection::Transaction_Serializable);

            if (tx.end())
                break;
        }

        connection.shutdown();
        connection.wait();
    }

    bool verifyContents(SequenceValue::Transfer expected)
    {
        syncDatabase();

        auto result = Archive::Access(databaseFile).readSynchronous(Archive::Selection());
        for (auto rv = result.begin(); rv != result.end();) {
            auto check =
                    std::find_if(expected.begin(), expected.end(), [=](const SequenceValue &e) {
                        if (!FP::equal(e.getStart(), rv->getStart()))
                            return false;
                        if (!FP::equal(e.getEnd(), rv->getEnd()))
                            return false;
                        if (e.getUnit() != rv->getUnit())
                            return false;
                        if (e.getPriority() != rv->getPriority())
                            return false;
                        return e.getValue() == rv->getValue();
                    });
            if (check == expected.end()) {
                ++rv;
                continue;
            }

            rv = result.erase(rv);
            expected.erase(check);
        }

        if (!result.empty()) {
            qDebug() << "Unmatched result values:" << result;
            return false;
        }
        if (!expected.empty()) {
            qDebug() << "Unmatched expected values:" << expected;
            return false;
        }
        return true;
    }

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component = qobject_cast<ExternalSinkComponent *>(ComponentLoader::create("archive"));
        QVERIFY(component);

        QVERIFY(databaseFile.open());

        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("run-tasks")));
        QVERIFY(qobject_cast<ComponentOptionEnum *>(options.get("remove")));
        QVERIFY(qobject_cast<ComponentOptionEnum *>(options.get("remove-flavors")));
    }

    void simpleWrite()
    {
        ComponentOptions options(component->getOptions());
        ExternalSink *arc = component->createDataEgress(NULL, options);
        QVERIFY(arc != NULL);

        {
            SequenceValue::Transfer values
                    {SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                     SequenceValue({"brw", "raw_meta", "BsG_S11"}, Variant::Root(1.1), 100.0,
                                   200.0),
                     SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(2.0), 100.0, 200.0),
                     SequenceValue({"alt", "raw", "BsB_S11"}, Variant::Root(3.0), 200.0, 300.0),
                     SequenceValue({"brw", "clean", "BsB_S11"}, Variant::Root(4.0), 300.0, 400.0)};
            arc->incomingData(std::move(values));
        }
        arc->start();
        arc->endData();
        arc->wait();
        delete arc;

        SequenceValue::Transfer values
                {SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"brw", "raw_meta", "BsG_S11"}, Variant::Root(1.1), 100.0, 200.0),
                 SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(2.0), 100.0, 200.0),
                 SequenceValue({"alt", "raw", "BsB_S11"}, Variant::Root(3.0), 200.0, 300.0),
                 SequenceValue({"brw", "clean", "BsB_S11"}, Variant::Root(4.0), 300.0, 400.0)};
        QVERIFY(verifyContents(values));
    }

    void simpleWriteNoBuffer()
    {
        ComponentOptions options(component->getOptions());
        ExternalSink *arc = component->createDataEgress(NULL, options);
        QVERIFY(arc != NULL);

        SequenceValue::Transfer values
                {SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 200.0),
                 SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(2.0), 100.0, 200.0),
                 SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(3.0), 200.0, 300.0),
                 SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(4.0), 300.0, 400.0)};
        arc->start();
        QTest::qSleep(50);
        arc->incomingData(values.at(0));
        QTest::qSleep(50);
        arc->incomingData(values.at(1));
        QTest::qSleep(50);
        arc->incomingData(values.at(2));
        QTest::qSleep(50);
        arc->incomingData(values.at(3));
        QTest::qSleep(50);
        arc->endData();
        arc->wait();
        delete arc;

        QVERIFY(verifyContents(values));
    }

    void deleteOverlapping()
    {
        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(0.1), 50.0, 110.0),
                SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(0.2), 109.0, 150.0),
                SequenceValue({"brw", "raw", "BsG_S11", {"pm1"}}, Variant::Root(0.3), 150.0, 250.0),
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.4), 50.0, 150.0),
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.5), 200.0, 250.0),
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.7), 100.0, 300.0, 1)});

        syncDatabase();

        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionEnum *>(options.get("remove"))->set("variable-priority-zero");
        ExternalSink *arc = component->createDataEgress(NULL, options);
        QVERIFY(arc != NULL);
        arc->start();

        SequenceValue::Transfer values
                {SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(-1.0), 50.0, 300.0, 1),
                 SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 150.0),
                 SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(2.0), 150.0, 200.0),
                 SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(3.0), 150.0, 200.0)};
        arc->incomingData(values);
        arc->endData();
        arc->wait();
        delete arc;

        values.push_back(SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.4), 50.0, 150.0));
        values.push_back(
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.5), 200.0, 250.0));
        values.push_back(
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.7), 100.0, 300.0, 1));
        QVERIFY(verifyContents(values));
    }

    void deleteOverlappingIgnoreFlavors()
    {
        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(0.1), 50.0, 110.0),
                SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(0.2), 109.0, 150.0),
                SequenceValue({"brw", "raw", "BsG_S11", {"pm1"}}, Variant::Root(0.3), 150.0, 250.0),
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.4), 50.0, 150.0),
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.5), 200.0, 250.0),
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.7), 100.0, 300.0, 1),
                SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(0.8), 40.0, 300.0),});

        syncDatabase();

        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionEnum *>(options.get("remove"))->set("variable-priority-zero");
        qobject_cast<ComponentOptionEnum *>(options.get("remove-flavors"))->set("exact");
        ExternalSink *arc = component->createDataEgress(NULL, options);
        QVERIFY(arc != NULL);

        SequenceValue::Transfer values
                {SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(-1.0), 50.0, 150.0),
                 SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 150.0),
                 SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(2.0), 150.0, 200.0),
                 SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(3.0), 150.0, 200.0)};
        arc->incomingData(values);
        arc->start();
        arc->endData();
        arc->wait();
        delete arc;

        values.push_back(SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(0.8), 40.0, 50.0));
        values.push_back(
                SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(0.8), 200.0, 300.0));
        values.push_back(
                SequenceValue({"brw", "raw", "BsG_S11", {"pm1"}}, Variant::Root(0.3), 150.0,
                              250.0));
        values.push_back(SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.4), 50.0, 150.0));
        values.push_back(
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.5), 200.0, 250.0));
        values.push_back(
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.7), 100.0, 300.0, 1));
        QVERIFY(verifyContents(values));
    }

    void deleteOverlappingAllPriorities()
    {
        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(0.1), 50.0, 110.0),
                SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(0.2), 109.0, 150.0),
                SequenceValue({"brw", "raw", "BsG_S11", {"pm1"}}, Variant::Root(0.3), 150.0, 250.0),
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.4), 50.0, 150.0),
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.5), 200.0, 250.0),
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.7), 100.0, 300.0, 1)});

        syncDatabase();

        ComponentOptions options(component->getOptions());
        ExternalSink *arc = component->createDataEgress(NULL, options);
        QVERIFY(arc != NULL);

        SequenceValue::Transfer values
                {SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 150.0, 1),
                 SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(2.0), 150.0, 200.0),
                 SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(3.0), 150.0, 200.0)};
        arc->incomingData(values);
        arc->start();
        arc->endData();
        arc->wait();
        delete arc;

        values.push_back(SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(0.1), 50.0, 100.0));
        values.push_back(
                SequenceValue({"brw", "raw", "BsG_S11", {"pm1"}}, Variant::Root(0.3), 200.0,
                              250.0));
        values.push_back(SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.4), 50.0, 150.0));
        values.push_back(
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.5), 200.0, 250.0));
        values.push_back(
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.7), 100.0, 150.0, 1));
        values.push_back(
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.7), 200.0, 300.0, 1));
        QVERIFY(verifyContents(values));
    }

    void deleteOverlappingInstrument()
    {
        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(0.1), 50.0, 110.0),
                SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(0.2), 109.0, 150.0),
                SequenceValue({"brw", "raw", "BsG_S11", {"pm1"}}, Variant::Root(0.3), 150.0, 250.0),
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.4), 50.0, 150.0),
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.5), 200.0, 250.0),
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.7), 100.0, 300.0, 1)});

        syncDatabase();

        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionEnum *>(options.get("remove"))->set("instrument-priority-zero");
        ExternalSink *arc = component->createDataEgress(NULL, options);
        QVERIFY(arc != NULL);

        SequenceValue::Transfer values
                {SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(-1.0), 50.0, 300.0, 1),
                 SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 150.0),
                 SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(2.0), 150.0, 200.0)};
        arc->incomingData(values);
        arc->start();
        arc->endData();
        arc->wait();
        delete arc;

        values.push_back(
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.7), 100.0, 300.0, 1));
        QVERIFY(verifyContents(values));
    }

    void deleteOverlappingInstrumentAllPriorities()
    {
        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(0.1), 50.0, 110.0),
                SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(0.2), 109.0, 150.0),
                SequenceValue({"brw", "raw", "BsG_S11", {"pm1"}}, Variant::Root(0.3), 150.0, 250.0),
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.4), 50.0, 150.0),
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.5), 200.0, 250.0),
                SequenceValue({"brw", "raw", "BsB_S12"}, Variant::Root(0.6), 40.0, 350.0),
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.7), 100.0, 300.0, 1)});

        syncDatabase();

        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionEnum *>(options.get("remove"))->set("instrument");
        ExternalSink *arc = component->createDataEgress(NULL, options);
        QVERIFY(arc != NULL);

        SequenceValue::Transfer values
                {SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(-1.0), 50.0, 300.0, 1),
                 SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 150.0),
                 SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(2.0), 150.0, 200.0)};
        arc->incomingData(values);
        arc->start();
        arc->endData();
        arc->wait();
        delete arc;

        values.push_back(SequenceValue({"brw", "raw", "BsB_S12"}, Variant::Root(0.6), 40.0, 350.0));
        QVERIFY(verifyContents(values));
    }

    void replaceOnly()
    {
        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(0.1), 50.0, 150.0),
                SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(0.2), 100.0, 150.0),
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.4), 100.0, 150.0)});

        syncDatabase();

        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionEnum *>(options.get("remove"))->set("none");
        ExternalSink *arc = component->createDataEgress(NULL, options);
        QVERIFY(arc != NULL);

        SequenceValue::Transfer values
                {SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(-1.0), 50.0, 300.0, 1),
                 SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(1.0), 100.0, 150.0),
                 SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(2.0), 150.0, 200.0),
                 SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(3.0), 150.0, 200.0)};
        arc->incomingData(values);
        arc->start();
        arc->endData();
        QVERIFY(arc->wait());
        delete arc;

        values.push_back(SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(0.1), 50.0, 150.0));
        values.push_back(
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.4), 100.0, 150.0));
        QVERIFY(verifyContents(values));
    }

    void purgeExact()
    {
        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(0.1), 50.0, 150.0),
                SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(0.2), 100.0, 150.0),
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.4), 100.0, 150.0)});

        syncDatabase();

        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionEnum *>(options.get("remove"))->set("purge");
        ExternalSink *arc = component->createDataEgress(NULL, options);
        QVERIFY(arc != NULL);

        {
            SequenceValue::Transfer values
                    {SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(1.1), 50.0, 150.0),
                     SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(1.2), 100.0, 170.0)};
            arc->incomingData(std::move(values));
        }
        arc->start();
        arc->endData();
        arc->wait();
        delete arc;

        SequenceValue::Transfer values
                {SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(0.2), 100.0, 150.0),
                 SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.4), 100.0, 150.0)};
        QVERIFY(verifyContents(values));
    }

    void purgeVariable()
    {
        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(0.1), 50.0, 150.0),
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.2), 50.0, 200.0),
                SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(0.3), 100.0, 150.0),
                SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(0.4), 100.0, 250.0)});

        syncDatabase();

        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionEnum *>(options.get("remove"))->set("purge-variable");
        ExternalSink *arc = component->createDataEgress(NULL, options);
        QVERIFY(arc != NULL);

        {
            SequenceValue::Transfer values
                    {SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(1.1), 50.0, 150.0),
                     SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(1.2), 100.0, 160.0)};
            arc->incomingData(std::move(values));
        }
        arc->start();
        arc->endData();
        arc->wait();
        delete arc;

        SequenceValue::Transfer values
                {SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.2), 50.0, 200.0),
                 SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(0.4), 160.0, 250.0)};
        QVERIFY(verifyContents(values));
    }

    void purgeInstrument()
    {
        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(0.1), 50.0, 150.0),
                SequenceValue({"brw", "raw", "BsB_S12"}, Variant::Root(0.2), 50.0, 200.0),
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(0.3), 100.0, 150.0),
                SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(0.4), 100.0, 250.0)});

        syncDatabase();

        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionEnum *>(options.get("remove"))->set("purge-instrument");
        ExternalSink *arc = component->createDataEgress(NULL, options);
        QVERIFY(arc != NULL);

        {
            SequenceValue::Transfer values
                    {SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(1.1), 50.0, 150.0),
                     SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(1.2), 100.0, 160.0)};
            arc->incomingData(std::move(values));
        }
        arc->start();
        arc->endData();
        arc->wait();
        delete arc;

        SequenceValue::Transfer values
                {SequenceValue({"brw", "raw", "BsB_S12"}, Variant::Root(0.2), 50.0, 200.0),
                 SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(0.4), 160.0, 250.0)};
        QVERIFY(verifyContents(values));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
