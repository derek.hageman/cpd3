/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <unordered_map>
#include <functional>
#include <QtDebug>
#include <QRegularExpression>
#include <QLoggingCategory>

#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/util.hxx"
#include "datacore/externalsink.hxx"

#include "archive.hxx"

Q_LOGGING_CATEGORY(log_component_archive, "cpd3.component.archive", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;

namespace {

class RemoveReplaceOnly final
        : public CPD3::Data::StreamSink, public CPD3::Data::Archive::Access::BackgroundWrite {
    bool ended;
    bool finished;
    SequenceValue::Transfer incoming;
    SequenceValue::Transfer processing;

    bool stall() const
    { return incoming.size() > stallThreshold; }

public:
    RemoveReplaceOnly(CPD3::Data::Archive::Access &access) : BackgroundWrite(access),
                                                             ended(false),
                                                             finished(false)
    { }

    virtual ~RemoveReplaceOnly() = default;

    void incomingData(const SequenceValue::Transfer &values) override
    {
        if (values.empty())
            return;
        wake([=] {
            Util::append(values, incoming);
            return true;
        }, std::bind(&RemoveReplaceOnly::stall, this));
    }

    void incomingData(SequenceValue::Transfer &&values) override
    {
        if (values.empty())
            return;
        wake([this, &values] {
            Util::append(std::move(values), incoming);
            return true;
        }, std::bind(&RemoveReplaceOnly::stall, this));
    }

    void incomingData(const SequenceValue &value) override
    {
        wake([=] {
            incoming.emplace_back(value);
            return true;
        }, std::bind(&RemoveReplaceOnly::stall, this));
    }

    void incomingData(SequenceValue &&value) override
    {
        wake([this, &value] {
            incoming.emplace_back(std::move(value));
            return true;
        }, std::bind(&RemoveReplaceOnly::stall, this));
    }

    void endData() override
    {
        wake([=] {
            ended = true;
            return true;
        });
    }

    Threading::Signal<double> progress;

protected:
    virtual bool prepare()
    {
        finished = ended;

        if (incoming.empty())
            return !processing.empty();

        Util::append(std::move(incoming), processing);
        incoming.clear();
        return true;
    }

    virtual Result operation(CPD3::Data::Archive::Access &access)
    {
        if (!processing.empty())
            progress(processing.front().getStart());
        access.writeSynchronous(processing, true);
        return finished ? Success : Again;
    }

    virtual void success()
    {
        if (!processing.empty())
            progress(processing.back().getStart());
        processing.clear();
    }

    virtual bool isWaitComplete()
    { return ended; }
};

class RemovePurgeExact final
        : public CPD3::Data::StreamSink, public CPD3::Data::Archive::Access::BackgroundWrite {
    bool ended;
    bool finished;
    SequenceIdentity::Transfer incoming;
    SequenceIdentity::Transfer processing;

    bool stall() const
    { return incoming.size() > stallThreshold; }

public:
    RemovePurgeExact(CPD3::Data::Archive::Access &access) : BackgroundWrite(access),
                                                            ended(false),
                                                            finished(false)
    { }

    virtual ~RemovePurgeExact() = default;

    void incomingData(const SequenceValue::Transfer &values) override
    {
        if (values.empty())
            return;
        wake([=] {
            for (const auto &add : values) {
                incoming.emplace_back(add);
            }
            return true;
        }, std::bind(&RemovePurgeExact::stall, this));
    }

    void incomingData(SequenceValue::Transfer &&values) override
    {
        if (values.empty())
            return;
        wake([this, &values] {
            for (auto &add : values) {
                incoming.emplace_back(std::move(add));
            }
            return true;
        }, std::bind(&RemovePurgeExact::stall, this));
    }

    void incomingData(const SequenceValue &value) override
    {
        wake([=] {
            incoming.emplace_back(value);
            return true;
        }, std::bind(&RemovePurgeExact::stall, this));
    }

    void incomingData(SequenceValue &&value) override
    {
        wake([this, &value] {
            incoming.emplace_back(std::move(value));
            return true;
        }, std::bind(&RemovePurgeExact::stall, this));
    }

    void endData() override
    {
        wake([=] {
            ended = true;
            return true;
        });
    }

    Threading::Signal<double> progress;

protected:
    virtual bool prepare()
    {
        finished = ended;

        if (incoming.empty())
            return !processing.empty();

        Util::append(std::move(incoming), processing);
        incoming.clear();
        return true;
    }

    virtual Result operation(CPD3::Data::Archive::Access &access)
    {
        if (!processing.empty())
            progress(processing.front().getStart());
        access.removeSynchronous(processing, true);
        return finished ? Success : Again;
    }

    virtual void success()
    {
        if (!processing.empty())
            progress(processing.back().getStart());
        processing.clear();
    }

    virtual bool isWaitComplete()
    { return ended; }
};


using FlavorSequenceKeyApply = std::function<void(SequenceName &)>;
using FlavorSelectionGenerate = std::function<
        CPD3::Data::Archive::Selection::List(CPD3::Data::Archive::Selection &&)>;

struct FlavorsHandling {
    FlavorSequenceKeyApply key;
    FlavorSelectionGenerate selection;
};

static void flavorsSequenceKeyExact(SequenceName &)
{ }

static void flavorsSequenceKeyIgnore(SequenceName &key)
{ key.clearFlavors(); }

static const SequenceName::Flavors flattenFlavors{"cover", "end", "stats"};

static void flavorsSequenceKeyFlatten(SequenceName &key)
{ key.removeFlavors(flattenFlavors); }

static CPD3::Data::Archive::Selection::List flavorsSelectionExact(CPD3::Data::Archive::Selection &&sel)
{ return {std::move(sel)}; }

static CPD3::Data::Archive::Selection::List flavorsSelectionIgnore(CPD3::Data::Archive::Selection &&sel)
{
    sel.exactFlavors.clear();
    return {std::move(sel)};
}

static CPD3::Data::Archive::Selection::List flavorsSelectionFlatten(CPD3::Data::Archive::Selection &&sel)
{
    CPD3::Data::Archive::Selection::List result{std::move(sel)};
    for (const auto &add : flattenFlavors) {
        result.emplace_back(result.front());
        if (result.back().exactFlavors.size() == 1 && result.back().exactFlavors.front().empty()) {
            result.back().exactFlavors.clear();
        }
        result.back().exactFlavors.emplace_back(add);
    }
    return result;
}

template<typename KeyType, typename DataType = SequenceValue>
class RemoveRangeBase
        : public CPD3::Data::StreamSink, public CPD3::Data::Archive::Access::BackgroundWrite {
    typedef typename DataType::Transfer Transfer;

    bool ended;
    bool finished;
    Transfer incoming;
    Transfer processing;

    bool stall() const
    { return incoming.size() > stallThreshold; }

    std::unordered_map<KeyType, double> removalHistory;
    std::unordered_map<KeyType, double> pendingRemovalHistory;

    static inline void maybeFlush(CPD3::Data::Archive::Access &access,
                                  SequenceValue::Transfer &modify,
                                  SequenceIdentity::Transfer &remove)
    {
        if (modify.size() + remove.size() <= stallThreshold)
            return;
        access.writeSynchronous(modify, remove);
        modify.clear();
        remove.clear();
    }

public:
    RemoveRangeBase(CPD3::Data::Archive::Access &access) : BackgroundWrite(access),
                                                           ended(false),
                                                           finished(false)
    { }

    virtual ~RemoveRangeBase() = default;

    void incomingData(const SequenceValue::Transfer &values) override
    {
        if (values.empty())
            return;
        wake([this, &values] {
            addIncoming(values, incoming);
            return true;
        }, std::bind(&RemoveRangeBase<KeyType, DataType>::stall, this));
    }

    void incomingData(SequenceValue::Transfer &&values) override
    {
        if (values.empty())
            return;
        wake([this, &values] {
            addIncoming(std::move(values), incoming);
            return true;
        }, std::bind(&RemoveRangeBase<KeyType, DataType>::stall, this));
    }

    void incomingData(const SequenceValue &value) override
    {
        wake([this, &value] {
            addIncoming(value, incoming);
            return true;
        }, std::bind(&RemoveRangeBase<KeyType, DataType>::stall, this));
    }

    void incomingData(SequenceValue &&value) override
    {
        wake([this, &value] {
            addIncoming(std::move(value), incoming);
            return true;
        }, std::bind(&RemoveRangeBase<KeyType, DataType>::stall, this));
    }

    void endData() override
    {
        wake([this] {
            ended = true;
            return true;
        });
    }

    Threading::Signal<double> progress;

protected:
    virtual bool prepare()
    {
        finished = ended;

        if (incoming.empty())
            return !processing.empty();

        Util::append(std::move(incoming), processing);
        incoming.clear();
        return true;
    }

    virtual Result operation(CPD3::Data::Archive::Access &access)
    {
        if (!processing.empty())
            progress(processing.front().getStart());

        pendingRemovalHistory = removalHistory;

        /* First, build the ranges of data we need to remove */
        std::unordered_map<KeyType, Time::Bounds> processRemovals;
        for (const auto &in : processing) {
            if (bypassRange(in))
                continue;
            auto key = toRemovalKey(in);
            auto pr = processRemovals.find(key);

            if (pr == processRemovals.end()) {
                /* A new removal type, so create one */
                double start = in.getStart();

                /* Check the history to make sure we haven't already removed this (which
                 * also catches removing data we previously inserted) */
                auto history = pendingRemovalHistory.find(key);
                if (history != pendingRemovalHistory.end()) {
                    if (!FP::defined(history->second))
                        continue;

                    if (!FP::defined(start) || start < history->second) {
                        start = history->second;

                        /* Only check this after adjustment, so we can still insert
                         * zero length values, but we'll remove values that became
                         * zero length but were not before */
                        if (start >= in.getEnd())
                            continue;
                    }
                }

                Q_ASSERT(Range::compareStartEnd(start, in.getEnd()) <= 0);
                processRemovals.emplace(key, Time::Bounds(start, in.getEnd()));
                continue;
            }

            /* Already have a removal, so the start is already set, so just see if we need
             * to extend the end */
            if (Range::compareEnd(in.getEnd(), pr->second.getEnd()) > 0) {
                pr->second.setEnd(in.getEnd());
            }
        }

        SequenceValue::Transfer modify;
        SequenceIdentity::Transfer remove;

        /* Now that we have the ranges to remove, query the archive for any data that
         * intersects them. */
        for (const auto &rem : processRemovals) {
            double start = rem.second.getStart();
            double end = rem.second.getEnd();

            /* First, update the removal history, so we don't repeat this */
            Q_ASSERT(pendingRemovalHistory.count(rem.first) == 0 ||
                             Range::compareEnd(pendingRemovalHistory[rem.first], end) < 0);
            pendingRemovalHistory[rem.first] = end;

            auto selections = removalSelections(rem.first, start, end);
            if (selections.empty())
                continue;

            /* Go through the existing data, removing and modifying anything
             * that's affected by the new range */
            StreamSink::Iterator data;
            auto read = access.readStream(selections, &data);
            SequenceValue::Transfer values;
            while (!data.all(values) || !values.empty()) {
                for (const auto &check : values) {
                    if (!isAffectedByRemove(check))
                        continue;
                    if (Range::compareStartEnd(start, check.getEnd()) >= 0)
                        continue;
                    if (Range::compareStartEnd(check.getStart(), end) >= 0)
                        continue;

                    /* It's going to be removed: either it's being modified into
                     * one or more segments or it's contained entirely */
                    remove.push_back(SequenceIdentity(check));

                    if (Range::compareStart(check.getStart(), start) < 0) {
                        /* Create a segmented part before the removal range */
                        SequenceValue m = check;
                        m.setEnd(start);
                        modify.emplace_back(std::move(m));
                    }
                    if (Range::compareEnd(check.getEnd(), end) > 0) {
                        /* Create a segmented part after the removal range */
                        SequenceValue m = check;
                        m.setStart(end);
                        modify.emplace_back(std::move(m));
                    }
                }
                values.clear();
                maybeFlush(access, modify, remove);
            }
            read->wait();
        }

        finishWrite(access, modify, remove, processing);
        return finished ? Success : Again;
    }

    virtual void success()
    {
        removalHistory = std::move(pendingRemovalHistory);
        pendingRemovalHistory.clear();

        if (!processing.empty())
            progress(processing.back().getStart());

        processing.clear();
    }

    virtual bool isWaitComplete()
    { return ended; }


    virtual bool bypassRange(const DataType &incoming)
    {
        static const SequenceName::Component archiveEvents = "events";
        return incoming.getArchive() == archiveEvents;
    }

    virtual KeyType toRemovalKey(const SequenceIdentity &incoming) const = 0;

    virtual CPD3::Data::Archive::Selection::List removalSelections(const KeyType &key,
                                                                   double start,
                                                                   double end) const = 0;

    virtual bool isAffectedByRemove(const SequenceIdentity &archive) const = 0;

    virtual void addIncoming(const SequenceValue::Transfer &values, Transfer &target) = 0;

    virtual void addIncoming(SequenceValue::Transfer &&values, Transfer &target)
    { addIncoming(values, target); }

    virtual void addIncoming(const SequenceValue &value, Transfer &target)
    { addIncoming(SequenceValue::Transfer{value}, target); }

    virtual void addIncoming(SequenceValue &&value, Transfer &target)
    { addIncoming(SequenceValue::Transfer{std::move(value)}, target); }

    virtual void finishWrite(CPD3::Data::Archive::Access &access,
                             SequenceValue::Transfer &modify,
                             SequenceIdentity::Transfer &remove,
                             const Transfer &values) = 0;
};


template<typename KeyType = SequenceName>
class RemoveRangeSimple : public RemoveRangeBase<KeyType> {
public:
    RemoveRangeSimple(CPD3::Data::Archive::Access &access) : RemoveRangeBase<KeyType>(access)
    { }

    ~RemoveRangeSimple() = default;

protected:
    virtual void addIncoming(const SequenceValue::Transfer &values, SequenceValue::Transfer &target)
    { Util::append(values, target); }

    virtual void addIncoming(SequenceValue::Transfer &&values, SequenceValue::Transfer &target)
    { Util::append(std::move(values), target); }

    virtual void addIncoming(const SequenceValue &value, SequenceValue::Transfer &target)
    { target.emplace_back(value); }

    virtual void addIncoming(SequenceValue &&value, SequenceValue::Transfer &target)
    { target.emplace_back(std::move(value)); }

    virtual void finishWrite(CPD3::Data::Archive::Access &access,
                             SequenceValue::Transfer &modify,
                             SequenceIdentity::Transfer &remove,
                             const SequenceValue::Transfer &values)
    {
        if (!modify.empty() || !remove.empty()) {
            std::copy(values.begin(), values.end(), Util::back_emplacer(modify));
            access.writeSynchronous(modify, remove);
        } else {
            access.writeSynchronous(values, true);
        }
    }
};


class RemoveRangeVariable : public RemoveRangeSimple<SequenceName> {
    FlavorsHandling flavors;
public:
    RemoveRangeVariable(CPD3::Data::Archive::Access &access, FlavorsHandling &&flavors)
            : RemoveRangeSimple<SequenceName>(access), flavors(std::move(flavors))
    { }

    virtual ~RemoveRangeVariable() = default;

protected:
    virtual SequenceName toRemovalKey(const SequenceIdentity &incoming) const
    {
        SequenceName n = incoming.getName();
        flavors.key(n);
        return n;
    }

    virtual CPD3::Data::Archive::Selection::List removalSelections(const SequenceName &key,
                                                                   double start,
                                                                   double end) const
    {
        CPD3::Data::Archive::Selection sel(key, start, end);
        sel.includeMetaArchive = false;
        sel.includeDefaultStation = false;
        return flavors.selection(std::move(sel));
    }
};

class RemoveRangeVariablePriorityZero final : public RemoveRangeVariable {
public:
    RemoveRangeVariablePriorityZero(CPD3::Data::Archive::Access &access, FlavorsHandling &&flavors)
            : RemoveRangeVariable(access, std::move(flavors))
    { }

    virtual ~RemoveRangeVariablePriorityZero() = default;

protected:
    /* We generate the ranges based on all priorities, but only remove priority zero */
#if 0
    virtual bool bypassRange(const SequenceValue &incoming)
    {
        if (incoming.getPriority() != 0)
            return false;
        return RemoveRangeBase<SequenceName>::bypassRange(incoming);
    }
#endif

    virtual bool isAffectedByRemove(const SequenceIdentity &archive) const
    { return archive.getPriority() == 0; }
};

class RemoveRangeVariablePriorityAll final : public RemoveRangeVariable {
public:
    RemoveRangeVariablePriorityAll(CPD3::Data::Archive::Access &access, FlavorsHandling &&flavors)
            : RemoveRangeVariable(access, std::move(flavors))
    { }

    virtual ~RemoveRangeVariablePriorityAll() = default;

protected:
    virtual bool isAffectedByRemove(const SequenceIdentity &) const
    { return true; }
};


class RemoveRangeInstrument : public RemoveRangeSimple<SequenceName> {
    FlavorsHandling flavors;
public:
    RemoveRangeInstrument(CPD3::Data::Archive::Access &access, FlavorsHandling &&flavors)
            : RemoveRangeSimple<SequenceName>(access), flavors(std::move(flavors))
    { }

    virtual ~RemoveRangeInstrument() = default;

protected:
    virtual SequenceName toRemovalKey(const SequenceIdentity &incoming) const
    {
        SequenceName n = incoming.getName();
        flavors.key(n);
        n.clearMeta();
        n.setVariable(Util::suffix(n.getVariable(), '_'));
        return n;
    }

    virtual CPD3::Data::Archive::Selection::List removalSelections(const SequenceName &key,
                                                                   double start,
                                                                   double end) const
    {
        CPD3::Data::Archive::Selection sel(key, start, end);
        sel.variables.clear();
        sel.variables
           .push_back(
                   "[^_]+_" + QRegularExpression::escape(key.getVariableQString()).toStdString());
        sel.includeMetaArchive = true;
        sel.includeDefaultStation = false;
        return flavors.selection(std::move(sel));
    }
};

class RemoveRangeInstrumentPriorityZero final : public RemoveRangeInstrument {
public:
    RemoveRangeInstrumentPriorityZero(CPD3::Data::Archive::Access &access,
                                      FlavorsHandling &&flavors) : RemoveRangeInstrument(access,
                                                                                         std::move(
                                                                                                 flavors))
    { }

    virtual ~RemoveRangeInstrumentPriorityZero() = default;

protected:
    /* We generate the ranges based on all priorities, but only remove priority zero */
#if 0
    virtual bool bypassRange(const SequenceValue &incoming)
    {
        if (incoming.getPriority() != 0)
            return false;
        return RemoveRangeBase<SequenceName>::bypassRange(incoming);
    }
#endif

    virtual bool isAffectedByRemove(const SequenceIdentity &archive) const
    { return archive.getPriority() == 0; }
};

class RemoveRangeInstrumentPriorityAll final : public RemoveRangeInstrument {
public:
    RemoveRangeInstrumentPriorityAll(CPD3::Data::Archive::Access &access, FlavorsHandling &&flavors)
            : RemoveRangeInstrument(access, std::move(flavors))
    { }

    virtual ~RemoveRangeInstrumentPriorityAll() = default;

protected:
    virtual bool isAffectedByRemove(const SequenceIdentity &) const
    { return true; }
};


template<typename KeyType = SequenceName>
class PurgeRangeBase : public RemoveRangeBase<KeyType, SequenceIdentity> {
public:
    PurgeRangeBase(CPD3::Data::Archive::Access &access) : RemoveRangeBase<KeyType,
                                                                          SequenceIdentity>(access)
    { }

    ~PurgeRangeBase() = default;

protected:
    virtual void addIncoming(const SequenceValue::Transfer &values,
                             SequenceIdentity::Transfer &target)
    { std::copy(values.begin(), values.end(), Util::back_emplacer(target)); }

    virtual void addIncoming(SequenceValue::Transfer &&values, SequenceIdentity::Transfer &target)
    { std::move(values.begin(), values.end(), Util::back_emplacer(target)); }

    virtual void addIncoming(const SequenceValue &value, SequenceIdentity::Transfer &target)
    { target.emplace_back(value); }

    virtual void addIncoming(SequenceValue &&value, SequenceIdentity::Transfer &target)
    { target.emplace_back(std::move(value)); }

    virtual void finishWrite(CPD3::Data::Archive::Access &access,
                             SequenceValue::Transfer &modify,
                             SequenceIdentity::Transfer &remove,
                             const SequenceIdentity::Transfer &values)
    {
        if (modify.empty()) {
            if (remove.empty()) {
                access.removeSynchronous(values, true);
            } else {
                std::copy(values.begin(), values.end(), Util::back_emplacer(remove));
                access.removeSynchronous(remove);
            }
        } else {
            std::copy(values.begin(), values.end(), Util::back_emplacer(remove));
            access.writeSynchronous(modify, remove);
        }
    }
};


class PurgeRangeVariable : public PurgeRangeBase<SequenceName> {
    FlavorsHandling flavors;
public:
    PurgeRangeVariable(CPD3::Data::Archive::Access &access, FlavorsHandling &&flavors)
            : PurgeRangeBase<SequenceName>(access), flavors(std::move(flavors))
    { }

    virtual ~PurgeRangeVariable() = default;

protected:
    virtual SequenceName toRemovalKey(const SequenceIdentity &incoming) const
    {
        SequenceName n = incoming.getName();
        flavors.key(n);
        return n;
    }

    virtual CPD3::Data::Archive::Selection::List removalSelections(const SequenceName &key,
                                                                   double start,
                                                                   double end) const
    {
        CPD3::Data::Archive::Selection sel(key, start, end);
        sel.includeMetaArchive = false;
        sel.includeDefaultStation = false;
        return flavors.selection(std::move(sel));
    }
};

class PurgeRangeVariablePriorityAll final : public PurgeRangeVariable {
public:
    PurgeRangeVariablePriorityAll(CPD3::Data::Archive::Access &access, FlavorsHandling &&flavors)
            : PurgeRangeVariable(access, std::move(flavors))
    { }

    virtual ~PurgeRangeVariablePriorityAll() = default;

protected:
    virtual bool isAffectedByRemove(const SequenceIdentity &) const
    { return true; }
};


class PurgeRangeInstrument : public PurgeRangeBase<SequenceName> {
    FlavorsHandling flavors;
public:
    PurgeRangeInstrument(CPD3::Data::Archive::Access &access, FlavorsHandling &&flavors)
            : PurgeRangeBase<SequenceName>(access), flavors(std::move(flavors))
    { }

    virtual ~PurgeRangeInstrument() = default;

protected:
    virtual SequenceName toRemovalKey(const SequenceIdentity &incoming) const
    {
        SequenceName n = incoming.getName();
        flavors.key(n);
        n.clearMeta();
        n.setVariable(Util::suffix(n.getVariable(), '_'));
        return n;
    }

    virtual CPD3::Data::Archive::Selection::List removalSelections(const SequenceName &key,
                                                                   double start,
                                                                   double end) const
    {
        CPD3::Data::Archive::Selection sel(key, start, end);
        sel.variables.clear();
        sel.variables
           .push_back(
                   "[^_]+_" + QRegularExpression::escape(key.getVariableQString()).toStdString());
        sel.includeMetaArchive = true;
        sel.includeDefaultStation = false;
        return flavors.selection(std::move(sel));
    }
};

class PurgeRangeInstrumentPriorityAll final : public PurgeRangeInstrument {
public:
    PurgeRangeInstrumentPriorityAll(CPD3::Data::Archive::Access &access, FlavorsHandling &&flavors)
            : PurgeRangeInstrument(access, std::move(flavors))
    { }

    virtual ~PurgeRangeInstrumentPriorityAll() = default;

protected:
    virtual bool isAffectedByRemove(const SequenceIdentity &) const
    { return true; }
};

}


::Archive::Archive(const ComponentOptions &options)
{
    removeMode = RemoveMode::RemoveVariableAllPriorities;
    flavorsMode = FlavorsMode::FlavorsAny;
    shouldRunTasks = false;
    receivedTerminate = false;
    threadComplete = false;
    target = nullptr;

    if (options.isSet("run-tasks")) {
        shouldRunTasks = qobject_cast<ComponentOptionBoolean *>(options.get("run-tasks"))->get();
    }
    if (options.isSet("remove")) {
        removeMode = static_cast<RemoveMode>(qobject_cast<ComponentOptionEnum *>(
                options.get("remove"))->get().getID());
    }
    if (options.isSet("remove-flavors")) {
        flavorsMode = static_cast<FlavorsMode>(qobject_cast<ComponentOptionEnum *>(
                options.get("remove-flavors"))->get().getID());
    }

    FlavorsHandling flavors;
    switch (flavorsMode) {
    case FlavorsMode::FlavorsExact:
        flavors.key = flavorsSequenceKeyExact;
        flavors.selection = flavorsSelectionExact;
        break;
    case FlavorsMode::FlavorsAny:
        flavors.key = flavorsSequenceKeyIgnore;
        flavors.selection = flavorsSelectionIgnore;
        break;
    case FlavorsMode::FlavorsFlatten:
        flavors.key = flavorsSequenceKeyFlatten;
        flavors.selection = flavorsSelectionFlatten;
        break;
    }

    access.reset(new CPD3::Data::Archive::Access);

    Q_ASSERT(access.get() != nullptr);
    switch (removeMode) {
    case RemoveMode::RemoveNone: {
        auto op = std::make_shared<RemoveReplaceOnly>(*access);
        progress = op->progress;
        target = op.get();
        write = std::move(op);
        break;
    }
    case RemoveMode::RemoveVariablePriorityZero: {
        auto op = std::make_shared<RemoveRangeVariablePriorityZero>(*access, std::move(flavors));
        progress = op->progress;
        target = op.get();
        write = std::move(op);
        break;
    }
    case RemoveMode::RemoveVariableAllPriorities: {
        auto op = std::make_shared<RemoveRangeVariablePriorityAll>(*access, std::move(flavors));
        progress = op->progress;
        target = op.get();
        write = std::move(op);
        break;
    }
    case RemoveMode::RemoveInstrumentPriorityZero: {
        auto op = std::make_shared<RemoveRangeInstrumentPriorityZero>(*access, std::move(flavors));
        progress = op->progress;
        target = op.get();
        write = std::move(op);
        break;
    }
    case RemoveMode::RemoveInstrumentAllPriorities: {
        auto op = std::make_shared<RemoveRangeInstrumentPriorityAll>(*access, std::move(flavors));
        progress = op->progress;
        target = op.get();
        write = std::move(op);
        break;
    }
    case RemoveMode::PurgeExact: {
        auto op = std::make_shared<RemovePurgeExact>(*access);
        progress = op->progress;
        target = op.get();
        write = std::move(op);
        break;
    }
    case RemoveMode::PurgeVariableAllPriorities: {
        auto op = std::make_shared<PurgeRangeVariablePriorityAll>(*access, std::move(flavors));
        progress = op->progress;
        target = op.get();
        write = std::move(op);
        break;
    }
    case RemoveMode::PurgeInstrumentAllPriorities: {
        auto op = std::make_shared<PurgeRangeInstrumentPriorityAll>(*access, std::move(flavors));
        progress = op->progress;
        target = op.get();
        write = std::move(op);
        break;
    }
    }

    firstProgress = true;
    progress.connect([this](double time) {
        if (firstProgress) {
            firstProgress = false;
            feedback.emitStage(QObject::tr("Archiving data"), QObject::tr(
                    "The system is processing data and writing it into the archive."), true);
        }
        feedback.emitProgressTime(time);
    });
    terminateRequested.connect([this] {
        write->signalTerminate();
    });
}

::Archive::~Archive()
{
    signalTerminate();
    if (thread.joinable())
        thread.join();
    progress.disconnect();
    terminateRequested.disconnect();
    target = nullptr;
    if (write) {
        write->signalTerminate();
        write->wait();
        write.reset();
    }
    access.reset();
}

void ::Archive::run()
{
    if (isTerminated())
        write->signalTerminate();
    write->wait();
    progress.disconnect();
    terminateRequested.disconnect();

    if (isTerminated())
        return threadExit();

    target = nullptr;
    write.reset();
    access.reset();

    if (!shouldRunTasks)
        return threadExit();
    auto comp = qobject_cast<ActionComponent *>(ComponentLoader::create("tasks"));
    if (!comp)
        return threadExit();
    std::unique_ptr<CPD3Action> tasks(comp->createAction(comp->getOptions()));
    if (!tasks)
        return threadExit();

    tasks->feedback.forward(feedback);
    terminateRequested.connect(tasks.get(), [&tasks] {
        tasks->signalTerminate();
    }, true);
    tasks->start();

    if (isTerminated())
        tasks->signalTerminate();
    tasks->wait();
    terminateRequested.disconnect();
    tasks.reset();

    return threadExit();
}

void ::Archive::threadExit()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        threadComplete = true;
    }
    finished();
}

bool ::Archive::isFinished()
{
    std::lock_guard<std::mutex> lock(mutex);
    return threadComplete;
}

bool ::Archive::isTerminated()
{
    std::lock_guard<std::mutex> lock(mutex);
    return receivedTerminate;
}

void ::Archive::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        receivedTerminate = true;
    }
    terminateRequested();
}

void ::Archive::start()
{ thread = std::thread(std::bind(&::Archive::run, this)); }

void ::Archive::incomingData(const SequenceValue::Transfer &values)
{ target->incomingData(values); }

void ::Archive::incomingData(SequenceValue::Transfer &&values)
{ target->incomingData(std::move(values)); }

void ::Archive::incomingData(const SequenceValue &value)
{ target->incomingData(value); }

void ::Archive::incomingData(SequenceValue &&value)
{ target->incomingData(std::move(value)); }

void ::Archive::endData()
{
    Q_ASSERT(target);
    target->endData();
    target = nullptr;
}


ComponentOptions ArchiveComponent::getOptions()
{
    ComponentOptions options;

    options.add("run-tasks",
                new ComponentOptionBoolean(tr("run-tasks", "name"), tr("Run the archive add tasks"),
                                           tr("If set then the immediate tasks are run after the archive "
                                              "add."), tr("Enabled")));

    ComponentOptionEnum *remove =
            new ComponentOptionEnum(tr("remove", "name"), tr("Existing removal mode"),
                                    tr("This defines the data explicitly removed as part of the write.  "
                                       "Identical (same station, archive, variable, flavors, start, end, "
                                       "and priority) values are always replaced and do not need removal.  "
                                       "Data in the event archive is never removed."),
                                    tr("Remove values at all priorities within the updated time range"));
    remove->add(static_cast<int>(::Archive::RemoveMode::RemoveNone), "none",
                tr("none", "mode name"),
                tr("Do not explicitly remove any values, only directly replace any inputs."));
    remove->add(static_cast<int>(::Archive::RemoveMode::RemoveVariablePriorityZero),
                "variable-priority-zero",
                tr("variable-priority-zero", "mode name"),
                tr("Remove all values of a variable (unique station, archive, and "
                   "variable) with priority zero within the range the update of that "
                   "variable spans."));
    remove->add(static_cast<int>(::Archive::RemoveMode::RemoveVariableAllPriorities), "variable",
                tr("variable", "mode name"),
                tr("Remove all values of a variable (unique station, archive, and "
                   "variable) of any priority within the range the update of that "
                   "variable spans."));
    remove->add(static_cast<int>(::Archive::RemoveMode::RemoveInstrumentPriorityZero),
                "instrument-priority-zero",
                tr("instrument-priority-zero", "mode name"),
                tr("Remove all values of an instrument (unique station, archive, and "
                   "suffix) with priority zero within the range the update of that "
                   "instrument spans."));
    remove->add(static_cast<int>(::Archive::RemoveMode::RemoveInstrumentAllPriorities),
                "instrument",
                tr("instrument", "mode name"),
                tr("Remove all values of an instrument (unique station, archive, and "
                   "suffix) of any priority within the range the update of that "
                   "instrument spans."));
    remove->add(static_cast<int>(::Archive::RemoveMode::PurgeExact), "purge",
                tr("purge", "mode name"),
                tr("Remove all values seen in the input from the archive entirely."));
    remove->add(static_cast<int>(::Archive::RemoveMode::PurgeVariableAllPriorities),
                "purge-variable",
                tr("purge-variable", "mode name"),
                tr("Remove all values of a variable (unique station, archive, and "
                   "variable) of any priority within the range of data while "
                   "also removing any values seen in the input."));
    remove->add(static_cast<int>(::Archive::RemoveMode::PurgeInstrumentAllPriorities),
                "purge-instrument",
                tr("purge-instrument", "mode name"),
                tr("Remove all values of an instrument (unique station, archive, and "
                   "suffix) of any priority within the range the of data while "
                   "also removing any values seen in the input."));
    options.add("remove", remove);

    ComponentOptionEnum *flavors =
            new ComponentOptionEnum(tr("remove-flavors", "name"), tr("Flavors removal mode"),
                                    tr("This defines how flavors are considered during data removal."),
                                    tr("Ignore flavors and only consider the other components"));
    flavors->add(static_cast<int>(::Archive::FlavorsMode::FlavorsAny), "any",
                 tr("any", "mode name"),
                 tr("Ignore flavors and only consider the other components."));
    flavors->add(static_cast<int>(::Archive::FlavorsMode::FlavorsExact), "exact",
                 tr("exact", "mode name"),
                 tr("Require an exact match of all flavors for removal."));
    flavors->add(static_cast<int>(::Archive::FlavorsMode::FlavorsFlatten), "flatten",
                 tr("flatten", "mode name"),
                 tr("Require a match of flavors but consider secondary components (e.x. coverage) "
                    "as part of their main variable."));
    options.add("remove-flavors", flavors);

    return options;
}

QList<ComponentExample> ArchiveComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Defaults", "default example name"),
                                     tr("This will add data to the archive, merging the metadata with any "
                                        "that already exists to form a continuous record.  Data are "
                                        "replaced on a per-variable basis.")));
    examples.last().setInputTypes(ComponentExample::Input_FileOnly);

    return examples;
}

bool ArchiveComponent::requiresOutputDevice()
{ return false; }

ExternalSink *ArchiveComponent::createDataSink(std::unique_ptr<CPD3::IO::Generic::Stream> &&,
                                               const CPD3::ComponentOptions &options)
{
    return new ::Archive(options);
}
