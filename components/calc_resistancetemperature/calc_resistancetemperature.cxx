/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <cmath>
#include <QRegularExpression>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "core/environment.hxx"
#include "core/qtcompat.hxx"
#include "core/timeutils.hxx"

#include "calc_resistancetemperature.hxx"

using namespace CPD3;
using namespace CPD3::Data;


CalcResistanceTemperature::CalcResistanceTemperature(const ComponentOptions &options)
{
    restrictedInputs = false;

    Calculation calc = Calculation::Steinhart_Hart;
    if (options.isSet("type")) {
        calc = static_cast<Calculation>(qobject_cast<ComponentOptionEnum *>(
                options.get("type"))->get().getID());
    }
    defaultCalculation.reset(new DynamicPrimitive<Calculation>::Constant(calc));

    if (options.isSet("resistance-calibration")) {
        defaultResistanceCalibration.reset(qobject_cast<DynamicCalibrationOption *>(
                options.get("resistance-calibration"))->getInput());
    } else {
        switch (calc) {
        case Calculation::Steinhart_Hart:
        case Calculation::NTC:
            defaultResistanceCalibration.reset(
                    new DynamicCalibration::Constant(Calibration(QVector<double>{0, 1000.0})));
            break;
        case Calculation::None:
        case Calculation::Table:
        case Calculation::RTD:
            defaultResistanceCalibration.reset(new DynamicCalibration::Constant);
            break;
        }
    }

    if (options.isSet("a")) {
        defaultInputA.reset(qobject_cast<DynamicInputOption *>(options.get("a"))->getInput());
    } else {
        switch (calc) {
        case Calculation::Steinhart_Hart:
            defaultInputA.reset(new DynamicInput::Constant(0.0010295));
            break;
        case Calculation::NTC:
            defaultInputA.reset(new DynamicInput::Constant(10));
            break;
        case Calculation::RTD:
            defaultInputA.reset(new DynamicInput::Constant(100));
            break;
        case Calculation::Table:
            defaultInputA.reset(new DynamicInput::Constant);
            break;
        case Calculation::None:
            defaultInputA.reset(new DynamicInput::Constant);
            break;
        }

    }
    if (options.isSet("b")) {
        defaultInputB.reset(qobject_cast<DynamicInputOption *>(options.get("b"))->getInput());
    } else {
        switch (calc) {
        case Calculation::Steinhart_Hart:
            defaultInputB.reset(new DynamicInput::Constant(0.0002391));
            break;
        case Calculation::NTC:
            defaultInputB.reset(new DynamicInput::Constant(4000));
            break;
        case Calculation::RTD:
            defaultInputB.reset(new DynamicInput::Constant(0.003851));
            break;
        case Calculation::Table:
            defaultInputB.reset(new DynamicInput::Constant);
            break;
        case Calculation::None:
            defaultInputB.reset(new DynamicInput::Constant);
            break;
        }
    }
    if (options.isSet("c")) {
        defaultInputC.reset(qobject_cast<DynamicInputOption *>(options.get("c"))->getInput());
    } else {
        switch (calc) {
        case Calculation::Steinhart_Hart:
            defaultInputC.reset(new DynamicInput::Constant(0.0000001568));
            break;
        case Calculation::NTC:
            defaultInputC.reset(new DynamicInput::Constant(25));
            break;
        case Calculation::RTD:
            defaultInputC.reset(new DynamicInput::Constant(0));
            break;
        case Calculation::Table:
            defaultInputC.reset(new DynamicInput::Constant);
            break;
        case Calculation::None:
            defaultInputC.reset(new DynamicInput::Constant);
            break;
        }
    }

    if (options.isSet("resistance") || options.isSet("output")) {
        restrictedInputs = true;

        Processing proc;

        SequenceName::Component suffix;
        SequenceName::Component station;
        SequenceName::Component archive;
        SequenceName::Flavors flavors;
        if (options.isSet("output")) {
            proc.operateOutput
                .reset(qobject_cast<DynamicSequenceSelectionOption *>(
                        options.get("output"))->getOperator());

            QRegularExpression reCheck("(?:T|TD)([uc]?\\d*_.+)");
            for (const auto &name : proc.operateOutput->getAllUnits()) {
                if (!suffix.empty())
                    break;

                station = name.getStation();
                archive = name.getArchive();
                flavors = name.getFlavors();

                auto r =
                        reCheck.match(name.getVariableQString(), 0, QRegularExpression::NormalMatch,
                                      QRegularExpression::AnchoredMatchOption);
                if (r.hasMatch()) {
                    suffix = r.captured(1).toStdString();
                }
            }
        }
        if (options.isSet("resistance")) {
            proc.inputResistance
                .reset(qobject_cast<DynamicInputOption *>(options.get("resistance"))->getInput());

            QRegularExpression reCheck("(?:W|V)([uc]?\\d*_.+)");
            for (const auto &name : proc.inputResistance->getUsedInputs()) {
                if (!suffix.empty())
                    break;

                station = name.getStation();
                archive = name.getArchive();
                flavors = name.getFlavors();

                auto r =
                        reCheck.match(name.getVariableQString(), 0, QRegularExpression::NormalMatch,
                                      QRegularExpression::AnchoredMatchOption);
                if (r.hasMatch()) {
                    suffix = r.captured(1).toStdString();
                }
            }
        }


        if (!proc.operateOutput) {
            if (suffix.empty()) {
                proc.operateOutput
                    .reset(new DynamicSequenceSelection::Match(QString::fromStdString(station),
                                                               QString::fromStdString(archive),
                                                               "T[uc]?\\d*_.*", flavors));
            } else {
                proc.operateOutput
                    .reset(new DynamicSequenceSelection::Basic(
                            SequenceName(station, archive, "T" + suffix, flavors)));
            }
        }
        if (!proc.inputResistance) {
            auto sel = new DynamicInput::Variable;
            proc.inputResistance.reset(sel);
            if (suffix.empty()) {
                sel->set(SequenceMatch::OrderedLookup(
                        SequenceMatch::Element(station, archive, "W[uc]?\\d*_.*", flavors)),
                         Calibration());
            } else {
                sel->set(SequenceMatch::OrderedLookup(
                        SequenceMatch::Element(station, archive, "W" + suffix, flavors)),
                         Calibration());
            }
        }

        proc.calculation = std::move(defaultCalculation);
        proc.resistanceCalibration = std::move(defaultResistanceCalibration);
        proc.inputA = std::move(defaultInputA);
        proc.inputB = std::move(defaultInputB);
        proc.inputC = std::move(defaultInputC);
        proc.lookupTable.reset(new DynamicPrimitive<LookupTable>::Constant);

        processing.emplace_back(std::move(proc));
    }

    if (options.isSet("suffix")) {
        filterSuffixes = Util::set_from_qstring(
                qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->get());
    }
}

CalcResistanceTemperature::CalcResistanceTemperature(const ComponentOptions &options,
                                                     double,
                                                     double,
                                                     const QList<SequenceName> &inputs)
        : CalcResistanceTemperature(options)
{
    for (const auto &name : inputs) {
        CalcResistanceTemperature::unhandled(name, nullptr);
    }
}

static Calculation convertCalculation(const Variant::Read &value)
{
    const auto &type = value.toString();
    if (Util::equal_insensitive(type, "NTC", "Thermistor"))
        return Calculation::NTC;
    else if (Util::equal_insensitive(type, "RTD", "PT100", "PT1000"))
        return Calculation::RTD;
    else if (Util::equal_insensitive(type, "Table", "LookupTable", "Lookup"))
        return Calculation::Table;
    else if (Util::equal_insensitive(type, "None", "Temperature", "Disable"))
        return Calculation::None;

    return Calculation::Steinhart_Hart;
}

static LookupTable convertLookupTable(const Variant::Read &value)
{
    LookupTable result;
    for (const auto &i : value.toKeyframe()) {
        if (!FP::defined(i.first))
            continue;
        double t = i.second.toReal();
        if (!FP::defined(t))
            continue;
        result.emplace(i.first, t);
    }
    return result;
}

CalcResistanceTemperature::CalcResistanceTemperature(double start,
                                                     double end,
                                                     const SequenceName::Component &station,
                                                     const SequenceName::Component &archive,
                                                     const ValueSegment::Transfer &config)
{
    restrictedInputs = true;

    std::unordered_set<Variant::PathElement::HashIndex> children;
    for (const auto &add : config) {
        Util::merge(add.read().toHash().keys(), children);
    }
    children.erase(Variant::PathElement::HashIndex());

    for (const auto &child : children) {
        Processing proc;

        proc.operateOutput
            .reset(DynamicSequenceSelection::fromConfiguration(config,
                                                               QString("%1/CalculateTemperature").arg(
                                                                       QString::fromStdString(
                                                                               child)), start,
                                                               end));

        proc.inputResistance
            .reset(DynamicInput::fromConfiguration(config, QString("%1/Resistance").arg(
                    QString::fromStdString(child)), start, end));
        proc.inputA
            .reset(DynamicInput::fromConfiguration(config, QString("%1/A").arg(
                    QString::fromStdString(child)), start, end));
        proc.inputB
            .reset(DynamicInput::fromConfiguration(config, QString("%1/B").arg(
                    QString::fromStdString(child)), start, end));
        proc.inputC
            .reset(DynamicInput::fromConfiguration(config, QString("%1/C").arg(
                    QString::fromStdString(child)), start, end));

        proc.resistanceCalibration
            .reset(DynamicCalibrationOption::fromConfiguration(config,
                                                               QString("%1/ResistanceCalibration").arg(
                                                                       QString::fromStdString(
                                                                               child)), start,
                                                               end));

        proc.calculation
            .reset(DynamicPrimitive<Calculation>::fromConfiguration(convertCalculation, config,
                                                                    QString("%1/Type").arg(
                                                                            QString::fromStdString(
                                                                                    child)), start,
                                                                    end));

        proc.lookupTable
            .reset(DynamicPrimitive<LookupTable>::fromConfiguration(convertLookupTable, config,
                                                                    QString("%1/Table").arg(
                                                                            QString::fromStdString(
                                                                                    child)), start,
                                                                    end));

        proc.operateOutput->registerExpected(station, archive);
        proc.inputResistance->registerExpected(station, archive);
        proc.inputA->registerExpected(station, archive);
        proc.inputB->registerExpected(station, archive);
        proc.inputC->registerExpected(station, archive);

        processing.emplace_back(std::move(proc));
    }
}


CalcResistanceTemperature::~CalcResistanceTemperature() = default;

void CalcResistanceTemperature::handleNewProcessing(const SequenceName &name,
                                                    std::size_t id,
                                                    SegmentProcessingStage::SequenceHandlerControl *control)
{
    auto &proc = processing[id];

    SequenceName::Set reg;

    proc.operateOutput
        ->registerExpected(name.getStation(), name.getArchive(), {}, name.getFlavors());
    Util::merge(proc.operateOutput->getAllUnits(), reg);

    if (!control)
        return;

    for (const auto &n : reg) {
        bool isNew = !control->isHandled(n);
        control->filterUnit(n, id);
        if (isNew)
            registerPossibleInput(n, control, id);
    }
}

void CalcResistanceTemperature::registerPossibleInput(const SequenceName &name,
                                                      SegmentProcessingStage::SequenceHandlerControl *control,
                                                      std::size_t filterID)
{
    for (std::size_t id = 0, max = processing.size(); id < max; id++) {
        bool usedAsInput = false;
        auto &proc = processing[id];

        if (proc.inputResistance->registerInput(name)) {
            if (control)
                control->inputUnit(name, id);
            usedAsInput = true;
        }
        if (proc.inputA->registerInput(name)) {
            if (control)
                control->inputUnit(name, id);
            usedAsInput = true;
        }
        if (proc.inputB->registerInput(name)) {
            if (control)
                control->inputUnit(name, id);
            usedAsInput = true;
        }
        if (proc.inputC->registerInput(name)) {
            if (control)
                control->inputUnit(name, id);
            usedAsInput = true;
        }
        if (usedAsInput && id != filterID)
            handleNewProcessing(name, id, control);
    }

    if (defaultInputA && defaultInputA->registerInput(name) && control)
        control->deferHandling(name);
    if (defaultInputB && defaultInputB->registerInput(name) && control)
        control->deferHandling(name);
    if (defaultInputC && defaultInputC->registerInput(name) && control)
        control->deferHandling(name);
}

void CalcResistanceTemperature::unhandled(const SequenceName &name,
                                          SegmentProcessingStage::SequenceHandlerControl *control)
{
    registerPossibleInput(name, control);

    for (std::size_t id = 0, max = processing.size(); id < max; id++) {
        if (processing[id].operateOutput->registerInput(name)) {
            if (control)
                control->filterUnit(name, id);
            handleNewProcessing(name, id, control);
            return;
        }
    }

    if (restrictedInputs)
        return;

    if (name.hasFlavor(SequenceName::flavor_cover) || name.hasFlavor(SequenceName::flavor_stats))
        return;

    if (Util::starts_with(name.getVariable(), "T")) {
        if (control)
            control->deferHandling(name);
        return;
    }

    QRegularExpression reCheck("(?:W)([uc]?\\d*)_(.+)");
    auto r = reCheck.match(name.getVariableQString(), 0, QRegularExpression::NormalMatch,
                           QRegularExpression::AnchoredMatchOption);
    if (!r.hasMatch())
        return;
    auto suffix = r.captured(2).toStdString();
    if (suffix.empty())
        return;
    if (!filterSuffixes.empty() && !filterSuffixes.count(suffix))
        return;

    auto prefix = r.captured(1).toStdString();

    SequenceName temperatureName
            (name.getStation(), name.getArchive(), "T" + prefix + "_" + suffix, name.getFlavors());

    Processing proc;

    proc.operateOutput.reset(new DynamicSequenceSelection::Single(temperatureName));
    proc.operateOutput->registerInput(name);
    proc.operateOutput->registerInput(temperatureName);

    proc.inputResistance.reset(new DynamicInput::Basic(name));
    proc.inputResistance->registerInput(name);
    proc.inputResistance->registerInput(temperatureName);

    if (defaultInputA) {
        proc.inputA.reset(defaultInputA->clone());
    } else {
        proc.inputA.reset(new DynamicInput::Constant);
    }
    proc.inputA->registerInput(name);
    proc.inputA->registerInput(temperatureName);

    if (defaultInputB) {
        proc.inputB.reset(defaultInputB->clone());
    } else {
        proc.inputB.reset(new DynamicInput::Constant);
    }
    proc.inputB->registerInput(name);
    proc.inputB->registerInput(temperatureName);

    if (defaultInputC) {
        proc.inputC.reset(defaultInputC->clone());
    } else {
        proc.inputC.reset(new DynamicInput::Constant);
    }
    proc.inputC->registerInput(name);
    proc.inputC->registerInput(temperatureName);

    proc.lookupTable.reset(new DynamicPrimitive<LookupTable>::Constant);

    if (defaultResistanceCalibration) {
        proc.resistanceCalibration.reset(defaultResistanceCalibration->clone());
    } else {
        proc.resistanceCalibration.reset(new DynamicCalibration::Constant);
    }

    if (defaultCalculation) {
        proc.calculation.reset(defaultCalculation->clone());
    } else {
        proc.calculation.reset(new DynamicPrimitive<Calculation>::Constant(Calculation::None));
    }

    auto id = processing.size();

    if (control) {
        for (const auto &n : proc.operateOutput->getAllUnits()) {
            if (control->isHandled(n))
                continue;
            control->filterUnit(n, id);
        }

        for (const auto &n : proc.inputResistance->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : proc.inputA->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : proc.inputB->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : proc.inputC->getUsedInputs()) {
            control->inputUnit(n, id);
        }
    }

    processing.emplace_back(std::move(proc));
}


static double calculate(Calculation calc,
                        double w,
                        double a,
                        double b,
                        double c,
                        const Calibration &resistanceCalibration)
{
    switch (calc) {
    case Calculation::Table:
    case Calculation::None:
        return w;
    case Calculation::Steinhart_Hart: {
        if (!FP::defined(w) || !FP::defined(a) || !FP::defined(b) || !FP::defined(c))
            return FP::undefined();
        if (w <= 0.0)
            return FP::undefined();
        w = std::log(w);
        w = a + b * w + c * w * w * w;
        if (w == 0.0)
            return FP::undefined();
        return 1.0 / w - 273.15;
    }
    case Calculation::NTC: {
        if (!FP::defined(w) || !FP::defined(a) || !FP::defined(b) || !FP::defined(c))
            return FP::undefined();
        if (w <= 0.0)
            return FP::undefined();
        double R0 = resistanceCalibration.apply(a);
        double beta = b;
        double T0 = c;
        if (beta <= 0.0)
            return FP::undefined();
        if (R0 == 0.0)
            return FP::undefined();
        if (T0 < 150.0)
            T0 += 273.15;
        if (T0 <= 0.0)
            return FP::undefined();
        double Rinf = R0 * std::exp(-beta / T0);
        if (Rinf <= 0.0)
            return FP::undefined();
        w = std::log(w / Rinf);
        if (w == 0.0)
            return FP::undefined();
        return beta / w - 273.15;
    }
    case Calculation::RTD: {
        if (!FP::defined(w) || !FP::defined(a) || !FP::defined(b) || !FP::defined(c))
            return FP::undefined();
        double R0 = resistanceCalibration.apply(a);
        double alpha = b;
        double T0 = c;
        if (alpha == 0.0)
            return FP::undefined();
        if (R0 == 0.0)
            return FP::undefined();
        return (w / R0 - 1.0) / alpha + T0;
    }
    }

    Q_ASSERT(false);
    return FP::undefined();
}

static double applyTable(double w, const LookupTable &table)
{
    if (!FP::defined(w))
        return w;

    switch (table.size()) {
    case 0:
        return FP::undefined();
    case 1:
        return table.begin()->second;
    default:
        break;
    }

    auto upper = table.lower_bound(w);
    auto lower = upper;
    if (upper == table.begin()) {
        ++upper;
        Q_ASSERT(upper != table.end());
    } else {
        if (upper == table.end()) {
            --upper;
        }
        lower = upper;
        Q_ASSERT(lower != table.begin());
        --lower;
    }

    return (w - lower->first) * (upper->second - lower->second) / (upper->first - lower->first) +
            lower->second;
}

void CalcResistanceTemperature::process(int id, SequenceSegment &data)
{
    auto &proc = processing[id];

    double w = proc.inputResistance->get(data);
    const auto &cal = proc.resistanceCalibration->get(data);
    w = cal.apply(w);

    auto type = proc.calculation->get(data);
    double value;
    if (type == Calculation::Table) {
        value = applyTable(w, proc.lookupTable->get(data));
    } else {
        double a = proc.inputA->get(data);
        double b = proc.inputB->get(data);
        double c = proc.inputC->get(data);

        value = calculate(type, w, a, b, c, cal);
    }

    for (const auto &i : proc.operateOutput->get(data)) {
        data[i].setReal(value);
    }
}

static std::string describeOperation(Calculation calc)
{
    switch (calc) {
    case Calculation::Steinhart_Hart:
        return "Steinhart-Hart";
    case Calculation::NTC:
        return "NTC";
    case Calculation::RTD:
        return "RTD";
    case Calculation::Table:
        return "Table";
    case Calculation::None:
        return "None";
    }
    Q_ASSERT(false);
    return {};
}

void CalcResistanceTemperature::processMeta(int id, SequenceSegment &data)
{
    auto &proc = processing[id];

    Variant::Root meta;

    meta["By"].setString("calc_resistancetemperature");
    meta["At"].setDouble(Time::time());
    meta["Environment"].setString(Environment::describe());
    meta["Revision"].setString(Environment::revision());

    auto operation = describeOperation(proc.calculation->get(data));
    meta["Parameters"].hash("Operation") = operation;
    meta["Parameters"].hash("Resistance") = proc.inputResistance->describe(data);
    meta["Parameters"].hash("A") = proc.inputA->describe(data);
    meta["Parameters"].hash("B") = proc.inputB->describe(data);
    meta["Parameters"].hash("C") = proc.inputC->describe(data);

    Variant::Read base = Variant::Read::empty();
    for (auto i : proc.inputResistance->getUsedInputs()) {
        i.setMeta();
        if (!data.exists(i))
            continue;
        base = data[i];
        if (!base.exists())
            continue;
        break;
    }

    for (auto i : proc.operateOutput->get(data)) {
        i.setMeta();
        if (!data[i].exists())
            data[i].set(base);

        data[i].metadataReal("Format").setString("00.0");
        data[i].metadataReal("Units").setString("\xC2\xB0\x43");
        if (!data[i].metadataReal("Description").exists())
            data[i].metadataReal("Description").setString("Calculated temperature");

        data[i].metadata("Processing").toArray().after_back().set(meta);

        data[i].metadataReal("MVC").remove();
        data[i].metadataReal("GroupUnits").remove();
        data[i].metadataReal("Smoothing").remove();
    }
}


SequenceName::Set CalcResistanceTemperature::requestedInputs()
{
    SequenceName::Set out;
    for (const auto &p : processing) {
        Util::merge(p.inputResistance->getUsedInputs(), out);
        Util::merge(p.inputA->getUsedInputs(), out);
        Util::merge(p.inputB->getUsedInputs(), out);
        Util::merge(p.inputC->getUsedInputs(), out);
    }
    return out;
}

SequenceName::Set CalcResistanceTemperature::predictedOutputs()
{
    SequenceName::Set out;
    for (const auto &p : processing) {
        Util::merge(p.operateOutput->getAllUnits(), out);
    }
    return out;
}

QSet<double> CalcResistanceTemperature::metadataBreaks(int id)
{
    const auto &p = processing[id];
    QSet<double> result;
    Util::merge(p.operateOutput->getChangedPoints(), result);
    Util::merge(p.calculation->getChangedPoints(), result);
    Util::merge(p.inputResistance->getChangedPoints(), result);
    Util::merge(p.inputA->getChangedPoints(), result);
    Util::merge(p.inputB->getChangedPoints(), result);
    Util::merge(p.inputC->getChangedPoints(), result);
    Util::merge(p.lookupTable->getChangedPoints(), result);
    return result;
}

QDataStream &operator>>(QDataStream &stream, Calculation &op)
{
    quint32 n = 0;
    stream >> n;
    op = static_cast<Calculation>(n);
    return stream;
}

QDataStream &operator<<(QDataStream &stream, Calculation op)
{
    stream << static_cast<quint32>(op);
    return stream;
}

QDebug operator<<(QDebug stream, Calculation op)
{
    stream << describeOperation(op).c_str();
    return stream;
}

CalcResistanceTemperature::CalcResistanceTemperature(QDataStream &stream)
{
    stream >> defaultCalculation;
    stream >> defaultResistanceCalibration;
    stream >> defaultInputA;
    stream >> defaultInputB;
    stream >> defaultInputC;
    stream >> filterSuffixes;
    stream >> restrictedInputs;

    Deserialize::container(stream, processing, [&stream]() {
        Processing proc;

        stream >> proc.operateOutput;
        stream >> proc.calculation;
        stream >> proc.resistanceCalibration;
        stream >> proc.inputResistance;
        stream >> proc.inputA;
        stream >> proc.inputB;
        stream >> proc.inputC;
        stream >> proc.lookupTable;

        return proc;
    });
}

void CalcResistanceTemperature::serialize(QDataStream &stream)
{
    stream << defaultCalculation;
    stream << defaultResistanceCalibration;
    stream << defaultInputA;
    stream << defaultInputB;
    stream << defaultInputC;
    stream << filterSuffixes;
    stream << restrictedInputs;

    Serialize::container(stream, processing, [&stream](const Processing &proc) {
        stream << proc.operateOutput;
        stream << proc.calculation;
        stream << proc.resistanceCalibration;
        stream << proc.inputResistance;
        stream << proc.inputA;
        stream << proc.inputB;
        stream << proc.inputC;
        stream << proc.lookupTable;
    });
}


QString CalcResistanceTemperatureComponent::getBasicSerializationName() const
{ return QString::fromLatin1("calc_resistancetemperature"); }

ComponentOptions CalcResistanceTemperatureComponent::getOptions()
{
    ComponentOptions options;

    options.add("resistance",
                new DynamicInputOption(tr("resistance", "name"), tr("Input resistance"),
                                       tr("This is the input resistance.  This option is "
                                          "mutually exclusive with with instrument specification."),
                                       tr("All resistances")));

    options.add("a", new DynamicInputOption(tr("a", "name"), tr("Input A"),
                                            tr("The first input.  The usage depends on the calculation selected."),
                                            tr("Calculation dependent")));

    options.add("b", new DynamicInputOption(tr("b", "name"), tr("Input B"),
                                            tr("The second input.  The usage depends on the calculation selected."),
                                            tr("Calculation dependent")));

    options.add("c", new DynamicInputOption(tr("c", "name"), tr("Input C"),
                                            tr("The third input.  The usage depends on the calculation selected."),
                                            tr("Calculation dependent")));

    options.add("output",
                new DynamicSequenceSelectionOption(tr("output", "name"), tr("Output value"),
                                                   tr("This is output temperature.  This option is "
                                                      "mutually exclusive with with instrument specification."),
                                                   {}));

    options.add("calibration",
                new DynamicCalibrationOption(tr("calibration", "name"), tr("Calibration applied"),
                                             tr("The calibration applied to the input and any same unit types used in the calculation."),
                                             tr("Conversion to ohms when required")));

    options.add("suffix", new ComponentOptionInstrumentSuffixSet(tr("instruments", "name"),
                                                                 tr("Instrument suffixes to correct"),
                                                                 tr("These are the instrument suffixes to calculate temperatures for.  "
                                                                    "This option is mutually exclusive with manual "
                                                                    "variable specification."),
                                                                 tr("All instrument suffixes")));
    options.exclude("resistance", "suffix");
    options.exclude("output", "suffix");
    options.exclude("suffix", "resistance");
    options.exclude("suffix", "output");


    ComponentOptionEnum *calc =
            new ComponentOptionEnum(tr("type", "name"), tr("Calculation performed"),
                                    tr("This is the calculation performed on the inputs to generate the temperature."),
                                    tr("Steinhart-Hart"), -1);
    calc->add(static_cast<int>(Calculation::Steinhart_Hart), "steinhart_hart",
              tr("Steinhart-Hart", "calculation name"),
              tr("Steinhart-Hart with A, B, and C coefficients", "calculation description"));
    calc->add(static_cast<int>(Calculation::NTC), "ntc", tr("ntc", "calculation name"),
              tr("Simple NTC thermistor with R₀=A, β=B, T₀=C (default 25)",
                     "calculation description"));
    calc->add(static_cast<int>(Calculation::RTD), "ntc", tr("rtd", "calculation name"),
              tr("Linear RTD with R₀=A, α=B, T₀=C (default 0)", "calculation description"));
    calc->alias(static_cast<int>(Calculation::RTD), tr("PT100"));
    calc->alias(static_cast<int>(Calculation::RTD), tr("PT1000"));
    calc->add(static_cast<int>(Calculation::None), "none",
              tr("Disable calculation", "calculation name"),
              tr("No calculation applied", "calculation description"));

    options.add("type", calc);

    return options;
}

QList<ComponentExample> CalcResistanceTemperatureComponent::getExamples()
{
    QList<ComponentExample> examples;

    return examples;
}

SegmentProcessingStage *CalcResistanceTemperatureComponent::createBasicFilterDynamic(const ComponentOptions &options)
{ return new CalcResistanceTemperature(options); }

SegmentProcessingStage *CalcResistanceTemperatureComponent::createBasicFilterPredefined(const ComponentOptions &options,
                                                                                        double start,
                                                                                        double end,
                                                                                        const QList<
                                                                                                SequenceName> &inputs)
{ return new CalcResistanceTemperature(options, start, end, inputs); }

SegmentProcessingStage *CalcResistanceTemperatureComponent::createBasicFilterEditing(double start,
                                                                                     double end,
                                                                                     const SequenceName::Component &station,
                                                                                     const SequenceName::Component &archive,
                                                                                     const ValueSegment::Transfer &config)
{ return new CalcResistanceTemperature(start, end, station, archive, config); }

SegmentProcessingStage *CalcResistanceTemperatureComponent::deserializeBasicFilter(QDataStream &stream)
{ return new CalcResistanceTemperature(stream); }
