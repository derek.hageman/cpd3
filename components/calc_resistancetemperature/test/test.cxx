/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <cmath>
#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/segmentprocessingstage.hxx"
#include "core/component.hxx"

#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace QTest {

template<>
char *toString(const QSet<double> &set)
{
    QByteArray ba = "QSet<double>(";
    for (QSet<double>::const_iterator i = set.constBegin(); i != set.constEnd(); ++i) {
        if (i != set.constBegin()) ba += ", ";
        ba += QByteArray::number(*i);
    }
    ba += ")";
    return qstrdup(ba.data());
}
}

class TestController : public SegmentProcessingStage::SequenceHandlerControl {
public:
    struct ID {
        SequenceName::Set filter;
        SequenceName::Set input;

        ID() = default;

        ID(SequenceName::Set filter, SequenceName::Set input) : filter(std::move(filter)),
                                                                input(std::move(input))
        { }

        bool operator==(const ID &other) const
        { return filter == other.filter && input == other.input; }
    };

    std::unordered_map<int, ID> contents;

    int find(const SequenceName::Set &filter, const SequenceName::Set &input = {}) const
    {
        for (const auto &check : contents) {
            if (check.second.filter != filter)
                continue;
            if (check.second.input != input)
                continue;
            return check.first;
        }
        return -1;
    }

    void filterUnit(const SequenceName &name, int targetID) override
    {
        contents[targetID].input.erase(name);
        contents[targetID].filter.insert(name);
    }

    void inputUnit(const SequenceName &name, int targetID) override
    {
        if (contents[targetID].filter.count(name))
            return;
        contents[targetID].input.insert(name);
    }

    bool isHandled(const SequenceName &name) override
    {
        for (const auto &check : contents) {
            if (check.second.filter.count(name))
                return true;
            if (check.second.input.count(name))
                return true;
        }
        return false;
    }
};

class TestComponent : public QObject {
Q_OBJECT

    SegmentProcessingStageComponent *component;

private slots:

    void initTestCase()
    {
        component = qobject_cast<SegmentProcessingStageComponent *>(
                ComponentLoader::create("calc_resistancetemperature"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("resistance")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("a")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("b")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("c")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("output")));
        QVERIFY(qobject_cast<DynamicCalibrationOption *>(options.get("calibration")));
        QVERIFY(qobject_cast<ComponentOptionEnum *>(options.get("type")));
    }

    void dynamicDefaults()
    {
        ComponentOptions options(component->getOptions());
        std::unique_ptr<SegmentProcessingStage>
                filter(component->createBasicFilterDynamic(options));
        QVERIFY(filter.get());
        TestController controller;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "Q_X1"), &controller);
        QVERIFY(controller.contents.empty());
        {
            std::unique_ptr<SegmentProcessingStage> filter2;
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                filter2.reset(component->deserializeBasicFilter(stream));
                QVERIFY(stream.atEnd());
            }
            QVERIFY(filter2.get());
            filter2->unhandled(SequenceName("brw", "raw", "Q_X1"), &controller);
            QVERIFY(controller.contents.empty());
        }

        SequenceName W_X1_1("brw", "raw", "W_X1");
        SequenceName T_X1_1("brw", "raw", "T_X1");
        SequenceName W_X1_2("sgp", "raw", "W_X1");
        SequenceName T_X1_2("sgp", "raw", "T_X1");
        SequenceName W_XM1_1("brw", "raw", "W1_XM1");
        SequenceName T_XM1_1("brw", "raw", "T1_XM1");

        filter->unhandled(W_X1_1, &controller);
        filter->unhandled(T_X1_1, &controller);
        QCOMPARE((int) controller.contents.size(), 1);
        int X1_1 = controller.find({T_X1_1}, {W_X1_1});
        QVERIFY(X1_1 != -1);

        filter->unhandled(SequenceName("brw", "raw", "X_P01"), &controller);
        QCOMPARE((int) controller.contents.size(), 1);

        filter->unhandled(W_X1_2, &controller);
        QCOMPARE((int) controller.contents.size(), 2);
        int X1_2 = controller.find({T_X1_2}, {W_X1_2});
        QVERIFY(X1_2 != -1);

        filter->unhandled(T_XM1_1, &controller);
        filter->unhandled(W_XM1_1, &controller);
        QCOMPARE((int) controller.contents.size(), 3);
        int XM_1 = controller.find({T_XM1_1}, {W_XM1_1});
        QVERIFY(XM_1 != -1);

        data.setStart(15.0);
        data.setEnd(16.0);

        data.setValue(W_X1_1, Variant::Root(20.0));
        filter->process(X1_1, data);
        QCOMPARE(data.value(T_X1_1).toReal(), 8.561756989082255);

        {
            std::unique_ptr<SegmentProcessingStage> filter2;
            QByteArray sd;
            {
                QDataStream stream(&sd, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&sd, QIODevice::ReadOnly);
                filter2.reset(component->deserializeBasicFilter(stream));
            }
            QVERIFY(filter2.get());
            data.setValue(T_X1_1, Variant::Root());
            data.setValue(W_X1_1, Variant::Root(20.0));
            filter->process(X1_1, data);
            QCOMPARE(data.value(T_X1_1).toReal(), 8.561756989082255);
        }

        data.setValue(W_X1_2, Variant::Root(15.0));
        filter->process(X1_2, data);
        QCOMPARE(data.value(T_X1_2).toReal(), 15.19630884352756);

        data.setValue(W_XM1_1, Variant::Root(35.0));
        filter->process(XM_1, data);
        QCOMPARE(data.value(T_XM1_1).toReal(), -3.6690035220123036);

        data.setValue(W_X1_1, Variant::Root(FP::undefined()));
        filter->process(X1_1, data);
        QVERIFY(!FP::defined(data.value(T_X1_1).toReal()));

        filter->processMeta(X1_1, data);
        QCOMPARE(data.value(T_X1_1.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
    }

    void dynamicOptions()
    {
        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionEnum *>(options.get("type"))->set("ntc");
        qobject_cast<DynamicInputOption *>(options.get("a"))->set(100.0);
        qobject_cast<DynamicInputOption *>(options.get("b"))->set(3950.0);
        qobject_cast<DynamicInputOption *>(options.get("resistance"))->set(
                SequenceMatch::OrderedLookup(
                        SequenceMatch::Element(QString("brw"), QString("raw"), "W_R11",
                                               SequenceName::Flavors())), Calibration());
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("output"))->set("brw", "raw",
                                                                                   "Tx_R11",
                                                                                   SequenceName::Flavors());

        std::unique_ptr<SegmentProcessingStage>
                filter(component->createBasicFilterDynamic(options));
        QVERIFY(filter.get());
        TestController controller;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "Wx_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "W1_R11"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "Tx_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "T1_R11"), &controller);
        QVERIFY(controller.contents.empty());

        SequenceName W_R11("brw", "raw", "W_R11");
        SequenceName Tx_R11("brw", "raw", "Tx_R11");
        filter->unhandled(W_R11, &controller);
        filter->unhandled(Tx_R11, &controller);

        QCOMPARE((int) controller.contents.size(), 1);
        int id = controller.find({Tx_R11}, {W_R11});
        QVERIFY(id != -1);

        data.setValue(W_R11, Variant::Root(90.0));
        filter->process(id, data);
        QCOMPARE(data.value(Tx_R11).toReal(), 27.390110874512175);

        filter.reset();


        options = component->getOptions();
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->add("R31");

        filter.reset(component->createBasicFilterDynamic(options));
        QVERIFY(filter != NULL);
        controller.contents.clear();

        SequenceName W_R31("brw", "raw", "W_R31");
        SequenceName T_R31("brw", "raw", "T_R31");

        filter->unhandled(SequenceName("brw", "raw", "Wx_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "W1_R11"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "Tx_V12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "T1_R11"), &controller);
        QVERIFY(controller.contents.empty());

        filter->unhandled(W_R31, &controller);

        QCOMPARE((int) controller.contents.size(), 1);
        id = controller.find({T_R31}, {W_R31});
        QVERIFY(id != -1);

        data.setValue(W_R31, Variant::Root(60.0));
        data.setValue(T_R31, Variant::Root());
        filter->process(id, data);
        QCOMPARE(data.value(T_R31).toReal(), -14.680097304369497);

        filter.reset();
    }

    void predefined()
    {
        ComponentOptions options(component->getOptions());

        SequenceName W_A11("brw", "raw", "W_A11");
        SequenceName T_A11("brw", "raw", "T_A11");
        QList<SequenceName> input;
        input << W_A11;

        std::unique_ptr<SegmentProcessingStage> filter
                (component->createBasicFilterPredefined(options, FP::undefined(), FP::undefined(),
                                                        input));
        QVERIFY(filter.get());
        TestController controller;
        QList<int> idL;

        QCOMPARE(filter->requestedInputs(), (SequenceName::Set{W_A11}));
        QCOMPARE(filter->predictedOutputs(), (SequenceName::Set{T_A11}));
    }

    void editing()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["XM1/CalculateTemperature"] = "::T_XM1:=";
        cv["XM1/Resistance"] = "::W_XM1:=";
        cv["XM1/A"] = 0.0010295;
        cv["XM1/B"] = 0.0002391;
        cv["XM1/C"] = 0.0000001568;
        cv["XM1/ResistanceCalibration/#0"] = 0.0;
        cv["XM1/ResistanceCalibration/#1"] = 1000.0;
        config.emplace_back(FP::undefined(), 13.0, cv);
        cv.write().setEmpty();
        cv["XM1/CalculateTemperature"] = "::T_XM1:=";
        cv["XM1/Resistance"] = "::W2_XM1:=";
        cv["XM1/A"] = 100E3;
        cv["XM1/B"] = 3950.0;
        cv["XM1/C"] = 25.0;
        cv["XM1/Type"] = "NTC";
        config.emplace_back(13.0, FP::undefined(), cv);

        std::unique_ptr<SegmentProcessingStage>
                filter(component->createBasicFilterEditing(12.0, 15.0, "brw", "raw", config));
        QVERIFY(filter.get());
        TestController controller;

        SequenceName T_XM1("brw", "raw", "T_XM1");
        SequenceName W_XM1("brw", "raw", "W_XM1");
        SequenceName W2_XM1("brw", "raw", "W2_XM1");

        QCOMPARE(filter->requestedInputs(), (SequenceName::Set{W_XM1, W2_XM1}));
        QCOMPARE(filter->predictedOutputs(), (SequenceName::Set{T_XM1}));

        filter->unhandled(T_XM1, &controller);
        filter->unhandled(W_XM1, &controller);
        filter->unhandled(W2_XM1, &controller);

        int id = controller.find({T_XM1}, {W_XM1, W2_XM1});
        QVERIFY(id != -1);

        QCOMPARE(filter->metadataBreaks(id), QSet<double>() << 13.0);

        SequenceSegment data1;
        data1.setStart(12.0);
        data1.setEnd(13.0);

        data1.setValue(W_XM1, Variant::Root(10.0));
        filter->process(id, data1);
        QCOMPARE(data1.value(T_XM1).toReal(), 24.98343226099348);

        data1.setValue(W_XM1, Variant::Root());
        filter->process(id, data1);
        QVERIFY(!FP::defined(data1.value(T_XM1).toReal()));

        filter->processMeta(id, data1);
        QCOMPARE(data1.value(T_XM1.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);


        SequenceSegment data2;
        data2.setStart(13.0);
        data2.setEnd(15.0);

        data2.setValue(W2_XM1, Variant::Root(80E3));
        filter->process(id, data2);
        QCOMPARE(data2.value(T_XM1).toReal(), 30.107801852770194);

        filter->processMeta(id, data2);
        QCOMPARE(data2.value(T_XM1.toMeta()).metadata("Processing").getType(),
                 Variant::Type::Array);
    }

    void table()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["XM1/CalculateTemperature"] = "::T_XM1:=";
        cv["XM1/Resistance"] = "::W_XM1:=";
        cv["XM1/Table/@0"] = 1.0;
        cv["XM1/Table/@10"] = 2.0;
        cv["XM1/Table/@20"] = 4.0;
        cv["XM1/Type"] = "Table";
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        std::unique_ptr<SegmentProcessingStage>
                filter(component->createBasicFilterEditing(12.0, 15.0, "brw", "raw", config));
        QVERIFY(filter.get());
        TestController controller;

        SequenceName T_XM1("brw", "raw", "T_XM1");
        SequenceName W_XM1("brw", "raw", "W_XM1");

        QCOMPARE(filter->requestedInputs(), (SequenceName::Set{W_XM1}));
        QCOMPARE(filter->predictedOutputs(), (SequenceName::Set{T_XM1}));

        filter->unhandled(T_XM1, &controller);
        filter->unhandled(W_XM1, &controller);

        int id = controller.find({T_XM1}, {W_XM1});
        QVERIFY(id != -1);

        SequenceSegment data1;
        data1.setStart(12.0);
        data1.setEnd(13.0);

        data1.setValue(W_XM1, Variant::Root(10.0));
        filter->process(id, data1);
        QCOMPARE(data1.value(T_XM1).toReal(), 2.0);

        data1.setValue(W_XM1, Variant::Root());
        filter->process(id, data1);
        QVERIFY(!FP::defined(data1.value(T_XM1).toReal()));

        data1.setValue(W_XM1, Variant::Root(5.0));
        filter->process(id, data1);
        QCOMPARE(data1.value(T_XM1).toReal(), 1.5);

        data1.setValue(W_XM1, Variant::Root(2.5));
        filter->process(id, data1);
        QCOMPARE(data1.value(T_XM1).toReal(), 1.25);

        data1.setValue(W_XM1, Variant::Root(0.0));
        filter->process(id, data1);
        QCOMPARE(data1.value(T_XM1).toReal(), 1.0);

        data1.setValue(W_XM1, Variant::Root(-5.0));
        filter->process(id, data1);
        QCOMPARE(data1.value(T_XM1).toReal(), 0.5);

        data1.setValue(W_XM1, Variant::Root(15.0));
        filter->process(id, data1);
        QCOMPARE(data1.value(T_XM1).toReal(), 3.0);

        data1.setValue(W_XM1, Variant::Root(20.0));
        filter->process(id, data1);
        QCOMPARE(data1.value(T_XM1).toReal(), 4.0);

        data1.setValue(W_XM1, Variant::Root(30.0));
        filter->process(id, data1);
        QCOMPARE(data1.value(T_XM1).toReal(), 6.0);
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
