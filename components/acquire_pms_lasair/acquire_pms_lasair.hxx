/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIREPMSLASAIR_H
#define ACQUIREPMSLASAIR_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QDateTime>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"

class AcquirePMSLASAIR : public CPD3::Acquisition::FramedInstrument {
    double lastRecordTime;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum ResponseState {
        /* Passive autoprobe initialization, ignoring the first timeout */
        RESP_AUTOPROBE_PASSIVE_INITIALIZE,
        /* Waiting for a valid record to confirm passive mode autoprobe */
        RESP_AUTOPROBE_PASSIVE_WAIT,

        /* Waiting for a valid record to start passive acquisition */
        RESP_PASSIVE_WAIT,

        /* Acquiring data in passive mode */
        RESP_PASSIVE_RUN,
        /* Acquiring data in interactive mode, but only reading unpolled data */
        RESP_UNPOLLED_RUN,

        /* Waiting for the completion of the "ES" command to stop sampling */
                RESP_INTERACTIVE_START_STOPREPORTS,
        /* Waiting for the completion of the "TD ..." command to set the
         * instrument time */
                RESP_INTERACTIVE_START_SETTIME,
        /* Waiting for the completion of the "TI nnn" command to set the
         * sampling interval */
                RESP_INTERACTIVE_START_SETINTERVAL,
        /* Waiting for the completion of the "SN" command to set continuous
         * sampling mode */
                RESP_INTERACTIVE_START_SETCONTINUOUS,

        /* Waiting for the first valid record */
                RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID,

        /* Waiting before attempting a communications restart */
                RESP_INTERACTIVE_RESTART_WAIT,
        /* Initial state for interactive start */
                RESP_INTERACTIVE_INITIALIZE,
    };
    ResponseState responseState;
    int autoprobeValidRecords;

    friend class Configuration;

    class Configuration {
        double start;
        double end;
    public:
        bool strictMode;
        double reportInterval;

        double sizes[8];

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under, const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    struct {
        double time;

        CPD3::Data::Variant::Root sampleInterval;
        CPD3::Data::Variant::Root dQt;
        CPD3::Data::Variant::Root firmwareVersion;
        CPD3::Data::Variant::Root manifoldPosition;

        CPD3::Data::Variant::Root N;

        CPD3::Data::Variant::Root V;
        CPD3::Data::Variant::Root Q;
    } pending;
    int nextLine;

    double effectiveSizes[8];

    void setDefaultInvalid();

    CPD3::Data::Variant::Root instrumentMeta;

    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;

    void logValue(double startTime,
                  double endTime,
                  CPD3::Data::SequenceName::Component name,
                  CPD3::Data::Variant::Root &&value);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root &&value);

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    void setEffectiveSizes(int model);

    int processInstrumentID(const CPD3::Util::ByteView &line);

    int processDateTime(const CPD3::Util::ByteView &date, const CPD3::Util::ByteView &time);

    CPD3::Data::Variant::Root processFirmwareVersion(const CPD3::Util::ByteView &field);

    int processLine1(std::deque<CPD3::Util::ByteView> &fields);

    int processLine2(std::deque<CPD3::Util::ByteView> &fields);

    int processLine3(std::deque<CPD3::Util::ByteView> &fields);

    int processLine4(std::deque<CPD3::Util::ByteView> &fields);

    int finishReport(double &frameTime);

    int processNextLine(const CPD3::Util::ByteView &line, bool possibleChecksum = true);

    int processRecord(const CPD3::Util::ByteView &frame, double &frameTime);

    void configAdvance(double frameTime);

    void configurationChanged();

public:
    AcquirePMSLASAIR(const CPD3::Data::ValueSegment::Transfer &config,
                     const std::string &loggingContext);

    AcquirePMSLASAIR(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquirePMSLASAIR();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus() override;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus() override;

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    std::size_t dataFrameStart(std::size_t offset,
                               const CPD3::Util::ByteArray &input) const override;

    std::size_t dataFrameEnd(std::size_t start, const CPD3::Util::ByteArray &input) const override;

    void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingInstrumentTimeout(double time) override;

    void discardDataCompleted(double time) override;
};

class AcquirePMSLASAIRComponent
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_pms_lasair"
                              FILE
                              "acquire_pms_lasair.json")

    CPD3::ComponentOptions getBaseOptions();

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
