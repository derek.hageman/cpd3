/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/csv.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/wavelength.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_pms_lasair.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

#define TOTAL_BINS          8


static const double *getSizes(int model, bool allowDefault = true)
{
    static const double size110[TOTAL_BINS] =
            {(0.10 + 0.15) / 2.0, (0.15 + 0.20) / 2.0, (0.20 + 0.25) / 2.0, (0.25 + 0.30) / 2.0,
             (0.30 + 0.50) / 2.0, (0.50 + 0.70) / 2.0, (0.70 + 1.00) / 2.0,
             (1.00 + (1.00 - 0.70) / 2.0) / 2.0};
    static const double size1510[TOTAL_BINS] =
            {(0.15 + 0.20) / 2.0, (0.20 + 0.30) / 2.0, (0.30 + 0.50) / 2.0, (0.50 + 0.70) / 2.0,
             (0.70 + 1.00) / 2.0, (1.00 + 2.00) / 2.0, (2.00 + 5.00) / 2.0,
             (5.00 + (5.00 - 2.00) / 2.0) / 2.0};
    static const double size210[TOTAL_BINS] =
            {(0.20 + 0.30) / 2.0, (0.30 + 0.50) / 2.0, (0.50 + 0.70) / 2.0, (0.70 + 1.00) / 2.0,
             (1.00 + 2.00) / 2.0, (2.00 + 3.00) / 2.0, (3.00 + 5.00) / 2.0,
             (5.00 + (5.00 - 3.00) / 2.0) / 2.0};
    static const double size1X[TOTAL_BINS] =
            {(0.10 + 0.20) / 2.0, (0.20 + 0.30) / 2.0, (0.30 + 0.40) / 2.0, (0.40 + 0.50) / 2.0,
             (0.50 + 0.70) / 2.0, (0.70 + 1.00) / 2.0, (1.00 + 2.00) / 2.0,
             (2.00 + (2.00 - 1.00) / 2.0) / 2.0};

    switch (model) {
    case 110:
        return size110;
    case 1510:
        return size1510;
    case 210:
        return size210;
        break;

    case 101:
    case 1001:
    case 1002:
    case 1003:
    case 1004:
        return size1X;

    default:
        if (allowDefault)
            return size1X;
        break;
    }

    return NULL;
}

AcquirePMSLASAIR::Configuration::Configuration() : start(FP::undefined()),
                                                   end(FP::undefined()),
                                                   strictMode(true),
                                                   reportInterval(6.0),
                                                   sizes()
{
    for (int i = 0; i < TOTAL_BINS; i++) {
        sizes[i] = FP::undefined();
    }
}

AcquirePMSLASAIR::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          strictMode(other.strictMode),
          reportInterval(other.reportInterval),
          sizes()
{
    memcpy(sizes, other.sizes, sizeof(sizes));
}

AcquirePMSLASAIR::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s), end(e), strictMode(true), reportInterval(6.0), sizes()
{
    for (int i = 0; i < TOTAL_BINS; i++) {
        sizes[i] = FP::undefined();
    }
    setFromSegment(other);
}

AcquirePMSLASAIR::Configuration::Configuration(const Configuration &under,
                                               const ValueSegment &over,
                                               double s,
                                               double e) : start(s),
                                                           end(e),
                                                           strictMode(under.strictMode),
                                                           reportInterval(under.reportInterval),
                                                           sizes()
{
    memcpy(sizes, under.sizes, sizeof(sizes));
    setFromSegment(over);
}

void AcquirePMSLASAIR::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["StrictMode"].exists())
        strictMode = config["StrictMode"].toBool();

    {
        double v = config["ReportInterval"].toDouble();
        if (FP::defined(v) && v > 0.0)
            reportInterval = v;
    }

    if (INTEGER::defined(config["Model"].toInt64())) {
        const double *set = getSizes((int) config["Model"].toInt64(), false);
        if (set != NULL) {
            memcpy(sizes, set, sizeof(sizes));
        }
    }

    switch (config["Diameter"].getType()) {
    case Variant::Type::Array: {
        auto children = config["Diameter"].toArray();
        int idx = 0;
        for (auto add = children.begin(), endAdd = children.end();
                add != endAdd && idx < TOTAL_BINS;
                ++add, ++idx) {
            sizes[idx] = (*add).toDouble();
        }
        break;
    }

    case Variant::Type::String: {
        QStringList children(config["Diameter"].toQString().split(QRegExp("[:;,]")));
        int idx = 0;
        for (QStringList::const_iterator add = children.constBegin(), endAdd = children.constEnd();
                add != endAdd && idx < TOTAL_BINS;
                ++add, ++idx) {
            bool ok = false;
            double v = add->toDouble(&ok);
            if (!ok)
                v = FP::undefined();
            sizes[idx] = v;
        }
        break;
    }

    case Variant::Type::Empty:
        break;

    default:
        for (int i = 0; i < TOTAL_BINS; i++) {
            sizes[i] = FP::undefined();
        }
        break;
    }
}


void AcquirePMSLASAIR::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("PMS");
    instrumentMeta["Model"].setString("LASAIR");

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    nextLine = 0;

    for (int i = 0; i < TOTAL_BINS; i++) {
        effectiveSizes[i] = FP::undefined();
    }
}

AcquirePMSLASAIR::AcquirePMSLASAIR(const ValueSegment::Transfer &configData,
                                   const std::string &loggingContext) : FramedInstrument("lasair",
                                                                                         loggingContext),
                                                                        lastRecordTime(
                                                                                FP::undefined()),
                                                                        autoprobeStatus(
                                                                                AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                        responseState(
                                                                                RESP_PASSIVE_WAIT)
{
    setDefaultInvalid();
    config.append(Configuration());
    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
    configurationChanged();
}

void AcquirePMSLASAIR::setToAutoprobe()
{
    responseState = RESP_AUTOPROBE_PASSIVE_INITIALIZE;
    autoprobeValidRecords = 0;
}

void AcquirePMSLASAIR::setToInteractive()
{ responseState = RESP_INTERACTIVE_INITIALIZE; }


ComponentOptions AcquirePMSLASAIRComponent::getBaseOptions()
{
    ComponentOptions options;

    ComponentOptionSingleDouble *d =
            new ComponentOptionSingleDouble(tr("wavelength", "name"), tr("Sample wavelength"),
                                            tr("This wavelength in nm of the laser in the LASAIR."),
                                            tr("532nm", "default wavelength"), 1);
    d->setMinimum(0.0, false);
    options.add("wavelength", d);

    return options;
}

AcquirePMSLASAIR::AcquirePMSLASAIR(const ComponentOptions &options,
                                   const std::string &loggingContext) : FramedInstrument("lasair",
                                                                                         loggingContext),
                                                                        lastRecordTime(
                                                                                FP::undefined()),
                                                                        autoprobeStatus(
                                                                                AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                        responseState(
                                                                                RESP_PASSIVE_WAIT)
{
    Q_UNUSED(options);

    setDefaultInvalid();

    config.append(Configuration());
    configurationChanged();
}

AcquirePMSLASAIR::~AcquirePMSLASAIR()
{
}


void AcquirePMSLASAIR::logValue(double startTime,
                                double endTime,
                                SequenceName::Component name,
                                Variant::Root &&value)
{
    if (!loggingEgress && !realtimeEgress)
        return;
    SequenceValue dv({{}, "raw", std::move(name)}, std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        if (FP::defined(startTime))
            loggingEgress->incomingData(std::move(dv));
        return;
    }
    if (loggingEgress && FP::defined(startTime))
        loggingEgress->incomingData(dv);
    if (FP::defined(endTime)) {
        dv.setStart(endTime);
        dv.setEnd(endTime + 1.0);
    } else if (FP::defined(startTime)) {
        dv.setEnd(startTime + 1.0);
    }
    realtimeEgress->incomingData(std::move(dv));
}

void AcquirePMSLASAIR::realtimeValue(double time,
                                     SequenceName::Component name,
                                     Variant::Root &&value)
{
    if (!realtimeEgress)
        return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

SequenceValue::Transfer AcquirePMSLASAIR::buildLogMeta(double time) const
{
    Q_ASSERT(!config.isEmpty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_pms_lasair");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["InstrumentID"].set(instrumentMeta["InstrumentID"]);
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    processing["Model"].set(instrumentMeta["Model"]);

    result.emplace_back(SequenceName({}, "raw_meta", "N"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0");
    result.back().write().metadataReal("Units").setString("cm\xE2\x81\xBB³");
    result.back().write().metadataReal("Description").setString("Total concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Concentration"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "Q"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Sample flow");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Flow"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "V"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Laser reference voltage");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Laser"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);

    result.emplace_back(SequenceName({}, "raw_meta", "ZMANIFOLD"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataInteger("Format").setString("00");
    result.back().write().metadataInteger("Description").setString("Manifold port being sampled");
    result.back().write().metadataInteger("Source").set(instrumentMeta);
    result.back().write().metadataInteger("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataInteger("Realtime")
          .hash("Name")
          .setString(QObject::tr("Manifold"));
    result.back().write().metadataInteger("Realtime").hash("RowOrder").setInt64(3);

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.emplace_back(SequenceName({}, "raw_meta", "Nb"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataArray("Count").setInt64(TOTAL_BINS);
    result.back().write().metadataArray("Children").metadataReal("Format").setString("00000.0");
    result.back()
          .write()
          .metadataArray("Children")
          .metadataReal("Units")
          .setString("cm\xE2\x81\xBB³");
    result.back().write().metadataArray("Description").setString("Bin concentration");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Realtime").hash("Page").setInt64(1);
    result.back()
          .write()
          .metadataArray("Realtime")
          .hash("Name")
          .setString(QObject::tr("Concentration"));
    result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "Ns"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataArray("Count").setInt64(TOTAL_BINS);
    result.back().write().metadataArray("Children").metadataReal("Format").setString("00.00");
    result.back().write().metadataArray("Units").setString("\xCE\xBCm");
    result.back().write().metadataArray("Description").setString("Bin center diameter");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Realtime").hash("Page").setInt64(1);
    result.back().write().metadataArray("Realtime").hash("Name").setString(QObject::tr("Diameter"));
    result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInt64(1);

    return result;
}

SequenceValue::Transfer AcquirePMSLASAIR::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_pms_lasair");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["InstrumentID"].set(instrumentMeta["InstrumentID"]);
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    processing["Model"].set(instrumentMeta["Model"]);

    result.emplace_back(SequenceName({}, "raw_meta", "ZN"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataArray("Count").setInt64(TOTAL_BINS);
    result.back().write().metadataArray("Children").metadataReal("Format").setString("000000");
    result.back().write().metadataArray("Description").setString("Bin counts");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Realtime").hash("Page").setInt64(1);
    result.back()
          .write()
          .metadataArray("Realtime")
          .hash("Name")
          .setString(QObject::tr("Cumulative"));
    result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "ZNb"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataArray("Count").setInt64(TOTAL_BINS);
    result.back().write().metadataArray("Children").metadataReal("Format").setString("000000");
    result.back().write().metadataArray("Description").setString("Bin counts");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Realtime").hash("Page").setInt64(1);
    result.back()
          .write()
          .metadataArray("Realtime")
          .hash("Name")
          .setString(QObject::tr("Differential"));
    result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInt64(1);

    return result;
}

static std::deque<Util::ByteView> splitLines(const Util::ByteView &frame)
{
    std::deque<Util::ByteView> result;
    for (const char *begin = frame.data<const char *>(),
            *end = frame.data<const char *>() + frame.size(); begin != end;) {
        const char *ptr = (const char *) memchr(begin, '\r', end - begin);
        if (ptr == NULL)
            ptr = end;
        const char *ptr2 = (const char *) memchr(begin, '\n', ptr - begin);
        if (ptr2 != NULL)
            ptr = ptr2;

        result.emplace_back(begin, ptr - begin);
        if (ptr == end)
            break;
        begin = ptr + 1;
        if (begin == end)
            break;

        if (*ptr == '\n' && *begin == '\r')
            ++begin;
        else if (*ptr == '\r' && *begin == '\n')
            ++begin;
    }
    return result;
}

static bool isSpaceDelimiter(char c)
{
    return (c == ' ' || c == '\t' || c == '\r' || c == '\n' || c == '\v');
}

void AcquirePMSLASAIR::setEffectiveSizes(int model)
{
    const double *expectedSizes = getSizes(model);
    if (expectedSizes == NULL)
        return;

    for (int i = 0; i < TOTAL_BINS; i++) {
        double cs = config.first().sizes[i];
        if (FP::defined(cs)) {
            effectiveSizes[i] = cs;
            continue;
        }
        effectiveSizes[i] = expectedSizes[i];
    }
}

int AcquirePMSLASAIR::processInstrumentID(const Util::ByteView &line)
{
    if (line.empty()) return 1;

    QString str(line.toQString(false));
    if (str != instrumentMeta["InstrumentID"].toQString()) {
        instrumentMeta["InstrumentID"].setString(str);
        haveEmittedLogMeta = false;
        haveEmittedRealtimeMeta = false;
        sourceMetadataUpdated();
    }

    bool ok = false;
    int n = line.parse_i32(&ok);
    if (ok && n > 0) {
        Variant::Root model(QString("LASAIR%1").arg(n));
        if (model.read() != instrumentMeta["Model"]) {
            instrumentMeta["Model"].set(model);
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }
        setEffectiveSizes(n);
    } else {
        QRegExp reModel("\\s*LASAIR\\s*(\\d+)\\s*", Qt::CaseInsensitive);
        if (reModel.exactMatch(str)) {
            n = reModel.cap(1).toInt(&ok);
            if (ok && n > 0) {
                Variant::Root model(QString("LASAIR%1").arg(n));
                if (model.read() != instrumentMeta["Model"]) {
                    instrumentMeta["Model"].set(model);
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;
                    sourceMetadataUpdated();
                }
                setEffectiveSizes(n);
            }
        }
    }

    return 0;
}

int AcquirePMSLASAIR::processDateTime(const Util::ByteView &date, const Util::ByteView &time)
{
    auto fields = Util::as_deque(Util::ByteView(date).string_trimmed().split('/'));
    if (fields.size() != 3) return 1;

    Util::ByteView field;
    bool ok = false;

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root year;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 0) return 2;
        if (field.size() == 2) {
            raw += 2000;
            year.write().setInt64(raw);
        } else if (field.size() == 4) {
            if (raw < 1900 || raw > 2100) return 3;
            year.write().setInt64(raw);
        } else {
            return 4;
        }
    }
    remap("YEAR", year);

    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root month;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 1 || raw > 12) return 5;
        month.write().setInt64(raw);
    }
    remap("MONTH", month);

    if (fields.empty()) return 6;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.empty()) return 7;
    Variant::Root day;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 1 || raw > 31) return 8;
        day.write().setInt64(raw);
    }
    remap("DAY", day);


    fields = Util::as_deque(Util::ByteView(time).string_trimmed().split(':'));
    if (fields.size() != 3) return 9;

    field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.empty()) return 10;
    Variant::Root hour;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 0 || raw > 23) return 11;
        hour.write().setInt64(raw);
    }
    remap("HOUR", hour);

    field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.empty()) return 12;
    Variant::Root minute;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 0 || raw > 59) return 13;
        minute.write().setInt64(raw);
    }
    remap("MINUTE", minute);

    field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.empty()) return 14;
    Variant::Root second;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 0 || raw > 60) return 15;
        second.write().setInt64(raw);
    }
    remap("SECOND", second);

    qint64 y = year.read().toInt64();
    qint64 mo = month.read().toInt64();
    qint64 d = day.read().toInt64();
    qint64 h = hour.read().toInt64();
    qint64 m = minute.read().toInt64();
    qint64 s = second.read().toInt64();
    if (INTEGER::defined(y) &&
            y > 1900 &&
            y < 2100 &&
            INTEGER::defined(mo) &&
            mo >= 1 &&
            mo <= 12 &&
            INTEGER::defined(d) &&
            d >= 1 &&
            d <= 31 &&
            INTEGER::defined(h) &&
            h >= 0 &&
            h <= 23 &&
            INTEGER::defined(m) &&
            h >= 0 &&
            h <= 59 &&
            INTEGER::defined(s) &&
            h >= 0 &&
            h <= 60) {
        QDate qd((int) y, (int) mo, (int) d);
        QTime qt((int) h, (int) m, (int) s);
        if (qd.isValid() && qt.isValid()) {
            pending.time = Time::fromDateTime(QDateTime(qd, qt, Qt::UTC));
        }
    }

    return 0;
}

Variant::Root AcquirePMSLASAIR::processFirmwareVersion(const Util::ByteView &field)
{
    if (!field.string_start_insensitive("v"))
        return Variant::Root();
    return Variant::Root(field.mid(1).string_trimmed().toString());
}

int AcquirePMSLASAIR::processLine1(std::deque<Util::ByteView> &fields)
{
    if (fields.size() < 2) return 90;
    auto d = fields.front().string_trimmed();
    fields.pop_front();
    auto t = fields.front().string_trimmed();
    fields.pop_front();
    return processDateTime(d, t);
}

static double convertVolume(double input)
{
    if (!FP::defined(input) || input < 0.0)
        return FP::undefined();
    return input / 35.3147;
}

int AcquirePMSLASAIR::processLine2(std::deque<Util::ByteView> &fields)
{
    Util::ByteView field;
    bool ok = false;

    if (fields.empty()) return 1;
    field = fields.front().string_trimmed();
    fields.pop_front();
    pending.sampleInterval.write().setReal(field.parse_real(&ok));
    if (!ok) return 2;
    if (!FP::defined(pending.sampleInterval.read().toReal())) return 3;
    remap("SAMPLEINTERVAL", pending.sampleInterval);

    if (fields.empty()) return 1;
    field = fields.front().string_trimmed();
    fields.pop_front();
    {
        double v = field.parse_real(&ok);
        if (!ok) return 4;
        if (!FP::defined(v)) return 5;
        pending.dQt.write().setReal(convertVolume(v));
    }
    remap("Qtd", pending.dQt);

    if (fields.empty()) return 6;
    pending.firmwareVersion = processFirmwareVersion(fields.front().string_trimmed());
    fields.pop_front();
    if (!pending.firmwareVersion.read().exists()) return 7;

    /* "Always zero", ignored */
    if (fields.empty()) return 8;
    fields.pop_front();

    /* "Searching", ignored */
    if (fields.empty()) return 9;
    fields.pop_front();

    if (fields.empty()) return 10;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (field == "0") {
        pending.manifoldPosition.write().setInt64(INTEGER::undefined());
    } else {
        qint64 n = field.parse_i64(&ok);
        if (!ok) return 11;
        if (!INTEGER::defined(n)) return 12;
        pending.manifoldPosition.write().setInt64(n);
    }
    remap("ZMANIFOLD", pending.manifoldPosition);

    /* Sample number, ignored */
    if (fields.empty()) return 13;
    fields.pop_front();

    return 0;
}

int AcquirePMSLASAIR::processLine3(std::deque<Util::ByteView> &fields)
{
    Util::ByteView field;
    bool ok = false;

    pending.N.write().setEmpty();
    for (int i = 0; i < TOTAL_BINS; i++) {
        if (fields.empty()) return (i + 1) * 5 + 0;
        field = fields.front().string_trimmed();
        fields.pop_front();

        double v = field.parse_real(&ok);
        if (!ok) return (i + 1) * 5 + 1;
        if (!FP::defined(v)) return (i + 1) * 5 + 2;
        pending.N.write().array(i).setDouble(v);
    }
    remap("ZNb", pending.N);

    return 0;
}

static double convertFlow(double input)
{
    if (!FP::defined(input) || input < 0.0)
        return FP::undefined();
    return input * 28.3168;
}

int AcquirePMSLASAIR::processLine4(std::deque<Util::ByteView> &fields)
{
    Util::ByteView field;
    bool ok = false;

    if (fields.empty()) return 1;
    field = fields.front().string_trimmed();
    fields.pop_front();
    pending.V.write().setReal(field.parse_real(&ok));
    if (!ok) return 2;
    if (!FP::defined(pending.V.read().toReal())) return 3;
    remap("V", pending.V);

    if (fields.empty()) return 1;
    field = fields.front().string_trimmed();
    fields.pop_front();
    {
        double v = field.parse_real(&ok);
        if (!ok) return 4;
        if (!FP::defined(v)) return 5;
        pending.Q.write().setReal(convertFlow(v));
    }
    remap("Q", pending.Q);

    /* Analog value, ignored */
    if (fields.empty()) return 6;
    fields.pop_front();

    /* Analog value, ignored */
    if (fields.empty()) return 7;
    fields.pop_front();

    /* Analog value, ignored */
    if (fields.empty()) return 8;
    fields.pop_front();

    /* Analog value, ignored */
    if (fields.empty()) return 9;
    fields.pop_front();

    /* Always 0.000 (unusable analog), ignored */
    if (fields.empty()) return 10;
    fields.pop_front();

    return 0;
}

static double calculateSampleVolume(double Q,
                                    double sampleInterval,
                                    double recordStart,
                                    double recordEnd)
{
    if (!FP::defined(Q) || Q <= 0.0)
        return FP::undefined();
    if (!FP::defined(sampleInterval) &&
            FP::defined(recordStart) &&
            FP::defined(recordEnd) &&
            recordEnd > recordStart) {
        sampleInterval = recordEnd - recordStart;
    }
    if (!FP::defined(sampleInterval) || sampleInterval <= 0.0)
        return FP::undefined();
    return (Q * 1000.0 / 60.0) * sampleInterval;
}

static double calculateConcentration(double count, double dQt)
{
    if (!FP::defined(count) || !FP::defined(dQt) || dQt == 0.0)
        return FP::undefined();
    return count / (dQt * 1E6);
}

int AcquirePMSLASAIR::finishReport(double &frameTime)
{
    if (!FP::defined(frameTime)) {
        frameTime = pending.time;
        configAdvance(frameTime);
    }
    if (!FP::defined(frameTime) || (FP::defined(lastRecordTime) && frameTime <= lastRecordTime)) {
        return -1;
    }

    double startTime = lastRecordTime;
    double endTime = frameTime;
    lastRecordTime = endTime;

    if (pending.firmwareVersion.read().exists() &&
            pending.firmwareVersion.read() != instrumentMeta["FirmwareVersion"]) {
        instrumentMeta["FirmwareVersion"].set(pending.firmwareVersion);
        haveEmittedLogMeta = false;
        haveEmittedRealtimeMeta = false;
        sourceMetadataUpdated();
    }

    if (!haveEmittedLogMeta && loggingEgress != NULL && FP::defined(startTime)) {
        haveEmittedLogMeta = true;
        loggingEgress->incomingData(buildLogMeta(startTime));
    }
    if (!haveEmittedRealtimeMeta && realtimeEgress != NULL) {
        haveEmittedRealtimeMeta = true;
        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }

    double dQt = pending.dQt.read().toDouble();
    if (!FP::defined(dQt)) {
        Variant::Root v(calculateSampleVolume(pending.Q.read().toDouble(),
                                              pending.sampleInterval.read().toDouble(), startTime,
                                              endTime));
        remap("ZQtd", v);
        dQt = v.read().toDouble();
    }
    Variant::Root Nb;
    Variant::Root ZN;
    Variant::Root Ns;
    double accumulator = 0.0;
    for (int i = 0; i < TOTAL_BINS; i++) {
        double c = pending.N.read().array(i).toDouble();
        Nb.write().array(i).setDouble(calculateConcentration(c, dQt));

        if (FP::defined(c))
            accumulator += c;
        ZN.write().array(i).setDouble(accumulator);

        Ns.write().array(i).setDouble(effectiveSizes[i]);
    }
    remap("Nb", Nb);
    remap("Ns", Ns);

    Variant::Root N(calculateConcentration(accumulator, dQt));
    remap("N", N);


    logValue(startTime, endTime, "F1", Variant::Root(Variant::Type::Flags));
    logValue(startTime, endTime, "Q", Variant::Root(pending.Q));
    logValue(startTime, endTime, "V", Variant::Root(pending.V));
    logValue(startTime, endTime, "ZMANIFOLD", Variant::Root(pending.manifoldPosition));
    logValue(startTime, endTime, "Nb", std::move(Nb));
    logValue(startTime, endTime, "Ns", std::move(Ns));
    logValue(startTime, endTime, "N", std::move(N));

    realtimeValue(endTime, "ZN", std::move(ZN));
    realtimeValue(endTime, "ZNb", Variant::Root(pending.N));

    return 0;
}

int AcquirePMSLASAIR::processNextLine(const Util::ByteView &line, bool possibleChecksum)
{

    switch (nextLine) {
    case 0: {
        int code = processInstrumentID(line.toQByteArray().trimmed());
        if (code > 0) return code + 100;
        if (code < 0) return code;
        break;
    }

    case 1: {
        auto fields = CSV::acquisitionSplit(line);
        int code = processLine1(fields);
        if (code > 0) return code + 200;
        if (code < 0) return code;
        if (config.first().strictMode && !fields.empty()) return 299;
        break;
    }

    case 2: {
        auto fields = CSV::acquisitionSplit(line);
        int code = processLine2(fields);
        if (code > 0) return code + 300;
        if (code < 0) return code;
        if (config.first().strictMode && !fields.empty()) return 399;
        break;
    }

    case 3: {
        auto fields = CSV::acquisitionSplit(line);
        int code = processLine3(fields);
        if (code > 0) return code + 400;
        if (code < 0) return code;
        if (config.first().strictMode && !fields.empty()) return 499;
        break;
    }

    case 4: {
        auto fields = CSV::acquisitionSplit(line);
        int code = processLine4(fields);
        if (code > 0) return code + 500;
        if (code < 0) return code;
        if (config.first().strictMode && !fields.empty()) return 599;
        break;
    }

    case 5:
        if (possibleChecksum) {
            bool ok = false;
            quint32 sum = line.toQByteArray().trimmed().toUInt(&ok, 16);
            if (!ok) return 600;
            if (config.first().strictMode && sum == 0) return 601;
            /* Can't actually verify the checksum here since we don't know
             * what ended up delimiting the lines (i.e. the input may have
             * had DOS/Unix line conversions done) */
            break;
        }
        /* Fall through */

    default:
        if (config.first().strictMode)
            return 999;
    }

    ++nextLine;

    return 0;
}

int AcquirePMSLASAIR::processRecord(const Util::ByteView &frame, double &frameTime)
{
    Q_ASSERT(!config.isEmpty());

    if (frame.size() < 3)
        return 1;

    Util::ByteView field;
    bool ok = false;

    {
        auto lines = splitLines(frame);
        if (lines.size() > 1) {
            /* Multiple lines, so we know it's conventional framing */

            {
                /* Checksum is everything except the checksum digits 
                 * themselves, the space right before them, and the 
                 * STX/ETX characters */
                field = lines.back().string_trimmed();
                quint32 sum = field.parse_u32(&ok, 16);
                if (ok && sum != 0) {
                    Q_ASSERT(frame.size() > 0);
                    const char *begin = frame.data<const char *>();
                    const char *end = begin + frame.size() - 1;
                    for (; end != begin && isSpaceDelimiter(*end); --end) { }
                    for (; end != begin && !isSpaceDelimiter(*end); --end) { }
                    for (; end != begin && isSpaceDelimiter(*end); --end) { }
                    if (end != begin) {
                        end++;
                        quint32 calc = 0;
                        for (; begin != end; ++begin) {
                            calc += *((const quint8 *) begin);
                        }
                        if (calc != sum)
                            return 2;
                        lines.pop_back();
                    }
                }
            }

            nextLine = 0;
            for (const auto &line : lines) {
                int code = processNextLine(line.toQByteArray().trimmed(), false);
                if (code > 0) return 1000 + code;
                if (code < 0) return code;
            }
            nextLine = 0;

            int code = finishReport(frameTime);
            if (code > 0) return 2000 + code;
            if (code < 0) return code;
            return 0;
        }
    }

    auto fields = CSV::acquisitionSplit(frame);
    if (fields.size() >= TOTAL_BINS + 16) {
        /* All concatenated together, so process it all */

        for (std::size_t idx = 1, max = fields.size(); idx < max; ++idx) {
            field = fields[idx].string_trimmed();
            if (field.empty())
                continue;

            if (idx > 0) {
                int code = processDateTime(fields[idx - 1].string_trimmed(),
                                           fields[idx].string_trimmed());
                if (code == 0) {
                    if (idx > 1) {
                        Util::ByteArray line;
                        for (auto add = fields.begin(), end = fields.begin() + (idx - 1);
                                add != end;
                                ++add) {
                            if (!line.empty())
                                line.push_back(' ');
                            line += *add;
                        }
                        code = processInstrumentID(Util::ByteView(line).string_trimmed());
                        if (code > 0) return 3000 + code;
                        if (code < 0) return code;
                    }

                    fields.erase(fields.begin(), fields.begin() + idx + 1);
                    break;
                }
            }

            if (idx > 1 && processFirmwareVersion(field).read().exists()) {
                fields.erase(fields.begin(), fields.begin() + idx - 2);
                pending.time = FP::undefined();
                break;
            }
        }

        int code = processLine2(fields);
        if (code > 0) return 4000 + code;
        if (code < 0) return code;

        code = processLine3(fields);
        if (code > 0) return 4100 + code;
        if (code < 0) return code;

        code = processLine4(fields);
        if (code > 0) return 4200 + code;
        if (code < 0) return code;

        if (!fields.empty()) {
            ok = false;
            quint32 sum = fields.front().string_trimmed().parse_u32(&ok, 16);
            fields.pop_front();
            if (!ok) return 4300;
            if (config.first().strictMode && sum == 0) return 4301;
        }

        if (config.first().strictMode && !fields.empty()) return 4999;

        code = finishReport(frameTime);
        if (code > 0) return 5000 + code;
        if (code < 0) return code;
        return 0;
    }

    /* Just a single line, so assume it's the next in order and process it */

    auto trimmed_frame = frame;
    trimmed_frame.string_trimmed();
    int code = processNextLine(trimmed_frame);
    if (code > 0) {
        /* This gets complicated because we might not have the checksum,
         * so we have to see if finishing the report then retrying as
         * the first line is valid. */
        if (nextLine < 5) return 6000 + code;

        /* This might emit data, but at this stage we've got everything
         * expect the checksum, so it's probably valid anyway */
        code = finishReport(frameTime);
        if (code > 0) return 7000 + code;
        if (code < 0) return code;

        nextLine = 0;
        code = processNextLine(trimmed_frame);
        if (code > 0) return 8000 + code;
        if (code < 0) return code;

        return 0;
    }
    if (code < 0) return code;

    if (nextLine >= 6) {
        code = finishReport(frameTime);
        if (code > 0) return 9000 + code;
        if (code < 0) return code;

        nextLine = 0;
        return 0;
    } else {
        if (!FP::defined(frameTime))
            frameTime = pending.time;
    }

    return -1;
}

void AcquirePMSLASAIR::configAdvance(double frameTime)
{
    Q_ASSERT(!config.isEmpty());
    int oldSize = config.size();
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.isEmpty());

    if (oldSize == config.size())
        return;
    configurationChanged();
}

void AcquirePMSLASAIR::configurationChanged()
{
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;

    for (int i = 0; i < TOTAL_BINS; i++) {
        double cs = config.first().sizes[i];
        if (!FP::defined(cs))
            continue;
        effectiveSizes[i] = cs;
    }
}


std::size_t AcquirePMSLASAIR::dataFrameStart(std::size_t offset, const Util::ByteArray &input) const
{
    auto max = input.size();
    while (offset < max) {
        auto ch = input[offset];
        if (ch == 0x02) {
            if (offset + 1 > max)
                return input.npos;
            return offset + 1;
        }
        if (ch == '\r' || ch == '\n' || ch == 0x03) {
            offset++;
            continue;
        }
        return offset;
    }
    return input.npos;
}

std::size_t AcquirePMSLASAIR::dataFrameEnd(std::size_t start, const Util::ByteArray &input) const
{
    if (start >= 1 && input[start - 1] == 0x02) {
        /* If we have an STX, wait for an ETX */
        for (auto max = input.size(); start < max; start++) {
            char ch = input[start];
            if (ch == 0x03)
                return start;
        }
    } else {
        for (auto max = input.size(); start < max; start++) {
            auto ch = input[start];
            if (ch == '\r' || ch == '\n')
                return start;
        }
    }
    return input.npos;
}

void AcquirePMSLASAIR::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (FP::defined(frameTime))
        configAdvance(frameTime);

    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 6.0;

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            if (++autoprobeValidRecords > 5) {
                qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

                responseState = RESP_PASSIVE_RUN;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                if (FP::defined(frameTime))
                    timeoutAt(frameTime + unpolledResponseTime * 2.0);

                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("PassiveAutoprobeWait");
                event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);
            }
        } else if (code > 0) {
            autoprobeValidRecords = 0;
            nextLine = 0;
            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;
    }
    case RESP_PASSIVE_WAIT: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);

            responseState = RESP_PASSIVE_RUN;
            ++autoprobeValidRecords;
            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveWait");
            event(frameTime, QObject::tr("Passive communications established."), false, info);
        } else if (code > 0) {
            autoprobeValidRecords = 0;
            nextLine = 0;
            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;
    }

    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

            if (FP::defined(frameTime))
                timeoutAt(frameTime + unpolledResponseTime * 2.0);
            responseState = RESP_UNPOLLED_RUN;
            ++autoprobeValidRecords;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            event(frameTime, QObject::tr("Communications established."), false, info);
        } else if (code > 0) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "rejected with code" << code;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            autoprobeValidRecords = 0;
            nextLine = 0;

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            discardData(frameTime + 30.0);

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Communications start rejected the first line (code %1).").arg(code),
                  true, info);

            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;
    }

    case RESP_PASSIVE_RUN:
    case RESP_UNPOLLED_RUN: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            if (FP::defined(frameTime))
                timeoutAt(frameTime + unpolledResponseTime * 2.0);
            ++autoprobeValidRecords;
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            Variant::Write info = Variant::Write::empty();

            if (responseState == RESP_PASSIVE_RUN) {
                timeoutAt(FP::undefined());
                responseState = RESP_PASSIVE_WAIT;
                info.hash("ResponseState").setString("PassiveRun");
            } else {
                if (controlStream != NULL) {
                    controlStream->writeControl("ES\r\n");
                }
                responseState = RESP_INTERACTIVE_START_STOPREPORTS;
                if (FP::defined(frameTime)) {
                    discardData(frameTime + 1.0);
                    timeoutAt(frameTime + 30.0);
                }
                info.hash("ResponseState").setString("Run");
            }
            generalStatusUpdated();
            autoprobeValidRecords = 0;
            nextLine = 0;

            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;
    }

    default:
        break;
    }
}

void AcquirePMSLASAIR::incomingInstrumentTimeout(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configAdvance(frameTime);

    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 6.0;

    switch (responseState) {
    case RESP_UNPOLLED_RUN:
        qCDebug(log) << "Timeout in unpolled state" << responseState << "at"
                     << Logging::time(frameTime);

        if (controlStream != NULL) {
            controlStream->writeControl("ES\r\n");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        discardData(frameTime + 1.0);
        timeoutAt(frameTime + 30.0);
        generalStatusUpdated();

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("UnpolledRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;

    case RESP_PASSIVE_RUN:
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        responseState = RESP_PASSIVE_WAIT;
        generalStatusUpdated();

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_SETINTERVAL:
    case RESP_INTERACTIVE_START_SETCONTINUOUS:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
    case RESP_INTERACTIVE_RESTART_WAIT:
        qCDebug(log) << "Timeout in interactive start state" << responseState << "at"
                     << Logging::time(frameTime);

        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;

    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        if (controlStream != NULL) {
            controlStream->writeControl("ES\r\n");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        discardData(frameTime + 1.0);
        timeoutAt(frameTime + 30.0);
        break;

    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;

    default:
        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;
    }
}

void AcquirePMSLASAIR::discardDataCompleted(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configAdvance(frameTime);

    Q_ASSERT(!config.isEmpty());
    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 6.0;

    switch (responseState) {
    case RESP_INTERACTIVE_START_STOPREPORTS:
        if (controlStream != NULL) {
            QDateTime st(Time::toDateTime(frameTime + 0.5));
            Util::ByteArray command("TD ");
            command += QByteArray::number(st.date().year() % 100).rightJustified(2, '0');
            command.push_back('/');
            command += QByteArray::number(st.date().month()).rightJustified(2, '0');
            command.push_back('/');
            command += QByteArray::number(st.date().day()).rightJustified(2, '0');
            command.push_back(' ');
            command += QByteArray::number(st.time().hour()).rightJustified(2, '0');
            command.push_back(':');
            command += QByteArray::number(st.time().minute()).rightJustified(2, '0');
            command.push_back(':');
            command += QByteArray::number(st.time().second()).rightJustified(2, '0');
            command += "\r\n";
            controlStream->writeControl(command);
        }
        responseState = RESP_INTERACTIVE_START_SETTIME;
        discardData(frameTime + 1.0);
        break;

    case RESP_INTERACTIVE_START_SETTIME:
        if (controlStream != NULL) {
            QByteArray command("TI ");
            command.append(QByteArray::number(unpolledResponseTime, 'f', 0).rightJustified(3, '0'));
            command.append("\r\n");
            controlStream->writeControl(command);
        }
        responseState = RESP_INTERACTIVE_START_SETINTERVAL;
        discardData(frameTime + 1.0);
        break;

    case RESP_INTERACTIVE_START_SETINTERVAL:
        if (controlStream != NULL) {
            controlStream->writeControl("SN\r\n");
        }
        responseState = RESP_INTERACTIVE_START_SETCONTINUOUS;
        discardData(frameTime + 1.0);
        break;

    case RESP_INTERACTIVE_START_SETCONTINUOUS:
        if (controlStream != NULL) {
            controlStream->writeControl("SS\r\n");
        }
        responseState = RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID;
        timeoutAt(frameTime + unpolledResponseTime * 4.0 + 1.0);
        discardData(frameTime + 1.0, 1);
        break;

    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
        break;

    case RESP_INTERACTIVE_RESTART_WAIT:
        if (controlStream != NULL) {
            controlStream->writeControl("ES\r\n");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        discardData(frameTime + 1.0);
        timeoutAt(frameTime + 30.0);
        break;

    default:
        break;
    }
}


void AcquirePMSLASAIR::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_UNUSED(frame);
    Q_UNUSED(frameTime);
}

Variant::Root AcquirePMSLASAIR::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquirePMSLASAIR::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_UNPOLLED_RUN:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

AcquisitionInterface::AutoprobeStatus AcquirePMSLASAIR::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquirePMSLASAIR::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    Q_ASSERT(!config.isEmpty());
    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 6.0;

    switch (responseState) {
    case RESP_UNPOLLED_RUN:
        /* Already running/started, do nothing */
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << responseState << "at" << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_SETINTERVAL:
    case RESP_INTERACTIVE_START_SETCONTINUOUS:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        if (controlStream != NULL) {
            controlStream->writeControl("ES\r\n");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        timeoutAt(time + 30.0);
        discardData(time + 1.0);

        generalStatusUpdated();
        break;
    }
}

void AcquirePMSLASAIR::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    Q_ASSERT(!config.isEmpty());
    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 6.0;

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
    autoprobeValidRecords = 0;
    timeoutAt(time + unpolledResponseTime * 6.0 + 3.0);
    generalStatusUpdated();
}

void AcquirePMSLASAIR::autoprobePromote(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    Q_ASSERT(!config.isEmpty());
    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 6.0;

    switch (responseState) {
    case RESP_UNPOLLED_RUN:
        qCDebug(log) << "Promoted from interactive run to interactive acquisition at"
                     << Logging::time(time);
        /* Reset timeout in case we didn't have a control stream */
        timeoutAt(time + unpolledResponseTime + 2.0);
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_SETINTERVAL:
    case RESP_INTERACTIVE_START_SETCONTINUOUS:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);
        /* Reset timeout in case we didn't have a control stream */
        timeoutAt(time + unpolledResponseTime * 2.0 + 30.0);
        break;

    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        if (controlStream != NULL) {
            controlStream->writeControl("ES\r\n");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS;
        timeoutAt(time + 30.0);
        discardData(time + 1.0);

        generalStatusUpdated();
        break;
    }
}

void AcquirePMSLASAIR::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    Q_ASSERT(!config.isEmpty());
    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 6.0;

    /* Reset timeout in case we didn't have a control stream */
    timeoutAt(time + unpolledResponseTime + 2.0);

    switch (responseState) {
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);

        break;

    case RESP_UNPOLLED_RUN:
        responseState = RESP_PASSIVE_RUN;

        qCDebug(log) << "Promoted from unpolled interactive state to passive acquisition at"
                     << Logging::time(time);
        break;

    default:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_PASSIVE_RUN;

        discardData(FP::undefined());
        generalStatusUpdated();
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquirePMSLASAIR::getDefaults()
{
    AutomaticDefaults result;
    result.name = "N$1$2";
    result.setSerialN81(1200);
    return result;
}


ComponentOptions AcquirePMSLASAIRComponent::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options, true);

    return options;
}

ComponentOptions AcquirePMSLASAIRComponent::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquirePMSLASAIRComponent::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples());
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquirePMSLASAIRComponent::requiresInputDevice()
{ return true; }

ExternalConverter *AcquirePMSLASAIRComponent::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options, true);
}

QList<ComponentExample> AcquirePMSLASAIRComponent::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data.")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquirePMSLASAIRComponent::createAcquisitionPassive(const ComponentOptions &options,
                                                                                  const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquirePMSLASAIR(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquirePMSLASAIRComponent::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquirePMSLASAIR(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquirePMSLASAIRComponent::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                    const std::string &loggingContext)
{
    std::unique_ptr<AcquirePMSLASAIR> i(new AcquirePMSLASAIR(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<
        AcquisitionInterface> AcquirePMSLASAIRComponent::createAcquisitionInteractive(const ValueSegment::Transfer &config,
                                                                                      const std::string &loggingContext)
{
    std::unique_ptr<AcquirePMSLASAIR> i(new AcquirePMSLASAIR(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
