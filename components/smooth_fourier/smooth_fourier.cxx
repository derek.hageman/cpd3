/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/component.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "smoothing/smoothingengine.hxx"

#include "smooth_fourier.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Smoothing;


QString SmoothFourierComponent::getGeneralSerializationName() const
{ return QString::fromLatin1("smooth_fourier"); }

ComponentOptions SmoothFourierComponent::getOptions()
{
    ComponentOptions options;

    TimeIntervalSelectionOption *tis =
            new TimeIntervalSelectionOption(tr("low", "name"), tr("The lower cutoff"),
                                            tr("This is the low period cutoff.  All frequencies with periods "
                                                       "shorter than this are removed.  However when used with a high "
                                                       "cutoff that would be mutually exclusive (shorter) it is instead "
                                                       "interpreted to be the upper bound of a band stop filter.  In this "
                                                       "case is specifies the longest period frequency to be removed."),
                                            QString());
    tis->setAllowZero(false);
    tis->setAllowNegative(false);
    tis->setAllowUndefined(true);
    tis->setDefaultAligned(false);
    options.add("low", tis);

    tis = new TimeIntervalSelectionOption(tr("high", "name"), tr("The high cutoff"),
                                          tr("This the high cutoff period.  All frequencies with periods longer "
                                                     "than this are removed.  However when used with a low cutoff "
                                                     "that would be mutually exclusive (longer) it is instead "
                                                     "interpreted to be the lower bound of a band stop filter.  In this "
                                                     "context is specifies the shortest period frequency to be removed."),
                                          QString());
    tis->setAllowZero(false);
    tis->setAllowNegative(false);
    tis->setAllowUndefined(true);
    tis->setDefaultAligned(false);
    options.add("high", tis);

    tis = new TimeIntervalSelectionOption(tr("gap", "name"), tr("The maximum allowed gap"),
                                          tr("If two values are seperated by this much time then the smoother is "
                                                     "reset.  That is, data separated by this much will be smoothed as "
                                                     "independent runs of data."),
                                          tr("Infinite", "default gap"));
    tis->setAllowZero(true);
    tis->setAllowNegative(false);
    tis->setAllowUndefined(true);
    tis->setDefaultAligned(true);
    options.add("gap", tis);

    options.add("ignore-undefined", new ComponentOptionBoolean(tr("ignore-undefined", "name"),
                                                               tr("Ignore undefined values"),
                                                               tr("If this option is set undefined values are ignored instead of "
                                                                          "forcing a smoother reset and another Fourier transform."),
                                                               tr("Enabled, undefined values are ignored",
                                                                  "default ignore undefined mode")));

    return options;
}

QList<ComponentExample> SmoothFourierComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    qobject_cast<TimeIntervalSelectionOption *>(options.get("low"))->set(Time::Hour, 1, false);
    examples.append(ComponentExample(options, tr("Low pass filter", "default example name"),
                                     tr("This create a low pass filter by removing all frequencies below "
                                                "one hour in period.")));

    options = getOptions();
    qobject_cast<TimeIntervalSelectionOption *>(options.get("low"))->set(Time::Minute, 70, false);
    qobject_cast<TimeIntervalSelectionOption *>(options.get("high"))->set(Time::Minute, 50, false);
    examples.append(ComponentExample(options, tr("Band stop filter", "default example name"),
                                     tr("This create a band stop filter that removes all frequencies "
                                                "between 50 and 70 minutes in period.  This will nominally remove "
                                                "any hourly cycles in the data.")));

    return examples;
}

ProcessingStage *SmoothFourierComponent::createGeneralFilterDynamic(const ComponentOptions &options)
{
    bool smoothUndefined = true;
    if (options.isSet("ignore-undefined")) {
        smoothUndefined =
                qobject_cast<ComponentOptionBoolean *>(options.get("ignore-undefined"))->get();
    }

    DynamicTimeInterval *gap;
    if (options.isSet("gap")) {
        gap = qobject_cast<TimeIntervalSelectionOption *>(options.get("gap"))->getInterval();
    } else {
        gap = NULL;
    }

    DynamicTimeInterval *low = NULL;
    if (options.isSet("low")) {
        low = qobject_cast<TimeIntervalSelectionOption *>(options.get("low"))->getInterval();
    }

    DynamicTimeInterval *high = NULL;
    if (options.isSet("high")) {
        high = qobject_cast<TimeIntervalSelectionOption *>(options.get("high"))->getInterval();
    }

    return SmoothingEngine::createFourier(low, high, gap, smoothUndefined);
}

ProcessingStage *SmoothFourierComponent::createGeneralFilterEditing(double start,
                                                                    double end,
                                                                    const SequenceName::Component &station,
                                                                    const SequenceName::Component &archive,
                                                                    const ValueSegment::Transfer &config)
{
    Q_UNUSED(station);
    Q_UNUSED(archive);

    DynamicTimeInterval *gap = DynamicTimeInterval::fromConfiguration(config, "Gap", start, end);
    DynamicTimeInterval *low = DynamicTimeInterval::fromConfiguration(config, "Low", start, end);
    DynamicTimeInterval *high = DynamicTimeInterval::fromConfiguration(config, "High", start, end);

    bool smoothUndefined = true;
    for (const auto &seg : config) {
        if (!Range::intersects(seg.getStart(), seg.getEnd(), start, end))
            continue;
        if (seg.getValue().getPath("IgnoreUndefined").exists()) {
            smoothUndefined = seg.getValue().getPath("IgnoreUndefined").toBool();
        }
    }

    return SmoothingEngine::createFourier(low, high, gap, smoothUndefined);
}

ProcessingStage *SmoothFourierComponent::deserializeGeneralFilter(QDataStream &stream)
{
    return SmoothingEngine::deserializeFourier(stream);
}
