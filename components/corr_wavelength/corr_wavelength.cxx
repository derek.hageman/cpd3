/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QRegExp>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "core/environment.hxx"

#include "corr_wavelength.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Smoothing;
using namespace CPD3::Editing;

void CorrWavelength::handleOptions(const ComponentOptions &options)
{
    if (options.isSet("smoothing")) {
        defaultSmoother =
                qobject_cast<BaselineSmootherOption *>(options.get("smoothing"))->getSmoother();
    } else {
        defaultSmoother = NULL;
    }

    if (options.isSet("angstrom")) {
        defaultFallbackAngstrom =
                qobject_cast<DynamicInputOption *>(options.get("angstrom"))->getInput();
        if (options.isSet("angstrom-wavelength")) {
            defaultFallbackAngstromWavelength = qobject_cast<DynamicDoubleOption *>(
                    options.get("angstrom-wavelength"))->getInput();
            if (options.isSet("angstrom-distance")) {
                defaultFallbackAngstromDistance = qobject_cast<DynamicDoubleOption *>(
                        options.get("angstrom-distance"))->getInput();
            } else {
                defaultFallbackAngstromDistance = NULL;
            }
        } else {
            defaultFallbackAngstromWavelength = NULL;
            defaultFallbackAngstromDistance = NULL;
        }
    } else {
        defaultFallbackAngstrom = NULL;
        defaultFallbackAngstromWavelength = NULL;
        defaultFallbackAngstromDistance = NULL;
    }

    if (options.isSet("wavelengths")) {
        targetWavelengths =
                qobject_cast<ComponentOptionDoubleSet *>(options.get("wavelengths"))->get();
    }
    if (targetWavelengths.isEmpty()) {
        targetWavelengths |= 450.0;
        targetWavelengths |= 550.0;
        targetWavelengths |= 700.0;
    }

    restrictedInputs = false;

    if (options.isSet("correct")) {
        restrictedInputs = true;

        Processing p;

        for (QSet<double>::const_iterator wavelength = targetWavelengths.constBegin(),
                end = targetWavelengths.constEnd(); wavelength != end; ++wavelength) {
            Processing::WavelengthSet wl;

            wl.operate = qobject_cast<DynamicSequenceSelectionOption *>(
                    options.get("correct"))->getOperator();
            wl.wavelength = new DynamicPrimitive<double>::Constant(*wavelength);

            p.targets.push_back(wl);
        }

        Q_ASSERT(!p.targets.empty());
        p.adjust = WavelengthAdjust::fromInput(p.targets.front().operate->clone(),
                                               defaultSmoother != NULL ? defaultSmoother->clone()
                                                                       : new BaselineLatest);
        if (defaultFallbackAngstrom != NULL) {
            p.adjust
             ->addFallbackAngstrom(defaultFallbackAngstrom->clone(),
                                   defaultFallbackAngstromWavelength != NULL
                                   ? defaultFallbackAngstromWavelength->clone() : NULL,
                                   defaultFallbackAngstromDistance != NULL
                                   ? defaultFallbackAngstromDistance->clone() : NULL);
        }

        processing.push_back(p);
    }

    if (options.isSet("suffix")) {
        filterSuffixes = Util::set_from_qstring(
                qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->get());
    }

    if (options.isSet("create-outputs")) {
        createOutputs =
                qobject_cast<ComponentOptionBoolean *>(options.get("create-outputs"))->get();
    } else {
        createOutputs = false;
    }
}


CorrWavelength::CorrWavelength()
{ Q_ASSERT(false); }

CorrWavelength::CorrWavelength(const ComponentOptions &options)
{
    handleOptions(options);
}

CorrWavelength::CorrWavelength(const ComponentOptions &options,
                               double start,
                               double end,
                               const QList<SequenceName> &inputs)
{
    Q_UNUSED(start);
    Q_UNUSED(end);
    handleOptions(options);

    for (QList<SequenceName>::const_iterator unit = inputs.constBegin(), endU = inputs.constEnd();
            unit != endU;
            ++unit) {
        CorrWavelength::unhandled(*unit, NULL);
    }
}

CorrWavelength::CorrWavelength(double start,
                               double end,
                               const SequenceName::Component &station,
                               const SequenceName::Component &archive,
                               const ValueSegment::Transfer &config)
{
    defaultSmoother = NULL;
    defaultFallbackAngstrom = NULL;
    defaultFallbackAngstromWavelength = NULL;
    defaultFallbackAngstromDistance = NULL;
    createOutputs = false;
    restrictedInputs = true;

    struct ChildData {
        std::unordered_set<Variant::PathElement::HashIndex> optical;
    };
    std::unordered_map<Variant::PathElement::HashIndex, ChildData> children;
    for (const auto &add : config) {
        auto h = add.read().toHash();
        for (auto child : h) {
            auto &target = children[child.first];
            Util::merge(child.second["Optical"].toHash().keys(), target.optical);
        }
    }
    children.erase(Variant::PathElement::HashIndex());

    for (auto &child : children) {
        child.second.optical.erase(Variant::PathElement::HashIndex());

        Processing p;

        for (const auto &wl : child.second.optical) {
            Processing::WavelengthSet t;

            t.wavelength = DynamicDoubleOption::fromConfiguration(config,
                                                                  QString("%1/Optical/%2/Wavelength")
                                                                          .arg(QString::fromStdString(
                                                                                  child.first),
                                                                               QString::fromStdString(
                                                                                       wl)), start,
                                                                  end);
            t.operate = DynamicSequenceSelection::fromConfiguration(config,
                                                                    QString("%1/Optical/%2/Data").arg(
                                                                            QString::fromStdString(
                                                                                    child.first),
                                                                            QString::fromStdString(
                                                                                    wl)), start,
                                                                    end);

            t.operate->registerExpected(station, archive);

            p.targets.push_back(t);
        }

        p.adjust = WavelengthAdjust::fromConfiguration(config, QString("%1/Adjust").arg(
                QString::fromStdString(child.first)), start, end);

        p.adjust->registerExpected(station, archive);

        processing.push_back(p);
    }
}

void CorrWavelengthComponent::extendBasicFilterEditing(double &start,
                                                       double &end,
                                                       const SequenceName::Component &,
                                                       const SequenceName::Component &,
                                                       const ValueSegment::Transfer &config,
                                                       Archive::Access *)
{
    if (!FP::defined(start))
        return;

    std::unordered_set<Variant::PathElement::HashIndex> children;
    for (const auto &add : config) {
        Util::merge(add.read().toHash().keys(), children);
    }
    children.erase(Variant::PathElement::HashIndex());

    double result = start;
    for (const auto &child : children) {
        WavelengthAdjust *wl = WavelengthAdjust::fromConfiguration(config, QString("%1/Adjust").arg(
                QString::fromStdString(child)), start, end);
        double check = wl->spinupTime(start);
        if (FP::defined(check) && check < result)
            result = check;
        delete wl;
    }
    start = result;
}


CorrWavelength::~CorrWavelength()
{
    if (defaultSmoother != NULL)
        delete defaultSmoother;
    if (defaultFallbackAngstrom != NULL)
        delete defaultFallbackAngstrom;
    if (defaultFallbackAngstromWavelength != NULL)
        delete defaultFallbackAngstromWavelength;
    if (defaultFallbackAngstromDistance != NULL)
        delete defaultFallbackAngstromDistance;

    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        for (std::vector<Processing::WavelengthSet>::const_iterator wl = p->targets.begin(),
                end = p->targets.end(); wl != end; ++wl) {
            delete wl->operate;
            delete wl->wavelength;
        }
        delete p->adjust;
    }
}

void CorrWavelength::unhandled(const SequenceName &unit,
                               SegmentProcessingStage::SequenceHandlerControl *control)
{
    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        if (processing[id].adjust->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
        }
    }
    if (defaultFallbackAngstrom != NULL &&
            defaultFallbackAngstrom->registerInput(unit) &&
            control != NULL)
        control->deferHandling(unit);

    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        bool hit = false;
        for (std::vector<Processing::WavelengthSet>::iterator t = processing[id].targets.begin(),
                endT = processing[id].targets.end(); t != endT; ++t) {
            if (t->operate->registerInput(unit)) {
                hit = true;
            }
        }
        if (hit) {
            if (control != NULL)
                control->filterUnit(unit, id);
            return;
        }
        /* If we're creating and we already have a target for it, then don't add a new one */
        if (!restrictedInputs && createOutputs) {
            if (processing[id].adjust->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
                return;
            }
        }
    }

    if (restrictedInputs)
        return;

    if (unit.hasFlavor(SequenceName::flavor_cover) || unit.hasFlavor(SequenceName::flavor_stats))
        return;

    if (!Util::starts_with(unit.getVariable(), "B"))
        return;

    QRegExp reCheck("((?:Bb?sn?)|(?:B[ae]n?))[A-Z0-9]*_(.+)");
    if (!reCheck.exactMatch(unit.getVariableQString()))
        return;
    auto prefix = QRegExp::escape(reCheck.cap(1)).toStdString();
    auto suffix = reCheck.cap(2).toStdString();
    if (!filterSuffixes.empty() && !filterSuffixes.count(suffix))
        return;

    Q_ASSERT(!targetWavelengths.isEmpty());

    Processing p;

    bool useNumbering = false;
    if (createOutputs) {
        SequenceName::ComponentSet codes;
        std::vector<double> sorted;
        for (auto wavelength : targetWavelengths) {
            if (!FP::defined(wavelength))
                continue;
            const auto &code = Wavelength::code(wavelength);
            if (codes.count(code)) {
                useNumbering = true;
            }
            codes.insert(std::move(code));
            sorted.push_back(wavelength);
        }
        std::sort(sorted.begin(), sorted.end());

        int index = 1;
        for (auto wavelength : sorted) {
            Processing::WavelengthSet wl;

            SequenceName::Component code;
            if (useNumbering) {
                code = std::to_string(index);
            } else {
                code = Wavelength::code(wavelength);
            }
            ++index;

            wl.overrideInputWavelength = wavelength;
            wl.operate = new DynamicSequenceSelection::Match(unit.getStationQString(),
                                                             unit.getArchiveQString(),
                                                             QString("%1%2_%3").arg(QRegExp::escape(
                                                                     QString::fromStdString(
                                                                             prefix)),
                                                                                    QString::fromStdString(
                                                                                            code),
                                                                                    QRegExp::escape(
                                                                                            QString::fromStdString(
                                                                                                    suffix))),
                                                             unit.getFlavors());
            wl.operate->registerInput(unit);
            wl.operate
              ->registerExpected(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());

            wl.wavelength = new DynamicPrimitive<double>::Constant(wavelength);

            p.targets.emplace_back(std::move(wl));
        }

        p.adjust = WavelengthAdjust::fromInput(
                new DynamicSequenceSelection::Match(unit.getStationQString(),
                                                    unit.getArchiveQString(),
                                                    QString("%1[A-Z0-9]*_%2").arg(QRegExp::escape(
                                                            QString::fromStdString(prefix)),
                                                                                  QRegExp::escape(
                                                                                          QString::fromStdString(
                                                                                                  suffix))),
                                                    unit.getFlavors()),
                defaultSmoother ? defaultSmoother->clone() : new BaselineLatest);
    } else {
        for (auto wavelength : targetWavelengths) {
            Processing::WavelengthSet wl;

            wl.operate = new DynamicSequenceSelection::Match(unit.getStationQString(),
                                                             unit.getArchiveQString(),
                                                             QString("%1[A-Z0-9]*_%2").arg(
                                                                     QRegExp::escape(
                                                                             QString::fromStdString(
                                                                                     prefix)),
                                                                     QRegExp::escape(
                                                                             QString::fromStdString(
                                                                                     suffix))),
                                                             unit.getFlavors());
            wl.operate->registerInput(unit);

            wl.wavelength = new DynamicPrimitive<double>::Constant(wavelength);

            p.targets.emplace_back(std::move(wl));
        }

        Q_ASSERT(!p.targets.empty());
        p.adjust = WavelengthAdjust::fromInput(p.targets.front().operate->clone(),
                                               defaultSmoother ? defaultSmoother->clone()
                                                               : new BaselineLatest);
    }

    if (defaultFallbackAngstrom) {
        p.adjust
         ->addFallbackAngstrom(defaultFallbackAngstrom->clone(),
                               defaultFallbackAngstromWavelength ? defaultFallbackAngstromWavelength
                                       ->clone() : nullptr, defaultFallbackAngstromDistance
                                                            ? defaultFallbackAngstromDistance->clone()
                                                            : nullptr);
    }
    p.adjust->registerInput(unit);

    int id = (int) processing.size();
    processing.push_back(p);

    if (createOutputs) {
        SequenceName::Set filter;
        for (const auto &wl : p.targets) {
            Util::merge(wl.operate->getAllUnits(), filter);
        }
        for (const auto &f : filter) {
            if (control) {
                if (control->isHandled(f)) {
                    control->inputUnit(f, id);
                } else {
                    control->filterUnit(f, id);
                }
            }
        }
        if (control) {
            control->inputUnit(unit, id);
        }
    } else {
        if (control) {
            control->filterUnit(unit, id);
        }
    }
}

CPD3::Data::SequenceName::Map<
        const CorrWavelength::Processing::WavelengthSet *> CorrWavelength::buildAdjustments(
        Processing *p,
        double start,
        double end) const
{
    SequenceName::Map<const Processing::WavelengthSet *> adjustments;
    for (auto t = p->targets.cbegin(), endT = p->targets.cend(); t != endT; ++t) {
        const SequenceName *closest = nullptr;
        double closestDistance = FP::undefined();
        const auto &s = t->operate->get(start, end);
        for (const auto &i : s) {
            double wavelength = t->overrideInputWavelength;
            if (!FP::defined(wavelength))
                wavelength = p->adjust->getWavelength(i);
            if (!FP::defined(wavelength))
                continue;
            double target = t->wavelength->get(start, end);
            if (!FP::defined(target))
                continue;

            double distance = std::fabs(wavelength - target);
            auto existing = adjustments.find(i);
            if (existing != adjustments.end()) {
                if (std::fabs(wavelength - existing->second->wavelength->get(start, end)) <
                        distance)
                    continue;
            }

            if (!closest || distance < closestDistance) {
                closestDistance = distance;
                closest = &i;
            }
        }
        if (closest) {
            auto existing = adjustments.find(*closest);
            if (existing != adjustments.end()) {
                existing->second = &(*t);
            } else {
                adjustments.emplace(*closest, &(*t));
            }
        }
    }
    return adjustments;
}


void CorrWavelength::process(int id, SequenceSegment &data)
{
    Processing *proc = &processing[id];

    proc->adjust->incoming(data);

    for (const auto &adjust : buildAdjustments(proc, data.getStart(), data.getEnd())) {
        if (!createOutputs) {
            if (!data.exists(adjust.first))
                continue;
            if (!Variant::Composite::isDefined(data[adjust.first]))
                continue;
        }
        data[adjust.first].set(proc->adjust->getAdjusted(adjust.second->wavelength->get(data)));
    }
}

void CorrWavelength::processMeta(int id, SequenceSegment &data)
{
    Processing *p = &processing[id];

    Variant::Root meta;

    meta["By"].setString("corr_wavelength");
    meta["At"].setDouble(Time::time());
    meta["Environment"].setString(Environment::describe());
    meta["Revision"].setString(Environment::revision());

    /* Do this first so we can update the wavelengths */
    p->adjust->incomingMeta(data);

    for (const auto &adjust : buildAdjustments(p, data.getStart(), data.getEnd())) {
        SequenceName dvu(adjust.first.toMeta());
        if (!data.exists(dvu)) {
            if (!createOutputs)
                continue;

            Variant::Root base;
            for (auto find : p->adjust->getAllUnits()) {
                find.setMeta();
                if (!data.exists(find))
                    continue;
                auto check = data[find];
                if (!check.metadata("Wavelength").exists())
                    continue;
                base.write().set(check);
                break;
            }

            auto wr = base.write();
            wr.metadata("Processing").remove();
            wr.metadata("Smoothing").remove();

            data.setValue(dvu, std::move(base));
        }

        double target = adjust.second->wavelength->get(data);

        meta["Parameters"].hash("OriginalWavelength")
                          .setDouble(p->adjust->getWavelength(adjust.first));
        meta["Parameters"].hash("TargetWavelength").setDouble(target);

        data[dvu].metadata("Processing").toArray().after_back().set(meta);
        data[dvu].metadata("Wavelength").setDouble(target);
    }
}


SequenceName::Set CorrWavelength::requestedInputs()
{
    SequenceName::Set out;
    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        Util::merge(p->adjust->getAllUnits(), out);
        for (std::vector<Processing::WavelengthSet>::const_iterator t = p->targets.begin(),
                end = p->targets.end(); t != end; ++t) {
            Util::merge(t->operate->getAllUnits(), out);
        }
    }
    return out;
}

QSet<double> CorrWavelength::metadataBreaks(int id)
{
    Processing *p = &processing[id];
    QSet<double> result;
    Util::merge(p->adjust->getChangedPoints(), result);
    for (std::vector<Processing::WavelengthSet>::const_iterator t = p->targets.begin(),
            end = p->targets.end(); t != end; ++t) {
        Util::merge(t->operate->getChangedPoints(), result);
    }
    return result;
}

CorrWavelength::CorrWavelength(QDataStream &stream)
{
    stream >> defaultSmoother;
    stream >> defaultFallbackAngstrom;
    stream >> defaultFallbackAngstromWavelength;
    stream >> defaultFallbackAngstromDistance;
    stream >> targetWavelengths;
    stream >> filterSuffixes;
    stream >> createOutputs;
    stream >> restrictedInputs;

    quint32 n;
    stream >> n;
    for (int i = 0; i < (int) n; i++) {
        Processing p;
        stream >> p.adjust;

        quint32 m;
        stream >> m;
        for (int j = 0; j < (int) m; j++) {
            Processing::WavelengthSet wl;
            stream >> wl.operate;
            stream >> wl.wavelength;
            stream >> wl.overrideInputWavelength;
            p.targets.push_back(wl);
        }

        processing.push_back(p);
    }
}

void CorrWavelength::serialize(QDataStream &stream)
{
    stream << defaultSmoother;
    stream << defaultFallbackAngstrom;
    stream << defaultFallbackAngstromWavelength;
    stream << defaultFallbackAngstromDistance;
    stream << targetWavelengths;
    stream << filterSuffixes;
    stream << createOutputs;
    stream << restrictedInputs;

    quint32 n = (quint32) processing.size();
    stream << n;
    for (int i = 0; i < (int) n; i++) {
        stream << processing[i].adjust;

        quint32 m = (quint32) processing[i].targets.size();
        stream << m;
        for (int j = 0; j < (int) m; j++) {
            stream << processing[i].targets[j].operate;
            stream << processing[i].targets[j].wavelength;
            stream << processing[i].targets[j].overrideInputWavelength;
        }
    }
}


QString CorrWavelengthComponent::getBasicSerializationName() const
{ return QString::fromLatin1("corr_wavelength"); }

ComponentOptions CorrWavelengthComponent::getOptions()
{
    ComponentOptions options;

    options.add("correct",
                new DynamicSequenceSelectionOption(tr("adjust", "name"), tr("Variables to adjust"),
                                                   tr("These are the variables to apply the adjustment to.  They are "
                                                      "matched to the nearest wavelength in the target set."),
                                                   tr("All scatterings, extinctions, and absorptions")));

    options.add("suffix", new ComponentOptionInstrumentSuffixSet(tr("instruments", "name"),
                                                                 tr("Instrument suffixes to correct"),
                                                                 tr("These are the instrument suffixes to correct.  "
                                                                    "For example S11 would usually specifies the reference nephelometer.  "
                                                                    "This option is mutually exclusive with manual variable specification."),
                                                                 tr("All instrument suffixes")));
    options.exclude("correct", "suffix");
    options.exclude("suffix", "correct");

    options.add("create-outputs", new ComponentOptionBoolean(tr("create-outputs", "name"),
                                                             tr("Create output variables"),
                                                             tr("When enabled, this will cause the creation of outputs at the specified wavelengths, even if a matching one does not exist in the input."),
                                                             QString()));

    BaselineSmootherOption *smoothing =
            new BaselineSmootherOption(tr("smoothing", "name"), tr("Input smoothing"),
                                       tr("This is the smoothing applied to inputs before adjustment."),
                                       QString());
    smoothing->setStabilityDetection(false);
    smoothing->setSpikeDetection(false);
    smoothing->setDefault(new BaselineLatest);
    options.add("smoothing", smoothing);

    ComponentOptionDoubleSet *wl = new ComponentOptionDoubleSet(tr("wavelengths", "name"),
                                                                tr("Wavelengths data are adjusted to"),
                                                                tr("These are the wavelengths that the input data are adjusted to.  "
                                                                   "Input data are adjusted to the nearest matching wavelength, if "
                                                                   "there are more data than wavelengths the further ones are left "
                                                                   "un-adjusted."),
                                                                tr("450,550,700"));
    wl->setMinimum(0.0, false);
    options.add("wavelengths", wl);


    options.add("angstrom", new DynamicInputOption(tr("angstrom", "name"),
                                                   tr("Adjustment fallback \xC3\x85ngstr\xC3\xB6m exponent"),
                                                   tr("This is the \xC3\x85ngstr\xC3\xB6m exponent used to perform "
                                                          "wavelength adjustment when interpolation is not possible.  When "
                                                          "not set, data are simply left as undefined when they cannot be "
                                                          "adjusted to the target wavelengths."),
                                                   QString()));

    DynamicDoubleOption *angWL = new DynamicDoubleOption(tr("angstrom-wavelength", "name"),
                                                         tr("Adjustment fallback \xC3\x85ngstr\xC3\xB6m wavelength"),
                                                         tr("This is the wavelength of the \xC3\x85ngstr\xC3\xB6m exponent "
                                                                "used to perform wavelength adjustment when interpolation is "
                                                                "not possible.  This is used to limit how far data are "
                                                                "extrapolated when used in conjunction with angstrom-distance."),
                                                         QString());
    angWL->setMinimum(0.0, false);
    angWL->setAllowUndefined(true);
    options.add("angstrom-wavelength", angWL);

    DynamicDoubleOption *angDist = new DynamicDoubleOption(tr("angstrom-distance", "name"),
                                                           tr("Adjustment fallback \xC3\x85ngstr\xC3\xB6m distance"),
                                                           tr("This is the maximum distance that the fallback "
                                                                  "\xC3\x85ngstr\xC3\xB6m exponent is valid for.  This is used in "
                                                                  "conjunction with angstrom-wavelength to limit how far data are "
                                                                  "extrapolated."), QString());
    angDist->setMinimum(0.0, true);
    angDist->setAllowUndefined(true);
    options.add("angstrom-distance", angDist);

    return options;
}

QList<ComponentExample> CorrWavelengthComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Defaults", "default example name"),
                                     tr("This will correct light scattering, absorption and extinctions "
                                        "present in the input to 450, 550, and 700 nm, using whichever is "
                                        "nearest to the original wavelength.")));

    options = getOptions();
    (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")))->add("S11");
    (qobject_cast<ComponentOptionDoubleSet *>(options.get("wavelengths")))->set(
            QSet<double>() << 567);
    examples.append(ComponentExample(options, tr("Single instrument with manual wavelength"),
                                     tr("This will correct the nearest wavelength from S11 to 567 nm.")));

    return examples;
}

SegmentProcessingStage *CorrWavelengthComponent::createBasicFilterDynamic(const ComponentOptions &options)
{
    return new CorrWavelength(options);
}

SegmentProcessingStage *CorrWavelengthComponent::createBasicFilterPredefined(const ComponentOptions &options,
                                                                             double start,
                                                                             double end,
                                                                             const QList<
                                                                                     SequenceName> &inputs)
{
    return new CorrWavelength(options, start, end, inputs);
}

SegmentProcessingStage *CorrWavelengthComponent::createBasicFilterEditing(double start,
                                                                          double end,
                                                                          const SequenceName::Component &station,
                                                                          const SequenceName::Component &archive,
                                                                          const ValueSegment::Transfer &config)
{
    return new CorrWavelength(start, end, station, archive, config);
}

SegmentProcessingStage *CorrWavelengthComponent::deserializeBasicFilter(QDataStream &stream)
{
    return new CorrWavelength(stream);
}
