/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CORRWAVELENGTH_H
#define CORRWAVELENGTH_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QObject>
#include <QList>
#include <QSet>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "editing/wavelengthadjust.hxx"
#include "smoothing/baseline.hxx"

class CorrWavelength : public CPD3::Data::SegmentProcessingStage {
    class Processing {
    public:
        class WavelengthSet {
        public:
            CPD3::Data::DynamicSequenceSelection *operate;
            CPD3::Data::DynamicDouble *wavelength;
            double overrideInputWavelength;

            WavelengthSet() : operate(NULL), wavelength(),
                              overrideInputWavelength(CPD3::FP::undefined())
            { }
        };

        std::vector<WavelengthSet> targets;
        CPD3::Editing::WavelengthAdjust *adjust;

        Processing() : targets(), adjust()
        { }
    };

    CPD3::Smoothing::BaselineSmoother *defaultSmoother;
    CPD3::Data::DynamicInput *defaultFallbackAngstrom;
    CPD3::Data::DynamicDouble *defaultFallbackAngstromWavelength;
    CPD3::Data::DynamicDouble *defaultFallbackAngstromDistance;
    QSet<double> targetWavelengths;
    CPD3::Data::SequenceName::ComponentSet filterSuffixes;
    bool createOutputs;

    bool restrictedInputs;

    void handleOptions(const CPD3::ComponentOptions &options);

    std::vector<Processing> processing;

    CPD3::Data::SequenceName::Map<const Processing::WavelengthSet *>
            buildAdjustments(Processing *p, double start, double end) const;

public:
    CorrWavelength();

    CorrWavelength(const CPD3::ComponentOptions &options);

    CorrWavelength(const CPD3::ComponentOptions &options,
                   double start, double end, const QList<CPD3::Data::SequenceName> &inputs);

    CorrWavelength(double start,
                   double end,
                   const CPD3::Data::SequenceName::Component &station,
                   const CPD3::Data::SequenceName::Component &archive,
                   const CPD3::Data::ValueSegment::Transfer &config);

    virtual ~CorrWavelength();

    void unhandled(const CPD3::Data::SequenceName &unit,
                   CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control) override;

    void process(int id, CPD3::Data::SequenceSegment &data) override;

    void processMeta(int id, CPD3::Data::SequenceSegment &data) override;

    CPD3::Data::SequenceName::Set requestedInputs() override;

    QSet<double> metadataBreaks(int id) override;

    CorrWavelength(QDataStream &stream);

    void serialize(QDataStream &stream) override;
};

class CorrWavelengthComponent
        : public QObject, virtual public CPD3::Data::SegmentProcessingStageComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ProcessingStageComponent
                         CPD3::Data::SegmentProcessingStageComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.corr_wavelength"
                              FILE
                              "corr_wavelength.json")

public:
    virtual QString getBasicSerializationName() const;

    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual CPD3::Data::SegmentProcessingStage
            *createBasicFilterDynamic(const CPD3::ComponentOptions &options);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterPredefined
            (const CPD3::ComponentOptions &options,
             double start,
             double end, const QList<CPD3::Data::SequenceName> &inputs);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterEditing(double start,
                                                                         double end,
                                                                         const CPD3::Data::SequenceName::Component &station,
                                                                         const CPD3::Data::SequenceName::Component &archive,
                                                                         const CPD3::Data::ValueSegment::Transfer &config);

    virtual void extendBasicFilterEditing(double &start,
                                          double &end,
                                          const CPD3::Data::SequenceName::Component &station,
                                          const CPD3::Data::SequenceName::Component &archive,
                                          const CPD3::Data::ValueSegment::Transfer &config,
                                          CPD3::Data::Archive::Access *access);

    virtual CPD3::Data::SegmentProcessingStage *deserializeBasicFilter(QDataStream &stream);
};

#endif
