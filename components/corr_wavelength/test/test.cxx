/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/segmentprocessingstage.hxx"
#include "core/component.hxx"

#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "smoothing/baseline.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Smoothing;

namespace QTest {

template<>
char *toString(const QSet<double> &set)
{
    QByteArray ba = "QSet<double>(";
    for (QSet<double>::const_iterator i = set.constBegin(); i != set.constEnd(); ++i) {
        if (i != set.constBegin()) ba += ", ";
        ba += QByteArray::number(*i);
    }
    ba += ")";
    return qstrdup(ba.data());
}
}

class TestController : public SegmentProcessingStage::SequenceHandlerControl {
public:
    class ID {
    public:
        ID() : filter(), input()
        { }

        ID(const QSet<SequenceName> &setFilter, const QSet<SequenceName> &setInput) : filter(
                setFilter), input(setInput)
        { }

        bool operator==(const ID &other) const
        { return filter == other.filter && input == other.input; }

        QSet<SequenceName> filter;
        QSet<SequenceName> input;
    };

    QHash<int, ID> contents;

    virtual void filterUnit(const SequenceName &unit, int targetID)
    {
        contents[targetID].input.remove(unit);
        contents[targetID].filter.insert(unit);
    }

    virtual void inputUnit(const SequenceName &unit, int targetID)
    {
        if (contents[targetID].filter.contains(unit))
            return;
        contents[targetID].input.insert(unit);
    }

    virtual bool isHandled(const SequenceName &unit)
    {
        for (QHash<int, ID>::const_iterator it = contents.constBegin();
                it != contents.constEnd();
                ++it) {
            if (it.value().filter.contains(unit))
                return true;
            if (it.value().input.contains(unit))
                return true;
        }
        return false;
    }
};

class TestComponent : public QObject {
Q_OBJECT

    SegmentProcessingStageComponent *component;

private slots:

    void initTestCase()
    {
        component = qobject_cast<SegmentProcessingStageComponent *>(
                ComponentLoader::create("corr_wavelength"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("correct")));
        QVERIFY(qobject_cast<BaselineSmootherOption *>(options.get("smoothing")));
        QVERIFY(qobject_cast<ComponentOptionDoubleSet *>(options.get("wavelengths")));
        QVERIFY(qobject_cast<ComponentOptionBoolean *>(options.get("create-outputs")));
        QVERIFY(qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("angstrom")));
        QVERIFY(qobject_cast<DynamicDoubleOption *>(options.get("angstrom-distance")));
        QVERIFY(qobject_cast<DynamicDoubleOption *>(options.get("angstrom-wavelength")));

        QVERIFY(options.excluded().value("correct").contains("suffix"));
        QVERIFY(options.excluded().value("suffix").contains("correct"));
    }

    void dynamicDefaults()
    {
        ComponentOptions options(component->getOptions());
        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        SegmentProcessingStage *filter2;
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "Vx_S11q"), &controller);
        QVERIFY(controller.contents.isEmpty());
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                filter2 = component->deserializeBasicFilter(stream);
                QVERIFY(stream.atEnd());
            }
            QVERIFY(filter2 != NULL);
            filter2->unhandled(SequenceName("brw", "raw", "Vx_S11q"), &controller);
            QVERIFY(controller.contents.isEmpty());
            delete filter2;
        }

        SequenceName BsB_S11_1("brw", "raw", "BsB_S11");
        SequenceName BsG_S11_1("brw", "raw", "BsG_S11");
        SequenceName BsR_S11_1("brw", "raw", "BsR_S11");
        SequenceName BsB_S11_2("sgp", "raw", "BsB_S11");
        SequenceName BsG_S11_2("sgp", "raw", "BsG_S11");
        SequenceName BsR_S11_2("sgp", "raw", "BsR_S11");
        SequenceName BbsB_S11_1("brw", "raw", "BbsB_S12");
        SequenceName BbsR_S11_1("brw", "raw", "BbsR_S12");
        SequenceName BaG_A11_1("brw", "raw", "BaG_S11");
        SequenceName BaR_A11_1("brw", "raw", "BaR_S11");

        filter->unhandled(BsB_S11_1, &controller);
        filter->unhandled(BsG_S11_1, &controller);
        filter->unhandled(BsR_S11_1, &controller);
        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(
                                QSet<SequenceName>() << BsB_S11_1 << BsG_S11_1 << BsR_S11_1,
                                QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        int S11_1 = idL.at(0);

        filter->unhandled(BsB_S11_2, &controller);
        filter->unhandled(BsG_S11_2, &controller);
        filter->unhandled(BsR_S11_2, &controller);
        QCOMPARE(controller.contents.size(), 2);
        idL = controller.contents
                        .keys(TestController::ID(
                                QSet<SequenceName>() << BsB_S11_2 << BsG_S11_2 << BsR_S11_2,
                                QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        int S11_2 = idL.at(0);

        filter->unhandled(BbsB_S11_1, &controller);
        filter->unhandled(BbsR_S11_1, &controller);
        QCOMPARE(controller.contents.size(), 3);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << BbsB_S11_1 << BbsR_S11_1,
                                                 QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        int S11_3 = idL.at(0);

        filter->unhandled(BaG_A11_1, &controller);
        filter->unhandled(BaR_A11_1, &controller);
        QCOMPARE(controller.contents.size(), 4);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << BaG_A11_1 << BaR_A11_1,
                                                 QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        int A11_1 = idL.at(0);

        data.setStart(15.0);
        data.setEnd(16.0);

        {
            Variant::Root meta;
            meta.write().metadataReal("Wavelength").setDouble(400);
            data.setValue(BsB_S11_1.toMeta(), meta);
            meta.write().metadataReal("Wavelength").setDouble(550);
            data.setValue(BsG_S11_1.toMeta(), meta);
            meta.write().metadataReal("Wavelength").setDouble(650);
            data.setValue(BsR_S11_1.toMeta(), meta);
            filter->processMeta(S11_1, data);
            QCOMPARE(data.value(BsB_S11_1.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
            QCOMPARE(data.value(BsB_S11_1.toMeta()).metadata("Wavelength").toDouble(), 450.0);
        }

        data.setValue(BsB_S11_1, Variant::Root(10.0));
        data.setValue(BsG_S11_1, Variant::Root(8.0));
        data.setValue(BsR_S11_1, Variant::Root(6.0));
        filter->process(S11_1, data);
        QCOMPARE(data.value(BsB_S11_1).toDouble(), 9.20782257096547);
        QCOMPARE(data.value(BsG_S11_1).toDouble(), 8.0);
        QCOMPARE(data.value(BsR_S11_1).toDouble(), 5.28112379028539);

        {
            QByteArray sd;
            {
                QDataStream stream(&sd, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&sd, QIODevice::ReadOnly);
                filter2 = component->deserializeBasicFilter(stream);
            }
            QVERIFY(filter2 != NULL);
            data.setValue(BsB_S11_1, Variant::Root(10.0));
            data.setValue(BsG_S11_1, Variant::Root(8.0));
            data.setValue(BsR_S11_1, Variant::Root(6.0));
            filter2->process(S11_1, data);
            QCOMPARE(data.value(BsB_S11_1).toDouble(), 9.20782257096547);
            QCOMPARE(data.value(BsG_S11_1).toDouble(), 8.0);
            QCOMPARE(data.value(BsR_S11_1).toDouble(), 5.28112379028539);
            delete filter2;
        }

        data.setValue(BsB_S11_1, Variant::Root(10.0));
        data.setValue(BsG_S11_1, Variant::Root(8.0));
        data.setValue(BsR_S11_1, Variant::Root());
        filter->process(S11_1, data);
        QCOMPARE(data.value(BsB_S11_1).toDouble(), 9.20782257096547);
        QCOMPARE(data.value(BsG_S11_1).toDouble(), 8.0);
        QVERIFY(!FP::defined(data.value(BsR_S11_1).toDouble()));

        {
            Variant::Root meta;
            meta.write().metadataReal("Wavelength").setDouble(450);
            data.setValue(BsB_S11_2.toMeta(), meta);
            meta.write().metadataReal("Wavelength").setDouble(500);
            data.setValue(BsG_S11_2.toMeta(), meta);
            meta.write().metadataReal("Wavelength").setDouble(750);
            data.setValue(BsR_S11_2.toMeta(), meta);
            filter->processMeta(S11_2, data);
            QCOMPARE(data.value(BsB_S11_2.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
            QCOMPARE(data.value(BsB_S11_2.toMeta()).metadata("Wavelength").toDouble(), 450.0);
            QCOMPARE(data.value(BsG_S11_2.toMeta()).metadata("Wavelength").toDouble(), 550.0);
        }

        data.setValue(BsB_S11_2, Variant::Root(11.0));
        data.setValue(BsG_S11_2, Variant::Root(10.0));
        data.setValue(BsR_S11_2, Variant::Root(9.0));
        filter->process(S11_2, data);
        QCOMPARE(data.value(BsB_S11_2).toDouble(), 11.0);
        QCOMPARE(data.value(BsG_S11_2).toDouble(), 9.75537726193752);
        QCOMPARE(data.value(BsR_S11_2).toDouble(), 9.16280582266458);

        {
            Variant::Root meta;
            meta.write().metadataReal("Wavelength").setDouble(400);
            data.setValue(BbsB_S11_1.toMeta(), meta);
            meta.write().metadataReal("Wavelength").setDouble(800);
            data.setValue(BbsR_S11_1.toMeta(), meta);
            filter->processMeta(S11_3, data);
            QCOMPARE(data.value(BbsB_S11_1.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
            QCOMPARE(data.value(BbsB_S11_1.toMeta()).metadata("Wavelength").toDouble(), 450.0);
            QCOMPARE(data.value(BbsR_S11_1.toMeta()).metadata("Wavelength").toDouble(), 700.0);

            meta.write().metadataReal("Wavelength").setDouble(520);
            data.setValue(BaG_A11_1.toMeta(), meta);
            meta.write().metadataReal("Wavelength").setDouble(720);
            data.setValue(BaR_A11_1.toMeta(), meta);
            filter->processMeta(A11_1, data);
            QCOMPARE(data.value(BaG_A11_1.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
            QCOMPARE(data.value(BaG_A11_1.toMeta()).metadata("Wavelength").toDouble(), 550.0);
            QCOMPARE(data.value(BaR_A11_1.toMeta()).metadata("Wavelength").toDouble(), 700.0);
        }

        data.setValue(BbsB_S11_1, Variant::Root(6.0));
        data.setValue(BbsR_S11_1, Variant::Root(5.0));
        filter->process(S11_3, data);
        QCOMPARE(data.value(BbsB_S11_1).toDouble(), 5.81696401323065);
        QCOMPARE(data.value(BbsR_S11_1).toDouble(), 5.17873730453664);

        data.setValue(BaG_A11_1, Variant::Root(4.0));
        data.setValue(BaR_A11_1, Variant::Root(3.0));
        filter->process(A11_1, data);
        QCOMPARE(data.value(BaG_A11_1).toDouble(), 3.80649865403501);
        QCOMPARE(data.value(BaR_A11_1).toDouble(), 3.07564948774975);


        delete filter;
    }

    void dynamicOptions()
    {
        ComponentOptions options(component->getOptions());

        qobject_cast<ComponentOptionDoubleSet *>(options.get("wavelengths"))->set(
                QSet<double>() << 450 << 700);
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("correct"))->set("", "", "I.*");

        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "BsB_S11"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BbsG_S11"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BaG_A11"), &controller);
        QVERIFY(controller.contents.isEmpty());

        SequenceName I1_S11("brw", "raw", "I1_S11");
        SequenceName I2_S11("brw", "raw", "I2_S11");
        SequenceName I3_S12("brw", "raw", "I3_S12");
        filter->unhandled(I1_S11, &controller);
        filter->unhandled(I2_S11, &controller);
        filter->unhandled(I3_S12, &controller);

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << I1_S11 << I2_S11 << I3_S12,
                                                 QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        int id = idL.at(0);

        {
            Variant::Root meta;
            meta.write().metadataReal("Wavelength").setDouble(400);
            data.setValue(I1_S11.toMeta(), meta);
            meta.write().metadataReal("Wavelength").setDouble(520);
            data.setValue(I2_S11.toMeta(), meta);
            meta.write().metadataReal("Wavelength").setDouble(675);
            data.setValue(I3_S12.toMeta(), meta);
            filter->processMeta(id, data);

            QCOMPARE(data.value(I1_S11.toMeta()).metadata("Wavelength").toDouble(), 450.0);
            QCOMPARE(data.value(I2_S11.toMeta()).metadata("Wavelength").toDouble(), 520.0);
            QCOMPARE(data.value(I3_S12.toMeta()).metadata("Wavelength").toDouble(), 700.0);
        }

        data.setValue(I1_S11, Variant::Root(10.0));
        data.setValue(I2_S11, Variant::Root(6.0));
        data.setValue(I3_S12, Variant::Root(2.0));
        filter->process(id, data);
        QCOMPARE(data.value(I1_S11).toDouble(), 7.95070373408984);
        QCOMPARE(data.value(I2_S11).toDouble(), 6.0);
        QCOMPARE(data.value(I3_S12).toDouble(), 1.71600482160913);

        delete filter;


        options = component->getOptions();
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->add("S11");
        filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        controller.contents.clear();

        filter->unhandled(SequenceName("brw", "raw", "BsB_S12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BsG_S12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BbsG_S12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BaG_S12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BeR_S12"), &controller);
        QVERIFY(controller.contents.isEmpty());

        SequenceName BsG_S11("brw", "raw", "BsG_S11");
        SequenceName BsR_S11("brw", "raw", "BsR_S11");
        filter->unhandled(BsG_S11, &controller);
        filter->unhandled(BsR_S11, &controller);
        filter->unhandled(SequenceName("brw", "raw", "BsG_S12"), &controller);

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << BsG_S11 << BsR_S11,
                                                 QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        id = idL.at(0);

        delete filter;


        options = component->getOptions();
        qobject_cast<ComponentOptionBoolean *>(options.get("create-outputs"))->set(true);
        qobject_cast<DynamicInputOption *>(options.get("angstrom"))->set(1.0);
        qobject_cast<ComponentOptionDoubleSet *>(options.get("wavelengths"))->set(
                QSet < double > () << 550);
        filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        controller.contents.clear();

        filter->unhandled(BsR_S11, &controller);
        filter->unhandled(BsG_S11, &controller);

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(QSet < SequenceName > () << BsG_S11,
                                                 QSet < SequenceName > () << BsR_S11));
        QCOMPARE(idL.size(), 1);
        id = idL.at(0);

        {
            Variant::Root meta;
            meta.write().metadataReal("Wavelength").setDouble(500);
            data.setValue(BsG_S11.toMeta(), meta);
            meta.write().metadataReal("Wavelength").setDouble(750);
            data.setValue(BsR_S11.toMeta(), meta);
            filter->processMeta(id, data);

            QCOMPARE(data.value(BsG_S11.toMeta()).metadata("Wavelength").toDouble(), 550.0);
            QCOMPARE(data.value(BsR_S11.toMeta()).metadata("Wavelength").toDouble(), 750.0);
        }

        data.setValue(BsG_S11, Variant::Root(10.0));
        data.setValue(BsR_S11, Variant::Root(9.0));
        filter->process(id, data);
        QCOMPARE(data.value(BsG_S11).toDouble(), 9.75537726193752);
        QCOMPARE(data.value(BsR_S11).toDouble(), 9.0);

        SequenceName BsR_S12("brw", "raw", "BsR_S12");
        SequenceName BsG_S12("brw", "raw", "BsG_S12");
        filter->unhandled(BsR_S12, &controller);

        QCOMPARE(controller.contents.size(), 2);
        idL = controller.contents
                        .keys(TestController::ID(QSet < SequenceName > () << BsG_S12,
                                                 QSet < SequenceName > () << BsR_S12));
        QCOMPARE(idL.size(), 1);
        id = idL.at(0);

        {
            Variant::Root meta;
            meta.write().metadataReal("Wavelength").setDouble(660);
            data.setValue(BsR_S12.toMeta(), meta);
            filter->processMeta(id, data);

            QCOMPARE(data.value(BsG_S12.toMeta()).metadata("Wavelength").toDouble(), 550.0);
            QCOMPARE(data.value(BsR_S12.toMeta()).metadata("Wavelength").toDouble(), 660.0);
        }

        data.setValue(BsR_S12, Variant::Root(10.0));
        filter->process(id, data);
        QCOMPARE(data.value(BsG_S12).toDouble(), 12.0);
        QCOMPARE(data.value(BsR_S12).toDouble(), 10.0);

        delete filter;
    }

    void predefined()
    {
        ComponentOptions options(component->getOptions());

        SequenceName BsG_S11("brw", "raw", "BsG_S11");
        SequenceName BsR_S11("brw", "raw", "BsG_S11");
        QList<SequenceName> input;
        input << BsG_S11 << BsR_S11;

        SegmentProcessingStage *filter =
                component->createBasicFilterPredefined(options, FP::undefined(), FP::undefined(),
                                                       input);
        QVERIFY(filter != NULL);
        TestController controller;
        QList<int> idL;

        QCOMPARE(filter->requestedInputs(), (SequenceName::Set{BsG_S11, BsR_S11}));

        delete filter;
    }

    void editing()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["S11/Optical/Blue/Wavelength"] = 450.0;
        cv["S11/Optical/Blue/Data"] = "::BsB_S11:=";
        cv["S11/Optical/Green/Wavelength"] = 550.0;
        cv["S11/Optical/Green/Data"] = "::BsG_S11:=";
        cv["S11/Adjust/Input"] = "::Bs[BGR]_S11:=";
        cv["S12/Optical/Blue/Wavelength"] = 430.0;
        cv["S12/Optical/Blue/Data"] = "::BsB_S12:=";
        cv["S12/Optical/Green/Wavelength"] = 550.0;
        cv["S12/Optical/Green/Data"] = "::BsG_S12:=";
        cv["S12/Adjust/Input"] = "::Bs[BGR]_S12:=";
        config.emplace_back(FP::undefined(), 13.0, cv);
        cv.write().setEmpty();
        cv["S11/Optical/Blue/Wavelength"] = 460.0;
        cv["S11/Optical/Blue/Data"] = "::BsB_S11:=";
        cv["S11/Adjust/Input"] = "::Bs[BGR]_S11:=";
        config.emplace_back(13.0, FP::undefined(), cv);

        SegmentProcessingStage
                *filter = component->createBasicFilterEditing(12.0, 15.0, "brw", "raw", config);
        QVERIFY(filter != NULL);
        TestController controller;

        SequenceName BsB_S11("brw", "raw", "BsB_S11");
        SequenceName BsG_S11("brw", "raw", "BsG_S11");
        SequenceName BsR_S11("brw", "raw", "BsR_S11");
        SequenceName BsB_S12("brw", "raw", "BsB_S12");
        SequenceName BsG_S12("brw", "raw", "BsG_S12");

        QCOMPARE(filter->requestedInputs(),
                 (SequenceName::Set{BsB_S11, BsG_S11, BsB_S12, BsG_S12}));

        filter->unhandled(BsB_S11, &controller);
        filter->unhandled(BsG_S11, &controller);
        filter->unhandled(BsR_S11, &controller);
        filter->unhandled(BsB_S12, &controller);
        filter->unhandled(BsG_S12, &controller);

        QList<int> idL = controller.contents
                                   .keys(TestController::ID(
                                           QSet<SequenceName>() << BsB_S11 << BsG_S11,
                                           QSet<SequenceName>() << BsR_S11));
        QCOMPARE(idL.size(), 1);
        int S11 = idL.at(0);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << BsB_S12 << BsG_S12,
                                                 QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        int S12 = idL.at(0);

        QCOMPARE(filter->metadataBreaks(S11), QSet<double>() << 13.0);
        QCOMPARE(filter->metadataBreaks(S12), QSet<double>() << 13.0);

        SequenceSegment data1;
        data1.setStart(12.0);
        data1.setEnd(13.0);

        {
            Variant::Root meta;
            meta.write().metadataReal("Wavelength").setDouble(400);
            data1.setValue(BsB_S11.toMeta(), meta);
            data1.setValue(BsB_S12.toMeta(), meta);
            meta.write().metadataReal("Wavelength").setDouble(580);
            data1.setValue(BsG_S11.toMeta(), meta);
            data1.setValue(BsG_S12.toMeta(), meta);
            meta.write().metadataReal("Wavelength").setDouble(700);
            data1.setValue(BsR_S11.toMeta(), meta);

            filter->processMeta(S11, data1);
            filter->processMeta(S12, data1);

            QCOMPARE(data1.value(BsB_S11.toMeta()).metadata("Wavelength").toDouble(), 450.0);
            QCOMPARE(data1.value(BsB_S12.toMeta()).metadata("Wavelength").toDouble(), 430.0);
            QCOMPARE(data1.value(BsG_S11.toMeta()).metadata("Wavelength").toDouble(), 550.0);
            QCOMPARE(data1.value(BsG_S12.toMeta()).metadata("Wavelength").toDouble(), 550.0);
            QCOMPARE(data1.value(BsR_S11.toMeta()).metadata("Wavelength").toDouble(), 700.0);

            QCOMPARE(data1.value(BsB_S11.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
            QCOMPARE(data1.value(BsG_S11.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
            QCOMPARE(data1.value(BsB_S12.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
            QVERIFY(!data1.value(BsR_S11.toMeta()).metadata("Processing").exists());
        }

        data1.setValue(BsB_S11, Variant::Root(10.0));
        data1.setValue(BsG_S11, Variant::Root(8.0));
        data1.setValue(BsR_S11, Variant::Root(6.0));
        filter->process(S11, data1);
        QCOMPARE(data1.value(BsB_S11).toDouble(), 9.31708820671747);
        QCOMPARE(data1.value(BsG_S11).toDouble(), 8.25927489381246);
        QCOMPARE(data1.value(BsR_S11).toDouble(), 6.0);

        data1.setValue(BsB_S12, Variant::Root(11.0));
        data1.setValue(BsG_S12, Variant::Root(7.0));
        filter->process(S12, data1);
        QCOMPARE(data1.value(BsB_S12).toDouble(), 10.0736334294682);
        QCOMPARE(data1.value(BsG_S12).toDouble(), 7.46716280551913);


        SequenceSegment data2;
        data2.setStart(13.0);
        data2.setEnd(15.0);

        {
            Variant::Root meta;
            meta.write().metadataReal("Wavelength").setDouble(400);
            data2.setValue(BsB_S11.toMeta(), meta);
            data2.setValue(BsB_S12.toMeta(), meta);
            meta.write().metadataReal("Wavelength").setDouble(580);
            data2.setValue(BsG_S11.toMeta(), meta);
            data2.setValue(BsG_S12.toMeta(), meta);
            meta.write().metadataReal("Wavelength").setDouble(700);
            data2.setValue(BsR_S11.toMeta(), meta);

            filter->processMeta(S11, data2);
            filter->processMeta(S12, data2);

            QCOMPARE(data2.value(BsB_S11.toMeta()).metadata("Wavelength").toDouble(), 460.0);
            QCOMPARE(data2.value(BsB_S12.toMeta()).metadata("Wavelength").toDouble(), 400.0);
            QCOMPARE(data2.value(BsG_S11.toMeta()).metadata("Wavelength").toDouble(), 580.0);
            QCOMPARE(data2.value(BsG_S12.toMeta()).metadata("Wavelength").toDouble(), 580.0);
            QCOMPARE(data2.value(BsR_S11.toMeta()).metadata("Wavelength").toDouble(), 700.0);

            QCOMPARE(data2.value(BsB_S11.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
            QVERIFY(!data2.value(BsG_S11.toMeta()).metadata("Processing").exists());
            QVERIFY(!data2.value(BsR_S11.toMeta()).metadata("Processing").exists());
            QVERIFY(!data2.value(BsB_S12.toMeta()).metadata("Processing").exists());
        }

        data2.setValue(BsB_S11, Variant::Root(9.0));
        data2.setValue(BsG_S11, Variant::Root(7.0));
        data2.setValue(BsR_S11, Variant::Root(5.0));
        filter->process(S11, data2);
        QCOMPARE(data2.value(BsB_S11).toDouble(), 8.18819757868552);
        QCOMPARE(data2.value(BsG_S11).toDouble(), 7.0);
        QCOMPARE(data2.value(BsR_S11).toDouble(), 5.0);

        data2.setValue(BsB_S12, Variant::Root(3.0));
        data2.setValue(BsG_S12, Variant::Root(4.0));
        filter->process(S12, data2);
        QCOMPARE(data2.value(BsB_S12).toDouble(), 3.0);
        QCOMPARE(data2.value(BsG_S12).toDouble(), 4.0);

        delete filter;
    }

    void fixedAngstrom()
    {
        ComponentOptions options(component->getOptions());
        qobject_cast<DynamicInputOption *>(options.get("angstrom"))->set(1.1);
        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        SegmentProcessingStage *filter2;
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        SequenceName BsB_S11_1("brw", "raw", "BsB_S11");

        filter->unhandled(BsB_S11_1, &controller);
        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << BsB_S11_1,
                                                 QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        int S11_1 = idL.at(0);

        data.setStart(15.0);
        data.setEnd(16.0);

        {
            Variant::Root meta;
            meta.write().metadataReal("Wavelength").setDouble(400);
            data.setValue(BsB_S11_1.toMeta(), meta);
            filter->processMeta(S11_1, data);
            QCOMPARE(data.value(BsB_S11_1.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
            QCOMPARE(data.value(BsB_S11_1.toMeta()).metadata("Wavelength").toDouble(), 450.0);
        }

        data.setValue(BsB_S11_1, Variant::Root(10.0));
        filter->process(S11_1, data);
        QCOMPARE(data.value(BsB_S11_1).toDouble(), 8.78480701440783);

        {
            QByteArray sd;
            {
                QDataStream stream(&sd, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&sd, QIODevice::ReadOnly);
                filter2 = component->deserializeBasicFilter(stream);
            }
            QVERIFY(filter2 != NULL);
            data.setValue(BsB_S11_1, Variant::Root(10.0));
            filter2->process(S11_1, data);
            QCOMPARE(data.value(BsB_S11_1).toDouble(), 8.78480701440783);
            delete filter2;
        }

        delete filter;
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
