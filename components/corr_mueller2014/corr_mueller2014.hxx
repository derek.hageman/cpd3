/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CORRMUELLER2014_H
#define CORRMUELLER2014_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QObject>
#include <QList>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "editing/wavelengthadjust.hxx"

#include "solver.hxx"

class CorrMueller2014 : public CPD3::Data::SegmentProcessingStage {
    typedef CPD3::Data::DynamicPrimitive<CTSSolver::BlackParameterization> TimeInputBlack;
    typedef CPD3::Data::DynamicPrimitive<CTSSolver::WhiteParameterization> TimeInputWhite;

    class CorrectionSet {
    public:
        CPD3::Data::DynamicSequenceSelection *operateAbsorption;

        CPD3::Data::DynamicInput *transmittanceStart;
        CPD3::Data::DynamicInput *transmittanceEnd;
        CPD3::Data::DynamicInput *lengthStart;
        CPD3::Data::DynamicInput *lengthEnd;
        CPD3::Data::DynamicInput *sodStart;
        CPD3::Data::DynamicInput *sodEnd;
        CPD3::Data::DynamicInput *aodStart;
        CPD3::Data::DynamicInput *aodEnd;
        CPD3::Data::DynamicInput *asymmetryStart;
        CPD3::Data::DynamicInput *asymmetryEnd;
        CPD3::Data::DynamicInput *deltaLength;

        CPD3::Data::DynamicInput *inputDeltaSF;
        CPD3::Data::DynamicInput *inputDeltaAF;
        CPD3::Data::DynamicInput *inputGF;
        CPD3::Data::DynamicInput *inputEtaF;

        TimeInputBlack *inputBlack;
        TimeInputWhite *inputWhite;

        CTSSolver solver;

        CorrectionSet() : operateAbsorption(NULL),
                          transmittanceStart(NULL),
                          transmittanceEnd(NULL),
                          lengthStart(NULL),
                          lengthEnd(NULL),
                          sodStart(NULL),
                          sodEnd(NULL),
                          aodStart(NULL),
                          aodEnd(NULL),
                          asymmetryStart(NULL),
                          asymmetryEnd(NULL),
                          deltaLength(NULL),
                          inputDeltaSF(NULL),
                          inputDeltaAF(NULL),
                          inputGF(NULL),
                          inputEtaF(NULL),
                          inputBlack(NULL),
                          inputWhite(NULL)
        { }

        double calculate
                (double deltaA, double deltaS, double g, const CTSSolver::Parameters &parameters);

        double calculateIr
                (double Ir, double deltaS, double g, const CTSSolver::Parameters &parameters);
    };

    class Processing {
    public:
        std::vector<CorrectionSet> operations;

        CPD3::Data::DynamicSequenceSelection *operateFlags;

        CPD3::Editing::WavelengthTracker absorptionWavelength;

        CPD3::Data::SequenceName suffix;

        Processing() : operations(), operateFlags(NULL), absorptionWavelength(), suffix()
        { }
    };

    CPD3::Data::DynamicInput *defaultTransmittanceStart;
    CPD3::Data::DynamicInput *defaultTransmittanceEnd;
    CPD3::Data::DynamicInput *defaultLengthStart;
    CPD3::Data::DynamicInput *defaultLengthEnd;
    CPD3::Data::DynamicInput *defaultSODStart;
    CPD3::Data::DynamicInput *defaultSODEnd;
    CPD3::Data::DynamicInput *defaultAODStart;
    CPD3::Data::DynamicInput *defaultAODEnd;
    CPD3::Data::DynamicInput *defaultAsymmetryStart;
    CPD3::Data::DynamicInput *defaultAsymmetryEnd;
    CPD3::Data::DynamicInput *defaultDeltaLength;
    CPD3::Data::DynamicInput *defaultDeltaSF;
    CPD3::Data::DynamicInput *defaultDeltaAF;
    CPD3::Data::DynamicInput *defaultGF;
    CPD3::Data::DynamicInput *defaultEtaF;
    CPD3::Data::SequenceName::ComponentSet filterSuffixes;

    bool restrictedInputs;

    void handleOptions(const CPD3::ComponentOptions &options);

    std::vector<Processing> processing;

public:
    CorrMueller2014();

    CorrMueller2014(const CPD3::ComponentOptions &options);

    CorrMueller2014(const CPD3::ComponentOptions &options,
                    double start, double end, const QList<CPD3::Data::SequenceName> &inputs);

    CorrMueller2014(double start,
                    double end,
                    const CPD3::Data::SequenceName::Component &station,
                    const CPD3::Data::SequenceName::Component &archive,
                    const CPD3::Data::ValueSegment::Transfer &config);

    virtual ~CorrMueller2014();

    void unhandled(const CPD3::Data::SequenceName &unit,
                   CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control) override;

    void process(int id, CPD3::Data::SequenceSegment &data) override;

    void processMeta(int id, CPD3::Data::SequenceSegment &data) override;

    CPD3::Data::SequenceName::Set requestedInputs() override;

    QSet<double> metadataBreaks(int id) override;

    CorrMueller2014(QDataStream &stream);

    void serialize(QDataStream &stream) override;
};

class CorrMueller2014Component
        : public QObject, virtual public CPD3::Data::SegmentProcessingStageComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ProcessingStageComponent
                         CPD3::Data::SegmentProcessingStageComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.corr_mueller2014"
                              FILE
                              "corr_mueller2014.json")

public:
    virtual QString getBasicSerializationName() const;

    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual CPD3::Data::SegmentProcessingStage
            *createBasicFilterDynamic(const CPD3::ComponentOptions &options);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterPredefined
            (const CPD3::ComponentOptions &options,
             double start,
             double end, const QList<CPD3::Data::SequenceName> &inputs);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterEditing(double start,
                                                                         double end,
                                                                         const CPD3::Data::SequenceName::Component &station,
                                                                         const CPD3::Data::SequenceName::Component &archive,
                                                                         const CPD3::Data::ValueSegment::Transfer &config);

    virtual CPD3::Data::SegmentProcessingStage *deserializeBasicFilter(QDataStream &stream);
};

#endif
