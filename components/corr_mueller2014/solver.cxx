/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/number.hxx"

#include "solver.hxx"

using namespace CPD3;


typedef std::pair<double, double> double2;

#define MU1 0.57735

/* t, r */
static inline double2 stream1L(double da, double ds, double g, double daf, double dsf, double gf)
{
    double de = ds + da + dsf + daf;
    if (de == 0.0) return double2(FP::undefined(), FP::undefined());
    double ssa = (ds + dsf) / de;

    double gc;
    if (ds <= 0.0) gc = gf; else gc = (fabs(g * ds) + dsf * gf) / (fabs(ds) + dsf);

    double ssaAS = (1.0 - ssa) * (1.0 - ssa * gc);

    if (ssaAS > 0.0) {
        double K = sqrt(ssaAS);
        double v = K * de / MU1;
        double ep = exp(v);
        double en = 1.0 / ep;
        double sinhp = (ep - en) * 0.5;
        double coshp2 = (ep + en);
        double ssa1g = ssa * (1.0 + gc);
        double sinhpK = sinhp / K;
        double ssa1g2 = 2.0 - ssa1g;

        double div = ssa1g2 * sinhpK + coshp2;
        if (div == 0.0) return double2(FP::undefined(), FP::undefined());
        return double2(2.0 / div, (ssa * (1.0 - gc) * sinhpK) / div);
    } else if (ssaAS == 0.0) {
        double ssade = ssa * de;
        double ssadeg = ssade * gc;
        double ssademdeg = ssade - ssadeg;
        double div = 2.0 * de - ssademdeg + 2.0 * MU1;
        if (div == 0.0) return double2(FP::undefined(), FP::undefined());
        return double2(2.0 * MU1 / div, ssademdeg / div);
    } else {
        double x = fabs(ssaAS);
        double sqrtx = sqrt(x);
        double ssa1g2 = 2.0 - ssa * (1.0 + gc);
        double xdemu1 = sqrtx * de / MU1;
        double sinp = sin(xdemu1);
        double cosp2 = 2.0 * cos(xdemu1);
        double sinpsx = sinp / sqrtx;
        double div = ssa1g2 * sinpsx + cosp2;
        if (div == 0.0) return double2(FP::undefined(), FP::undefined());
        double2 r;
        r.first = 2.0 / div;
        div = ssa1g2 * sinp / x + cosp2;
        if (div == 0.0) return double2(FP::undefined(), FP::undefined());
        r.second = (ssa * (1.0 - gc) * sinpsx) / div;
        return r;
    }
}

static inline double stream2L(const CTSSolver::Parameters &parameters,
                              const CTSSolver::State &state,
                              double da,
                              double ds,
                              double g)
{
    double2 r = stream1L(da, ds, g, state.deltaAFetaF, state.deltaSFetaF, parameters.gF);
    if (!FP::defined(r.first) || !FP::defined(r.second))
        return FP::undefined();

    double ir12 = 1.0 - r.second * state.cR2;
    if (ir12 == 0.0) return FP::undefined();

    return r.first * state.cT2 / ir12;
}

static inline double df2S(const CTSSolver::Parameters &parameters,
                          const CTSSolver::State &state,
                          double da,
                          double ds,
                          double g)
{
    double t1 = stream2L(parameters, state, da, ds, g);
    if (!FP::defined(t1))
        return t1;

    double lp = t1 / state.TZero;
    if (lp <= 0.0) return FP::undefined();
    return -log(lp);
}

static inline double F_f(const CTSSolver::Parameters &parameters,
                         const CTSSolver::State &state,
                         double da,
                         double ds,
                         double g)
{
    double d1 = df2S(parameters, state, 0, ds, g);
    if (!FP::defined(d1))
        return FP::undefined();

    double d2 = df2S(parameters, state, da, 0, g);
    if (!FP::defined(d2))
        return FP::undefined();

    d1 += d2;
    if (d1 == 0) FP::undefined();

    d2 = df2S(parameters, state, da, ds, g);
    if (!FP::defined(d2))
        return FP::undefined();

    return d2 / d1;
}

#define B0  0.1509
#define B1  -0.1611
#define B2  4.5414
#define B3  -5.7062
#define B4  -1.9031
#define B5  0.01

static inline double F_s_NOAA(double ds, double g)
{
    if (ds <= 0.0)
        return FP::undefined();

    double div = B3 + B4 * g;
    if (div == 0.0) return FP::undefined();

    double inner = (log(ds) + B2) / div;

    return B5 + (B0 + B1 * g) * exp(-(inner * inner * inner * inner));
}

#define K1  0.02
#define K2  1.22
#define K3  0.85
#define C1  1.0796
#define C2  0.71
#define LOGC1C2 0.581992131194 /* log(C1+C2): log(constant) doesn't get folded */

static inline double F_a_Bond1999(double da)
{
    if (da <= 0.0)
        return (K2 / K3) * (C1 + C2);

    double inner = (exp(C2 * (K2 / K3 * da + K1 / K3 * 0.0) + LOGC1C2) - C1) / C2;
    if (inner <= 0.0) return FP::undefined();

    return log(inner) / da;
}

static inline double F_a_Virkkula2005(const CTSSolver::Parameters &parameters,
                                      const CTSSolver::State &state,
                                      double da)
{
    if (da <= 0.0)
        return 1.0 / parameters.black.p.virkkula2005.c1;
    return (sqrt(da * 2.0 / parameters.black.p.virkkula2005.c2 + state.black.virkkula2005.c1c2sq) -
            state.black.virkkula2005.c1c2) / da;
}

static inline bool prepareState(const CTSSolver::Parameters &parameters,
                                CTSSolver::State &state,
                                double ds,
                                double g)
{
    switch (parameters.white.type) {
    case CTSSolver::WhiteParameterization::NOAA:
    default:
        state.F_sdS = F_s_NOAA(ds, g);
        break;
    }
    if (!FP::defined(state.F_sdS))
        return false;

    state.F_sdS *= ds;
    return true;
}

static inline double deltaF(const CTSSolver::Parameters &parameters,
                            const CTSSolver::State &state,
                            double da,
                            double ds,
                            double g)
{
    double fa;
    switch (parameters.black.type) {
    case CTSSolver::BlackParameterization::Virkkula2005:
        fa = F_a_Virkkula2005(parameters, state, da);
        break;
    case CTSSolver::BlackParameterization::Bond1999:
    default:
        fa = F_a_Bond1999(da);
        break;
    }
    if (!FP::defined(fa)) return FP::undefined();

    double ff = F_f(parameters, state, da, ds, g);
    if (!FP::defined(ff)) return FP::undefined();

    return (fa * da + state.F_sdS) * ff;
}


CTSSolver::SolverInternal::SolverInternal(const CTSSolver *s, double indS, double ing) : solver(s),
                                                                                         dS(indS),
                                                                                         g(ing)
{ }

double CTSSolver::SolverInternal::initial(double yTarget) const
{
    Q_UNUSED(yTarget);
    return solver->result;
}

double CTSSolver::SolverInternal::firstStep(double dA) const
{
    return dA + 0.01;
}

double CTSSolver::SolverInternal::evaluate(double dA) const
{
    return deltaF(solver->parameters, solver->state, dA, dS, g);
}

bool CTSSolver::SolverInternal::completed(double ddA) const
{
    return fabs(ddA) < 1E-7;
}


void CTSSolver::initialize()
{
    state.isValid = false;

    switch (parameters.black.type) {
    case CTSSolver::BlackParameterization::Bond1999:
        break;
    case CTSSolver::BlackParameterization::Virkkula2005:
        if (!FP::defined(parameters.black.p.virkkula2005.c1) ||
                parameters.black.p.virkkula2005.c1 <= 0.0)
            return;
        if (!FP::defined(parameters.black.p.virkkula2005.c2) ||
                parameters.black.p.virkkula2005.c2 <= 0.0)
            return;
        state.black.virkkula2005.c1c2 =
                parameters.black.p.virkkula2005.c1 / parameters.black.p.virkkula2005.c2;
        state.black.virkkula2005.c1c2sq =
                state.black.virkkula2005.c1c2 * state.black.virkkula2005.c1c2;
        break;
    }

    state.deltaSFetaF = parameters.deltaSF * parameters.etaF;
    state.deltaAFetaF = parameters.deltaAF * parameters.etaF;

    double etaF1 = 1.0 - parameters.etaF;
    double2 r = stream1L(0.0, 0.0, 0.0, parameters.deltaAF * etaF1, parameters.deltaSF * etaF1,
                         parameters.gF);
    state.cT2 = r.first;
    state.cR2 = r.second;
    if (!FP::defined(state.cT2) || !FP::defined(state.cT2))
        return;

    state.TZero = stream2L(parameters, state, 0, 0, 0);
    if (!FP::defined(state.TZero))
        return;

    state.isValid = true;
}

void CTSSolver::reset()
{
    result = 0.0;
}


bool CTSSolver::Parameters::operator==(const Parameters &other) const
{
    if (deltaSF != other.deltaSF)
        return false;
    if (deltaAF != other.deltaAF)
        return false;
    if (gF != other.gF)
        return false;
    if (etaF != other.etaF)
        return false;

    if (black != other.black)
        return false;
    if (white != other.white)
        return false;

    return true;
}

bool CTSSolver::Parameters::operator!=(const Parameters &other) const
{
    if (deltaSF != other.deltaSF)
        return true;
    if (deltaAF != other.deltaAF)
        return true;
    if (gF != other.gF)
        return true;
    if (etaF != other.etaF)
        return true;

    if (black != other.black)
        return true;
    if (white != other.white)
        return true;

    return false;
}

bool CTSSolver::Parameters::valid() const
{
    if (!FP::defined(deltaSF) || deltaSF < 0.0)
        return false;
    if (!FP::defined(deltaAF) || deltaAF < 0.0)
        return false;
    if (!FP::defined(gF) || gF < 0.0)
        return false;
    if (!FP::defined(etaF) || etaF < 0.0 || etaF > 1.0)
        return false;

    if (!black.valid())
        return false;
    if (!white.valid())
        return false;

    return true;
}

CTSSolver::State::State() : isValid(false),
                            TZero(FP::undefined()),
                            cT2(FP::undefined()),
                            cR2(FP::undefined())
{
    memset(&black, 0, sizeof(black));
}

CTSSolver::BlackParameterization::BlackParameterization() : type(Bond1999)
{ }

bool CTSSolver::BlackParameterization::operator==(const BlackParameterization &other) const
{
    switch (type) {
    case Bond1999:
        if (other.type != Bond1999)
            return false;
        break;
    case Virkkula2005:
        if (other.type != Virkkula2005)
            return false;
        if (p.virkkula2005.c1 != other.p.virkkula2005.c1)
            return false;
        if (p.virkkula2005.c2 != other.p.virkkula2005.c2)
            return false;
        break;
    }
    return true;
}

bool CTSSolver::BlackParameterization::operator!=(const BlackParameterization &other) const
{
    switch (type) {
    case Bond1999:
        if (other.type != Bond1999)
            return true;
        break;
    case Virkkula2005:
        if (other.type != Virkkula2005)
            return true;
        if (p.virkkula2005.c1 != other.p.virkkula2005.c1)
            return true;
        if (p.virkkula2005.c2 != other.p.virkkula2005.c2)
            return true;
        break;
    }
    return true;
}

bool CTSSolver::BlackParameterization::valid() const
{
    switch (type) {
    case Bond1999:
        break;
    case Virkkula2005:
        if (!FP::defined(p.virkkula2005.c1) || p.virkkula2005.c1 <= 0.0)
            return false;
        if (!FP::defined(p.virkkula2005.c2 || p.virkkula2005.c2 <= 0.0))
            return false;
        break;
    }
    return true;
}


CTSSolver::WhiteParameterization::WhiteParameterization() : type(NOAA)
{ }

bool CTSSolver::WhiteParameterization::operator==(const WhiteParameterization &other) const
{
    switch (type) {
    case NOAA:
        if (other.type != NOAA)
            return false;
        break;
    }
    return true;
}

bool CTSSolver::WhiteParameterization::operator!=(const WhiteParameterization &other) const
{
    switch (type) {
    case NOAA:
        if (other.type != NOAA)
            return true;
        break;
    }
    return true;
}

bool CTSSolver::WhiteParameterization::valid() const
{
    switch (type) {
    case NOAA:
        break;
    }
    return true;
}

CTSSolver::CTSSolver() : parameters(), state(), result(0.0)
{
    parameters.deltaSF = FP::undefined();
    parameters.deltaAF = FP::undefined();
    parameters.gF = FP::undefined();
    parameters.etaF = FP::undefined();
}

double CTSSolver::solve(double dF, double dS, double g, const Parameters &parameters)
{
    if (!FP::defined(dF) || dF < -0.1)
        return FP::undefined();
    if (!FP::defined(dS) || dS < -100.0)
        return FP::undefined();
    if (!FP::defined(g) || g < -0.1)
        return FP::undefined();

    if (!parameters.valid())
        return FP::undefined();
    if (!this->parameters.valid() || this->parameters != parameters) {
        this->parameters = parameters;
        initialize();
    }
    if (!state.valid())
        return FP::undefined();

    if (!prepareState(parameters, state, dS, g))
        return FP::undefined();

    double dA = SolverInternal(this, dS, g).solve(dF);
    if (!FP::defined(dA))
        return FP::undefined();
    result = dA;
    return dA;
}
