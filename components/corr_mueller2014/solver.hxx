/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef SOLVER_H
#define SOLVER_H

#include "core/first.hxx"

#include <QtGlobal>

#include "algorithms/numericsolve.hxx"

class CTSSolver {
public:
    struct BlackParameterization {
        BlackParameterization();

        enum {
            Bond1999, Virkkula2005,
        } type;
        union {
            struct {
            } bond1999;
            struct {
                double c1;
                double c2;
            } virkkula2005;
        } p;

        bool operator==(const BlackParameterization &other) const;

        bool operator!=(const BlackParameterization &other) const;

        bool valid() const;
    };

    struct WhiteParameterization {
        WhiteParameterization();

        enum {
            NOAA,
        } type;
        union {
            struct {
            } noaa;
        } p;

        bool operator==(const WhiteParameterization &other) const;

        bool operator!=(const WhiteParameterization &other) const;

        bool valid() const;
    };

    struct Parameters {
        double deltaSF;
        double deltaAF;
        double gF;
        double etaF;

        BlackParameterization black;
        WhiteParameterization white;

        inline Parameters()
        { }

        inline Parameters(const BlackParameterization &b, const WhiteParameterization &w) : black(
                b), white(w)
        { }

        bool operator==(const Parameters &other) const;

        bool operator!=(const Parameters &other) const;

        bool valid() const;
    };

    struct State {
        bool isValid;

        double TZero;
        double cT2;
        double cR2;

        double deltaSFetaF;
        double deltaAFetaF;

        union {
            struct {
            } bond1999;
            struct {
                double c1c2;
                double c1c2sq;
            } virkkula2005;
        } black;

        double F_sdS;

        State();

        inline bool valid() const
        { return isValid; }
    };

private:

    Parameters parameters;
    State state;

    double result;

    class SolverInternal : public CPD3::Algorithms::NewtonRaphson<SolverInternal> {
        const CTSSolver *solver;
        double dS;
        double g;
    public:
        SolverInternal(const CTSSolver *solver, double dS, double g);

        double initial(double yTarget) const;

        double firstStep(double x0) const;

        double evaluate(double x) const;

        bool completed(double dX) const;
    };

    void initialize();

public:
    CTSSolver();

    double solve(double deltaF, double deltaS, double g, const Parameters &parameters);

    void reset();
};

#endif
