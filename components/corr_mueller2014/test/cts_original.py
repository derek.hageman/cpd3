#!/usr/bin/python

from math import *


## speed up version  ##
#2.71828**x is much fastern than exp(x)
# sqrt(x) is very slow function better use x**0.5
# abs(x) is very slov function

def abs3(x):
	if x<0:
		return -x
	else:
		return x

####### For more information about the "Two Stream Method" see Arnott et al. 2005




# 2 Stream forward calculation (single layer)
# input are particle abs., sca., and asymmetry (delta_a,delta_s,g)
# returns transmittance and reflectance	
# ap: particle absorption 
# sp: particle scattering
# af: filter absorption
# sf: filter scattering
# gp: particle asymmetrie parameter
# gf: filter asymmetrie parameter
# 
# Mathematical problems emerge when : 	"K = sqrt((1-ssa)*(1-ssa*g))  < 1 "
# As long as the filter is absorbing the single scattering albedo (ssa) is samller than "1" and K is a positiv number 
# In our case the filter is absorbing : delta_af=0.017, in function _2Stream_2L(...)) 
# What happens if delta_ap is negative because of noisy data, first steps of iterative algorithm before delta_ap converges, etc.. ? 
# Solution : 
# 	 i)	calculate [T,R] for three cases
#					a)	(1-ssa)*(1-ssa*g) > 0, this is the smple case which was used in Arnott et al.(2005)
#				    b)  (1-ssa)*(1-ssa*g) = 0 
#					c)  (1-ssa)*(1-ssa*g) < 0   
#	 ii)   use relations sinh(1i*x)=i*sin(x) and cosh(1i*x)=cos(x) 
# With this approach we avoid imaginary numbers and division by zero
# 	

def _2Stream_1L(delta_ap,delta_sp,gp,delta_af,delta_sf,gf):
	mu1=0.57735											# universal constant 
	
	
	# average filter + particle properties						
	delta_e=delta_sp+delta_ap+delta_sf+delta_af			# total extinction optical depth

	
	# if delta_sp<0 the g is calculated using abs(delta_sp)
	if delta_sp<0.0 or gp<0.0:
		g=gf
	else:	
		g=(gp*delta_sp+gf*delta_sf)/(delta_sp+delta_sf)		# average asymmetry parameter
	ssa=(delta_sp+delta_sf)/delta_e						# single scattering albedo
	ssaTmp=(1.0-ssa)*(1.0-ssa*g)
	
	
	if ssaTmp>0:								# case a) 
		K=ssaTmp**0.5	
		#print g,delta_sp,delta_sf 
		v=K*delta_e/mu1								# temp variable
    		ep=2.71828**v
    		en=1/ep
    		sinhp=(ep-en)/2.0
    		coshp=(ep+en)/2.0
    		sinhpK=sinhp/K
    		ssa1g  = ssa*(1.0+g)
    		ssa1g2 = 2.0-ssa1g
		T=2.0/( ssa1g2 * sinhpK + 2.0 * coshp    )
		R=(ssa*(1.0-g)*sinhpK)/( ssa1g2 * sinhpK + 2.0 * coshp    )
		return [T,R]
		
	elif ssaTmp==0.0:						# case b) 
														# use: 
														# 	K=0														#  	limes(K->0,sinh(x*k)/k = x 
														#   lines(K->0,cosh(x*k) = 1
		K=0
		v=K*delta_e/mu1								# temp variable
		T=2*mu1                      /(  2*delta_e-ssa*delta_e-ssa*g*delta_e+2*mu1)
		R=(ssa*delta_e-ssa*g*delta_e)/(  2*delta_e-ssa*delta_e-ssa*g*delta_e+2*mu1)
		return [T,R]
	
	else: 												# case c) 	
		#print delta_ap,
		x=abs(ssaTmp)						# K = 1i*sqrt( x )
		sqrtx=x**0.5
		ssa1g=ssa*(1.0+g)
		xdemu1=sqrtx*delta_e/mu1
		sinp=sin(xdemu1)
		cosp=cos(xdemu1)
		sinpsx=sinp/sqrtx
										# use sinh(1i*x) = 1i * sin(x)
														# and cosh(1i*x)=c
		T=2.0/( (2.0-ssa1g) * sinpsx + 2.0 * cosp  )
		R=(ssa*(1.0-g)*sinpsx)/( (2.0-ssa1g) * sinp/x + 2.0 * cosp )
		return [T,R]
	# tested 05/22/2005 Th. Muller
	
	
	

# 2 Stream with two layers
# first layer is composite of particles and fiber filters
# second layer is filter only
# first layer is fraction (chi) of total filter thickness
# returns transmittance and reflectance
# filter parameters are defined as:
# delta_sf=9.631
# delta_af=0.017
# gf=0.75
def _2Stream_2L(delta_ap,delta_sp,gp):
	delta_sf=7.69
 	delta_af=0.038
	gf=0.75
	chi=0.2		###################  < ----- chi
	
	
	
	
	(T1,R1)=_2Stream_1L(delta_ap,delta_sp,gp,delta_af*chi,delta_sf*chi,gf)
	(T2,R2)=_2Stream_1L(0,0,0,delta_af*(1.0-chi),delta_sf*(1.0-chi),gf)
	
	IR12=1.0-R1*R2
	T2L=T1*T2/IR12
	R2L=R1+ (T1*T1)*R2 / IR12
	
	#T2L=_2Stream_1L(delta_ap,delta_sp,gp,delta_af*chi,delta_sf*chi,gf)[0]*_2Stream_1L(0,0,0,delta_af*(1-chi),delta_sf*(1-chi),gf)[0]/(1-_2Stream_1L(delta_ap,delta_sp,gp,delta_af*chi,delta_sf*chi,gf)[1]*_2Stream_1L(0,0,0,delta_af*(1-chi),delta_sf*(1-chi),gf)[1])
	#R2L=_2Stream_1L(delta_ap,delta_sp,gp,delta_af*chi,delta_sf*chi,gf)[1]+_2Stream_1L(delta_ap,delta_sp,gp,delta_af*chi,delta_sf*chi,gf)[0]**2.0*_2Stream_1L(0,0,0,delta_af*(1-chi),delta_sf*(1-chi),gf)[1]/(1-_2Stream_1L(delta_ap,delta_sp,gp,delta_af*chi,delta_sf*chi,gf)[1]*_2Stream_1L(0,0,0,delta_af*(1-chi),delta_sf*(1-chi),gf)[1])
	# tested 05/11/2005 Th. Muller
	return [T2L,R2L]

	
# optical depth using 2 stream approach
def delta_f_2S(delta_ap,delta_sp,gp):
	# tested 05/11/2005 Th. Muller
	delta_f=-log(_2Stream_2L(delta_ap,delta_sp,gp)[0]/_2Stream_2L(0,0,0)[0])
	#if delta_f<0.0:
	#	print "error"
	return  delta_f


# F_f results from two stream approximation 
# note : delta_f_2S (0,delta_sp,gp) -> ssa=1
# 		 delta_f_2S (delta_ap,0,gp) -> ssa=0 ! 	F_a calculated from RAOS (ssa=0.3) data is corrected to ssa=0 leading to a new function F_a0
def F_f(delta_ap,delta_sp,gp):
	try:
		F_f=delta_f_2S(delta_ap,delta_sp,gp) /(delta_f_2S(0,delta_sp,gp)+delta_f_2S(delta_ap,0,gp))
		return [F_f,True]
	except:
		#print "numerical exception in subroutine F_f"
		return [-9999,False]
	

	
	
#  parametrization for NaCl runs, NOAA
# tested 12-Oct-2010 Th. Muller
def F_s(delta_s,g):			
	
	
	b0=0.1509	
	b1=-0.1611
	b2=4.5414
	b3=-5.7062
	b4=-1.9031	
	b5=0.01
	try:

		F_s=b5+(b0+b1*g)*exp(-((log(delta_s)+b2)/(b3+b4*g))**4)     
		return [F_s,True]
	except:
		#print "numerical  exception in subroutine F_s"
		return [-9999,False]
	





# RAOS, single wavelength PSAP Virkkula et. al (2005), ssa=0.0
# tested 05/23/2005 Th. Muller
def F_a_FAKE(delta_a):
	#c1=0.306	# 530 nm modified 3 lambda PSAP
	#c2=0.522
	c1=0.354	# 574 nm, old 1 lambda PSAP 
	c2=0.617   ### * 1.192 # Error correct 2010-06-01   ??????  correction only for 3 lambda PSAP
	try:
		if delta_a==0:
			F_a=1./c1
		else:
			F_a=((delta_a*2/c2+(c1/c2)**2)**0.5-(c1/c2))/delta_a
		return [F_a,True]
	except:
		#print "numerical  exception in subroutine F_a"
		return [-9999,False]
	

def F_a(delta_a):   ###Bond func !!!!!
	K1=0.02
	K2=1.22
	K3=0.85
	c1=1.0796
	c2=0.71
	
	 
	
	try:
		if delta_a==0:
			F_=K2/K
		else:
			F_a=log(   (exp( c2*(K2/K3*delta_a+K1/K3*0.0) +log(c1+c2) ) -c1 )  /c2)/delta_a
		return [F_a,True]
	except:
		#print "numerical  exception in subroutine F_a"
		return [-9999,False]
	
	
	


# filter optical depth
# tested 05/23/2005 Th. Muller
# delta_f = (F_a0 *delta_a + F_s *delta_s )* F_f
def delta_f(delta_a,delta_s,g):
	result_F_a=F_a(delta_a)						# calibration function for ssa=0
	result_F_s=F_s(delta_s,g)						# calibration function for ssa=1
	result_F_f=F_f(delta_a,delta_s,g)					# cross term
	
	if (result_F_a[1]==True and result_F_s[1]==True and result_F_f[1]==True):
		return [(result_F_a[0]*delta_a+result_F_s[0]*delta_s)*result_F_f[0],True]
	else:
		return [(result_F_a[0]*delta_a+result_F_s[0]*delta_s)*result_F_f[0],False]
	




# Constrained Two Stream Solver
# solve for delta_a
# Method: Newton approximation
# delta_f_meas :  calculated from transmittance or integration of sigma_f
# delta_s_meas :  calculated using nephelometer total scattering,  flow rate and spot size of PSAP
# g_meas	   :  calculated using nephelometer total and back scattering ( g is function a of BS/TS)
# delta_a_start:  is delta_a_start the value of the absorption optical depth calculated before, this algorithm converges very fast 
def CTS_solver(delta_f_meas,delta_s_meas,g_meas,delta_a_start):
	maxiter=5								# maximum number of iterations
	k=0.01								# used for calculating the 1st derivative
	delta_a=[]								# define empty list 
	delta_a.append(delta_a_start)			# starting value
	
	for i in range(0,maxiter):
			
		result_delta_f=delta_f(delta_a[i],delta_s_meas,g_meas)
		result_delta_f_plus_k=delta_f(delta_a[i]+k,delta_s_meas,g_meas)
		
		if ((result_delta_f)[1]==True and  (result_delta_f_plus_k)[1]==True	): 
			return_code=True
		else:
			return_code=False
		first_derivative=(result_delta_f_plus_k[0]-result_delta_f[0] )/k
		
		
		modification=(result_delta_f[0] - delta_f_meas)/first_derivative
		delta_a.append(delta_a[i]-  modification)
		
		
		if abs(modification)<10**-4.0:		# makes this method much faster, for higher precision use values smaller than 10**-4.0 or do not use it...
			break
		
	# return_code = True   -> no error
	# return_code = False  -> numerical error in one of the sub routines -> no solution
	#print first_derivative
	return [delta_a.pop(),return_code]
	
	



if __name__ == '__main__': 
	#examples

	
	#for g in [0.5,0.55,0.6,0.65,0.7,0.75]:
	#	for delta_a in [0.01,0.1,0.2,0.3,0.4,0.5]:	
	#		for delta_s in [0.01,1.0,2.0,3.0,4.0,5.0]:
	#			print delta_a,delta_s,g,CTS_solver(delta_a,delta_s,g,0)   		 
	#for i in range(100):
	#	CTS_solver(0.1,0.1,0.7,0.0)
	#delta_sf=9.631
 	#delta_af=0.017
	#gf=0.75
	#chi=0.2		
	#print _2Stream_1L(0.1,1.0,0.7,delta_af,delta_sf,gf)
	#print _2Stream_2L(0.1,1.0,0.7)
	#print F_s(1.,0.7)
	#print F_a(0.1)
	#print F_a0(0.1,0.7)
	#print delta_f(0.1,10,0.7)
	
	#print CTS_solver(0.1,1.0,0.7,0.1)
	#print -log(_2Stream_2L(0,1,0.6)[0]/_2Stream_2L(0,0,0.6)[0]),-log(_2Stream_2L(0,1,0.6)[1]/_2Stream_2L(0,0,0.6)[1])
	#print -log(_2Stream_2L(0,1,0.7)[0]/_2Stream_2L(0,0,0.7)[0]),-log(_2Stream_2L(0,1,0.7)[1]/_2Stream_2L(0,0,0.7)[1])
	#print F_a0(0.1,0.50)[0]/F_a(0.1)[0]
	#print F_a0(0.5,0.50)[0]/F_a(0.5)[0]
	
	#print delta_f(0.1,0.1,0.7)
	#print delta_f(0.1,0.0,0.7)
	


    print CTS_solver(0.5,2.0,0.7,0)
    print CTS_solver(0.5,0.2,0.7,0)
    print CTS_solver(0.5,2.0,0.6,0)
    print CTS_solver(0.4,2.0,0.7,0)
	
	
	
	
	
