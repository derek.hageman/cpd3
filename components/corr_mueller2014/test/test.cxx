/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/segmentprocessingstage.hxx"
#include "core/component.hxx"

#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"

#include "solver.hxx"
#include "solver.cxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace QTest {

template<>
char *toString(const QSet<double> &set)
{
    QByteArray ba = "QSet<double>(";
    for (QSet<double>::const_iterator i = set.constBegin(); i != set.constEnd(); ++i) {
        if (i != set.constBegin()) ba += ", ";
        ba += QByteArray::number(*i);
    }
    ba += ")";
    return qstrdup(ba.data());
}
}

class TestController : public SegmentProcessingStage::SequenceHandlerControl {
public:
    class ID {
    public:
        ID() : filter(), input()
        { }

        ID(const QSet<SequenceName> &setFilter, const QSet<SequenceName> &setInput) : filter(
                setFilter), input(setInput)
        { }

        bool operator==(const ID &other) const
        { return filter == other.filter && input == other.input; }

        QSet<SequenceName> filter;
        QSet<SequenceName> input;
    };

    QHash<int, ID> contents;

    virtual void filterUnit(const SequenceName &unit, int targetID)
    {
        contents[targetID].input.remove(unit);
        contents[targetID].filter.insert(unit);
    }

    virtual void inputUnit(const SequenceName &unit, int targetID)
    {
        if (contents[targetID].filter.contains(unit))
            return;
        contents[targetID].input.insert(unit);
    }

    virtual bool isHandled(const SequenceName &unit)
    {
        for (QHash<int, ID>::const_iterator it = contents.constBegin();
                it != contents.constEnd();
                ++it) {
            if (it.value().filter.contains(unit))
                return true;
            if (it.value().input.contains(unit))
                return true;
        }
        return false;
    }
};

class TestComponent : public QObject {
Q_OBJECT

    SegmentProcessingStageComponent *component;

    static void setIfUndefined(double &target, double value)
    {
        if (FP::defined(target))
            return;
        target = value;
    }

    static void resolveParameters(double wavelength, CTSSolver::Parameters &parameters)
    {
        int selW = 550;
        if (fabs(wavelength - 467.0) < fabs(wavelength - (double) selW))
            selW = 467;
        if (fabs(wavelength - 530.0) < fabs(wavelength - (double) selW))
            selW = 530;
        if (fabs(wavelength - 660.0) < fabs(wavelength - (double) selW))
            selW = 660;

        switch (selW) {
        case 550:
            setIfUndefined(parameters.deltaSF, 7.63);
            setIfUndefined(parameters.deltaAF, 0.029);
            setIfUndefined(parameters.gF, 0.75);
            setIfUndefined(parameters.etaF, 0.2);
            break;
        case 467:
            setIfUndefined(parameters.deltaSF, 7.76);
            setIfUndefined(parameters.deltaAF, 0.033);
            setIfUndefined(parameters.gF, 0.75);
            setIfUndefined(parameters.etaF, 0.2);
            break;
        case 530:
            setIfUndefined(parameters.deltaSF, 7.69);
            setIfUndefined(parameters.deltaAF, 0.038);
            setIfUndefined(parameters.gF, 0.75);
            setIfUndefined(parameters.etaF, 0.2);
            break;
        case 660:
            setIfUndefined(parameters.deltaSF, 7.34);
            setIfUndefined(parameters.deltaAF, 0.019);
            setIfUndefined(parameters.gF, 0.75);
            setIfUndefined(parameters.etaF, 0.2);
            break;
        default:
            Q_ASSERT(false);
            break;
        }

        switch (parameters.black.type) {
        case CTSSolver::BlackParameterization::Virkkula2005:
            selW = 574;
            if (fabs(wavelength - 467.0) < fabs(wavelength - (double) selW))
                selW = 467;
            if (fabs(wavelength - 530.0) < fabs(wavelength - (double) selW))
                selW = 530;
            if (fabs(wavelength - 660.0) < fabs(wavelength - (double) selW))
                selW = 660;

            switch (selW) {
            case 574:
                setIfUndefined(parameters.black.p.virkkula2005.c1, 0.354);
                setIfUndefined(parameters.black.p.virkkula2005.c2, 0.617);
                break;
            case 467:
                setIfUndefined(parameters.black.p.virkkula2005.c1, 0.377);
                setIfUndefined(parameters.black.p.virkkula2005.c2, 0.640);
                break;
            case 530:
                setIfUndefined(parameters.black.p.virkkula2005.c1, 0.358);
                setIfUndefined(parameters.black.p.virkkula2005.c2, 0.640);
                break;
            case 660:
                setIfUndefined(parameters.black.p.virkkula2005.c1, 0.352);
                setIfUndefined(parameters.black.p.virkkula2005.c2, 0.674);
                break;
            default:
                Q_ASSERT(false);
                break;
            }
            break;
        case CTSSolver::BlackParameterization::Bond1999:
            break;
        }

        switch (parameters.white.type) {
        case CTSSolver::WhiteParameterization::NOAA:
            break;
        }
    }

    static double calculateAbsorption(double dL,
                                      double Ir0,
                                      double Ir1,
                                      double SOD0,
                                      double SOD1,
                                      double G0,
                                      double G1,
                                      double wavelength,
                                      double deltaSF = FP::undefined(),
                                      double deltaAF = FP::undefined(),
                                      double gF = FP::undefined(),
                                      double etaF = FP::undefined(),
                                      const CTSSolver::BlackParameterization &black = CTSSolver::BlackParameterization(),
                                      const CTSSolver::WhiteParameterization &white = CTSSolver::WhiteParameterization())
    {
        CTSSolver::Parameters parameters;
        parameters.deltaSF = deltaSF;
        parameters.deltaAF = deltaAF;
        parameters.gF = gF;
        parameters.etaF = etaF;
        parameters.black = black;
        parameters.white = white;

        resolveParameters(wavelength, parameters);

        CTSSolver s;

        double dA0 = s.solve(-log(Ir0), SOD0, G0, parameters);
        double dA1 = s.solve(-log(Ir1), SOD1, G1, parameters);

        return ((dA1 - dA0) / dL) * 1E6;
    }

    static double calculateAbsorptionDA(double dL,
                                        double AOD0,
                                        double AOD1,
                                        double SOD0,
                                        double SOD1,
                                        double G0,
                                        double G1,
                                        double wavelength,
                                        double deltaSF = FP::undefined(),
                                        double deltaAF = FP::undefined(),
                                        double gF = FP::undefined(),
                                        double etaF = FP::undefined(),
                                        const CTSSolver::BlackParameterization &black = CTSSolver::BlackParameterization(),
                                        const CTSSolver::WhiteParameterization &white = CTSSolver::WhiteParameterization())
    {
        CTSSolver::Parameters parameters;
        parameters.deltaSF = deltaSF;
        parameters.deltaAF = deltaAF;
        parameters.gF = gF;
        parameters.etaF = etaF;
        parameters.black = black;
        parameters.white = white;

        resolveParameters(wavelength, parameters);

        CTSSolver s;

        double dA0 = s.solve(AOD0, SOD0, G0, parameters);
        double dA1 = s.solve(AOD1, SOD1, G1, parameters);

        return ((dA1 - dA0) / dL) * 1E6;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<SegmentProcessingStageComponent *>(
                ComponentLoader::create("corr_mueller2014"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("transmittance-start")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("transmittance-end")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("length-start")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("length-end")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("deltas-start")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("deltas-end")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("asymmetry-start")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("asymmetry-end")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("delta-l")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("delta-sf")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("delta-af")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("gf")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("etaf")));

        QVERIFY(options.excluded().value("absorption").contains("suffix"));
        QVERIFY(options.excluded().value("suffix").contains("absorption"));
    }

    void solver()
    {
        QFETCH(double, dF);
        QFETCH(double, dS);
        QFETCH(double, g);
        QFETCH(double, dA);

        CTSSolver::Parameters parameters;
        parameters.deltaSF = 7.69;
        parameters.deltaAF = 0.038;
        parameters.gF = 0.75;
        parameters.etaF = 0.2;

        CTSSolver s;

        for (int t = 0; t < 5; t++) {
            double r = s.solve(dF, dS, g, parameters);
            if (!FP::defined(dA)) {
                QVERIFY(!FP::defined(r));
            } else {
                QVERIFY(FP::defined(r));
                QVERIFY(fabs(r - dA) < 1E-6);
            }
        }

#if 0
        QBENCHMARK {
            s.reset();
            s.solve(dF, dS, g, parameters);
        }
#endif
    }

    void solver_data()
    {
        QTest::addColumn<double>("dF");
        QTest::addColumn<double>("dS");
        QTest::addColumn<double>("g");
        QTest::addColumn<double>("dA");

        QTest::newRow("dF=0.5 dS=2.0 g=0.7") << 0.5 << 2.0 << 0.7 << 0.16263691042468775;
        QTest::newRow("dF=0.5 dS=0.2 g=0.7") << 0.5 << 0.2 << 0.7 << 0.21579776535410444;
        QTest::newRow("dF=0.5 dS=2.0 g=0.6") << 0.5 << 2.0 << 0.6 << 0.14946995534610732;
        QTest::newRow("dF=0.4 dS=2.0 g=0.7") << 0.4 << 2.0 << 0.7 << 0.1216056853715764;
    }

    void dynamicDefaults()
    {
        ComponentOptions options(component->getOptions());
        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        SegmentProcessingStage *filter2;
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "T_A11q"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BsG_S12"), &controller);
        QVERIFY(controller.contents.isEmpty());
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                filter2 = component->deserializeBasicFilter(stream);
                QVERIFY(stream.atEnd());
            }
            QVERIFY(filter2 != NULL);
            filter2->unhandled(SequenceName("brw", "raw", "T_A11q"), &controller);
            QVERIFY(controller.contents.isEmpty());
            delete filter2;
        }

        SequenceName BaB_A11_1("brw", "raw", "BaB_A11");
        SequenceName BaG_A11_1("brw", "raw", "BaG_A11");
        SequenceName BaR_A11_1("brw", "raw", "BaR_A11");
        SequenceName IrB_A11_1("brw", "raw", "IrB_A11");
        SequenceName IrG_A11_1("brw", "raw", "IrG_A11");
        SequenceName IrR_A11_1("brw", "raw", "IrR_A11");
        SequenceName IrBe_A11_1("brw", "raw", "IrB_A11", {SequenceName::flavor_end});
        SequenceName IrGe_A11_1("brw", "raw", "IrG_A11", {SequenceName::flavor_end});
        SequenceName IrRe_A11_1("brw", "raw", "IrR_A11", {SequenceName::flavor_end});
        SequenceName F1_A11_1("brw", "raw", "F1_A11");
        SequenceName L_A11_1("brw", "raw", "L_A11");
        SequenceName Le_A11_1("brw", "raw", "L_A11", {SequenceName::flavor_end});
        SequenceName OsB_A11_1("brw", "raw", "OsB_A11");
        SequenceName OsG_A11_1("brw", "raw", "OsG_A11");
        SequenceName OsR_A11_1("brw", "raw", "OsR_A11");
        SequenceName OsBe_A11_1("brw", "raw", "OsB_A11", {SequenceName::flavor_end});
        SequenceName OsGe_A11_1("brw", "raw", "OsG_A11", {SequenceName::flavor_end});
        SequenceName OsRe_A11_1("brw", "raw", "OsR_A11", {SequenceName::flavor_end});
        SequenceName OaB_A11_1("brw", "raw", "OaB_A11");
        SequenceName OaG_A11_1("brw", "raw", "OaG_A11");
        SequenceName OaR_A11_1("brw", "raw", "OaR_A11");
        SequenceName OaBe_A11_1("brw", "raw", "OaB_A11", {SequenceName::flavor_end});
        SequenceName OaGe_A11_1("brw", "raw", "OaG_A11", {SequenceName::flavor_end});
        SequenceName OaRe_A11_1("brw", "raw", "OaR_A11", {SequenceName::flavor_end});
        SequenceName ZGB_A11_1("brw", "raw", "ZGB_A11");
        SequenceName ZGG_A11_1("brw", "raw", "ZGG_A11");
        SequenceName ZGR_A11_1("brw", "raw", "ZGR_A11");
        SequenceName ZGBe_A11_1("brw", "raw", "ZGB_A11", {SequenceName::flavor_end});
        SequenceName ZGGe_A11_1("brw", "raw", "ZGG_A11", {SequenceName::flavor_end});
        SequenceName ZGRe_A11_1("brw", "raw", "ZGR_A11", {SequenceName::flavor_end});

        SequenceName BaB_A12_1("brw", "raw", "BaB_A12");
        SequenceName BaG_A12_1("brw", "raw", "BaG_A12");
        SequenceName BaR_A12_1("brw", "raw", "BaR_A12");
        SequenceName IrB_A12_1("brw", "raw", "IrB_A12");
        SequenceName IrG_A12_1("brw", "raw", "IrG_A12");
        SequenceName IrR_A12_1("brw", "raw", "IrR_A12");
        SequenceName IrBe_A12_1("brw", "raw", "IrB_A12", {SequenceName::flavor_end});
        SequenceName IrGe_A12_1("brw", "raw", "IrG_A12", {SequenceName::flavor_end});
        SequenceName IrRe_A12_1("brw", "raw", "IrR_A12", {SequenceName::flavor_end});
        SequenceName F1_A12_1("brw", "raw", "F1_A12");
        SequenceName L_A12_1("brw", "raw", "L_A12");
        SequenceName Le_A12_1("brw", "raw", "L_A12", {SequenceName::flavor_end});
        SequenceName OsB_A12_1("brw", "raw", "OsB_A12");
        SequenceName OsG_A12_1("brw", "raw", "OsG_A12");
        SequenceName OsR_A12_1("brw", "raw", "OsR_A12");
        SequenceName OsBe_A12_1("brw", "raw", "OsB_A12", {SequenceName::flavor_end});
        SequenceName OsGe_A12_1("brw", "raw", "OsG_A12", {SequenceName::flavor_end});
        SequenceName OsRe_A12_1("brw", "raw", "OsR_A12", {SequenceName::flavor_end});
        SequenceName OaB_A12_1("brw", "raw", "OaB_A12");
        SequenceName OaG_A12_1("brw", "raw", "OaG_A12");
        SequenceName OaR_A12_1("brw", "raw", "OaR_A12");
        SequenceName OaBe_A12_1("brw", "raw", "OaB_A12", {SequenceName::flavor_end});
        SequenceName OaGe_A12_1("brw", "raw", "OaG_A12", {SequenceName::flavor_end});
        SequenceName OaRe_A12_1("brw", "raw", "OaR_A12", {SequenceName::flavor_end});
        SequenceName ZGB_A12_1("brw", "raw", "ZGB_A12");
        SequenceName ZGG_A12_1("brw", "raw", "ZGG_A12");
        SequenceName ZGR_A12_1("brw", "raw", "ZGR_A12");
        SequenceName ZGBe_A12_1("brw", "raw", "ZGB_A12", {SequenceName::flavor_end});
        SequenceName ZGGe_A12_1("brw", "raw", "ZGG_A12", {SequenceName::flavor_end});
        SequenceName ZGRe_A12_1("brw", "raw", "ZGR_A12", {SequenceName::flavor_end});

        SequenceName BaB_A11_2("sgp", "raw", "BaB_A11");
        SequenceName BaG_A11_2("sgp", "raw", "BaG_A11");
        SequenceName IrB_A11_2("sgp", "raw", "IrB_A11");
        SequenceName IrG_A11_2("sgp", "raw", "IrG_A11");
        SequenceName IrBe_A11_2("sgp", "raw", "IrB_A11", {SequenceName::flavor_end});
        SequenceName IrGe_A11_2("sgp", "raw", "IrG_A11", {SequenceName::flavor_end});
        SequenceName F1_A11_2("sgp", "raw", "F1_A11");
        SequenceName L_A11_2("sgp", "raw", "L_A11");
        SequenceName Le_A11_2("sgp", "raw", "L_A11", {SequenceName::flavor_end});
        SequenceName OsB_A11_2("sgp", "raw", "OsB_A11");
        SequenceName OsG_A11_2("sgp", "raw", "OsG_A11");
        SequenceName OsBe_A11_2("sgp", "raw", "OsB_A11", {SequenceName::flavor_end});
        SequenceName OsGe_A11_2("sgp", "raw", "OsG_A11", {SequenceName::flavor_end});
        SequenceName OaB_A11_2("sgp", "raw", "OaB_A11");
        SequenceName OaG_A11_2("sgp", "raw", "OaG_A11");
        SequenceName OaBe_A11_2("sgp", "raw", "OaB_A11", {SequenceName::flavor_end});
        SequenceName OaGe_A11_2("sgp", "raw", "OaG_A11", {SequenceName::flavor_end});
        SequenceName ZGB_A11_2("sgp", "raw", "ZGB_A11");
        SequenceName ZGG_A11_2("sgp", "raw", "ZGG_A11");
        SequenceName ZGBe_A11_2("sgp", "raw", "ZGB_A11", {SequenceName::flavor_end});
        SequenceName ZGGe_A11_2("sgp", "raw", "ZGG_A11", {SequenceName::flavor_end});


        filter->unhandled(IrB_A11_1, &controller);
        filter->unhandled(BaB_A11_1, &controller);
        filter->unhandled(BaG_A11_1, &controller);
        filter->unhandled(BaR_A11_1, &controller);
        filter->unhandled(IrG_A11_1, &controller);
        filter->unhandled(IrR_A11_1, &controller);
        filter->unhandled(F1_A11_1, &controller);
        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents.keys(TestController::ID(QSet<SequenceName>() <<
                                                                               BaB_A11_1 <<
                                                                               BaG_A11_1 <<
                                                                               BaR_A11_1 << F1_A11_1,
                                                          QSet<SequenceName>() <<
                                                                               IrB_A11_1 <<
                                                                               IrG_A11_1 <<
                                                                               IrR_A11_1 <<
                                                                               IrBe_A11_1 <<
                                                                               IrGe_A11_1 <<
                                                                               IrRe_A11_1 <<
                                                                               L_A11_1 <<
                                                                               Le_A11_1 <<
                                                         OsB_A11_1 <<
                                                         OsG_A11_1 <<
                                                         OsR_A11_1 <<
                                                         OsBe_A11_1 <<
                                                         OsGe_A11_1 <<
                                                         OsRe_A11_1 <<
                                                         OaB_A11_1 <<
                                                         OaG_A11_1 <<
                                                         OaR_A11_1 <<
                                                         OaBe_A11_1 <<
                                                         OaGe_A11_1 <<
                                                         OaRe_A11_1 <<
                                                         ZGB_A11_1 <<
                                                         ZGG_A11_1 <<
                                                         ZGR_A11_1 <<
                                                         ZGBe_A11_1 <<
                                                         ZGGe_A11_1 <<
                                                         ZGRe_A11_1));
        QCOMPARE(idL.size(), 1);
        int A11_1 = idL.at(0);

        filter->unhandled(L_A12_1, &controller);
        filter->unhandled(OsB_A12_1, &controller);
        filter->unhandled(ZGB_A12_1, &controller);
        filter->unhandled(BaB_A12_1, &controller);
        filter->unhandled(BaG_A12_1, &controller);
        filter->unhandled(BaR_A12_1, &controller);
        filter->unhandled(F1_A12_1, &controller);
        QCOMPARE(controller.contents.size(), 2);
        idL = controller.contents.keys(TestController::ID(QSet<SequenceName>() <<
                                                                               BaB_A12_1 <<
                                                                               BaG_A12_1 <<
                                                                               BaR_A12_1 << F1_A12_1,
                                                          QSet<SequenceName>() <<
                                                                               IrB_A12_1 <<
                                                                               IrG_A12_1 <<
                                                                               IrR_A12_1 <<
                                                                               IrBe_A12_1 <<
                                                                               IrGe_A12_1 <<
                                                                               IrRe_A12_1 <<
                                                                               L_A12_1 <<
                                                                               Le_A12_1 <<
                                                         OsB_A12_1 <<
                                                         OsG_A12_1 <<
                                                         OsR_A12_1 <<
                                                         OsBe_A12_1 <<
                                                         OsGe_A12_1 <<
                                                         OsRe_A12_1 <<
                                                         OaB_A12_1 <<
                                                         OaG_A12_1 <<
                                                         OaR_A12_1 <<
                                                         OaBe_A12_1 <<
                                                         OaGe_A12_1 <<
                                                         OaRe_A12_1 <<
                                                         ZGB_A12_1 <<
                                                         ZGG_A12_1 <<
                                                         ZGR_A12_1 <<
                                                                               ZGBe_A12_1 <<
                                                                               ZGGe_A12_1 <<
                                                                               ZGRe_A12_1));
        QCOMPARE(idL.size(), 1);
        int A12_1 = idL.at(0);

        filter->unhandled(F1_A11_2, &controller);
        filter->unhandled(BaB_A11_2, &controller);
        filter->unhandled(BaG_A11_2, &controller);
        QCOMPARE(controller.contents.size(), 3);
        idL = controller.contents
                        .keys(TestController::ID(
                                QSet<SequenceName>() << BaB_A11_2 << BaG_A11_2 << F1_A11_2,
                                QSet<SequenceName>() <<
                                        IrB_A11_2 <<
                                        IrG_A11_2 <<
                                        IrBe_A11_2 <<
                                        IrGe_A11_2 <<
                                        L_A11_2 <<
                                        Le_A11_2 <<
                                        OsB_A11_2 <<
                                        OsG_A11_2 <<
                                        OsBe_A11_2 <<
                                        OsGe_A11_2 <<
                                        OaB_A11_2 <<
                                        OaG_A11_2 <<
                                        OaBe_A11_2 <<
                                        OaGe_A11_2 <<
                                        ZGB_A11_2 <<
                                        ZGG_A11_2 <<
                                        ZGBe_A11_2 <<
                                        ZGGe_A11_2));
        QCOMPARE(idL.size(), 1);
        int A11_2 = idL.at(0);

        data.setStart(15.0);
        data.setEnd(16.0);

        {
            Variant::Root meta;
            meta.write().metadataReal("Wavelength").setDouble(467.0);
            data.setValue(BaB_A11_1.toMeta(), meta);
            data.setValue(BaB_A12_1.toMeta(), meta);
            data.setValue(BaB_A11_2.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(530.0);
            data.setValue(BaG_A11_1.toMeta(), meta);
            data.setValue(BaG_A12_1.toMeta(), meta);
            data.setValue(BaG_A11_2.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(660.0);
            data.setValue(BaR_A11_1.toMeta(), meta);

            data.setValue(BaR_A12_1.toMeta(), meta);
            data.setValue(F1_A11_1.toMeta(), Variant::Root(Variant::Type::MetadataFlags));
            data.setValue(F1_A11_2.toMeta(), Variant::Root(Variant::Type::MetadataFlags));
            data.setValue(F1_A12_1.toMeta(), Variant::Root(Variant::Type::MetadataFlags));

            filter->processMeta(A11_1, data);
            QVERIFY(data.value(F1_A11_1.toMeta()).metadataSingleFlag("Mueller2014").exists());
            QCOMPARE(data.value(BaB_A11_1.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);

            filter->processMeta(A11_2, data);
            QVERIFY(data.value(F1_A11_2.toMeta()).metadataSingleFlag("Mueller2014").exists());
            QCOMPARE(data.value(BaB_A11_2.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);

            filter->processMeta(A12_1, data);
            QVERIFY(data.value(F1_A12_1.toMeta()).metadataSingleFlag("Mueller2014").exists());
            QCOMPARE(data.value(BaB_A12_1.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
        }

        data.setValue(F1_A11_1, Variant::Root(Variant::Type::Flags));
        data.setValue(BaB_A11_1, Variant::Root(20.0));
        data.setValue(BaG_A11_1, Variant::Root(18.0));
        data.setValue(BaR_A11_1, Variant::Root(14.0));
        data.setValue(L_A11_1, Variant::Root(100.0));
        data.setValue(Le_A11_1, Variant::Root(150.0));
        data.setValue(IrB_A11_1, Variant::Root(0.7));
        data.setValue(IrG_A11_1, Variant::Root(0.8));
        data.setValue(IrR_A11_1, Variant::Root(0.9));
        data.setValue(IrBe_A11_1, Variant::Root(0.69));
        data.setValue(IrGe_A11_1, Variant::Root(0.79));
        data.setValue(IrRe_A11_1, Variant::Root(0.89));
        data.setValue(OsB_A11_1, Variant::Root(0.300));
        data.setValue(OsG_A11_1, Variant::Root(0.200));
        data.setValue(OsR_A11_1, Variant::Root(0.100));
        data.setValue(OsBe_A11_1, Variant::Root(0.303));
        data.setValue(OsGe_A11_1, Variant::Root(0.202));
        data.setValue(OsRe_A11_1, Variant::Root(0.101));
        data.setValue(ZGB_A11_1, Variant::Root(0.2));
        data.setValue(ZGG_A11_1, Variant::Root(0.3));
        data.setValue(ZGR_A11_1, Variant::Root(0.4));
        data.setValue(ZGBe_A11_1, Variant::Root(0.21));
        data.setValue(ZGGe_A11_1, Variant::Root(0.31));
        data.setValue(ZGRe_A11_1, Variant::Root(0.41));
        filter->process(A11_1, data);
        QVERIFY(fabs(data.value(BaB_A11_1).toDouble() -
                             calculateAbsorption(50.0, 0.7, 0.69, 0.300, 0.303, 0.2, 0.21, 467)) <
                        1E-4);
        QVERIFY(fabs(data.value(BaG_A11_1).toDouble() -
                             calculateAbsorption(50.0, 0.8, 0.79, 0.200, 0.202, 0.3, 0.31, 530)) <
                        1E-4);
        QVERIFY(fabs(data.value(BaR_A11_1).toDouble() -
                             calculateAbsorption(50.0, 0.9, 0.89, 0.100, 0.101, 0.4, 0.41, 660)) <
                        1E-4);
        QVERIFY(data.value(F1_A11_1).testFlag("Mueller2014"));

        {
            QByteArray sd;
            {
                QDataStream stream(&sd, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&sd, QIODevice::ReadOnly);
                filter2 = component->deserializeBasicFilter(stream);
            }
            QVERIFY(filter2 != NULL);
            data.setValue(F1_A11_1, Variant::Root(Variant::Type::Flags));
            data.setValue(BaB_A11_1, Variant::Root(20.0));
            data.setValue(BaG_A11_1, Variant::Root(18.0));
            data.setValue(BaR_A11_1, Variant::Root(14.0));
            data.setValue(L_A11_1, Variant::Root(100.0));
            data.setValue(Le_A11_1, Variant::Root(150.0));
            data.setValue(IrB_A11_1, Variant::Root(0.7));
            data.setValue(IrG_A11_1, Variant::Root(0.8));
            data.setValue(IrR_A11_1, Variant::Root(0.9));
            data.setValue(IrBe_A11_1, Variant::Root(0.69));
            data.setValue(IrGe_A11_1, Variant::Root(0.79));
            data.setValue(IrRe_A11_1, Variant::Root(0.89));
            data.setValue(OsB_A11_1, Variant::Root(0.300));
            data.setValue(OsG_A11_1, Variant::Root(0.200));
            data.setValue(OsR_A11_1, Variant::Root(0.100));
            data.setValue(OsBe_A11_1, Variant::Root(0.303));
            data.setValue(OsGe_A11_1, Variant::Root(0.202));
            data.setValue(OsRe_A11_1, Variant::Root(0.101));
            data.setValue(ZGB_A11_1, Variant::Root(0.2));
            data.setValue(ZGG_A11_1, Variant::Root(0.3));
            data.setValue(ZGR_A11_1, Variant::Root(0.4));
            data.setValue(ZGBe_A11_1, Variant::Root(0.21));
            data.setValue(ZGGe_A11_1, Variant::Root(0.31));
            data.setValue(ZGRe_A11_1, Variant::Root(0.41));
            filter2->process(A11_1, data);
            QVERIFY(fabs(data.value(BaB_A11_1).toDouble() -
                                 calculateAbsorption(50.0, 0.7, 0.69, 0.300, 0.303, 0.2, 0.21,
                                                     467)) < 1E-4);
            QVERIFY(fabs(data.value(BaG_A11_1).toDouble() -
                                 calculateAbsorption(50.0, 0.8, 0.79, 0.200, 0.202, 0.3, 0.31,
                                                     530)) < 1E-4);
            QVERIFY(fabs(data.value(BaR_A11_1).toDouble() -
                                 calculateAbsorption(50.0, 0.9, 0.89, 0.100, 0.101, 0.4, 0.41,
                                                     660)) < 1E-4);
            QVERIFY(data.value(F1_A11_1).testFlag("Mueller2014"));
            delete filter2;
        }


        data.setValue(F1_A11_1, Variant::Root(Variant::Type::Flags));
        data.setValue(BaB_A11_1, Variant::Root());
        data.setValue(BaG_A11_1, Variant::Root(18.0));
        data.setValue(BaR_A11_1, Variant::Root(14.0));
        data.setValue(L_A11_1, Variant::Root(100.0));
        data.setValue(Le_A11_1, Variant::Root(150.0));
        data.setValue(IrB_A11_1, Variant::Root(0.7));
        data.setValue(IrG_A11_1, Variant::Root(0.8));
        data.setValue(IrR_A11_1, Variant::Root(0.9));
        data.setValue(IrBe_A11_1, Variant::Root(0.69));
        data.setValue(IrGe_A11_1, Variant::Root(0.79));
        data.setValue(IrRe_A11_1, Variant::Root(0.89));
        data.setValue(OsB_A11_1, Variant::Root(0.300));
        data.setValue(OsG_A11_1, Variant::Root(0.200));
        data.setValue(OsR_A11_1, Variant::Root(0.100));
        data.setValue(OsBe_A11_1, Variant::Root(0.303));
        data.setValue(OsGe_A11_1, Variant::Root(0.202));
        data.setValue(OsRe_A11_1, Variant::Root(0.101));
        data.setValue(ZGB_A11_1, Variant::Root(0.2));
        data.setValue(ZGG_A11_1, Variant::Root(0.3));
        data.setValue(ZGR_A11_1, Variant::Root(0.4));
        data.setValue(ZGBe_A11_1, Variant::Root(0.21));
        data.setValue(ZGGe_A11_1, Variant::Root(0.31));
        data.setValue(ZGRe_A11_1, Variant::Root(0.41));
        filter->process(A11_1, data);
        QVERIFY(!FP::defined(data.value(BaB_A11_1).toReal()));
        QVERIFY(fabs(data.value(BaG_A11_1).toDouble() -
                             calculateAbsorption(50.0, 0.8, 0.79, 0.200, 0.202, 0.3, 0.31, 530)) <
                        1E-4);
        QVERIFY(fabs(data.value(BaR_A11_1).toDouble() -
                             calculateAbsorption(50.0, 0.9, 0.89, 0.100, 0.101, 0.4, 0.41, 660)) <
                        1E-4);
        QVERIFY(data.value(F1_A11_1).testFlag("Mueller2014"));

        data.setValue(F1_A12_1, Variant::Root(Variant::Type::Flags));
        data.setValue(BaB_A12_1, Variant::Root(21.0));
        data.setValue(BaG_A12_1, Variant::Root(19.0));
        data.setValue(BaR_A12_1, Variant::Root(15.0));
        data.setValue(L_A12_1, Variant::Root(100.0));
        data.setValue(Le_A12_1, Variant::Root(160.0));
        data.setValue(IrB_A12_1, Variant::Root(0.71));
        data.setValue(IrG_A12_1, Variant::Root(0.81));
        data.setValue(IrR_A12_1, Variant::Root(0.91));
        data.setValue(IrBe_A12_1, Variant::Root(0.68));
        data.setValue(IrGe_A12_1, Variant::Root(0.78));
        data.setValue(IrRe_A12_1, Variant::Root());
        data.setValue(OsB_A12_1, Variant::Root(0.301));
        data.setValue(OsG_A12_1, Variant::Root(0.201));
        data.setValue(OsR_A12_1, Variant::Root(0.101));
        data.setValue(OsBe_A12_1, Variant::Root(0.304));
        data.setValue(OsGe_A12_1, Variant::Root(0.203));
        data.setValue(OsRe_A12_1, Variant::Root(0.102));
        data.setValue(ZGB_A12_1, Variant::Root(0.22));
        data.setValue(ZGG_A12_1, Variant::Root(0.32));
        data.setValue(ZGR_A12_1, Variant::Root(0.42));
        data.setValue(ZGBe_A12_1, Variant::Root(0.20));
        data.setValue(ZGGe_A12_1, Variant::Root(0.30));
        data.setValue(ZGRe_A12_1, Variant::Root(0.40));
        filter->process(A12_1, data);
        QVERIFY(fabs(data.value(BaB_A12_1).toDouble() -
                             calculateAbsorption(60.0, 0.71, 0.68, 0.301, 0.304, 0.22, 0.20, 467)) <
                        1E-4);
        QVERIFY(fabs(data.value(BaG_A12_1).toDouble() -
                             calculateAbsorption(60.0, 0.81, 0.78, 0.201, 0.203, 0.32, 0.30, 530)) <
                        1E-4);
        QVERIFY(!FP::defined(data.value(BaR_A12_1).toDouble()));
        QVERIFY(data.value(F1_A12_1).testFlag("Mueller2014"));

        data.setValue(F1_A11_2, Variant::Root(Variant::Type::Flags));
        data.setValue(BaB_A11_2, Variant::Root(10.0));
        data.setValue(BaG_A11_2, Variant::Root(18.0));
        data.setValue(L_A11_2, Variant::Root(110.0));
        data.setValue(Le_A11_2, Variant::Root(180.0));
        data.setValue(IrB_A11_2, Variant::Root(0.71));
        data.setValue(IrG_A11_2, Variant::Root(0.81));
        data.setValue(IrBe_A11_2, Variant::Root(0.7));
        data.setValue(IrGe_A11_2, Variant::Root(0.8));
        data.setValue(OsB_A11_2, Variant::Root(0.303));
        data.setValue(OsG_A11_2, Variant::Root(0.202));
        data.setValue(OsBe_A11_2, Variant::Root(0.300));
        data.setValue(OsGe_A11_2, Variant::Root(0.200));
        data.setValue(ZGB_A11_2, Variant::Root(0.2));
        data.setValue(ZGG_A11_2, Variant::Root());
        data.setValue(ZGBe_A11_2, Variant::Root(0.21));
        data.setValue(ZGGe_A11_2, Variant::Root(0.31));
        filter->process(A11_2, data);
        QVERIFY(fabs(data.value(BaB_A11_2).toDouble() -
                             calculateAbsorption(70.0, 0.71, 0.7, 0.303, 0.300, 0.2, 0.21, 467)) <
                        1E-4);
        QVERIFY(fabs(data.value(BaG_A11_2).toDouble() -
                             calculateAbsorption(70.0, 0.81, 0.8, 0.202, 0.200, 0.31, 0.31, 530)) <
                        1E-4);
        QVERIFY(data.value(F1_A11_2).testFlag("Mueller2014"));

        data.setValue(F1_A11_2, Variant::Root(Variant::Type::Flags));
        data.setValue(BaB_A11_2, Variant::Root(10.0));
        data.setValue(BaG_A11_2, Variant::Root(18.0));
        data.setValue(L_A11_2, Variant::Root(100.0));
        data.setValue(Le_A11_2, Variant::Root(170.0));
        data.setValue(IrB_A11_2, Variant::Root(0.71));
        data.setValue(IrG_A11_2, Variant::Root(0.81));
        data.setValue(IrBe_A11_2, Variant::Root(0.7));
        data.setValue(IrGe_A11_2, Variant::Root(0.8));
        data.setValue(OsB_A11_2, Variant::Root(0.303));
        data.setValue(OsG_A11_2, Variant::Root(0.202));
        data.setValue(OsBe_A11_2, Variant::Root(0.300));
        data.setValue(OsGe_A11_2, Variant::Root(0.200));
        data.setValue(ZGB_A11_2, Variant::Root(0.2));
        data.setValue(ZGG_A11_2, Variant::Root(0.3));
        data.setValue(ZGBe_A11_2, Variant::Root());
        data.setValue(ZGGe_A11_2, Variant::Root(0.31));
        filter->process(A11_2, data);
        QVERIFY(fabs(data.value(BaB_A11_2).toDouble() -
                             calculateAbsorption(70.0, 0.71, 0.7, 0.303, 0.300, 0.2, 0.2, 467)) <
                        1E-4);
        QVERIFY(fabs(data.value(BaG_A11_2).toDouble() -
                             calculateAbsorption(70.0, 0.81, 0.8, 0.202, 0.200, 0.3, 0.31, 530)) <
                        1E-4);
        QVERIFY(data.value(F1_A11_2).testFlag("Mueller2014"));

        data.setValue(F1_A11_1, Variant::Root(Variant::Type::Flags));
        data.setValue(BaB_A11_1, Variant::Root(20.0));
        data.setValue(BaG_A11_1, Variant::Root(18.0));
        data.setValue(BaR_A11_1, Variant::Root(14.0));
        data.setValue(L_A11_1, Variant::Root(100.0));
        data.setValue(Le_A11_1, Variant::Root(150.0));
        data.setValue(IrB_A11_1, Variant::Root(0.7));
        data.setValue(IrG_A11_1, Variant::Root(0.8));
        data.setValue(IrR_A11_1, Variant::Root(0.9));
        data.setValue(IrBe_A11_1, Variant::Root(0.69));
        data.setValue(IrGe_A11_1, Variant::Root(0.79));
        data.setValue(IrRe_A11_1, Variant::Root(0.89));
        data.setValue(OsB_A11_1, Variant::Root());
        data.setValue(OsG_A11_1, Variant::Root(0.200));
        data.setValue(OsR_A11_1, Variant::Root(0.100));
        data.setValue(OsBe_A11_1, Variant::Root(0.303));
        data.setValue(OsGe_A11_1, Variant::Root());
        data.setValue(OsRe_A11_1, Variant::Root(0.101));
        data.setValue(ZGB_A11_1, Variant::Root(0.2));
        data.setValue(ZGG_A11_1, Variant::Root(0.3));
        data.setValue(ZGR_A11_1, Variant::Root());
        data.setValue(ZGBe_A11_1, Variant::Root(0.21));
        data.setValue(ZGGe_A11_1, Variant::Root(0.31));
        data.setValue(ZGRe_A11_1, Variant::Root());
        filter->process(A11_1, data);
        QVERIFY(!FP::defined(data.value(BaB_A11_1).toReal()));
        QVERIFY(!FP::defined(data.value(BaG_A11_1).toReal()));
        QVERIFY(!FP::defined(data.value(BaR_A11_1).toReal()));

        data.setValue(F1_A11_1, Variant::Root(Variant::Type::Flags));
        data.setValue(BaB_A11_1, Variant::Root(20.0));
        data.setValue(BaG_A11_1, Variant::Root(18.0));
        data.setValue(BaR_A11_1, Variant::Root(14.0));
        data.setValue(L_A11_1, Variant::Root(100.0));
        data.setValue(Le_A11_1, Variant::Root(150.0));
        data.setValue(IrB_A11_1, Variant::Root());
        data.setValue(IrG_A11_1, Variant::Root());
        data.setValue(IrR_A11_1, Variant::Root());
        data.setValue(IrBe_A11_1, Variant::Root());
        data.setValue(IrGe_A11_1, Variant::Root());
        data.setValue(IrRe_A11_1, Variant::Root());
        data.setValue(OaB_A11_1, Variant::Root(1.001));
        data.setValue(OaG_A11_1, Variant::Root(2.002));
        data.setValue(OaR_A11_1, Variant::Root(3.003));
        data.setValue(OaBe_A11_1, Variant::Root(1.002));
        data.setValue(OaGe_A11_1, Variant::Root(2.004));
        data.setValue(OaRe_A11_1, Variant::Root(3.006));
        data.setValue(OsB_A11_1, Variant::Root(0.300));
        data.setValue(OsG_A11_1, Variant::Root(0.200));
        data.setValue(OsR_A11_1, Variant::Root(0.100));
        data.setValue(OsBe_A11_1, Variant::Root(0.303));
        data.setValue(OsGe_A11_1, Variant::Root(0.202));
        data.setValue(OsRe_A11_1, Variant::Root(0.101));
        data.setValue(ZGB_A11_1, Variant::Root(0.2));
        data.setValue(ZGG_A11_1, Variant::Root(0.3));
        data.setValue(ZGR_A11_1, Variant::Root(0.4));
        data.setValue(ZGBe_A11_1, Variant::Root(0.21));
        data.setValue(ZGGe_A11_1, Variant::Root(0.31));
        data.setValue(ZGRe_A11_1, Variant::Root(0.41));
        filter->process(A11_1, data);
        QVERIFY(fabs(data.value(BaB_A11_1).toDouble() -
                             calculateAbsorptionDA(50.0, 1.001, 1.002, 0.300, 0.303, 0.2, 0.21,
                                                   467)) < 1E-4);
        QVERIFY(fabs(data.value(BaG_A11_1).toDouble() -
                             calculateAbsorptionDA(50.0, 2.002, 2.004, 0.200, 0.202, 0.3, 0.31,
                                                   530)) < 1E-4);
        QVERIFY(fabs(data.value(BaR_A11_1).toDouble() -
                             calculateAbsorptionDA(50.0, 3.003, 3.006, 0.100, 0.101, 0.4, 0.41,
                                                   660)) < 1E-4);
        QVERIFY(data.value(F1_A11_1).testFlag("Mueller2014"));

        delete filter;
    }

    void dynamicOptions()
    {
        ComponentOptions options(component->getOptions());

        qobject_cast<DynamicInputOption *>(options.get("delta-sf"))->set(9.0);
        qobject_cast<DynamicInputOption *>(options.get("delta-af"))->set(0.02);
        qobject_cast<DynamicInputOption *>(options.get("gf"))->set(0.8);
        qobject_cast<DynamicInputOption *>(options.get("etaf"))->set(0.3);
        qobject_cast<DynamicInputOption *>(options.get("transmittance-start"))->set(0.7);
        qobject_cast<DynamicInputOption *>(options.get("transmittance-end"))->set(0.69);
        qobject_cast<DynamicInputOption *>(options.get("length-start"))->set(200.0);
        qobject_cast<DynamicInputOption *>(options.get("length-end"))->set(250.0);
        qobject_cast<DynamicInputOption *>(options.get("deltas-start"))->set(0.100);
        qobject_cast<DynamicInputOption *>(options.get("deltas-end"))->set(0.101);
        qobject_cast<DynamicInputOption *>(options.get("deltaa-start"))->set(FP::undefined());
        qobject_cast<DynamicInputOption *>(options.get("deltaa-end"))->set(FP::undefined());
        qobject_cast<DynamicInputOption *>(options.get("asymmetry-start"))->set(0.2);
        qobject_cast<DynamicInputOption *>(options.get("asymmetry-end"))->set(0.3);
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("absorption"))->set("", "",
                                                                                       "A.*");

        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "BaG_A11"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "IrG_A11"), &controller);
        QVERIFY(controller.contents.isEmpty());

        SequenceName A_A11("brw", "raw", "A_A11");
        filter->unhandled(A_A11, &controller);

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << A_A11,
                                                 QSet<SequenceName>()));
        QCOMPARE(idL.size(), 1);
        int id = idL.at(0);

        {
            Variant::Root meta;
            meta.write().metadataReal("Wavelength").setDouble(550.0);
            data.setValue(A_A11.toMeta(), meta);

            filter->processMeta(id, data);
            QCOMPARE(data.value(A_A11.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
        }

        data.setValue(A_A11, Variant::Root(10.0));
        filter->process(id, data);
        QVERIFY(fabs(data.value(A_A11).toDouble() -
                             calculateAbsorption(50.0, 0.7, 0.69, 0.100, 0.101, 0.2, 0.3, 550, 9.0,
                                                 0.02, 0.8, 0.3)) < 1E-4);

        data.setValue(A_A11, Variant::Root());
        filter->process(id, data);
        QVERIFY(!FP::defined(data.value(A_A11).toDouble()));

        delete filter;

        data = SequenceSegment();
        options = component->getOptions();
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->add("A11");
        qobject_cast<DynamicInputOption *>(options.get("delta-l"))->set(30.0);
        filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        controller.contents.clear();

        filter->unhandled(SequenceName("brw", "raw", "BaG_A12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "IrG_A12"), &controller);
        QVERIFY(controller.contents.isEmpty());

        SequenceName BaB_A11("brw", "raw", "BaB_A11");
        SequenceName IrB_A11("brw", "raw", "IrB_A11");
        SequenceName IrBe_A11("brw", "raw", "IrB_A11", {SequenceName::flavor_end});
        SequenceName F1_A11("brw", "raw", "F1_A11");
        SequenceName L_A11("brw", "raw", "L_A11");
        SequenceName Le_A11("brw", "raw", "L_A11", {SequenceName::flavor_end});
        SequenceName OsB_A11("brw", "raw", "OsB_A11");
        SequenceName OsBe_A11("brw", "raw", "OsB_A11", {SequenceName::flavor_end});
        SequenceName OaB_A11("brw", "raw", "OaB_A11");
        SequenceName OaBe_A11("brw", "raw", "OaB_A11", {SequenceName::flavor_end});
        SequenceName ZGB_A11("brw", "raw", "ZGB_A11");
        SequenceName ZGBe_A11("brw", "raw", "ZGB_A11", {SequenceName::flavor_end});
        filter->unhandled(BaB_A11, &controller);
        filter->unhandled(IrB_A11, &controller);
        filter->unhandled(L_A11, &controller);
        filter->unhandled(Le_A11, &controller);
        filter->unhandled(OsB_A11, &controller);
        filter->unhandled(OsBe_A11, &controller);
        filter->unhandled(ZGB_A11, &controller);
        filter->unhandled(ZGBe_A11, &controller);

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << BaB_A11 << F1_A11,
                                                 QSet<SequenceName>() <<
                                                         IrB_A11 <<
                                                         IrBe_A11 <<
                                                         L_A11 <<
                                                         Le_A11 <<
                                                         OsB_A11 <<
                                                         OsBe_A11 <<
                                                         OaB_A11 <<
                                                         OaBe_A11 <<
                                                         ZGB_A11 <<
                                                         ZGBe_A11));
        QCOMPARE(idL.size(), 1);
        id = idL.at(0);

        {
            Variant::Root meta;
            meta.write().metadataReal("Wavelength").setDouble(467.0);
            data.setValue(BaB_A11.toMeta(), meta);

            data.setValue(F1_A11.toMeta(), Variant::Root(Variant::Type::MetadataFlags));

            filter->processMeta(id, data);
            QCOMPARE(data.value(BaB_A11.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
            QVERIFY(data.value(F1_A11.toMeta()).metadataSingleFlag("Mueller2014").exists());
        }

        data.setValue(F1_A11, Variant::Root(Variant::Type::Flags));
        data.setValue(BaB_A11, Variant::Root(10.0));
        data.setValue(IrB_A11, Variant::Root(0.7));
        data.setValue(IrBe_A11, Variant::Root(0.68));
        data.setValue(OsB_A11, Variant::Root(0.30));
        data.setValue(OsBe_A11, Variant::Root(0.30));
        data.setValue(ZGBe_A11, Variant::Root(0.3));
        filter->process(id, data);
        QVERIFY(fabs(data.value(BaB_A11).toDouble() -
                             calculateAbsorption(30.0, 0.7, 0.68, 0.3, 0.3, 0.3, 0.3, 467)) < 1E-4);
        QVERIFY(data.value(F1_A11).testFlag("Mueller2014"));

        delete filter;
    }

    void predefined()
    {
        ComponentOptions options(component->getOptions());

        SequenceName BaB_A11("brw", "raw", "BaB_A11");
        SequenceName IrB_A11("brw", "raw", "IrB_A11");
        SequenceName IrBe_A11("brw", "raw", "IrB_A11", {SequenceName::flavor_end});
        SequenceName F1_A11("brw", "raw", "F1_A11");
        SequenceName L_A11("brw", "raw", "L_A11");
        SequenceName Le_A11("brw", "raw", "L_A11", {SequenceName::flavor_end});
        SequenceName OsB_A11("brw", "raw", "OsB_A11");
        SequenceName OsBe_A11("brw", "raw", "OsB_A11", {SequenceName::flavor_end});
        SequenceName OaB_A11("brw", "raw", "OaB_A11");
        SequenceName OaBe_A11("brw", "raw", "OaB_A11", {SequenceName::flavor_end});
        SequenceName ZGB_A11("brw", "raw", "ZGB_A11");
        SequenceName ZGBe_A11("brw", "raw", "ZGB_A11", {SequenceName::flavor_end});
        QList<SequenceName> input;
        input << BaB_A11;

        SegmentProcessingStage *filter =
                component->createBasicFilterPredefined(options, FP::undefined(), FP::undefined(),
                                                       input);
        QVERIFY(filter != NULL);
        TestController controller;

        QCOMPARE(filter->requestedInputs(),
                 (SequenceName::Set{F1_A11, BaB_A11, IrB_A11, IrBe_A11, L_A11, Le_A11, OsB_A11,
                                    OsBe_A11, OaB_A11, OaBe_A11, ZGB_A11, ZGBe_A11}));

        delete filter;
    }

    void editing()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["A11/Flags"] = "::F1_A11:=";
        cv["A11/Correct/Blue/Absorption"] = "::BaB_A11:=";
        cv["A11/Correct/Blue/Transmittance/Start/Input"] = "::IrB_A11:=";
        cv["A11/Correct/Blue/Transmittance/End/Input"] = "::IrB_A11:=end";
        cv["A11/Correct/Blue/Length/Start/Input"] = "::L_A11:=";
        cv["A11/Correct/Blue/Length/End/Input"] = "::L_A11:=end";
        cv["A11/Correct/Blue/ScatteringOpticalDepth/Start/Input"] = "::OsB_A11:=";
        cv["A11/Correct/Blue/ScatteringOpticalDepth/End/Input"] = "::OsB_A11:=end";
        cv["A11/Correct/Blue/AsymmetryParameter/Start/Input"] = "::ZGB_A11:=";
        cv["A11/Correct/Blue/AsymmetryParameter/End/Input"] = "::ZGB_A11:=end";
        cv["A11/Correct/Blue/DeltaSF"] = 9.0;
        cv["A11/Correct/Blue/DeltaAF"] = 0.02;
        cv["A11/Correct/Blue/GF"] = 0.8;
        cv["A11/Correct/Blue/Eta"] = 0.3;
        cv["A11/Correct/Red/Absorption"] = "::BaR_A11:=";
        cv["A11/Correct/Red/Transmittance/Start/Input"] = "::IrR_A11:=";
        cv["A11/Correct/Red/Transmittance/End/Input"] = "::IrR_A11:=end";
        cv["A11/Correct/Red/Length/Start/Input"] = "::L_A11:=";
        cv["A11/Correct/Red/Length/End/Input"] = "::L_A11:=end";
        cv["A11/Correct/Red/ScatteringOpticalDepth/Start/Input"] = "::OsR_A11:=";
        cv["A11/Correct/Red/ScatteringOpticalDepth/End/Input"] = "::OsR_A11:=end";
        cv["A11/Correct/Red/AsymmetryParameter/Start/Input"] = "::ZGR_A11:=";
        cv["A11/Correct/Red/AsymmetryParameter/End/Input"] = "::ZGR_A11:=end";

        cv["A12/Correct/Blue/Absorption"] = "::BaB_A12:=";
        cv["A12/Correct/Blue/Transmittance/Start/Input"] = "::IrB_A12:=";
        cv["A12/Correct/Blue/Transmittance/End/Input"] = "::IrB_A12:=end";
        cv["A12/Correct/Blue/Length/Start/Input"] = "::L_A12:=";
        cv["A12/Correct/Blue/Length/End/Input"] = "::L_A12:=end";
        cv["A12/Correct/Blue/ScatteringOpticalDepth/Start/Input"] = "::OsB_A12:=";
        cv["A12/Correct/Blue/ScatteringOpticalDepth/End/Input"] = "::OsB_A12:=end";
        cv["A12/Correct/Blue/AsymmetryParameter/Start/Input"] = "::ZGB_A12:=";
        cv["A12/Correct/Blue/AsymmetryParameter/End/Input"] = "::ZGB_A12:=end";
        config.emplace_back(FP::undefined(), 13.0, cv);

        cv.write().setEmpty();
        cv["A11/Correct/Blue/Absorption"] = "::BaB_A11:=";
        cv["A11/Correct/Blue/Transmittance/Start/Input"] = "::IrB_A11:=";
        cv["A11/Correct/Blue/Transmittance/End/Input"] = "::IrB_A11:=end";
        cv["A11/Correct/Blue/Length/Start/Input"] = "::L_A11:=";
        cv["A11/Correct/Blue/Length/End/Input"] = "::L_A11:=end";
        cv["A11/Correct/Blue/ScatteringOpticalDepth/Start/Input"] = "::OsB_A11:=";
        cv["A11/Correct/Blue/ScatteringOpticalDepth/End/Input"] = "::OsB_A11:=end";
        cv["A11/Correct/Blue/AsymmetryParameter/Start/Input"] = "::ZGB_A11:=";
        cv["A11/Correct/Blue/AsymmetryParameter/End/Input"] = "::ZGB_A11:=end";
        cv["A11/Correct/Blue/Black/Type"] = "Virkkula2005";
        config.emplace_back(13.0, FP::undefined(), cv);

        SegmentProcessingStage
                *filter = component->createBasicFilterEditing(12.0, 15.0, "brw", "raw", config);
        QVERIFY(filter != NULL);
        TestController controller;

        SequenceName BaB_A11("brw", "raw", "BaB_A11");
        SequenceName BaR_A11("brw", "raw", "BaR_A11");
        SequenceName IrB_A11("brw", "raw", "IrB_A11");
        SequenceName IrR_A11("brw", "raw", "IrR_A11");
        SequenceName IrBe_A11("brw", "raw", "IrB_A11", {SequenceName::flavor_end});
        SequenceName IrRe_A11("brw", "raw", "IrR_A11", {SequenceName::flavor_end});
        SequenceName F1_A11("brw", "raw", "F1_A11");
        SequenceName L_A11("brw", "raw", "L_A11");
        SequenceName Le_A11("brw", "raw", "L_A11", {SequenceName::flavor_end});
        SequenceName OsB_A11("brw", "raw", "OsB_A11");
        SequenceName OsR_A11("brw", "raw", "OsR_A11");
        SequenceName OsBe_A11("brw", "raw", "OsB_A11", {SequenceName::flavor_end});
        SequenceName OsRe_A11("brw", "raw", "OsR_A11", {SequenceName::flavor_end});
        SequenceName ZGB_A11("brw", "raw", "ZGB_A11");
        SequenceName ZGR_A11("brw", "raw", "ZGR_A11");
        SequenceName ZGBe_A11("brw", "raw", "ZGB_A11", {SequenceName::flavor_end});
        SequenceName ZGRe_A11("brw", "raw", "ZGR_A11", {SequenceName::flavor_end});

        SequenceName BaB_A12("brw", "raw", "BaB_A12");
        SequenceName IrB_A12("brw", "raw", "IrB_A12");
        SequenceName IrBe_A12("brw", "raw", "IrB_A12", {SequenceName::flavor_end});
        SequenceName L_A12("brw", "raw", "L_A12");
        SequenceName Le_A12("brw", "raw", "L_A12", {SequenceName::flavor_end});
        SequenceName OsB_A12("brw", "raw", "OsB_A12");
        SequenceName OsBe_A12("brw", "raw", "OsB_A12", {SequenceName::flavor_end});
        SequenceName ZGB_A12("brw", "raw", "ZGB_A12");
        SequenceName ZGBe_A12("brw", "raw", "ZGB_A12", {SequenceName::flavor_end});

        QCOMPARE(filter->requestedInputs(),
                 (SequenceName::Set{F1_A11, BaB_A11, BaR_A11, IrB_A11, IrR_A11, L_A11, OsB_A11,
                                    OsR_A11, ZGB_A11, ZGR_A11, BaB_A12, IrB_A12, L_A12, OsB_A12,
                                    ZGB_A12}));

        filter->unhandled(F1_A11, &controller);
        filter->unhandled(BaB_A11, &controller);
        filter->unhandled(BaR_A11, &controller);
        filter->unhandled(IrB_A11, &controller);
        filter->unhandled(IrR_A11, &controller);
        filter->unhandled(IrBe_A11, &controller);
        filter->unhandled(IrRe_A11, &controller);
        filter->unhandled(OsB_A11, &controller);
        filter->unhandled(OsR_A11, &controller);
        filter->unhandled(OsBe_A11, &controller);
        filter->unhandled(OsRe_A11, &controller);
        filter->unhandled(ZGB_A11, &controller);
        filter->unhandled(ZGR_A11, &controller);
        filter->unhandled(ZGBe_A11, &controller);
        filter->unhandled(ZGRe_A11, &controller);
        filter->unhandled(L_A11, &controller);
        filter->unhandled(Le_A11, &controller);

        filter->unhandled(BaB_A12, &controller);
        filter->unhandled(IrB_A12, &controller);
        filter->unhandled(IrBe_A12, &controller);
        filter->unhandled(OsB_A12, &controller);
        filter->unhandled(OsBe_A12, &controller);
        filter->unhandled(ZGB_A12, &controller);
        filter->unhandled(ZGBe_A12, &controller);
        filter->unhandled(L_A12, &controller);
        filter->unhandled(Le_A12, &controller);

        QList<int> idL = controller.contents
                                   .keys(TestController::ID(
                                           QSet<SequenceName>() << F1_A11 << BaB_A11 << BaR_A11,
                                           QSet<SequenceName>() <<
                                                   IrB_A11 <<
                                                   IrR_A11 <<
                                                   IrBe_A11 <<
                                                   IrRe_A11 <<
                                                   L_A11 <<
                                                   Le_A11 <<
                                                   OsB_A11 <<
                                                   OsR_A11 <<
                                                   OsBe_A11 <<
                                                   OsRe_A11 <<
                                                   ZGB_A11 <<
                                                   ZGR_A11 <<
                                                   ZGBe_A11 <<
                                                   ZGRe_A11));
        QCOMPARE(idL.size(), 1);
        int A11 = idL.at(0);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << BaB_A12,
                                                 QSet<SequenceName>() <<
                                                         IrB_A12 <<
                                                         IrBe_A12 <<
                                                         L_A12 <<
                                                         Le_A12 <<
                                                         OsB_A12 <<
                                                         OsBe_A12 <<
                                                         ZGB_A12 <<
                                                         ZGBe_A12));
        QCOMPARE(idL.size(), 1);
        int A12 = idL.at(0);

        QCOMPARE(filter->metadataBreaks(A11), QSet<double>() << 13.0);
        QCOMPARE(filter->metadataBreaks(A12), QSet<double>() << 13.0);

        SequenceSegment data;
        {
            Variant::Root meta;
            meta.write().metadataReal("Wavelength").setDouble(467.0);
            data.setValue(BaB_A11.toMeta(), meta);
            data.setValue(BaB_A12.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(650.0);
            data.setValue(BaR_A11.toMeta(), meta);

            data.setValue(F1_A11.toMeta(), Variant::Root(Variant::Type::MetadataFlags));

            filter->processMeta(A11, data);
            QCOMPARE(data.value(BaB_A11.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
            QVERIFY(data.value(F1_A11.toMeta()).metadataSingleFlag("Mueller2014").exists());

            filter->processMeta(A12, data);
            QCOMPARE(data.value(BaB_A12.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
        }

        data.setStart(12.0);
        data.setEnd(13.0);

        data.setValue(F1_A11, Variant::Root(Variant::Type::Flags));
        data.setValue(BaB_A11, Variant::Root(20.0));
        data.setValue(BaR_A11, Variant::Root(14.0));
        data.setValue(L_A11, Variant::Root(100.0));
        data.setValue(Le_A11, Variant::Root(150.0));
        data.setValue(IrB_A11, Variant::Root(0.7));
        data.setValue(IrR_A11, Variant::Root(0.9));
        data.setValue(IrBe_A11, Variant::Root(0.69));
        data.setValue(IrRe_A11, Variant::Root(0.89));
        data.setValue(OsB_A11, Variant::Root(0.300));
        data.setValue(OsR_A11, Variant::Root(0.100));
        data.setValue(OsBe_A11, Variant::Root(0.303));
        data.setValue(OsRe_A11, Variant::Root(0.101));
        data.setValue(ZGB_A11, Variant::Root(0.2));
        data.setValue(ZGR_A11, Variant::Root(0.4));
        data.setValue(ZGBe_A11, Variant::Root(0.21));
        data.setValue(ZGRe_A11, Variant::Root(0.41));
        filter->process(A11, data);
        QVERIFY(fabs(data.value(BaB_A11).toDouble() -
                             calculateAbsorption(50.0, 0.7, 0.69, 0.300, 0.303, 0.2, 0.21, 467, 9.0,
                                                 0.02, 0.8, 0.3)) < 1E-4);
        QVERIFY(fabs(data.value(BaR_A11).toDouble() -
                             calculateAbsorption(50.0, 0.9, 0.89, 0.100, 0.101, 0.4, 0.41, 650)) <
                        1E-4);
        QVERIFY(data.value(F1_A11).testFlag("Mueller2014"));

        data.setValue(BaB_A12, Variant::Root(20.0));
        data.setValue(L_A12, Variant::Root(100.0));
        data.setValue(Le_A12, Variant::Root(150.0));
        data.setValue(IrB_A12, Variant::Root(0.7));
        data.setValue(IrBe_A12, Variant::Root(0.69));
        data.setValue(OsB_A12, Variant::Root(0.300));
        data.setValue(OsBe_A12, Variant::Root(0.303));
        data.setValue(ZGB_A12, Variant::Root(0.2));
        data.setValue(ZGBe_A12, Variant::Root(0.21));
        filter->process(A12, data);
        QVERIFY(fabs(data.value(BaB_A12).toDouble() -
                             calculateAbsorption(50.0, 0.7, 0.69, 0.300, 0.303, 0.2, 0.21, 467)) <
                        1E-4);


        data = SequenceSegment();
        data.setStart(14.0);
        data.setEnd(15.0);

        data.setValue(F1_A11, Variant::Root());
        data.setValue(BaB_A11, Variant::Root(20.0));
        data.setValue(BaR_A11, Variant::Root(14.0));
        data.setValue(L_A11, Variant::Root(100.0));
        data.setValue(Le_A11, Variant::Root(150.0));
        data.setValue(IrB_A11, Variant::Root(0.7));
        data.setValue(IrR_A11, Variant::Root(0.9));
        data.setValue(IrBe_A11, Variant::Root(0.69));
        data.setValue(IrRe_A11, Variant::Root(0.89));
        data.setValue(OsB_A11, Variant::Root(0.300));
        data.setValue(OsR_A11, Variant::Root(0.100));
        data.setValue(OsBe_A11, Variant::Root(0.303));
        data.setValue(OsRe_A11, Variant::Root(0.101));
        data.setValue(ZGB_A11, Variant::Root(0.2));
        data.setValue(ZGR_A11, Variant::Root(0.4));
        data.setValue(ZGBe_A11, Variant::Root(0.21));
        data.setValue(ZGRe_A11, Variant::Root(0.41));
        filter->process(A11, data);
        CTSSolver::BlackParameterization black;
        black.type = CTSSolver::BlackParameterization::Virkkula2005;
        black.p.virkkula2005.c1 = FP::undefined();
        black.p.virkkula2005.c2 = FP::undefined();
        QVERIFY(fabs(data.value(BaB_A11).toDouble() -
                             calculateAbsorption(50.0, 0.7, 0.69, 0.300, 0.303, 0.2, 0.21, 467,
                                                 FP::undefined(), FP::undefined(), FP::undefined(),
                                                 FP::undefined(), black)) < 1E-4);
        QCOMPARE(data.value(BaR_A11).toDouble(), 14.0);
        QVERIFY(!data.value(F1_A11).exists());

        data.setValue(BaB_A12, Variant::Root(20.0));
        data.setValue(L_A12, Variant::Root(100.0));
        data.setValue(Le_A12, Variant::Root(150.0));
        data.setValue(IrB_A12, Variant::Root(0.7));
        data.setValue(IrBe_A12, Variant::Root(0.69));
        data.setValue(OsB_A12, Variant::Root(0.300));
        data.setValue(OsBe_A12, Variant::Root(0.303));
        data.setValue(ZGB_A12, Variant::Root(0.2));
        data.setValue(ZGBe_A12, Variant::Root(0.21));
        filter->process(A12, data);
        QCOMPARE(data.value(BaB_A12).toDouble(), 20.0);

        delete filter;
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
