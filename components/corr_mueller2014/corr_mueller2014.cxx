/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QRegExp>
#include <QDebug>
#include <QDebugStateSaver>

#include "core/timeutils.hxx"
#include "core/environment.hxx"

#include "corr_mueller2014.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Editing;
using namespace CPD3::Smoothing;

/* When set, metadata about the processing involved in the optical depths is 
 * preserved in the absorption metadata. */
//#define RECORD_DEPTH_PROCESSING

namespace {
struct DefaultParameters {
    double wavelength;
    double deltaSF;
    double deltaAF;
    double gF;
    double etaF;
};
static const DefaultParameters defaultParameters[] = {
        /* WL   deltaSF     deltaAF     gf          etaF */
        {467, 7.76, 0.033, 0.75, 0.2},
        {530, 7.69, 0.038, 0.75, 0.2},
        {550, 7.63, 0.029, 0.75, 0.2},
        {660, 7.34, 0.019, 0.75, 0.2},};

struct DefaultVirkkula2005 {
    double wavelength;
    double c1;
    double c2;
};
static const DefaultVirkkula2005 defaultVirkkula2005[] = {
        /* WL   C1          C2 */
        {467, 0.377, 0.640},
        {530, 0.358, 0.640},
        {574, 0.354, 0.617},
        {660, 0.352, 0.674},};

template<typename DefaultType>
static const DefaultType *getDefaults(double wavelength,
                                      const DefaultType *begin,
                                      const DefaultType *end)
{
    const DefaultType *best = begin;
    if (FP::defined(wavelength)) {
        double bestDelta = fabs(best->wavelength - wavelength);
        for (++begin; begin != end; ++begin) {
            double delta = fabs(begin->wavelength - wavelength);
            if (delta > bestDelta)
                continue;
            best = begin;
            bestDelta = delta;
        }
    }
    return best;
}

static const DefaultParameters *getDefaults(double wavelength)
{
    return getDefaults(wavelength, &defaultParameters[0],
                       &defaultParameters[sizeof(defaultParameters) /
                               sizeof(defaultParameters[0])]);
}

static const DefaultVirkkula2005 *getVirkkula2005(double wavelength)
{
    return getDefaults(wavelength, &defaultVirkkula2005[0],
                       &defaultVirkkula2005[sizeof(defaultVirkkula2005) /
                               sizeof(defaultVirkkula2005[0])]);
}


};

void CorrMueller2014::handleOptions(const ComponentOptions &options)
{
    if (options.isSet("transmittance-start")) {
        defaultTransmittanceStart =
                qobject_cast<DynamicInputOption *>(options.get("transmittance-start"))->getInput();
    } else {
        defaultTransmittanceStart = NULL;
    }

    if (options.isSet("transmittance-end")) {
        defaultTransmittanceEnd =
                qobject_cast<DynamicInputOption *>(options.get("transmittance-end"))->getInput();
    } else {
        defaultTransmittanceEnd = NULL;
    }

    if (options.isSet("length-start")) {
        defaultLengthStart =
                qobject_cast<DynamicInputOption *>(options.get("length-start"))->getInput();
    } else {
        defaultLengthStart = NULL;
    }

    if (options.isSet("length-end")) {
        defaultLengthEnd =
                qobject_cast<DynamicInputOption *>(options.get("length-end"))->getInput();
    } else {
        defaultLengthEnd = NULL;
    }

    if (options.isSet("deltas-start")) {
        defaultSODStart =
                qobject_cast<DynamicInputOption *>(options.get("deltas-start"))->getInput();
    } else {
        defaultSODStart = NULL;
    }

    if (options.isSet("deltas-end")) {
        defaultSODEnd = qobject_cast<DynamicInputOption *>(options.get("deltas-end"))->getInput();
    } else {
        defaultSODEnd = NULL;
    }

    if (options.isSet("deltaa-start")) {
        defaultAODStart =
                qobject_cast<DynamicInputOption *>(options.get("deltaa-start"))->getInput();
    } else {
        defaultAODStart = NULL;
    }

    if (options.isSet("deltaa-end")) {
        defaultAODEnd = qobject_cast<DynamicInputOption *>(options.get("deltaa-end"))->getInput();
    } else {
        defaultAODEnd = NULL;
    }

    if (options.isSet("asymmetry-start")) {
        defaultAsymmetryStart =
                qobject_cast<DynamicInputOption *>(options.get("asymmetry-start"))->getInput();
    } else {
        defaultAsymmetryStart = NULL;
    }

    if (options.isSet("asymmetry-end")) {
        defaultAsymmetryEnd =
                qobject_cast<DynamicInputOption *>(options.get("asymmetry-end"))->getInput();
    } else {
        defaultAsymmetryEnd = NULL;
    }


    if (options.isSet("delta-l")) {
        defaultDeltaLength = qobject_cast<DynamicInputOption *>(options.get("delta-l"))->getInput();
    } else {
        defaultDeltaLength = NULL;
    }
    if (options.isSet("delta-sf")) {
        defaultDeltaSF = qobject_cast<DynamicInputOption *>(options.get("delta-sf"))->getInput();
    } else {
        defaultDeltaSF = NULL;
    }
    if (options.isSet("delta-af")) {
        defaultDeltaAF = qobject_cast<DynamicInputOption *>(options.get("delta-af"))->getInput();
    } else {
        defaultDeltaAF = NULL;
    }
    if (options.isSet("gf")) {
        defaultGF = qobject_cast<DynamicInputOption *>(options.get("gf"))->getInput();
    } else {
        defaultGF = NULL;
    }
    if (options.isSet("etaf")) {
        defaultEtaF = qobject_cast<DynamicInputOption *>(options.get("etaf"))->getInput();
    } else {
        defaultEtaF = NULL;
    }


    restrictedInputs = false;

    if (options.isSet("absorption")) {
        restrictedInputs = true;

        Processing p;

        SequenceName::Component station;
        SequenceName::Component archive;
        SequenceName::Component suffix;
        SequenceName::Component wavelengthSuffix;
        SequenceName::Flavors flavors;

        CorrectionSet s;
        s.operateAbsorption = qobject_cast<DynamicSequenceSelectionOption *>(
                options.get("absorption"))->getOperator();

        for (const auto &name : s.operateAbsorption->getAllUnits()) {
            station = name.getStation();
            archive = name.getArchive();
            flavors = name.getFlavors();

            if (suffix.empty())
                suffix = Util::suffix(name.getVariable(), '_');

            if (Util::starts_with(name.getVariable(), "Ba"))
                wavelengthSuffix = name.getVariable().substr(2);
        }

        SequenceName::Flavors flavorsEnd = flavors;
        flavorsEnd.insert(SequenceName::flavor_end);

        p.operateFlags = new DynamicSequenceSelection::Single(
                SequenceName(station, archive, "F1_" + suffix, flavors));

        if (defaultTransmittanceStart == NULL) {
            s.transmittanceStart = new DynamicInput::Basic(
                    SequenceName(station, archive, "Ir" + wavelengthSuffix, flavors));
        } else {
            s.transmittanceStart = defaultTransmittanceStart->clone();
        }

        if (defaultTransmittanceEnd == NULL) {
            s.transmittanceEnd = new DynamicInput::Basic(
                    SequenceName(station, archive, "Ir" + wavelengthSuffix, flavorsEnd));
        } else {
            s.transmittanceEnd = defaultTransmittanceEnd->clone();
        }

        if (defaultLengthStart == NULL) {
            s.lengthStart = new DynamicInput::Basic(
                    SequenceName(station, archive, "L_" + suffix, flavors));
        } else {
            s.lengthStart = defaultLengthStart->clone();
        }

        if (defaultLengthEnd == NULL) {
            s.lengthEnd = new DynamicInput::Basic(
                    SequenceName(station, archive, "L_" + suffix, flavorsEnd));
        } else {
            s.lengthEnd = defaultLengthEnd->clone();
        }

        if (defaultSODStart == NULL) {
            s.sodStart = new DynamicInput::Basic(
                    SequenceName(station, archive, "Os" + wavelengthSuffix, flavors));
        } else {
            s.sodStart = defaultSODStart->clone();
        }

        if (defaultSODEnd == NULL) {
            s.sodEnd = new DynamicInput::Basic(
                    SequenceName(station, archive, "Os" + wavelengthSuffix, flavorsEnd));
        } else {
            s.sodEnd = defaultSODEnd->clone();
        }

        if (defaultAODStart == NULL) {
            s.aodStart = new DynamicInput::Basic(
                    SequenceName(station, archive, "Oa" + wavelengthSuffix, flavors));
        } else {
            s.aodStart = defaultAODStart->clone();
        }

        if (defaultAODEnd == NULL) {
            s.aodEnd = new DynamicInput::Basic(
                    SequenceName(station, archive, "Oa" + wavelengthSuffix, flavorsEnd));
        } else {
            s.aodEnd = defaultAODEnd->clone();
        }

        if (defaultAsymmetryStart == NULL) {
            s.asymmetryStart = new DynamicInput::Basic(
                    SequenceName(station, archive, "ZG" + wavelengthSuffix, flavors));
        } else {
            s.asymmetryStart = defaultAsymmetryStart->clone();
        }

        if (defaultAsymmetryEnd == NULL) {
            s.asymmetryEnd = new DynamicInput::Basic(
                    SequenceName(station, archive, "ZG" + wavelengthSuffix, flavorsEnd));
        } else {
            s.asymmetryEnd = defaultAsymmetryEnd->clone();
        }


        if (defaultDeltaLength == NULL)
            s.deltaLength = new DynamicInput::Constant;
        else
            s.deltaLength = defaultDeltaLength->clone();

        if (defaultDeltaSF == NULL)
            s.inputDeltaSF = new DynamicInput::Constant;
        else
            s.inputDeltaSF = defaultDeltaSF->clone();

        if (defaultDeltaAF == NULL)
            s.inputDeltaAF = new DynamicInput::Constant;
        else
            s.inputDeltaAF = defaultDeltaAF->clone();

        if (defaultGF == NULL)
            s.inputGF = new DynamicInput::Constant;
        else
            s.inputGF = defaultGF->clone();

        if (defaultEtaF == NULL)
            s.inputEtaF = new DynamicInput::Constant;
        else
            s.inputEtaF = defaultEtaF->clone();

        s.inputBlack = new DynamicPrimitive<CTSSolver::BlackParameterization>::Constant();
        s.inputWhite = new DynamicPrimitive<CTSSolver::WhiteParameterization>::Constant();

        p.operations.push_back(s);
        processing.push_back(p);
    }

    if (options.isSet("suffix")) {
        filterSuffixes = Util::set_from_qstring(
                qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->get());
    }
}


CorrMueller2014::CorrMueller2014()
{ Q_ASSERT(false); }

CorrMueller2014::CorrMueller2014(const ComponentOptions &options)
{
    handleOptions(options);
}

CorrMueller2014::CorrMueller2014(const ComponentOptions &options,
                                 double start,
                                 double end,
                                 const QList<SequenceName> &inputs)
{
    Q_UNUSED(start);
    Q_UNUSED(end);
    handleOptions(options);

    for (QList<SequenceName>::const_iterator unit = inputs.constBegin(), endU = inputs.constEnd();
            unit != endU;
            ++unit) {
        CorrMueller2014::unhandled(*unit, NULL);
    }
}


static CTSSolver::BlackParameterization convertBlack(const Variant::Read &v)
{
    const auto &type = v.hash("Type").toString();

    CTSSolver::BlackParameterization result;
    if (Util::equal_insensitive(type, "virkkula2005", "v2005")) {
        result.type = CTSSolver::BlackParameterization::Virkkula2005;

        double d = v.hash("C1").toDouble();
        if (FP::defined(d) && d > 0.0)
            result.p.virkkula2005.c1 = d;
        else
            result.p.virkkula2005.c1 = FP::undefined();

        d = v.hash("C2").toDouble();
        if (FP::defined(d) && d > 0.0)
            result.p.virkkula2005.c2 = d;
        else
            result.p.virkkula2005.c2 = FP::undefined();
    } else if (Util::equal_insensitive(type, "bond1999", "b1999")) {
        result.type = CTSSolver::BlackParameterization::Bond1999;
    }

    return result;
}

static CTSSolver::WhiteParameterization convertWhite(const Variant::Read &v)
{
    const auto &type = v.hash("Type").toString();

    CTSSolver::WhiteParameterization result;
    if (Util::equal_insensitive(type, "noaa", "nacl")) {
        result.type = CTSSolver::WhiteParameterization::NOAA;
    }

    return result;
}

CorrMueller2014::CorrMueller2014(double start,
                                 double end,
                                 const SequenceName::Component &station,
                                 const SequenceName::Component &archive,
                                 const ValueSegment::Transfer &config)
{
    defaultTransmittanceStart = NULL;
    defaultTransmittanceEnd = NULL;
    defaultLengthStart = NULL;
    defaultLengthEnd = NULL;
    defaultSODStart = NULL;
    defaultSODEnd = NULL;
    defaultAODStart = NULL;
    defaultAODEnd = NULL;
    defaultAsymmetryStart = NULL;
    defaultAsymmetryEnd = NULL;
    defaultDeltaLength = NULL;
    defaultDeltaSF = NULL;
    defaultDeltaAF = NULL;
    defaultGF = NULL;
    defaultEtaF = NULL;
    restrictedInputs = true;

    std::unordered_set<Variant::PathElement::HashIndex> children;
    for (const auto &add : config) {
        Util::merge(add.read().toHash().keys(), children);
    }
    children.erase(Variant::PathElement::HashIndex());

    for (const auto &child : children) {
        Processing p;

        p.operateFlags = DynamicSequenceSelection::fromConfiguration(config,
                                                                     QString("%1/Flags").arg(
                                                                             QString::fromStdString(
                                                                                     child)), start,
                                                                     end);

        p.operateFlags->registerExpected(station, archive);

        std::unordered_set<Variant::PathElement::HashIndex> operateChildren;
        for (const auto &add : config) {
            Util::merge(
                    add[QString("%1/Correct").arg(QString::fromStdString(child))].toHash().keys(),
                    operateChildren);
        }
        operateChildren.erase(Variant::PathElement::HashIndex());

        for (const auto &operate : operateChildren) {
            CorrectionSet s;

            s.operateAbsorption = DynamicSequenceSelection::fromConfiguration(config,
                                                                              QString("%1/Correct/%2/Absorption")
                                                                                      .arg(QString::fromStdString(
                                                                                              child),
                                                                                           QString::fromStdString(
                                                                                                   operate)),
                                                                              start, end);

            s.transmittanceStart = DynamicInput::fromConfiguration(config,
                                                                   QString("%1/Correct/%2/Transmittance/Start")
                                                                           .arg(QString::fromStdString(
                                                                                   child),
                                                                                QString::fromStdString(
                                                                                        operate)),
                                                                   start, end);
            s.transmittanceEnd = DynamicInput::fromConfiguration(config,
                                                                 QString("%1/Correct/%2/Transmittance/End")
                                                                         .arg(QString::fromStdString(
                                                                                 child),
                                                                              QString::fromStdString(
                                                                                      operate)),
                                                                 start, end);
            s.lengthStart = DynamicInput::fromConfiguration(config,
                                                            QString("%1/Correct/%2/Length/Start").arg(
                                                                    QString::fromStdString(child),
                                                                    QString::fromStdString(
                                                                            operate)), start, end);
            s.lengthEnd = DynamicInput::fromConfiguration(config,
                                                          QString("%1/Correct/%2/Length/End").arg(
                                                                  QString::fromStdString(child),
                                                                  QString::fromStdString(operate)),
                                                          start, end);
            s.deltaLength = DynamicInput::fromConfiguration(config,
                                                            QString("%1/Correct/%2/Length/Delta").arg(
                                                                    QString::fromStdString(child),
                                                                    QString::fromStdString(
                                                                            operate)), start, end);
            s.sodStart = DynamicInput::fromConfiguration(config,
                                                         QString("%1/Correct/%2/ScatteringOpticalDepth/Start")
                                                                 .arg(QString::fromStdString(child),
                                                                      QString::fromStdString(
                                                                              operate)), start,
                                                         end);
            s.sodEnd = DynamicInput::fromConfiguration(config,
                                                       QString("%1/Correct/%2/ScatteringOpticalDepth/End")
                                                               .arg(QString::fromStdString(child),
                                                                    QString::fromStdString(
                                                                            operate)), start, end);
            s.aodStart = DynamicInput::fromConfiguration(config,
                                                         QString("%1/Correct/%2/AbsorptionOpticalDepth/Start")
                                                                 .arg(QString::fromStdString(child),
                                                                      QString::fromStdString(
                                                                              operate)), start,
                                                         end);
            s.aodEnd = DynamicInput::fromConfiguration(config,
                                                       QString("%1/Correct/%2/AbsorptionOpticalDepth/End")
                                                               .arg(QString::fromStdString(child),
                                                                    QString::fromStdString(
                                                                            operate)), start, end);
            s.asymmetryStart = DynamicInput::fromConfiguration(config,
                                                               QString("%1/Correct/%2/AsymmetryParameter/Start")
                                                                       .arg(QString::fromStdString(
                                                                               child),
                                                                            QString::fromStdString(
                                                                                    operate)),
                                                               start, end);
            s.asymmetryEnd = DynamicInput::fromConfiguration(config,
                                                             QString("%1/Correct/%2/AsymmetryParameter/End")
                                                                     .arg(QString::fromStdString(
                                                                             child),
                                                                          QString::fromStdString(
                                                                                  operate)), start,
                                                             end);

            s.inputDeltaSF = DynamicInput::fromConfiguration(config,
                                                             QString("%1/Correct/%2/DeltaSF").arg(
                                                                     QString::fromStdString(child),
                                                                     QString::fromStdString(
                                                                             operate)), start, end);
            s.inputDeltaAF = DynamicInput::fromConfiguration(config,
                                                             QString("%1/Correct/%2/DeltaAF").arg(
                                                                     QString::fromStdString(child),
                                                                     QString::fromStdString(
                                                                             operate)), start, end);
            s.inputGF = DynamicInput::fromConfiguration(config, QString("%1/Correct/%2/GF").arg(
                    QString::fromStdString(child), QString::fromStdString(operate)), start, end);
            s.inputEtaF = DynamicInput::fromConfiguration(config, QString("%1/Correct/%2/Eta").arg(
                    QString::fromStdString(child), QString::fromStdString(operate)), start, end);

            s.inputBlack = TimeInputBlack::fromConfiguration(convertBlack, config,
                                                             QString("%1/Correct/%2/Black").arg(
                                                                     QString::fromStdString(child),
                                                                     QString::fromStdString(
                                                                             operate)), start, end);
            s.inputWhite = TimeInputWhite::fromConfiguration(convertWhite, config,
                                                             QString("%1/Correct/%2/White").arg(
                                                                     QString::fromStdString(child),
                                                                     QString::fromStdString(
                                                                             operate)), start, end);

            s.operateAbsorption->registerExpected(station, archive);
            s.transmittanceStart->registerExpected(station, archive);
            s.transmittanceEnd->registerExpected(station, archive);
            s.lengthStart->registerExpected(station, archive);
            s.lengthEnd->registerExpected(station, archive);
            s.sodStart->registerExpected(station, archive);
            s.sodEnd->registerExpected(station, archive);
            s.asymmetryStart->registerExpected(station, archive);
            s.asymmetryEnd->registerExpected(station, archive);
            s.deltaLength->registerExpected(station, archive);

            s.inputDeltaSF->registerExpected(station, archive);
            s.inputDeltaAF->registerExpected(station, archive);
            s.inputGF->registerExpected(station, archive);
            s.inputEtaF->registerExpected(station, archive);

            p.operations.push_back(s);
        }

        processing.push_back(p);
    }
}

CorrMueller2014::~CorrMueller2014()
{
    if (defaultTransmittanceStart != NULL)
        delete defaultTransmittanceStart;
    if (defaultTransmittanceEnd != NULL)
        delete defaultTransmittanceEnd;
    if (defaultLengthStart != NULL)
        delete defaultLengthStart;
    if (defaultLengthEnd != NULL)
        delete defaultLengthEnd;
    if (defaultSODStart != NULL)
        delete defaultSODStart;
    if (defaultSODEnd != NULL)
        delete defaultSODEnd;
    if (defaultAODStart != NULL)
        delete defaultAODStart;
    if (defaultAODEnd != NULL)
        delete defaultAODEnd;
    if (defaultAsymmetryStart != NULL)
        delete defaultAsymmetryStart;
    if (defaultAsymmetryEnd != NULL)
        delete defaultAsymmetryEnd;
    if (defaultDeltaLength != NULL)
        delete defaultDeltaLength;
    if (defaultDeltaSF != NULL)
        delete defaultDeltaSF;
    if (defaultDeltaAF != NULL)
        delete defaultDeltaAF;
    if (defaultGF != NULL)
        delete defaultGF;
    if (defaultEtaF != NULL)
        delete defaultEtaF;

    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        delete p->operateFlags;
        for (std::vector<CorrectionSet>::const_iterator s = p->operations.begin(),
                endOp = p->operations.end(); s != endOp; ++s) {
            delete s->operateAbsorption;
            delete s->transmittanceStart;
            delete s->transmittanceEnd;
            delete s->lengthStart;
            delete s->lengthEnd;
            delete s->sodStart;
            delete s->sodEnd;
            delete s->aodStart;
            delete s->aodEnd;
            delete s->asymmetryStart;
            delete s->asymmetryEnd;
            delete s->deltaLength;
            delete s->inputDeltaSF;
            delete s->inputDeltaAF;
            delete s->inputGF;
            delete s->inputEtaF;
            delete s->inputBlack;
            delete s->inputWhite;
        }
    }
}

void CorrMueller2014::unhandled(const SequenceName &unit,
                                SegmentProcessingStage::SequenceHandlerControl *control)
{
    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        for (int s = 0, max = (int) processing[id].operations.size(); s < max; s++) {
            if (processing[id].operations[s].transmittanceStart->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
            if (processing[id].operations[s].transmittanceEnd->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
            if (processing[id].operations[s].lengthStart->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
            if (processing[id].operations[s].lengthEnd->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
            if (processing[id].operations[s].sodStart->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
            if (processing[id].operations[s].sodEnd->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
            if (processing[id].operations[s].aodStart->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
            if (processing[id].operations[s].aodEnd->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
            if (processing[id].operations[s].asymmetryStart->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
            if (processing[id].operations[s].asymmetryEnd->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
            if (processing[id].operations[s].deltaLength->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
            if (processing[id].operations[s].inputDeltaSF->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
            if (processing[id].operations[s].inputDeltaAF->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
            if (processing[id].operations[s].inputGF->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
            if (processing[id].operations[s].inputEtaF->registerInput(unit)) {
                if (control != NULL)
                    control->inputUnit(unit, id);
            }
        }
    }
    if (defaultTransmittanceStart != NULL &&
            defaultTransmittanceStart->registerInput(unit) &&
            control != NULL)
        control->deferHandling(unit);
    if (defaultTransmittanceEnd != NULL &&
            defaultTransmittanceEnd->registerInput(unit) &&
            control != NULL)
        control->deferHandling(unit);
    if (defaultLengthStart != NULL && defaultLengthStart->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultLengthEnd != NULL && defaultLengthEnd->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultSODStart != NULL && defaultSODStart->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultSODEnd != NULL && defaultSODEnd->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultAODStart != NULL && defaultAODStart->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultAODEnd != NULL && defaultAODEnd->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultAsymmetryStart != NULL &&
            defaultAsymmetryStart->registerInput(unit) &&
            control != NULL)
        control->deferHandling(unit);
    if (defaultAsymmetryEnd != NULL && defaultAsymmetryEnd->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultDeltaLength != NULL && defaultDeltaLength->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultDeltaSF != NULL && defaultDeltaSF->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultDeltaAF != NULL && defaultDeltaAF->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultGF != NULL && defaultGF->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultEtaF != NULL && defaultEtaF->registerInput(unit) && control != NULL)
        control->deferHandling(unit);

    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        if (processing[id].operateFlags->registerInput(unit)) {
            if (control != NULL)
                control->filterUnit(unit, id);
            return;
        }
        for (int s = 0, max = (int) processing[id].operations.size(); s < max; s++) {
            if (processing[id].operations[s].operateAbsorption->registerInput(unit)) {
                if (control != NULL)
                    control->filterUnit(unit, id);
                return;
            }
        }
    }

    if (restrictedInputs)
        return;

    auto suffix = Util::suffix(unit.getVariable(), '_');
    if (suffix.empty())
        return;
    if (!filterSuffixes.empty() && !filterSuffixes.count(suffix))
        return;

    if (!Util::starts_with(unit.getVariable(), "Ba")) {
        if (control != NULL) {
            if (Util::starts_with(unit.getVariable(), "Ir") ||
                    Util::starts_with(unit.getVariable(), "Os") ||
                    Util::starts_with(unit.getVariable(), "Oa") ||
                    Util::starts_with(unit.getVariable(), "ZG") ||
                    Util::starts_with(unit.getVariable(), "L_"))
                control->deferHandling(unit);
        }
        return;
    }

    if (unit.hasFlavor("cover") ||
            unit.hasFlavor("stats") ||
            unit.hasFlavor(SequenceName::flavor_end))
        return;

    SequenceName suffixUnit(unit.getStation(), unit.getArchive(), suffix, unit.getFlavors());

    auto wavelengthSuffix = unit.getVariable().substr(2);
    SequenceName transmittanceStartUnit
            (unit.getStation(), unit.getArchive(), "Ir" + wavelengthSuffix,
             unit.getFlavors());
    SequenceName transmittanceEndUnit(transmittanceStartUnit.withFlavor(SequenceName::flavor_end));
    SequenceName sodStartUnit
            (unit.getStation(), unit.getArchive(), "Os" + wavelengthSuffix,
             unit.getFlavors());
    SequenceName sodEndUnit(sodStartUnit.withFlavor(SequenceName::flavor_end));
    SequenceName aodStartUnit
            (unit.getStation(), unit.getArchive(), "Oa" + wavelengthSuffix,
             unit.getFlavors());
    SequenceName aodEndUnit(aodStartUnit.withFlavor(SequenceName::flavor_end));
    SequenceName asymmetryStartUnit
            (unit.getStation(), unit.getArchive(), "ZG" + wavelengthSuffix,
             unit.getFlavors());
    SequenceName asymmetryEndUnit(asymmetryStartUnit.withFlavor(SequenceName::flavor_end));
    SequenceName lengthStartUnit
            (unit.getStation(), unit.getArchive(), "L_" + suffix, unit.getFlavors());
    SequenceName lengthEndUnit(lengthStartUnit.withFlavor(SequenceName::flavor_end));

    SequenceName::Set inputUnits;
    inputUnits.insert(unit);
    inputUnits.insert(transmittanceStartUnit);
    inputUnits.insert(transmittanceEndUnit);
    inputUnits.insert(sodStartUnit);
    inputUnits.insert(sodEndUnit);
    inputUnits.insert(aodStartUnit);
    inputUnits.insert(aodEndUnit);
    inputUnits.insert(asymmetryStartUnit);
    inputUnits.insert(asymmetryEndUnit);
    inputUnits.insert(lengthStartUnit);
    inputUnits.insert(lengthEndUnit);

    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        if (processing[id].suffix != suffixUnit)
            continue;

        CorrectionSet s;

        s.operateAbsorption = new DynamicSequenceSelection::Single(unit);

        if (defaultTransmittanceStart == NULL) {
            s.transmittanceStart = new DynamicInput::Basic(transmittanceStartUnit);
        } else {
            s.transmittanceStart = defaultTransmittanceStart->clone();
        }

        if (defaultTransmittanceEnd == NULL) {
            s.transmittanceEnd = new DynamicInput::Basic(transmittanceEndUnit);
        } else {
            s.transmittanceEnd = defaultTransmittanceEnd->clone();
        }

        if (defaultLengthStart == NULL) {
            s.lengthStart = new DynamicInput::Basic(lengthStartUnit);
        } else {
            s.lengthStart = defaultLengthStart->clone();
        }

        if (defaultLengthEnd == NULL) {
            s.lengthEnd = new DynamicInput::Basic(lengthEndUnit);
        } else {
            s.lengthEnd = defaultLengthEnd->clone();
        }

        if (defaultSODStart == NULL) {
            s.sodStart = new DynamicInput::Basic(sodStartUnit);
        } else {
            s.sodStart = defaultSODStart->clone();
        }

        if (defaultSODEnd == NULL) {
            s.sodEnd = new DynamicInput::Basic(sodEndUnit);
        } else {
            s.sodEnd = defaultSODEnd->clone();
        }

        if (defaultAODStart == NULL) {
            s.aodStart = new DynamicInput::Basic(aodStartUnit);
        } else {
            s.aodStart = defaultAODStart->clone();
        }

        if (defaultAODEnd == NULL) {
            s.aodEnd = new DynamicInput::Basic(aodEndUnit);
        } else {
            s.aodEnd = defaultAODEnd->clone();
        }

        if (defaultAsymmetryStart == NULL) {
            s.asymmetryStart = new DynamicInput::Basic(asymmetryStartUnit);
        } else {
            s.asymmetryStart = defaultAsymmetryStart->clone();
        }

        if (defaultAsymmetryEnd == NULL) {
            s.asymmetryEnd = new DynamicInput::Basic(asymmetryEndUnit);
        } else {
            s.asymmetryEnd = defaultAsymmetryEnd->clone();
        }

        if (defaultDeltaLength == NULL)
            s.deltaLength = new DynamicInput::Constant;
        else
            s.deltaLength = defaultDeltaLength->clone();

        if (defaultDeltaSF == NULL)
            s.inputDeltaSF = new DynamicInput::Constant;
        else
            s.inputDeltaSF = defaultDeltaSF->clone();

        if (defaultDeltaAF == NULL)
            s.inputDeltaAF = new DynamicInput::Constant;
        else
            s.inputDeltaAF = defaultDeltaAF->clone();

        if (defaultGF == NULL)
            s.inputGF = new DynamicInput::Constant;
        else
            s.inputGF = defaultGF->clone();

        if (defaultEtaF == NULL)
            s.inputEtaF = new DynamicInput::Constant;
        else
            s.inputEtaF = defaultEtaF->clone();

        for (const auto &n : inputUnits) {
            s.transmittanceStart->registerInput(n);
            s.transmittanceEnd->registerInput(n);
            s.lengthStart->registerInput(n);
            s.lengthEnd->registerInput(n);
            s.sodStart->registerInput(n);
            s.sodEnd->registerInput(n);
            s.aodStart->registerInput(n);
            s.aodEnd->registerInput(n);
            s.asymmetryStart->registerInput(n);
            s.asymmetryEnd->registerInput(n);
            s.deltaLength->registerInput(n);
            s.inputDeltaSF->registerInput(n);
            s.inputDeltaAF->registerInput(n);
            s.inputGF->registerInput(n);
            s.inputEtaF->registerInput(n);
        }

        s.inputBlack = new DynamicPrimitive<CTSSolver::BlackParameterization>::Constant();
        s.inputWhite = new DynamicPrimitive<CTSSolver::WhiteParameterization>::Constant();

        processing[id].operations.push_back(s);

        if (control) {
            for (const auto &n : s.transmittanceStart->getUsedInputs()) {
                control->inputUnit(n, id);
            }
            for (const auto &n : s.transmittanceEnd->getUsedInputs()) {
                control->inputUnit(n, id);
            }
            for (const auto &n : s.lengthStart->getUsedInputs()) {
                control->inputUnit(n, id);
            }
            for (const auto &n : s.lengthEnd->getUsedInputs()) {
                control->inputUnit(n, id);
            }
            for (const auto &n : s.sodStart->getUsedInputs()) {
                control->inputUnit(n, id);
            }
            for (const auto &n : s.sodEnd->getUsedInputs()) {
                control->inputUnit(n, id);
            }
            for (const auto &n : s.aodStart->getUsedInputs()) {
                control->inputUnit(n, id);
            }
            for (const auto &n : s.aodEnd->getUsedInputs()) {
                control->inputUnit(n, id);
            }
            for (const auto &n : s.asymmetryStart->getUsedInputs()) {
                control->inputUnit(n, id);
            }
            for (const auto &n : s.asymmetryEnd->getUsedInputs()) {
                control->inputUnit(n, id);
            }
            for (const auto &n : s.deltaLength->getUsedInputs()) {
                control->inputUnit(n, id);
            }
            for (const auto &n : s.inputDeltaSF->getUsedInputs()) {
                control->inputUnit(n, id);
            }
            for (const auto &n : s.inputDeltaAF->getUsedInputs()) {
                control->inputUnit(n, id);
            }
            for (const auto &n : s.inputGF->getUsedInputs()) {
                control->inputUnit(n, id);
            }
            for (const auto &n : s.inputEtaF->getUsedInputs()) {
                control->inputUnit(n, id);
            }

            control->filterUnit(unit, id);
        }
        return;
    }

    Processing p;
    p.suffix = suffixUnit;

    SequenceName flagUnit = unit;
    flagUnit.setVariable("F1_" + suffix);
    inputUnits.insert(flagUnit);

    p.operateFlags = new DynamicSequenceSelection::Single(flagUnit);
    for (const auto &n : inputUnits) {
        p.operateFlags->registerInput(n);
    }


    CorrectionSet s;

    s.operateAbsorption = new DynamicSequenceSelection::Single(unit);

    if (defaultTransmittanceStart == NULL) {
        s.transmittanceStart = new DynamicInput::Basic(transmittanceStartUnit);
    } else {
        s.transmittanceStart = defaultTransmittanceStart->clone();
    }

    if (defaultTransmittanceEnd == NULL) {
        s.transmittanceEnd = new DynamicInput::Basic(transmittanceEndUnit);
    } else {
        s.transmittanceEnd = defaultTransmittanceEnd->clone();
    }

    if (defaultLengthStart == NULL) {
        s.lengthStart = new DynamicInput::Basic(lengthStartUnit);
    } else {
        s.lengthStart = defaultLengthStart->clone();
    }

    if (defaultLengthEnd == NULL) {
        s.lengthEnd = new DynamicInput::Basic(lengthEndUnit);
    } else {
        s.lengthEnd = defaultLengthEnd->clone();
    }

    if (defaultSODStart == NULL) {
        s.sodStart = new DynamicInput::Basic(sodStartUnit);
    } else {
        s.sodStart = defaultSODStart->clone();
    }

    if (defaultSODEnd == NULL) {
        s.sodEnd = new DynamicInput::Basic(sodEndUnit);
    } else {
        s.sodEnd = defaultSODEnd->clone();
    }

    if (defaultAODStart == NULL) {
        s.aodStart = new DynamicInput::Basic(aodStartUnit);
    } else {
        s.aodStart = defaultAODStart->clone();
    }

    if (defaultAODEnd == NULL) {
        s.aodEnd = new DynamicInput::Basic(aodEndUnit);
    } else {
        s.aodEnd = defaultAODEnd->clone();
    }

    if (defaultAsymmetryStart == NULL) {
        s.asymmetryStart = new DynamicInput::Basic(asymmetryStartUnit);
    } else {
        s.asymmetryStart = defaultAsymmetryStart->clone();
    }

    if (defaultAsymmetryEnd == NULL) {
        s.asymmetryEnd = new DynamicInput::Basic(asymmetryEndUnit);
    } else {
        s.asymmetryEnd = defaultAsymmetryEnd->clone();
    }


    if (defaultDeltaLength == NULL)
        s.deltaLength = new DynamicInput::Constant;
    else
        s.deltaLength = defaultDeltaLength->clone();

    if (defaultDeltaSF == NULL)
        s.inputDeltaSF = new DynamicInput::Constant;
    else
        s.inputDeltaSF = defaultDeltaSF->clone();

    if (defaultDeltaAF == NULL)
        s.inputDeltaAF = new DynamicInput::Constant;
    else
        s.inputDeltaAF = defaultDeltaAF->clone();

    if (defaultGF == NULL)
        s.inputGF = new DynamicInput::Constant;
    else
        s.inputGF = defaultGF->clone();

    if (defaultEtaF == NULL)
        s.inputEtaF = new DynamicInput::Constant;
    else
        s.inputEtaF = defaultEtaF->clone();

    for (const auto &n : inputUnits) {
        s.transmittanceStart->registerInput(n);
        s.transmittanceEnd->registerInput(n);
        s.lengthStart->registerInput(n);
        s.lengthEnd->registerInput(n);
        s.sodStart->registerInput(n);
        s.sodEnd->registerInput(n);
        s.aodStart->registerInput(n);
        s.aodEnd->registerInput(n);
        s.asymmetryStart->registerInput(n);
        s.asymmetryEnd->registerInput(n);
        s.deltaLength->registerInput(n);
        s.inputDeltaSF->registerInput(n);
        s.inputDeltaAF->registerInput(n);
        s.inputGF->registerInput(n);
        s.inputEtaF->registerInput(n);
    }

    s.inputBlack = new DynamicPrimitive<CTSSolver::BlackParameterization>::Constant();
    s.inputWhite = new DynamicPrimitive<CTSSolver::WhiteParameterization>::Constant();

    p.operations.push_back(s);

    int id = (int) processing.size();
    processing.push_back(p);

    if (control) {
        for (const auto &n : s.transmittanceStart->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : s.transmittanceEnd->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : s.lengthStart->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : s.lengthEnd->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : s.sodStart->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : s.sodEnd->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : s.aodStart->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : s.aodEnd->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : s.asymmetryStart->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : s.asymmetryEnd->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : s.deltaLength->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : s.inputDeltaSF->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : s.inputDeltaAF->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : s.inputGF->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : s.inputEtaF->getUsedInputs()) {
            control->inputUnit(n, id);
        }

        control->filterUnit(unit, id);
        if (unit != flagUnit)
            control->filterUnit(flagUnit, id);
    }
}

double CorrMueller2014::CorrectionSet::calculate(double deltaA,
                                                 double deltaS,
                                                 double g,
                                                 const CTSSolver::Parameters &parameters)
{
    if (!FP::defined(deltaS))
        return FP::undefined();
    if (deltaS < 1E-7)
        deltaS = 1E-7;
    return solver.solve(deltaA, deltaS, g, parameters);
}

double CorrMueller2014::CorrectionSet::calculateIr(double Ir,
                                                   double deltaS,
                                                   double g,
                                                   const CTSSolver::Parameters &parameters)
{
    Q_ASSERT(FP::defined(Ir));
    if (Ir <= 0.0)
        return FP::undefined();
    return calculate(-log(Ir), deltaS, g, parameters);
}

static const Variant::Flag FLAG_NAME = "Mueller2014";

void CorrMueller2014::process(int id, SequenceSegment &data)
{
    Processing *proc = &processing[id];

    for (const auto &i : proc->operateFlags->get(data)) {
        if (!data.exists(i))
            continue;
        auto d = data[i];
        if (!d.exists())
            continue;
        d.applyFlag(FLAG_NAME);
    }

    for (std::vector<CorrectionSet>::iterator set = proc->operations.begin(),
            endSet = proc->operations.end(); set != endSet; ++set) {
        double startIr = set->transmittanceStart->get(data);
        double endIr = set->transmittanceEnd->get(data);
        double startL = set->lengthStart->get(data);
        double endL = set->lengthEnd->get(data);
        double startDS = set->sodStart->get(data);
        double endDS = set->sodEnd->get(data);
        double startDA = set->aodStart->get(data);
        double endDA = set->aodEnd->get(data);
        double startG = set->asymmetryStart->get(data);
        double endG = set->asymmetryEnd->get(data);
        double dL = set->deltaLength->get(data);

        if (!FP::defined(dL) && FP::defined(startL) && FP::defined(endL))
            dL = endL - startL;

        if (!FP::defined(endG))
            endG = startG;
        else if (!FP::defined(startG))
            startG = endG;

        double deltaSF = set->inputDeltaSF->get(data);
        double deltaAF = set->inputDeltaAF->get(data);
        double gF = set->inputGF->get(data);
        double etaF = set->inputEtaF->get(data);

        for (const auto &i : set->operateAbsorption->get(data)) {
            double wavelength = proc->absorptionWavelength.get(i);

            const DefaultParameters *def = getDefaults(wavelength);

            CTSSolver::Parameters
                    parameters(set->inputBlack->get(data), set->inputWhite->get(data));

            if (FP::defined(deltaSF))
                parameters.deltaSF = deltaSF;
            else
                parameters.deltaSF = def->deltaSF;

            if (FP::defined(deltaAF))
                parameters.deltaAF = deltaAF;
            else
                parameters.deltaAF = def->deltaAF;

            if (FP::defined(gF))
                parameters.gF = gF;
            else
                parameters.gF = def->gF;

            if (FP::defined(etaF))
                parameters.etaF = etaF;
            else
                parameters.etaF = def->etaF;

            switch (parameters.black.type) {
            case CTSSolver::BlackParameterization::Bond1999:
                break;
            case CTSSolver::BlackParameterization::Virkkula2005: {
                const DefaultVirkkula2005 *bdef = getVirkkula2005(wavelength);
                if (!FP::defined(parameters.black.p.virkkula2005.c1))
                    parameters.black.p.virkkula2005.c1 = bdef->c1;
                if (!FP::defined(parameters.black.p.virkkula2005.c2))
                    parameters.black.p.virkkula2005.c2 = bdef->c2;
                break;
            }
            }

            switch (parameters.white.type) {
            case CTSSolver::WhiteParameterization::NOAA:
                break;
            }

            Variant::Composite::applyInplace(data[i], [&](Variant::Write &d) {
                double v = d.toDouble();
                /* Leave it undefined if it was already removed by something else
                 * even if we could calculate it */
                if (!FP::defined(v) || !FP::defined(dL) || dL < 1E-6) {
                    Variant::Composite::invalidate(d);
                    set->solver.reset();
                    return;
                }

                double delAStart;
                if (FP::defined(startIr)) {
                    delAStart = set->calculateIr(startIr, startDS, startG, parameters);
                } else {
                    delAStart = set->calculate(startDA, startDS, startG, parameters);
                }

                double delAEnd;
                if (FP::defined(endIr)) {
                    delAEnd = set->calculateIr(endIr, endDS, endG, parameters);
                } else {
                    delAEnd = set->calculate(endDA, endDS, endG, parameters);
                }

                if (FP::defined(delAStart) && FP::defined(delAEnd)) {
                    d.setDouble(((delAEnd - delAStart) / dL) * 1E6);
                } else {
                    Variant::Composite::invalidate(d);
                }
            });
        }
    }
}

#ifdef RECORD_DEPTH_PROCESSING
static void findValidProcessing( SequenceSegment &data,
        const SequenceName::Set &units, Value &output ) {
    if (output.defined())
        return;
    for (auto i : units) {
        i.setMeta();
        if (!data.exists(i))
            continue;
        Value meta(data[i]);
        if (meta.metadata("Processing").arraySize() == 0)
            continue;
        output.set(meta.metadata("Processing"));
        return;
    }
}
#endif

void CorrMueller2014::processMeta(int id, SequenceSegment &data)
{
    Processing *p = &processing[id];

    Variant::Root meta;

    meta["By"].setString("corr_mueller2014");
    meta["At"].setDouble(Time::time());
    meta["Environment"].setString(Environment::describe());
    meta["Revision"].setString(Environment::revision());

    for (std::vector<CorrectionSet>::const_iterator set = p->operations.begin(),
            endSet = p->operations.end(); set != endSet; ++set) {
#ifdef RECORD_DEPTH_PROCESSING
        Value sodProcessing;
        findValidProcessing(data, set->sodStart->getUsedInputs(),
            sodProcessing);
        findValidProcessing(data, set->sodEnd->getUsedInputs(),
            sodProcessing);
            
        Value aodProcessing;
        findValidProcessing(data, set->aodStart->getUsedInputs(),
            aodProcessing);
        findValidProcessing(data, set->aodEnd->getUsedInputs(),
            aodProcessing);
            
        Value asymmetryProcessing;
        findValidProcessing(data, set->asymmetryStart->getUsedInputs(),
            sodProcessing);
        findValidProcessing(data, set->asymmetryEnd->getUsedInputs(),
            sodProcessing);
            
        meta["Parameters"].hash("DeltaSProcessing",
            sodProcessing);
        meta["Parameters"].hash("DeltaAProcessing",
            aodProcessing);
        meta["Parameters"].hash("GProcessing",
            asymmetryProcessing);
#endif

        const auto &s = set->operateAbsorption->get(data);
        p->absorptionWavelength.incomingMeta(data, s);
        for (auto i : s) {
            i.setMeta();
            if (!data.exists(i))
                continue;

            double wavelength = p->absorptionWavelength.get(i);
            const DefaultParameters *def = getDefaults(wavelength);

            meta["Parameters"].hash("DeltaSF") =
                    set->inputDeltaSF->describe(data, FP::decimalFormat(def->deltaSF));
            meta["Parameters"].hash("DeltaAF") =
                    set->inputDeltaAF->describe(data, FP::decimalFormat(def->deltaAF));
            meta["Parameters"].hash("Gf") =
                    set->inputGF->describe(data, FP::decimalFormat(def->gF));
            meta["Parameters"].hash("EtaF") =
                    set->inputEtaF->describe(data, FP::decimalFormat(def->etaF));

            CTSSolver::BlackParameterization black(set->inputBlack->get(data));
            switch (black.type) {
            case CTSSolver::BlackParameterization::Bond1999:
                meta["Parameters"].hash("Black").hash("Type").setString("Bond1999");
                break;
            case CTSSolver::BlackParameterization::Virkkula2005: {
                meta["Parameters"].hash("Black").hash("Type").setString("Virkkula2005");

                const DefaultVirkkula2005 *bdef = getVirkkula2005(wavelength);
                if (FP::defined(black.p.virkkula2005.c1)) {
                    meta["Parameters"].hash("Black")
                                      .hash("C1")
                                      .setString(FP::decimalFormat(black.p.virkkula2005.c1));
                } else {
                    meta["Parameters"].hash("Black")
                                      .hash("C1")
                                      .setString(FP::decimalFormat(bdef->c1));
                }
                if (FP::defined(black.p.virkkula2005.c2)) {
                    meta["Parameters"].hash("Black")
                                      .hash("C2")
                                      .setString(FP::decimalFormat(black.p.virkkula2005.c2));
                } else {
                    meta["Parameters"].hash("Black")
                                      .hash("C2")
                                      .setString(FP::decimalFormat(bdef->c2));
                }
                break;
            }
            }

            CTSSolver::WhiteParameterization white(set->inputWhite->get(data));
            switch (white.type) {
            case CTSSolver::WhiteParameterization::NOAA:
                meta["Parameters"].hash("White").hash("Type").setString("NOAA");
                break;
            }

            data[i].metadata("Processing").toArray().after_back().set(meta);
            clearPropagatedSmoothing(data[i].metadata("Smoothing"));
        }
    }

    for (auto i : p->operateFlags->get(data)) {
        i.setMeta();
        if (!data.exists(i))
            continue;
        auto flagMeta = data[i].metadataSingleFlag(FLAG_NAME);
        flagMeta.hash("Origin").toArray().after_back().setString("corr_mueller2014");
        flagMeta.hash("Description").setString("Mueller 2014 (CTS) correction applied");
    }
}


SequenceName::Set CorrMueller2014::requestedInputs()
{
    SequenceName::Set out;
    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        Util::merge(p->operateFlags->getAllUnits(), out);
        for (std::vector<CorrectionSet>::const_iterator s = p->operations.begin(),
                endS = p->operations.end(); s != endS; ++s) {
            Util::merge(s->operateAbsorption->getAllUnits(), out);
            Util::merge(s->transmittanceStart->getUsedInputs(), out);
            Util::merge(s->transmittanceEnd->getUsedInputs(), out);
            Util::merge(s->lengthStart->getUsedInputs(), out);
            Util::merge(s->lengthEnd->getUsedInputs(), out);
            Util::merge(s->sodStart->getUsedInputs(), out);
            Util::merge(s->sodEnd->getUsedInputs(), out);
            Util::merge(s->aodStart->getUsedInputs(), out);
            Util::merge(s->aodEnd->getUsedInputs(), out);
            Util::merge(s->asymmetryStart->getUsedInputs(), out);
            Util::merge(s->asymmetryEnd->getUsedInputs(), out);
            Util::merge(s->deltaLength->getUsedInputs(), out);
            Util::merge(s->inputDeltaSF->getUsedInputs(), out);
            Util::merge(s->inputDeltaAF->getUsedInputs(), out);
            Util::merge(s->inputGF->getUsedInputs(), out);
            Util::merge(s->inputEtaF->getUsedInputs(), out);
        }
    }
    return out;
}

QSet<double> CorrMueller2014::metadataBreaks(int id)
{
    Processing *p = &processing[id];
    QSet<double> result;
    Util::merge(p->operateFlags->getChangedPoints(), result);
    for (std::vector<CorrectionSet>::const_iterator s = p->operations.begin(),
            end = p->operations.end(); s != end; ++s) {
        Util::merge(s->operateAbsorption->getChangedPoints(), result);
        Util::merge(s->transmittanceStart->getChangedPoints(), result);
        Util::merge(s->transmittanceEnd->getChangedPoints(), result);
        Util::merge(s->lengthStart->getChangedPoints(), result);
        Util::merge(s->lengthEnd->getChangedPoints(), result);
        Util::merge(s->sodStart->getChangedPoints(), result);
        Util::merge(s->sodEnd->getChangedPoints(), result);
        Util::merge(s->aodStart->getChangedPoints(), result);
        Util::merge(s->aodEnd->getChangedPoints(), result);
        Util::merge(s->asymmetryStart->getChangedPoints(), result);
        Util::merge(s->asymmetryEnd->getChangedPoints(), result);
        Util::merge(s->deltaLength->getChangedPoints(), result);
        Util::merge(s->inputDeltaSF->getChangedPoints(), result);
        Util::merge(s->inputDeltaAF->getChangedPoints(), result);
        Util::merge(s->inputGF->getChangedPoints(), result);
        Util::merge(s->inputEtaF->getChangedPoints(), result);
        Util::merge(s->inputBlack->getChangedPoints(), result);
        Util::merge(s->inputWhite->getChangedPoints(), result);
    }
    return result;
}

QDataStream &operator<<(QDataStream &stream, const CTSSolver::BlackParameterization &param)
{
    stream << (quint8) param.type;
    switch (param.type) {
    case CTSSolver::BlackParameterization::Bond1999:
        break;
    case CTSSolver::BlackParameterization::Virkkula2005:
        stream << param.p.virkkula2005.c1 << param.p.virkkula2005.c2;
        break;
    }
    return stream;
}

QDataStream &operator>>(QDataStream &stream, CTSSolver::BlackParameterization &param)
{
    quint8 i8;
    stream >> i8;
    switch (i8) {
    case (quint8) CTSSolver::BlackParameterization::Bond1999:
        param.type = CTSSolver::BlackParameterization::Bond1999;
        break;
    case (quint8) CTSSolver::BlackParameterization::Virkkula2005:
        param.type = CTSSolver::BlackParameterization::Virkkula2005;
        stream >> param.p.virkkula2005.c1 >> param.p.virkkula2005.c2;
        break;
    default:
        Q_ASSERT(false);
        break;
    }
    return stream;
}

QDebug operator<<(QDebug stream, const CTSSolver::BlackParameterization &param)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    switch (param.type) {
    case CTSSolver::BlackParameterization::Bond1999:
        stream << "Bond1999";
        break;
    case CTSSolver::BlackParameterization::Virkkula2005:
        stream << "Virkkula2005(" << param.p.virkkula2005.c1 << ',' << param.p.virkkula2005.c2
               << ')';
        break;
    }
    return stream;
}

QDataStream &operator<<(QDataStream &stream, const CTSSolver::WhiteParameterization &param)
{
    stream << (quint8) param.type;
    switch (param.type) {
    case CTSSolver::WhiteParameterization::NOAA:
        break;
    }
    return stream;
}

QDataStream &operator>>(QDataStream &stream, CTSSolver::WhiteParameterization &param)
{
    quint8 i8;
    stream >> i8;
    switch (i8) {
    case (quint8) CTSSolver::WhiteParameterization::NOAA:
        param.type = CTSSolver::WhiteParameterization::NOAA;
        break;
    default:
        Q_ASSERT(false);
        break;
    }
    return stream;
}

QDebug operator<<(QDebug stream, const CTSSolver::WhiteParameterization &param)
{
    QDebugStateSaver saver(stream);
    stream.nospace();
    switch (param.type) {
    case CTSSolver::WhiteParameterization::NOAA:
        stream << "NOAA";
        break;
    }
    return stream;
}


CorrMueller2014::CorrMueller2014(QDataStream &stream)
{
    stream >> defaultTransmittanceStart;
    stream >> defaultTransmittanceEnd;
    stream >> defaultLengthStart;
    stream >> defaultLengthEnd;
    stream >> defaultSODStart;
    stream >> defaultSODEnd;
    stream >> defaultAODStart;
    stream >> defaultAODEnd;
    stream >> defaultAsymmetryStart;
    stream >> defaultAsymmetryEnd;
    stream >> defaultDeltaLength;
    stream >> defaultDeltaSF;
    stream >> defaultDeltaAF;
    stream >> defaultGF;
    stream >> defaultEtaF;
    stream >> filterSuffixes;
    stream >> restrictedInputs;

    quint32 n;
    stream >> n;
    for (int i = 0; i < (int) n; i++) {
        Processing p;
        stream >> p.operateFlags;
        stream >> p.absorptionWavelength;
        stream >> p.suffix;

        quint32 m;
        stream >> m;
        for (int j = 0; j < (int) m; j++) {
            CorrectionSet s;
            stream >> s.operateAbsorption;
            stream >> s.transmittanceStart;
            stream >> s.transmittanceEnd;
            stream >> s.lengthStart;
            stream >> s.lengthEnd;
            stream >> s.sodStart;
            stream >> s.sodEnd;
            stream >> s.aodStart;
            stream >> s.aodEnd;
            stream >> s.asymmetryStart;
            stream >> s.asymmetryEnd;
            stream >> s.deltaLength;
            stream >> s.inputDeltaSF;
            stream >> s.inputDeltaAF;
            stream >> s.inputGF;
            stream >> s.inputEtaF;
            stream >> s.inputBlack;
            stream >> s.inputWhite;

            p.operations.push_back(s);
        }

        processing.push_back(p);
    }
}

void CorrMueller2014::serialize(QDataStream &stream)
{
    stream << defaultTransmittanceStart;
    stream << defaultTransmittanceEnd;
    stream << defaultLengthStart;
    stream << defaultLengthEnd;
    stream << defaultSODStart;
    stream << defaultSODEnd;
    stream << defaultAODStart;
    stream << defaultAODEnd;
    stream << defaultAsymmetryStart;
    stream << defaultAsymmetryEnd;
    stream << defaultDeltaLength;
    stream << defaultDeltaSF;
    stream << defaultDeltaAF;
    stream << defaultGF;
    stream << defaultEtaF;
    stream << filterSuffixes;
    stream << restrictedInputs;

    quint32 n = (quint32) processing.size();
    stream << n;
    for (int i = 0; i < (int) n; i++) {
        stream << processing[i].operateFlags;
        stream << processing[i].absorptionWavelength;
        stream << processing[i].suffix;

        quint32 m = (quint32) processing[i].operations.size();
        stream << m;
        for (int j = 0; j < (int) m; j++) {
            stream << processing[i].operations[j].operateAbsorption;
            stream << processing[i].operations[j].transmittanceStart;
            stream << processing[i].operations[j].transmittanceEnd;
            stream << processing[i].operations[j].lengthStart;
            stream << processing[i].operations[j].lengthEnd;
            stream << processing[i].operations[j].sodStart;
            stream << processing[i].operations[j].sodEnd;
            stream << processing[i].operations[j].aodStart;
            stream << processing[i].operations[j].aodEnd;
            stream << processing[i].operations[j].asymmetryStart;
            stream << processing[i].operations[j].asymmetryEnd;
            stream << processing[i].operations[j].deltaLength;
            stream << processing[i].operations[j].inputDeltaSF;
            stream << processing[i].operations[j].inputDeltaAF;
            stream << processing[i].operations[j].inputGF;
            stream << processing[i].operations[j].inputEtaF;
            stream << processing[i].operations[j].inputBlack;
            stream << processing[i].operations[j].inputWhite;
        }
    }
}


QString CorrMueller2014Component::getBasicSerializationName() const
{ return QString::fromLatin1("corr_mueller2014"); }

ComponentOptions CorrMueller2014Component::getOptions()
{
    ComponentOptions options;

    options.add("transmittance-start", new DynamicInputOption(tr("transmittance-start", "name"),
                                                              tr("Input start transmittance"),
                                                              tr("This is the input start transmittance used in the correction.  "
                                                                         "When this option is set, this transmittance is used for all "
                                                                         "absorption channels.  Therefor, normally only a single absorption "
                                                                         "can be corrected while using this option."),
                                                              tr("Resolved automatically by instrument suffix")));

    options.add("transmittance-end", new DynamicInputOption(tr("transmittance-end", "name"),
                                                            tr("Input end transmittance"),
                                                            tr("This is the input end transmittance used in the correction.  "
                                                                       "When this option is set, this transmittance is used for all "
                                                                       "absorption channels.  Therefor, normally only a single absorption "
                                                                       "can be corrected while using this option."),
                                                            tr("Resolved automatically by instrument suffix")));

    options.add("deltaa-start", new DynamicInputOption(tr("deltaf-start", "name"),
                                                       tr("Input start filter optical depth"),
                                                       tr("This is the input start filter optical depth used in the "
                                                                  "correction.  When this option is set, this value is used for all "
                                                                  "absorption channels.  Therefor, normally only a single absorption "
                                                                  "can be corrected while using this option."),
                                                       tr("Resolved automatically by instrument suffix")));

    options.add("deltaa-end", new DynamicInputOption(tr("deltaf-end", "name"),
                                                     tr("Input end filter optical depth"),
                                                     tr("This is the input end filter optical depth used in the "
                                                                "correction.  When this option is set, this value is used for all "
                                                                "absorption channels.  Therefor, normally only a single absorption "
                                                                "can be corrected while using this option."),
                                                     tr("Resolved automatically by instrument suffix")));

    options.add("length-start",
                new DynamicInputOption(tr("length-start", "name"), tr("Input start sample length"),
                                       tr("This is the input start sample length used in the correction.  "
                                                  "When this option is set, this length is used for all "
                                                  "absorption channels."),
                                       tr("Resolved automatically by instrument suffix")));

    options.add("length-end",
                new DynamicInputOption(tr("length-end", "name"), tr("Input end sample length"),
                                       tr("This is the input end sample length used in the correction.  "
                                                  "When this option is set, this length is used for all "
                                                  "absorption channels."),
                                       tr("Resolved automatically by instrument suffix")));

    options.add("deltas-start", new DynamicInputOption(tr("deltas-start", "name"),
                                                       tr("Input start scattering optical depth"),
                                                       tr("This is the input start scattering optical depth used in the "
                                                                  "correction.  That is, this is the scattering integrated with the "
                                                                  "sample length at the start of each calculation interval."
                                                                  "When this option is set, this is used for all absorption "
                                                                  "channels.  Therefor, normally only a single absorption "
                                                                  "can be corrected while using this option if there is any "
                                                                  "wavelength dependence."),
                                                       tr("Resolved automatically by instrument suffix")));

    options.add("deltas-end", new DynamicInputOption(tr("deltas-end", "name"),
                                                     tr("Input end scattering optical depth"),
                                                     tr("This is the input end scattering optical depth used in the "
                                                                "correction.  That is, this is the scattering integrated with the "
                                                                "sample length at the start of each calculation interval."
                                                                "When this option is set, this is used for all absorption "
                                                                "channels.  Therefor, normally only a single absorption "
                                                                "can be corrected while using this option if there is any "
                                                                "wavelength dependence."),
                                                     tr("Resolved automatically by instrument suffix")));

    options.add("asymmetry-start", new DynamicInputOption(tr("asymmetry-start", "name"),
                                                          tr("Input start asymmetry parameter"),
                                                          tr("This is the input asymmetry parameter at the start of each sampling "
                                                                     "interval.  That is, the asymmetry parameter calculated from the "
                                                                     "scattering and back scattering integrated optical depths.  "
                                                                     "When this option is set, this is used for all absorption "
                                                                     "channels.  Therefor, normally only a single absorption "
                                                                     "can be corrected while using this option if there is any "
                                                                     "wavelength dependence."),
                                                          tr("Resolved automatically by instrument suffix")));

    options.add("asymmetry-end", new DynamicInputOption(tr("asymmetry-end", "name"),
                                                        tr("Input end asymmetry parameter"),
                                                        tr("This is the input asymmetry parameter at the end of each sampling "
                                                                   "interval.  That is, the asymmetry parameter calculated from the "
                                                                   "scattering and back scattering integrated optical depths.  "
                                                                   "When this option is set, this is used for all absorption "
                                                                   "channels.  Therefor, normally only a single absorption "
                                                                   "can be corrected while using this option if there is any "
                                                                   "wavelength dependence."),
                                                        tr("Resolved automatically by instrument suffix")));

    options.add("delta-l", new DynamicInputOption(tr("dl", "name"), tr("Input delta sample length"),
                                                  tr("This is the change in sample length used for each calculation "
                                                             "interval.  When omitted it is calculated from the start and end "
                                                             "sample lengths."), QString()));


    options.add("delta-sf", new DynamicInputOption(tr("delta-sf", "name"),
                                                   tr("\xCE\xB4SF - filter scattering optical depth"),
                                                   tr("This is the scattering optical of an unloaded filter used in "
                                                              "the model calculation."),
                                                   tr("Wavelength dependent")));
    options.add("delta-af", new DynamicInputOption(tr("delta-af", "name"),
                                                   tr("\xCE\xB4\x41\x46 - filter absorption optical depth"),
                                                   tr("This is the absorption optical of an unloaded filter used in "
                                                              "the model calculation."),
                                                   tr("Wavelength dependent")));
    options.add("gf",
                new DynamicInputOption(tr("gf", "name"), tr("Filter fiber asymmetry parameter"),
                                       tr("This is the average asymmetry parameter of the filter fibers used "
                                                  "in the model calculation."),
                                       tr("0.75", "gf default")));
    options.add("etaf", new DynamicInputOption(tr("hf", "name"),
                                               tr("\xCE\xB7\x66 - particle layer thickness"),
                                               tr("This is the fractional thickness of the particle layer used in the "
                                                          "two stream model calculation."),
                                               tr("0.2", "etaf default")));


    options.add("absorption", new DynamicSequenceSelectionOption(tr("correct", "name"),
                                                                 tr("Absorptions to correct"),
                                                                 tr("These are the variables to that the correction is applied to.  "
                                                                            "This option is mutually exclusive with instrument specification."),
                                                                 tr("All absorptions")));

    options.add("suffix", new ComponentOptionInstrumentSuffixSet(tr("instruments", "name"),
                                                                 tr("Instrument suffixes to correct"),
                                                                 tr("These are the instrument suffixes to correct.  "
                                                                            "For example S11 would usually specifies the reference nephelometer.  "
                                                                            "This option is mutually exclusive with manual variable specification."),
                                                                 tr("All instrument suffixes")));

    options.exclude("absorption", "suffix");
    options.exclude("suffix", "absorption");

    return options;
}

QList<ComponentExample> CorrMueller2014Component::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Defaults", "default example name"),
                                     tr("This will correct all absorptions present using pre-calculated "
                                                "scattering optical depths associated with each wavelength.")));

    options = getOptions();
    (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")))->add("A11");
    (qobject_cast<DynamicInputOption *>(options.get("delta-af")))->set(0.02);
    examples.append(ComponentExample(options, tr("Single instrument with a manual constant"),
                                     tr("This will correct all absorptions from A11 using an alternate "
                                                "setting for the absorption optical depth of an unloaded "
                                                "filter.")));

    return examples;
}

SegmentProcessingStage *CorrMueller2014Component::createBasicFilterDynamic(const ComponentOptions &options)
{
    return new CorrMueller2014(options);
}

SegmentProcessingStage *CorrMueller2014Component::createBasicFilterPredefined(const ComponentOptions &options,
                                                                              double start,
                                                                              double end,
                                                                              const QList<
                                                                                      SequenceName> &inputs)
{
    return new CorrMueller2014(options, start, end, inputs);
}

SegmentProcessingStage *CorrMueller2014Component::createBasicFilterEditing(double start,
                                                                           double end,
                                                                           const SequenceName::Component &station,
                                                                           const SequenceName::Component &archive,
                                                                           const ValueSegment::Transfer &config)
{
    return new CorrMueller2014(start, end, station, archive, config);
}

SegmentProcessingStage *CorrMueller2014Component::deserializeBasicFilter(QDataStream &stream)
{
    return new CorrMueller2014(stream);
}
