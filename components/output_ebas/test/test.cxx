/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QDir>
#include <QFile>
#include <QTemporaryFile>

#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "core/actioncomponent.hxx"
#include "core/number.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class TestComponent : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

    ActionComponentTime *component;

    static QByteArray takeLine(QByteArray &buffer)
    {
        int idx = buffer.indexOf('\n');
        if (idx < 0)
            return buffer;
        QByteArray result(buffer.mid(0, idx));
        buffer = buffer.mid(idx + 1);
        return result;
    }

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component = qobject_cast<ActionComponentTime *>(ComponentLoader::create("output_ebas"));
        QVERIFY(component);

        QVERIFY(databaseFile.open());
        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("profile")));
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("output")));
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("type")));
        QVERIFY(qobject_cast<ComponentOptionDirectory *>(options.get("directory")));
    }

    void basic()
    {
        QTemporaryFile tempOutput;
        QVERIFY(tempOutput.open());

        Variant::Root cfg;

        cfg["Profiles/aerosol/Level/Types/Neph/Filename"] = tempOutput.fileName();
        cfg["Profiles/aerosol/Level/Types/Neph/Originator"] = "Originator";
        cfg["Profiles/aerosol/Level/Types/Neph/Organization"] = "Organization ${LOCATION|STR|Org}";
        cfg["Profiles/aerosol/Level/Types/Neph/Submitter"] = "Submitter";
        cfg["Profiles/aerosol/Level/Types/Neph/Projects"] = "Projects";
        cfg["Profiles/aerosol/Level/Types/Neph/Interval"] = "Interval";

        cfg["Profiles/aerosol/Level/Types/Neph/Dependencies/Require"].setFlags({"scattering"});

        cfg["Profiles/aerosol/Level/Types/Neph/Variables/Bs/SortOrder"] = 100;
        cfg["Profiles/aerosol/Level/Types/Neph/Variables/Bs/Data/Input/#0/Variable"] =
                "Bs[BGR]_S11";
        cfg["Profiles/aerosol/Level/Types/Neph/Variables/Bs/Data/Input/#0/Archive"] = "raw";
        cfg["Profiles/aerosol/Level/Types/Neph/Variables/Bs/Header/Title/Component"] = "comp_name";
        cfg["Profiles/aerosol/Level/Types/Neph/Variables/Bs/Header/Title/Extra/#0"] =
                "key=${ConstantValue}";
        cfg["Profiles/aerosol/Level/Types/Neph/Variables/Bs/Header/Column"] = "sc${WAVELENGTH|0}";
        cfg["Profiles/aerosol/Level/Types/Neph/Variables/Bs/Dependencies/Provide"].setFlags(
                {"scattering", "other"});

        cfg["Profiles/aerosol/Level/Types/Neph/Variables/T/SortOrder"] = 200;
        cfg["Profiles/aerosol/Level/Types/Neph/Variables/T/Data/Input/#0/Variable"] = "T_S11";
        cfg["Profiles/aerosol/Level/Types/Neph/Variables/T/Data/Input/#0/Archive"] = "raw";
        cfg["Profiles/aerosol/Level/Types/Neph/Variables/T/Processing"] = "Optional";
        cfg["Profiles/aerosol/Level/Types/Neph/Variables/T/Calibration/#0"] = 10.0;
        cfg["Profiles/aerosol/Level/Types/Neph/Variables/T/Calibration/#1"] = 1.0;
        cfg["Profiles/aerosol/Level/Types/Neph/Variables/T/Header/Title/Full"] = "temperature";
        cfg["Profiles/aerosol/Level/Types/Neph/Variables/T/Header/Column"] = "T_header";
        cfg["Profiles/aerosol/Level/Types/Neph/Variables/T/MVC"] = "999.99";
        cfg["Profiles/aerosol/Level/Types/Neph/Variables/T/Format"] = "000.00";

        cfg["Profiles/aerosol/Level/Types/Neph/Substitutions/Location/Input/Variable"] = "site";
        cfg["Profiles/aerosol/Level/Types/Neph/Substitutions/Location/Input/Archive"] =
                "configuration";
        cfg["Profiles/aerosol/Level/Types/Neph/Substitutions/ConstantValue"] = "c_value";

        cfg["Profiles/aerosol/Level/Types/Neph/Headers/HType/Name"] = "Station WDCA-ID";
        cfg["Profiles/aerosol/Level/Types/Neph/Headers/HType/Contents"] =
                "GAWA${LOCATION|STR|Country}${LOCATION|STR|State}${STATION|UPPER}";


        Variant::Root meta;
        meta.write().metadataReal("Format").setString("0000.00");
        meta.write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");

        auto metaB = meta;
        metaB.write().metadataReal("Wavelength").setDouble(450);
        auto metaG = meta;
        metaG.write().metadataReal("Wavelength").setDouble(550);
        auto metaR = meta;
        metaR.write().metadataReal("Wavelength").setDouble(700);
        Variant::Root site;
        site["Org"] = "ESRL/GMD";
        site["Country"] = "US";
        site["State"] = "AK";

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"brw", "configuration", "ebas"}, cfg, FP::undefined(),
                              FP::undefined()),
                SequenceValue({"brw", "raw_meta", "BsB_S11"}, metaB, FP::undefined(),
                              FP::undefined()),
                SequenceValue({"brw", "raw_meta", "BsG_S11"}, metaG, FP::undefined(),
                              FP::undefined()),
                SequenceValue({"brw", "raw_meta", "BsR_S11"}, metaR, FP::undefined(),
                              FP::undefined()),
                SequenceValue({"brw", "configuration", "site"}, site, FP::undefined(),
                              FP::undefined()),
                SequenceValue({"brw", "raw", "T_S11"}, Variant::Root(25.0), 1388534400, 1388534520),
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(1.0), 1388534400,
                              1388534460),
                SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(1.1), 1388534400,
                              1388534460),
                SequenceValue({"brw", "raw", "BsR_S11"}, Variant::Root(1.2), 1388534400,
                              1388534460),
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(2.0), 1388534460,
                              1388534520),
                SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(2.1), 1388534460,
                              1388534520),
                SequenceValue({"brw", "raw", "BsR_S11"}, Variant::Root(2.2), 1388534460,
                              1388534520),
                SequenceValue({"brw", "raw", "BsB_S11"}, Variant::Root(3.0), 1388534520,
                              1388534580),
                SequenceValue({"brw", "raw", "BsG_S11"}, Variant::Root(3.1), 1388534520,
                              1388534580)});


        ComponentOptions options(component->getOptions());
        CPD3Action *action =
                component->createTimeAction(options, 1388534400, 1388620800, {"brw"});
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait(30000));
        delete action;


        QByteArray result(tempOutput.readAll());

        QCOMPARE(takeLine(result), QByteArray("22 1001"));
        QCOMPARE(takeLine(result), QByteArray("Originator"));
        QCOMPARE(takeLine(result), QByteArray("Organization ESRL/GMD"));
        QCOMPARE(takeLine(result), QByteArray("Submitter"));
        QCOMPARE(takeLine(result), QByteArray("Projects"));
        QCOMPARE(takeLine(result), QByteArray("1 1"));
        QVERIFY(takeLine(result).startsWith("2014 01 01 "));
        QCOMPARE(takeLine(result), QByteArray("Interval"));
        QCOMPARE(takeLine(result), QByteArray("days from file reference point"));
        QCOMPARE(takeLine(result), QByteArray("6"));
        QCOMPARE(takeLine(result), QByteArray("1 1 1 1 1 1"));
        QCOMPARE(takeLine(result),
                 QByteArray("9999.999999 9999.99 9999.99 9999.99 999.99 9.999999999"));
        QCOMPARE(takeLine(result),
                 QByteArray("end_time of measurement, days from the file reference point"));
        QCOMPARE(takeLine(result), QByteArray("comp_name, 1/Mm, Wavelength=450 nm, key=c_value"));
        QCOMPARE(takeLine(result), QByteArray("comp_name, 1/Mm, Wavelength=550 nm, key=c_value"));
        QCOMPARE(takeLine(result), QByteArray("comp_name, 1/Mm, Wavelength=700 nm, key=c_value"));
        QCOMPARE(takeLine(result), QByteArray("temperature"));
        QCOMPARE(takeLine(result), QByteArray("numflag"));
        QCOMPARE(takeLine(result), QByteArray("0"));
        QCOMPARE(takeLine(result), QByteArray("2"));
        QCOMPARE(takeLine(result), QByteArray("Station WDCA-ID: GAWAUSAKBRW"));
        QCOMPARE(takeLine(result),
                 QByteArray("start_time end_time sc450 sc550 sc700 T_header numflag"));
        QCOMPARE(takeLine(result),
                 QByteArray("   0.000000    0.000694    1.00    1.10    1.20  35.00 0.000000000"));
        QCOMPARE(takeLine(result),
                 QByteArray("   0.000694    0.001389    2.00    2.10    2.20  35.00 0.000000000"));
        QCOMPARE(takeLine(result),
                 QByteArray("   0.001389    0.002083    3.00    3.10 9999.99 999.99 0.000000000"));

        QVERIFY(result.isEmpty());
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
