/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef OUTPUTEBAS_H
#define OUTPUTEBAS_H

#include "core/first.hxx"

#include <mutex>
#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QList>
#include <QHash>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "fileoutput/ebas.hxx"

class OutputEBAS : public CPD3::CPD3Action {
Q_OBJECT

    std::string profile;
    std::string output;
    std::string type;
    QString directory;
    std::vector<CPD3::Data::SequenceName::Component> stations;
    double start;
    double end;

    bool terminated;
    std::unique_ptr<CPD3::Output::Engine> engine;
    std::mutex mutex;

    OutputEBAS() = delete;

    bool testTerminated();

public:
    OutputEBAS(const CPD3::ComponentOptions &options,
               double start,
               double end,
               const std::vector<CPD3::Data::SequenceName::Component> &setStations = {});

    virtual ~OutputEBAS();

    virtual void signalTerminate();

protected:
    virtual void run();
};

class OutputEBASComponent : public QObject, virtual public CPD3::ActionComponentTime {
Q_OBJECT
    Q_INTERFACES(CPD3::ActionComponentTime)

    Q_PLUGIN_METADATA(IID
                              "CPD3.output_ebas"
                              FILE
                              "output_ebas.json")

public:
    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual int actionRequireStations();

    virtual int actionAllowStations();

    virtual bool actionRequiresTime();

    virtual CPD3::CPD3Action *createTimeAction(const CPD3::ComponentOptions &options,
                                               double start,
                                               double end,
                                               const std::vector<std::string> &stations = {});
};

#endif
