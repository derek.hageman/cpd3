/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLoggingCategory>
#include <QRegularExpression>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "core/memory.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"

#include "output_ebas.hxx"


Q_LOGGING_CATEGORY(log_component_output_ebas, "cpd3.component.output.ebas", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Output;

OutputEBAS::OutputEBAS(const ComponentOptions &options,
                       double setStart,
                       double setEnd,
                       const std::vector<SequenceName::Component> &setStations) : profile(
        SequenceName::impliedProfile()),
                                                                                  directory(),
                                                                                  stations(
                                                                                          setStations),
                                                                                  start(setStart),
                                                                                  end(setEnd),
                                                                                  terminated(false)
{
    if (options.isSet("profile")) {
        profile = qobject_cast<ComponentOptionSingleString *>(options.get("profile"))->get()
                                                                                     .toLower()
                                                                                     .toStdString();
    }
    if (options.isSet("output")) {
        output = qobject_cast<ComponentOptionSingleString *>(options.get("output"))->get()
                                                                                   .toStdString();
    }
    if (options.isSet("type")) {
        type = qobject_cast<ComponentOptionSingleString *>(options.get("type"))->get()
                                                                               .toStdString();
    }
    if (options.isSet("directory")) {
        directory = qobject_cast<ComponentOptionDirectory *>(options.get("directory"))->get();
    }
}

OutputEBAS::~OutputEBAS()
{
    if (engine) {
        engine->signalTerminate();
        engine->wait();
        engine.reset();
    }
}

void OutputEBAS::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
        if (engine)
            engine->signalTerminate();
    }
}

bool OutputEBAS::testTerminated()
{
    std::lock_guard<std::mutex> lock(mutex);
    return terminated;
}

namespace {
class EBASEngine : public Engine {
    std::unique_ptr<QThread> pipelineThread;
    QObject *context;
    std::unique_ptr<StreamPipeline> pipeline;

    void endPipelineThread()
    {
        pipeline.reset();
        context->deleteLater();
        context = nullptr;
        pipelineThread->quit();
    }

public:
    explicit EBASEngine(std::unique_ptr<Stage> &&stage) : Engine(std::move(stage)), context(nullptr)
    { }

    virtual ~EBASEngine()
    {
        if (pipelineThread) {
            Q_ASSERT(context);

            Threading::runQueuedFunctor(context, [this] {
                if (pipeline) {
                    pipeline->signalTerminate();
                    pipeline->waitInEventLoop();
                }

                return endPipelineThread();
            });

            pipelineThread->wait();
            pipelineThread.reset();
        }
    }

protected:
    bool startFirstStage(Stage *stage) override
    {
        pipelineThread.reset(new QThread);
        pipelineThread->start();

        context = new QObject;
        context->moveToThread(pipelineThread.get());

        bool ok = false;
        Threading::runBlockingFunctor(context, [this, stage, &ok] {
            std::unique_ptr<StreamPipeline> result(new StreamPipeline(true, true, true));

            if (!result->setOutputIngress(getDataSink())) {
                qCDebug(log_component_output_ebas) << "Failed to set pipeline output:"
                                                   << result->getOutputError();
                return;
            }

            if (!EBAS::configurePipeline(stage, result.get()))
                return;

            if (!result->start()) {
                qCDebug(log_component_output_ebas) << "Pipeline start failed";
                return;
            }

            pipeline = std::move(result);
            ok = true;
        });

        return ok;
    }

    void firstStageComplete(Stage *) override
    {
        Q_ASSERT(pipelineThread);
        Q_ASSERT(context);
        Q_ASSERT(pipeline);

        Threading::runQueuedFunctor(context, [this] {
            pipeline->waitInEventLoop();

            return endPipelineThread();
        });

        pipelineThread->wait();
        pipelineThread.reset();
    }

    void firstStageAborted(Stage *) override
    {
        if (!pipelineThread)
            return;

        Q_ASSERT(context);
        Q_ASSERT(pipeline);
        Threading::runQueuedFunctor(context, [this] {
            pipeline->signalTerminate();
            pipeline->waitInEventLoop();

            return endPipelineThread();
        });

        pipelineThread->wait();
        pipelineThread.reset();
    }
};
}

void OutputEBAS::run()
{
    if (testTerminated())
        return;

    profile = Util::to_lower(std::move(profile));
    if (profile.empty()) {
        qCDebug(log_component_output_ebas) << "Invalid profile";
        return;
    }

    double startProcessing = Time::time();

    using TypeConfig = ValueSegment::Transfer;
    using OutputConfig = std::unordered_map<std::string, TypeConfig>;
    using RunConfig = std::unordered_map<SequenceName::Component, OutputConfig>;
    RunConfig stationConfig;
    {
        Archive::Access access;
        Archive::Access::ReadLock lock(access);

        auto allStations = access.availableStations(stations);
        if (allStations.empty()) {
            qCDebug(log_component_output_ebas) << "No stations available";
            return;
        }
        stations.clear();
        std::copy(allStations.begin(), allStations.end(), Util::back_emplacer(stations));

        for (const auto &station : stations) {
            auto config = ValueSegment::Stream::read(
                    Archive::Selection(start, end, {station}, {"configuration"}, {"ebas"}),
                    &access);

            OutputConfig outputs;
            for (const auto &seg : config) {
                Variant::Read profileRoot = seg.read().hash("Profiles").hash(profile);
                if (!profileRoot.exists())
                    continue;

                for (auto profileOutput : profileRoot.toHash()) {
                    if (profileOutput.first.empty())
                        continue;
                    if (!profileOutput.second.exists())
                        continue;

                    if (!output.empty()) {
                        if (!Util::exact_match(profileOutput.first,
                                               QRegularExpression(QString::fromStdString(output),
                                                                  QRegularExpression::CaseInsensitiveOption))) {
                            continue;
                        }
                    }

                    outputs[profileOutput.first].emplace_back(seg.getStart(), seg.getEnd(),
                                                              Variant::Root(profileOutput.second));
                }
            }
            if (outputs.empty())
                continue;

            stationConfig.emplace(station, std::move(outputs));
        }

        stations.clear();
        for (const auto &cfg : stationConfig) {
            stations.emplace_back(cfg.first);
        }
    }

    if (testTerminated())
        return;

    std::sort(stations.begin(), stations.end());

    qCDebug(log_component_output_ebas) << "Starting output for" << stations.size() << "station(s)";

    for (const auto &station : stations) {
        auto &outputsConfig = stationConfig[station];

        qCDebug(log_component_output_ebas) << "Generating output for" << station << "with"
                                           << outputsConfig.size() << "output type(s)";

        for (auto &generateConfig : outputsConfig) {
            qCDebug(log_component_output_ebas) << "Generating output for" << station << "and output"
                                               << generateConfig.first;

            Memory::release_unused();

            feedback.emitStage(
                    tr("Output EBAS for %1/%2").arg(QString::fromStdString(station).toUpper(),
                                                    QString::fromStdString(generateConfig.first)),
                    tr("EBAS data output is starting up for %1 output %2.").arg(
                            QString::fromStdString(station).toUpper(),
                            QString::fromStdString(generateConfig.first)));

            Engine *outputEngine;
            {
                EBAS config;
                config.start = start;
                config.end = end;
                config.station = station;
                config.typeRestrict = type;
                config.config = std::move(generateConfig.second);

                config.createOutput = [this](const std::string &fileName) {
                    std::unique_ptr<QFile> result;

                    if (directory.isEmpty()) {
                        result.reset(new QFile(QString::fromStdString(fileName)));
                    } else {
                        result.reset(new QFile(
                                QDir(directory).filePath(QString::fromStdString(fileName))));
                    }

                    if (!result->open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text))
                        return std::unique_ptr<QFile>();

                    qCDebug(log_component_output_ebas) << "Opened output to" << result->fileName();

                    return std::move(result);
                };

                outputEngine = new EBASEngine(EBAS::stage(std::move(config)));
            }

            outputEngine->feedback.forward(feedback);
            outputEngine->start();

            {
                std::unique_lock<std::mutex> lock(mutex);
                engine.reset(outputEngine);
                if (terminated) {
                    engine.reset();
                    lock.unlock();
                    outputEngine->signalTerminate();
                    outputEngine->wait();
                    return;
                }
            }

            outputEngine->wait();

            {
                std::lock_guard<std::mutex> lock(mutex);
                engine.reset();
                if (terminated)
                    return;
            }
        }
    }

    double endProcessing = Time::time();
    qCDebug(log_component_output_ebas) << "Finished output after"
                                       << Logging::elapsed(endProcessing - startProcessing);

    QCoreApplication::processEvents();
}


ComponentOptions OutputEBASComponent::getOptions()
{
    ComponentOptions options;

    options.add("profile",
                new ComponentOptionSingleString(tr("profile", "name"), tr("Execution profile"),
                                                tr("This is the profile name to output.  Multiple profiles can be "
                                                   "defined to specify different sets of data that can be output "
                                                   "independently.  Consult the station configuration for available profiles."),
                                                tr("aerosol")));

    options.add("output",
                new ComponentOptionSingleString(tr("output", "name"), tr("Output configuration"),
                                                tr("This is the output configuration to generate.  Output configurations "
                                                   "generally corispond to EBAS data levels.   Consult the profile "
                                                   "configuration for available outputs.  This is a regular expression "
                                                   "that can match multiple outputs."),
                                                tr("All available")));

    options.add("type", new ComponentOptionSingleString(tr("type", "name"), tr("Output type"),
                                                        tr("This is the type of output to generate.  Different output types "
                                                           "generally correspond to different cut size and instrument "
                                                           "pairs.  Consult the profile configuration for available types.  "
                                                           "This is a regular expression that can match multiple types."),
                                                        tr("All available")));

    options.add("directory",
                new ComponentOptionDirectory(tr("directory", "name"), tr("Output directory"),
                                             tr("This is the directory files will be placed in."),
                                             tr("Current directory")));

    return options;
}

QList<ComponentExample> OutputEBASComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Output EBAS data", "default example name"),
                                     tr("This will output data for the default \"aerosol\" profile.")));

    return examples;
}

int OutputEBASComponent::actionRequireStations()
{ return 1; }

int OutputEBASComponent::actionAllowStations()
{ return INT_MAX; }

bool OutputEBASComponent::actionRequiresTime()
{ return true; }

CPD3Action *OutputEBASComponent::createTimeAction(const ComponentOptions &options,
                                                  double start,
                                                  double end,
                                                  const std::vector<std::string> &stations)
{ return new OutputEBAS(options, start, end, stations); }
