/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef SELECT_H
#define SELECT_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"

class Select : public CPD3::Data::AsyncProcessingStage {
    CPD3::Data::DynamicSequenceSelection *include;
    CPD3::Data::DynamicSequenceSelection *exclude;
    CPD3::Data::DynamicSequenceSelection *pathOperate;
    CPD3::Data::DynamicString *pathSelect;
    CPD3::Data::DynamicBool *pathRemove;
    CPD3::Data::DynamicBool *explicitMetadata;
    CPD3::Data::DynamicBool *removeUndefined;

    CPD3::Data::DynamicSequenceSelection *setOperate;
    CPD3::Data::DynamicBool *setDuplicate;
    CPD3::Data::DynamicString *captureStation;
    CPD3::Data::DynamicString *setStation;
    CPD3::Data::DynamicString *captureArchive;
    CPD3::Data::DynamicString *setArchive;
    CPD3::Data::DynamicString *captureVariable;
    CPD3::Data::DynamicString *setVariable;
    CPD3::Data::DynamicString *addFlavor;
    CPD3::Data::DynamicString *removeFlavor;

    double minimumStart;
    double maximumEnd;

    enum State {
        MaybePass = 0x01,
        MaybeDrop = 0x02,
        MaybeApplyPath = 0x04,
        MaybeSet = 0x08,
        CheckTimes = 0x10,

        IsMeta = 0x80,
    };
    QHash<CPD3::Data::SequenceName, quint32> actions;
    bool pastStart;
    bool pastEnd;

    quint32 getDispatchBits(const CPD3::Data::SequenceName &unit);

public:
    Select(const CPD3::ComponentOptions &options);

    Select(double start,
           double end,
           const CPD3::Data::SequenceName::Component &station,
           const CPD3::Data::SequenceName::Component &archive,
           const CPD3::Data::ValueSegment::Transfer &config);

    Select(QDataStream &stream);

    ~Select();

    virtual void serialize(QDataStream &stream);

protected:
    virtual void process(CPD3::Data::SequenceValue::Transfer &&incoming);

    virtual bool continueProcessing();
};

class SelectComponent : public QObject, public CPD3::Data::ProcessingStageComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ProcessingStageComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.select"
                              FILE
                              "select.json")

public:
    virtual QString getGeneralSerializationName() const;

    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual CPD3::Data::ProcessingStage *createGeneralFilterDynamic
            (const CPD3::ComponentOptions &options = CPD3::ComponentOptions());

    virtual CPD3::Data::ProcessingStage *createGeneralFilterEditing(double start,
                                                                    double end,
                                                                    const CPD3::Data::SequenceName::Component &station,
                                                                    const CPD3::Data::SequenceName::Component &archive,
                                                                    const CPD3::Data::ValueSegment::Transfer &config);

    virtual CPD3::Data::ProcessingStage *deserializeGeneralFilter(QDataStream &stream);

};

#endif
