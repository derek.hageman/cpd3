/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/textsubstitution.hxx"
#include "datacore/variant/composite.hxx"

#include "select.hxx"

using namespace CPD3;
using namespace CPD3::Data;

Select::Select(const ComponentOptions &options) : include(NULL),
                                                  exclude(NULL),
                                                  pathOperate(NULL),
                                                  pathSelect(NULL),
                                                  pathRemove(NULL),
                                                  explicitMetadata(NULL),
                                                  removeUndefined(NULL),
                                                  setOperate(NULL),
                                                  setDuplicate(NULL),
                                                  captureStation(NULL),
                                                  setStation(NULL),
                                                  captureArchive(NULL),
                                                  setArchive(NULL),
                                                  captureVariable(NULL),
                                                  setVariable(NULL),
                                                  addFlavor(NULL),
                                                  removeFlavor(NULL),
                                                  minimumStart(FP::undefined()),
                                                  maximumEnd(FP::undefined()),
                                                  actions(),
                                                  pastStart(false),
                                                  pastEnd(false)
{
    if (options.isSet("include")) {
        include = qobject_cast<DynamicSequenceSelectionOption *>(
                options.get("include"))->getOperator();
    } else {
        include =
                new DynamicSequenceSelection::Match(QString(), QString(), QString(), QStringList(),
                                                    QStringList());
    }

    if (options.isSet("exclude")) {
        exclude = qobject_cast<DynamicSequenceSelectionOption *>(
                options.get("exclude"))->getOperator();
    } else {
        exclude = new DynamicSequenceSelection::None;
    }

    if (options.isSet("path")) {
        pathSelect = qobject_cast<DynamicStringOption *>(options.get("path"))->getInput();

        if (options.isSet("path-remove")) {
            pathRemove = qobject_cast<DynamicBoolOption *>(options.get("path-remove"))->getInput();
        } else {
            pathRemove = new DynamicPrimitive<bool>::Constant(false);
        }

        if (options.isSet("path-apply")) {
            pathOperate = qobject_cast<DynamicSequenceSelectionOption *>(options.get("path-apply"))
                    ->getOperator();
        } else {
            pathOperate = new DynamicSequenceSelection::Match(QString(), QString(), QString(),
                                                              QStringList(), QStringList());
        }
    } else {
        pathSelect = new DynamicPrimitive<QString>::Constant;
        pathRemove = new DynamicPrimitive<bool>::Constant(false);
        pathOperate = new DynamicSequenceSelection::None;
    }

    if (options.isSet("explicit-meta")) {
        explicitMetadata =
                qobject_cast<DynamicBoolOption *>(options.get("explicit-meta"))->getInput();
    } else {
        explicitMetadata = new DynamicPrimitive<bool>::Constant(false);
    }

    if (options.isSet("remove-undefined")) {
        removeUndefined =
                qobject_cast<DynamicBoolOption *>(options.get("remove-undefined"))->getInput();
    } else {
        removeUndefined = new DynamicPrimitive<bool>::Constant(false);
    }

    if (options.isSet("set-station") ||
            options.isSet("set-archive") ||
            options.isSet("set-variable") ||
            options.isSet("set-flavor-add") ||
            options.isSet("set-flavor-remove")) {
        if (options.isSet("set-apply")) {
            setOperate = qobject_cast<DynamicSequenceSelectionOption *>(
                    options.get("set-apply"))->getOperator();
        } else {
            setOperate = new DynamicSequenceSelection::Match(QString(), QString(), QString(),
                                                             QStringList(), QStringList());
        }

        if (options.isSet("set-duplicate")) {
            setDuplicate =
                    qobject_cast<DynamicBoolOption *>(options.get("set-duplicate"))->getInput();
        } else {
            setDuplicate = new DynamicPrimitive<bool>::Constant(false);
        }

        if (options.isSet("set-capture-station")) {
            captureStation = qobject_cast<DynamicStringOption *>(options.get("set-capture-station"))
                    ->getInput();
        } else {
            captureStation = new DynamicPrimitive<QString>::Constant;
        }

        if (options.isSet("set-station")) {
            setStation =
                    qobject_cast<DynamicStringOption *>(options.get("set-station"))->getInput();
        } else {
            setStation = new DynamicPrimitive<QString>::Constant;
        }

        if (options.isSet("set-capture-archive")) {
            captureArchive = qobject_cast<DynamicStringOption *>(options.get("set-capture-archive"))
                    ->getInput();
        } else {
            captureArchive = new DynamicPrimitive<QString>::Constant;
        }

        if (options.isSet("set-archive")) {
            setArchive =
                    qobject_cast<DynamicStringOption *>(options.get("set-archive"))->getInput();
        } else {
            setArchive = new DynamicPrimitive<QString>::Constant;
        }

        if (options.isSet("set-capture-variable")) {
            captureVariable = qobject_cast<DynamicStringOption *>(
                    options.get("set-capture-variable"))->getInput();
        } else {
            captureVariable = new DynamicPrimitive<QString>::Constant;
        }

        if (options.isSet("set-variable")) {
            setVariable =
                    qobject_cast<DynamicStringOption *>(options.get("set-variable"))->getInput();
        } else {
            setVariable = new DynamicPrimitive<QString>::Constant;
        }

        if (options.isSet("set-flavor-add")) {
            addFlavor =
                    qobject_cast<DynamicStringOption *>(options.get("set-flavor-add"))->getInput();
        } else {
            addFlavor = new DynamicPrimitive<QString>::Constant;
        }

        if (options.isSet("set-flavor-remove")) {
            removeFlavor = qobject_cast<DynamicStringOption *>(
                    options.get("set-flavor-remove"))->getInput();
        } else {
            removeFlavor = new DynamicPrimitive<QString>::Constant;
        }
    } else {
        setOperate = new DynamicSequenceSelection::None;
        setDuplicate = new DynamicPrimitive<bool>::Constant(false);
        captureStation = new DynamicPrimitive<QString>::Constant;
        setStation = new DynamicPrimitive<QString>::Constant;
        captureArchive = new DynamicPrimitive<QString>::Constant;
        setArchive = new DynamicPrimitive<QString>::Constant;
        captureVariable = new DynamicPrimitive<QString>::Constant;
        setVariable = new DynamicPrimitive<QString>::Constant;
        addFlavor = new DynamicPrimitive<QString>::Constant;
        removeFlavor = new DynamicPrimitive<QString>::Constant;
    }

    if (options.isSet("start")) {
        minimumStart = qobject_cast<ComponentOptionSingleTime *>(options.get("start"))->get();
        pastStart = !FP::defined(minimumStart);
    } else {
        pastStart = true;
    }
    if (options.isSet("end")) {
        maximumEnd = qobject_cast<ComponentOptionSingleTime *>(options.get("end"))->get();
    }
}

Select::Select(double start,
               double end,
               const SequenceName::Component &station,
               const SequenceName::Component &archive,
               const ValueSegment::Transfer &config) : include(NULL),
                                                       exclude(NULL),
                                                       pathOperate(NULL),
                                                       pathSelect(NULL),
                                                       pathRemove(NULL),
                                                       explicitMetadata(NULL),
                                                       removeUndefined(NULL),
                                                       setOperate(NULL),
                                                       setDuplicate(NULL),
                                                       captureStation(NULL),
                                                       setStation(NULL),
                                                       captureArchive(NULL),
                                                       setArchive(NULL),
                                                       captureVariable(NULL),
                                                       setVariable(NULL),
                                                       addFlavor(NULL),
                                                       removeFlavor(NULL),
                                                       minimumStart(FP::undefined()),
                                                       maximumEnd(FP::undefined()),
                                                       actions(),
                                                       pastStart(true),
                                                       pastEnd(false)
{
    include = DynamicSequenceSelection::fromConfiguration(config, "Include", start, end);
    exclude = DynamicSequenceSelection::fromConfiguration(config, "Exclude", start, end);
    pathOperate = DynamicSequenceSelection::fromConfiguration(config, "Path/Apply", start, end);
    pathSelect = DynamicStringOption::fromConfiguration(config, "Path/Value", start, end);
    pathRemove = DynamicBoolOption::fromConfiguration(config, "Path/Remove", start, end);
    explicitMetadata = DynamicBoolOption::fromConfiguration(config, "ExplicitMetadata", start, end);

    setOperate = DynamicSequenceSelection::fromConfiguration(config, "Set/Apply", start, end);
    setDuplicate = DynamicBoolOption::fromConfiguration(config, "Set/Duplicate", start, end);
    captureStation =
            DynamicStringOption::fromConfiguration(config, "Set/Capture/Station", start, end);
    setStation = DynamicStringOption::fromConfiguration(config, "Set/Station", start, end);
    captureArchive =
            DynamicStringOption::fromConfiguration(config, "Set/Capture/Archive", start, end);
    setArchive = DynamicStringOption::fromConfiguration(config, "Set/Archive", start, end);
    captureVariable =
            DynamicStringOption::fromConfiguration(config, "Set/Capture/Variable", start, end);
    setVariable = DynamicStringOption::fromConfiguration(config, "Set/Variable", start, end);
    addFlavor = DynamicStringOption::fromConfiguration(config, "Set/Flavor/Add", start, end);
    removeFlavor = DynamicStringOption::fromConfiguration(config, "Set/Flavor/Remove", start, end);

    include->registerExpected(station, archive);
    exclude->registerExpected(station, archive);
    pathOperate->registerExpected(station, archive);
    setOperate->registerExpected(station, archive);
}

Select::~Select()
{
    delete include;
    delete exclude;
    delete pathOperate;
    delete pathSelect;
    delete pathRemove;
    delete explicitMetadata;
    delete removeUndefined;
    delete setOperate;
    delete setDuplicate;
    delete captureStation;
    delete setStation;
    delete captureArchive;
    delete setArchive;
    delete captureVariable;
    delete setVariable;
    delete addFlavor;
    delete removeFlavor;
}

Select::Select(QDataStream &stream) : AsyncProcessingStage(stream),
                                      include(NULL),
                                      exclude(NULL),
                                      pathOperate(NULL),
                                      pathSelect(NULL),
                                      pathRemove(NULL),
                                      explicitMetadata(NULL),
                                      removeUndefined(NULL),
                                      setOperate(NULL),
                                      setDuplicate(NULL),
                                      captureStation(NULL),
                                      setStation(NULL),
                                      captureArchive(NULL),
                                      setArchive(NULL),
                                      captureVariable(NULL),
                                      setVariable(NULL),
                                      addFlavor(NULL),
                                      removeFlavor(NULL),
                                      minimumStart(FP::undefined()),
                                      maximumEnd(FP::undefined()),
                                      actions(),
                                      pastStart(false),
                                      pastEnd(false)
{
    stream >> include >> exclude >> pathOperate >> pathSelect >> pathRemove >> explicitMetadata
           >> removeUndefined >> setOperate >> setDuplicate >> captureStation >> setStation
           >> captureArchive >> setArchive >> captureVariable >> setVariable >> addFlavor
           >> removeFlavor >> minimumStart >> maximumEnd >> actions >> pastStart >> pastEnd;
}

void Select::serialize(QDataStream &stream)
{
    AsyncProcessingStage::serialize(stream);
    stream << include << exclude << pathOperate << pathSelect << pathRemove << explicitMetadata
           << removeUndefined << setOperate << setDuplicate << captureStation << setStation
           << captureArchive << setArchive << captureVariable << setVariable << addFlavor
           << removeFlavor << minimumStart << maximumEnd << actions << pastStart << pastEnd;
}

quint32 Select::getDispatchBits(const SequenceName &unit)
{
    QHash<SequenceName, quint32>::const_iterator check = actions.constFind(unit);
    if (check != actions.constEnd())
        return check.value();

    quint32 bits = 0;

    if (include->registerInput(unit))
        bits |= MaybePass;
    if (exclude->registerInput(unit))
        bits |= MaybeDrop;
    if (pathOperate->registerInput(unit))
        bits |= MaybeApplyPath;
    if (setOperate->registerInput(unit))
        bits |= MaybeSet;

    if (unit.isMeta()) {
        bits |= IsMeta;
        bits |= getDispatchBits(unit.fromMeta());
    }

    if (!pastStart && FP::defined(minimumStart))
        bits |= CheckTimes;
    if (FP::defined(maximumEnd))
        bits |= CheckTimes;

    actions.insert(unit, bits);
    return bits;
}

bool Select::continueProcessing()
{ return !pastEnd; }

static SequenceName::Component applySet(const SequenceName::Component &input,
                                        CPD3::Data::DynamicString *capture,
                                        CPD3::Data::DynamicString *set,
                                        double start,
                                        double end,
                                        Qt::CaseSensitivity cs)
{
    QString result(set->get(start, end));
    if (result.isEmpty())
        return {};
    QString ce(capture->get(start, end));
    if (ce.isEmpty())
        return result.toStdString();

    QRegExp re(ce, cs);
    if (!re.exactMatch(QString::fromStdString(input)))
        return {};
    return NumberedSubstitution(re).apply(result).toStdString();
}

static void appendAndOutput(SequenceValue::Transfer &output, StreamSink *egress,
                            SequenceValue &&add)
{
    if (output.size() > 16384) {
        egress->incomingData(std::move(output));
        output.clear();
    }
    output.emplace_back(std::move(add));
}

void Select::process(SequenceValue::Transfer &&incoming)
{
    if (pastEnd)
        return;

    SequenceValue::Transfer output;
    for (auto &add : incoming) {
        quint32 bits = getDispatchBits(add.getUnit());

        if (bits & IsMeta) {
            Q_ASSERT(add.getUnit().isMeta());
            if (!explicitMetadata->get(add.getStart(), add.getEnd())) {
                if (!(bits & MaybePass))
                    continue;
                auto nometa = add.getUnit().fromMeta();
                if (!include->get(add.getStart(), add.getEnd()).count(nometa) != 0)
                    continue;
                if (bits & MaybeDrop) {
                    if (exclude->get(add.getStart(), add.getEnd()).count(nometa) != 0)
                        continue;
                }

                if (!pastStart) {
                    Q_ASSERT(FP::defined(minimumStart));
                    if (!FP::defined(add.getStart()) || add.getStart() < minimumStart) {
                        if (FP::defined(add.getEnd()) && add.getEnd() > minimumStart) {
                            add.setStart(minimumStart);
                        } else {
                            continue;
                        }
                    }
                }
                if (FP::defined(maximumEnd)) {
                    if (!FP::defined(add.getEnd()) || add.getEnd() >= maximumEnd) {
                        if (FP::defined(add.getStart()) && add.getStart() >= maximumEnd) {
                            pastEnd = true;
                            break;
                        }
                        add.setEnd(maximumEnd);
                    }
                }

                if (bits & MaybeSet) {
                    if (setOperate->get(add.getStart(), add.getEnd()).count(nometa) != 0) {
                        if (setDuplicate->get(add.getStart(), add.getEnd())) {
                            appendAndOutput(output, egress, SequenceValue(add));
                        }

                        if (!nometa.isDefaultStation()) {
                            auto s = applySet(nometa.getStation(), captureStation, setStation,
                                              add.getStart(), add.getEnd(), Qt::CaseInsensitive);
                            if (!s.empty())
                                nometa.setStation(s);
                        }
                        auto s = applySet(nometa.getVariable(), captureVariable, setVariable,
                                          add.getStart(), add.getEnd(), Qt::CaseSensitive);
                        if (!s.empty())
                            nometa.setVariable(s);

                        s = applySet(nometa.getArchive(), captureArchive, setArchive,
                                     add.getStart(), add.getEnd(), Qt::CaseInsensitive);
                        if (!s.empty())
                            nometa.setArchive(s);

                        s = addFlavor->get(add.getStart(), add.getEnd()).toStdString();
                        if (!s.empty())
                            nometa.addFlavor(s);

                        s = removeFlavor->get(add.getStart(), add.getEnd()).toStdString();
                        if (!s.empty())
                            nometa.removeFlavor(s);

                        add.setUnit(nometa.toMeta());
                    }
                }

                if (add.read().isMetadata() && add.read().metadata("Processing").exists()) {
                    auto meta = add.write().metadata("Processing").toArray().after_back();
                    meta["By"].setString("select");
                    meta["At"].setDouble(Time::time());
                    meta["Environment"].setString(Environment::describe());
                    meta["Revision"].setString(Environment::revision());

                    if (bits & MaybeApplyPath) {
                        meta["Parameters"].hash("Path")
                                          .setString(pathSelect->get(add.getStart(), add.getEnd()));
                    }
                }

                appendAndOutput(output, egress, std::move(add));
                continue;
            }
        }

        if (!(bits & MaybePass))
            continue;
        if (!include->get(add.getStart(), add.getEnd()).count(add.getUnit()) != 0)
            continue;
        if (bits & MaybeDrop) {
            if (exclude->get(add.getStart(), add.getEnd()).count(add.getUnit()) != 0)
                continue;
        }

        if (!(bits & (MaybeSet | MaybeApplyPath | CheckTimes))) {
            if (removeUndefined->get(add.getStart(), add.getEnd()) &&
                    !Variant::Composite::isDefined(add.read()))
                continue;
            appendAndOutput(output, egress, std::move(add));
            continue;
        }

        if (bits & MaybeApplyPath) {
            QString path(pathSelect->get(add.getStart(), add.getEnd()));
            if (!path.isEmpty() &&
                    pathOperate->get(add.getStart(), add.getEnd()).count(add.getUnit()) != 0) {
                if (pathRemove->get(add.getStart(), add.getEnd())) {
                    add.write().getPath(path).remove();
                } else {
                    add.setRoot(Variant::Root(add.read()[path]));
                }
            }
        }

        if (removeUndefined->get(add.getStart(), add.getEnd()) &&
                !Variant::Composite::isDefined(add.read()))
            continue;

        if (!pastStart) {
            Q_ASSERT(FP::defined(minimumStart));
            if (!FP::defined(add.getStart()) || add.getStart() < minimumStart) {
                if (FP::defined(add.getEnd()) && add.getEnd() > minimumStart) {
                    add.setStart(minimumStart);
                } else {
                    continue;
                }
            }
        }
        if (FP::defined(maximumEnd)) {
            if (!FP::defined(add.getEnd()) || add.getEnd() >= maximumEnd) {
                if (FP::defined(add.getStart()) && add.getStart() >= maximumEnd) {
                    pastEnd = true;
                    break;
                }
                add.setEnd(maximumEnd);
            }
        }

        if (bits & MaybeSet) {
            if (setOperate->get(add.getStart(), add.getEnd()).count(add.getUnit()) != 0) {
                if (setDuplicate->get(add.getStart(), add.getEnd()))
                    appendAndOutput(output, egress, SequenceValue(add));

                if (!add.getUnit().isDefaultStation()) {
                    auto s = applySet(add.getStation(), captureStation, setStation, add.getStart(),
                                      add.getEnd(), Qt::CaseInsensitive);
                    if (!s.empty())
                        add.setStation(s);
                }

                auto s = applySet(add.getArchive(), captureArchive, setArchive, add.getStart(),
                                  add.getEnd(), Qt::CaseInsensitive);
                if (!s.empty())
                    add.setArchive(s);

                s = applySet(add.getVariable(), captureVariable, setVariable, add.getStart(),
                             add.getEnd(), Qt::CaseSensitive);
                if (!s.empty())
                    add.setVariable(s);

                s = addFlavor->get(add.getStart(), add.getEnd()).toStdString();
                if (!s.empty()) {
                    add.getName().addFlavor(s);
                }

                s = removeFlavor->get(add.getStart(), add.getEnd()).toStdString();
                if (!s.empty()) {
                    add.getName().removeFlavor(s);
                }
            }
        }

        appendAndOutput(output, egress, std::move(add));
    }


    egress->incomingData(std::move(output));
}

QString SelectComponent::getGeneralSerializationName() const
{ return QString::fromLatin1("select"); }

ComponentOptions SelectComponent::getOptions()
{
    ComponentOptions options;

    options.add("include", new DynamicSequenceSelectionOption(tr("include", "name"),
                                                              tr("The values to include in the output"),
                                                              tr("This option sets the values that are included in the output."),
                                                              tr("Everything", "default include")));

    options.add("exclude", new DynamicSequenceSelectionOption(tr("exclude", "name"),
                                                              tr("The values to exclude from the output"),
                                                              tr("This option sets the values that are excluded from the output."),
                                                              tr("Nothing", "default exclude")));

    options.add("path-apply", new DynamicSequenceSelectionOption(tr("path-apply", "name"),
                                                                 tr("The values that have the path applied to them"),
                                                                 tr("This option sets the values that have the path applied to them "
                                                                    "in the output."),
                                                                 tr("Everything", "default path")));

    options.add("path",
                new DynamicStringOption(tr("path", "name"), tr("The path within values to access"),
                                        tr("This is the path applied to values.  This only makes sense for "
                                           "values with child types (e.x. hashes or metadata).  The value "
                                           "accessed at the path will become the new root for everything it "
                                           "is applied to."), tr("None", "default path")));

    options.add("path-remove", new DynamicBoolOption(tr("path-remove", "name"),
                                                     tr("Remove the path instead of applying it"),
                                                     tr("When enabled this changes the path to be removed from the value "
                                                        "instead of applying it and passing the value.  This is, this "
                                                        "causes the path within values to be deleted."),
                                                     QString()));

    options.add("explicit-meta", new DynamicBoolOption(tr("explicit-meta", "name"),
                                                       tr("Enable explicit handling of metadata"),
                                                       tr("When enabled this changes the handling of metadata so it must be "
                                                          "matched manually.  Normally metadata is unchanged by path access "
                                                          "and is passed if the corresponding normal value passes."),
                                                       QString()));

    options.add("remove-undefined", new DynamicBoolOption(tr("remove-undefined", "name"),
                                                          tr("Remove all undefined values"),
                                                          tr("When enabled this this removes all undefined values from the "
                                                             "output."), QString()));


    options.add("set-apply", new DynamicSequenceSelectionOption(tr("set-apply", "name"),
                                                                tr("The values that have set operations applied"),
                                                                tr("This option sets the values that have the alteration of station, "
                                                                   "archive and/or variable applied to them."),
                                                                tr("Everything", "default set")));

    options.add("set-duplicate", new DynamicBoolOption(tr("set-duplicate", "name"),
                                                       tr("Copy the value is made before alteration"),
                                                       tr("When enabled this causes a copy of the value to be generated "
                                                          "before the station, archive, or variable are changed."),
                                                       QString()));

    options.add("set-station",
                new DynamicStringOption(tr("set-station", "name"), tr("The station to set"),
                                        tr("This is the station that values are set to."),
                                        tr("No change", "default set station")));

    options.add("set-capture-station", new DynamicStringOption(tr("capture-station", "name"),
                                                               tr("The regular expression to capture from the station"),
                                                               tr("This is a regular expression the station must match to be "
                                                                  "changed.  When set the captures ${1}, ${2}, ... are available "
                                                                  "for substitution in the output station."),
                                                               QString()));

    options.add("set-archive",
                new DynamicStringOption(tr("set-archive", "name"), tr("The station to set"),
                                        tr("This is the archive that values are set to."),
                                        tr("No change", "default set archive")));

    options.add("set-capture-archive", new DynamicStringOption(tr("capture-archive", "name"),
                                                               tr("The regular expression to capture from the archive"),
                                                               tr("This is a regular expression the archive must match to be "
                                                                  "changed.  When set the captures ${1}, ${2}, ... are available "
                                                                  "for substitution in the output archive."),
                                                               QString()));

    options.add("set-variable",
                new DynamicStringOption(tr("set-variable", "name"), tr("The station to set"),
                                        tr("This is the variable that values are set to."),
                                        tr("No change", "default set variable")));

    options.add("set-capture-variable", new DynamicStringOption(tr("capture-variable", "name"),
                                                                tr("The regular expression to capture from the variable"),
                                                                tr("This is a regular expression the variable must match to be "
                                                                   "changed.  When set the captures ${1}, ${2}, ... are available "
                                                                   "for substitution in the output variable."),
                                                                QString()));

    options.add("set-flavor-add",
                new DynamicStringOption(tr("set-flavor-add", "name"), tr("A flavor to add"),
                                        tr("This is a flavor that is added."),
                                        tr("No change", "default add flavor")));

    options.add("set-flavor-remove",
                new DynamicStringOption(tr("set-flavor-remove", "name"), tr("A flavor to remove"),
                                        tr("This is a flavor that is removed."),
                                        tr("No change", "default removed flavor")));


    options.add("start",
                new ComponentOptionSingleTime(tr("start", "name"), tr("The start time to output"),
                                              tr("This is the start time to output data.  If this is set then no "
                                                 "data before this time is output."), QString()));

    options.add("end",
                new ComponentOptionSingleTime(tr("end", "name"), tr("The end time to output"),
                                              tr("This is the end time to output data.  If this is set then no "
                                                 "data after this time is output."), QString()));

    return options;
}

QList<ComponentExample> SelectComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    qobject_cast<DynamicSequenceSelectionOption *>(options.get("include"))->set("", "", "BsG_S11");
    examples.append(ComponentExample(options, tr("Select variable", "select variable example name"),
                                     tr("This creates output that only contains a single variable.")));

    options = getOptions();
    qobject_cast<DynamicSequenceSelectionOption *>(options.get("include"))->set("bnd", "raw_meta",
                                                                                "BsG_S11",
                                                                                SequenceName::Flavors());
    qobject_cast<DynamicBoolOption *>(options.get("explicit-meta"))->set(true);
    qobject_cast<DynamicBoolOption *>(options.get("remove-undefined"))->set(true);
    qobject_cast<DynamicStringOption *>(options.get("path"))->set("^Wavelength");
    qobject_cast<DynamicStringOption *>(options.get("set-archive"))->set("raw");
    examples.append(
            ComponentExample(options, tr("Extract wavelength", "wavelength select example name"),
                             tr("This creates output that contains only the wavelength information "
                                "from a single variable.")));

    return examples;
}

ProcessingStage *SelectComponent::createGeneralFilterDynamic(const ComponentOptions &options)
{ return new Select(options); }

ProcessingStage *SelectComponent::createGeneralFilterEditing(double start,
                                                             double end,
                                                             const SequenceName::Component &station,
                                                             const SequenceName::Component &archive,
                                                             const ValueSegment::Transfer &config)
{ return new Select(start, end, station, archive, config); }

ProcessingStage *SelectComponent::deserializeGeneralFilter(QDataStream &stream)
{ return new Select(stream); }
