/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/processingstage.hxx"
#include "core/component.hxx"

#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "datacore/stream.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace CPD3 {
namespace Data {
bool operator==(const SequenceValue &a, const SequenceValue &b)
{ return SequenceValue::ValueEqual()(a, b); }
}
}

class TestComponent : public QObject {
Q_OBJECT

    ProcessingStageComponent *component;

private slots:

    void initTestCase()
    {
        component = qobject_cast<ProcessingStageComponent *>(ComponentLoader::create("select"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("include")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("exclude")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("path-apply")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("set-apply")));
        QVERIFY(qobject_cast<DynamicStringOption *>(options.get("path")));
        QVERIFY(qobject_cast<DynamicBoolOption *>(options.get("path-remove")));
        QVERIFY(qobject_cast<DynamicBoolOption *>(options.get("set-duplicate")));
        QVERIFY(qobject_cast<DynamicStringOption *>(options.get("set-station")));
        QVERIFY(qobject_cast<DynamicStringOption *>(options.get("set-capture-station")));
        QVERIFY(qobject_cast<DynamicStringOption *>(options.get("set-archive")));
        QVERIFY(qobject_cast<DynamicStringOption *>(options.get("set-capture-archive")));
        QVERIFY(qobject_cast<DynamicStringOption *>(options.get("set-variable")));
        QVERIFY(qobject_cast<DynamicStringOption *>(options.get("set-capture-variable")));
        QVERIFY(qobject_cast<DynamicStringOption *>(options.get("set-flavor-add")));
        QVERIFY(qobject_cast<DynamicStringOption *>(options.get("set-flavor-remove")));
        QVERIFY(qobject_cast<DynamicBoolOption *>(options.get("explicit-meta")));
        QVERIFY(qobject_cast<ComponentOptionSingleTime *>(options.get("start")));
        QVERIFY(qobject_cast<ComponentOptionSingleTime *>(options.get("end")));
    }

    void includeExclude()
    {
        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("include"))->set("", "",
                                                                                    "Bs[BGR]_S11");
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("exclude"))->set("", "",
                                                                                    "BsR_S11");

        ProcessingStage *filter = component->createGeneralFilterDynamic(options);
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName ui("brw", "raw", "BsG_S11");
        SequenceName ue("brw", "raw", "BsR_S11");
        SequenceName uo("brw", "raw", "BaG_A11");
        filter->incomingData(SequenceValue(ui.toMeta(), Variant::Root(0.25), 0.5, 100.0));
        filter->incomingData(SequenceValue(ue.toMeta(), Variant::Root(0.5), 0.5, 100.0));
        filter->incomingData(SequenceValue(uo.toMeta(), Variant::Root(0.75), 0.5, 100.0));
        filter->incomingData(SequenceValue(ui, Variant::Root(1.0), 1.0, 2.0));
        filter->incomingData(SequenceValue(ue, Variant::Root(2.0), 2.0, 3.0));
        filter->incomingData(SequenceValue(uo, Variant::Root(3.0), 3.0, 4.0));
        filter->endData();

        QVERIFY(filter->wait());
        delete filter;

        QCOMPARE((int) e.values().size(), 2);
        QCOMPARE(e.values()[0], SequenceValue(ui.toMeta(), Variant::Root(0.25), 0.5, 100.0));
        QCOMPARE(e.values()[1], SequenceValue(ui, Variant::Root(1.0), 1.0, 2.0));
    }

    void startEnd()
    {
        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<ComponentOptionSingleTime *>(options.get("start"))->set(100);
        qobject_cast<ComponentOptionSingleTime *>(options.get("end"))->set(200);

        ProcessingStage *filter = component->createGeneralFilterDynamic(options);
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName u("brw", "raw", "BsG_S11");
        filter->incomingData(SequenceValue(u, Variant::Root(1.0), 50.0, 60.0));
        filter->incomingData(SequenceValue(u.toMeta(), Variant::Root(2.0), 50.0, 150.0));
        filter->incomingData(SequenceValue(u, Variant::Root(3.0), 60.0, 70.0));
        filter->incomingData(SequenceValue(u, Variant::Root(4.0), 70.0, 250.0));
        filter->incomingData(SequenceValue(u, Variant::Root(5.0), 100.0, 200.0));
        filter->incomingData(SequenceValue(u, Variant::Root(6.0), 110.0, 190.0));
        filter->incomingData(SequenceValue(u, Variant::Root(7.0), 150.0, 210.0));
        filter->incomingData(SequenceValue(u, Variant::Root(8.0), 160.0, 190.0));
        filter->incomingData(SequenceValue(u, Variant::Root(9.0), 200.0, 210.0));
        filter->incomingData(SequenceValue(u, Variant::Root(10.0), 250.0, 300.0));
        filter->endData();

        QVERIFY(filter->wait());
        delete filter;

        QCOMPARE((int) e.values().size(), 6);
        QCOMPARE(e.values()[0], SequenceValue(u.toMeta(), Variant::Root(2.0), 100.0, 150.0));
        QCOMPARE(e.values()[0].getValue(), Variant::Root(2.0).read());
        QCOMPARE(e.values()[1], SequenceValue(u, Variant::Root(4.0), 100.0, 200.0));
        QCOMPARE(e.values()[1].getValue(), Variant::Root(4.0).read());
        QCOMPARE(e.values()[2], SequenceValue(u, Variant::Root(5.0), 100.0, 200.0));
        QCOMPARE(e.values()[2].getValue(), Variant::Root(5.0).read());
        QCOMPARE(e.values()[3], SequenceValue(u, Variant::Root(6.0), 110.0, 190.0));
        QCOMPARE(e.values()[3].getValue(), Variant::Root(6.0).read());
        QCOMPARE(e.values()[4], SequenceValue(u, Variant::Root(7.0), 150.0, 200.0));
        QCOMPARE(e.values()[4].getValue(), Variant::Root(7.0).read());
        QCOMPARE(e.values()[5], SequenceValue(u, Variant::Root(8.0), 160.0, 190.0));
        QCOMPARE(e.values()[5].getValue(), Variant::Root(8.0).read());
    }

    void pathSelect()
    {
        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<DynamicStringOption *>(options.get("path"))->set("/A/B");

        ProcessingStage *filter = component->createGeneralFilterDynamic(options);
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName u1("brw", "raw", "BsG_S11");
        Variant::Root v;
        v.write().hash("A").hash("B").setDouble(1.0);
        v.write().hash("A").hash("C").setDouble(2.0);
        v.write().hash("D").setDouble(3.0);

        filter->incomingData(SequenceValue(u1, v, 1.0, 2.0));
        filter->endData();

        QVERIFY(filter->wait());
        delete filter;

        QCOMPARE((int) e.values().size(), 1);
        QCOMPARE(e.values()[0], SequenceValue(u1, Variant::Root(1.0), 1.0, 2.0));
        QCOMPARE(e.values()[0].getValue(), Variant::Root(1.0).read());
    }

    void unitAlter()
    {
        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<DynamicStringOption *>(options.get("set-station"))->set("bnd");
        qobject_cast<DynamicStringOption *>(options.get("set-archive"))->set("clean");
        qobject_cast<DynamicStringOption *>(options.get("set-capture-variable"))->set(
                "Bs(G)_S11");
        qobject_cast<DynamicStringOption *>(options.get("set-variable"))->set("Ba${1}_A11");
        qobject_cast<DynamicStringOption *>(options.get("set-flavor-add"))->set("pm1");
        qobject_cast<DynamicStringOption *>(options.get("set-flavor-remove"))->set("pm10");

        ProcessingStage *filter = component->createGeneralFilterDynamic(options);
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName u1("brw", "raw", "BsG_S11", {"pm10"});
        filter->incomingData(SequenceValue(u1.toMeta(), Variant::Root(1.0), 1.0, 2.0));
        filter->incomingData(SequenceValue(u1, Variant::Root(2.0), 2.0, 3.0));
        filter->endData();

        QVERIFY(filter->wait());
        delete filter;

        u1 = SequenceName("bnd", "clean", "BaG_A11", {"pm1"});
        QCOMPARE((int) e.values().size(), 2);
        QCOMPARE(e.values()[0], SequenceValue(u1.toMeta(), Variant::Root(1.0), 1.0, 2.0));
        QCOMPARE(e.values()[1], SequenceValue(u1, Variant::Root(2.0), 2.0, 3.0));
    }

    void complex()
    {
        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<DynamicStringOption *>(options.get("set-archive"))->set("raw");
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("set-apply"))->set("", "",
                                                                                      "BsB_S11");
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("include"))->set("", "",
                                                                                    "Bs[BGR]_S11");
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("exclude"))->set("", "",
                                                                                    "BsR_S11");
        qobject_cast<DynamicStringOption *>(options.get("path"))->set("/A/B");
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("path-apply"))->set("",
                                                                                       "raw_meta",
                                                                                       "BsB_S11");
        qobject_cast<DynamicBoolOption *>(options.get("explicit-meta"))->set(true);

        ProcessingStage *filter = component->createGeneralFilterDynamic(options);
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        Variant::Root v;
        v.write().hash("A").hash("B").setDouble(0.5);
        v.write().hash("A").hash("C").setDouble(2.0);
        v.write().hash("D").setDouble(3.0);

        SequenceName u1("brw", "raw", "BsG_S11");
        SequenceName u2("brw", "raw", "BsR_S11");
        SequenceName u3("brw", "raw", "BsB_S11");
        SequenceName u4("brw", "raw", "BaG_A11");
        filter->incomingData(SequenceValue(u3.toMeta(), v, 1.0, 2.0));
        filter->incomingData(SequenceValue(u1, Variant::Root(1.0), 2.0, 3.0));
        filter->incomingData(SequenceValue(u2, Variant::Root(2.0), 3.0, 4.0));
        filter->incomingData(SequenceValue(u3, Variant::Root(3.0), 4.0, 5.0));
        filter->incomingData(SequenceValue(u4, Variant::Root(4.0), 5.0, 6.0));
        filter->endData();

        QVERIFY(filter->wait());
        delete filter;

        QCOMPARE((int) e.values().size(), 3);
        QCOMPARE(e.values()[0], SequenceValue(u3, Variant::Root(0.5), 1.0, 2.0));
        QCOMPARE(e.values()[0].getValue(), Variant::Root(0.5).read());
        QCOMPARE(e.values()[1], SequenceValue(u1, Variant::Root(1.0), 2.0, 3.0));
        QCOMPARE(e.values()[1].getValue(), Variant::Root(1.0).read());
        QCOMPARE(e.values()[2], SequenceValue(u3, Variant::Root(3.0), 4.0, 5.0));
        QCOMPARE(e.values()[2].getValue(), Variant::Root(3.0).read());
    }

    void serialize()
    {
        ComponentOptions options;
        options = component->getOptions();
        qobject_cast<DynamicStringOption *>(options.get("set-archive"))->set("clean");

        ProcessingStage *filter = component->createGeneralFilterDynamic(options);
        QVERIFY(filter != NULL);
        filter->start();
        StreamSink::Buffer e;
        filter->setEgress(&e);

        SequenceName u1("brw", "raw", "T_S11");
        for (int i = 0; i < 10000; i++) {
            filter->incomingData(SequenceValue(u1, Variant::Root(1.0 + i), 1.0 + i, 2.0 + i));
        }
        filter->setEgress(NULL);

        QByteArray data;
        {
            QDataStream stream(&data, QIODevice::WriteOnly);
            filter->serialize(stream);
        }
        filter->signalTerminate();
        QVERIFY(filter->wait());
        delete filter;
        {
            QDataStream stream(&data, QIODevice::ReadOnly);
            filter = component->deserializeGeneralFilter(stream);
            QVERIFY(stream.atEnd());
        }
        QVERIFY(filter != NULL);
        filter->start();
        filter->setEgress(&e);

        for (int i = 10000; i < 20000; i++) {
            filter->incomingData(SequenceValue(u1, Variant::Root(1.0 + i), 1.0 + i, 2.0 + i));
        }
        filter->endData();

        QVERIFY(filter->wait());
        delete filter;

        u1.setArchive("clean");

        QCOMPARE((int) e.values().size(), 20000);
        for (int i = 10000; i < 20000; i++) {
            QCOMPARE(e.values()[i], SequenceValue(u1, Variant::Root(1.0 + i), 1.0 + i, 2.0 + i));
        }
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
