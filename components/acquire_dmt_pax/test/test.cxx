/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray outgoing;
    QByteArray incoming;
    double unpolledRemaining;
    int mode;
    double modeRemainingTime;

    QDateTime time;
    double Bs;
    double IBs;
    double Ba;
    double BaPhase;
    double Bag;
    double VAlaser;
    double laserPhase;
    double Q;
    double micPressure;
    double C;
    double Bsz;
    double IMic;
    double Baz;
    double BazPhase;
    double RHsample;
    double Tsample;
    double analogVPlus;
    double analogVMinus;
    double V3;
    double V5;
    double V12;
    double Aphotodiode;
    double Alaser;
    double Tlaser;
    double Pcell;
    double Pinlet;
    double Pvacuum;


    ModelInstrument() : unpolledRemaining(0),
                        mode(0),
                        modeRemainingTime(600),
                        time(QDate(2013, 2, 3), QTime(1, 2, 0))
    {
        Bs = 10.0;
        IBs = 5.12;
        Ba = 5.0;
        BaPhase = 12.0;
        Bag = 0.1;
        VAlaser = 20.0;
        laserPhase = 12.5;
        Q = 1.2;
        micPressure = 35.0;
        C = 2300.0;
        Bsz = 0.5;
        IMic = 6.15;
        Baz = 0.25;
        BazPhase = 6.0;
        RHsample = 23.0;
        Tsample = 25.0;
        analogVPlus = 5.7;
        analogVMinus = -5.0;
        V3 = 3.3;
        V5 = 5.0;
        V12 = 12.0;
        Aphotodiode = 0.15;
        Alaser = 2.0;
        Tlaser = 30.0;
        Pcell = 1010.0;
        Pinlet = 1011.0;
        Pvacuum = 800.0;
    }

    void advance(double seconds)
    {
        modeRemainingTime -= seconds;
        if (modeRemainingTime <= 0.0) {
            switch (mode) {
            case 0:
                mode = 1;
                modeRemainingTime = 30.0;
                break;
            case 1:
                mode = 2;
                modeRemainingTime = 10.0;
                break;
            case 2:
                mode = 0;
                modeRemainingTime = 600.0;
                break;
            case 3:
                mode = 0;
                modeRemainingTime = 600.0;
                break;
            }
        }

        unpolledRemaining -= seconds;
        while (unpolledRemaining <= 0.0) {
            unpolledRemaining += 10.0;

            QDateTime startOfDay(time.date(), QTime(0, 0, 0));

            uint secondsAfterDay = time.toTime_t() - startOfDay.toTime_t();
            outgoing.append(QByteArray::number(secondsAfterDay));
            outgoing.append(',');
            outgoing.append(QByteArray::number(time.date().dayOfYear() - 1));
            outgoing.append(',');
            outgoing.append(QByteArray::number(time.date().year()));
            outgoing.append(',');
            outgoing.append(QByteArray::number(secondsAfterDay));
            outgoing.append(',');
            outgoing.append(QByteArray::number(time.date().dayOfYear() - 1));
            outgoing.append(',');
            outgoing.append(QByteArray::number(time.date().year()));
            outgoing.append(',');
            outgoing.append(QByteArray::number(Bs, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(IBs, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(Ba, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(BaPhase, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(Bag, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(VAlaser, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(laserPhase, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(Q, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(micPressure, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(C, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(Bsz, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(IMic, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(Baz, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(BazPhase, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(Bs + Ba, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(Bs / (Bs + Ba), 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(Ba * 6.0, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(RHsample, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(Tsample, 'f', 2));
            outgoing.append(",-10.0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,");
            outgoing.append(QByteArray::number(analogVPlus, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(V3, 'f', 2));
            outgoing.append(",0.12,");
            outgoing.append(QByteArray::number(V5, 'f', 2));
            outgoing.append(",3.0,0.13,0.14,");
            outgoing.append(QByteArray::number(Aphotodiode, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(Alaser, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(Tlaser, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(analogVMinus, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(Pcell, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(Pinlet, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(Pvacuum, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(V12, 'f', 2));
            outgoing.append(",10,");
            outgoing.append(QByteArray::number(mode));
            outgoing.append(',');
            outgoing.append(QByteArray::number((int) modeRemainingTime));
            outgoing.append(",1.5,100,0,ABSENT,0,2013-01-01,00:00:00\n");
        }
    }
};

class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream,
                   double time = FP::undefined(),
                   const SequenceName::Component &wl = "G")
    {
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Bs" + wl, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Ba" + wl, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Ba" + wl + "g", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZBa" + wl + "Phase", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZIMic" + wl, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZIBs" + wl, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("P1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("P2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Pu", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("U", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("C", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("A1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("A2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("VA", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZLaserPhase", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZMicPressure", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZQ", Variant::Root(), QString(), time))
            return false;

        if (!stream.hasMeta("Tz", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Pz", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Bsz" + wl, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Baz" + wl, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZBaz" + wl + "Phase", Variant::Root(), QString(), time))
            return false;

        if (!stream.hasMeta("ZSPANCHECK", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("PCTc" + wl, Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream,
                           double time = FP::undefined(),
                           const SequenceName::Component &wl = "G")
    {
        if (!stream.hasMeta("V1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("V2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("V3", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("V4", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("V5", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZRTIME", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Bszd" + wl, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Bazd" + wl, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZBs" + wl, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZBa" + wl, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZBa" + wl + "g", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZBa" + wl + "Phase2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZSTATE", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkContiguous(StreamCapture &stream, const SequenceName::Component &wl = "G")
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("Bs" + wl))
            return false;
        if (!stream.checkContiguous("Ba" + wl))
            return false;
        if (!stream.checkContiguous("Ba" + wl + "g"))
            return false;
        if (!stream.checkContiguous("ZBa" + wl + "Phase"))
            return false;
        if (!stream.checkContiguous("ZIMic" + wl))
            return false;
        if (!stream.checkContiguous("ZIBs" + wl))
            return false;
        if (!stream.checkContiguous("P1"))
            return false;
        if (!stream.checkContiguous("P2"))
            return false;
        if (!stream.checkContiguous("Pu"))
            return false;
        if (!stream.checkContiguous("T1"))
            return false;
        if (!stream.checkContiguous("T2"))
            return false;
        if (!stream.checkContiguous("U"))
            return false;
        if (!stream.checkContiguous("C"))
            return false;
        if (!stream.checkContiguous("A1"))
            return false;
        if (!stream.checkContiguous("A2"))
            return false;
        if (!stream.checkContiguous("VA"))
            return false;
        if (!stream.checkContiguous("ZLaserPhase"))
            return false;
        if (!stream.checkContiguous("ZMicPressure"))
            return false;
        if (!stream.checkContiguous("ZQ"))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream,
                     const ModelInstrument &model,
                     double time = FP::undefined(),
                     const SequenceName::Component &wl = "G")
    {
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("Bs" + wl, Variant::Root(model.Bs), time))
            return false;
        if (!stream.hasAnyMatchingValue("Ba" + wl, Variant::Root(model.Ba), time))
            return false;
        if (!stream.hasAnyMatchingValue("Ba" + wl + "g", Variant::Root(model.Bag), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZBa" + wl + "Phase", Variant::Root(model.BaPhase), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZIMic" + wl, Variant::Root(model.IMic), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZIBs" + wl, Variant::Root(model.IBs), time))
            return false;
        if (!stream.hasAnyMatchingValue("P1", Variant::Root(model.Pcell), time))
            return false;
        if (!stream.hasAnyMatchingValue("P2", Variant::Root(model.Pvacuum), time))
            return false;
        if (!stream.hasAnyMatchingValue("Pu", Variant::Root(model.Pinlet), time))
            return false;
        if (!stream.hasAnyMatchingValue("T1", Variant::Root(model.Tsample), time))
            return false;
        if (!stream.hasAnyMatchingValue("T2", Variant::Root(model.Tlaser), time))
            return false;
        if (!stream.hasAnyMatchingValue("U", Variant::Root(model.RHsample), time))
            return false;
        if (!stream.hasAnyMatchingValue("C", Variant::Root(model.C), time))
            return false;
        if (!stream.hasAnyMatchingValue("A1", Variant::Root(model.Alaser), time))
            return false;
        if (!stream.hasAnyMatchingValue("A2", Variant::Root(model.Aphotodiode), time))
            return false;
        if (!stream.hasAnyMatchingValue("VA", Variant::Root(model.VAlaser), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZLaserPhase", Variant::Root(model.laserPhase), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZMicPressure", Variant::Root(model.micPressure), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZQ", Variant::Root(model.Q), time))
            return false;
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream,
                             const ModelInstrument &model,
                             double time = FP::undefined(),
                             const SequenceName::Component &wl = "G")
    {
        if (!stream.hasAnyMatchingValue("V1", Variant::Root(model.V3), time))
            return false;
        if (!stream.hasAnyMatchingValue("V2", Variant::Root(model.V5), time))
            return false;
        if (!stream.hasAnyMatchingValue("V3", Variant::Root(model.V12), time))
            return false;
        if (!stream.hasAnyMatchingValue("V4", Variant::Root(model.analogVMinus), time))
            return false;
        if (!stream.hasAnyMatchingValue("V5", Variant::Root(model.analogVPlus), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZRTIME", Variant::Root(), time))
            return false;

        if (!stream.hasAnyMatchingValue("ZBs" + wl, Variant::Root(model.Bs), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZBa" + wl, Variant::Root(model.Ba), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZBa" + wl + "g", Variant::Root(model.Bag), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZBa" + wl + "Phase2", Variant::Root(model.BaPhase), time))
            return false;

        if (!stream.hasAnyMatchingValue("ZSTATE", Variant::Root(), time))
            return false;

        return true;
    }

    bool checkZero(StreamCapture &stream,
                   const ModelInstrument &model,
                   double time = FP::undefined(),
                   const SequenceName::Component &wl = "G")
    {
        if (!stream.hasAnyMatchingValue("Baz" + wl, Variant::Root(model.Baz), time))
            return false;
        if (!stream.hasAnyMatchingValue("Bsz" + wl, Variant::Root(model.Bsz), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZBaz" + wl + "Phase", Variant::Root(model.BazPhase), time))
            return false;
        if (!stream.hasAnyMatchingValue("Tz", Variant::Root(model.Tsample), time))
            return false;
        if (!stream.hasAnyMatchingValue("Pz", Variant::Root(model.Pcell), time))
            return false;
        return true;
    }

private slots:

    void initTestCase()
    {
        component =
                qobject_cast<AcquisitionComponent *>(ComponentLoader::create("acquire_dmt_pax"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_dmt_pax"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("wavelength")));
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(5.0);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(5.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(5.0);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        instrument.modeRemainingTime = 0.0;
        for (int i = 0; i < 100; i++) {
            control.advance(10.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(checkZero(persistent, instrument));
        QVERIFY(checkZero(realtime, instrument));
    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Wavelength"].setDouble(880);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 200; i++) {
            control.advance(5.0);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging, FP::undefined(), "Q"));
        QVERIFY(checkMeta(realtime, FP::undefined(), "Q"));
        QVERIFY(checkRealtimeMeta(realtime, FP::undefined(), "Q"));

        QVERIFY(checkValues(logging, instrument, FP::undefined(), "Q"));
        QVERIFY(checkValues(realtime, instrument, FP::undefined(), "Q"));
        QVERIFY(checkRealtimeValues(realtime, instrument, FP::undefined(), "Q"));
    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 200; i++) {
            control.advance(5.0);
            QTest::qSleep(50);
        }

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
