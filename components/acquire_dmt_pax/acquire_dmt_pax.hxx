/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIREDMTPAX_H
#define ACQUIREDMTPAX_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "acquisition/spancheck.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"

class AcquireDMTPAX : public CPD3::Acquisition::FramedInstrument {
    double lastRecordTime;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum ResponseState {
        /* Waiting for enough valid records to confirm autoprobe success */
        RESP_AUTOPROBE_WAIT,

        /* Waiting for a valid record to start acquisition */
        RESP_WAIT,

        /* Acquiring data in passive mode */
        RESP_RUN,

        /* Same as wait, but discard the first timeout */
        RESP_INITIALIZE,

        /* Same as initialize but autoprobing */
        RESP_AUTOPROBE_INITIALIZE,
    };
    ResponseState responseState;
    int autoprobeValidRecords;

    enum {
        /* Normal operation. */
                SAMPLE_RUN,

        /* Air flush */
                SAMPLE_FLUSH,

        /* Zero in progress */
                SAMPLE_ZERO_RUN,

        /* Acoustic calibration in progress */
                SAMPLE_AC_RUN,

        /* In spancheck state, under the control of the spancheck controller. */
                SAMPLE_SPANCHECK,
    } sampleState;

    class SpancheckInterface
            : public CPD3::Acquisition::NephelometerSpancheckController::Interface {
        AcquireDMTPAX *parent;
    public:
        SpancheckInterface(AcquireDMTPAX *neph);

        virtual ~SpancheckInterface();

        virtual void setBypass(bool enable);

        virtual void switchToFilteredAir();

        virtual void switchToGas(CPD3::Algorithms::Rayleigh::Gas gas);

        virtual void issueZero();

        virtual void issueCommand(const QString &target, const CPD3::Data::Variant::Read &command);

        virtual bool start();

        virtual void completed();

        virtual void aborted();
    };

    SpancheckInterface spancheckInterface;

    friend class Configuration;

    class Configuration {
        double start;
        double end;
    public:
        double wavelength;
        bool strictMode;
        double reportInterval;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under, const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    friend class SpancheckInterface;

    CPD3::Acquisition::NephelometerSpancheckController *spancheckController;

    void setDefaultInvalid();

    CPD3::Data::Variant::Root instrumentMeta;

    /* State that needs to be saved */
    CPD3::Data::SequenceValue spancheckDetails;

    CPD3::Data::SequenceValue lastMode;

    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;
    bool realtimeStateUpdated;

    double zeroEffectiveTime;
    CPD3::Data::Variant::Root Bsz;
    CPD3::Data::Variant::Root Baz;
    CPD3::Data::Variant::Root ZBazPhase;
    CPD3::Data::Variant::Root Bszd;
    CPD3::Data::Variant::Root Bazd;
    CPD3::Data::Variant::Root Tz;
    CPD3::Data::Variant::Root Pz;
    bool zeroPersistentUpdated;
    bool zeroRealtimeUpdated;
    int zeroTCount;
    double zeroTSum;
    int zeroPCount;
    double zeroPSum;

    void logValue(double startTime,
                  double endTime,
                  CPD3::Data::SequenceName::Component name,
                  CPD3::Data::Variant::Root &&value);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root &&value);

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    void handleZeroUpdate(double currentTime);

    void zeroCompleted(double currentTime,
                       const CPD3::Data::Variant::Read &Bsz,
                       const CPD3::Data::Variant::Read &Baz,
                       const CPD3::Data::Variant::Read &ZBazPhase);

    void configAdvance(double frameTime);

    void configurationChanged();

    int processRecord(const CPD3::Util::ByteView &line, double &frameTime);

    CPD3::Data::SequenceValue::Transfer constructSpancheckVariables();

    void updateSpancheckData();

public:
    AcquireDMTPAX(const CPD3::Data::ValueSegment::Transfer &config,
                  const std::string &loggingContext);

    AcquireDMTPAX(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireDMTPAX();

    virtual CPD3::Data::SequenceValue::Transfer getPersistentValues();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    virtual CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus();

    virtual CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus();

    virtual CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables();

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

    virtual void serializeState(QDataStream &stream);

    virtual void deserializeState(QDataStream &stream);

protected:
    virtual void command(const CPD3::Data::Variant::Read &value);

    virtual void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingInstrumentTimeout(double time);

    virtual void discardDataCompleted(double time);
};

class AcquireDMTPAXComponent
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_dmt_pax"
                              FILE
                              "acquire_dmt_pax.json")

    CPD3::ComponentOptions getBaseOptions();

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
