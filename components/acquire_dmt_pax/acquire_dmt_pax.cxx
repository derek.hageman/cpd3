/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/wavelength.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_dmt_pax.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;


AcquireDMTPAX::Configuration::Configuration() : start(FP::undefined()),
                                                end(FP::undefined()),
                                                wavelength(532),
                                                strictMode(true),
                                                reportInterval(10.0)
{ }

AcquireDMTPAX::Configuration::Configuration(const Configuration &other, double s, double e) : start(
        s),
                                                                                              end(e),
                                                                                              wavelength(
                                                                                                      other.wavelength),
                                                                                              strictMode(
                                                                                                      other.strictMode),
                                                                                              reportInterval(
                                                                                                      other.reportInterval)
{ }

AcquireDMTPAX::Configuration::Configuration(const ValueSegment &other, double s, double e) : start(
        s), end(e), wavelength(532), strictMode(true), reportInterval(10.0)
{
    setFromSegment(other);
}

AcquireDMTPAX::Configuration::Configuration(const Configuration &under,
                                            const ValueSegment &over,
                                            double s,
                                            double e) : start(s),
                                                        end(e),
                                                        wavelength(under.wavelength),
                                                        strictMode(under.strictMode),
                                                        reportInterval(under.reportInterval)
{
    setFromSegment(over);
}

void AcquireDMTPAX::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["StrictMode"].exists())
        strictMode = config["StrictMode"].toBool();

    {
        double v = config["ReportInterval"].toDouble();
        if (FP::defined(v) && v > 0.0)
            reportInterval = v;
    }

    if (FP::defined(config["Wavelength"].toDouble()))
        wavelength = config["Wavelength"].toDouble();
}


void AcquireDMTPAX::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("DMT");
    instrumentMeta["Model"].setString("PAX");

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    realtimeStateUpdated = true;

    zeroEffectiveTime = FP::undefined();
    zeroPersistentUpdated = true;
    zeroRealtimeUpdated = true;
    zeroTCount = 0;
    zeroTSum = 0;
    zeroPCount = 0;
    zeroPSum = 0;

    spancheckDetails.setUnit(SequenceName({}, "raw", "ZSPANCHECK"));
    lastMode.setUnit(SequenceName({}, "raw", "F2"));
}

static Variant::Root defaultSpancheckConfiguration()
{
    Variant::Root result;
    return result;
}

AcquireDMTPAX::AcquireDMTPAX(const ValueSegment::Transfer &configData,
                             const std::string &loggingContext) : FramedInstrument("dmtpax",
                                                                                   loggingContext),
                                                                  lastRecordTime(FP::undefined()),
                                                                  autoprobeStatus(
                                                                          AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                  responseState(RESP_WAIT),
                                                                  autoprobeValidRecords(0),
                                                                  sampleState(SAMPLE_RUN),
                                                                  spancheckInterface(this)
{
    spancheckController =
            new NephelometerSpancheckController(ValueSegment::withPath(configData, "Spancheck"),
                                                defaultSpancheckConfiguration(),
                                                NephelometerSpancheckController::NoCounts);
    spancheckController->setInterface(&spancheckInterface);

    setDefaultInvalid();
    config.append(Configuration());
    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
}

void AcquireDMTPAX::setToAutoprobe()
{
    responseState = RESP_AUTOPROBE_INITIALIZE;
    autoprobeValidRecords = 0;
}

void AcquireDMTPAX::setToInteractive()
{ responseState = RESP_INITIALIZE; }


ComponentOptions AcquireDMTPAXComponent::getBaseOptions()
{
    ComponentOptions options;

    ComponentOptionSingleDouble *d =
            new ComponentOptionSingleDouble(tr("wavelength", "name"), tr("Sample wavelength"),
                                            tr("This wavelength in nm of the laser in the PAX."),
                                            tr("532nm", "default wavelength"), 1);
    d->setMinimum(0.0, false);
    options.add("wavelength", d);

    return options;
}

AcquireDMTPAX::AcquireDMTPAX(const ComponentOptions &options, const std::string &loggingContext)
        : FramedInstrument("dmtpax", loggingContext),
          lastRecordTime(FP::undefined()),
          autoprobeStatus(AcquisitionInterface::AutoprobeStatus::InProgress),
          responseState(RESP_WAIT),
          autoprobeValidRecords(0),
          sampleState(SAMPLE_RUN),
          spancheckInterface(this)
{
    spancheckController = new NephelometerSpancheckController(ValueSegment::Transfer(),
                                                              defaultSpancheckConfiguration(),
                                                              NephelometerSpancheckController::NoCounts);
    spancheckController->setInterface(&spancheckInterface);

    setDefaultInvalid();

    config.append(Configuration());

    if (options.isSet("wavelength")) {
        double value =
                qobject_cast<ComponentOptionSingleDouble *>(options.get("wavelength"))->get();
        if (FP::defined(value) && value > 0.0)
            config.last().wavelength = value;
    }
}

AcquireDMTPAX::~AcquireDMTPAX()
{
    delete spancheckController;
}


void AcquireDMTPAX::logValue(double startTime,
                             double endTime,
                             SequenceName::Component name,
                             Variant::Root &&value)
{
    if (!loggingEgress && !realtimeEgress) return;
    SequenceValue dv({{}, "raw", std::move(name)}, std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        if (FP::defined(startTime))
            loggingEgress->incomingData(std::move(dv));
        return;
    }
    if (loggingEgress && FP::defined(startTime))
        loggingEgress->incomingData(dv);
    if (FP::defined(endTime)) {
        dv.setStart(endTime);
        dv.setEnd(endTime + 1.0);
    } else if (FP::defined(startTime)) {
        dv.setEnd(startTime + 1.0);
    }
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireDMTPAX::realtimeValue(double time, SequenceName::Component name, Variant::Root &&value)
{
    if (!realtimeEgress) return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

SequenceValue::Transfer AcquireDMTPAX::buildLogMeta(double time) const
{
    Q_ASSERT(!config.isEmpty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_dmt_pax");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());

    const auto &wlCode = Wavelength::code(config.first().wavelength);

    result.emplace_back(SequenceName({}, "raw_meta", "Bs" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);

    result.emplace_back(SequenceName({}, "raw_meta", "Ba" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light absorption coefficient");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);

    result.emplace_back(SequenceName({}, "raw_meta", "Ba" + wlCode + "g"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back().write().metadataReal("GroupUnits").setString("Noise");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light absorption coefficient noise estimate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);

    result.emplace_back(SequenceName({}, "raw_meta", "ZBa" + wlCode + "Phase"), Variant::Root(),
                        time, FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("degrees");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Light absorption vector phase angle to signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);

    result.emplace_back(SequenceName({}, "raw_meta", "ZIMic" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back().write().metadataReal("Format").setString("00.000000");
    result.back().write().metadataReal("Description").setString("Microphone raw intensity");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr(" Intensity"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "ZIBs" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back().write().metadataReal("Format").setString("00.000000");
    result.back().write().metadataReal("Description").setString("Scattering raw intensity");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr(" Intensity"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "P1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Sample pressure");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));

    result.emplace_back(SequenceName({}, "raw_meta", "P2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Vacuum pressure");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Vacuum"));

    result.emplace_back(SequenceName({}, "raw_meta", "Pu"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Inlet pressure");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Inlet"));

    result.emplace_back(SequenceName({}, "raw_meta", "T1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Sample temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));

    result.emplace_back(SequenceName({}, "raw_meta", "T2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Laser temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Laser"));

    result.emplace_back(SequenceName({}, "raw_meta", "U"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Sample relative humidity");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));

    result.emplace_back(SequenceName({}, "raw_meta", "C"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000000.00");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Resonant frequency");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Frequency"));

    result.emplace_back(SequenceName({}, "raw_meta", "A1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0000");
    result.back().write().metadataReal("Units").setString("A");
    result.back().write().metadataReal("Description").setString("Laser current");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Laser"));

    result.emplace_back(SequenceName({}, "raw_meta", "A2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0000");
    result.back().write().metadataReal("Units").setString("A");
    result.back().write().metadataReal("Description").setString("Photodiode current");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Photodiode"));

    result.emplace_back(SequenceName({}, "raw_meta", "VA"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0000");
    result.back().write().metadataReal("Units").setString("W");
    result.back().write().metadataReal("Description").setString("Laser power");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Laser"));

    result.emplace_back(SequenceName({}, "raw_meta", "ZLaserPhase"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000.000");
    result.back().write().metadataReal("Units").setString("degrees");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Laser phase relative to microphone");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Laser phase"));

    result.emplace_back(SequenceName({}, "raw_meta", "ZMicPressure"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000.000");
    result.back().write().metadataReal("Units").setString("dB");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Microphone pressure at resonant frequency");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Microphone pressure"));

    result.emplace_back(SequenceName({}, "raw_meta", "ZQ"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000.0000");
    result.back().write().metadataReal("Description").setString("Resonator Q factor");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Q Factor"));


    result.emplace_back(SequenceName({}, "raw_meta", "Tz"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Sample temperature during zero");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataReal("Realtime").hash("Hide").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "Pz"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Sample pressure during zero");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataReal("Realtime").hash("Hide").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "Bsz" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient during zero measurement");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Scat zero"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "Baz" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light absorption coefficient during zero measurement");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Abs zero"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "ZBaz" + wlCode + "Phase"), Variant::Root(),
                        time, FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("000.000");
    result.back().write().metadataReal("Units").setString("degrees");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Light absorption vector phase angle to signal during zero measurement");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataReal("Realtime").hash("Hide").setBool(true);


    result.emplace_back(SequenceName({}, "raw_meta", "ZSPANCHECK"),
                        spancheckController->metadata(NephelometerSpancheckController::FullResults,
                                                      processing), time, FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadata("Realtime").hash("Hide").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "PCTc" + wlCode),
                        spancheckController->metadata(
                                NephelometerSpancheckController::TotalPercentError, processing),
                        time, FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);
    result.back().write().metadata("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadata("Realtime").hash("Page").setInt64(1);
    result.back()
          .write()
          .metadata("Realtime")
          .hash("Name")
          .setString(QObject::tr("Spancheck Error"));
    result.back().write().metadata("Realtime").hash("NetworkPriority").setInt64(1);
    result.back().write().metadata("Realtime").hash("Persistent").setBool(true);


    result.emplace_back(SequenceName({}, "raw_meta", "F2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataInteger("Description").setString("Instrument mode number");
    result.back().write().metadataInteger("Source").set(instrumentMeta);
    result.back().write().metadataInteger("Processing").toArray().after_back().set(processing);
    result.back().write().metadataInteger("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataInteger("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataInteger("Realtime").hash("Hide").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataSingleFlag("Blank")
          .hash("Description")
          .setString("Data removed in blank period");
    result.back().write().metadataSingleFlag("Blank").hash("Bits").setInt64(0x40000000);
    result.back()
          .write()
          .metadataSingleFlag("Blank")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_pax");

    result.back()
          .write()
          .metadataSingleFlag("AcousticCalibration")
          .hash("Description")
          .setString("Data removed in acoustic calibration");
    result.back()
          .write()
          .metadataSingleFlag("AcousticCalibration")
          .hash("Bits")
          .setInt64(0x40000000);
    result.back()
          .write()
          .metadataSingleFlag("AcousticCalibration")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_pax");

    result.back()
          .write()
          .metadataSingleFlag("Zero")
          .hash("Description")
          .setString("Zero in progress");
    result.back().write().metadataSingleFlag("Zero").hash("Bits").setInt64(0x20000000);
    result.back()
          .write()
          .metadataSingleFlag("Zero")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_pax");

    result.back()
          .write()
          .metadataSingleFlag("Spancheck")
          .hash("Description")
          .setString("Spancheck in progress");
    result.back()
          .write()
          .metadataSingleFlag("Spancheck")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_pax");

    return result;
}

SequenceValue::Transfer AcquireDMTPAX::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_dmt_pax");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());

    const auto &wlCode = Wavelength::code(config.first().wavelength);

    result.emplace_back(SequenceName({}, "raw_meta", "V1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("3.3V supply");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("3.3V supply measurement"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "V2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("5V supply");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("5V supply measurement"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "V3"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("12V supply");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("12V supply measurement"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "V4"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Analog negative supply");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Analog negative"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "V5"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Analog positive supply");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Analog positive"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "ZRTIME"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataInteger("Format").setString("00000");
    result.back().write().metadataInteger("Units").setString("s");
    result.back()
          .write()
          .metadataInteger("Description")
          .setString("Time remaining in current state");
    result.back().write().metadataInteger("Source").set(instrumentMeta);
    result.back().write().metadataInteger("Processing").toArray().after_back().set(processing);
    result.back().write().metadataInteger("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataInteger("Realtime").hash("Name").setString("Time left");
    result.back().write().metadataInteger("Realtime").hash("Persistent").setBool(true);


    result.emplace_back(SequenceName({}, "raw_meta", "Bszd" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient zero shift");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr(" Shift"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "Bazd" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light absorption coefficient zero shift");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr(" Shift"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(1);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);


    result.emplace_back(SequenceName({}, "raw_meta", "ZBs" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light scattering coefficient");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Scattering"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");

    result.emplace_back(SequenceName({}, "raw_meta", "ZBa" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light absorption coefficient");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Absorption"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");

    result.emplace_back(SequenceName({}, "raw_meta", "ZBa" + wlCode + "g"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light absorption coefficient noise estimate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);

    result.emplace_back(SequenceName({}, "raw_meta", "ZBa" + wlCode + "Phase2"), Variant::Root(),
                        time, FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("degrees");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Light absorption vector phase angle to signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr(" Phase"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);
    result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");


    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataString("Realtime").hash("PageMask").setInt64((1 << 0) | (1 << 1));
    result.back().write().metadataString("Realtime").hash("Name").setString(QString());
    result.back().write().metadataString("Realtime").hash("Units").setString(QString());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back().write().metadataString("Realtime").hash("RowOrder").setInt64(2);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Run")
          .setString(QObject::tr("", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Zero")
          .setString(QObject::tr("Zero in progress", "zero state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Spancheck")
          .setString(QObject::tr("Spancheck in progress", "spancheck state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Blank")
          .setString(QObject::tr("Blank in progress", "spancheck state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("AcousticCalibration")
          .setString(QObject::tr("Acoustic calibration in progress",
                                 "acoustic calibration state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidResponse")
          .setString(QObject::tr("NO COMMS: Invalid response"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Timeout")
          .setString(QObject::tr("NO COMMS: Timeout"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Wait")
          .setString(QObject::tr("STARTING COMMS"));

    result.emplace_back(SequenceName({}, "raw_meta", "ZSPANCHECKSTATE"),
                        spancheckController->metadata(
                                NephelometerSpancheckController::RealtimeState, processing), time,
                        FP::undefined());
    result.back().write().metadata("Source").set(instrumentMeta);

    return result;
}

SequenceMatch::Composite AcquireDMTPAX::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "Tz");
    sel.append({}, {}, "Pz");
    sel.append({}, {}, "B[sa]zd?[BGRQ0-9]?");
    sel.append({}, {}, "F2");
    sel.append({}, {}, "PCTc[BGRQ0-9]?");
    sel.append({}, {}, "ZRTIME");
    sel.append({}, {}, "ZBaz[BGRQ0-9]?Phase");
    sel.append({}, {}, "ZSPANCHECK.*");
    sel.append({}, {}, "ZSTATE");
    return sel;
}

void AcquireDMTPAX::handleZeroUpdate(double currentTime)
{
    if ((!zeroPersistentUpdated || persistentEgress == NULL) &&
            (!zeroRealtimeUpdated || realtimeEgress == NULL))
        return;
    if (!FP::defined(zeroEffectiveTime))
        return;

    const auto &wlCode = Wavelength::code(config.first().wavelength);

    SequenceValue::Transfer result;

    if (Tz.read().exists()) {
        result.emplace_back(SequenceIdentity({{}, "raw", "Tz"}, zeroEffectiveTime, FP::undefined()),
                            Tz);
    }
    if (Pz.read().exists()) {
        result.emplace_back(SequenceIdentity({{}, "raw", "Pz"}, zeroEffectiveTime, FP::undefined()),
                            Pz);
    }
    result.emplace_back(
            SequenceIdentity({{}, "raw", "Bsz" + wlCode}, zeroEffectiveTime, FP::undefined()), Bsz);
    result.emplace_back(
            SequenceIdentity({{}, "raw", "Baz" + wlCode}, zeroEffectiveTime, FP::undefined()), Baz);
    result.emplace_back(SequenceIdentity({{}, "raw", "ZBaz" + wlCode + "Phase"}, zeroEffectiveTime,
                                         FP::undefined()), ZBazPhase);


    if (zeroPersistentUpdated && persistentEgress != NULL) {
        zeroPersistentUpdated = false;
        persistentEgress->incomingData(result);
    }

    if (!zeroRealtimeUpdated || realtimeEgress == NULL)
        return;
    zeroRealtimeUpdated = false;

    for (auto &det : result) {
        det.setStart(currentTime);
    }

    result.emplace_back(
            SequenceIdentity({{}, "raw", "Bszd" + wlCode}, currentTime, FP::undefined()), Bszd);
    result.emplace_back(
            SequenceIdentity({{}, "raw", "Bazd" + wlCode}, currentTime, FP::undefined()), Bazd);

    realtimeEgress->incomingData(std::move(result));
}

static double calculateZeroChange(double previous, double current)
{
    if (!FP::defined(previous) || !FP::defined(current))
        return FP::undefined();
    return current - previous;
}

void AcquireDMTPAX::zeroCompleted(double currentTime,
                                  const Variant::Read &Bsz,
                                  const Variant::Read &Baz,
                                  const Variant::Read &ZBazPhase)
{
    Q_UNUSED(ZBazPhase);

    zeroEffectiveTime = currentTime;
    zeroPersistentUpdated = true;
    zeroRealtimeUpdated = true;

    Bszd.write().setDouble(calculateZeroChange(this->Bsz.read().toDouble(), Bsz.toDouble()));
    Bazd.write().setDouble(calculateZeroChange(this->Baz.read().toDouble(), Baz.toDouble()));

    if (zeroTCount != 0)
        Tz.write().setDouble(zeroTSum / (double) zeroTCount);
    else
        Tz.write().setEmpty();
    if (zeroPCount != 0)
        Pz.write().setDouble(zeroPSum / (double) zeroPCount);
    else
        Pz.write().setEmpty();

    zeroTCount = 0;
    zeroTSum = 0;
    zeroPCount = 0;
    zeroPSum = 0;
}

int AcquireDMTPAX::processRecord(const Util::ByteView &line, double &frameTime)
{
    Q_ASSERT(!config.isEmpty());

    if (line.size() < 3)
        return 1;

    const auto &wlCode = Wavelength::code(config.first().wavelength);

    auto fields = Util::as_deque(line.split(','));
    bool ok = false;

    if (fields.empty()) return 2;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    qint64 isecond = field.parse_i32(&ok);
    if (!ok) return 3;
    if (isecond < 0) return 4;
    if (isecond > 86400) return 5;
    Variant::Root second(isecond);
    remap("SECOND", second);
    isecond = second.read().toInt64();

    if (fields.empty()) return 6;
    field = fields.front().string_trimmed();
    fields.pop_front();
    qint64 idoy = field.parse_i32(&ok);
    if (!ok) return 7;
    if (idoy < 0) return 8;
    if (idoy > 365) return 9;
    Variant::Root doy(idoy);
    remap("DOY", doy);
    idoy = doy.read().toInt64();

    if (fields.empty()) return 10;
    field = fields.front().string_trimmed();
    fields.pop_front();
    qint64 iyear = field.parse_i32(&ok);
    if (!ok) return 11;
    if (iyear < 1970) return 12;
    if (iyear > 2100) return 13;
    Variant::Root year(iyear);
    remap("YEAR", year);
    iyear = year.read().toInt64();

    /* Local second, DOY, and year */
    if (fields.empty()) return 14;
    fields.pop_front();
    if (fields.empty()) return 15;
    fields.pop_front();
    if (fields.empty()) return 16;
    fields.pop_front();

    if (fields.empty()) return 17;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Bs(field.parse_real(&ok));
    if (!ok) return 18;
    if (!FP::defined(Bs.read().toReal())) return 19;
    remap("Bs" + wlCode, Bs);

    if (fields.empty()) return 20;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZIBs(field.parse_real(&ok));
    if (!ok) return 21;
    if (!FP::defined(ZIBs.read().toReal())) return 22;
    remap("ZIBs" + wlCode, ZIBs);

    if (fields.empty()) return 23;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Ba(field.parse_real(&ok));
    if (!ok) return 24;
    if (!FP::defined(Ba.read().toReal())) return 25;
    remap("Ba" + wlCode, Ba);

    if (fields.empty()) return 26;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZBaPhase(field.parse_real(&ok));
    if (!ok) return 27;
    if (!FP::defined(ZBaPhase.read().toReal())) return 28;
    remap("ZBa" + wlCode + "Phase", ZBaPhase);

    if (fields.empty()) return 29;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Bag(field.parse_real(&ok));
    if (!ok) return 30;
    if (!FP::defined(Bag.read().toReal())) return 31;
    remap("Ba" + wlCode + "g", Bag);

    if (fields.empty()) return 32;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root VA(field.parse_real(&ok));
    if (!ok) return 33;
    if (!FP::defined(VA.read().toReal())) return 34;
    remap("VA", VA);

    if (fields.empty()) return 35;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZLaserPhase(field.parse_real(&ok));
    if (!ok) return 36;
    if (!FP::defined(ZLaserPhase.read().toReal())) return 37;
    remap("ZLaserPhase", ZLaserPhase);

    if (fields.empty()) return 38;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZQ(field.parse_real(&ok));
    if (!ok) return 39;
    if (!FP::defined(ZQ.read().toReal())) return 40;
    remap("ZQ", ZQ);

    if (fields.empty()) return 41;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZMicPressure(field.parse_real(&ok));
    if (!ok) return 42;
    if (!FP::defined(ZMicPressure.read().toReal())) return 43;
    remap("ZMicPressure", ZMicPressure);

    if (fields.empty()) return 44;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root C(field.parse_real(&ok));
    if (!ok) return 45;
    if (!FP::defined(C.read().toReal())) return 46;
    remap("C", C);

    if (fields.empty()) return 47;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Bsz(field.parse_real(&ok));
    if (!ok) return 48;
    if (!FP::defined(Bsz.read().toReal())) return 49;
    remap("Bsz" + wlCode, Bsz);

    if (fields.empty()) return 50;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZIMic(field.parse_real(&ok));
    if (!ok) return 51;
    if (!FP::defined(ZIMic.read().toReal())) return 52;
    remap("ZIMic" + wlCode, ZIMic);

    if (fields.empty()) return 53;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Baz(field.parse_real(&ok));
    if (!ok) return 54;
    if (!FP::defined(Baz.read().toReal())) return 55;
    remap("Baz" + wlCode, Baz);

    if (fields.empty()) return 56;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZBazPhase(field.parse_real(&ok));
    if (!ok) return 57;
    if (!FP::defined(ZBazPhase.read().toReal())) return 58;
    remap("ZBaz" + wlCode + "Phase", ZBazPhase);

    /* Be, ignored (equal to Ba + Bs) */
    if (fields.empty()) return 59;
    fields.pop_front();
    /* SSA, ignored (equal to Bs/Be) */
    if (fields.empty()) return 60;
    fields.pop_front();
    /* BC Mass, ignored (efficiency calculation) */
    if (fields.empty()) return 61;
    fields.pop_front();

    if (fields.empty()) return 62;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root U(field.parse_real(&ok));
    if (!ok) return 63;
    if (!FP::defined(U.read().toReal())) return 64;
    remap("U", U);

    if (fields.empty()) return 65;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T1(field.parse_real(&ok));
    if (!ok) return 66;
    if (!FP::defined(T1.read().toReal())) return 67;
    remap("T1", T1);

    /* Dewpoint, redundant */
    if (fields.empty()) return 68;
    fields.pop_front();
    /* Unused analog inputs */
    if (fields.size() < 9) return 69;
    fields.erase(fields.begin(), fields.begin() + 9);

    if (fields.empty()) return 70;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root V5(field.parse_real(&ok));
    if (!ok) return 71;
    if (!FP::defined(V5.read().toReal())) return 72;
    remap("V5", V5);

    if (fields.empty()) return 73;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root V1(field.parse_real(&ok));
    if (!ok) return 74;
    if (!FP::defined(V1.read().toReal())) return 75;
    remap("V1", V1);

    /* Unused input */
    if (fields.empty()) return 76;
    fields.pop_front();

    if (fields.empty()) return 77;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root V2(field.parse_real(&ok));
    if (!ok) return 78;
    if (!FP::defined(V2.read().toReal())) return 79;
    remap("V2", V2);

    /* RTC battery, unused */
    if (fields.empty()) return 80;
    fields.pop_front();
    /* Unused analog inputs */
    if (fields.size() < 2) return 81;
    fields.erase(fields.begin(), fields.begin() + 2);

    if (fields.empty()) return 82;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root A2(field.parse_real(&ok));
    if (!ok) return 83;
    if (!FP::defined(A2.read().toReal())) return 84;
    remap("A2", A2);

    if (fields.empty()) return 85;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root A1(field.parse_real(&ok));
    if (!ok) return 86;
    if (!FP::defined(A1.read().toReal())) return 87;
    remap("A1", A1);

    if (fields.empty()) return 88;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T2(field.parse_real(&ok));
    if (!ok) return 89;
    if (!FP::defined(T2.read().toReal())) return 90;
    remap("T2", T2);

    if (fields.empty()) return 91;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root V4(field.parse_real(&ok));
    if (!ok) return 92;
    if (!FP::defined(V4.read().toReal())) return 93;
    remap("V4", V4);

    if (fields.empty()) return 94;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root P1(field.parse_real(&ok));
    if (!ok) return 95;
    if (!FP::defined(P1.read().toReal())) return 96;
    remap("P1", P1);

    if (fields.empty()) return 97;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Pu(field.parse_real(&ok));
    if (!ok) return 98;
    if (!FP::defined(Pu.read().toReal())) return 99;
    remap("Pu", Pu);

    if (fields.empty()) return 100;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root P2(field.parse_real(&ok));
    if (!ok) return 101;
    if (!FP::defined(P2.read().toReal())) return 102;
    remap("P2", P2);

    if (fields.empty()) return 103;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root V3(field.parse_real(&ok));
    if (!ok) return 104;
    if (!FP::defined(V3.read().toReal())) return 105;
    remap("V3", V3);

    /* Sample count, unused */
    if (fields.empty()) return 106;
    fields.pop_front();

    if (fields.empty()) return 107;
    field = fields.front().string_trimmed();
    fields.pop_front();
    double v = field.parse_real(&ok);
    if (!ok) return 108;
    if (!FP::defined(v) || v < 0.0 || v >= 4.0) return 109;
    Variant::Root mode(static_cast<qint64>(v));
    remap("MODE", mode);

    if (fields.empty()) return 110;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root RTIME((qint64) field.parse_u64(&ok));
    if (!ok) return 111;
    if (!INTEGER::defined(RTIME.read().toInteger())) return 112;
    remap("RTIME", RTIME);

    /* Unused: Free space, Laser on time, Spare, USB Status, Alarm,
     * Local Date, Local time */
    if (fields.size() < 7) return 113;
    fields.erase(fields.begin(), fields.begin() + 7);

    if (config.first().strictMode) {
        if (!fields.empty())
            return 114;
    }

    if (!FP::defined(frameTime) &&
            INTEGER::defined(iyear) &&
            iyear >= 1900 &&
            iyear <= 2999 &&
            INTEGER::defined(idoy) &&
            idoy >= 0 &&
            idoy <= 355 &&
            INTEGER::defined(isecond) &&
            isecond >= 0 &&
            isecond <= 86400) {
        frameTime =
                Time::fromDateTime(QDateTime(QDate((int) iyear, 1, 1), QTime(0, 0, 0), Qt::UTC));
        frameTime += (double) (idoy * 86400);
        frameTime += (double) (isecond);

        configAdvance(frameTime);
    }
    if (!FP::defined(frameTime))
        return -1;
    if (FP::defined(lastRecordTime) && frameTime < lastRecordTime)
        return -1;
    double startTime = lastRecordTime;
    double endTime = frameTime;
    lastRecordTime = frameTime;
    if (!haveEmittedLogMeta && FP::defined(startTime) && loggingEgress != NULL) {
        haveEmittedLogMeta = true;
        loggingEgress->incomingData(buildLogMeta(startTime));
    }

    if (!haveEmittedRealtimeMeta && FP::defined(frameTime) && realtimeEgress != NULL) {
        haveEmittedRealtimeMeta = true;
        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }

    Variant::Flags flags;

    auto ZBs = Bs;
    auto ZBa = Ba;
    auto ZBag = Bag;
    auto ZBaPhase2 = ZBaPhase;

    switch (mode.read().toInt64()) {
    case 0:     /* Measurement */
        switch (sampleState) {
        case SAMPLE_RUN:
            break;

        case SAMPLE_AC_RUN:
        case SAMPLE_FLUSH:
            realtimeStateUpdated = true;
            sampleState = SAMPLE_RUN;
            break;

        case SAMPLE_ZERO_RUN:
            sampleState = SAMPLE_RUN;
            zeroCompleted(frameTime, Bsz, Baz, ZBazPhase);
            break;

        case SAMPLE_SPANCHECK:
            break;
        }
        break;

    case 1:     /* Zero */
        flags.insert("Zero");
        Bs.write().setEmpty();
        Ba.write().setEmpty();
        Bag.write().setEmpty();
        ZBaPhase.write().setEmpty();

        switch (sampleState) {
        case SAMPLE_RUN:
        case SAMPLE_AC_RUN:
        case SAMPLE_FLUSH:
            sampleState = SAMPLE_ZERO_RUN;
            realtimeStateUpdated = true;
            break;

        case SAMPLE_ZERO_RUN:
            break;

        case SAMPLE_SPANCHECK:
            break;
        }
        break;

    case 2:     /* Flush */
        flags.insert("Blank");
        Bs.write().setEmpty();
        Ba.write().setEmpty();
        Bag.write().setEmpty();
        ZBaPhase.write().setEmpty();

        switch (sampleState) {
        case SAMPLE_RUN:
        case SAMPLE_AC_RUN:
            sampleState = SAMPLE_FLUSH;
            realtimeStateUpdated = true;
            break;

        case SAMPLE_FLUSH:
            break;

        case SAMPLE_ZERO_RUN:
            sampleState = SAMPLE_FLUSH;
            realtimeStateUpdated = true;
            zeroCompleted(frameTime, Bsz, Baz, ZBazPhase);
            break;

        case SAMPLE_SPANCHECK:
            break;
        }
        break;

    case 3:     /* Acoustic Calibration */
        flags.insert("AcousticCalibration");
        Bs.write().setEmpty();
        Ba.write().setEmpty();
        Bag.write().setEmpty();
        ZBaPhase.write().setEmpty();

        switch (sampleState) {
        case SAMPLE_RUN:
        case SAMPLE_FLUSH:
            realtimeStateUpdated = true;
            sampleState = SAMPLE_AC_RUN;
            break;

        case SAMPLE_AC_RUN:
            break;

        case SAMPLE_ZERO_RUN:
            sampleState = SAMPLE_AC_RUN;
            zeroCompleted(frameTime, Bsz, Baz, ZBazPhase);
            break;

        case SAMPLE_SPANCHECK:
            break;
        }
        break;

    default:
        break;
    }

    switch (sampleState) {
    case SAMPLE_RUN:
        break;

    case SAMPLE_FLUSH:
        break;

    case SAMPLE_ZERO_RUN: {
        double v = T1.read().toDouble();
        if (FP::defined(v)) {
            zeroTCount++;
            zeroTSum += v;
        }
        v = P1.read().toDouble();
        if (FP::defined(v)) {
            zeroPCount++;
            zeroPSum += v;
        }
        break;
    }

    case SAMPLE_SPANCHECK:
        break;

    case SAMPLE_AC_RUN:
        break;
    }

    if (mode.read() != lastMode.read()) {
        realtimeStateUpdated = true;

        if (persistentEgress != NULL && FP::defined(lastMode.getStart())) {
            lastMode.setEnd(frameTime);
            persistentEgress->incomingData(lastMode);
        }
        lastMode.setRoot(mode);
        lastMode.setStart(frameTime);
        lastMode.setEnd(FP::undefined());

        persistentValuesUpdated();
    }

    if (realtimeStateUpdated && responseState == RESP_RUN && realtimeEgress != NULL) {
        realtimeStateUpdated = false;

        Variant::Root state;
        switch (sampleState) {
        case SAMPLE_RUN:
            state.write().setString("Run");
            break;
        case SAMPLE_FLUSH:
            state.write().setString("Blank");
            break;
        case SAMPLE_ZERO_RUN:
            state.write().setString("Zero");
            break;
        case SAMPLE_AC_RUN:
            state.write().setString("AcousticCalibration");
            break;
        case SAMPLE_SPANCHECK:
            state.write().setString("Spancheck");
            break;
        }
        realtimeEgress->incomingData(
                SequenceValue({{}, "raw", "ZSTATE"}, std::move(state), frameTime, FP::undefined()));
        realtimeEgress->incomingData(lastMode);
    }

    this->Bsz = Bsz;
    this->Baz = Baz;
    this->ZBazPhase = ZBazPhase;

    spancheckController->updateTotal(NephelometerSpancheckController::Scattering,
                                     Bs.read().toDouble(), 0);
    spancheckController->update(NephelometerSpancheckController::Temperature, T1.read().toDouble());
    spancheckController->update(NephelometerSpancheckController::Pressure, P1.read().toDouble());

    spancheckController->advance(frameTime, realtimeEgress);

    if (!FP::defined(zeroEffectiveTime) && Baz.read().exists())
        zeroEffectiveTime = frameTime;
    handleZeroUpdate(frameTime);

    if (!FP::defined(startTime) || !FP::defined(endTime))
        return 0;

    logValue(startTime, endTime, "F1", Variant::Root(flags));
    logValue(startTime, endTime, "Bs" + wlCode, std::move(Bs));
    logValue(startTime, endTime, "Ba" + wlCode, std::move(Ba));
    logValue(startTime, endTime, "Ba" + wlCode + "g", std::move(Bag));
    logValue(startTime, endTime, "ZBa" + wlCode + "Phase", std::move(ZBaPhase));
    logValue(startTime, endTime, "P1", std::move(P1));
    logValue(startTime, endTime, "P2", std::move(P2));
    logValue(startTime, endTime, "Pu", std::move(Pu));
    logValue(startTime, endTime, "T1", std::move(T1));
    logValue(startTime, endTime, "T2", std::move(T2));
    logValue(startTime, endTime, "U", std::move(U));
    logValue(startTime, endTime, "C", std::move(C));
    logValue(startTime, endTime, "A1", std::move(A1));
    logValue(startTime, endTime, "A2", std::move(A2));
    logValue(startTime, endTime, "VA", std::move(VA));
    logValue(startTime, endTime, "ZLaserPhase", std::move(ZLaserPhase));
    logValue(startTime, endTime, "ZMicPressure", std::move(ZMicPressure));
    logValue(startTime, endTime, "ZQ", std::move(ZQ));
    logValue(startTime, endTime, "ZIMic" + wlCode, std::move(ZIMic));
    logValue(startTime, endTime, "ZIBs" + wlCode, std::move(ZIBs));

    realtimeValue(frameTime, "ZBs" + wlCode, std::move(ZBs));
    realtimeValue(frameTime, "ZBa" + wlCode, std::move(ZBa));
    realtimeValue(frameTime, "ZBa" + wlCode + "g", std::move(ZBag));
    realtimeValue(frameTime, "ZBa" + wlCode + "Phase2", std::move(ZBaPhase2));

    realtimeValue(frameTime, "V1", std::move(V1));
    realtimeValue(frameTime, "V2", std::move(V2));
    realtimeValue(frameTime, "V3", std::move(V3));
    realtimeValue(frameTime, "V4", std::move(V4));
    realtimeValue(frameTime, "V5", std::move(V5));
    realtimeValue(frameTime, "ZRTIME", std::move(RTIME));

    return 0;
}

void AcquireDMTPAX::configAdvance(double frameTime)
{
    Q_ASSERT(!config.isEmpty());
    int oldSize = config.size();
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.isEmpty());

    if (oldSize == config.size())
        return;
    configurationChanged();
}

void AcquireDMTPAX::configurationChanged()
{
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;

    spancheckController->setWavelength(0, config.first().wavelength, QString::fromStdString(
            Wavelength::code(config.first().wavelength)));
}

void AcquireDMTPAX::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (FP::defined(frameTime))
        configAdvance(frameTime);

    switch (responseState) {
    case RESP_AUTOPROBE_WAIT:
    case RESP_AUTOPROBE_INITIALIZE: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            if (++autoprobeValidRecords > 3) {
                qCDebug(log) << "Autoprobe succeeded at" << Logging::time(frameTime);

                responseState = RESP_RUN;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                timeoutAt(frameTime + config.first().reportInterval * 2.0 + 1.0);

                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("AutoprobeWait");
                event(frameTime, QObject::tr("Autoprobe succeeded."), false, info);

                if (realtimeEgress != NULL && FP::defined(frameTime)) {
                    realtimeEgress->incomingData(
                            SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                          FP::undefined()));
                }
            }
        } else if (code > 0) {
            qCDebug(log) << "Autoprobe failed at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            responseState = RESP_WAIT;
            autoprobeValidRecords = 0;

            if (realtimeEgress != NULL && FP::defined(frameTime)) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        } else {
            autoprobeValidRecords = 0;
            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;
    }

    case RESP_WAIT:
    case RESP_INITIALIZE: {
        if (processRecord(frame, frameTime) == 0) {
            qCDebug(log) << "Comms established at" << Logging::time(frameTime);

            responseState = RESP_RUN;
            ++autoprobeValidRecords;
            generalStatusUpdated();

            timeoutAt(frameTime + config.first().reportInterval * 2.0 + 1.0);

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("Wait");
            event(frameTime, QObject::tr("Communications established."), false, info);

            if (realtimeEgress != NULL && FP::defined(frameTime)) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                      FP::undefined()));
            }
        } else {
            autoprobeValidRecords = 0;
            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;
    }

    case RESP_RUN: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + config.first().reportInterval * 2.0 + 1.0);
            ++autoprobeValidRecords;
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            responseState = RESP_WAIT;
            discardData(frameTime + 0.5, 1);
            generalStatusUpdated();
            autoprobeValidRecords = 0;

            Variant::Write info = Variant::Write::empty();
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            info.hash("ResponseState").setString("Run");
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            if (realtimeEgress != NULL && FP::defined(frameTime)) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;
    }

    default:
        break;
    }
}

void AcquireDMTPAX::incomingInstrumentTimeout(double frameTime)
{
    if (!FP::defined(frameTime))
        return;

    switch (responseState) {
    case RESP_RUN: {
        qCDebug(log) << "Timeout in unpolled mode at" << Logging::time(frameTime);

        responseState = RESP_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        discardData(frameTime + 2.0, 1);
        generalStatusUpdated();
        autoprobeValidRecords = 0;

        Variant::Write info = Variant::Write::empty();
        info.hash("ResponseState").setString("Run");
        event(frameTime, QObject::tr("Timeout waiting for report.  Communications dropped."), true,
              info);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;
    }

    case RESP_AUTOPROBE_WAIT:
        qCDebug(log) << "Timeout waiting for autoprobe records" << Logging::time(frameTime);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        if (controlStream != NULL)
            controlStream->resetControl();
        discardData(frameTime + 2.0, 1);
        responseState = RESP_WAIT;
        autoprobeValidRecords = 0;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;

    case RESP_INITIALIZE:
        discardData(frameTime + 0.5, 1);
        timeoutAt(frameTime + config.first().reportInterval * 3.0 + 1.0);
        responseState = RESP_WAIT;
        break;

    case RESP_AUTOPROBE_INITIALIZE:
        discardData(frameTime + 0.5, 1);
        timeoutAt(frameTime + config.first().reportInterval * 3.0 + 1.0);
        responseState = RESP_AUTOPROBE_WAIT;
        break;

    default:
        discardData(frameTime + 0.5, 1);
        loggingLost(frameTime);
        break;
    }
}

void AcquireDMTPAX::discardDataCompleted(double frameTime)
{
    Q_UNUSED(frameTime);
}

void AcquireDMTPAX::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_UNUSED(frameTime);
    Q_UNUSED(frame);
}


SequenceValue::Transfer AcquireDMTPAX::constructSpancheckVariables()
{
    SequenceValue::Transfer result;
    if (!spancheckDetails.read().exists())
        return result;

    result.emplace_back(spancheckDetails);

    const auto &wlCode = Wavelength::code(config.first().wavelength);

    auto add = NephelometerSpancheckController::results(spancheckDetails.read(),
                                                        NephelometerSpancheckController::TotalPercentError,
                                                        QString::fromStdString(wlCode));
    if (add.read().exists()) {
        result.emplace_back(SequenceName({}, "raw", "PCTc" + wlCode), std::move(add),
                            spancheckDetails.getStart(), spancheckDetails.getEnd());
    }

    return result;
}

void AcquireDMTPAX::updateSpancheckData()
{
    if (persistentEgress) {
        spancheckDetails.setEnd(lastRecordTime);
        persistentEgress->incomingData(constructSpancheckVariables());
    }

    spancheckDetails.setRoot(
            spancheckController->results(NephelometerSpancheckController::FullResults));
    spancheckDetails.setStart(lastRecordTime);
    spancheckDetails.setEnd(FP::undefined());

    persistentValuesUpdated();

    if (realtimeEgress) {
        realtimeEgress->incomingData(constructSpancheckVariables());
    }
}

SequenceValue::Transfer AcquireDMTPAX::getPersistentValues()
{
    PauseLock paused(*this);
    SequenceValue::Transfer result(constructSpancheckVariables());

    if (FP::defined(lastMode.getStart())) {
        result.emplace_back(lastMode);
    }

    return result;
}

AcquireDMTPAX::SpancheckInterface::SpancheckInterface(AcquireDMTPAX *neph) : parent(neph)
{ }

AcquireDMTPAX::SpancheckInterface::~SpancheckInterface()
{ }

void AcquireDMTPAX::SpancheckInterface::setBypass(bool enable)
{
    if (parent->state == NULL)
        return;
    if (enable) {
        parent->state->setBypassFlag("Spancheck");
        qCDebug(parent->log) << "Spancheck bypass flag set";
    } else {
        parent->state->clearBypassFlag("Spancheck");
        qCDebug(parent->log) << "Spancheck bypass flag cleared";
    }
}

void AcquireDMTPAX::SpancheckInterface::switchToFilteredAir()
{
    Q_ASSERT(!parent->config.isEmpty());
    qCDebug(parent->log) << "Spancheck switching to filtered air";
}

void AcquireDMTPAX::SpancheckInterface::switchToGas(CPD3::Algorithms::Rayleigh::Gas gas)
{
    Q_UNUSED(gas)
    qCDebug(parent->log) << "Spancheck switching to span gas";
}

void AcquireDMTPAX::SpancheckInterface::issueZero()
{
    Q_ASSERT(!parent->config.isEmpty());
    qCDebug(parent->log) << "Spancheck issuing zero";
}

void AcquireDMTPAX::SpancheckInterface::issueCommand(const QString &target,
                                                     const CPD3::Data::Variant::Read &command)
{
    if (parent->state == NULL)
        return;
    parent->state->sendCommand(target.toStdString(), command);
}

bool AcquireDMTPAX::SpancheckInterface::start()
{
    Q_ASSERT(!parent->config.isEmpty());

    switch (parent->responseState) {
    case AcquireDMTPAX::RESP_RUN:
        break;
    default:
        return false;
    }
    switch (parent->sampleState) {
    case AcquireDMTPAX::SAMPLE_RUN:
        break;
    default:
        return false;
    }

    qCDebug(parent->log) << "Beginning spancheck";
    parent->sampleState = SAMPLE_SPANCHECK;
    parent->realtimeStateUpdated = true;

    return true;
}

void AcquireDMTPAX::SpancheckInterface::completed()
{
    Q_ASSERT(!parent->config.isEmpty());

    parent->updateSpancheckData();
    if (parent->sampleState == SAMPLE_SPANCHECK)
        parent->sampleState = SAMPLE_RUN;
    parent->realtimeStateUpdated = true;
    qCDebug(parent->log) << "Spancheck completed";
}

void AcquireDMTPAX::SpancheckInterface::aborted()
{
    Q_ASSERT(!parent->config.isEmpty());

    switch (parent->responseState) {
    case AcquireDMTPAX::RESP_RUN:
        break;
    default:
        return;
    }

    parent->sampleState = SAMPLE_RUN;
    parent->realtimeStateUpdated = true;

    qCDebug(parent->log) << "Spancheck aborted";
}


void AcquireDMTPAX::command(const Variant::Read &command)
{
    Q_ASSERT(!config.isEmpty());

    spancheckController->command(command);
}

AcquisitionInterface::AutoprobeStatus AcquireDMTPAX::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

Variant::Root AcquireDMTPAX::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireDMTPAX::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_RUN:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

void AcquireDMTPAX::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_RUN:
        if (autoprobeValidRecords > 3) {
            /* Already running, just notify that we succeeded. */
            qCDebug(log) << "Interactive autoprobe requested with communications established at"
                         << Logging::time(time);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            break;
        }
        /* Fall through */

    default:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        timeoutAt(time + config.first().reportInterval * 4.0 + 5.0);
        discardData(time + 0.5, 1);
        responseState = RESP_AUTOPROBE_WAIT;
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Wait"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireDMTPAX::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    timeoutAt(time + config.first().reportInterval * 4.0 + 5.0);
    discardData(time + 0.5, 1);
    responseState = RESP_AUTOPROBE_WAIT;
    generalStatusUpdated();
}

void AcquireDMTPAX::autoprobePromote(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + config.first().reportInterval * 2.0 + 1.0);

    switch (responseState) {
    case RESP_RUN:
    case RESP_WAIT:
    case RESP_INITIALIZE:
        qCDebug(log) << "Promoted from state" << responseState << "to interactive acquisition at"
                     << Logging::time(time);
        break;

    case RESP_AUTOPROBE_WAIT:
    case RESP_AUTOPROBE_INITIALIZE:
        qCDebug(log) << "Promoted from passive autoprobe to interactive acquisition at"
                     << Logging::time(time);

        responseState = RESP_WAIT;

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Wait"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireDMTPAX::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + config.first().reportInterval * 2.0 + 1.0);

    switch (responseState) {
    case RESP_RUN:
    case RESP_WAIT:
    case RESP_INITIALIZE:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);
        break;

    case RESP_AUTOPROBE_WAIT:
    case RESP_AUTOPROBE_INITIALIZE:
        qCDebug(log) << "Promoted from passive autoprobe to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_WAIT;
        break;
    }
}

static const quint16 serializedStateVersion = 1;

void AcquireDMTPAX::serializeState(QDataStream &stream)
{
    PauseLock paused(*this);

    stream << serializedStateVersion;

    stream << spancheckDetails.getStart();
    stream << spancheckDetails.read();
}

void AcquireDMTPAX::deserializeState(QDataStream &stream)
{
    quint16 version = 0;
    stream >> version;
    if (version != serializedStateVersion)
        return;

    PauseLock paused(*this);

    double start = FP::undefined();
    stream >> start;
    spancheckDetails.setStart(start);
    stream >> spancheckDetails.root();
}

AcquisitionInterface::AutomaticDefaults AcquireDMTPAX::getDefaults()
{
    AutomaticDefaults result;
    result.name = "E$1$2";
    result.setSerialN81(115200);
    result.autoprobeInitialOrder = 20;
    return result;
}


ComponentOptions AcquireDMTPAXComponent::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options);
    return options;
}

ComponentOptions AcquireDMTPAXComponent::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireDMTPAXComponent::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples());
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireDMTPAXComponent::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireDMTPAXComponent::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options, true);
}

QList<ComponentExample> AcquireDMTPAXComponent::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data.")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireDMTPAXComponent::createAcquisitionPassive(const ComponentOptions &options,
                                                                               const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireDMTPAX(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireDMTPAXComponent::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                               const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireDMTPAX(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireDMTPAXComponent::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                 const std::string &loggingContext)
{
    std::unique_ptr<AcquireDMTPAX> i(new AcquireDMTPAX(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<
        AcquisitionInterface> AcquireDMTPAXComponent::createAcquisitionInteractive(const ValueSegment::Transfer &config,
                                                                                   const std::string &loggingContext)
{
    std::unique_ptr<AcquireDMTPAX> i(new AcquireDMTPAX(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
