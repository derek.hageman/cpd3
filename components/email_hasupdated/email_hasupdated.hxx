/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef EMAILHASUPDATED_H
#define EMAILHASUPDATED_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QList>
#include <QStringList>

#include "core/component.hxx"
#include "core/textsubstitution.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/sequencematch.hxx"

class EmailHasUpdated : public CPD3::Data::ExternalSourceProcessor {
    std::string profile;
    std::vector<CPD3::Data::SequenceName::Component> stations;
    double start;
    double end;

    CPD3::Data::Archive::Access reader;

    CPD3::TextSubstitutionStack substitutions;

    std::vector<CPD3::Data::Variant::Root> messages;

    double walkBackwards(double firstPossible,
                         double end, const CPD3::Data::SequenceMatch::Composite &selection,
                         bool requireDefined);

public:
    EmailHasUpdated();

    EmailHasUpdated(const CPD3::ComponentOptions &options,
                    double start,
                    double end,
                    const std::vector<CPD3::Data::SequenceName::Component> &setStations = {});

    virtual ~EmailHasUpdated();

    void incomingData(const CPD3::Util::ByteView &data) override;

    void endData() override;

    void signalTerminate() override;

protected:
    bool prepare() override;

    bool process() override;

    bool begin() override;
};

class EmailHasUpdatedComponent
        : public QObject, virtual public CPD3::Data::ExternalSourceComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalSourceComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.email_hasupdated"
                              FILE
                              "email_hasupdated.json")

public:
    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual int ingressAllowStations();

    virtual bool ingressRequiresTime();

    virtual CPD3::Data::ExternalConverter *createExternalIngress(const CPD3::ComponentOptions &options,
                                                                 double start,
                                                                 double end,
                                                                 const std::vector<CPD3::Data::SequenceName::Component> &stations = {});
};

#endif
