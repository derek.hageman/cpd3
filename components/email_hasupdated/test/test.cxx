/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QString>
#include <QTemporaryFile>

#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "core/number.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/externalsource.hxx"

using namespace CPD3;
using namespace CPD3::Data;

namespace CPD3 {
namespace Data {
namespace Variant {
bool operator==(const Root &a, const Root &b)
{ return a.read() == b.read(); }
}
}
}


class TestComponent : public QObject {
Q_OBJECT

    QTemporaryFile databaseFile;

    ExternalSourceComponent *component;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component = qobject_cast<ExternalSourceComponent *>(
                ComponentLoader::create("email_hasupdated"));
        QVERIFY(component);

        QVERIFY(databaseFile.open());
        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("profile")));
    }

    void basic()
    {
        Variant::Root v;
        v["HasUpdated/aerosol/Input/Variable"].setString("BsG_S11");
        v["HasUpdated/aerosol/Output"].setString("No data seen for ${AGE|||AUTOUNITS}");

        SequenceValue::Transfer toAdd;
        toAdd.push_back(SequenceValue({"sfa", "configuration", "email"}, v, FP::undefined(),
                                      FP::undefined()));

        toAdd.push_back(SequenceValue({"sfa", "raw", "BsG_S11"}, Variant::Root(1.0), 1388534400.0,
                                      1388620800.0));
        toAdd.push_back(SequenceValue({"sfa", "raw", "BsG_S11"}, Variant::Root(2.0), 1388620800.0,
                                      1388707200.0));

        Archive::Access(databaseFile).writeSynchronous(toAdd);

        std::vector<Variant::Root> outputValues;
        {
            ComponentOptions options(component->getOptions());
            ExternalConverter
                    *input = component->createExternalIngress(options, 1388534400, 1388793600);
            QVERIFY(input != NULL);
            input->start();

            StreamSink::Buffer ingress;
            input->setEgress(&ingress);

            QVERIFY(input->wait());
            delete input;

            QVERIFY(ingress.ended());
            for (const auto &dv : ingress.values()) {
                outputValues.emplace_back(dv.root());
            }
        }

        std::vector<Variant::Root> values;
        v.write().setEmpty();
        v["Type"].setString("Break");
        values.emplace_back(v);

        v.write().setEmpty();
        v["Type"].setString("Text");
        v["Text"].setString("No data seen for 2 day(s)");
        values.emplace_back(v);

        QCOMPARE(outputValues, values);

        {
            ComponentOptions options(component->getOptions());
            ExternalConverter
                    *input = component->createExternalIngress(options, 1388534400, 1388624400);
            QVERIFY(input != NULL);
            input->start();

            StreamSink::Buffer ingress;
            input->setEgress(&ingress);

            QVERIFY(input->wait());
            delete input;

            QVERIFY(ingress.ended());
            QVERIFY(ingress.values().empty());
        }
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
