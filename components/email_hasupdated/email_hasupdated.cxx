/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLoggingCategory>

#include "core/component.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/valueoptionparse.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/segment.hxx"
#include "datacore/variant/composite.hxx"

#include "email_hasupdated.hxx"


Q_LOGGING_CATEGORY(log_email_hasupdated, "cpd3.email.hasupdated", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;

EmailHasUpdated::EmailHasUpdated() : profile(SequenceName::impliedProfile()),
                                     stations(),
                                     start(FP::undefined()),
                                     end(FP::undefined())
{ }

EmailHasUpdated::EmailHasUpdated(const ComponentOptions &options,
                                 double start,
                                 double end,
                                 const std::vector<SequenceName::Component> &setStations) : profile(
        SequenceName::impliedProfile()), stations(setStations), start(start), end(end)
{
    if (options.isSet("profile")) {
        profile = qobject_cast<ComponentOptionSingleString *>(options.get("profile"))->get()
                                                                                     .toLower()
                                                                                     .toStdString();
    }
}

EmailHasUpdated::~EmailHasUpdated() = default;

void EmailHasUpdated::incomingData(const CPD3::Util::ByteView &)
{ externalNotify(); }

void EmailHasUpdated::endData()
{
    externalNotify();
}

static double latestInRange(Archive::Access &reader,
                            double start,
                            double end,
                            const SequenceMatch::Composite &selection,
                            bool requireDefined)
{
    StreamSink::Iterator data;
    {
        auto selections = selection.toArchiveSelections();
        for (auto &mod : selections) {
            mod.start = start;
            mod.end = end;
        }
        reader.readStream(selections, &data)->detach();
    }

    double latest = FP::undefined();
    while (data.hasNext()) {
        auto value = data.next();

        if (!selection.matches(value.getUnit()))
            continue;
        if (!FP::defined(value.getStart()))
            continue;
        if (FP::defined(start) && value.getStart() < start)
            continue;
        if (requireDefined && !Variant::Composite::isDefined(value.read()))
            continue;
        latest = value.getStart();
    }

    return latest;
}

double EmailHasUpdated::walkBackwards(double firstPossible,
                                      double end,
                                      const SequenceMatch::Composite &selection,
                                      bool requireDefined)
{
    Q_ASSERT(FP::defined(end));
    double addInterval = 6 * 3600.0;
    for (int i = 0; i < 1000; i++) {
        if (isTerminated())
            return FP::undefined();

        double start = end - addInterval;
        if (FP::defined(firstPossible) && start < firstPossible) {
            return latestInRange(reader, firstPossible, end, selection, requireDefined);
        }

        double check = latestInRange(reader, start, end, selection, requireDefined);
        if (FP::defined(check))
            return check;

        end = start;
        addInterval *= 2.0;
    }
    return FP::undefined();
}

bool EmailHasUpdated::begin()
{
    if (!FP::defined(end))
        return false;

    do {
        Archive::Access::ReadLock lock(reader, true);

        auto allStations = reader.availableStations(stations);
        if (allStations.empty()) {
            qCDebug(log_email_hasupdated) << "No stations available";
            return false;
        }
        stations.clear();
        std::copy(allStations.begin(), allStations.end(), Util::back_emplacer(stations));

        double tnow = Time::time();
        SequenceSegment configSegment;
        {
            auto confList = SequenceSegment::Stream::read(
                    Archive::Selection(tnow - 1.0, tnow + 1.0, stations, {"configuration"},
                                       {"email"}), &reader);
            auto f = Range::findIntersecting(confList, tnow);
            if (f == confList.end())
                break;
            configSegment = *f;
        }

        for (const auto &station : stations) {
            if (isTerminated())
                break;

            SequenceName stationUnit(station, "configuration", "email");
            auto config =
                    configSegment.takeValue(stationUnit).read().hash("HasUpdated").hash(profile);
            if (!config.exists())
                continue;
            config.detachFromRoot();

            SequenceMatch::Composite
                    selection(config["Input"], {QString::fromStdString(station)}, {"raw"}, {});

            double lastSeen = walkBackwards(
                    Variant::Composite::offsetTimeInterval(config["Maximum"], end, false, Time::Day,
                                                           30), end, selection,
                    !config["AcceptAny"].toBool());
            /* No data seen, so assume the station is down for good */
            if (!FP::defined(lastSeen)) {
                qCDebug(log_email_hasupdated) << "No data seen inside maximum range";
                continue;
            }

            double warningThreshold =
                    Variant::Composite::offsetTimeInterval(config["Warning"], end, false,
                                                           Time::Hour, 17);

            qCDebug(log_email_hasupdated) << "Data last seen at" << Logging::time(lastSeen)
                                          << "with warning threshold at"
                                          << Logging::time(warningThreshold);

            if (!FP::defined(warningThreshold) || lastSeen >= warningThreshold)
                continue;

            substitutions.clear();
            substitutions.setString("profile", QString::fromStdString(profile));
            substitutions.setTime("start", start);
            substitutions.setTime("end", end);
            substitutions.setTime("latest", lastSeen);
            substitutions.setDuration("age", end - lastSeen);

            QString output(config["Output"].toQString());
            output = substitutions.apply(output);
            if (output.isEmpty())
                continue;

            if (messages.empty()) {
                Variant::Root addBreak(config["Message"]);
                addBreak["Type"].setString("Break");
                messages.emplace_back(std::move(addBreak));
            }

            Variant::Root addMessage(config["Message"]);
            addMessage["Type"].setString("Text");
            addMessage["Text"].setString(output);
            messages.emplace_back(std::move(addMessage));
        }
    } while (false);

    if (isTerminated())
        return false;

    return true;
}

bool EmailHasUpdated::prepare()
{ return true; }

bool EmailHasUpdated::process()
{
    SequenceValue::Transfer toOutput;
    double tnow = Time::time();
    qCDebug(log_email_hasupdated) << "Generated" << messages.size() << "message(s)";
    int priority = 0;
    for (auto &v : messages) {
        toOutput.emplace_back(
                SequenceIdentity({"message", "message", "message"}, tnow, tnow, priority++),
                std::move(v));
    }

    messages.clear();
    outputData(toOutput);
    return false;
}

void EmailHasUpdated::signalTerminate()
{
    reader.signalTerminate();
    ExternalSourceProcessor::signalTerminate();
}


ComponentOptions EmailHasUpdatedComponent::getOptions()
{
    ComponentOptions options;

    options.add("profile",
                new ComponentOptionSingleString(tr("profile", "name"), tr("Execution profile"),
                                                tr("This is the profile name to execute.  Multiple profiles can be "
                                                   "defined to specify different output types for the same station.  "
                                                   "Consult the station configuration for available profiles."),
                                                tr("aerosol")));

    return options;
}

QList<ComponentExample> EmailHasUpdatedComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Generate output alerts", "default example name"),
                                     tr("This will generate the alerts for the \"aerosol\" profile.")));

    return examples;
}

int EmailHasUpdatedComponent::ingressAllowStations()
{ return INT_MAX; }

bool EmailHasUpdatedComponent::ingressRequiresTime()
{ return true; }

ExternalConverter *EmailHasUpdatedComponent::createExternalIngress(const ComponentOptions &options,
                                                                   double start,
                                                                   double end,
                                                                   const std::vector<
                                                                           SequenceName::Component> &stations)
{ return new EmailHasUpdated(options, start, end, stations); }
