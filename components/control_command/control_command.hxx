/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CONTROLCOMMAND_H
#define CONTROLCOMMAND_H

#include "core/first.hxx"

#include <vector>
#include <mutex>
#include <QtGlobal>
#include <QObject>
#include <QHash>
#include <QLoggingCategory>

#include "core/component.hxx"
#include "core/timeutils.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/processingtapchain.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/variant/composite.hxx"
#include "luascript/engine.hxx"

class ControlCommand : public CPD3::Acquisition::AcquisitionInterface {
    std::string loggingName;
    QLoggingCategory log;

    bool terminated;
    CPD3::Acquisition::AcquisitionState *state;
    CPD3::Acquisition::AcquisitionControlStream *controlStream;

    CPD3::Data::Variant::Root instrumentMeta;

    class RealtimeIngress : public CPD3::Data::StreamSink {
        ControlCommand *control;

    public:
        RealtimeIngress(ControlCommand *ce);

        virtual ~RealtimeIngress();

        void incomingData(const CPD3::Data::SequenceValue::Transfer &values) override;

        void incomingData(CPD3::Data::SequenceValue::Transfer &&values) override;

        void incomingData(const CPD3::Data::SequenceValue &value) override;

        void incomingData(CPD3::Data::SequenceValue &&value) override;

        virtual void endData();
    };

    void processRealtime(const CPD3::Data::SequenceValue::Transfer &values);

    void processRealtime(CPD3::Data::SequenceValue::Transfer &&values);

    void processRealtime(const CPD3::Data::SequenceValue &value);

    void processRealtime(CPD3::Data::SequenceValue &&value);

    friend class RealtimeIngress;

    RealtimeIngress realtimeIngress;

    std::unordered_map<QString, CPD3::Data::Variant::Root> subsititionState;

    std::mutex realtimeMutex;

    struct RealtimeInput {
        CPD3::Data::DynamicSequenceSelection *input;
        std::vector<CPD3::Data::SequenceValue> values;

        void incoming(const CPD3::Data::SequenceValue &value);

        void incoming(CPD3::Data::SequenceValue &&value);

        ~RealtimeInput();
    };

    std::unordered_map<QString, std::unique_ptr<RealtimeInput>> realtimeInputs;
    CPD3::Data::SequenceName::Map<std::vector<RealtimeInput *>> realtimeDispatch;

    const std::vector<RealtimeInput *> &lookupRealtime(const CPD3::Data::SequenceName &name);

    struct Operation {
        std::unordered_map<QString, qint64> bitSet;
        std::unordered_map<QString, qint64> bitClear;
        std::unordered_map<QString, CPD3::Data::Variant::Root> overlay;

        void configure(const CPD3::Data::Variant::Read &config);
    };

    void apply(const Operation &op);

    std::unique_ptr<CPD3::Lua::Engine> scriptEngine;

    class Substitutions : public CPD3::Data::Variant::Composite::SubstitutionStack {
        ControlCommand *parent;
    public:
        CPD3::Data::Variant::Read command;

        Substitutions(ControlCommand *parent);

        virtual ~Substitutions();

    protected:
        virtual QString literalCheck(QStringRef &key) const;

        virtual QString literalSubstitution(const QStringRef &key, const QString &close) const;
    };

    struct Command {
        QString write;
        Operation before;
        Operation after;

        CPD3::Data::Variant::Root commandData;

        void overlay(const CPD3::Data::Variant::Read &config);
    };

    void execute(const Command &command, const CPD3::Data::Variant::Read &incoming);

    class Configuration {
        double start;
        double end;

    public:
        QHash<QString, Command> commands;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        ~Configuration();

        Configuration(const Configuration &other);

        Configuration &operator=(const Configuration &other);

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under, const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    void advanceTime(double executeTime);

public:
    virtual ~ControlCommand();

    ControlCommand(const CPD3::Data::ValueSegment::Transfer &config,
                   const std::string &loggingContext);

    void setState(CPD3::Acquisition::AcquisitionState *state) override;

    void setControlStream(CPD3::Acquisition::AcquisitionControlStream *controlStream) override;

    void setRealtimeEgress(CPD3::Data::StreamSink *egress) override;

    void setLoggingEgress(CPD3::Data::StreamSink *egress) override;

    void incomingData(const CPD3::Util::ByteView &data,
                      double time,
                      AcquisitionInterface::IncomingDataType type = IncomingDataType::Stream) override;

    void incomingControl(const CPD3::Util::ByteView &data,
                         double time,
                         AcquisitionInterface::IncomingDataType type = IncomingDataType::Stream) override;

    void incomingTimeout(double time) override;

    void incomingAdvance(double time) override;

    void signalTerminate() override;

    void incomingCommand(const CPD3::Data::Variant::Read &command) override;

    CPD3::Data::Variant::Root getCommands() override;

    CPD3::Data::StreamSink *getRealtimeIngress() override;

    CPD3::Data::Variant::Root getSourceMetadata() override;

    CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus() override;

    AutomaticDefaults getDefaults() override;

    bool isCompleted() override;
};

class ControlCommandComponent
        : public QObject, virtual public CPD3::Acquisition::AcquisitionComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.control_command"
                              FILE
                              "control_command.json")
public:
    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config = {},
                                                                                const std::string &loggingContext = {}) override;
};

#endif
