/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/timeutils.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class TextControlStream : public AcquisitionControlStream {
public:
    QByteArray control;

    TextControlStream()
    { }

    virtual ~TextControlStream()
    { }

    virtual void writeControl(const Util::ByteView &data)
    {
        control += data.toQByteArray();
    }

    virtual void resetControl()
    { }

    virtual void requestTimeout(double time)
    {
        Q_UNUSED(time);
    }
};

class TestComponent : public QObject {
Q_OBJECT
    AcquisitionComponent *component;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component =
                qobject_cast<AcquisitionComponent *>(ComponentLoader::create("control_command"));
        QVERIFY(component);
    }

    void basic()
    {
        Variant::Root config;

        config["Commands/C1/Output"] = "Com1 ${COMMAND|STR}";
        config["Commands/C1/After/BitSet/S1"].setInt64(0x05);

        config["Commands/C2/Before/BitClear/S1"].setInt64(0x04);
        config["Commands/C2/Output"] = "Com2 ${S1|INTEGER}";
        config["Commands/C2/After/BitSet/S1"].setInt64(0x02);

        config["Commands/C3/Output"] =
                "Com3 ${<return 'SCRIPT ' .. S1:toInteger() .. ((S2 and S2:toString()) or '');>}";
        config["Commands/C3/After/Overlay/S2"] = "STATE2";

        config["Commands/C4/Output"] = "Com4 ${I1|INTEGER}";

        config["Inputs/I1"] = "bnd:rt_instant:a";

        std::unique_ptr<AcquisitionInterface> interface(component->createAcquisitionInteractive(
                {ValueSegment(FP::undefined(), FP::undefined(), config)}));
        QVERIFY(interface.get());

        TextControlStream output;
        interface->setControlStream(&output);

        {
            Variant::Write cmd = Variant::Write::empty();
            cmd["C1"] = "Foobar";
            interface->incomingCommand(cmd);
        }
        QCOMPARE(output.control, QByteArray("Com1 Foobar"));
        output.control.clear();

        {
            Variant::Write cmd = Variant::Write::empty();
            cmd["C2"] = true;
            interface->incomingCommand(cmd);
        }
        QCOMPARE(output.control, QByteArray("Com2 1"));
        output.control.clear();

        {
            Variant::Write cmd = Variant::Write::empty();
            cmd["C2"] = true;
            interface->incomingCommand(cmd);
        }
        QCOMPARE(output.control, QByteArray("Com2 3"));
        output.control.clear();

        {
            Variant::Root v;
            v.write().setInt64(32);
            interface->getRealtimeIngress()
                     ->incomingData(SequenceValue({"bnd", "rt_instant", "a"}, v));
        }

        {
            Variant::Write cmd = Variant::Write::empty();
            cmd["C3"] = true;
            interface->incomingCommand(cmd);
        }
        QCOMPARE(output.control, QByteArray("Com3 SCRIPT 3"));
        output.control.clear();

        {
            Variant::Write cmd = Variant::Write::empty();
            cmd["C3"] = true;
            interface->incomingCommand(cmd);
        }
        QCOMPARE(output.control, QByteArray("Com3 SCRIPT 3STATE2"));
        output.control.clear();

        {
            Variant::Write cmd = Variant::Write::empty();
            cmd["C4"] = true;
            interface->incomingCommand(cmd);
        }
        QCOMPARE(output.control, QByteArray("Com4 32"));
        output.control.clear();

        {
            Variant::Root v;
            v.write().setInt64(33);
            interface->getRealtimeIngress()
                     ->incomingData(SequenceValue({"bnd", "rt_instant", "a"}, v));
        }

        {
            Variant::Write cmd = Variant::Write::empty();
            cmd["C4"] = true;
            interface->incomingCommand(cmd);
        }
        QCOMPARE(output.control, QByteArray("Com4 33"));
        output.control.clear();
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
