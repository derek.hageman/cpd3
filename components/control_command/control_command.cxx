/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>
#include <QTemporaryFile>
#include <QDateTime>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "datacore/stream.hxx"
#include "datacore/segment.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "luascript/engine.hxx"
#include "luascript/libs/variant.hxx"

#include "control_command.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;


static std::string assembleLoggingCategory(const std::string &loggingContext)
{
    std::string result = "cpd3.acquisition.control.command";
    if (!loggingContext.empty()) {
        result += '.';
        result += loggingContext;
    }
    return result;
}

ControlCommand::ControlCommand(const ValueSegment::Transfer &configData,
                               const std::string &loggingContext) : loggingName(
        assembleLoggingCategory(loggingContext)),
                                                                    log(loggingName.data()),
                                                                    terminated(false),
                                                                    state(NULL),
                                                                    controlStream(NULL),
                                                                    instrumentMeta(),
                                                                    realtimeIngress(this)
{
    instrumentMeta["InstrumentCode"].setString("controlcommand");

    config.append(Configuration());
    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
    Range::intersectShift(config, Time::time());

    std::unordered_set<Variant::PathElement::HashIndex> children;
    for (const auto &add : configData) {
        Util::merge(add["Inputs"].toHash().keys(), children);
    }
    children.erase(Variant::PathElement::HashIndex());

    for (const auto &child : children) {
        Q_ASSERT(!realtimeInputs.count(QString::fromStdString(child)));

        RealtimeInput *add = new RealtimeInput;

        add->input = DynamicSequenceSelection::fromConfiguration(configData,
                                                                 QString("Inputs/%1").arg(
                                                                         QString::fromStdString(
                                                                                 child)));

        realtimeInputs.emplace(QString::fromStdString(child), std::unique_ptr<RealtimeInput>(add));
    }
}

ControlCommand::~ControlCommand() = default;

ControlCommand::RealtimeInput::~RealtimeInput()
{
    delete input;
}


ControlCommand::Configuration::Configuration() : start(FP::undefined()),
                                                 end(FP::undefined()),
                                                 commands()
{ }

ControlCommand::Configuration::~Configuration()
{ }

ControlCommand::Configuration::Configuration(const Configuration &other) : start(other.start),
                                                                           end(other.end),
                                                                           commands(other.commands)
{ }

ControlCommand::Configuration &ControlCommand::Configuration::operator=(const Configuration &other)
{
    if (&other == this)
        return *this;

    start = other.start;
    end = other.end;
    commands = other.commands;

    return *this;
}

ControlCommand::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s), end(e), commands(other.commands)
{ }

ControlCommand::Configuration::Configuration(const ValueSegment &other, double s, double e) : start(
        s), end(e), commands()
{
    setFromSegment(other);
}

ControlCommand::Configuration::Configuration(const Configuration &under, const ValueSegment &over,
                                             double s,
                                             double e) : start(s), end(e), commands(under.commands)
{
    setFromSegment(over);
}

void ControlCommand::Operation::configure(const Variant::Read &config)
{
    for (auto add : config["BitSet"].toHash()) {
        if (add.first.empty())
            continue;
        Util::insert_or_assign(bitSet, QString::fromStdString(add.first), add.second.toInteger());
    }

    for (auto add : config["BitClear"].toHash()) {
        if (add.first.empty())
            continue;
        Util::insert_or_assign(bitClear, QString::fromStdString(add.first), add.second.toInteger());
    }

    for (auto add : config["Overlay"].toHash()) {
        if (add.first.empty())
            continue;
        auto key = QString::fromStdString(add.first);
        auto target = overlay.find(key);
        if (target == overlay.end()) {
            overlay.emplace(std::move(key), Variant::Root(add.second));
            continue;
        }

        target->second = Variant::Root::overlay(target->second, Variant::Root(add.second));
    }
}

void ControlCommand::Command::overlay(const Variant::Read &config)
{
    if (config["Output"].exists())
        write = config["Output"].toQString();

    before.configure(config["Before"]);
    after.configure(config["After"]);

    if (config["Information"].exists()) {
        commandData = Variant::Root::overlay(commandData, Variant::Root(config["Information"]));
    }
}

void ControlCommand::Configuration::setFromSegment(const ValueSegment &config)
{

    for (auto c : config["Commands"].toHash()) {
        if (c.first.empty())
            continue;

        commands[QString::fromStdString(c.first)].overlay(c.second);
    }
}


void ControlCommand::setState(AcquisitionState *state)
{
    this->state = state;
}

void ControlCommand::setControlStream(AcquisitionControlStream *controlStream)
{
    this->controlStream = controlStream;
}

void ControlCommand::setRealtimeEgress(StreamSink *egress)
{
    if (egress != NULL)
        egress->endData();
}

void ControlCommand::setLoggingEgress(StreamSink *egress)
{
    if (egress != NULL)
        egress->endData();
}

Variant::Root ControlCommand::getSourceMetadata()
{ return instrumentMeta; }

AcquisitionInterface::GeneralStatus ControlCommand::getGeneralStatus()
{ return AcquisitionInterface::GeneralStatus::Normal; }

void ControlCommand::advanceTime(double executeTime)
{
    Range::intersectShift(config, executeTime);
}

void ControlCommand::incomingData(const Util::ByteView &data,
                                  double time,
                                  AcquisitionInterface::IncomingDataType type)
{
    Q_UNUSED(data);
    Q_UNUSED(type);
    advanceTime(time);
    if (state != NULL)
        state->realtimeAdvanced(time);
}

void ControlCommand::incomingControl(const Util::ByteView &data,
                                     double time,
                                     AcquisitionInterface::IncomingDataType type)
{
    Q_UNUSED(data);
    Q_UNUSED(type);
    advanceTime(time);
    if (state != NULL)
        state->realtimeAdvanced(time);
}

void ControlCommand::incomingTimeout(double time)
{
    advanceTime(time);
    if (state != NULL)
        state->realtimeAdvanced(time);
}

void ControlCommand::incomingAdvance(double time)
{
    advanceTime(time);
    if (state != NULL)
        state->realtimeAdvanced(time);
}


void ControlCommand::apply(const ControlCommand::Operation &op)
{
    for (const auto &bit : op.bitClear) {
        if (!INTEGER::defined(bit.second))
            continue;
        qint64 i = subsititionState[bit.first].read().toInt64();
        if (!INTEGER::defined(i))
            i = 0;
        i &= ~bit.second;
        subsititionState[bit.first].write().setInteger(i);
    }
    for (const auto &bit : op.bitSet) {
        if (!INTEGER::defined(bit.second))
            continue;
        qint64 i = subsititionState[bit.first].read().toInt64();
        if (!INTEGER::defined(i))
            i = 0;
        i |= bit.second;
        subsititionState[bit.first].write().setInteger(i);
    }
    for (const auto &o : op.overlay) {
        auto target = subsititionState.find(o.first);
        if (target == subsititionState.end()) {
            subsititionState.emplace(o.first, o.second);
            continue;
        }
        target->second = Variant::Root::overlay(target->second, o.second);
    }
}

void ControlCommand::execute(const ControlCommand::Command &command,
                             const CPD3::Data::Variant::Read &incoming)
{
    apply(command.before);

    if (controlStream != NULL) {
        Substitutions subs(this);
        subs.command = incoming;

        subs.setTime("time", Time::time());
        subs.setVariant("command", subs.command);

        {
            std::lock_guard<std::mutex> lock(realtimeMutex);
            for (const auto &s : realtimeInputs) {
                subs.setVariant(s.first, SequenceSegment::Stream::overlayValues(s.second->values));
            }
        }

        for (const auto &s : subsititionState) {
            subs.setVariant(s.first, s.second);
        }

        QString wr(subs.apply(command.write));
        controlStream->writeControl(wr.toUtf8());
    }

    apply(command.after);
}

void ControlCommand::incomingCommand(const Variant::Read &command)
{
    if (config.isEmpty())
        return;
    for (auto issue : command.toHash()) {
        if (issue.first.empty())
            continue;
        auto cd = config.first().commands.constFind(QString::fromStdString(issue.first));
        if (cd == config.first().commands.constEnd())
            continue;
        execute(cd.value(), issue.second);
    }
}

Variant::Root ControlCommand::getCommands()
{
    Variant::Root result;

    for (QList<Configuration>::const_iterator cfg = config.constBegin(), endCfg = config.constEnd();
            cfg != endCfg;
            ++cfg) {
        for (QHash<QString, Command>::const_iterator c = cfg->commands.constBegin(),
                endC = cfg->commands.constEnd(); c != endC; ++c) {
            if (!c.value().commandData.read().exists())
                continue;
            result.write().hash(c.key()).set(c.value().commandData);
        }
    }

    return result;
}

StreamSink *ControlCommand::getRealtimeIngress()
{ return &realtimeIngress; }

ControlCommand::RealtimeIngress::RealtimeIngress(ControlCommand *ce) : control(ce)
{ }

ControlCommand::RealtimeIngress::~RealtimeIngress() = default;

void ControlCommand::RealtimeInput::incoming(const CPD3::Data::SequenceValue &value)
{
    if (!input->get(value).count(value.getName()))
        return;
    SequenceSegment::Stream::maintainValues(values, value);
}

void ControlCommand::RealtimeInput::incoming(CPD3::Data::SequenceValue &&value)
{
    if (!input->get(value).count(value.getName()))
        return;
    SequenceSegment::Stream::maintainValues(values, std::move(value));
}

const std::vector<
        ControlCommand::RealtimeInput *> &ControlCommand::lookupRealtime(const CPD3::Data::SequenceName &name)
{
    auto target = realtimeDispatch.find(name);
    if (target != realtimeDispatch.end())
        return target->second;

    std::vector<RealtimeInput *> targets;
    for (const auto &check : realtimeInputs) {
        if (!check.second->input->registerInput(name))
            continue;
        targets.emplace_back(check.second.get());
    }
    return realtimeDispatch.emplace(name, std::move(targets)).first->second;
}

void ControlCommand::processRealtime(const SequenceValue::Transfer &values)
{
    if (values.empty())
        return;
    std::lock_guard<std::mutex> lock(realtimeMutex);
    for (const auto &v : values) {
        for (auto t : lookupRealtime(v.getName())) {
            t->incoming(v);
        }
    }
}

void ControlCommand::processRealtime(SequenceValue::Transfer &&values)
{
    if (values.empty())
        return;
    std::lock_guard<std::mutex> lock(realtimeMutex);
    for (auto &v : values) {
        const auto &targets = lookupRealtime(v.getName());
        if (targets.empty())
            continue;
        auto t = targets.begin();
        for (auto end = targets.end() - 1; t != end; ++t) {
            (*t)->incoming(v);
        }
        (*t)->incoming(std::move(v));
    }
}

void ControlCommand::processRealtime(const SequenceValue &value)
{
    std::lock_guard<std::mutex> lock(realtimeMutex);
    for (auto t : lookupRealtime(value.getName())) {
        t->incoming(value);
    }
}

void ControlCommand::processRealtime(SequenceValue &&value)
{
    std::lock_guard<std::mutex> lock(realtimeMutex);
    const auto &targets = lookupRealtime(value.getName());
    if (targets.empty())
        return;
    auto t = targets.begin();
    for (auto end = targets.end() - 1; t != end; ++t) {
        (*t)->incoming(value);
    }
    (*t)->incoming(std::move(value));
}

void ControlCommand::RealtimeIngress::incomingData(const SequenceValue::Transfer &values)
{ control->processRealtime(values); }

void ControlCommand::RealtimeIngress::incomingData(SequenceValue::Transfer &&values)
{ control->processRealtime(std::move(values)); }

void ControlCommand::RealtimeIngress::incomingData(const SequenceValue &value)
{ control->processRealtime(value); }

void ControlCommand::RealtimeIngress::incomingData(SequenceValue &&value)
{ control->processRealtime(std::move(value)); }

void ControlCommand::RealtimeIngress::endData()
{ }


ControlCommand::Substitutions::Substitutions(ControlCommand *p) : parent(p)
{ }

ControlCommand::Substitutions::~Substitutions()
{ }

QString ControlCommand::Substitutions::literalCheck(QStringRef &key) const
{
    if (key.length() < 1)
        return QString();
    if (key.at(0) == '<') {
        key = QStringRef(key.string(), key.position() + 1, key.length() - 1);
        return QString('>');
    }
    return TextSubstitution::literalCheck(key);
}

QString ControlCommand::Substitutions::literalSubstitution(const QStringRef &key,
                                                           const QString &close) const
{
    Q_UNUSED(close);
    if (key.isEmpty())
        return QString();
    if (!parent->scriptEngine) {
        parent->scriptEngine.reset(new Lua::Engine);

        {
            Lua::Engine::Frame root(*(parent->scriptEngine));
            Lua::Engine::Assign assign(root, root.global(), "print");
            auto p = parent;
            assign.push(std::function<void(Lua::Engine::Entry &)>([p](Lua::Engine::Entry &entry) {
                for (std::size_t i = 0, max = entry.size(); i < max; i++) {
                    qCInfo(p->log) << entry[i].toOutputString();
                }
            }));
        }
    }

    Lua::Engine::Frame root(*(parent->scriptEngine));

    {
        Lua::Engine::Assign assign(root, parent->scriptEngine->global(), "COMMAND");
        assign.pushData<Lua::Libs::Variant>(Variant::Root(command).write());
    }
    {
        std::lock_guard<std::mutex> lock(parent->realtimeMutex);
        for (const auto &s : parent->realtimeInputs) {
            Lua::Engine::Assign assign(root, parent->scriptEngine->global(), s.first);
            assign.pushData<Lua::Libs::Variant>(
                    SequenceSegment::Stream::overlayValues(s.second->values).write());
        }
    }
    for (auto &s : parent->subsititionState) {
        Lua::Engine::Assign assign(root, parent->scriptEngine->global(), s.first);
        assign.pushData<Lua::Libs::Variant>(s.second);
    }

    Lua::Engine::Call call(root);
    if (!call.pushChunk(key.toString().toStdString(), false)) {
        parent->scriptEngine->clearError();
        return QString();
    }
    if (!call.execute(1)) {
        parent->scriptEngine->clearError();
        return QString();
    }

    return call.back().toQString();
}

AcquisitionInterface::AutomaticDefaults ControlCommand::getDefaults()
{
    AutomaticDefaults result;
    result.name = "COMMAND$2";
    return result;
}

bool ControlCommand::isCompleted()
{ return terminated; }

void ControlCommand::signalTerminate()
{
    terminated = true;
    completed();
}


std::unique_ptr<
        AcquisitionInterface> ControlCommandComponent::createAcquisitionPassive(const ComponentOptions &,
                                                                                const std::string &loggingContext)
{
    qWarning() << "Option based creation is not supported";
    return std::unique_ptr<AcquisitionInterface>(
            new ControlCommand(ValueSegment::Transfer(), loggingContext));
}

std::unique_ptr<
        AcquisitionInterface> ControlCommandComponent::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext)
{
    return std::unique_ptr<AcquisitionInterface>(new ControlCommand(config, loggingContext));
}

std::unique_ptr<
        AcquisitionInterface> ControlCommandComponent::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext)
{ return {}; }
