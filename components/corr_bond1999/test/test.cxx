/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>

#include "datacore/segmentprocessingstage.hxx"
#include "core/component.hxx"

#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "smoothing/baseline.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Smoothing;

namespace QTest {

template<>
char *toString(const QSet<double> &set)
{
    QByteArray ba = "QSet<double>(";
    for (QSet<double>::const_iterator i = set.constBegin(); i != set.constEnd(); ++i) {
        if (i != set.constBegin()) ba += ", ";
        ba += QByteArray::number(*i);
    }
    ba += ")";
    return qstrdup(ba.data());
}
}

class TestController : public SegmentProcessingStage::SequenceHandlerControl {
public:
    class ID {
    public:
        ID() : filter(), input()
        { }

        ID(const QSet<SequenceName> &setFilter, const QSet<SequenceName> &setInput) : filter(
                setFilter), input(setInput)
        { }

        bool operator==(const ID &other) const
        { return filter == other.filter && input == other.input; }

        QSet<SequenceName> filter;
        QSet<SequenceName> input;
    };

    QHash<int, ID> contents;

    virtual void filterUnit(const SequenceName &unit, int targetID)
    {
        contents[targetID].input.remove(unit);
        contents[targetID].filter.insert(unit);
    }

    virtual void inputUnit(const SequenceName &unit, int targetID)
    {
        if (contents[targetID].filter.contains(unit))
            return;
        contents[targetID].input.insert(unit);
    }

    virtual bool isHandled(const SequenceName &unit)
    {
        for (QHash<int, ID>::const_iterator it = contents.constBegin();
                it != contents.constEnd();
                ++it) {
            if (it.value().filter.contains(unit))
                return true;
            if (it.value().input.contains(unit))
                return true;
        }
        return false;
    }
};

class TestComponent : public QObject {
Q_OBJECT

    SegmentProcessingStageComponent *component;

private slots:

    void initTestCase()
    {
        component = qobject_cast<SegmentProcessingStageComponent *>(
                ComponentLoader::create("corr_bond1999"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("k1")));
        QVERIFY(qobject_cast<DynamicInputOption *>(options.get("k2")));
        QVERIFY(qobject_cast<DynamicBoolOption *>(options.get("coarse")));
        QVERIFY(qobject_cast<BaselineSmootherOption *>(options.get("smoothing")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("absorption")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("scattering")));
        QVERIFY(qobject_cast<DynamicSequenceSelectionOption *>(options.get("extinction")));
        QVERIFY(qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")));
        QVERIFY(qobject_cast<ComponentOptionInstrumentSuffixSet *>(
                options.get("scattering-suffix")));
        QVERIFY(qobject_cast<ComponentOptionInstrumentSuffixSet *>(
                options.get("extinction-suffix")));

        QVERIFY(options.excluded().value("absorption").contains("suffix"));
        QVERIFY(options.excluded().value("suffix").contains("absorption"));
        QVERIFY(options.excluded().value("scattering").contains("scattering-suffix"));
        QVERIFY(options.excluded().value("scattering-suffix").contains("scattering"));
        QVERIFY(options.excluded().value("extinction").contains("extinction-suffix"));
        QVERIFY(options.excluded().value("extinction-suffix").contains("extinction"));
    }

    void dynamicDefaults()
    {
        ComponentOptions options(component->getOptions());
        qobject_cast<BaselineSmootherOption *>(options.get("smoothing"))->overlay(
                new BaselineLatest, FP::undefined(), FP::undefined());
        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        SegmentProcessingStage *filter2;
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "Vx_S11q"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BsG_S12"), &controller);
        QVERIFY(controller.contents.isEmpty());
        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                filter2 = component->deserializeBasicFilter(stream);
                QVERIFY(stream.atEnd());
            }
            QVERIFY(filter2 != NULL);
            filter2->unhandled(SequenceName("brw", "raw", "Vx_S11q"), &controller);
            QVERIFY(controller.contents.isEmpty());
            delete filter2;
        }

        SequenceName BsB_S11_1("brw", "raw", "BsB_S11");
        SequenceName BsG_S11_1("brw", "raw", "BsG_S11");
        SequenceName BsR_S11_1("brw", "raw", "BsR_S11");
        SequenceName BaB_A11_1("brw", "raw", "BaB_A11");
        SequenceName BaG_A11_1("brw", "raw", "BaG_A11");
        SequenceName BaR_A11_1("brw", "raw", "BaR_A11");
        SequenceName F1_A11_1("brw", "raw", "F1_A11");
        SequenceName BaB_A12_1("brw", "raw", "BaB_A12");
        SequenceName BaG_A12_1("brw", "raw", "BaG_A12");
        SequenceName BaR_A12_1("brw", "raw", "BaR_A12");
        SequenceName F1_A12_1("brw", "raw", "F1_A12");

        SequenceName BsB_S11_2("sgp", "raw", "BsB_S11");
        SequenceName BsG_S11_2("sgp", "raw", "BsG_S11");
        SequenceName BsR_S11_2("sgp", "raw", "BsR_S11");
        SequenceName BaB_A11_2("sgp", "raw", "BaB_A11");
        SequenceName BaG_A11_2("sgp", "raw", "BaG_A11");
        SequenceName BaR_A11_2("sgp", "raw", "BaR_A11");
        SequenceName F1_A11_2("sgp", "raw", "F1_A11");


        filter->unhandled(BaB_A11_1, &controller);
        filter->unhandled(BaG_A11_1, &controller);
        filter->unhandled(BaR_A11_1, &controller);
        filter->unhandled(BsB_S11_1, &controller);
        filter->unhandled(BsG_S11_1, &controller);
        filter->unhandled(BsR_S11_1, &controller);
        filter->unhandled(F1_A11_1, &controller);
        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents.keys(TestController::ID(QSet<SequenceName>() <<
                                                                               BaB_A11_1 <<
                                                                               BaG_A11_1 <<
                                                                               BaR_A11_1 << F1_A11_1,
                                                          QSet<SequenceName>() <<
                                                                               BsB_S11_1 <<
                                                                               BsG_S11_1 <<
                                                                               BsR_S11_1));
        QCOMPARE(idL.size(), 1);
        int A11_1 = idL.at(0);

        filter->unhandled(BaB_A12_1, &controller);
        filter->unhandled(BaG_A12_1, &controller);
        filter->unhandled(BaR_A12_1, &controller);
        filter->unhandled(F1_A12_1, &controller);
        QCOMPARE(controller.contents.size(), 2);
        idL = controller.contents.keys(TestController::ID(QSet<SequenceName>() <<
                                                                               BaB_A12_1 <<
                                                                               BaG_A12_1 <<
                                                                               BaR_A12_1 << F1_A12_1,
                                                          QSet<SequenceName>() <<
                                                                               BsB_S11_1 <<
                                                                               BsG_S11_1 <<
                                                                               BsR_S11_1));
        QCOMPARE(idL.size(), 1);
        int A12_1 = idL.at(0);

        filter->unhandled(F1_A11_2, &controller);
        filter->unhandled(BsB_S11_2, &controller);
        filter->unhandled(BaB_A11_2, &controller);
        filter->unhandled(BaG_A11_2, &controller);
        filter->unhandled(BaR_A11_2, &controller);
        filter->unhandled(BsG_S11_2, &controller);
        filter->unhandled(BsR_S11_2, &controller);
        QCOMPARE(controller.contents.size(), 3);
        idL = controller.contents.keys(TestController::ID(QSet<SequenceName>() <<
                                                                               BaB_A11_2 <<
                                                                               BaG_A11_2 <<
                                                                               BaR_A11_2 << F1_A11_2,
                                                          QSet<SequenceName>() <<
                                                                               BsB_S11_2 <<
                                                                               BsG_S11_2 <<
                                                                               BsR_S11_2));
        QCOMPARE(idL.size(), 1);
        int A11_2 = idL.at(0);

        data.setStart(15.0);
        data.setEnd(16.0);

        {
            Variant::Root meta;
            meta.write().metadataReal("Wavelength").setDouble(450.0);
            data.setValue(BsB_S11_1.toMeta(), meta);
            meta.write().metadataReal("Wavelength").setDouble(430.0);
            data.setValue(BsB_S11_2.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(467.0);
            data.setValue(BaB_A11_1.toMeta(), meta);
            data.setValue(BaB_A12_1.toMeta(), meta);
            meta.write().metadataReal("Wavelength").setDouble(460.0);
            data.setValue(BaB_A11_2.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(550.0);
            data.setValue(BsG_S11_1.toMeta(), meta);
            data.setValue(BsG_S11_2.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(528.0);
            data.setValue(BaG_A11_1.toMeta(), meta);
            data.setValue(BaG_A12_1.toMeta(), meta);
            data.setValue(BaG_A11_2.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(700.0);
            data.setValue(BsR_S11_1.toMeta(), meta);
            data.setValue(BsR_S11_2.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(660.0);
            data.setValue(BaR_A11_1.toMeta(), meta);
            data.setValue(BaR_A12_1.toMeta(), meta);
            data.setValue(BaR_A11_2.toMeta(), meta);

            data.setValue(F1_A11_1.toMeta(), Variant::Root(Variant::Type::MetadataFlags));
            data.setValue(F1_A11_2.toMeta(), Variant::Root(Variant::Type::MetadataFlags));
            data.setValue(F1_A12_1.toMeta(), Variant::Root(Variant::Type::MetadataFlags));

            filter->processMeta(A11_1, data);
            QVERIFY(data.value(F1_A11_1.toMeta()).metadataSingleFlag("Bond1999").exists());
            QCOMPARE(data.value(BaB_A11_1.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);

            filter->processMeta(A11_2, data);
            QVERIFY(data.value(F1_A11_2.toMeta()).metadataSingleFlag("Bond1999").exists());
            QCOMPARE(data.value(BaB_A11_2.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);

            filter->processMeta(A12_1, data);
            QVERIFY(data.value(F1_A12_1.toMeta()).metadataSingleFlag("Bond1999").exists());
            QCOMPARE(data.value(BaB_A12_1.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
        }

        data.setValue(F1_A11_1, Variant::Root(Variant::Type::Flags));
        data.setValue(BsB_S11_1, Variant::Root(10.0));
        data.setValue(BsG_S11_1, Variant::Root(8.0));
        data.setValue(BsR_S11_1, Variant::Root(6.0));
        data.setValue(BaB_A11_1, Variant::Root(20.0));
        data.setValue(BaG_A11_1, Variant::Root(18.0));
        data.setValue(BaR_A11_1, Variant::Root(14.0));
        filter->process(A11_1, data);
        QCOMPARE(data.value(BaB_A11_1).toDouble(), 15.7443271855651);
        QCOMPARE(data.value(BaG_A11_1).toDouble(), 14.1742374222753);
        QCOMPARE(data.value(BaR_A11_1).toDouble(), 11.0256348033907);
        QVERIFY(data.value(F1_A11_1).testFlag("Bond1999"));

        {
            QByteArray sd;
            {
                QDataStream stream(&sd, QIODevice::WriteOnly);
                filter->serialize(stream);
            }
            {
                QDataStream stream(&sd, QIODevice::ReadOnly);
                filter2 = component->deserializeBasicFilter(stream);
            }
            QVERIFY(filter2 != NULL);
            data.setValue(F1_A11_1, Variant::Root(Variant::Type::Flags));
            data.setValue(BsB_S11_1, Variant::Root(10.0));
            data.setValue(BsG_S11_1, Variant::Root(8.0));
            data.setValue(BsR_S11_1, Variant::Root(6.0));
            data.setValue(BaB_A11_1, Variant::Root(20.0));
            data.setValue(BaG_A11_1, Variant::Root(18.0));
            data.setValue(BaR_A11_1, Variant::Root(14.0));
            filter2->process(A11_1, data);
            QCOMPARE(data.value(BaB_A11_1).toDouble(), 15.7443271855651);
            QCOMPARE(data.value(BaG_A11_1).toDouble(), 14.1742374222753);
            QCOMPARE(data.value(BaR_A11_1).toDouble(), 11.0256348033907);
            QVERIFY(data.value(F1_A11_1).testFlag("Bond1999"));
            delete filter2;
        }


        data.setValue(F1_A11_1, Variant::Root(Variant::Type::Flags));
        data.setValue(BsB_S11_1, Variant::Root(10.0));
        data.setValue(BsG_S11_1, Variant::Root(8.0));
        data.setValue(BsR_S11_1, Variant::Root(6.0));
        data.setValue(BaB_A11_1, Variant::Root());
        data.setValue(BaG_A11_1, Variant::Root(18.0));
        data.setValue(BaR_A11_1, Variant::Root(14.0));
        filter->process(A11_1, data);
        QVERIFY(!data.value(BaB_A11_1).exists());
        QCOMPARE(data.value(BaG_A11_1).toDouble(), 14.1742374222753);
        QCOMPARE(data.value(BaR_A11_1).toDouble(), 11.0256348033907);
        QVERIFY(data.value(F1_A11_1).testFlag("Bond1999"));

        data.setValue(F1_A11_1, Variant::Root(Variant::Type::Flags));
        data.setValue(BsB_S11_1, Variant::Root());
        data.setValue(BsG_S11_1, Variant::Root());
        data.setValue(BsR_S11_1, Variant::Root());
        data.setValue(BaB_A11_1, Variant::Root(20.0));
        data.setValue(BaG_A11_1, Variant::Root(18.0));
        data.setValue(BaR_A11_1, Variant::Root(14.0));
        filter->process(A11_1, data);
        QVERIFY(!FP::defined(data.value(BaB_A11_1).toReal()));
        QVERIFY(!FP::defined(data.value(BaG_A11_1).toReal()));
        QVERIFY(!FP::defined(data.value(BaR_A11_1).toReal()));
        QVERIFY(data.value(F1_A11_1).testFlag("Bond1999"));

        data.setValue(F1_A11_2, Variant::Root(Variant::Type::Flags));
        data.setValue(BsB_S11_2, Variant::Root(12.0));
        data.setValue(BsG_S11_2, Variant::Root(10.0));
        data.setValue(BsR_S11_2, Variant::Root(8.0));
        data.setValue(BaB_A11_2, Variant::Root(40.0));
        data.setValue(BaG_A11_2, Variant::Root(30.0));
        data.setValue(BaR_A11_2, Variant::Root(20.0));
        filter->process(A11_2, data);
        QCOMPARE(data.value(BaB_A11_2).toDouble(), 31.6161434884227);
        QCOMPARE(data.value(BaG_A11_2).toDouble(), 23.6834917258577);
        QCOMPARE(data.value(BaR_A11_2).toDouble(), 15.7631536307957);
        QVERIFY(data.value(F1_A11_2).testFlag("Bond1999"));

        data.setValue(F1_A12_1, Variant::Root(Variant::Type::Flags));
        data.setValue(BsB_S11_1, Variant::Root(12.0));
        data.setValue(BsG_S11_1, Variant::Root(11.0));
        data.setValue(BsR_S11_1, Variant::Root(10.0));
        data.setValue(BaB_A12_1, Variant::Root(25.0));
        data.setValue(BaG_A12_1, Variant::Root(20.0));
        data.setValue(BaR_A12_1, Variant::Root(15.0));
        filter->process(A12_1, data);
        QCOMPARE(data.value(BaB_A12_1).toDouble(), 19.6834656024485);
        QCOMPARE(data.value(BaG_A12_1).toDouble(), 15.7180911597267);
        QCOMPARE(data.value(BaR_A12_1).toDouble(), 11.7584382007717);
        QVERIFY(data.value(F1_A12_1).testFlag("Bond1999"));

        delete filter;
    }

    void dynamicOptions()
    {
        ComponentOptions options(component->getOptions());

        qobject_cast<BaselineSmootherOption *>(options.get("smoothing"))->overlay(
                new BaselineLatest, FP::undefined(), FP::undefined());
        qobject_cast<DynamicInputOption *>(options.get("k1"))->set(0.05);
        qobject_cast<DynamicInputOption *>(options.get("k2"))->set(1.5);
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("absorption"))->set("", "",
                                                                                       "A.*");
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("scattering"))->set("", "",
                                                                                       "S.*");
        qobject_cast<DynamicSequenceSelectionOption *>(options.get("extinction"))->set("", "",
                                                                                       "E.*");

        SegmentProcessingStage *filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        TestController controller;
        QList<int> idL;
        SequenceSegment data;

        filter->unhandled(SequenceName("brw", "raw", "BaG_A11"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BsG_S11"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BeG_E11"), &controller);
        QVERIFY(controller.contents.isEmpty());

        SequenceName A_A11("brw", "raw", "A_A11");
        SequenceName S1_S11("brw", "raw", "S1_S11");
        SequenceName S2_S11("brw", "raw", "S2_S11");
        SequenceName E1_E11("brw", "raw", "E1_E11");
        SequenceName E2_E11("brw", "raw", "E2_E11");
        filter->unhandled(A_A11, &controller);
        filter->unhandled(S1_S11, &controller);
        filter->unhandled(S2_S11, &controller);
        filter->unhandled(E1_E11, &controller);
        filter->unhandled(E2_E11, &controller);

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << A_A11,
                                                 QSet<SequenceName>() <<
                                                         S1_S11 <<
                                                         S2_S11 <<
                                                         E1_E11 <<
                                                         E2_E11));
        QCOMPARE(idL.size(), 1);
        int id = idL.at(0);

        {
            Variant::Root meta;
            meta.write().metadataReal("Wavelength").setDouble(500.0);
            data.setValue(A_A11.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(400.0);
            data.setValue(S1_S11.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(600.0);
            data.setValue(S2_S11.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(350.0);
            data.setValue(E1_E11.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(550.0);
            data.setValue(E2_E11.toMeta(), meta);

            filter->processMeta(id, data);
            QCOMPARE(data.value(A_A11.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
        }

        data.setValue(A_A11, Variant::Root(10.0));
        data.setValue(S1_S11, Variant::Root(15.0));
        data.setValue(S2_S11, Variant::Root(5.0));
        data.setValue(E1_E11, Variant::Root(30.0));
        data.setValue(E2_E11, Variant::Root(20.0));
        filter->process(id, data);
        QCOMPARE(data.value(A_A11).toDouble(), 6.19352294038512);

        data.setValue(A_A11, Variant::Root(10.0));
        data.setValue(S1_S11, Variant::Root());
        data.setValue(S2_S11, Variant::Root());
        data.setValue(E1_E11, Variant::Root(30.0));
        data.setValue(E2_E11, Variant::Root(20.0));
        filter->process(id, data);
        QCOMPARE(data.value(A_A11).toDouble(), 5.93843992976324);

        delete filter;

        data = SequenceSegment();
        options = component->getOptions();
        qobject_cast<BaselineSmootherOption *>(options.get("smoothing"))->overlay(
                new BaselineLatest, FP::undefined(), FP::undefined());
        qobject_cast<DynamicBoolOption *>(options.get("coarse"))->set(true);
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->add("A11");
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("scattering-suffix"))->add(
                "S11");
        qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("extinction-suffix"))->add(
                "E11");
        filter = component->createBasicFilterDynamic(options);
        QVERIFY(filter != NULL);
        controller.contents.clear();

        filter->unhandled(SequenceName("brw", "raw", "BaG_A12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BsG_S12"), &controller);
        filter->unhandled(SequenceName("brw", "raw", "BeG_E12"), &controller);
        QVERIFY(controller.contents.isEmpty());

        SequenceName BaG_A11("brw", "raw", "BaG_A11");
        SequenceName F1_A11("brw", "raw", "F1_A11");
        SequenceName Bs1_S11("brw", "raw", "Bs1_S11");
        SequenceName Bs2_S11("brw", "raw", "Bs2_S11");
        SequenceName Be1_E11("brw", "raw", "Be1_E11");
        SequenceName Be2_E11("brw", "raw", "Be2_E11");
        filter->unhandled(BaG_A11, &controller);
        filter->unhandled(Bs1_S11, &controller);
        filter->unhandled(Bs2_S11, &controller);
        filter->unhandled(Be1_E11, &controller);
        filter->unhandled(Be2_E11, &controller);

        QCOMPARE(controller.contents.size(), 1);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << BaG_A11 << F1_A11,
                                                 QSet<SequenceName>() <<
                                                         Bs1_S11 <<
                                                         Bs2_S11 <<
                                                         Be1_E11 <<
                                                         Be2_E11));
        QCOMPARE(idL.size(), 1);
        id = idL.at(0);

        {
            Variant::Root meta;
            meta.write().metadataReal("Wavelength").setDouble(500.0);
            data.setValue(BaG_A11.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(400.0);
            data.setValue(Bs1_S11.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(600.0);
            data.setValue(Bs2_S11.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(350.0);
            data.setValue(Be1_E11.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(550.0);
            data.setValue(Be2_E11.toMeta(), meta);

            data.setValue(F1_A11.toMeta(), Variant::Root(Variant::Type::MetadataFlags));

            filter->processMeta(id, data);
            QCOMPARE(data.value(BaG_A11.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
            QVERIFY(data.value(F1_A11.toMeta()).metadataSingleFlag("Bond1999").exists());
        }

        data.setValue(F1_A11, Variant::Root(Variant::Type::Flags));
        data.setValue(BaG_A11, Variant::Root(10.0));
        data.setValue(Bs1_S11, Variant::Root(8.0));
        data.setValue(Bs2_S11, Variant::Root(7.0));
        data.setValue(Be1_E11, Variant::Root(30.0));
        data.setValue(Be2_E11, Variant::Root(20.0));
        filter->process(id, data);
        QCOMPARE(data.value(BaG_A11).toDouble(), 7.88380170615968);
        QVERIFY(data.value(F1_A11).testFlag("Bond1999"));

        data.setValue(F1_A11, Variant::Root(Variant::Type::Flags));
        data.setValue(BaG_A11, Variant::Root(10.0));
        data.setValue(Bs1_S11, Variant::Root());
        data.setValue(Bs2_S11, Variant::Root());
        data.setValue(Be1_E11, Variant::Root(30.0));
        data.setValue(Be2_E11, Variant::Root(29.0));
        filter->process(id, data);
        QCOMPARE(data.value(BaG_A11).toDouble(), 7.83378677308453);
        QVERIFY(data.value(F1_A11).testFlag("Bond1999"));

        delete filter;
    }

    void predefined()
    {
        ComponentOptions options(component->getOptions());

        SequenceName F1_A11("brw", "raw", "F1_A11");
        SequenceName BaG_A11("brw", "raw", "BaG_A11");
        SequenceName BsG_S11("brw", "raw", "BsG_S11");
        QList<SequenceName> input;
        input << BaG_A11 << BsG_S11;

        SegmentProcessingStage *filter =
                component->createBasicFilterPredefined(options, FP::undefined(), FP::undefined(),
                                                       input);
        QVERIFY(filter != NULL);
        TestController controller;

        QCOMPARE(filter->requestedInputs(), (SequenceName::Set{F1_A11, BaG_A11, BsG_S11}));

        delete filter;
    }

    void editing()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["A11/CorrectAbsorption"] = "::Ba[BGR]_A11:=";
        cv["A11/Flags"] = "::F1_A11:=";
        cv["A11/K1"] = 0.02;
        cv["A11/K2"] = 1.22;
        cv["A11/Scattering/Input"] = "::Bs[BGR]_S11:=";
        cv["A11/Scattering/Smoothing/Type"] = "Latest";
        cv["A12/CorrectAbsorption"] = "::BaB_A12:=";
        cv["A12/Scattering/Input"] = "::BsB_S11:=";
        cv["A12/Scattering/Smoothing/Type"] = "Latest";
        cv["A12/Extinction/Input"] = "::BeB_E11:=";
        cv["A12/Extinction/Smoothing/Type"] = "Latest";
        config.emplace_back(FP::undefined(), 13.0, cv);
        cv.write().setEmpty();
        cv["A11/CorrectAbsorption"] = "::BaB_A11:=";
        cv["A11/K1"] = 0.05;
        cv["A11/K2"] = 1.5;
        cv["A11/Coarse"] = true;
        cv["A11/Scattering/Input"] = "::Bs[BGR]_S11:=";
        cv["A11/Scattering/Smoothing/Type"] = "Latest";
        cv["A11/Extinction/Input"] = "::Be[BGR]_E11:=";
        cv["A11/Extinction/Smoothing/Type"] = "Latest";
        config.emplace_back(13.0, FP::undefined(), cv);

        SegmentProcessingStage
                *filter = component->createBasicFilterEditing(12.0, 15.0, "brw", "raw", config);
        QVERIFY(filter != NULL);
        TestController controller;

        SequenceName F1_A11("brw", "raw", "F1_A11");
        SequenceName BaB_A11("brw", "raw", "BaB_A11");
        SequenceName BaR_A11("brw", "raw", "BaR_A11");
        SequenceName BsB_S11("brw", "raw", "BsB_S11");
        SequenceName BsR_S11("brw", "raw", "BsR_S11");
        SequenceName BeB_E11("brw", "raw", "BeB_E11");
        SequenceName BeR_E11("brw", "raw", "BeR_E11");
        SequenceName BaB_A12("brw", "raw", "BaB_A12");

        QCOMPARE(filter->requestedInputs(),
                 (SequenceName::Set{F1_A11, BaB_A11, BaB_A12, BsB_S11, BeB_E11}));

        filter->unhandled(F1_A11, &controller);
        filter->unhandled(BaB_A11, &controller);
        filter->unhandled(BaR_A11, &controller);
        filter->unhandled(BsB_S11, &controller);
        filter->unhandled(BsR_S11, &controller);
        filter->unhandled(BeB_E11, &controller);
        filter->unhandled(BeR_E11, &controller);
        filter->unhandled(BaB_A12, &controller);

        QList<int> idL = controller.contents
                                   .keys(TestController::ID(
                                           QSet<SequenceName>() << F1_A11 << BaB_A11 << BaR_A11,
                                           QSet<SequenceName>() <<
                                                   BsB_S11 <<
                                                   BsR_S11 <<
                                                   BeB_E11 <<
                                                   BeR_E11));
        QCOMPARE(idL.size(), 1);
        int A11 = idL.at(0);
        idL = controller.contents
                        .keys(TestController::ID(QSet<SequenceName>() << BaB_A12,
                                                 QSet<SequenceName>() << BsB_S11 << BeB_E11));
        QCOMPARE(idL.size(), 1);
        int A12 = idL.at(0);

        QCOMPARE(filter->metadataBreaks(A11), QSet<double>() << 13.0);
        QCOMPARE(filter->metadataBreaks(A12), QSet<double>() << 13.0);

        SequenceSegment data;
        {
            Variant::Root meta;
            meta.write().metadataReal("Wavelength").setDouble(400.0);
            data.setValue(BaB_A11.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(650.0);
            data.setValue(BaR_A11.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(450.0);
            data.setValue(BsB_S11.toMeta(), meta);
            data.setValue(BeB_E11.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(700.0);
            data.setValue(BsR_S11.toMeta(), meta);
            data.setValue(BeR_E11.toMeta(), meta);

            meta.write().metadataReal("Wavelength").setDouble(450.0);
            data.setValue(BaB_A12.toMeta(), meta);

            data.setValue(F1_A11.toMeta(), Variant::Root(Variant::Type::MetadataFlags));

            filter->processMeta(A11, data);
            QCOMPARE(data.value(BaB_A11.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
            QVERIFY(data.value(F1_A11.toMeta()).metadataSingleFlag("Bond1999").exists());

            filter->processMeta(A12, data);
            QCOMPARE(data.value(BaB_A12.toMeta()).metadata("Processing").getType(),
                     Variant::Type::Array);
        }

        data.setStart(12.0);
        data.setEnd(13.0);

        data.setValue(F1_A11, Variant::Root(Variant::Type::Flags));
        data.setValue(BaB_A11, Variant::Root(6.0));
        data.setValue(BsB_S11, Variant::Root(10.0));
        data.setValue(BsR_S11, Variant::Root(9.0));
        filter->process(A11, data);
        QCOMPARE(data.value(BaB_A11).toDouble(), 4.60188770756924);
        QVERIFY(data.value(F1_A11).testFlag("Bond1999"));

        data.setValue(F1_A11, Variant::Root());
        data.setValue(BaB_A11, Variant::Root(6.0));
        data.setValue(BsB_S11, Variant::Root());
        data.setValue(BsR_S11, Variant::Root(9.0));
        filter->process(A11, data);
        QVERIFY(!FP::defined(data.value(BaB_A11).toReal()));
        QVERIFY(!data.value(F1_A11).exists());

        data.setValue(BaB_A12, Variant::Root(5.0));
        data.setValue(BsB_S11, Variant::Root(10.0));
        data.setValue(BeB_E11, Variant::Root(12.0));
        filter->process(A12, data);
        QCOMPARE(data.value(BaB_A12).toDouble(), 3.81147540983607);

        data.setValue(BaB_A12, Variant::Root(5.0));
        data.setValue(BsB_S11, Variant::Root());
        data.setValue(BeB_E11, Variant::Root(12.0));
        filter->process(A12, data);
        QCOMPARE(data.value(BaB_A12).toDouble(), 3.84166666666667);

        data.setValue(BaB_A12, Variant::Root(5.0));
        data.setValue(BsB_S11, Variant::Root());
        data.setValue(BeB_E11, Variant::Root());
        filter->process(A12, data);
        QVERIFY(!FP::defined(data.value(BaB_A12).toReal()));

        data = SequenceSegment();
        data.setStart(14.0);
        data.setEnd(15.0);

        data.setValue(BaB_A11, Variant::Root(8.0));
        data.setValue(BsB_S11, Variant::Root(10.0));
        data.setValue(BsR_S11, Variant::Root(9.0));
        filter->process(A11, data);
        QCOMPARE(data.value(BaB_A11).toDouble(), 5.11872321164320);

        data.setValue(BaB_A11, Variant::Root(8.0));
        data.setValue(BsB_S11, Variant::Root());
        data.setValue(BsR_S11, Variant::Root(9.0));
        data.setValue(BeB_E11, Variant::Root(10.0));
        data.setValue(BeR_E11, Variant::Root(9.0));
        filter->process(A11, data);
        QCOMPARE(data.value(BaB_A11).toDouble(), 5.14604750757663);

        data.setValue(BaB_A11, Variant::Root(8.0));
        data.setValue(BsB_S11, Variant::Root());
        data.setValue(BsR_S11, Variant::Root(9.0));
        data.setValue(BeB_E11, Variant::Root(10.0));
        data.setValue(BeR_E11, Variant::Root());
        filter->process(A11, data);
        QVERIFY(!FP::defined(data.value(BaB_A11).toReal()));

        delete filter;
    }

    void extend()
    {
        double start = 3600;
        double end = 7200;
        Variant::Root cv;
        cv["A11/CorrectAbsorption"] = "::Ba[BGR]_A11:=";
        cv["A11/Flags"] = "::F1_A11:=";
        cv["A11/Scattering/Input"] = "::Bs[BGR]_S11:=";
        component->extendBasicFilterEditing(start, end, "bnd", "raw", ValueSegment::Transfer{
                ValueSegment(FP::undefined(), FP::undefined(), cv)});
        QVERIFY(start <= 3600 - 15 * 60);
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
