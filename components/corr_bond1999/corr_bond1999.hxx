/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef CORRBOND1999_H
#define CORRBOND1999_H

#include "core/first.hxx"

#include <vector>
#include <QtGlobal>
#include <QObject>
#include <QList>

#include "core/component.hxx"
#include "datacore/segmentprocessingstage.hxx"
#include "datacore/dynamicinput.hxx"
#include "datacore/dynamicsequenceselection.hxx"
#include "datacore/dynamicprimitive.hxx"
#include "editing/wavelengthadjust.hxx"
#include "smoothing/baseline.hxx"

class CorrBond1999 : public CPD3::Data::SegmentProcessingStage {
    class Processing {
    public:
        CPD3::Data::DynamicSequenceSelection *operateAbsorption;
        CPD3::Data::DynamicSequenceSelection *operateFlags;

        CPD3::Data::DynamicInput *inputK1;
        CPD3::Data::DynamicInput *inputK2;
        CPD3::Data::DynamicBool *inputCoarse;

        CPD3::Editing::WavelengthAdjust *scattering;
        CPD3::Editing::WavelengthAdjust *extinction;
        CPD3::Editing::WavelengthTracker absorptionWavelength;

        Processing() : operateAbsorption(NULL),
                       operateFlags(NULL),
                       inputK1(NULL),
                       inputK2(NULL),
                       inputCoarse(NULL),
                       scattering(NULL),
                       extinction(NULL),
                       absorptionWavelength()
        { }
    };

    CPD3::Smoothing::BaselineSmoother *defaultSmoother;
    CPD3::Data::DynamicInput *defaultK1;
    CPD3::Data::DynamicInput *defaultK2;
    CPD3::Data::DynamicBool *defaultCoarse;
    CPD3::Data::DynamicSequenceSelection *defaultScattering;
    CPD3::Data::DynamicSequenceSelection *defaultExtinction;
    CPD3::Data::SequenceName::ComponentSet filterSuffixes;

    bool restrictedInputs;

    void handleOptions(const CPD3::ComponentOptions &options);

    std::vector<Processing> processing;

public:
    CorrBond1999();

    CorrBond1999(const CPD3::ComponentOptions &options);

    CorrBond1999(const CPD3::ComponentOptions &options,
                 double start, double end, const QList<CPD3::Data::SequenceName> &inputs);

    CorrBond1999(double start,
                 double end,
                 const CPD3::Data::SequenceName::Component &station,
                 const CPD3::Data::SequenceName::Component &archive,
                 const CPD3::Data::ValueSegment::Transfer &config);

    virtual ~CorrBond1999();

    void unhandled(const CPD3::Data::SequenceName &unit,
                   CPD3::Data::SegmentProcessingStage::SequenceHandlerControl *control) override;

    void process(int id, CPD3::Data::SequenceSegment &data) override;

    void processMeta(int id, CPD3::Data::SequenceSegment &data) override;

    CPD3::Data::SequenceName::Set requestedInputs() override;

    QSet<double> metadataBreaks(int id) override;

    CorrBond1999(QDataStream &stream);

    void serialize(QDataStream &stream) override;
};

class CorrBond1999Component
        : public QObject, virtual public CPD3::Data::SegmentProcessingStageComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ProcessingStageComponent
                         CPD3::Data::SegmentProcessingStageComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.corr_bond1999"
                              FILE
                              "corr_bond1999.json")
public:
    virtual QString getBasicSerializationName() const;

    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual CPD3::Data::SegmentProcessingStage
            *createBasicFilterDynamic(const CPD3::ComponentOptions &options);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterPredefined
            (const CPD3::ComponentOptions &options,
             double start,
             double end, const QList<CPD3::Data::SequenceName> &inputs);

    virtual CPD3::Data::SegmentProcessingStage *createBasicFilterEditing(double start,
                                                                         double end,
                                                                         const CPD3::Data::SequenceName::Component &station,
                                                                         const CPD3::Data::SequenceName::Component &archive,
                                                                         const CPD3::Data::ValueSegment::Transfer &config);

    virtual void extendBasicFilterEditing(double &start,
                                          double &end,
                                          const CPD3::Data::SequenceName::Component &station,
                                          const CPD3::Data::SequenceName::Component &archive,
                                          const CPD3::Data::ValueSegment::Transfer &,
                                          CPD3::Data::Archive::Access *access);

    virtual CPD3::Data::SegmentProcessingStage *deserializeBasicFilter(QDataStream &stream);
};

#endif
