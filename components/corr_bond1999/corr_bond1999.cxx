/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QRegExp>

#include "core/timeutils.hxx"
#include "core/environment.hxx"

#include "corr_bond1999.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Editing;
using namespace CPD3::Smoothing;

static BaselineSmoother *makeDefaultSmoother()
{
    return new BaselineDigitalFilter(
            new DigitalFilterSinglePoleLowPass(new DynamicTimeInterval::Constant(Time::Minute, 3),
                                               new DynamicTimeInterval::Constant(Time::Minute, 35),
                                               false));
}

static QString makeSuffixMatcher(const QSet<QString> &input)
{
    if (input.isEmpty())
        return ".+";
    QStringList list(input.values());
    for (QStringList::iterator mod = list.begin(), endMod = list.end(); mod != endMod; ++mod) {
        *mod = QString("(?:%1)").arg(QRegExp::escape(*mod));
    }
    std::sort(list.begin(), list.end());
    return QString("(?:%1)").arg(list.join("|"));
}

void CorrBond1999::handleOptions(const ComponentOptions &options)
{
    if (options.isSet("k1")) {
        defaultK1 = qobject_cast<DynamicInputOption *>(options.get("k1"))->getInput();
    } else {
        defaultK1 = NULL;
    }
    if (options.isSet("k2")) {
        defaultK2 = qobject_cast<DynamicInputOption *>(options.get("k2"))->getInput();
    } else {
        defaultK2 = NULL;
    }
    if (options.isSet("coarse")) {
        defaultCoarse = qobject_cast<DynamicBoolOption *>(options.get("coarse"))->getInput();
    } else {
        defaultCoarse = NULL;
    }

    if (options.isSet("scattering")) {
        defaultScattering = qobject_cast<DynamicSequenceSelectionOption *>(
                options.get("scattering"))->getOperator();
    } else if (options.isSet("scattering-suffix")) {
        defaultScattering = new DynamicSequenceSelection::Match(QString(), QString(),
                                                                QString("Bsn?[A-Z0-9]*_%1").arg(
                                                                        makeSuffixMatcher(
                                                                                qobject_cast<
                                                                                        ComponentOptionInstrumentSuffixSet *>(
                                                                                        options.get(
                                                                                                "scattering-suffix"))
                                                                                        ->get())));
    } else {
        defaultScattering = NULL;
    }

    if (options.isSet("extinction")) {
        defaultExtinction = qobject_cast<DynamicSequenceSelectionOption *>(
                options.get("extinction"))->getOperator();
    } else if (options.isSet("extinction-suffix")) {
        defaultExtinction = new DynamicSequenceSelection::Match(QString(), QString(),
                                                                QString("Ben?[A-Z0-9]*_%1").arg(
                                                                        makeSuffixMatcher(
                                                                                qobject_cast<
                                                                                        ComponentOptionInstrumentSuffixSet *>(
                                                                                        options.get(
                                                                                                "extinction-suffix"))
                                                                                        ->get())));
    } else {
        defaultExtinction = NULL;
    }

    if (defaultScattering == NULL && defaultExtinction == NULL) {
        defaultScattering = new DynamicSequenceSelection::Match(QString(), QString(),
                                                                QString("Bsn?[A-Z0-9]*_S11"));
    }

    if (options.isSet("smoothing")) {
        defaultSmoother =
                qobject_cast<BaselineSmootherOption *>(options.get("smoothing"))->getSmoother();
    } else {
        defaultSmoother = NULL;
    }


    restrictedInputs = false;

    if (options.isSet("absorption")) {
        restrictedInputs = true;

        Processing p;

        SequenceName::Component station;
        SequenceName::Component archive;
        SequenceName::Component suffix;
        SequenceName::Flavors flavors;

        p.operateAbsorption = qobject_cast<DynamicSequenceSelectionOption *>(
                options.get("absorption"))->getOperator();
        for (const auto &name : p.operateAbsorption->getAllUnits()) {
            suffix = Util::suffix(name.getVariable(), '_');
            station = name.getStation();
            archive = name.getArchive();
            flavors = name.getFlavors();
            if (suffix.length() != 0)
                break;
        }

        p.operateFlags = new DynamicSequenceSelection::Single(
                SequenceName(station, archive, "F1_%1" + suffix, flavors));

        if (defaultK1 != NULL)
            p.inputK1 = defaultK1->clone();
        else
            p.inputK1 = new DynamicInput::Constant(0.02);
        if (defaultK2 != NULL)
            p.inputK2 = defaultK2->clone();
        else
            p.inputK2 = new DynamicInput::Constant(1.22);
        if (defaultCoarse != NULL)
            p.inputCoarse = defaultCoarse->clone();
        else
            p.inputCoarse = new DynamicPrimitive<bool>::Constant(false);

        if (defaultScattering != NULL) {
            p.scattering = WavelengthAdjust::fromInput(defaultScattering->clone(),
                                                       defaultSmoother != NULL
                                                       ? defaultSmoother->clone()
                                                       : makeDefaultSmoother());
        } else {
            p.scattering = WavelengthAdjust::fromInput(new DynamicSequenceSelection::None);
        }

        if (defaultExtinction != NULL) {
            p.extinction = WavelengthAdjust::fromInput(defaultExtinction->clone(),
                                                       defaultSmoother != NULL
                                                       ? defaultSmoother->clone()
                                                       : makeDefaultSmoother());
        } else {
            p.extinction = WavelengthAdjust::fromInput(new DynamicSequenceSelection::None);
        }

        processing.push_back(p);
    }

    if (options.isSet("suffix")) {
        filterSuffixes = Util::set_from_qstring(
                qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix"))->get());
    }
}


CorrBond1999::CorrBond1999()
{ Q_ASSERT(false); }

CorrBond1999::CorrBond1999(const ComponentOptions &options)
{
    handleOptions(options);
}

CorrBond1999::CorrBond1999(const ComponentOptions &options,
                           double start,
                           double end,
                           const QList<SequenceName> &inputs)
{
    Q_UNUSED(start);
    Q_UNUSED(end);
    handleOptions(options);

    for (QList<SequenceName>::const_iterator unit = inputs.constBegin(), endU = inputs.constEnd();
            unit != endU;
            ++unit) {
        CorrBond1999::unhandled(*unit, NULL);
    }
}

CorrBond1999::CorrBond1999(double start,
                           double end,
                           const SequenceName::Component &station,
                           const SequenceName::Component &archive,
                           const ValueSegment::Transfer &config)
{
    defaultSmoother = NULL;
    defaultK1 = NULL;
    defaultK2 = NULL;
    defaultCoarse = NULL;
    defaultScattering = NULL;
    defaultExtinction = NULL;
    restrictedInputs = true;

    std::unordered_set<Variant::PathElement::HashIndex> children;
    for (const auto &add : config) {
        Util::merge(add.read().toHash().keys(), children);
    }
    children.erase(Variant::PathElement::HashIndex());

    Variant::Root defaultAdjust;
    defaultAdjust["Smoothing/Type"].setString("SinglePoleLowPass");
    defaultAdjust["Smoothing/TimeConstant/Units"].setString("Minute");
    defaultAdjust["Smoothing/TimeConstant/Count"].setInt64(3);
    defaultAdjust["Smoothing/Gap/Units"].setString("Minute");
    defaultAdjust["Smoothing/Gap/Count"].setInt64(35);
    defaultAdjust["Smoothing/ResetOnUndefined"].setBool(false);

    for (const auto &child : children) {
        Processing p;

        p.operateAbsorption = DynamicSequenceSelection::fromConfiguration(config,
                                                                          QString("%1/CorrectAbsorption")
                                                                                  .arg(QString::fromStdString(
                                                                                          child)),
                                                                          start, end);
        p.operateFlags = DynamicSequenceSelection::fromConfiguration(config,
                                                                     QString("%1/Flags").arg(
                                                                             QString::fromStdString(
                                                                                     child)), start,
                                                                     end);

        p.inputK1 = DynamicInput::fromConfiguration(config, QString("%1/K1").arg(
                QString::fromStdString(child)), start, end);
        p.inputK2 = DynamicInput::fromConfiguration(config, QString("%1/K2").arg(
                QString::fromStdString(child)), start, end);
        p.inputCoarse = DynamicBoolOption::fromConfiguration(config, QString("%1/Coarse").arg(
                QString::fromStdString(child)), start, end);

        p.scattering = WavelengthAdjust::fromConfiguration(config, QString("%1/Scattering").arg(
                QString::fromStdString(child)), start, end, defaultAdjust);
        p.extinction = WavelengthAdjust::fromConfiguration(config, QString("%1/Extinction").arg(
                QString::fromStdString(child)), start, end, defaultAdjust);

        p.operateAbsorption->registerExpected(station, archive);
        p.operateFlags->registerExpected(station, archive);
        p.inputK1->registerExpected(station, archive);
        p.inputK2->registerExpected(station, archive);
        p.scattering->registerExpected(station, archive);
        p.extinction->registerExpected(station, archive);

        processing.push_back(p);
    }
}

void CorrBond1999Component::extendBasicFilterEditing(double &start,
                                                     double &end,
                                                     const SequenceName::Component &station,
                                                     const SequenceName::Component &archive,
                                                     const ValueSegment::Transfer &config,
                                                     Archive::Access *)
{
    if (!FP::defined(start))
        return;

    std::unordered_set<Variant::PathElement::HashIndex> children;
    for (const auto &add : config) {
        Util::merge(add.read().toHash().keys(), children);
    }
    children.erase(Variant::PathElement::HashIndex());

    Variant::Root defaultAdjust;
    defaultAdjust["Smoothing/Type"].setString("SinglePoleLowPass");
    defaultAdjust["Smoothing/TimeConstant/Units"].setString("Minute");
    defaultAdjust["Smoothing/TimeConstant/Count"].setInt64(3);
    defaultAdjust["Smoothing/Gap/Units"].setString("Minute");
    defaultAdjust["Smoothing/Gap/Count"].setInt64(35);
    defaultAdjust["Smoothing/ResetOnUndefined"].setBool(false);

    double result = start;
    for (const auto &child : children) {
        WavelengthAdjust *wl = WavelengthAdjust::fromConfiguration(config,
                                                                   QString("%1/Scattering").arg(
                                                                           QString::fromStdString(
                                                                                   child)), start,
                                                                   end, defaultAdjust);
        double check = wl->spinupTime(start);
        if (FP::defined(check) && check < result)
            result = check;
        delete wl;

        wl = WavelengthAdjust::fromConfiguration(config, QString("%1/Extinction").arg(
                QString::fromStdString(child)), start, end, defaultAdjust);
        check = wl->spinupTime(start);
        if (FP::defined(check) && check < result)
            result = check;
        delete wl;
    }
    start = result;
}

CorrBond1999::~CorrBond1999()
{
    if (defaultSmoother != NULL)
        delete defaultSmoother;
    if (defaultK1 != NULL)
        delete defaultK1;
    if (defaultK2 != NULL)
        delete defaultK2;
    if (defaultCoarse != NULL)
        delete defaultCoarse;
    if (defaultScattering != NULL)
        delete defaultScattering;
    if (defaultExtinction != NULL)
        delete defaultExtinction;

    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        delete p->operateAbsorption;
        delete p->operateFlags;
        delete p->inputK1;
        delete p->inputK2;
        delete p->inputCoarse;
        delete p->scattering;
        delete p->extinction;
    }
}

void CorrBond1999::unhandled(const SequenceName &unit,
                             SegmentProcessingStage::SequenceHandlerControl *control)
{
    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        if (processing[id].inputK1->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
        }
        if (processing[id].inputK2->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
        }
        if (processing[id].scattering->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
        }
        if (processing[id].extinction->registerInput(unit)) {
            if (control != NULL)
                control->inputUnit(unit, id);
        }
    }
    if (defaultK1 != NULL && defaultK1->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultK2 != NULL && defaultK2->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultScattering != NULL && defaultScattering->registerInput(unit) && control != NULL)
        control->deferHandling(unit);
    if (defaultExtinction != NULL && defaultExtinction->registerInput(unit) && control != NULL)
        control->deferHandling(unit);

    for (int id = 0, max = (int) processing.size(); id < max; id++) {
        if (processing[id].operateAbsorption->registerInput(unit)) {
            if (control != NULL)
                control->filterUnit(unit, id);
            return;
        }
        if (processing[id].operateFlags->registerInput(unit)) {
            if (control != NULL)
                control->filterUnit(unit, id);
            return;
        }
    }

    if (restrictedInputs)
        return;

    if (unit.hasFlavor(SequenceName::flavor_cover) || unit.hasFlavor(SequenceName::flavor_stats))
        return;


    auto suffix = Util::suffix(unit.getVariable(), '_');
    if (suffix.empty())
        return;
    if (!filterSuffixes.empty() && !filterSuffixes.count(suffix))
        return;

    if (!Util::starts_with(unit.getVariable(), "Ba")) {
        if (control != NULL) {
            if (Util::starts_with(unit.getVariable(), "F1_"))
                control->deferHandling(unit);
        }
        return;
    }

    Processing p;

    SequenceName flagUnit(unit.getStation(), unit.getArchive(), "F1_" + suffix, unit.getFlavors());

    p.operateAbsorption =
            new DynamicSequenceSelection::Match(unit.getStationQString(), unit.getArchiveQString(),
                                                QString("Ba.*_%1").arg(QRegExp::escape(
                                                        QString::fromStdString(suffix))),
                                                unit.getFlavors());
    p.operateAbsorption->registerInput(unit);

    if (defaultK1 != NULL)
        p.inputK1 = defaultK1->clone();
    else
        p.inputK1 = new DynamicInput::Constant(0.02);
    p.inputK1->registerInput(unit);
    p.inputK1->registerInput(flagUnit);

    if (defaultK2 != NULL)
        p.inputK2 = defaultK2->clone();
    else
        p.inputK2 = new DynamicInput::Constant(1.22);
    p.inputK2->registerInput(unit);
    p.inputK2->registerInput(flagUnit);

    if (defaultCoarse != NULL)
        p.inputCoarse = defaultCoarse->clone();
    else
        p.inputCoarse = new DynamicPrimitive<bool>::Constant(false);

    if (defaultScattering != NULL) {
        DynamicSequenceSelection *s = defaultScattering->clone();
        s->forceUnit(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
        p.scattering = WavelengthAdjust::fromInput(s, defaultSmoother != NULL
                                                      ? defaultSmoother->clone()
                                                      : makeDefaultSmoother());
    } else {
        p.scattering = WavelengthAdjust::fromInput(new DynamicSequenceSelection::None);
    }

    if (defaultExtinction != NULL) {
        DynamicSequenceSelection *e = defaultExtinction->clone();
        e->forceUnit(unit.getStation(), unit.getArchive(), {}, unit.getFlavors());
        p.extinction = WavelengthAdjust::fromInput(e, defaultSmoother != NULL
                                                      ? defaultSmoother->clone()
                                                      : makeDefaultSmoother());
    } else {
        p.extinction = WavelengthAdjust::fromInput(new DynamicSequenceSelection::None);
    }

    p.operateFlags = new DynamicSequenceSelection::Single(flagUnit);
    p.operateFlags->registerInput(unit);
    p.operateFlags->registerInput(flagUnit);

    p.scattering->registerInput(unit);
    p.scattering->registerInput(flagUnit);
    p.extinction->registerInput(unit);
    p.extinction->registerInput(flagUnit);

    int id = (int) processing.size();
    processing.push_back(p);

    if (control) {
        for (const auto &n : p.scattering->getAllUnits()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : p.extinction->getAllUnits()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : p.inputK1->getUsedInputs()) {
            control->inputUnit(n, id);
        }
        for (const auto &n : p.inputK2->getUsedInputs()) {
            control->inputUnit(n, id);
        }

        control->filterUnit(unit, id);
        if (unit != flagUnit)
            control->filterUnit(flagUnit, id);
    }
}

static double convertReferenced(const Variant::Read &base, const Variant::Read &reference)
{
    double a = base.getPath(reference.currentPath()).toReal();
    if (FP::defined(a))
        return a;
    switch (base.getType()) {
    case Variant::Type::Array:
    case Variant::Type::Matrix:
        if (reference.currentPath().empty()) {
            return base.array(0).toReal();
        }
        return FP::undefined();
    default:
        break;
    }

    return base.toReal();
}

static const Variant::Flag FLAG_NAME = "Bond1999";

void CorrBond1999::process(int id, SequenceSegment &data)
{
    Processing *proc = &processing[id];

    for (const auto &i : proc->operateFlags->get(data)) {
        if (!data.exists(i))
            continue;
        auto d = data[i];
        if (!d.exists())
            continue;
        d.applyFlag(FLAG_NAME);
    }

    proc->scattering->incoming(data);
    proc->extinction->incoming(data);

    double K1 = proc->inputK1->get(data);
    if (!FP::defined(K1) || K1 < 0.0 || K1 > 0.1)
        K1 = 0.02;
    double K2 = proc->inputK2->get(data);
    if (!FP::defined(K2) || K2 <= 0.0)
        K2 = 1.22;
    bool isCoarse = proc->inputCoarse->get(data);

    /* Constant factor implicit in the fits, so undo it */
    static const double wavelengthCorrection = 0.97;

    for (const auto &i : proc->operateAbsorption->get(data)) {
        if (!data.exists(i))
            continue;

        double wavelength = proc->absorptionWavelength.get(i);
        auto scattering = proc->scattering->getAdjusted(wavelength);
        auto extinction = proc->extinction->getAdjusted(wavelength);

        Variant::Composite::applyInplace(data[i], [&](Variant::Write &d) {
            if (isCoarse) {
                double a = convertReferenced(proc->scattering->getAngstrom(wavelength), d);
                if (!FP::defined(a))
                    a = convertReferenced(proc->extinction->getAngstrom(wavelength), d);

                if (!FP::defined(a)) {
                    K1 = 0.0;
                } else if (a >= 0.6) {
                    K1 = 0.02;
                } else if (a <= 0.2) {
                    K1 = 0.00668;
                } else {
                    K1 = 0.0334 * a;
                }
            }

            double v = d.toReal();
            if (!FP::defined(v) || !FP::defined(K1) || !FP::defined(K2) || K2 == 0.0) {
                Variant::Composite::invalidate(d);
                return;
            }

            double bs = convertReferenced(scattering, d);
            double be = convertReferenced(extinction, d);

            if (FP::defined(bs)) {
                d.setReal((v * wavelengthCorrection - K1 * bs) / K2);
            } else if (FP::defined(be) && K1 != K2) {
                d.setReal((v * wavelengthCorrection - K1 * be) / (K2 - K1));
            } else {
                Variant::Composite::invalidate(d);
            }
        });
    }
}

void CorrBond1999::processMeta(int id, SequenceSegment &data)
{
    Processing *p = &processing[id];

    p->scattering->incomingMeta(data);
    p->extinction->incomingMeta(data);

    Variant::Root meta;

    meta["By"].setString("corr_bond1999");
    meta["At"].setDouble(Time::time());
    meta["Environment"].setString(Environment::describe());
    meta["Revision"].setString(Environment::revision());

    if (p->inputCoarse->get(data)) {
        meta["Parameters"].hash("K1") = "Coarse";
    } else {
        meta["Parameters"].hash("K1") = p->inputK1->describe(data, "0.02");
    }
    meta["Parameters"].hash("K2") = p->inputK2->describe(data, "1.22");
    meta["Parameters"].hash("WavelengthFactor") = "0.97";

    {
        const auto &s = p->operateAbsorption->get(data);
        p->absorptionWavelength.incomingMeta(data, s);
        for (auto i : s) {
            i.setMeta();
            if (!data.exists(i))
                continue;
            data[i].metadata("Processing").toArray().after_back().set(meta);
            clearPropagatedSmoothing(data[i].metadata("Smoothing"));
        }
    }

    {
        const auto &s = p->operateFlags->get(data);
        for (auto i : s) {
            i.setMeta();
            if (!data.exists(i))
                continue;
            auto flagMeta = data[i].metadataSingleFlag(FLAG_NAME);
            flagMeta.hash("Origin").toArray().after_back().setString("corr_bond1999");
            flagMeta.hash("Bits").setInt64(0x0100);
            flagMeta.hash("Description").setString("Bond 1999 correction applied");
        }
    }
}


SequenceName::Set CorrBond1999::requestedInputs()
{
    SequenceName::Set out;
    for (std::vector<Processing>::const_iterator p = processing.begin(), endProc = processing.end();
            p != endProc;
            ++p) {
        Util::merge(p->inputK1->getUsedInputs(), out);
        Util::merge(p->inputK2->getUsedInputs(), out);
        Util::merge(p->scattering->getAllUnits(), out);
        Util::merge(p->extinction->getAllUnits(), out);
        Util::merge(p->operateAbsorption->getAllUnits(), out);
        Util::merge(p->operateFlags->getAllUnits(), out);
    }
    return out;
}

QSet<double> CorrBond1999::metadataBreaks(int id)
{
    Processing *p = &processing[id];
    return p->operateAbsorption->getChangedPoints() |
            p->operateFlags->getChangedPoints() |
            p->inputK1->getChangedPoints() |
            p->inputK2->getChangedPoints() |
            p->inputCoarse->getChangedPoints() |
            p->scattering->getChangedPoints() |
            p->extinction->getChangedPoints();
}

CorrBond1999::CorrBond1999(QDataStream &stream)
{
    stream >> defaultSmoother;
    stream >> defaultK1;
    stream >> defaultK2;
    stream >> defaultCoarse;
    stream >> defaultScattering;
    stream >> defaultExtinction;
    stream >> filterSuffixes;
    stream >> restrictedInputs;

    quint32 n;
    stream >> n;
    for (int i = 0; i < (int) n; i++) {
        Processing p;
        stream >> p.operateAbsorption;
        stream >> p.operateFlags;
        stream >> p.inputK1;
        stream >> p.inputK2;
        stream >> p.inputCoarse;
        stream >> p.scattering;
        stream >> p.extinction;
        stream >> p.absorptionWavelength;
        processing.push_back(p);
    }
}

void CorrBond1999::serialize(QDataStream &stream)
{
    stream << defaultSmoother;
    stream << defaultK1;
    stream << defaultK2;
    stream << defaultCoarse;
    stream << defaultScattering;
    stream << defaultExtinction;
    stream << filterSuffixes;
    stream << restrictedInputs;

    quint32 n = (quint32) processing.size();
    stream << n;
    for (int i = 0; i < (int) n; i++) {
        stream << processing[i].operateAbsorption;
        stream << processing[i].operateFlags;
        stream << processing[i].inputK1;
        stream << processing[i].inputK2;
        stream << processing[i].inputCoarse;
        stream << processing[i].scattering;
        stream << processing[i].extinction;
        stream << processing[i].absorptionWavelength;
    }
}


QString CorrBond1999Component::getBasicSerializationName() const
{ return QString::fromLatin1("corr_bond1999"); }

ComponentOptions CorrBond1999Component::getOptions()
{
    ComponentOptions options;

    options.add("k1",
                new DynamicInputOption(tr("k1", "name"), tr("K1 (scattering factor) constant"),
                                       tr("This is the fraction of (wavelength adjusted) scattering "
                                              "subtracted from the absorption values.  This option is ignored "
                                              "when operating in coarse mode, where the fraction subtracted is "
                                              "a function of the \xC3\x85ngstr\xC3\xB6m exponent."),
                                       tr("0.02")));
    options.add("k2", new DynamicInputOption(tr("k2", "name"), tr("K2 (scale factor) constant"),
                                             tr("This is the factor that the resulting absorption (after "
                                                "scattering subtraction) is divided by to get the final result.  "
                                                "When operating from extinction the K1 value is subtracted from "
                                                "this."), tr("1.22")));
    options.add("coarse",
                new DynamicBoolOption(tr("coarse", "name"), tr("Enable coarse mode constants"),
                                      tr("When enabled, this sets the K1 values to a function of the "
                                         "scattering \xC3\x85ngstr\xC3\xB6m exponent suitable for "
                                         "coarse-mode aerosol."), tr("Disabled")));

    BaselineSmootherOption *smoothing = new BaselineSmootherOption(tr("smoothing", "name"),
                                                                   tr("Scattering and extinction smoothing"),
                                                                   tr("This is the smoothing applied to the scattering and/or "
                                                                      "extinction inputs before any calculation using them."),
                                                                   tr("3-minute TC low pass filter"));
    smoothing->setStabilityDetection(false);
    smoothing->setSpikeDetection(false);
    smoothing->setDefault(makeDefaultSmoother());
    options.add("smoothing", smoothing);

    options.add("absorption", new DynamicSequenceSelectionOption(tr("correct", "name"),
                                                                 tr("Absorptions to correct"),
                                                                 tr("These are the variables to that the correction is applied to.  "
                                                                    "This option is mutually exclusive with instrument specification."),
                                                                 tr("All absorptions")));
    options.add("scattering", new DynamicSequenceSelectionOption(tr("scattering", "name"),
                                                                 tr("Input scatterings"),
                                                                 tr("These are the variables to the light scattering coefficients used "
                                                                    "by the correction are contained in.  This option is mutually "
                                                                    "exclusive with instrument specification."),
                                                                 tr("All scatterings from S11")));
    options.add("extinction", new DynamicSequenceSelectionOption(tr("extinction", "name"),
                                                                 tr("Input extinctions"),
                                                                 tr("These are the variables to the light extinction coefficients used "
                                                                    "by the correction are contained in.  By default none are used.  "
                                                                    "This option is mutually exclusive with instrument specification."),
                                                                 QString()));

    options.add("suffix", new ComponentOptionInstrumentSuffixSet(tr("instruments", "name"),
                                                                 tr("Instrument suffixes to correct"),
                                                                 tr("These are the instrument suffixes to correct.  "
                                                                    "For example S11 would usually specifies the reference nephelometer.  "
                                                                    "This option is mutually exclusive with manual variable specification."),
                                                                 tr("All instrument suffixes")));
    options.add("scattering-suffix",
                new ComponentOptionInstrumentSuffixSet(tr("scattering-instruments", "name"),
                                                       tr("Scattering instrument suffixes"),
                                                       tr("These are the instrument suffixes that input light scatterings are "
                                                          "read from.  "
                                                          "For example S11 would usually specifies the reference nephelometer.  "
                                                          "This option is mutually exclusive with manual variable specification."),
                                                       tr("S11")));
    options.add("extinction-suffix",
                new ComponentOptionInstrumentSuffixSet(tr("extinction-instruments", "name"),
                                                       tr("Extinction instrument suffixes"),
                                                       tr("These are the instrument suffixes that input light extinctions are "
                                                          "read from.  "
                                                          "For example S11 would usually specifies the reference nephelometer.  "
                                                          "This option is mutually exclusive with manual variable specification."),
                                                       QString()));

    options.exclude("absorption", "suffix");
    options.exclude("scattering", "scattering-suffix");
    options.exclude("extinction", "extinction-suffix");
    options.exclude("suffix", "absorption");
    options.exclude("scattering-suffix", "scattering");
    options.exclude("extinction-suffix", "extinction");

    return options;
}

QList<ComponentExample> CorrBond1999Component::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Defaults", "default example name"),
                                     tr("This will correct all absorptions present using the scatterings "
                                        "from the S11 instrument.  The default values for K1 (0.02) and "
                                        "K2 (1.22) will be used.")));
    examples.back().setInputTypes(ComponentExample::Input_Absorption);

    options = getOptions();
    (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("suffix")))->add("A11");
    (qobject_cast<ComponentOptionInstrumentSuffixSet *>(options.get("extinction-suffix")))->add(
            "E11");
    (qobject_cast<DynamicInputOption *>(options.get("k1")))->set(0.03);
    (qobject_cast<DynamicInputOption *>(options.get("k2")))->set(1.23);
    examples.append(ComponentExample(options, tr("Single instrument with manual constants"),
                                     tr("This will correct all absorptions from A11 using extinctions from "
                                        "E11.  Non-standard constants with K1 = 0.03 and K2 = 1.23 are "
                                        "also used.")));
    examples.back().setInputTypes(ComponentExample::Input_Absorption);

    options = getOptions();
    (qobject_cast<DynamicSequenceSelectionOption *>(options.get("absorption")))->set("", "",
                                                                                     "BaG_A11");
    (qobject_cast<DynamicBoolOption *>(options.get("coarse")))->set(true);
    examples.append(ComponentExample(options, tr("Single variable in coarse mode"),
                                     tr("This will correct only the variable BaG_A11 using the coarse mode "
                                        "constants.")));
    examples.back().setInputTypes(ComponentExample::Input_Absorption);

    return examples;
}

SegmentProcessingStage *CorrBond1999Component::createBasicFilterDynamic(const ComponentOptions &options)
{ return new CorrBond1999(options); }

SegmentProcessingStage *CorrBond1999Component::createBasicFilterPredefined(const ComponentOptions &options,
                                                                           double start,
                                                                           double end,
                                                                           const QList<
                                                                                   SequenceName> &inputs)
{ return new CorrBond1999(options, start, end, inputs); }

SegmentProcessingStage *CorrBond1999Component::createBasicFilterEditing(double start,
                                                                        double end,
                                                                        const SequenceName::Component &station,
                                                                        const SequenceName::Component &archive,
                                                                        const ValueSegment::Transfer &config)
{ return new CorrBond1999(start, end, station, archive, config); }

SegmentProcessingStage *CorrBond1999Component::deserializeBasicFilter(QDataStream &stream)
{ return new CorrBond1999(stream); }
