/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIREGMDCLAP3W_H
#define ACQUIREGMDCLAP3W_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"
#include "smoothing/baseline.hxx"

class AcquireGMDCLAP3W : public CPD3::Acquisition::FramedInstrument {
    double lastRecordTime;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum {
        /* Passive autoprobe initialization, ignoring the first timeout */
        RESP_AUTOPROBE_PASSIVE_INITIALIZE,
        /* Waiting for a valid record to confirm passive mode autoprobe */
        RESP_AUTOPROBE_PASSIVE_WAIT,

        /* Waiting for a valid record to start passive acquisition */
        RESP_PASSIVE_WAIT,

        /* Acquiring data in passive mode */
        RESP_PASSIVE_RUN,
        /* Acquiring data in interactive mode, but only reading unpolled data */
        RESP_UNPOLLED_RUN,

        /* Discarding data in passive mode (after a command) */
                RESP_PASSIVE_DISCARD,
        /* Discarding data in interactive mode (after a command) */
                RESP_UNPOLLED_DISCARD,

        /* Waiting for the completion of the "main" command during start
         * communications */
                RESP_INTERACTIVE_START_STOPREPORTS_MAIN,
        /* Waiting for the completion of the "hide" command during start
         * communications as well as flushing out all remaining data reports */
                RESP_INTERACTIVE_START_STOPREPORTS_HIDE,

        /* Waiting for data to be flushed after the "cfg" command before
         * issuing the "sn" command */
                RESP_INTERACTIVE_START_CONFIG_SN,
        /* Waiting for the result of the "sn" command */
                RESP_INTERACTIVE_START_CONFIG_READSN,
        /* Waiting for the flush data after the "sn" command before
         * issuing the "fw" command */
                RESP_INTERACTIVE_START_CONFIG_FW,
        /* Waiting for the result of the "fw" command */
                RESP_INTERACTIVE_START_CONFIG_READFW,

        /* Waiting for data to be flushed after the "fw" command before
         * returning to the main menu */
                RESP_INTERACTIVE_START_SETTINGS_WAIT,
        /* Waiting for data to be flushed after returning to the main
         * menu */
                RESP_INTERACTIVE_START_SETTINGS_MAIN,
        /* Waiting for data to be flushed after entering the "cal" menu
         * before issuing the flow read command. */
                RESP_INTERACTIVE_START_SETTINGS_CAL,
        /* Waiting for the result of the flow read, will also cause the
         * write out of the flow if needed, which then returns to the
         * writeflow state */
                RESP_INTERACTIVE_START_SETTINGS_READFLOW,
        /* Waiting for data to be flushed before trying to read the flow
         * again */
                RESP_INTERACTIVE_START_SETTINGS_WRITEFLOW,

        /* Waiting for data to be flushed before returning to the main
         * menu */
                RESP_INTERACTIVE_START_UNPOLLED_WAIT,
        /* Waiting for data to be flushed in the main menu before starting
         * communications */
                RESP_INTERACTIVE_START_UNPOLLED_MAIN,
        /* Discarding the first line, after starting communications */
                RESP_INTERACTIVE_START_UNPOLLED_DISCARDFIRST,
        /* Waiting for the first valid record */
                RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID,

        /* Waiting before attempting a communications restart */
                RESP_INTERACTIVE_RESTART_WAIT,
        /* Initial state for interactive start */
                RESP_INTERACTIVE_INITIALIZE,
    } responseState;
    int responseTry;

    enum SampleState {
        /* Normal operation */
                SAMPLE_RUN,
        /* Sampling on a spot to get the normalization for it */
                SAMPLE_SPOT_NORMALIZE,
        /* Waiting for the spot to change before normalization */
                SAMPLE_SPOT_ADVANCE,
        /* No spots sampling, getting a baseline for the filter */
                SAMPLE_FILTER_BASELINE,
        /* Waiting for the filter changing bit to be cleared */
                SAMPLE_FILTER_BASELINE_START,
        /* Filter currently changing */
                SAMPLE_FILTER_CHANGING,
        /* Waiting for the filter changing flag to be set */
                SAMPLE_FILTER_CHANGE_START,
        /* No spots sampling, getting a baseline for a white filter */
                SAMPLE_WHITE_FILTER_BASELINE,
        /* Waiting for the filter changing bit to be cleared */
                SAMPLE_WHITE_FILTER_BASELINE_START,
        /* Changing a white filter */
                SAMPLE_WHITE_FILTER_CHANGING,
        /* Waiting for the filter change flag to be set */
                SAMPLE_WHITE_FILTER_CHANGE_START,

        /* Same as above, but with the flow bypassed */
                SAMPLE_BYPASSED_RUN,
        SAMPLE_BYPASSED_SPOT_NORMALIZE,
        SAMPLE_BYPASSED_FILTER_BASELINE,
        SAMPLE_BYPASSED_FILTER_BASELINE_START,
        SAMPLE_BYPASSED_FILTER_CHANGING,
        SAMPLE_BYPASSED_FILTER_CHANGE_START,
        SAMPLE_BYPASSED_WHITE_FILTER_BASELINE,
        SAMPLE_BYPASSED_WHITE_FILTER_BASELINE_START,
        SAMPLE_BYPASSED_WHITE_FILTER_CHANGING,
        SAMPLE_BYPASSED_WHITE_FILTER_CHANGE_START,

        /* Exiting a bypass state to the run state, waiting for the spot */
                SAMPLE_UNBYPASS_RUN,
        /* Exiting a bypass state to the run state, waiting for the spot */
                SAMPLE_UNBYPASS_SPOT_NORMALIZE,

        /* In interactive mode but need a filter change */
                SAMPLE_REQUIRE_FILTER_CHANGE,
        /* In interactive mode but need a white filter change */
                SAMPLE_REQUIRE_WHITE_FILTER_CHANGE,
        /* Same but bypassed */
                SAMPLE_BYPASSED_REQUIRE_FILTER_CHANGE,
        SAMPLE_BYPASSED_REQUIRE_WHITE_FILTER_CHANGE,
    };
    SampleState sampleState;

    class Configuration {
        double start;
        double end;

    public:
        CPD3::Calibration hardwareFlowCal;
        double flowScale;
        double area[8];
        double wavelengths[3];
        bool strictMode;
        bool integrateVolume;
        bool checkNormalizationInstrumentID;

        bool enableAutodetectStart;
        bool enableAutodetectEnd;
        int autodetectStartTriggers;

        double verifyWhiteBand;

        double spotAdvanceTr[3];
        double autodetectStartIntensityLimit;
        double autodetectStartDarkLimit;
        double autodetectEndBand;

        bool realtimeDiagnostics;
        bool checkInstrumentTiming;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under, const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    class IntensityData {
    public:
        double startTime;
        double ID[10];
        double I[10][3];
        double In[8][3];

        IntensityData();
    };

    friend QDataStream &operator<<(QDataStream &stream, const IntensityData &data);

    friend QDataStream &operator>>(QDataStream &stream, IntensityData &data);

    int spotChangeCheck;
    int spotSetTarget;
    int bypassedSpot;
    double instrumentAcknowledgeTimeout;

    /* State that needs to be saved */
    double baseVolume;
    double spotNormalization[3];
    bool filterIsNotWhite;
    double normalizationStartTime;
    quint32 normalizationFilterID;
    quint8 normalizationSpot;
    double normalizationLastVolume;
    quint32 normalizationLastInstrumentTime;
    CPD3::Data::Variant::Root normalizationSerialNumber;
    CPD3::Data::Variant::Root normalizationFirmwareVersion;
    double normalizationLastTime;
    IntensityData filterStart;
    IntensityData filterWhite;
    CPD3::Data::Variant::Root priorInstrumentSerialNumber;
    CPD3::Data::Variant::Root priorInstrumentFirmwareVersion;

    bool haveAcceptedNormalization;
    bool havePriorInstrument;
    double whiteFilterChangeStartTime;

    double discardEndTime;
    double priorVolume;
    double priorIr[3];
    double priorIn[3];

    CPD3::Data::Variant::Root instrumentMeta;
    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;
    bool forceRealtimeStateEmit;

    bool forceFilterBreak;
    bool requireFilterBreak;

    CPD3::Data::SequenceValue persistentFn;
    CPD3::Data::SequenceValue persistentFf;

    CPD3::Data::DynamicTimeInterval *acceptNormalizationTime;
    CPD3::Data::DynamicTimeInterval *bypassResumeContaminate;
    CPD3::Smoothing::BaselineSmoother *filterBaselineID[10];
    CPD3::Smoothing::BaselineSmoother *filterBaselineI[10][3];
    CPD3::Smoothing::BaselineSmoother *filterBaselineIn[8][3];
    CPD3::Smoothing::BaselineSmoother *spotNormalizationIn[3];

    std::vector<double> recordTimeCheck;

    void setDefaultInvalid();

    void logValue(double startTime,
                  double endTime,
                  CPD3::Data::SequenceName::Component name,
                  CPD3::Data::Variant::Root &&value);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root &&value);

    bool autodetectStart(const CPD3::Data::Variant::Root ID[10],
                         const CPD3::Data::Variant::Root I[10][3]) const;

    bool filterBaseline() const;

    bool autodetectEnd() const;

    bool normalizationDone() const;

    bool isSameNormalization(double currentTime, qint64 Ff, int spotNumber, qint64 etime) const;

    bool isSameInstrument(double currentTime) const;

    bool isSameFilter(double currentTime) const;

    void invalidateNormalization();

    void saveFilterStart(double time);

    bool isWhiteFilter() const;

    void setInvalidFilterStart();

    void resetFilterBaseline();

    void spotNormalizationStart();

    bool requireFilterChangeOnSpotFailure() const;

    bool whiteFilterTimeout(double time) const;

    CPD3::Data::Variant::Root buildFilterValue(const IntensityData &data) const;

    CPD3::Data::Variant::Root buildNormalizationValue() const;

    void emitFilterEnd(double endTime);

    void emitWhiteEnd(double endTime);

    void emitNormalizationEnd(double endTime);

    void insertDiagnosticFilterValues(CPD3::Data::Variant::Write &target,
                                      const QString &name,
                                      const IntensityData &source) const;

    void insertDiagnosticNormalization(CPD3::Data::Variant::Write &target) const;

    void insertDiagnosticFilterBaseline(CPD3::Data::Variant::Write &target) const;

    void insertDiagnosticNormalizationStable(CPD3::Data::Variant::Write &target) const;

    CPD3::Data::Variant::Root buildFilterValueMeta() const;

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    int processRecord(const CPD3::Util::ByteView &line, double startTime, double endTime);

public:
    AcquireGMDCLAP3W(const CPD3::Data::ValueSegment::Transfer &config,
                     const std::string &loggingContext);

    AcquireGMDCLAP3W(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireGMDCLAP3W();

    virtual CPD3::Data::SequenceValue::Transfer getPersistentValues();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    virtual CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus();

    CPD3::Data::Variant::Root getCommands() override;

    virtual CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables();

    virtual CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus();

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    virtual void serializeState(QDataStream &stream);

    virtual void deserializeState(QDataStream &stream);

    virtual void initializeState();

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    virtual void command(const CPD3::Data::Variant::Read &value);

    virtual void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingInstrumentTimeout(double time);

    virtual void discardDataCompleted(double time);
};

class AcquireGMDCLAP3WComponent
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_gmd_clap3w"
                              FILE
                              "acquire_gmd_clap3w.json")

    CPD3::ComponentOptions getBaseOptions();

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
