/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <cmath>
#include <cstdint>
#include <cstring>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"
#include "smoothing/baseline.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;
using namespace CPD3::Smoothing;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    double unpolledRemaining;
    double accumulatedTime;

    double intensities[40];
    double deltaI[40];
    bool changing;
    double filterTime;
    int filterID;
    int spotNumber;
    double flow;
    double volume;
    double T;
    double T_case;

    QByteArray sn;
    QByteArray fw;
    double flowCal[4];

    enum {
        MAIN, CFG, CAL,
    } menu;

    ModelInstrument() : incoming(), outgoing(), unpolledRemaining(0)
    {
        for (int i = 0; i < 40; i++) {
            if (i % 4 == 0) {
                intensities[i] = -100.0 + i;
                deltaI[i] = (double) i * -0.005;
            } else if (i < 4 || i >= 40 - 4) {
                intensities[i] = 80000.0 + i;
                deltaI[i] = (double) i * -0.001;
            } else {
                intensities[i] = 100000.0 + i;
                deltaI[i] = (double) i * -1.5;
            }
        }

        changing = false;
        filterTime = 0.0;
        filterID = 0;
        spotNumber = 1;
        flow = 0.6;
        volume = 0.0;
        T = 30.0;
        T_case = 25.0;
        accumulatedTime = 0.0;

        sn = "10.001";
        fw = "10.100";
        flowCal[0] = 0.0;
        flowCal[1] = 1.0;
        flowCal[2] = 0.0;
        flowCal[3] = 0.0;

        menu = MAIN;
    }

    bool advance(double seconds)
    {
        for (int i = 0; i < 40; i++) {
            intensities[i] += deltaI[i] * seconds;
        }
        accumulatedTime += seconds;

        int idxCR = -1;
        while ((idxCR = incoming.indexOf('\r')) >= 0) {
            QByteArray line(incoming.mid(0, idxCR));

            switch (menu) {
            case MAIN:
                if (line == "main") {
                    outgoing.append("main\r");
                    break;
                } else if (line == "hide") {
                    unpolledRemaining = FP::undefined();
                    outgoing.append("unpolled reports disabled\r");
                    break;
                } else if (line == "show") {
                    unpolledRemaining = 0.0;
                    outgoing.append("unpolled reports enabled\r");
                    break;
                } else if (line == "cal") {
                    menu = CAL;
                    outgoing.append("calibration menu\r");
                    break;
                } else if (line == "cfg") {
                    menu = CFG;
                    outgoing.append("configuration menu\r");
                    break;
                } else if (line == "go") {
                    changing = false;
                    filterID++;
                    filterTime = 0;
                    outgoing.append("filter change end\r");
                    break;
                } else if (line == "stop") {
                    changing = true;
                    spotNumber = 0;
                    filterTime = 0;
                    volume = 0.0;
                    outgoing.append("filter change start\r");
                    break;
                } else if (line.startsWith("spot")) {
                    int idxE = -1;
                    if ((idxE = line.indexOf('=')) > 0) {
                        QByteArray value(line.mid(idxE + 1));
                        bool ok = false;
                        int priorSpot = spotNumber;
                        spotNumber = value.toInt(&ok);
                        if (!ok || spotNumber < 0 || spotNumber > 8)
                            spotNumber = 0;
                        if (spotNumber == 0 || priorSpot != spotNumber) {
                            filterTime = 0;
                            volume = 0.0;
                        }
                    }
                    outgoing.append("spot = ");
                    outgoing.append(QByteArray::number(spotNumber));
                    outgoing.append("\r");
                } else {
                    outgoing.append("?\r");
                }
                break;

            case CFG:
                if (line == "main") {
                    menu = MAIN;
                    outgoing.append("main menu\r");
                    break;
                } else if (line == "sn") {
                    outgoing.append("sn = ");
                    outgoing.append(sn);
                    outgoing.append('\r');
                    break;
                } else if (line == "fw") {
                    outgoing.append("fw = ");
                    outgoing.append(fw);
                    outgoing.append('\r');
                    break;
                } else {
                    outgoing.append("?\r");
                }
                break;

            case CAL:
                if (line == "main") {
                    menu = MAIN;
                    outgoing.append("main menu\r");
                    break;
                } else if (line == "flow") {
                    outgoing.append("flow = ");
                    for (int i = 0; i < 4; i++) {
                        if (i != 0) outgoing.append(", ");
                        outgoing.append(QByteArray::number(flowCal[i], 'E'));
                    }
                    outgoing.append('\r');
                    break;
                } else if (line.startsWith("flow")) {
                    int idxE = -1;
                    if ((idxE = line.indexOf('=')) > 0) {
                        QByteArray value(line.mid(idxE + 1));
                        QList<QByteArray> fields(value.split(','));
                        for (int i = 0, max = qMin(4, fields.length()); i < max; i++) {
                            bool ok = false;
                            flowCal[i] = fields.at(i).toDouble(&ok);
                            if (!ok) {
                                flowCal[i] = false;
                                break;
                            }
                        }
                    }
                } else {
                    outgoing.append("?\r");
                }
                break;
            }

            if (idxCR >= incoming.size() - 1) {
                incoming.clear();
                break;
            }
            incoming = incoming.mid(idxCR + 1);
        }

        if (spotNumber != 0) {
            filterTime += seconds;
            volume += flow * seconds / 60000.0;
        }
        if (FP::defined(unpolledRemaining)) {
            unpolledRemaining -= seconds;

            if (unpolledRemaining <= 0.0) {
                unpolledRemaining = 1.0;

                outgoing.append("03, ");
                if (changing)
                    outgoing.append("0001, ");
                else
                    outgoing.append("0000, ");
                outgoing.append(QByteArray::number(static_cast<int>(std::floor(filterTime)),
                                                   16).rightJustified(8, '0'));
                outgoing.append(", ");
                outgoing.append(QByteArray::number(filterID, 16).rightJustified(4, '0'));
                outgoing.append(", ");
                outgoing.append(QByteArray::number(spotNumber, 16).rightJustified(2, '0'));
                outgoing.append(", ");
                outgoing.append(QByteArray::number(flow, 'f', 3));
                outgoing.append(", ");
                outgoing.append(QByteArray::number(volume, 'f', 6));
                outgoing.append(", ");
                outgoing.append(QByteArray::number(T_case, 'f', 2));
                outgoing.append(", ");
                outgoing.append(QByteArray::number(T, 'f', 2));

                for (int i = 0; i < 40; i++) {
                    outgoing.append(", ");
                    float f = static_cast<float>(intensities[i]);
                    quint32 raw;
                    std::memcpy(&raw, &f, 4);
                    //raw = qToLittleEndian<quint32>(raw);
                    outgoing.append(QByteArray::number(raw, 16).rightJustified(8, '0'));
                }

                outgoing.append("\r");
                return true;
            }
        }
        return false;
    }

    double expectedID(double dT, int detector)
    {
        Q_ASSERT(detector >= 0 && detector < 10);
        detector *= 4;
        return (float) (intensities[detector] + dT * deltaI[detector]);
    }

    /* B, G, R */
    double expectedI(double dT, int detector, int color)
    {
        Q_ASSERT(detector >= 0 && detector < 10 && color >= 0 && color < 3);
        detector = detector * 4 + (2 - color) + 1;
        return (float) (intensities[detector] + dT * deltaI[detector]);
    }

    /* Zero indexed */
    double expectedIRef(double dT, int spot, int color)
    {
        Q_ASSERT(spot >= 0 && spot < 8 && color >= 0 && color < 3);
        int detector = ((spot % 2) == 0) ? 9 : 0;
        double ID = expectedID(dT, detector);
        double I = expectedI(dT, detector, color);
        return I - ID;
    }

    double expectedISam(double dT, int spot, int color)
    {
        Q_ASSERT(spot >= 0 && spot < 8 && color >= 0 && color < 3);
        double ID = expectedID(dT, spot + 1);
        double I = expectedI(dT, spot + 1, color);
        return I - ID;
    }

    double expectedIn(double dT, int spot, int color)
    {
        Q_ASSERT(spot >= 0 && spot < 8 && color >= 0 && color < 3);
        double IRef = expectedIRef(dT, spot, color);
        double ISam = expectedISam(dT, spot, color);
        if (IRef == 0.0) return FP::undefined();
        return ISam / IRef;
    }

    double expectedTr(double dT0, double dT, int spot, int color)
    {
        Q_ASSERT(spot >= 0 && spot < 8 && color >= 0 && color < 3);
        double In0 = expectedIn(dT0, spot, color);
        double In = expectedIn(dT, spot, color);
        if (!FP::defined(In0) || In0 == 0.0) return FP::undefined();
        return In / In0;
    }

    double expectedQt(double dT, double calQ = 1.0)
    {
        return volume * calQ + dT * flow * calQ / 60000.0;
    }

    double expectedL(double dT, double calQ = 1.0, double area = 19.9)
    {
        return expectedQt(dT, calQ) / (area * 1E-6);
    }

    double expectedBa(double dTStart,
                      double dT0,
                      double dT1,
                      int spot,
                      int color,
                      double calQ = 1.0,
                      double area = 19.9)
    {
        Q_ASSERT(spot >= 0 && spot < 8 && color >= 0 && color < 3);
        double Qt0 = expectedQt(dT0, calQ);
        double Qt1 = expectedQt(dT1, calQ);
        double Ir0 = expectedTr(dTStart, dT0, spot, color);
        double Ir1 = expectedTr(dTStart, dT1, spot, color);
        if (Qt0 == Qt1 || !FP::defined(Ir0) || !FP::defined(Ir1) || Ir0 <= 0.0 || Ir1 <= 0.0)
            return FP::undefined();
        return (area / (Qt1 - Qt0)) * log(Ir0 / Ir1);
    }
};

class TestControl : public ModelInstrumentControl<ModelInstrument> {
public:
    double firstDataRecord;

    TestControl(ModelInstrument &instrument, AcquisitionInterface *interface)
            : ModelInstrumentControl<ModelInstrument>(instrument, interface),
              firstDataRecord(FP::undefined())
    { }

protected:
    virtual void executeAdvance(double seconds, ModelInstrument &instrument)
    {
        if (instrument.advance(seconds)) {
            if (!FP::defined(firstDataRecord))
                firstDataRecord = instrument.accumulatedTime;
        }
    }
};

class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Ff", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Fn", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Q", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Qt", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("IrB", Variant::Root(467.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("IrG", Variant::Root(528.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("IrR", Variant::Root(652.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BaB", Variant::Root(467.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BaG", Variant::Root(528.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("BaR", Variant::Root(652.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("IfB", Variant::Root(467.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("IfG", Variant::Root(528.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("IfR", Variant::Root(652.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("IpB", Variant::Root(467.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("IpG", Variant::Root(528.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("IpR", Variant::Root(652.0), "^Wavelength", time))
            return false;
        if (!stream.hasMeta("ZSPOT", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZWHITE", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZFILTER", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("VQ", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZF1", Variant::Root(), QString(), time))
            return false;

        for (int i = 0; i < 10; i++) {
            if (!stream.hasMeta("I" + std::to_string(i + 1) + "D", Variant::Root(), QString(),
                                time))
                return false;
            if (!stream.hasMeta("I" + std::to_string(i + 1) + "B", Variant::Root(467.0),
                                "^Wavelength", time))
                return false;
            if (!stream.hasMeta("I" + std::to_string(i + 1) + "G", Variant::Root(528.0),
                                "^Wavelength", time))
                return false;
            if (!stream.hasMeta("I" + std::to_string(i + 1) + "R", Variant::Root(652.0),
                                "^Wavelength", time))
                return false;

            if (!stream.hasMeta("Ig" + std::to_string(i + 1) + "D", Variant::Root(), QString(),
                                time))
                return false;
            if (!stream.hasMeta("Ig" + std::to_string(i + 1) + "B", Variant::Root(467.0),
                                "^Wavelength", time))
                return false;
            if (!stream.hasMeta("Ig" + std::to_string(i + 1) + "G", Variant::Root(528.0),
                                "^Wavelength", time))
                return false;
            if (!stream.hasMeta("Ig" + std::to_string(i + 1) + "R", Variant::Root(652.0),
                                "^Wavelength", time))
                return false;
        }
        for (int i = 0; i < 8; i++) {
            if (!stream.hasMeta("In" + std::to_string(i + 1) + "B", Variant::Root(467.0),
                                "^Wavelength", time))
                return false;
            if (!stream.hasMeta("In" + std::to_string(i + 1) + "G", Variant::Root(528.0),
                                "^Wavelength", time))
                return false;
            if (!stream.hasMeta("In" + std::to_string(i + 1) + "R", Variant::Root(652.0),
                                "^Wavelength", time))
                return false;

            if (!stream.hasMeta("Ing" + std::to_string(i + 1) + "B", Variant::Root(467.0),
                                "^Wavelength", time))
                return false;
            if (!stream.hasMeta("Ing" + std::to_string(i + 1) + "G", Variant::Root(528.0),
                                "^Wavelength", time))
                return false;
            if (!stream.hasMeta("Ing" + std::to_string(i + 1) + "R", Variant::Root(652.0),
                                "^Wavelength", time))
                return false;

            if (!stream.hasMeta("Irw" + std::to_string(i + 1) + "B", Variant::Root(467.0),
                                "^Wavelength", time))
                return false;
            if (!stream.hasMeta("Irw" + std::to_string(i + 1) + "G", Variant::Root(528.0),
                                "^Wavelength", time))
                return false;
            if (!stream.hasMeta("Irw" + std::to_string(i + 1) + "R", Variant::Root(652.0),
                                "^Wavelength", time))
                return false;

            if (!stream.hasMeta("Irz" + std::to_string(i + 1) + "B", Variant::Root(467.0),
                                "^Wavelength", time))
                return false;
            if (!stream.hasMeta("Irz" + std::to_string(i + 1) + "G", Variant::Root(528.0),
                                "^Wavelength", time))
                return false;
            if (!stream.hasMeta("Irz" + std::to_string(i + 1) + "R", Variant::Root(652.0),
                                "^Wavelength", time))
                return false;
        }

        return true;
    }

    bool checkValues(StreamCapture &stream,
                     ModelInstrument &instrument,
                     double dT0,
                     double dT1,
                     double calQ = 1.0,
                     bool isRealtime = false,
                     double dTNormalize = 1.0,
                     double endTime = -1.0)
    {
        if (endTime < 0.0)
            endTime = instrument.accumulatedTime;

        QList<double> times;
        QList<double> timesBa;
        QList<double> Q;
        QList<double> Qt;
        QList<double> L;
        QList<double> T1;
        QList<double> T2;
        QList<double> IrB;
        QList<double> IrG;
        QList<double> IrR;
        QList<double> BaB;
        QList<double> BaG;
        QList<double> BaR;
        QList<double> IfB;
        QList<double> IfG;
        QList<double> IfR;
        QList<double> IpB;
        QList<double> IpG;
        QList<double> IpR;
        for (double dT = dT0; dT <= dT1; dT += 1.0) {
            if (!isRealtime)
                times.append(dT - 1.0);
            else
                times.append(dT);

            double edT = dT - endTime;

            Q.append(instrument.flow * calQ);
            Qt.append(instrument.expectedQt(edT, calQ));
            L.append(instrument.expectedL(edT, calQ));
            T1.append(instrument.T);
            T2.append(instrument.T_case);
            IrB.append(instrument.expectedTr(dTNormalize - endTime, edT, instrument.spotNumber - 1,
                                             0));
            IrG.append(instrument.expectedTr(dTNormalize - endTime, edT, instrument.spotNumber - 1,
                                             1));
            IrR.append(instrument.expectedTr(dTNormalize - endTime, edT, instrument.spotNumber - 1,
                                             2));
            if (dT > dTNormalize) {
                if (!isRealtime)
                    timesBa.append(dT - 1.0);
                else
                    timesBa.append(dT);

                BaB.append(instrument.expectedBa(dTNormalize - endTime, edT - 1.0, edT,
                                                 instrument.spotNumber - 1, 0, calQ));
                BaG.append(instrument.expectedBa(dTNormalize - endTime, edT - 1.0, edT,
                                                 instrument.spotNumber - 1, 1, calQ));
                BaR.append(instrument.expectedBa(dTNormalize - endTime, edT - 1.0, edT,
                                                 instrument.spotNumber - 1, 2, calQ));
            }
            IfB.append(instrument.expectedIRef(edT, instrument.spotNumber - 1, 0));
            IfG.append(instrument.expectedIRef(edT, instrument.spotNumber - 1, 1));
            IfR.append(instrument.expectedIRef(edT, instrument.spotNumber - 1, 2));
            IpB.append(instrument.expectedISam(edT, instrument.spotNumber - 1, 0));
            IpG.append(instrument.expectedISam(edT, instrument.spotNumber - 1, 1));
            IpR.append(instrument.expectedISam(edT, instrument.spotNumber - 1, 2));
        }

        if (!stream.verifyValues("Q", times, Q, !isRealtime))
            return false;
        if (!stream.verifyValues("Qt", times, Qt, !isRealtime))
            return false;
        if (!stream.verifyValues("L", times, L, !isRealtime))
            return false;
        if (!stream.verifyValues("T1", times, T1, !isRealtime))
            return false;
        if (!stream.verifyValues("T2", times, T2, !isRealtime))
            return false;
        if (!stream.verifyValues("IfB", times, IfB, !isRealtime))
            return false;
        if (!stream.verifyValues("IfG", times, IfG, !isRealtime))
            return false;
        if (!stream.verifyValues("IfR", times, IfR, !isRealtime))
            return false;
        if (!stream.verifyValues("IpB", times, IpB, !isRealtime))
            return false;
        if (!stream.verifyValues("IpG", times, IpG, !isRealtime))
            return false;
        if (!stream.verifyValues("IpR", times, IpR, !isRealtime))
            return false;
        if (!stream.verifyValues("IrB", times, IrB, !isRealtime))
            return false;
        if (!stream.verifyValues("IrG", times, IrG, !isRealtime))
            return false;
        if (!stream.verifyValues("IrR", times, IrR, !isRealtime))
            return false;
        if (!stream.verifyValues("BaB", timesBa, BaB, !isRealtime))
            return false;
        if (!stream.verifyValues("BaG", timesBa, BaG, !isRealtime))
            return false;
        if (!stream.verifyValues("BaR", timesBa, BaR, !isRealtime))
            return false;

        if (!isRealtime)
            return true;

        for (int detector = 0; detector < 10; detector++) {
            QList<double> ID;
            QList<double> IB;
            QList<double> IG;
            QList<double> IR;
            for (double dT = dT0; dT <= dT1; dT += 1.0) {
                double edT = dT - endTime;

                ID.append(instrument.expectedID(edT, detector));
                IB.append(instrument.expectedI(edT, detector, 0));
                IG.append(instrument.expectedI(edT, detector, 1));
                IR.append(instrument.expectedI(edT, detector, 2));
            }

            if (!stream.verifyValues("I" + std::to_string(detector + 1) + "D", times, ID,
                                     !isRealtime))
                return false;
            if (!stream.verifyValues("I" + std::to_string(detector + 1) + "B", times, IB,
                                     !isRealtime))
                return false;
            if (!stream.verifyValues("I" + std::to_string(detector + 1) + "G", times, IG,
                                     !isRealtime))
                return false;
            if (!stream.verifyValues("I" + std::to_string(detector + 1) + "R", times, IR,
                                     !isRealtime))
                return false;
        }

        for (int spot = 0; spot < 8; spot++) {
            QList<double> InB;
            QList<double> InG;
            QList<double> InR;
            for (double dT = dT0; dT <= dT1; dT += 1.0) {
                double edT = dT - endTime;

                InB.append(instrument.expectedIn(edT, spot, 0));
                InG.append(instrument.expectedIn(edT, spot, 1));
                InR.append(instrument.expectedIn(edT, spot, 2));
            }

            if (!stream.verifyValues("In" + std::to_string(spot + 1) + "B", times, InB,
                                     !isRealtime))
                return false;
            if (!stream.verifyValues("In" + std::to_string(spot + 1) + "G", times, InG,
                                     !isRealtime))
                return false;
            if (!stream.verifyValues("In" + std::to_string(spot + 1) + "R", times, InR,
                                     !isRealtime))
                return false;
        }

        return true;
    }

    bool checkState(StreamCapture &stream,
                    ModelInstrument &instrument,
                    double time = FP::undefined())
    {
        if (!stream.hasValue("Fn", Variant::Root(instrument.spotNumber), QString(), time))
            return false;
        if (!stream.hasValue("Ff", Variant::Root(instrument.filterID), QString(), time))
            return false;
        if (!stream.hasValue("ZSTATE", Variant::Root(), QString(), time))
            return false;

        return true;
    }

    bool checkPersistent(const SequenceValue::Transfer &values, ModelInstrument &instrument)
    {
        if (!StreamCapture::findValue(values, "raw", "Fn", Variant::Root(instrument.spotNumber)))
            return false;
        if (!StreamCapture::findValue(values, "raw", "Ff", Variant::Root(instrument.filterID)))
            return false;
        return true;
    }

    bool checkSpot(const SequenceValue::Transfer &values, double time, double area = 19.9)
    {
        if (!StreamCapture::findValue(values, "raw", "ZSPOT", Variant::Root(area), "Area", time))
            return false;
        return true;
    }

private slots:

    void initTestCase()
    {
        component =
                qobject_cast<AcquisitionComponent *>(ComponentLoader::create("acquire_gmd_clap3w"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_gmd_clap3w"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("area0")));
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("area1")));
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("area2")));
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("area3")));
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("area4")));
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("area5")));
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("area6")));
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("area7")));
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("cal-q")));
        QVERIFY(qobject_cast<BaselineSmootherOption *>(options.get("filter-baseline")));
        QVERIFY(qobject_cast<BaselineSmootherOption *>(options.get("spot-normalize")));
    }

    void passiveAutoprobe()
    {
        static const double calQ = 1.1;
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["FlowScale"].setDouble(calQ);
        cv["StrictMode"].setBool(true);
        cv["Filter/Baseline/Type"].setString("SinglePoint");
        cv["Spot/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        TestControl control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 10; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 10; i++) {
            control.advance(1.0);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, 3.0, 10.0, calQ, false));
        QVERIFY(checkValues(realtime, instrument, 3.0, 10.0, calQ, true));
        QVERIFY(checkState(realtime, instrument));

        QVERIFY(checkPersistent(realtime.values(), instrument));
        QVERIFY(checkPersistent(persistentValues, instrument));
        QVERIFY(checkSpot(persistentValues, 1.0));

    }

    void interactiveAutoprobe()
    {
        static const double calQ = 1.1;
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["FlowScale"].setDouble(calQ);
        cv["Filter/Baseline/Type"].setString("SinglePoint");
        cv["Spot/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = FP::undefined();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        TestControl control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.125);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        double loggingStart = instrument.accumulatedTime;
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, loggingStart + 1.0, instrument.accumulatedTime,
                            calQ, false, control.firstDataRecord));
        QVERIFY(checkValues(realtime, instrument, loggingStart + 1.0, instrument.accumulatedTime,
                            calQ, true, control.firstDataRecord));
        QVERIFY(checkState(realtime, instrument));

        QVERIFY(checkPersistent(realtime.values(), instrument));
        QVERIFY(checkPersistent(persistentValues, instrument));
        QVERIFY(checkSpot(persistentValues, control.firstDataRecord));

    }

    void passiveAcquisition()
    {
        static const double calQ = 1.2;
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["FlowScale"].setDouble(calQ);
        cv["Filter/Baseline/Type"].setString("SinglePoint");
        cv["Spot/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        cv["Filter/CheckInstrumentID"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        TestControl control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, 2.0, 10.0, calQ, false));
        QVERIFY(checkValues(realtime, instrument, 2.0, 10.0, calQ, true));
        QVERIFY(checkState(realtime, instrument));

        QVERIFY(checkPersistent(realtime.values(), instrument));
        QVERIFY(checkPersistent(persistentValues, instrument));
        QVERIFY(checkSpot(persistentValues, 1.0));

    }

    void passiveAcquisitionOptions()
    {
        static const double calQ = 0.9;
        ComponentOptions options(component->getPassiveOptions());
        qobject_cast<ComponentOptionSingleDouble *>(options.get("cal-q"))->set(calQ);
        qobject_cast<BaselineSmootherOption *>(options.get("filter-baseline"))->overlay(
                new BaselineSinglePoint, FP::undefined(), FP::undefined());
        qobject_cast<BaselineSmootherOption *>(options.get("spot-normalize"))->overlay(
                new BaselineSinglePoint, FP::undefined(), FP::undefined());

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(options));
        QVERIFY(interface.get());
        TestControl control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, 2.0, 10.0, calQ, false));
        QVERIFY(checkValues(realtime, instrument, 2.0, 10.0, calQ, true));
        QVERIFY(checkState(realtime, instrument));

        QVERIFY(checkPersistent(realtime.values(), instrument));
        QVERIFY(checkPersistent(persistentValues, instrument));
        QVERIFY(checkSpot(persistentValues, 1.0));

    }

    void interactiveAcquisition()
    {
        static const double calQ = 1.15;
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["FlowScale"].setDouble(calQ);
        cv["Filter/Baseline/Type"].setString("SinglePoint");
        cv["Spot/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = FP::undefined();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        TestControl control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkPersistent(realtime.values(), instrument));
        QVERIFY(checkPersistent(persistentValues, instrument));
        QVERIFY(checkSpot(persistentValues, control.firstDataRecord));

    }

    void instrumentFilterChange()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Filter/Baseline/Type"].setString("SinglePoint");
        cv["Spot/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        cv["Filter/CheckInstrumentID"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = FP::undefined();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        TestControl control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        control.advance(1.0);

        instrument.changing = true;
        instrument.spotNumber = 0;
        while (control.time() < 120.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("FilterChange")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);
        control.advance(1.0);

        double filterChangeEndTime = control.time();
        instrument.changing = false;
        instrument.filterID++;
        while (control.time() < 180.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), filterChangeEndTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 180.0);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));
        QVERIFY(logging.hasAnyMatchingValue("F1", Variant::Root(
                Variant::Flags{"STP", "FilterChanging"})));
        QVERIFY(logging.hasAnyMatchingValue("F1", Variant::Root(Variant::Flags{"STP"})));

        QVERIFY(checkPersistent(persistentValues, instrument));
        QVERIFY(persistent.hasValue("Fn"));
        QVERIFY(persistent.hasValue("Ff"));
        QVERIFY(StreamCapture::findValue(persistentValues, "raw", "ZFILTER", Variant::Root()));

    }

    void autodetectFilterChange()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Filter/Baseline/Type"].setString("FixedTime");
        cv["Filter/Baseline/Time"].setDouble(5.0);
        cv["Filter/Baseline/RSD"].setDouble(0.01);
        cv["Autodetect/Start/Reference/Type"].setString("FixedTime");
        cv["Autodetect/Start/Reference/Time"].setDouble(5.0);
        cv["Autodetect/Start/Reference/RSD"].setDouble(0.01);
        cv["Spot/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(true);
        cv["Autodetect/Start/Enable"].setBool(true);
        cv["Autodetect/Start/RequiredTriggers"].setInteger(1);
        cv["Autodetect/End/Enable"].setBool(true);
        cv["Filter/CheckInstrumentID"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        for (int detector = 0; detector < 40; detector++) {
            instrument.deltaI[detector] = 0;
        }
        instrument.unpolledRemaining = FP::undefined();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        TestControl control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 1);
        QVERIFY(!instrument.changing);

        for (double dTE = control.time() + 6.0; control.time() <= dTE;) {
            control.advance(0.125);
        }
        instrument.intensities[0] = 20000;
        while (control.time() < 120.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("FilterChange")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 0);
        QVERIFY(instrument.changing);

        double filterChangeEndTime = control.time();
        instrument.intensities[0] = -200;
        while (control.time() < 180.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), filterChangeEndTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 180.0);
        QCOMPARE(instrument.spotNumber, 1);
        QVERIFY(!instrument.changing);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkPersistent(persistentValues, instrument));
        QVERIFY(persistent.hasValue("Fn"));
        QVERIFY(persistent.hasValue("Ff"));
        QVERIFY(StreamCapture::findValue(persistentValues, "raw", "ZFILTER", Variant::Root()));

    }

    void commandIssue()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Filter/Baseline/Type"].setString("SinglePoint");
        cv["Spot/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        cv["Filter/CheckInstrumentID"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = FP::undefined();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        TestControl control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        control.advance(0.125);

        Variant::Write cmd = Variant::Write::empty();
        cmd["StartFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 120.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("FilterChange")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 0);
        QVERIFY(instrument.changing);

        double checkTime1 = control.time();
        cmd.setEmpty();
        cmd["Bypass"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 180.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("BypassedFilterChange")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 180.0);
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 0);
        QVERIFY(instrument.changing);

        double checkTime2 = control.time();
        cmd.setEmpty();
        cmd["UnBypass"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 240.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("FilterChange"), checkTime1))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 0);
        QVERIFY(instrument.changing);

        cmd.setEmpty();
        cmd["StopFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 300.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime2))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 300.0);
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 1);
        QVERIFY(!instrument.changing);

        checkTime1 = control.time();
        cmd.setEmpty();
        cmd["Bypass"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 360.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("BypassedRun"), checkTime2))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 360.0);
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 0);
        QVERIFY(!instrument.changing);

        checkTime2 = control.time();
        cmd.setEmpty();
        cmd["StartFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 420.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("BypassedFilterChange"),
                                             checkTime1))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 420.0);
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 0);
        QVERIFY(instrument.changing);

        checkTime1 = control.time();
        cmd.setEmpty();
        cmd["UnBypass"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 480.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("FilterChange"), checkTime2))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 480.0);
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 0);
        QVERIFY(instrument.changing);

        cmd.setEmpty();
        cmd["StartWhiteFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 540.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("WhiteFilterChange")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 540.0);
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 0);
        QVERIFY(instrument.changing);

        checkTime2 = control.time();
        cmd.setEmpty();
        cmd["StopFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 600.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime1))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 600.0);
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 1);
        QVERIFY(!instrument.changing);

        checkTime1 = control.time();
        cmd.setEmpty();
        cmd["Bypass"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 660.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("BypassedRun"), checkTime2))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 660.0);
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 0);
        QVERIFY(!instrument.changing);

        checkTime2 = control.time();
        cmd.setEmpty();
        cmd["StartWhiteFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 720.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("BypassedWhiteFilterChange"),
                                             checkTime1))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 720.0);
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 0);
        QVERIFY(instrument.changing);

        checkTime1 = control.time();
        cmd.setEmpty();
        cmd["StopFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 780.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("BypassedNormalize"),
                                             checkTime2))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 780.0);
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 0);
        QVERIFY(!instrument.changing);

        checkTime2 = control.time();
        cmd.setEmpty();
        cmd["UnBypass"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 840.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime1))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 840.0);
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 1);
        QVERIFY(!instrument.changing);

        checkTime1 = control.time();
        cmd.setEmpty();
        cmd["AdvanceSpot"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 900.0) {
            control.advance(0.125);
            if (instrument.spotNumber == 2 &&
                    realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime2))
                break;
            QTest::qSleep(50);
        }
        while (control.time() < 900.0) {
            if (instrument.spotNumber == 2 &&
                    realtime.hasAnyMatchingValue("Fn", Variant::Root(2), checkTime2))
                break;
            QTest::qSleep(50);
            control.advance(0.125);
        }
        QVERIFY(control.time() < 900.0);
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 2);
        QVERIFY(!instrument.changing);

        checkTime2 = control.time();
        cmd.setEmpty();
        cmd["Bypass"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 960.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("BypassedRun"), checkTime1))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 960.0);
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 0);
        QVERIFY(!instrument.changing);

        checkTime1 = control.time();
        cmd.setEmpty();
        cmd["UnBypass"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 1220.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime2))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 1220.0);
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 2);
        QVERIFY(!instrument.changing);

        checkTime2 = control.time();
        cmd.setEmpty();
        cmd["Bypass"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 1280.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("BypassedRun"), checkTime1))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 1280.0);
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 0);
        QVERIFY(!instrument.changing);

        checkTime1 = control.time();
        cmd.setEmpty();
        cmd["AdvanceSpot"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 1340.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("BypassedNormalize"),
                                             checkTime2))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 1340.0);
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 0);
        QVERIFY(!instrument.changing);

        cmd.setEmpty();
        cmd["UnBypass"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 1500.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime1))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 1500.0);
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 3);
        QVERIFY(!instrument.changing);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkPersistent(persistentValues, instrument));

    }

    void stateSaveRestore()
    {
        static const double calQ = 1.2;
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["FlowScale"].setDouble(calQ);
        cv["Filter/Baseline/Type"].setString("SinglePoint");
        cv["Spot/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        cv["Filter/CheckInstrumentID"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        TestControl control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);

        ElapsedTimer timeoutCheck;
        timeoutCheck.start();
        for (double tCheck = realtime.latestTime();
                timeoutCheck.elapsed() < 30000 && (!FP::defined(tCheck) || tCheck < 4.0);
                tCheck = realtime.latestTime()) {
            QTest::qSleep(50);
        }

        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                interface->serializeState(stream);
            }
            interface->initiateShutdown();
            interface->wait();
            interface = component->createAcquisitionPassive(config);
            control.attach(interface.get());
            interface->start();
            interface->setLoggingEgress(&logging);
            interface->setRealtimeEgress(&realtime);
            interface->setPersistentEgress(&persistent);
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                interface->deserializeState(stream);
            }
        }

        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);
        control.advance(1.0);

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, 2.0, 4.0, calQ, false));
        QVERIFY(checkValues(logging, instrument, 6.0, 10.0, calQ, false));
        QVERIFY(checkValues(realtime, instrument, 2.0, 4.0, calQ, true));
        QVERIFY(checkValues(realtime, instrument, 6.0, 10.0, calQ, true));
        QVERIFY(checkState(realtime, instrument));

        QVERIFY(checkPersistent(realtime.values(), instrument));
        QVERIFY(checkPersistent(persistentValues, instrument));
        QVERIFY(checkSpot(persistentValues, 1.0));

    }

    void resumeLogic()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Filter/Baseline/Type"].setString("SinglePoint");
        cv["Spot/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        cv["Filter/CheckInstrumentID"].setBool(true);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        for (int detector = 0; detector < 40; detector++) {
            instrument.deltaI[detector] = 0;
        }
        instrument.unpolledRemaining = FP::undefined();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        TestControl control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();
        interface->initializeState();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("RequireWhiteFilterChange")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        double checkTime = control.time();
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 0);
        QVERIFY(!instrument.changing);


        Variant::Write cmd = Variant::Write::empty();
        cmd["StartWhiteFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 120.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("WhiteFilterChange"),
                                             checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);
        checkTime = control.time();
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 0);
        QVERIFY(instrument.changing);

        cmd.setEmpty();
        cmd["StopFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 180.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 180.0);
        checkTime = control.time();
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 1);
        QVERIFY(!instrument.changing);

        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                interface->serializeState(stream);
            }
            interface->initiateShutdown();
            interface->wait();
            interface = component->createAcquisitionInteractive(config);
            control.attach(interface.get());
            interface->setState(&control);
            interface->setControlStream(&control);
            interface->start();
            interface->setLoggingEgress(&logging);
            interface->setRealtimeEgress(&realtime);
            interface->setPersistentEgress(&persistent);
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                interface->deserializeState(stream);
            }
        }

        while (control.time() < 240.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);
        checkTime = control.time();
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 1);
        QVERIFY(!instrument.changing);

        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                interface->serializeState(stream);
            }
            interface->initiateShutdown();
            interface->wait();
            interface = component->createAcquisitionInteractive(config);
            control.attach(interface.get());
            interface->setState(&control);
            interface->setControlStream(&control);
            interface->start();
            interface->setLoggingEgress(&logging);
            interface->setRealtimeEgress(&realtime);
            interface->setPersistentEgress(&persistent);
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                interface->deserializeState(stream);
            }
        }

        instrument.spotNumber = 0;
        while (control.time() < 300.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 300.0);
        checkTime = control.time();
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 1);
        QVERIFY(!instrument.changing);

        for (int switchSpot = 2; switchSpot <= 8; switchSpot++) {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                interface->serializeState(stream);
            }
            interface->initiateShutdown();
            interface->wait();
            interface = component->createAcquisitionInteractive(config);
            control.attach(interface.get());
            interface->setState(&control);
            interface->setControlStream(&control);
            interface->start();
            interface->setLoggingEgress(&logging);
            interface->setRealtimeEgress(&realtime);
            interface->setPersistentEgress(&persistent);
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                interface->deserializeState(stream);
            }

            instrument.filterID++;
            while (control.time() < 600.0) {
                control.advance(0.125);
                if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime))
                    break;
                QTest::qSleep(50);
            }
            QVERIFY(control.time() < 600.0);
            checkTime = control.time() + 0.1;
            control.advance(0.125);
            QCOMPARE(instrument.spotNumber, switchSpot);
            QVERIFY(!instrument.changing);
        }

        {
            QByteArray data;
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                interface->serializeState(stream);
            }
            interface->initiateShutdown();
            interface->wait();
            interface = component->createAcquisitionInteractive(config);
            control.attach(interface.get());
            interface->setState(&control);
            interface->setControlStream(&control);
            interface->start();
            interface->setLoggingEgress(&logging);
            interface->setRealtimeEgress(&realtime);
            interface->setPersistentEgress(&persistent);
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                interface->deserializeState(stream);
            }
        }

        instrument.filterID++;
        while (control.time() < 660.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("RequireFilterChange"),
                                             checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 660.0);
        checkTime = control.time();
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 0);
        QVERIFY(!instrument.changing);

        cmd.setEmpty();
        cmd["StartFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 720.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("FilterChange"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 720.0);
        checkTime = control.time();
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 0);
        QVERIFY(instrument.changing);

        cmd.setEmpty();
        cmd["StopFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 780.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 780.0);
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 1);
        QVERIFY(!instrument.changing);

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkPersistent(persistentValues, instrument));
        QVERIFY(persistent.hasValue("Fn"));
        QVERIFY(persistent.hasValue("Ff"));

    }

    void resumeDelayed()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Filter/Baseline/Type"].setString("SinglePoint");
        cv["Spot/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        for (int detector = 0; detector < 40; detector++) {
            instrument.deltaI[detector] = 0;
        }
        instrument.unpolledRemaining = FP::undefined();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        TestControl control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();
        interface->initializeState();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("RequireWhiteFilterChange")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        double checkTime = control.time();
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 0);
        QVERIFY(!instrument.changing);


        Variant::Write cmd = Variant::Write::empty();
        cmd["StartWhiteFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 120.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("WhiteFilterChange"),
                                             checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);
        checkTime = control.time();
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 0);
        QVERIFY(instrument.changing);

        cmd.setEmpty();
        cmd["StopFilterChange"].setBool(true);
        interface->incomingCommand(cmd);
        while (control.time() < 180.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 180.0);
        checkTime = control.time();
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 1);
        QVERIFY(!instrument.changing);

        QByteArray data;
        {
            {
                QDataStream stream(&data, QIODevice::WriteOnly);
                interface->serializeState(stream);
            }
            interface->initiateShutdown();
            interface->wait();
            interface = component->createAcquisitionInteractive(config);
            control.attach(interface.get());
            interface->setState(&control);
            interface->setControlStream(&control);
            interface->setLoggingEgress(&logging);
            interface->setRealtimeEgress(&realtime);
            interface->setPersistentEgress(&persistent);
            interface->start();
        }

        while (control.time() < 240.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);
        checkTime = control.time();
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 1);
        QVERIFY(!instrument.changing);

        {
            {
                QDataStream stream(&data, QIODevice::ReadOnly);
                interface->deserializeState(stream);
            }
        }

        while (control.time() < 300.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 300.0);
        control.advance(0.125);
        QCOMPARE(instrument.spotNumber, 1);
        QVERIFY(!instrument.changing);


        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkPersistent(persistentValues, instrument));
        QVERIFY(persistent.hasValue("Fn"));
        QVERIFY(persistent.hasValue("Ff"));

    }

    void filterChangingSingleGap()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["StrictMode"].setBool(true);
        cv["Filter/Baseline/Type"].setString("SinglePoint");
        cv["Spot/Normalize/Type"].setString("SinglePoint");
        cv["Autodetect/Start/Enable"].setBool(false);
        cv["Autodetect/End/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        TestControl control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 10; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 10; i++) {
            control.advance(1.0);
        }
        instrument.changing = true;
        instrument.spotNumber = 0;
        for (int i = 0; i < 20; i++) {
            control.advance(1.0);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        int gapCount = 0;
        double prior = FP::undefined();
        for (const auto &v : logging.values()) {
            if (v.getVariable() != "L")
                continue;
            if (FP::defined(prior) && v.getStart() != prior)
                gapCount++;
            prior = v.getEnd();
        }
        QCOMPARE(gapCount, 1);

        gapCount = 0;
        for (const auto &v : realtime.values()) {
            if (v.getVariable() != "L")
                continue;
            if (!FP::defined(v.getValue().toDouble()))
                gapCount++;
        }
        QVERIFY(gapCount > 0);

    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
