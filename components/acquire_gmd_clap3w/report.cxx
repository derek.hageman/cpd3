/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include "core/qtcompat.hxx"

#include "acquire_gmd_clap3w.hxx"

using namespace CPD3;
using namespace CPD3::Data;

/* Instrument generated codes */
enum {
    FLAG_CHANGING = 0x0001,
    FLOW_ERROR = 0x0002,
    LAMP_ERROR = 0x0100,
    TEMPERATURE_OUT_OF_RANGE = 0x0200,
    CASE_TEMPERATURE_UNSTABLE = 0x0400,
};

static const char *instrumentFlagTranslation[16] = {"FilterChanging",           /* 0x0001 */
                                                    "FlowError",                /* 0x0002 */
                                                    NULL,                       /* 0x0004 */
                                                    NULL,                       /* 0x0008 */
                                                    NULL,                       /* 0x0010 */
                                                    NULL,                       /* 0x0020 */
                                                    NULL,                       /* 0x0040 */
                                                    NULL,                       /* 0x0080 */
                                                    "LampError",                /* 0x0100 */
                                                    "TemperatureOutOfRange",    /* 0x0200 */
                                                    "CaseTemperatureUnstable",  /* 0x0400 */
                                                    NULL,                       /* 0x0800 */
                                                    NULL,                       /* 0x1000 */
                                                    NULL,                       /* 0x2000 */
                                                    NULL,                       /* 0x4000 */
                                                    NULL,                       /* 0x8000 */
};

static const double instrumentWaitAcknowledgeTime = 5.0;

static double calculateIn(double Iref, double IDref, double Isam, double IDsam)
{
    if (!FP::defined(Iref) || !FP::defined(IDref) || !FP::defined(Isam) || !FP::defined(IDsam))
        return FP::undefined();
    Iref -= IDref;
    if (Iref == 0.0)
        return FP::undefined();
    Isam -= IDsam;
    return Isam / Iref;
}

static double calculateIfp(double I, double ID)
{
    if (!FP::defined(I) || !FP::defined(ID))
        return FP::undefined();
    return I - ID;
}

static double calculateIr(double In0, double In)
{
    if (!FP::defined(In0) || !FP::defined(In))
        return FP::undefined();
    if (In0 == 0.0)
        return FP::undefined();
    return In / In0;
}

static double calculateL(double Qt, double area)
{
    if (!FP::defined(Qt) || !FP::defined(area) || area <= 0.0 || Qt <= 0.0)
        return FP::undefined();
    return Qt / (area * 1E-6);
}

static double calculateBa(double Ir0, double Qt0, double Ir1, double Qt1, double area)
{
    if (!FP::defined(Ir0) ||
            !FP::defined(Qt0) ||
            !FP::defined(Ir1) ||
            !FP::defined(Qt1) ||
            !FP::defined(area))
        return FP::undefined();
    if (Ir0 <= 0.0 || Ir1 <= 0.0)
        return FP::undefined();
    double dQt = Qt1 - Qt0;
    if (dQt <= 0.0)
        return FP::undefined();
    return log(Ir0 / Ir1) * (area / dQt);
}

Variant::Root AcquireGMDCLAP3W::buildNormalizationValue() const
{
    Variant::Root result;

    result["InB"].setDouble(spotNormalization[0]);
    result["InG"].setDouble(spotNormalization[1]);
    result["InR"].setDouble(spotNormalization[2]);
    result["InBLatest"].setDouble(priorIn[0]);
    result["InGLatest"].setDouble(priorIn[1]);
    result["InRLatest"].setDouble(priorIn[2]);
    result["IrB"].setDouble(priorIr[0]);
    result["IrG"].setDouble(priorIr[1]);
    result["IrR"].setDouble(priorIr[2]);
    result["Ff"].setInt64(normalizationFilterID);
    result["Fn"].setInt64(normalizationSpot);
    result["ETIME"].setInt64(normalizationLastInstrumentTime);
    result["Qt"].setDouble(normalizationLastVolume);
    result["SerialNumber"].set(normalizationSerialNumber);
    result["FirmwareVersion"].set(normalizationFirmwareVersion);
    if (!config.isEmpty() && normalizationSpot > 0 && normalizationSpot < 9) {
        result["L"].setDouble(
                calculateL(normalizationLastVolume, config.first().area[normalizationSpot - 1]));
        result["Area"].setDouble(config.first().area[normalizationSpot - 1]);
    }
    result["WasWhite"].setBool(!filterIsNotWhite);

    return result;
}

/* Use a macro so we don't have to pass a huge number of variables for a
 * purely diagnostic function */
#define insertDiagnosticCommonValues(target) do { \
    target.hash("Values").hash("RawFlags").set(flags); \
    target.hash("Values").hash("Flags").set(outputFlags); \
    target.hash("Values").hash("ETIME").set(etime); \
    target.hash("Values").hash("Ff").set(Ff); \
    target.hash("Values").hash("Fn").set(Fn); \
    target.hash("Values").hash("SpotNumber").setInt64(spotNumber); \
    target.hash("Values").hash("Q").set(Q); \
    target.hash("Values").hash("VQ").set(VQ); \
    target.hash("Values").hash("Qt").set(Qt); \
    target.hash("Values").hash("T1").set(T1); \
    target.hash("Values").hash("T2").set(T2); \
    for (int detector=0; detector<10; detector++) { \
        target.hash("Values").hash("ID").array(detector).set(ID[detector]); \
        target.hash("Values").hash("IB").array(detector).set(I[detector][0]); \
        target.hash("Values").hash("IG").array(detector).set(I[detector][1]); \
        target.hash("Values").hash("IR").array(detector).set(I[detector][2]); \
    } \
    for (int spot=0; spot<8; spot++) { \
        target.hash("Values").hash("InB").array(spot).set(In[spot][0]); \
        target.hash("Values").hash("InG").array(spot).set(In[spot][1]); \
        target.hash("Values").hash("InR").array(spot).set(In[spot][2]); \
    } \
    for (int spot=0; spot<8; spot++) { \
        target.hash("Values").hash("InB").array(spot).set(In[spot][0]); \
        target.hash("Values").hash("InG").array(spot).set(In[spot][1]); \
        target.hash("Values").hash("InR").array(spot).set(In[spot][2]); \
    } \
} while(0)

void AcquireGMDCLAP3W::insertDiagnosticFilterValues(Variant::Write &target,
                                                    const QString &name,
                                                    const IntensityData &source) const
{
    target.hash(name).hash("StartTime").setDouble(source.startTime);
    for (int detector = 0; detector < 10; detector++) {
        target.hash(name).hash("ID").array(detector).setDouble(source.ID[detector]);
        target.hash(name).hash("IB").array(detector).setDouble(source.I[detector][0]);
        target.hash(name).hash("IG").array(detector).setDouble(source.I[detector][1]);
        target.hash(name).hash("IR").array(detector).setDouble(source.I[detector][2]);
    }
    for (int spot = 0; spot < 8; spot++) {
        target.hash(name).hash("InB").array(spot).setDouble(source.In[spot][0]);
        target.hash(name).hash("InG").array(spot).setDouble(source.In[spot][1]);
        target.hash(name).hash("InR").array(spot).setDouble(source.In[spot][2]);
    }
}

void AcquireGMDCLAP3W::insertDiagnosticNormalization(Variant::Write &target) const
{
    target.hash("Normalization").hash("StartTime").setDouble(normalizationStartTime);
    target.hash("Normalization").hash("InB").setDouble(spotNormalization[0]);
    target.hash("Normalization").hash("InG").setDouble(spotNormalization[1]);
    target.hash("Normalization").hash("InR").setDouble(spotNormalization[2]);
    target.hash("Normalization").hash("Spot").setInt64(normalizationSpot);
    target.hash("Normalization").hash("FilterID").setInt64(normalizationFilterID);
    target.hash("Normalization").hash("Volume").setDouble(normalizationLastVolume);
    target.hash("Normalization").hash("InstrumentTime").setInt64(normalizationLastInstrumentTime);
}

void AcquireGMDCLAP3W::insertDiagnosticNormalizationStable(Variant::Write &target) const
{
    target.hash("NormalizationStability").hash("InB").set(spotNormalizationIn[0]->describeState());
    target.hash("NormalizationStability").hash("InG").set(spotNormalizationIn[1]->describeState());
    target.hash("NormalizationStability").hash("InR").set(spotNormalizationIn[2]->describeState());
}

void AcquireGMDCLAP3W::insertDiagnosticFilterBaseline(Variant::Write &target) const
{
    for (int detector = 0; detector < 10; detector++) {
        target.hash("Baseline")
              .hash("ID")
              .array(detector)
              .set(filterBaselineID[detector]->describeState());
        target.hash("Baseline")
              .hash("IB")
              .array(detector)
              .set(filterBaselineI[detector][0]->describeState());
        target.hash("Baseline")
              .hash("IG")
              .array(detector)
              .set(filterBaselineI[detector][1]->describeState());
        target.hash("Baseline")
              .hash("IR")
              .array(detector)
              .set(filterBaselineI[detector][2]->describeState());
    }
    for (int spot = 0; spot < 8; spot++) {
        target.hash("Baseline")
              .hash("InB")
              .array(spot)
              .set(filterBaselineIn[spot][0]->describeState());
        target.hash("Baseline")
              .hash("InG")
              .array(spot)
              .set(filterBaselineIn[spot][1]->describeState());
        target.hash("Baseline")
              .hash("InR")
              .array(spot)
              .set(filterBaselineIn[spot][2]->describeState());
    }
}

int AcquireGMDCLAP3W::processRecord(const Util::ByteView &line, double startTime, double endTime)
{
    Q_ASSERT(!config.isEmpty());

    auto fields = Util::as_deque(line.split(','));
    bool ok = false;

    auto field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.size() != 2) return 1;
    if (field[0] != '0') return 2;
    if (field != "03") return -1;

    if (fields.empty()) return 3;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root flags((qint64) field.parse_u16(&ok, 16));
    if (!ok) return 4;
    remap("FRAW", flags);

    if (fields.empty()) return 5;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root etime((qint64) field.parse_u32(&ok, 16));
    if (!ok) return 6;
    remap("ETIME", etime);

    if (fields.empty()) return 7;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Ff((qint64) field.parse_u32(&ok, 16));
    if (!ok) return 8;
    remap("Ff", Ff);

    if (fields.empty()) return 9;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Fn((qint64) field.parse_u16(&ok, 16));
    if (!ok) return 10;
    remap("Fn", Fn);

    if (fields.empty()) return 11;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Q(field.parse_real(&ok));
    if (!ok) return 12;
    if (!FP::defined(Q.read().toReal())) return 13;
    Variant::Root VQ(config.first()
                           .hardwareFlowCal
                           .inverse(Q.read().toDouble(), Calibration::BestInRange, 0.0, 5.0));
    remap("Q", Q);
    remap("VQ", VQ);

    if (fields.empty()) return 14;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Qt(field.parse_real(&ok));
    if (!ok) return 15;
    if (!FP::defined(Qt.read().toReal())) return 16;
    remap("Qt", Qt);

    if (fields.empty()) return 17;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T2(field.parse_real(&ok)); /* Case */
    if (!ok) return 18;
    if (!FP::defined(T2.read().toReal())) return 19;
    remap("T2", T2);

    if (fields.empty()) return 20;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T1(field.parse_real(&ok)); /* Sample */
    if (!ok) return 21;
    if (!FP::defined(T1.read().toReal())) return 22;
    remap("T1", T1);

    Variant::Root ID[10];
    /* B, G, R, but raw data is ordered R, G, B */
    Variant::Root I[10][3];
    for (int i = 0; i < 40; i++) {
        if (fields.empty()) return 100 + i;
        field = fields.front().string_trimmed();
        fields.pop_front();

        quint32 raw = field.parse_u32(&ok, 16);
        if (!ok) return 200 + i;
        /* Endian-ness shouldn't matter here since the conversion from hex
         * should put it in machine byte order.  This does assume that float
         * byte order is the same as machine, however. */
        /* raw = qFromLittleEndian<quint32>(raw); */
        /* Inf/NaN */
        if ((raw & 0x7F800000) == 0x7F800000) return 300 + i;
        float f;
        memcpy(&f, &raw, 4);

        int sensor = i / 4;
        switch (i % 4) {
        case 0:
            if (!FP::defined(f)) return 400 + i;
            ID[sensor].write().setDouble((double) f);
            remap("ID" + std::to_string(sensor + 1), ID[sensor]);
            break;
        case 1:
            if (!FP::defined(f)) return 400 + i;
            I[sensor][2].write().setDouble((double) f);
            remap("IR" + std::to_string(sensor + 1), I[sensor][2]);
            break;
        case 2:
            if (!FP::defined(f)) return 400 + i;
            I[sensor][1].write().setDouble((double) f);
            remap("IG" + std::to_string(sensor + 1), I[sensor][1]);
            break;
        case 3:
            if (!FP::defined(f)) return 400 + i;
            I[sensor][0].write().setDouble((double) f);
            remap("IB" + std::to_string(sensor + 1), I[sensor][0]);
            break;
        default:
            Q_ASSERT(false);
            break;
        }
    }

    if (config.first().strictMode) {
        if (!fields.empty())
            return 500;
        if (line.size() != 459)
            return 501;
    }

    if (!haveEmittedLogMeta && loggingEgress && FP::defined(startTime)) {
        haveEmittedLogMeta = true;
        loggingEgress->incomingData(buildLogMeta(startTime));
    }
    if (!haveEmittedRealtimeMeta && realtimeEgress) {
        haveEmittedRealtimeMeta = true;
        SequenceValue::Transfer meta = buildLogMeta(endTime);
        Util::append(buildRealtimeMeta(endTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }

    int spotNumber = 0;
    if (INTEGER::defined(Fn.read().toInt64()))
        spotNumber = Fn.read().toInt64();

    if (FP::defined(Q.read().toDouble())) {
        Q.write().setDouble(Q.read().toDouble() * config.first().flowScale);
    }
    if (spotNumber == 0) {
        Qt.write().setDouble(0);
        Q.write().setDouble(0);
    } else if (config.first().integrateVolume) {
        if (FP::defined(Q.read().toDouble()) &&
                FP::defined(startTime) &&
                FP::defined(endTime) &&
                endTime > startTime) {
            baseVolume += Q.read().toDouble() * (endTime - startTime) / 60000.0;
            Qt.write().setDouble(0);
        }
    }
    if (FP::defined(Qt.read().toDouble())) {
        Qt.write().setDouble(baseVolume + Qt.read().toDouble() * config.first().flowScale);
    }

    Variant::Root In[8][3];
    for (int i = 0; i < 8; i++) {
        int sample = i + 1;
        int reference = ((i % 2 == 0) ? 9 : 0);

        for (int color = 0; color < 3; color++) {
            In[i][color].write()
                        .setDouble(calculateIn(I[reference][color].read().toDouble(),
                                               ID[reference].read().toDouble(),
                                               I[sample][color].read().toDouble(),
                                               ID[sample].read().toDouble()));
        }

        remap("InB" + std::to_string(i + 1), In[i][0]);
        remap("InG" + std::to_string(i + 1), In[i][1]);
        remap("InR" + std::to_string(i + 1), In[i][2]);
    }

    for (int detector = 0; detector < 10; detector++) {
        for (int color = 0; color < 3; color++) {
            filterBaselineI[detector][color]->add(I[detector][color].read().toDouble(), startTime,
                                                  endTime);
        }
        filterBaselineID[detector]->add(ID[detector].read().toDouble(), startTime, endTime);
    }
    for (int spot = 0; spot < 8; spot++) {
        for (int color = 0; color < 3; color++) {
            filterBaselineIn[spot][color]->add(In[spot][color].read().toDouble(), startTime,
                                               endTime);
        }
    }

    /* Check sample state change */
    SampleState priorSampleState = sampleState;
    SampleState initialSampleState = sampleState;
    Variant::Root Ir[3];
    Variant::Root If[3];
    Variant::Root Ip[3];
    Variant::Root Ba[3];
    Variant::Root L;
    Variant::Root outputFlags(Variant::Type::Flags);
    outputFlags.write().applyFlag("STP"); /* Flow meter is already at STP */
    do {
        priorSampleState = sampleState;

        switch (sampleState) {
        case SAMPLE_RUN:
            if (INTEGER::defined(flags.read().toInt64()) &&
                    (flags.read().toInt64() & FLAG_CHANGING)) {
                qCDebug(log) << "Instrument initiated filter change at" << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("Run");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "Filter", filterStart);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                insertDiagnosticNormalization(info);
                event(endTime, QObject::tr("Instrument initiated filter change started."), true,
                      info);

                if (controlStream != NULL && spotNumber != 0) {
                    qCDebug(log) << "Instrument has filter change flag set but is not on spot zero";
                    controlStream->writeControl("spot=0\rstop\r");
                    discardData(endTime + 0.5, 1);
                    timeoutAt(endTime + 3.0);
                }
                emitFilterEnd(endTime);
                forceFilterBreak = true;
                sampleState = SAMPLE_FILTER_CHANGE_START;
                spotChangeCheck = spotNumber;
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
                break;
            }

            if (!haveAcceptedNormalization) {
                haveAcceptedNormalization = true;
                persistentValuesUpdated();
                if (!isSameInstrument(endTime)) {
                    havePriorInstrument = false;

                    Variant::Write info = Variant::Write::empty();
                    info.hash("State").setString("Run");
                    insertDiagnosticCommonValues(info);
                    insertDiagnosticFilterValues(info, "Filter", filterStart);
                    insertDiagnosticFilterValues(info, "White", filterWhite);
                    insertDiagnosticNormalization(info);

                    info.hash("NormalizationSerialNumber").set(normalizationSerialNumber);
                    info.hash("NormalizationFirmwareVersion").set(normalizationFirmwareVersion);
                    info.hash("PriorSerialNumber").set(priorInstrumentSerialNumber);
                    info.hash("PriorFirmwareVersion").set(priorInstrumentFirmwareVersion);
                    info.hash("InstrumentSerialNumber").set(instrumentMeta["SerialNumber"]);
                    info.hash("InstrumentFirmwareVersion").set(instrumentMeta["FirmwareVersion"]);

                    invalidateNormalization();
                    sampleState = SAMPLE_REQUIRE_WHITE_FILTER_CHANGE;
                    forceRealtimeStateEmit = true;
                    forceFilterBreak = true;

                    qCDebug(log) << "Instrument identifier (" << instrumentMeta["SerialNumber"]
                                 << ":" << instrumentMeta["FirmwareVersion"]
                                 << ") does not match the normalization ("
                                 << normalizationSerialNumber << ":" << normalizationFirmwareVersion
                                 << ") white filter change required";

                    event(endTime, QObject::tr(
                            "The CLAP instrument has changed.  A white filter change is required."),
                          true, info);
                    break;
                } else if (!isSameFilter(endTime)) {
                    Variant::Write info = Variant::Write::empty();
                    info.hash("State").setString("Run");
                    insertDiagnosticCommonValues(info);
                    insertDiagnosticFilterValues(info, "Filter", filterStart);
                    insertDiagnosticFilterValues(info, "White", filterWhite);
                    insertDiagnosticNormalization(info);

                    invalidateNormalization();
                    sampleState = SAMPLE_REQUIRE_FILTER_CHANGE;
                    forceRealtimeStateEmit = true;
                    forceFilterBreak = true;

                    qCDebug(log) << "Inconsistent filter state at" << Logging::time(endTime)
                                 << "filter change required";

                    event(endTime, QObject::tr(
                            "The CLAP filter is not consistent.  A filter change is required."),
                          true, info);
                    break;
                } else if (isSameNormalization(endTime, Ff.read().toInt64(), spotNumber,
                                               etime.read().toInt64())) {
                    if (spotNumber > 0 && spotNumber < 9 && spotNumber == normalizationSpot) {
                        if (FP::defined(normalizationStartTime)) {
                            qCDebug(log) << "Accepted pending normalization at"
                                         << Logging::time(endTime);
                        } else {
                            qCDebug(log) << "Accepted pending spot start up at"
                                         << Logging::time(endTime);
                            sampleState = SAMPLE_SPOT_NORMALIZE;
                            forceFilterBreak = true;
                            break;
                        }
                    } else if (FP::defined(normalizationStartTime)) {
                        qCDebug(log) << "Accepted pending normalization at"
                                     << Logging::time(endTime) << "with desired spot"
                                     << normalizationSpot << "but current" << spotNumber;

                        if (controlStream != NULL) {
                            QByteArray cmd("spot=");
                            cmd.append(QByteArray::number(normalizationSpot));
                            cmd.append('\r');
                            controlStream->writeControl(cmd);
                            discardData(endTime + 0.5, 1);
                            timeoutAt(endTime + 3.0);
                            spotSetTarget = normalizationSpot;

                            Variant::Write info = Variant::Write::empty();
                            info.hash("State").setString("Run");
                            insertDiagnosticCommonValues(info);
                            insertDiagnosticFilterValues(info, "Filter", filterStart);
                            insertDiagnosticFilterValues(info, "White", filterWhite);
                            insertDiagnosticNormalization(info);
                            event(endTime, QObject::tr(
                                    "Accepted normalization and switching back to spot %1.").arg(
                                    normalizationSpot), false, info);
                        } else {
                            spotSetTarget = -1;

                            Variant::Write info = Variant::Write::empty();
                            info.hash("State").setString("Run");
                            insertDiagnosticCommonValues(info);
                            insertDiagnosticFilterValues(info, "Filter", filterStart);
                            insertDiagnosticFilterValues(info, "White", filterWhite);
                            insertDiagnosticNormalization(info);
                            event(endTime, QObject::tr(
                                    "Accepted normalization and waiting for spot to switch back to %1.")
                                          .arg(normalizationSpot), false, info);
                        }


                        baseVolume = normalizationLastVolume;
                        spotChangeCheck = normalizationSpot;
                        bypassedSpot = normalizationSpot;
                        instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;

                        discardEndTime = bypassResumeContaminate->apply(endTime, endTime, true);
                    } else {
                        qCDebug(log) << "Accepted pending spot start at" << Logging::time(endTime)
                                     << "with desired spot" << normalizationSpot << "but current"
                                     << spotNumber;

                        if (controlStream != NULL) {
                            QByteArray cmd("spot=");
                            cmd.append(QByteArray::number(normalizationSpot));
                            cmd.append('\r');
                            controlStream->writeControl(cmd);
                            discardData(endTime + 0.5, 1);
                            timeoutAt(endTime + 3.0);
                            spotSetTarget = normalizationSpot;
                        } else {
                            spotSetTarget = -1;
                        }

                        Variant::Write info = Variant::Write::empty();
                        info.hash("State").setString("Run");
                        insertDiagnosticCommonValues(info);
                        insertDiagnosticFilterValues(info, "Filter", filterStart);
                        insertDiagnosticFilterValues(info, "White", filterWhite);
                        insertDiagnosticNormalization(info);
                        event(endTime, QObject::tr(
                                "Accepted spot resume and restarting normalize on spot %1.").arg(
                                normalizationSpot), false, info);

                        forceFilterBreak = true;
                        sampleState = SAMPLE_SPOT_ADVANCE;
                        bypassedSpot = normalizationSpot;
                        spotChangeCheck = spotNumber;
                        instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;

                        break;
                    }
                } else {
                    Variant::Write info = Variant::Write::empty();
                    info.hash("State").setString("Run");
                    insertDiagnosticCommonValues(info);
                    insertDiagnosticFilterValues(info, "Filter", filterStart);
                    insertDiagnosticFilterValues(info, "White", filterWhite);
                    insertDiagnosticNormalization(info);
                    event(endTime, QObject::tr("Rejected saved normalization."), false, info);

                    normalizationStartTime = FP::undefined();
                    for (int color = 0; color < 3; color++) {
                        spotNormalization[color] = FP::undefined();
                    }

                    if (normalizationSpot > 0 && normalizationSpot < 8) {
                        qCDebug(log) << "Rejected pending normalization at"
                                     << Logging::time(endTime) << "advancing to spot"
                                     << (normalizationSpot + 1);

                        if (controlStream != NULL) {
                            QByteArray cmd("spot=");
                            cmd.append(QByteArray::number(normalizationSpot + 1));
                            cmd.append('\r');
                            controlStream->writeControl(cmd);
                            discardData(endTime + 0.5, 1);
                            timeoutAt(endTime + 3.0);
                            spotSetTarget = normalizationSpot + 1;
                        } else {
                            spotSetTarget = -1;
                        }

                        Variant::Write info = Variant::Write::empty();
                        info.hash("State").setString("Run");
                        insertDiagnosticCommonValues(info);
                        insertDiagnosticFilterValues(info, "Filter", filterStart);
                        insertDiagnosticFilterValues(info, "White", filterWhite);
                        event(endTime, QObject::tr(
                                "Advancing to spot %1 after normalization rejection.").arg(
                                normalizationSpot + 1), false, info);

                        forceFilterBreak = true;
                        sampleState = SAMPLE_SPOT_ADVANCE;
                        bypassedSpot = normalizationSpot + 1;
                        spotChangeCheck = spotNumber;
                        instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
                        break;
                    } else {
                        qCDebug(log) << "Rejected pending normalization at"
                                     << Logging::time(endTime) << "while on the last spot";

                        if (spotNumber != 0 && controlStream != NULL) {
                            controlStream->writeControl("spot=0\r");
                            discardData(endTime + 0.5, 1);
                            timeoutAt(endTime + 3.0);
                        }

                        Variant::Write info = Variant::Write::empty();
                        info.hash("State").setString("Run");
                        insertDiagnosticCommonValues(info);
                        insertDiagnosticFilterValues(info, "Filter", filterStart);
                        insertDiagnosticFilterValues(info, "White", filterWhite);
                        event(endTime, QObject::tr(
                                "Normalization rejected on the last spot; filter change required."),
                              true, info);

                        emitFilterEnd(endTime);
                        instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
                        invalidateNormalization();
                        sampleState = SAMPLE_REQUIRE_FILTER_CHANGE;
                        forceFilterBreak = true;
                        break;
                    }
                }
            } else if (havePriorInstrument) {
                if (!isSameInstrument(endTime)) {
                    havePriorInstrument = false;

                    Variant::Write info = Variant::Write::empty();
                    info.hash("State").setString("Run");
                    insertDiagnosticCommonValues(info);
                    insertDiagnosticFilterValues(info, "Filter", filterStart);
                    insertDiagnosticFilterValues(info, "White", filterWhite);
                    insertDiagnosticNormalization(info);

                    info.hash("NormalizationSerialNumber").set(normalizationSerialNumber);
                    info.hash("NormalizationFirmwareVersion").set(normalizationFirmwareVersion);
                    info.hash("PriorSerialNumber").set(priorInstrumentSerialNumber);
                    info.hash("PriorFirmwareVersion").set(priorInstrumentFirmwareVersion);
                    info.hash("InstrumentSerialNumber").set(instrumentMeta["SerialNumber"]);
                    info.hash("InstrumentFirmwareVersion").set(instrumentMeta["FirmwareVersion"]);

                    invalidateNormalization();
                    sampleState = SAMPLE_REQUIRE_WHITE_FILTER_CHANGE;
                    forceRealtimeStateEmit = true;
                    forceFilterBreak = true;

                    qCDebug(log) << "Instrument identifier (" << instrumentMeta["SerialNumber"]
                                 << ":" << instrumentMeta["FirmwareVersion"]
                                 << ") does not match the normalization ("
                                 << normalizationSerialNumber << ":" << normalizationFirmwareVersion
                                 << ") white filter change required";

                    event(endTime, QObject::tr(
                            "The CLAP instrument has changed.  A white filter change is required."),
                          true, info);
                    break;
                }
            }

            spotChangeCheck = spotNumber;
            if (controlStream != NULL && autodetectStart(ID, I)) {
                qCDebug(log) << "Auto-detected filter change start at" << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("Run");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "Filter", filterStart);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                insertDiagnosticNormalization(info);
                event(endTime, QObject::tr("Auto-detected filter change started."), true, info);
                forceFilterBreak = true;
                sampleState = SAMPLE_FILTER_CHANGE_START;
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
                spotChangeCheck = spotNumber;

                controlStream->writeControl("stop\r");
                discardData(endTime + 0.5, 1);
                timeoutAt(endTime + 3.0);
                emitFilterEnd(endTime);
                normalizationStartTime = FP::undefined();
                break;
            }


            if (spotNumber != normalizationSpot) {
                baseVolume = normalizationLastVolume;
                if (instrumentAcknowledgeTimeout >= endTime)
                    break;

                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;

                if (controlStream != NULL) {
                    discardEndTime = bypassResumeContaminate->apply(endTime, endTime, true);

                    qCDebug(log) << "Sending spot change from spot" << spotNumber << "to"
                                 << normalizationSpot;

                    QByteArray cmd("spot=");
                    cmd.append(QByteArray::number(normalizationSpot));
                    cmd.append('\r');
                    controlStream->writeControl(cmd);
                    discardData(endTime + 0.5, 1);
                    timeoutAt(endTime + 3.0);
                    spotSetTarget = normalizationSpot;
                } else {
                    spotSetTarget = -1;
                }

                /* In passive mode, we have to accept what the instrument
                 * reports, so re-normalize if we get there */
                if (spotNumber != 0) {
                    switch (responseState) {
                    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
                    case RESP_AUTOPROBE_PASSIVE_WAIT:
                    case RESP_PASSIVE_WAIT:
                    case RESP_PASSIVE_RUN:
                    case RESP_PASSIVE_DISCARD:
                        forceFilterBreak = true;
                        sampleState = SAMPLE_SPOT_ADVANCE;
                        bypassedSpot = spotNumber;
                        spotChangeCheck = normalizationSpot;
                        break;
                    default:
                        break;
                    }
                }

                break;
            }

            if (INTEGER::defined(Ff.read().toInt64()))
                normalizationFilterID = (quint32) Ff.read().toInt64();
            normalizationSpot = (quint8) spotNumber;
            normalizationLastVolume = Qt.read().toDouble();
            normalizationLastTime = endTime;
            if (INTEGER::defined(etime.read().toInt64()))
                normalizationLastInstrumentTime = (quint32) etime.read().toInt64();
            normalizationFirmwareVersion.write().set(instrumentMeta["FirmwareVersion"]);
            normalizationSerialNumber.write().set(instrumentMeta["SerialNumber"]);

            break;

        case SAMPLE_SPOT_NORMALIZE:
            outputFlags.write().applyFlag("Normalizing");

            if (INTEGER::defined(flags.read().toInt64()) &&
                    (flags.read().toInt64() & FLAG_CHANGING)) {
                qCDebug(log) << "Instrument initiated filter change at" << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("Normalize");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "Filter", filterStart);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                insertDiagnosticNormalizationStable(info);
                event(endTime, QObject::tr("Instrument initiated filter change started."), true,
                      info);

                if (controlStream != NULL && spotNumber != 0) {
                    qCDebug(log) << "Instrument has filter change flag set but is not on spot zero";
                    controlStream->writeControl("spot=0\rstop\r");
                    discardData(endTime + 0.5, 1);
                    timeoutAt(endTime + 3.0);
                }
                emitFilterEnd(endTime);
                forceFilterBreak = true;
                sampleState = SAMPLE_FILTER_CHANGE_START;
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
                spotChangeCheck = spotNumber;
                break;
            }

            if (!haveAcceptedNormalization) {
                haveAcceptedNormalization = true;
                persistentValuesUpdated();
                if (!isSameInstrument(endTime)) {
                    havePriorInstrument = false;

                    Variant::Write info = Variant::Write::empty();
                    info.hash("State").setString("Normalize");
                    insertDiagnosticCommonValues(info);
                    insertDiagnosticFilterValues(info, "Filter", filterStart);
                    insertDiagnosticFilterValues(info, "White", filterWhite);
                    insertDiagnosticNormalization(info);

                    info.hash("NormalizationSerialNumber").set(normalizationSerialNumber);
                    info.hash("NormalizationFirmwareVersion").set(normalizationFirmwareVersion);
                    info.hash("PriorSerialNumber").set(priorInstrumentSerialNumber);
                    info.hash("PriorFirmwareVersion").set(priorInstrumentFirmwareVersion);
                    info.hash("InstrumentSerialNumber").set(instrumentMeta["SerialNumber"]);
                    info.hash("InstrumentFirmwareVersion").set(instrumentMeta["FirmwareVersion"]);

                    invalidateNormalization();
                    sampleState = SAMPLE_REQUIRE_WHITE_FILTER_CHANGE;
                    forceRealtimeStateEmit = true;
                    forceFilterBreak = true;

                    qCDebug(log) << "Instrument identifier (" << instrumentMeta["SerialNumber"]
                                 << ":" << instrumentMeta["FirmwareVersion"]
                                 << ") does not match the normalization ("
                                 << normalizationSerialNumber << ":" << normalizationFirmwareVersion
                                 << ") white filter change required";

                    event(endTime, QObject::tr(
                            "The CLAP instrument has changed.  A white filter change is required."),
                          true, info);
                    break;
                } else if (!isSameFilter(endTime)) {
                    Variant::Write info = Variant::Write::empty();
                    info.hash("State").setString("Normalize");
                    insertDiagnosticCommonValues(info);
                    insertDiagnosticFilterValues(info, "Filter", filterStart);
                    insertDiagnosticFilterValues(info, "White", filterWhite);
                    insertDiagnosticNormalization(info);

                    invalidateNormalization();
                    sampleState = SAMPLE_REQUIRE_FILTER_CHANGE;
                    forceRealtimeStateEmit = true;
                    forceFilterBreak = true;

                    qCDebug(log) << "Inconsistent filter state at" << Logging::time(endTime)
                                 << "filter change required";

                    event(endTime, QObject::tr(
                            "The CLAP filter is not consistent.  A filter change is required."),
                          true, info);
                    break;
                } else if (isSameNormalization(endTime, Ff.read().toInt64(), spotNumber,
                                               etime.read().toInt64())) {
                    if (spotNumber > 0 && spotNumber < 9 && spotNumber == normalizationSpot) {
                        if (FP::defined(normalizationStartTime)) {
                            qCDebug(log) << "Accepted pending normalization at"
                                         << Logging::time(endTime);
                            sampleState = SAMPLE_RUN;
                            break;
                        } else {
                            qCDebug(log) << "Accepted pending spot start up at"
                                         << Logging::time(endTime);
                        }
                        forceFilterBreak = true;
                    } else if (FP::defined(normalizationStartTime)) {
                        qCDebug(log) << "Accepted pending normalization at"
                                     << Logging::time(endTime) << "with desired spot"
                                     << normalizationSpot << "but current" << spotNumber;

                        if (controlStream != NULL) {
                            QByteArray cmd("spot=");
                            cmd.append(QByteArray::number(normalizationSpot));
                            cmd.append('\r');
                            controlStream->writeControl(cmd);
                            discardData(endTime + 0.5, 1);
                            timeoutAt(endTime + 3.0);
                            spotSetTarget = normalizationSpot;

                            Variant::Write info = Variant::Write::empty();
                            info.hash("State").setString("Normalize");
                            insertDiagnosticCommonValues(info);
                            insertDiagnosticFilterValues(info, "Filter", filterStart);
                            insertDiagnosticFilterValues(info, "White", filterWhite);
                            insertDiagnosticNormalization(info);
                            event(endTime, QObject::tr(
                                    "Accepted normalization and switching back to spot %1.").arg(
                                    normalizationSpot), false, info);
                        } else {
                            spotSetTarget = -1;

                            Variant::Write info = Variant::Write::empty();
                            info.hash("State").setString("Run");
                            insertDiagnosticCommonValues(info);
                            insertDiagnosticFilterValues(info, "Filter", filterStart);
                            insertDiagnosticFilterValues(info, "White", filterWhite);
                            insertDiagnosticNormalization(info);
                            event(endTime, QObject::tr(
                                    "Accepted normalization and waiting for spot to switch back to %1.")
                                          .arg(normalizationSpot), false, info);
                        }


                        baseVolume = normalizationLastVolume;
                        spotChangeCheck = normalizationSpot;
                        bypassedSpot = normalizationSpot;
                        instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;

                        discardEndTime = bypassResumeContaminate->apply(endTime, endTime, true);

                        forceFilterBreak = true;
                        sampleState = SAMPLE_RUN;
                        break;
                    } else {
                        qCDebug(log) << "Accepted pending spot start at" << Logging::time(endTime)
                                     << "with desired spot" << normalizationSpot << "but current"
                                     << spotNumber;

                        if (controlStream != NULL) {
                            QByteArray cmd("spot=");
                            cmd.append(QByteArray::number(normalizationSpot));
                            cmd.append('\r');
                            controlStream->writeControl(cmd);
                            discardData(endTime + 0.5, 1);
                            timeoutAt(endTime + 3.0);
                            spotSetTarget = normalizationSpot;
                        } else {
                            spotSetTarget = -1;
                        }

                        Variant::Write info = Variant::Write::empty();
                        info.hash("State").setString("Normalize");
                        insertDiagnosticCommonValues(info);
                        insertDiagnosticFilterValues(info, "Filter", filterStart);
                        insertDiagnosticFilterValues(info, "White", filterWhite);
                        insertDiagnosticNormalization(info);
                        event(endTime, QObject::tr(
                                "Accepted spot resume and restarting normalize on spot %1.").arg(
                                normalizationSpot), false, info);

                        sampleState = SAMPLE_SPOT_ADVANCE;
                        forceFilterBreak = true;
                        bypassedSpot = normalizationSpot;
                        spotChangeCheck = spotNumber;
                        instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;

                        break;
                    }
                } else {
                    Variant::Write info = Variant::Write::empty();
                    info.hash("State").setString("Normalize");
                    insertDiagnosticCommonValues(info);
                    insertDiagnosticFilterValues(info, "Filter", filterStart);
                    insertDiagnosticFilterValues(info, "White", filterWhite);
                    insertDiagnosticNormalization(info);
                    event(endTime, QObject::tr("Rejected saved normalization."), false, info);

                    normalizationStartTime = FP::undefined();
                    for (int color = 0; color < 3; color++) {
                        spotNormalization[color] = FP::undefined();
                    }

                    if (normalizationSpot > 0 && normalizationSpot < 8) {
                        qCDebug(log) << "Rejected pending normalization at"
                                     << Logging::time(endTime) << "advancing to spot"
                                     << (normalizationSpot + 1);

                        if (controlStream != NULL) {
                            QByteArray cmd("spot=");
                            cmd.append(QByteArray::number(normalizationSpot + 1));
                            cmd.append('\r');
                            controlStream->writeControl(cmd);
                            discardData(endTime + 0.5, 1);
                            timeoutAt(endTime + 3.0);
                            spotSetTarget = normalizationSpot + 1;
                        } else {
                            spotSetTarget = -1;
                        }

                        Variant::Write info = Variant::Write::empty();
                        info.hash("State").setString("Normalize");
                        insertDiagnosticCommonValues(info);
                        insertDiagnosticFilterValues(info, "Filter", filterStart);
                        insertDiagnosticFilterValues(info, "White", filterWhite);
                        event(endTime, QObject::tr(
                                "Advancing to spot %1 after normalization rejection.").arg(
                                normalizationSpot + 1), false, info);

                        sampleState = SAMPLE_SPOT_ADVANCE;
                        forceFilterBreak = true;
                        bypassedSpot = normalizationSpot + 1;
                        spotChangeCheck = spotNumber;
                        instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
                        break;
                    } else {
                        qCDebug(log) << "Rejected pending normalization at"
                                     << Logging::time(endTime) << "while on the last spot";

                        if (spotNumber != 0 && controlStream != NULL) {
                            controlStream->writeControl("spot=0\r");
                            discardData(endTime + 0.5, 1);
                            timeoutAt(endTime + 3.0);
                        }

                        Variant::Write info = Variant::Write::empty();
                        info.hash("State").setString("Normalize");
                        insertDiagnosticCommonValues(info);
                        insertDiagnosticFilterValues(info, "Filter", filterStart);
                        insertDiagnosticFilterValues(info, "White", filterWhite);
                        event(endTime, QObject::tr(
                                "Normalization rejected on the last spot; filter change required."),
                              true, info);

                        emitFilterEnd(endTime);
                        instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
                        invalidateNormalization();
                        sampleState = SAMPLE_REQUIRE_FILTER_CHANGE;
                        forceFilterBreak = true;
                        break;
                    }
                }
            } else if (havePriorInstrument) {
                if (!isSameInstrument(endTime)) {
                    havePriorInstrument = false;

                    Variant::Write info = Variant::Write::empty();
                    info.hash("State").setString("Normalize");
                    insertDiagnosticCommonValues(info);
                    insertDiagnosticFilterValues(info, "Filter", filterStart);
                    insertDiagnosticFilterValues(info, "White", filterWhite);
                    insertDiagnosticNormalization(info);

                    info.hash("NormalizationSerialNumber").set(normalizationSerialNumber);
                    info.hash("NormalizationFirmwareVersion").set(normalizationFirmwareVersion);
                    info.hash("PriorSerialNumber").set(priorInstrumentSerialNumber);
                    info.hash("PriorFirmwareVersion").set(priorInstrumentFirmwareVersion);
                    info.hash("InstrumentSerialNumber").set(instrumentMeta["SerialNumber"]);
                    info.hash("InstrumentFirmwareVersion").set(instrumentMeta["FirmwareVersion"]);

                    invalidateNormalization();
                    sampleState = SAMPLE_REQUIRE_WHITE_FILTER_CHANGE;
                    forceRealtimeStateEmit = true;
                    forceFilterBreak = true;

                    qCDebug(log) << "Instrument identifier (" << instrumentMeta["SerialNumber"]
                                 << ":" << instrumentMeta["FirmwareVersion"]
                                 << ") does not match the normalization ("
                                 << normalizationSerialNumber << ":" << normalizationFirmwareVersion
                                 << ") white filter change required";

                    event(endTime, QObject::tr(
                            "The CLAP instrument has changed.  A white filter change is required."),
                          true, info);
                    break;
                }
            }

            if (controlStream != NULL && autodetectStart(ID, I)) {
                qCDebug(log) << "Auto-detected filter change start at" << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("Normalize");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "Filter", filterStart);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                insertDiagnosticNormalizationStable(info);
                event(endTime, QObject::tr("Auto-detected filter change started."), true, info);

                spotChangeCheck = spotNumber;
                forceFilterBreak = true;
                sampleState = SAMPLE_FILTER_CHANGE_START;
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;

                controlStream->writeControl("stop\r");
                discardData(endTime + 0.5, 1);
                timeoutAt(endTime + 3.0);
                emitFilterEnd(endTime);
                normalizationStartTime = FP::undefined();
                break;
            }

            spotChangeCheck = spotNumber;
            if (spotNumber <= 0 || spotNumber > 8) {
                for (int color = 0; color < 3; color++) {
                    spotNormalizationIn[color]->reset();
                }
            } else {
                for (int color = 0; color < 3; color++) {
                    spotNormalizationIn[color]->add(In[spotNumber - 1][color].read().toDouble(),
                                                    startTime, endTime);
                }
            }
            if (!normalizationDone())
                break;
            emitNormalizationEnd(endTime);
            normalizationStartTime = endTime;
            for (int color = 0; color < 3; color++) {
                spotNormalization[color] = spotNormalizationIn[color]->value();
            }
            if (INTEGER::defined(Ff.read().toInt64()))
                normalizationFilterID = (quint32) Ff.read().toInt64();
            normalizationSpot = (quint8) spotNumber;
            normalizationLastVolume = Qt.read().toDouble();
            normalizationLastTime = endTime;
            if (INTEGER::defined(etime.read().toInt64()))
                normalizationLastInstrumentTime = (quint32) etime.read().toInt64();
            normalizationFirmwareVersion.write().set(instrumentMeta["FirmwareVersion"]);
            normalizationSerialNumber.write().set(instrumentMeta["SerialNumber"]);

            qCDebug(log) << "Normalization established at" << Logging::time(endTime);

            {
                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("Normalize");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "Filter", filterStart);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                insertDiagnosticNormalizationStable(info);
                insertDiagnosticNormalization(info);
                event(endTime, QObject::tr("Spot normalization completed."), false, info);
            }
            if (state != NULL)
                state->requestStateSave();

            haveAcceptedNormalization = true;
            sampleState = SAMPLE_RUN;

            persistentValuesUpdated();
            break;

        case SAMPLE_SPOT_ADVANCE:
            forceFilterBreak = true;
            if (!FP::defined(instrumentAcknowledgeTimeout)) {
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
            }
            if (instrumentAcknowledgeTimeout >= endTime) {
                if (spotNumber == spotChangeCheck)
                    break;
            } else if (spotSetTarget != -1 &&
                    controlStream != NULL &&
                    spotNumber == spotChangeCheck &&
                    spotSetTarget != spotChangeCheck) {
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;

                qCDebug(log) << "Retrying spot set to" << spotSetTarget << "at"
                             << Logging::time(endTime);

                QByteArray cmd("spot=");
                cmd.append(QByteArray::number(spotSetTarget));
                cmd.append('\r');
                controlStream->writeControl(cmd);
                discardData(endTime + 0.5, 1);
                timeoutAt(endTime + 3.0);

                spotSetTarget = -1;
                break;
            } else {
                qCDebug(log) << "Spot advance from spot" << spotChangeCheck
                             << "acknowledge timeout ("
                             << Logging::time(instrumentAcknowledgeTimeout) << ") at"
                             << Logging::time(endTime) << "filter change required";

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("SpotAdvance");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "Filter", filterStart);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                if (requireFilterChangeOnSpotFailure()) {
                    event(endTime, QObject::tr(
                            "Timeout waiting for instrument to acknowledge spot advance.  A filter change is required."),
                          true, info);

                    invalidateNormalization();
                    sampleState = SAMPLE_REQUIRE_FILTER_CHANGE;
                    forceRealtimeStateEmit = true;
                    forceFilterBreak = true;
                    break;
                } else {
                    event(endTime, QObject::tr(
                            "Timeout waiting for instrument to acknowledge spot advance."), false,
                          info);
                }
            }

            if (INTEGER::defined(flags.read().toInt64()) &&
                    (flags.read().toInt64() & FLAG_CHANGING)) {
                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("SpotAdvance");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "Filter", filterStart);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                event(endTime, QObject::tr("Instrument initiated filter change started."), true,
                      info);

                if (controlStream != NULL && spotNumber != 0) {
                    qCDebug(log) << "Instrument has filter change flag set but is not on spot zero";
                    controlStream->writeControl("spot=0\rstop\r");
                    discardData(endTime + 0.5, 1);
                    timeoutAt(endTime + 3.0);
                }
                emitFilterEnd(endTime);
                forceFilterBreak = true;
                sampleState = SAMPLE_FILTER_CHANGE_START;
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
                spotChangeCheck = spotNumber;
                break;
            }

            if (spotNumber == 0) {
                qCDebug(log) << "Spot reset to zero during advance at" << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("SpotAdvance");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "Filter", filterStart);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                event(endTime, QObject::tr(
                        "Spot reset to zero during spot advance.  Filter change required."), true,
                      info);

                emitFilterEnd(endTime);
                sampleState = SAMPLE_REQUIRE_FILTER_CHANGE;
                invalidateNormalization();
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
                break;
            }

            qCDebug(log) << "Spot advance completed at" << Logging::time(endTime);
            normalizationSpot = spotNumber;
            sampleState = SAMPLE_SPOT_NORMALIZE;
            haveAcceptedNormalization = true;
            spotNormalizationStart();
            persistentValuesUpdated();
            break;

        case SAMPLE_FILTER_BASELINE_START:
            outputFlags.write().applyFlag("FilterChanging");
            if (!FP::defined(instrumentAcknowledgeTimeout)) {
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
            }
            if (instrumentAcknowledgeTimeout >= endTime) {
                if (INTEGER::defined(flags.read().toInt64()) &&
                        (flags.read().toInt64() & FLAG_CHANGING))
                    break;
                if (spotChangeCheck != 0 && spotNumber == spotChangeCheck)
                    break;
                qCDebug(log) << "Filter baseline acknowledged by instrument at"
                             << Logging::time(endTime);
            } else {
                qCDebug(log) << "Filter baseline acknowledge timeout ("
                             << Logging::elapsed(instrumentAcknowledgeTimeout) << ") at"
                             << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("FilterBaselineStart");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                event(endTime, QObject::tr(
                        "Timeout waiting for instrument to acknowledge filter baseline start."),
                      false, info);
            }

            sampleState = SAMPLE_FILTER_BASELINE;
            /* Fall through */
        case SAMPLE_FILTER_BASELINE:
            outputFlags.write().applyFlag("FilterChanging");
            if (sampleState == SAMPLE_WHITE_FILTER_BASELINE)
                outputFlags.write().applyFlag("WhiteFilterChanging");
            if (INTEGER::defined(flags.read().toInt64()) &&
                    (flags.read().toInt64() & FLAG_CHANGING)) {
                qCDebug(log) << "Instrument initiated filter change at" << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterBaseline(info);
                info.hash("State").setString("FilterBaseline");
                insertDiagnosticFilterValues(info, "White", filterWhite);
                event(endTime, QObject::tr("Instrument initiated filter change started."), true,
                      info);

                if (controlStream != NULL && spotNumber != 0) {
                    qCDebug(log) << "Instrument has filter change flag set but is not on spot zero";
                    controlStream->writeControl("spot=0\rstop\r");
                    discardData(endTime + 0.5, 1);
                    timeoutAt(endTime + 3.0);
                }
                emitFilterEnd(endTime);
                sampleState = SAMPLE_FILTER_CHANGE_START;
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
                spotChangeCheck = spotNumber;
                break;
            }

            if (spotNumber != 0) {
                qCDebug(log) << "Non-zero detected during filter baseline at"
                             << Logging::time(endTime) << "ending baseline sampling";

                Variant::Write info = Variant::Write::empty();
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterBaseline(info);
                info.hash("State").setString("FilterBaseline");
                insertDiagnosticFilterValues(info, "White", filterWhite);
                event(endTime, QObject::tr(
                        "Instrument changed to non-zero spot during baseline establishment.  Baseline discarded."),
                      true, info);

                setInvalidFilterStart();
                spotChangeCheck = spotNumber;
                normalizationSpot = spotNumber;
                sampleState = SAMPLE_SPOT_NORMALIZE;
                spotNormalizationStart();
                break;
            }

            if (!filterBaseline())
                break;
            qCDebug(log) << "Baseline established at" << Logging::time(endTime);

            saveFilterStart(endTime);
            filterIsNotWhite = !isWhiteFilter();

            if (filterIsNotWhite) {
                Variant::Write info = Variant::Write::empty();
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterBaseline(info);
                insertDiagnosticFilterValues(info, "Filter", filterStart);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                info.hash("State").setString("FilterBaseline");
                event(endTime, QObject::tr(
                        "Filter baseline established on what does not appear to be a white filter."),
                      true, info);
            } else {
                Variant::Write info = Variant::Write::empty();
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterBaseline(info);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                info.hash("State").setString("FilterBaseline");
                event(endTime, QObject::tr("Filter baseline established."), false, info);
            }

            spotChangeCheck = 0;
            if (controlStream != NULL) {
                controlStream->writeControl("spot=1\r");
                discardData(endTime + 0.5, 1);
                timeoutAt(endTime + 3.0);
                spotSetTarget = 1;
            } else {
                spotSetTarget = -1;
            }
            sampleState = SAMPLE_SPOT_ADVANCE;
            instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
            haveAcceptedNormalization = true;
            bypassedSpot = 1;
            priorInstrumentFirmwareVersion.write().set(instrumentMeta["FirmwareVersion"]);
            priorInstrumentSerialNumber.write().set(instrumentMeta["SerialNumber"]);
            havePriorInstrument = false;

            persistentValuesUpdated();
            break;

        case SAMPLE_WHITE_FILTER_BASELINE_START:
            outputFlags.write().applyFlag("FilterChanging");
            outputFlags.write().applyFlag("WhiteFilterChanging");
            if (!FP::defined(instrumentAcknowledgeTimeout)) {
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
            }
            if (instrumentAcknowledgeTimeout >= endTime) {
                if (INTEGER::defined(flags.read().toInt64()) &&
                        (flags.read().toInt64() & FLAG_CHANGING))
                    break;
                if (spotChangeCheck != 0 && spotNumber == spotChangeCheck)
                    break;
                qCDebug(log) << "Filter white baseline acknowledged by instrument at"
                             << Logging::time(endTime);
            } else {
                qCDebug(log) << "Filter white baseline acknowledge timeout ("
                             << Logging::time(instrumentAcknowledgeTimeout) << ") at"
                             << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("WhiteFilterBaselineStart");
                insertDiagnosticCommonValues(info);
                event(endTime, QObject::tr(
                        "Timeout waiting for instrument to acknowledge white filter baseline start."),
                      false, info);
            }

            sampleState = SAMPLE_WHITE_FILTER_BASELINE;
            /* Fall through */
        case SAMPLE_WHITE_FILTER_BASELINE:
            outputFlags.write().applyFlag("FilterChanging");
            outputFlags.write().applyFlag("WhiteFilterChanging");
            if (INTEGER::defined(flags.read().toInt64()) &&
                    (flags.read().toInt64() & FLAG_CHANGING)) {
                qCDebug(log) << "Instrument initiated filter change at" << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterBaseline(info);
                info.hash("State").setString("WhiteFilterBaseline");
                event(endTime, QObject::tr("Instrument initiated filter change started."), true,
                      info);

                if (controlStream != NULL && spotNumber != 0) {
                    qCDebug(log) << "Instrument has filter change flag set but is not on spot zero";
                    controlStream->writeControl("spot=0\rstop\r");
                    discardData(endTime + 0.5, 1);
                    timeoutAt(endTime + 3.0);
                }
                emitFilterEnd(endTime);
                sampleState = SAMPLE_FILTER_CHANGE_START;
                forceFilterBreak = true;
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
                spotChangeCheck = spotNumber;
                break;
            }

            if (spotNumber != 0) {
                qCDebug(log) << "Non-zero detected during filter baseline at"
                             << Logging::time(endTime) << "ending baseline sampling";

                Variant::Write info = Variant::Write::empty();
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterBaseline(info);
                info.hash("State").setString("WhiteFilterBaseline");
                event(endTime, QObject::tr(
                        "Instrument changed to non-zero spot during white baseline establishment.  Baseline discarded."),
                      true, info);

                setInvalidFilterStart();
                spotChangeCheck = spotNumber;
                normalizationSpot = spotNumber;
                sampleState = SAMPLE_SPOT_NORMALIZE;
                spotNormalizationStart();
                break;
            }

            if (!filterBaseline())
                break;
            qCDebug(log) << "White baseline established at" << Logging::time(endTime);

            saveFilterStart(endTime);
            emitWhiteEnd(endTime);
            filterWhite = filterStart;
            filterIsNotWhite = false;

            {
                Variant::Write info = Variant::Write::empty();
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterBaseline(info);
                info.hash("State").setString("WhiteFilterBaseline");
                event(endTime, QObject::tr("White filter baseline established."), false, info);
            }

            spotChangeCheck = 0;
            if (controlStream != NULL && spotNumber == 0) {
                controlStream->writeControl("spot=1\r");
                discardData(endTime + 0.5, 1);
                timeoutAt(endTime + 3.0);
                spotSetTarget = 1;

                haveAcceptedNormalization = true;
                sampleState = SAMPLE_SPOT_ADVANCE;
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
            }
            bypassedSpot = 1;
            priorInstrumentFirmwareVersion.write().set(instrumentMeta["FirmwareVersion"]);
            priorInstrumentSerialNumber.write().set(instrumentMeta["SerialNumber"]);
            havePriorInstrument = false;

            persistentValuesUpdated();
            break;

        case SAMPLE_BYPASSED_FILTER_BASELINE_START:
            outputFlags.write().applyFlag("FilterChanging");
            if (!FP::defined(instrumentAcknowledgeTimeout)) {
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
            }
            if (instrumentAcknowledgeTimeout >= endTime) {
                if (INTEGER::defined(flags.read().toInt64()) &&
                        (flags.read().toInt64() & FLAG_CHANGING))
                    break;
                if (spotChangeCheck != 0 && spotNumber == spotChangeCheck)
                    break;
                qCDebug(log) << "Bypassed filter baseline acknowledged by instrument at"
                             << Logging::time(endTime);
            } else {
                qCDebug(log) << "Bypassed filter baseline acknowledge timeout ("
                             << Logging::time(instrumentAcknowledgeTimeout) << ") at"
                             << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("BypassedFilterBaselineStart");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                event(endTime, QObject::tr(
                        "Timeout waiting for instrument to acknowledge filter baseline start."),
                      false, info);
            }

            sampleState = SAMPLE_BYPASSED_FILTER_BASELINE;
            /* Fall through */
        case SAMPLE_BYPASSED_FILTER_BASELINE:
            outputFlags.write().applyFlag("FilterChanging");

            if (INTEGER::defined(flags.read().toInt64()) &&
                    (flags.read().toInt64() & FLAG_CHANGING)) {
                qCDebug(log) << "Instrument initiated bypassed filter change at"
                             << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("SpotAdvance");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterBaseline(info);
                info.hash("State").setString("BypassedFilterBaseline");
                insertDiagnosticFilterValues(info, "White", filterWhite);
                event(endTime, QObject::tr("Instrument initiated filter change started."), true,
                      info);

                if (controlStream != NULL && spotNumber != 0) {
                    qCDebug(log) << "Instrument has filter change flag set but is not on spot zero";
                    controlStream->writeControl("spot=0\rstop\r");
                    discardData(endTime + 0.5, 1);
                    timeoutAt(endTime + 3.0);
                }
                emitFilterEnd(endTime);
                sampleState = SAMPLE_BYPASSED_FILTER_CHANGE_START;
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
                spotChangeCheck = spotNumber;
                break;
            }

            if (spotNumber != 0) {
                qCDebug(log) << "Non-zero detected during bypassed filter baseline at"
                             << Logging::time(endTime) << " ending baseline sampling";

                Variant::Write info = Variant::Write::empty();
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterBaseline(info);
                if (sampleState == SAMPLE_BYPASSED_FILTER_BASELINE) {
                    info.hash("State").setString("BypassedFilterBaseline");
                    insertDiagnosticFilterValues(info, "White", filterWhite);
                } else {
                    info.hash("State").setString("BypassedWhiteFilterBaseline");
                }
                event(endTime, QObject::tr(
                        "Instrument changed to non-zero spot during baseline establishment.  Baseline discarded."),
                      true, info);

                setInvalidFilterStart();
                spotChangeCheck = spotNumber;
                normalizationSpot = spotNumber;
                sampleState = SAMPLE_SPOT_NORMALIZE;
                spotNormalizationStart();
                break;
            }

            if (!filterBaseline())
                break;
            qCDebug(log) << "Bypassed baseline established at" << Logging::time(endTime);

            saveFilterStart(endTime);
            filterIsNotWhite = !isWhiteFilter();

            if (filterIsNotWhite) {
                Variant::Write info = Variant::Write::empty();
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterBaseline(info);
                insertDiagnosticFilterValues(info, "Filter", filterStart);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                info.hash("State").setString("BypassedFilterBaseline");
                event(endTime, QObject::tr(
                        "Filter baseline established on what does not appear to be a white filter."),
                      true, info);
            } else {
                Variant::Write info = Variant::Write::empty();
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterBaseline(info);
                info.hash("State").setString("BypassedFilterBaseline");
                insertDiagnosticFilterValues(info, "White", filterWhite);
                event(endTime, QObject::tr("Filter baseline established."), false, info);
            }

            spotChangeCheck = 0;
            bypassedSpot = 1;
            sampleState = SAMPLE_BYPASSED_SPOT_NORMALIZE;
            priorInstrumentFirmwareVersion.write().set(instrumentMeta["FirmwareVersion"]);
            priorInstrumentSerialNumber.write().set(instrumentMeta["SerialNumber"]);
            havePriorInstrument = false;

            persistentValuesUpdated();
            break;

        case SAMPLE_BYPASSED_WHITE_FILTER_BASELINE_START:
            outputFlags.write().applyFlag("FilterChanging");
            outputFlags.write().applyFlag("WhiteFilterChanging");
            if (!FP::defined(instrumentAcknowledgeTimeout)) {
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
            }
            if (instrumentAcknowledgeTimeout >= endTime) {
                if (INTEGER::defined(flags.read().toInt64()) &&
                        (flags.read().toInt64() & FLAG_CHANGING))
                    break;
                if (spotChangeCheck != 0 && spotNumber == spotChangeCheck)
                    break;
                qCDebug(log) << "Bypassed white filter baseline acknowledged by instrument at"
                             << Logging::time(endTime);
            } else {
                qCDebug(log) << "Bypassed white filter baseline acknowledge timeout ("
                             << Logging::time(instrumentAcknowledgeTimeout) << ") at"
                             << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("BypassedWhiteFilterBaselineStart");
                insertDiagnosticCommonValues(info);
                event(endTime, QObject::tr(
                        "Timeout waiting for instrument to acknowledge white filter baseline start."),
                      false, info);
            }

            sampleState = SAMPLE_BYPASSED_WHITE_FILTER_BASELINE;
            /* Fall through */
        case SAMPLE_BYPASSED_WHITE_FILTER_BASELINE:
            outputFlags.write().applyFlag("FilterChanging");
            outputFlags.write().applyFlag("WhiteFilterChanging");

            if (INTEGER::defined(flags.read().toInt64()) &&
                    (flags.read().toInt64() & FLAG_CHANGING)) {
                qCDebug(log) << "Instrument initiated bypassed filter change at"
                             << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("SpotAdvance");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterBaseline(info);
                info.hash("State").setString("BypassedWhiteFilterBaseline");
                event(endTime, QObject::tr("Instrument initiated filter change started."), true,
                      info);

                if (controlStream != NULL && spotNumber != 0) {
                    qCDebug(log) << "Instrument has filter change flag set but is not on spot zero";
                    controlStream->writeControl("spot=0\rstop\r");
                    discardData(endTime + 0.5, 1);
                    timeoutAt(endTime + 3.0);
                }
                emitFilterEnd(endTime);
                sampleState = SAMPLE_BYPASSED_FILTER_CHANGE_START;
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
                spotChangeCheck = spotNumber;
                break;
            }

            if (spotNumber != 0) {
                qCDebug(log) << "Non-zero detected during bypassed white filter baseline at"
                             << Logging::time(endTime) << "ending baseline sampling";

                Variant::Write info = Variant::Write::empty();
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterBaseline(info);
                info.hash("State").setString("BypassedWhiteFilterBaseline");
                event(endTime, QObject::tr(
                        "Instrument changed to non-zero spot during white baseline establishment.  Baseline discarded."),
                      true, info);

                setInvalidFilterStart();
                spotChangeCheck = spotNumber;
                normalizationSpot = spotNumber;
                sampleState = SAMPLE_SPOT_NORMALIZE;
                spotNormalizationStart();
                break;
            }

            if (!filterBaseline())
                break;
            qCDebug(log) << "Bypassed white baseline established at" << Logging::time(endTime);


            saveFilterStart(endTime);
            emitWhiteEnd(endTime);
            filterWhite = filterStart;
            filterIsNotWhite = false;

            {
                Variant::Write info = Variant::Write::empty();
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterBaseline(info);
                info.hash("State").setString("BypassedWhiteFilterBaseline");
                event(endTime, QObject::tr("Filter baseline established."), false, info);
            }

            spotChangeCheck = 0;
            bypassedSpot = 1;
            sampleState = SAMPLE_BYPASSED_SPOT_NORMALIZE;
            priorInstrumentFirmwareVersion.write().set(instrumentMeta["FirmwareVersion"]);
            priorInstrumentSerialNumber.write().set(instrumentMeta["SerialNumber"]);
            havePriorInstrument = false;

            persistentValuesUpdated();
            break;

        case SAMPLE_FILTER_CHANGE_START:
            outputFlags.write().applyFlag("FilterChanging");
            emitFilterEnd(endTime);

            resetFilterBaseline();

            if (!FP::defined(instrumentAcknowledgeTimeout)) {
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
            }
            if (instrumentAcknowledgeTimeout >= endTime) {
                if (INTEGER::defined(flags.read().toInt64()) &&
                        !(flags.read().toInt64() & FLAG_CHANGING))
                    break;
                if (spotChangeCheck != 0 && spotNumber == spotChangeCheck)
                    break;
                qCDebug(log) << "Filter change acknowledged by instrument at"
                             << Logging::time(endTime);
            } else {
                qCDebug(log) << "Filter change acknowledge timeout ("
                             << Logging::time(instrumentAcknowledgeTimeout) << ") at"
                             << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("FilterChangeStart");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                event(endTime, QObject::tr(
                        "Timeout waiting for instrument to acknowledge filter change start."),
                      false, info);
            }

            sampleState = SAMPLE_FILTER_CHANGING;
            /* Fall through */
        case SAMPLE_FILTER_CHANGING:
            outputFlags.write().applyFlag("FilterChanging");
            emitFilterEnd(endTime);

            if (INTEGER::defined(flags.read().toInt64()) &&
                    !(flags.read().toInt64() & FLAG_CHANGING)) {
                qCDebug(log) << "Instrument initiated filter change end at"
                             << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("FilterChanging");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                insertDiagnosticFilterBaseline(info);
                event(endTime, QObject::tr("Instrument initiated filter change end."), true, info);

                resetFilterBaseline();
                spotChangeCheck = 0;
                sampleState = SAMPLE_FILTER_BASELINE;
                break;
            }

            if (spotNumber != 0) {
                if (INTEGER::defined(flags.read().toInt64())) {
                    qCDebug(log) << "Non-zero spot detected during filter change at"
                                 << Logging::time(endTime) << "but changing flag is still set";
                    if (controlStream != NULL) {
                        controlStream->writeControl("go\r");
                        discardData(endTime + 0.5, 1);
                        timeoutAt(endTime + 3.0);
                    }
                } else {
                    qCDebug(log) << "Non-zero spot detected during filter change at"
                                 << Logging::time(endTime) << "starting normalization";
                }

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("FilterChanging");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterBaseline(info);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                event(endTime, QObject::tr(
                        "Instrument switched to non-zero spot during filter change.  Starting normalization."),
                      false, info);

                spotChangeCheck = spotNumber;
                normalizationSpot = spotNumber;
                sampleState = SAMPLE_SPOT_NORMALIZE;
                setInvalidFilterStart();
                spotNormalizationStart();
                break;
            }

            if (controlStream != NULL && autodetectEnd()) {
                qCDebug(log) << "Auto-detected filter change end at" << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("FilterChanging");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                insertDiagnosticFilterBaseline(info);
                event(endTime, QObject::tr("Auto-detected filter change end."), true, info);

                if (controlStream != NULL) {
                    controlStream->writeControl("go\r");
                    discardData(endTime + 0.5, 1);
                    timeoutAt(endTime + 3.0);
                }

                spotChangeCheck = 0;
                sampleState = SAMPLE_FILTER_BASELINE_START;
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
                break;
            }
            break;

        case SAMPLE_BYPASSED_FILTER_CHANGE_START:
            outputFlags.write().applyFlag("FilterChanging");
            emitFilterEnd(endTime);

            resetFilterBaseline();

            if (!FP::defined(instrumentAcknowledgeTimeout)) {
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
            }
            if (instrumentAcknowledgeTimeout >= endTime) {
                if (INTEGER::defined(flags.read().toInt64()) &&
                        !(flags.read().toInt64() & FLAG_CHANGING))
                    break;
                if (spotChangeCheck != 0 && spotNumber == spotChangeCheck)
                    break;
                qCDebug(log) << "Bypassed filter change acknowledged by instrument at"
                             << Logging::time(endTime);
            } else {
                qCDebug(log) << "Bypassed filter change acknowledge timeout ("
                             << Logging::time(instrumentAcknowledgeTimeout) << ") at"
                             << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("BypassedFilterChangeStart");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                event(endTime, QObject::tr(
                        "Timeout waiting for instrument to acknowledge filter change start."),
                      false, info);
            }

            sampleState = SAMPLE_BYPASSED_FILTER_CHANGING;
            /* Fall through */
        case SAMPLE_BYPASSED_FILTER_CHANGING:
            outputFlags.write().applyFlag("FilterChanging");
            emitFilterEnd(endTime);

            if (INTEGER::defined(flags.read().toInt64()) &&
                    !(flags.read().toInt64() & FLAG_CHANGING)) {
                qCDebug(log) << "Instrument initiated filter change end at"
                             << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("BypassedFilterChanging");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                insertDiagnosticFilterBaseline(info);
                event(endTime, QObject::tr("Instrument initiated filter change end."), true, info);

                resetFilterBaseline();
                spotChangeCheck = 0;
                sampleState = SAMPLE_BYPASSED_FILTER_BASELINE;
                break;
            }

            if (spotNumber != 0) {
                if (INTEGER::defined(flags.read().toInt64())) {
                    qCDebug(log) << "Non-zero spot detected during bypassed filter change at"
                                 << Logging::time(endTime) << "but changing flag is still set";
                    if (controlStream != NULL) {
                        controlStream->writeControl("go\r");
                        discardData(endTime + 0.5, 1);
                        timeoutAt(endTime + 3.0);
                    }
                } else {
                    qCDebug(log) << "Non-zero spot detected during bypassed filter change at"
                                 << Logging::time(endTime) << "starting normalization";
                }

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("BypassedFilterChanging");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterBaseline(info);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                event(endTime, QObject::tr(
                        "Instrument switched to non-zero spot during filter change.  Starting normalization."),
                      false, info);

                spotChangeCheck = spotNumber;
                normalizationSpot = spotNumber;
                sampleState = SAMPLE_SPOT_NORMALIZE;
                setInvalidFilterStart();
                spotNormalizationStart();
                break;
            }

            if (controlStream != NULL && autodetectEnd()) {
                qCDebug(log) << "Auto-detected bypassed filter change end at"
                             << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("BypassedFilterChanging");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                insertDiagnosticFilterBaseline(info);
                event(endTime, QObject::tr("Auto-detected filter change end."), true, info);

                if (controlStream != NULL) {
                    controlStream->writeControl("go\r");
                    discardData(endTime + 0.5, 1);
                    timeoutAt(endTime + 3.0);
                }
                sampleState = SAMPLE_BYPASSED_FILTER_BASELINE_START;
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
                break;
            }
            break;

        case SAMPLE_WHITE_FILTER_CHANGE_START:
            outputFlags.write().applyFlag("FilterChanging");
            outputFlags.write().applyFlag("WhiteFilterChanging");
            emitFilterEnd(endTime);

            resetFilterBaseline();

            if (!FP::defined(instrumentAcknowledgeTimeout)) {
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
            }
            if (instrumentAcknowledgeTimeout >= endTime) {
                if (INTEGER::defined(flags.read().toInt64()) &&
                        !(flags.read().toInt64() & FLAG_CHANGING))
                    break;
                if (spotChangeCheck != 0 && spotNumber == spotChangeCheck)
                    break;

                qCDebug(log) << "White filter change acknowledged by instrument at"
                             << Logging::time(endTime);
            } else {
                qCDebug(log) << "White filter change acknowledge timeout ("
                             << Logging::time(instrumentAcknowledgeTimeout) << ") at"
                             << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("WhiteFilterChangeStart");
                insertDiagnosticCommonValues(info);
                event(endTime, QObject::tr(
                        "Timeout waiting for instrument to acknowledge white filter change start."),
                      false, info);
            }

            sampleState = SAMPLE_WHITE_FILTER_CHANGING;
            /* Fall through */
        case SAMPLE_WHITE_FILTER_CHANGING:
            outputFlags.write().applyFlag("FilterChanging");
            outputFlags.write().applyFlag("WhiteFilterChanging");
            emitFilterEnd(endTime);

            if (INTEGER::defined(flags.read().toInt64()) &&
                    !(flags.read().toInt64() & FLAG_CHANGING) &&
                    spotNumber == 0) {
                qCDebug(log) << "Instrument initiated white filter change end at"
                             << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("WhiteFilterChanging");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterBaseline(info);
                event(endTime, QObject::tr("Instrument initiated white filter change end."), true,
                      info);

                resetFilterBaseline();
                sampleState = SAMPLE_WHITE_FILTER_BASELINE;
                break;
            }

            if (spotNumber != 0) {
                if (INTEGER::defined(flags.read().toInt64())) {
                    qCDebug(log) << "Non-zero spot detected during white filter change at"
                                 << Logging::time(endTime) << "but changing flag is still set";
                    if (controlStream != NULL) {
                        controlStream->writeControl("go\r");
                        discardData(endTime + 0.5, 1);
                        timeoutAt(endTime + 3.0);
                    }
                } else {
                    qCDebug(log) << "Non-zero spot detected during white filter change at"
                                 << Logging::time(endTime) << "starting normalization";
                }

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("WhiteFilterChanging");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterBaseline(info);
                event(endTime, QObject::tr(
                        "Instrument switched to non-zero spot during white filter change.  Starting normalization."),
                      false, info);

                spotChangeCheck = spotNumber;
                normalizationSpot = spotNumber;
                sampleState = SAMPLE_SPOT_NORMALIZE;
                setInvalidFilterStart();
                spotNormalizationStart();
                break;
            }

            if (!FP::defined(whiteFilterChangeStartTime))
                whiteFilterChangeStartTime = endTime;
            if (controlStream != NULL && whiteFilterTimeout(endTime)) {
                qCDebug(log) << "White filter change timeout at" << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("WhiteFilterChanging");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterBaseline(info);
                event(endTime, QObject::tr("White filter change ending due to timeout."), true,
                      info);

                if (controlStream != NULL) {
                    controlStream->writeControl("go\r");
                    discardData(endTime + 0.5, 1);
                    timeoutAt(endTime + 3.0);
                }
                sampleState = SAMPLE_WHITE_FILTER_BASELINE_START;
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
            }

            break;

        case SAMPLE_BYPASSED_WHITE_FILTER_CHANGE_START:
            outputFlags.write().applyFlag("FilterChanging");
            outputFlags.write().applyFlag("WhiteFilterChanging");
            emitFilterEnd(endTime);

            resetFilterBaseline();

            if (!FP::defined(instrumentAcknowledgeTimeout)) {
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
            }
            if (instrumentAcknowledgeTimeout >= endTime) {
                if (INTEGER::defined(flags.read().toInt64()) &&
                        !(flags.read().toInt64() & FLAG_CHANGING))
                    break;
                if (spotChangeCheck != 0 && spotNumber == spotChangeCheck)
                    break;

                qCDebug(log) << "Bypassed white filter change acknowledged by instrument at"
                             << Logging::time(endTime);
            } else {
                qCDebug(log) << "Bypassed white filter change acknowledge timeout ("
                             << Logging::time(instrumentAcknowledgeTimeout) << ") at"
                             << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("BypassedWhiteFilterChangeStart");
                insertDiagnosticCommonValues(info);
                event(endTime, QObject::tr(
                        "Timeout waiting for instrument to acknowledge white filter change start."),
                      false, info);
            }

            sampleState = SAMPLE_BYPASSED_WHITE_FILTER_CHANGING;
            /* Fall through */
        case SAMPLE_BYPASSED_WHITE_FILTER_CHANGING:
            outputFlags.write().applyFlag("FilterChanging");
            outputFlags.write().applyFlag("WhiteFilterChanging");
            emitFilterEnd(endTime);

            if (spotChangeCheck != 0 && spotNumber == spotChangeCheck)
                break;
            spotChangeCheck = 0;

            if (INTEGER::defined(flags.read().toInt64()) &&
                    !(flags.read().toInt64() & FLAG_CHANGING) &&
                    spotNumber == 0) {
                qCDebug(log) << "Instrument initiated white filter change end at"
                             << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("BypassedWhiteFilterChanging");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterBaseline(info);
                event(endTime, QObject::tr("Instrument initiated white filter change end."), true,
                      info);

                resetFilterBaseline();
                sampleState = SAMPLE_BYPASSED_WHITE_FILTER_BASELINE;
                break;
            }

            if (spotNumber != 0) {
                if (INTEGER::defined(flags.read().toInt64())) {
                    qCDebug(log) << "Non-zero spot detected during bypassed white filter change at"
                                 << Logging::time(endTime) << "but changing flag is still set";
                    if (controlStream != NULL) {
                        controlStream->writeControl("go\r");
                        discardData(endTime + 0.5, 1);
                        timeoutAt(endTime + 3.0);
                    }
                } else {
                    qCDebug(log) << "Non-zero spot detected during bypassed white filter change at"
                                 << Logging::time(endTime) << " starting normalization";
                }

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("BypassedWhiteFilterChanging");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterBaseline(info);
                event(endTime, QObject::tr(
                        "Instrument switched to non-zero spot during white filter change.  Starting normalization."),
                      false, info);

                spotChangeCheck = spotNumber;
                normalizationSpot = spotNumber;
                sampleState = SAMPLE_SPOT_NORMALIZE;
                setInvalidFilterStart();
                spotNormalizationStart();
                break;
            }

            if (!FP::defined(whiteFilterChangeStartTime))
                whiteFilterChangeStartTime = endTime;
            if (controlStream != NULL && whiteFilterTimeout(endTime)) {
                qCDebug(log) << "White filter change timeout at" << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("BypassedWhiteFilterChanging");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterBaseline(info);
                event(endTime, QObject::tr("White filter change ending due to timeout."), true,
                      info);

                if (controlStream != NULL) {
                    controlStream->writeControl("go\r");
                    discardData(endTime + 0.5, 1);
                    timeoutAt(endTime + 3.0);
                }
                bypassedSpot = 1;
                sampleState = SAMPLE_BYPASSED_WHITE_FILTER_BASELINE_START;
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
            }

            break;

        case SAMPLE_BYPASSED_RUN:
        case SAMPLE_BYPASSED_SPOT_NORMALIZE:
            spotNumber = bypassedSpot;
            Fn.write().setInt64(bypassedSpot);
            break;

        case SAMPLE_UNBYPASS_RUN:
            forceFilterBreak = true;
            if (!FP::defined(instrumentAcknowledgeTimeout)) {
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
            }
            if (instrumentAcknowledgeTimeout >= endTime) {
                if (spotNumber != bypassedSpot)
                    break;
                qCDebug(log) << "Left bypass to run mode at" << Logging::time(endTime);
            } else if (spotSetTarget != -1 &&
                    controlStream != NULL &&
                    spotNumber == spotChangeCheck &&
                    spotSetTarget != spotChangeCheck) {
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;

                qCDebug(log) << "Retrying unbypass spot set to" << spotSetTarget << "at"
                             << Logging::time(endTime);

                QByteArray cmd("spot=");
                cmd.append(QByteArray::number(spotSetTarget));
                cmd.append('\r');
                controlStream->writeControl(cmd);
                discardData(endTime + 0.5, 1);
                timeoutAt(endTime + 3.0);

                spotSetTarget = -1;
                break;
            } else {
                qCDebug(log) << "Bypass end acknowledge timeout ("
                             << Logging::time(instrumentAcknowledgeTimeout) << ") at"
                             << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("UnbypassRun");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "Filter", filterStart);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                insertDiagnosticNormalization(info);
                if (requireFilterChangeOnSpotFailure()) {
                    event(endTime, QObject::tr(
                            "Timeout waiting for instrument to acknowledge bypass spot resume.  A filter change is required."),
                          true, info);

                    invalidateNormalization();
                    sampleState = SAMPLE_REQUIRE_FILTER_CHANGE;
                    forceRealtimeStateEmit = true;
                    forceFilterBreak = true;
                    break;
                } else {
                    event(endTime, QObject::tr(
                            "Timeout waiting for instrument to acknowledge bypass spot resume."),
                          true, info);
                }
            }

            sampleState = SAMPLE_RUN;

            discardEndTime = bypassResumeContaminate->apply(endTime, endTime, true);
            break;
        case SAMPLE_UNBYPASS_SPOT_NORMALIZE:
            if (!FP::defined(instrumentAcknowledgeTimeout)) {
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
            }
            if (instrumentAcknowledgeTimeout >= endTime) {
                if (spotNumber != bypassedSpot)
                    break;
                qCDebug(log) << "Left bypass to normalize mode at" << Logging::time(endTime);
            } else {
                qCDebug(log) << "Bypass end to normalize acknowledge timeout ("
                             << Logging::time(instrumentAcknowledgeTimeout) << ") at "
                             << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("UnbypassSpotNormalize");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "Filter", filterStart);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                event(endTime, QObject::tr(
                        "Timeout waiting for instrument to acknowledge bypass spot resume."), false,
                      info);
            }

            spotChangeCheck = spotNumber;
            normalizationSpot = spotNumber;
            sampleState = SAMPLE_SPOT_NORMALIZE;
            spotNormalizationStart();

            discardEndTime = bypassResumeContaminate->apply(endTime, endTime, true);
            break;

        case SAMPLE_REQUIRE_FILTER_CHANGE:
        case SAMPLE_REQUIRE_WHITE_FILTER_CHANGE:
            emitFilterEnd(endTime);

            if (INTEGER::defined(flags.read().toInt64()) &&
                    (flags.read().toInt64() & FLAG_CHANGING)) {
                qCDebug(log) << "Instrument initiated filter change at " << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                if (sampleState == SAMPLE_REQUIRE_FILTER_CHANGE)
                    info.hash("State").setString("RequireFilterChange");
                else
                    info.hash("State").setString("RequireWhiteFilterChange");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "Filter", filterStart);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                insertDiagnosticNormalizationStable(info);
                event(endTime, QObject::tr("Instrument initiated filter change started."), true,
                      info);

                if (controlStream != NULL && spotNumber != 0) {
                    qCDebug(log) << "Instrument has filter change flag set but is not on spot zero";
                    controlStream->writeControl("spot=0\rstop\r");
                    discardData(endTime + 0.5, 1);
                    timeoutAt(endTime + 3.0);
                }
                sampleState = SAMPLE_FILTER_CHANGE_START;
                forceFilterBreak = true;
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
                spotChangeCheck = spotNumber;
                break;
            }

            if (controlStream != NULL && autodetectStart(ID, I)) {
                qCDebug(log) << "Auto-detected filter change start at" << Logging::time(endTime);

                Variant::Write info = Variant::Write::empty();
                if (sampleState == SAMPLE_REQUIRE_FILTER_CHANGE)
                    info.hash("State").setString("RequireFilterChange");
                else
                    info.hash("State").setString("RequireWhiteFilterChange");
                insertDiagnosticCommonValues(info);
                insertDiagnosticFilterValues(info, "Filter", filterStart);
                insertDiagnosticFilterValues(info, "White", filterWhite);
                insertDiagnosticNormalizationStable(info);
                event(endTime, QObject::tr("Auto-detected filter change started."), true, info);

                spotChangeCheck = spotNumber;
                sampleState = SAMPLE_FILTER_CHANGE_START;
                forceFilterBreak = true;
                instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;

                controlStream->writeControl("stop\r");
                discardData(endTime + 0.5, 1);
                timeoutAt(endTime + 3.0);
                normalizationStartTime = FP::undefined();
                break;
            }

            if (spotNumber != 0 && controlStream != NULL) {
                controlStream->writeControl("spot=0\r");
                discardData(endTime + 0.5, 1);
                timeoutAt(endTime + 3.0);
            }

            break;

        case SAMPLE_BYPASSED_REQUIRE_FILTER_CHANGE:
        case SAMPLE_BYPASSED_REQUIRE_WHITE_FILTER_CHANGE:
            emitFilterEnd(endTime);
            break;
        }

        for (int color = 0; color < 3; color++) {
            if (spotNumber > 0 && spotNumber < 9) {
                if (haveAcceptedNormalization &&
                        sampleState == SAMPLE_RUN &&
                        spotNumber == normalizationSpot) {
                    Ir[color].write()
                             .setDouble(calculateIr(spotNormalization[color],
                                                    In[spotNumber - 1][color].read().toDouble()));
                } else {
                    Ir[color].write().setEmpty();
                }

                int sample = spotNumber;
                int reference = ((spotNumber % 2 == 0) ? 0 : 9);
                If[color].write()
                         .setDouble(calculateIfp(I[reference][color].read().toDouble(),
                                                 ID[reference].read().toDouble()));
                Ip[color].write()
                         .setDouble(calculateIfp(I[sample][color].read().toDouble(),
                                                 ID[sample].read().toDouble()));
            } else {
                Ir[color].write().setDouble(FP::undefined());
                If[color].write().setDouble(FP::undefined());
                Ip[color].write().setDouble(FP::undefined());
            }
        }
        remap("IrB", Ir[0]);
        remap("IrG", Ir[1]);
        remap("IrR", Ir[2]);
        remap("IfB", If[0]);
        remap("IfG", If[1]);
        remap("IfR", If[2]);
        remap("IpB", Ip[0]);
        remap("IpG", Ip[1]);
        remap("IpR", Ip[2]);

        if (spotNumber > 0 &&
                spotNumber < 9 &&
                sampleState == SAMPLE_RUN &&
                spotNumber == normalizationSpot) {
            double area = config.first().area[spotNumber - 1];
            L.write().setDouble(calculateL(Qt.read().toDouble(), area));
            for (int color = 0; color < 3; color++) {
                Ba[color].write()
                         .setDouble(calculateBa(priorIr[color], priorVolume,
                                                Ir[color].read().toDouble(), Qt.read().toDouble(),
                                                area));
            }

            if (FP::defined(discardEndTime) && discardEndTime > endTime) {
                outputFlags.write().applyFlag("ContaminateFilterDiscard");
            }
        } else {
            L.write().setEmpty();
            for (int color = 0; color < 3; color++) {
                Ba[color].write().setEmpty();
            }
        }

        remap("BaB", Ba[0]);
        remap("BaG", Ba[1]);
        remap("BaR", Ba[2]);
        remap("L", L);

        if (controlStream != NULL && sampleState == SAMPLE_RUN) {
            if (spotNumber < 8) {
                bool doAdvance = false;
                /* Check low transmittance advance */
                for (int color = 0; color < 3; color++) {
                    double threshold = config.first().spotAdvanceTr[color];
                    if (!FP::defined(threshold))
                        continue;
                    double check = Ir[color].read().toDouble();
                    if (FP::defined(check) && check < threshold) {
                        doAdvance = true;
                        break;
                    }
                }

                if (doAdvance) {
                    qCDebug(log) << "Advancing spot due to low transmittance at"
                                 << Logging::time(endTime);

                    Variant::Write info = Variant::Write::empty();
                    info.hash("State").setString("Run");
                    insertDiagnosticCommonValues(info);
                    insertDiagnosticFilterValues(info, "Filter", filterStart);
                    insertDiagnosticFilterValues(info, "White", filterWhite);
                    insertDiagnosticNormalization(info);
                    info.hash("AdvanceIrB").setDouble(config.first().spotAdvanceTr[0]);
                    info.hash("AdvanceIrG").setDouble(config.first().spotAdvanceTr[1]);
                    info.hash("AdvanceIrR").setDouble(config.first().spotAdvanceTr[2]);
                    event(endTime, QObject::tr("Spot advancing due to low transmittance."), false,
                          info);


                    QByteArray cmd("spot=");
                    cmd.append(QByteArray::number(spotNumber + 1));
                    cmd.append('\r');
                    controlStream->writeControl(cmd);
                    controlStream->writeControl(cmd);
                    spotSetTarget = spotNumber + 1;
                    discardData(endTime + 0.5, 1);
                    timeoutAt(endTime + 3.0);

                    spotChangeCheck = spotNumber;
                    sampleState = SAMPLE_SPOT_ADVANCE;
                    bypassedSpot = spotNumber + 1;
                    instrumentAcknowledgeTimeout = endTime + instrumentWaitAcknowledgeTime;
                }
            }
        }

        for (int color = 0; color < 3; color++) {
            double check = Ir[color].read().toDouble();
            if (!FP::defined(check))
                continue;
            if (check > 0.7)
                continue;

            switch (color) {
            case 0:
                outputFlags.write().applyFlag("BlueTransmittanceMedium");
                break;
            case 1:
                outputFlags.write().applyFlag("GreenTransmittanceMedium");
                break;
            case 2:
                outputFlags.write().applyFlag("RedTransmittanceMedium");
                break;
            default:
                Q_ASSERT(false);
                break;
            }

            if (check < 0.5) {
                switch (color) {
                case 0:
                    outputFlags.write().applyFlag("BlueTransmittanceLow");
                    break;
                case 1:
                    outputFlags.write().applyFlag("GreenTransmittanceLow");
                    break;
                case 2:
                    outputFlags.write().applyFlag("RedTransmittanceLow");
                    break;
                default:
                    Q_ASSERT(false);
                    break;
                }
            }
        }
    } while (priorSampleState != sampleState);

    for (int color = 0; color < 3; color++) {
        priorIr[color] = Ir[color].read().toDouble();
    }
    if (spotNumber > 0 && spotNumber < 9) {
        int sample = spotNumber;
        int reference = ((spotNumber % 2 == 0) ? 0 : 9);
        for (int color = 0; color < 3; color++) {
            priorIn[color] = calculateIn(I[reference][color].read().toDouble(),
                                         ID[reference].read().toDouble(),
                                         I[sample][color].read().toDouble(),
                                         ID[sample].read().toDouble());
        }
    }
    priorVolume = Qt.read().toDouble();

    if (INTEGER::defined(flags.read().toInt64())) {
        qint64 flagsBits(flags.read().toInt64());
        for (int i = 0;
                i <
                        (int) (sizeof(instrumentFlagTranslation) /
                                sizeof(instrumentFlagTranslation[0]));
                i++) {
            if (flagsBits & (Q_INT64_C(1) << i)) {
                if (instrumentFlagTranslation[i] != NULL) {
                    outputFlags.write().applyFlag(instrumentFlagTranslation[i]);
                } else {
                    qCDebug(log) << "Unrecognized bit " << hex << ((1 << i))
                                 << " set in instrument flags";
                }
            }
        }
    }
    if (filterIsNotWhite) {
        outputFlags.write().applyFlag("NonWhiteFilter");
    }

    if (persistentFn.read() != Fn.read()) {
        if (persistentEgress != NULL && FP::defined(persistentFn.getStart())) {
            persistentFn.setEnd(endTime);
            persistentEgress->incomingData(persistentFn);
            persistentFn.setEnd(FP::undefined());
        }
        persistentFn.setRoot(Fn);
        persistentFn.setStart(endTime);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "Fn"}, Fn, endTime, FP::undefined()));
        }
    } else if (forceRealtimeStateEmit && realtimeEgress != NULL) {
        realtimeEgress->incomingData(
                SequenceValue({{}, "raw", "Fn"}, Fn, endTime, FP::undefined()));
    }
    if (persistentFf.read() != Ff) {
        if (persistentEgress != NULL && FP::defined(persistentFf.getStart())) {
            persistentFf.setEnd(endTime);
            persistentEgress->incomingData(persistentFf);
            persistentFf.setEnd(FP::undefined());
        }
        persistentFf.setRoot(Ff);
        persistentFf.setStart(endTime);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "Ff"}, Ff, endTime, FP::undefined()));
        }
    } else if (forceRealtimeStateEmit && realtimeEgress != NULL) {
        realtimeEgress->incomingData(
                SequenceValue({{}, "raw", "Ff"}, Ff, endTime, FP::undefined()));
    }
    if (realtimeEgress != NULL && (initialSampleState != sampleState || forceRealtimeStateEmit)) {
        Variant::Root state;
        switch (sampleState) {
        case SAMPLE_RUN:
            state.write().setString("Run");
            break;
        case SAMPLE_SPOT_NORMALIZE:
            state.write().setString("Normalize");
            break;
        case SAMPLE_SPOT_ADVANCE:
            state.write().setString("SpotAdvance");
            break;
        case SAMPLE_FILTER_BASELINE_START:
            state.write().setString("FilterBaselineStart");
            break;
        case SAMPLE_FILTER_BASELINE:
            state.write().setString("FilterBaseline");
            break;
        case SAMPLE_FILTER_CHANGE_START:
            state.write().setString("FilterChangeStart");
            break;
        case SAMPLE_FILTER_CHANGING:
            state.write().setString("FilterChange");
            break;
        case SAMPLE_WHITE_FILTER_BASELINE_START:
            state.write().setString("WhiteFilterBaselineStart");
            break;
        case SAMPLE_WHITE_FILTER_BASELINE:
            state.write().setString("WhiteFilterBaseline");
            break;
        case SAMPLE_WHITE_FILTER_CHANGE_START:
            state.write().setString("WhiteFilterChangeStart");
            break;
        case SAMPLE_WHITE_FILTER_CHANGING:
            state.write().setString("WhiteFilterChange");
            break;
        case SAMPLE_BYPASSED_RUN:
            state.write().setString("BypassedRun");
            break;
        case SAMPLE_BYPASSED_SPOT_NORMALIZE:
            state.write().setString("BypassedNormalize");
            break;
        case SAMPLE_BYPASSED_FILTER_BASELINE_START:
            state.write().setString("BypassedFilterBaselineStart");
            break;
        case SAMPLE_BYPASSED_FILTER_BASELINE:
            state.write().setString("BypassedFilterBaseline");
            break;
        case SAMPLE_BYPASSED_FILTER_CHANGE_START:
            state.write().setString("BypassedFilterChangeStart");
            break;
        case SAMPLE_BYPASSED_FILTER_CHANGING:
            state.write().setString("BypassedFilterChange");
            break;
        case SAMPLE_BYPASSED_WHITE_FILTER_BASELINE_START:
            state.write().setString("BypassedWhiteFilterBaselineStart");
            break;
        case SAMPLE_BYPASSED_WHITE_FILTER_BASELINE:
            state.write().setString("BypassedWhiteFilterBaseline");
            break;
        case SAMPLE_BYPASSED_WHITE_FILTER_CHANGE_START:
            state.write().setString("BypassedWhiteFilterChangeStart");
            break;
        case SAMPLE_BYPASSED_WHITE_FILTER_CHANGING:
            state.write().setString("BypassedWhiteFilterChange");
            break;
        case SAMPLE_UNBYPASS_RUN:
            state.write().setString("UnbypassRun");
            break;
        case SAMPLE_UNBYPASS_SPOT_NORMALIZE:
            state.write().setString("UnbypassNormalize");
            break;
        case SAMPLE_REQUIRE_FILTER_CHANGE:
            state.write().setString("RequireFilterChange");
            break;
        case SAMPLE_REQUIRE_WHITE_FILTER_CHANGE:
            state.write().setString("RequireWhiteFilterChange");
            break;
        case SAMPLE_BYPASSED_REQUIRE_FILTER_CHANGE:
            state.write().setString("BypassedRequireFilterChange");
            break;
        case SAMPLE_BYPASSED_REQUIRE_WHITE_FILTER_CHANGE:
            state.write().setString("BypassedRequireWhiteFilterChange");
            break;
        }
        realtimeEgress->incomingData(
                SequenceValue({{}, "raw", "ZSTATE"}, state, endTime, FP::undefined()));
    }

    if (realtimeEgress != NULL)
        forceRealtimeStateEmit = false;

    logValue(startTime, endTime, "F1", Variant::Root(outputFlags));
    logValue(startTime, endTime, "Q", std::move(Q));
    logValue(startTime, endTime, "Qt", std::move(Qt));
    logValue(startTime, endTime, "T1", std::move(T1));
    logValue(startTime, endTime, "T2", std::move(T2));
    logValue(startTime, endTime, "BaB", std::move(Ba[0]));
    logValue(startTime, endTime, "BaG", std::move(Ba[1]));
    logValue(startTime, endTime, "BaR", std::move(Ba[2]));
    logValue(startTime, endTime, "IfB", std::move(If[0]));
    logValue(startTime, endTime, "IfG", std::move(If[1]));
    logValue(startTime, endTime, "IfR", std::move(If[2]));
    logValue(startTime, endTime, "IpB", std::move(Ip[0]));
    logValue(startTime, endTime, "IpG", std::move(Ip[1]));
    logValue(startTime, endTime, "IpR", std::move(Ip[2]));

    /* Force breaks in the data stream on filter changes */
    if (forceFilterBreak && requireFilterBreak) {
        realtimeValue(endTime, "L", Variant::Root(FP::undefined()));
        realtimeValue(endTime, "IrB", Variant::Root(FP::undefined()));
        realtimeValue(endTime, "IrG", Variant::Root(FP::undefined()));
        realtimeValue(endTime, "IrR", Variant::Root(FP::undefined()));
        requireFilterBreak = false;
    } else {
        logValue(startTime, endTime, "L", std::move(L));
        logValue(startTime, endTime, "IrB", std::move(Ir[0]));
        logValue(startTime, endTime, "IrG", std::move(Ir[1]));
        logValue(startTime, endTime, "IrR", std::move(Ir[2]));
        requireFilterBreak = true;
    }
    forceFilterBreak = false;

    realtimeValue(endTime, "VQ", std::move(VQ));
    realtimeValue(endTime, "ZF1", std::move(outputFlags));

    if (config.first().realtimeDiagnostics && realtimeEgress) {
        for (int i = 0; i < 10; i++) {
            realtimeValue(endTime, "I" + std::to_string(i + 1) + "D", std::move(ID[i]));
            realtimeValue(endTime, "I" + std::to_string(i + 1) + "B", std::move(I[i][0]));
            realtimeValue(endTime, "I" + std::to_string(i + 1) + "G", std::move(I[i][1]));
            realtimeValue(endTime, "I" + std::to_string(i + 1) + "R", std::move(I[i][2]));

            realtimeValue(endTime, "Ig" + std::to_string(i + 1) + "D",
                          Variant::Root(filterBaselineID[i]->stabilityFactor()));
            realtimeValue(endTime, "Ig" + std::to_string(i + 1) + "B",
                          Variant::Root(filterBaselineI[i][0]->stabilityFactor()));
            realtimeValue(endTime, "Ig" + std::to_string(i + 1) + "G",
                          Variant::Root(filterBaselineI[i][1]->stabilityFactor()));
            realtimeValue(endTime, "Ig" + std::to_string(i + 1) + "R",
                          Variant::Root(filterBaselineI[i][2]->stabilityFactor()));
        }
        for (int i = 0; i < 8; i++) {
            realtimeValue(endTime, "Ing" + std::to_string(i + 1) + "B",
                          Variant::Root(filterBaselineIn[i][0]->stabilityFactor()));
            realtimeValue(endTime, "Ing" + std::to_string(i + 1) + "G",
                          Variant::Root(filterBaselineIn[i][1]->stabilityFactor()));
            realtimeValue(endTime, "Ing" + std::to_string(i + 1) + "R",
                          Variant::Root(filterBaselineIn[i][2]->stabilityFactor()));

            realtimeValue(endTime, "Irw" + std::to_string(i + 1) + "B", Variant::Root(
                    calculateIr(filterWhite.In[i][0], In[i][0].read().toDouble())));
            realtimeValue(endTime, "Irw" + std::to_string(i + 1) + "G", Variant::Root(
                    calculateIr(filterWhite.In[i][1], In[i][1].read().toDouble())));
            realtimeValue(endTime, "Irw" + std::to_string(i + 1) + "R", Variant::Root(
                    calculateIr(filterWhite.In[i][2], In[i][2].read().toDouble())));

            realtimeValue(endTime, "Irz" + std::to_string(i + 1) + "B", Variant::Root(
                    calculateIr(filterStart.In[i][0], In[i][0].read().toDouble())));
            realtimeValue(endTime, "Irz" + std::to_string(i + 1) + "G", Variant::Root(
                    calculateIr(filterStart.In[i][1], In[i][1].read().toDouble())));
            realtimeValue(endTime, "Irz" + std::to_string(i + 1) + "R", Variant::Root(
                    calculateIr(filterStart.In[i][2], In[i][2].read().toDouble())));

            realtimeValue(endTime, "In" + std::to_string(i + 1) + "B", std::move(In[i][0]));
            realtimeValue(endTime, "In" + std::to_string(i + 1) + "G", std::move(In[i][1]));
            realtimeValue(endTime, "In" + std::to_string(i + 1) + "R", std::move(In[i][2]));
        }
    }

    return 0;
}