/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>

#include "core/component.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/composite.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_gmd_clap3w.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;
using namespace CPD3::Smoothing;

AcquireGMDCLAP3W::Configuration::Configuration() : start(FP::undefined()),
                                                   end(FP::undefined()),
                                                   hardwareFlowCal(QVector<double>()),
                                                   flowScale(1.0),
                                                   area(),
                                                   wavelengths(),
                                                   strictMode(false),
                                                   integrateVolume(false),
                                                   checkNormalizationInstrumentID(true),
                                                   enableAutodetectStart(true),
                                                   enableAutodetectEnd(true),
                                                   autodetectStartTriggers(2),
                                                   verifyWhiteBand(0.9),
                                                   spotAdvanceTr(),
                                                   autodetectStartIntensityLimit(1000.0),
                                                   autodetectStartDarkLimit(1000.0),
                                                   autodetectEndBand(0.9),
                                                   realtimeDiagnostics(true),
                                                   checkInstrumentTiming(true)
{
    for (int i = 0; i < 8; i++) {
        area[i] = 19.9;
    }
    wavelengths[0] = 467;
    wavelengths[1] = 528;
    wavelengths[2] = 652;
    spotAdvanceTr[0] = 0.5;
    spotAdvanceTr[1] = 0.7;
    spotAdvanceTr[2] = 0.5;
}

AcquireGMDCLAP3W::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          hardwareFlowCal(other.hardwareFlowCal),
          flowScale(other.flowScale),
          area(),
          wavelengths(),
          strictMode(other.strictMode),
          integrateVolume(other.integrateVolume),
          checkNormalizationInstrumentID(other.checkNormalizationInstrumentID),
          enableAutodetectStart(other.enableAutodetectStart),
          enableAutodetectEnd(other.enableAutodetectEnd),
          autodetectStartTriggers(other.autodetectStartTriggers),
          verifyWhiteBand(other.verifyWhiteBand),
          spotAdvanceTr(),
          autodetectStartIntensityLimit(other.autodetectStartIntensityLimit),
          autodetectStartDarkLimit(other.autodetectStartDarkLimit),
          autodetectEndBand(other.autodetectEndBand),
          realtimeDiagnostics(other.realtimeDiagnostics),
          checkInstrumentTiming(other.checkInstrumentTiming)
{
    memcpy(area, other.area, sizeof(area));
    memcpy(wavelengths, other.wavelengths, sizeof(wavelengths));
    memcpy(spotAdvanceTr, other.spotAdvanceTr, sizeof(spotAdvanceTr));
}

AcquireGMDCLAP3W::IntensityData::IntensityData() : startTime(FP::undefined())
{
    for (int detector = 0; detector < 10; detector++) {
        ID[detector] = FP::undefined();
        for (int color = 0; color < 3; color++) {
            I[detector][color] = FP::undefined();
        }
    }
    for (int spot = 0; spot < 8; spot++) {
        for (int color = 0; color < 3; color++) {
            In[spot][color] = FP::undefined();
        }
    }
}

QDataStream &operator<<(QDataStream &stream, const AcquireGMDCLAP3W::IntensityData &data)
{
    stream << data.startTime;
    for (int detector = 0; detector < 10; detector++) {
        stream << data.ID[detector];
        for (int color = 0; color < 3; color++) {
            stream << data.I[detector][color];
        }
    }
    for (int spot = 0; spot < 8; spot++) {
        for (int color = 0; color < 3; color++) {
            stream << data.In[spot][color];
        }
    }
    return stream;
}

QDataStream &operator>>(QDataStream &stream, AcquireGMDCLAP3W::IntensityData &data)
{
    stream >> data.startTime;
    for (int detector = 0; detector < 10; detector++) {
        stream >> data.ID[detector];
        for (int color = 0; color < 3; color++) {
            stream >> data.I[detector][color];
        }
    }
    for (int spot = 0; spot < 8; spot++) {
        for (int color = 0; color < 3; color++) {
            stream >> data.In[spot][color];
        }
    }
    return stream;
}

void AcquireGMDCLAP3W::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("GMD");
    instrumentMeta["Model"].setString("CLAP-3W");

    persistentFn.setUnit(SequenceName({}, "raw", "Fn"));
    persistentFf.setUnit(SequenceName({}, "raw", "Ff"));

    instrumentAcknowledgeTimeout = FP::undefined();
    spotChangeCheck = -1;
    spotSetTarget = -1;
    bypassedSpot = -1;

    baseVolume = 0;
    for (int color = 0; color < 3; color++) {
        spotNormalization[color] = FP::undefined();
    }
    filterIsNotWhite = false;
    normalizationStartTime = FP::undefined();
    normalizationFilterID = 0xFFFFFFFF;
    normalizationSpot = 0;
    normalizationLastVolume = FP::undefined();
    normalizationLastInstrumentTime = 0;
    normalizationSerialNumber.write().setEmpty();
    normalizationFirmwareVersion.write().setEmpty();

    haveAcceptedNormalization = true;
    havePriorInstrument = false;
    whiteFilterChangeStartTime = FP::undefined();

    discardEndTime = FP::undefined();
    priorVolume = FP::undefined();
    for (int color = 0; color < 3; color++) {
        priorIr[color] = FP::undefined();
        priorIn[color] = FP::undefined();
    }

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    forceRealtimeStateEmit = false;

    forceFilterBreak = false;
    requireFilterBreak = false;
}

AcquireGMDCLAP3W::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s),
          end(e),
          hardwareFlowCal(QVector<double>()),
          flowScale(1.0),
          area(),
          wavelengths(),
          strictMode(false),
          integrateVolume(false),
          checkNormalizationInstrumentID(true),
          enableAutodetectStart(true),
          enableAutodetectEnd(true),
          autodetectStartTriggers(2),
          verifyWhiteBand(0.9),
          spotAdvanceTr(),
          autodetectStartIntensityLimit(1000.0),
          autodetectStartDarkLimit(1000.0),
          autodetectEndBand(0.9),
          realtimeDiagnostics(true),
          checkInstrumentTiming(true)
{
    for (int i = 0; i < 8; i++) {
        area[i] = 19.9;
    }
    wavelengths[0] = 467;
    wavelengths[1] = 528;
    wavelengths[2] = 652;
    spotAdvanceTr[0] = 0.5;
    spotAdvanceTr[1] = 0.7;
    spotAdvanceTr[2] = 0.5;

    setFromSegment(other);
}

AcquireGMDCLAP3W::Configuration::Configuration(const Configuration &under,
                                               const ValueSegment &over,
                                               double s,
                                               double e) : start(s),
                                                           end(e),
                                                           hardwareFlowCal(under.hardwareFlowCal),
                                                           flowScale(under.flowScale),
                                                           area(),
                                                           wavelengths(),
                                                           strictMode(under.strictMode),
                                                           integrateVolume(under.integrateVolume),
                                                           checkNormalizationInstrumentID(
                                                                   under.checkNormalizationInstrumentID),
                                                           enableAutodetectStart(
                                                                   under.enableAutodetectStart),
                                                           enableAutodetectEnd(
                                                                   under.enableAutodetectEnd),
                                                           autodetectStartTriggers(
                                                                   under.autodetectStartTriggers),
                                                           verifyWhiteBand(under.verifyWhiteBand),
                                                           spotAdvanceTr(),
                                                           autodetectStartIntensityLimit(
                                                                   under.autodetectStartIntensityLimit),
                                                           autodetectStartDarkLimit(
                                                                   under.autodetectStartDarkLimit),
                                                           autodetectEndBand(
                                                                   under.autodetectEndBand),
                                                           realtimeDiagnostics(
                                                                   under.realtimeDiagnostics),
                                                           checkInstrumentTiming(
                                                                   under.checkInstrumentTiming)
{
    memcpy(area, under.area, sizeof(area));
    memcpy(wavelengths, under.wavelengths, sizeof(wavelengths));
    memcpy(spotAdvanceTr, under.spotAdvanceTr, sizeof(spotAdvanceTr));
    setFromSegment(over);
}

void AcquireGMDCLAP3W::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["HardwareFlowCalibration"].exists())
        hardwareFlowCal = Variant::Composite::toCalibration(config["HardwareFlowCalibration"]);
    if (FP::defined(config["FlowScale"].toDouble()) && config["FlowScale"].toDouble() > 0.0)
        flowScale = config["FlowScale"].toDouble();
    if (config["Area"].exists()) {
        if (config["Area"].getType() == Variant::Type::Array) {
            for (int spot = 0; spot < 8; spot++) {
                double a = config["Area"].array(spot).toDouble();
                if (FP::defined(a) && a > 0.0) {
                    if (a < 0.1)
                        a *= 1E6;
                    area[spot] = a;
                }
            }
        } else {
            double a = config["Area"].toDouble();
            if (FP::defined(a) && a > 0.0) {
                if (a < 0.1)
                    a *= 1E6;
                for (int spot = 0; spot < 8; spot++) {
                    area[spot] = a;
                }
            }
        }
    }

    if (FP::defined(config["Wavelength/B"].toDouble()))
        wavelengths[0] = config["Wavelength/B"].toDouble();
    if (FP::defined(config["Wavelength/G"].toDouble()))
        wavelengths[1] = config["Wavelength/G"].toDouble();
    if (FP::defined(config["Wavelength/R"].toDouble()))
        wavelengths[2] = config["Wavelength/R"].toDouble();

    if (config["IntegrateVolume"].exists())
        integrateVolume = config["IntegrateVolume"].toBool();

    if (config["StrictMode"].exists())
        strictMode = config["StrictMode"].toBool();

    if (config["Autodetect/Start/Enable"].exists())
        enableAutodetectStart = config["Autodetect/Start/Enable"].toBool();
    if (config["Autodetect/Start/Intensity"].exists())
        autodetectStartIntensityLimit = config["Autodetect/Start/Intensity"].toDouble();
    if (INTEGER::defined(config["Autodetect/Start/RequiredTriggers"].toInteger()))
        autodetectStartTriggers = config["Autodetect/Start/RequiredTriggers"].toInteger();
    if (config["Autodetect/Start/Dark"].exists())
        autodetectStartDarkLimit = config["Autodetect/Start/Dark"].toDouble();
    if (config["Autodetect/End/Enable"].exists())
        enableAutodetectEnd = config["Autodetect/End/Enable"].toBool();
    if (config["Autodetect/End/Band"].exists())
        autodetectEndBand = config["Autodetect/End/Band"].toDouble();

    if (config["Filter/VerifyWhiteBand"].exists())
        verifyWhiteBand = config["Filter/VerifyWhiteBand"].toDouble();
    if (config["Filter/CheckInstrumentID"].exists())
        checkNormalizationInstrumentID = config["Filter/CheckInstrumentID"].toBool();

    if (config["Spot/AdvanceTransmittance"].getType() == Variant::Type::Real) {
        double v = config["Spot/AdvanceTransmittance"].toDouble();
        for (int color = 0; color < 3; color++) {
            spotAdvanceTr[color] = v;
        }
    }
    if (config["Spot/AdvanceTransmittance/B"].exists())
        spotAdvanceTr[0] = config["Spot/AdvanceTransmittance/B"].toDouble();
    if (config["Spot/AdvanceTransmittance/G"].exists())
        spotAdvanceTr[1] = config["Spot/AdvanceTransmittance/G"].toDouble();
    if (config["Spot/AdvanceTransmittance/R"].exists())
        spotAdvanceTr[2] = config["Spot/AdvanceTransmittance/R"].toDouble();

    if (config["RealtimeDiagnostics"].exists())
        realtimeDiagnostics = config["RealtimeDiagnostics"].toBool();
    if (config["CheckInstrumentTiming"].exists())
        checkInstrumentTiming = config["CheckInstrumentTiming"].toBool();
}

AcquireGMDCLAP3W::AcquireGMDCLAP3W(const ValueSegment::Transfer &configData,
                                   const std::string &loggingContext) : FramedInstrument("clap3w",
                                                                                         loggingContext),
                                                                        lastRecordTime(
                                                                                FP::undefined()),
                                                                        autoprobeStatus(
                                                                                AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                        responseState(
                                                                                RESP_PASSIVE_WAIT),
                                                                        responseTry(0),
                                                                        sampleState(
                                                                                SAMPLE_SPOT_NORMALIZE)
{
    setDefaultInvalid();
    config.append(Configuration());

    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }

    Variant::Write defaultSmoother = Variant::Write::empty();
    defaultSmoother["Type"].setString("FixedTime");
    defaultSmoother["Time"].setDouble(90.0);
    defaultSmoother["MinimumTime"].setDouble(90.0);
    defaultSmoother["RSD"].setDouble(0.001);
    for (int detector = 0; detector < 10; detector++) {
        filterBaselineID[detector] =
                BaselineSmoother::fromConfiguration(defaultSmoother, configData, "Filter/Baseline");
        for (int color = 0; color < 3; color++) {
            filterBaselineI[detector][color] =
                    BaselineSmoother::fromConfiguration(defaultSmoother, configData,
                                                        "Filter/Baseline");
        }
    }
    for (int spot = 0; spot < 8; spot++) {
        for (int color = 0; color < 3; color++) {
            filterBaselineIn[spot][color] =
                    BaselineSmoother::fromConfiguration(defaultSmoother, configData,
                                                        "Filter/Baseline");
        }
    }

    defaultSmoother["Time"].setDouble(60.0);
    defaultSmoother["MinimumTime"].setDouble(30.0);
    defaultSmoother["DiscardTime"].setDouble(8.0);
    for (int color = 0; color < 3; color++) {
        spotNormalizationIn[color] =
                BaselineSmoother::fromConfiguration(defaultSmoother, configData, "Spot/Normalize");
    }

    DynamicTimeInterval::Variable *defaultTime = new DynamicTimeInterval::Variable;
    defaultTime->set(Time::Second, 30, true);
    bypassResumeContaminate =
            DynamicTimeInterval::fromConfiguration(configData, "BypassResumeContaminate",
                                                   FP::undefined(), FP::undefined(), true,
                                                   defaultTime);

    defaultTime->set(Time::Minute, 30, false);
    acceptNormalizationTime =
            DynamicTimeInterval::fromConfiguration(configData, "Spot/ResumeTimeout",
                                                   FP::undefined(), FP::undefined(), true,
                                                   defaultTime);

    delete defaultTime;
}

void AcquireGMDCLAP3W::setToAutoprobe()
{ responseState = RESP_AUTOPROBE_PASSIVE_INITIALIZE; }

void AcquireGMDCLAP3W::setToInteractive()
{ responseState = RESP_INTERACTIVE_INITIALIZE; }


ComponentOptions AcquireGMDCLAP3WComponent::getBaseOptions()
{
    ComponentOptions options;

    for (int spot = 0; spot < 8; spot++) {
        ComponentOptionSingleDouble *area =
                new ComponentOptionSingleDouble(tr("spot%1", "name").arg(spot + 1),
                                                tr("Area of spot %1").arg(spot + 1),
                                                tr("This is the area for spot %1.  If it is greater than 0.1 "
                                                       "it is assumed to be in mm\xC2\xB2, otherwise it is treated as "
                                                       "being in m\xC2\xB2."),
                                                tr("19.9 mm\xC2\xB2"), 1);
        area->setMinimum(0.0, false);
        options.add(QString("area%1").arg(spot), area);
    }

    ComponentOptionSingleDouble *d =
            new ComponentOptionSingleDouble(tr("cal-q", "name"), tr("Flow rate scalar"),
                                            tr("This is the constant multiplier to the reported flow and "
                                               "volume.  This is applied after the hardware calibrates the "
                                               "voltage to a flow rate and integrates that to a volume."),
                                            tr("1.0", "default flow scale"), 1);
    d->setMinimum(0.0, false);
    options.add("cal-q", d);

    BaselineSmootherOption *smoother = new BaselineSmootherOption(tr("filter-baseline", "name"),
                                                                  tr("Filter baseline smoother"),
                                                                  tr("This defines the smoothing used to establish a baseline at the "
                                                                     "start of a filter."),
                                                                  tr("90 second duration with <0.001 RSD",
                                                                     "filter smoother"));
    smoother->setDefault(new BaselineFixedTime(90, 90, 0.001, FP::undefined(), 0));
    smoother->setSpikeDetection(false);
    smoother->setStabilityDetection(true);
    options.add("filter-baseline", smoother);

    smoother = new BaselineSmootherOption(tr("spot-normalize", "name"),
                                          tr("Spot normalization smoother"),
                                          tr("This defines the smoothing used to establish a normalization at "
                                             "the start of a spot."),
                                          tr("60 seconds max, 30 seconds min, 8 second delay, <0.001 RSD",
                                             "normalize smoother"));
    smoother->setDefault(new BaselineFixedTime(30, 60, 0.001, FP::undefined(), 8.0));
    smoother->setSpikeDetection(false);
    smoother->setStabilityDetection(true);
    options.add("spot-normalize", smoother);

    return options;
}

AcquireGMDCLAP3W::AcquireGMDCLAP3W(const ComponentOptions &options,
                                   const std::string &loggingContext) : FramedInstrument("clap3w",
                                                                                         loggingContext),
                                                                        lastRecordTime(
                                                                                FP::undefined()),
                                                                        autoprobeStatus(
                                                                                AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                        responseState(
                                                                                RESP_PASSIVE_WAIT),
                                                                        responseTry(0),
                                                                        sampleState(
                                                                                SAMPLE_SPOT_NORMALIZE)
{
    setDefaultInvalid();
    config.append(Configuration());

    for (int spot = 0; spot < 8; spot++) {
        if (options.isSet(QString("area%1").arg(spot))) {
            double value = qobject_cast<ComponentOptionSingleDouble *>(
                    options.get(QString("area%1").arg(spot)))->get();
            Q_ASSERT(FP::defined(value) && value > 0.0);
            if (value < 0.1)
                value *= 1E6;
            config.first().area[spot] = value;
        }
    }

    if (options.isSet("cal-q-hardware")) {
        config.first().hardwareFlowCal = qobject_cast<ComponentOptionSingleCalibration *>(
                options.get("cal-q-hardware"))->get();
    }
    if (options.isSet("cal-q")) {
        config.first().flowScale =
                qobject_cast<ComponentOptionSingleDouble *>(options.get("cal-q"))->get();
    }

    BaselineSmootherOption *smootherOption =
            qobject_cast<BaselineSmootherOption *>(options.get("filter-baseline"));
    for (int detector = 0; detector < 10; detector++) {
        filterBaselineID[detector] = smootherOption->getSmoother();
        for (int color = 0; color < 3; color++) {
            filterBaselineI[detector][color] = smootherOption->getSmoother();
        }
    }
    for (int spot = 0; spot < 8; spot++) {
        for (int color = 0; color < 3; color++) {
            filterBaselineIn[spot][color] = smootherOption->getSmoother();
        }
    }

    smootherOption = qobject_cast<BaselineSmootherOption *>(options.get("spot-normalize"));
    for (int color = 0; color < 3; color++) {
        spotNormalizationIn[color] = smootherOption->getSmoother();
    }

    /* Set these so it never rejects things (since it's under external/no
     * control). */
    for (int color = 0; color < 3; color++) {
        config.first().spotAdvanceTr[color] = FP::undefined();
    }
    config.first().enableAutodetectStart = false;
    config.first().enableAutodetectEnd = false;
    config.first().checkNormalizationInstrumentID = false;
    config.first().autodetectStartIntensityLimit = FP::undefined();
    config.first().autodetectStartDarkLimit = FP::undefined();
    config.first().autodetectEndBand = FP::undefined();
    acceptNormalizationTime = new DynamicTimeInterval::Undefined;
    bypassResumeContaminate = new DynamicTimeInterval::Constant(Time::Second, 30, true);
}

AcquireGMDCLAP3W::~AcquireGMDCLAP3W()
{
    for (int detector = 0; detector < 10; detector++) {
        delete filterBaselineID[detector];
        for (int color = 0; color < 3; color++) {
            delete filterBaselineI[detector][color];
        }
    }
    for (int spot = 0; spot < 8; spot++) {
        for (int color = 0; color < 3; color++) {
            delete filterBaselineIn[spot][color];
        }
    }
    for (int color = 0; color < 3; color++) {
        delete spotNormalizationIn[color];
    }
    delete bypassResumeContaminate;
    delete acceptNormalizationTime;
}

void AcquireGMDCLAP3W::logValue(double startTime,
                                double endTime,
                                SequenceName::Component name,
                                Variant::Root &&value)
{
    if (!loggingEgress && !realtimeEgress)
        return;
    SequenceValue dv({{}, "raw", std::move(name)}, std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        if (FP::defined(startTime))
            loggingEgress->incomingData(std::move(dv));
        return;
    }
    if (loggingEgress && FP::defined(startTime))
        loggingEgress->incomingData(dv);
    if (FP::defined(endTime)) {
        dv.setStart(endTime);
        dv.setEnd(endTime + 1.0);
    } else if (FP::defined(startTime)) {
        dv.setEnd(startTime + 1.0);
    }
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireGMDCLAP3W::realtimeValue(double time,
                                     SequenceName::Component name,
                                     Variant::Root &&value)
{
    if (!realtimeEgress)
        return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

bool AcquireGMDCLAP3W::autodetectStart(const Variant::Root ID[10],
                                       const Variant::Root I[10][3]) const
{
    if (!config.first().enableAutodetectStart || config.first().autodetectStartTriggers <= 0)
        return false;

    int detectedTriggers = 0;

    double darkLimit = config.first().autodetectStartDarkLimit;
    if (FP::defined(darkLimit) && darkLimit > 0.0) {
        for (int detector = 0; detector < 10; detector++) {
            double value = ID[detector].read().toDouble();
            if (!FP::defined(value))
                continue;
            if (fabs(value) > darkLimit)
                ++detectedTriggers;
        }
    }

    double intensityLimit = config.first().autodetectStartIntensityLimit;
    if (FP::defined(intensityLimit) && intensityLimit > 0.0) {
        for (int detector = 0; detector < 10; detector++) {
            for (int color = 0; color < 3; color++) {
                double value = I[detector][color].read().toDouble();
                if (!FP::defined(value))
                    continue;
                if (value < intensityLimit)
                    ++detectedTriggers;
            }
        }
    }

    return detectedTriggers >= config.first().autodetectStartTriggers;
}

bool AcquireGMDCLAP3W::filterBaseline() const
{
    for (int detector = 0; detector < 10; detector++) {
        if (!filterBaselineID[detector]->ready())
            return false;
        for (int color = 0; color < 3; color++) {
            if (!filterBaselineI[detector][color]->ready())
                return false;
        }
    }
    for (int spot = 0; spot < 8; spot++) {
        for (int color = 0; color < 3; color++) {
            if (!filterBaselineIn[spot][color]->ready())
                return false;
            if (!filterBaselineIn[spot][color]->stable())
                return false;
        }
    }
    return true;
}

void AcquireGMDCLAP3W::resetFilterBaseline()
{
    for (int detector = 0; detector < 10; detector++) {
        filterBaselineID[detector]->reset();
        for (int color = 0; color < 3; color++) {
            filterBaselineI[detector][color]->reset();
        }
    }
    for (int spot = 0; spot < 8; spot++) {
        for (int color = 0; color < 3; color++) {
            filterBaselineIn[spot][color]->reset();
        }
    }
}

void AcquireGMDCLAP3W::spotNormalizationStart()
{
    for (int color = 0; color < 3; color++) {
        spotNormalizationIn[color]->reset();
    }
}

bool AcquireGMDCLAP3W::autodetectEnd() const
{
    if (!config.first().enableAutodetectEnd)
        return false;
    if (!filterBaseline())
        return false;

    /* Ensure it's within the bounds for the autodetect start so we don't just
     * toggle back and forth between states.  This will also catch the case of
     * the lid on sideways (always amusing!) */
    double darkLimit = config.first().autodetectStartDarkLimit;
    if (FP::defined(darkLimit) && darkLimit > 0.0) {
        for (int detector = 0; detector < 10; detector++) {
            double value = filterBaselineID[detector]->value();
            if (!FP::defined(value))
                continue;
            if (fabs(value) > darkLimit)
                return false;
        }
    }
    double intensityLimit = config.first().autodetectStartIntensityLimit;
    if (FP::defined(intensityLimit) && intensityLimit > 0.0) {
        for (int detector = 0; detector < 10; detector++) {
            for (int color = 0; color < 3; color++) {
                double value = filterBaselineI[detector][color]->value();
                if (!FP::defined(value))
                    continue;
                if (value < intensityLimit)
                    return false;
            }
        }
    }

    double checkBand = config.first().autodetectEndBand;
    if (FP::defined(checkBand)) {
        for (int detector = 0; detector < 10; detector++) {
            for (int color = 0; color < 3; color++) {
                if (!FP::defined(filterWhite.I[detector][color]))
                    continue;
                double value = filterBaselineI[detector][color]->value();
                if (!FP::defined(value))
                    continue;
                if (!BaselineSmoother::inRelativeBand(filterWhite.I[detector][color], value,
                                                      checkBand))
                    return false;
            }
        }

        for (int spot = 0; spot < 8; spot++) {
            for (int color = 0; color < 3; color++) {
                if (!FP::defined(filterWhite.In[spot][color]))
                    continue;
                double value = filterBaselineIn[spot][color]->value();
                if (!FP::defined(value))
                    continue;
                if (!BaselineSmoother::inRelativeBand(filterWhite.In[spot][color], value,
                                                      checkBand))
                    return false;
            }
        }
    }

    return true;
}

bool AcquireGMDCLAP3W::normalizationDone() const
{
    for (int color = 0; color < 3; color++) {
        if (!spotNormalizationIn[color]->ready())
            return false;
        if (!spotNormalizationIn[color]->stable())
            return false;
    }
    return true;
}

bool AcquireGMDCLAP3W::whiteFilterTimeout(double time) const
{
    if (!FP::defined(whiteFilterChangeStartTime))
        return false;
    return time > whiteFilterChangeStartTime + 3600.0;
}

bool AcquireGMDCLAP3W::isSameNormalization(double currentTime,
                                           qint64 Ff,
                                           int spotNumber,
                                           qint64 etime) const
{
    if (!INTEGER::defined(Ff))
        return false;
    if ((quint32) Ff != normalizationFilterID)
        return false;
    if (spotNumber != 0 && (quint8) spotNumber != normalizationSpot)
        return false;
    if (!INTEGER::defined(etime))
        return false;
    if ((quint32) etime < normalizationLastInstrumentTime)
        return false;
    if (FP::defined(currentTime) && FP::defined(normalizationLastTime)) {
        double checkTime =
                acceptNormalizationTime->applyConst(normalizationLastTime, normalizationLastTime,
                                                    true);
        if (FP::defined(checkTime) && checkTime <= currentTime)
            return false;
    }
    return true;
}

bool AcquireGMDCLAP3W::isSameInstrument(double currentTime) const
{
    Q_UNUSED(currentTime);
    if (config.first().checkNormalizationInstrumentID) {
        if (normalizationFirmwareVersion.read().exists() &&
                instrumentMeta["FirmwareVersion"].exists()) {
            if (instrumentMeta["FirmwareVersion"] != normalizationFirmwareVersion.read())
                return false;
        }
        if (normalizationSerialNumber.read().exists() && instrumentMeta["SerialNumber"].exists()) {
            if (instrumentMeta["SerialNumber"] != normalizationSerialNumber)
                return false;
        }
    }
    if (havePriorInstrument) {
        if (instrumentMeta["FirmwareVersion"] != priorInstrumentFirmwareVersion)
            return false;
        if (instrumentMeta["SerialNumber"] != priorInstrumentSerialNumber)
            return false;
    }
    return true;
}

bool AcquireGMDCLAP3W::isSameFilter(double currentTime) const
{
    Q_UNUSED(currentTime);
    return true;
}

void AcquireGMDCLAP3W::invalidateNormalization()
{
    baseVolume = 0;
    for (int color = 0; color < 3; color++) {
        spotNormalization[color] = FP::undefined();
    }
    filterIsNotWhite = false;
    normalizationStartTime = FP::undefined();
    normalizationFilterID = 0;
    normalizationSpot = 0;
    normalizationLastVolume = FP::undefined();
    normalizationLastInstrumentTime = 0;
}

void AcquireGMDCLAP3W::saveFilterStart(double time)
{
    filterStart.startTime = time;
    for (int detector = 0; detector < 10; detector++) {
        filterStart.ID[detector] = filterBaselineID[detector]->value();
        for (int color = 0; color < 3; color++) {
            filterStart.I[detector][color] = filterBaselineI[detector][color]->value();
        }
    }
    for (int spot = 0; spot < 8; spot++) {
        for (int color = 0; color < 3; color++) {
            filterStart.In[spot][color] = filterBaselineIn[spot][color]->value();
        }
    }
}

bool AcquireGMDCLAP3W::isWhiteFilter() const
{
    double checkBand = config.first().verifyWhiteBand;
    if (FP::defined(checkBand)) {
        for (int spot = 0; spot < 8; spot++) {
            for (int color = 0; color < 3; color++) {
                if (!FP::defined(filterStart.In[spot][color]))
                    continue;
                if (!FP::defined(filterWhite.In[spot][color]))
                    continue;
                if (!BaselineSmoother::inRelativeBand(filterWhite.In[spot][color],
                                                      filterStart.In[spot][color], checkBand))
                    return false;
            }
        }
    }
    return true;
}

bool AcquireGMDCLAP3W::requireFilterChangeOnSpotFailure() const
{
    switch (responseState) {
    case RESP_UNPOLLED_RUN:
    case RESP_UNPOLLED_DISCARD:
        return true;
    default:
        break;
    }
    return false;
}

void AcquireGMDCLAP3W::setInvalidFilterStart()
{
    filterStart.startTime = FP::undefined();
    for (int detector = 0; detector < 10; detector++) {
        filterStart.ID[detector] = FP::undefined();
        for (int color = 0; color < 3; color++) {
            filterStart.I[detector][color] = FP::undefined();
        }
    }
    for (int spot = 0; spot < 8; spot++) {
        for (int color = 0; color < 3; color++) {
            filterStart.In[spot][color] = FP::undefined();
        }
    }
}

Variant::Root AcquireGMDCLAP3W::buildFilterValue(const IntensityData &data) const
{
    Variant::Root result;

    for (std::size_t detector = 0; detector < 10; detector++) {
        result["ID"].array(detector).setDouble(data.ID[detector]);
        result["IB"].array(detector).setDouble(data.I[detector][0]);
        result["IG"].array(detector).setDouble(data.I[detector][1]);
        result["IR"].array(detector).setDouble(data.I[detector][2]);
    }
    for (std::size_t spot = 0; spot < 8; spot++) {
        result["InB"].array(spot).setDouble(data.In[spot][0]);
        result["InG"].array(spot).setDouble(data.In[spot][1]);
        result["InR"].array(spot).setDouble(data.In[spot][2]);
    }

    return result;
}

void AcquireGMDCLAP3W::emitFilterEnd(double endTime)
{
    if (!FP::defined(filterStart.startTime))
        return;
    double startTime = filterStart.startTime;
    filterStart.startTime = FP::undefined();
    if (persistentEgress == NULL)
        return;

    SequenceValue result
            (SequenceName({}, "raw", "ZFILTER"), buildFilterValue(filterStart), startTime, endTime);
    persistentEgress->incomingData(result);
}

void AcquireGMDCLAP3W::emitWhiteEnd(double endTime)
{
    if (!FP::defined(filterWhite.startTime))
        return;
    double startTime = filterWhite.startTime;
    filterWhite.startTime = FP::undefined();
    if (persistentEgress == NULL)
        return;

    SequenceValue result
            (SequenceName({}, "raw", "ZWHITE"), buildFilterValue(filterWhite), startTime, endTime);
    persistentEgress->incomingData(result);
}

void AcquireGMDCLAP3W::emitNormalizationEnd(double endTime)
{
    if (!FP::defined(normalizationStartTime))
        return;
    double startTime = normalizationStartTime;
    normalizationStartTime = FP::undefined();
    if (persistentEgress == NULL)
        return;

    SequenceValue
            result(SequenceName({}, "raw", "ZSPOT"), buildNormalizationValue(), startTime, endTime);
    persistentEgress->incomingData(result);
}

static bool hardwareCalibrationsEqual(const Calibration &a, const Calibration &b)
{
    if (a.size() != b.size())
        return false;
    for (int i = 0, max = a.size(); i < max; i++) {
        double ca = a.get(i);
        double cb = b.get(i);
        double delta = ::fabs(ca - cb);
        double norm = qMax(::fabs(ca), ::fabs(cb));
        if (norm <= 0.0)
            norm = 1.0;
        delta /= norm;
        if (delta > 1E-5)
            return false;
    }
    return true;
}

void AcquireGMDCLAP3W::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    double startTime;
    if (!FP::defined(lastRecordTime)) {
        startTime = FP::undefined();
        lastRecordTime = frameTime;
    } else {
        startTime = lastRecordTime;
        if (lastRecordTime != frameTime)
            lastRecordTime = frameTime;
    }

    bool hardwareCalChanged = false;
    {
        Q_ASSERT(!config.isEmpty());
        int oldSize = config.size();
        Calibration oldCal(config.first().hardwareFlowCal);
        double oldWavelengths[3];
        std::copy(&config.first().wavelengths[0], &config.first().wavelengths[3],
                  &oldWavelengths[0]);
        if (!Range::intersectShift(config, frameTime)) {
            qCWarning(log) << "Can't find active configuration (list corrupted)";
        }
        Q_ASSERT(!config.isEmpty());
        if (oldSize != config.size()) {
            hardwareCalChanged = (config.first().hardwareFlowCal != oldCal);
            if (!std::equal(&config.first().wavelengths[0], &config.first().wavelengths[3],
                            &oldWavelengths[0])) {
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
            }
        }
    }

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
        if (processRecord(frame, startTime, frameTime) == 0) {
            qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

            responseState = RESP_PASSIVE_RUN;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            timeoutAt(frameTime + 2.0);

            recordTimeCheck.clear();
            recordTimeCheck.push_back(frameTime);
            forceRealtimeStateEmit = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveAutoprobeWait");
            event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);
        } else {
            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;
    case RESP_PASSIVE_WAIT:
        if (processRecord(frame, startTime, frameTime) == 0) {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);
            responseState = RESP_PASSIVE_RUN;
            timeoutAt(frameTime + 2.0);
            generalStatusUpdated();

            recordTimeCheck.clear();
            recordTimeCheck.push_back(frameTime);
            forceRealtimeStateEmit = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveWait");
            event(frameTime, QObject::tr("Passive communications established."), false, info);
        } else {
            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;

    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID: {
        int code = processRecord(frame, startTime, frameTime);
        if (code == 0) {
            qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

            timeoutAt(frameTime + 2.0);
            responseState = RESP_UNPOLLED_RUN;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            recordTimeCheck.clear();
            recordTimeCheck.push_back(frameTime);
            forceRealtimeStateEmit = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            event(frameTime, QObject::tr("Communications established."), false, info);
        } else if (code > 0) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "rejected with code" << code;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidRecord"),
                                      frameTime, FP::undefined()));
            }

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Communications start rejected the first line (code %1).").arg(code),
                  true, info);

            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        } else {
            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;
    }

    case RESP_PASSIVE_RUN:
    case RESP_UNPOLLED_RUN: {
        int code = processRecord(frame, startTime, frameTime);
        if (code == 0) {
            if (responseState == RESP_UNPOLLED_RUN && hardwareCalChanged) {
                if (controlStream != NULL) {
                    controlStream->writeControl("hide\r");
                    discardData(frameTime + 0.5, 1);
                    timeoutAt(frameTime + 3.0);
                }
                responseState = RESP_INTERACTIVE_START_SETTINGS_MAIN;
                discardData(frameTime + 0.5);
                timeoutAt(frameTime + 10.0);
                generalStatusUpdated();
            } else {
                timeoutAt(frameTime + 2.0);
            }

            if (config.first().checkInstrumentTiming) {
                recordTimeCheck.push_back(frameTime);
                if (std::fabs(recordTimeCheck.back() - recordTimeCheck.front()) >= 3600.0) {
                    if (recordTimeCheck.size() < 3240 || recordTimeCheck.size() >= 3660) {
                        Variant::Write info = Variant::Write::empty();
                        info.hash("Passive").setBool(responseState == RESP_PASSIVE_RUN);
                        info.hash("FirstTime").setDouble(recordTimeCheck.front());
                        info.hash("LastTime").setDouble(recordTimeCheck.back());
                        info.hash("Count").setInt64(recordTimeCheck.size());
                        event(frameTime, QObject::tr(
                                "Instrument clock error: received %1 records in the last hour.").arg(
                                recordTimeCheck.size()), true, info);
                    }
                    qCDebug(log) << "In" << Logging::elapsed(
                                recordTimeCheck.back() - recordTimeCheck.front()) << "got"
                                 << recordTimeCheck.size() << "record(s)";
                    recordTimeCheck.clear();
                } else if (recordTimeCheck.size() > 3660) {
                    Variant::Write info = Variant::Write::empty();
                    info.hash("Passive").setBool(responseState == RESP_PASSIVE_RUN);
                    info.hash("FirstTime").setDouble(recordTimeCheck.front());
                    info.hash("LastTime").setDouble(recordTimeCheck.back());
                    info.hash("Count").setInt64(recordTimeCheck.size());
                    event(frameTime, QObject::tr(
                            "Instrument clock error: received %1 records in less than an hour").arg(
                            recordTimeCheck.size()), true, info);
                    qCDebug(log) << "In" << Logging::elapsed(
                                recordTimeCheck.back() - recordTimeCheck.front()) << "got"
                                 << recordTimeCheck.size() << "record(s)";
                    recordTimeCheck.clear();
                }
            } else {
                recordTimeCheck.clear();
            }
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            Variant::Write info = Variant::Write::empty();

            if (responseState == RESP_PASSIVE_RUN) {
                timeoutAt(FP::undefined());
                responseState = RESP_PASSIVE_WAIT;
                info.hash("ResponseState").setString("PassiveRun");
            } else {
                if (controlStream != NULL) {
                    controlStream->writeControl(Util::ByteArray::filled('\r', 16));
                    controlStream->writeControl("\r\r\rmain\r\rmain\r");
                }
                responseState = RESP_INTERACTIVE_START_STOPREPORTS_MAIN;
                discardData(frameTime + 1.0);
                timeoutAt(frameTime + 10.0);
                info.hash("ResponseState").setString("Run");
            }
            generalStatusUpdated();

            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        } else if (responseState == RESP_UNPOLLED_RUN && hardwareCalChanged) {
            if (controlStream != NULL) {
                controlStream->writeControl("hide\r");
            }
            responseState = RESP_INTERACTIVE_START_SETTINGS_MAIN;
            discardData(frameTime + 1.0);
            timeoutAt(frameTime + 10.0);
            generalStatusUpdated();
        }
        break;
    }

    case RESP_PASSIVE_DISCARD:
        responseState = RESP_PASSIVE_RUN;
        discardData(frameTime + 0.5, 1);
        timeoutAt(frameTime + 3.0);
        break;

    case RESP_UNPOLLED_DISCARD:
        responseState = RESP_UNPOLLED_RUN;
        discardData(frameTime + 0.5, 1);
        timeoutAt(frameTime + 3.0);
        break;

    case RESP_INTERACTIVE_START_CONFIG_READSN: {
        auto equalsIndex = frame.indexOf('=');
        if (equalsIndex == frame.npos || equalsIndex >= frame.size() - 1) {
            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            qCDebug(log) << "Invalid serial number response" << frame << "at"
                         << Logging::time(frameTime);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
        } else {
            QString str(QString::fromLatin1(frame.mid(equalsIndex + 1).toQByteArrayRef()));
            QRegExp re("\\s*10\\.(\\d{3})\\s*");
            int sn = -1;
            bool ok;
            Variant::Root oldValue(instrumentMeta["SerialNumber"]);
            if (re.exactMatch(str) && (sn = re.cap(1).toInt(&ok, 10)) > 0 && ok) {
                instrumentMeta["SerialNumber"].setInt64(sn);
            } else if ((sn = str.toInt(&ok, 10)) > 0 && ok) {
                instrumentMeta["SerialNumber"].setInt64(sn);
            } else {
                instrumentMeta["SerialNumber"].setString(str.trimmed());
            }

            timeoutAt(frameTime + 2.0);
            discardData(frameTime + 0.5);
            responseState = RESP_INTERACTIVE_START_CONFIG_FW;

            if (oldValue.read() != instrumentMeta["SerialNumber"]) {
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadFirmwareVersionFlushing"), frameTime,
                                                           FP::undefined()));
            }
        }
    }
        break;

    case RESP_INTERACTIVE_START_CONFIG_READFW: {
        auto equalsIndex = frame.indexOf('=');
        if (equalsIndex == frame.npos || equalsIndex >= frame.size() - 1) {
            qCDebug(log) << "Invalid firmware version response" << frame << "at"
                         << Logging::time(frameTime);

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
        } else {
            QString str(QString::fromLatin1(frame.mid(equalsIndex + 1).toQByteArrayRef()));
            QRegExp re("\\s*10\\.(\\d{3})\\s*");
            int fw = -1;
            bool ok;
            Variant::Root oldValue(instrumentMeta["FirmwareVersion"]);
            if (re.exactMatch(str) && (fw = re.cap(1).toInt(&ok, 10)) > 0 && ok) {
                instrumentMeta["FirmwareVersion"].setInt64(fw);
            } else {
                instrumentMeta["FirmwareVersion"].setString(str.trimmed());
            }

            timeoutAt(frameTime + 10.0);
            discardData(frameTime + 0.5);
            responseState = RESP_INTERACTIVE_START_SETTINGS_WAIT;

            if (oldValue.read() != instrumentMeta["FirmwareVersion"]) {
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveMainMenuReturnFlushing"), frameTime, FP::undefined()));
            }
        }
    }
        break;

    case RESP_INTERACTIVE_START_SETTINGS_READFLOW: {
        auto equalsIndex = frame.indexOf('=');
        if (equalsIndex == frame.npos || equalsIndex >= frame.size() - 1) {
            qCDebug(log) << "Invalid flow calibration response" << frame << "at"
                         << Logging::time(frameTime);

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
        } else {
            QString str(frame.mid(equalsIndex + 1).toQByteArrayRef());
            QStringList parts(str.split(QRegExp("[:;,]")));
            Calibration cal;
            cal.clear();
            for (QStringList::const_iterator add = parts.constBegin(), end = parts.constEnd();
                    add != end;
                    ++add) {
                bool ok;
                double value = add->trimmed().toDouble(&ok);
                if (ok && FP::defined(value))
                    cal.append(value);
            }
            Q_ASSERT(!config.isEmpty());

            if (config.first().hardwareFlowCal.size() == 0) {
                config.first().hardwareFlowCal = cal;

                responseState = RESP_INTERACTIVE_START_UNPOLLED_WAIT;
                timeoutAt(frameTime + 10.0);
                discardData(frameTime + 0.5);

                if (realtimeEgress != NULL) {
                    realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                            "StartInteractiveUnpolledStartFlushing"), frameTime, FP::undefined()));
                }
            } else if (hardwareCalibrationsEqual(cal, config.first().hardwareFlowCal)) {
                responseState = RESP_INTERACTIVE_START_UNPOLLED_WAIT;
                timeoutAt(frameTime + 10.0);
                discardData(frameTime + 0.5);

                if (realtimeEgress != NULL) {
                    realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                            "StartInteractiveUnpolledStartFlushing"), frameTime, FP::undefined()));
                }
            } else if (responseTry < 5) {
                responseTry++;
                if (controlStream != NULL) {
                    Util::ByteArray wr("flow=");
                    for (int i = 0, max = config.first().hardwareFlowCal.size(); i < max; i++) {
                        if (i != 0) wr.push_back(',');
                        wr += QByteArray::number(config.first().hardwareFlowCal.get(i), 'E', 8);
                    }
                    wr.push_back('\r');
                    controlStream->writeControl(std::move(wr));
                }
                responseState = RESP_INTERACTIVE_START_SETTINGS_WRITEFLOW;
                timeoutAt(frameTime + 10.0);
                discardData(frameTime + 0.5);

                if (realtimeEgress != NULL) {
                    realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                            "StartInteractiveWriteHardwareFlowCalibrationFlushing"), frameTime,
                                                               FP::undefined()));
                }
            } else {
                qCDebug(log) << "Invalid flow calibration response" << frame << "at"
                             << Logging::time(frameTime);

                responseState = RESP_INTERACTIVE_RESTART_WAIT;
                if (controlStream != NULL)
                    controlStream->resetControl();
                timeoutAt(FP::undefined());
                discardData(frameTime + 10.0);

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
                }
                autoprobeStatusUpdated();
                if (realtimeEgress != NULL) {
                    realtimeEgress->incomingData(
                            SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                          frameTime, FP::undefined()));
                }
            }
        }
    }
        break;

    default:
        break;
    }
}

void AcquireGMDCLAP3W::incomingInstrumentTimeout(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    lastRecordTime = FP::undefined();
    switch (responseState) {
    case RESP_UNPOLLED_RUN:
        qCDebug(log) << "Timeout in unpolled mode at" << Logging::time(frameTime);
        timeoutAt(frameTime + 30.0);

        if (controlStream != NULL) {
            controlStream->writeControl(Util::ByteArray::filled('\r', 16));
            controlStream->writeControl("\r\r\rmain\r\rmain\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS_MAIN;
        discardData(frameTime + 1.0);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("UnpolledRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;

    case RESP_PASSIVE_RUN:
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        responseState = RESP_PASSIVE_WAIT;
        generalStatusUpdated();
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;

    case RESP_PASSIVE_DISCARD:
        responseState = RESP_PASSIVE_RUN;
        discardData(frameTime + 0.5, 1);
        timeoutAt(frameTime + 3.0);
        break;

    case RESP_UNPOLLED_DISCARD:
        responseState = RESP_UNPOLLED_RUN;
        discardData(frameTime + 0.5, 1);
        timeoutAt(frameTime + 3.0);
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS_MAIN:
    case RESP_INTERACTIVE_START_STOPREPORTS_HIDE:
    case RESP_INTERACTIVE_START_CONFIG_SN:
    case RESP_INTERACTIVE_START_CONFIG_READSN:
    case RESP_INTERACTIVE_START_CONFIG_FW:
    case RESP_INTERACTIVE_START_CONFIG_READFW:
    case RESP_INTERACTIVE_START_SETTINGS_WAIT:
    case RESP_INTERACTIVE_START_SETTINGS_MAIN:
    case RESP_INTERACTIVE_START_SETTINGS_CAL:
    case RESP_INTERACTIVE_START_SETTINGS_READFLOW:
    case RESP_INTERACTIVE_START_SETTINGS_WRITEFLOW:
    case RESP_INTERACTIVE_START_UNPOLLED_WAIT:
    case RESP_INTERACTIVE_START_UNPOLLED_MAIN:
    case RESP_INTERACTIVE_START_UNPOLLED_DISCARDFIRST:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
    case RESP_INTERACTIVE_RESTART_WAIT:
        qCDebug(log) << "Timeout in interactive start state" << responseState << "at"
                     << Logging::time(frameTime);

        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;

    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        timeoutAt(frameTime + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl(Util::ByteArray::filled('\r', 16));
            controlStream->writeControl("\r\r\rmain\r\rmain\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS_MAIN;
        discardData(frameTime + 0.5);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* fall through */
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;

    default:
        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;
    }
}

void AcquireGMDCLAP3W::discardDataCompleted(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    switch (responseState) {
    case RESP_INTERACTIVE_START_STOPREPORTS_MAIN:
        if (controlStream != NULL) {
            controlStream->writeControl("hide\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS_HIDE;
        discardData(frameTime + 0.5);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveStopReportsFlushing"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS_HIDE:
        if (controlStream != NULL) {
            controlStream->writeControl("cfg\r");
        }
        responseState = RESP_INTERACTIVE_START_CONFIG_SN;
        discardData(frameTime + 1.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveConfigMenuFlushing"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_CONFIG_SN:
        if (controlStream != NULL) {
            controlStream->writeControl("sn\r");
        }
        responseState = RESP_INTERACTIVE_START_CONFIG_READSN;
        timeoutAt(frameTime + 2.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadSerialNumber"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_CONFIG_FW:
        if (controlStream != NULL) {
            controlStream->writeControl("fw\r");
        }
        responseState = RESP_INTERACTIVE_START_CONFIG_READFW;
        timeoutAt(frameTime + 2.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadFirmwareVersion"), frameTime, FP::undefined()));
        }
        break;


    case RESP_INTERACTIVE_START_SETTINGS_WAIT:
        if (controlStream != NULL) {
            controlStream->writeControl(Util::ByteArray::filled('\r', 16));
            controlStream->writeControl("\r\r\rmain\r\rmain\r");
        }
        responseState = RESP_INTERACTIVE_START_SETTINGS_MAIN;
        discardData(frameTime + 0.5);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveMainMenuFlushing"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_SETTINGS_MAIN:
        if (controlStream != NULL) {
            controlStream->writeControl("cal\r");
        }
        responseState = RESP_INTERACTIVE_START_SETTINGS_CAL;
        discardData(frameTime + 1.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveCalMenuFlushing"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_SETTINGS_CAL:
        responseTry = 0;
    case RESP_INTERACTIVE_START_SETTINGS_WRITEFLOW:
        if (controlStream != NULL) {
            controlStream->writeControl("flow\r");
        }
        responseState = RESP_INTERACTIVE_START_SETTINGS_READFLOW;
        timeoutAt(frameTime + 2.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadHardwareFlowCalibration"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_UNPOLLED_WAIT:
        if (controlStream != NULL) {
            controlStream->writeControl(Util::ByteArray::filled('\r', 16));
            controlStream->writeControl("\r\r\rmain\r\rmain\r");
        }
        responseState = RESP_INTERACTIVE_START_UNPOLLED_MAIN;
        discardData(frameTime + 1.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveUnpolledStartMainMenuFlushing"), frameTime, FP::undefined()));
        }
        break;
    case RESP_INTERACTIVE_START_UNPOLLED_MAIN:
        if (controlStream != NULL) {
            controlStream->writeControl("show\r");
        }
        responseState = RESP_INTERACTIVE_START_UNPOLLED_DISCARDFIRST;
        timeoutAt(frameTime + 2.0);
        discardData(FP::undefined(), 1);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveUnpolledStartDiscardFirstLine"), frameTime, FP::undefined()));
        }
        break;
    case RESP_INTERACTIVE_START_UNPOLLED_DISCARDFIRST:
        responseState = RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID;
        timeoutAt(frameTime + 2.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveUnpolledStartReadFirstRecord"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_RESTART_WAIT:
        timeoutAt(frameTime + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl(Util::ByteArray::filled('\r', 16));
            controlStream->writeControl("\r\r\rmain\r\rmain\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS_MAIN;
        discardData(frameTime + 0.5);
        lastRecordTime = FP::undefined();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    default:
        break;
    }
}


void AcquireGMDCLAP3W::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_UNUSED(frameTime);
    Q_UNUSED(frame);
    /* Maybe implement something to recover from a hide more gracefully than
     * timing out? */
}

SequenceValue::Transfer AcquireGMDCLAP3W::getPersistentValues()
{
    PauseLock paused(*this);
    SequenceValue::Transfer result;

    if (persistentFn.read().exists()) {
        result.emplace_back(persistentFn);
    }
    if (persistentFf.read().exists()) {
        result.emplace_back(persistentFf);
    }
    if (FP::defined(filterStart.startTime)) {
        result.emplace_back(SequenceName({}, "raw", "ZFILTER"), buildFilterValue(filterStart),
                            filterStart.startTime, FP::undefined());
    }
    if (FP::defined(filterWhite.startTime)) {
        result.emplace_back(SequenceName({}, "raw", "ZWHITE"), buildFilterValue(filterWhite),
                            filterWhite.startTime, FP::undefined());
    }
    if (haveAcceptedNormalization && FP::defined(normalizationStartTime)) {
        result.emplace_back(SequenceName({}, "raw", "ZSPOT"), buildNormalizationValue(),
                            normalizationStartTime, FP::undefined());
    }

    return result;
}

Variant::Root AcquireGMDCLAP3W::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireGMDCLAP3W::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_UNPOLLED_RUN:
    case RESP_PASSIVE_DISCARD:
    case RESP_UNPOLLED_DISCARD:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

void AcquireGMDCLAP3W::command(const Variant::Read &command)
{
    if (command.hash("AdvanceSpot").exists()) {
        switch (sampleState) {
        case SAMPLE_RUN:
        case SAMPLE_SPOT_NORMALIZE:
            if (spotChangeCheck > 0 && spotChangeCheck < 8) {
                qCDebug(log) << "Advancing to spot" << (spotChangeCheck + 1)
                             << "on external request in state" << sampleState;

                Variant::Write info = Variant::Write::empty();
                if (sampleState == SAMPLE_RUN) {
                    info.hash("State").setString("Run");
                    forceFilterBreak = true;
                } else {
                    info.hash("State").setString("Normalize");
                }
                info.hash("Spot").setInt64(spotChangeCheck + 1);
                event(Time::time(), QObject::tr("Advancing to spot %1 on external request.").arg(
                        (spotChangeCheck + 1)), false, info);


                if (controlStream != NULL) {
                    Util::ByteArray cmd("spot=");
                    cmd += QByteArray::number(spotChangeCheck + 1);
                    cmd.push_back('\r');
                    controlStream->writeControl(cmd);
                    controlStream->writeControl(std::move(cmd));
                    spotSetTarget = spotChangeCheck + 1;
                } else {
                    spotSetTarget = -1;
                }
                sampleState = SAMPLE_SPOT_ADVANCE;
                instrumentAcknowledgeTimeout = FP::undefined();
                forceRealtimeStateEmit = true;

                switch (responseState) {
                case RESP_PASSIVE_RUN:
                    responseState = RESP_PASSIVE_DISCARD;
                    break;
                case RESP_UNPOLLED_RUN:
                    responseState = RESP_UNPOLLED_DISCARD;
                    break;
                default:
                    break;
                }
            }
            break;

            /* No effect in these states */
        case SAMPLE_SPOT_ADVANCE:
        case SAMPLE_FILTER_BASELINE_START:
        case SAMPLE_FILTER_BASELINE:
        case SAMPLE_FILTER_CHANGE_START:
        case SAMPLE_FILTER_CHANGING:
        case SAMPLE_WHITE_FILTER_BASELINE_START:
        case SAMPLE_WHITE_FILTER_BASELINE:
        case SAMPLE_WHITE_FILTER_CHANGE_START:
        case SAMPLE_WHITE_FILTER_CHANGING:
        case SAMPLE_BYPASSED_FILTER_BASELINE_START:
        case SAMPLE_BYPASSED_FILTER_BASELINE:
        case SAMPLE_BYPASSED_FILTER_CHANGE_START:
        case SAMPLE_BYPASSED_FILTER_CHANGING:
        case SAMPLE_BYPASSED_WHITE_FILTER_BASELINE_START:
        case SAMPLE_BYPASSED_WHITE_FILTER_BASELINE:
        case SAMPLE_BYPASSED_WHITE_FILTER_CHANGE_START:
        case SAMPLE_BYPASSED_WHITE_FILTER_CHANGING:
        case SAMPLE_REQUIRE_FILTER_CHANGE:
        case SAMPLE_REQUIRE_WHITE_FILTER_CHANGE:
        case SAMPLE_BYPASSED_REQUIRE_FILTER_CHANGE:
        case SAMPLE_BYPASSED_REQUIRE_WHITE_FILTER_CHANGE:
            qCDebug(log) << "Discarding spot advance in state" << sampleState;
            break;

        case SAMPLE_BYPASSED_RUN:
        case SAMPLE_BYPASSED_SPOT_NORMALIZE:
            if (bypassedSpot > 0 && bypassedSpot < 8) {
                qCDebug(log) << "Advancing to spot" << (bypassedSpot + 1)
                             << "on external request during bypass";

                Variant::Write info = Variant::Write::empty();
                if (sampleState == SAMPLE_BYPASSED_RUN) {
                    info.hash("State").setString("BypassedRun");
                    sampleState = SAMPLE_BYPASSED_SPOT_NORMALIZE;
                    forceRealtimeStateEmit = true;
                    forceFilterBreak = true;
                } else {
                    info.hash("State").setString("BypassedNormalize");
                }
                info.hash("Spot").setInt64(bypassedSpot + 1);
                event(Time::time(), QObject::tr("Advancing to spot %1 on external request.").arg(
                        bypassedSpot + 1), false, info);

                bypassedSpot = bypassedSpot + 1;
            }
            break;

        case SAMPLE_UNBYPASS_RUN:
        case SAMPLE_UNBYPASS_SPOT_NORMALIZE:
            if (bypassedSpot > 0 && bypassedSpot < 8) {
                qCDebug(log) << "Advancing to spot" << (bypassedSpot + 1) << "on external request";

                Variant::Write info = Variant::Write::empty();
                if (sampleState == SAMPLE_UNBYPASS_RUN) {
                    info.hash("State").setString("UnbypassRun");
                } else {
                    info.hash("State").setString("UnbypassNormalize");
                }
                info.hash("Spot").setInt64(bypassedSpot + 1);
                event(Time::time(), QObject::tr("Advancing to spot %1 on external request.").arg(
                        bypassedSpot + 1), false, info);


                if (controlStream != NULL) {
                    Util::ByteArray cmd("spot=");
                    cmd += QByteArray::number(bypassedSpot + 1);
                    cmd.push_back('\r');
                    controlStream->writeControl(std::move(cmd));
                    spotSetTarget = bypassedSpot + 1;
                } else {
                    spotSetTarget = -1;
                }

                switch (responseState) {
                case RESP_PASSIVE_RUN:
                    responseState = RESP_PASSIVE_DISCARD;
                    break;
                case RESP_UNPOLLED_RUN:
                    responseState = RESP_UNPOLLED_DISCARD;
                    break;
                default:
                    break;
                }

                sampleState = SAMPLE_UNBYPASS_SPOT_NORMALIZE;
                forceRealtimeStateEmit = true;
                forceFilterBreak = true;
            }
            break;
        }
    }

    if (command.hash("StartFilterChange").exists()) {
        switch (sampleState) {
            /* Start an unbypassed change */
        case SAMPLE_FILTER_BASELINE_START:
        case SAMPLE_WHITE_FILTER_BASELINE_START:
        case SAMPLE_UNBYPASS_RUN:
        case SAMPLE_UNBYPASS_SPOT_NORMALIZE:
            /* In a transition state, clear at all pending */
        {
            std::lock_guard<std::mutex> lock(mutex);
            clearDataBuffer();
        }
        case SAMPLE_RUN:
        case SAMPLE_SPOT_NORMALIZE:
        case SAMPLE_SPOT_ADVANCE:
        case SAMPLE_FILTER_BASELINE:
        case SAMPLE_WHITE_FILTER_BASELINE:
        case SAMPLE_REQUIRE_FILTER_CHANGE:
        case SAMPLE_REQUIRE_WHITE_FILTER_CHANGE:
            qCDebug(log) << "Manual filter change started in state" << sampleState;

            {
                Variant::Write info = Variant::Write::empty();
                switch (sampleState) {
                case SAMPLE_RUN:
                    info.hash("State").setString("Run");
                    break;
                case SAMPLE_SPOT_NORMALIZE:
                    info.hash("State").setString("Normalize");
                    insertDiagnosticNormalization(info);
                    break;
                case SAMPLE_UNBYPASS_RUN:
                    info.hash("State").setString("UnbypassRun");
                    break;
                case SAMPLE_UNBYPASS_SPOT_NORMALIZE:
                    info.hash("State").setString("UnbypassNormalize");
                    break;
                case SAMPLE_SPOT_ADVANCE:
                    info.hash("State").setString("SpotAdvance");
                    break;
                case SAMPLE_FILTER_BASELINE:
                    info.hash("State").setString("FilterBaseline");
                    insertDiagnosticFilterBaseline(info);
                    break;
                case SAMPLE_FILTER_BASELINE_START:
                    info.hash("State").setString("FilterBaselineStart");
                    insertDiagnosticFilterBaseline(info);
                    break;
                case SAMPLE_WHITE_FILTER_BASELINE:
                    info.hash("State").setString("WhiteFilterBaseline");
                    insertDiagnosticFilterBaseline(info);
                    break;
                case SAMPLE_WHITE_FILTER_BASELINE_START:
                    info.hash("State").setString("WhiteFilterBaselineStart");
                    insertDiagnosticFilterBaseline(info);
                    break;
                case SAMPLE_REQUIRE_FILTER_CHANGE:
                    info.hash("State").setString("RequireFilterChange");
                    break;
                case SAMPLE_REQUIRE_WHITE_FILTER_CHANGE:
                    info.hash("State").setString("RequireWhiteFilterChange");
                    break;
                default:
                    break;
                }
                event(Time::time(), QObject::tr("Manual filter change started."), true, info);
            }
            sampleState = SAMPLE_FILTER_CHANGE_START;
            forceRealtimeStateEmit = true;
            instrumentAcknowledgeTimeout = FP::undefined();
            forceFilterBreak = true;

            if (controlStream != NULL) {
                controlStream->writeControl("stop\r");
            }
            normalizationStartTime = FP::undefined();

            switch (responseState) {
            case RESP_PASSIVE_RUN:
                responseState = RESP_PASSIVE_DISCARD;
                break;
            case RESP_UNPOLLED_RUN:
                responseState = RESP_UNPOLLED_DISCARD;
                break;
            default:
                break;
            }
            break;

            /* Start a bypassed change */
        case SAMPLE_BYPASSED_FILTER_BASELINE_START:
        case SAMPLE_BYPASSED_WHITE_FILTER_BASELINE_START:
            /* In a transition state, clear at all pending */
        {
            std::lock_guard<std::mutex> lock(mutex);
            clearDataBuffer();
        }
        case SAMPLE_BYPASSED_FILTER_BASELINE:
        case SAMPLE_BYPASSED_WHITE_FILTER_BASELINE:
        case SAMPLE_BYPASSED_RUN:
        case SAMPLE_BYPASSED_SPOT_NORMALIZE:
        case SAMPLE_BYPASSED_REQUIRE_FILTER_CHANGE:
        case SAMPLE_BYPASSED_REQUIRE_WHITE_FILTER_CHANGE:
            qCDebug(log) << "Manual filter change during bypass started in state" << sampleState;

            {
                Variant::Write info = Variant::Write::empty();
                switch (sampleState) {
                case SAMPLE_BYPASSED_FILTER_BASELINE:
                    info.hash("State").setString("BypassedFilterBaseline");
                    insertDiagnosticFilterBaseline(info);
                    break;
                case SAMPLE_BYPASSED_FILTER_BASELINE_START:
                    info.hash("State").setString("BypassedFilterBaselineStart");
                    insertDiagnosticFilterBaseline(info);
                    break;
                case SAMPLE_BYPASSED_WHITE_FILTER_BASELINE:
                    info.hash("State").setString("BypassedWhiteFilterBaseline");
                    insertDiagnosticFilterBaseline(info);
                    break;
                case SAMPLE_BYPASSED_WHITE_FILTER_BASELINE_START:
                    info.hash("State").setString("BypassedWhiteFilterBaselineStart");
                    insertDiagnosticFilterBaseline(info);
                    break;
                case SAMPLE_BYPASSED_RUN:
                    info.hash("State").setString("BypassedRun");
                    break;
                case SAMPLE_BYPASSED_SPOT_NORMALIZE:
                    info.hash("State").setString("BypassedNormalize");
                    break;
                case SAMPLE_BYPASSED_REQUIRE_FILTER_CHANGE:
                    info.hash("State").setString("BypassedRequireFilterChange");
                    break;
                case SAMPLE_BYPASSED_REQUIRE_WHITE_FILTER_CHANGE:
                    info.hash("State").setString("BypassedRequireWhiteFilterChange");
                    break;
                default:
                    break;
                }
                event(Time::time(), QObject::tr("Manual filter change started."), true, info);
            }
            sampleState = SAMPLE_BYPASSED_FILTER_CHANGE_START;
            forceRealtimeStateEmit = true;
            instrumentAcknowledgeTimeout = FP::undefined();
            forceFilterBreak = true;

            if (controlStream != NULL) {
                controlStream->writeControl("stop\r");
            }
            normalizationStartTime = FP::undefined();

            switch (responseState) {
            case RESP_PASSIVE_RUN:
                responseState = RESP_PASSIVE_DISCARD;
                break;
            case RESP_UNPOLLED_RUN:
                responseState = RESP_UNPOLLED_DISCARD;
                break;
            default:
                break;
            }
            break;

            /* No effect in these states (already changing) */
        case SAMPLE_FILTER_CHANGING:
        case SAMPLE_FILTER_CHANGE_START:
        case SAMPLE_WHITE_FILTER_CHANGING:
        case SAMPLE_WHITE_FILTER_CHANGE_START:
        case SAMPLE_BYPASSED_FILTER_CHANGING:
        case SAMPLE_BYPASSED_FILTER_CHANGE_START:
        case SAMPLE_BYPASSED_WHITE_FILTER_CHANGING:
        case SAMPLE_BYPASSED_WHITE_FILTER_CHANGE_START:
            qCDebug(log) << "Discarding filter change start in state" << sampleState;
            break;
        }
    }

    if (command.hash("StartWhiteFilterChange").exists()) {
        switch (sampleState) {
            /* Start an unbypassed change */
        case SAMPLE_UNBYPASS_RUN:
        case SAMPLE_UNBYPASS_SPOT_NORMALIZE:
        case SAMPLE_FILTER_BASELINE_START:
        case SAMPLE_WHITE_FILTER_BASELINE_START:
            /* In a transition state, clear at all pending */
        {
            std::lock_guard<std::mutex> lock(mutex);
            clearDataBuffer();
        }
        case SAMPLE_RUN:
        case SAMPLE_SPOT_NORMALIZE:
        case SAMPLE_SPOT_ADVANCE:
        case SAMPLE_FILTER_BASELINE:
        case SAMPLE_WHITE_FILTER_BASELINE:
        case SAMPLE_REQUIRE_FILTER_CHANGE:
        case SAMPLE_REQUIRE_WHITE_FILTER_CHANGE:
            qCDebug(log) << "White filter change started in state" << sampleState;

            {
                Variant::Write info = Variant::Write::empty();
                switch (sampleState) {
                case SAMPLE_RUN:
                    info.hash("State").setString("Run");
                    break;
                case SAMPLE_SPOT_NORMALIZE:
                    info.hash("State").setString("Normalize");
                    insertDiagnosticNormalization(info);
                    break;
                case SAMPLE_UNBYPASS_RUN:
                    info.hash("State").setString("UnbypassRun");
                    break;
                case SAMPLE_UNBYPASS_SPOT_NORMALIZE:
                    info.hash("State").setString("UnbypassNormalize");
                    break;
                case SAMPLE_SPOT_ADVANCE:
                    info.hash("State").setString("SpotAdvance");
                    break;
                case SAMPLE_FILTER_BASELINE:
                    info.hash("State").setString("FilterBaseline");
                    insertDiagnosticFilterBaseline(info);
                    break;
                case SAMPLE_WHITE_FILTER_BASELINE:
                    info.hash("State").setString("WhiteFilterBaseline");
                    insertDiagnosticFilterBaseline(info);
                    break;
                case SAMPLE_REQUIRE_FILTER_CHANGE:
                    info.hash("State").setString("RequireFilterChange");
                    break;
                case SAMPLE_REQUIRE_WHITE_FILTER_CHANGE:
                    info.hash("State").setString("RequireWhiteFilterChange");
                    break;
                default:
                    break;
                }
                event(Time::time(), QObject::tr("White filter change started."), true, info);
            }
            sampleState = SAMPLE_WHITE_FILTER_CHANGE_START;
            forceRealtimeStateEmit = true;
            whiteFilterChangeStartTime = FP::undefined();
            instrumentAcknowledgeTimeout = FP::undefined();
            forceFilterBreak = true;

            if (controlStream != NULL) {
                controlStream->writeControl("stop\r");
            }
            normalizationStartTime = FP::undefined();

            switch (responseState) {
            case RESP_PASSIVE_RUN:
                responseState = RESP_PASSIVE_DISCARD;
                break;
            case RESP_UNPOLLED_RUN:
                responseState = RESP_UNPOLLED_DISCARD;
                break;
            default:
                break;
            }
            break;

            /* Start a bypassed change */
        case SAMPLE_BYPASSED_FILTER_BASELINE_START:
        case SAMPLE_BYPASSED_WHITE_FILTER_BASELINE_START:
            /* In a transition state, clear at all pending */
        {
            std::lock_guard<std::mutex> lock(mutex);
            clearDataBuffer();
        }
        case SAMPLE_BYPASSED_FILTER_BASELINE:
        case SAMPLE_BYPASSED_WHITE_FILTER_BASELINE:
        case SAMPLE_BYPASSED_RUN:
        case SAMPLE_BYPASSED_SPOT_NORMALIZE:
        case SAMPLE_BYPASSED_REQUIRE_FILTER_CHANGE:
        case SAMPLE_BYPASSED_REQUIRE_WHITE_FILTER_CHANGE:
            qCDebug(log) << "White filter change during bypass started in state" << sampleState;

            {
                Variant::Write info = Variant::Write::empty();
                switch (sampleState) {
                case SAMPLE_BYPASSED_FILTER_BASELINE:
                    info.hash("State").setString("BypassedFilterBaseline");
                    insertDiagnosticFilterBaseline(info);
                    break;
                case SAMPLE_BYPASSED_FILTER_BASELINE_START:
                    info.hash("State").setString("BypassedFilterBaselineStart");
                    insertDiagnosticFilterBaseline(info);
                    break;
                case SAMPLE_BYPASSED_WHITE_FILTER_BASELINE:
                    info.hash("State").setString("BypassedWhiteFilterBaseline");
                    insertDiagnosticFilterBaseline(info);
                    break;
                case SAMPLE_BYPASSED_WHITE_FILTER_BASELINE_START:
                    info.hash("State").setString("BypassedWhiteFilterBaselineStart");
                    insertDiagnosticFilterBaseline(info);
                    break;
                case SAMPLE_BYPASSED_RUN:
                    info.hash("State").setString("BypassedRun");
                    break;
                case SAMPLE_BYPASSED_SPOT_NORMALIZE:
                    info.hash("State").setString("BypassedNormalize");
                    break;
                case SAMPLE_BYPASSED_REQUIRE_FILTER_CHANGE:
                    info.hash("State").setString("BypassedRequireFilterChange");
                    break;
                case SAMPLE_BYPASSED_REQUIRE_WHITE_FILTER_CHANGE:
                    info.hash("State").setString("BypassedRequireWhiteFilterChange");
                    break;
                default:
                    break;
                }
                event(Time::time(), QObject::tr("White filter change started."), true, info);
            }
            sampleState = SAMPLE_BYPASSED_WHITE_FILTER_CHANGE_START;
            forceRealtimeStateEmit = true;
            instrumentAcknowledgeTimeout = FP::undefined();
            whiteFilterChangeStartTime = FP::undefined();
            forceFilterBreak = true;

            if (controlStream != NULL) {
                controlStream->writeControl("stop\r");
            }
            normalizationStartTime = FP::undefined();

            switch (responseState) {
            case RESP_PASSIVE_RUN:
                responseState = RESP_PASSIVE_DISCARD;
                break;
            case RESP_UNPOLLED_RUN:
                responseState = RESP_UNPOLLED_DISCARD;
                break;
            default:
                break;
            }
            break;

            /* Already changing, to just switch a white check */
        case SAMPLE_FILTER_CHANGING:
        case SAMPLE_FILTER_CHANGE_START:
        case SAMPLE_BYPASSED_FILTER_CHANGING:
        case SAMPLE_BYPASSED_FILTER_CHANGE_START:
            qCDebug(log) << "Switch to white filter change from state" << sampleState;

            {
                Variant::Write info = Variant::Write::empty();
                switch (sampleState) {
                case SAMPLE_FILTER_CHANGING:
                    info.hash("State").setString("FilterChanging");
                    insertDiagnosticFilterBaseline(info);
                    sampleState = SAMPLE_WHITE_FILTER_CHANGING;
                    break;
                case SAMPLE_FILTER_CHANGE_START:
                    info.hash("State").setString("FilterChangeStart");
                    insertDiagnosticFilterBaseline(info);
                    sampleState = SAMPLE_WHITE_FILTER_CHANGE_START;
                    break;
                case SAMPLE_BYPASSED_FILTER_CHANGING:
                    info.hash("State").setString("BypassedFilterChanging");
                    insertDiagnosticFilterBaseline(info);
                    sampleState = SAMPLE_WHITE_FILTER_CHANGING;
                    break;
                case SAMPLE_BYPASSED_FILTER_CHANGE_START:
                    info.hash("State").setString("BypassedFilterChangeStart");
                    insertDiagnosticFilterBaseline(info);
                    sampleState = SAMPLE_WHITE_FILTER_CHANGE_START;
                    break;
                default:
                    Q_ASSERT(false);
                    break;
                }
                event(Time::time(), QObject::tr("Switching to white filter change mode."), true,
                      info);
            }

            forceRealtimeStateEmit = true;
            whiteFilterChangeStartTime = FP::undefined();
            break;

            /* No effect */
        case SAMPLE_WHITE_FILTER_CHANGING:
        case SAMPLE_WHITE_FILTER_CHANGE_START:
        case SAMPLE_BYPASSED_WHITE_FILTER_CHANGING:
        case SAMPLE_BYPASSED_WHITE_FILTER_CHANGE_START:
            qCDebug(log) << "Discarding white filter change start in state" << sampleState;
            break;
        }
    }

    if (command.hash("StopFilterChange").exists()) {
        switch (sampleState) {
        case SAMPLE_FILTER_CHANGE_START:
        case SAMPLE_FILTER_CHANGING:
            qCDebug(log) << "Filter change ended in state" << sampleState;
            {
                Variant::Write info = Variant::Write::empty();
                switch (sampleState) {
                case SAMPLE_FILTER_CHANGING:
                    info.hash("State").setString("FilterChanging");
                    break;
                case SAMPLE_FILTER_CHANGE_START:
                    info.hash("State").setString("FilterChangeStart");
                    break;
                default:
                    break;
                }
                insertDiagnosticFilterBaseline(info);
                event(Time::time(), QObject::tr("Filter change ended."), true, info);
            }

            if (controlStream != NULL) {
                controlStream->writeControl("go\r");
            }
            sampleState = SAMPLE_FILTER_BASELINE_START;
            instrumentAcknowledgeTimeout = FP::undefined();
            forceRealtimeStateEmit = true;
            resetFilterBaseline();

            switch (responseState) {
            case RESP_PASSIVE_RUN:
                responseState = RESP_PASSIVE_DISCARD;
                break;
            case RESP_UNPOLLED_RUN:
                responseState = RESP_UNPOLLED_DISCARD;
                break;
            default:
                break;
            }
            break;

        case SAMPLE_WHITE_FILTER_CHANGE_START:
            /* In a transition state, clear at all pending */
        {
            std::lock_guard<std::mutex> lock(mutex);
            clearDataBuffer();
        }
        case SAMPLE_WHITE_FILTER_CHANGING:
            qCDebug(log) << "White filter change ended, state:" << sampleState;
            {
                Variant::Write info = Variant::Write::empty();
                switch (sampleState) {
                case SAMPLE_WHITE_FILTER_CHANGING:
                    info.hash("State").setString("WhiteFilterChanging");
                    break;
                case SAMPLE_WHITE_FILTER_CHANGE_START:
                    info.hash("State").setString("WhiteFilterChangeStart");
                    break;
                default:
                    break;
                }
                insertDiagnosticFilterBaseline(info);
                event(Time::time(), QObject::tr("White filter change ended."), true, info);
            }

            if (controlStream != NULL) {
                controlStream->writeControl("go\r");
            }
            sampleState = SAMPLE_WHITE_FILTER_BASELINE_START;
            instrumentAcknowledgeTimeout = FP::undefined();
            forceRealtimeStateEmit = true;
            resetFilterBaseline();

            switch (responseState) {
            case RESP_PASSIVE_RUN:
                responseState = RESP_PASSIVE_DISCARD;
                break;
            case RESP_UNPOLLED_RUN:
                responseState = RESP_UNPOLLED_DISCARD;
                break;
            default:
                break;
            }
            break;

        case SAMPLE_BYPASSED_FILTER_CHANGE_START:
            /* In a transition state, clear at all pending */
        {
            std::lock_guard<std::mutex> lock(mutex);
            clearDataBuffer();
        }
        case SAMPLE_BYPASSED_FILTER_CHANGING:
            qCDebug(log) << "Bypassed filter change ended in state" << sampleState;

            {
                Variant::Write info = Variant::Write::empty();
                switch (sampleState) {
                case SAMPLE_BYPASSED_FILTER_CHANGING:
                    info.hash("State").setString("BypassedFilterChanging");
                    break;
                case SAMPLE_BYPASSED_FILTER_CHANGE_START:
                    info.hash("State").setString("BypassedFilterChangeStart");
                    break;
                default:
                    break;
                }
                insertDiagnosticFilterBaseline(info);
                event(Time::time(), QObject::tr("Filter change ended."), true, info);
            }

            if (controlStream != NULL) {
                controlStream->writeControl("go\r");
            }
            bypassedSpot = 1;
            sampleState = SAMPLE_BYPASSED_FILTER_BASELINE_START;
            instrumentAcknowledgeTimeout = FP::undefined();
            forceRealtimeStateEmit = true;
            resetFilterBaseline();

            switch (responseState) {
            case RESP_PASSIVE_RUN:
                responseState = RESP_PASSIVE_DISCARD;
                break;
            case RESP_UNPOLLED_RUN:
                responseState = RESP_UNPOLLED_DISCARD;
                break;
            default:
                break;
            }
            break;

        case SAMPLE_BYPASSED_WHITE_FILTER_CHANGE_START:
            /* In a transition state, clear at all pending */
        {
            std::lock_guard<std::mutex> lock(mutex);
            clearDataBuffer();
        }
        case SAMPLE_BYPASSED_WHITE_FILTER_CHANGING:
            qCDebug(log) << "Bypassed white filter change ended in state" << sampleState;
            {
                Variant::Write info = Variant::Write::empty();
                switch (sampleState) {
                case SAMPLE_BYPASSED_WHITE_FILTER_CHANGING:
                    info.hash("State").setString("BypassedWhiteFilterChanging");
                    break;
                case SAMPLE_BYPASSED_WHITE_FILTER_CHANGE_START:
                    info.hash("State").setString("BypassedWhiteFilterChangeStart");
                    break;
                default:
                    break;
                }
                insertDiagnosticFilterBaseline(info);
                event(Time::time(), QObject::tr("White filter change ended."), true, info);
            }

            if (controlStream != NULL) {
                controlStream->writeControl("go\r");
            }
            bypassedSpot = 1;
            sampleState = SAMPLE_BYPASSED_WHITE_FILTER_BASELINE_START;
            instrumentAcknowledgeTimeout = FP::undefined();
            forceRealtimeStateEmit = true;
            resetFilterBaseline();

            switch (responseState) {
            case RESP_PASSIVE_RUN:
                responseState = RESP_PASSIVE_DISCARD;
                break;
            case RESP_UNPOLLED_RUN:
                responseState = RESP_UNPOLLED_DISCARD;
                break;
            default:
                break;
            }
            break;

            /* No effect */
        case SAMPLE_RUN:
        case SAMPLE_SPOT_NORMALIZE:
        case SAMPLE_UNBYPASS_RUN:
        case SAMPLE_UNBYPASS_SPOT_NORMALIZE:
        case SAMPLE_SPOT_ADVANCE:
        case SAMPLE_FILTER_BASELINE:
        case SAMPLE_WHITE_FILTER_BASELINE:
        case SAMPLE_BYPASSED_FILTER_BASELINE:
        case SAMPLE_BYPASSED_WHITE_FILTER_BASELINE:
        case SAMPLE_FILTER_BASELINE_START:
        case SAMPLE_WHITE_FILTER_BASELINE_START:
        case SAMPLE_BYPASSED_FILTER_BASELINE_START:
        case SAMPLE_BYPASSED_WHITE_FILTER_BASELINE_START:
        case SAMPLE_BYPASSED_RUN:
        case SAMPLE_BYPASSED_SPOT_NORMALIZE:
        case SAMPLE_REQUIRE_FILTER_CHANGE:
        case SAMPLE_REQUIRE_WHITE_FILTER_CHANGE:
        case SAMPLE_BYPASSED_REQUIRE_FILTER_CHANGE:
        case SAMPLE_BYPASSED_REQUIRE_WHITE_FILTER_CHANGE:
            qCDebug(log) << "Discarding filter change stop, state:" << sampleState;
            break;
        }
    }

    if (command.hash("Bypass").exists()) {
        switch (sampleState) {
            /* Just a state transition, since there's no spot active */
        case SAMPLE_FILTER_CHANGING:
            sampleState = SAMPLE_BYPASSED_FILTER_CHANGING;
            forceRealtimeStateEmit = true;
            qCDebug(log) << "Switching to bypassed filter change";
            break;
        case SAMPLE_FILTER_CHANGE_START:
            sampleState = SAMPLE_BYPASSED_FILTER_CHANGE_START;
            forceRealtimeStateEmit = true;
            qCDebug(log) << "Switching to bypassed start filter change";
            break;
        case SAMPLE_WHITE_FILTER_CHANGING:
            sampleState = SAMPLE_BYPASSED_WHITE_FILTER_CHANGING;
            forceRealtimeStateEmit = true;
            qCDebug(log) << "Switching to bypassed white filter change";
            break;
        case SAMPLE_WHITE_FILTER_CHANGE_START:
            sampleState = SAMPLE_BYPASSED_WHITE_FILTER_CHANGE_START;
            forceRealtimeStateEmit = true;
            qCDebug(log) << "Switching to bypassed start white filter change";
            break;
        case SAMPLE_FILTER_BASELINE:
            sampleState = SAMPLE_BYPASSED_FILTER_BASELINE;
            forceRealtimeStateEmit = true;
            qCDebug(log) << "Switching to bypassed filter baseline";
            break;
        case SAMPLE_FILTER_BASELINE_START:
            sampleState = SAMPLE_BYPASSED_FILTER_BASELINE_START;
            forceRealtimeStateEmit = true;
            qCDebug(log) << "Switching to bypassed start filter baseline";
            break;
        case SAMPLE_WHITE_FILTER_BASELINE:
            sampleState = SAMPLE_BYPASSED_WHITE_FILTER_BASELINE;
            forceRealtimeStateEmit = true;
            qCDebug(log) << "Switching to bypassed white filter baseline";
            break;
        case SAMPLE_WHITE_FILTER_BASELINE_START:
            sampleState = SAMPLE_BYPASSED_WHITE_FILTER_BASELINE_START;
            forceRealtimeStateEmit = true;
            qCDebug(log) << "Switching to bypassed start white filter baseline";
            break;
        case SAMPLE_REQUIRE_FILTER_CHANGE:
            sampleState = SAMPLE_BYPASSED_REQUIRE_FILTER_CHANGE;
            forceRealtimeStateEmit = true;
            qCDebug(log) << "Switching to bypassed filter change required";
            break;
        case SAMPLE_REQUIRE_WHITE_FILTER_CHANGE:
            sampleState = SAMPLE_BYPASSED_REQUIRE_WHITE_FILTER_CHANGE;
            forceRealtimeStateEmit = true;
            qCDebug(log) << "Switching to bypassed white filter change required";
            break;

            /* Set to spot zero */
        case SAMPLE_RUN:
            bypassedSpot = spotChangeCheck;
        case SAMPLE_UNBYPASS_RUN:
            qCDebug(log) << "Switching to bypass mode from run mode, spot" << bypassedSpot
                         << ", state" << sampleState;
            {
                Variant::Write info = Variant::Write::empty();
                switch (sampleState) {
                case SAMPLE_RUN:
                    info.hash("State").setString("Run");
                    break;
                case SAMPLE_UNBYPASS_RUN:
                    info.hash("State").setString("UnbypassRun");
                    /* In a transition state, clear at all pending */
                    {
                        std::lock_guard<std::mutex> lock(mutex);
                        clearDataBuffer();
                    }
                    break;
                default:
                    break;
                }
                info.hash("Spot").setInt64(bypassedSpot);
                event(Time::time(), QObject::tr("Spot sampling bypassed."), false, info);
            }

            if (controlStream != NULL) {
                controlStream->writeControl("spot=0\r");
                spotSetTarget = 0;
            } else {
                spotSetTarget = -1;
            }
            sampleState = SAMPLE_BYPASSED_RUN;
            forceRealtimeStateEmit = true;

            switch (responseState) {
            case RESP_PASSIVE_RUN:
                responseState = RESP_PASSIVE_DISCARD;
                break;
            case RESP_UNPOLLED_RUN:
                responseState = RESP_UNPOLLED_DISCARD;
                break;
            default:
                break;
            }
            break;

        case SAMPLE_SPOT_NORMALIZE:
            bypassedSpot = spotChangeCheck;
        case SAMPLE_UNBYPASS_SPOT_NORMALIZE:
        case SAMPLE_SPOT_ADVANCE:
            qCDebug(log) << "Switching to bypass mode from normalize mode, spot" << bypassedSpot
                         << ", state" << sampleState;
            {
                Variant::Write info = Variant::Write::empty();
                switch (sampleState) {
                case SAMPLE_SPOT_NORMALIZE:
                    info.hash("State").setString("Normalize");
                    break;
                case SAMPLE_UNBYPASS_SPOT_NORMALIZE:
                    info.hash("State").setString("UnbypassNormalize");
                    /* In a transition state, clear at all pending */
                    {
                        std::lock_guard<std::mutex> lock(mutex);
                        clearDataBuffer();
                    }
                    break;
                case SAMPLE_SPOT_ADVANCE:
                    info.hash("State").setString("Advance");
                    /* In a transition state, clear at all pending */
                    {
                        std::lock_guard<std::mutex> lock(mutex);
                        clearDataBuffer();
                    }
                    break;
                default:
                    break;
                }
                info.hash("Spot").setInt64(bypassedSpot);
                event(Time::time(), QObject::tr("Spot normalization bypassed."), false, info);
            }

            if (controlStream != NULL) {
                controlStream->writeControl("spot=0\r");
                spotSetTarget = 0;
            } else {
                spotSetTarget = -1;
            }
            sampleState = SAMPLE_BYPASSED_SPOT_NORMALIZE;
            forceRealtimeStateEmit = true;

            switch (responseState) {
            case RESP_PASSIVE_RUN:
                responseState = RESP_PASSIVE_DISCARD;
                break;
            case RESP_UNPOLLED_RUN:
                responseState = RESP_UNPOLLED_DISCARD;
                break;
            default:
                break;
            }
            break;

            /* No effect */
        case SAMPLE_BYPASSED_FILTER_CHANGING:
        case SAMPLE_BYPASSED_WHITE_FILTER_CHANGING:
        case SAMPLE_BYPASSED_FILTER_BASELINE:
        case SAMPLE_BYPASSED_WHITE_FILTER_BASELINE:
        case SAMPLE_BYPASSED_FILTER_CHANGE_START:
        case SAMPLE_BYPASSED_WHITE_FILTER_CHANGE_START:
        case SAMPLE_BYPASSED_FILTER_BASELINE_START:
        case SAMPLE_BYPASSED_WHITE_FILTER_BASELINE_START:
        case SAMPLE_BYPASSED_RUN:
        case SAMPLE_BYPASSED_SPOT_NORMALIZE:
        case SAMPLE_BYPASSED_REQUIRE_FILTER_CHANGE:
        case SAMPLE_BYPASSED_REQUIRE_WHITE_FILTER_CHANGE:
            qCDebug(log) << "Discarding bypass command, state: " << sampleState;
            break;
        }
    }

    if (command.hash("UnBypass").exists()) {
        switch (sampleState) {
            /* Just a state transition, since there's no spot active */
        case SAMPLE_BYPASSED_FILTER_CHANGING:
            sampleState = SAMPLE_FILTER_CHANGING;
            forceRealtimeStateEmit = true;
            qCDebug(log) << "Switching to unbypassed filter change";
            break;
        case SAMPLE_BYPASSED_FILTER_CHANGE_START:
            sampleState = SAMPLE_FILTER_CHANGE_START;
            forceRealtimeStateEmit = true;
            qCDebug(log) << "Switching to unbypassed start filter change";
            break;
        case SAMPLE_BYPASSED_WHITE_FILTER_CHANGING:
            sampleState = SAMPLE_WHITE_FILTER_CHANGING;
            forceRealtimeStateEmit = true;
            qCDebug(log) << "Switching to unbypassed white filter change";
            break;
        case SAMPLE_BYPASSED_WHITE_FILTER_CHANGE_START:
            sampleState = SAMPLE_WHITE_FILTER_CHANGE_START;
            forceRealtimeStateEmit = true;
            qCDebug(log) << "Switching to unbypassed start white filter change";
            break;
        case SAMPLE_BYPASSED_FILTER_BASELINE:
            sampleState = SAMPLE_FILTER_BASELINE;
            forceRealtimeStateEmit = true;
            qCDebug(log) << "Switching to unbypassed filter baseline";
            break;
        case SAMPLE_BYPASSED_FILTER_BASELINE_START:
            sampleState = SAMPLE_FILTER_BASELINE_START;
            forceRealtimeStateEmit = true;
            qCDebug(log) << "Switching to unbypassed start filter baseline";
            break;
        case SAMPLE_BYPASSED_WHITE_FILTER_BASELINE:
            sampleState = SAMPLE_WHITE_FILTER_BASELINE;
            forceRealtimeStateEmit = true;
            qCDebug(log) << "Switching to unbypassed white filter baseline";
            break;
        case SAMPLE_BYPASSED_WHITE_FILTER_BASELINE_START:
            sampleState = SAMPLE_WHITE_FILTER_BASELINE_START;
            forceRealtimeStateEmit = true;
            qCDebug(log) << "Switching to unbypassed start white filter baseline";
            break;
        case SAMPLE_BYPASSED_REQUIRE_FILTER_CHANGE:
            sampleState = SAMPLE_REQUIRE_FILTER_CHANGE;
            forceRealtimeStateEmit = true;
            qCDebug(log) << "Switching to unbypassed filter change required";
            break;
        case SAMPLE_BYPASSED_REQUIRE_WHITE_FILTER_CHANGE:
            sampleState = SAMPLE_REQUIRE_WHITE_FILTER_CHANGE;
            forceRealtimeStateEmit = true;
            qCDebug(log) << "Switching to unbypassed white filter change required";
            break;

        case SAMPLE_BYPASSED_RUN:
            qCDebug(log) << "Unbypassing to run mode, spot" << bypassedSpot;

            {
                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("BypassedRun");
                info.hash("Spot").setInt64(bypassedSpot);
                event(Time::time(), QObject::tr("Resuming normal sampling from bypass."), false,
                      info);
            }

            if (controlStream != NULL) {
                Util::ByteArray cmd("spot=");
                cmd += QByteArray::number(bypassedSpot);
                cmd.push_back('\r');
                controlStream->writeControl(std::move(cmd));
                spotSetTarget = bypassedSpot;
            } else {
                spotSetTarget = -1;
            }
            sampleState = SAMPLE_UNBYPASS_RUN;
            forceRealtimeStateEmit = true;
            instrumentAcknowledgeTimeout = FP::undefined();

            switch (responseState) {
            case RESP_PASSIVE_RUN:
                responseState = RESP_PASSIVE_DISCARD;
                break;
            case RESP_UNPOLLED_RUN:
                responseState = RESP_UNPOLLED_DISCARD;
                break;
            default:
                break;
            }
            break;
        case SAMPLE_BYPASSED_SPOT_NORMALIZE:
            qCDebug(log) << "Unbypassing to normalization, spot" << bypassedSpot;

            {
                Variant::Write info = Variant::Write::empty();
                info.hash("State").setString("BypassedNormalize");
                info.hash("Spot").setInt64(bypassedSpot);
                event(Time::time(), QObject::tr("Resuming normalize from bypass."), false, info);
            }

            if (controlStream != NULL) {
                Util::ByteArray cmd("spot=");
                cmd += QByteArray::number(bypassedSpot);
                cmd.push_back('\r');
                controlStream->writeControl(std::move(cmd));
                spotSetTarget = bypassedSpot;
            } else {
                spotSetTarget = -1;
            }
            sampleState = SAMPLE_UNBYPASS_SPOT_NORMALIZE;
            instrumentAcknowledgeTimeout = FP::undefined();
            forceRealtimeStateEmit = true;

            switch (responseState) {
            case RESP_PASSIVE_RUN:
                responseState = RESP_PASSIVE_DISCARD;
                break;
            case RESP_UNPOLLED_RUN:
                responseState = RESP_UNPOLLED_DISCARD;
                break;
            default:
                break;
            }
            break;


            /* No effect */
        case SAMPLE_FILTER_CHANGING:
        case SAMPLE_WHITE_FILTER_CHANGING:
        case SAMPLE_FILTER_BASELINE:
        case SAMPLE_WHITE_FILTER_BASELINE:
        case SAMPLE_FILTER_CHANGE_START:
        case SAMPLE_WHITE_FILTER_CHANGE_START:
        case SAMPLE_FILTER_BASELINE_START:
        case SAMPLE_WHITE_FILTER_BASELINE_START:
        case SAMPLE_RUN:
        case SAMPLE_UNBYPASS_RUN:
        case SAMPLE_SPOT_NORMALIZE:
        case SAMPLE_UNBYPASS_SPOT_NORMALIZE:
        case SAMPLE_SPOT_ADVANCE:
        case SAMPLE_REQUIRE_FILTER_CHANGE:
        case SAMPLE_REQUIRE_WHITE_FILTER_CHANGE:
            qCDebug(log) << "Discarding unbypass command, state:" << sampleState;
            break;
        }
    }
}

Variant::Root AcquireGMDCLAP3W::getCommands()
{
    Variant::Root result;

    result["AdvanceSpot"].hash("DisplayName").setString("&Advance Sampling Spot");
    result["AdvanceSpot"].hash("DisplayPriority").setInt64(2);
    result["AdvanceSpot"].hash("ToolTip")
                         .setString(
                                 "Advance to the next spot.  This increments the active spot by one.");
    result["AdvanceSpot"].hash("Exclude").array(0).hash("Type").setString("Flags");
    result["AdvanceSpot"].hash("Exclude").array(0).hash("Flags").setFlags({"FilterChanging"});
    result["AdvanceSpot"].hash("Exclude").array(0).hash("Variable").hash("F1");
    result["AdvanceSpot"].hash("Include").array(0).hash("Type").setString("LessThan");
    result["AdvanceSpot"].hash("Include").array(0).hash("Value").setInt64(8);
    result["AdvanceSpot"].hash("Include").array(0).hash("Variable").setString("Fn");

    result["StartFilterChange"].hash("DisplayName").setString("Start &Filter Change");
    result["StartFilterChange"].hash("DisplayPriority").setInt64(-1);
    result["StartFilterChange"].hash("ToolTip")
                               .setString(
                                       "Start a filter change.  This will disable flow and allow you to change the filter in the instrument.");
    result["StartFilterChange"].hash("Exclude").array(0).hash("Type").setString("Flags");
    result["StartFilterChange"].hash("Exclude").array(0).hash("Flags").setFlags({"FilterChanging"});
    result["StartFilterChange"].hash("Exclude").array(0).hash("Variable").setString("F1");

    result["StopFilterChange"].hash("DisplayName").setString("End &Filter Change");
    result["StopFilterChange"].hash("DisplayPriority").setInt64(-1);
    result["StopFilterChange"].hash("ToolTip")
                              .setString(
                                      "This tells the instrument you are done changing the filter and it should resume sampling.");
    result["StopFilterChange"].hash("Include").array(0).hash("Type").setString("Flags");
    result["StopFilterChange"].hash("Include").array(0).hash("Flags").setFlags({"FilterChanging"});
    result["StopFilterChange"].hash("Include").array(0).hash("Variable").setString("F1");

    result["StartWhiteFilterChange"].hash("DisplayName").setString("Start a &White Filter Change");
    result["StartWhiteFilterChange"].hash("DisplayPriority").setInt64(1);
    result["StartWhiteFilterChange"].hash("ToolTip")
                                    .setString(
                                            "Start a filter change to a known to be white filter.  You should double check to make sure that only a single filter is placed in the instrument.");
    result["StartWhiteFilterChange"].hash("Exclude").array(0).hash("Type").setString("Flags");
    result["StartWhiteFilterChange"].hash("Exclude")
                                    .array(0)
                                    .hash("Flags")
                                    .setFlags({"WhiteFilterChanging"});
    result["StartWhiteFilterChange"].hash("Exclude").array(0).hash("Variable").setString("F1");

    return result;
}

AcquisitionInterface::AutoprobeStatus AcquireGMDCLAP3W::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireGMDCLAP3W::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_UNPOLLED_RUN:
    case RESP_UNPOLLED_DISCARD:
        /* Already running/started, do nothing */
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << responseState << "at" << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case RESP_INTERACTIVE_START_STOPREPORTS_MAIN:
    case RESP_INTERACTIVE_START_STOPREPORTS_HIDE:
    case RESP_INTERACTIVE_START_CONFIG_SN:
    case RESP_INTERACTIVE_START_CONFIG_READSN:
    case RESP_INTERACTIVE_START_CONFIG_FW:
    case RESP_INTERACTIVE_START_CONFIG_READFW:
    case RESP_INTERACTIVE_START_SETTINGS_WAIT:
    case RESP_INTERACTIVE_START_SETTINGS_MAIN:
    case RESP_INTERACTIVE_START_SETTINGS_CAL:
    case RESP_INTERACTIVE_START_SETTINGS_READFLOW:
    case RESP_INTERACTIVE_START_SETTINGS_WRITEFLOW:
    case RESP_INTERACTIVE_START_UNPOLLED_WAIT:
    case RESP_INTERACTIVE_START_UNPOLLED_MAIN:
    case RESP_INTERACTIVE_START_UNPOLLED_DISCARDFIRST:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_DISCARD:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        timeoutAt(time + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl(Util::ByteArray::filled('\r', 16));
            controlStream->writeControl("\r\r\rmain\r\rmain\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS_MAIN;
        discardData(time + 0.5);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireGMDCLAP3W::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
    discardData(time + 0.5, 1);
    timeoutAt(time + 5.0);
    generalStatusUpdated();
}

void AcquireGMDCLAP3W::autoprobePromote(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_UNPOLLED_RUN:
        qCDebug(log) << "Promoted from interactive run to interactive acquisition at"
                     << Logging::time(time);

        timeoutAt(time + 3.0);
        break;

    case RESP_UNPOLLED_DISCARD:
    case RESP_INTERACTIVE_START_STOPREPORTS_MAIN:
    case RESP_INTERACTIVE_START_STOPREPORTS_HIDE:
    case RESP_INTERACTIVE_START_CONFIG_SN:
    case RESP_INTERACTIVE_START_CONFIG_READSN:
    case RESP_INTERACTIVE_START_CONFIG_FW:
    case RESP_INTERACTIVE_START_CONFIG_READFW:
    case RESP_INTERACTIVE_START_SETTINGS_WAIT:
    case RESP_INTERACTIVE_START_SETTINGS_MAIN:
    case RESP_INTERACTIVE_START_SETTINGS_CAL:
    case RESP_INTERACTIVE_START_SETTINGS_READFLOW:
    case RESP_INTERACTIVE_START_SETTINGS_WRITEFLOW:
    case RESP_INTERACTIVE_START_UNPOLLED_WAIT:
    case RESP_INTERACTIVE_START_UNPOLLED_MAIN:
    case RESP_INTERACTIVE_START_UNPOLLED_DISCARDFIRST:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + 10.0);
        break;

    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_DISCARD:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl(Util::ByteArray::filled('\r', 16));
            controlStream->writeControl("\r\r\rmain\r\rmain\r");
        }
        responseState = RESP_INTERACTIVE_START_STOPREPORTS_MAIN;
        discardData(time + 0.5);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireGMDCLAP3W::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + 2.0);

    switch (responseState) {
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_DISCARD:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case RESP_UNPOLLED_RUN:
        /* Already in unpolled, so don't reset timeout */
        responseState = RESP_PASSIVE_RUN;

        qCDebug(log) << "Promoted from unpolled interactive state to passive acquisition at"
                     << Logging::time(time);
        break;

    case RESP_UNPOLLED_DISCARD:
        /* Already in unpolled, so don't reset timeout */
        responseState = RESP_PASSIVE_DISCARD;

        qCDebug(log) << "Promoted from unpolled interactive discard state to passive acquisition at"
                     << Logging::time(time);
        break;

    default:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_PASSIVE_RUN;
        forceRealtimeStateEmit = true;
        recordTimeCheck.clear();
        generalStatusUpdated();
        break;
    }
}

static const quint16 serializedStateVersion = 1;

void AcquireGMDCLAP3W::serializeState(QDataStream &stream)
{
    PauseLock paused(*this);

    stream << serializedStateVersion;

    stream << baseVolume << spotNormalization[0] << spotNormalization[1] << spotNormalization[2]
           << filterIsNotWhite << normalizationStartTime << normalizationFilterID
           << normalizationSpot << normalizationLastVolume << normalizationLastInstrumentTime
           << normalizationSerialNumber << normalizationFirmwareVersion << normalizationLastTime
           << filterStart << filterWhite << priorInstrumentSerialNumber
           << priorInstrumentFirmwareVersion;
}

void AcquireGMDCLAP3W::deserializeState(QDataStream &stream)
{
    quint16 version = 0;
    stream >> version;
    if (version != serializedStateVersion) {
        initializeState();
        return;
    }

    PauseLock paused(*this);

    stream >> baseVolume >> spotNormalization[0] >> spotNormalization[1] >> spotNormalization[2]
           >> filterIsNotWhite >> normalizationStartTime >> normalizationFilterID
           >> normalizationSpot >> normalizationLastVolume >> normalizationLastInstrumentTime
           >> normalizationSerialNumber >> normalizationFirmwareVersion >> normalizationLastTime
           >> filterStart >> filterWhite >> priorInstrumentSerialNumber
           >> priorInstrumentFirmwareVersion;
    haveAcceptedNormalization = false;
    havePriorInstrument = true;
}

void AcquireGMDCLAP3W::initializeState()
{
    PauseLock paused(*this);
    havePriorInstrument = true;
}

AcquisitionInterface::AutomaticDefaults AcquireGMDCLAP3W::getDefaults()
{
    AutomaticDefaults result;
    result.name = "A$1$2";
    result.setSerialN81(57600);
    return result;
}


ComponentOptions AcquireGMDCLAP3WComponent::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options);
    return options;
}

ComponentOptions AcquireGMDCLAP3WComponent::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    ComponentOptionSingleCalibration *cal =
            new ComponentOptionSingleCalibration(tr("cal-q-hardware", "name"),
                                                 tr("Hardware MFM voltage to flow rate calibration"),
                                                 tr("This is the calibration used to convert the hardware measured "
                                                    "MFM voltage into a flow in LPM.  This is normally only used "
                                                    "to back out the calculation and calculate a voltage, however "
                                                    "if the instrument is operated in interactive mode it will "
                                                    "be set during communications start."), "");
    cal->setAllowInvalid(false);
    cal->setAllowConstant(true);
    options.add("cal-q-hardware", cal);

    return options;
}

QList<ComponentExample> AcquireGMDCLAP3WComponent::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples());
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireGMDCLAP3WComponent::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireGMDCLAP3WComponent::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options);
}

QList<ComponentExample> AcquireGMDCLAP3WComponent::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(
            ComponentExample(options, tr("Convert data with the default areas and calibrations")));

    options = getBaseOptions();
    (qobject_cast<ComponentOptionSingleDouble *>(options.get("area0")))->set(19.74);
    (qobject_cast<ComponentOptionSingleDouble *>(options.get("area1")))->set(19.71);
    (qobject_cast<ComponentOptionSingleDouble *>(options.get("area2")))->set(20.05);
    (qobject_cast<ComponentOptionSingleDouble *>(options.get("area3")))->set(19.25);
    (qobject_cast<ComponentOptionSingleDouble *>(options.get("area4")))->set(19.76);
    (qobject_cast<ComponentOptionSingleDouble *>(options.get("area5")))->set(19.65);
    (qobject_cast<ComponentOptionSingleDouble *>(options.get("area6")))->set(19.47);
    (qobject_cast<ComponentOptionSingleDouble *>(options.get("area7")))->set(19.88);
    (qobject_cast<ComponentOptionSingleDouble *>(options.get("cal-q")))->set(1.004);
    examples.append(
            ComponentExample(options, tr("Explicitly defined areas with a flow calibration")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireGMDCLAP3WComponent::createAcquisitionPassive(const ComponentOptions &options,
                                                                                  const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireGMDCLAP3W(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireGMDCLAP3WComponent::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireGMDCLAP3W(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireGMDCLAP3WComponent::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                    const std::string &loggingContext)
{
    std::unique_ptr<AcquireGMDCLAP3W> i(new AcquireGMDCLAP3W(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<
        AcquisitionInterface> AcquireGMDCLAP3WComponent::createAcquisitionInteractive(const ValueSegment::Transfer &config,
                                                                                      const std::string &loggingContext)
{
    std::unique_ptr<AcquireGMDCLAP3W> i(new AcquireGMDCLAP3W(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
