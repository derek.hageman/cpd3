/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */



#include "core/first.hxx"

#include "core/environment.hxx"

#include "acquire_gmd_clap3w.hxx"

using namespace CPD3;
using namespace CPD3::Data;

Variant::Root AcquireGMDCLAP3W::buildFilterValueMeta() const
{
    Variant::Root root;
    auto result = root.write();
    result.metadataHashChild("ID").metadataArray("Description").setString("Dark detector count");
    result.metadataHashChild("ID").metadataArray("Count").setInt64(10);
    result.metadataHashChild("ID")
          .metadataArray("Children")
          .metadataReal("Format")
          .setString("0000.00");
    result.metadataHashChild("IB")
          .metadataArray("Description")
          .setString("Detector illuminated count");
    result.metadataHashChild("IB")
          .metadataArray("Wavelength")
          .setDouble(config.first().wavelengths[0]);
    result.metadataHashChild("IB").metadataArray("Count").setInt64(10);
    result.metadataHashChild("IB")
          .metadataArray("Children")
          .metadataReal("Format")
          .setString("0000000.00");
    result.metadataHashChild("IG")
          .metadataArray("Description")
          .setString("Detector illuminated count");
    result.metadataHashChild("IG")
          .metadataArray("Wavelength")
          .setDouble(config.first().wavelengths[1]);
    result.metadataHashChild("IG").metadataArray("Count").setInt64(10);
    result.metadataHashChild("IG")
          .metadataArray("Children")
          .metadataReal("Format")
          .setString("0000000.00");
    result.metadataHashChild("IR")
          .metadataArray("Description")
          .setString("Detector illuminated count");
    result.metadataHashChild("IR")
          .metadataArray("Wavelength")
          .setDouble(config.first().wavelengths[2]);
    result.metadataHashChild("IR").metadataArray("Count").setInt64(10);
    result.metadataHashChild("IR")
          .metadataArray("Children")
          .metadataReal("Format")
          .setString("0000000.00");
    result.metadataHashChild("InB")
          .metadataArray("Description")
          .setString("Normalized sample spot intensity");
    result.metadataHashChild("InB")
          .metadataArray("Wavelength")
          .setDouble(config.first().wavelengths[0]);
    result.metadataHashChild("InB").metadataArray("Count").setInt64(8);
    result.metadataHashChild("InB")
          .metadataArray("Children")
          .metadataReal("Format")
          .setString("00.0000000");
    result.metadataHashChild("InG")
          .metadataArray("Description")
          .setString("Normalized sample spot intensity");
    result.metadataHashChild("InG")
          .metadataArray("Wavelength")
          .setDouble(config.first().wavelengths[1]);
    result.metadataHashChild("InG").metadataArray("Count").setInt64(8);
    result.metadataHashChild("InG")
          .metadataArray("Children")
          .metadataReal("Format")
          .setString("00.0000000");
    result.metadataHashChild("InR")
          .metadataArray("Description")
          .setString("Normalized sample spot intensity");
    result.metadataHashChild("InR")
          .metadataArray("Wavelength")
          .setDouble(config.first().wavelengths[2]);
    result.metadataHashChild("InR").metadataArray("Count").setInt64(8);
    result.metadataHashChild("InR")
          .metadataArray("Children")
          .metadataReal("Format")
          .setString("00.0000000");
    return root;
}

SequenceValue::Transfer AcquireGMDCLAP3W::buildLogMeta(double time) const
{
    Q_ASSERT(!config.isEmpty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_gmd_clap3w");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);

    result.emplace_back(SequenceName({}, "raw_meta", "Q"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Sample flow");
    result.back().write().metadataReal("ReportT").setDouble(0);
    result.back().write().metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Flow"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);

    result.emplace_back(SequenceName({}, "raw_meta", "Qt"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000.00000");
    result.back().write().metadataReal("Units").setString("m³");
    result.back().write().metadataReal("Description").setString("Accumulated sample volume");
    result.back().write().metadataReal("ReportT").setDouble(0);
    result.back().write().metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");

    result.emplace_back(SequenceName({}, "raw_meta", "L"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0000");
    result.back().write().metadataReal("Units").setString("m");
    result.back().write().metadataReal("Description").setString("Integrated sample length, Qt/A");
    result.back().write().metadataReal("ReportT").setDouble(0);
    result.back().write().metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");

    result.emplace_back(SequenceName({}, "raw_meta", "T1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Sample temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);

    result.emplace_back(SequenceName({}, "raw_meta", "T2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Case temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Case"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);

    result.emplace_back(SequenceName({}, "raw_meta", "IrB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadataReal("Format").setString("0.0000000");
    result.back().write().metadataReal("GroupUnits").setString("Transmittance");
    result.back().write().metadataReal("Description").setString("Transmittance");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("Transmittance"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Blue"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "IrG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadataReal("Format").setString("0.0000000");
    result.back().write().metadataReal("GroupUnits").setString("Transmittance");
    result.back().write().metadataReal("Description").setString("Transmittance");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("Transmittance"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Green"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "IrR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadataReal("Format").setString("0.0000000");
    result.back().write().metadataReal("GroupUnits").setString("Transmittance");
    result.back().write().metadataReal("Description").setString("Transmittance");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("Transmittance"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Red"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "BaB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back().write().metadataReal("Description")
          .setString("Aerosol light absorption coefficient");
    result.back().write().metadataReal("ReportT").setDouble(0);
    result.back().write().metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write().metadataReal("Smoothing").hash("Mode").setString("BeersLawAbsorptionInitial");
    result.back().write().metadataReal("Smoothing").hash("Parameters").hash("L").setString("L");
    result.back()
          .write()
          .metadataReal("Smoothing")
          .hash("Parameters")
          .hash("Transmittance")
          .setString("IrB");
    result.back().write().metadataReal("Realtime").hash("GroupName").setString(QObject::tr("Bap"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Blue"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "BaG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back().write().metadataReal("Description")
          .setString("Aerosol light absorption coefficient");
    result.back().write().metadataReal("ReportT").setDouble(0);
    result.back().write().metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write().metadataReal("Smoothing").hash("Mode").setString("BeersLawAbsorptionInitial");
    result.back().write().metadataReal("Smoothing").hash("Parameters").hash("L").setString("L");
    result.back()
          .write()
          .metadataReal("Smoothing")
          .hash("Parameters")
          .hash("Transmittance")
          .setString("IrG");
    result.back().write().metadataReal("Realtime").hash("GroupName").setString(QObject::tr("Bap"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Green"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "BaR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back().write().metadataReal("Description")
          .setString("Aerosol light absorption coefficient");
    result.back().write().metadataReal("ReportT").setDouble(0);
    result.back().write().metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write().metadataReal("Smoothing").hash("Mode").setString("BeersLawAbsorptionInitial");
    result.back().write().metadataReal("Smoothing").hash("Parameters").hash("L").setString("L");
    result.back()
          .write()
          .metadataReal("Smoothing")
          .hash("Parameters")
          .hash("Transmittance")
          .setString("IrR");
    result.back().write().metadataReal("Realtime").hash("GroupName").setString(QObject::tr("Bap"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Red"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "IfB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadataReal("Format").setString("0000000.00");
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back().write().metadataReal("Description").setString("Reference detector signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("I reference"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Blue"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);

    result.emplace_back(SequenceName({}, "raw_meta", "IfG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadataReal("Format").setString("0000000.00");
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back().write().metadataReal("Description").setString("Reference detector signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("I reference"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Green"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);

    result.emplace_back(SequenceName({}, "raw_meta", "IfR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadataReal("Format").setString("0000000.00");
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back().write().metadataReal("Description").setString("Reference detector signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("I reference"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Red"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);

    result.emplace_back(SequenceName({}, "raw_meta", "IpB"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
    result.back().write().metadataReal("Format").setString("0000000.00");
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back().write().metadataReal("Description").setString("Sample detector signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("I sample"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Blue"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);

    result.emplace_back(SequenceName({}, "raw_meta", "IpG"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
    result.back().write().metadataReal("Format").setString("0000000.00");
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back().write().metadataReal("Description").setString("Sample detector signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("I sample"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Green"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);

    result.emplace_back(SequenceName({}, "raw_meta", "IpR"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
    result.back().write().metadataReal("Format").setString("0000000.00");
    result.back().write().metadataReal("GroupUnits").setString("Intensity");
    result.back().write().metadataReal("Description").setString("Sample detector signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupName")
          .setString(QObject::tr("I sample"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("GroupColumn")
          .setString(QObject::tr("Red"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);

    result.emplace_back(SequenceName({}, "raw_meta", "Ff"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataInteger("Format").setString("00000000");
    result.back().write().metadataInteger("GroupUnits").setString("FilterID");
    result.back().write().metadataInteger("Description").setString("Filter ID");
    result.back().write().metadataInteger("Source").set(instrumentMeta);
    result.back().write().metadataInteger("Processing").toArray().after_back().set(processing);
    result.back().write().metadataInteger("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataInteger("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataInteger("Realtime").hash("Hide").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "Fn"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataInteger("Format").setString("00");
    result.back().write().metadataInteger("GroupUnits").setString("SpotNumber");
    result.back().write().metadataInteger("Description").setString("Active spot number");
    result.back().write().metadataInteger("Source").set(instrumentMeta);
    result.back().write().metadataInteger("Processing").toArray().after_back().set(processing);
    result.back().write().metadataInteger("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataInteger("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataInteger("Realtime").hash("Name").setString(QObject::tr("Spot"));
    result.back().write().metadataInteger("Realtime").hash("RowOrder").setInt64(7);
    if (config.first().realtimeDiagnostics) {
        result.back().write().metadataInteger("Realtime").hash("PageMask").setInt64(0x7F);
    }

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataSingleFlag("STP")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");
    result.back().write().metadataSingleFlag("STP").hash("Bits").setInt64(0x0200);
    result.back()
          .write()
          .metadataSingleFlag("STP")
          .hash("Description")
          .setString("Data reported at STP");

    result.back()
          .write()
          .metadataSingleFlag("FilterChanging")
          .hash("Description")
          .setString("Changing filter");
    result.back().write().metadataSingleFlag("FilterChanging").hash("Bits").setInt64(0x00010000);
    result.back()
          .write()
          .metadataSingleFlag("FilterChanging")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("WhiteFilterChanging")
          .hash("Description")
          .setString("Changing to a known white filter");
    result.back()
          .write()
          .metadataSingleFlag("WhiteFilterChanging")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("Normalizing")
          .hash("Description")
          .setString("Establishing normalization factors for the active spot");
    result.back()
          .write()
          .metadataSingleFlag("Normalizing")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("FlowError")
          .hash("Description")
          .setString("Flow error");
    result.back().write().metadataSingleFlag("FlowError").hash("Bits").setInt64(0x00020000);
    result.back()
          .write()
          .metadataSingleFlag("FlowError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("BlueTransmittanceMedium")
          .hash("Description")
          .setString("Blue transmittance less than 0.7");
    result.back()
          .write()
          .metadataSingleFlag("BlueTransmittanceMedium")
          .hash("Bits")
          .setInt64(0x00040000);
    result.back()
          .write()
          .metadataSingleFlag("BlueTransmittanceMedium")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("BlueTransmittanceLow")
          .hash("Description")
          .setString("Blue transmittance less than 0.5");
    result.back()
          .write()
          .metadataSingleFlag("BlueTransmittanceLow")
          .hash("Bits")
          .setInt64(0x00080000);
    result.back()
          .write()
          .metadataSingleFlag("BlueTransmittanceLow")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("GreenTransmittanceMedium")
          .hash("Description")
          .setString("Green transmittance less than 0.7");
    result.back()
          .write()
          .metadataSingleFlag("GreenTransmittanceMedium")
          .hash("Bits")
          .setInt64(0x00100000);
    result.back()
          .write()
          .metadataSingleFlag("GreenTransmittanceMedium")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("GreenTransmittanceLow")
          .hash("Description")
          .setString("Green transmittance less than 0.5");
    result.back()
          .write()
          .metadataSingleFlag("GreenTransmittanceLow")
          .hash("Bits")
          .setInt64(0x00200000);
    result.back()
          .write()
          .metadataSingleFlag("GreenTransmittanceLow")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("RedTransmittanceMedium")
          .hash("Description")
          .setString("Red transmittance less than 0.7");
    result.back()
          .write()
          .metadataSingleFlag("RedTransmittanceMedium")
          .hash("Bits")
          .setInt64(0x00400000);
    result.back()
          .write()
          .metadataSingleFlag("RedTransmittanceMedium")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("RedTransmittanceLow")
          .hash("Description")
          .setString("Red transmittance less than 0.5");
    result.back()
          .write()
          .metadataSingleFlag("RedTransmittanceLow")
          .hash("Bits")
          .setInt64(0x00800000);
    result.back()
          .write()
          .metadataSingleFlag("RedTransmittanceLow")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("LampError")
          .hash("Description")
          .setString("Lamp error");
    result.back().write().metadataSingleFlag("LampError").hash("Bits").setInt64(0x01000000);
    result.back()
          .write()
          .metadataSingleFlag("LampError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("TemperatureOutOfRange")
          .hash("Description")
          .setString("Temperature out of range");
    result.back()
          .write()
          .metadataSingleFlag("TemperatureOutOfRange")
          .hash("Bits")
          .setInt64(0x02000000);
    result.back()
          .write()
          .metadataSingleFlag("TemperatureOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("CaseTemperatureUnstable")
          .hash("Description")
          .setString("Case temperature too far from setpoint");
    result.back()
          .write()
          .metadataSingleFlag("CaseTemperatureUnstable")
          .hash("Bits")
          .setInt64(0x04000000);
    result.back()
          .write()
          .metadataSingleFlag("CaseTemperatureUnstable")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("NonWhiteFilter")
          .hash("Description")
          .setString("Filter did not appear to be white");
    result.back()
          .write()
          .metadataSingleFlag("NonWhiteFilter")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("ContaminateFilterDiscard")
          .hash("Description")
          .setString("Data contaminated due to filter change discard period");
    result.back()
          .write()
          .metadataSingleFlag("ContaminateFilterDiscard")
          .hash("Bits")
          .setInt64(0x000F);
    result.back()
          .write()
          .metadataSingleFlag("ContaminateFilterDiscard")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");


    result.emplace_back(SequenceName({}, "raw_meta", "ZFILTER"), buildFilterValueMeta(), time,
                        FP::undefined());
    result.back().write().metadataHash("Description").setString("Filter start parameters");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
    result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");

    result.emplace_back(SequenceName({}, "raw_meta", "ZWHITE"), buildFilterValueMeta(), time,
                        FP::undefined());
    result.back().write().metadataHash("Description").setString("White filter parameters");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
    result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");

    result.emplace_back(SequenceName({}, "raw_meta", "ZSPOT"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataHash("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataHash("Description").setString("Spot sampling parameters");
    result.back().write().metadataHash("Source").set(instrumentMeta);
    result.back().write().metadataHash("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataHashChild("InB")
          .metadataReal("Description")
          .setString("Spot start normalized intensity");
    result.back()
          .write()
          .metadataHashChild("InB")
          .metadataReal("Wavelength")
          .setDouble(config.first().wavelengths[0]);
    result.back().write().metadataHashChild("InB").metadataReal("Format").setString("00.0000000");
    result.back()
          .write()
          .metadataHashChild("InG")
          .metadataReal("Description")
          .setString("Spot start normalized intensity");
    result.back()
          .write()
          .metadataHashChild("InG")
          .metadataReal("Wavelength")
          .setDouble(config.first().wavelengths[1]);
    result.back().write().metadataHashChild("InG").metadataReal("Format").setString("00.0000000");
    result.back()
          .write()
          .metadataHashChild("InR")
          .metadataReal("Description")
          .setString("Spot start normalized intensity");
    result.back()
          .write()
          .metadataHashChild("InR")
          .metadataReal("Wavelength")
          .setDouble(config.first().wavelengths[2]);
    result.back().write().metadataHashChild("InR").metadataReal("Format").setString("00.0000000");
    result.back()
          .write()
          .metadataHashChild("InBLatest")
          .metadataReal("Description")
          .setString("Spot end normalized intensity");
    result.back()
          .write()
          .metadataHashChild("InBLatest")
          .metadataReal("Wavelength")
          .setDouble(config.first().wavelengths[0]);
    result.back()
          .write()
          .metadataHashChild("InBLatest")
          .metadataReal("Format")
          .setString("00.0000000");
    result.back()
          .write()
          .metadataHashChild("InGLatest")
          .metadataReal("Description")
          .setString("Spot end normalized intensity");
    result.back()
          .write()
          .metadataHashChild("InGLatest")
          .metadataReal("Wavelength")
          .setDouble(config.first().wavelengths[1]);
    result.back()
          .write()
          .metadataHashChild("InGLatest")
          .metadataReal("Format")
          .setString("00.0000000");
    result.back()
          .write()
          .metadataHashChild("InRLatest")
          .metadataReal("Description")
          .setString("Spot end normalized intensity");
    result.back()
          .write()
          .metadataHashChild("InRLatest")
          .metadataReal("Wavelength")
          .setDouble(config.first().wavelengths[2]);
    result.back()
          .write()
          .metadataHashChild("InRLatest")
          .metadataReal("Format")
          .setString("00.0000000");
    result.back()
          .write()
          .metadataHashChild("IrB")
          .metadataReal("Description")
          .setString("Spot end transmittance");
    result.back()
          .write()
          .metadataHashChild("IrB")
          .metadataReal("Wavelength")
          .setDouble(config.first().wavelengths[0]);
    result.back().write().metadataHashChild("IrB").metadataReal("Format").setString("0.0000000");
    result.back()
          .write()
          .metadataHashChild("IrG")
          .metadataReal("Description")
          .setString("Spot end transmittance");
    result.back()
          .write()
          .metadataHashChild("IrG")
          .metadataReal("Wavelength")
          .setDouble(config.first().wavelengths[1]);
    result.back().write().metadataHashChild("IrG").metadataReal("Format").setString("0.0000000");
    result.back()
          .write()
          .metadataHashChild("IrR")
          .metadataReal("Description")
          .setString("Spot end transmittance");
    result.back()
          .write()
          .metadataHashChild("IrR")
          .metadataReal("Wavelength")
          .setDouble(config.first().wavelengths[2]);
    result.back().write().metadataHashChild("IrR").metadataReal("Format").setString("0.0000000");
    result.back()
          .write()
          .metadataHashChild("Ff")
          .metadataInteger("Description")
          .setString("Filter ID");
    result.back().write().metadataHashChild("Ff").metadataInteger("Format").setString("00000000");
    result.back()
          .write()
          .metadataHashChild("Fn")
          .metadataInteger("Description")
          .setString("Spot number");
    result.back().write().metadataHashChild("Fn").metadataInteger("Format").setString("00");
    result.back()
          .write()
          .metadataHashChild("ETIME")
          .metadataInteger("Description")
          .setString("Instrument elapsed time at end of spot");
    result.back()
          .write()
          .metadataHashChild("ETIME")
          .metadataInteger("Format")
          .setString("000000000000");
    result.back().write().metadataHashChild("ETIME").metadataInteger("Units").setString("seconds");
    result.back()
          .write()
          .metadataHashChild("Qt")
          .metadataReal("Description")
          .setString("Total spot sample volume");
    result.back().write().metadataHashChild("Qt").metadataReal("Format").setString("000.00");
    result.back().write().metadataHashChild("Qt").metadataReal("Units").setString("m³");
    result.back().write().metadataHashChild("Qt").metadataReal("ReportT").setDouble(0);
    result.back().write().metadataHashChild("Qt").metadataReal("ReportP").setDouble(1013.25);
    result.back()
          .write()
          .metadataHashChild("L")
          .metadataReal("Description")
          .setString("Total integrated spot sample length, Qt/A");
    result.back().write().metadataHashChild("L").metadataReal("Format").setString("00000.0000");
    result.back().write().metadataHashChild("L").metadataReal("Units").setString("m");
    result.back().write().metadataHashChild("L").metadataReal("ReportT").setDouble(0);
    result.back().write().metadataHashChild("L").metadataReal("ReportP").setDouble(1013.25);
    result.back()
          .write()
          .metadataHashChild("Area")
          .metadataReal("Description")
          .setString("Spot area");
    result.back().write().metadataHashChild("Area").metadataReal("Format").setString("00.000");
    result.back().write().metadataHashChild("Area").metadataReal("Units").setString("mm²");
    result.back()
          .write()
          .metadataHashChild("WasWhite")
          .metadataBoolean("Description")
          .setString("Spot was determined to be white at the start of sampling");

    return result;
}

SequenceValue::Transfer AcquireGMDCLAP3W::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_gmd_clap3w");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);

    result.emplace_back(SequenceName({}, "raw_meta", "VQ"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0000");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Flow meter input voltage");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Flow voltage"));
    result.back().write().metadataReal("Realtime").hash("Units").setString(QString());
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);

    /* So we can output flags that match the instrument data */
    result.emplace_back(SequenceName({}, "raw_meta", "ZF1"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);
    result.back().write().metadataFlags("Realtime").hash("Name").setString(QObject::tr("Flags"));
    result.back().write().metadataFlags("Realtime").hash("RowOrder").setInt64(6);

    result.back()
          .write()
          .metadataSingleFlag("STP")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");
    result.back()
          .write()
          .metadataSingleFlag("STP")
          .hash("Description")
          .setString("Data reported at STP");

    result.back()
          .write()
          .metadataSingleFlag("FilterChanging")
          .hash("Description")
          .setString("Changing filter");
    result.back().write().metadataSingleFlag("FilterChanging").hash("Bits").setInt64(0x0001);
    result.back()
          .write()
          .metadataSingleFlag("FilterChanging")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("WhiteFilterChanging")
          .hash("Description")
          .setString("Changing to a known white filter");
    result.back()
          .write()
          .metadataSingleFlag("WhiteFilterChanging")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("Normalizing")
          .hash("Description")
          .setString("Establishing normalization factors for the active spot");
    result.back()
          .write()
          .metadataSingleFlag("Normalizing")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("FlowError")
          .hash("Description")
          .setString("Flow error");
    result.back().write().metadataSingleFlag("FlowError").hash("Bits").setInt64(0x0002);
    result.back()
          .write()
          .metadataSingleFlag("FlowError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("BlueTransmittanceMedium")
          .hash("Description")
          .setString("Blue transmittance less than 0.7");
    result.back()
          .write()
          .metadataSingleFlag("BlueTransmittanceMedium")
          .hash("Bits")
          .setInt64(0x0004);
    result.back()
          .write()
          .metadataSingleFlag("BlueTransmittanceMedium")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("BlueTransmittanceLow")
          .hash("Description")
          .setString("Blue transmittance less than 0.5");
    result.back().write().metadataSingleFlag("BlueTransmittanceLow").hash("Bits").setInt64(0x0008);
    result.back()
          .write()
          .metadataSingleFlag("BlueTransmittanceLow")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("GreenTransmittanceMedium")
          .hash("Description")
          .setString("Green transmittance less than 0.7");
    result.back()
          .write()
          .metadataSingleFlag("GreenTransmittanceMedium")
          .hash("Bits")
          .setInt64(0x0010);
    result.back()
          .write()
          .metadataSingleFlag("GreenTransmittanceMedium")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("GreenTransmittanceLow")
          .hash("Description")
          .setString("Green transmittance less than 0.5");
    result.back().write().metadataSingleFlag("GreenTransmittanceLow").hash("Bits").setInt64(0x0020);
    result.back()
          .write()
          .metadataSingleFlag("GreenTransmittanceLow")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("RedTransmittanceMedium")
          .hash("Description")
          .setString("Red transmittance less than 0.7");
    result.back()
          .write()
          .metadataSingleFlag("RedTransmittanceMedium")
          .hash("Bits")
          .setInt64(0x0040);
    result.back()
          .write()
          .metadataSingleFlag("RedTransmittanceMedium")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("RedTransmittanceLow")
          .hash("Description")
          .setString("Red transmittance less than 0.5");
    result.back().write().metadataSingleFlag("RedTransmittanceLow").hash("Bits").setInt64(0x0080);
    result.back()
          .write()
          .metadataSingleFlag("RedTransmittanceLow")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("LampError")
          .hash("Description")
          .setString("Lamp error");
    result.back().write().metadataSingleFlag("LampError").hash("Bits").setInt64(0x0100);
    result.back()
          .write()
          .metadataSingleFlag("LampError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("TemperatureOutOfRange")
          .hash("Description")
          .setString("Temperature out of range");
    result.back().write().metadataSingleFlag("TemperatureOutOfRange").hash("Bits").setInt64(0x0200);
    result.back()
          .write()
          .metadataSingleFlag("TemperatureOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("CaseTemperatureUnstable")
          .hash("Description")
          .setString("Case temperature too far from setpoint");
    result.back()
          .write()
          .metadataSingleFlag("CaseTemperatureUnstable")
          .hash("Bits")
          .setInt64(0x0400);
    result.back()
          .write()
          .metadataSingleFlag("CaseTemperatureUnstable")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("NonWhiteFilter")
          .hash("Description")
          .setString("Filter did not appear to be white");
    result.back()
          .write()
          .metadataSingleFlag("NonWhiteFilter")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");

    result.back()
          .write()
          .metadataSingleFlag("ContaminateFilterDiscard")
          .hash("Description")
          .setString("Data contaminated due to filter change discard period");
    result.back()
          .write()
          .metadataSingleFlag("ContaminateFilterDiscard")
          .hash("Bits")
          .setInt64(0x000F);
    result.back()
          .write()
          .metadataSingleFlag("ContaminateFilterDiscard")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_gmd_clap3w");


    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataString("Realtime").hash("Name").setString(QString());
    result.back().write().metadataString("Realtime").hash("Units").setString(QString());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back().write().metadataString("Realtime").hash("RowOrder").setInt64(8);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Run")
          .setString(QObject::tr("", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Normalize")
          .setString(QObject::tr("Waiting for spot stability..."));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SpotAdvance")
          .setString(QObject::tr("Advancing spot..."));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("FilterBaselineStart")
          .setString(QObject::tr("Establishing filter baseline..."));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("FilterBaseline")
          .setString(QObject::tr("Establishing filter baseline..."));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("FilterChangeStart")
          .setString(QObject::tr("Starting filter change..."));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("FilterChange")
          .setString(QObject::tr("Changing filter..."));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("WhiteFilterBaselineStart")
          .setString(QObject::tr("Establishing white filter baseline..."));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("WhiteFilterBaseline")
          .setString(QObject::tr("Establishing white filter baseline..."));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("WhiteFilterChangeStart")
          .setString(QObject::tr("Starting white filter change..."));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("WhiteFilterChange")
          .setString(QObject::tr("Changing white filter..."));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("BypassedRun")
          .setString(QObject::tr("FLOW BYPASSED"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("BypassedNormalize")
          .setString(QObject::tr("FLOW BYPASSED DURING SPOT NORMALIZE"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("BypassedFilterBaselineStart")
          .setString(QObject::tr("FLOW BYPASSED DURING FILTER BASELINE"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("BypassedFilterBaseline")
          .setString(QObject::tr("FLOW BYPASSED DURING FILTER BASELINE"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("BypassedFilterChangeStart")
          .setString(QObject::tr("FLOW BYPASSED DURING FILTER CHANGE"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("BypassedFilterChange")
          .setString(QObject::tr("FLOW BYPASSED DURING FILTER CHANGE"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("BypassedWhiteFilterBaselineStart")
          .setString(QObject::tr("FLOW BYPASSED DURING WHITE FILTER BASELINE"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("BypassedWhiteFilterBaseline")
          .setString(QObject::tr("FLOW BYPASSED DURING WHITE FILTER BASELINE"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("BypassedWhiteFilterChangeStart")
          .setString(QObject::tr("FLOW BYPASSED DURING WHITE FILTER CHANGE"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("BypassedWhiteFilterChange")
          .setString(QObject::tr("FLOW BYPASSED DURING WHITE FILTER CHANGE"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("UnbypassRun")
          .setString(QObject::tr("Flow resume in progress..."));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("UnbypassNormalize")
          .setString(QObject::tr("Flow resume in progress..."));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("RequireFilterChange")
          .setString(QObject::tr("FILTER CHANGE REQUIRED"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("RequireWhiteFilterChange")
          .setString(QObject::tr("WHITE FILTER CHANGE REQUIRED"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("BypassedRequireFilterChange")
          .setString(QObject::tr("FILTER CHANGE REQUIRED"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("BypassedRequireWhiteFilterChange")
          .setString(QObject::tr("WHITE FILTER CHANGE REQUIRED"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidRecord")
          .setString(QObject::tr("NO COMMS: Invalid record"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidResponse")
          .setString(QObject::tr("NO COMMS: Invalid response"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Timeout")
          .setString(QObject::tr("NO COMMS: Timeout"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractive")
          .setString(QObject::tr("STARTING COMMS"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadFirmwareVersionFlushing")
          .setString(QObject::tr("STARTING COMMS: Reading firmware version"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadFirmwareVersion")
          .setString(QObject::tr("STARTING COMMS: Reading firmware version"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveWriteHardwareFlowCalibrationFlushing")
          .setString(QObject::tr("STARTING COMMS: Writing flow calibration"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadHardwareFlowCalibration")
          .setString(QObject::tr("STARTING COMMS: Reading flow calibration"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveStopReportsFlushing")
          .setString(QObject::tr("STARTING COMMS: Stopping unpolled records"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadSerialNumber")
          .setString(QObject::tr("STARTING COMMS: Reading serial number"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveMainMenuReturnFlushing")
          .setString(QObject::tr("STARTING COMMS: Menu navigation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveConfigMenuFlushing")
          .setString(QObject::tr("STARTING COMMS: Menu navigation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveMainMenuFlushing")
          .setString(QObject::tr("STARTING COMMS: Menu navigation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveCalMenuFlushing")
          .setString(QObject::tr("STARTING COMMS: Menu navigation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveUnpolledStartMainMenuFlushing")
          .setString(QObject::tr("STARTING COMMS: Menu navigation"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveUnpolledStartFlushing")
          .setString(QObject::tr("STARTING COMMS: Flushing initial unpolled"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveUnpolledStartDiscardFirstLine")
          .setString(QObject::tr("STARTING COMMS: Flushing initial unpolled"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveUnpolledStartReadFirstRecord")
          .setString(QObject::tr("STARTING COMMS: Waiting for first record"));

    Q_ASSERT(!config.isEmpty());
    for (int i = 0; i < 10; i++) {
        if (!config.first().realtimeDiagnostics)
            continue;

        QString groupName;
        if (i == 0) {
            groupName = QObject::tr("Even Ref");
        } else if (i == 9) {
            groupName = QObject::tr("Odd Ref");
        } else {
            groupName = QObject::tr("Spot %1").arg(i);
        }

        result.emplace_back(SequenceName({}, "raw_meta", "I" + std::to_string(i + 1) + "D"),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Format").setString("0000.00");
        result.back().write().metadataReal("GroupUnits").setString("IntensityDark");
        result.back().write().metadataReal("Description").setString("Detector dark count");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(3);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Eavesdropper")
              .hash("Verbose")
              .setBool(true);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Box")
              .setString(QObject::tr("Raw Intensities"));
        result.back().write().metadataReal("Realtime").hash("GroupName").setString(groupName);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Dark"));
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(3);

        result.emplace_back(SequenceName({}, "raw_meta", "I" + std::to_string(i + 1) + "B"),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
        result.back().write().metadataReal("Format").setString("0000000.00");
        result.back().write().metadataReal("GroupUnits").setString("Intensity");
        result.back().write().metadataReal("Description").setString("Detector illuminated count");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(3);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Eavesdropper")
              .hash("Verbose")
              .setBool(true);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Box")
              .setString(QObject::tr("Raw Intensities"));
        result.back().write().metadataReal("Realtime").hash("GroupName").setString(groupName);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Blue"));
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(0);

        result.emplace_back(SequenceName({}, "raw_meta", "I" + std::to_string(i + 1) + "G"),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
        result.back().write().metadataReal("Format").setString("0000000.00");
        result.back().write().metadataReal("GroupUnits").setString("Intensity");
        result.back().write().metadataReal("Description").setString("Detector illuminated count");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(3);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Eavesdropper")
              .hash("Verbose")
              .setBool(true);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Box")
              .setString(QObject::tr("Raw Intensities"));
        result.back().write().metadataReal("Realtime").hash("GroupName").setString(groupName);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Green"));
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(1);

        result.emplace_back(SequenceName({}, "raw_meta", "I" + std::to_string(i + 1) + "R"),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
        result.back().write().metadataReal("Format").setString("0000000.00");
        result.back().write().metadataReal("GroupUnits").setString("Intensity");
        result.back().write().metadataReal("Description").setString("Detector illuminated count");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(3);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Eavesdropper")
              .hash("Verbose")
              .setBool(true);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Box")
              .setString(QObject::tr("Raw Intensities"));
        result.back().write().metadataReal("Realtime").hash("GroupName").setString(groupName);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Red"));
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(2);

        result.emplace_back(SequenceName({}, "raw_meta", "Ig" + std::to_string(i + 1) + "D"),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Format").setString("0.0000000");
        result.back().write().metadataReal("GroupUnits").setString("StabilityFactor");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Detector dark count stability factor");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(3);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Eavesdropper")
              .hash("Hide")
              .setBool(true);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(2);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Box")
              .setString(QObject::tr("Raw Intensity Stability"));
        result.back().write().metadataReal("Realtime").hash("GroupName").setString(groupName);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Dark"));
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(3);

        result.emplace_back(SequenceName({}, "raw_meta", "Ig" + std::to_string(i + 1) + "B"),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
        result.back().write().metadataReal("Format").setString("0.0000000");
        result.back().write().metadataReal("GroupUnits").setString("StabilityFactor");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Detector illuminated count stability factor");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(3);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Eavesdropper")
              .hash("Hide")
              .setBool(true);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(2);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Box")
              .setString(QObject::tr("Raw Intensity Stability"));
        result.back().write().metadataReal("Realtime").hash("GroupName").setString(groupName);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Blue"));
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(0);

        result.emplace_back(SequenceName({}, "raw_meta", "Ig" + std::to_string(i + 1) + "G"),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
        result.back().write().metadataReal("Format").setString("0.0000000");
        result.back().write().metadataReal("GroupUnits").setString("StabilityFactor");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Detector illuminated count stability factor");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(3);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Eavesdropper")
              .hash("Hide")
              .setBool(true);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(2);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Box")
              .setString(QObject::tr("Raw Intensity Stability"));
        result.back().write().metadataReal("Realtime").hash("GroupName").setString(groupName);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Green"));
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(1);

        result.emplace_back(SequenceName({}, "raw_meta", "Ig" + std::to_string(i + 1) + "R"),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
        result.back().write().metadataReal("Format").setString("0.0000000");
        result.back().write().metadataReal("GroupUnits").setString("StabilityFactor");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Detector illuminated count stability factor");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(3);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Eavesdropper")
              .hash("Hide")
              .setBool(true);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(2);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Box")
              .setString(QObject::tr("Raw Intensity Stability"));
        result.back().write().metadataReal("Realtime").hash("GroupName").setString(groupName);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Red"));
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(2);
    }
    for (int i = 0; i < 8; i++) {
        if (!config.first().realtimeDiagnostics)
            continue;

        QString groupName(QObject::tr("Spot %1").arg(i + 1));

        result.emplace_back(SequenceName({}, "raw_meta", "In" + std::to_string(i + 1) + "B"),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
        result.back().write().metadataReal("Format").setString("00.0000000");
        result.back().write().metadataReal("GroupUnits").setString("NormalizedIntensity");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Normalized sample spot intensity");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(3);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Eavesdropper")
              .hash("Hide")
              .setBool(true);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(3);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Box")
              .setString(QObject::tr("Normalized Intensity"));
        result.back().write().metadataReal("Realtime").hash("GroupName").setString(groupName);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Blue"));

        result.emplace_back(SequenceName({}, "raw_meta", "In" + std::to_string(i + 1) + "G"),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
        result.back().write().metadataReal("Format").setString("00.0000000");
        result.back().write().metadataReal("GroupUnits").setString("NormalizedIntensity");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Normalized sample spot intensity");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(3);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Eavesdropper")
              .hash("Hide")
              .setBool(true);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(3);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Box")
              .setString(QObject::tr("Normalized Intensity"));
        result.back().write().metadataReal("Realtime").hash("GroupName").setString(groupName);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Green"));

        result.emplace_back(SequenceName({}, "raw_meta", "In" + std::to_string(i + 1) + "R"),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
        result.back().write().metadataReal("Format").setString("00.0000000");
        result.back().write().metadataReal("GroupUnits").setString("NormalizedIntensity");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Normalized sample spot intensity");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(3);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Eavesdropper")
              .hash("Hide")
              .setBool(true);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(3);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Box")
              .setString(QObject::tr("Normalized Intensity"));
        result.back().write().metadataReal("Realtime").hash("GroupName").setString(groupName);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Red"));

        result.emplace_back(SequenceName({}, "raw_meta", "Ing" + std::to_string(i + 1) + "B"),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
        result.back().write().metadataReal("Format").setString("0.0000000");
        result.back().write().metadataReal("GroupUnits").setString("StabilityFactor");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Normalized sample spot intensity stability factor");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(3);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Eavesdropper")
              .hash("Hide")
              .setBool(true);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(4);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Box")
              .setString(QObject::tr("Normalized Intensity Stability"));
        result.back().write().metadataReal("Realtime").hash("GroupName").setString(groupName);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Blue"));

        result.emplace_back(SequenceName({}, "raw_meta", "Ing" + std::to_string(i + 1) + "G"),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
        result.back().write().metadataReal("Format").setString("0.0000000");
        result.back().write().metadataReal("GroupUnits").setString("StabilityFactor");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Normalized sample spot intensity stability factor");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(3);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Eavesdropper")
              .hash("Hide")
              .setBool(true);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(4);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Box")
              .setString(QObject::tr("Normalized Intensity Stability"));
        result.back().write().metadataReal("Realtime").hash("GroupName").setString(groupName);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Green"));

        result.emplace_back(SequenceName({}, "raw_meta", "Ing" + std::to_string(i + 1) + "R"),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
        result.back().write().metadataReal("Format").setString("0.0000000");
        result.back().write().metadataReal("GroupUnits").setString("StabilityFactor");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Normalized sample spot intensity stability factor");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(3);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Eavesdropper")
              .hash("Hide")
              .setBool(true);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(4);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Box")
              .setString(QObject::tr("Normalized Intensity Stability"));
        result.back().write().metadataReal("Realtime").hash("GroupName").setString(groupName);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Red"));

        result.emplace_back(SequenceName({}, "raw_meta", "Irw" + std::to_string(i + 1) + "B"),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
        result.back().write().metadataReal("Format").setString("0.0000000");
        result.back().write().metadataReal("GroupUnits").setString("Transmittance");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Transmittance with respect to the last white filter");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(3);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Eavesdropper")
              .hash("Hide")
              .setBool(true);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(5);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Box")
              .setString(QObject::tr("White Filter Ratio"));
        result.back().write().metadataReal("Realtime").hash("GroupName").setString(groupName);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Blue"));

        result.emplace_back(SequenceName({}, "raw_meta", "Irw" + std::to_string(i + 1) + "G"),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
        result.back().write().metadataReal("Format").setString("0.0000000");
        result.back().write().metadataReal("GroupUnits").setString("Transmittance");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Transmittance with respect to the last white filter");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(3);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Eavesdropper")
              .hash("Hide")
              .setBool(true);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(5);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Box")
              .setString(QObject::tr("White Filter Ratio"));
        result.back().write().metadataReal("Realtime").hash("GroupName").setString(groupName);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Green"));

        result.emplace_back(SequenceName({}, "raw_meta", "Irw" + std::to_string(i + 1) + "R"),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
        result.back().write().metadataReal("Format").setString("0.0000000");
        result.back().write().metadataReal("GroupUnits").setString("Transmittance");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Transmittance with respect to the last white filter");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(3);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Eavesdropper")
              .hash("Hide")
              .setBool(true);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(5);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Box")
              .setString(QObject::tr("White Filter Ratio"));
        result.back().write().metadataReal("Realtime").hash("GroupName").setString(groupName);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Red"));

        result.emplace_back(SequenceName({}, "raw_meta", "Irz" + std::to_string(i + 1) + "B"),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[0]);
        result.back().write().metadataReal("Format").setString("0.0000000");
        result.back().write().metadataReal("GroupUnits").setString("Transmittance");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Transmittance with respect to the filter start");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(3);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Eavesdropper")
              .hash("Hide")
              .setBool(true);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(6);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Box")
              .setString(QObject::tr("Filter Start Ratio"));
        result.back().write().metadataReal("Realtime").hash("GroupName").setString(groupName);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Blue"));

        result.emplace_back(SequenceName({}, "raw_meta", "Irz" + std::to_string(i + 1) + "G"),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[1]);
        result.back().write().metadataReal("Format").setString("0.0000000");
        result.back().write().metadataReal("GroupUnits").setString("Transmittance");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Transmittance with respect to the filter start");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(3);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Eavesdropper")
              .hash("Hide")
              .setBool(true);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(6);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Box")
              .setString(QObject::tr("Filter Start Ratio"));
        result.back().write().metadataReal("Realtime").hash("GroupName").setString(groupName);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Green"));

        result.emplace_back(SequenceName({}, "raw_meta", "Irz" + std::to_string(i + 1) + "R"),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(config.first().wavelengths[2]);
        result.back().write().metadataReal("Format").setString("0.0000000");
        result.back().write().metadataReal("GroupUnits").setString("Transmittance");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Transmittance with respect to the filter start");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Realtime").hash("NetworkPriority").setInteger(3);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Eavesdropper")
              .hash("Hide")
              .setBool(true);
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(6);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Box")
              .setString(QObject::tr("Filter Start Ratio"));
        result.back().write().metadataReal("Realtime").hash("GroupName").setString(groupName);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupColumn")
              .setString(QObject::tr("Red"));
    }

    return result;
}

SequenceMatch::Composite AcquireGMDCLAP3W::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "Ff");
    sel.append({}, {}, "Fn");
    sel.append({}, {}, "ZFILTER");
    sel.append({}, {}, "ZWHITE");
    sel.append({}, {}, "ZSPOT");
    sel.append({}, {}, "ZSTATE");
    return sel;
}