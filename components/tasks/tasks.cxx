/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <limits.h>
#include <QStringList>
#include <QSet>
#include <QDateTime>
#include <QLoggingCategory>
#include <QBuffer>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "core/memory.hxx"
#include "datacore/variant/root.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"
#include "datacore/segment.hxx"
#include "datacore/valueoptionparse.hxx"
#include "io/process.hxx"

#include "tasks.hxx"


Q_LOGGING_CATEGORY(log_component_tasks, "cpd3.component.tasks", QtWarningMsg)

using namespace CPD3;
using namespace CPD3::Data;

Tasks::Tasks() : priority(0), stations(), terminated(false), runDetached(false)
{ }

Tasks::Tasks(const ComponentOptions &options,
             const std::vector<SequenceName::Component> &setStations) : priority(0),
                                                                        stations(setStations),
                                                                        terminated(false),
                                                                        runDetached(false)
{
    bool prioritySet = false;
    if (options.isSet("priority")) {
        priority = qobject_cast<ComponentOptionSingleInteger *>(options.get("priority"))->get();
        prioritySet = true;
    }

    if (options.isSet("daily") &&
            qobject_cast<ComponentOptionBoolean *>(options.get("daily"))->get()) {
        if (!prioritySet)
            priority = 3000;

        /* Explicitly NOT UTC because we want to run this relative to the local
         * time */
        QDateTime today(QDateTime::currentDateTime());
        if (today.date().dayOfWeek() == Qt::Saturday) {
            QDateTime lastWeek(today.addDays(-7));
            if (lastWeek.date().year() != today.date().year()) {
                priority += 3000;
            } else if (lastWeek.date().month() != today.date().month()) {
                priority += 2000;
            } else {
                priority += 1000;
            }
        }
    }
    if (options.isSet("detached") &&
            qobject_cast<ComponentOptionBoolean *>(options.get("detached"))->get()) {
        runDetached = true;
    }
}

Tasks::~Tasks()
{
    if (activeAction) {
        activeAction->signalTerminate();
        activeAction->wait();
    }
}

void Tasks::signalTerminate()
{
    {
        std::lock_guard<std::mutex> lock(mutex);
        terminated = true;
        if (activeAction)
            activeAction->signalTerminate();
    }
    terminateRequested();
}

class RunItem {
public:
    virtual ~RunItem() = default;

    virtual bool mergeFrom(const RunItem *)
    { return false; }

    virtual bool equals(const RunItem *) const
    { return false; }

    virtual bool lessThan(const RunItem *other) const
    { return priority < other->priority; }

    virtual bool prepareRun(const std::vector<SequenceName::Component> &, bool)
    { return true; }

    virtual void execute(Tasks *tasks) = 0;

    std::int_fast64_t priority;
protected:
    RunItem() : priority(0)
    { }

    explicit RunItem(std::int_fast64_t priority) : priority(priority)
    { }
};

class RunComponentBase : public RunItem {
protected:
    QObject *component;
    ComponentLoader::Information componentInfo;
    bool runIndividual;
    QString componentName;
    Variant::Read options;
    SequenceName::ComponentSet stations;

    RunComponentBase() : component(nullptr), runIndividual(false)
    { }

    RunComponentBase(const SequenceName::Component &station,
                     std::int_fast64_t priority,
                     const Variant::Read &task) : RunItem(priority), component(nullptr)
    {
        componentName = task["Name"].toQString();
        options = task["Options"];
        runIndividual = task["SplitStations"].toBoolean();
        stations.insert(station);
    }

    void executeInner(Tasks *tasks, const std::vector<SequenceName::Component> &rs)
    {
        std::unique_ptr<CPD3Action> action;

        if (ActionComponent *actionComponent = qobject_cast<ActionComponent *>(component)) {
            ComponentOptions o = actionComponent->getOptions();
            ValueOptionParse::parse(o, options);
            action.reset(actionComponent->createAction(o, rs));
        }
        if (!action) {
            if (ActionComponentTime
                    *actionComponentTime = qobject_cast<ActionComponentTime *>(component)) {
                ComponentOptions o = actionComponentTime->getOptions();
                ValueOptionParse::parse(o, options);
                action.reset(actionComponentTime->createTimeAction(o, rs));
            }
        }
        if (!action) {
            qCWarning(log_component_tasks) << "Unhandled action type for component"
                                           << componentName;
            return;
        }

        {
            std::unique_lock<std::mutex> lock(tasks->mutex);
            if (tasks->terminated) {
                lock.unlock();
                return;
            }
        }

        action->feedback.forward(tasks->feedback);

        double start = Time::time();
        if (rs.empty()) {
            qCDebug(log_component_tasks) << "Starting component" << componentName;
        } else {
            qCDebug(log_component_tasks) << "Starting component" << componentName << "for" << rs;
        }

        tasks->actionStations(rs);

        action->start();
        {
            auto local = action.get();
            {
                std::unique_lock<std::mutex> lock(tasks->mutex);
                if (tasks->terminated) {
                    lock.unlock();
                    action->signalTerminate();
                    action->wait();
                    return;
                }
                tasks->activeAction = std::move(action);
            }
            local->wait();
        }

        {
            std::lock_guard<std::mutex> lock(tasks->mutex);
            tasks->activeAction.reset();
            if (tasks->terminated) {
                return;
            }
        }

        double end = Time::time();
        if (rs.empty()) {
            qCInfo(log_component_tasks) << "Executed component" << componentName << "in"
                                        << Logging::elapsed(end - start);
        } else {
            qCInfo(log_component_tasks) << "Executed component" << componentName << "for" << rs
                                        << "in" << Logging::elapsed(end - start);
        }
    }

public:

    bool mergeFrom(const RunItem *other) override
    {
        if (other->priority != priority)
            return false;
        auto cmp = dynamic_cast<const RunComponentBase *>(other);
        if (!cmp)
            return false;
        if (cmp->componentName != componentName)
            return false;
        if (cmp->options != options)
            return false;
        Util::merge(cmp->stations, stations);
        return true;
    }

    bool equals(const RunItem *other) const override
    {
        if (other->priority != priority)
            return false;
        auto cmp = dynamic_cast<const RunComponentBase *>(other);
        if (!cmp)
            return false;
        if (cmp->componentName != componentName)
            return false;
        if (cmp->stations != stations)
            return false;
        return cmp->options == options;
    }

    bool lessThan(const RunItem *other) const override
    {
        if (priority != other->priority)
            return priority < other->priority;
        auto cmp = dynamic_cast<const RunComponentBase *>(other);
        if (!cmp)
            return false;
        if (componentName != cmp->componentName)
            return componentName < cmp->componentName;
        return stations.size() < cmp->stations.size();
    }

    bool prepareRun(const std::vector<SequenceName::Component> &allStations,
                    bool restrictedStations) override
    {
        if (componentName.isEmpty())
            return false;
        if (!restrictedStations &&
                stations == SequenceName::ComponentSet(allStations.begin(), allStations.end())) {
            stations.clear();
        }

        {
            auto loaded = ComponentLoader::information(componentName);
            if (!loaded.second) {
                qCWarning(log_component_tasks) << "Can't load component" << componentName << "for"
                                               << stations;
                return false;
            }
            component = loaded.second;
            componentInfo = loaded.first;
        }

        std::size_t allowStations = 0;
        std::size_t requireStations = 0;

        ActionComponent *actionComponent = nullptr;
        if ((actionComponent = qobject_cast<ActionComponent *>(component))) {
            allowStations = static_cast<std::size_t>(actionComponent->actionAllowStations());
            requireStations = static_cast<std::size_t>(actionComponent->actionRequireStations());
        }

        ActionComponentTime *actionComponentTime = nullptr;
        if (!actionComponent &&
                (actionComponentTime = qobject_cast<ActionComponentTime *>(component))) {
            if (actionComponentTime->actionRequiresTime()) {
                qCWarning(log_component_tasks) << "Refusing to run" << componentName
                                               << "since it requires a time.  Stations:"
                                               << stations;
                return false;
            }
            allowStations = static_cast<std::size_t>(actionComponentTime->actionAllowStations());
            requireStations =
                    static_cast<std::size_t>(actionComponentTime->actionRequireStations());
        }

        if (stations.empty() && requireStations > 0) {
            stations = SequenceName::ComponentSet(allStations.begin(), allStations.end());
        } else if (requireStations > 0 && stations.size() < requireStations) {
            stations.clear();
        } else if (allowStations == 0 || stations.size() > allowStations) {
            stations.clear();
        } else if (runIndividual &&
                stations.empty() &&
                requireStations <= 0 &&
                allowStations >= 1) {
            stations = SequenceName::ComponentSet(allStations.begin(), allStations.end());
        }

        if (requireStations == 1 && allowStations <= 1) {
            if (stations.empty()) {
                stations = SequenceName::ComponentSet(allStations.begin(), allStations.end());
                runIndividual = true;
                return true;
            } else if (stations.size() != 1) {
                runIndividual = true;
                return true;
            }
        }
        if ((requireStations > 0 && stations.size() < requireStations) ||
                stations.size() > allowStations) {
            qCWarning(log_component_tasks)
                << "Can't resolve station numbers requirement, on component" << componentName
                << stations << allStations << allowStations << requireStations;
            return false;
        }

        return true;
    }
};

class RunComponent : public RunComponentBase {
public:
    virtual ~RunComponent() = default;

    RunComponent() = default;

    RunComponent(const SequenceName::Component &station,
                 std::int_fast64_t priority,
                 const Variant::Read &task) : RunComponentBase(station, priority, task)
    { }

    void execute(Tasks *tasks) override
    {
        if (!component)
            return;

        std::vector<SequenceName::ComponentSet> runs;
        if (runIndividual) {
            for (const auto &station : stations) {
                runs.emplace_back(SequenceName::ComponentSet{station});
            }
        } else {
            runs.emplace_back(stations);
        }

        tasks->actionStarted(componentInfo.title());

        for (const auto &rs : runs) {
            std::vector<SequenceName::Component> rsList(rs.begin(), rs.end());
            std::sort(rsList.begin(), rsList.end());
            executeInner(tasks, rsList);
        }
    }
};

class RunDetatachedComponent : public RunComponentBase {
    static std::string detachedTaskProgram()
    {
        QDir programDir(QCoreApplication::applicationDirPath());
        if (!programDir.exists())
            return "cpd3_detached_task";
        {
            QFileInfo file(programDir.absoluteFilePath("cpd3_detached_task"));
            if (file.exists())
                return file.absoluteFilePath().toStdString();
        }
        {
            QFileInfo file(programDir.absoluteFilePath("cpd3_detached_task.exe"));
            if (file.exists())
                return file.absoluteFilePath().toStdString();
        }
        return "cpd3_detached_task";
    }
public:
    virtual ~RunDetatachedComponent() = default;

    RunDetatachedComponent() = default;

    RunDetatachedComponent(const SequenceName::Component &station,
                           std::int_fast64_t priority,
                           const Variant::Read &task) : RunComponentBase(station, priority, task)
    { }

    void execute(Tasks *tasks) override
    {
        if (!component)
            return;

        std::vector<SequenceName::ComponentSet> runs;
        if (runIndividual) {
            for (const auto &station : stations) {
                runs.emplace_back(SequenceName::ComponentSet{station});
            }
        } else {
            runs.emplace_back(stations);
        }

        tasks->actionStarted(componentInfo.title());

        for (const auto &rs : runs) {
            std::vector<SequenceName::Component> rsList(rs.begin(), rs.end());
            std::sort(rsList.begin(), rsList.end());

            double start = Time::time();

#ifdef Q_OS_UNIX
            IO::Process::Spawn spawn("cpd3_detached_task", {});
            spawn.forward();
            spawn.inputStream = IO::Process::Spawn::StreamMode::Capture;
#else
            std::string serializedArgument;
            {
                Variant::Root config;
                config["Options"].set(options);
                config["Name"].setString(componentName);
                for (const auto &s : rsList) {
                    config["Stations"].toArray().after_back().setString(s);
                }
                QByteArray data;
                {
                    QDataStream stream(&data, QIODevice::WriteOnly);
                    stream << config;
                }
                serializedArgument = data.toBase64().toStdString();
            }
            if (serializedArgument.size() >= 32767) {
                qCDebug(log_component_tasks)
                    << "Serialized request too large for deteched execution, using internal";
                executeInner(tasks, rsList);
                continue;
            }

            IO::Process::Spawn spawn("cpd3_detached_task", {std::move(serializedArgument)});
            spawn.forward();
#endif

            auto proc = spawn.create();
            if (!proc) {
                qCDebug(log_component_tasks)
                    << "Failed to create detached execution, using internal";
                executeInner(tasks, rsList);
                continue;
            }

            Threading::Receiver receiver;
            tasks->terminateRequested
                 .connect(receiver, std::bind(&IO::Process::Instance::terminate, proc.get()));
            {
                std::lock_guard<std::mutex> lock(tasks->mutex);
                if (tasks->terminated)
                    return;
            }

            if (!proc->start()) {
                qCDebug(log_component_tasks)
                    << "Failed to start detached execution, using internal";
                executeInner(tasks, rsList);
                continue;
            }

#ifdef Q_OS_UNIX
            {
                Variant::Root config;
                config["Options"].set(options);
                config["Name"].setString(componentName);
                for (const auto &s : rsList) {
                    config["Stations"].toArray().after_back().setString(s);
                }
                QByteArray data;
                {
                    QDataStream stream(&data, QIODevice::WriteOnly);
                    stream << config;
                }

                auto stream = proc->inputStream();
                stream->write(std::move(data));
                stream->close();
            }
#endif

            tasks->actionStations(rsList);

            proc->wait();
            receiver.disconnect();

            double end = Time::time();
            if (rs.empty()) {
                qCInfo(log_component_tasks) << "Executed detached component" << componentName
                                            << "in" << Logging::elapsed(end - start);
            } else {
                qCInfo(log_component_tasks) << "Executed detached component" << componentName
                                            << "for" << rs << "in" << Logging::elapsed(end - start);
            }
        }
    }
};

class RunCommand : public RunItem {
    enum class StationMode {
        NoStation, SingleStation, MultipleStations
    } stationMode;
    bool ignoreTerminate;
    QString program;
    QStringList arguments;
    SequenceName::ComponentSet stations;
public:
    RunCommand() : stationMode(StationMode::NoStation), ignoreTerminate(false)
    { }

    RunCommand(const SequenceName::Component &station,
               std::int_fast64_t priority,
               const Variant::Read &task) : RunItem(priority), stationMode(StationMode::NoStation)
    {
        program = task["Program"].toQString();
        program.replace("${STATION}", QString::fromStdString(station), Qt::CaseInsensitive);
        if (task["Arguments"].getType() == Variant::Type::String) {
            arguments =
                    task["Arguments"].toQString().split(QRegExp("\\s+"), QString::SkipEmptyParts);
        } else {
            for (auto add : task["Arguments"].toArray()) {
                arguments.append(add.toQString());
            }
        }
        if (task["MultipleStations"].toBool()) {
            stationMode = StationMode::MultipleStations;
        } else if (task["SingleStation"].toBool()) {
            stationMode = StationMode::SingleStation;
        } else {
            stationMode = StationMode::NoStation;
        }
        ignoreTerminate = task["IgnoreTerminate"].toBool();
        stations.insert(station);
    }

    virtual ~RunCommand() = default;

    bool mergeFrom(const RunItem *other) override
    {
        if (other->priority != priority)
            return false;
        auto cmp = dynamic_cast<const RunCommand *>(other);
        if (!cmp)
            return false;
        if (cmp->ignoreTerminate != ignoreTerminate)
            return false;
        if (cmp->program != program)
            return false;
        if (cmp->arguments != arguments)
            return false;
        Util::merge(cmp->stations, stations);
        return true;
    }

    bool equals(const RunItem *other) const override
    {
        if (other->priority != priority)
            return false;
        auto cmp = dynamic_cast<const RunCommand *>(other);
        if (!cmp)
            return false;
        if (cmp->ignoreTerminate != ignoreTerminate)
            return false;
        if (cmp->program != program)
            return false;
        if (cmp->arguments != arguments)
            return false;
        return cmp->stations == stations;
    }

    bool lessThan(const RunItem *other) const override
    {
        if (priority != other->priority)
            return priority < other->priority;
        auto cmp = dynamic_cast<const RunCommand *>(other);
        if (!cmp)
            return false;
        if (program != cmp->program)
            return program < cmp->program;
        return stations.size() < cmp->stations.size();
    }

    bool prepareRun(const std::vector<SequenceName::Component> &allStations,
                    bool restrictedStations) override
    {
        if (program.isEmpty())
            return false;
        if (stationMode == StationMode::NoStation)
            stations.clear();
        else if (!restrictedStations && stations.empty())
            stations = SequenceName::ComponentSet(allStations.begin(), allStations.end());
        return true;
    }

    void execute(Tasks *tasks) override
    {
        QList<QStringList> invoke;

        switch (stationMode) {
        case StationMode::NoStation:
            invoke.append(QStringList(program) << arguments);
            break;
        case StationMode::MultipleStations: {
            if (stations.empty())
                return;
            auto sortedStations = Util::to_qstringlist(stations);
            std::sort(sortedStations.begin(), sortedStations.end());
            QString replaceStations(sortedStations.join(","));
            for (int i = 0, max = arguments.size(); i < max; i++) {
                arguments[i].replace("${STATION}", replaceStations, Qt::CaseInsensitive);
                arguments[i].replace("${STATIONS}", replaceStations, Qt::CaseInsensitive);
            }
            invoke.append(QStringList(program) << arguments);
            break;
        }
        case StationMode::SingleStation: {
            if (stations.empty())
                return;
            auto sortedStations = Util::to_qstringlist(stations);
            std::sort(sortedStations.begin(), sortedStations.end());
            for (const auto &station : sortedStations) {
                QStringList addArguments(arguments);
                for (auto &arg : addArguments) {
                    arg.replace("${STATION}", station, Qt::CaseInsensitive);
                }
                invoke.append(QStringList(program) << addArguments);
            }
            break;
        }
        }

        if (invoke.isEmpty())
            return;

        tasks->programStarted(program);
        for (auto &i : invoke) {
            {
                std::lock_guard<std::mutex> lock(tasks->mutex);
                if (tasks->terminated)
                    return;
            }

            QString command(i.takeFirst());
            if (invoke.size() > 1)
                tasks->processArguments(i);

            double start = Time::time();

            auto proc = IO::Process::Spawn(command, i).forward().create();
            if (!proc)
                continue;

            IO::Process::Instance::ExitMonitor exitOk(*proc);
            if (!proc->start())
                continue;
            if (!proc->wait())
                continue;

            double end = Time::time();
            if (!exitOk.isNormal() || exitOk.getExitCode() != 0) {
                if (i.isEmpty()) {
                    qCInfo(log_component_tasks) << "External program" << command
                                                << "exited abnormally (" << exitOk.getExitCode()
                                                << ") after" << Logging::elapsed(end - start);
                } else {
                    qCInfo(log_component_tasks) << "External program" << command << '(' << i.size()
                                                << " arguments) exited abnormally ("
                                                << exitOk.getExitCode() << ") after"
                                                << Logging::elapsed(end - start);
                }
            } else {
                if (i.isEmpty()) {
                    qCInfo(log_component_tasks) << "External program" << command << "finished after"
                                                << Logging::elapsed(end - start);
                } else {
                    qCInfo(log_component_tasks) << "External program" << command << '(' << i.size()
                                                << " arguments) finished after"
                                                << Logging::elapsed(end - start);
                }
            }
        }
    }
};

void Tasks::run()
{
    if (this->priority > 0) {
        qCDebug(log_component_tasks) << "Tasks run for priority" << this->priority << "starting";
    }

    {
        std::lock_guard<std::mutex> lock(mutex);
        if (terminated) {
            qCDebug(log_component_tasks) << "Tasks priority" << this->priority
                                         << " run terminated on startup";
            return;
        }
    }

    double start = Time::time();

    std::vector<std::unique_ptr<RunItem>> toRun;
    bool restrictStations = !stations.empty();
    {
        SequenceSegment configSegment;
        {
            Archive::Access archive;
            Archive::Access::ReadLock lock(archive);

            auto allStations = archive.availableStations(stations);
            if (allStations.empty()) {
                qCDebug(log_component_tasks) << "No stations available";
                return;
            }
            stations.clear();
            std::copy(allStations.begin(), allStations.end(), Util::back_emplacer(stations));
            std::sort(stations.begin(), stations.end());

            double tnow = Time::time();
            auto confList = SequenceSegment::Stream::read(
                    {tnow - 1.0, tnow + 1.0, stations, {"configuration"}, {"tasks"}}, &archive);
            auto f = Range::findIntersecting(confList, tnow);
            if (f == confList.cend()) {
                qCDebug(log_component_tasks) << "No configuration for" << stations;
                return;
            }
            configSegment = *f;
        }

        std::unordered_map<Variant::PathElement, std::vector<std::unique_ptr<RunItem>>> mergeRun;
        for (const auto &station : stations) {
            SequenceName stationUnit(station, "configuration", "tasks");
            auto config = configSegment.takeValue(stationUnit).read().hash("Run");
            if (!config.exists())
                continue;
            auto children = config.toChildren();
            for (auto task = children.begin(), endTask = children.end(); task != endTask; ++task) {
                if (!task.value().exists())
                    continue;
                auto taskPriority = task.value().getPath("Priority").toInteger();
                if (!INTEGER::defined(taskPriority))
                    taskPriority = 0;
                if (taskPriority > this->priority)
                    continue;

                std::unique_ptr<RunItem> add;
                if (task.value().getPath("Component").exists()) {
                    if (runDetached) {
                        add.reset(new RunDetatachedComponent(station, taskPriority,
                                                             task.value().getPath("Component")));
                    } else {
                        add.reset(new RunComponent(station, taskPriority,
                                                   task.value().getPath("Component")));
                    }
                } else if (task.value().getPath("Command").exists()) {
                    add.reset(
                            new RunCommand(station, taskPriority, task.value().getPath("Command")));
                }
                if (!add) {
                    qCDebug(log_component_tasks) << "Invalid task for" << station << ':'
                                                 << task.value();
                    continue;
                }

                auto &target = mergeRun[task.key()];
                for (int i = 0, max = target.size(); i < max; i++) {
                    if (target[i]->mergeFrom(add.get())) {
                        add.reset();
                        break;
                    }
                }

                if (!add)
                    continue;
                target.emplace_back(std::move(add));
            }
        }
        for (auto &add : mergeRun) {
            Util::append(std::move(add.second), toRun);
        }
    }

    {
        std::unique_lock<std::mutex> lock(mutex);
        if (terminated) {
            lock.unlock();
            qCDebug(log_component_tasks) << "Tasks run priority" << this->priority << "terminated";
            return;
        }
    }

    for (auto it = toRun.begin(); it != toRun.end();) {
        if (!(*it)->prepareRun(stations, restrictStations)) {
            it = toRun.erase(it);
            continue;
        }
        ++it;
    }
    std::sort(toRun.begin(), toRun.end(),
              [](const std::unique_ptr<RunItem> &a, const std::unique_ptr<RunItem> &b) {
                  return a->lessThan(b.get());
              });
    if (toRun.size() > 1) {
        for (auto it = toRun.begin() + 1; it != toRun.end();) {
            if ((*it)->equals((it - 1)->get())) {
                it = toRun.erase(it);
                continue;
            }
            ++it;
        }
    }

    if (toRun.empty()) {
        if (this->priority > 0) {
            qCDebug(log_component_tasks) << "No tasks to run for priority" << this->priority;
        }
        return;
    }

    qCDebug(log_component_tasks) << "Starting" << toRun.size() << "task(s) at priority"
                                 << this->priority;

    for (auto &run : toRun) {
        {
            std::lock_guard<std::mutex> lock(mutex);
            if (terminated)
                return;
        }
        run->execute(this);

        run.reset();

        Memory::release_unused();
    }
    toRun.clear();

    double end = Time::time();
    qCDebug(log_component_tasks) << "Finished tasks after" << Logging::elapsed(end - start);

    QCoreApplication::processEvents();
}

void Tasks::actionStarted(const QString &name)
{
    feedback.emitStage(tr("Start - %1", "action format").arg(name),
                       tr("The system is starting an action."), false);
}

void Tasks::actionStations(const std::vector<SequenceName::Component> &stations)
{
    if (stations.size() < 4) {
        QStringList display;
        for (const auto &add : stations) {
            display.push_back(QString::fromStdString(add));
        }
        feedback.emitState(display.join(",").toUpper());
    } else if (!stations.empty()) {
        feedback.emitState(tr("Multiple stations", "large station list"));
    }
}

void Tasks::programStarted(const QString &program)
{
    feedback.emitStage(tr("Execute - %1", "external program format").arg(program),
                       tr("An external program is being run."), false);
}

void Tasks::processArguments(const QStringList &arguments)
{
    feedback.emitState(arguments.join(" "));
}


ComponentOptions TasksComponent::getOptions()
{
    ComponentOptions options;

    ComponentOptionSingleInteger *priority =
            new ComponentOptionSingleInteger(tr("priority", "name"), tr("Maximum priority"),
                                             tr("This is the maximum priority level of tasks to execute.  Priority "
                                                "zero tasks are run every time data is processed.  Priority 1000 is "
                                                "run \"near real time\" (generally every ten minutes).  Priority "
                                                "2000 is run \"frequently\" (generally hourly).  Priority 3000 is "
                                                "run less frequently (generally daily).  Priority 4000 is run on a "
                                                "more differed basis (generally weekly).  Priority 5000 is run on "
                                                "a response time irrelevant basis (generally monthly).  Priority "
                                                "6000 is run on an even lower time basis (generally yearly)."),
                                             tr("0"));
    priority->setMinimum(0);
    options.add("priority", priority);

    options.add("daily",
                new ComponentOptionBoolean(tr("daily", "name"), tr("Execute in daily mode"),
                                           tr("This changes the execution mode such that the priority is "
                                              "increased when the current day is the first one of various "
                                              "intervals.  That is, this executes at the specified priority when "
                                              "this is not the first day of something.  It will execute at the "
                                              "priority plus 1000 when this is the first day of the week "
                                              "(defined as Saturday), plus 2000 when it is the first weekend of "
                                              "the month, and plus 3000 when it is the fist weekend of the "
                                              "year.  When the priority is not set it defaults to 3000 when this "
                                              "option is used."), QString()));

    options.add("detached", new ComponentOptionBoolean(tr("detached", "name"),
                                                       tr("Execute tasks in detached mode"),
                                                       tr("This option causes all tasks to be executed in detached processes.  "
                                                          "This allows for better isolation in the event of failure or other problems."),
                                                       QString()));

    return options;
}

QList<ComponentExample> TasksComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Run all immediate tasks", "default example name"),
                                     tr("This will run all tasks that are flagged as immediate.  Normally "
                                        "this is not required as it is run automatically by the data "
                                        "archive modifier.")));

    options = getOptions();
    (qobject_cast<ComponentOptionSingleInteger *>(options.get("priority")))->set(4000);
    examples.append(ComponentExample(options, tr("Run all weekly tasks"),
                                     tr("This will run all tasks of weekly or higher frequency.")));

    return examples;
}

int TasksComponent::actionAllowStations()
{ return INT_MAX; }

CPD3Action *TasksComponent::createAction(const ComponentOptions &options,
                                         const std::vector<std::string> &stations)
{ return new Tasks(options, stations); }
