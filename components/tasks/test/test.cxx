/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QTemporaryFile>

#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "core/actioncomponent.hxx"
#include "datacore/stream.hxx"
#include "datacore/archive/access.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class TestComponent : public QObject {
Q_OBJECT

#ifdef Q_OS_UNIX
    /* Can't keep a QTemporaryFile, because that can result in ETXTBSY */
    QString reportScript;
#endif

    ActionComponent *component;
    QTemporaryFile databaseFile;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();
        ::qputenv("QT_LOGGING_RULES", "*=false");

        QVERIFY(databaseFile.open());

        QVERIFY(qputenv("CPD3ARCHIVE",
                        (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1()));
        QCOMPARE(qgetenv("CPD3ARCHIVE"),
                 (QString("sqlite:%1").arg(databaseFile.fileName())).toLatin1());

        component = qobject_cast<ActionComponent *>(ComponentLoader::create("tasks"));
        QVERIFY(component);

#ifdef Q_OS_UNIX
        {
            QTemporaryFile file;
            file.setAutoRemove(false);
            QVERIFY(file.open());
            reportScript = file.fileName();
            file.write(QByteArray("#!/bin/sh\n"
                                  "FILE=\"$1\"\n"
                                  "shift\n"
                                  "echo \"$@\" >> \"$FILE\"\n"));
            file.setPermissions(file.permissions() | QFile::ExeUser);
            file.flush();
        }
#endif
    }

    void init()
    {
        databaseFile.resize(0);
    }

    void cleanupTestCase()
    {
#ifdef Q_OS_UNIX
        if (!reportScript.isEmpty()) {
            QFile::remove(reportScript);
        }
#endif
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleInteger *>(options.get("priority")));
    }

    void componentRun()
    {
        Variant::Root config;
        config["/Run/Update/Component/Name"].setString("update_passed");
        config["/Run/Update/Priority"].setInteger(500);

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "configuration", "tasks"}, config, FP::undefined(),
                              FP::undefined()),
                SequenceValue({"sfa", "passed", "aerosol"}, Variant::Root(), 3600.0, 7200.0)});

        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionSingleInteger *>(options.get("priority"))->set(1000);
        std::unique_ptr<CPD3Action> action(component->createAction(options));
        QVERIFY(action.get() != nullptr);
        action->start();
        QVERIFY(action->wait());
        action.reset();

        auto events = Archive::Access(databaseFile).readSynchronous(
                Archive::Selection(FP::undefined(), FP::undefined(), {"sfa"}, {"events"},
                                   {"editing"}));
        QCOMPARE((int) events.size(), 1);
        QCOMPARE(events.front().read()["Information/FirstStart"].toDouble(), 3600.0);
        QCOMPARE(events.front().read()["Information/LastEnd"].toDouble(), 7200.0);
    }

#ifdef Q_OS_UNIX

    void componentDetached()
    {
        Variant::Root config;
        config["/Run/Update/Component/Name"].setString("update_passed");
        config["/Run/Update/Priority"].setInteger(500);

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue({"sfa", "configuration", "tasks"}, config, FP::undefined(),
                              FP::undefined()),
                SequenceValue({"sfa", "passed", "aerosol"}, Variant::Root(), 3600.0, 7200.0)});

        ComponentOptions options(component->getOptions());
        qobject_cast<ComponentOptionSingleInteger *>(options.get("priority"))->set(1000);
        qobject_cast<ComponentOptionBoolean *>(options.get("detached"))->set(true);
        std::unique_ptr<CPD3Action> action(component->createAction(options));
        QVERIFY(action.get() != nullptr);
        action->start();
        QVERIFY(action->wait());
        action.reset();

        auto events = Archive::Access(databaseFile).readSynchronous(
                Archive::Selection(FP::undefined(), FP::undefined(), {"sfa"}, {"events"},
                                   {"editing"}));
        QCOMPARE((int) events.size(), 1);
        QCOMPARE(events.front().read()["Information/FirstStart"].toDouble(), 3600.0);
        QCOMPARE(events.front().read()["Information/LastEnd"].toDouble(), 7200.0);
    }

    void commandInvoke()
    {
        QTemporaryFile reportNoStation;
        QVERIFY(reportNoStation.open());
        QTemporaryFile reportAllStations;
        QVERIFY(reportAllStations.open());
        QTemporaryFile reportSingleStation;
        QVERIFY(reportSingleStation.open());

        Variant::Root config;
        config["/Run/Zero/Command/Program"].setString(reportScript);
        config["/Run/Zero/Command/Arguments/#0"].setString(reportNoStation.fileName());
        config["/Run/Zero/Command/Arguments/#1"].setString("WASRUN");

        config["/Run/One/Priority"].setInt64(1);
        config["/Run/One/Command/Program"].setString(reportScript);
        config["/Run/One/Command/MultipleStations"].setBool(true);
        config["/Run/One/Command/Arguments/#0"].setString(reportAllStations.fileName());
        config["/Run/One/Command/Arguments/#1"].setString("${STATIONS}");

        config["/Run/Two/Priority"].setInt64(2);
        config["/Run/Two/Command/Program"].setString(reportScript);
        config["/Run/Two/Command/SingleStation"].setBool(true);
        config["/Run/Two/Command/Arguments/#0"].setString(reportSingleStation.fileName());
        config["/Run/Two/Command/Arguments/#1"].setString("${STATION}");

        Archive::Access(databaseFile).writeSynchronous(SequenceValue::Transfer{
                SequenceValue(SequenceName("brw", "configuration", "tasks"), config,
                              FP::undefined(), FP::undefined()),
                SequenceValue(SequenceName("alt", "configuration", "tasks"), config,
                              FP::undefined(), FP::undefined()),
                SequenceValue(SequenceName("sgp", "configuration", "tasks"), Variant::Root(),
                              FP::undefined(), FP::undefined())});

        ComponentOptions options(component->getOptions());
        CPD3Action *action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;
        QCOMPARE(reportNoStation.readAll(), QByteArray("WASRUN\n"));
        QCOMPARE(reportAllStations.size(), Q_INT64_C(0));
        QCOMPARE(reportSingleStation.size(), Q_INT64_C(0));
        reportNoStation.resize(0);
        reportNoStation.seek(0);
        reportAllStations.resize(0);
        reportAllStations.seek(0);
        reportSingleStation.resize(0);
        reportSingleStation.seek(0);

        options = component->getOptions();
        qobject_cast<ComponentOptionSingleInteger *>(options.get("priority"))->set(1);
        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;
        QCOMPARE(reportNoStation.readAll(), QByteArray("WASRUN\n"));
        QCOMPARE(reportAllStations.readAll(), QByteArray("alt,brw\n"));
        QCOMPARE(reportSingleStation.size(), Q_INT64_C(0));
        reportNoStation.resize(0);
        reportNoStation.seek(0);
        reportAllStations.resize(0);
        reportAllStations.seek(0);
        reportSingleStation.resize(0);
        reportSingleStation.seek(0);

        options = component->getOptions();
        qobject_cast<ComponentOptionSingleInteger *>(options.get("priority"))->set(2);
        action = component->createAction(options);
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;
        QCOMPARE(reportNoStation.readAll(), QByteArray("WASRUN\n"));
        QCOMPARE(reportAllStations.readAll(), QByteArray("alt,brw\n"));
        QCOMPARE(reportSingleStation.readAll(), QByteArray("alt\nbrw\n"));
        reportNoStation.resize(0);
        reportNoStation.seek(0);
        reportAllStations.resize(0);
        reportAllStations.seek(0);
        reportSingleStation.resize(0);
        reportSingleStation.seek(0);

        options = component->getOptions();
        qobject_cast<ComponentOptionSingleInteger *>(options.get("priority"))->set(2);
        action = component->createAction(options, {"brw"});
        QVERIFY(action != NULL);
        action->start();
        QVERIFY(action->wait());
        delete action;
        QCOMPARE(reportNoStation.readAll(), QByteArray("WASRUN\n"));
        QCOMPARE(reportAllStations.readAll(), QByteArray("brw\n"));
        QCOMPARE(reportSingleStation.readAll(), QByteArray("brw\n"));
        reportNoStation.resize(0);
        reportNoStation.seek(0);
        reportAllStations.resize(0);
        reportAllStations.seek(0);
        reportSingleStation.resize(0);
        reportSingleStation.seek(0);
    }

#endif
};

QTEST_MAIN(TestComponent)

#include "test.moc"
