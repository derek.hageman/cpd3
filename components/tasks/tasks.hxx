/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef TASKS_H
#define TASKS_H

#include "core/first.hxx"

#include <mutex>
#include <memory>
#include <QtGlobal>
#include <QObject>
#include <QList>
#include <QStringList>

#include "core/component.hxx"
#include "core/actioncomponent.hxx"
#include "core/threading.hxx"
#include "datacore/stream.hxx"

class RunComponentBase;

class RunComponent;

class RunDetatachedComponent;

class RunCommand;

class Tasks : public CPD3::CPD3Action {
Q_OBJECT

    int priority;
    std::vector<CPD3::Data::SequenceName::Component> stations;

    bool terminated;
    bool runDetached;

    std::unique_ptr<CPD3::CPD3Action> activeAction;
    std::mutex mutex;

    CPD3::Threading::Signal<> terminateRequested;

    friend class RunComponentBase;

    friend class RunComponent;

    friend class RunDetatachedComponent;

    friend class RunCommand;

public:
    Tasks();

    Tasks(const CPD3::ComponentOptions &options,
          const std::vector<CPD3::Data::SequenceName::Component> &setStations = {});

    virtual ~Tasks();

    void signalTerminate() override;

protected:
    void run() override;

private:
    void actionStarted(const QString &name);

    void actionStations(const std::vector<CPD3::Data::SequenceName::Component> &stations);

    void programStarted(const QString &program);

    void processArguments(const QStringList &arguments);
};

class TasksComponent : public QObject, virtual public CPD3::ActionComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::ActionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.tasks"
                              FILE
                              "tasks.json")

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    int actionAllowStations() override;

    CPD3::CPD3Action *createAction
            (const CPD3::ComponentOptions &options = {},
             const std::vector<std::string> &stations = {}) override;
};

#endif
