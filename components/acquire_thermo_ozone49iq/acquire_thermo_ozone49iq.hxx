/*
 * Copyright (c) 2020 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIRETHERMOOZONE49IQ_HXX
#define ACQUIRETHERMOOZONE49IQ_HXX

#include "core/first.hxx"

#include <memory>
#include <deque>
#include <map>
#include <array>
#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"
#include "algorithms/crc.hxx"

class AcquireThermoOzone49iQ : public CPD3::Acquisition::FramedInstrument {
    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;
    int autoprobeValidRecords;

    enum class ResponseState : int {
        /* Waiting a data record in passive mode */
        Passive_Run,

        /* Waiting for enough data to confirm communications */
        Passive_Wait,

        /* Passive initialization (same as passive wait, but discard the
         * first timeout). */
        Passive_Initialize,

        /* Same as passive wait but currently autoprobing */
        Autoprobe_Passive_Wait,

        /* Same as passive initialize but autoprobing */
        Autoprobe_Passive_Initialize,

        /* Stream flush */
        Interactive_Start_Flush,

        /* Reading the serial number */
        Interactive_Start_Read_SerialNumber,

        /* Reading the firmware version */
        Interactive_Start_Read_FirmwareVersion,

        /* Reading the host name */
        Interactive_Start_Read_HostName,

        /* Reading the gas units */
        Interactive_Start_Read_GasUnits,

        /* Setting the gas units to ppb */
        Interactive_Start_Set_PPB,

        /* Sending SetClockSource = 3 */
        Interactive_Start_Set_Clock_Start, /* Sending SetClockData */
        Interactive_Start_Set_Clock_Data, /* Sending clock commit */
        Interactive_Start_Commit_Clock, /* Sending SetClockSource = 0 */
        Interactive_Start_Set_Clock_End,

        /* Reading initial concentration */
        Interactive_Start_Read_Concentration,

        /* Waiting before attempting a communications restart */
        Interactive_Restart_Wait,

        /* Initial state for interactive start */
        Interactive_Initialize,

        /* In interactive mode, waiting for a command response */
        Interactive_Run_Read, /* In interactive mode, waiting before polling again */
        Interactive_Run_Wait,
    } responseState;

    enum class RegisterType : std::uint16_t {
        INVALID = 0,

        SerialNumber = 515, /* 7 registers, string */
        FirmwareVersion = 523, /* 16 registers, string */
        HostName = 539, /* 8 registers, string */

        GasUnits = 7555, /* 3 registers, string */

        SetClockData = 5185, /* 6 registers, hour, minute, second, month, day, year */
        SetClockSource = 5207, /* 1 register, set to 3 to begin, 0 to end */
        SetClockCommit = 5235, /* 1 register, set to 1 after data */

        AlarmStatus = 1457, /* 13 registers, U16 */
        Concentration = 2100, /* 2 registers, float */

        FlowA = 1413, /* 2 registers, float */
        PhotometerPressureA = 1441, /* 2 registers, float */
        PumpPressure = 1444, /* 2 registers, float */
        BenchTemperature = 1470, /* 2 registers, float */
        LampTemperature = 1472, /* 2 registers, float */

        LampCurrent = 1474, /* 2 registers, float */
        LampHeaterCurrent = 1451, /* 2 registers, float */

        CountsA = 1453, /* 2 registers, float */
        CountsANoise = 2108, /* 2 registers, float */
        CountsB = 1455, /* 2 registers, float */
        CountsBNoise = 2110, /* 2 registers, float */

        //PhotometerPressureB = 1447, /* 2 registers, float -- returns garbage */
        //FlowB = 2134, /* 2 registers, float -- returns garbage */
    };

    struct Report {
        CPD3::Data::Variant::Flags alarms;
        double X;
        double Q;
        double P1;
        double P2;
        double T1;
        double T2;

        double VA1;
        double VA2;

        double C1;
        double C1g;
        double C2;
        double C2g;

        Report();

        bool operator==(const Report &other) const;
    };

    Report lastOutputData;
    Report currentData;
    double lastOutputEnd;

    class Command {
        CPD3::Util::ByteArray packet;
    public:
        Command();

        explicit Command(CPD3::Util::ByteArray data);

        Command(const Command &);

        Command &operator=(const Command &);

        Command(Command &&);

        Command &operator=(Command &&);

        void stateDescription(CPD3::Data::Variant::Write state) const;

        bool isRead() const;

        RegisterType reg() const;

        bool checkResponse(const CPD3::Util::ByteView &response) const;

        std::vector<std::uint16_t> toData(const CPD3::Util::ByteView &response) const;

        CPD3::Util::ByteView toBytes(const CPD3::Util::ByteView &response) const;

        std::string toString(const CPD3::Util::ByteView &response) const;

        float toFloat(const CPD3::Util::ByteView &response) const;
    };

    std::deque<Command> queuedCommands;

    void sendRequest(CPD3::Util::ByteArray &&request);

    void sendReadCommand(std::uint16_t reg, std::uint8_t count = 1);

    void sendReadCommand(RegisterType reg);

    void sendWriteCommand(std::uint16_t reg, const std::vector<std::uint16_t> &data);

    void sendWriteCommand(RegisterType reg, const std::vector<std::uint16_t> &data);

    void sendWriteCommand(RegisterType reg, std::uint16_t value);

    void sendWriteCommand(RegisterType reg, const std::string &value);

    class Configuration {
        double start;
        double end;
    public:
        double pollInterval;
        std::uint8_t address;
        bool modbusTCP;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under,
                      const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    std::deque<Configuration> config;
    CPD3::Algorithms::CRC16 crc;

    void setDefaultInvalid();

    CPD3::Data::Variant::Root instrumentMeta;

    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;
    bool realtimeStateUpdated;
    CPD3::Data::Variant::Flags realtimeStateAlarms;

    void logValue(double startTime,
                  double endTime,
                  CPD3::Data::SequenceName::Component name,
                  CPD3::Data::Variant::Root &&value);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root &&value);

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    void invalidateLogValues(double frameTime);

    void configurationAdvance(double frameTime);

    CPD3::Data::Variant::Write invalidResponseBase(const CPD3::Util::ByteArray &frame,
                                                   double time,
                                                   int code);

    void invalidResponse(const CPD3::Util::ByteArray &frame, double time, int code);

    void invalidResponse(const CPD3::Util::ByteArray &frame,
                         double time,
                         const Command &command,
                         int code);

    void updateRunState(double frameTime);

    void emitDataRecord(double frameTime);

public:
    AcquireThermoOzone49iQ(const CPD3::Data::ValueSegment::Transfer &config,
                           const std::string &loggingContext);

    AcquireThermoOzone49iQ(const CPD3::ComponentOptions &options,
                           const std::string &loggingContext);

    virtual ~AcquireThermoOzone49iQ();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus() override;

    CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables() override;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus() override;

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingInstrumentTimeout(double time) override;

    void discardDataCompleted(double time) override;

    std::size_t dataFrameStart(std::size_t offset,
                               const CPD3::Util::ByteArray &input) const override;

    std::size_t dataFrameEnd(std::size_t start, const CPD3::Util::ByteArray &input) const override;

    std::size_t controlFrameStart(std::size_t offset,
                                  const CPD3::Util::ByteArray &input) const override;

    std::size_t controlFrameEnd(std::size_t start,
                                const CPD3::Util::ByteArray &input) const override;

    void limitDataBuffer(CPD3::Util::ByteArray &buffer) override;
};

class AcquireThermoOzone49iQComponent
        : public QObject, virtual public CPD3::Acquisition::AcquisitionComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_thermo_ozone49iq"
                              FILE
                              "acquire_thermo_ozone49iq.json")

    CPD3::ComponentOptions getBaseOptions();

public:

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
