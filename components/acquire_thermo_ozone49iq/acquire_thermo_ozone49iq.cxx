/*
 * Copyright (c) 2020 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"


#include <cstring>
#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "core/csv.hxx"
#include "core/timeutils.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/composite.hxx"
#include "acquisition/acquisitioncomponent.hxx"

#include "acquire_thermo_ozone49iq.hxx"


using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;


static const std::array<std::string, 13> instrumentAlarmTranslation
        {"IntensityAHighAlarm", "IntensityBHighAlarm", "LampTemperatureShortAlarm",
         "LampTemperatureOpenAlarm", "SampleTemperatureShortAlarm", "SampleTemperatureOpenAlarm",
         "LampConnectionAlarm", "LampShortAlarm", "CommunicationsAlarm", "PowerSupplyAlarm",
         "LampCurrentAlarm", "LampTemperatureAlarm", "SampleTemperatureAlarm",};


AcquireThermoOzone49iQ::Configuration::Configuration() : start(FP::undefined()),
                                                         end(FP::undefined()),
                                                         pollInterval(0.5),
                                                         address(1),
                                                         modbusTCP(false)
{ }

AcquireThermoOzone49iQ::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          pollInterval(other.pollInterval),
          address(other.address),
          modbusTCP(other.modbusTCP)
{ }

void AcquireThermoOzone49iQ::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("Thermo");
    instrumentMeta["Model"].setString("49iQ");

    autoprobeValidRecords = 0;
    realtimeStateAlarms.clear();

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    realtimeStateUpdated = false;
}

AcquireThermoOzone49iQ::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s), end(e), pollInterval(0.5), address(1), modbusTCP(false)
{
    setFromSegment(other);
}

AcquireThermoOzone49iQ::Configuration::Configuration(const Configuration &under,
                                                     const ValueSegment &over,
                                                     double s,
                                                     double e) : start(s),
                                                                 end(e),
                                                                 pollInterval(under.pollInterval),
                                                                 address(under.address),
                                                                 modbusTCP(under.modbusTCP)
{
    setFromSegment(over);
}

void AcquireThermoOzone49iQ::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["PollInterval"].exists())
        pollInterval = config["PollInterval"].toDouble();

    if (config["Interface"].exists()) {
        if (Util::equal_insensitive(config["Interface/Type"].toString(), "RemoteServer")) {
            modbusTCP = true;
        } else {
            modbusTCP = false;
        }
    }
    if (config["TCPFraming"].exists())
        modbusTCP = config["TCPFraming"].toBool();

    if (config["Address"].exists()) {
        auto check = config["Address"].toInteger();
        if (INTEGER::defined(check) && check > 0 && check <= 0xFF)
            address = static_cast<std::uint8_t>(check);
    }
}

AcquireThermoOzone49iQ::AcquireThermoOzone49iQ(const ValueSegment::Transfer &configData,
                                               const std::string &loggingContext)
        : FramedInstrument("ozone49iq", loggingContext),
          autoprobeStatus(AcquisitionInterface::AutoprobeStatus::InProgress),
          autoprobeValidRecords(0),
          responseState(ResponseState::Passive_Initialize)
{
    setDefaultInvalid();
    config.emplace_back();

    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
}

void AcquireThermoOzone49iQ::setToAutoprobe()
{
    responseState = ResponseState::Autoprobe_Passive_Initialize;
}

void AcquireThermoOzone49iQ::setToInteractive()
{ responseState = ResponseState::Interactive_Initialize; }


ComponentOptions AcquireThermoOzone49iQComponent::getBaseOptions()
{
    ComponentOptions options;

    return options;
}

AcquireThermoOzone49iQ::AcquireThermoOzone49iQ(const ComponentOptions &,
                                               const std::string &loggingContext)
        : FramedInstrument("ozone49iq", loggingContext),
          autoprobeStatus(AcquisitionInterface::AutoprobeStatus::InProgress),
          autoprobeValidRecords(0),
          responseState(ResponseState::Passive_Initialize)
{
    setDefaultInvalid();
    config.emplace_back();
}

AcquireThermoOzone49iQ::~AcquireThermoOzone49iQ() = default;

void AcquireThermoOzone49iQ::invalidateLogValues(double frameTime)
{
    autoprobeValidRecords = 0;
    lastOutputData = Report();
    currentData = Report();
    lastOutputEnd = FP::undefined();
    loggingLost(frameTime);
}

SequenceValue::Transfer AcquireThermoOzone49iQ::buildLogMeta(double time) const
{
    Q_ASSERT(!config.empty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_thermo_ozone49iq");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    if (instrumentMeta["FirmwareVersion"].exists())
        processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    if (instrumentMeta["SoftwareVersion"].exists())
        processing["SoftwareVersion"].set(instrumentMeta["SoftwareVersion"]);

    result.emplace_back(SequenceName({}, "raw_meta", "X"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000000.00");
    result.back().write().metadataReal("Units").setString("ppb");
    result.back().write().metadataReal("Description").setString("Ozone concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Ozone"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "T1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Sample temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);

    result.emplace_back(SequenceName({}, "raw_meta", "T2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Lamp temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Lamp"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);

    result.emplace_back(SequenceName({}, "raw_meta", "P1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Sample pressure");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);

    result.emplace_back(SequenceName({}, "raw_meta", "P2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Pump pressure");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Pump"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);

    result.emplace_back(SequenceName({}, "raw_meta", "Q"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Sample flow");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Flow"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);

    result.emplace_back(SequenceName({}, "raw_meta", "C1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Cell A lamp intensity");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Cell A"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);

    result.emplace_back(SequenceName({}, "raw_meta", "C2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Cell B lamp intensity");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Cell B"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);

    result.emplace_back(SequenceName({}, "raw_meta", "VA1"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("mA");
    result.back().write().metadataReal("Description").setString("Lamp current");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Lamp"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(6);

    result.emplace_back(SequenceName({}, "raw_meta", "VA2"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("A");
    result.back().write().metadataReal("Description").setString("Lamp heater current");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Heater"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(6);

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataSingleFlag("IntensityAHighAlarm")
          .hash("Description")
          .setString("Cell A frequency max alarm active");
    result.back()
          .write()
          .metadataSingleFlag("IntensityAHighAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49iq");

    result.back()
          .write()
          .metadataSingleFlag("IntensityBHighAlarm")
          .hash("Description")
          .setString("Cell B frequency max alarm active");
    result.back()
          .write()
          .metadataSingleFlag("IntensityBHighAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49iq");

    result.back()
          .write()
          .metadataSingleFlag("LampTemperatureShortAlarm")
          .hash("Description")
          .setString("Lamp temperature sensor short alarm");
    result.back()
          .write()
          .metadataSingleFlag("LampTemperatureShortAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49iq");

    result.back()
          .write()
          .metadataSingleFlag("LampTemperatureOpenAlarm")
          .hash("Description")
          .setString("Lamp temperature sensor open alarm");
    result.back()
          .write()
          .metadataSingleFlag("LampTemperatureOpenAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49iq");

    result.back()
          .write()
          .metadataSingleFlag("SampleTemperatureShortAlarm")
          .hash("Description")
          .setString("Bench temperature sensor short alarm");
    result.back()
          .write()
          .metadataSingleFlag("SampleTemperatureShortAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49iq");

    result.back()
          .write()
          .metadataSingleFlag("SampleTemperatureOpenAlarm")
          .hash("Description")
          .setString("Bench temperature sensor open alarm");
    result.back()
          .write()
          .metadataSingleFlag("SampleTemperatureOpenAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49iq");

    result.back()
          .write()
          .metadataSingleFlag("LampConnectionAlarm")
          .hash("Description")
          .setString("Lamp connection alarm");
    result.back()
          .write()
          .metadataSingleFlag("LampConnectionAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49iq");

    result.back()
          .write()
          .metadataSingleFlag("LampShortAlarm")
          .hash("Description")
          .setString("Lamp short alarm");
    result.back()
          .write()
          .metadataSingleFlag("LampShortAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49iq");

    result.back()
          .write()
          .metadataSingleFlag("CommunicationsAlarm")
          .hash("Description")
          .setString("Communications alarm");
    result.back()
          .write()
          .metadataSingleFlag("CommunicationsAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49iq");

    result.back()
          .write()
          .metadataSingleFlag("PowerSupplyAlarm")
          .hash("Description")
          .setString("Power supply alarm");
    result.back()
          .write()
          .metadataSingleFlag("PowerSupplyAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49iq");

    result.back()
          .write()
          .metadataSingleFlag("LampCurrentAlarm")
          .hash("Description")
          .setString("Lamp current alarm");
    result.back()
          .write()
          .metadataSingleFlag("LampCurrentAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49iq");

    result.back()
          .write()
          .metadataSingleFlag("LampTemperatureAlarm")
          .hash("Description")
          .setString("Lamp temperature alarm");
    result.back()
          .write()
          .metadataSingleFlag("LampTemperatureAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49iq");

    result.back()
          .write()
          .metadataSingleFlag("SampleTemperatureAlarm")
          .hash("Description")
          .setString("Bench temperature alarm");
    result.back()
          .write()
          .metadataSingleFlag("SampleTemperatureAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_thermo_ozone49iq");

    return result;
}

SequenceValue::Transfer AcquireThermoOzone49iQ::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_thermo_ozone49iq");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    if (instrumentMeta["FirmwareVersion"].exists())
        processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    if (instrumentMeta["SoftwareVersion"].exists())
        processing["SoftwareVersion"].set(instrumentMeta["SoftwareVersion"]);


    result.emplace_back(SequenceName({}, "raw_meta", "C1g"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Cell A lamp intensity noise");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Cell A noise"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);

    result.emplace_back(SequenceName({}, "raw_meta", "C2g"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Cell B lamp intensity noise");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Cell B noise"));
    result.back().write().metadataReal("Realtime").hash("Page").setInt64(0);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);


    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Realtime").hash("Name").setString(std::string());
    result.back().write().metadataString("Realtime").hash("Units").setString(std::string());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back().write().metadataString("Realtime").hash("RowOrder").setInteger(7);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Run")
          .setString(QObject::tr("", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidResponse")
          .setString(QObject::tr("NO COMMS: Invalid response"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Timeout")
          .setString(QObject::tr("NO COMMS: Timeout"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractive")
          .setString(QObject::tr("STARTING COMMS"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadSerialNumber")
          .setString(QObject::tr("STARTING COMMS: Reading serial number"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadFirmwareVersion")
          .setString(QObject::tr("STARTING COMMS: Reading firmware version"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadHostName")
          .setString(QObject::tr("STARTING COMMS: Reading host name"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadGasUnits")
          .setString(QObject::tr("STARTING COMMS: Reading gas units"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetPPB")
          .setString(QObject::tr("STARTING COMMS: Setting gas units to ppb"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetClockStart")
          .setString(QObject::tr("STARTING COMMS: Initiating clock set sequence"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetClockData")
          .setString(QObject::tr("STARTING COMMS: Sending clock data"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetClockCommit")
          .setString(QObject::tr("STARTING COMMS: Applying clock time"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetClockEnd")
          .setString(QObject::tr("STARTING COMMS: Finalizing clock set"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadConcentration")
          .setString(QObject::tr("STARTING COMMS: Reading ozone concentration"));

    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("AlarmActive")
          .setString(QObject::tr("MULTIPLE ALARMS PRESENT"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("IntensityAHighAlarm")
          .setString(QObject::tr("ALARM: Cell A frequency max"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("IntensityBHighAlarm")
          .setString(QObject::tr("ALARM: Cell B frequency max"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("LampTemperatureShortAlarm")
          .setString(QObject::tr("ALARM: Lamp temperature sensor shorted"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("LampTemperatureOpenAlarm")
          .setString(QObject::tr("ALARM: Lamp temperature sensor open"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SampleTemperatureShortAlarm")
          .setString(QObject::tr("ALARM: Bench temperature sensor shorted"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SampleTemperatureOpenAlarm")
          .setString(QObject::tr("ALARM: Bench temperature sensor open"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("LampConnectionAlarm")
          .setString(QObject::tr("ALARM: Lamp connection fault"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("LampShortAlarm")
          .setString(QObject::tr("ALARM: Lamp shorted"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("CommunicationsAlarm")
          .setString(QObject::tr("ALARM: Internal communications fault"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("PowerSupplyAlarm")
          .setString(QObject::tr("ALARM: Power supply fault"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("LampCurrentAlarm")
          .setString(QObject::tr("ALARM: Lamp current out of range"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("LampTemperatureAlarm")
          .setString(QObject::tr("ALARM: Lamp temperature out of range"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SampleTemperatureAlarm")
          .setString(QObject::tr("ALARM: Bench temperature out of range"));

    return result;
}

SequenceMatch::Composite AcquireThermoOzone49iQ::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "ZSTATE");
    return sel;
}

void AcquireThermoOzone49iQ::logValue(double startTime,
                                      double endTime,
                                      SequenceName::Component name,
                                      Variant::Root &&value)
{
    if (!loggingEgress && !realtimeEgress)
        return;
    SequenceValue dv({{}, "raw", std::move(name)}, std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        if (FP::defined(startTime))
            loggingEgress->incomingData(std::move(dv));
        return;
    }
    if (loggingEgress && FP::defined(startTime))
        loggingEgress->incomingData(dv);
    if (FP::defined(endTime)) {
        dv.setStart(endTime);
        dv.setEnd(endTime + 1.0);
    } else if (FP::defined(startTime)) {
        dv.setEnd(startTime + 1.0);
    }
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireThermoOzone49iQ::realtimeValue(double time,
                                           SequenceName::Component name,
                                           Variant::Root &&value)
{
    if (!realtimeEgress)
        return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

void AcquireThermoOzone49iQ::emitDataRecord(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    if (!FP::defined(lastOutputEnd)) {
        lastOutputEnd = frameTime;
        return;
    }

    if (lastOutputData == currentData)
        return;
    lastOutputData = currentData;

    double start = lastOutputEnd;
    double end = frameTime;
    lastOutputEnd = end;
    if (start >= end)
        return;

    Variant::Root F1(currentData.alarms);
    remap("F1", F1);

    Variant::Root X(currentData.X);
    remap("X", X);

    Variant::Root Q(currentData.Q);
    remap("Q", Q);

    Variant::Root P1(currentData.P1);
    remap("P1", P1);

    Variant::Root P2(currentData.P2);
    remap("P2", P2);

    Variant::Root T1(currentData.T1);
    remap("T1", T1);

    Variant::Root T2(currentData.T2);
    remap("T2", T2);

    Variant::Root VA1(currentData.VA1);
    remap("VA1", VA1);

    Variant::Root VA2(currentData.VA2);
    remap("VA2", VA2);

    Variant::Root C1(currentData.C1);
    remap("C1", C1);

    Variant::Root C1g(currentData.C1g);
    remap("C1g", C1g);

    Variant::Root C2(currentData.C2);
    remap("C2", C2);

    Variant::Root C2g(currentData.C2g);
    remap("C2g", C2g);


    if (!haveEmittedLogMeta && loggingEgress) {
        haveEmittedLogMeta = true;

        loggingEgress->incomingData(buildLogMeta(start));
    }


    logValue(start, end, "F1", std::move(F1));
    logValue(start, end, "X", std::move(X));
    logValue(start, end, "Q", std::move(Q));
    logValue(start, end, "P1", std::move(P1));
    logValue(start, end, "P2", std::move(P2));
    logValue(start, end, "T1", std::move(T1));
    logValue(start, end, "T2", std::move(T2));
    logValue(start, end, "VA1", std::move(VA1));
    logValue(start, end, "VA2", std::move(VA2));
    logValue(start, end, "C1", std::move(C1));
    logValue(start, end, "C2", std::move(C2));

    realtimeValue(frameTime, "C1g", std::move(C1g));
    realtimeValue(frameTime, "C2g", std::move(C2g));
}

AcquireThermoOzone49iQ::Report::Report() : X(FP::undefined()),
                                           Q(FP::undefined()),
                                           P1(FP::undefined()),
                                           P2(FP::undefined()),
                                           T1(FP::undefined()),
                                           T2(FP::undefined()),
                                           VA1(FP::undefined()),
                                           VA2(FP::undefined()),
                                           C1(FP::undefined()),
                                           C1g(FP::undefined()),
                                           C2(FP::undefined()),
                                           C2g(FP::undefined())
{ }

bool AcquireThermoOzone49iQ::Report::operator==(const Report &other) const
{
    return alarms == other.alarms &&
            FP::equal(X, other.X) &&
            FP::equal(Q, other.Q) &&
            FP::equal(P1, other.P1) &&
            FP::equal(P2, other.P2) &&
            FP::equal(T1, other.T1) &&
            FP::equal(T2, other.T2) &&
            FP::equal(VA1, other.VA1) &&
            FP::equal(VA2, other.VA2) &&
            FP::equal(C1, other.C1) &&
            FP::equal(C1g, other.C1g) &&
            FP::equal(C2, other.C2) &&
            FP::equal(C2g, other.C2g);
}

AcquireThermoOzone49iQ::Command::Command() = default;

AcquireThermoOzone49iQ::Command::Command(Util::ByteArray data) : packet(std::move(data))
{
    packet.pop_front();
}

AcquireThermoOzone49iQ::Command::Command(const Command &) = default;

AcquireThermoOzone49iQ::Command &AcquireThermoOzone49iQ::Command::operator=(const Command &) = default;

AcquireThermoOzone49iQ::Command::Command(Command &&) = default;

AcquireThermoOzone49iQ::Command &AcquireThermoOzone49iQ::Command::operator=(Command &&) = default;

void AcquireThermoOzone49iQ::Command::stateDescription(Variant::Write state) const
{
    state["RawCommand"].setBinary(packet);
    if (packet.empty())
        return;

    switch (packet[0]) {
    case 0x03:
    case 0x04:
        state["Command"].setString("Read");
        if (packet.size() >= 5) {
            state["Register"].setInteger(qFromBigEndian<quint16>(packet.data<const uchar *>(1)));
            state["Count"].setInteger(qFromBigEndian<quint16>(packet.data<const uchar *>(3)));
        }
        break;
    case 0x06:
        state["Command"].setString("Write");
        if (packet.size() >= 5) {
            state["Register"].setInteger(qFromBigEndian<quint16>(packet.data<const uchar *>(1)));
            state["Value"].setInteger(qFromBigEndian<quint16>(packet.data<const uchar *>(3)));
        }
        break;
    case 0x10:
        state["Command"].setString("Write");
        if (packet.size() >= 5) {
            state["Register"].setInteger(qFromBigEndian<quint16>(packet.data<const uchar *>(1)));
            state["Count"].setInteger(qFromBigEndian<quint16>(packet.data<const uchar *>(3)));
        }
        break;
    case 0x17:
        state["Command"].setString("ReadWrite");
        if (packet.size() >= 9) {
            state["ReadRegister"].setInteger(
                    qFromBigEndian<quint16>(packet.data<const uchar *>(1)));
            state["ReadCount"].setInteger(qFromBigEndian<quint16>(packet.data<const uchar *>(3)));
            state["WriteRegister"].setInteger(
                    qFromBigEndian<quint16>(packet.data<const uchar *>(5)));
            state["WriteCount"].setInteger(qFromBigEndian<quint16>(packet.data<const uchar *>(7)));
        }
        break;
    default:
        break;
    }
}

bool AcquireThermoOzone49iQ::Command::isRead() const
{
    if (packet.empty())
        return false;
    switch (packet[0]) {
    case 0x03:
    case 0x04:
    case 0x17:
        return true;
    default:
        break;
    }
    return false;
}

AcquireThermoOzone49iQ::RegisterType AcquireThermoOzone49iQ::Command::reg() const
{
    if (packet.empty())
        return RegisterType::INVALID;

    switch (packet[0]) {
    case 0x03:
    case 0x04:
    case 0x06:
    case 0x10:
    case 0x17:
        if (packet.size() < 3)
            return RegisterType::INVALID;
        return static_cast<RegisterType>(qFromBigEndian<quint16>(packet.data<const uchar *>(1)));
    default:
        break;
    }
    return RegisterType::INVALID;
}

bool AcquireThermoOzone49iQ::Command::checkResponse(const Util::ByteView &response) const
{
    if (packet.empty())
        return false;
    if (response.empty())
        return false;

    if (response[0] & 0x80) {
        if (response.size() < 2)
            return false;
        return true;
    }

    switch (packet[0]) {
    case 0x03:
    case 0x04:
    case 0x17: {
        if (packet.size() < 5)
            return false;
        auto expectedReg = qFromBigEndian<quint16>(packet.data<const uchar *>(3));
        if (expectedReg < 1 || expectedReg > 0x7D)
            return false;
        if (response.size() <= 2)
            return false;
        if (response[0] != packet[0])
            return false;
        if (response[1] != expectedReg * 2)
            return false;
        if (response.size() != static_cast<std::size_t>(expectedReg * 2 + 2))
            return false;
        break;
    }
    case 0x06:
    case 0x10:
        if (packet.size() < 5)
            return false;
        if (response.size() < 5)
            return false;
        if (qFromBigEndian<quint16>(packet.data<const uchar *>(1)) !=
                qFromBigEndian<quint16>(response.data<const uchar *>(1)))
            return false;
        if (qFromBigEndian<quint16>(packet.data<const uchar *>(3)) !=
                qFromBigEndian<quint16>(response.data<const uchar *>(3)))
            return false;
        break;
    default:
        return false;
    }

    return true;
}

Util::ByteView AcquireThermoOzone49iQ::Command::toBytes(const Util::ByteView &response) const
{
    if (packet.empty())
        return {};
    switch (packet[0]) {
    case 0x03:
    case 0x04:
    case 0x17: {
        auto byteCount = response[1];
        return response.mid(2, byteCount);
    }
    default:
        break;
    }
    return {};
}

std::vector<
        std::uint16_t> AcquireThermoOzone49iQ::Command::toData(const Util::ByteView &response) const
{
    auto raw = toBytes(response);
    if (raw.empty())
        return {};
    std::vector<std::uint16_t> result;
    for (auto ptr = raw.data<const uchar *>(), end = ptr + raw.size() - 2; ptr <= end; ptr += 2) {
        result.emplace_back(qFromBigEndian<quint16>(ptr));
    }
    return result;
}

std::string AcquireThermoOzone49iQ::Command::toString(const Util::ByteView &response) const
{
    auto raw = toBytes(response);
    if (raw.empty())
        return {};
    auto end = raw.indexOf(0);
    if (end == raw.npos)
        return raw.toString();
    return raw.mid(0, end).toString();
}

float AcquireThermoOzone49iQ::Command::toFloat(const Util::ByteView &response) const
{
    auto raw = toBytes(response);
    if (raw.size() < 4)
        return {};
    /* Floats are sent as a IEEE 754 float, but with each half sent as a big endian
     * U16 (this was probably the result of a little endian machine casting the float into two
     * U16s then sending it through a generic encoder that byte swaps them).  The result is that
     * the halves need a byte swap on any endianness. */
    std::array<std::uint8_t, 4> data;
    data[0] = raw[1];
    data[1] = raw[0];
    data[2] = raw[3];
    data[3] = raw[2];
    float result;
    std::memcpy(&result, &data[0], 4);
    return result;
}

void AcquireThermoOzone49iQ::sendRequest(CPD3::Util::ByteArray &&request)
{
    request.push_front(config.front().address);

    queuedCommands.emplace_back(request);

    if (config.front().modbusTCP) {
        Util::ByteArray header;
        qToBigEndian<quint16>(0, header.tail<uchar *>(2));
        qToBigEndian<quint16>(0, header.tail<uchar *>(2));
        qToBigEndian<quint16>(request.size(), header.tail<uchar *>(2));
        header += std::move(request);
        request = std::move(header);
    } else {
        auto c = crc.calculate(request);
        qToLittleEndian<quint16>(c, request.tail<uchar *>(2));
    }

    if (controlStream) {
        controlStream->writeControl(std::move(request));
    }
}

void AcquireThermoOzone49iQ::sendReadCommand(std::uint16_t reg, std::uint8_t count)
{
    Util::ByteArray request;
    request.push_back(0x03);
    qToBigEndian<quint16>(reg, request.tail<uchar *>(2));
    qToBigEndian<quint16>(count, request.tail<uchar *>(2));
    return sendRequest(std::move(request));
}

void AcquireThermoOzone49iQ::sendReadCommand(RegisterType reg)
{
    std::uint8_t count = 2;
    switch (reg) {
    case RegisterType::SerialNumber:
        count = 7;
        break;
    case RegisterType::FirmwareVersion:
        count = 16;
        break;
    case RegisterType::HostName:
        count = 8;
        break;
    case RegisterType::GasUnits:
        count = 3;
        break;
    case RegisterType::SetClockData:
        count = 6;
        break;
    case RegisterType::SetClockSource:
    case RegisterType::SetClockCommit:
        count = 1;
        break;
    case RegisterType::AlarmStatus:
        count = 13;
        break;
    default:
        break;
    }
    return sendReadCommand(static_cast<std::uint16_t>(reg), count);
}

void AcquireThermoOzone49iQ::sendWriteCommand(std::uint16_t reg,
                                              const std::vector<std::uint16_t> &data)
{
    Util::ByteArray request;
    switch (data.size()) {
    case 0:
        return;
    case 1:
        request.push_back(0x06);
        qToBigEndian<quint16>(reg, request.tail<uchar *>(2));
        qToBigEndian<quint16>(data.front(), request.tail<uchar *>(2));
        break;
    default:
        Q_ASSERT(data.size() <= 0x7B);
        request.push_back(0x10);
        qToBigEndian<quint16>(reg, request.tail<uchar *>(2));
        qToBigEndian<quint16>(data.size(), request.tail<uchar *>(2));
        request.push_back(data.size() * 2);
        for (auto add : data) {
            qToBigEndian<quint16>(add, request.tail<uchar *>(2));
        }
        break;
    }
    return sendRequest(std::move(request));
}

void AcquireThermoOzone49iQ::sendWriteCommand(RegisterType reg,
                                              const std::vector<std::uint16_t> &data)
{ return sendWriteCommand(static_cast<std::uint16_t>(reg), data); }

void AcquireThermoOzone49iQ::sendWriteCommand(RegisterType reg, std::uint16_t value)
{
    Util::ByteArray request;
    request.push_back(0x06);
    qToBigEndian<quint16>(static_cast<std::uint16_t>(reg), request.tail<uchar *>(2));
    qToBigEndian<quint16>(value, request.tail<uchar *>(2));
    return sendRequest(std::move(request));
}

void AcquireThermoOzone49iQ::sendWriteCommand(RegisterType reg, const std::string &value)
{
    Util::ByteArray request;
    request.push_back(0x10);
    qToBigEndian<quint16>(static_cast<std::uint16_t>(reg), request.tail<uchar *>(2));
    if (value.empty()) {
        qToBigEndian<quint16>(1, request.tail<uchar *>(2));
        request.push_back(2);
        qToBigEndian<quint16>(0, request.tail<uchar *>(2));
    } else {
        auto len = value.size() + 1;
        auto nreg = len / 2 + len % 2;
        Q_ASSERT(nreg <= 0x7B);
        qToBigEndian<quint16>(nreg, request.tail<uchar *>(2));
        request.push_back(nreg * 2);

        auto target = request.tail<char *>(nreg * 2);
        std::copy(value.begin(), value.end(), target);
        target += value.size();
        std::fill(target, request.data<char *>() + request.size(), 0);
    }
    return sendRequest(std::move(request));
}

Variant::Write AcquireThermoOzone49iQ::invalidResponseBase(const Util::ByteArray &frame,
                                                           double time,
                                                           int code)
{
    Variant::Write info = Variant::Write::empty();
    info.hash("Code").setInteger(code);
    info.hash("Response").setBinary(frame);

    switch (responseState) {
    case ResponseState::Passive_Initialize:
        info.hash("ResponseState").setString("PassiveInitialize");
        timeoutAt(FP::undefined());
        responseState = ResponseState::Passive_Wait;
        break;
    case ResponseState::Passive_Wait:
        info.hash("ResponseState").setString("PassiveWait");
        timeoutAt(FP::undefined());
        responseState = ResponseState::Passive_Wait;
        break;
    case ResponseState::Passive_Run:
        info.hash("ResponseState").setString("PassiveRun");
        timeoutAt(FP::undefined());
        responseState = ResponseState::Passive_Wait;
        break;
    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
    case ResponseState::Interactive_Start_Flush:
    case ResponseState::Interactive_Restart_Wait:
        return Variant::Write::empty();
    case ResponseState::Interactive_Start_Read_SerialNumber:
    case ResponseState::Interactive_Start_Read_FirmwareVersion:
    case ResponseState::Interactive_Start_Read_HostName:
    case ResponseState::Interactive_Start_Read_GasUnits:
    case ResponseState::Interactive_Start_Set_PPB:
    case ResponseState::Interactive_Start_Set_Clock_Start:
    case ResponseState::Interactive_Start_Set_Clock_Data:
    case ResponseState::Interactive_Start_Commit_Clock:
    case ResponseState::Interactive_Start_Set_Clock_End:
        responseState = ResponseState::Interactive_Restart_Wait;
        if (controlStream)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(time + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        generalStatusUpdated();
        invalidateLogValues(time);
        return Variant::Write::empty();

    case ResponseState::Interactive_Start_Read_Concentration:
        info.hash("ResponseState").setString("InteractiveStartReadConcentration");

        responseState = ResponseState::Interactive_Restart_Wait;
        if (controlStream)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(time + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        break;


    case ResponseState::Interactive_Initialize:
        info.hash("ResponseState").setString("InteractiveInitialize");
        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(time + 10.0);
        discardData(time + 1.0);
        break;
    case ResponseState::Interactive_Run_Read:
        info.hash("ResponseState").setString("InteractiveRunRead");
        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(time + 10.0);
        discardData(time + 1.0);
        break;
    case ResponseState::Interactive_Run_Wait:
        info.hash("ResponseState").setString("InteractiveRunWait");
        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(time + 10.0);
        discardData(time + 1.0);
        break;
    }
    generalStatusUpdated();

    invalidateLogValues(time);

    return info;
}

void AcquireThermoOzone49iQ::invalidResponse(const Util::ByteArray &frame, double time, int code)
{
    Variant::Write info = invalidResponseBase(frame, time, code);
    if (!info.exists())
        return;

    qCDebug(log) << "Response at" << Logging::time(time) << ":" << frame << "rejected with code"
                 << code;

    event(time,
          QObject::tr("Invalid response received (code %1).  Communications dropped.").arg(code),
          true, info);
}

void AcquireThermoOzone49iQ::invalidResponse(const Util::ByteArray &frame,
                                             double time,
                                             const Command &command,
                                             int code)
{
    Variant::Write info = invalidResponseBase(frame, time, code);
    if (!info.exists())
        return;

    qCDebug(log) << "Response at" << Logging::time(time) << ":" << frame << "in response to"
                 << static_cast<int>(command.reg()) << "rejected with code" << code;

    command.stateDescription(info);
    event(time,
          QObject::tr("Invalid response received (code %1).  Communications dropped.").arg(code),
          true, info);
}

void AcquireThermoOzone49iQ::configurationAdvance(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.empty());
}

void AcquireThermoOzone49iQ::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    configurationAdvance(frameTime);

    Util::ByteView response;
    if (config.front().modbusTCP) {
        if (frame.size() < 2)
            return invalidResponse(frame, frameTime, 1);
        if (frame.front() != config.front().address)
            return;
        response = frame.mid(1);
    } else {
        if (frame.size() < 4)
            return invalidResponse(frame, frameTime, 2);
        if (crc.calculate(frame) != 0)
            return invalidResponse(frame, frameTime, 3);
        if (frame.front() != config.front().address)
            return;
        response = frame.mid(1, frame.size() - 3);
    }
    Q_ASSERT(response.size() >= 2);

    if (queuedCommands.empty())
        return invalidResponse(frame, frameTime, 4);

    auto command = std::move(queuedCommands.front());
    queuedCommands.pop_front();
    if (!command.checkResponse(response))
        return invalidResponse(frame, frameTime, command, 5);
    if (response[0] & 0x80)
        return invalidResponse(frame, frameTime, command, 100 + response[1]);

    if (!haveEmittedRealtimeMeta && realtimeEgress) {
        haveEmittedRealtimeMeta = true;

        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }

    if (command.isRead()) {
        switch (command.reg()) {
        default:
            break;

        case RegisterType::SerialNumber: {
            bool ok = false;
            auto i = command.toBytes(response).string_trimmed().parse_i64(&ok);
            if (ok && INTEGER::defined(i) && i > 0) {
                if (instrumentMeta["SerialNumber"].toInteger() != i) {
                    instrumentMeta["SerialNumber"].setInteger(i);
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;
                    sourceMetadataUpdated();
                }
            } else {
                auto str = Util::trimmed(command.toString(response));
                if (!str.empty() && instrumentMeta["SerialNumber"].toString() != str) {
                    instrumentMeta["SerialNumber"].setString(str);
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;
                    sourceMetadataUpdated();
                }
            }
            break;
        }

        case RegisterType::FirmwareVersion: {
            auto str = Util::trimmed(command.toString(response));
            auto parts = Util::halves(str, ' ');
            if (Util::contains(parts.first, "iQ")) {
                if (!parts.first.empty() && instrumentMeta["Model"].toString() != parts.first) {
                    instrumentMeta["Model"].setString(parts.first);
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;
                    sourceMetadataUpdated();
                }

                str = std::move(parts.second);
            }

            if (!str.empty() && instrumentMeta["FirmwareVersion"].toString() != str) {
                instrumentMeta["FirmwareVersion"].setString(str);
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }

            break;
        }

        case RegisterType::HostName: {
            auto str = Util::trimmed(command.toString(response));
            if (!str.empty() && instrumentMeta["HostName"].toString() != str) {
                instrumentMeta["HostName"].setString(str);
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }

            break;
        }

        case RegisterType::AlarmStatus: {
            auto setAlarms = command.toData(response);
            currentData.alarms.clear();
            for (std::size_t i = 0, max =
                    std::min<std::size_t>(setAlarms.size(), instrumentAlarmTranslation.size());
                    i < max;
                    i++) {
                if (!setAlarms[i])
                    continue;
                currentData.alarms.insert(instrumentAlarmTranslation[i]);
            }

            if (realtimeEgress &&
                    (realtimeStateUpdated || realtimeStateAlarms != currentData.alarms)) {
                realtimeStateUpdated = false;
                realtimeStateAlarms = currentData.alarms;

                if (realtimeStateAlarms.empty()) {
                    realtimeEgress->incomingData(
                            SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                          FP::undefined()));
                } else if (realtimeStateAlarms.size() == 1) {
                    realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                            *realtimeStateAlarms.begin()), frameTime, FP::undefined()));
                } else {
                    realtimeEgress->incomingData(
                            SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("AlarmActive"),
                                          frameTime, FP::undefined()));
                }
            }
            break;
        }

        case RegisterType::Concentration:
            currentData.X = command.toFloat(response);
            if (FP::defined(currentData.X) && currentData.X > -20 && currentData.X < 1000)
                ++autoprobeValidRecords;
            break;
        case RegisterType::FlowA:
            currentData.Q = command.toFloat(response);
            if (FP::defined(currentData.Q) && currentData.Q > -1 && currentData.Q < 10)
                ++autoprobeValidRecords;
            break;
        case RegisterType::PhotometerPressureA:
            currentData.P1 = command.toFloat(response) * 1.33322;
            if (FP::defined(currentData.P1) && currentData.P1 > 100 && currentData.P1 < 1200)
                ++autoprobeValidRecords;
            break;
        case RegisterType::PumpPressure:
            currentData.P2 = command.toFloat(response) * 1.33322;
            if (FP::defined(currentData.P2) && currentData.P2 > 0 && currentData.P2 < 1200)
                ++autoprobeValidRecords;
            break;
        case RegisterType::BenchTemperature:
            currentData.T1 = command.toFloat(response);
            if (FP::defined(currentData.T1) && currentData.T1 > -100 && currentData.T1 < 120)
                ++autoprobeValidRecords;
            break;
        case RegisterType::LampTemperature:
            currentData.T2 = command.toFloat(response);
            if (FP::defined(currentData.T2) && currentData.T2 > -100 && currentData.T2 < 120)
                ++autoprobeValidRecords;
            break;
        case RegisterType::LampCurrent:
            currentData.VA1 = command.toFloat(response);
            break;
        case RegisterType::LampHeaterCurrent:
            currentData.VA2 = command.toFloat(response);
            break;
        case RegisterType::CountsA:
            currentData.C1 = command.toFloat(response);
            if (FP::defined(currentData.C1) && currentData.C1 >= 0.0)
                ++autoprobeValidRecords;
            break;
        case RegisterType::CountsANoise:
            currentData.C1g = command.toFloat(response);
            break;
        case RegisterType::CountsB:
            currentData.C2 = command.toFloat(response);
            if (FP::defined(currentData.C2) && currentData.C2 >= 0.0)
                ++autoprobeValidRecords;
            break;
        case RegisterType::CountsBNoise:
            currentData.C2g = command.toFloat(response);
            break;
        }
    }

    double pollDelay = config.front().pollInterval;
    if (!FP::defined(pollDelay) || pollDelay < 0.0)
        pollDelay = 0.0;
    switch (responseState) {
    case ResponseState::Passive_Run:
        timeoutAt(frameTime + pollDelay + 10.0);
        emitDataRecord(frameTime);
        break;

    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Initialize:
        if (autoprobeValidRecords > 0) {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);
            responseState = ResponseState::Passive_Run;
            generalStatusUpdated();

            timeoutAt(frameTime + pollDelay + 10.0);

            realtimeStateUpdated = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveWait");
            event(frameTime, QObject::tr("Passive communications established."), false, info);
        }
        break;

    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
        timeoutAt(frameTime + pollDelay + 10.0);
        if (autoprobeValidRecords > 3) {
            qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

            responseState = ResponseState::Passive_Run;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            realtimeStateUpdated = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveAutoprobeWait");
            event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);
        }
        break;

    case ResponseState::Interactive_Start_Read_SerialNumber:
        if (command.reg() != RegisterType::SerialNumber || !command.isRead())
            return invalidResponse(frame, frameTime, command, 1000);
        responseState = ResponseState::Interactive_Start_Read_FirmwareVersion;
        timeoutAt(frameTime + 5.0);
        sendReadCommand(RegisterType::FirmwareVersion);

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadFirmwareVersion"), frameTime, FP::undefined()));
        }
        break;
    case ResponseState::Interactive_Start_Read_FirmwareVersion:
        if (command.reg() != RegisterType::FirmwareVersion || !command.isRead())
            return invalidResponse(frame, frameTime, command, 1001);
        responseState = ResponseState::Interactive_Start_Read_HostName;
        timeoutAt(frameTime + 5.0);
        sendReadCommand(RegisterType::HostName);

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadHostName"), frameTime, FP::undefined()));
        }
        break;
    case ResponseState::Interactive_Start_Read_HostName:
        if (command.reg() != RegisterType::HostName || !command.isRead())
            return invalidResponse(frame, frameTime, command, 1002);
        responseState = ResponseState::Interactive_Start_Read_GasUnits;
        timeoutAt(frameTime + 5.0);
        sendReadCommand(RegisterType::GasUnits);

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadGasUnits"), frameTime, FP::undefined()));
        }
        break;
    case ResponseState::Interactive_Start_Read_GasUnits:
        if (command.reg() != RegisterType::GasUnits || !command.isRead())
            return invalidResponse(frame, frameTime, command, 1003);
        if (command.toString(response) == "ppb") {
            responseState = ResponseState::Interactive_Start_Set_Clock_Start;
            timeoutAt(frameTime + 5.0);
            sendWriteCommand(RegisterType::SetClockSource, 3);

            if (realtimeEgress) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveSetClockStart"), frameTime, FP::undefined()));
            }
        } else {
            responseState = ResponseState::Interactive_Start_Set_PPB;
            timeoutAt(frameTime + 5.0);
            sendWriteCommand(RegisterType::GasUnits, "ppb");

            if (realtimeEgress) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                           Variant::Root("StartInteractiveSetPPB"),
                                                           frameTime, FP::undefined()));
            }
        }
        break;
    case ResponseState::Interactive_Start_Set_PPB:
        if (command.reg() != RegisterType::GasUnits || command.isRead())
            return invalidResponse(frame, frameTime, command, 1004);
        responseState = ResponseState::Interactive_Start_Set_Clock_Start;
        timeoutAt(frameTime + 5.0);
        sendWriteCommand(RegisterType::SetClockSource, 3);

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveSetClockStart"), frameTime, FP::undefined()));
        }
        break;
    case ResponseState::Interactive_Start_Set_Clock_Start: {
        if (command.reg() != RegisterType::SetClockSource || command.isRead())
            return invalidResponse(frame, frameTime, command, 1005);
        responseState = ResponseState::Interactive_Start_Set_Clock_Data;
        timeoutAt(frameTime + 5.0);

        auto dt = Time::toDateTime(frameTime + 0.5);
        sendWriteCommand(RegisterType::SetClockData,
                         std::vector<std::uint16_t>{static_cast<std::uint16_t>(dt.time().hour()),
                                                    static_cast<std::uint16_t>(dt.time().minute()),
                                                    static_cast<std::uint16_t>(dt.time().second()),
                                                    static_cast<std::uint16_t>(dt.date().month()),
                                                    static_cast<std::uint16_t>(dt.date().day()),
                                                    static_cast<std::uint16_t>(dt.date().year()),});

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveSetClockData"), frameTime, FP::undefined()));
        }
        break;
    }
    case ResponseState::Interactive_Start_Set_Clock_Data:
        if (command.reg() != RegisterType::SetClockData || command.isRead())
            return invalidResponse(frame, frameTime, command, 1006);
        responseState = ResponseState::Interactive_Start_Commit_Clock;
        timeoutAt(frameTime + 5.0);
        sendWriteCommand(RegisterType::SetClockCommit, 1);

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveSetClockCommit"), frameTime, FP::undefined()));
        }
        break;
    case ResponseState::Interactive_Start_Commit_Clock:
        if (command.reg() != RegisterType::SetClockCommit || command.isRead())
            return invalidResponse(frame, frameTime, command, 1006);
        responseState = ResponseState::Interactive_Start_Set_Clock_End;
        timeoutAt(frameTime + 5.0);
        sendWriteCommand(RegisterType::SetClockSource, 0);

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                       Variant::Root("StartInteractiveSetClockEnd"),
                                                       frameTime, FP::undefined()));
        }
        break;
    case ResponseState::Interactive_Start_Set_Clock_End:
        if (command.reg() != RegisterType::SetClockSource || command.isRead())
            return invalidResponse(frame, frameTime, command, 1007);
        responseState = ResponseState::Interactive_Start_Read_Concentration;
        timeoutAt(frameTime + 5.0);
        sendReadCommand(RegisterType::Concentration);

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadConcentration"), frameTime, FP::undefined()));
        }
        break;
    case ResponseState::Interactive_Start_Read_Concentration: {
        if (command.reg() != RegisterType::Concentration || !command.isRead())
            return invalidResponse(frame, frameTime, command, 1008);
        if (!FP::defined(currentData.X) || currentData.X < -100.0)
            return invalidResponse(frame, frameTime, command, 1009);

        qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

        lastOutputData = Report();
        currentData = Report();
        lastOutputEnd = FP::undefined();

        responseState = ResponseState::Interactive_Run_Read;
        timeoutAt(frameTime + 10.0);
        sendReadCommand(RegisterType::AlarmStatus);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        generalStatusUpdated();

        Variant::Write info = Variant::Write::empty();
        info.hash("ResponseState").setString("StartInteractiveRead");
        event(frameTime, QObject::tr("Communications established."), false, info);

        realtimeStateUpdated = true;
        break;
    }

    case ResponseState::Interactive_Run_Read: {
        switch (command.reg()) {
        case RegisterType::AlarmStatus:
            timeoutAt(frameTime + 10.0);
            sendReadCommand(RegisterType::Concentration);
            break;
        case RegisterType::Concentration:
            timeoutAt(frameTime + 10.0);
            sendReadCommand(RegisterType::FlowA);
            emitDataRecord(frameTime);
            break;
        case RegisterType::FlowA:
            timeoutAt(frameTime + 10.0);
            sendReadCommand(RegisterType::PhotometerPressureA);
            break;
        case RegisterType::PhotometerPressureA:
            timeoutAt(frameTime + 10.0);
            sendReadCommand(RegisterType::PumpPressure);
            break;
        case RegisterType::PumpPressure:
            timeoutAt(frameTime + 10.0);
            sendReadCommand(RegisterType::BenchTemperature);
            break;
        case RegisterType::BenchTemperature:
            timeoutAt(frameTime + 10.0);
            sendReadCommand(RegisterType::LampTemperature);
            break;
        case RegisterType::LampTemperature:
            timeoutAt(frameTime + 10.0);
            sendReadCommand(RegisterType::LampCurrent);
            break;
        case RegisterType::LampCurrent:
            timeoutAt(frameTime + 10.0);
            sendReadCommand(RegisterType::LampHeaterCurrent);
            break;
        case RegisterType::LampHeaterCurrent:
            timeoutAt(frameTime + 10.0);
            sendReadCommand(RegisterType::CountsA);
            break;
        case RegisterType::CountsA:
            timeoutAt(frameTime + 10.0);
            sendReadCommand(RegisterType::CountsANoise);
            break;
        case RegisterType::CountsANoise:
            timeoutAt(frameTime + 10.0);
            sendReadCommand(RegisterType::CountsB);
            break;
        case RegisterType::CountsB:
            timeoutAt(frameTime + 10.0);
            sendReadCommand(RegisterType::CountsBNoise);
            break;
        case RegisterType::CountsBNoise:
            /* Fall through */
        default:
            if (FP::defined(pollDelay) && pollDelay > 0.0) {
                responseState = ResponseState::Interactive_Run_Wait;
                timeoutAt(frameTime + pollDelay * 2.0 + 5.0);
                discardData(frameTime + pollDelay);
            } else {
                responseState = ResponseState::Interactive_Run_Read;
                timeoutAt(frameTime + 10.0);
                sendReadCommand(RegisterType::AlarmStatus);
            }
            break;
        }

        break;
    }

    default:
        break;
    }
}

void AcquireThermoOzone49iQ::incomingInstrumentTimeout(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);

    switch (responseState) {
    case ResponseState::Interactive_Start_Flush:
    case ResponseState::Interactive_Start_Read_SerialNumber:
    case ResponseState::Interactive_Start_Read_FirmwareVersion:
    case ResponseState::Interactive_Start_Read_HostName:
    case ResponseState::Interactive_Start_Read_GasUnits:
    case ResponseState::Interactive_Start_Set_PPB:
    case ResponseState::Interactive_Start_Set_Clock_Start:
    case ResponseState::Interactive_Start_Set_Clock_Data:
    case ResponseState::Interactive_Start_Commit_Clock:
    case ResponseState::Interactive_Start_Set_Clock_End:
    case ResponseState::Interactive_Start_Read_Concentration:
        qCDebug(log) << "Timeout in interactive start state" << static_cast<int>(responseState)
                     << "at" << Logging::time(frameTime);

        invalidateLogValues(frameTime);

        responseState = ResponseState::Interactive_Restart_Wait;
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);
        autoprobeValidRecords = 0;
        generalStatusUpdated();

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        queuedCommands.clear();
        break;


    case ResponseState::Passive_Run: {
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        responseState = ResponseState::Passive_Wait;
        generalStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            if (!queuedCommands.empty())
                queuedCommands.front().stateDescription(info);
            info.hash("ResponseState").setString("PassiveRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        queuedCommands.clear();
        invalidateLogValues(frameTime);
        break;
    }

    case ResponseState::Interactive_Run_Read:
    case ResponseState::Interactive_Run_Wait: {
        qCDebug(log) << "Timeout in interactive run state" << static_cast<int>(responseState)
                     << "at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 1.0);

        generalStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            if (!queuedCommands.empty())
                queuedCommands.front().stateDescription(info);
            switch (responseState) {
            case ResponseState::Interactive_Run_Read:
                info.hash("ResponseState").setString("Read");
                break;
            case ResponseState::Interactive_Run_Wait:
                info.hash("ResponseState").setString("PollWait");
                break;
            default:
                break;
            }
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        queuedCommands.clear();
        break;
    }

    case ResponseState::Passive_Initialize:
    case ResponseState::Passive_Wait:
        responseState = ResponseState::Passive_Wait;
        autoprobeValidRecords = 0;
        invalidateLogValues(frameTime);
        queuedCommands.clear();
        break;

    case ResponseState::Autoprobe_Passive_Wait: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case ResponseState::Autoprobe_Passive_Initialize:
        responseState = ResponseState::Autoprobe_Passive_Wait;
        autoprobeValidRecords = 0;
        invalidateLogValues(frameTime);
        queuedCommands.clear();
        break;

    case ResponseState::Interactive_Restart_Wait:
    case ResponseState::Interactive_Initialize:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 1.0);

        generalStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }

        queuedCommands.clear();
        break;
    }
}

void AcquireThermoOzone49iQ::discardDataCompleted(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);

    switch (responseState) {
    case ResponseState::Interactive_Start_Flush:
        responseState = ResponseState::Interactive_Start_Read_SerialNumber;

        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadSerialNumber"), frameTime, FP::undefined()));
        }

        timeoutAt(frameTime + 5.0);
        sendReadCommand(RegisterType::SerialNumber);
        break;

    case ResponseState::Interactive_Restart_Wait:
        if (config.front().modbusTCP && controlStream) {
            controlStream->resetControl();
        }
    case ResponseState::Interactive_Initialize:
        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(frameTime + 10.0);
        discardData(frameTime + 1.0);

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    case ResponseState::Interactive_Run_Wait:
        responseState = ResponseState::Interactive_Run_Read;
        timeoutAt(frameTime + 10.0);
        sendReadCommand(RegisterType::AlarmStatus);
        break;

    default:
        break;
    }
}

static std::size_t responseSize(const Util::ByteView &data)
{
    Q_ASSERT(data.size() >= 2);

    auto functionCode = data[1];
    if (functionCode & 0x80)
        return 3;

    switch (functionCode) {
    case 0x17:
    case 0x03:
    case 0x04: {
        auto len = data[2];
        if (static_cast<std::size_t>(2 + 1 + len) > data.size())
            return data.npos;
        return 2 + 1 + len;
    }
    case 0x06:
    case 0x10:
        return 2 + 4;
    default:
        break;
    }

    return 2;
}

static std::size_t requestSize(const Util::ByteView &data)
{
    Q_ASSERT(data.size() >= 1);

    auto functionCode = data[1];
    switch (functionCode) {
    case 0x03:
    case 0x04:
    case 0x06:
        if (data.size() < 6)
            return data.npos;
        return 2 + 4;
    case 0x10: {
        if (data.size() < 7)
            return data.npos;
        auto len = data[6];
        if (static_cast<std::size_t>(2 + 5 + len) > data.size())
            return data.npos;
        return 2 + 5 + len;
    }
    case 0x17: {
        if (data.size() < 11)
            return data.npos;
        auto len = data[10];
        if (static_cast<std::size_t>(2 + 9 + len) > data.size())
            return data.npos;
        return 2 + 9 + len;
    }
    default:
        break;
    }

    return 2;
}

void AcquireThermoOzone49iQ::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    configurationAdvance(frameTime);

    if (frame.size() < 2)
        return;
    if (frame.front() != config.front().address)
        return;

    bool isFirstCommand = queuedCommands.empty();
    if (config.front().modbusTCP) {
        if (frame.size() < requestSize(frame))
            return;
        queuedCommands.emplace_front(frame);
    } else {
        if (frame.size() - 2 < requestSize(frame))
            return;
        if (crc.calculate(frame) != 0)
            return;
        queuedCommands.emplace_front(Util::ByteArray(frame.mid(0, frame.size() - 2)));
    }

    switch (responseState) {
    case ResponseState::Autoprobe_Passive_Initialize:
        responseState = ResponseState::Autoprobe_Passive_Wait;
    case ResponseState::Autoprobe_Passive_Wait:
        if (isFirstCommand)
            timeoutAt(frameTime + 10.0);
        break;
    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Run:
        if (isFirstCommand)
            timeoutAt(frameTime + 2.0);
        break;

    default:
        break;
    }
}

std::size_t AcquireThermoOzone49iQ::dataFrameStart(std::size_t offset,
                                                   const Util::ByteArray &input) const
{
    auto addr = config.front().address;
    if (config.front().modbusTCP) {
        for (auto max = input.size(); offset < max; ++offset) {
            if (offset + 6 >= max)
                break;
            auto protocol = qFromBigEndian<quint16>(input.data<const uchar *>(offset + 2));
            if (protocol != 0)
                continue;
            auto len = qFromBigEndian<quint16>(input.data<const uchar *>(offset + 4));
            if (len <= 1)
                continue;
            if (offset + 6 + len > max)
                break;
            if (input[offset + 6] != addr) {
                offset += 6 + len;
                continue;
            }
            return offset + 6;
        }
    } else {
        for (auto max = input.size(); offset < max; ++offset) {
            offset = input.indexOf(addr, offset);
            if (offset == input.npos)
                break;
            if (offset + 4 > max)
                break;
            auto size = responseSize(input.mid(offset, input.size() - offset - 2));
            if (size == input.npos)
                break;
            if (crc.calculate(input.mid(offset, size + 2)) != 0)
                continue;
            return offset;
        }
    }
    return input.npos;
}

std::size_t AcquireThermoOzone49iQ::dataFrameEnd(std::size_t start,
                                                 const Util::ByteArray &input) const
{
    if (config.front().modbusTCP) {
        if (start < 6)
            return input.npos;
        Q_ASSERT(input[start] == config.front().address);
        auto len = qFromBigEndian<quint16>(input.data<const uchar *>(start - 2));
        if (len <= 1)
            return input.npos;
        return start + len;
    } else {
        if (start + 4 > input.size())
            return input.npos;
        Q_ASSERT(input[start] == config.front().address);
        auto size = responseSize(input.mid(start));
        if (size == input.npos)
            return input.npos;
        size += 2;
        if (start + size > input.size())
            return input.npos;
        return start + size;
    }
}

std::size_t AcquireThermoOzone49iQ::controlFrameStart(std::size_t offset,
                                                      const Util::ByteArray &input) const
{
    auto addr = config.front().address;
    if (config.front().modbusTCP) {
        for (auto max = input.size(); offset < max; ++offset) {
            if (offset + 6 >= max)
                break;
            auto protocol = qFromBigEndian<quint16>(input.data<const uchar *>(offset + 2));
            if (protocol != 0)
                continue;
            auto len = qFromBigEndian<quint16>(input.data<const uchar *>(offset + 4));
            if (len <= 1)
                continue;
            if (offset + 6 + len > max)
                break;
            if (input[offset + 6] != addr) {
                offset += 6 + len;
                continue;
            }
            return offset + 6;
        }
    } else {
        for (auto max = input.size(); offset < max; ++offset) {
            offset = input.indexOf(addr, offset);
            if (offset == input.npos)
                break;
            if (offset + 4 > max)
                break;
            auto size = requestSize(input.mid(offset, input.size() - offset - 2));
            if (size == input.npos)
                break;
            if (crc.calculate(input.mid(offset, size + 2)) != 0)
                continue;
            return offset;
        }
    }
    return input.npos;
}

std::size_t AcquireThermoOzone49iQ::controlFrameEnd(std::size_t start,
                                                    const Util::ByteArray &input) const
{
    if (config.front().modbusTCP) {
        if (start < 6)
            return input.npos;
        Q_ASSERT(input[start] == config.front().address);
        auto len = qFromBigEndian<quint16>(input.data<const uchar *>(start - 2));
        if (len <= 1)
            return input.npos;
        return start + len;
    } else {
        if (start + 4 > input.size())
            return input.npos;
        Q_ASSERT(input[start] == config.front().address);
        auto size = requestSize(input.mid(start));
        if (size == input.npos)
            return input.npos;
        size += 2;
        if (start + size > input.size())
            return input.npos;
        return start + size;
    }
}

void AcquireThermoOzone49iQ::limitDataBuffer(Util::ByteArray &buffer)
{
    if (config.front().modbusTCP) {
        static constexpr std::size_t maxmimumSize = 0xFFFF + 6;
        if (buffer.size() <= maxmimumSize)
            return;

        auto firstAddressFound = buffer.indexOf(config.front().address);
        while (firstAddressFound != buffer.npos &&
                firstAddressFound <= 6 &&
                buffer.size() > maxmimumSize) {
            buffer.pop_front(firstAddressFound + 1);
            firstAddressFound = buffer.indexOf(config.front().address);
        }

        if (firstAddressFound == buffer.npos) {
            buffer.clear();
            return;
        }

        buffer.pop_front(firstAddressFound - 6);
    } else {
        static constexpr std::size_t maxmimumSize = 0xFF * 2 + 12;
        if (buffer.size() <= maxmimumSize)
            return;

        auto firstAddressFound = buffer.indexOf(config.front().address);
        while (firstAddressFound == 0 && buffer.size() > maxmimumSize) {
            buffer.pop_front();
            firstAddressFound = buffer.indexOf(config.front().address);
        }

        if (firstAddressFound == buffer.npos) {
            buffer.clear();
            return;
        }

        buffer.pop_front(firstAddressFound);
    }
}


Variant::Root AcquireThermoOzone49iQ::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireThermoOzone49iQ::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case ResponseState::Passive_Run:
    case ResponseState::Interactive_Run_Read:
    case ResponseState::Interactive_Run_Wait:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

AcquisitionInterface::AutoprobeStatus AcquireThermoOzone49iQ::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireThermoOzone49iQ::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case ResponseState::Interactive_Run_Read:
    case ResponseState::Interactive_Run_Wait:
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << static_cast<int>(responseState) << "at" << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case ResponseState::Interactive_Start_Flush:
    case ResponseState::Interactive_Start_Read_SerialNumber:
    case ResponseState::Interactive_Start_Read_FirmwareVersion:
    case ResponseState::Interactive_Start_Read_HostName:
    case ResponseState::Interactive_Start_Read_GasUnits:
    case ResponseState::Interactive_Start_Set_PPB:
    case ResponseState::Interactive_Start_Set_Clock_Start:
    case ResponseState::Interactive_Start_Set_Clock_Data:
    case ResponseState::Interactive_Start_Commit_Clock:
    case ResponseState::Interactive_Start_Set_Clock_End:
    case ResponseState::Interactive_Start_Read_Concentration:
    case ResponseState::Passive_Run:
    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Initialize:
    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
    case ResponseState::Interactive_Initialize:
    case ResponseState::Interactive_Restart_Wait:
        qCDebug(log) << "Interactive autoprobe started from state"
                     << static_cast<int>(responseState) << "at" << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(time + 10.0);
        discardData(time + 1.0);

        generalStatusUpdated();

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireThermoOzone49iQ::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    configurationAdvance(time);

    Q_ASSERT(!config.empty());
    double unpolledResponseTime = config.front().pollInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 10.0;
    else
        unpolledResponseTime += 5.0;

    discardData(time + 0.5);
    timeoutAt(time + unpolledResponseTime * 4.0 + 6.0);

    qCDebug(log) << "Reset to passive autoprobe from state" << static_cast<int>(responseState)
                 << "at" << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = ResponseState::Autoprobe_Passive_Wait;
    autoprobeValidRecords = 0;
}

void AcquireThermoOzone49iQ::autoprobePromote(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + 5.0);

    switch (responseState) {
    case ResponseState::Interactive_Start_Flush:
    case ResponseState::Interactive_Start_Read_SerialNumber:
    case ResponseState::Interactive_Start_Read_FirmwareVersion:
    case ResponseState::Interactive_Start_Read_HostName:
    case ResponseState::Interactive_Start_Read_GasUnits:
    case ResponseState::Interactive_Start_Set_PPB:
    case ResponseState::Interactive_Start_Set_Clock_Start:
    case ResponseState::Interactive_Start_Set_Clock_Data:
    case ResponseState::Interactive_Start_Commit_Clock:
    case ResponseState::Interactive_Start_Set_Clock_End:
    case ResponseState::Interactive_Start_Read_Concentration:
    case ResponseState::Interactive_Run_Read:
    case ResponseState::Interactive_Run_Wait:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << static_cast<int>(responseState)
                     << "to interactive acquisition at" << Logging::time(time);
        break;

    case ResponseState::Passive_Run:
    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Initialize:
    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
    case ResponseState::Interactive_Initialize:
    case ResponseState::Interactive_Restart_Wait:
        qCDebug(log) << "Promoted from passive state" << static_cast<int>(responseState)
                     << "to interactive acquisition at" << Logging::time(time);

        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(time + 30.0);
        discardData(time + 1.0);

        generalStatusUpdated();

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireThermoOzone49iQ::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    configurationAdvance(time);

    Q_ASSERT(!config.empty());
    double unpolledResponseTime = config.front().pollInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 10.0;
    else
        unpolledResponseTime += 5.0;

    timeoutAt(time + unpolledResponseTime * 2.0 + 1.0);

    switch (responseState) {
    case ResponseState::Passive_Run:
    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Initialize:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << static_cast<int>(responseState)
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from autoprobe passive state" << static_cast<int>(responseState)
                     << "to passive acquisition at" << Logging::time(time);
        responseState = ResponseState::Passive_Initialize;
        break;

    case ResponseState::Interactive_Initialize:
    case ResponseState::Interactive_Start_Flush:
    case ResponseState::Interactive_Start_Read_SerialNumber:
    case ResponseState::Interactive_Start_Read_FirmwareVersion:
    case ResponseState::Interactive_Start_Read_HostName:
    case ResponseState::Interactive_Start_Read_GasUnits:
    case ResponseState::Interactive_Start_Set_PPB:
    case ResponseState::Interactive_Start_Set_Clock_Start:
    case ResponseState::Interactive_Start_Set_Clock_Data:
    case ResponseState::Interactive_Start_Commit_Clock:
    case ResponseState::Interactive_Start_Set_Clock_End:
    case ResponseState::Interactive_Start_Read_Concentration:
    case ResponseState::Interactive_Run_Read:
    case ResponseState::Interactive_Run_Wait:
    case ResponseState::Interactive_Restart_Wait:
        qCDebug(log) << "Promoted from state" << static_cast<int>(responseState)
                     << "to passive acquisition at" << Logging::time(time);

        discardData(FP::undefined());

        responseState = ResponseState::Passive_Initialize;
        generalStatusUpdated();
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireThermoOzone49iQ::getDefaults()
{
    AutomaticDefaults result;
    result.name = "G$1$2";
    result.setSerialN81(9600);
    return result;
}


ComponentOptions AcquireThermoOzone49iQComponent::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireThermoOzone49iQComponent::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data")));

    return examples;
}

std::unique_ptr<AcquisitionInterface> AcquireThermoOzone49iQComponent::createAcquisitionPassive(
        const ComponentOptions &options,
        const std::string &loggingContext)
{
    return std::unique_ptr<AcquisitionInterface>(
            new AcquireThermoOzone49iQ(options, loggingContext));
}

std::unique_ptr<AcquisitionInterface> AcquireThermoOzone49iQComponent::createAcquisitionPassive(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    return std::unique_ptr<AcquisitionInterface>(
            new AcquireThermoOzone49iQ(config, loggingContext));
}

std::unique_ptr<AcquisitionInterface> AcquireThermoOzone49iQComponent::createAcquisitionAutoprobe(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireThermoOzone49iQ> i(new AcquireThermoOzone49iQ(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<AcquisitionInterface> AcquireThermoOzone49iQComponent::createAcquisitionInteractive(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireThermoOzone49iQ> i(new AcquireThermoOzone49iQ(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
