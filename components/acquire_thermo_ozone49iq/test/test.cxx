/*
 * Copyright (c) 2020 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <array>
#include <cstdint>
#include <cstring>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>
#include <QtEndian>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"
#include "algorithms/crc.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    bool useTCPFraming;
    std::uint8_t address;
    double updateRemaining;

    Algorithms::CRC16 crc;

    std::array<bool, 13> alarms;

    double X;
    double Q;
    double P1;
    double P2;
    double T1;
    double T2;

    double VA1;
    double VA2;

    double C1;
    double C1g;
    double C2;
    double C2g;

    ModelInstrument()
            : incoming(), outgoing(), useTCPFraming(true), address(1), updateRemaining(0.0)
    {
        alarms.fill(false);

        X = 24.25;
        Q = 1.25;
        P1 = 700;
        P2 = 300;
        T1 = 30.0;
        T2 = 50.0;
        VA1 = 1.5;
        VA2 = 1.75;
        C1 = 10000;
        C1g = 100;
        C2 = 11000;
        C2g = 30;
    }

    void sendResponse(const QByteArray &data)
    {
        if (useTCPFraming) {
            QByteArray header(6, 0);
            qToBigEndian<quint16>(0, reinterpret_cast<uchar *>(header.data()));
            qToBigEndian<quint16>(0, reinterpret_cast<uchar *>(header.data()) + 2);
            qToBigEndian<quint16>(data.size(), reinterpret_cast<uchar *>(header.data()) + 4);
            outgoing += header;
            outgoing += data;
        } else {
            outgoing += data;
            auto location = outgoing.size();
            outgoing.resize(location + 2);
            qToLittleEndian<quint16>(crc.calculate(data),
                                     reinterpret_cast<uchar *>(outgoing.data()) + location);
        }
    }

    void sendError(int functionCode, int exceptionCode = 0x01)
    {
        QByteArray data(2, 0);
        data[0] = functionCode | 0x80;
        data[1] = exceptionCode;
        return sendResponse(data);
    }

    QByteArray packFloat(float value)
    {
        using std::swap;
        Util::ByteArray result;
        result.resize(4);
        std::memcpy(result.data(), &value, 4);
        swap(result[0], result[1]);
        swap(result[2], result[3]);
        return result.toQByteArray();
    }

    static std::size_t requestSize(const Util::ByteView &data)
    {
        Q_ASSERT(data.size() >= 1);

        auto functionCode = data[1];
        switch (functionCode) {
        case 0x03:
        case 0x04:
        case 0x06:
            if (data.size() < 6)
                return data.npos;
            return 2 + 4;
        case 0x10: {
            if (data.size() < 7)
                return data.npos;
            auto len = data[6];
            if (static_cast<std::size_t>(2 + 5 + len) > data.size())
                return data.npos;
            return 2 + 5 + len;
        }
        case 0x17: {
            if (data.size() < 11)
                return data.npos;
            auto len = data[10];
            if (static_cast<std::size_t>(2 + 9 + len) > data.size())
                return data.npos;
            return 2 + 9 + len;
        }
        default:
            break;
        }

        return 2;
    }

    QByteArray nextFrame()
    {
        if (useTCPFraming) {
            for (; incoming.size() > 7; incoming.remove(0, 1)) {
                //auto txID = qFromBigEndian<quint16>(reinterpret_cast<const uchar *>(incoming.data()));
                auto protocolID = qFromBigEndian<quint16>(
                        reinterpret_cast<const uchar *>(incoming.data()) + 2);
                auto length = qFromBigEndian<quint16>(
                        reinterpret_cast<const uchar *>(incoming.data()) + 4);
                //auto unitID = static_cast<std::uint8_t>(incoming[6]);
                if (protocolID != 0)
                    continue;
                if (incoming.size() - 6 < length)
                    return {};
                auto result = incoming.mid(6, length);
                incoming.remove(0, length + 6);
                return result;
            }
        } else {
            for (; incoming.size() >= 2; incoming.remove(0, 1)) {
                if (static_cast<std::uint8_t>(incoming[0]) != address)
                    continue;
                auto length = requestSize(incoming);
                if (length == Util::ByteView::npos)
                    return {};
                if (incoming.size() < static_cast<int>(length - 2))
                    return {};
                if (crc.calculate(incoming.mid(0, length + 2)) != 0)
                    continue;
                auto result = incoming.mid(0, length);
                incoming.remove(0, length + 2);
                return result;
            }
        }

        return {};
    }

    void advance(double seconds)
    {
        updateRemaining -= seconds;
        if (updateRemaining <= 0.0) {
            while (updateRemaining <= 0.0) {
                updateRemaining += 1.0;
            }
            alarms[0] = !alarms[0];
        }

        QByteArray request;
        while (!(request = nextFrame()).isEmpty()) {
            auto expectedSize = requestSize(request);
            if (static_cast<int>(expectedSize) != request.size())
                continue;

            std::uint8_t functionCode = request[1];
            switch (functionCode) {
            case 0x03:
            case 0x04: {
                auto reg = qFromBigEndian<quint16>(
                        reinterpret_cast<const uchar *>(request.data()) + 2) + 1;
                auto count = qFromBigEndian<quint16>(
                        reinterpret_cast<const uchar *>(request.data()) + 4);

                QByteArray response;
                switch (reg) {
                case 516:
                    response = "12345";
                    break;
                case 524:
                    response = "49iQ 1.6.10.33674";
                    break;
                case 540:
                    response = "iQSeries";
                    break;
                case 7556:
                    response = "ppb";
                    break;
                case 1458:
                    for (auto a : alarms) {
                        response.push_back(static_cast<char>(0));
                        response.push_back(static_cast<char>(a ? 1 : 0));
                    }
                    break;
                case 2101:
                    response = packFloat(X);
                    break;
                case 1414:
                    response = packFloat(Q);
                    break;
                case 1442:
                    response = packFloat(P1);
                    break;
                case 1445:
                    response = packFloat(P2);
                    break;
                case 1471:
                    response = packFloat(T1);
                    break;
                case 1473:
                    response = packFloat(T2);
                    break;
                case 1475:
                    response = packFloat(VA1);
                    break;
                case 1452:
                    response = packFloat(VA2);
                    break;
                case 1454:
                    response = packFloat(C1);
                    break;
                case 1456:
                    response = packFloat(C2);
                    break;
                case 2109:
                    response = packFloat(C1g);
                    break;
                case 2111:
                    response = packFloat(C2g);
                    break;
                default:
                    break;
                }

                while (response.size() < count * 2) {
                    response.push_back(static_cast<char>(0));
                }
                response.resize(count * 2);

                response.insert(0, response.size());
                response.insert(0, functionCode);
                response.insert(0, address);
                sendResponse(response);

                break;
            }

            case 0x06: {
                auto reg =
                        qFromBigEndian<quint16>(reinterpret_cast<uchar *>(request.data()) + 2) + 1;
                auto value = qFromBigEndian<quint16>(reinterpret_cast<uchar *>(request.data()) + 4);

                switch (reg) {
                case 5186:
                case 5187:
                case 5188:
                case 5189:
                case 5190:
                case 5191:
                    break;
                case 5208:
                    if (value == 0)
                        break;
                    if (value == 3)
                        break;
                    sendError(functionCode, 0x03);
                    continue;
                case 5236:
                    if (value == 1)
                        break;
                    sendError(functionCode, 0x03);
                    continue;
                default:
                    sendError(functionCode, 0x02);
                    continue;
                }

                sendResponse(request);
                break;
            }

            case 0x10: {
                auto reg = qFromBigEndian<quint16>(
                        reinterpret_cast<const uchar *>(request.data()) + 2) + 1;
                auto count = qFromBigEndian<quint16>(
                        reinterpret_cast<const uchar *>(request.data()) + 4);
                if (static_cast<std::uint8_t>(request[6]) != count * 2) {
                    sendError(functionCode, 0x04);
                    continue;
                }
                switch (reg) {
                case 5186:
                    break;
                case 7556:
                    break;
                default:
                    sendError(functionCode, 0x02);
                    continue;
                }

                sendResponse(request.mid(0, 6));
                break;
            }

            default:
                sendError(functionCode, 0x01);
                break;
            }
        }
    }
};


class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;

    bool checkMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("X", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("Q", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("P1", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("P2", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("T1", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("T2", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("VA1", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("VA2", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("C1", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("C2", Variant::Root(), {}, time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("ZSTATE", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("C1g", Variant::Root(), {}, time))
            return false;
        if (!stream.hasMeta("C2g", Variant::Root(), {}, time))
            return false;
        return true;
    }

    bool checkContiguous(StreamCapture &stream)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("X"))
            return false;
        if (!stream.checkContiguous("Q"))
            return false;
        if (!stream.checkContiguous("P1"))
            return false;
        if (!stream.checkContiguous("P2"))
            return false;
        if (!stream.checkContiguous("T1"))
            return false;
        if (!stream.checkContiguous("T2"))
            return false;
        if (!stream.checkContiguous("VA1"))
            return false;
        if (!stream.checkContiguous("VA2"))
            return false;
        if (!stream.checkContiguous("C1"))
            return false;
        if (!stream.checkContiguous("C2"))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream,
                     const ModelInstrument &model,
                     double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("X", Variant::Root(model.X), time))
            return false;
        if (!stream.hasAnyMatchingValue("Q", Variant::Root(model.Q), time))
            return false;
        if (!stream.hasAnyMatchingValue("P1", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("P2", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("T1", Variant::Root(model.T1), time))
            return false;
        if (!stream.hasAnyMatchingValue("T2", Variant::Root(model.T2), time))
            return false;
        if (!stream.hasAnyMatchingValue("VA1", Variant::Root(model.VA1), time))
            return false;
        if (!stream.hasAnyMatchingValue("VA2", Variant::Root(model.VA2), time))
            return false;
        if (!stream.hasAnyMatchingValue("C1", Variant::Root(model.C1), time))
            return false;
        if (!stream.hasAnyMatchingValue("C2", Variant::Root(model.C2), time))
            return false;
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream,
                             const ModelInstrument &model,
                             double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("C1g", Variant::Root(model.C1g), time))
            return false;
        if (!stream.hasAnyMatchingValue("C2g", Variant::Root(model.C2g), time))
            return false;
        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_thermo_ozone49iq"));
        QVERIFY(component);

        QLocale::setDefault(QLocale::C);
        //Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["TCPFraming"] = false;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.useTCPFraming = false;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        QByteArray readConcentration(8, 0);
        readConcentration[0] = instrument.address;
        readConcentration[1] = 0x03;
        qToBigEndian<quint16>(2100, reinterpret_cast<uchar *>(readConcentration.data()) + 2);
        qToBigEndian<quint16>(2, reinterpret_cast<uchar *>(readConcentration.data()) + 4);
        qToLittleEndian<quint16>(instrument.crc.calculate(readConcentration.mid(0, 6)),
                                 reinterpret_cast<uchar *>(readConcentration.data()) + 6);

        QByteArray readAlarms(8, 0);
        readAlarms[0] = instrument.address;
        readAlarms[1] = 0x04;
        qToBigEndian<quint16>(1457, reinterpret_cast<uchar *>(readAlarms.data()) + 2);
        qToBigEndian<quint16>(13, reinterpret_cast<uchar *>(readAlarms.data()) + 4);
        qToLittleEndian<quint16>(instrument.crc.calculate(readAlarms.mid(0, 6)),
                                 reinterpret_cast<uchar *>(readAlarms.data()) + 6);

        for (int i = 0; i < 20; i++) {
            control.externalControl(readConcentration);
            control.advance(0.25);
            control.externalControl(readAlarms);
            control.advance(0.25);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated.wait([&] {
            return interface->getAutoprobeStatus() ==
                    AcquisitionInterface::AutoprobeStatus::Success;
        }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 50; i++) {
            control.externalControl(readConcentration);
            control.advance(0.25);
            control.externalControl(readAlarms);
            control.advance(0.25);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(logging.hasAnyMatchingValue("X", Variant::Root(instrument.X)));
        QVERIFY(realtime.hasAnyMatchingValue("X", Variant::Root(instrument.X)));
    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["TCPFraming"] = true;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.useTCPFraming = true;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 300; i++) {
            control.advance(0.5);
            QTest::qSleep(10);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["TCPFraming"] = true;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.useTCPFraming = true;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QByteArray readConcentration(12, 0);
        readConcentration[5] = 6;
        readConcentration[6] = instrument.address;
        readConcentration[7] = 0x03;
        qToBigEndian<quint16>(2100, reinterpret_cast<uchar *>(readConcentration.data()) + 8);
        qToBigEndian<quint16>(2, reinterpret_cast<uchar *>(readConcentration.data()) + 10);

        QByteArray readAlarms(12, 0);
        readAlarms[5] = 6;
        readAlarms[6] = instrument.address;
        readAlarms[7] = 0x04;
        qToBigEndian<quint16>(1457, reinterpret_cast<uchar *>(readAlarms.data()) + 8);
        qToBigEndian<quint16>(13, reinterpret_cast<uchar *>(readAlarms.data()) + 10);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.externalControl(readConcentration);
            control.advance(0.25);
            control.externalControl(readAlarms);
            control.advance(0.25);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }

        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")));
        control.advance(0.1);

        for (int i = 0; i < 300; i++) {
            control.externalControl(readConcentration);
            control.advance(0.25);
            control.externalControl(readAlarms);
            control.advance(0.25);
            QTest::qSleep(10);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(logging.hasAnyMatchingValue("X", Variant::Root(instrument.X)));
        QVERIFY(realtime.hasAnyMatchingValue("X", Variant::Root(instrument.X)));
    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["TCPFraming"] = false;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.useTCPFraming = false;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        ElapsedTimer elapsedTime;
        elapsedTime.start();
        while (elapsedTime.elapsed() < 30000) {
            control.advance(0.1);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")));

        for (int i = 0; i < 300; i++) {
            control.advance(0.5);
            QTest::qSleep(10);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

};

QTEST_MAIN(TestComponent)

#include "test.moc"
