/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/component.hxx"
#include "datacore/processingstage.hxx"
#include "datacore/dynamictimeinterval.hxx"
#include "smoothing/smoothingengine.hxx"

#include "smooth_df.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Smoothing;


QString SmoothGeneralDigitalFilterComponent::getGeneralSerializationName() const
{ return QString::fromLatin1("smooth_df"); }

ComponentOptions SmoothGeneralDigitalFilterComponent::getOptions()
{
    ComponentOptions options;

    ComponentOptionDoubleList *dl =
            new ComponentOptionDoubleList(tr("a", "name"), tr("The \"A\" values of the filter"),
                                          tr("These are the constants multiplied with the un-smoothed values.  "
                                                     "during filter calculation.  The more recent ones are first "
                                                     "(leftmost) in the list.  For example the multiplier on the value "
                                                     "being smoothed is the first value on the left."),
                                          tr("1.0"));
    dl->setMinimumComponents(1);
    options.add("a", dl);

    options.add("b",
                new ComponentOptionDoubleList(tr("b", "name"), tr("The \"B\" values of the filter"),
                                              tr("These are the constants multiplied with the smoothed values.  "
                                                         "during filter calculation.  The more recent ones are first "
                                                         "(leftmost) in the list.  For example the multiplier on the "
                                                         "previous output is the first value on the left."),
                                              QString()));

    TimeIntervalSelectionOption *tis =
            new TimeIntervalSelectionOption(tr("gap", "name"), tr("The maximum allowed gap"),
                                            tr("If two values are seperated by this much time then the smoother is "
                                                       "reset.  That is, data separated by this much will be smoothed as "
                                                       "independent runs of data."),
                                            tr("Infinite", "default gap"));
    tis->setAllowZero(true);
    tis->setAllowNegative(false);
    tis->setAllowUndefined(true);
    tis->setDefaultAligned(true);
    options.add("gap", tis);

    options.add("ignore-undefined", new ComponentOptionBoolean(tr("ignore-undefined", "name"),
                                                               tr("Ignore undefined values"),
                                                               tr("If this option is set undefined values are ignored instead of "
                                                                          "forcing a smoother reset."),
                                                               tr("Disabled, undefined values cause a reset",
                                                                  "default ignore undefined mode")));

    return options;
}

QList<ComponentExample> SmoothGeneralDigitalFilterComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    qobject_cast<ComponentOptionDoubleList *>(options.get("a"))->set(QList<double>() << 0.18127);
    qobject_cast<ComponentOptionDoubleList *>(options.get("b"))->set(QList<double>() << 0.81873);
    examples.append(ComponentExample(options, tr("Single pole low pass", "default example name"),
                                     tr("This filter specifies a single pole low pass configuration with "
                                                "a time constant of five data points.  This will smooth all input "
                                                "data over any gap length but will reset on undefined values.")));

    return examples;
}

ProcessingStage *SmoothGeneralDigitalFilterComponent::createGeneralFilterDynamic(const ComponentOptions &options)
{
    bool smoothUndefined = false;
    if (options.isSet("ignore-undefined")) {
        smoothUndefined =
                qobject_cast<ComponentOptionBoolean *>(options.get("ignore-undefined"))->get();
    }

    DynamicTimeInterval *gap;
    if (options.isSet("gap")) {
        gap = qobject_cast<TimeIntervalSelectionOption *>(options.get("gap"))->getInterval();
    } else {
        gap = NULL;
    }

    QVector<double> a;
    if (options.isSet("a")) {
        a = qobject_cast<ComponentOptionDoubleList *>(options.get("a"))->get().toVector();
    } else {
        a << 1.0;
    }

    QVector<double> b;
    if (options.isSet("b")) {
        b = qobject_cast<ComponentOptionDoubleList *>(options.get("b"))->get().toVector();
    }

    return SmoothingEngine::createGeneralDigitalFilter(a, b, gap, smoothUndefined);
}

static QVector<double> valueToConstantList(const Variant::Read &v)
{
    switch (v.getType()) {
    case Variant::Type::Real: {
        QVector<double> c;
        double check = v.toDouble();
        if (FP::defined(check))
            c << check;
        return c;
    }
    default:
        break;
    }

    QVector<double> c;
    for (auto add : v.toChildren()) {
        double check = add.toDouble();
        if (!FP::defined(check))
            check = 0.0;
        c.append(check);
    }
    return c;
}

ProcessingStage *SmoothGeneralDigitalFilterComponent::createGeneralFilterEditing(double start,
                                                                                 double end,
                                                                                 const SequenceName::Component &station,
                                                                                 const SequenceName::Component &archive,
                                                                                 const ValueSegment::Transfer &config)
{
    Q_UNUSED(station);
    Q_UNUSED(archive);

    DynamicTimeInterval *gap = DynamicTimeInterval::fromConfiguration(config, "Gap", start, end);

    bool smoothUndefined = false;
    QVector<double> a;
    QVector<double> b;
    for (const auto &seg : config) {
        if (!Range::intersects(seg.getStart(), seg.getEnd(), start, end))
            continue;
        if (seg.getValue().getPath("IgnoreUndefined").exists()) {
            smoothUndefined = seg.getValue().getPath("IgnoreUndefined").toBool();
        }

        if (seg.getValue().getPath("A").exists() || seg.getValue().getPath("B").exists()) {
            a = valueToConstantList(seg.getValue().getPath("A"));
            b = valueToConstantList(seg.getValue().getPath("B"));
        }
    }

    return SmoothingEngine::createGeneralDigitalFilter(a, b, gap, smoothUndefined);
}

ProcessingStage *SmoothGeneralDigitalFilterComponent::deserializeGeneralFilter(QDataStream &stream)
{
    return SmoothingEngine::deserializeGeneralDigitalFilter(stream);
}
