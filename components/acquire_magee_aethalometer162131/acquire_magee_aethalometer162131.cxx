/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <math.h>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/composite.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_magee_aethalometer162131.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

static double convertWavelength(int index, int maximum)
{
    switch (maximum) {
    case 1:
        return 880.0;
    case 2:
        switch (index) {
        case 0:
            return 880.0;
        case 1:
            return 370.0;
        default:
            break;
        }
        break;
    case 7:
        switch (index) {
        case 0:
            return 370.0;
        case 1:
            return 470.0;
        case 2:
            return 520.0;
        case 3:
            return 590.0;
        case 4:
            return 660.0;
        case 5:
            return 880.0;
        case 6:
            return 950.0;
        default:
            break;
        }
        break;
    default:
        break;
    }
    return 0.0;
}


AcquireMageeAethalometer162131::Configuration::Configuration() : start(FP::undefined()),
                                                                 end(FP::undefined()),
                                                                 area(50.0),
                                                                 reportInterval(300.0),
                                                                 ignoreFraction(false),
                                                                 bcUnits(BC_unknown),
                                                                 filterChangeDetectThreshold(0.1),
                                                                 meanRatio(1.0),
                                                                 absorptionEfficiency(),
                                                                 flowCalibration(),
                                                                 sampleTemperature(20.0),
                                                                 samplePressure(1013.0),
                                                                 wavelengths(),
                                                                 strictMode(false)
{ }

AcquireMageeAethalometer162131::Configuration::Configuration(const Configuration &other,
                                                             double s,
                                                             double e) : start(s),
                                                                         end(e),
                                                                         area(other.area),
                                                                         reportInterval(
                                                                                 other.reportInterval),
                                                                         ignoreFraction(
                                                                                 other.ignoreFraction),
                                                                         bcUnits(other.bcUnits),
                                                                         filterChangeDetectThreshold(
                                                                                 other.filterChangeDetectThreshold),
                                                                         meanRatio(other.meanRatio),
                                                                         absorptionEfficiency(
                                                                                 other.absorptionEfficiency),
                                                                         flowCalibration(
                                                                                 other.flowCalibration),
                                                                         sampleTemperature(
                                                                                 other.sampleTemperature),
                                                                         samplePressure(
                                                                                 other.samplePressure),
                                                                         wavelengths(
                                                                                 other.wavelengths),
                                                                         strictMode(
                                                                                 other.strictMode)
{ }

AcquireMageeAethalometer162131::Configuration::Configuration(const ValueSegment &other,
                                                             double s,
                                                             double e) : start(s),
                                                                         end(e),
                                                                         area(50.0),
                                                                         reportInterval(300.0),
                                                                         ignoreFraction(false),
                                                                         bcUnits(BC_unknown),
                                                                         filterChangeDetectThreshold(
                                                                                 0.1),
                                                                         meanRatio(1.0),
                                                                         absorptionEfficiency(),
                                                                         flowCalibration(),
                                                                         sampleTemperature(20.0),
                                                                         samplePressure(1013.0),
                                                                         wavelengths(),
                                                                         strictMode(false)
{
    setFromSegment(other);
}

AcquireMageeAethalometer162131::Configuration::Configuration(const Configuration &under,
                                                             const ValueSegment &over,
                                                             double s,
                                                             double e) : start(s),
                                                                         end(e),
                                                                         area(under.area),
                                                                         reportInterval(
                                                                                 under.reportInterval),
                                                                         ignoreFraction(
                                                                                 under.ignoreFraction),
                                                                         bcUnits(under.bcUnits),
                                                                         filterChangeDetectThreshold(
                                                                                 under.filterChangeDetectThreshold),
                                                                         meanRatio(under.meanRatio),
                                                                         absorptionEfficiency(
                                                                                 under.absorptionEfficiency),
                                                                         flowCalibration(
                                                                                 under.flowCalibration),
                                                                         sampleTemperature(
                                                                                 under.sampleTemperature),
                                                                         samplePressure(
                                                                                 under.samplePressure),
                                                                         wavelengths(
                                                                                 under.wavelengths),
                                                                         strictMode(
                                                                                 under.strictMode)
{
    setFromSegment(over);
}

void AcquireMageeAethalometer162131::Configuration::setFromSegment(const ValueSegment &config)
{
    double d = config["Area"].toDouble();
    if (FP::defined(d) && d > 0.0) {
        if (d < 0.1)
            d *= 1E6;
        area = d;
    }

    {
        double v = config["ReportInterval"].toDouble();
        if (!FP::defined(v) || v <= 0.0)
            v = config["Timebase"].toDouble();
        if (FP::defined(v) && v > 0.0)
            reportInterval = v;
    }

    if (config["IgnoreFraction"].exists())
        ignoreFraction = config["IgnoreFraction"].toBool();

    if (FP::defined(config["FilterChangeDetectThreshold"].toDouble())) {
        filterChangeDetectThreshold = config["FilterChangeDetectThreshold"].toDouble();
    }

    const auto &s = config["BCUnits"].toString();
    if (!s.empty()) {
        if (Util::equal_insensitive(s, "u", "ug", "ugm3", "ug/m3")) {
            bcUnits = BC_ugm3;
        } else if (Util::equal_insensitive(s, "n", "ng", "ngm3", "ng/m3")) {
            bcUnits = BC_ngm3;
        } else {
            bcUnits = BC_unknown;
        }
    }

    if (config["MeanRatio"].exists()) {
        d = config["MeanRatio"].toDouble();
        if (FP::defined(d) || d > 0.0)
            meanRatio = d;
    }
    switch (config["AbsorptionEfficiency"].getType()) {
    case Variant::Type::Empty:
        break;
    case Variant::Type::Hash: {
        absorptionEfficiency.clear();
        for (auto child : config["AbsorptionEfficiency"].toHash()) {
            bool ok = false;
            double wl = QString::fromStdString(child.first).toDouble(&ok);
            if (!ok || FP::defined(wl) || wl < 0.0)
                continue;
            double e = child.second.toDouble();
            if (!FP::defined(e) || e <= 0.0)
                continue;
            absorptionEfficiency.insert(wl, e);
        }
        break;
    }
    case Variant::Type::Keyframe: {
        absorptionEfficiency.clear();
        for (auto child : config["AbsorptionEfficiency"].toKeyframe()) {
            if (child.first < 0.0)
                continue;
            double e = child.second.toDouble();
            if (!FP::defined(e) || e <= 0.0)
                continue;
            absorptionEfficiency.insert(child.first, e);
        }
        break;
    }
    case Variant::Type::Array: {
        absorptionEfficiency.clear();
        auto children = config["AbsorptionEfficiency"].toArray();
        for (int idx = 0, max = children.size(); idx < max; idx++) {
            double e = children[idx].toDouble();
            if (!FP::defined(e) || e <= 0.0)
                continue;
            absorptionEfficiency.insert(convertWavelength(idx, max), e);
        }
        break;
    }
    case Variant::Type::Boolean:
        absorptionEfficiency.clear();
        if (config["AbsorptionEfficiency"].toBool())
            absorptionEfficiency.insert(0.0, 14625.0);
        break;
    default: {
        absorptionEfficiency.clear();
        double e = config["AbsorptionEfficiency"].toDouble();
        if (!FP::defined(e) || e < 0.0)
            break;
        absorptionEfficiency.insert(0.0, e);
        break;
    }
    }

    if (config["FlowCalibration"].exists())
        flowCalibration = Variant::Composite::toCalibration(config["FlowCalibration"]);

    if (FP::defined(config["SampleTemperature"].toDouble()))
        sampleTemperature = config["SampleTemperature"].toDouble();
    if (FP::defined(config["SamplePressure"].toDouble()))
        samplePressure = config["SamplePressure"].toDouble();

    if (config["Wavelengths"].exists()) {
        wavelengths.clear();
        switch (config["Wavelengths"].getType()) {
        case Variant::Type::Array: {
            for (auto add : config["Wavelengths"].toArray()) {
                double wl = add.toDouble();
                if (!FP::defined(wl) || wl <= 0.0)
                    continue;
                wavelengths.append(wl);
            }
            break;
        }
        default: {
            qint64 n = config["Wavelengths"].toInt64();
            if (INTEGER::defined(n) && n > 0) {
                for (int i = 0, max = (int) n; i < max; i++) {
                    wavelengths.append(convertWavelength(i, max));
                    if (wavelengths.last() <= 0.0)
                        wavelengths.pop_back();
                }
            }
            break;
        }
        }
    }

    if (config["StrictMode"].exists())
        strictMode = config["StrictMode"].toBool();
}


void AcquireMageeAethalometer162131::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("Magee");
    instrumentMeta["Model"].setString("AE31");

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;

    monthLookup.insert("jan", 1);
    monthLookup.insert("feb", 2);
    monthLookup.insert("mar", 3);
    monthLookup.insert("apr", 4);
    monthLookup.insert("may", 5);
    monthLookup.insert("jun", 6);
    monthLookup.insert("jul", 7);
    monthLookup.insert("aug", 8);
    monthLookup.insert("sep", 9);
    monthLookup.insert("oct", 10);
    monthLookup.insert("nov", 11);
    monthLookup.insert("dec", 12);

    priorVolume = FP::undefined();
    priorIr.clear();
    lastBCUnits = BC_unknown;
    lastBCUnitsUncertain = false;
}


AcquireMageeAethalometer162131::AcquireMageeAethalometer162131(const ValueSegment::Transfer &configData,
                                                               const std::string &loggingContext)
        : FramedInstrument("aethalometer", loggingContext),
          lastRecordTime(FP::undefined()),
          lastStartTime(FP::undefined()),
          autoprobeStatus(AcquisitionInterface::AutoprobeStatus::InProgress),
          responseState(RESP_WAIT),
          effectiveWavelengths()
{
    setDefaultInvalid();
    config.append(Configuration());
    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
}

void AcquireMageeAethalometer162131::setToAutoprobe()
{ responseState = RESP_AUTOPROBE_INITIALIZE; }

void AcquireMageeAethalometer162131::setToInteractive()
{ responseState = RESP_INITIALIZE; }


ComponentOptions AcquireMageeAethalometer162131Component::getBaseOptions()
{
    ComponentOptions options;

    ComponentOptionSingleDouble *d =
            new ComponentOptionSingleDouble(tr("spot", "name"), tr("Area of the spot"),
                                            tr("This is the sampling area of the filter spot.  If it is "
                                                   "greater than 0.1 it is assumed to be in mm\xC2\xB2, otherwise "
                                                   "it is treated as being in m\xC2\xB2."),
                                            tr("50.0 mm\xC2\xB2"), 1);
    d->setMinimum(0.0, false);
    options.add("spot", d);

    ComponentOptionEnum *bcUnits =
            new ComponentOptionEnum(tr("bcunits", "name"), tr("Set the units of the EBC"),
                                    tr("This specifies the units that the instrument is set to report "
                                       "the EBC in."), tr("Autodetect from the data"));
    bcUnits->add(BC_ugm3, "ug", tr("ug", "mode name"), tr("\xCE\xBCg/m\xC2\xB3"));
    bcUnits->add(BC_ugm3, "ng", tr("ng", "mode name"), tr("ng/m\xC2\xB3"));
    options.add("bcunits", bcUnits);

    options.add("t", new ComponentOptionSingleDouble(tr("t", "name"), tr("Sample temperature"),
                                                     tr("This is the sample temperature the aethalometer is set to.  "
                                                            "The value is used to convert to an STP of zero \xC2\xB0\x43 and "
                                                            "1013.25 hPa."),
                                                     tr("20 \xC2\xB0\x43")));

    d = new ComponentOptionSingleDouble(tr("p", "name"), tr("Sample pressure"),
                                        tr("This is the sample pressure the aethalometer is set to.  "
                                               "The value is used to convert to an STP of zero \xC2\xB0\x43 and "
                                               "1013.25 hPa."), tr("1013 hPa"));
    d->setMinimum(10.0, false);
    options.add("p", d);

    return options;
}

AcquireMageeAethalometer162131::AcquireMageeAethalometer162131(const ComponentOptions &options,
                                                               const std::string &loggingContext)
        : FramedInstrument("aethalometer", loggingContext),
          lastRecordTime(FP::undefined()),
          lastStartTime(FP::undefined()),
          autoprobeStatus(AcquisitionInterface::AutoprobeStatus::InProgress),
          responseState(RESP_WAIT),
          effectiveWavelengths()
{
    setDefaultInvalid();

    config.append(Configuration());

    if (options.isSet("spot")) {
        double value = qobject_cast<ComponentOptionSingleDouble *>(options.get("spot"))->get();
        if (FP::defined(value) && value > 0.0) {
            if (value <= 0.1)
                value *= 1E6;
            config.last().area = value;
        }
    }

    if (options.isSet("t")) {
        double value = qobject_cast<ComponentOptionSingleDouble *>(options.get("t"))->get();
        if (FP::defined(value))
            config.last().sampleTemperature = value;
    }

    if (options.isSet("p")) {
        double value = qobject_cast<ComponentOptionSingleDouble *>(options.get("p"))->get();
        if (FP::defined(value) && value > 10.0)
            config.last().samplePressure = value;
    }

    if (options.isSet("bcunits")) {
        config.last().bcUnits =
                (BCUnits) qobject_cast<ComponentOptionEnum *>(options.get("bcunits"))->get()
                                                                                     .getID();
    }

    if (options.isSet("cal-q")) {
        config.last().flowCalibration =
                qobject_cast<ComponentOptionSingleCalibration *>(options.get("cal-q"))->get();
    }

    if (options.isSet("timebase")) {
        qint64 i = qobject_cast<ComponentOptionSingleInteger *>(options.get("timebase"))->get();
        if (INTEGER::defined(i) && i > 0)
            config.last().reportInterval = 60.0 * (double) i;
    }
}

AcquireMageeAethalometer162131::~AcquireMageeAethalometer162131()
{
}


void AcquireMageeAethalometer162131::logValue(double startTime,
                                              double endTime,
                                              SequenceName::Component name,
                                              Variant::Root &&value)
{
    if (!loggingEgress && !realtimeEgress)
        return;
    SequenceValue dv({{}, "raw", std::move(name)}, std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        if (FP::defined(startTime))
            loggingEgress->incomingData(std::move(dv));
        return;
    }
    if (loggingEgress && FP::defined(startTime))
        loggingEgress->incomingData(dv);
    if (FP::defined(endTime)) {
        dv.setStart(endTime);
        dv.setEnd(endTime + 1.0);
    } else if (FP::defined(startTime)) {
        dv.setEnd(startTime + 1.0);
    }
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireMageeAethalometer162131::realtimeValue(double time,
                                                   SequenceName::Component name,
                                                   Variant::Root &&value)
{
    if (realtimeEgress == NULL)
        return;
    realtimeEgress->emplaceData(SequenceName({}, "raw", std::move(name)), std::move(value), time,
                                time + 1.0);
}

static double calculateEfficiency(double wavelength, const QHash<double, double> &efficiency)
{
    double e = efficiency.value(wavelength, 0.0);
    if (e > 0.0)
        return e;
    e = efficiency.value(0.0, 0.0);
    if (e <= 0.0)
        return FP::undefined();
    return e / wavelength;
}

SequenceValue::Transfer AcquireMageeAethalometer162131::buildLogMeta(double time) const
{
    Q_ASSERT(!config.isEmpty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_magee_aethalometer162131");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);
    processing["Model"].set(instrumentMeta["Model"]);
    processing["Parameters"].hash("SampleT").setDouble(config.first().sampleTemperature);
    processing["Parameters"].hash("SampleP").setDouble(config.first().samplePressure);
    processing["Parameters"].hash("MeanRatio").setDouble(config.first().meanRatio);

    result.emplace_back(SequenceName({}, "raw_meta", "Q"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Sample flow");
    result.back().write().metadataReal("ReportT").setDouble(0);
    result.back().write().metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Flow"));

    result.emplace_back(SequenceName({}, "raw_meta", "Qt"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000.00000");
    result.back().write().metadataReal("Units").setString("m³");
    result.back().write().metadataReal("Description").setString("Accumulated sample volume");
    result.back().write().metadataReal("ReportT").setDouble(0);
    result.back().write().metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");

    result.emplace_back(SequenceName({}, "raw_meta", "L"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0000");
    result.back().write().metadataReal("Units").setString("m");
    result.back().write().metadataReal("Description").setString("Integrated sample length, Qt/A");
    result.back().write().metadataReal("ReportT").setDouble(0);
    result.back().write().metadataReal("ReportP").setDouble(1013.25);
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");

    result.emplace_back(SequenceName({}, "raw_meta", "PCT"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000.000");
    result.back().write().metadataReal("Units").setString("%");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Bypass fraction, percentage of data actually sampled");

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataSingleFlag("STP")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer162131");
    result.back().write().metadataSingleFlag("STP").hash("Bits").setInt64(0x0200);
    result.back()
          .write()
          .metadataSingleFlag("STP")
          .hash("Description")
          .setString("Data reported at STP");

    result.back()
          .write()
          .metadataSingleFlag("SpotAdvanced")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_magee_aethalometer162131");
    result.back()
          .write()
          .metadataSingleFlag("SpotAdvanced")
          .hash("Description")
          .setString("The spot has just advanced and data may be suspect");


    for (int idx = 0, max = effectiveWavelengths.size(); idx < max; ++idx) {
        double wavelength = effectiveWavelengths.at(idx);
        QString columnName(QString::number(wavelength, 'f', 0));

        auto trVariable = "Ir" + std::to_string(idx + 1);
        result.emplace_back(SequenceName({}, "raw_meta", trVariable), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Format").setString("0.0000000");
        result.back().write().metadataReal("GroupUnits").setString("Transmittance");
        result.back().write().metadataReal("Description").setString("Transmittance");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Transmittance"));
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);

        result.emplace_back(SequenceName({}, "raw_meta", "Ba" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Format").setString("0000.00");
        result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Aerosol light absorption coefficient");
        result.back().write().metadataReal("ReportT").setDouble(0);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
        result.back().write().metadataReal("Source").set(instrumentMeta);
        double e = calculateEfficiency(wavelength, config.first().absorptionEfficiency);
        if (!FP::defined(e)) {
            result.back()
                  .write()
                  .metadataReal("Smoothing")
                  .hash("Mode")
                  .setString("BeersLawAbsorptionInitial");
            result.back()
                  .write()
                  .metadataReal("Smoothing")
                  .hash("Parameters")
                  .hash("L")
                  .setString("L");
            result.back()
                  .write()
                  .metadataReal("Smoothing")
                  .hash("Parameters")
                  .hash("Transmittance")
                  .setString(trVariable);
            result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        } else {
            auto mp = processing;
            mp["Parameters"].hash("Efficiency").setDouble(e);
            result.back().write().metadataReal("Processing").toArray().after_back().set(mp);
        }
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Bap"));
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);

        result.emplace_back(SequenceName({}, "raw_meta", "If" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Format").setString("00.0000");
        result.back().write().metadataReal("GroupUnits").setString("Intensity");
        result.back().write().metadataReal("Description").setString("Reference detector signal");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("I reference"));
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);

        result.emplace_back(SequenceName({}, "raw_meta", "Ip" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Format").setString("00.0000");
        result.back().write().metadataReal("GroupUnits").setString("Intensity");
        result.back().write().metadataReal("Description").setString("Sample detector signal");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("I sample"));
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);

        result.emplace_back(SequenceName({}, "raw_meta", "X" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Units").setString("\xCE\xBCg/m\xC2\xB3");
        result.back().write().metadataReal("Format").setString("0000.000");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Equivalent black carbon as reported by the instrument");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("ReportT").setDouble(0);
        result.back().write().metadataReal("ReportP").setDouble(1013.25);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("EBC"));
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);
        {
            auto mp = processing;
            switch (config.first().bcUnits) {
            case BC_unknown:
                mp["Parameters"].hash("OriginalUnits").setString("Autodetected");
                break;
            case BC_ugm3:
                mp["Parameters"].hash("OriginalUnits").setString("ugm3");
                break;
            case BC_ngm3:
                mp["Parameters"].hash("OriginalUnits").setString("ngm3");
                break;
            }
            result.back().write().metadataReal("Processing").toArray().after_back().set(mp);
        }
    }

    return result;
}

SequenceValue::Transfer AcquireMageeAethalometer162131::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_magee_aethalometer162131");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);

    for (int idx = 0, max = effectiveWavelengths.size(); idx < max; ++idx) {
        double wavelength = effectiveWavelengths.at(idx);
        QString columnName(QString::number(wavelength, 'f', 0));

        result.emplace_back(SequenceName({}, "raw_meta", "ZATN" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Format").setString("000.000");
        result.back().write().metadataReal("GroupUnits").setString("Attenuation");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Optical attenuation, -100*ln(Ir)");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Attenuation"));
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(idx);

        result.emplace_back(SequenceName({}, "raw_meta", "ZIp" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Format").setString("00.0000");
        result.back().write().metadataReal("GroupUnits").setString("Intensity");
        result.back().write().metadataReal("Description").setString("Raw sample intensity");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Sample"));
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(idx);

        result.emplace_back(SequenceName({}, "raw_meta", "ZIpz" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Format").setString("00.0000");
        result.back().write().metadataReal("GroupUnits").setString("Intensity");
        result.back().write().metadataReal("Description").setString("Raw sample intensity zero");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Zero"));
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(idx);

        result.emplace_back(SequenceName({}, "raw_meta", "ZIf" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Format").setString("00.0000");
        result.back().write().metadataReal("GroupUnits").setString("Intensity");
        result.back().write().metadataReal("Description").setString("Raw reference intensity");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Reference"));
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(idx);

        result.emplace_back(SequenceName({}, "raw_meta", "ZIfz" + std::to_string(idx + 1)),
                            Variant::Root(), time, FP::undefined());
        result.back().write().metadataReal("Format").setString("00.0000");
        result.back().write().metadataReal("GroupUnits").setString("Intensity");
        result.back().write().metadataReal("Description").setString("Raw reference intensity zero");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Wavelength").setDouble(wavelength);
        result.back().write().metadataReal("Smoothing").hash("Mode").setString("DifferenceInitial");
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("GroupName")
              .setString(QObject::tr("Ref Zero"));
        result.back().write().metadataReal("Realtime").hash("Page").setInt64(1);
        result.back().write().metadataReal("Realtime").hash("GroupColumn").setString(columnName);
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);
        result.back().write().metadataReal("Realtime").hash("ColumnOrder").setInt64(idx);
    }

    return result;
}

SequenceValue::Transfer AcquireMageeAethalometer162131::invalidateLogMetadata(double time,
                                                                              int from,
                                                                              int to) const
{
    SequenceValue::Transfer result;

    for (int idx = from; idx < to; ++idx) {
        result.emplace_back(SequenceName({}, "raw_meta", "Ir" + std::to_string(idx)), Variant::Root(),
                            time, FP::undefined());

        result.emplace_back(SequenceName({}, "raw_meta", "Ba" + std::to_string(idx)), Variant::Root(),
                            time, FP::undefined());

        result.emplace_back(SequenceName({}, "raw_meta", "If" + std::to_string(idx)), Variant::Root(),
                            time, FP::undefined());

        result.emplace_back(SequenceName({}, "raw_meta", "Ip" + std::to_string(idx)), Variant::Root(),
                            time, FP::undefined());
    }

    return result;
}

SequenceValue::Transfer AcquireMageeAethalometer162131::invalidateRealtimeMetadata(double time,
                                                                                   int from,
                                                                                   int to) const
{
    SequenceValue::Transfer result;

    for (int idx = from; idx < to; ++idx) {
        result.emplace_back(SequenceName({}, "raw_meta", "ZATN" + std::to_string(idx)),
                            Variant::Root(), time, FP::undefined());

        result.emplace_back(SequenceName({}, "raw_meta", "ZIp" + std::to_string(idx)),
                            Variant::Root(), time, FP::undefined());

        result.emplace_back(SequenceName({}, "raw_meta", "ZIpz" + std::to_string(idx)),
                            Variant::Root(), time, FP::undefined());

        result.emplace_back(SequenceName({}, "raw_meta", "ZIf" + std::to_string(idx)),
                            Variant::Root(), time, FP::undefined());

        result.emplace_back(SequenceName({}, "raw_meta", "ZIfz" + std::to_string(idx)),
                            Variant::Root(), time, FP::undefined());
    }

    return result;
}

static double calculateDensity(double T, double P)
{
    if (!FP::defined(T))
        T = 0.0;
    if (!FP::defined(P))
        P = 1013.25;
    if (T < 150.0) T += 273.15;
    if (T < 100.0 || T > 350.0 || P < 10.0 || P > 2000.0)
        return FP::undefined();
    return (P / 1013.25) * (273.15 / T);
}

static double correctFlow(double Q, double T, double P)
{
    if (!FP::defined(Q))
        return FP::undefined();
    double den = calculateDensity(T, P);
    if (!FP::defined(den))
        return FP::undefined();
    return Q * den;
}

static double correctEBC(double EBC, double T, double P)
{
    if (!FP::defined(EBC))
        return FP::undefined();
    double den = calculateDensity(T, P);
    if (!FP::defined(den))
        return FP::undefined();
    if (den <= 0.0)
        return FP::undefined();
    return EBC / den;
}

static double integrateVolume(double Q,
                              double Qt,
                              double fraction,
                              double startTime,
                              double endTime)
{
    if (!FP::defined(Qt))
        Qt = 0.0;
    if (!FP::defined(Q) ||
            !FP::defined(startTime) ||
            !FP::defined(endTime) ||
            !FP::defined(fraction))
        return Qt;
    if (startTime >= endTime)
        return Qt;
    /* Assume negative flows are zero due to calibration errors */
    if (Q <= 0.0 || fraction <= 0.0)
        return Qt;
    return Qt + Q * (fraction / 100.0) * (endTime - startTime) / 60000.0;
}

static double calculateIfp(double I, double ID)
{
    if (!FP::defined(I) || !FP::defined(ID))
        return FP::undefined();
    return I - ID;
}

static double calculateIr(double atn)
{
    if (!FP::defined(atn))
        return FP::undefined();
    if (atn <= 0.0)
        return FP::undefined();
    return exp(atn / -100.0);
}

static double calculateL(double Qt, double area)
{
    if (!FP::defined(Qt) || !FP::defined(area) || area <= 0.0 || Qt <= 0.0)
        return FP::undefined();
    return Qt / (area * 1E-6);
}

static double calculateBa(double Ir0, double Qt0, double Ir1, double Qt1, double area)
{
    if (!FP::defined(Ir0) ||
            !FP::defined(Qt0) ||
            !FP::defined(Ir1) ||
            !FP::defined(Qt1) ||
            !FP::defined(area))
        return FP::undefined();
    if (Ir0 <= 0.0 || Ir1 <= 0.0)
        return FP::undefined();
    double dQt = Qt1 - Qt0;
    if (dQt <= 0.0)
        return FP::undefined();
    return log(Ir0 / Ir1) * (area / dQt);
}

static double calculateBaEBC(double ebc, double meanRatio, double e, double T, double P)
{
    if (!FP::defined(ebc) || !FP::defined(meanRatio))
        return FP::undefined();
    if (meanRatio <= 0.0)
        return FP::undefined();
    double den = calculateDensity(T, P);
    if (!FP::defined(den))
        return FP::undefined();
    if (den <= 0.0)
        return FP::undefined();

    Q_ASSERT(FP::defined(e));
    return ((ebc * e) / meanRatio) / den;
}

static double calculateEBC(double Ba, double meanRatio, double e)
{
    if (!FP::defined(Ba) || !FP::defined(meanRatio))
        return FP::undefined();
    if (meanRatio <= 0.0)
        return FP::undefined();
    Q_ASSERT(FP::defined(e));
    if (e <= 0.0)
        return FP::undefined();
    return ((Ba * meanRatio) / e);
}

static void stripFieldQuotes(Util::ByteView &field)
{
    if (field.string_start("\"")) {
        if (field.size() <= 1)
            return;
        if (field.string_end("\"")) {
            field = field.mid(1, field.size() - 2);
            return;
        }
        return;
    }
}

static bool isAllDigits(const Util::ByteView &data)
{
    for (auto d : data) {
        if (d < '0' || d > '9')
            return false;
    }
    return true;
}

static bool isCompressedFormat(const Util::ByteView &data)
{
    if (data.size() != 16)
        return false;
    for (auto d : data) {
        if (d >= 'a' && d <= 'z')
            continue;
        if (d >= 'B' && d <= 'Y')
            continue;
        return false;
    }
    return true;
}

static double decompressedField(const Util::ByteView &data)
{
    if (data.empty())
        return FP::undefined();
    double result = 0.0;
    for (auto d : data) {
        result *= 50.0;
        if (d >= 'a' && d <= 'z') {
            result += (double) (d - 'a');
        } else if (d >= 'B' && d <= 'Y') {
            result += (double) (d - 'B') + 26;
        } else {
            return FP::undefined();
        }
    }
    return result;
}

int AcquireMageeAethalometer162131::processRecord(const Util::ByteView &line, double &frameTime)
{
    Q_ASSERT(!config.isEmpty());

    auto fields = Util::as_deque(line.split(','));
    bool ok = false;

    if (fields.empty()) return 1;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    if (fields.empty()) return 2;
    stripFieldQuotes(field);
    if (fields.empty()) return 3;

    Variant::Root oldSN(instrumentMeta["SerialNumber"]);
    instrumentMeta["SerialNumber"].remove();
    if (isAllDigits(field)) {
        int sn = field.parse_i32(&ok, 10);
        if (ok && sn > 0) {
            instrumentMeta["SerialNumber"].setInt64(sn);
            if (fields.empty()) return 4;
            field = fields.front().string_trimmed();
            fields.pop_front();
            if (fields.empty()) return 5;
            stripFieldQuotes(field);
            if (fields.empty()) return 6;
        }
    }
    if (instrumentMeta["SerialNumber"] != oldSN.read()) {
        haveEmittedLogMeta = false;
        haveEmittedRealtimeMeta = false;
        sourceMetadataUpdated();
    }

    auto subfields = Util::as_deque(field.split('-'));

    if (subfields.empty()) return 7;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    if (fields.empty()) return 8;
    Variant::Root day;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 1 || raw > 31) return 9;
        day.write().setInt64(raw);
    }
    remap("DAY", day);

    if (subfields.empty()) return 10;
    Variant::Root month;
    {
        int raw = monthLookup.value(subfields.front().toQByteArray().trimmed().toLower(), -1);
        if (raw < 1 || raw > 12) return 11;
        month.write().setInt64(raw);
    }
    subfields.pop_front();
    remap("MONTH", month);

    if (subfields.empty()) return 12;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    Variant::Root year;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 0) return 13;
        if (field.size() == 2) {
            /* This is not "right", but I'm hoping that all these
             * instruments will be replaced by 2070... */
            if (raw >= 70)
                raw += 1900;
            else
                raw += 2000;
            year.write().setInt64(raw);
        } else if (field.size() == 4) {
            if (raw < 1900 || raw > 2100) return 14;
            year.write().setInt64(raw);
        } else {
            return 15;
        }
    }
    remap("YEAR", year);

    if (!subfields.empty()) return 16;

    if (fields.empty()) return 17;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (fields.empty()) return 18;
    stripFieldQuotes(field);
    if (fields.empty()) return 19;
    subfields = Util::as_deque(field.split(':'));

    if (subfields.empty()) return 20;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    if (fields.empty()) return 21;
    Variant::Root hour;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 0 || raw > 23) return 22;
        hour.write().setInt64(raw);
    }
    remap("HOUR", hour);

    /* The > 60 here is intentional; I'm not sure how it's handling leap
     * seconds, so we have to allow for them and rounding */

    if (subfields.empty()) return 23;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    if (fields.empty()) return 24;
    Variant::Root minute;
    {
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 0 || raw > 60) return 25;
        minute.write().setInt64(raw);
    }
    remap("MINUTE", minute);

    Variant::Root second(0);
    if (!subfields.empty()) {
        field = subfields.front().string_trimmed();
        subfields.pop_front();
        if (fields.empty()) return 26;
        int raw = field.parse_i32(&ok);
        if (!ok || raw < 0 || raw > 60) return 27;
        second.write().setInt64(raw);
    }
    remap("SECOND", second);

    double startTime;
    double endTime;
    {
        qint64 y = year.read().toInt64();
        if (!INTEGER::defined(y) || y < 1900 || y > 2100)
            return 28;
        qint64 mo = month.read().toInt64();
        if (!INTEGER::defined(mo) || mo < 1 || mo > 12)
            return 29;
        qint64 d = day.read().toInt64();
        if (!INTEGER::defined(d) || d < 1 || d > 31)
            return 30;
        QDate date((int) y, (int) mo, (int) d);
        if (!date.isValid())
            return 31;

        qint64 h = hour.read().toInt64();
        if (!INTEGER::defined(y) || h < 0 || h > 23)
            return 32;
        qint64 m = minute.read().toInt64();
        if (!INTEGER::defined(m) || m < 0 || m > 60)
            return 33;
        qint64 s = second.read().toInt64();
        if (!INTEGER::defined(s) || s < 0 || s > 60)
            return 34;
        /* Potential leap second rounding weirdness, so correct for it */
        if (m == 60 && s == 0) {
            m = 59;
            s = 0;
        }
        QTime time((int) h, (int) m, (int) s);
        if (!time.isValid())
            return 35;

        if (!FP::defined(frameTime)) {
            /* The reported time is the start of sampling, so adjust the logic */
            startTime = Time::fromDateTime(QDateTime(date, time, Qt::UTC));
            if (!FP::defined(startTime))
                return -1;

            if (FP::defined(lastStartTime)) {
                if (startTime <= lastStartTime)
                    return -1;
                endTime = startTime + (startTime - lastStartTime);
            } else {
                endTime = startTime + config.first().reportInterval;
            }
            lastStartTime = startTime;

            if (!FP::defined(lastRecordTime) || endTime >= lastRecordTime) {
                frameTime = endTime;
                configurationAdvance(frameTime);
            }
            lastRecordTime = endTime;
        } else {
            if (!FP::defined(lastRecordTime)) {
                startTime = frameTime - config.first().reportInterval;
                endTime = frameTime;
            } else {
                startTime = lastRecordTime;
                endTime = frameTime;
            }

            lastRecordTime = endTime;
            if (FP::defined(startTime)) {
                /* Could happen in odd configuration changes, so catch it */
                if (FP::defined(lastStartTime) && startTime < lastStartTime)
                    return -1;
                lastStartTime = startTime;
                if (startTime >= endTime)
                    return -1;
            }
        }
    }


    static const Util::ByteView mvcField("\"\"");

    double sampleTemperature = config.first().sampleTemperature;
    double samplePressure = config.first().samplePressure;
    double meanRatio = config.first().meanRatio;

    QVector<double> wavelengths(config.first().wavelengths);
    Variant::Root Q;
    Variant::Root PCT;
    bool ae21mode = false;
    bool compressedFormat = false;
    switch (fields.size()) {
    case 8:
        if (wavelengths.isEmpty()) {
            wavelengths.append(880);
        }
        if (instrumentMeta["Model"].toString() != "AE16") {
            instrumentMeta["Model"] = "AE16";
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }
        field = fields[1].toQByteArray();
        if (field == mvcField) {
            Q.write().setDouble(FP::undefined());
        } else {
            stripFieldQuotes(field);
            Q.write().setDouble(field.string_trimmed().parse_real(&ok));
            if (!ok) return 36;
            if (!FP::defined(Q.read().toReal())) return 37;
            Q.write()
             .setDouble(config.first()
                              .flowCalibration
                              .apply(correctFlow(Q.read().toDouble(), sampleTemperature,
                                                 samplePressure)));
        }
        field = fields[6].toQByteArray();
        if (field == mvcField) {
            PCT.write().setDouble(FP::undefined());
        } else {
            stripFieldQuotes(field);
            PCT.write().setDouble(field.string_trimmed().parse_real(&ok));
            if (!ok) return 38;
            if (!FP::defined(PCT.read().toReal())) return 39;
            if (PCT.read().toDouble() < 0.0 || PCT.read().toDouble() > 100.0) return 40;
            PCT.write().setDouble(PCT.read().toDouble() * 100.0);
        }
        break;
    case 14:
        field = fields[7].toQByteArray();
        stripFieldQuotes(field);
        if (isCompressedFormat(field)) {
            compressedFormat = true;

            if (wavelengths.isEmpty()) {
                wavelengths.append(370);
                wavelengths.append(470);
                wavelengths.append(520);
                wavelengths.append(590);
                wavelengths.append(660);
                wavelengths.append(880);
                wavelengths.append(950);
            }
            if (instrumentMeta["Model"].toString() != "AE31") {
                instrumentMeta["Model"] = "AE31";
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }

            Q.write().setDouble(decompressedField(field.mid(14, 2)));
            if (!FP::defined(Q.read().toReal())) return 41;
            Q.write()
             .setDouble(config.first()
                              .flowCalibration
                              .apply(correctFlow(Q.read().toDouble() / 10.0, sampleTemperature,
                                                 samplePressure)));

            PCT.write().setDouble(decompressedField(field.mid(13, 1)));
            if (!FP::defined(PCT.read().toReal())) return 42;
            PCT.write().setDouble((PCT.read().toDouble() + 1.0) / 50.0);
            if (PCT.read().toDouble() < 0.0 || PCT.read().toDouble() > 1.0) return 43;
            PCT.write().setDouble(PCT.read().toDouble() * 100.0);
            break;
        }

        if (wavelengths.isEmpty()) {
            wavelengths.append(880);
            wavelengths.append(370);
        }
        if (instrumentMeta["Model"].toString() != "AE21") {
            instrumentMeta["Model"] = "AE21";
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }

        field = fields[2].toQByteArray();
        if (field == mvcField) {
            Q.write().setDouble(FP::undefined());
        } else {
            stripFieldQuotes(field);
            Q.write().setDouble(field.string_trimmed().parse_real(&ok));
            if (!ok) return 44;
            if (!FP::defined(Q.read().toReal())) return 45;
            Q.write()
             .setDouble(config.first()
                              .flowCalibration
                              .apply(correctFlow(Q.read().toDouble(), sampleTemperature,
                                                 samplePressure)));
        }

        /* Extract this here because AE21 instruments have
         * a funny format */
        ae21mode = true;
        field = fields[3].toQByteArray();
        fields.erase(fields.begin() + 3);
        if (field == mvcField) {
            PCT.write().setDouble(FP::undefined());
        } else {
            stripFieldQuotes(field);
            PCT.write().setDouble(field.string_trimmed().parse_real(&ok));
            if (!ok) return 46;
            if (!FP::defined(PCT.read().toReal())) return 47;
            if (PCT.read().toDouble() < 0.0 || PCT.read().toDouble() > 1.0) return 48;
            PCT.write().setDouble(PCT.read().toDouble() * 100.0);
        }
        break;
    case 50:
        if (wavelengths.isEmpty()) {
            wavelengths.append(370);
            wavelengths.append(470);
            wavelengths.append(520);
            wavelengths.append(590);
            wavelengths.append(660);
            wavelengths.append(880);
            wavelengths.append(950);
        }
        if (instrumentMeta["Model"].toString() != "AE31") {
            instrumentMeta["Model"] = "AE31";
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }

        field = fields[7].toQByteArray();
        if (field == mvcField) {
            Q.write().setDouble(FP::undefined());
        } else {
            stripFieldQuotes(field);
            Q.write().setDouble(field.string_trimmed().parse_real(&ok));
            if (!ok) return 49;
            if (!FP::defined(Q.read().toReal())) return 50;
            Q.write()
             .setDouble(config.first()
                              .flowCalibration
                              .apply(correctFlow(Q.read().toDouble(), sampleTemperature,
                                                 samplePressure)));
        }
        field = fields[12].toQByteArray();
        if (field == mvcField) {
            PCT.write().setDouble(FP::undefined());
        } else {
            stripFieldQuotes(field);
            PCT.write().setDouble(field.string_trimmed().parse_real(&ok));
            if (!ok) return 51;
            if (!FP::defined(PCT.read().toReal())) return 52;
            if (PCT.read().toDouble() < 0.0 || PCT.read().toDouble() > 1.0) return 53;
            PCT.write().setDouble(PCT.read().toDouble() * 100.0);
        }
        break;
    case 2:
        field = fields[1].toQByteArray();
        stripFieldQuotes(field);
        if (!isCompressedFormat(field))
            return 54;
        compressedFormat = true;

        if (wavelengths.isEmpty()) {
            wavelengths.append(880);
        }
        if (instrumentMeta["Model"].toString() != "AE16") {
            instrumentMeta["Model"] = "AE16";
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }

        Q.write().setDouble(decompressedField(field.mid(14, 2)));
        if (!FP::defined(Q.read().toReal())) return 55;
        Q.write()
         .setDouble(config.first()
                          .flowCalibration
                          .apply(correctFlow(Q.read().toDouble(), sampleTemperature,
                                             samplePressure)));

        PCT.write().setDouble(decompressedField(field.mid(13, 1)));
        if (!FP::defined(PCT.read().toReal())) return 56;
        if (PCT.write().toDouble() < 0.0 || PCT.read().toDouble() > 1.0) return 57;
        PCT.write().setDouble(PCT.read().toDouble() * 100.0);
        break;
    case 4:
        field = fields[2].toQByteArray();
        stripFieldQuotes(field);
        if (!isCompressedFormat(field))
            return 58;
        compressedFormat = true;

        if (wavelengths.isEmpty()) {
            wavelengths.append(880);
            wavelengths.append(370);
        }
        if (instrumentMeta["Model"].toString() != "AE21") {
            instrumentMeta["Model"] = "AE21";
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }

        Q.write().setDouble(decompressedField(field.mid(14, 2)));
        if (!FP::defined(Q.read().toReal())) return 59;
        Q.write()
         .setDouble(config.first()
                          .flowCalibration
                          .apply(correctFlow(Q.read().toDouble(), sampleTemperature,
                                             samplePressure)));

        PCT.write().setDouble(decompressedField(field.mid(13, 1)));
        if (!FP::defined(PCT.read().toReal())) return 60;
        if (PCT.read().toDouble() < 0.0 || PCT.read().toDouble() > 1.0) return 61;
        PCT.write().setDouble(PCT.read().toDouble() * 100.0);
        break;
    case 15:
        if (wavelengths.isEmpty()) {
            wavelengths.append(880);
            wavelengths.append(370);
        }
        if (instrumentMeta["Model"].toString() != "AE22") {
            instrumentMeta["Model"] = "AE22";
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }

        field = fields[2].toQByteArray();
        if (field == mvcField) {
            Q.write().setDouble(FP::undefined());
        } else {
            stripFieldQuotes(field);
            Q.write().setDouble(field.string_trimmed().parse_real(&ok));
            if (!ok) return 62;
            if (!FP::defined(Q.read().toReal())) return 63;
            Q.write()
             .setDouble(config.first()
                              .flowCalibration
                              .apply(correctFlow(Q.read().toDouble(), sampleTemperature,
                                                 samplePressure)));
        }
        field = fields[7].toQByteArray();
        if (field == mvcField) {
            PCT.write().setDouble(FP::undefined());
        } else {
            stripFieldQuotes(field);
            PCT.write().setDouble(field.string_trimmed().parse_real(&ok));
            if (!ok) return 64;
            if (!FP::defined(PCT.read().toReal())) return 65;
            if (PCT.read().toDouble() < 0.0 || PCT.read().toDouble() > 1.0) return 66;
            PCT.write().setDouble(PCT.read().toDouble() * 100.0);
        }
        break;
    default:
        if (wavelengths.isEmpty())
            return 100;
        if (fields.empty())
            return 101;
        field = fields[0].toQByteArray();
        stripFieldQuotes(field);
        if (isCompressedFormat(field)) {
            if (fields.size() < wavelengths.size())
                return 102;

            compressedFormat = true;

            Q.write().setDouble(decompressedField(field.mid(14, 2)));
            if (!FP::defined(Q.read().toReal())) return 103;
            Q.write()
             .setDouble(config.first()
                              .flowCalibration
                              .apply(correctFlow(Q.read().toDouble(), sampleTemperature,
                                                 samplePressure)));

            PCT.write().setDouble(decompressedField(field.mid(13, 1)));
            if (!FP::defined(PCT.read().toReal())) return 104;
            if (PCT.read().toDouble() < 0.0 || PCT.read().toDouble() > 1.0) return 105;
            PCT.write().setDouble(PCT.read().toDouble() * 100.0);
            break;
        }

        if (fields.size() < wavelengths.size() + 1)
            return 106;

        field = fields[wavelengths.size()].toQByteArray();
        if (field == mvcField) {
            Q.write().setDouble(FP::undefined());
        } else {
            stripFieldQuotes(field);
            Q.write().setDouble(field.string_trimmed().parse_real(&ok));
            if (!ok) return 107;
            if (!FP::defined(Q.read().toReal())) return 108;
            Q.write()
             .setDouble(config.first()
                              .flowCalibration
                              .apply(correctFlow(Q.read().toDouble(), sampleTemperature,
                                                 samplePressure)));
        }

        if (fields.size() < wavelengths.size() + 6)
            return 109;

        field = fields[wavelengths.size() + 5].toQByteArray();
        if (field == mvcField) {
            PCT.write().setDouble(FP::undefined());
        } else {
            stripFieldQuotes(field);
            PCT.write().setDouble(field.string_trimmed().parse_real(&ok));
            if (!ok) return 110;
            if (!FP::defined(PCT.read().toReal())) return 111;
            if (PCT.read().toDouble() < 0.0 || PCT.read().toDouble() > 1.0) return 112;
            PCT.write().setDouble(PCT.read().toDouble() * 100.0);
        }
        break;
    }
    Q_ASSERT(!wavelengths.isEmpty());

    if (config.first().ignoreFraction)
        PCT.write().setDouble(100.0);

    remap("Q", Q);
    remap("PCT", PCT);

    if (wavelengths.size() < effectiveWavelengths.size()) {
        if (loggingEgress != NULL) {
            loggingEgress->incomingData(invalidateLogMetadata(startTime, wavelengths.size(),
                                                              effectiveWavelengths.size()));
        }
        if (realtimeEgress != NULL) {
            auto meta = invalidateLogMetadata(startTime, wavelengths.size(),
                                              effectiveWavelengths.size());
            Util::append(invalidateRealtimeMetadata(startTime, wavelengths.size(),
                                                    effectiveWavelengths.size()), meta);
            realtimeEgress->incomingData(std::move(meta));
        }
    }
    effectiveWavelengths = wavelengths;

    QVector<double> X;
    enum {
        Decimals_Unknown, Decimals_AllPresent, Decimals_AllAbsent, Decimals_Ambiguous,
    } bcDecimals = Decimals_Unknown;
    for (int idx = 0, max = wavelengths.size(); idx < max; idx++) {
        if (fields.empty()) return 1000 * (idx + 1) + 1;
        field = fields.front().toQByteArray();
        fields.pop_front();
        if (field == mvcField) {
            X.append(FP::undefined());
        } else {
            stripFieldQuotes(field);
            double v = field.string_trimmed().parse_real(&ok);
            if (!ok || !FP::defined(v)) return 1000 * (idx + 1) + 2;
            X.append(correctEBC(v, sampleTemperature, samplePressure));

            if (field.indexOf('.') != field.npos) {
                switch (bcDecimals) {
                case Decimals_Unknown:
                case Decimals_AllPresent:
                    bcDecimals = Decimals_AllPresent;
                    break;
                case Decimals_AllAbsent:
                case Decimals_Ambiguous:
                    bcDecimals = Decimals_Ambiguous;
                    break;
                }
            } else {
                switch (bcDecimals) {
                case Decimals_Unknown:
                case Decimals_AllAbsent:
                    bcDecimals = Decimals_AllAbsent;
                    break;
                case Decimals_AllPresent:
                case Decimals_Ambiguous:
                    bcDecimals = Decimals_Ambiguous;
                    break;
                }
            }
        }
    }

    /* Flow (already extracted above) */
    if (!compressedFormat) {
        if (fields.empty()) return 100;
        fields.pop_front();
    }

    std::vector<Variant::Root> rawATN;
    QVector<double> ATN;
    QVector<double> Ip;
    QVector<double> Ipz;
    QVector<double> If;
    QVector<double> Ifz;
    for (int idx = 0, max = wavelengths.size(); idx < max; idx++) {
        if (fields.empty()) return 1000 * (idx + 1) + 3;
        field = fields.front().toQByteArray();
        fields.pop_front();

        if (compressedFormat) {
            stripFieldQuotes(field);
            if (field.size() != 16) return 1000 * (idx + 1) + 4;

            double v = decompressedField(field.mid(0, 3));
            if (!FP::defined(v)) return 1000 * (idx + 1) + 5;
            v = v / 1000.0 - 10.0;
            rawATN.emplace_back(v);
            remap("ZATN" + std::to_string(idx + 1), rawATN.back());
            ATN.append(rawATN.back().read().toDouble());

            v = decompressedField(field.mid(3, 2));
            if (!FP::defined(v)) return 1000 * (idx + 1) + 6;
            v = v / 10000.0;
            Ipz.append(v);

            v = decompressedField(field.mid(5, 2));
            if (!FP::defined(v)) return 1000 * (idx + 1) + 7;
            v = v / 10000.0;
            Ifz.append(v);

            v = decompressedField(field.mid(7, 3));
            if (!FP::defined(v)) return 1000 * (idx + 1) + 8;
            v = v / 10000.0 - 1.0;
            Ip.append(v);

            v = decompressedField(field.mid(10, 3));
            if (!FP::defined(v)) return 1000 * (idx + 1) + 9;
            v = v / 10000.0 - 1.0;
            If.append(v);

            continue;
        }

        if (field == mvcField) {
            Ipz.append(FP::undefined());
        } else {
            stripFieldQuotes(field);
            double v = field.string_trimmed().parse_real(&ok);
            if (!ok || !FP::defined(v)) return 1000 * (idx + 1) + 10;
            Ipz.append(v);
        }

        if (fields.empty()) return 1000 * (idx + 1) + 11;
        field = fields.front().toQByteArray();
        fields.pop_front();
        if (field == mvcField) {
            Ip.append(FP::undefined());
        } else {
            stripFieldQuotes(field);
            double v = field.string_trimmed().parse_real(&ok);
            if (!ok || !FP::defined(v)) return 1000 * (idx + 1) + 12;
            Ip.append(v);
        }

        if (fields.empty()) return 1000 * (idx + 1) + 13;
        field = fields.front().toQByteArray();
        fields.pop_front();
        if (field == mvcField) {
            Ifz.append(FP::undefined());
        } else {
            stripFieldQuotes(field);
            double v = field.string_trimmed().parse_real(&ok);
            if (!ok || !FP::defined(v)) return 1000 * (idx + 1) + 14;
            Ifz.append(v);
        }

        if (fields.empty()) return 1000 * (idx + 1) + 15;
        field = fields.front().toQByteArray();
        fields.pop_front();
        if (field == mvcField) {
            If.append(FP::undefined());
        } else {
            stripFieldQuotes(field);
            double v = field.string_trimmed().parse_real(&ok);
            if (!ok || !FP::defined(v)) return 1000 * (idx + 1) + 16;
            If.append(v);
        }

        if (!ae21mode) {
            /* Bypass fraction (extracted above) */
            if (fields.empty()) return 1000 * (idx + 1) + 17;
            fields.pop_front();
        }

        if (fields.empty()) return 1000 * (idx + 1) + 18;
        field = fields.front().toQByteArray();
        fields.pop_front();
        if (field == mvcField) {
            rawATN.emplace_back(FP::undefined());
        } else {
            stripFieldQuotes(field);
            double v = field.string_trimmed().parse_real(&ok);
            if (!ok || !FP::defined(v)) return 1000 * (idx + 1) + 19;
            rawATN.emplace_back(v);
        }
        remap("ZATN" + std::to_string(idx + 1), rawATN.back());
        ATN.append(rawATN.back().read().toDouble());
    }

    if (config.first().strictMode) {
        if (!fields.empty())
            return 999;
    }

    Q_ASSERT(X.size() == wavelengths.size());
    Q_ASSERT(ATN.size() == wavelengths.size());
    Q_ASSERT(static_cast<int>(rawATN.size()) == wavelengths.size());
    Q_ASSERT(Ip.size() == wavelengths.size());
    Q_ASSERT(Ipz.size() == wavelengths.size());
    Q_ASSERT(If.size() == wavelengths.size());
    Q_ASSERT(Ifz.size() == wavelengths.size());

    QVector<double> Ir;
    Variant::Flags flags;
    flags.insert("STP");
    bool spotChanged = false;
    for (int idx = 0, max = wavelengths.size(); idx < max; idx++) {
        if (idx >= priorIr.size()) {
            priorIr.append(FP::undefined());
        }

        double t = calculateIr(ATN.at(idx));
        Ir.append(t);
        if (!FP::defined(t))
            continue;
        /* Explicitly don't discard on undefined values so the state restore
         * works correctly without causing big spikes on startup */
        if (FP::defined(priorIr.at(idx)) &&
                (t - priorIr.at(idx)) > config.first().filterChangeDetectThreshold) {
            priorVolume = FP::undefined();
            flags.insert("SpotAdvanced");

            if (!spotChanged) {
                qCDebug(log) << "Spot begin detected at" << Logging::time(frameTime);
            }
            spotChanged = true;

            if (state != NULL)
                state->requestStateSave();
        }
    }
    Q_ASSERT(priorIr.size() >= wavelengths.size());

    double startVolume = priorVolume;
    double endVolume =
            integrateVolume(Q.read().toDouble(), priorVolume, PCT.read().toDouble(), startTime,
                            endTime);
    priorVolume = endVolume;
    double area = config.first().area;

    BCUnits bcUnits = config.first().bcUnits;
    bool ngBCProbable = false;
    bool ugBCProbable = false;
    if (bcUnits == BC_unknown) {
        for (int idx = 0, max = wavelengths.size(); idx < max; idx++) {
            double bc = X.at(idx);
            if (!FP::defined(bc))
                continue;

            double Ba = calculateBa(priorIr.at(idx), startVolume, Ir.at(idx), endVolume, area);
            if (!FP::defined(Ba))
                continue;

            double e =
                    calculateEfficiency(wavelengths.at(idx), config.first().absorptionEfficiency);
            if (!FP::defined(e))
                e = 14625.0 / wavelengths.at(idx);
            if (!FP::defined(e))
                continue;

            double ugEBC = calculateEBC(Ba, meanRatio, e);
            if (!FP::defined(ugEBC))
                continue;
            double ngEBC = ugEBC * 1E3;

            /* When they're too small to work out, try comparing ratios as a fallback */
            double ugABS = ::fabs(ugEBC);
            double bcABS = ::fabs(bc);
            if (ugABS < 10.0 || bcABS < 10.0) {
                if (bcABS <= 1.0)
                    continue;
                double ugRatio = ugABS / bcABS;
                ugRatio -= 1.0;
                double ngRatio = ::fabs(ngEBC) / bcABS;
                ngRatio -= 1.0;

                bool ngCheck = ::fabs(ngRatio) < 0.3;
                bool ugCheck = ::fabs(ugRatio) < 0.3;

                if (ngCheck && !ugCheck)
                    ngBCProbable = true;
                else if (ugCheck && !ngCheck)
                    ugBCProbable = true;
                continue;
            }

            if (::fabs(bc - ugEBC) < ::fabs(bc - ngEBC)) {
                bcUnits = BC_ugm3;
            } else {
                bcUnits = BC_ngm3;
            }
            break;
        }
    }
    bool bcUnitsUncertain = false;
    if (bcUnits == BC_unknown) {
        bcUnits = lastBCUnits;
        bcUnitsUncertain = lastBCUnitsUncertain;
    }
    if (bcUnits == BC_unknown || bcUnitsUncertain) {
        if (ngBCProbable) {
            bcUnits = BC_ngm3;
            bcUnitsUncertain = true;
        } else if (ugBCProbable) {
            bcUnits = BC_ugm3;
            bcUnitsUncertain = true;
        }
    }
    if (bcUnits == BC_unknown) {
        switch (bcDecimals) {
        case Decimals_Unknown:
        case Decimals_Ambiguous:
            break;
        case Decimals_AllPresent:
            bcUnits = BC_ugm3;
            bcUnitsUncertain = true;
            break;
        case Decimals_AllAbsent:
            bcUnits = BC_ngm3;
            bcUnitsUncertain = true;
            break;
        }
    }

    if (endTime == startTime) {
        priorIr = Ir;
        lastBCUnits = bcUnits;
        lastBCUnitsUncertain = bcUnitsUncertain;
        return 0;
    }

    if (!haveEmittedLogMeta && loggingEgress && FP::defined(startTime)) {
        haveEmittedLogMeta = true;
        loggingEgress->incomingData(buildLogMeta(startTime));
    }
    if (!haveEmittedRealtimeMeta && realtimeEgress != NULL) {
        haveEmittedRealtimeMeta = true;
        SequenceValue::Transfer meta = buildLogMeta(endTime);
        Util::append(buildRealtimeMeta(endTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }

    logValue(startTime, endTime, "Q", std::move(Q));
    logValue(startTime, endTime, "PCT", std::move(PCT));
    logValue(startTime, endTime, "F1", Variant::Root(flags));

    if (!spotChanged) {
        Variant::Root L(calculateL(endVolume, area));
        remap("L", L);
        logValue(startTime, endTime, "L", std::move(L));
    } else {
        realtimeValue(endTime, "L", Variant::Root(FP::undefined()));
    }
    if (!spotChanged) {
        Variant::Root Qt(endVolume);
        remap("Qt", Qt);
        logValue(startTime, endTime, "Qt", std::move(Qt));
    } else {
        realtimeValue(endTime, "Qt", Variant::Root(FP::undefined()));
    }

    for (int idx = 0, max = wavelengths.size(); idx < max; idx++) {
        double bc = X.at(idx);
        switch (bcUnits) {
        case BC_unknown:
            bc = FP::undefined();
            break;
        case BC_ugm3:
            break;
        case BC_ngm3:
            if (FP::defined(bc))
                bc *= 1E-3;
            break;
        }
        Variant::Root ebc(bc);
        auto nameEBC = "X" + std::to_string(idx + 1);
        remap(nameEBC, ebc);

        double e = calculateEfficiency(wavelengths.at(idx), config.first().absorptionEfficiency);
        if (FP::defined(e)) {
            Variant::Root Ba(calculateBaEBC(ebc.read().toDouble(), meanRatio, e, sampleTemperature,
                                            samplePressure));
            auto name = "Ba" + std::to_string(idx + 1);
            remap(name, Ba);
            logValue(startTime, endTime, std::move(name), std::move(Ba));
        } else {
            Variant::Root
                    Ba(calculateBa(priorIr.at(idx), startVolume, Ir.at(idx), endVolume, area));
            auto name = "Ba" + std::to_string(idx + 1);
            remap(name, Ba);
            logValue(startTime, endTime, std::move(name), std::move(Ba));
        }

        if (!spotChanged) {
            Variant::Root Irv(Ir.at(idx));
            auto name = "Ir" + std::to_string(idx + 1);
            remap(name, Irv);
            logValue(startTime, endTime, std::move(name), std::move(Irv));
        } else {
            realtimeValue(endTime, "Ir" + std::to_string(idx + 1), Variant::Root(FP::undefined()));
        }

        Variant::Root ZIp(Ip.at(idx));
        auto nameZIp = "ZIp" + std::to_string(idx + 1);
        remap(nameZIp, ZIp);

        Variant::Root ZIpz(Ipz.at(idx));
        auto nameZIpz = "ZIpz" + std::to_string(idx + 1);
        remap(nameZIpz, ZIpz);

        Variant::Root ZIf(If.at(idx));
        auto nameZIf = "ZIf" + std::to_string(idx + 1);
        remap(nameZIf, ZIf);

        Variant::Root ZIfz(Ifz.at(idx));
        auto nameZIfz = "ZIfz" + std::to_string(idx + 1);
        remap(nameZIfz, ZIfz);

        {
            Variant::Root Ip(calculateIfp(ZIp.read().toDouble(), ZIpz.read().toDouble()));
            auto name = "Ip" + std::to_string(idx + 1);
            remap(name, Ip);
            logValue(startTime, endTime, std::move(name), std::move(Ip));
        }

        {
            Variant::Root If(calculateIfp(ZIf.read().toDouble(), ZIfz.read().toDouble()));
            auto name = "If" + std::to_string(idx + 1);
            remap(name, If);
            logValue(startTime, endTime, std::move(name), std::move(If));
        }

        logValue(startTime, endTime, nameEBC, std::move(ebc));

        realtimeValue(endTime, std::move(nameZIp), std::move(ZIp));
        realtimeValue(endTime, std::move(nameZIpz), std::move(ZIpz));
        realtimeValue(endTime, std::move(nameZIf), std::move(ZIf));
        realtimeValue(endTime, std::move(nameZIfz), std::move(ZIfz));
        realtimeValue(endTime, "ZATN" + std::to_string(idx + 1), std::move(rawATN[idx]));
    }

    priorIr = Ir;
    lastBCUnits = bcUnits;
    lastBCUnitsUncertain = bcUnitsUncertain;

    return 0;
}


void AcquireMageeAethalometer162131::configurationChanged()
{
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
}

void AcquireMageeAethalometer162131::configurationAdvance(double frameTime)
{
    Q_ASSERT(!config.isEmpty());
    int oldSize = config.size();
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.isEmpty());

    if (oldSize == config.size())
        return;
    configurationChanged();
}


void AcquireMageeAethalometer162131::incomingDataFrame(const Util::ByteArray &frame,
                                                       double frameTime)
{

    if (FP::defined(frameTime))
        configurationAdvance(frameTime);

    switch (responseState) {
    case RESP_AUTOPROBE_WAIT:
    case RESP_AUTOPROBE_INITIALIZE:
        if (processRecord(frame, frameTime) == 0) {
            qCDebug(log) << "Autoprobe succeeded at" << Logging::time(frameTime);

            responseState = RESP_RUN;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            timeoutAt(frameTime + config.first().reportInterval + 60.0);

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("AutoprobeWait");
            event(frameTime, QObject::tr("Autoprobe succeeded."), false, info);
        } else {
            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            responseState = RESP_WAIT;

            lastRecordTime = FP::undefined();
            lastStartTime = frameTime;
            loggingLost(frameTime);
        }
        break;

    case RESP_WAIT:
    case RESP_INITIALIZE:
        if (processRecord(frame, frameTime) == 0) {
            qCDebug(log) << "Comms established at" << Logging::time(frameTime);

            responseState = RESP_RUN;
            generalStatusUpdated();

            timeoutAt(frameTime + config.first().reportInterval + 60.0);

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("Wait");
            event(frameTime, QObject::tr("Communications established."), false, info);
        } else {
            lastRecordTime = FP::undefined();
            /* Don't set last start time, since it was set at comms loss,
             * and resetting it leads to never advancing far enough */
            loggingLost(frameTime);
        }
        break;

    case RESP_RUN: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + config.first().reportInterval + 60.0);
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            responseState = RESP_WAIT;
            discardData(frameTime + 0.5, 1);
            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            info.hash("ResponseState").setString("Run");
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            lastRecordTime = FP::undefined();
            lastStartTime = frameTime;
            loggingLost(frameTime);
        }
        break;
    }

    default:
        break;
    }
}

void AcquireMageeAethalometer162131::incomingInstrumentTimeout(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configurationAdvance(frameTime);

    switch (responseState) {
    case RESP_RUN: {
        qCDebug(log) << "Timeout in unpolled mode at" << Logging::time(frameTime);

        if (controlStream != NULL)
            controlStream->resetControl();
        responseState = RESP_WAIT;
        discardData(frameTime + 2.0);
        generalStatusUpdated();

        Variant::Write info = Variant::Write::empty();
        info.hash("ResponseState").setString("Run");
        event(frameTime, QObject::tr("Timeout waiting for report.  Communications dropped."), true,
              info);

        lastRecordTime = FP::undefined();
        lastStartTime = frameTime;
        loggingLost(frameTime);
        break;
    }

    case RESP_AUTOPROBE_WAIT:
        qCDebug(log) << "Timeout waiting for autoprobe records" << Logging::time(frameTime);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        if (controlStream != NULL)
            controlStream->resetControl();
        discardData(frameTime + 2.0);
        responseState = RESP_WAIT;

        lastRecordTime = FP::undefined();
        lastStartTime = frameTime;
        loggingLost(frameTime);
        break;

    case RESP_INITIALIZE:
        discardData(frameTime + 0.5, 1);
        timeoutAt(frameTime + config.first().reportInterval * 2.0 + 60.0);
        responseState = RESP_WAIT;
        break;

    case RESP_AUTOPROBE_INITIALIZE:
        discardData(frameTime + 0.5, 1);
        timeoutAt(frameTime + config.first().reportInterval * 2.0 + 60.0);
        responseState = RESP_AUTOPROBE_WAIT;
        break;

    default:
        discardData(frameTime + 0.5);
        lastRecordTime = FP::undefined();
        lastStartTime = frameTime;
        loggingLost(frameTime);
        break;
    }
}

void AcquireMageeAethalometer162131::discardDataCompleted(double frameTime)
{
    Q_UNUSED(frameTime);
}

void AcquireMageeAethalometer162131::incomingControlFrame(const Util::ByteArray &frame,
                                                          double frameTime)
{
    Q_UNUSED(frameTime);
    Q_UNUSED(frame);
}

AcquisitionInterface::AutoprobeStatus AcquireMageeAethalometer162131::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

Variant::Root AcquireMageeAethalometer162131::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireMageeAethalometer162131::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_RUN:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

void AcquireMageeAethalometer162131::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_RUN:
        /* Already running, just notify that we succeeded. */
        qCDebug(log) << "Interactive autoprobe requested with communications established at"
                     << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    default:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        timeoutAt(time + config.first().reportInterval * 2.0 + 60.0);
        discardData(time + 0.5, 1);
        responseState = RESP_AUTOPROBE_WAIT;
        generalStatusUpdated();
        break;
    }
}

void AcquireMageeAethalometer162131::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    timeoutAt(time + config.first().reportInterval * 3.0 + 60.0);
    discardData(time + 0.5, 1);
    responseState = RESP_AUTOPROBE_WAIT;
    generalStatusUpdated();
}

void AcquireMageeAethalometer162131::autoprobePromote(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + config.first().reportInterval + 60.0);

    switch (responseState) {
    case RESP_RUN:
    case RESP_WAIT:
    case RESP_INITIALIZE:
        qCDebug(log) << "Promoted from state" << responseState << "to interactive acquisition at"
                     << Logging::time(time);
        break;

    case RESP_AUTOPROBE_WAIT:
    case RESP_AUTOPROBE_INITIALIZE:
        qCDebug(log) << "Promoted from passive autoprobe to interactive acquisition at"
                     << Logging::time(time);

        responseState = RESP_WAIT;
        break;
    }
}

void AcquireMageeAethalometer162131::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + config.first().reportInterval + 60.0);

    switch (responseState) {
    case RESP_RUN:
    case RESP_WAIT:
    case RESP_INITIALIZE:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);
        break;

    case RESP_AUTOPROBE_WAIT:
    case RESP_AUTOPROBE_INITIALIZE:
        qCDebug(log) << "Promoted from passive autoprobe to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_WAIT;
        break;
    }
}

static const quint16 serializedStateVersion = 1;

void AcquireMageeAethalometer162131::serializeState(QDataStream &stream)
{
    PauseLock paused(*this);

    stream << serializedStateVersion;

    /* This is very dumb, but there's not really a good way to verify it, so
     * oh well */
    stream << priorVolume << (quint32) priorIr.size();
}

void AcquireMageeAethalometer162131::deserializeState(QDataStream &stream)
{
    quint16 version = 0;
    stream >> version;
    if (version != serializedStateVersion)
        return;

    PauseLock paused(*this);

    quint32 nIr;
    stream >> priorVolume >> nIr;

    if (priorIr.size() != (int) nIr) {
        priorIr = QVector<double>((int) nIr, FP::undefined());
    }
}

AcquisitionInterface::AutomaticDefaults AcquireMageeAethalometer162131::getDefaults()
{
    AutomaticDefaults result;
    result.name = "A$1$2";
    result.setSerialN81(9600);
    result.autoprobeInitialOrder = 100;
    result.autoprobeLevelPriority = 100;
    result.autoprobeTries = 2;
    return result;
}


ComponentOptions AcquireMageeAethalometer162131Component::getOptions()
{
    ComponentOptions options(getBaseOptions());

    ComponentOptionSingleInteger *i = new ComponentOptionSingleInteger(tr("timebase", "name"),
                                                                       tr("The number of minutes between records"),
                                                                       tr("This is the number of minutes expected between records."),
                                                                       tr("5 minutes"));
    i->setMinimum(1);
    options.add("timebase", i);

    LineIngressWrapper::addOptions(options, true);
    return options;
}

ComponentOptions AcquireMageeAethalometer162131Component::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    ComponentOptionSingleCalibration *cal =
            new ComponentOptionSingleCalibration(tr("cal-q", "name"),
                                                 tr("The calibration to apply to the flow"),
                                                 tr("This is the calibration applied to the reported flow rate.  "
                                                    ""), "");
    cal->setAllowInvalid(false);
    cal->setAllowConstant(true);
    options.add("cal-q", cal);

    return options;
}

QList<ComponentExample> AcquireMageeAethalometer162131Component::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples(true));
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireMageeAethalometer162131Component::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireMageeAethalometer162131Component::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options, true);
}

QList<ComponentExample> AcquireMageeAethalometer162131Component::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options,
                                     tr("Convert data using the default high sensitivity spot size.")));

    options = getBaseOptions();
    (qobject_cast<ComponentOptionSingleDouble *>(options.get("spot")))->set(42.12);
    examples.append(ComponentExample(options, tr("Explicitly set the sample spot size.")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireMageeAethalometer162131Component::createAcquisitionPassive(
        const ComponentOptions &options,
        const std::string &loggingContext)
{
    return std::unique_ptr<AcquisitionInterface>(
            new AcquireMageeAethalometer162131(options, loggingContext));
}

std::unique_ptr<
        AcquisitionInterface> AcquireMageeAethalometer162131Component::createAcquisitionPassive(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    return std::unique_ptr<AcquisitionInterface>(
            new AcquireMageeAethalometer162131(config, loggingContext));
}

std::unique_ptr<
        AcquisitionInterface> AcquireMageeAethalometer162131Component::createAcquisitionAutoprobe(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireMageeAethalometer162131>
            i(new AcquireMageeAethalometer162131(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<
        AcquisitionInterface> AcquireMageeAethalometer162131Component::createAcquisitionInteractive(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireMageeAethalometer162131>
            i(new AcquireMageeAethalometer162131(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
