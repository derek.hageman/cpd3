/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    double reportRate;
    double nextUnpolled;
    double accumulatedTime;

    bool compressed;

    int sn;

    double flow;
    double bypassFraction;

    int channels;
    bool ae21mode;
    double Ip[7];
    double Ipz[7];
    double If[7];
    double Ifz[7];

    double dIp[7];
    double dIpz[7];
    double dIf[7];
    double dIfz[7];

    double In0[7];

    double wavelengths[7];
    double efficiency[7];

    double area;

    static double intensity(const double I[7], const double dI[7], double time, int channel)
    {
        return I[channel] + dI[channel] * time;
    }

    double sampleSignal(double time, int channel) const
    {
        return intensity(Ip, dIp, time, channel);
    }

    double sampleZero(double time, int channel) const
    {
        return intensity(Ipz, dIpz, time, channel);
    }

    double referenceSignal(double time, int channel) const
    {
        return intensity(If, dIf, time, channel);
    }

    double referenceZero(double time, int channel) const
    {
        return intensity(Ifz, dIfz, time, channel);
    }

    double sampleIntensity(double time, int channel) const
    {
        return sampleSignal(time, channel) - sampleZero(time, channel);
    }

    double referenceIntensity(double time, int channel) const
    {
        return referenceSignal(time, channel) - referenceZero(time, channel);
    }

    double normalizedIntensity(double time, int channel) const
    {
        return sampleIntensity(time, channel);
    }

    double transmittance(double time, int channel) const
    {
        return normalizedIntensity(time, channel) / In0[channel];
    }

    double attenuation(double time, int channel) const
    {
        return -100.0 * log(transmittance(time, channel));
    }

    double dQt(double dT = 0.0) const
    {
        if (dT <= 0.0)
            dT = reportRate;
        return flow * bypassFraction * dT / 60000.0;
    }

    double absorption(double startTime, double endTime, int channel) const
    {
        return log(transmittance(startTime, channel) / transmittance(endTime, channel)) *
                (area / dQt(endTime - startTime));
    }

    double ebc(double startTime, double endTime, int channel) const
    {
        return absorption(startTime, endTime, channel) / efficiency[channel];
    }

    ModelInstrument(int wl = 7, bool AE21 = false) : reportRate(60.0),
                                                     nextUnpolled(reportRate),
                                                     accumulatedTime(0.0),
                                                     sn(-1)
    {
        flow = 1.5;
        bypassFraction = 1.0;
        channels = wl;
        ae21mode = AE21;
        compressed = false;

        area = 50.0;

        wavelengths[0] = 370.0;
        wavelengths[1] = 470.0;
        wavelengths[2] = 520.0;
        wavelengths[3] = 590.0;
        wavelengths[4] = 660.0;
        wavelengths[5] = 880.0;
        wavelengths[6] = 950.0;

        switch (wl) {
        case 1:
            wavelengths[0] = 880.0;
            break;
        case 2:
            wavelengths[0] = 880.0;
            wavelengths[1] = 370.0;
            break;
        default:
            break;
        }


        for (int i = 0; i < 7; i++) {
            Ip[i] = 0.5 + (double) i * 0.25;
            If[i] = 0.3 + (double) i * 0.1;
            Ipz[i] = 0.05 + (double) i * 0.025;
            Ifz[i] = 0.03 + (double) i * 0.01;

            dIp[i] = -0.0001 * (double) (i + 1);
            dIpz[i] = 0.0;
            dIf[i] = -0.0002 * (double) (i + 1);
            dIfz[i] = 0.0;

            In0[i] = normalizedIntensity(0.0, i);

            efficiency[i] = 14625.0 / wavelengths[i];
        }
    }

    static QByteArray generateTime(double time)
    {
        QDateTime dt(Time::toDateTime(time));

        QByteArray result;
        result.append('\"');
        result.append(QByteArray::number(dt.date().day()).rightJustified(2, '0'));
        result.append('-');
        switch (dt.date().month()) {
        case 1:
            result.append("jan");
            break;
        case 2:
            result.append("feb");
            break;
        case 3:
            result.append("mar");
            break;
        case 4:
            result.append("apr");
            break;
        case 5:
            result.append("may");
            break;
        case 6:
            result.append("jun");
            break;
        case 7:
            result.append("jul");
            break;
        case 8:
            result.append("aug");
            break;
        case 9:
            result.append("sep");
            break;
        case 10:
            result.append("oct");
            break;
        case 11:
            result.append("nov");
            break;
        case 12:
            result.append("dec");
            break;
        default:
            Q_ASSERT(false);
            break;
        }
        result.append('-');
        result.append(QByteArray::number(dt.date().year() % 100).rightJustified(2, '0'));
        result.append("\",\"");
        result.append(QByteArray::number(dt.time().hour()).rightJustified(2, '0'));
        result.append(':');
        result.append(QByteArray::number(dt.time().minute()).rightJustified(2, '0'));
        result.append('\"');
        return result;
    }

    QByteArray generateSignals(double time, int channel) const
    {
        QByteArray result;
        result.append(QByteArray::number(sampleZero(time, channel), 'f'));
        result.append(',');
        result.append(QByteArray::number(sampleSignal(time, channel), 'f'));
        result.append(',');
        result.append(QByteArray::number(referenceZero(time, channel), 'f'));
        result.append(',');
        result.append(QByteArray::number(referenceSignal(time, channel), 'f'));
        return result;
    }

    QByteArray generateAttenuation(double time, int channel) const
    {
        return QByteArray::number(attenuation(time, channel));
    }

    static QByteArray compressDecimal(int value, int digits)
    {
        static const char radix[] = "abcdefghijklmnopqrstuvwxyzBCDEFGHIJKLMNOPQRSTUVWXY";
        QByteArray result(digits, 'a');
        for (int i = 0; i < digits; i++) {
            result[(digits - 1) - i] = radix[value % (sizeof(radix) - 1)];
            value /= (sizeof(radix) - 1);
        }
        return result;
    }

    QByteArray generateCompressed(double time, int channel)
    {
        QByteArray result;
        result.append(compressDecimal(qRound((attenuation(time, channel) + 10.0) * 1000.0), 3));
        result.append(compressDecimal(qRound((sampleZero(time, channel)) * 10000.0), 2));
        result.append(compressDecimal(qRound((referenceZero(time, channel)) * 10000.0), 2));
        result.append(compressDecimal(qRound((sampleSignal(time, channel) + 1.0) * 10000.0), 3));
        result.append(compressDecimal(qRound((referenceSignal(time, channel) + 1.0) * 10000.0), 3));
        result.append(compressDecimal(qRound((bypassFraction * 50.0) - 1.0), 1));
        result.append(compressDecimal(qRound((flow) * 10.0), 2));
        return result;
    }

    void advance(double seconds)
    {
        accumulatedTime += seconds;
        while (accumulatedTime >= nextUnpolled) {
            double startTime = nextUnpolled;
            double endTime = nextUnpolled + reportRate;
            nextUnpolled = endTime;

            if (sn > 0) {
                outgoing.append(QByteArray::number(sn));
                outgoing.append(',');
            }

            outgoing.append(generateTime(startTime));
            for (int i = 0; i < channels; i++) {
                outgoing.append(',');
                outgoing.append(
                        QByteArray::number(ebc(startTime, endTime, i), 'f').rightJustified(6, ' '));
            }

            if (compressed) {
                for (int i = 0; i < channels; i++) {
                    outgoing.append(',');
                    outgoing.append(generateCompressed(endTime, i));
                }
                outgoing.append('\r');
                continue;
            }

            outgoing.append(',');
            outgoing.append(QByteArray::number(flow, 'f'));

            if (ae21mode) {
                outgoing.append(',');
                outgoing.append(QByteArray::number(bypassFraction, 'f', 2));
                outgoing.append(',');
                outgoing.append(generateSignals(endTime, 0));
                outgoing.append(',');
                outgoing.append(generateAttenuation(endTime, 0));
                outgoing.append(',');
                outgoing.append(generateSignals(endTime, 1));
                outgoing.append(',');
                outgoing.append(generateAttenuation(endTime, 1));
            } else {
                for (int i = 0; i < channels; i++) {
                    outgoing.append(',');
                    outgoing.append(generateSignals(endTime, i));
                    outgoing.append(',');
                    outgoing.append(QByteArray::number(bypassFraction, 'f', 2));
                    outgoing.append(',');
                    outgoing.append(generateAttenuation(endTime, i));
                }
            }

            outgoing.append('\r');
        }
    }
};

class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream, const ModelInstrument &model,
                   double time = FP::undefined(),
                   const SequenceName::Component &suffix = {})
    {
        if (!stream.hasMeta("F1" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Q" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Qt" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("L" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("PCT" + suffix, Variant::Root(), QString(), time))
            return false;
        for (int i = 0; i < model.channels; i++) {
            if (!stream.hasMeta("X" + std::to_string(i + 1) + suffix,
                                Variant::Root(model.wavelengths[i]), "^Wavelength", time))
                return false;
            if (!stream.hasMeta("Ba" + std::to_string(i + 1) + suffix,
                                Variant::Root(model.wavelengths[i]), "^Wavelength", time))
                return false;
            if (!stream.hasMeta("Ir" + std::to_string(i + 1) + suffix,
                                Variant::Root(model.wavelengths[i]), "^Wavelength", time))
                return false;
            if (!stream.hasMeta("Ip" + std::to_string(i + 1) + suffix,
                                Variant::Root(model.wavelengths[i]), "^Wavelength", time))
                return false;
            if (!stream.hasMeta("If" + std::to_string(i + 1) + suffix,
                                Variant::Root(model.wavelengths[i]), "^Wavelength", time))
                return false;
        }
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, const ModelInstrument &model,
                           double time = FP::undefined())
    {
        for (int i = 0; i < model.channels; i++) {
            if (!stream.hasMeta("ZATN" + std::to_string(i + 1), Variant::Root(model.wavelengths[i]),
                                "^Wavelength", time))
                return false;
            if (!stream.hasMeta("ZIp" + std::to_string(i + 1), Variant::Root(model.wavelengths[i]),
                                "^Wavelength", time))
                return false;
            if (!stream.hasMeta("ZIpz" + std::to_string(i + 1), Variant::Root(model.wavelengths[i]),
                                "^Wavelength", time))
                return false;
            if (!stream.hasMeta("ZIf" + std::to_string(i + 1), Variant::Root(model.wavelengths[i]),
                                "^Wavelength", time))
                return false;
            if (!stream.hasMeta("ZIfz" + std::to_string(i + 1), Variant::Root(model.wavelengths[i]),
                                "^Wavelength", time))
                return false;
        }
        return true;
    }

    bool checkValues(StreamCapture &stream, const ModelInstrument &model,
                     double instrumentTime,
                     double logTimeOffset = FP::undefined(),
                     const SequenceName::Component &suffix = {})
    {
        if (FP::defined(logTimeOffset))
            logTimeOffset += instrumentTime;
        if (!stream.hasAnyMatchingValue("Q" + suffix, Variant::Root(model.flow), logTimeOffset))
            return false;
        if (!stream.hasAnyMatchingValue("Qt" + suffix, Variant::Root(), logTimeOffset))
            return false;
        if (!stream.hasAnyMatchingValue("L" + suffix, Variant::Root(), logTimeOffset))
            return false;
        if (!stream.hasAnyMatchingValue("PCT" + suffix, Variant::Root(model.bypassFraction * 100.0),
                                        logTimeOffset))
            return false;
        if (!stream.hasAnyMatchingValue("F1" + suffix, Variant::Root(), logTimeOffset))
            return false;
        for (int i = 0; i < model.channels; i++) {
            if (!stream.hasAnyMatchingValue("Ba" + std::to_string(i + 1) + suffix, Variant::Root(
                    model.absorption(instrumentTime, instrumentTime + model.reportRate, i)),
                                            logTimeOffset, QString(), 1E-3))
                return false;
            if (!stream.hasAnyMatchingValue("X" + std::to_string(i + 1) + suffix, Variant::Root(
                    model.ebc(instrumentTime, instrumentTime + model.reportRate, i)), logTimeOffset,
                                            QString(), 1E-3))
                return false;
            if (!stream.hasAnyMatchingValue("Ir" + std::to_string(i + 1) + suffix, Variant::Root(
                    model.transmittance(instrumentTime + model.reportRate, i)), logTimeOffset,
                                            QString(), 1E-3))
                return false;
            if (!stream.hasAnyMatchingValue("If" + std::to_string(i + 1) + suffix, Variant::Root(
                    model.referenceIntensity(instrumentTime + model.reportRate, i)), logTimeOffset,
                                            QString(), 1E-3))
                return false;
            if (!stream.hasAnyMatchingValue("Ip" + std::to_string(i + 1) + suffix, Variant::Root(
                    model.sampleIntensity(instrumentTime + model.reportRate, i)), logTimeOffset,
                                            QString(), 1E-3))
                return false;
        }
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream, const ModelInstrument &model,
                             double instrumentTime = FP::undefined(),
                             double time = FP::undefined())
    {
        for (int i = 0; i < model.channels; i++) {
            if (!stream.hasAnyMatchingValue("ZATN" + std::to_string(i + 1),
                                            Variant::Root(model.attenuation(instrumentTime, i)),
                                            time, QString(), 1E-3))
                return false;
            if (!stream.hasAnyMatchingValue("ZIp" + std::to_string(i + 1),
                                            Variant::Root(model.sampleSignal(instrumentTime, i)),
                                            time, QString(), 1E-3))
                return false;
            if (!stream.hasAnyMatchingValue("ZIpz" + std::to_string(i + 1),
                                            Variant::Root(model.sampleZero(instrumentTime, i)),
                                            time, QString(), 1E-3))
                return false;
            if (!stream.hasAnyMatchingValue("ZIf" + std::to_string(i + 1),
                                            Variant::Root(model.referenceSignal(instrumentTime, i)),
                                            time, QString(), 1E-3))
                return false;
            if (!stream.hasAnyMatchingValue("ZIfz" + std::to_string(i + 1),
                                            Variant::Root(model.referenceZero(instrumentTime, i)),
                                            time, QString(), 1E-3))
                return false;
        }
        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_magee_aethalometer162131"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_magee_aethalometer162131"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();

        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("t")));
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("p")));
        QVERIFY(qobject_cast<ComponentOptionSingleDouble *>(options.get("spot")));
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["SampleTemperature"].setDouble(0.0);
        cv["SamplePressure"].setDouble(1013.25);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(10.0);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }
        QVERIFY(control.time() < 180.0);

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(10.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument, 180.0));
        QVERIFY(checkValues(realtime, instrument, 180.0));
        QVERIFY(checkRealtimeValues(realtime, instrument, 180.0));

    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["SampleTemperature"].setDouble(0.0);
        cv["SamplePressure"].setDouble(1013.25);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument(2, true);
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(10.0);
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 180.0);

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(10.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument, 240));
        QVERIFY(checkValues(realtime, instrument, 240));
        QVERIFY(checkRealtimeValues(realtime, instrument, 240));

    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["SampleTemperature"].setDouble(0.0);
        cv["SamplePressure"].setDouble(1013.25);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument(1);
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 200; i++) {
            control.advance(10.0);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument, 240));
        QVERIFY(checkValues(realtime, instrument, 240));
        QVERIFY(checkRealtimeValues(realtime, instrument, 240));

    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["SampleTemperature"].setDouble(0.0);
        cv["SamplePressure"].setDouble(1013.25);
        cv["Wavelengths"].setInt64(7);
        cv["AbsorptionEfficiency"].setDouble(14625.0);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 200; i++) {
            control.advance(10.0);
            QTest::qSleep(50);
        }

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument, 240));
        QVERIFY(checkValues(realtime, instrument, 240));
        QVERIFY(checkRealtimeValues(realtime, instrument, 240));

    }

    void instrumentTimeConvert()
    {
        ComponentOptions options;
        options = ingress->getOptions();

        qobject_cast<ComponentOptionSingleDouble *>(options.get("t"))->set(0.0);
        qobject_cast<ComponentOptionSingleDouble *>(options.get("p"))->set(1013.25);
        std::unique_ptr<ExternalConverter> input(ingress->createDataIngress(options));
        QVERIFY(input.get());
        input->start();

        ModelInstrument instrument;
        instrument.advance(960.0);

        StreamCapture logging;
        input->setEgress(&logging);
        input->incomingData(instrument.outgoing);
        input->endData();
        input->wait(30000);
        input.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkMeta(logging, instrument, FP::undefined(), "_A11"));
        QVERIFY(checkValues(logging, instrument, 600, FP::undefined(), "_A11"));
    }

    void serialNumberPassive()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["SampleTemperature"].setDouble(0.0);
        cv["SamplePressure"].setDouble(1013.25);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.sn = 1234;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 200; i++) {
            control.advance(10.0);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument, 240));
        QVERIFY(checkValues(realtime, instrument, 240));
        QVERIFY(checkRealtimeValues(realtime, instrument, 240));

    }

    void compressed()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["SampleTemperature"].setDouble(0.0);
        cv["SamplePressure"].setDouble(1013.25);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.compressed = true;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(10.0);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }
        QVERIFY(control.time() < 180.0);

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(10.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument, 180.0));
        QVERIFY(checkValues(realtime, instrument, 180.0));
        QVERIFY(checkRealtimeValues(realtime, instrument, 180.0));

    }

    void timeout()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["SampleTemperature"].setDouble(0.0);
        cv["SamplePressure"].setDouble(1013.25);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument(2, true);
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(10.0);
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 180.0);

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        instrument.nextUnpolled = control.time() + 86400;

        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getGeneralStatus() == AcquisitionInterface::GeneralStatus::Normal;) {
            control.advance(10.0);
            QTest::qSleep(50);
        }
        QCOMPARE(interface->getGeneralStatus(),
                 AcquisitionInterface::GeneralStatus::NoCommunications);

        instrument.nextUnpolled = control.time();
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getGeneralStatus() != AcquisitionInterface::GeneralStatus::Normal;) {
            control.advance(10.0);
            QTest::qSleep(50);
        }
        QCOMPARE(interface->getGeneralStatus(), AcquisitionInterface::GeneralStatus::Normal);

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

    }

    void ae22()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["SampleTemperature"].setDouble(0.0);
        cv["SamplePressure"].setDouble(1013.25);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument(2, false);
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 200; i++) {
            control.advance(10.0);
            QTest::qSleep(50);
        }

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument, 240));
        QVERIFY(checkValues(realtime, instrument, 240));
        QVERIFY(checkRealtimeValues(realtime, instrument, 240));

    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
