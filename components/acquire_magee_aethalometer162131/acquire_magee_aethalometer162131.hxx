/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIREMAGEEAETHALOMETER162131_H
#define ACQUIREMAGEEAETHALOMETER162131_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "acquisition/spancheck.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"

enum BCUnits {
    BC_unknown, BC_ugm3, BC_ngm3
};

class AcquireMageeAethalometer162131 : public CPD3::Acquisition::FramedInstrument {
    double lastRecordTime;
    double lastStartTime;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum ResponseState {
        /* Waiting for enough valid records to confirm autoprobe success */
        RESP_AUTOPROBE_WAIT,

        /* Waiting for a valid record to start acquisition */
        RESP_WAIT,

        /* Acquiring data in passive mode */
        RESP_RUN,

        /* Same as wait, but discard the first timeout */
        RESP_INITIALIZE,

        /* Same as initialize but autoprobing */
        RESP_AUTOPROBE_INITIALIZE,
    };
    ResponseState responseState;
    QVector<double> effectiveWavelengths;

    friend class Configuration;

    class Configuration {
        double start;
        double end;
    public:
        double area;
        double reportInterval;
        bool ignoreFraction;
        BCUnits bcUnits;

        double filterChangeDetectThreshold;

        double meanRatio;
        QHash<double, double> absorptionEfficiency;

        CPD3::Calibration flowCalibration;
        double sampleTemperature;
        double samplePressure;

        QVector<double> wavelengths;

        bool strictMode;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under,
                      const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    void setDefaultInvalid();

    CPD3::Data::Variant::Root instrumentMeta;

    QHash<QByteArray, int> monthLookup;

    double priorVolume;
    QVector<double> priorIr;
    BCUnits lastBCUnits;
    bool lastBCUnitsUncertain;

    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;

    void logValue(double startTime,
                  double endTime,
                  CPD3::Data::SequenceName::Component name,
                  CPD3::Data::Variant::Root &&value);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root &&value);

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer invalidateLogMetadata(double time, int from, int to) const;

    CPD3::Data::SequenceValue::Transfer invalidateRealtimeMetadata(double time,
                                                                   int from,
                                                                   int to) const;

    int processRecord(const CPD3::Util::ByteView &line, double &frameTime);

    void configurationChanged();

    void configurationAdvance(double frameTime);

public:
    AcquireMageeAethalometer162131(const CPD3::Data::ValueSegment::Transfer &config,
                                   const std::string &loggingContext);

    AcquireMageeAethalometer162131(const CPD3::ComponentOptions &options,
                                   const std::string &loggingContext);

    virtual ~AcquireMageeAethalometer162131();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    virtual CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus();

    virtual CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus();

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    virtual void serializeState(QDataStream &stream);

    virtual void deserializeState(QDataStream &stream);

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    virtual void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingInstrumentTimeout(double time);

    virtual void discardDataCompleted(double time);
};

class AcquireMageeAethalometer162131Component
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_magee_aethalometer162131"
                              FILE
                              "acquire_magee_aethalometer162131.json")

    CPD3::ComponentOptions getBaseOptions();

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
