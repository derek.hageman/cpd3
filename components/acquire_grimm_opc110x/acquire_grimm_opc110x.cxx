/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QRegularExpression>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_grimm_opc110x.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

AcquireGrimmOPC110x::Configuration::Configuration() : start(FP::undefined()),
                                                      end(FP::undefined()),
                                                      useMeasuredTime(false),
                                                      strictMode(false),
                                                      model(),
                                                      firmware()
{ }

AcquireGrimmOPC110x::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          useMeasuredTime(other.useMeasuredTime),
          strictMode(other.strictMode),
          model(other.model),
          firmware(other.firmware)
{ }

void AcquireGrimmOPC110x::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("Grimm");
    instrumentMeta["Model"].setString("110x");

    memset(streamAge, 0, sizeof(streamAge));
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
    }

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    forceRealtimeStateEmit = true;

    instrumentGFScale = 1.0 / 20.0;
    instrumentFlowCorrection = 1.2;

    currentBinnedMasses = false;
    previousBinnedMasses = false;

    baseTime = FP::undefined();
    binInsertOffset = 0;
    minuteIndex = 0;

    lastSampleFlow = 1.2;
    previousVolume = FP::undefined();
    previousNumberOfBins = -1;
}

AcquireGrimmOPC110x::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s), end(e), useMeasuredTime(false), strictMode(false), model(), firmware()
{
    setFromSegment(other);
}

AcquireGrimmOPC110x::Configuration::Configuration(const Configuration &under,
                                                  const ValueSegment &over,
                                                  double s,
                                                  double e) : start(s),
                                                              end(e),
                                                              useMeasuredTime(
                                                                      under.useMeasuredTime),
                                                              strictMode(under.strictMode),
                                                              model(under.model),
                                                              firmware(under.firmware)
{
    setFromSegment(over);
}

void AcquireGrimmOPC110x::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["UseMeasuredTime"].exists())
        useMeasuredTime = config["UseMeasuredTime"].toBool();
    if (config["StrictMode"].exists())
        strictMode = config["StrictMode"].toBool();
    if (config["Model"].exists())
        model = config["Model"].toQString();
    if (config["Firmware"].exists())
        firmware = config["Firmware"].toQString();
}

AcquireGrimmOPC110x::AcquireGrimmOPC110x(const ValueSegment::Transfer &configData,
                                         const std::string &loggingContext) : FramedInstrument(
        "grimm110x", loggingContext),
                                                                              autoprobeStatus(
                                                                                      AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                              responseState(
                                                                                      RESP_PASSIVE_WAIT),
                                                                              autoprobeValidRecords(
                                                                                      0),
                                                                              responseIndex(0),
                                                                              lastResponseCode(0),
                                                                              loggingMux(
                                                                                      LogStream_TOTAL)
{
    setDefaultInvalid();
    config.append(Configuration());

    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }

    Q_ASSERT(!config.isEmpty());
    if (config.first().model.isEmpty())
        setFromModel("1.109");
    else
        setFromModel(config.first().model);
    if (!config.first().firmware.isEmpty())
        setFromFirmware(config.first().firmware);
}

void AcquireGrimmOPC110x::setToAutoprobe()
{ responseState = RESP_AUTOPROBE_PASSIVE_INITIALIZE; }

void AcquireGrimmOPC110x::setToInteractive()
{ responseState = RESP_INTERACTIVE_INITIALIZE; }


ComponentOptions AcquireGrimmOPC110xComponent::getBaseOptions()
{
    ComponentOptions options;

    return options;
}

AcquireGrimmOPC110x::AcquireGrimmOPC110x(const ComponentOptions &options,
                                         const std::string &loggingContext) : FramedInstrument(
        "grimm110x", loggingContext),
                                                                              autoprobeStatus(
                                                                                      AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                              responseState(
                                                                                      RESP_PASSIVE_WAIT),
                                                                              autoprobeValidRecords(
                                                                                      0),
                                                                              responseIndex(0),
                                                                              lastResponseCode(0),
                                                                              loggingMux(
                                                                                      LogStream_TOTAL)
{
    Q_UNUSED(options);

    setDefaultInvalid();
    config.append(Configuration());
    if (config.first().model.isEmpty())
        setFromModel("1.109");
    else
        setFromModel(config.first().model);
    if (!config.first().firmware.isEmpty())
        setFromFirmware(config.first().firmware);
}

AcquireGrimmOPC110x::~AcquireGrimmOPC110x()
{
}

void AcquireGrimmOPC110x::invalidateLogValues(double frameTime)
{
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
        streamAge[i] = 0;

        if (loggingEgress != NULL)
            loggingMux.advance(i, frameTime, loggingEgress);
    }
    loggingMux.clear();
    loggingLost(frameTime);
}

SequenceValue::Transfer AcquireGrimmOPC110x::buildLogMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_grimm_opc110x");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);

    result.emplace_back(SequenceName({}, "raw_meta", "N"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0");
    result.back().write().metadataReal("Units").setString("cm\xE2\x81\xBB³");
    result.back().write().metadataReal("Description").setString("Total concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Concentration"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);

    if (currentBinnedMasses) {
        result.emplace_back(SequenceName({}, "raw_meta", "X", {"pm10"}), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Units").setString("\xCE\xBCg/m\xC2\xB3");
        result.back().write().metadataReal("Format").setString("0000.0");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Mass concentration instrument calculations");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);

        result.emplace_back(SequenceName({}, "raw_meta", "X", {"pm25"}), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Units").setString("\xCE\xBCg/m\xC2\xB3");
        result.back().write().metadataReal("Format").setString("0000.0");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Mass concentration instrument calculations");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);

        result.emplace_back(SequenceName({}, "raw_meta", "X", {"pm1"}), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Units").setString("\xCE\xBCg/m\xC2\xB3");
        result.back().write().metadataReal("Format").setString("0000.0");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Mass concentration instrument calculations");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    } else if (previousBinnedMasses) {
        result.emplace_back(SequenceName({}, "raw_meta", "X", {"pm10"}), Variant::Root(), time,
                            FP::undefined());
        result.emplace_back(SequenceName({}, "raw_meta", "X", {"pm25"}), Variant::Root(), time,
                            FP::undefined());
        result.emplace_back(SequenceName({}, "raw_meta", "X", {"pm1"}), Variant::Root(), time,
                            FP::undefined());
    }

    result.emplace_back(SequenceName({}, "raw_meta", "Q"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Sample flow");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Flow"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.emplace_back(SequenceName({}, "raw_meta", "Nb"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataArray("Count").setInteger(currentSizes.read().toArray().size());
    result.back().write().metadataArray("Children").metadataReal("Format").setString("00000.0");
    result.back()
          .write()
          .metadataArray("Children")
          .metadataReal("Units")
          .setString("cm\xE2\x81\xBB³");
    result.back().write().metadataArray("Description").setString("Bin concentration");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Realtime").hash("Page").setInt64(1);
    result.back()
          .write()
          .metadataArray("Realtime")
          .hash("Name")
          .setString(QObject::tr("Concentration"));
    result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInt64(1);

    result.emplace_back(SequenceName({}, "raw_meta", "Ns"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataArray("Count").setInteger(currentSizes.read().toArray().size());
    result.back().write().metadataArray("Children").metadataReal("Format").setString("00.00");
    result.back().write().metadataArray("Units").setString("\xCE\xBCm");
    result.back().write().metadataArray("Description").setString("Bin center diameter");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Realtime").hash("Page").setInt64(1);
    result.back().write().metadataArray("Realtime").hash("Name").setString(QObject::tr("Diameter"));
    result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInt64(1);

    return result;
}

SequenceValue::Transfer AcquireGrimmOPC110x::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_grimm_opc110x");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);

    result.emplace_back(SequenceName({}, "raw_meta", "PCT1"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Battery percentage");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Battery"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);

    result.emplace_back(SequenceName({}, "raw_meta", "PCT2"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Pump motor power percentage");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Pump"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);

    /* Need this for the layout, which would otherwise flatten the flavors */
    if (currentBinnedMasses) {
        result.emplace_back(SequenceName({}, "raw_meta", "ZXPM10"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Units").setString("\xCE\xBCg/m\xC2\xB3");
        result.back().write().metadataReal("Format").setString("0000.0");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Mass concentration instrument calculations");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Name")
              .setString(QObject::tr("Mass PM10"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);

        result.emplace_back(SequenceName({}, "raw_meta", "ZXPM25"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Units").setString("\xCE\xBCg/m\xC2\xB3");
        result.back().write().metadataReal("Format").setString("0000.0");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Mass concentration instrument calculations");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Name")
              .setString(QObject::tr("Mass PM2.5"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);

        result.emplace_back(SequenceName({}, "raw_meta", "ZXPM1"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Units").setString("\xCE\xBCg/m\xC2\xB3");
        result.back().write().metadataReal("Format").setString("0000.0");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Mass concentration instrument calculations");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Name")
              .setString(QObject::tr("Mass PM1"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);
    } else if (previousBinnedMasses) {
        result.emplace_back(SequenceName({}, "raw_meta", "ZXPM10"), Variant::Root(), time,
                            FP::undefined());
        result.emplace_back(SequenceName({}, "raw_meta", "ZXPM25"), Variant::Root(), time,
                            FP::undefined());
        result.emplace_back(SequenceName({}, "raw_meta", "ZXPM1"), Variant::Root(), time,
                            FP::undefined());
    }


    result.emplace_back(SequenceName({}, "raw_meta", "ZN"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataArray("Count").setInteger(currentSizes.read().toArray().size());
    result.back().write().metadataArray("Children").metadataReal("Format").setString("000000");
    result.back().write().metadataArray("Description").setString("Bin counts");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Realtime").hash("Page").setInt64(1);
    result.back()
          .write()
          .metadataArray("Realtime")
          .hash("Name")
          .setString(QObject::tr("Cumulative"));
    result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInt64(1);


    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Realtime").hash("Name").setString(QString());
    result.back().write().metadataString("Realtime").hash("Units").setString(QString());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back().write().metadataString("Realtime").hash("RowOrder").setInt64(5);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Run")
          .setString(QObject::tr("", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidRecord")
          .setString(QObject::tr("NO COMMS: Invalid record"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidResponse")
          .setString(QObject::tr("NO COMMS: Invalid response"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Timeout")
          .setString(QObject::tr("NO COMMS: Timeout"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractive")
          .setString(QObject::tr("STARTING COMMS"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("StartInteractiveReadModel")
          .hash("StartInteractiveReadSerial")
          .setString(QObject::tr("STARTING COMMS: Reading serial and model numbers"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadSerial")
          .setString(QObject::tr("STARTING COMMS: Reading serial number"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadFirmwareVersion")
          .setString(QObject::tr("STARTING COMMS: Reading firmware version"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveStartMeasurement")
          .setString(QObject::tr("STARTING COMMS: Entering measurement mode"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadSizes")
          .setString(QObject::tr("STARTING COMMS: Reading bin sizes"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InteractiveQueryVolume")
          .setString(QObject::tr("STARTING COMMS: Reading current sample volume"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetCountMode")
          .setString(QObject::tr("STARTING COMMS: Changing to counts mode"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveDiscardFirst")
          .setString(QObject::tr("STARTING COMMS: Flushing status report"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveWaitAdvance")
          .setString(QObject::tr("STARTING COMMS: Waiting for record advance"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveStartReadFirstRecord")
          .setString(QObject::tr("STARTING COMMS: Waiting for first record"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("SelfTestFault")
          .setString(QObject::tr("Self test failure [code 128]"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("MemoryCardFault")
          .setString(QObject::tr("Memory card fault [code 64]"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("NozzleFault")
          .setString(QObject::tr("Nozzle fault (whirls >5%) [code 32]"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("BatteryDrained")
          .setString(QObject::tr("Battery drained (0%) [code 16]"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("BatteryLow")
          .setString(QObject::tr("Battery low (<10%) [code 8]"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("PumpCurrentHigh")
          .setString(QObject::tr("Pump current too high (>100%) [code 4]"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("FlowError")
          .setString(QObject::tr("Flow error (pump out of range) [code 3]"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("PumpLow")
          .setString(QObject::tr("Pump low, check filter (Imot < 20%) [code 2]"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("PumpHigh")
          .setString(QObject::tr("Pump high, check filter (Imot > 60%) [code 1]"));

    return result;
}

SequenceMatch::Composite AcquireGrimmOPC110x::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "ZSTATE");
    sel.append({}, {}, "X");
    return sel;
}

static double calculateConcentrationFromMass(double massConcentration, double gf)
{
    Q_UNUSED(massConcentration);
    Q_UNUSED(gf);
    return FP::undefined();
}

static double calculateBinCount(double current, double next)
{
    if (!FP::defined(current) || !FP::defined(next))
        return FP::undefined();
    if (current < next)
        return FP::undefined();
    return current - next;
}

static double calculateBinCount(double last)
{
    return last;
}

static double calculateConcentration(double count,
                                     double flow,
                                     double nominalFlow,
                                     double startTime,
                                     double endTime)
{
    if (!FP::defined(count) ||
            !FP::defined(flow) ||
            !FP::defined(startTime) ||
            !FP::defined(endTime))
        return FP::undefined();
    if (flow <= 0.0)
        return FP::undefined();
    double dT = endTime - startTime;
    if (dT <= 0.0)
        return FP::undefined();

    count /= flow * dT * (1000.0 / 60.0);
    count *= nominalFlow;
    return count;
}

static double calculateFlow(double previousTime, double previousVolume, double time, double volume)
{
    if (!FP::defined(previousTime) ||
            !FP::defined(previousVolume) ||
            !FP::defined(time) ||
            !FP::defined(volume))
        return FP::undefined();
    double dQt = volume - previousVolume;
    if (dQt <= 0.0)
        return FP::undefined();
    double dT = time - previousTime;
    if (dT <= 0.0)
        return FP::undefined();
    dQt *= 1000.0 * 60.0;
    dQt /= dT;
    return dQt;
}

static Variant::Root calculateFromCounts(const CPD3::Data::Variant::Read &counts,
                                         double flow,
                                         double nominalFlow,
                                         double startTime,
                                         double endTime)
{
    auto countsArray = counts.toArray();
    Variant::Root result;
    auto resultArray = result.write().toArray();
    for (std::size_t i = 0, max = countsArray.size(); i < max; i++) {
        double count;
        if (i == max - 1) {
            count = calculateBinCount(countsArray[i].toDouble());
        } else {
            count = calculateBinCount(countsArray[i].toDouble(), countsArray[i + 1].toDouble());
        }

        resultArray[i].setDouble(
                calculateConcentration(count, flow, nominalFlow, startTime, endTime));
    }
    return result;
}

static Variant::Root calculateFromMassConcentrations(const CPD3::Data::Variant::Read &masses,
                                                     double gf)
{
    Variant::Root result;
    auto resultArray = result.write().toArray();
    for (auto add : masses.toArray()) {
        resultArray.after_back().setDouble(calculateConcentrationFromMass(add.toDouble(), gf));
    }
    return result;
}

static Variant::Root calculateSizes(const CPD3::Data::Variant::Read &sizes)
{
    auto sizesArray = sizes.toArray();
    std::size_t max = sizesArray.size();
    if (max <= 0)
        return Variant::Root();
    Variant::Root result;
    auto resultArray = result.write().toArray();
    for (std::size_t i = 0, limit = max - 1; i < limit; i++) {
        double lower = sizesArray[i].toDouble();
        double upper = sizesArray[i + 1].toDouble();
        if (!FP::defined(lower) || !FP::defined(upper) || lower >= upper) {
            resultArray[i].setDouble(FP::undefined());
            continue;
        }
        resultArray[i].setDouble((lower + upper) * 0.5);
    }

    double first = sizesArray[max - 1].toDouble();
    if (max <= 1) {
        resultArray[0].setDouble(first);
    } else {
        double second = sizesArray[max - 2].toDouble();
        if (FP::defined(first) && FP::defined(second) && second < first) {
            resultArray[max - 1].setDouble(first + (first - second) * 0.5);
        } else {
            resultArray[max - 1].setDouble(FP::undefined());
        }
    }

    return result;
}

static int extractBins(std::deque<Util::ByteView> &fields, std::vector<double> &results)
{
    results.reserve(8);

    for (int i = 0; i < 8; i++) {
        if (fields.empty()) return 100 * (i + 1) + 1;
        bool ok = false;
        double value = fields.front().toQByteArray().trimmed().toDouble(&ok);
        fields.pop_front();
        if (!ok) return 100 * (i + 1) + 2;
        if (!FP::defined(value) || value < 0.0) return 100 * (i + 1) + 3;
        results.emplace_back(value);
    }

    return 0;
}

static bool isCaseEqual(char upper, char lower)
{
    Q_ASSERT(upper >= 'A' && upper <= 'Z');
    Q_ASSERT(lower >= 'a' && lower <= 'z');
    return upper == (lower - ('a' - 'A'));
}

static int convertMinuteIndex(char c)
{
    if (c < '0' || c > '9') return -1;
    return c - '0';
}

int AcquireGrimmOPC110x::getInsertOffset(bool upper) const
{
    if (upper) {
        return responseIndex != 0 ? 8 : 0;
    }
    return (responseIndex != 0 ? 8 : 0) + binInsertOffset;
}

void AcquireGrimmOPC110x::insertIntoValue(bool upper,
                                          const std::vector<double> &data,
                                          Variant::Root &target) const
{
    int offset = getInsertOffset(upper);
    if (offset == 0)
        target.write().setEmpty();
    Q_ASSERT(data.size() == 8);
    for (int i = 0, max = data.size(); i < max; i++) {
        target.write().array(i + offset).setReal(data.at(i));
    }
}

void AcquireGrimmOPC110x::emitMetadataIfNeeded(double frameTime)
{
    if (!FP::defined(frameTime))
        return;

    if (FP::defined(streamTime[LogStream_Metadata]) && streamTime[LogStream_Metadata] > frameTime)
        return;
    streamTime[LogStream_Metadata] = frameTime;

    if (!loggingEgress) {
        haveEmittedLogMeta = false;
        loggingMux.clear();
        memset(streamAge, 0, sizeof(streamAge));
        for (int i = 0; i < LogStream_TOTAL; i++) {
            streamTime[i] = FP::undefined();
        }
    } else {
        if (!haveEmittedLogMeta || (currentBinnedMasses != previousBinnedMasses)) {
            haveEmittedLogMeta = true;
            loggingMux.incoming(LogStream_Metadata, buildLogMeta(frameTime), loggingEgress);
        }
        loggingMux.advance(LogStream_Metadata, frameTime, loggingEgress);
    }
    if ((!haveEmittedRealtimeMeta || (currentBinnedMasses != previousBinnedMasses)) &&
            realtimeEgress) {
        haveEmittedRealtimeMeta = true;
        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }

    previousBinnedMasses = currentBinnedMasses;
}

void AcquireGrimmOPC110x::streamOverlapped(double frameTime, int streamID)
{
    Q_ASSERT(FP::defined(frameTime));

    SequenceValue F1(SequenceName({}, "raw", "F1"), Variant::Root(Variant::Type::Flags), frameTime,
                     frameTime + 1.0);
    if (realtimeEgress) {
        if (!loggingEgress)
            realtimeEgress->incomingData(std::move(F1));
        else
            realtimeEgress->incomingData(F1);
    }

    if (!loggingEgress) {
        invalidStreamTime();
        return;
    }

    emitMetadataIfNeeded(frameTime);

    streamAge[streamID] = 0;
    for (int i = LogStream_RecordBaseStart; i < LogStream_TOTAL; i++) {
        if (i == streamID)
            continue;
        if (streamAge[i] == 0) {
            loggingMux.advance(i, frameTime, loggingEgress);
            streamTime[i] = FP::undefined();
            continue;
        }

        streamAge[i] = 0;
    }

    if (FP::defined(streamTime[LogStream_State])) {
        F1.setStart(streamTime[LogStream_State]);
        F1.setEnd(frameTime);
        loggingMux.incoming(LogStream_State, std::move(F1), loggingEgress);
    }
    streamTime[LogStream_State] = frameTime;
    loggingMux.advance(LogStream_State, frameTime, loggingEgress);
}

void AcquireGrimmOPC110x::invalidStreamTime()
{
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
        streamAge[i] = 0;
    }
    loggingMux.clear();
}

void AcquireGrimmOPC110x::emitCounts(double frameTime, const Variant::Read &concentrations)
{
    int nConcentrations = concentrations.toArray().size();
    if (previousNumberOfBins != nConcentrations) {
        haveEmittedLogMeta = false;
        haveEmittedRealtimeMeta = false;
        previousNumberOfBins = nConcentrations;
    }

    if (FP::defined(frameTime)) {
        emitMetadataIfNeeded(frameTime);

        double startTime = streamTime[LogStream_Concentrations];
        double endTime = frameTime;
        if (endTime < startTime)
            return;

        Variant::Root raw(concentrations);
        remap("Nb", raw);

        double sum = FP::undefined();
        for (auto value : raw.read().toArray()) {
            double add = value.toDouble();
            if (!FP::defined(add))
                continue;
            if (!FP::defined(sum))
                sum = 0.0;
            sum += add;
        }

        SequenceValue Nb(SequenceName({}, "raw", "Nb"), std::move(raw), startTime, endTime);

        raw = Variant::Root(sum);
        remap("N", raw);
        SequenceValue N(SequenceName({}, "raw", "N"), std::move(raw), startTime, endTime);

        raw = calculateSizes(currentSizes);
        remap("Ns", raw);
        SequenceValue Ns(SequenceName({}, "raw", "Ns"), std::move(raw), startTime, endTime);


        if (loggingEgress && FP::defined(startTime)) {
            loggingMux.incoming(LogStream_Concentrations, Nb, loggingEgress);
            loggingMux.incoming(LogStream_Concentrations, Ns, loggingEgress);
            loggingMux.incoming(LogStream_Concentrations, N, loggingEgress);
            loggingMux.advance(LogStream_Concentrations, endTime, loggingEgress);
        }

        if (realtimeEgress) {
            Nb.setStart(frameTime);
            Nb.setEnd(frameTime + 10.0);
            realtimeEgress->incomingData(std::move(Nb));

            Ns.setStart(frameTime);
            Ns.setEnd(frameTime + 10.0);
            realtimeEgress->incomingData(std::move(Ns));

            N.setStart(frameTime);
            N.setEnd(frameTime + 10.0);
            realtimeEgress->incomingData(std::move(N));
        }

        streamAge[LogStream_Concentrations]++;
        streamTime[LogStream_Concentrations] = frameTime;
        if (streamAge[LogStream_Concentrations] > 10) {
            streamOverlapped(frameTime, LogStream_Concentrations);
        }
    } else {
        invalidStreamTime();
    }

    currentCounts.write().setEmpty();
    currentMasses.write().setEmpty();
}

void AcquireGrimmOPC110x::emitCountsDirect(double frameTime)
{
    auto raw = currentCounts;
    remap("ZN", raw);

    if (config.first().useMeasuredTime) {
        emitCounts(frameTime, calculateFromCounts(raw, lastSampleFlow, instrumentFlowCorrection,
                                                  streamTime[LogStream_Concentrations], frameTime));
    } else {
        emitCounts(frameTime,
                   calculateFromCounts(raw, lastSampleFlow, instrumentFlowCorrection, 0.0, 6.0));
    }

    if (realtimeEgress && FP::defined(frameTime)) {
        realtimeEgress->incomingData(
                SequenceValue({{}, "raw", "ZN"}, std::move(raw), frameTime, frameTime + 10.0));
    }
}

void AcquireGrimmOPC110x::issueVolumePoll(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    if (controlStream == NULL)
        return;
    if (responseState != RESP_INTERACTIVE_RUN)
        return;
    responseState = RESP_INTERACTIVE_QUERY_VOLUME;
    timeoutAt(frameTime + 3.0);
}

int AcquireGrimmOPC110x::processRecord(const Util::ByteView &line,
                                       double &frameTime,
                                       bool *advanceCheck)
{
    Q_ASSERT(!config.isEmpty());

    /* Ignore echo */
    if (line.size() == 1)
        return -1;

    if (line.size() < 3)
        return 1;

    auto fields = Util::as_deque(line.split(' '));
    for (auto f = fields.begin(); f != fields.end();) {
        if (f->empty()) {
            f = fields.erase(f);
            continue;
        }
        ++f;
    }
    if (fields.empty()) return 3;
    fields.pop_front();

    /* 180 can have a space before the ":" */
    if (fields.size() > 1 && (fields.front() == ":" || fields.front() == ";"))
        fields.pop_front();

    Util::ByteView field;
    bool ok = false;

    if (loggingEgress == NULL) {
        haveEmittedLogMeta = false;
        loggingMux.clear();
        memset(streamAge, 0, sizeof(streamAge));
        for (int i = 0; i < LogStream_TOTAL; i++) {
            streamTime[i] = FP::undefined();
        }
    }

    /* 180 "M" response line */
    if (line.string_start("Mean"))
        return -1;

    char responseCode = line[0];

    /* Handle 180 mass concentrations */
    if (responseCode == 'N' && line.size() >= 3 && line[2] == ',' && fields.size() == 3) {
        currentBinnedMasses = true;

        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root XPM10;
        {
            double v = field.parse_real(&ok);
            if (!ok) return 10;
            if (!FP::defined(v)) return 11;
            XPM10.write().setReal(v / 10.0);
        }
        remap("XPM10", XPM10);

        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root XPM25;
        {
            double v = field.parse_real(&ok);
            if (!ok) return 12;
            if (!FP::defined(v)) return 13;
            XPM25.write().setReal(v / 10.0);
        }
        remap("XPM25", XPM25);

        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root XPM1;
        {
            double v = field.parse_real(&ok);
            if (!ok) return 14;
            if (!FP::defined(v)) return 15;
            XPM1.write().setReal(v / 10.0);
        }
        remap("XPM1", XPM1);

        emitMetadataIfNeeded(frameTime);

        if (realtimeEgress && FP::defined(frameTime)) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "X", {"pm10"}}, XPM10, frameTime, frameTime + 10.0));
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "X", {"pm25"}}, XPM25, frameTime, frameTime + 10.0));
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "X", {"pm1"}}, XPM1, frameTime, frameTime + 10.0));

            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZXPM10"}, XPM10, frameTime, frameTime + 10.0));
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZXPM25"}, XPM25, frameTime, frameTime + 10.0));
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZXPM1"}, XPM1, frameTime, frameTime + 10.0));
        }

        if (FP::defined(frameTime)) {
            double startTime = streamTime[LogStream_BinnedMasses];
            double endTime = frameTime;
            if (loggingEgress && FP::defined(startTime) && endTime > startTime) {
                loggingMux.incoming(LogStream_BinnedMasses,
                                    SequenceValue({{}, "raw", "X", {"pm10"}}, std::move(XPM10),
                                                  startTime, endTime), loggingEgress);
                loggingMux.incoming(LogStream_BinnedMasses,
                                    SequenceValue({{}, "raw", "X", {"pm25"}}, std::move(XPM25),
                                                  startTime, endTime), loggingEgress);
                loggingMux.incoming(LogStream_BinnedMasses,
                                    SequenceValue({{}, "raw", "X", {"pm1"}}, std::move(XPM1),
                                                  startTime, endTime), loggingEgress);
            }

            loggingMux.advance(LogStream_BinnedMasses, endTime, loggingEgress);

            streamAge[LogStream_BinnedMasses]++;
            streamTime[LogStream_BinnedMasses] = frameTime;
            if (streamAge[LogStream_BinnedMasses] > 10) {
                streamOverlapped(frameTime, LogStream_BinnedMasses);
            }
        } else {
            invalidStreamTime();
        }

        return -1;
    }

    if (responseCode != lastResponseCode) {
        switch (responseCode) {
        case 'm':
        case 'j':
        case 'n':
        case 'c':
            switch (lastResponseCode) {
            case 'M':
            case 'J':
            case 'N':
            case 'C':
                /* Transition from the first to second, decide if we're on
                 * a 15 or 31 channel instrument based on if we
                 * saw two of the upper case records */
                if (isCaseEqual(lastResponseCode, responseCode) && responseIndex != 0) {
                    if (responseIndex > 1) {
                        binInsertOffset = 15;
                    } else {
                        binInsertOffset = 7;
                    }
                }
                break;
            default:
                /* Anything else doesn't actually make sense, so
                 * assume a 31 channel */
                binInsertOffset = 15;
                break;
            }
            break;

        case 'N':
        case 'M':
        case 'J':
        case 'C':
            switch (lastResponseCode) {
            case 'm':
            case 'j':
            case 'n':
            case 'c':
                /* Transition from the first to second, decide if we're on
                 * a 15 or 31 channel instrument based on if we
                 * saw two of the upper case records */
                if (isCaseEqual(responseCode, lastResponseCode) && responseIndex != 0) {
                    if (responseIndex > 1) {
                        binInsertOffset = 15;
                    } else {
                        binInsertOffset = 7;
                    }
                }
                break;
            default:
                /* Anything else doesn't actually make sense, so
                 * assume a 31 channel */
                binInsertOffset = 15;
                break;
            }
            break;

        case 'P':
        case 'K':
            minuteIndex = 0;
            break;

        default:
            break;
        }
        lastResponseCode = responseCode;
        responseIndex = 0;
    }

    switch (responseCode) {
    default:
        return 4;

    case 'P': {
        if (fields.empty()) return 100;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root year;
        {
            int raw = field.parse_i32(&ok, 10);
            if (!ok || raw < 0) return 101;
            if (field.size() == 2) {
                /* This is not "right", but I'm hoping that all these
                 * instruments will be replaced by 2070... */
                if (raw >= 70)
                    raw += 1900;
                else
                    raw += 2000;
                year.write().setInt64(raw);
            } else if (field.size() == 4) {
                if (raw < 1900 || raw > 2100) return 102;
                year.write().setInt64(raw);
            } else {
                if (raw < 100 || raw > 999) return 103;
                year.write().setInt64(2000 + raw);
            }
        }
        remap("YEAR", year);

        if (fields.empty()) return 104;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root month;
        {
            int raw = field.parse_i32(&ok, 10);
            if (!ok || raw < 1 || raw > 12) return 105;
            month.write().setInt64(raw);
        }
        remap("MONTH", month);

        if (fields.empty()) return 106;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root day;
        {
            int raw = field.parse_i32(&ok, 10);
            if (!ok || raw < 1 || raw > 31) return 107;
            day.write().setInt64(raw);
        }
        remap("DAY", day);

        if (fields.empty()) return 107;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root hour;
        {
            int raw = field.parse_i32(&ok);
            if (!ok || raw < 0 || raw > 23) return 108;
            hour.write().setInt64(raw);
        }
        remap("HOUR", hour);

        if (fields.empty()) return 109;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root minute;
        {
            int raw = field.parse_i32(&ok);
            if (!ok || raw < 0 || raw > 60) return 110;
            minute.write().setInt64(raw);
        }
        remap("MINUTE", minute);

        /* Location */
        if (fields.empty()) return 111;
        fields.pop_front();

        if (fields.empty()) return 112;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root gf(field.parse_real(&ok));
        if (!ok) return 113;
        if (!FP::defined(gf.read().toDouble()) || gf.read().toDouble() < 0.0) return 114;
        remap("GF", gf);
        lastGF = gf.read().toDouble() * instrumentGFScale;

        if (fields.empty()) return 115;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root errorCode(field.parse_i32(&ok, 10));
        if (!ok) return 116;
        if (!INTEGER::defined(errorCode.read().toInt64())) return 117;
        remap("ERROR", errorCode);

        if (fields.empty()) return 118;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root PCT1(field.parse_real(&ok));
        if (!ok) return 119;
        if (!FP::defined(PCT1.read().toDouble())) return 120;
        remap("PCT1", PCT1);

        if (fields.empty()) return 121;
        field = fields.front().string_trimmed();
        fields.pop_front();
        Variant::Root PCT2(field.parse_real(&ok));
        if (!ok) return 122;
        if (!FP::defined(PCT2.read().toDouble())) return 123;
        remap("PCT2", PCT2);

        /* Calibration and inputs */
        if (fields.size() < 5) return 124;
        fields.erase(fields.begin(), fields.begin() + 5);

        if (!FP::defined(frameTime)) {
            qint64 y = year.read().toInt64();
            if (!INTEGER::defined(y) || y < 1900 || y > 2100)
                return 125;
            qint64 mo = month.read().toInt64();
            if (!INTEGER::defined(mo) || mo < 1 || mo > 12)
                return 126;
            qint64 d = day.read().toInt64();
            if (!INTEGER::defined(d) || d < 1 || d > 31)
                return 127;
            QDate date((int) y, (int) mo, (int) d);
            if (!date.isValid())
                return 128;

            qint64 h = hour.read().toInt64();
            if (!INTEGER::defined(y) || h < 0 || h > 23)
                return 129;
            qint64 m = minute.read().toInt64();
            if (!INTEGER::defined(m) || m < 0 || m > 60)
                return 130;

            QTime time((int) h, (int) m, 0);
            if (!time.isValid())
                return 131;

            frameTime = baseTime = Time::fromDateTime(QDateTime(date, time, Qt::UTC));
            configurationAdvance(baseTime);
        } else if (advanceCheck == NULL) {
            timeoutAt(frameTime + 12.0);
        }
        emitMetadataIfNeeded(frameTime);

        Variant::Root state;
        if (INTEGER::defined(errorCode.read().toInt64()) && errorCode.read().toInt64() != 0) {
            switch (errorCode.read().toInteger()) {
            case 128:
                state.write().setString("SelfTestFault");
                break;
            case 64:
                state.write().setString("MemoryCardFault");
                break;
            case 32:
                state.write().setString("NozzleFault");
                break;
            case 16:
                state.write().setString("BatteryDrained");
                break;
            case 8:
                state.write().setString("BatteryLow");
                break;
            case 4:
                state.write().setString("PumpCurrentHigh");
                break;
            case 3:
                state.write().setString("FlowError");
                break;
            case 2:
                state.write().setString("PumpLow");
                break;
            case 1:
                state.write().setString("PumpHigh");
                break;
            default:
                state.write().setString(QString("Error code %1").arg(errorCode.read().toInt64()));
                break;
            }
        } else {
            state.write().setString("Run");
        }

        if (realtimeEgress != NULL && FP::defined(frameTime)) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "PCT1"}, PCT1, frameTime, frameTime + 60.0));
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "PCT2"}, PCT2, frameTime, frameTime + 60.0));

            if (forceRealtimeStateEmit || state.read() != lastState) {
                forceRealtimeStateEmit = false;
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, state, frameTime, frameTime + 60.0));
                lastState = std::move(state);
            }
        }

        if (FP::defined(frameTime)) {
            loggingMux.advance(LogStream_Status, frameTime, loggingEgress);

            streamAge[LogStream_Status]++;
            streamTime[LogStream_Status] = frameTime;
            if (streamAge[LogStream_Status] > 1) {
                streamOverlapped(frameTime, LogStream_Status);
            }
        } else {
            invalidStreamTime();
        }

        break;
    }

    case 'J': {
        /* 180 can return this on the first line */
        if (line.string_start("J:")) {
            auto str = line.toString();
            if (str.find("PM10") != str.npos ||
                    str.find("PM2.5") != str.npos ||
                    str.find("PM1.0") != str.npos)
                return -3;
        }

        std::vector<double> sizes;
        int code = extractBins(fields, sizes);
        if (code != 0) return code + 1000;
        Q_ASSERT(sizes.size() == 8);
        insertIntoValue(true, sizes, currentSizes);
        ++responseIndex;

        haveEmittedLogMeta = false;
        haveEmittedRealtimeMeta = false;
        return -3;
    }
    case 'j': {
        std::vector<double> sizes;
        int code = extractBins(fields, sizes);
        if (code != 0) return code + 2000;
        Q_ASSERT(sizes.size() == 8);
        insertIntoValue(false, sizes, currentSizes);
        ++responseIndex;

        haveEmittedLogMeta = false;
        haveEmittedRealtimeMeta = false;
        return -3;
    }

    case 'N': {
        int currentMinuteIndex = convertMinuteIndex(line[1]);
        if (currentMinuteIndex < 0) return 3000;
        std::vector<double> masses;
        int code = extractBins(fields, masses);
        if (code != 0) return code + 3000;
        Q_ASSERT(masses.size() == 8);

        if (FP::defined(frameTime) &&
                advanceCheck == NULL &&
                responseState != RESP_INTERACTIVE_QUERY_VOLUME) {
            timeoutAt(frameTime + 12.0);
        }

        /* Conventional distributions mean no binned masses, so advance that */
        currentBinnedMasses = false;
        if (FP::defined(frameTime)) {
            loggingMux.advance(LogStream_BinnedMasses, frameTime, loggingEgress);
        }

        if (minuteIndex != currentMinuteIndex) {
            if (advanceCheck == NULL) {
                if (currentMinuteIndex == 1) {
                    issueVolumePoll(frameTime);
                } else if (currentMinuteIndex == 2 &&
                        responseState == RESP_INTERACTIVE_QUERY_VOLUME) {
                    if (controlStream != NULL) {
                        controlStream->writeControl("M\r");
                    }
                    responseState = RESP_INTERACTIVE_RUN;
                }
            }

            if (!FP::defined(frameTime) &&
                    FP::defined(baseTime) &&
                    currentMinuteIndex >= 0 &&
                    currentMinuteIndex < 10 &&
                    currentMinuteIndex > minuteIndex) {
                frameTime = baseTime + 6.0 * currentMinuteIndex;
            }
            minuteIndex = currentMinuteIndex;
            responseIndex = 0;

            if (advanceCheck != NULL) {
                *advanceCheck = true;
                int n = currentMasses.read().toArray().size();
                if (n != previousNumberOfBins) {
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;
                    previousNumberOfBins = n;
                }
            } else {
                emitMetadataIfNeeded(frameTime);
                emitCounts(frameTime, calculateFromMassConcentrations(currentMasses, lastGF));
            }
        }

        insertIntoValue(true, masses, currentMasses);
        ++responseIndex;
        break;
    }
    case 'n': {
        int currentMinuteIndex = convertMinuteIndex(line[1]);
        if (currentMinuteIndex < 0) return 4000;
        std::vector<double> masses;
        int code = extractBins(fields, masses);
        if (code != 0) return code + 4000;
        Q_ASSERT(masses.size() == 8);

        if (FP::defined(frameTime) &&
                advanceCheck == NULL &&
                responseState != RESP_INTERACTIVE_QUERY_VOLUME) {
            timeoutAt(frameTime + 12.0);
        }

        if (minuteIndex != currentMinuteIndex) {
            if (advanceCheck == NULL) {
                if (currentMinuteIndex == 1) {
                    issueVolumePoll(frameTime);
                } else if (currentMinuteIndex == 2 &&
                        responseState == RESP_INTERACTIVE_QUERY_VOLUME) {
                    if (controlStream != NULL) {
                        controlStream->writeControl("M\r");
                    }
                    responseState = RESP_INTERACTIVE_RUN;
                }
            }

            if (!FP::defined(frameTime) &&
                    FP::defined(baseTime) &&
                    currentMinuteIndex >= 0 &&
                    currentMinuteIndex < 10 &&
                    currentMinuteIndex > minuteIndex) {
                frameTime = baseTime + 6.0 * currentMinuteIndex;
            }
            minuteIndex = currentMinuteIndex;
            responseIndex = 0;

            if (advanceCheck != NULL) {
                *advanceCheck = true;
                int n = currentMasses.read().toArray().size();
                if (n != previousNumberOfBins) {
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;
                    previousNumberOfBins = n;
                }
            } else {
                emitMetadataIfNeeded(frameTime);
                emitCounts(frameTime, calculateFromMassConcentrations(currentMasses, lastGF));
            }
        }

        insertIntoValue(false, masses, currentMasses);
        ++responseIndex;
        break;
    }

    case 'C': {
        int currentMinuteIndex = convertMinuteIndex(line[1]);
        if (currentMinuteIndex < 0) return 5000;
        std::vector<double> counts;
        int code = extractBins(fields, counts);
        if (code != 0) return code + 5000;
        Q_ASSERT(counts.size() == 8);

        if (FP::defined(frameTime) &&
                advanceCheck == NULL &&
                responseState != RESP_INTERACTIVE_QUERY_VOLUME) {
            timeoutAt(frameTime + 12.0);
        }

        if (minuteIndex != currentMinuteIndex) {
            if (advanceCheck == NULL) {
                if (currentMinuteIndex == 1) {
                    issueVolumePoll(frameTime);
                } else if (currentMinuteIndex == 2 &&
                        responseState == RESP_INTERACTIVE_QUERY_VOLUME) {
                    if (controlStream != NULL) {
                        controlStream->writeControl("M\r");
                    }
                    responseState = RESP_INTERACTIVE_RUN;
                }
            }

            if (!FP::defined(frameTime) &&
                    FP::defined(baseTime) &&
                    currentMinuteIndex >= 0 &&
                    currentMinuteIndex < 10 &&
                    currentMinuteIndex > minuteIndex) {
                frameTime = baseTime + 6.0 * currentMinuteIndex;
            }
            minuteIndex = currentMinuteIndex;
            responseIndex = 0;

            if (advanceCheck != NULL) {
                *advanceCheck = true;
                int n = currentCounts.read().toArray().size();
                if (n != previousNumberOfBins) {
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;
                    previousNumberOfBins = n;
                }
            } else {
                emitMetadataIfNeeded(frameTime);
                emitCountsDirect(frameTime);
            }
        }

        insertIntoValue(true, counts, currentCounts);
        ++responseIndex;
        break;
    }
    case 'c': {
        int currentMinuteIndex = convertMinuteIndex(line[1]);
        if (currentMinuteIndex < 0) return 6000;
        std::vector<double> counts;
        int code = extractBins(fields, counts);
        if (code != 0) return code + 6000;
        Q_ASSERT(counts.size() == 8);

        if (FP::defined(frameTime) &&
                advanceCheck == NULL &&
                responseState != RESP_INTERACTIVE_QUERY_VOLUME) {
            timeoutAt(frameTime + 12.0);
        }

        if (minuteIndex != currentMinuteIndex) {
            if (advanceCheck == NULL) {
                if (currentMinuteIndex == 1) {
                    issueVolumePoll(frameTime);
                } else if (currentMinuteIndex == 2 &&
                        responseState == RESP_INTERACTIVE_QUERY_VOLUME) {
                    if (controlStream != NULL) {
                        controlStream->writeControl("M\r");
                    }
                    responseState = RESP_INTERACTIVE_RUN;
                }
            }

            if (!FP::defined(frameTime) &&
                    FP::defined(baseTime) &&
                    currentMinuteIndex >= 0 &&
                    currentMinuteIndex < 10 &&
                    currentMinuteIndex > minuteIndex) {
                frameTime = baseTime + 6.0 * currentMinuteIndex;
            }
            minuteIndex = currentMinuteIndex;
            responseIndex = 0;

            if (advanceCheck != NULL) {
                *advanceCheck = true;
                int n = currentCounts.read().toArray().size();
                if (n != previousNumberOfBins) {
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;
                    previousNumberOfBins = n;
                }
            } else {
                emitMetadataIfNeeded(frameTime);
                emitCountsDirect(frameTime);
            }
        }

        insertIntoValue(false, counts, currentCounts);
        ++responseIndex;
        break;
    }

    case 'V': {
        if (fields.empty()) return 7000;
        field = fields.front().toQByteArray();
        fields.pop_front();
        Variant::Root volume;
        {
            auto idx = field.indexOf('m');
            if (idx != field.npos) {
                field = field.mid(0, idx);
            }
            double raw = field.string_trimmed().parse_real(&ok);
            if (!ok || !FP::defined(raw) || raw < 0.0) return 7001;
            volume.write().setDouble(raw);
        }
        remap("Qt", volume);

        if (!FP::defined(frameTime)) {
            frameTime = baseTime;
        } else if (advanceCheck == NULL) {
            timeoutAt(frameTime + 12.0);
        }

        double startTime = streamTime[LogStream_Flow];
        double endTime = frameTime;

        /* These are not regular records, so we always use the measured time */
        Variant::Root
                Q(calculateFlow(startTime, previousVolume, endTime, volume.read().toDouble()));
        remap("Q", Q);
        previousVolume = volume.read().toDouble();
        lastSampleFlow = Q.read().toDouble();

        /* Invalid timing, so just ignore it */
        if (endTime < startTime)
            return -1;

        if (advanceCheck == NULL) {
            emitMetadataIfNeeded(frameTime);

            if (loggingEgress && FP::defined(startTime) && FP::defined(endTime)) {
                loggingMux.incoming(LogStream_Flow,
                                    SequenceValue({{}, "raw", "Q"}, Q, startTime, endTime),
                                    loggingEgress);
                loggingMux.advance(LogStream_Flow, endTime, loggingEgress);
            }

            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "Q"}, std::move(Q), frameTime, frameTime + 60.0));
            }

            if (FP::defined(frameTime)) {
                streamAge[LogStream_Flow]++;
                streamTime[LogStream_Flow] = endTime;
                if (streamAge[LogStream_Flow] > 1) {
                    streamOverlapped(frameTime, LogStream_Flow);
                }
            } else {
                invalidStreamTime();
            }
        }

        /* Not enough for disambiguation, is discard */
        return -2;
    }

    case 'M':
    case 'm':
    case 'K':
        return -1;
    }

    if (config.first().strictMode) {
        if (!fields.empty())
            return 9000;
    }


    if (responseCode != 'P' &&
            realtimeEgress != NULL &&
            FP::defined(frameTime) &&
            advanceCheck == NULL) {
        if (forceRealtimeStateEmit) {
            forceRealtimeStateEmit = false;
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                  frameTime + 60.0));
            lastState.write().setString("Run");
        }
    }

    return 0;
}

void AcquireGrimmOPC110x::setFromModel(const QString &model)
{
    auto match = QRegularExpression("^1.10(\\d)$").match(model);
    QString extractedModel(model.toLower());
    if (match.isValid())
        extractedModel = match.captured(1);

    if (extractedModel == "1") {
        instrumentFlowCorrection = 0.6;
        binInsertOffset = 7;
        currentBinnedMasses = false;
        if (!currentSizes.read().exists()) {
            currentSizes.write().array(14).remove(true);
        }
    } else if (extractedModel == "4") {
        instrumentFlowCorrection = 1.2;
        binInsertOffset = 7;
        currentBinnedMasses = false;
        if (!currentSizes.read().exists()) {
            currentSizes.write().array(14).remove(true);
        }
    } else if (extractedModel == "5") {
        instrumentFlowCorrection = 1.2;
        binInsertOffset = 0;
        currentBinnedMasses = false;
        if (!currentSizes.read().exists()) {
            currentSizes.write().array(0).setDouble(0.75);
            currentSizes.write().array(1).setDouble(1.0);
            currentSizes.write().array(2).setDouble(2.0);
            currentSizes.write().array(3).setDouble(3.5);
            currentSizes.write().array(4).setDouble(5.0);
            currentSizes.write().array(5).setDouble(7.5);
            currentSizes.write().array(6).setDouble(10.0);
            currentSizes.write().array(7).setDouble(15.0);
        }
    } else if (extractedModel == "7" ||
            extractedModel == "9" ||
            extractedModel == "11-a" ||
            extractedModel == "180mc") {
        instrumentFlowCorrection = 1.2;
        binInsertOffset = 15;
        currentBinnedMasses = (extractedModel == "180mc");
        if (!currentSizes.read().exists()) {
            currentSizes.write().array(0).setDouble(0.25);
            currentSizes.write().array(1).setDouble(0.28);
            currentSizes.write().array(2).setDouble(0.30);
            currentSizes.write().array(3).setDouble(0.35);
            currentSizes.write().array(4).setDouble(0.40);
            currentSizes.write().array(5).setDouble(0.45);
            currentSizes.write().array(6).setDouble(0.50);
            currentSizes.write().array(7).setDouble(0.58);
            currentSizes.write().array(8).setDouble(0.65);
            currentSizes.write().array(9).setDouble(0.70);
            currentSizes.write().array(10).setDouble(0.80);
            currentSizes.write().array(11).setDouble(1.0);
            currentSizes.write().array(12).setDouble(1.3);
            currentSizes.write().array(13).setDouble(1.6);
            currentSizes.write().array(14).setDouble(2.0);
            currentSizes.write().array(15).setDouble(2.5);
            currentSizes.write().array(16).setDouble(3.0);
            currentSizes.write().array(17).setDouble(3.5);
            currentSizes.write().array(18).setDouble(4.0);
            currentSizes.write().array(19).setDouble(5.0);
            currentSizes.write().array(20).setDouble(6.5);
            currentSizes.write().array(21).setDouble(7.5);
            currentSizes.write().array(22).setDouble(8.5);
            currentSizes.write().array(23).setDouble(10.0);
            currentSizes.write().array(24).setDouble(12.5);
            currentSizes.write().array(25).setDouble(15.0);
            currentSizes.write().array(26).setDouble(17.5);
            currentSizes.write().array(27).setDouble(20.0);
            currentSizes.write().array(28).setDouble(25.0);
            currentSizes.write().array(29).setDouble(30.0);
            currentSizes.write().array(30).setDouble(32.0);
        }
    } else if (extractedModel == "8") {
        instrumentFlowCorrection = 1.2;
        binInsertOffset = 7;
        currentBinnedMasses = false;
        if (!currentSizes.read().exists()) {
            currentSizes.write().array(0).setDouble(0.30);
            currentSizes.write().array(1).setDouble(0.40);
            currentSizes.write().array(2).setDouble(0.50);
            currentSizes.write().array(3).setDouble(0.65);
            currentSizes.write().array(4).setDouble(0.80);
            currentSizes.write().array(5).setDouble(1.0);
            currentSizes.write().array(6).setDouble(1.6);
            currentSizes.write().array(7).setDouble(2.0);
            currentSizes.write().array(8).setDouble(3.0);
            currentSizes.write().array(9).setDouble(4.0);
            currentSizes.write().array(10).setDouble(5.0);
            currentSizes.write().array(11).setDouble(7.5);
            currentSizes.write().array(12).setDouble(10.0);
            currentSizes.write().array(13).setDouble(15.0);
            currentSizes.write().array(14).setDouble(20.0);
        }
    }
}

void AcquireGrimmOPC110x::setFromFirmware(const QString &firmware)
{
    double v = firmware.toDouble();
    if (!FP::defined(v))
        return;
    if (v >= 7.0)
        instrumentGFScale = 1.0 / 100.0;
    else
        instrumentGFScale = 1.0 / 20.0;
}

void AcquireGrimmOPC110x::configurationAdvance(double frameTime)
{
    Q_ASSERT(!config.isEmpty());
    int oldSize = config.size();
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.isEmpty());
    if (config.size() != oldSize) {
        if (!config.first().model.isEmpty())
            setFromModel(config.first().model);
        if (!config.first().firmware.isEmpty())
            setFromModel(config.first().firmware);
    }
}

static Util::ByteArray makeRestartStandbyCommand()
{
    Util::ByteArray result;
    result.push_back(0x1B);
    result.push_back('\r');
    result.push_back('\r');
    result.push_back('S');
    result.push_back('\r');
    result.push_back(0x1B);
    result.push_back('\r');
    result.push_back('\r');
    result.push_back('S');
    result.push_back('\r');
    result.push_back('S');
    result.push_back('\r');
    return result;
}

void AcquireGrimmOPC110x::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (FP::defined(frameTime))
        configurationAdvance(frameTime);

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            if (++autoprobeValidRecords > 6) {
                qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

                responseState = RESP_PASSIVE_RUN;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                forceRealtimeStateEmit = true;

                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("PassiveAutoprobeWait");
                event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);
            }
        } else if (code > 0) {
            autoprobeValidRecords = 0;
            invalidateLogValues(frameTime);
        } else {
            invalidateLogValues(frameTime);
        }
        break;
    }
    case RESP_PASSIVE_WAIT:
        if (processRecord(frame, frameTime) == 0) {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);
            responseState = RESP_PASSIVE_RUN;
            generalStatusUpdated();

            if (FP::defined(frameTime))
                timeoutAt(frameTime + 12.0);

            forceRealtimeStateEmit = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveWait");
            event(frameTime, QObject::tr("Passive communications established."), false, info);
        } else {
            invalidateLogValues(frameTime);
        }
        break;


    case RESP_INTERACTIVE_START_DISCARDFIRST:
    case RESP_INTERACTIVE_START_DISCARDSECOND: {
        bool advance = false;
        int code = processRecord(frame, frameTime, &advance);
        invalidateLogValues(frameTime);
        if (code == 0 && advance) {
            if (responseState == RESP_INTERACTIVE_START_DISCARDSECOND) {
                responseState = RESP_INTERACTIVE_START_FIRSTVALID;
                if (FP::defined(frameTime))
                    timeoutAt(frameTime + 12.0);

                if (realtimeEgress != NULL) {
                    realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                            "StartInteractiveStartReadFirstRecord"), frameTime, FP::undefined()));
                }
            } else {
                responseState = RESP_INTERACTIVE_START_DISCARDSECOND;
                if (FP::defined(frameTime))
                    timeoutAt(frameTime + 12.0);
            }
        } else if (code > 0) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "rejected with code" << code;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidRecord"),
                                      frameTime, FP::undefined()));
            }

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            if (FP::defined(frameTime))
                discardData(frameTime + 12.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Communications start rejected the first line (code %1).").arg(code),
                  true, info);

        }
        break;
    }

    case RESP_INTERACTIVE_START_FIRSTVALID: {
        bool advance = false;
        int code = processRecord(frame, frameTime, &advance);
        if (code == 0 && advance) {
            qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

            responseState = RESP_INTERACTIVE_RUN;
            forceRealtimeStateEmit = true;
            if (FP::defined(frameTime))
                timeoutAt(frameTime + 12.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            event(frameTime, QObject::tr("Communications established."), false, info);
        } else if (code > 0) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "rejected with code" << code;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidRecord"),
                                      frameTime, FP::undefined()));
            }

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            if (FP::defined(frameTime))
                discardData(frameTime + 12.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Communications start rejected the first line (code %1).").arg(code),
                  true, info);

            invalidateLogValues(frameTime);
        } else {
            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RUN:
    case RESP_INTERACTIVE_QUERY_VOLUME: {
        int code = processRecord(frame, frameTime);
        if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            Variant::Write info = Variant::Write::empty();

            if (responseState == RESP_PASSIVE_RUN) {
                timeoutAt(FP::undefined());
                responseState = RESP_PASSIVE_WAIT;
                info.hash("ResponseState").setString("PassiveRun");
            } else {
                if (controlStream != NULL) {
                    controlStream->writeControl(makeRestartStandbyCommand());
                }
                responseState = RESP_INTERACTIVE_START_STANDBY;
                if (FP::defined(frameTime))
                    discardData(frameTime + 1.0);
                info.hash("ResponseState").setString("Run");
            }
            generalStatusUpdated();

            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
        }
        break;
    }

    case RESP_INTERACTIVE_START_READMODEL: {
        /* Ignore command echo */
        if (frame.toQByteArray().trimmed().length() == 1)
            break;

        auto match = QRegularExpression(
                "^\\s*(?:OPC\\s+)?Model\\s*:?\\s*([^\\s:]+)\\s*Version\\s*:?\\s*([^\\s:].*)\\s*(?:[a-z]{2}\\s*)?$",
                QRegularExpression::CaseInsensitiveOption).match(frame.toQString());

        if (FP::defined(frameTime))
            timeoutAt(frameTime + 4.0);

        Variant::Root oldModel(instrumentMeta["Model"]);
        Variant::Root oldVersion(instrumentMeta["FirmwareVersion"]);
        QString str(frame.toQString(false));
        if (match.isValid()) {
            instrumentMeta["FirmwareVersion"].setString(match.captured(2).trimmed());
            instrumentMeta["Model"].setString(match.captured(1).trimmed());

            setFromFirmware(match.captured(2).trimmed());

            responseState = RESP_INTERACTIVE_START_READSERIAL;
            if (controlStream != NULL) {
                controlStream->writeControl("@\r");
            }

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadSerial"), frameTime, FP::undefined()));
            }
        } else {
            instrumentMeta["Model"].setString(str.trimmed());

            responseState = RESP_INTERACTIVE_START_READFIRMWAREVERSION;
            if (controlStream != NULL) {
                controlStream->writeControl("V\r");
            }

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadFirmwareVersion"), frameTime, FP::undefined()));
            }
        }

        if (oldModel.read() != instrumentMeta["Model"]) {
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
            setFromModel(instrumentMeta["Model"].toQString());
        } else if (oldVersion.read() != instrumentMeta["FirmwareVersion"]) {
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }
        break;
    }

    case RESP_INTERACTIVE_START_READFIRMWAREVERSION: {
        /* Ignore command echo */
        if (frame.toQByteArray().trimmed().length() == 1)
            break;

        auto match =
                QRegularExpression("^\\s*Ver(?:sion)?\\s*:?\\s*([^\\s:].*)\\s*(?:[a-z]{2}\\s*)?$",
                                   QRegularExpression::CaseInsensitiveOption).match(
                        frame.toQString());
        if (match.isValid()) {
            Variant::Root oldValue(instrumentMeta["FirmwareVersion"]);
            instrumentMeta["FirmwareVersion"].setString(match.captured(1).trimmed());
            setFromFirmware(match.captured(1).trimmed());
            if (oldValue.read() != instrumentMeta["FirmwareVersion"]) {
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }
        }

        if (FP::defined(frameTime))
            timeoutAt(frameTime + 4.0);
        responseState = RESP_INTERACTIVE_START_READSERIAL;
        if (controlStream != NULL) {
            controlStream->writeControl("@\r");
        }

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveReadSerialNumber"), frameTime, FP::undefined()));
        }
        break;
    }

    case RESP_INTERACTIVE_START_READSERIAL: {
        /* Ignore command echo */
        if (frame.toQByteArray().trimmed().length() == 1)
            break;

        auto match = QRegularExpression(
                "^\\s*Ser(?:ial)?\\s*\\.\\s*(?:(?:Number)|(?:No))\\s*\\.\\s*:?\\s*([^\\s:]+)\\s*(?:Sensor\\s*:\\s*([^\\s:]+)\\s*)?$",
                QRegularExpression::CaseInsensitiveOption).match(frame.toQString());
        if (match.isValid()) {
            Variant::Root oldSerial(instrumentMeta["SerialNumber"]);
            instrumentMeta["SerialNumber"].setString(match.captured(1).trimmed());
            if (oldSerial.read() != instrumentMeta["SerialNumber"]) {
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }

            if (!match.captured(2).trimmed().isEmpty()) {
                Variant::Root oldSensor(instrumentMeta["Sensor"]);
                instrumentMeta["Sensor"].setString(match.captured(2).trimmed());
                if (oldSerial.read() != instrumentMeta["Sensor"]) {
                    haveEmittedLogMeta = false;
                    haveEmittedRealtimeMeta = false;
                    sourceMetadataUpdated();
                }
            }
        }

        if (FP::defined(frameTime)) {
            timeoutAt(frameTime + 5.0);
            discardData(frameTime + 1.0);
        }
        responseState = RESP_INTERACTIVE_START_RUN;
        if (controlStream != NULL) {
            controlStream->writeControl("F\r");
        }

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveStartMeasurement"), frameTime, FP::undefined()));
        }
        break;
    }

    case RESP_INTERACTIVE_START_READVOLUME: {
        bool advance = false;
        int code = processRecord(frame, frameTime, &advance);
        if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "during initial volume read with code" << code;

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            invalidateLogValues(frameTime);
        } else if (code == -2) {
            /* Wait for the volume response to finish */
            responseState = RESP_INTERACTIVE_START_READSIZES_WAIT;
            if (FP::defined(frameTime)) {
                timeoutAt(frameTime + 10.0);
                discardData(frameTime + 3.0);
            }

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadSizes"), frameTime, FP::undefined()));
            }
        } /* Ignored */
        break;
    }

    case RESP_INTERACTIVE_START_READSIZES_WAIT:
    case RESP_INTERACTIVE_START_READSIZES: {
        int code = processRecord(frame, frameTime);
        if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "during initial size read with code" << code;

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            invalidateLogValues(frameTime);
        } else if (code == -3) {
            if (FP::defined(frameTime))
                timeoutAt(frameTime + 3.0);
        } else if (code == 0) {
            if (controlStream != NULL) {
                controlStream->writeControl("R\r");
            }
            responseState = RESP_INTERACTIVE_START_DISCARDFIRST;
            if (FP::defined(frameTime)) {
                timeoutAt(frameTime + 90.0);
                discardData(frameTime + 30.0, 1);
            }

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveDiscardFirst"), frameTime, FP::undefined()));
            }
        }
        break;
    }

    default:
        break;
    }
}

void AcquireGrimmOPC110x::incomingInstrumentTimeout(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));

    switch (responseState) {
    case RESP_INTERACTIVE_RUN:
        qCDebug(log) << "Timeout in interactive state" << responseIndex << "at"
                     << Logging::time(frameTime);
        timeoutAt(frameTime + 30.0);

        if (controlStream != NULL) {
            controlStream->writeControl(makeRestartStandbyCommand());
        }
        responseState = RESP_INTERACTIVE_START_STANDBY;
        discardData(frameTime + 0.5);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        {
            Variant::Write info = Variant::Write::empty();
            if (responseState == RESP_INTERACTIVE_QUERY_VOLUME) {
                info.hash("ResponseState").setString("InteractiveQueryVolume");
            } else {
                info.hash("ResponseState").setString("InteractiveRun");
            }
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_QUERY_VOLUME:
        if (controlStream != NULL) {
            controlStream->writeControl("M\r");
        }
        responseState = RESP_INTERACTIVE_RUN;
        timeoutAt(frameTime + 12.0);
        break;

    case RESP_PASSIVE_RUN:
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        responseState = RESP_PASSIVE_WAIT;
        generalStatusUpdated();
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_START_READSIZES_WAIT:
    case RESP_INTERACTIVE_START_READSIZES:
        if (controlStream != NULL) {
            controlStream->writeControl("R\r");
        }
        responseState = RESP_INTERACTIVE_START_DISCARDFIRST;
        timeoutAt(frameTime + 90.0);
        discardData(frameTime + 30.0, 1);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveDiscardFirst"), frameTime, FP::undefined()));
        }

        break;

    case RESP_INTERACTIVE_START_STANDBY:
    case RESP_INTERACTIVE_START_STANDBY_SECOND:
    case RESP_INTERACTIVE_START_READMODEL:
    case RESP_INTERACTIVE_START_READFIRMWAREVERSION:
    case RESP_INTERACTIVE_START_READSERIAL:
    case RESP_INTERACTIVE_START_RUN:
    case RESP_INTERACTIVE_START_REPORTCOUNTS:
    case RESP_INTERACTIVE_START_READVOLUME:
    case RESP_INTERACTIVE_START_DISCARDFIRST:
    case RESP_INTERACTIVE_START_DISCARDSECOND:
    case RESP_INTERACTIVE_START_FIRSTVALID:
    case RESP_INTERACTIVE_RESTART_WAIT:
        qCDebug(log) << "Timeout in interactive start state" << responseState << "at"
                     << Logging::time(frameTime);

        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        timeoutAt(frameTime + 10.0);
        if (controlStream != NULL) {
            controlStream->writeControl(makeRestartStandbyCommand());
        }
        responseState = RESP_INTERACTIVE_START_STANDBY;
        discardData(frameTime + 1.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
        invalidateLogValues(frameTime);
        break;

    default:
        invalidateLogValues(frameTime);
        break;
    }
}

void AcquireGrimmOPC110x::discardDataCompleted(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    switch (responseState) {
    case RESP_INTERACTIVE_START_STANDBY:
        if (controlStream != NULL) {
            controlStream->writeControl("S\r");
        }
        responseState = RESP_INTERACTIVE_START_STANDBY_SECOND;
        timeoutAt(frameTime + 15.0);
        discardData(frameTime + 7.0);
        break;

    case RESP_INTERACTIVE_START_STANDBY_SECOND:
        if (controlStream != NULL) {
            controlStream->writeControl("!\r");
        }
        responseState = RESP_INTERACTIVE_START_READMODEL;
        timeoutAt(frameTime + 5.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveReadModel"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_READSIZES_WAIT:
        if (controlStream != NULL) {
            controlStream->writeControl("J\r");
        }
        responseState = RESP_INTERACTIVE_START_READSIZES;
        timeoutAt(frameTime + 5.0);
        /* Discard echo */
        discardData(FP::undefined(), 1);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveReadSizes"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_RUN:
        if (controlStream != NULL) {
            controlStream->writeControl("C\r");
        }
        responseState = RESP_INTERACTIVE_START_REPORTCOUNTS;
        timeoutAt(frameTime + 5.0);
        discardData(frameTime + 1.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveSetCountMode"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_REPORTCOUNTS:
        if (controlStream != NULL) {
            controlStream->writeControl("M\r");
        }
        responseState = RESP_INTERACTIVE_START_READVOLUME;
        timeoutAt(frameTime + 5.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                       Variant::Root("StartInteractiveReadVolume"),
                                                       frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_DISCARDFIRST:
        timeoutAt(frameTime + 30.0);
        break;

    case RESP_INTERACTIVE_RESTART_WAIT:
        timeoutAt(frameTime + 30.0);
        if (controlStream != NULL) {
            controlStream->writeControl(makeRestartStandbyCommand());
        }
        responseState = RESP_INTERACTIVE_START_STANDBY;
        discardData(frameTime + 1.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    default:
        break;
    }
}


void AcquireGrimmOPC110x::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_UNUSED(frameTime);
    Q_UNUSED(frame);
    /* Maybe implement something to discard unknown command responses? */
}

Variant::Root AcquireGrimmOPC110x::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireGrimmOPC110x::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RUN:
    case RESP_INTERACTIVE_QUERY_VOLUME:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

AcquisitionInterface::AutoprobeStatus AcquireGrimmOPC110x::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireGrimmOPC110x::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN:
    case RESP_INTERACTIVE_QUERY_VOLUME:
        /* Already running/started, do nothing */
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << responseState << "at" << Logging::time(time);
        break;

    case RESP_INTERACTIVE_START_STANDBY:
    case RESP_INTERACTIVE_START_STANDBY_SECOND:
    case RESP_INTERACTIVE_START_READMODEL:
    case RESP_INTERACTIVE_START_READFIRMWAREVERSION:
    case RESP_INTERACTIVE_START_READSERIAL:
    case RESP_INTERACTIVE_START_RUN:
    case RESP_INTERACTIVE_START_REPORTCOUNTS:
    case RESP_INTERACTIVE_START_READVOLUME:
    case RESP_INTERACTIVE_START_READSIZES_WAIT:
    case RESP_INTERACTIVE_START_READSIZES:
    case RESP_INTERACTIVE_START_DISCARDFIRST:
    case RESP_INTERACTIVE_START_DISCARDSECOND:
    case RESP_INTERACTIVE_START_FIRSTVALID:
    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        timeoutAt(time + 10.0);
        if (controlStream != NULL) {
            controlStream->writeControl(makeRestartStandbyCommand());
        }
        responseState = RESP_INTERACTIVE_START_STANDBY;
        generalStatusUpdated();
        discardData(time + 1.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireGrimmOPC110x::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
    autoprobeValidRecords = 0;
    discardData(time + 0.5, 1);
    timeoutAt(time + 45.0);
    generalStatusUpdated();
}

void AcquireGrimmOPC110x::autoprobePromote(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN:
        qCDebug(log) << "Promoted from interactive run to interactive acquisition at"
                     << Logging::time(time);

        timeoutAt(time + 15.0);
        break;

    case RESP_INTERACTIVE_QUERY_VOLUME:
    case RESP_INTERACTIVE_START_STANDBY:
    case RESP_INTERACTIVE_START_STANDBY_SECOND:
    case RESP_INTERACTIVE_START_READMODEL:
    case RESP_INTERACTIVE_START_READFIRMWAREVERSION:
    case RESP_INTERACTIVE_START_READSERIAL:
    case RESP_INTERACTIVE_START_RUN:
    case RESP_INTERACTIVE_START_REPORTCOUNTS:
    case RESP_INTERACTIVE_START_READVOLUME:
    case RESP_INTERACTIVE_START_READSIZES_WAIT:
    case RESP_INTERACTIVE_START_READSIZES:
    case RESP_INTERACTIVE_START_DISCARDFIRST:
    case RESP_INTERACTIVE_START_DISCARDSECOND:
    case RESP_INTERACTIVE_START_FIRSTVALID:
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + 90.0);
        break;

    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + 10.0);
        if (controlStream != NULL) {
            controlStream->writeControl(makeRestartStandbyCommand());
        }
        responseState = RESP_INTERACTIVE_START_STANDBY;
        generalStatusUpdated();
        discardData(time + 1.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireGrimmOPC110x::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    Q_ASSERT(!config.isEmpty());

    timeoutAt(time + 15.0);

    switch (responseState) {
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case RESP_INTERACTIVE_RUN:
        /* Already in unpolled, so don't reset timeout */
        responseState = RESP_PASSIVE_RUN;

        qCDebug(log) << "Promoted from interactive to passive acquisition at"
                     << Logging::time(time);
        break;

    default:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_PASSIVE_RUN;
        generalStatusUpdated();
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireGrimmOPC110x::getDefaults()
{
    AutomaticDefaults result;
    result.name = "N$1$2";
    result.setSerialN81(9600);
    return result;
}


ComponentOptions AcquireGrimmOPC110xComponent::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options);
    return options;
}

ComponentOptions AcquireGrimmOPC110xComponent::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireGrimmOPC110xComponent::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples());
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireGrimmOPC110xComponent::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireGrimmOPC110xComponent::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options, true);
}

QList<ComponentExample> AcquireGrimmOPC110xComponent::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireGrimmOPC110xComponent::createAcquisitionPassive(const ComponentOptions &options,
                                                                                     const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireGrimmOPC110x(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireGrimmOPC110xComponent::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                     const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireGrimmOPC110x(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireGrimmOPC110xComponent::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                       const std::string &loggingContext)
{
    std::unique_ptr<AcquireGrimmOPC110x> i(new AcquireGrimmOPC110x(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<AcquisitionInterface> AcquireGrimmOPC110xComponent::createAcquisitionInteractive(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireGrimmOPC110x> i(new AcquireGrimmOPC110x(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
