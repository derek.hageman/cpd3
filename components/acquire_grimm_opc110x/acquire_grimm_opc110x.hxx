/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIREGRIMMOPC110X_H
#define ACQUIREGRIMMOPC110X_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QVector>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"

class AcquireGrimmOPC110x : public CPD3::Acquisition::FramedInstrument {
    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum {
        /* Passive autoprobe initialization, ignoring the first timeout */
        RESP_AUTOPROBE_PASSIVE_INITIALIZE,
        /* Waiting for a valid record to confirm passive mode autoprobe */
        RESP_AUTOPROBE_PASSIVE_WAIT,

        /* Waiting for a valid record to start passive acquisition */
        RESP_PASSIVE_WAIT,

        /* Acquiring data in passive mode */
        RESP_PASSIVE_RUN,

        /* Acquiring data in interactive mode, reading the response to a
         * query. */
                RESP_INTERACTIVE_RUN,
        /* Acquisting data in interactive mode, in the volume query mode
         * (waiting for the discard to complete) */
                RESP_INTERACTIVE_QUERY_VOLUME,

        /* Waiting for the completion of the "S" command during start
         * communications. */
                RESP_INTERACTIVE_START_STANDBY, /* And waiting again to make sure it went through */
                RESP_INTERACTIVE_START_STANDBY_SECOND,
        /* Reading the model number and firmware version with the "!" command */
                RESP_INTERACTIVE_START_READMODEL,
        /* Reading the firmware version with the "V" command.  This is only
         * needed if extraction from the model command fails */
                RESP_INTERACTIVE_START_READFIRMWAREVERSION,
        /* Reading the serial number with the "@" command */
                RESP_INTERACTIVE_START_READSERIAL,
        /* Waiting for the completion of the "F" command during start
         * communications. */
                RESP_INTERACTIVE_START_RUN,
        /* Waiting for the completion of the "C" command during start
         * communications. */
                RESP_INTERACTIVE_START_REPORTCOUNTS,
        /* Reading the initial volumes with an "M" command */
                RESP_INTERACTIVE_START_READVOLUME,
        /* Doing a discard and wait before the sizes read */
                RESP_INTERACTIVE_START_READSIZES_WAIT,
        /* Reading the sizes with a "J" command */
                RESP_INTERACTIVE_START_READSIZES,
        /* Discarding the first report, after starting communications */
                RESP_INTERACTIVE_START_DISCARDFIRST,
        /* Discarding the second report, after starting communications */
                RESP_INTERACTIVE_START_DISCARDSECOND,
        /* Waiting for the first valid record */
                RESP_INTERACTIVE_START_FIRSTVALID,

        /* Waiting before attempting a communications restart */
                RESP_INTERACTIVE_RESTART_WAIT,
        /* Initial state for interactive start */
                RESP_INTERACTIVE_INITIALIZE,
    } responseState;
    int autoprobeValidRecords;
    int responseIndex;
    char lastResponseCode;

    enum LogStreamID {
        LogStream_Metadata = 0, LogStream_State,

        LogStream_Status, LogStream_Flow, LogStream_BinnedMasses, LogStream_Concentrations,

        LogStream_TOTAL, LogStream_RecordBaseStart = LogStream_Status,
    };
    CPD3::Data::StaticMultiplexer loggingMux;
    quint32 streamAge[LogStream_TOTAL];
    double streamTime[LogStream_TOTAL];

    int binInsertOffset;
    int minuteIndex;

    class Configuration {
        double start;
        double end;

    public:
        bool useMeasuredTime;
        bool strictMode;
        QString model;
        QString firmware;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under, const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    CPD3::Data::Variant::Root instrumentMeta;
    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;
    bool forceRealtimeStateEmit;

    double instrumentGFScale;
    double instrumentFlowCorrection;

    bool currentBinnedMasses;
    bool previousBinnedMasses;

    double lastSampleFlow;
    double lastGF;
    double baseTime;
    double previousVolume;
    int previousNumberOfBins;
    CPD3::Data::Variant::Root lastState;
    CPD3::Data::Variant::Root currentSizes;
    CPD3::Data::Variant::Root currentCounts;
    CPD3::Data::Variant::Root currentMasses;

    void setDefaultInvalid();

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    int getInsertOffset(bool upper) const;

    void insertIntoValue(bool upper,
                         const std::vector<double> &data,
                         CPD3::Data::Variant::Root &target) const;

    void emitMetadataIfNeeded(double frameTime);

    void streamOverlapped(double frameTime, int streamID);

    void invalidStreamTime();

    void emitCounts(double frameTime, const CPD3::Data::Variant::Read &concentrations);

    void emitCountsDirect(double frameTime);

    void issueVolumePoll(double frameTime);

    void invalidateLogValues(double frameTime);

    int processRecord(const CPD3::Util::ByteView &line,
                      double &frameTime,
                      bool *advanceCheck = NULL);

    void configurationAdvance(double frameTime);

    void setFromFirmware(const QString &firmware);

    void setFromModel(const QString &model);

public:
    AcquireGrimmOPC110x(const CPD3::Data::ValueSegment::Transfer &config,
                        const std::string &loggingContext);

    AcquireGrimmOPC110x(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireGrimmOPC110x();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    virtual CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus();

    virtual CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus();

    virtual CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables();

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    virtual void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingInstrumentTimeout(double time);

    virtual void discardDataCompleted(double time);
};

class AcquireGrimmOPC110xComponent
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_grimm_opc110x"
                              FILE
                              "acquire_grimm_opc110x.json")

    CPD3::ComponentOptions getBaseOptions();

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
