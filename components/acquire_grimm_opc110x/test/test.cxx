/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>
#include <QDateTime>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    double unpolledRemaining;
    int unpolledSequence;
    QDateTime currentTime;

    double flow;
    double accumulatedVolume;
    double gf;
    double gfScale;
    double pump;
    double battery;

    QByteArray firmwareVersion;
    QByteArray serialNumber;
    QByteArray modelNumber;
    int nChannels;
    double counts[31];
    double sizes[31];
    double masses180[3];

    Variant::Read outputCounts() const
    {
        Variant::Write result = Variant::Write::empty();
        for (int i = 0; i < nChannels; i++) {
            result.array(i).setDouble(counts[i]);
        }
        return result;
    }

    Variant::Read outputConcentrations() const
    {
        Variant::Write result = Variant::Write::empty();
        /* Nominal 1.2 lpm correction and 6 seconds between reports */
        double scale = (1.2) / (flow * 6.0 * (1000.0 / 60.0));
        for (int i = 0; i < nChannels; i++) {
            if (i == nChannels - 1) {
                result.array(i).setDouble(counts[i] * scale);
            } else {
                result.array(i).setDouble((counts[i] - counts[i + 1]) * scale);
            }
        }
        return result;
    }

    Variant::Read outputSizes() const
    {
        Variant::Write result = Variant::Write::empty();
        for (int i = 0; i < nChannels; i++) {
            if (i == nChannels - 1) {
                result.array(i).setDouble(sizes[i] + (sizes[i] - sizes[i - 1]) * 0.5);
            } else {
                result.array(i).setDouble((sizes[i] + sizes[i + 1]) * 0.5);
            }
        }
        return result;
    }

    double totalConcentration() const
    {
        double sum = 0.0;
        for (auto add : outputConcentrations().toArray()) {
            sum += add.toDouble();
        }
        return sum;
    }

    ModelInstrument()
            : incoming(),
              outgoing(),
              unpolledRemaining(FP::undefined()),
              unpolledSequence(0),
              currentTime(QDate(2013, 01, 02), QTime(10, 12, 0), Qt::UTC)
    {
        flow = 1.2;
        accumulatedVolume = 0.0;
        gf = 1.0;
        gfScale = 20.0;
        pump = 50.0;
        battery = 95.0;

        firmwareVersion = "12.30 E";
        serialNumber = "9G040001";
        modelNumber = "1.109";
        nChannels = 31;
        for (int i = 0; i < 31; i++) {
            counts[i] = (30 - i) * 1000.0;
        }

        masses180[0] = 2.5;
        masses180[1] = 2.0;
        masses180[2] = 1.5;

        sizes[0] = 0.25;
        sizes[1] = 0.28;
        sizes[2] = 0.30;
        sizes[3] = 0.35;
        sizes[4] = 0.40;
        sizes[5] = 0.45;
        sizes[6] = 0.50;
        sizes[7] = 0.58;
        sizes[8] = 0.65;
        sizes[9] = 0.70;
        sizes[10] = 0.80;
        sizes[11] = 1.0;
        sizes[12] = 1.3;
        sizes[13] = 1.6;
        sizes[14] = 2.0;
        sizes[15] = 2.5;
        sizes[16] = 3.0;
        sizes[17] = 3.5;
        sizes[18] = 4.0;
        sizes[19] = 5.0;
        sizes[20] = 6.5;
        sizes[21] = 7.5;
        sizes[22] = 8.5;
        sizes[23] = 10.0;
        sizes[24] = 12.5;
        sizes[25] = 15.0;
        sizes[26] = 17.5;
        sizes[27] = 20.0;
        sizes[28] = 25.0;
        sizes[29] = 30.0;
        sizes[30] = 32.0;
    }

    void writeBins(const double *array, int offset, int digits = 0)
    {
        for (int i = 0; i < 8; i++) {
            outgoing.append(' ');
            outgoing.append(
                    QByteArray::number(array[i + offset], 'f', digits).rightJustified(8, ' '));
        }
    }

    static char toLower(char c)
    {
        if (c >= 'A' && c <= 'Z')
            return c + ('a' - 'A');
        return c;
    }

    void writeArray(const QByteArray &id, const double *array, int digits = 0)
    {
        switch (nChannels) {
        case 8:
        case 7:
            outgoing.append(id);
            outgoing.append(':');
            writeBins(array, 0, digits);
            outgoing.append('\r');
            break;
        case 15:
            outgoing.append(id);
            outgoing.append(':');
            writeBins(array, 0, digits);
            outgoing.append('\r');
            outgoing.append(id.toLower());
            outgoing.append(':');
            writeBins(array, 7, digits);
            outgoing.append('\r');
            break;
        case 31:
            outgoing.append(id);
            outgoing.append(':');
            writeBins(array, 0, digits);
            outgoing.append('\r');
            outgoing.append(id);
            outgoing.append(';');
            writeBins(array, 8, digits);
            outgoing.append('\r');
            outgoing.append(id.toLower());
            outgoing.append(':');
            writeBins(array, 15, digits);
            if (modelNumber.startsWith("180"))
                outgoing.append("     0");
            outgoing.append('\r');
            outgoing.append(id.toLower());
            outgoing.append(';');
            writeBins(array, 23, digits);
            if (modelNumber.startsWith("180"))
                outgoing.append("     0");
            outgoing.append('\r');
            break;
        default:
            Q_ASSERT(false);
            break;
        }
    }

    void advance(double seconds)
    {

        int idxCR = -1;
        while ((idxCR = incoming.indexOf('\r')) >= 0) {
            QByteArray line(incoming.mid(0, idxCR));
            /* Echo */
            outgoing.append(line);
            outgoing.append('\r');

            if (line == "C") {
            } else if (line == "F") {
                //unpolledRemaining = 6.0;
            } else if (line == "J") {
                if (modelNumber.startsWith("180")) {
                    outgoing.append("J:  PM10 PM2.5 PM1.0\r");
                    writeArray(QByteArray("J "), sizes, 2);
                } else {
                    writeArray(QByteArray("Jc"), sizes, 2);
                }
            } else if (line == "M") {
                /* Ignore M commands right after an unpolled response */
                if (!FP::defined(unpolledRemaining) ||
                        (unpolledRemaining <= 5.0 && unpolledRemaining >= 1.0)) {
                    if (modelNumber.startsWith("180")) {
                        outgoing.append("Mean PM10:     0.0 ; PM2.5:     0.0 ; PM1:     0.0\r");
                    } else {
                        writeArray(QByteArray("Mc"), sizes);
                    }
                    outgoing.append("V: ");
                    outgoing.append(QByteArray::number(accumulatedVolume, 'f', 4));
                    outgoing.append(" m3\r");
                }
            } else if (line == "R") {
                unpolledSequence = -1;
                unpolledRemaining = 6.0;
            } else if (line == "S") {
                unpolledRemaining = FP::undefined();
            } else if (line == "V") {
                outgoing.append("Version: ");
                outgoing.append(firmwareVersion);
                outgoing.append('\r');
            } else if (line == "!") {
                outgoing.append("Model: ");
                outgoing.append(modelNumber);
                outgoing.append("  Version: ");
                outgoing.append(firmwareVersion);
                outgoing.append('\r');
            } else if (line == "@") {
                outgoing.append("Ser.No.: ");
                outgoing.append(serialNumber);
                outgoing.append('\r');
            } else {
                outgoing.append("ERROR\r");
            }

            if (idxCR >= incoming.size() - 1) {
                incoming.clear();
                break;
            }
            incoming = incoming.mid(idxCR + 1);
        }

        if (FP::defined(unpolledRemaining)) {
            accumulatedVolume += (flow * seconds) / (60.0 * 1000.0);

            unpolledRemaining -= seconds;
            while (unpolledRemaining <= 0.0) {
                if (unpolledSequence <= 0) {
                    outgoing.append("P: ");
                    outgoing.append(QByteArray::number(currentTime.date().year() % 100));
                    outgoing.append(' ');
                    outgoing.append(QByteArray::number(currentTime.date().month()));
                    outgoing.append(' ');
                    outgoing.append(QByteArray::number(currentTime.date().day()));
                    outgoing.append(' ');
                    outgoing.append(QByteArray::number(currentTime.time().hour()));
                    outgoing.append(' ');
                    outgoing.append(QByteArray::number(currentTime.time().minute()));
                    outgoing.append(" 0 "); /* Location */
                    outgoing.append(QByteArray::number((int) (gf * gfScale)));
                    outgoing.append(" 0 "); /* Error */
                    outgoing.append(QByteArray::number((int) battery));
                    outgoing.append(' ');
                    outgoing.append(QByteArray::number((int) pump));
                    outgoing.append(" 0 0 0 0 0\r");

                    outgoing.append("K: 0 0 0 0\r");

                    currentTime = currentTime.addSecs(60);
                }

                if (unpolledSequence >= 0) {
                    unpolledRemaining += 6.0;

                    if (modelNumber.startsWith("180")) {
                        outgoing.append("N");
                        outgoing.append(QByteArray::number(unpolledSequence));
                        outgoing.append(", ");
                        outgoing.append(
                                QByteArray::number(masses180[0] * 10.0, 'f', 0).leftJustified(4,
                                                                                              ' '));
                        outgoing.append(' ');
                        outgoing.append(
                                QByteArray::number(masses180[1] * 10.0, 'f', 0).leftJustified(4,
                                                                                              ' '));
                        outgoing.append(' ');
                        outgoing.append(
                                QByteArray::number(masses180[2] * 10.0, 'f', 0).leftJustified(4,
                                                                                              ' '));
                        outgoing.append('\r');
                    }

                    QByteArray id("C");
                    id.append(QByteArray::number(unpolledSequence));
                    writeArray(id, counts);

                    unpolledSequence = (unpolledSequence + 1) % 10;
                } else {
                    unpolledRemaining += 14.0;
                    unpolledSequence = 1;
                }
            }
        }
    }
};

class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    static Variant::Read findMassValue(StreamCapture &stream,
                                       const SequenceName::Component &archive,
                                       const SequenceName::Component &variable,
                                       const SequenceName::Flavors &flavors)
    {
        for (const auto &v : stream.values()) {
            if (v.getArchive() != archive)
                continue;
            if (v.getVariable() != variable)
                continue;
            if (v.getFlavors() != flavors)
                continue;
            return v.read();
        }
        return Variant::Read::empty();
    }

    bool checkMeta(StreamCapture &stream,
                   const ModelInstrument &model,
                   double time = FP::undefined(),
                   bool haveMasses = false)
    {
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("N", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Q", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Ns", Variant::Root(model.nChannels), "^Count", time))
            return false;
        if (!stream.hasMeta("Nb", Variant::Root(model.nChannels), "^Count", time))
            return false;
        if (haveMasses) {
            if (!findMassValue(stream, "raw_meta", "X", {"pm10"}).exists())
                return false;
            if (!findMassValue(stream, "raw_meta", "X", {"pm25"}).exists())
                return false;
            if (!findMassValue(stream, "raw_meta", "X", {"pm1"}).exists())
                return false;
        } else {
            if (stream.hasMeta("X", Variant::Root(), QString(), time))
                return false;
        }
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, const ModelInstrument &model,
                           double time = FP::undefined())
    {
        if (!stream.hasMeta("PCT1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("PCT2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZN", Variant::Root(model.nChannels), "^Count", time))
            return false;
        return true;
    }

    static bool checkMassContiguous(StreamCapture &stream,
                                    const SequenceName::Component &name,
                                    const SequenceName::Flavors &flavors)
    {
        double lastTime = CPD3::FP::undefined();
        for (const auto &v : stream.values()) {
            if (v.getVariable() != name)
                continue;
            if (v.getArchive() != "raw")
                continue;
            if (v.getFlavors() != flavors)
                continue;
            if (!CPD3::FP::defined(lastTime)) {
                lastTime = v.getEnd();
                continue;
            }
            if (CPD3::Range::compareStartEnd(v.getStart(), lastTime) != 0)
                return false;
            lastTime = v.getEnd();
        }
        return true;
    }

    bool checkContiguous(StreamCapture &stream, bool haveMasses = false)
    {
        if (!stream.checkContiguous("N"))
            return false;
        if (!stream.checkContiguous("Q"))
            return false;
        if (!stream.checkContiguous("Ns"))
            return false;
        if (!stream.checkContiguous("Nb"))
            return false;
        if (!stream.checkContiguous("F1"))
            return false;
        if (haveMasses) {
            if (!checkMassContiguous(stream, "X", {"pm10"}))
                return false;
            if (!checkMassContiguous(stream, "X", {"pm25"}))
                return false;
            if (!checkMassContiguous(stream, "X", {"pm1"}))
                return false;
        }
        return true;
    }

    bool checkValues(StreamCapture &stream, const ModelInstrument &model,
                     double time = FP::undefined(), bool haveFlow = true, bool haveMasses = false)
    {
        if (!stream.hasAnyMatchingValue("Nb", model.outputConcentrations(), time))
            return false;
        if (!stream.hasAnyMatchingValue("Ns", model.outputSizes(), time))
            return false;
        if (!stream.hasAnyMatchingValue("N", Variant::Root(model.totalConcentration()), time))
            return false;
        if (haveFlow && !stream.hasAnyMatchingValue("Q", Variant::Root(model.flow), time))
            return false;
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        if (haveMasses) {
            if (findMassValue(stream, "raw", "X", {"pm10"}).toReal() != model.masses180[0])
                return false;
            if (findMassValue(stream, "raw", "X", {"pm25"}).toReal() != model.masses180[1])
                return false;
            if (findMassValue(stream, "raw", "X", {"pm1"}).toReal() != model.masses180[2])
                return false;
        } else {
            if (stream.hasAnyMatchingValue("X", Variant::Root(), time))
                return false;
        }
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream, const ModelInstrument &model,
                             double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("PCT1", Variant::Root(model.battery), time))
            return false;
        if (!stream.hasAnyMatchingValue("PCT2", Variant::Root(model.pump), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZN", Variant::Root(model.outputCounts()), time))
            return false;
        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_grimm_opc110x"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_grimm_opc110x"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = 0.0;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 60; i++) {
            control.advance(3.0);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 60; i++) {
            control.advance(3.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument, FP::undefined(), false));
        QVERIFY(checkValues(realtime, instrument, FP::undefined(), false));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;

        instrument.unpolledRemaining = FP::undefined();
        instrument.modelNumber = "1.105";
        instrument.nChannels = 8;
        instrument.counts[0] *= 1.25;
        instrument.counts[1] *= 1.25;
        instrument.sizes[0] = 0.5;
        instrument.sizes[1] = 1.0;
        instrument.sizes[2] = 2.1;
        instrument.sizes[3] = 3.5;
        instrument.sizes[4] = 5.0;
        instrument.sizes[5] = 7.5;
        instrument.sizes[6] = 10.0;
        instrument.sizes[7] = 15.0;

        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 10; i++) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(1.0);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 120; i++) {
            control.advance(3.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

    void interactiveAutoprobe180()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;

        instrument.unpolledRemaining = FP::undefined();
        instrument.modelNumber = "180MC";
        instrument.firmwareVersion = "7.80 US";

        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 10; i++) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(1.0);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 120; i++) {
            control.advance(3.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging, true));

        QVERIFY(checkMeta(logging, instrument, FP::undefined(), true));
        QVERIFY(checkMeta(realtime, instrument, FP::undefined(), true));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument, FP::undefined(), true, true));
        QVERIFY(checkValues(realtime, instrument, FP::undefined(), true, true));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Model"].setString("1.108");
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = 0;
        instrument.nChannels = 15;
        instrument.sizes[0] = 0.30;
        instrument.sizes[1] = 0.40;
        instrument.sizes[2] = 0.50;
        instrument.sizes[3] = 0.65;
        instrument.sizes[4] = 0.80;
        instrument.sizes[5] = 1.0;
        instrument.sizes[6] = 1.6;
        instrument.sizes[7] = 2.0;
        instrument.sizes[8] = 3.0;
        instrument.sizes[9] = 4.0;
        instrument.sizes[10] = 5.0;
        instrument.sizes[11] = 7.5;
        instrument.sizes[12] = 10.0;
        instrument.sizes[13] = 15.0;
        instrument.sizes[14] = 20.0;

        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(3.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        control.advance(0.1);

        for (int i = 0; i < 60; i++) {
            control.advance(3.0);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        QVERIFY(checkValues(logging, instrument, FP::undefined(), false));
        QVERIFY(checkValues(realtime, instrument, FP::undefined(), false));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.unpolledRemaining = FP::undefined();
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 240.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 240.0);

        interface->setLoggingEgress(&logging);

        double checkTime = control.time();
        while (control.time() < 480.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        control.advance(3.0);
        checkTime = control.time();
        while (control.time() < 720.0) {
            control.advance(1.0);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 660.0);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging, instrument));
        QVERIFY(checkMeta(realtime, instrument));
        QVERIFY(checkRealtimeMeta(realtime, instrument));

        /* Don't check the values here because we can't control the timing,
         * so the flow is slightly off (or we'd have to make the test
         * case unreasonably long running) */
        /*QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));*/
        QVERIFY(checkRealtimeValues(realtime, instrument));

        Variant::Write source = Variant::Write::empty();
        source.hash("SerialNumber").setString(instrument.serialNumber.toStdString());
        source.hash("FirmwareVersion").setString(instrument.firmwareVersion.toStdString());
        source.hash("Model").setString(instrument.modelNumber.toStdString());
        source.hash("Manufacturer").setString("Grimm");
        QVERIFY(logging.hasAnyMatchingMetaValue("F1", source, FP::undefined(), "^Source"));

        int count = 0;
        for (const auto &v : logging.values()) {
            if (v.getArchive() != "raw_meta")
                continue;
            if (v.getVariable() != "Nb")
                continue;
            ++count;
        }
        QCOMPARE(count, 1);

    }

    void instrumentTimeConvert()
    {
        ComponentOptions options;
        options = ingress->getOptions();

        std::unique_ptr<ExternalConverter> input(ingress->createDataIngress(options));
        QVERIFY(input.get());
        input->start();

        ModelInstrument instrument;
        instrument.unpolledRemaining = 0;
        instrument.advance(3600.0);

        StreamCapture logging;
        input->setEgress(&logging);
        input->incomingData(instrument.outgoing);
        input->endData();
        input->wait(30000);
        input.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(logging.hasAnyMatchingValue("F1_N11", Variant::Root(), 1357121640.0));
        QVERIFY(logging.hasAnyMatchingValue("Nb_N11", Variant::Root(), 1357121640.0));
        QVERIFY(logging.hasAnyMatchingValue("Ns_N11", Variant::Root(), 1357121640.0));
        QVERIFY(logging.hasAnyMatchingValue("N_N11", Variant::Root(), 1357121640.0));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
