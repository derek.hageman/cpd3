/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <cctype>
#include <QRegularExpression>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "core/csv.hxx"
#include "core/timeutils.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/composite.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_ad_cpcmagic200.hxx"


using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

static const std::array<std::string, 17>
        instrumentFlagTranslation{"ConditionerTemperatureOutOfRange",   /* 0x0001 */
                                  "InitiatorTemperatureOutOfRange",     /* 0x0002 */
                                  "ModeratorTemperatureOutOfRange",     /* 0x0004 */
                                  "OpticsTemperatureOutOfRange",        /* 0x0008 */
                                  "LaserOff",                           /* 0x0010 */
                                  "PumpOff",                            /* 0x0020 */
                                  "RHDataStale",                        /* 0x0040 */
                                  "I2CCommunicationFailure",            /* 0x0080 */
                                  "RHSensorError",                      /* 0x0100 */
                                  "Overheat",                           /* 0x0200 */
                                  {},                                   /* 0x0400 */
                                  "ModeratorInAbsoluteMode",            /* 0x0800 */
                                  "WaterPumpActivated",                 /* 0x1000 */
                                  "InvalidFlashRecord",                 /* 0x2000 */
                                  "FlashFull",                          /* 0x4000 */
                                  "FRAMDataInvalid",                    /* 0x8000 */
                                  {},                                   /* 0x10000 */
};


AcquireADCPCMagic200::Configuration::Configuration() : start(FP::undefined()),
                                                       end(FP::undefined()),
                                                       strictMode(true),
                                                       calculateConcentration(false),
                                                       useMeasuredTime(false),
                                                       flow(FP::undefined()),
                                                       reportInterval(1.0)
{ }

AcquireADCPCMagic200::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          strictMode(other.strictMode),
          calculateConcentration(other.calculateConcentration),
          useMeasuredTime(other.useMeasuredTime),
          flow(other.flow),
          reportInterval(other.reportInterval)
{ }

void AcquireADCPCMagic200::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("AerosolDynamics");
    instrumentMeta["Model"].setString("Magic200");

    lastRecordTime = FP::undefined();
    autoprobeValidRecords = 0;

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    realtimeStateUpdated = false;

    lastReportedFlags.clear();
}

AcquireADCPCMagic200::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s),
          end(e),
          strictMode(true),
          calculateConcentration(false),
          useMeasuredTime(false),
          flow(FP::undefined()),
          reportInterval(1.0)
{
    setFromSegment(other);
}

AcquireADCPCMagic200::Configuration::Configuration(const Configuration &under,
                                                   const ValueSegment &over,
                                                   double s,
                                                   double e) : start(s),
                                                               end(e),
                                                               strictMode(under.strictMode),
                                                               calculateConcentration(
                                                                       under.calculateConcentration),
                                                               useMeasuredTime(
                                                                       under.useMeasuredTime),
                                                               flow(under.flow),
                                                               reportInterval(under.reportInterval)
{
    setFromSegment(over);
}

void AcquireADCPCMagic200::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["StrictMode"].exists())
        strictMode = config["StrictMode"].toBool();

    if (config["CalculateConcentration"].exists())
        calculateConcentration = config["CalculateConcentration"].toBool();

    if (config["UseMeasuredTime"].exists())
        useMeasuredTime = config["UseMeasuredTime"].toBool();

    if (config["Flow"].exists())
        flow = config["Flow"].toReal();

    {
        double v = config["ReportInterval"].toReal();
        if (FP::defined(v) && v > 0.0)
            reportInterval = v;
    }
}

AcquireADCPCMagic200::AcquireADCPCMagic200(const ValueSegment::Transfer &configData,
                                           const std::string &loggingContext) : FramedInstrument(
        "magic200", loggingContext),
                                                                                lastRecordTime(
                                                                                        FP::undefined()),
                                                                                autoprobeStatus(
                                                                                        AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                                autoprobeValidRecords(
                                                                                        0),
                                                                                responseState(
                                                                                        ResponseState::Passive_Initialize)
{
    setDefaultInvalid();
    config.emplace_back();

    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
}

void AcquireADCPCMagic200::setToAutoprobe()
{
    responseState = ResponseState::Autoprobe_Passive_Initialize;
}

void AcquireADCPCMagic200::setToInteractive()
{ responseState = ResponseState::Interactive_Initialize; }


ComponentOptions AcquireADCPCMagic200Component::getBaseOptions()
{
    ComponentOptions options;

    return options;
}

AcquireADCPCMagic200::AcquireADCPCMagic200(const ComponentOptions &,
                                           const std::string &loggingContext) : FramedInstrument(
        "magic200", loggingContext),
                                                                                lastRecordTime(
                                                                                        FP::undefined()),
                                                                                autoprobeStatus(
                                                                                        AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                                autoprobeValidRecords(
                                                                                        0),
                                                                                responseState(
                                                                                        ResponseState::Passive_Initialize)
{
    setDefaultInvalid();
    config.emplace_back();
}

AcquireADCPCMagic200::~AcquireADCPCMagic200() = default;

void AcquireADCPCMagic200::invalidateLogValues(double frameTime)
{
    autoprobeValidRecords = 0;
    lastRecordTime = FP::undefined();
    loggingLost(frameTime);
}


SequenceValue::Transfer AcquireADCPCMagic200::buildLogMeta(double time) const
{
    Q_ASSERT(!config.empty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_ad_cpcmagic200");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    if (instrumentMeta["FirmwareVersion"].exists())
        processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    if (instrumentMeta["SerialNumber"].exists())
        processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);

    result.emplace_back(SequenceName({}, "raw_meta", "N"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0");
    result.back().write().metadataReal("Units").setString("cm\xE2\x81\xBB³");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Condensation nuclei concentration");
    if (FP::defined(config.front().flow)) {
        result.back().write().metadataReal("SampleFlow").setReal(config.front().flow);
    }
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Concentration"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(0);

    result.emplace_back(SequenceName({}, "raw_meta", "P"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Sample pressure");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Pressure"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(5);

    result.emplace_back(SequenceName({}, "raw_meta", "Pd"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Orifice pressure drop");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Orifice"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(6);

    result.emplace_back(SequenceName({}, "raw_meta", "Q"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Sample flow rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Flow"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(3);

    result.emplace_back(SequenceName({}, "raw_meta", "Tu"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Inlet temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Inlet"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Box")
          .setString(QObject::tr("Temperature"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("1");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(0);

    result.emplace_back(SequenceName({}, "raw_meta", "T1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Conditioner temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Conditioner"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Box")
          .setString(QObject::tr("Temperature"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("1");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(1);

    result.emplace_back(SequenceName({}, "raw_meta", "T2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Initiator temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Initiator"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Box")
          .setString(QObject::tr("Temperature"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("1");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(2);

    result.emplace_back(SequenceName({}, "raw_meta", "T3"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Moderator temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Moderator"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Box")
          .setString(QObject::tr("Temperature"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("1");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(3);

    result.emplace_back(SequenceName({}, "raw_meta", "T4"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Optics head temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Optics"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Box")
          .setString(QObject::tr("Temperature"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("1");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(4);

    result.emplace_back(SequenceName({}, "raw_meta", "T5"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Heat sink temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Heat sink"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Box")
          .setString(QObject::tr("Temperature"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("1");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(5);

    result.emplace_back(SequenceName({}, "raw_meta", "T6"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("PCB temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("PCB"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Box")
          .setString(QObject::tr("Temperature"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("1");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(6);

    result.emplace_back(SequenceName({}, "raw_meta", "T7"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Cabinet temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Cabinet"));
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Box")
          .setString(QObject::tr("Temperature"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("1");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(7);

    result.emplace_back(SequenceName({}, "raw_meta", "Uu"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Inlet RH");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Inlet"));
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Humidity"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("2");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(2);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(0);

    result.emplace_back(SequenceName({}, "raw_meta", "TDu"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Inlet dewpoint");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Dewpoint"));
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Humidity"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("2");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(2);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(1);

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataSingleFlag("ConditionerTemperatureOutOfRange")
          .hash("Description")
          .setString("Conditioner (1st stage) temperature out of range");
    result.back()
          .write()
          .metadataSingleFlag("ConditionerTemperatureOutOfRange")
          .hash("Bits")
          .setInteger(0x00010000);
    result.back()
          .write()
          .metadataSingleFlag("ConditionerTemperatureOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ad_cpcmagic200");

    result.back()
          .write()
          .metadataSingleFlag("InitiatorTemperatureOutOfRange")
          .hash("Description")
          .setString("Initiator (2nd stage) temperature out of range");
    result.back()
          .write()
          .metadataSingleFlag("InitiatorTemperatureOutOfRange")
          .hash("Bits")
          .setInteger(0x00020000);
    result.back()
          .write()
          .metadataSingleFlag("InitiatorTemperatureOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ad_cpcmagic200");

    result.back()
          .write()
          .metadataSingleFlag("ModeratorTemperatureOutOfRange")
          .hash("Description")
          .setString("Moderator (3rd stage) temperature out of range");
    result.back()
          .write()
          .metadataSingleFlag("ModeratorTemperatureOutOfRange")
          .hash("Bits")
          .setInteger(0x00040000);
    result.back()
          .write()
          .metadataSingleFlag("ModeratorTemperatureOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ad_cpcmagic200");

    result.back()
          .write()
          .metadataSingleFlag("OpticsTemperatureOutOfRange")
          .hash("Description")
          .setString("Optics head temperature out of range");
    result.back()
          .write()
          .metadataSingleFlag("OpticsTemperatureOutOfRange")
          .hash("Bits")
          .setInteger(0x00080000);
    result.back()
          .write()
          .metadataSingleFlag("OpticsTemperatureOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ad_cpcmagic200");

    result.back().write().metadataSingleFlag("LaserOff").hash("Description").setString("Laser off");
    result.back().write().metadataSingleFlag("LaserOff").hash("Bits").setInteger(0x00100000);
    result.back()
          .write()
          .metadataSingleFlag("LaserOff")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ad_cpcmagic200");

    result.back().write().metadataSingleFlag("PumpOff").hash("Description").setString("Pump off");
    result.back().write().metadataSingleFlag("PumpOff").hash("Bits").setInteger(0x00200000);
    result.back()
          .write()
          .metadataSingleFlag("PumpOff")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ad_cpcmagic200");

    result.back()
          .write()
          .metadataSingleFlag("RHDataStale")
          .hash("Description")
          .setString("RH data not updated since the last read");
    result.back().write().metadataSingleFlag("RHDataStale").hash("Bits").setInteger(0x00400000);
    result.back()
          .write()
          .metadataSingleFlag("RHDataStale")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ad_cpcmagic200");

    result.back()
          .write()
          .metadataSingleFlag("I2CCommunicationFailure")
          .hash("Description")
          .setString("I²C communication error");
    result.back()
          .write()
          .metadataSingleFlag("I2CCommunicationFailure")
          .hash("Bits")
          .setInteger(0x00800000);
    result.back()
          .write()
          .metadataSingleFlag("I2CCommunicationFailure")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ad_cpcmagic200");

    result.back()
          .write()
          .metadataSingleFlag("RHSensorError")
          .hash("Description")
          .setString("RH sensor error");
    result.back().write().metadataSingleFlag("RHSensorError").hash("Bits").setInteger(0x01000000);
    result.back()
          .write()
          .metadataSingleFlag("RHSensorError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ad_cpcmagic200");

    result.back()
          .write()
          .metadataSingleFlag("Overheat")
          .hash("Description")
          .setString("Overheating detected, TECs and optics turned off");
    result.back().write().metadataSingleFlag("Overheat").hash("Bits").setInteger(0x02000000);
    result.back()
          .write()
          .metadataSingleFlag("Overheat")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ad_cpcmagic200");

    result.back()
          .write()
          .metadataSingleFlag("ModeratorInAbsoluteMode")
          .hash("Description")
          .setString("Moderator switched to absolute mode due to RH sensor failure");
    result.back()
          .write()
          .metadataSingleFlag("ModeratorInAbsoluteMode")
          .hash("Bits")
          .setInteger(0x08000000);
    result.back()
          .write()
          .metadataSingleFlag("ModeratorInAbsoluteMode")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ad_cpcmagic200");

    result.back()
          .write()
          .metadataSingleFlag("WaterPumpActivated")
          .hash("Description")
          .setString("Water extraction pump activated");
    result.back()
          .write()
          .metadataSingleFlag("WaterPumpActivated")
          .hash("Bits")
          .setInteger(0x10000000);
    result.back()
          .write()
          .metadataSingleFlag("WaterPumpActivated")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ad_cpcmagic200");

    result.back()
          .write()
          .metadataSingleFlag("InvalidFlashRecord")
          .hash("Description")
          .setString("Invalid flash record");
    result.back().write().metadataSingleFlag("InvalidFlashRecord").hash("Bits")
          .setInteger(0x20000000);
    result.back()
          .write()
          .metadataSingleFlag("InvalidFlashRecord")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ad_cpcmagic200");

    result.back()
          .write()
          .metadataSingleFlag("FlashFull")
          .hash("Description")
          .setString("Flash memory full");
    result.back().write().metadataSingleFlag("FlashFull").hash("Bits").setInteger(0x40000000);
    result.back()
          .write()
          .metadataSingleFlag("FlashFull")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ad_cpcmagic200");

    result.back()
          .write()
          .metadataSingleFlag("FRAMDataInvalid")
          .hash("Description")
          .setString("FRAM data invalid");
    result.back().write().metadataSingleFlag("FRAMDataInvalid").hash("Bits").setInteger(0x80000000);
    result.back()
          .write()
          .metadataSingleFlag("FRAMDataInvalid")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_ad_cpcmagic200");

    return result;
}

SequenceValue::Transfer AcquireADCPCMagic200::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_ad_cpcmagic200");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    if (instrumentMeta["FirmwareVersion"].exists())
        processing["FirmwareVersion"].set(instrumentMeta["FirmwareVersion"]);
    if (instrumentMeta["SerialNumber"].exists())
        processing["SerialNumber"].set(instrumentMeta["SerialNumber"]);


    result.emplace_back(SequenceName({}, "raw_meta", "V"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Supply voltage");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Supply"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(7);

    result.emplace_back(SequenceName({}, "raw_meta", "C1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Lower threshold count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Lower"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(1);

    result.emplace_back(SequenceName({}, "raw_meta", "C2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000000");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Upper threshold count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Upper"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(2);

    result.emplace_back(SequenceName({}, "raw_meta", "ZQ"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Reported flow rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Display flow"));
    result.back().write().metadataReal("Realtime").hash("BoxID").setString("0");
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("SortOrder").setInteger(4);


    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Realtime").hash("Name").setString(std::string());
    result.back().write().metadataString("Realtime").hash("Units").setString(std::string());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back().write().metadataString("Realtime").hash("Box").setString(QObject::tr("Humidity"));
    result.back().write().metadataString("Realtime").hash("BoxID").setString("2");
    result.back().write().metadataString("Realtime").hash("RowOrder").setInteger(2);
    result.back().write().metadataString("Realtime").hash("SortOrder").setInteger(10);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Run")
          .setString(QObject::tr("", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Zero")
          .setString(QObject::tr("Zero in progress", "zero state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Spancheck")
          .setString(QObject::tr("Spancheck in progress", "spancheck state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Calibration")
          .setString(QObject::tr("Calibration in progress", "calibration state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Blank")
          .setString(QObject::tr("Blank in progress", "spancheck state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Warmup")
          .setString(QObject::tr("Warming up", "warmup state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidRecord")
          .setString(QObject::tr("NO COMMS: Invalid record"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidResponse")
          .setString(QObject::tr("NO COMMS: Invalid response"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Timeout")
          .setString(QObject::tr("NO COMMS: Timeout"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractive")
          .setString(QObject::tr("STARTING COMMS"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractivePoll")
          .setString(QObject::tr("STARTING COMMS: Polling concentration"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveReadVersion")
          .setString(QObject::tr("STARTING COMMS: Reading instrument version"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetTime")
          .setString(QObject::tr("STARTING COMMS: Setting instrument time"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetDate")
          .setString(QObject::tr("STARTING COMMS: Setting instrument date"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveFirstValid")
          .setString(QObject::tr("STARTING COMMS: Waiting for a valid record"));

    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("ConditionerTemperatureOutOfRange")
          .setString(QObject::tr("Conditioner (1st stage) temperature out of range"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InitiatorTemperatureOutOfRange")
          .setString(QObject::tr("Initiator (2nd stage) temperature out of range"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("ModeratorTemperatureOutOfRange")
          .setString(QObject::tr("Moderator (3rd stage) temperature out of range"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("OpticsTemperatureOutOfRange")
          .setString(QObject::tr("Optics head temperature out of range"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("LaserOff")
          .setString(QObject::tr("Laser off"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("PumpOff")
          .setString(QObject::tr("Pump off"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("RHDataStale")
          .setString(QObject::tr("RH data not updated since the last read"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("I2CCommunicationFailure")
          .setString(QObject::tr("I²C communication error"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("RHSensorError")
          .setString(QObject::tr("RH sensor error"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Overheat")
          .setString(QObject::tr("Overheating detected, TECs and optics turned off"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("ModeratorInAbsoluteMode")
          .setString(QObject::tr("Moderator switched to absolute mode due to RH sensor failure"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("WaterPumpActivated")
          .setString(QObject::tr("Water extraction pump activated"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidFlashRecord")
          .setString(QObject::tr("Invalid flash record"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("FlashFull")
          .setString(QObject::tr("Flash memory full"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("FRAMDataInvalid")
          .setString(QObject::tr("FRAM data invalid"));


    return result;
}

SequenceMatch::Composite AcquireADCPCMagic200::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "ZSTATE");
    return sel;
}

void AcquireADCPCMagic200::logValue(double startTime,
                                    double endTime,
                                    SequenceName::Component name,
                                    Variant::Root &&value)
{
    if (!loggingEgress && !realtimeEgress)
        return;
    SequenceValue dv({{}, "raw", std::move(name)}, std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        if (FP::defined(startTime))
            loggingEgress->incomingData(std::move(dv));
        return;
    }
    if (loggingEgress && FP::defined(startTime))
        loggingEgress->incomingData(dv);
    if (FP::defined(endTime)) {
        dv.setStart(endTime);
        dv.setEnd(endTime + 1.0);
    } else if (FP::defined(startTime)) {
        dv.setEnd(startTime + 1.0);
    }
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireADCPCMagic200::realtimeValue(double time,
                                         SequenceName::Component name,
                                         Variant::Root &&value)
{
    if (!realtimeEgress)
        return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

static double correctRate(double N, double instrument, double output)
{
    if (!FP::defined(N) || !FP::defined(instrument) || !FP::defined(output))
        return N;
    if (instrument == 0.0 || output == 0.0)
        return N;
    return N * (instrument / output);
}

static double calculateCountRate(double lower, double upper, double elapsed, double live)
{
    if (!FP::defined(elapsed) || !FP::defined(live))
        return FP::undefined();
    if (elapsed <= 0.0 || live <= 0.0)
        return FP::undefined();
    double N = lower;
    if (!FP::defined(N)) {
        N = upper;
    } else if (FP::defined(upper)) {
        N = std::max(N, upper);
    }
    if (!FP::defined(N))
        return FP::undefined();
    return N / (elapsed * live);
}

static double calculateConcentration(double N, double Q)
{
    if (!FP::defined(N) || !FP::defined(Q))
        return FP::undefined();
    if (Q <= 0.0)
        return FP::undefined();
    return N / (Q * 1000.0 / 60.0);
}

int AcquireADCPCMagic200::processRecord(const Util::ByteArray &line, double &frameTime)
{
    Q_ASSERT(!config.empty());

    if (line.size() < 3)
        return 1;

    auto fields = Util::as_deque(line.split(','));
    bool ok = false;

    if (fields.empty()) return 2;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    auto spacefields = Util::as_deque(field.split(' '));
    if (spacefields.size() != 2) return 3;
    auto subfields = Util::as_deque(spacefields.front().string_trimmed().split('/'));
    spacefields.pop_front();
    if (subfields.size() != 3) return 4;

    Q_ASSERT(!subfields.empty());
    qint64 iyear = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 5;
    if (iyear < 1900) {
        if (iyear > 99) return 6;
        if (iyear < 0) return 7;
        iyear += 2000;
    }
    if (iyear > 2999) return 8;
    Variant::Root year(iyear);
    remap("YEAR", year);
    iyear = year.read().toInt64();

    Q_ASSERT(!subfields.empty());
    qint64 imonth = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 9;
    if (imonth < 1) return 10;
    if (imonth > 12) return 11;
    Variant::Root month(imonth);
    remap("MONTH", month);
    imonth = month.read().toInt64();

    Q_ASSERT(!subfields.empty());
    qint64 iday = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 12;
    if (iday < 1) return 13;
    if (iday > 31) return 14;
    Variant::Root day(iday);
    remap("DAY", day);
    iday = day.read().toInt64();

    Q_ASSERT(!spacefields.empty());
    subfields = Util::as_deque(spacefields.front().string_trimmed().split(':'));
    spacefields.pop_front();
    if (subfields.size() != 3) return 15;

    Q_ASSERT(!subfields.empty());
    qint64 ihour = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 16;
    if (ihour < 0) return 17;
    if (ihour > 23) return 18;
    Variant::Root hour(ihour);
    remap("HOUR", hour);
    ihour = hour.read().toInt64();

    Q_ASSERT(!subfields.empty());
    qint64 iminute = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 19;
    if (iminute < 0) return 20;
    if (iminute > 59) return 21;
    Variant::Root minute(iminute);
    remap("MINUTE", minute);
    iminute = minute.read().toInt64();

    Q_ASSERT(!subfields.empty());
    qint64 isecond = subfields.front().string_trimmed().parse_i32(&ok);
    subfields.pop_front();
    if (!ok) return 22;
    if (isecond < 0) return 23;
    if (isecond > 60) return 24;
    Variant::Root second(isecond);
    remap("SECOND", second);
    isecond = second.read().toInt64();

    if (!FP::defined(frameTime) && INTEGER::defined(iyear) && iyear >= 1900 && iyear <= 2999 &&
            INTEGER::defined(imonth) &&
            imonth >= 1 &&
            imonth <= 12 &&
            INTEGER::defined(iday) &&
            iday >= 1 &&
            iday <= 31 &&
            INTEGER::defined(ihour) &&
            ihour >= 0 &&
            ihour <= 23 &&
            INTEGER::defined(iminute) &&
            iminute >= 0 &&
            iminute <= 59 &&
            INTEGER::defined(isecond) &&
            isecond >= 0 && isecond <= 60) {
        frameTime = Time::fromDateTime(QDateTime(
                QDate(static_cast<int>(iyear), static_cast<int>(imonth), static_cast<int>(iday)),
                QTime(static_cast<int>(ihour), static_cast<int>(iminute),
                      static_cast<int>(isecond)), Qt::UTC));
        configurationAdvance(frameTime);
    }
    if (FP::defined(lastRecordTime) && FP::defined(frameTime) && frameTime < lastRecordTime)
        return -1;

    if (fields.empty()) return 25;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root N(field.parse_real(&ok));
    if (!ok) return 26;
    if (!FP::defined(N.read().toReal())) return 27;
    remap("N", N);

    if (fields.empty()) return 28;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root TDu(field.parse_real(&ok));
    if (!ok) return 29;
    if (!FP::defined(TDu.read().toReal())) return 30;
    remap("TDu", TDu);

    if (fields.empty()) return 31;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Tu(field.parse_real(&ok));
    if (!ok) return 32;
    if (!FP::defined(Tu.read().toReal())) return 33;
    remap("Tu", Tu);

    if (fields.empty()) return 34;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Uu(field.parse_real(&ok));
    if (!ok) return 35;
    if (!FP::defined(Uu.read().toReal())) return 36;
    remap("Uu", Uu);

    if (fields.empty()) return 37;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T1(field.parse_real(&ok));
    if (!ok) return 38;
    if (!FP::defined(T1.read().toReal())) return 39;
    remap("T1", T1);

    if (fields.empty()) return 40;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T2(field.parse_real(&ok));
    if (!ok) return 41;
    if (!FP::defined(T2.read().toReal())) return 42;
    remap("T2", T2);

    if (fields.empty()) return 43;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T3(field.parse_real(&ok));
    if (!ok) return 44;
    if (!FP::defined(T3.read().toReal())) return 45;
    remap("T3", T3);

    if (fields.empty()) return 46;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T4(field.parse_real(&ok));
    if (!ok) return 47;
    if (!FP::defined(T4.read().toReal())) return 48;
    remap("T4", T4);

    if (fields.empty()) return 49;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T5(field.parse_real(&ok));
    if (!ok) return 50;
    if (!FP::defined(T5.read().toReal())) return 51;
    remap("T5", T5);

    if (fields.empty()) return 51;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T6(field.parse_real(&ok));
    if (!ok) return 52;
    if (!FP::defined(T6.read().toReal())) return 53;
    remap("T6", T6);

    if (fields.empty()) return 54;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T7(field.parse_real(&ok));
    if (!ok) return 55;
    if (!FP::defined(T7.read().toReal())) return 56;
    remap("T7", T7);

    if (fields.empty()) return 57;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root V(field.parse_real(&ok));
    if (!ok) return 58;
    if (!FP::defined(V.read().toReal())) return 59;
    remap("V", V);

    if (fields.empty()) return 60;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Pd(field.parse_real(&ok));
    if (!ok) return 61;
    if (!FP::defined(Pd.read().toReal())) return 62;
    remap("Pd", Pd);

    if (fields.empty()) return 63;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root P(field.parse_real(&ok));
    if (!ok) return 64;
    if (!FP::defined(P.read().toReal())) return 65;
    remap("P", P);

    if (fields.empty()) return 66;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Q;
    {
        double raw = field.parse_real(&ok);
        if (!ok) return 67;
        if (!FP::defined(raw)) return 68;
        raw /= 1000;
        Q.write().setReal(raw);
    }
    Variant::Root ZQ = Q;
    remap("ZQ", ZQ);
    if (FP::defined(config.front().flow))
        Q.write().setDouble(config.front().flow);
    remap("Q", Q);

    if (fields.empty()) return 69;
    field = fields.front().string_trimmed();
    fields.pop_front();
    double elapsed = field.parse_real(&ok);
    if (!ok) return 70;
    if (!FP::defined(elapsed)) return 71;

    if (fields.empty()) return 72;
    field = fields.front().string_trimmed();
    fields.pop_front();
    double liveFraction = field.parse_real(&ok);
    if (!ok) return 73;
    if (!FP::defined(elapsed)) return 74;
    liveFraction /= 10000;

    /* Dead time, ignored */
    if (fields.empty()) return 75;
    fields.pop_front();

    if (fields.empty()) return 76;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root C1(field.parse_real(&ok));
    if (!ok) return 77;
    if (!FP::defined(C1.read().toReal())) return 78;
    remap("C1", C1);

    if (fields.empty()) return 79;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root C2(field.parse_real(&ok));
    if (!ok) return 80;
    if (!FP::defined(C2.read().toReal())) return 81;
    remap("C2", C2);

    if (fields.empty()) return 82;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root flags(static_cast<std::int_fast64_t>(field.parse_u16(&ok, 16)));
    if (!ok) return 83;
    if (!INTEGER::defined(flags.read().toInteger())) return 84;
    remap("FRAW", flags);
    {
        auto prior = std::move(lastReportedFlags);
        lastReportedFlags.clear();
        auto flagsBits = flags.read().toInteger();
        std::int_fast64_t bit = 1;
        for (const auto &flag : instrumentFlagTranslation) {
            if (flagsBits & bit) {
                if (!flag.empty()) {
                    lastReportedFlags.insert(flag);
                } else {
                    qCDebug(log) << "Unrecognized bit" << hex << bit << "set in instrument flags";
                }
            }
            bit <<= 1;
        }
        if (prior != lastReportedFlags)
            realtimeStateUpdated = true;
    }

    /* Flags description, ignored */
    if (fields.empty()) return 85;
    fields.pop_front();

    if (fields.empty()) return 86;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.empty()) return 87;
    std::int_fast64_t sn = field.parse_i64(&ok);
    if (ok && INTEGER::defined(sn) && sn > 0) {
        if (sn != instrumentMeta["SerialNumber"].toInteger()) {
            haveEmittedRealtimeMeta = false;
            haveEmittedLogMeta = false;
            instrumentMeta["SerialNumber"].setInteger(sn);
        }
    } else {
        auto snString = field.toString();
        if (snString != instrumentMeta["SerialNumber"].toString()) {
            haveEmittedRealtimeMeta = false;
            haveEmittedLogMeta = false;
            instrumentMeta["SerialNumber"].setString(snString);
        }
    }

    if (!fields.empty()) {
        if (config.front().strictMode)
            return 999;
    }

    double measuredElapsed = frameTime - lastRecordTime;
    double startTime = lastRecordTime;
    double endTime = frameTime;
    lastRecordTime = frameTime;

    if (!FP::defined(startTime) || !FP::defined(endTime))
        return 0;

    N.write().setReal(correctRate(N.read().toReal(), ZQ.read().toReal(), Q.read().toReal()));

    if (config.front().calculateConcentration) {
        N.write()
         .setReal(calculateConcentration(
                 calculateCountRate(C1.read().toReal(), C2.read().toReal(), elapsed, liveFraction),
                 Q.read().toReal()));
        remap("N", N);
    }

    if (config.front().useMeasuredTime) {
        N.write().setReal(correctRate(N.read().toReal(), elapsed, measuredElapsed));
        remap("N", N);
    }

    if (!haveEmittedLogMeta && loggingEgress) {
        haveEmittedLogMeta = true;

        loggingEgress->incomingData(buildLogMeta(startTime));
    }

    if (!haveEmittedRealtimeMeta && realtimeEgress) {
        haveEmittedRealtimeMeta = true;

        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }

    logValue(startTime, endTime, "F1", Variant::Root(lastReportedFlags));
    logValue(startTime, endTime, "N", std::move(N));
    logValue(startTime, endTime, "Tu", std::move(Tu));
    logValue(startTime, endTime, "Uu", std::move(Uu));
    logValue(startTime, endTime, "TDu", std::move(TDu));
    logValue(startTime, endTime, "T1", std::move(T1));
    logValue(startTime, endTime, "T2", std::move(T2));
    logValue(startTime, endTime, "T3", std::move(T3));
    logValue(startTime, endTime, "T4", std::move(T4));
    logValue(startTime, endTime, "T5", std::move(T5));
    logValue(startTime, endTime, "T6", std::move(T6));
    logValue(startTime, endTime, "T7", std::move(T7));
    logValue(startTime, endTime, "P", std::move(P));
    logValue(startTime, endTime, "Pd", std::move(Pd));
    logValue(startTime, endTime, "Q", std::move(Q));

    realtimeValue(frameTime, "V", std::move(V));
    realtimeValue(frameTime, "C1", std::move(C1));
    realtimeValue(frameTime, "C2", std::move(C2));
    realtimeValue(frameTime, "ZQ", std::move(ZQ));

    if (realtimeStateUpdated && realtimeEgress != NULL && FP::defined(frameTime)) {
        realtimeStateUpdated = false;

        auto reportFlags = lastReportedFlags;
        reportFlags.erase("RHDataStale");

        if (!reportFlags.empty()) {
            if (lastReportedFlags.size() == 1) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        *reportFlags.begin()), frameTime, FP::undefined()));
            } else {
                QStringList sorted;
                for (const auto &add : reportFlags) {
                    sorted.append(QString::fromStdString(add));
                }
                std::sort(sorted.begin(), sorted.end());
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        QString("Status: %1").arg(sorted.join(','))), frameTime, FP::undefined()));
            }
        } else {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                  FP::undefined()));
        }
    }

    return 0;
}

void AcquireADCPCMagic200::configurationAdvance(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.empty());
}

static bool isValidHeader(const QString &line)
{
    static const std::vector<QStringList> identifiers = {{"Concentration",    "Dewpoint",       "Input"},
                                                         {"Init",             "Mod",            "Opt",           "HeatSink"},
                                                         {"Power Supply",     "Diff. Press",    "flow"},
                                                         {"log interval",     "corrected live", "measured dead", "raw counts2"},
                                                         {"Status(hex code)", "Serial Number"}};
    for (const auto &checkParts: identifiers) {
        bool allOk = true;
        for (const auto &substr: checkParts) {
            if (!line.contains(substr, Qt::CaseInsensitive)) {
                allOk = false;
                break;
            }
        }
        if (allOk)
            return true;
    }

    return false;
}

void AcquireADCPCMagic200::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    configurationAdvance(frameTime);

    switch (responseState) {
    case ResponseState::Passive_Run: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + config.front().reportInterval + 5.0);
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveRun");
            timeoutAt(FP::undefined());
            responseState = ResponseState::Passive_Wait;
            generalStatusUpdated();

            info.hash("Code").setInteger(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
        } /* Ignored line */
        break;
    }

    case ResponseState::Unpolled_Run: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + config.front().reportInterval + 5.0);
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("UnpolledRun");
            timeoutAt(FP::undefined());
            if (controlStream != NULL) {
                controlStream->writeControl("\r\rLog,0\r");
            }
            responseState = ResponseState::Interactive_Start_Flush;
            discardData(frameTime + 1.0);
            info.hash("ResponseState").setString("Run");
            generalStatusUpdated();

            info.hash("Code").setInteger(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
        } /* Ignored line */
        break;
    }


    case ResponseState::Autoprobe_Passive_Initialize:
    case ResponseState::Autoprobe_Passive_Wait: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            if (++autoprobeValidRecords > 5) {
                qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

                timeoutAt(frameTime + config.front().reportInterval + 5.0);

                responseState = ResponseState::Passive_Run;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                realtimeStateUpdated = true;

                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("PassiveAutoprobeWait");
                event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);
            }
        } else if (code > 0) {
            invalidateLogValues(frameTime);
        }
        break;
    }

    case ResponseState::Passive_Initialize:
    case ResponseState::Passive_Wait: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);
            responseState = ResponseState::Passive_Run;
            generalStatusUpdated();

            ++autoprobeValidRecords;
            timeoutAt(frameTime + config.front().reportInterval + 5.0);

            realtimeStateUpdated = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveWait");
            event(frameTime, QObject::tr("Passive communications established."), false, info);
        } else if (code > 0) {
            invalidateLogValues(frameTime);
        }
        break;
    }

    case ResponseState::Interactive_Start_Header_Initial: {
        auto line = frame.toQByteArray().trimmed();

        /* Ignore echo */
        if (line.isEmpty())
            break;
        if (line.startsWith("hdr"))
            break;
    }

        responseState = ResponseState::Interactive_Start_Header_Continue;
        timeoutAt(frameTime + 1.0);

        /* Fall through */
    case ResponseState::Interactive_Start_Header_Continue: {
        auto str = QString::fromUtf8(frame.toQByteArray().trimmed());

        if (str.startsWith("ERROR", Qt::CaseInsensitive) || !isValidHeader(str)) {
            qCDebug(log) << "Invalid header response" << frame << "during startup at"
                         << Logging::time(frameTime);

            invalidateLogValues(frameTime);

            responseState = ResponseState::Interactive_Restart_Wait;
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            autoprobeValidRecords = 0;
            generalStatusUpdated();

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
            return;
        }
        break;
    }

    case ResponseState::Interactive_Start_ReadVersion_Initial: {
        auto line = frame.toQByteArray().trimmed();

        /* Ignore echo */
        if (line.isEmpty())
            break;
        if (line.startsWith("rv"))
            break;
    }

        responseState = ResponseState::Interactive_Start_ReadVersion_Continue;
        timeoutAt(frameTime + 1.0);

        /* Fall through */
    case ResponseState::Interactive_Start_ReadVersion_Continue: {
        auto str = QString::fromUtf8(frame.toQByteArray().trimmed());
        if (str.startsWith("ERROR", Qt::CaseInsensitive)) {
            qCDebug(log) << "Invalid version response" << frame << "during startup at"
                         << Logging::time(frameTime);

            invalidateLogValues(frameTime);

            responseState = ResponseState::Interactive_Restart_Wait;
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            autoprobeValidRecords = 0;
            generalStatusUpdated();

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
            return;
        }

        if (str.startsWith("Serial Number:", Qt::CaseInsensitive)) {
            auto snString = str.mid(14).trimmed();
            bool ok = false;
            std::int_fast64_t sn = snString.toLongLong(&ok);
            if (ok && INTEGER::defined(sn) && sn > 0) {
                if (sn != instrumentMeta["SerialNumber"].toInteger()) {
                    haveEmittedRealtimeMeta = false;
                    haveEmittedLogMeta = false;
                    instrumentMeta["SerialNumber"].setInteger(sn);
                }
            } else {
                if (snString != instrumentMeta["SerialNumber"].toQString()) {
                    haveEmittedRealtimeMeta = false;
                    haveEmittedLogMeta = false;
                    instrumentMeta["SerialNumber"].setString(snString);
                }
            }
        } else if (str.startsWith("FW Ver:", Qt::CaseInsensitive)) {
            auto fwString = str.mid(7).trimmed();

            if (fwString != instrumentMeta["FirmwareVersion"].toQString()) {
                haveEmittedRealtimeMeta = false;
                haveEmittedLogMeta = false;
                instrumentMeta["FirmwareVersion"].setString(fwString);
            }
        }
        break;
    }

    case ResponseState::Interactive_Start_SetTime: {
        auto line = frame.toQByteArray().trimmed();

        /* Ignore echo */
        if (line.isEmpty())
            break;
        if (line.startsWith("rtc,"))
            break;

        if (line != "OK") {
            qCDebug(log) << "Invalid time set response" << frame << "during startup at"
                         << Logging::time(frameTime);

            invalidateLogValues(frameTime);

            responseState = ResponseState::Interactive_Restart_Wait;
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            autoprobeValidRecords = 0;
            generalStatusUpdated();

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
            return;
        }

        responseState = ResponseState::Interactive_Start_SetDate;
        timeoutAt(frameTime + 2.0);

        if (controlStream) {
            auto dt = Time::toDateTime(startupSetTime + 0.5);
            Util::ByteArray send("rtc,");
            send += QByteArray::number(dt.date().year() % 100).rightJustified(2, '0');
            send += "/";
            send += QByteArray::number(dt.date().month()).rightJustified(2, '0');
            send += "/";
            send += QByteArray::number(dt.date().day()).rightJustified(2, '0');
            send += "\r";
            controlStream->writeControl(std::move(send));
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveSetDate"),
                                  frameTime, FP::undefined()));
        }

        break;
    }

    case ResponseState::Interactive_Start_SetDate: {
        auto line = frame.toQByteArray().trimmed();

        /* Ignore echo */
        if (line.isEmpty())
            break;
        if (line.startsWith("rtc,"))
            break;

        if (line != "OK") {
            qCDebug(log) << "Invalid date set response" << frame << "during startup at"
                         << Logging::time(frameTime);

            invalidateLogValues(frameTime);

            responseState = ResponseState::Interactive_Restart_Wait;
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);
            autoprobeValidRecords = 0;
            generalStatusUpdated();

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            if (realtimeEgress) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
            return;
        }

        responseState = ResponseState::Interactive_Start_Unpolled_FirstValid;
        timeoutAt(frameTime + config.front().reportInterval * 3.0 + 5.0);
        discardData(frameTime + 0.5, 1);

        if (controlStream) {
            int seconds = static_cast<int>(std::floor(config.front().reportInterval) + 0.5);
            if (seconds < 1)
                seconds = 1;
            else if (seconds > 2000)
                seconds = 2000;
            Util::ByteArray send("Log,");
            send += QByteArray::number(seconds);
            send += "\r";
            controlStream->writeControl(std::move(send));
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                       Variant::Root("StartInteractiveFirstValid"),
                                                       frameTime, FP::undefined()));
        }

        break;
    }

    case ResponseState::Interactive_Start_Unpolled_FirstValid: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + config.front().reportInterval + 5.0);

            if (++autoprobeValidRecords > 3) {
                qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

                responseState = ResponseState::Unpolled_Run;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                realtimeStateUpdated = true;

                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("FirstValid");
                event(frameTime, QObject::tr("Communications established."), false, info);
            }
        } else if (code > 0) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "rejected with code" << code;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidRecord"),
                                      frameTime, FP::undefined()));
            }

            responseState = ResponseState::Interactive_Restart_Wait;
            if (controlStream != NULL)
                controlStream->resetControl();
            timeoutAt(FP::undefined());
            discardData(frameTime + 10.0);

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            info.hash("Code").setInteger(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Communications start rejected the first line (code %1).").arg(code),
                  true, info);

            invalidateLogValues(frameTime);
        } /* Ignored line */
        break;
    }
    default:
        break;
    }
}

void AcquireADCPCMagic200::incomingInstrumentTimeout(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);

    switch (responseState) {
    case ResponseState::Interactive_Start_Flush:
    case ResponseState::Interactive_Start_DisableReports:
    case ResponseState::Interactive_Start_Header_Initial:
    case ResponseState::Interactive_Start_ReadVersion_Initial:
    case ResponseState::Interactive_Start_SetTime:
    case ResponseState::Interactive_Start_SetDate:
    case ResponseState::Interactive_Start_Unpolled_FirstValid:
        qCDebug(log) << "Timeout in interactive start state" << static_cast<int>(responseState)
                     << "at" << Logging::time(frameTime);

        invalidateLogValues(frameTime);

        responseState = ResponseState::Interactive_Restart_Wait;
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);
        autoprobeValidRecords = 0;
        generalStatusUpdated();

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        break;

    case ResponseState::Passive_Run: {
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        responseState = ResponseState::Passive_Wait;
        generalStatusUpdated();
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;
    }

    case ResponseState::Unpolled_Run: {
        qCDebug(log) << "Timeout in unpolled mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(frameTime + 5.0);
        discardData(frameTime + 1.0);
        if (controlStream) {
            controlStream->writeControl("\r\rLog,0\r");
        }

        generalStatusUpdated();
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("UnpolledRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        invalidateLogValues(frameTime);
        break;
    }

    case ResponseState::Passive_Initialize:
    case ResponseState::Passive_Wait:
        responseState = ResponseState::Passive_Wait;
        autoprobeValidRecords = 0;
        invalidateLogValues(frameTime);
        break;

    case ResponseState::Autoprobe_Passive_Wait: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case ResponseState::Autoprobe_Passive_Initialize:
        responseState = ResponseState::Autoprobe_Passive_Wait;
        autoprobeValidRecords = 0;
        invalidateLogValues(frameTime);
        break;

    case ResponseState::Interactive_Restart_Wait:
    case ResponseState::Interactive_Initialize:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(frameTime + 5.0);
        discardData(frameTime + 1.0);
        if (controlStream) {
            controlStream->writeControl("\r\rLog,0\r");
        }

        generalStatusUpdated();
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    case ResponseState::Interactive_Start_Header_Continue:
        startupSetTime = frameTime;
        responseState = ResponseState::Interactive_Start_ReadVersion_Initial;
        timeoutAt(frameTime + 2.0);

        if (controlStream) {
            controlStream->writeControl("rv\r");
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                       Variant::Root("StartInteractiveReadVersion"),
                                                       frameTime, FP::undefined()));
        }
        break;

    case ResponseState::Interactive_Start_ReadVersion_Continue:
        startupSetTime = frameTime;
        if (controlStream) {
            auto dt = Time::toDateTime(startupSetTime + 0.5);
            Util::ByteArray send("rtc,");
            send += QByteArray::number(dt.time().hour()).rightJustified(2, '0');
            send += ":";
            send += QByteArray::number(dt.time().minute()).rightJustified(2, '0');
            send += ":";
            send += QByteArray::number(dt.time().second()).rightJustified(2, '0');
            send += "\r";
            controlStream->writeControl(std::move(send));
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveSetTime"),
                                  frameTime, FP::undefined()));
        }

        responseState = ResponseState::Interactive_Start_SetTime;
        timeoutAt(frameTime + 2.0);
        break;
    }
}

void AcquireADCPCMagic200::discardDataCompleted(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);

    switch (responseState) {
    case ResponseState::Interactive_Start_Flush:
        responseState = ResponseState::Interactive_Start_DisableReports;
        timeoutAt(frameTime + 5.0);
        discardData(frameTime + 1.0);

        if (controlStream) {
            controlStream->writeControl("Log,0\r");
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractivePoll"),
                                  frameTime, FP::undefined()));
        }
        break;

    case ResponseState::Interactive_Start_DisableReports:
        responseState = ResponseState::Interactive_Start_Header_Initial;
        timeoutAt(frameTime + 2.0);

        if (controlStream) {
            controlStream->writeControl("hdr\r");
        }
        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractivePoll"),
                                  frameTime, FP::undefined()));
        }
        break;

    case ResponseState::Interactive_Restart_Wait:
    case ResponseState::Interactive_Initialize:
        timeoutAt(frameTime + 2.0);

        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(frameTime + 5.0);
        discardData(frameTime + 1.0);
        if (controlStream) {
            controlStream->writeControl("\r\rLog,0\r");
        }

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    default:
        break;
    }
}

void AcquireADCPCMagic200::incomingControlFrame(const Util::ByteArray &, double)
{ }


Variant::Root AcquireADCPCMagic200::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireADCPCMagic200::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case ResponseState::Passive_Run:
    case ResponseState::Unpolled_Run:
        return AcquisitionInterface::GeneralStatus::Normal;
        break;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
        break;
    }
}

AcquisitionInterface::AutoprobeStatus AcquireADCPCMagic200::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireADCPCMagic200::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case ResponseState::Unpolled_Run:
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << static_cast<int>(responseState) << "at" << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case ResponseState::Interactive_Start_Flush:
    case ResponseState::Interactive_Start_DisableReports:
    case ResponseState::Interactive_Start_Header_Initial:
    case ResponseState::Interactive_Start_Header_Continue:
    case ResponseState::Interactive_Start_ReadVersion_Initial:
    case ResponseState::Interactive_Start_ReadVersion_Continue:
    case ResponseState::Interactive_Start_SetTime:
    case ResponseState::Interactive_Start_SetDate:
    case ResponseState::Interactive_Start_Unpolled_FirstValid:
    case ResponseState::Passive_Run:
    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Initialize:
    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
    case ResponseState::Interactive_Initialize:
    case ResponseState::Interactive_Restart_Wait:
        qCDebug(log) << "Interactive autoprobe started from state"
                     << static_cast<int>(responseState) << "at" << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(time + 5.0);
        if (controlStream) {
            controlStream->writeControl("\r\n\r\rLog,0\r");
        }
        discardData(time + 1.0);

        generalStatusUpdated();

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireADCPCMagic200::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    configurationAdvance(time);

    Q_ASSERT(!config.empty());
    double unpolledResponseTime = config.front().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    discardData(time + 0.5, 1);
    timeoutAt(time + unpolledResponseTime * 7.0 + 3.0);

    qCDebug(log) << "Reset to passive autoprobe from state" << static_cast<int>(responseState)
                 << "at" << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = ResponseState::Autoprobe_Passive_Wait;
    autoprobeValidRecords = 0;
    generalStatusUpdated();
}

void AcquireADCPCMagic200::autoprobePromote(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + 2.0);

    switch (responseState) {
    case ResponseState::Interactive_Start_Flush:
    case ResponseState::Interactive_Start_DisableReports:
    case ResponseState::Interactive_Start_Header_Initial:
    case ResponseState::Interactive_Start_Header_Continue:
    case ResponseState::Interactive_Start_ReadVersion_Initial:
    case ResponseState::Interactive_Start_ReadVersion_Continue:
    case ResponseState::Interactive_Start_SetTime:
    case ResponseState::Interactive_Start_SetDate:
    case ResponseState::Interactive_Start_Unpolled_FirstValid:
    case ResponseState::Unpolled_Run:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << static_cast<int>(responseState)
                     << "to interactive acquisition at" << Logging::time(time);
        break;

    case ResponseState::Passive_Run:
    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Initialize:
    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
    case ResponseState::Interactive_Initialize:
    case ResponseState::Interactive_Restart_Wait:
        qCDebug(log) << "Promoted from passive state" << static_cast<int>(responseState)
                     << "to interactive acquisition at" << Logging::time(time);

        responseState = ResponseState::Interactive_Start_Flush;
        timeoutAt(time + 5.0);
        discardData(time + 1.0);
        if (controlStream) {
            controlStream->writeControl("\r\rLog,0\r");
        }

        generalStatusUpdated();

        if (realtimeEgress) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireADCPCMagic200::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    configurationAdvance(time);

    Q_ASSERT(!config.empty());
    double unpolledResponseTime = config.front().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    timeoutAt(time + unpolledResponseTime * 2.0 + 1.0);

    switch (responseState) {
    case ResponseState::Passive_Run:
    case ResponseState::Passive_Wait:
    case ResponseState::Passive_Initialize:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << static_cast<int>(responseState)
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case ResponseState::Autoprobe_Passive_Wait:
    case ResponseState::Autoprobe_Passive_Initialize:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from autoprobe passive state" << static_cast<int>(responseState)
                     << "to passive acquisition at" << Logging::time(time);
        responseState = ResponseState::Passive_Initialize;
        break;

    case ResponseState::Unpolled_Run:
    case ResponseState::Interactive_Start_Unpolled_FirstValid:
        discardData(FP::undefined());

        responseState = ResponseState::Passive_Run;
        generalStatusUpdated();

        /* Already in unpolled, so do nothing */
        qCDebug(log) << "Promoted from unpolled state" << static_cast<int>(responseState)
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case ResponseState::Interactive_Start_Flush:
    case ResponseState::Interactive_Start_DisableReports:
    case ResponseState::Interactive_Start_Header_Initial:
    case ResponseState::Interactive_Start_Header_Continue:
    case ResponseState::Interactive_Start_ReadVersion_Initial:
    case ResponseState::Interactive_Start_ReadVersion_Continue:
    case ResponseState::Interactive_Start_SetTime:
    case ResponseState::Interactive_Start_SetDate:
    case ResponseState::Interactive_Initialize:
    case ResponseState::Interactive_Restart_Wait:
        /* We didn't have full communications, so just drop it and start
         * over. */
        qCDebug(log) << "Promoted from state" << static_cast<int>(responseState)
                     << "to passive acquisition at" << Logging::time(time);

        discardData(FP::undefined());

        responseState = ResponseState::Passive_Initialize;
        generalStatusUpdated();
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireADCPCMagic200::getDefaults()
{
    AutomaticDefaults result;
    result.name = "N$1$2";
    result.setSerialN81(115200);
    return result;
}


ComponentOptions AcquireADCPCMagic200Component::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options, true);
    return options;
}

ComponentOptions AcquireADCPCMagic200Component::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireADCPCMagic200Component::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples(true));
    examples.append(getPassiveExamples());
    return examples;
}

QList<ComponentExample> AcquireADCPCMagic200Component::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data")));

    return examples;
}

bool AcquireADCPCMagic200Component::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireADCPCMagic200Component::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options, true);
}

std::unique_ptr<
        AcquisitionInterface> AcquireADCPCMagic200Component::createAcquisitionPassive(const ComponentOptions &options,
                                                                                      const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireADCPCMagic200(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireADCPCMagic200Component::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                      const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireADCPCMagic200(config, loggingContext)); }

std::unique_ptr<AcquisitionInterface> AcquireADCPCMagic200Component::createAcquisitionAutoprobe(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireADCPCMagic200> i(new AcquireADCPCMagic200(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<AcquisitionInterface> AcquireADCPCMagic200Component::createAcquisitionInteractive(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireADCPCMagic200> i(new AcquireADCPCMagic200(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
