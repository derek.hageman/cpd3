/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIREADCPCMAGIC200_HXX
#define ACQUIREADCPCMAGIC200_HXX

#include "core/first.hxx"

#include <memory>
#include <deque>
#include <map>
#include <array>
#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "core/number.hxx"

class AcquireADCPCMagic200 : public CPD3::Acquisition::FramedInstrument {
    double lastRecordTime;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;
    int autoprobeValidRecords;

    enum class ResponseState : int {
        /* Waiting a response to a command in passive mode */
                Passive_Run,

        /* Waiting for enough data to confirm communications */
                Passive_Wait,

        /* Passive initialization (same as passive wait, but discard the
         * first timeout). */
                Passive_Initialize,

        /* Same as passive wait but currently autoprobing */
                Autoprobe_Passive_Wait,

        /* Same as passive initialize but autoprobing */
        Autoprobe_Passive_Initialize,

        /* Logging disable (Log,0) and flush */
        Interactive_Start_Flush,

        /* Log,0 (again) */
        Interactive_Start_DisableReports,

        /* hdr */
        Interactive_Start_Header_Initial,

        /* Remaining lines in hdr response */
        Interactive_Start_Header_Continue,

        /* rv */
        Interactive_Start_ReadVersion_Initial,

        /* Remaining lines in rv response */
        Interactive_Start_ReadVersion_Continue,

        /* rtc,hh:mm:ss */
        Interactive_Start_SetTime,

        /* rtc,yy/mm/dd */
                Interactive_Start_SetDate,

        /* Log,1, flush and the first record */
                Interactive_Start_Unpolled_FirstValid,

        /* Waiting before attempting a communications restart */
                Interactive_Restart_Wait,

        /* Initial state for interactive start */
                Interactive_Initialize,

        /* In unpolled mode, waiting for responses. */
                Unpolled_Run,
    } responseState;

    class Configuration {
        double start;
        double end;

    public:
        bool strictMode;
        bool calculateConcentration;
        bool useMeasuredTime;
        double flow;
        double reportInterval;


        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under,
                      const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    std::deque<Configuration> config;

    void setDefaultInvalid();

    CPD3::Data::Variant::Root instrumentMeta;

    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;
    bool realtimeStateUpdated;

    CPD3::Data::Variant::Flags lastReportedFlags;
    double startupSetTime;

    void logValue(double startTime,
                  double endTime,
                  CPD3::Data::SequenceName::Component name,
                  CPD3::Data::Variant::Root &&value);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root &&value);

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    void invalidateLogValues(double frameTime);

    void configurationAdvance(double frameTime);

    int processRecord(const CPD3::Util::ByteArray &line, double &frameTime);

public:
    AcquireADCPCMagic200(const CPD3::Data::ValueSegment::Transfer &config,
                         const std::string &loggingContext);

    AcquireADCPCMagic200(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireADCPCMagic200();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus() override;

    CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables() override;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus() override;

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingInstrumentTimeout(double time) override;

    void discardDataCompleted(double time) override;
};

class AcquireADCPCMagic200Component
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_ad_cpcmagic200"
                              FILE
                              "acquire_ad_cpcmagic200.json")

    CPD3::ComponentOptions getBaseOptions();

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
