/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef GENERATEEDITED_H
#define GENERATEEDITED_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "datacore/externalsource.hxx"


class GenerateEditedComponent : public QObject, virtual public CPD3::Data::ExternalSourceComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalSourceComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.generate_edited"
                              FILE
                              "generate_edited.json")

public:
    virtual CPD3::ComponentOptions getOptions();

    virtual QList<CPD3::ComponentExample> getExamples();

    virtual int ingressAllowStations();

    virtual int ingressRequireStations();

    virtual bool ingressRequiresTime();

    virtual CPD3::Time::LogicalTimeUnit ingressDefaultTimeUnit();

    CPD3::Data::ExternalConverter *createExternalIngress(const CPD3::ComponentOptions &options,
                                                                 double start,
                                                                 double end,
                                                                 const std::vector<CPD3::Data::SequenceName::Component> &stations = {}) override;
};

#endif
