/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QString>

#include "core/component.hxx"
#include "core/qtcompat.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/stream.hxx"

using namespace CPD3;
using namespace CPD3::Data;

class TestComponent : public QObject {
Q_OBJECT

    ExternalSourceComponent *component;

private slots:

    void initTestCase()
    {
        Logging::suppressForTesting();

        component = qobject_cast<ExternalSourceComponent *>(
                ComponentLoader::create("generate_edited"));
        QVERIFY(component);
    }

    void options()
    {
        ComponentOptions options;
        options = component->getOptions();
        QVERIFY(qobject_cast<ComponentOptionSingleString *>(options.get("profile")));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
