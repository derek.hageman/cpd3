/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <QLoggingCategory>

#include "editing/editingengine.hxx"
#include "generate_edited.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Editing;


Q_LOGGING_CATEGORY(log_component_generate_edited, "cpd3.component.generate.edited", QtWarningMsg)

ComponentOptions GenerateEditedComponent::getOptions()
{
    ComponentOptions options;

    options.add("profile",
                new ComponentOptionSingleString(tr("profile", "name"), tr("Execution profile"),
                                                tr("This is the profile name to generate data for.  Different editing "
                                                   "profiles specify different sets of data or methods of generating "
                                                   "final data within the same station.  "
                                                   "Consult the station configuration for available profiles."),
                                                tr("aerosol")));

    return options;
}

QList<ComponentExample> GenerateEditedComponent::getExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getOptions();
    examples.append(ComponentExample(options, tr("Generate edited data", "default example name"),
                                     tr("This will generate edited data for the \"aerosol\" profile.")));

    return examples;
}

int GenerateEditedComponent::ingressAllowStations()
{ return 1; }

int GenerateEditedComponent::ingressRequireStations()
{ return 1; }

bool GenerateEditedComponent::ingressRequiresTime()
{ return true; }

Time::LogicalTimeUnit GenerateEditedComponent::ingressDefaultTimeUnit()
{ return Time::Week; }

ExternalConverter *GenerateEditedComponent::createExternalIngress(const ComponentOptions &options,
                                                                  double start,
                                                                  double end,
                                                                  const std::vector<
                                                                          SequenceName::Component> &stations)
{
    if (stations.size() != 1) {
        qCWarning(log_component_generate_edited) << "Invalid number of station(s) specified:"
                                                 << stations.size();
        return nullptr;
    }

    const auto station = stations.front();

    auto profile = SequenceName::impliedProfile();
    if (options.isSet("profile")) {
        profile = qobject_cast<ComponentOptionSingleString *>(options.get("profile"))->get()
                                                                                     .toLower()
                                                                                     .toStdString();
    }

    return new EditingEngine(start, end, station, QString::fromStdString(profile));
}
