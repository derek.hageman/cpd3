/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIREAERODYNECAPS_H
#define ACQUIREAERODYNECAPS_H

#include "core/first.hxx"

#include <QtGlobal>
#include <QObject>
#include <QDateTime>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"

class AcquireAerodyneCAPS : public CPD3::Acquisition::FramedInstrument {
    double lastRecordTime;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum ResponseState {
        /* Passive autoprobe initializing, ignoring the first timeout */
        RESP_AUTOPROBE_PASSIVE_INITIALIZE,
        /* Waiting for a valid record to confirm passive mode autoprobe */
        RESP_AUTOPROBE_PASSIVE_WAIT,

        /* Waiting for a valid record to start passive acquisition */
        RESP_PASSIVE_WAIT,

        /* Acquiring data in passive mode */
        RESP_PASSIVE_RUN,
        /* Acquiring data in interactive mode, but only reading unpolled data */
        RESP_UNPOLLED_RUN,

        /* Waiting for the completion of the "q" command to exit the 
         * interpreter */
        RESP_INTERACTIVE_START_EXITINTERPRETER,
        /* Waiting for the acknowledge from the "Q" command that the
         * interpreter has been entered */
        RESP_INTERACTIVE_START_ENTERINTERPRETER,

        /* Waiting for the "D" command to set the date */
        RESP_INTERACTIVE_START_SETDATE,
        /* Waiting for the "T" command to set the time */
        RESP_INTERACTIVE_START_SETTIME,
        /* Waiting for the "H" or "h" command to enable/disable autozero */
        RESP_INTERACTIVE_START_SETAUTOZERO,
        /* Waiting for the "M 8" command to set the zero duration */
        RESP_INTERACTIVE_START_SETZERODURATION,
        /* Waiting for the "M 7" command to set the flush duration */
        RESP_INTERACTIVE_START_SETFLUSHDURATION,

        /* Waiting for the first valid record */
        RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID,

        /* Waiting before attempting a communications restart */
        RESP_INTERACTIVE_RESTART_WAIT,
        /* Initial state for interactive start */
        RESP_INTERACTIVE_INITIALIZE,
    };
    ResponseState responseState;
    int autoprobeValidRecords;

    enum {
        /* Normal operation. */
        SAMPLE_RUN,

        /* Air flush */
        SAMPLE_FLUSH,

        /* Zero in progress */
        SAMPLE_ZERO,
    } sampleState;

    friend class Configuration;

    class Configuration {
        double start;
        double end;
    public:
        double wavelength;
        bool strictMode;
        double reportInterval;
        double timeOrigin;

        bool enableAutozero;
        int zeroDuration;
        int flushDuration;

        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under,
                      const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    QList<Configuration> config;

    double effectiveWavelength;

    QDateTime setInstrumentTime;

    void setDefaultInvalid();

    CPD3::Data::Variant::Root instrumentMeta;

    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;
    bool realtimeStateUpdated;

    CPD3::Data::SequenceValue lastMode;

    double zeroEffectiveTime;
    CPD3::Data::Variant::Root Bez;
    CPD3::Data::Variant::Root priorBez;
    CPD3::Data::Variant::Root Bezd;
    CPD3::Data::Variant::Root Tz;
    CPD3::Data::Variant::Root Pz;
    bool zeroPersistentUpdated;
    bool zeroRealtimeUpdated;
    int zeroTCount;
    double zeroTSum;
    int zeroPCount;
    double zeroPSum;

    void logValue(double startTime,
                  double endTime,
                  CPD3::Data::SequenceName::Component name,
                  CPD3::Data::Variant::Root &&value);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root &&value);

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    void handleZeroUpdate(double currentTime);

    void zeroCompleted(double currentTime, const CPD3::Data::Variant::Read &Bez);

    void configAdvance(double frameTime);

    void configurationChanged();

    void sendCommand(CPD3::Util::ByteArray command);

    inline void sendCommand(const char *command)
    { sendCommand(CPD3::Util::ByteArray(command)); }

    int processRecord(const CPD3::Util::ByteArray &line, double &frameTime);

public:
    AcquireAerodyneCAPS(const CPD3::Data::ValueSegment::Transfer &config,
                        const std::string &loggingContext);

    AcquireAerodyneCAPS(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireAerodyneCAPS();

    virtual CPD3::Data::SequenceValue::Transfer getPersistentValues();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    virtual CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus();

    virtual CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus();

    CPD3::Data::Variant::Root getCommands() override;

    virtual CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables();

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    virtual void command(const CPD3::Data::Variant::Read &value);

    virtual void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time);

    virtual void incomingInstrumentTimeout(double time);

    virtual void discardDataCompleted(double time);
};

class AcquireAerodyneCAPSComponent
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_aerodyne_caps"
                              FILE
                              "acquire_aerodyne_caps.json")

    CPD3::ComponentOptions getBaseOptions();

public:

    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
