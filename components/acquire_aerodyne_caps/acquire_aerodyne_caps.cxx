/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/csv.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "datacore/wavelength.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_aerodyne_caps.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;


AcquireAerodyneCAPS::Configuration::Configuration() : start(FP::undefined()),
                                                      end(FP::undefined()),
                                                      wavelength(FP::undefined()),
                                                      strictMode(true),
                                                      reportInterval(1.0),
                                                      timeOrigin(FP::undefined()),
                                                      enableAutozero(false),
                                                      zeroDuration(-1),
                                                      flushDuration(-1)
{ }

AcquireAerodyneCAPS::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          wavelength(other.wavelength),
          strictMode(other.strictMode),
          reportInterval(other.reportInterval),
          timeOrigin(other.timeOrigin),
          enableAutozero(other.enableAutozero),
          zeroDuration(other.zeroDuration),
          flushDuration(other.flushDuration)
{ }

AcquireAerodyneCAPS::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s),
          end(e),
          wavelength(FP::undefined()),
          strictMode(true),
          reportInterval(1.0),
          timeOrigin(FP::undefined()),
          enableAutozero(false),
          zeroDuration(-1),
          flushDuration(-1)
{
    setFromSegment(other);
}

AcquireAerodyneCAPS::Configuration::Configuration(const Configuration &under,
                                                  const ValueSegment &over,
                                                  double s,
                                                  double e) : start(s),
                                                              end(e),
                                                              wavelength(under.wavelength),
                                                              strictMode(under.strictMode),
                                                              reportInterval(under.reportInterval),
                                                              timeOrigin(under.timeOrigin),
                                                              enableAutozero(under.enableAutozero),
                                                              zeroDuration(under.zeroDuration),
                                                              flushDuration(under.flushDuration)
{
    setFromSegment(over);
}

void AcquireAerodyneCAPS::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["StrictMode"].exists())
        strictMode = config["StrictMode"].toBool();

    if (config["Autozero"].exists())
        enableAutozero = config["Autozero"].toBool();

    {
        double v = config["ReportInterval"].toDouble();
        if (FP::defined(v) && v > 0.0)
            reportInterval = v;
    }

    if (config["BaseTime"].exists())
        timeOrigin = config["BaseTime"].toDouble();

    if (config["Wavelength"].exists())
        wavelength = config["Wavelength"].toDouble();

    if (config["ZeroDuration"].exists()) {
        qint64 i = config["ZeroDuration"].toInt64();
        if (INTEGER::defined(i) && i > 0) {
            zeroDuration = (int) i;
        } else {
            zeroDuration = -1;
        }
    }

    if (config["FlushDuration"].exists()) {
        qint64 i = config["FlushDuration"].toInt64();
        if (INTEGER::defined(i) && i > 0) {
            flushDuration = (int) i;
        } else {
            flushDuration = -1;
        }
    }
}


void AcquireAerodyneCAPS::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("Aerodyne");
    instrumentMeta["Model"].setString("CAPS");

    effectiveWavelength = FP::undefined();

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    realtimeStateUpdated = true;

    zeroEffectiveTime = FP::undefined();
    zeroPersistentUpdated = true;
    zeroRealtimeUpdated = true;
    zeroTCount = 0;
    zeroTSum = 0;
    zeroPCount = 0;
    zeroPSum = 0;

    lastMode.setUnit(SequenceName({}, "raw", "F2"));
}

AcquireAerodyneCAPS::AcquireAerodyneCAPS(const ValueSegment::Transfer &configData,
                                         const std::string &loggingContext) : FramedInstrument(
        "caps", loggingContext),
                                                                              lastRecordTime(
                                                                                      FP::undefined()),
                                                                              autoprobeStatus(
                                                                                      AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                              responseState(
                                                                                      RESP_PASSIVE_WAIT),
                                                                              sampleState(
                                                                                      SAMPLE_RUN)
{
    setDefaultInvalid();
    config.append(Configuration());
    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
}

void AcquireAerodyneCAPS::setToAutoprobe()
{
    responseState = RESP_AUTOPROBE_PASSIVE_INITIALIZE;
    autoprobeValidRecords = 0;
}

void AcquireAerodyneCAPS::setToInteractive()
{ responseState = RESP_INTERACTIVE_INITIALIZE; }


ComponentOptions AcquireAerodyneCAPSComponent::getBaseOptions()
{
    ComponentOptions options;

    ComponentOptionSingleDouble *d =
            new ComponentOptionSingleDouble(tr("wavelength", "name"), tr("Sample wavelength"),
                                            tr("This is the wavelength in nm of the laser in the CAPS."),
                                            tr("532nm", "default wavelength"), 1);
    d->setMinimum(0.0, false);
    options.add("wavelength", d);

    return options;
}

AcquireAerodyneCAPS::AcquireAerodyneCAPS(const ComponentOptions &options,
                                         const std::string &loggingContext) : FramedInstrument(
        "caps", loggingContext),
                                                                              lastRecordTime(
                                                                                      FP::undefined()),
                                                                              autoprobeStatus(
                                                                                      AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                              responseState(
                                                                                      RESP_PASSIVE_WAIT),
                                                                              sampleState(
                                                                                      SAMPLE_RUN)
{
    setDefaultInvalid();

    Configuration base;

    if (options.isSet("basetime")) {
        base.timeOrigin = qobject_cast<ComponentOptionSingleTime *>(options.get("basetime"))->get();
    }
    if (options.isSet("wavelength")) {
        double value =
                qobject_cast<ComponentOptionSingleDouble *>(options.get("wavelength"))->get();
        if (FP::defined(value) && value > 0.0)
            base.wavelength = value;
    }

    config.append(base);
}

AcquireAerodyneCAPS::~AcquireAerodyneCAPS()
{
}


void AcquireAerodyneCAPS::logValue(double startTime,
                                   double endTime,
                                   SequenceName::Component name,
                                   Variant::Root &&value)
{
    if (!loggingEgress && !realtimeEgress) return;
    SequenceValue dv({{}, "raw", std::move(name)}, std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        if (FP::defined(startTime))
            loggingEgress->incomingData(std::move(dv));
        return;
    }
    if (loggingEgress && FP::defined(startTime))
        loggingEgress->incomingData(dv);
    if (FP::defined(endTime)) {
        dv.setStart(endTime);
        dv.setEnd(endTime + 1.0);
    } else if (FP::defined(startTime)) {
        dv.setEnd(startTime + 1.0);
    }
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireAerodyneCAPS::realtimeValue(double time,
                                        SequenceName::Component name,
                                        Variant::Root &&value)
{
    if (!realtimeEgress) return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

SequenceValue::Transfer AcquireAerodyneCAPS::buildLogMeta(double time) const
{
    Q_ASSERT(!config.isEmpty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_aerodyne_caps");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());

    const auto &wlCode = Wavelength::code(effectiveWavelength);

    result.emplace_back(SequenceName({}, "raw_meta", "Be" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light extinction coefficient");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);

    result.emplace_back(SequenceName({}, "raw_meta", "ZBel" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back().write().metadataReal("Description").setString("Loss");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Loss"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(5);

    result.emplace_back(SequenceName({}, "raw_meta", "P"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Sample pressure");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(4);

    result.emplace_back(SequenceName({}, "raw_meta", "T"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Sample temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(3);

    result.emplace_back(SequenceName({}, "raw_meta", "V"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000000");
    result.back().write().metadataReal("Units").setString("mV");
    result.back().write().metadataReal("Description").setString("Light signal");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Signal"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(6);


    result.emplace_back(SequenceName({}, "raw_meta", "Tz"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Sample temperature during zero");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataReal("Realtime").hash("Hide").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "Pz"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Sample pressure during zero");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataReal("Realtime").hash("Hide").setBool(true);

    result.emplace_back(SequenceName({}, "raw_meta", "Bez" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light extinction coefficient during zero measurement");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Baseline"));
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(1);


    result.emplace_back(SequenceName({}, "raw_meta", "F2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataString("Description").setString("Instrument status string");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataString("Realtime").hash("Name").setString("Status");

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataSingleFlag("Blank")
          .hash("Description")
          .setString("Data removed in blank period");
    result.back().write().metadataSingleFlag("Blank").hash("Bits").setInt64(0x40000000);
    result.back()
          .write()
          .metadataSingleFlag("Blank")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_aerodyne_caps");

    result.back()
          .write()
          .metadataSingleFlag("Zero")
          .hash("Description")
          .setString("Zero in progress");
    result.back().write().metadataSingleFlag("Zero").hash("Bits").setInt64(0x20000000);
    result.back()
          .write()
          .metadataSingleFlag("Zero")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_aerodyne_caps");

    return result;
}

SequenceValue::Transfer AcquireAerodyneCAPS::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_aerodyne_caps");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());

    const auto &wlCode = Wavelength::code(effectiveWavelength);

    result.emplace_back(SequenceName({}, "raw_meta", "Bezd" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light extinction coefficient zero shift");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Smoothing").hash("Mode").setString("None");
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Baseline shift"));
    result.back().write().metadataReal("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(2);


    result.emplace_back(SequenceName({}, "raw_meta", "ZBe" + wlCode), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Wavelength").setDouble(effectiveWavelength);
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("Mm\xE2\x81\xBB¹");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Aerosol light extinction coefficient");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Extinction"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInt64(0);

    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Smoothing").hash("Mode").setString("None");
    result.back().write().metadataString("Realtime").hash("Name").setString(std::string());
    result.back().write().metadataString("Realtime").hash("Units").setString(std::string());
    result.back().write().metadataString("Realtime").hash("FullLine").setBool(true);
    result.back().write().metadataString("Realtime").hash("RowOrder").setInt64(6);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Run")
          .setString(QObject::tr("", "run state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Zero")
          .setString(QObject::tr("Zero in progress", "zero state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Blank")
          .setString(QObject::tr("Blank in progress", "spancheck state string"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidRecord")
          .setString(QObject::tr("NO COMMS: Invalid record"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("InvalidResponse")
          .setString(QObject::tr("NO COMMS: Invalid response"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("Timeout")
          .setString(QObject::tr("NO COMMS: Timeout"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractive")
          .setString(QObject::tr("STARTING COMMS"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveEnterInterpreter")
          .setString(QObject::tr("STARTING COMMS: Entering interpreter"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetDate")
          .setString(QObject::tr("STARTING COMMS: Setting date"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetDate")
          .setString(QObject::tr("STARTING COMMS: Setting time"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetAutozero")
          .setString(QObject::tr("STARTING COMMS: Setting autozero mode"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetZeroDuration")
          .setString(QObject::tr("STARTING COMMS: Setting zero duration"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveSetFlushDuration")
          .setString(QObject::tr("STARTING COMMS: Setting flush duration"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveUnpolledStartFlushing")
          .setString(QObject::tr("STARTING COMMS: Flushing initial unpolled"));
    result.back()
          .write()
          .metadataString("Realtime")
          .hash("Translation")
          .hash("StartInteractiveUnpolledStartReadFirstRecord")
          .setString(QObject::tr("STARTING COMMS: Waiting for first record"));

    return result;
}

SequenceMatch::Composite AcquireAerodyneCAPS::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "Tz");
    sel.append({}, {}, "Pz");
    sel.append({}, {}, "Bezd?[BGRQ0-9]?");
    sel.append({}, {}, "F2");
    sel.append({}, {}, "ZSTATE");
    return sel;
}

void AcquireAerodyneCAPS::handleZeroUpdate(double currentTime)
{
    if ((!zeroPersistentUpdated || persistentEgress == NULL) &&
            (!zeroRealtimeUpdated || realtimeEgress == NULL))
        return;
    if (!FP::defined(zeroEffectiveTime))
        return;

    const auto &wlCode = Wavelength::code(effectiveWavelength);

    SequenceValue::Transfer result;

    if (Tz.read().exists()) {
        result.emplace_back(SequenceIdentity({{}, "raw", "Tz"}, zeroEffectiveTime, FP::undefined()),
                            Tz);
    }
    if (Pz.read().exists()) {
        result.emplace_back(SequenceIdentity({{}, "raw", "Pz"}, zeroEffectiveTime, FP::undefined()),
                            Pz);
    }
    result.emplace_back(
            SequenceIdentity({{}, "raw", "Bez" + wlCode}, zeroEffectiveTime, FP::undefined()), Bez);


    if (zeroPersistentUpdated && persistentEgress != NULL) {
        zeroPersistentUpdated = false;
        persistentEgress->incomingData(result);
    }

    if (!zeroRealtimeUpdated || realtimeEgress == NULL)
        return;
    zeroRealtimeUpdated = false;

    result.emplace_back(
            SequenceIdentity({{}, "raw", "Bezd" + wlCode}, currentTime, FP::undefined()), Bezd);

    realtimeEgress->incomingData(std::move(result));
}

static double calculateZeroChange(double previous, double current)
{
    if (!FP::defined(previous) || !FP::defined(current))
        return FP::undefined();
    return current - previous;
}

void AcquireAerodyneCAPS::zeroCompleted(double currentTime, const Variant::Read &Bez)
{
    zeroEffectiveTime = currentTime;
    zeroPersistentUpdated = true;
    zeroRealtimeUpdated = true;

    /* Zero update can ocurr before the zero flag is cleared, so if the two are equal
     * assume it did and set the difference based on the prior difference */
    double currentZero = Bez.toDouble();
    double previousZero = this->Bez.read().toDouble();
    double priorPreviousZero = this->priorBez.read().toDouble();
    if (FP::defined(currentZero) &&
            FP::defined(previousZero) &&
            FP::defined(priorPreviousZero) &&
            currentZero == previousZero)
        Bezd.write().setDouble(calculateZeroChange(priorPreviousZero, previousZero));
    else
        Bezd.write().setDouble(calculateZeroChange(previousZero, currentZero));

    if (zeroTCount != 0)
        Tz.write().setDouble(zeroTSum / (double) zeroTCount);
    else
        Tz.write().setEmpty();
    if (zeroPCount != 0)
        Pz.write().setDouble(zeroPSum / (double) zeroPCount);
    else
        Pz.write().setEmpty();

    zeroTCount = 0;
    zeroTSum = 0;
    zeroPCount = 0;
    zeroPSum = 0;
}

int AcquireAerodyneCAPS::processRecord(const Util::ByteArray &line, double &frameTime)
{
    Q_ASSERT(!config.isEmpty());

    if (line.size() < 3)
        return 1;

    auto fields = CSV::acquisitionSplit(line);
    bool ok = false;

    if (fields.empty()) return 2;
    auto field = fields.front().string_trimmed();
    fields.pop_front();

    std::deque<Util::ByteView> subfields;
    if (field.indexOf(':') != field.npos) {
        subfields = Util::as_deque(field.split(':'));
    } else if (field.size() == 6) {
        subfields.emplace_back(field.mid(0, 2));
        subfields.emplace_back(field.mid(2, 2));
        subfields.emplace_back(field.mid(4, 2));
    } else {
        return 3;
    }

    if (subfields.empty()) return 4;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    qint64 ihour = field.parse_i32(&ok);
    if (!ok) return 5;
    if (ihour < 0) return 6;
    if (ihour > 23) return 7;
    Variant::Root hour(ihour);
    remap("HOUR", hour);
    ihour = hour.read().toInt64();

    if (subfields.empty()) return 8;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    qint64 iminute = field.parse_i32(&ok);
    if (!ok) return 9;
    if (iminute < 0) return 10;
    if (iminute > 59) return 11;
    Variant::Root minute(iminute);
    remap("MINUTE", minute);
    iminute = minute.read().toInt64();

    if (subfields.empty()) return 12;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    qint64 isecond = field.parse_i32(&ok);
    if (!ok) return 13;
    if (isecond < 0) return 14;
    if (isecond > 60) return 15;
    Variant::Root second(isecond);
    remap("SECOND", second);
    isecond = second.read().toInt64();

    if (fields.empty()) return 16;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Be;
    if (field.string_equal_insensitive("nan")) {
        Be.write().setReal(FP::undefined());
    } else {
        Be.write().setReal(field.parse_real(&ok));
        if (!ok) return 17;
        if (!FP::defined(Be.read().toReal())) return 18;
    }

    if (fields.empty()) return 19;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root ZBel;
    if (field.string_equal_insensitive("nan")) {
        Be.write().setReal(FP::undefined());
    } else {
        ZBel.write().setReal(field.parse_real(&ok));
        if (!ok) return 20;
        if (!FP::defined(ZBel.read().toReal())) return 21;
    }

    if (fields.empty()) return 23;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root P;
    {
        double value = field.parse_real(&ok);
        if (!ok) return 24;
        if (!FP::defined(value)) return 25;
        P.write().setDouble(value * 1.33322);
    }
    remap("P", P);

    if (fields.empty()) return 26;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root T;
    {
        double value = field.parse_real(&ok);
        if (!ok) return 27;
        if (!FP::defined(value)) return 28;
        T.write().setDouble(value - 273.15);
    }
    remap("T", T);

    if (fields.empty()) return 29;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root V(field.parse_real(&ok));
    if (!ok) return 30;
    if (!FP::defined(V.read().toReal())) return 31;
    remap("V", V);

    /* Flow sensor, which the manual says is unused */
    if (fields.empty()) return 32;
    fields.pop_front();

    if (fields.empty()) return 33;
    field = fields.front().string_trimmed();
    fields.pop_front();
    if (field.size() != 5) return 34;
    Variant::Root mode(field.toString());
    remap("F2", mode);

    Variant::Flags flags;

    auto modeString = mode.read().toString();
    if (modeString.empty()) return 35;
    switch (modeString.front()) {
    case '0':
        flags.insert("PumpOff");
        break;
    case '1':
        break;
    case '2':
        flags.insert("Alarm");
        break;
    case '5':
        /* Looks this happens during zeros */
        break;
    default:
        return 36;
    }
    modeString.erase(0, 1);

    if (modeString.empty()) return 37;
    switch (modeString.front()) {
    case '0':
        break;
    case '1':
        flags.insert("Blank");
        break;
    case '2':
        flags.insert("Zero");
        break;
    default:
        return 38;
    }
    modeString.erase(0, 1);

    /* Unused digit */
    if (modeString.empty()) return 39;
    modeString.erase(0, 1);

    if (modeString.empty()) return 40;
    switch (modeString.front()) {
    case '1':
    case '2':
        break;
    case '0':   /* Gas phase absorption */
    case '3':   /* Albedo monitor */
    default:
        return 41;
    }
    modeString.erase(0, 1);

    double instrumentWavelength = 0;
    if (modeString.empty()) return 42;
    switch (modeString.front()) {
    case '4':
        instrumentWavelength = 445.0;
        break;
    case '5':
        instrumentWavelength = 530.0;
        break;
    case '6':
        instrumentWavelength = 630.0;
        break;
    case '7':
        instrumentWavelength = 660.0;
        break;
    case '8':
        instrumentWavelength = 760.0;
        break;
    default:
        return 43;
    }
    modeString.erase(0, 1);
    if (!modeString.empty()) return 44;

    if (fields.empty()) return 45;
    field = fields.front().string_trimmed();
    fields.pop_front();
    Variant::Root Bez(field.parse_real(&ok));
    if (!ok) return 46;
    if (!FP::defined(Bez.read().toReal())) return 47;


    if (config.first().strictMode) {
        if (!fields.empty())
            return 48;
    }

    if (!FP::defined(frameTime) &&
            INTEGER::defined(ihour) &&
            ihour >= 0 &&
            ihour <= 23 &&
            INTEGER::defined(iminute) &&
            iminute >= 0 &&
            iminute <= 59 &&
            INTEGER::defined(isecond) &&
            isecond >= 0 &&
            isecond <= 60 &&
            FP::defined(config.first().timeOrigin)) {
        frameTime = config.first().timeOrigin + (double) (ihour * 3600 + iminute * 60 + isecond);

        configAdvance(frameTime);
    }
    if (!FP::defined(frameTime))
        return -1;
    if (FP::defined(lastRecordTime) && frameTime < lastRecordTime)
        return -1;

    double currentWavelength = config.first().wavelength;
    if (!FP::defined(currentWavelength))
        currentWavelength = instrumentWavelength;
    if (!FP::defined(currentWavelength))
        currentWavelength = 530;

    if (!FP::defined(effectiveWavelength) || currentWavelength != effectiveWavelength) {
        effectiveWavelength = currentWavelength;
        zeroEffectiveTime = FP::undefined();
        this->Bez.write().setEmpty();
        this->priorBez.write().setEmpty();
        this->Bezd.write().setEmpty();
        zeroTCount = 0;
        zeroTSum = 0;
        zeroPCount = 0;
        zeroPSum = 0;

        haveEmittedLogMeta = false;
        haveEmittedRealtimeMeta = false;
    }

    const auto &wlCode = Wavelength::code(effectiveWavelength);
    remap("Be" + wlCode, Be);
    remap("ZBel" + wlCode, ZBel);
    remap("Bez" + wlCode, Bez);

    double startTime = lastRecordTime;
    double endTime = frameTime;
    lastRecordTime = frameTime;
    if (!haveEmittedLogMeta && FP::defined(startTime) && loggingEgress != NULL) {
        haveEmittedLogMeta = true;
        loggingEgress->incomingData(buildLogMeta(startTime));
    }

    if (!haveEmittedRealtimeMeta && FP::defined(frameTime) && realtimeEgress != NULL) {
        haveEmittedRealtimeMeta = true;
        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }

    auto ZBe = Be;
    if (flags.count("Zero")) {
        Be.write().setEmpty();

        switch (sampleState) {
        case SAMPLE_RUN:
        case SAMPLE_FLUSH:
            sampleState = SAMPLE_ZERO;
            realtimeStateUpdated = true;

            zeroTCount = 0;
            zeroTSum = 0;
            zeroPCount = 0;
            zeroPSum = 0;
            break;

        case SAMPLE_ZERO:
            break;
        }

        double v = T.read().toDouble();
        if (FP::defined(v)) {
            zeroTCount++;
            zeroTSum += v;
        }
        v = P.read().toDouble();
        if (FP::defined(v)) {
            zeroPCount++;
            zeroPSum += v;
        }
    } else if (flags.count("Blank")) {
        Be.write().setEmpty();

        switch (sampleState) {
        case SAMPLE_RUN:
            sampleState = SAMPLE_FLUSH;
            realtimeStateUpdated = true;
            break;

        case SAMPLE_ZERO:
            zeroCompleted(frameTime, Bez);
            sampleState = SAMPLE_FLUSH;
            realtimeStateUpdated = true;
            break;

        case SAMPLE_FLUSH:
            break;
        }
    } else {
        switch (sampleState) {
        case SAMPLE_FLUSH:
            sampleState = SAMPLE_RUN;
            realtimeStateUpdated = true;
            break;

        case SAMPLE_ZERO:
            zeroCompleted(frameTime, Bez);
            sampleState = SAMPLE_RUN;
            realtimeStateUpdated = true;
            break;

        case SAMPLE_RUN:
            break;
        }
    }

    if (mode.read() != lastMode.read()) {
        realtimeStateUpdated = true;

        if (persistentEgress != NULL && FP::defined(lastMode.getStart())) {
            lastMode.setEnd(frameTime);
            persistentEgress->incomingData(lastMode);
        }
        lastMode.setRoot(mode);
        lastMode.setStart(frameTime);
        lastMode.setEnd(FP::undefined());

        persistentValuesUpdated();
    }

    if (realtimeStateUpdated &&
            (responseState == RESP_PASSIVE_RUN || responseState == RESP_UNPOLLED_RUN)) {
        realtimeStateUpdated = false;

        Variant::Root state;
        switch (sampleState) {
        case SAMPLE_RUN:
            state.write().setString("Run");
            break;
        case SAMPLE_FLUSH:
            state.write().setString("Blank");
            break;
        case SAMPLE_ZERO:
            state.write().setString("Zero");
            break;
        }
        realtimeEgress->incomingData(
                SequenceValue({{}, "raw", "ZSTATE"}, std::move(state), frameTime, FP::undefined()));

        realtimeEgress->incomingData(lastMode);
    }

    this->priorBez = this->Bez;
    this->Bez = Bez;

    if (!FP::defined(zeroEffectiveTime) && FP::defined(Bez.read().toReal()))
        zeroEffectiveTime = frameTime;
    handleZeroUpdate(frameTime);

    if (!FP::defined(startTime) || !FP::defined(endTime))
        return 0;

    logValue(startTime, endTime, "F1", Variant::Root(std::move(flags)));
    logValue(startTime, endTime, "Be" + wlCode, std::move(Be));
    logValue(startTime, endTime, "ZBel" + wlCode, std::move(ZBel));
    logValue(startTime, endTime, "P", std::move(P));
    logValue(startTime, endTime, "T", std::move(T));
    logValue(startTime, endTime, "V", std::move(V));

    realtimeValue(frameTime, "ZBe" + wlCode, std::move(ZBe));

    return 0;
}

void AcquireAerodyneCAPS::configAdvance(double frameTime)
{
    Q_ASSERT(!config.isEmpty());
    int oldSize = config.size();
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.isEmpty());

    if (oldSize == config.size())
        return;
    configurationChanged();
}

void AcquireAerodyneCAPS::configurationChanged()
{
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
}

void AcquireAerodyneCAPS::sendCommand(Util::ByteArray command)
{
    if (!controlStream)
        return;
    command.push_front(0x1B);
    command.push_back('\r');
    controlStream->writeControl(std::move(command));

}

void AcquireAerodyneCAPS::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (FP::defined(frameTime))
        configAdvance(frameTime);

    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;
    switch (sampleState) {
    case SAMPLE_RUN:
        break;
    case SAMPLE_FLUSH:
    case SAMPLE_ZERO:
        /* At the end of the zero, it looks like there's a hiccup while it processes,
         * so allow a delay there */
        unpolledResponseTime += 15.0;
        break;
    }

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
        if (processRecord(frame, frameTime) == 0) {
            if (++autoprobeValidRecords > 5) {
                qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

                responseState = RESP_PASSIVE_RUN;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                timeoutAt(frameTime + unpolledResponseTime + 1.0);

                realtimeStateUpdated = true;

                Variant::Write info = Variant::Write::empty();
                info.hash("ResponseState").setString("PassiveAutoprobeWait");
                event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);
            }
        } else {
            autoprobeValidRecords = 0;
            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;
    case RESP_PASSIVE_WAIT:
        if (processRecord(frame, frameTime) == 0) {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);

            responseState = RESP_PASSIVE_RUN;
            ++autoprobeValidRecords;
            generalStatusUpdated();

            realtimeStateUpdated = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveWait");
            event(frameTime, QObject::tr("Passive communications established."), false, info);
        } else {
            autoprobeValidRecords = 0;
            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;

    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Interactive start comms succeeded at" << Logging::time(frameTime);

            timeoutAt(frameTime + unpolledResponseTime + 1.0);
            responseState = RESP_UNPOLLED_RUN;
            ++autoprobeValidRecords;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            realtimeStateUpdated = true;

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            event(frameTime, QObject::tr("Communications established."), false, info);
        } else if (code > 0) {
            qCDebug(log) << "Interactive start comms failed at" << Logging::time(frameTime) << ":"
                         << frame << "rejected with code" << code;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidRecord"),
                                      frameTime, FP::undefined()));
            }

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
            autoprobeValidRecords = 0;

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            discardData(frameTime + 30.0);

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("FirstValid");
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Communications start rejected the first line (code %1).").arg(code),
                  true, info);

            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        } else {
            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
    }
        break;

    case RESP_PASSIVE_RUN:
    case RESP_UNPOLLED_RUN: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + unpolledResponseTime + 1.0);
            ++autoprobeValidRecords;
        } else if (code > 0) {
            qCDebug(log) << "Line at " << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            Variant::Write info = Variant::Write::empty();

            if (responseState == RESP_PASSIVE_RUN) {
                timeoutAt(FP::undefined());
                responseState = RESP_PASSIVE_WAIT;
                info.hash("ResponseState").setString("PassiveRun");
            } else {
                sendCommand("q");
                responseState = RESP_INTERACTIVE_START_EXITINTERPRETER;
                discardData(frameTime + unpolledResponseTime + 1.0);
                timeoutAt(frameTime + unpolledResponseTime + 6.0);
                info.hash("ResponseState").setString("Run");
            }
            generalStatusUpdated();
            autoprobeValidRecords = 0;

            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        } else {
            lastRecordTime = FP::undefined();
            loggingLost(frameTime);
        }
        break;
    }

    case RESP_INTERACTIVE_START_ENTERINTERPRETER:
        if (frame.toQByteArrayRef().contains("in the command interpreter")) {
            discardData(frameTime + 0.5);
        }
        break;

    default:
        break;
    }
}

void AcquireAerodyneCAPS::incomingInstrumentTimeout(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configAdvance(frameTime);

    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    switch (responseState) {
    case RESP_UNPOLLED_RUN:
        qCDebug(log) << "Timeout in unpolled state" << responseState << "at"
                     << Logging::time(frameTime);
        timeoutAt(frameTime + unpolledResponseTime + 30.0);

        sendCommand("q");
        responseState = RESP_INTERACTIVE_START_EXITINTERPRETER;
        discardData(frameTime + unpolledResponseTime + 2.0);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("UnpolledRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;

    case RESP_PASSIVE_RUN:
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        responseState = RESP_PASSIVE_WAIT;
        generalStatusUpdated();
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        {
            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("PassiveRun");
            event(frameTime, QObject::tr("Timeout waiting for report.  Communications Dropped."),
                  true, info);
        }

        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;

    case RESP_INTERACTIVE_START_EXITINTERPRETER:
    case RESP_INTERACTIVE_START_ENTERINTERPRETER:
    case RESP_INTERACTIVE_START_SETDATE:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_SETAUTOZERO:
    case RESP_INTERACTIVE_START_SETZERODURATION:
    case RESP_INTERACTIVE_START_SETFLUSHDURATION:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
    case RESP_INTERACTIVE_RESTART_WAIT:
        qCDebug(log) << "Timeout in interactive start state" << responseState << "at"
                     << Logging::time(frameTime);

        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        timeoutAt(FP::undefined());
        if (controlStream != NULL)
            controlStream->resetControl();
        discardData(frameTime + 10.0);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;

    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        timeoutAt(frameTime + unpolledResponseTime + 30.0);
        sendCommand("q");
        responseState = RESP_INTERACTIVE_START_EXITINTERPRETER;
        discardData(frameTime + unpolledResponseTime + 2.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;

    default:
        lastRecordTime = FP::undefined();
        loggingLost(frameTime);
        break;
    }
}

void AcquireAerodyneCAPS::discardDataCompleted(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configAdvance(frameTime);

    Q_ASSERT(!config.isEmpty());
    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    switch (responseState) {
    case RESP_INTERACTIVE_START_EXITINTERPRETER:
        sendCommand("Q");
        responseState = RESP_INTERACTIVE_START_ENTERINTERPRETER;
        timeoutAt(frameTime + 20.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveStopEnterInterpreter"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_ENTERINTERPRETER:
        setInstrumentTime = Time::toDateTime(frameTime + 0.5);
        {
            Util::ByteArray d("D ");
            d += QByteArray::number(setInstrumentTime.date().month()).rightJustified(2, '0');
            d += "/";
            d += QByteArray::number(setInstrumentTime.date().day()).rightJustified(2, '0');
            d += "/";
            d += QByteArray::number(setInstrumentTime.date().year()).rightJustified(4, '0');
            sendCommand(std::move(d));
        }
        responseState = RESP_INTERACTIVE_START_SETDATE;
        discardData(frameTime + 0.5);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveSetDate"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_SETDATE: {
        Util::ByteArray d("T ");
        d += QByteArray::number(setInstrumentTime.time().hour()).rightJustified(2, '0');
        d += ":";
        d += QByteArray::number(setInstrumentTime.time().minute()).rightJustified(2, '0');
        d += ":";
        d += QByteArray::number(setInstrumentTime.time().second()).rightJustified(2, '0');
        sendCommand(std::move(d));
    }
        responseState = RESP_INTERACTIVE_START_SETTIME;
        discardData(frameTime + 0.5);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveSetTime"),
                                  frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_SETTIME:
        if (config.first().enableAutozero)
            sendCommand("H");
        else
            sendCommand("h");
        responseState = RESP_INTERACTIVE_START_SETAUTOZERO;
        discardData(frameTime + 0.5);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                       Variant::Root("StartInteractiveSetAutozero"),
                                                       frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_SETAUTOZERO:
        if (config.first().zeroDuration > 0) {
            Util::ByteArray command("M 8 ");
            command += QByteArray::number(config.first().zeroDuration);
            sendCommand(std::move(command));
            responseState = RESP_INTERACTIVE_START_SETZERODURATION;
            discardData(frameTime + 2.0);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveSetZeroDuration"), frameTime, FP::undefined()));
            }
            break;
        }
        /* Fall through */
    case RESP_INTERACTIVE_START_SETZERODURATION:
        if (config.first().flushDuration > 0) {
            Util::ByteArray command("M 7 ");
            command += QByteArray::number(config.first().flushDuration);
            sendCommand(std::move(command));
            responseState = RESP_INTERACTIVE_START_SETFLUSHDURATION;
            discardData(frameTime + 2.0);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveSetFlushDuration"), frameTime, FP::undefined()));
            }
            break;
        }
        /* Fall through */
    case RESP_INTERACTIVE_START_SETFLUSHDURATION:
        sendCommand("G");
        responseState = RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID;

        discardData(frameTime + 2.0, 1);
        timeoutAt(frameTime + unpolledResponseTime * 3.0 + 3.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveUnpolledStartFlushing"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveUnpolledStartReadFirstRecord"), frameTime, FP::undefined()));
        }
        break;


    case RESP_INTERACTIVE_RESTART_WAIT:
        timeoutAt(frameTime + unpolledResponseTime + 30.0);
        sendCommand("q");
        responseState = RESP_INTERACTIVE_START_EXITINTERPRETER;
        discardData(frameTime + unpolledResponseTime + 2.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;

    default:
        break;
    }
}


void AcquireAerodyneCAPS::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_UNUSED(frame);
    Q_UNUSED(frameTime);
}

SequenceValue::Transfer AcquireAerodyneCAPS::getPersistentValues()
{
    PauseLock paused(*this);
    SequenceValue::Transfer result;

    if (FP::defined(lastMode.getStart())) {
        result.emplace_back(lastMode);
    }

    return result;
}

Variant::Root AcquireAerodyneCAPS::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireAerodyneCAPS::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_UNPOLLED_RUN:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}


void AcquireAerodyneCAPS::command(const Variant::Read &command)
{
    Q_ASSERT(!config.isEmpty());

    if (command.hash("StartZero").exists()) {
        if (sampleState != SAMPLE_RUN) {
            qCDebug(log) << "Discarding zero request, sample state:" << sampleState
                         << ", response state:" << responseState;
        } else {
            switch (responseState) {
            case RESP_UNPOLLED_RUN:
                qCDebug(log) << "Initiating zero";
                sendCommand("Z");
                break;

            case RESP_PASSIVE_WAIT:
            case RESP_PASSIVE_RUN:
            case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
            case RESP_AUTOPROBE_PASSIVE_WAIT:
                qCDebug(log) << "Discarding zero request during passive state:" << responseState;
                break;

            case RESP_INTERACTIVE_START_EXITINTERPRETER:
            case RESP_INTERACTIVE_START_ENTERINTERPRETER:
            case RESP_INTERACTIVE_START_SETDATE:
            case RESP_INTERACTIVE_START_SETTIME:
            case RESP_INTERACTIVE_START_SETAUTOZERO:
            case RESP_INTERACTIVE_START_SETZERODURATION:
            case RESP_INTERACTIVE_START_SETFLUSHDURATION:
            case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
            case RESP_INTERACTIVE_RESTART_WAIT:
            case RESP_INTERACTIVE_INITIALIZE:
                qCDebug(log) << "Discarding zero request during start communications state:"
                             << responseState;
                break;
            }
        }
    }
}

Variant::Root AcquireAerodyneCAPS::getCommands()
{
    Variant::Root result;

    result["StartZero"].hash("DisplayName").setString("Start &Zero");
    result["StartZero"].hash("ToolTip").setString("Start a zero offset adjustment.");
    result["StartZero"].hash("Include").array(0).hash("Type").setString("Equal");
    result["StartZero"].hash("Include").array(0).hash("Value").setString("Run");
    result["StartZero"].hash("Include").array(0).hash("Variable").setString("ZSTATE");

    return result;
}

AcquisitionInterface::AutoprobeStatus AcquireAerodyneCAPS::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireAerodyneCAPS::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    Q_ASSERT(!config.isEmpty());
    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    switch (responseState) {
    case RESP_UNPOLLED_RUN:
        /* Already running/started, do nothing */
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << responseState << "at" << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case RESP_INTERACTIVE_START_EXITINTERPRETER:
    case RESP_INTERACTIVE_START_ENTERINTERPRETER:
    case RESP_INTERACTIVE_START_SETDATE:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_SETAUTOZERO:
    case RESP_INTERACTIVE_START_SETZERODURATION:
    case RESP_INTERACTIVE_START_SETFLUSHDURATION:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        timeoutAt(time + unpolledResponseTime + 30.0);
        sendCommand("q");
        responseState = RESP_INTERACTIVE_START_EXITINTERPRETER;
        discardData(time + unpolledResponseTime + 2.0);

        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireAerodyneCAPS::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    Q_ASSERT(!config.isEmpty());
    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
    autoprobeValidRecords = 0;
    discardData(time + 0.5, 1);
    timeoutAt(time + unpolledResponseTime * 7.0 + 2.0);
    generalStatusUpdated();
}

void AcquireAerodyneCAPS::autoprobePromote(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    Q_ASSERT(!config.isEmpty());
    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    switch (responseState) {
    case RESP_UNPOLLED_RUN:
        qCDebug(log) << "Promoted from interactive run to interactive acquisition at "
                     << Logging::time(time);
        /* Reset timeout in case we didn't have a control stream */
        timeoutAt(time + unpolledResponseTime + 2.0);
        break;

    case RESP_INTERACTIVE_START_EXITINTERPRETER:
    case RESP_INTERACTIVE_START_ENTERINTERPRETER:
    case RESP_INTERACTIVE_START_SETDATE:
    case RESP_INTERACTIVE_START_SETTIME:
    case RESP_INTERACTIVE_START_SETAUTOZERO:
    case RESP_INTERACTIVE_START_SETZERODURATION:
    case RESP_INTERACTIVE_START_SETFLUSHDURATION:
    case RESP_INTERACTIVE_START_UNPOLLED_FIRSTVALID:
        /* Already running/started, do nothing */
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);
        break;

    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + unpolledResponseTime + 30.0);
        sendCommand("q");
        responseState = RESP_INTERACTIVE_START_EXITINTERPRETER;
        discardData(time + unpolledResponseTime + 2.0);

        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireAerodyneCAPS::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    Q_ASSERT(!config.isEmpty());
    double unpolledResponseTime = config.first().reportInterval;
    if (!FP::defined(unpolledResponseTime) || unpolledResponseTime < 0.0)
        unpolledResponseTime = 1.0;

    /* Reset timeout in case we didn't have a control stream */
    timeoutAt(time + unpolledResponseTime + 2.0);

    switch (responseState) {
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);

        break;

    case RESP_UNPOLLED_RUN:
        responseState = RESP_PASSIVE_RUN;

        qCDebug(log) << "Promoted from unpolled interactive state to passive acquisition at"
                     << Logging::time(time);
        break;

    default:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_PASSIVE_RUN;
        realtimeStateUpdated = true;

        discardData(FP::undefined());
        generalStatusUpdated();
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireAerodyneCAPS::getDefaults()
{
    AutomaticDefaults result;
    result.name = "E$1$2";
    result.setSerialN81(9600);
    return result;
}


ComponentOptions AcquireAerodyneCAPSComponent::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options, true);

    options.add("basetime",
                new ComponentOptionSingleTime(tr("basetime", "name"), tr("File time origin"),
                                              tr("When dealing with files without a full timestamp on each "
                                                 "record, this time is used to reconstruct the full time from "
                                                 "the time of day."), ""));

    return options;
}

ComponentOptions AcquireAerodyneCAPSComponent::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireAerodyneCAPSComponent::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples(true));
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireAerodyneCAPSComponent::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireAerodyneCAPSComponent::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options, true);
}

QList<ComponentExample> AcquireAerodyneCAPSComponent::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data.")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireAerodyneCAPSComponent::createAcquisitionPassive(const ComponentOptions &options,
                                                                                     const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireAerodyneCAPS(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireAerodyneCAPSComponent::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                     const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireAerodyneCAPS(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireAerodyneCAPSComponent::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                       const std::string &loggingContext)
{
    std::unique_ptr<AcquireAerodyneCAPS> i(new AcquireAerodyneCAPS(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<AcquisitionInterface> AcquireAerodyneCAPSComponent::createAcquisitionInteractive(
        const ValueSegment::Transfer &config,
        const std::string &loggingContext)
{
    std::unique_ptr<AcquireAerodyneCAPS> i(new AcquireAerodyneCAPS(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
