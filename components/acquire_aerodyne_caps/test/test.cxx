/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    double unpolledRemaining;
    enum {
        Sample, PreFlush, Zero, PostFlush,
    } mode;
    double modeRemainingTime;

    bool inInterpreter;
    int wavelengthNumber;
    bool pumpOn;
    double zeroTime;
    double flushTime;

    QDateTime time;
    double Be;
    double Bel;
    double T;
    double P;
    double signal;
    double Bez;

    ModelInstrument()
            : incoming(),
              outgoing(),
              unpolledRemaining(0),
              mode(Sample),
              modeRemainingTime(600),
              time(QDate(2013, 2, 3), QTime(1, 2, 0))
    {
        inInterpreter = false;
        wavelengthNumber = 5;
        pumpOn = true;
        zeroTime = 30.0;
        flushTime = 10.0;

        Be = 50.0;
        Bel = 200.0;
        T = 25.0;
        P = 933.254;
        signal = 1050.1;
        Bez = 10.0;
    }

    void advance(double seconds)
    {
        int idxCR = -1;
        while ((idxCR = incoming.indexOf('\r')) >= 0) {
            QByteArray line(incoming.mid(0, idxCR));

            /* Apparently putting \x1B in strings breaks qmake's parser */
            QByteArray escape;
            escape.append(0x1B);

            if (line == escape + "q") {
                inInterpreter = false;
                unpolledRemaining = 1.0;
            } else if (line == escape + "Z") {
                if (!inInterpreter) {
                    mode = PreFlush;
                    modeRemainingTime = flushTime;
                }
            } else if (line == escape + "Q") {
                inInterpreter = true;
                unpolledRemaining = FP::undefined();
                outgoing.append("CAPS online\rSystem in the command interpreter\rReady:\r");
            } else if (!inInterpreter) {
                /* No response */
            } else if (line == escape + "G" ||
                    line == escape + "g" ||
                    line == escape + "Q" ||
                    line == escape + "q") {
                inInterpreter = false;
                unpolledRemaining = 1.0;
            } else if (line == escape + "h") {
                modeRemainingTime = FP::undefined();
                mode = Sample;
            } else if (line == escape + "H") {
                if (!FP::defined(modeRemainingTime)) {
                    modeRemainingTime = 600.0;
                    mode = Sample;
                }
            } else if (line.startsWith(escape + "D ")) {
                QList<QByteArray> fields(line.split('/'));
                if (fields.size() == 4) {
                    time.setDate(QDate(fields.at(3).toInt(), fields.at(1).toInt(),
                                       fields.at(2).toInt()));
                }
            } else if (line.startsWith(escape + "T ")) {
                QList<QByteArray> fields(line.split(':'));
                if (fields.size() == 4) {
                    time.setTime(QTime(fields.at(1).toInt(), fields.at(2).toInt(),
                                       fields.at(3).toInt()));
                }
            } else if (line.startsWith(escape + "M ")) {
                QList<QByteArray> fields(line.split(' '));
                if (fields.size() == 3) {
                    switch (fields.at(1).toInt()) {
                    case 8:
                        zeroTime = fields.at(2).toDouble();
                        break;
                    case 7:
                        flushTime = fields.at(2).toDouble();
                        break;
                    default:
                        break;
                    }
                }
            }

            if (idxCR >= incoming.size() - 1) {
                incoming.clear();
                break;
            }
            incoming = incoming.mid(idxCR + 1);
        }

        if (FP::defined(modeRemainingTime)) {
            modeRemainingTime -= seconds;
            if (modeRemainingTime <= 0.0) {
                switch (mode) {
                case Sample:
                    mode = PreFlush;
                    modeRemainingTime = flushTime;
                    break;
                case PreFlush:
                    mode = Zero;
                    modeRemainingTime = zeroTime;
                    break;
                case Zero:
                    mode = PostFlush;
                    modeRemainingTime = flushTime;
                    break;
                case PostFlush:
                    mode = Sample;
                    modeRemainingTime = 600.0;
                    break;
                }
            }
        }

        if (FP::defined(unpolledRemaining)) {
            unpolledRemaining -= seconds;
            while (unpolledRemaining <= 0.0) {
                unpolledRemaining += 1.0;

                outgoing.append(QByteArray::number(time.time().hour()).rightJustified(2, '0'));
                outgoing.append(QByteArray::number(time.time().minute()).rightJustified(2, '0'));
                outgoing.append(QByteArray::number(time.time().second()).rightJustified(2, '0'));
                outgoing.append(',');
                outgoing.append(mode == Zero ? "NaN" : QByteArray::number(Be, 'f', 3));
                outgoing.append(',');
                outgoing.append(mode == Zero ? "NaN" : QByteArray::number(Bel, 'f', 2));
                outgoing.append(',');
                outgoing.append(QByteArray::number(P / 1.33322, 'f', 2));
                outgoing.append(',');
                outgoing.append(QByteArray::number(T + 273.15, 'f', 2));
                outgoing.append(',');
                outgoing.append(QByteArray::number(signal, 'f', 2));
                outgoing.append(",0.000,");
                if (pumpOn)
                    outgoing.append('1');
                else
                    outgoing.append('0');
                switch (mode) {
                case Sample:
                    outgoing.append('0');
                    break;
                case PreFlush:
                    outgoing.append('1');
                    break;
                case Zero:
                    outgoing.append('2');
                    break;
                case PostFlush:
                    outgoing.append('1');
                    break;
                }
                outgoing.append('0');
                outgoing.append('2');
                outgoing.append(QByteArray::number(wavelengthNumber));
                outgoing.append(',');
                outgoing.append(QByteArray::number(Bez, 'f', 3));
                outgoing.append('\r');
            }
        }
    }
};

class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream,
                   double time = FP::undefined(),
                   const SequenceName::Component &wl = "G")
    {
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("F2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Be" + wl, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZBel" + wl, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("P", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("V", Variant::Root(), QString(), time))
            return false;

        if (!stream.hasMeta("Tz", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Pz", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Bez" + wl, Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream,
                           double time = FP::undefined(),
                           const SequenceName::Component &wl = "G")
    {
        if (!stream.hasMeta("Bezd" + wl, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZBe" + wl, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZSTATE", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkContiguous(StreamCapture &stream, const SequenceName::Component &wl = "G")
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("F2"))
            return false;
        if (!stream.checkContiguous("Be" + wl))
            return false;
        if (!stream.checkContiguous("ZBel" + wl))
            return false;
        if (!stream.checkContiguous("P"))
            return false;
        if (!stream.checkContiguous("T"))
            return false;
        if (!stream.checkContiguous("V"))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream,
                     const ModelInstrument &model,
                     double time = FP::undefined(),
                     const SequenceName::Component &wl = "G")
    {
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(), time))
            return false;
        if (!stream.hasAnyMatchingValue("Be" + wl, Variant::Root(model.Be), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZBel" + wl, Variant::Root(model.Bel), time))
            return false;
        if (!stream.hasAnyMatchingValue("P", Variant::Root(model.P), time))
            return false;
        if (!stream.hasAnyMatchingValue("T", Variant::Root(model.T), time))
            return false;
        if (!stream.hasAnyMatchingValue("V", Variant::Root(model.signal), time))
            return false;
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream,
                             const ModelInstrument &model,
                             double time = FP::undefined())
    {
        Q_UNUSED(model);
        if (!stream.hasAnyMatchingValue("ZSTATE", Variant::Root(), time))
            return false;
        return true;
    }

    bool checkZero(StreamCapture &stream,
                   const ModelInstrument &model,
                   double time = FP::undefined(),
                   const SequenceName::Component &wl = "G")
    {
        if (!stream.hasAnyMatchingValue("Bez" + wl, Variant::Root(model.Bez), time))
            return false;
        if (!stream.hasAnyMatchingValue("Tz", Variant::Root(model.T), time))
            return false;
        if (!stream.hasAnyMatchingValue("Pz", Variant::Root(model.P), time))
            return false;
        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_aerodyne_caps"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_aerodyne_caps"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 200; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(0.2);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.2);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        instrument.modeRemainingTime = 0.0;
        for (int i = 0; i < 200; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(checkZero(persistent, instrument));
        QVERIFY(checkZero(realtime, instrument));
    }

    void interactiveAutoprobeSetTimings()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["ZeroDuration"] = 30;
        cv["FlushDuration"] = 30;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.2);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        instrument.modeRemainingTime = 0.0;
        for (int i = 0; i < 200; i++) {
            control.advance(1.0);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(checkZero(persistent, instrument));
        QVERIFY(checkZero(realtime, instrument));
    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Wavelength"].setReal(630);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 200; i++) {
            control.advance(0.2);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging, "R"));

        QVERIFY(checkMeta(logging, FP::undefined(), "R"));
        QVERIFY(checkMeta(realtime, FP::undefined(), "R"));
        QVERIFY(checkRealtimeMeta(realtime, FP::undefined(), "R"));

        QVERIFY(checkValues(logging, instrument, FP::undefined(), "R"));
        QVERIFY(checkValues(realtime, instrument, FP::undefined(), "R"));
        QVERIFY(checkRealtimeValues(realtime, instrument, FP::undefined()));
    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.wavelengthNumber = 4;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);

        QVERIFY(persistent.hasAnyMatchingValue("BezB", Variant::Root(instrument.Bez)));

        Variant::Write cmd = Variant::Write::empty();
        cmd["StartZero"].setBool(true);
        double zeroIssueTime = control.time();
        interface->incomingCommand(cmd);
        instrument.Bez = 45.0;
        control.advance(0.1);
        control.advance(0.1);

        while (control.time() < 120.0) {
            control.advance(0.5);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Zero"), zeroIssueTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);
        double inZeroTime = control.time();

        while (control.time() < 120.0) {
            control.advance(0.5);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run"), inZeroTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging, "B"));

        QVERIFY(checkMeta(logging, FP::undefined(), "B"));
        QVERIFY(checkMeta(realtime, FP::undefined(), "B"));
        QVERIFY(checkRealtimeMeta(realtime, FP::undefined(), "B"));

        QVERIFY(checkValues(logging, instrument, FP::undefined(), "B"));
        QVERIFY(checkValues(realtime, instrument, FP::undefined(), "B"));
        QVERIFY(checkRealtimeValues(realtime, instrument, FP::undefined()));

        QVERIFY(checkZero(persistent, instrument, zeroIssueTime, "B"));
        QVERIFY(checkZero(realtime, instrument, zeroIssueTime, "B"));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
