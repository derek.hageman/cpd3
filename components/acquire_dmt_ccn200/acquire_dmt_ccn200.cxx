/*
 * Copyright (c) 2021 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <cctype>
#include <QRegularExpression>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "core/csv.hxx"
#include "core/timeutils.hxx"
#include "datacore/stream.hxx"
#include "datacore/variant/composite.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"
#include "io/process.hxx"
#include "io/drivers/file.hxx"
#include "algorithms/statistics.hxx"

#include "acquire_dmt_ccn200.hxx"


//#define RECORD_MODEL_INPUTS

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;
using namespace CPD3::Smoothing;
using namespace CPD3::Algorithms;

static constexpr std::size_t totalBins = 21;

static const std::array<std::string, 10>
        instrumentFlagTranslation{"LaserOverCurrent",             /* 1 */
                                  "FirstStageMonitorOverVoltage", /* 2 */
                                  "FlowOutOfRange",               /* 4 */
                                  "TemperatureOutOfRange",        /* 8 */
                                  "SampleTemperatureOutOfRange",  /* 16 */
                                  "OPCError",                     /* 32 */
                                  "CCNCountsLow",                 /* 64 */
                                  "ColumnTemperaturesUnstableAlarm",/* 128 */
                                  "NoOPCCommunications",          /* 256 */
                                  "DuplicatedFile",               /* 512 */
};

static const std::string modelDefaultParameters = R"(*
* ==[ PARAMETERS FOR DMT CCN COUNTER SIMULATOR - UNITS IN SI ]==
*
===[ INLET PARAMETRES ]=================================================
Parabolic inlet velocity profile  [UPARAB]
.TRUE.
Sheath, Aerosol inlet RH (0-1 scale) [RHINS, RHINA]
1.0, 0.95
===[ GRID PARAMETERS ]==================================================
Number of cells in X-direction, Y-direction  [NI, NJ]
50, 50
===[ DEPENDENT VARIABLES ]==============================================
Solve for U, V, P equations ?
.false.
Solve for T, C equations ?
.true., .true.
===[ TERMINATION CRITERIA FOR ITERATIONS ]==============================
Maximum number of iterations per time step  [MAXIT]
100
Maximum acceptable value of residuals  [SORMAX]
1.0E-6
===[ UNDER-RELAXATION ]=================================================
Under-relaxation factor for U, P equation  [URFU, URFP]
0.7, 0.05
Under-relaxation factor for T, C equation  [URFT, URFC]
1.0, 1.0
===[ EQUATION SOLVER SELECTION ]========================================
Solver selection for U, V, P equation  [ISLVU, ISLVP]
1, 1
Solver selection for T, C equation  [ISLVT, ISLVC]
2, 2
===[ INPUT-OUTPUT PARAMETERS ]==========================================
Exit S is average over last (%) of chamber (0=exit point only) [SOAVGP]
5
)";


bool AcquireDMTCCN200::ModelInput::valid() const
{
    if (!FP::defined(Qtot) ||
            !FP::defined(Qshaer) ||
            !FP::defined(P) ||
            !FP::defined(Tinlet) ||
            !FP::defined(Tcold) ||
            !FP::defined(Thot))
        return false;
    if (Qtot < 1.6667E-6 ||
            Qtot > 1E-5 ||
            Qshaer < 2.0 ||
            Qshaer > 20.0 ||
            P < 50000.0 ||
            P > 120000.0 ||
            Tinlet < 290.0 ||
            Tinlet > 320.0 ||
            Tcold < 290.0 ||
            Tcold > 320.0 ||
            Thot < 290.0 ||
            Thot > 320.0)
        return false;
    return true;
}

AcquireDMTCCN200::ModelInput::ModelInput(std::deque<ModelPoint> &points, double intervalEnd)
        : ModelInput()
{
    start = points.front().start;

    struct Calculate {
        const std::deque<ModelPoint> &points;

        Calculate(const std::deque<ModelPoint> &points) : points(points)
        { }

        double weightedAverage(const std::function<double(const ModelPoint &)> &fetch)
        {
            double totalTime = 0.0;
            for (const auto &p : points) {
                double dT = p.end - p.start;
                if (dT <= 0.0)
                    continue;
                if (!FP::defined(fetch(p)))
                    continue;
                totalTime += dT;
            }
            if (totalTime <= 0.0)
                return FP::undefined();
            double result = 0.0;
            for (const auto &p : points) {
                double dT = p.end - p.start;
                if (dT <= 0.0)
                    continue;
                double value = fetch(p);
                if (!FP::defined(value))
                    continue;
                result += value * (dT / totalTime);
            }
            return result;
        }
    };

    Calculate calc(points);

    double Q1 = calc.weightedAverage([](const ModelPoint &p) { return p.Q1; });
    double Q2 = calc.weightedAverage([](const ModelPoint &p) { return p.Q2; });
    if (FP::defined(Q1) && FP::defined(Q2)) {
        Qtot = (Q1 + Q2) * 1E-3 / 60.0;
        if (Q1 != 0.0) {
            Qshaer = Q2 / Q1;
        } else {
            Qshaer = FP::undefined();
        }
    } else {
        Qtot = FP::undefined();
        Qshaer = FP::undefined();
    }

    P = calc.weightedAverage([](const ModelPoint &p) { return p.P; });
    if (FP::defined(P))
        P *= 1E2;
    Tcold = calc.weightedAverage([](const ModelPoint &p) { return p.T1; });
    if (FP::defined(Tcold))
        Tcold += 273.15;
    Thot = calc.weightedAverage([](const ModelPoint &p) { return p.T3; });
    if (FP::defined(Thot))
        Thot += 273.15;
    Tinlet = calc.weightedAverage([](const ModelPoint &p) { return p.Tu; });
    if (FP::defined(Tinlet))
        Tinlet += 273.15;

    if (points.back().end <= intervalEnd || intervalEnd <= points.front().start) {
        end = points.back().end;
        points.clear();
    } else {
        end = intervalEnd;
        points.erase(points.begin(), points.end() - 1);
        points.back().start = intervalEnd;
    }
}

class AcquireDMTCCN200::ModelRunner {
    std::map<std::uint_fast64_t, ModelResult> resultReady;
    std::uint_fast64_t resultNext;
    std::uint_fast64_t resultSubmit;

    struct Worker {
        ModelRunner *runner;
        Util::ByteArray buffer;
        std::shared_ptr<IO::Process::Instance> process;
        std::unique_ptr<IO::Process::Stream> io;
        std::deque<std::pair<std::uint_fast64_t, ModelInput>> sent;

        Worker() = default;

        Worker(const Worker &) = delete;

        Worker &operator=(const Worker &) = delete;

        Worker(Worker &&) = default;

        Worker &operator=(Worker &&) = default;

        bool processReady()
        {
            bool anyAdded = false;
            Util::ByteArray::LineIterator it(buffer);
            while (auto line = it.next()) {
                if (sent.empty())
                    continue;

                auto data = line.string_trimmed();
                bool ok = false;

                ModelResult result;
                result.input = sent.front().second;
                result.valid = true;
                result.ss = data.parse_real(&ok);
                if (!ok) {
                    result.ss = FP::undefined();
                }

                runner->resultReady.emplace(sent.front().first, std::move(result));
                sent.pop_front();
                anyAdded = true;
            }

            buffer.pop_front(it.getCurrentOffset());

            return anyAdded;
        }

        bool submit(const ModelInput &input, std::uint_fast64_t id)
        {
            if (!process) {
                process = runner->spawn.create();
                if (!process) {
                    qWarning("Failed to create CCN model process");
                    return false;
                }
                if (!process->start()) {
                    qWarning("Failed to start CCN model process");
                    process.reset();
                    return false;
                }
            }
            if (!io) {
                io = process->outputStream(true);
                if (!io) {
                    qWarning("Failed to get CCN model interface");
                    process.reset();
                    return false;
                }
                io->read.connect([this](const Util::ByteArray &data) {
                    {
                        std::lock_guard<std::mutex> lock(runner->mutex);
                        buffer += data;
                        if (!processReady())
                            return;
                    }
                    runner->available();
                });
                io->ended.connect([this]() {
                    std::lock_guard<std::mutex> lock(runner->mutex);
                    io.reset();
                    process.reset();
                });
                io->start();
                if (runner->parametersFile) {
                    io->write(runner->parametersFile->filename() + "\n");
                }
            }

            Util::ByteArray result;
            result += QByteArray::number(input.Qtot, 'E', 6);
            result += ", ";
            result += QByteArray::number(input.Qshaer, 'E', 6);
            result += ", ";
            result += QByteArray::number(input.P, 'E', 6);
            result += ", ";
            result += QByteArray::number(input.Tinlet, 'E', 6);
            result += ", ";
            result += QByteArray::number(input.Tcold, 'E', 6);
            result += ", ";
            result += QByteArray::number(input.Thot, 'E', 6);
            result += ", \'nul\'\n";
            io->write(std::move(result));

            sent.emplace_back(id, input);

            return true;
        }
    };

    std::vector<Worker> workers;

    std::mutex mutex;
    IO::Process::Spawn spawn;
    std::shared_ptr<IO::File::Backing> parametersFile;
    std::size_t maximumBacklog;

    bool expectingMore() const
    {
        for (const auto &worker : workers) {
            if (worker.sent.empty())
                continue;
            return true;
        }
        return false;
    }

public:
    ModelRunner(const std::string &command,
                const std::string &parameters,
                std::size_t maximumBacklog) : resultNext(0),
                                              resultSubmit(0),
                                              spawn(command),
                                              maximumBacklog(maximumBacklog)
    {
        spawn.inputStream = IO::Process::Spawn::StreamMode::Capture;
        spawn.outputStream = IO::Process::Spawn::StreamMode::Capture;
        spawn.environmentSet.emplace("GFORTRAN_UNBUFFERED_ALL", "1");

        auto n = std::thread::hardware_concurrency();
        if (n > 1) {
            workers.resize(n);
        } else {
            workers.resize(1);
        }
        for (auto &worker : workers) {
            worker.runner = this;
        }

        if (parameters.empty())
            return;

        parametersFile = IO::Access::temporaryFile();
        if (!parametersFile) {
            qWarning("Unable to create model parameters file");
            return;
        }
        auto write = parametersFile->block();
        if (!write) {
            qWarning("Unable to open parameters file for writing");
            return;
        }
        write->write(parameters);
    }

    ~ModelRunner() = default;

    void submit(const ModelInput &input)
    {
        {
            std::lock_guard<std::mutex> lock(mutex);
            Worker *target = nullptr;
            std::size_t backlogSize = 0;
            for (auto &worker : workers) {
                backlogSize += worker.sent.size();

                if (!target) {
                    target = &worker;
                } else if (worker.sent.size() < target->sent.size()) {
                    target = &worker;
                }
            }
            Q_ASSERT(target != nullptr);

            if (maximumBacklog == static_cast<std::size_t>(-1) || backlogSize <= maximumBacklog) {
                if (target->submit(input, resultSubmit)) {
                    resultSubmit++;
                    return;
                }
            }

            ModelResult result;
            result.input = input;
            result.valid = false;
            result.ss = FP::undefined();
            resultReady.emplace(resultSubmit, std::move(result));
            resultSubmit++;
        }
        available();
    }

    std::vector<ModelResult> consume()
    {
        std::vector<ModelResult> result;
        std::lock_guard<std::mutex> lock(mutex);
        while (!resultReady.empty()) {
            auto next = resultReady.begin();
            if (next->first > resultNext)
                break;

            result.emplace_back(std::move(next->second));
            resultNext = next->first + 1;
            resultReady.erase(next);
        }
        return result;
    }

    void finish()
    {
        std::condition_variable cv;
        Threading::Receiver rx;
        available.connect(rx, [&] { cv.notify_all(); });

        std::unique_lock<std::mutex> lock(mutex);
        if (maximumBacklog == static_cast<std::size_t>(-1)) {
            while (expectingMore()) {
                cv.wait(lock);
            }
        } else {
            using Clock = std::chrono::steady_clock;
            auto timeout = Clock::now() + std::chrono::seconds(30);

            while (timeout > Clock::now()) {
                if (!expectingMore())
                    break;
                cv.wait_until(lock, timeout);
            }
        }
    }

    Threading::Signal<> available;

    static std::size_t defaultMaximumBacklog()
    {
        auto n = std::thread::hardware_concurrency();
        if (n <= 1)
            return 2;
        return n * 2;
    }
};


AcquireDMTCCN200::Configuration::Configuration() : start(FP::undefined()),
                                                   end(FP::undefined()),
                                                   enableModel(true),
#ifndef Q_OS_WIN32
                                                   modelProgram("cpd3_ccn_dmtmodel"),
#else
        modelProgram("cpd3_ccn_dmtmodel.exe"),
#endif
                                                   modelParameters(modelDefaultParameters),
                                                   maximumModelBacklog(
                                                           ModelRunner::defaultMaximumBacklog()),
                                                   useReportedStability(true),
                                                   useCalculatedStability(false),
                                                   timeOrigin(FP::undefined()),
                                                   reportInterval(2.0),
                                                   strictMode(false)
{ }

AcquireDMTCCN200::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s),
          end(e),
          enableModel(other.enableModel),
          modelProgram(other.modelProgram),
          modelParameters(other.modelParameters),
          maximumModelBacklog(other.maximumModelBacklog),
          useReportedStability(other.useReportedStability),
          useCalculatedStability(other.useCalculatedStability),
          timeOrigin(other.timeOrigin),
          reportInterval(other.reportInterval),
          strictMode(other.strictMode)
{ }

void AcquireDMTCCN200::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("DMT");
    instrumentMeta["Model"].setString("CCN200");

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;

    streamAge.fill(0);
    streamTime.fill(FP::undefined());

    lastSampleFlowA = FP::undefined();
    lastSampleFlowB = FP::undefined();
    lastHRecordTime = FP::undefined();
    lastBinNumberA = INTEGER::undefined();
    lastBinNumberB = INTEGER::undefined();
    reportedStableA = true;
    reportedStableB = true;

    dtStabilityA->reset();
    dtStabilityB->reset();
}

AcquireDMTCCN200::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s),
          end(e),
          enableModel(true),
#ifndef Q_OS_WIN32
          modelProgram("cpd3_ccn_dmtmodel"),
#else
        modelProgram("cpd3_ccn_dmtmodel.exe"),
#endif
          modelParameters(modelDefaultParameters),
          maximumModelBacklog(ModelRunner::defaultMaximumBacklog()),
          useReportedStability(true),
          useCalculatedStability(false),
          timeOrigin(FP::undefined()),
          reportInterval(2.0),
          strictMode(false)
{
    setFromSegment(other);
}

AcquireDMTCCN200::Configuration::Configuration(const Configuration &under,
                                               const ValueSegment &over,
                                               double s,
                                               double e) : start(s),
                                                           end(e),
                                                           enableModel(under.enableModel),
                                                           modelProgram(under.modelProgram),
                                                           modelParameters(under.modelParameters),
                                                           maximumModelBacklog(
                                                                   under.maximumModelBacklog),
                                                           useReportedStability(
                                                                   under.useReportedStability),
                                                           useCalculatedStability(
                                                                   under.useCalculatedStability),
                                                           timeOrigin(under.timeOrigin),
                                                           reportInterval(under.reportInterval),
                                                           strictMode(under.strictMode)
{
    setFromSegment(over);
}

void AcquireDMTCCN200::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["Model/Enable"].exists())
        enableModel = config["Model/Enable"].toBool();
    if (config["Model/Program"].exists())
        modelProgram = config["Model/Program"].toString();

    if (INTEGER::defined(config["Model/MaximumBacklog"].toInteger())) {
        auto n = config["Model/MaximumBacklog"].toInteger();
        if (n > 0) {
            maximumModelBacklog = static_cast<std::size_t>(n);
        } else {
            maximumModelBacklog = static_cast<std::size_t>(-1);
        }
    }

    if (config["Model/Parameters"].exists()) {
        modelParameters = config["Model/Parameters"].toString();
    }

    if (config["StrictMode"].exists())
        strictMode = config["StrictMode"].toBool();

    if (config["BaseTime"].exists())
        timeOrigin = config["BaseTime"].toDouble();

    if (config["ReportInterval"].exists())
        reportInterval = config["ReportInterval"].toDouble();

    if (config["Processing/UseReportedStability"].exists()) {
        useReportedStability = config["Processing/UseReportedStability"].toBool();
    }

    if (config["Processing/UseCalculatedStability"].exists()) {
        useCalculatedStability = config["Processing/UseCalculatedStability"].toBool();
    }
}

AcquireDMTCCN200::AcquireDMTCCN200(const ValueSegment::Transfer &configData,
                                   const std::string &loggingContext) : FramedInstrument("magic200",
                                                                                         loggingContext),
                                                                        autoprobeStatus(
                                                                                AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                        responseState(
                                                                                ResponseState::Initialize),
                                                                        loggingMux(
                                                                                static_cast<std::size_t>(LogStreamID::TOTAL))
{
    DynamicTimeInterval::Variable oneMinute;
    oneMinute.set(Time::Minute, 1, true);
    modelInterval.reset(
            DynamicTimeInterval::fromConfiguration(configData, "Model/Interval", FP::undefined(),
                                                   FP::undefined(), true, &oneMinute));
    processingInterval.reset(
            DynamicTimeInterval::fromConfiguration(configData, "Processing/Interval",
                                                   FP::undefined(), FP::undefined(), true,
                                                   &oneMinute));

    Variant::Write defaultSmoother = Variant::Write::empty();
    defaultSmoother["Type"].setString("SinglePoint");
    dtStabilityA.reset(BaselineSmoother::fromConfiguration(defaultSmoother, configData,
                                                           "Processing/TemperatureStability"));
    dtStabilityB.reset(BaselineSmoother::fromConfiguration(defaultSmoother, configData,
                                                           "Processing/TemperatureStability"));

    setDefaultInvalid();
    config.emplace_back();
    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }
    configurationChanged();
}

void AcquireDMTCCN200::setToAutoprobe()
{ responseState = ResponseState::Autoprobe_Initialize; }

void AcquireDMTCCN200::setToInteractive()
{ responseState = ResponseState::Initialize; }


ComponentOptions AcquireDMTCCN200Component::getBaseOptions()
{
    ComponentOptions options;

    TimeIntervalSelectionOption *ti = new TimeIntervalSelectionOption(tr("model-interval", "name"),
                                                                      tr("Model processing interval"),
                                                                      tr("This is the interval the model is executed at.  This defines the "
                                                                         "amount of data averaged together to make a single request for the "
                                                                         "model to generate a supersaturation for."),
                                                                      tr("One minute",
                                                                         "default model interval"));
    ti->setDefaultAligned(true);
    options.add("model-interval", ti);

    ti = new TimeIntervalSelectionOption(tr("processing-interval", "name"),
                                         tr("General processing interval"),
                                         tr("This is the interval the that general processing is executed at.  "
                                            "This defines the amount of time that enters into the reported dT "
                                            "standard deviation."),
                                         tr("One minute", "default processing interval"));
    ti->setDefaultAligned(true);
    options.add("processing-interval", ti);

    BaselineSmootherOption *bs =
            new BaselineSmootherOption(tr("stability", "name"), tr("dT stability smoother"),
                                       tr("This is the smoother that is used to by the acquisition system to "
                                          "determine if T3-T1 is stable."),
                                       tr("Always stable", "default stability"));
    bs->setDefault(new BaselineSinglePoint);
    options.add("stability", bs);

    return options;
}

AcquireDMTCCN200::AcquireDMTCCN200(const ComponentOptions &options,
                                   const std::string &loggingContext) : FramedInstrument("ccn200",
                                                                                         loggingContext),
                                                                        autoprobeStatus(
                                                                                AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                        responseState(
                                                                                ResponseState::Initialize),
                                                                        loggingMux(
                                                                                static_cast<std::size_t>(LogStreamID::TOTAL))
{
    DynamicTimeInterval::Variable oneMinute;
    oneMinute.set(Time::Minute, 1, true);

    if (options.isSet("model-interval")) {
        modelInterval.reset(qobject_cast<TimeIntervalSelectionOption *>(
                options.get("model-interval"))->getInterval());
    } else {
        modelInterval.reset(oneMinute.clone());
    }

    if (options.isSet("processing-interval")) {
        processingInterval.reset(qobject_cast<TimeIntervalSelectionOption *>(
                options.get("processing-interval"))->getInterval());
    } else {
        processingInterval.reset(oneMinute.clone());
    }

    dtStabilityA.reset(
            qobject_cast<BaselineSmootherOption *>(options.get("stability"))->getSmoother());
    dtStabilityB.reset(
            qobject_cast<BaselineSmootherOption *>(options.get("stability"))->getSmoother());

    setDefaultInvalid();
    Configuration base;
    base.maximumModelBacklog = -1;

    if (options.isSet("basetime")) {
        base.timeOrigin = qobject_cast<ComponentOptionSingleTime *>(options.get("basetime"))->get();
    }

    if (options.isSet("model-backlog")) {
        auto i = qobject_cast<ComponentOptionSingleInteger *>(options.get("model-backlog"))->get();
        if (INTEGER::defined(i) && i > 0)
            base.maximumModelBacklog = static_cast<std::size_t>(i);
        else
            base.maximumModelBacklog = static_cast<std::size_t>(-1);
    }

    config.emplace_back(base);
    configurationChanged();
}

AcquireDMTCCN200::~AcquireDMTCCN200() = default;


SequenceValue::Transfer AcquireDMTCCN200::buildLogMeta(double time) const
{
    Q_ASSERT(!config.empty());

    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_dmt_ccn200");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());

    result.emplace_back(SequenceName({}, "raw_meta", "T1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("TEC 1 temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("TEC 1"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(3);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column A"));

    result.emplace_back(SequenceName({}, "raw_meta", "T2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("TEC 2 temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("TEC 2"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(4);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column A"));

    result.emplace_back(SequenceName({}, "raw_meta", "T3"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("TEC 3 temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("TEC 3"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(5);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column A"));

    result.emplace_back(SequenceName({}, "raw_meta", "T4"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Sample temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(11);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column A"));

    result.emplace_back(SequenceName({}, "raw_meta", "T5"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("OPC temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("OPC"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(9);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column A"));

    result.emplace_back(SequenceName({}, "raw_meta", "T6"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Nafion temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Nafion"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(10);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column A"));

    result.emplace_back(SequenceName({}, "raw_meta", "Tu"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Inlet temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Inlet"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(8);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column A"));

    result.emplace_back(SequenceName({}, "raw_meta", "DT"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("GroupUnits").setString("d\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Temperature gradient setpoint");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("dT setpoint"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(6);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column A"));

    result.emplace_back(SequenceName({}, "raw_meta", "DTg"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("GroupUnits").setString("d\xC2\xB0\x43");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Standard deviation of T3 - T1");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("dT stddev"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(7);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column A"));

    /* NOTE: Volume flows so no report T/P */
    result.emplace_back(SequenceName({}, "raw_meta", "Q1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Sample flow");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(13);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column A"));

    result.emplace_back(SequenceName({}, "raw_meta", "Q2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Sheath flow");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sheath"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(14);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column A"));

    result.emplace_back(SequenceName({}, "raw_meta", "U"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0.000");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Supersaturation setting");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("SS Setting"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column A"));


    result.emplace_back(SequenceName({}, "raw_meta", "P"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Sample pressure");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(12);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column A"));

    result.emplace_back(SequenceName({}, "raw_meta", "V1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("First stage monitor");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("1st stage monitor"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(16);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column A"));

    result.emplace_back(SequenceName({}, "raw_meta", "V2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000.00");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Proportional valve");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Proportional valve"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(17);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column A"));

    result.emplace_back(SequenceName({}, "raw_meta", "A"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("mA");
    result.back().write().metadataReal("Description").setString("Laser current");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Laser current"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(15);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column A"));

    result.emplace_back(SequenceName({}, "raw_meta", "N"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0");
    result.back().write().metadataReal("Units").setString("cm\xE2\x81\xBB³");
    result.back().write().metadataReal("Description").setString("Total concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Concentration"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column A"));

    result.emplace_back(SequenceName({}, "raw_meta", "Np"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0");
    result.back().write().metadataReal("Units").setString("cm\xE2\x81\xBB³");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Total concentration without unstable data");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);

    result.emplace_back(SequenceName({}, "raw_meta", "ZBin"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataInteger("Format").setString("0");
    result.back()
          .write()
          .metadataInteger("Description")
          .setString("Instrument reported minimum bin");
    result.back().write().metadataInteger("Source").set(instrumentMeta);
    result.back().write().metadataInteger("Processing").toArray().after_back().set(processing);
    result.back().write().metadataInteger("Smoothing").hash("Mode").setString("None");

    result.emplace_back(SequenceName({}, "raw_meta", "Nb"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataArray("Count").setInt64(totalBins);
    result.back().write().metadataArray("Children").metadataReal("Format").setString("00000.0");
    result.back()
          .write()
          .metadataArray("Children")
          .metadataReal("Units")
          .setString("cm\xE2\x81\xBB³");
    result.back().write().metadataArray("Description").setString("Bin concentration");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Realtime").hash("Page").setInteger(2);
    result.back()
          .write()
          .metadataArray("Realtime")
          .hash("Name")
          .setString(QObject::tr("Concentration A"));
    result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInt64(1);


    result.emplace_back(SequenceName({}, "raw_meta", "Ts1"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("TEC 1 temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("TEC 1"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(3);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column B"));

    result.emplace_back(SequenceName({}, "raw_meta", "Ts2"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("TEC 2 temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("TEC 2"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(4);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column B"));

    result.emplace_back(SequenceName({}, "raw_meta", "Ts3"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("TEC 3 temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("TEC 3"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(5);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column B"));

    result.emplace_back(SequenceName({}, "raw_meta", "Ts4"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Sample temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(11);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column B"));

    result.emplace_back(SequenceName({}, "raw_meta", "Ts5"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("OPC temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("OPC"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(9);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column B"));

    result.emplace_back(SequenceName({}, "raw_meta", "Ts6"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Nafion temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Nafion"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(10);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column B"));

    result.emplace_back(SequenceName({}, "raw_meta", "Tus"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Inlet temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Inlet"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(8);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column B"));

    result.emplace_back(SequenceName({}, "raw_meta", "DTs"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("GroupUnits").setString("d\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Temperature gradient setpoint");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("dT setpoint"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(6);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column B"));

    result.emplace_back(SequenceName({}, "raw_meta", "DTgs"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("GroupUnits").setString("d\xC2\xB0\x43");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Standard deviation of T3 - T1");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("dT stddev"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(7);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column B"));

    /* NOTE: Volume flows so no report T/P */
    result.emplace_back(SequenceName({}, "raw_meta", "Qs1"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Sample flow");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(13);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column B"));

    result.emplace_back(SequenceName({}, "raw_meta", "Qs2"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Sheath flow");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sheath"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(14);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column B"));

    result.emplace_back(SequenceName({}, "raw_meta", "Us"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0.000");
    result.back().write().metadataReal("Units").setString("%");
    result.back().write().metadataReal("Description").setString("Supersaturation setting");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("SS Setting"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column B"));


    result.emplace_back(SequenceName({}, "raw_meta", "Ps"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.0");
    result.back().write().metadataReal("Units").setString("hPa");
    result.back().write().metadataReal("Description").setString("Sample pressure");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(12);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column B"));

    result.emplace_back(SequenceName({}, "raw_meta", "Vs1"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("00.00");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("First stage monitor");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("1st stage monitor"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(16);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column B"));

    result.emplace_back(SequenceName({}, "raw_meta", "Vs2"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("000.00");
    result.back().write().metadataReal("Units").setString("V");
    result.back().write().metadataReal("Description").setString("Proportional valve");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Proportional valve"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(17);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column B"));

    result.emplace_back(SequenceName({}, "raw_meta", "As"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0000.00");
    result.back().write().metadataReal("Units").setString("mA");
    result.back().write().metadataReal("Description").setString("Laser current");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Laser current"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(15);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column B"));

    result.emplace_back(SequenceName({}, "raw_meta", "Ns"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0");
    result.back().write().metadataReal("Units").setString("cm\xE2\x81\xBB³");
    result.back().write().metadataReal("Description").setString("Total concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Concentration"));
    result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(0);
    result.back().write().metadataReal("Realtime").hash("Page").setInteger(1);
    result.back().write().metadataReal("Realtime").hash("Box").setString(QObject::tr("Column B"));

    result.emplace_back(SequenceName({}, "raw_meta", "Nps"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0");
    result.back().write().metadataReal("Units").setString("cm\xE2\x81\xBB³");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Total concentration without unstable data");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);

    result.emplace_back(SequenceName({}, "raw_meta", "ZsBin"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataInteger("Format").setString("0");
    result.back()
          .write()
          .metadataInteger("Description")
          .setString("Instrument reported minimum bin");
    result.back().write().metadataInteger("Source").set(instrumentMeta);
    result.back().write().metadataInteger("Processing").toArray().after_back().set(processing);
    result.back().write().metadataInteger("Smoothing").hash("Mode").setString("None");

    result.emplace_back(SequenceName({}, "raw_meta", "Nbs"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataArray("Count").setInt64(totalBins);
    result.back().write().metadataArray("Children").metadataReal("Format").setString("00000.0");
    result.back()
          .write()
          .metadataArray("Children")
          .metadataReal("Units")
          .setString("cm\xE2\x81\xBB³");
    result.back().write().metadataArray("Description").setString("Bin concentration");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Realtime").hash("Page").setInteger(2);
    result.back()
          .write()
          .metadataArray("Realtime")
          .hash("Name")
          .setString(QObject::tr("Concentration B"));
    result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInt64(1);


    if (config.front().enableModel) {
        result.emplace_back(SequenceName({}, "raw_meta", "Uc"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("00.000");
        result.back().write().metadataReal("Units").setString("%");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Super saturation calculated from the model");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("ModelParameters")
              .setString(config.front().modelParameters);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Name")
              .setString(QObject::tr("SS Calculated"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(2);
        result.back().write().metadataReal("Realtime").hash("Page").setInteger(0);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Box")
              .setString(QObject::tr("Column A"));

        result.emplace_back(SequenceName({}, "raw_meta", "Ucs"), Variant::Root(), time,
                            FP::undefined());
        result.back().write().metadataReal("Format").setString("00.000");
        result.back().write().metadataReal("Units").setString("%");
        result.back()
              .write()
              .metadataReal("Description")
              .setString("Super saturation calculated from the model");
        result.back().write().metadataReal("Source").set(instrumentMeta);
        result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
        result.back()
              .write()
              .metadataReal("ModelParameters")
              .setString(config.front().modelParameters);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Name")
              .setString(QObject::tr("SS Calculated"));
        result.back().write().metadataReal("Realtime").hash("RowOrder").setInteger(2);
        result.back().write().metadataReal("Realtime").hash("Page").setInteger(1);
        result.back()
              .write()
              .metadataReal("Realtime")
              .hash("Box")
              .setString(QObject::tr("Column B"));

#ifdef RECORD_MODEL_INPUTS
        result.emplace_back(SequenceName({}, "raw_meta", "ZMODEL"), Variant::Root(),time, FP::undefined());
        result.back().write().metadataHash("Description").
            setString("Super saturation model inputs");
        result.back().write().metadataHash("Source").set(instrumentMeta);
        result.back().write().metadataHash("Processing").
            arrayAppend(processing);
        result.back().write().metadataHashChild("ZQshaer").
            metadataDouble("Format").setString("0.000E0");
        result.back().write().metadataHashChild("ZQshaer").
            metadataDouble("Description").
            setString("Ratio of sheath to sample flow");
        result.back().write().metadataHashChild("Q").
            metadataDouble("Format").setString("0.000E0");
        result.back().write().metadataHashChild("Q").
            metadataDouble("Units").setString("m³/s");
        result.back().write().metadataHashChild("Q").
            metadataDouble("Description").setString("Total flow");
        result.back().write().metadataHashChild("P").
            metadataDouble("Format").setString("0.000E0");
        result.back().write().metadataHashChild("P").
            metadataDouble("Units").setString("Pa");
        result.back().write().metadataHashChild("P").
            metadataDouble("Description").setString("Pressure");
        result.back().write().metadataHashChild("Tu").
            metadataDouble("Format").setString("0.000E0");
        result.back().write().metadataHashChild("Tu").
            metadataDouble("Units").setString("K");
        result.back().write().metadataHashChild("Tu").
            metadataDouble("Description").setString("Inlet temperature");
        result.back().write().metadataHashChild("T1").
            metadataDouble("Format").setString("0.000E0");
        result.back().write().metadataHashChild("T1").
            metadataDouble("Units").setString("K");
        result.back().write().metadataHashChild("T1").
            metadataDouble("Description").setString("Cold temperature");
        result.back().write().metadataHashChild("T3").
            metadataDouble("Format").setString("0.000E0");
        result.back().write().metadataHashChild("T3").
            metadataDouble("Units").setString("K");
        result.back().write().metadataHashChild("T3").
            metadataDouble("Description").setString("Hot temperature");

        result.emplace_back(SequenceName({}, "raw_meta", "ZsMODEL"), Variant::Root(),time, FP::undefined());
        result.back().write().metadataHash("Description").
            setString("Super saturation model inputs");
        result.back().write().metadataHash("Source").set(instrumentMeta);
        result.back().write().metadataHash("Processing").
            arrayAppend(processing);
        result.back().write().metadataHashChild("ZQshaer").
            metadataDouble("Format").setString("0.000E0");
        result.back().write().metadataHashChild("ZQshaer").
            metadataDouble("Description").
            setString("Ratio of sheath to sample flow");
        result.back().write().metadataHashChild("Q").
            metadataDouble("Format").setString("0.000E0");
        result.back().write().metadataHashChild("Q").
            metadataDouble("Units").setString("m³/s");
        result.back().write().metadataHashChild("Q").
            metadataDouble("Description").setString("Total flow");
        result.back().write().metadataHashChild("P").
            metadataDouble("Format").setString("0.000E0");
        result.back().write().metadataHashChild("P").
            metadataDouble("Units").setString("Pa");
        result.back().write().metadataHashChild("P").
            metadataDouble("Description").setString("Pressure");
        result.back().write().metadataHashChild("Tu").
            metadataDouble("Format").setString("0.000E0");
        result.back().write().metadataHashChild("Tu").
            metadataDouble("Units").setString("K");
        result.back().write().metadataHashChild("Tu").
            metadataDouble("Description").setString("Inlet temperature");
        result.back().write().metadataHashChild("T1").
            metadataDouble("Format").setString("0.000E0");
        result.back().write().metadataHashChild("T1").
            metadataDouble("Units").setString("K");
        result.back().write().metadataHashChild("T1").
            metadataDouble("Description").setString("Cold temperature");
        result.back().write().metadataHashChild("T3").
            metadataDouble("Format").setString("0.000E0");
        result.back().write().metadataHashChild("T3").
            metadataDouble("Units").setString("K");
        result.back().write().metadataHashChild("T3").
            metadataDouble("Description").setString("Hot temperature");
#endif
    }


    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataSingleFlag("LaserOverCurrent")
          .hash("Description")
          .setString("Laser current too high");
    result.back().write().metadataSingleFlag("LaserOverCurrent").hash("Bits").setInt64(0x00010000);
    result.back()
          .write()
          .metadataSingleFlag("LaserOverCurrent")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_ccn");

    result.back()
          .write()
          .metadataSingleFlag("FirstStageMonitorOverVoltage")
          .hash("Description")
          .setString("First stage montor voltage greater than 4.7V");
    result.back()
          .write()
          .metadataSingleFlag("FirstStageMonitorOverVoltage")
          .hash("Bits")
          .setInt64(0x00020000);
    result.back()
          .write()
          .metadataSingleFlag("FirstStageMonitorOverVoltage")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_ccn");

    result.back()
          .write()
          .metadataSingleFlag("FlowOutOfRange")
          .hash("Description")
          .setString("Flow deviation greater than 20% or flow ratio outside of 5-15");
    result.back().write().metadataSingleFlag("FlowOutOfRange").hash("Bits").setInt64(0x00040000);
    result.back()
          .write()
          .metadataSingleFlag("FlowOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_ccn");

    result.back()
          .write()
          .metadataSingleFlag("TemperatureOutOfRange")
          .hash("Description")
          .setString("Any temperature setpoint more than 10C from the target");
    result.back()
          .write()
          .metadataSingleFlag("TemperatureOutOfRange")
          .hash("Bits")
          .setInt64(0x00080000);
    result.back()
          .write()
          .metadataSingleFlag("TemperatureOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_ccn");

    result.back()
          .write()
          .metadataSingleFlag("SampleTemperatureOutOfRange")
          .hash("Description")
          .setString("Sample temperature outside of 0-50C");
    result.back()
          .write()
          .metadataSingleFlag("SampleTemperatureOutOfRange")
          .hash("Bits")
          .setInt64(0x00100000);
    result.back()
          .write()
          .metadataSingleFlag("SampleTemperatureOutOfRange")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_ccn");

    result.back()
          .write()
          .metadataSingleFlag("OPCError")
          .hash("Description")
          .setString("First stage monitor greater than 4.7V and CCN counts too low");
    result.back().write().metadataSingleFlag("OPCError").hash("Bits").setInt64(0x00200000);
    result.back()
          .write()
          .metadataSingleFlag("OPCError")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_ccn");

    result.back()
          .write()
          .metadataSingleFlag("CCNCountsLow")
          .hash("Description")
          .setString("CCN counts too low");
    result.back().write().metadataSingleFlag("CCNCountsLow").hash("Bits").setInt64(0x00400000);
    result.back()
          .write()
          .metadataSingleFlag("CCNCountsLow")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_ccn");

    result.back()
          .write()
          .metadataSingleFlag("ColumnTemperaturesUnstableAlarm")
          .hash("Description")
          .setString("Column temperature unstable alarm active");
    result.back()
          .write()
          .metadataSingleFlag("ColumnTemperaturesUnstableAlarm")
          .hash("Bits")
          .setInt64(0x00800000);
    result.back()
          .write()
          .metadataSingleFlag("ColumnTemperaturesUnstableAlarm")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_ccn");

    result.back()
          .write()
          .metadataSingleFlag("NoOPCCommunications")
          .hash("Description")
          .setString("OPC not communicating with CCN control computer");
    result.back()
          .write()
          .metadataSingleFlag("NoOPCCommunications")
          .hash("Bits")
          .setInt64(0x01000000);
    result.back()
          .write()
          .metadataSingleFlag("NoOPCCommunications")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_ccn");

    result.back()
          .write()
          .metadataSingleFlag("DuplicatedFile")
          .hash("Description")
          .setString("A duplicate file was detected on the CCN computer");
    result.back().write().metadataSingleFlag("DuplicatedFile").hash("Bits").setInt64(0x02000000);
    result.back()
          .write()
          .metadataSingleFlag("DuplicatedFile")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_ccn");

    result.back()
          .write()
          .metadataSingleFlag("ReportedTemperatureInstability")
          .hash("Description")
          .setString(
                  "The temperature difference setpoint is unstable as calculated by the instrument");
    result.back()
          .write()
          .metadataSingleFlag("ReportedTemperatureInstability")
          .hash("Bits")
          .setInt64(0x10000000);
    result.back()
          .write()
          .metadataSingleFlag("ReportedTemperatureInstability")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_ccn");

    result.back()
          .write()
          .metadataSingleFlag("CalculatedTemperatureInstability")
          .hash("Description")
          .setString(
                  "The temperature difference setpoint is unstable as calculated by the acquisition system");
    result.back()
          .write()
          .metadataSingleFlag("CalculatedTemperatureInstability")
          .hash("Bits")
          .setInt64(0x20000000);
    result.back()
          .write()
          .metadataSingleFlag("CalculatedTemperatureInstability")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_ccn");

    result.back()
          .write()
          .metadataSingleFlag("SafeModeActive")
          .hash("Description")
          .setString("The CCN control program has entered safe mode due to a serious alarm");
    result.back().write().metadataSingleFlag("SafeModeActive").hash("Bits").setInt64(0x80000000);
    result.back()
          .write()
          .metadataSingleFlag("SafeModeActive")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_dmt_ccn");

    return result;
}

SequenceValue::Transfer AcquireDMTCCN200::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_dmt_ccn");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());

    result.emplace_back(SequenceName({}, "raw_meta", "ZNc"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataArray("Count").setInt64(totalBins);
    result.back().write().metadataArray("Children").metadataReal("Format").setString("00000");
    result.back().write().metadataArray("Children").metadataReal("Units").setString("Hz");
    result.back().write().metadataArray("Description").setString("Bin counts");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Realtime").hash("Page").setInt64(2);
    result.back().write().metadataArray("Realtime").hash("Name").setString(QObject::tr("Counts A"));
    result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInt64(2);

    result.emplace_back(SequenceName({}, "raw_meta", "ZNcs"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataArray("Count").setInt64(totalBins);
    result.back().write().metadataArray("Children").metadataReal("Format").setString("00000");
    result.back().write().metadataArray("Children").metadataReal("Units").setString("Hz");
    result.back().write().metadataArray("Description").setString("Bin counts");
    result.back().write().metadataArray("Source").set(instrumentMeta);
    result.back().write().metadataArray("Processing").toArray().after_back().set(processing);
    result.back().write().metadataArray("Realtime").hash("Page").setInt64(2);
    result.back().write().metadataArray("Realtime").hash("Name").setString(QObject::tr("Counts B"));
    result.back().write().metadataArray("Realtime").hash("NetworkPriority").setInt64(3);

    return result;
}

SequenceMatch::Composite AcquireDMTCCN200::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "ZSTATE");
    return sel;
}

void AcquireDMTCCN200::logValue(double startTime,
                                double endTime,
                                SequenceName::Component name,
                                Variant::Root &&value,
                                LogStreamID streamID)
{
    if (!loggingEgress && !realtimeEgress)
        return;
    SequenceValue dv({{}, "raw", std::move(name)}, std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        loggingMux.incoming(static_cast<std::size_t>(streamID), std::move(dv), loggingEgress);
        return;
    }
    loggingMux.incoming(static_cast<std::size_t>(streamID), dv, loggingEgress);
    if (FP::defined(endTime)) {
        dv.setStart(endTime);
        dv.setEnd(endTime + 1.0);
    } else if (FP::defined(startTime)) {
        dv.setEnd(startTime + 1.0);
    }
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireDMTCCN200::realtimeValue(double time,
                                     SequenceName::Component name,
                                     Variant::Root &&value)
{
    if (!realtimeEgress)
        return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

void AcquireDMTCCN200::configAdvance(double frameTime)
{
    Q_ASSERT(!config.empty());
    auto oldSize = config.size();
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.empty());

    if (oldSize == config.size())
        return;
    configurationChanged(frameTime);
}

void AcquireDMTCCN200::emptyModelProcessing(double frameTime)
{
    if (!model)
        return;
    model->finish();
    processModelPending(frameTime);
}

void AcquireDMTCCN200::configurationChanged(double frameTime)
{
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;

    if (model) {
        emptyModelProcessing(frameTime);
        model.reset();
    }
    if (!config.front().enableModel)
        return;

    model.reset(new ModelRunner(config.front().modelProgram, config.front().modelParameters,
                                config.front().maximumModelBacklog));
}

static double calculateConcentration(double counts, double flow)
{
    if (!FP::defined(counts) || !FP::defined(flow))
        return FP::undefined();
    if (flow <= 0.0)
        return FP::undefined();
    return counts / (flow * (1000.0 / 60.0));
}

static double calculateDt(double T1, double T3)
{
    if (!FP::defined(T1) || !FP::defined(T3))
        return FP::undefined();
    return T3 - T1;
}

void AcquireDMTCCN200::processModelPending(double frameTime)
{
    Q_ASSERT(model.get() != nullptr);
    for (const auto &result : model->consume()) {
        Q_ASSERT(FP::defined(result.input.start));
        Q_ASSERT(FP::defined(result.input.end));

        if (!result.valid || result.input.start == result.input.end) {
            loggingMux.advance(static_cast<std::size_t>(LogStreamID::Model), result.input.end,
                               loggingEgress);
            continue;
        }

        SequenceValue dv
                ({{}, "raw", result.input.columnNumber ? "Ucs" : "Uc"}, Variant::Root(result.ss),
                 result.input.start, result.input.end);
        if (realtimeEgress) {
            loggingMux.incoming(static_cast<std::size_t>(LogStreamID::Model), dv, loggingEgress);
            if (FP::defined(frameTime)) {
                dv.setStart(frameTime);
                dv.setEnd(frameTime + 1.0);
            }
            realtimeEgress->incomingData(std::move(dv));
        } else {
            loggingMux.incoming(static_cast<std::size_t>(LogStreamID::Model), std::move(dv),
                                loggingEgress);
        }

#ifdef RECORD_MODEL_INPUTS
        Variant::Root parameters;
        parameters.write().hash("ZQshaer").setDouble(result.input.Qshaer);
        parameters.write().hash("Q").setDouble(result.input.Qtot);
        parameters.write().hash("P").setDouble(result.input.P);
        parameters.write().hash("Tu").setDouble(result.input.Tinlet);
        parameters.write().hash("T1").setDouble(result.input.Tcold);
        parameters.write().hash("T3").setDouble(result.input.Thot);
        loggingMux.incoming(static_cast<std::size_t>(LogStreamID::Model), SequenceValue(
            {{}, "raw_meta", result.input.columnNumber ? "ZMODEL" : "ZsMODEL"}, std::move(parameters),
            result.input.start, result.input.end), loggingEgress);
#endif

        loggingMux.advance(static_cast<std::size_t>(LogStreamID::Model), result.input.start,
                           loggingEgress);
    }
}

void AcquireDMTCCN200::invalidateLogValues(double frameTime)
{
    emptyModelProcessing(frameTime);
    for (std::size_t i = 0; i < streamAge.size(); i++) {
        streamTime[i] = FP::undefined();
        streamAge[i] = 0;

        if (loggingEgress)
            loggingMux.advance(i, frameTime, loggingEgress);
    }
    loggingMux.clear();
    loggingLost(frameTime);
}

void AcquireDMTCCN200::emitMetadata(double frameTime)
{
    if (!FP::defined(frameTime))
        return;

    if (!haveEmittedLogMeta && loggingEgress) {
        haveEmittedLogMeta = true;
        loggingMux.incoming(static_cast<std::size_t>(LogStreamID::Metadata),
                            buildLogMeta(frameTime), loggingEgress);
    }

    if (!haveEmittedRealtimeMeta && realtimeEgress) {
        haveEmittedRealtimeMeta = true;

        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }
}

double AcquireDMTCCN200::remapModelInput(const SequenceName::Component &name,
                                         const Variant::Read &input)
{
    Variant::Root v(input);
    remap("ZMODEL" + name, v);
    return v.read().toDouble();
}

void AcquireDMTCCN200::advanceStream(LogStreamID streamID, double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));

    if (!loggingEgress) {
        streamAge.fill(0);
        streamTime.fill(FP::undefined());
        loggingMux.clear();
    }

    emitMetadata(frameTime);
    loggingMux.advance(static_cast<std::size_t>(LogStreamID::Metadata), frameTime, loggingEgress);

    std::uint_fast32_t streamBits =
            static_cast<std::uint_fast32_t>(1) << static_cast<std::uint_fast32_t>(streamID);
    for (std::size_t i = static_cast<std::size_t>(LogStreamID::RecordBegin);
            i < static_cast<std::size_t>(LogStreamID::TOTAL);
            i++) {
        if (i == static_cast<std::size_t>(streamID))
            continue;
        if (!(streamAge[i] & streamBits)) {
            streamAge[i] |= streamBits;
            continue;
        }

        streamTime[i] = FP::undefined();
        streamAge[i] = 0;
        loggingMux.advance(i, frameTime, loggingEgress);
    }

    if (streamAge[static_cast<std::size_t>(LogStreamID::State)] & streamBits) {
        double startTime = streamTime[static_cast<std::size_t>(LogStreamID::State)];
        double endTime = frameTime;
        if (FP::defined(startTime) && FP::defined(endTime)) {
            logValue(startTime, endTime, "F1", Variant::Root(instrumentAlarmFlags),
                     LogStreamID::State);
        } else {
            loggingMux.advance(static_cast<std::size_t>(LogStreamID::State), endTime,
                               loggingEgress);
        }
        streamTime[static_cast<std::size_t>(LogStreamID::State)] = endTime;
        streamAge[static_cast<std::size_t>(LogStreamID::State)] = 0;

        instrumentAlarmFlags.clear();
    } else {
        streamAge[static_cast<std::size_t>(LogStreamID::State)] |= streamBits;
    }

    streamAge[static_cast<std::size_t>(streamID)] = 0;
    streamTime[static_cast<std::size_t>(streamID)] = frameTime;

    if (model && config.front().enableModel) {
        if (modelDataA.empty()) {
            ModelPoint add;
            add.start = frameTime;
            add.end = frameTime;
            add.Q1 = FP::undefined();
            add.Q2 = FP::undefined();
            add.P = FP::undefined();
            add.T1 = FP::undefined();
            add.T3 = FP::undefined();
            add.Tu = FP::undefined();
            modelDataA.emplace_back(add);
        }
        double intervalEnd = modelDataA.front().start;
        intervalEnd = modelInterval->apply(frameTime, intervalEnd, true);
        if (!FP::defined(intervalEnd) || modelDataA.back().end >= intervalEnd) {
            if (!FP::defined(intervalEnd))
                intervalEnd = frameTime;
            ModelInput input(modelDataA, intervalEnd);
            input.columnNumber = 0;
            model->submit(std::move(input));
        }

        if (modelDataB.empty()) {
            ModelPoint add;
            add.start = frameTime;
            add.end = frameTime;
            add.Q1 = FP::undefined();
            add.Q2 = FP::undefined();
            add.P = FP::undefined();
            add.T1 = FP::undefined();
            add.T3 = FP::undefined();
            add.Tu = FP::undefined();
            modelDataB.emplace_back(add);
        }
        intervalEnd = modelDataB.front().start;
        intervalEnd = modelInterval->apply(frameTime, intervalEnd, true);
        if (!FP::defined(intervalEnd) || modelDataB.back().end >= intervalEnd) {
            if (!FP::defined(intervalEnd))
                intervalEnd = frameTime;
            ModelInput input(modelDataB, intervalEnd);
            input.columnNumber = 1;
            model->submit(std::move(input));
        }
    } else {
        modelDataA.clear();
        modelDataB.clear();
        loggingMux.advance(static_cast<std::size_t>(LogStreamID::Model), frameTime, loggingEgress);
    }


    if (processingDataA.empty()) {
        ProcessingPoint add;
        add.start = frameTime;
        add.end = frameTime;
        add.DT = FP::undefined();
        processingDataA.emplace_back(add);
    }

    double intervalEnd = processingDataA.front().start;
    intervalEnd = processingInterval->apply(frameTime, intervalEnd, true);
    if (!FP::defined(intervalEnd) || processingDataA.back().end >= intervalEnd) {
        if (!FP::defined(intervalEnd))
            intervalEnd = frameTime;
        double startTime = processingDataA.front().start;

        std::vector<double> dtData;
        for (const auto &p : processingDataA) {
            if (!FP::defined(p.DT))
                continue;
            dtData.emplace_back(p.DT);
        }

        double endTime;
        if (processingDataA.back().end <= intervalEnd ||
                intervalEnd <= processingDataA.front().start) {
            endTime = processingDataA.back().end;
            processingDataA.clear();
        } else {
            endTime = intervalEnd;
            processingDataA.erase(processingDataA.begin(), processingDataA.end() - 1);
            processingDataA.back().start = intervalEnd;
        }

        logValue(startTime, endTime, "DTg", Variant::Root(Statistics::sd(dtData)),
                 LogStreamID::Processing);
        logValue(startTime, endTime, "ZBin", Variant::Root(lastBinNumberA),
                 LogStreamID::Processing);
    }


    if (processingDataB.empty()) {
        ProcessingPoint add;
        add.start = frameTime;
        add.end = frameTime;
        add.DT = FP::undefined();
        processingDataB.emplace_back(add);
    }

    intervalEnd = processingDataB.front().start;
    intervalEnd = processingInterval->apply(frameTime, intervalEnd, true);
    if (!FP::defined(intervalEnd) || processingDataB.back().end >= intervalEnd) {
        if (!FP::defined(intervalEnd))
            intervalEnd = frameTime;
        double startTime = processingDataB.front().start;

        std::vector<double> dtData;
        for (const auto &p : processingDataB) {
            if (!FP::defined(p.DT))
                continue;
            dtData.emplace_back(p.DT);
        }

        double endTime;
        if (processingDataB.back().end <= intervalEnd ||
                intervalEnd <= processingDataB.front().start) {
            endTime = processingDataB.back().end;
            processingDataB.clear();
        } else {
            endTime = intervalEnd;
            processingDataB.erase(processingDataB.begin(), processingDataB.end() - 1);
            processingDataB.back().start = intervalEnd;
        }

        logValue(startTime, endTime, "DTgs", Variant::Root(Statistics::sd(dtData)),
                 LogStreamID::Processing);
        logValue(startTime, endTime, "ZsBin", Variant::Root(lastBinNumberB),
                 LogStreamID::Processing);
    }
}

int AcquireDMTCCN200::processH(std::deque<Util::ByteView> &fields, double &frameTime)
{
    bool ok = false;

    if (fields.empty()) return 1000;
    auto field = fields.front().string_trimmed();
    fields.pop_front();
    auto subfields = Util::as_deque(field.split(':'));

    if (subfields.empty()) return 1001;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    qint64 ihour = field.parse_i64(&ok);
    if (!ok) return 1002;
    if (ihour < 0) return 1003;
    if (ihour > 23) return 1004;
    Variant::Root hour(ihour);
    remap("HOUR", hour);
    ihour = hour.read().toInt64();

    if (subfields.empty()) return 1005;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    qint64 iminute = field.parse_i64(&ok);
    if (!ok) return 1006;
    if (iminute < 0) return 1007;
    if (iminute > 59) return 1008;
    Variant::Root minute(iminute);
    remap("MINUTE", minute);
    iminute = minute.read().toInt64();

    if (subfields.empty()) return 1009;
    field = subfields.front().string_trimmed();
    subfields.pop_front();
    qint64 isecond = field.parse_i64(&ok);
    if (!ok) return 1010;
    if (isecond < 0) return 1011;
    if (isecond > 60) return 1012;
    Variant::Root second(isecond);
    remap("SECOND", second);
    isecond = second.read().toInt64();

    if (!FP::defined(frameTime) &&
            FP::defined(config.front().timeOrigin) &&
            INTEGER::defined(ihour) &&
            ihour >= 0 &&
            ihour <= 23 &&
            INTEGER::defined(iminute) &&
            iminute >= 0 &&
            iminute <= 59 &&
            INTEGER::defined(isecond) &&
            isecond >= 0 &&
            isecond <= 60) {
        frameTime = config.front().timeOrigin +
                static_cast<double>(ihour * 3600) +
                static_cast<double>(iminute * 60) +
                static_cast<double>(isecond);
        configAdvance(frameTime);
    }
    lastHRecordTime = frameTime;

    struct ColumnData {
        Variant::Root U;
        Variant::Root tempStable;
        Variant::Root DT;
        Variant::Root T1;
        Variant::Root T2;
        Variant::Root T3;
        Variant::Root T6;
        Variant::Root Tu;
        Variant::Root T5;
        Variant::Root T4;
        Variant::Root Q1;
        Variant::Root Q2;
        Variant::Root P;
        Variant::Root A;
        Variant::Root V1;
        Variant::Root V2;

        int parse(std::deque<Util::ByteView> &fields)
        {
            bool ok = false;

            if (fields.empty()) return 1;
            auto field = fields.front().string_trimmed();
            fields.pop_front();
            U.write().setReal(field.parse_real(&ok));
            if (!ok) return 2;
            if (!FP::defined(U.read().toReal())) return 3;

            if (fields.empty()) return 4;
            field = fields.front().string_trimmed();
            fields.pop_front();
            tempStable.write().setBool(field.parse_real(&ok) != 0.0);
            if (!ok) return 5;

            if (fields.empty()) return 6;
            field = fields.front().string_trimmed();
            fields.pop_front();
            DT.write().setReal(field.parse_real(&ok));
            if (!ok) return 7;
            if (!FP::defined(DT.read().toReal())) return 8;

            if (fields.empty()) return 9;
            field = fields.front().string_trimmed();
            fields.pop_front();
            T1.write().setReal(field.parse_real(&ok));
            if (!ok) return 10;
            if (!FP::defined(T1.read().toReal())) return 11;

            if (fields.empty()) return 12;
            field = fields.front().string_trimmed();
            fields.pop_front();
            T2.write().setReal(field.parse_real(&ok));
            if (!ok) return 13;
            if (!FP::defined(T2.read().toReal())) return 14;

            if (fields.empty()) return 15;
            field = fields.front().string_trimmed();
            fields.pop_front();
            T3.write().setReal(field.parse_real(&ok));
            if (!ok) return 16;
            if (!FP::defined(T3.read().toReal())) return 17;

            if (fields.empty()) return 18;
            field = fields.front().string_trimmed();
            fields.pop_front();
            T6.write().setReal(field.parse_real(&ok));
            if (!ok) return 19;
            if (!FP::defined(T6.read().toReal())) return 20;

            if (fields.empty()) return 21;
            field = fields.front().string_trimmed();
            fields.pop_front();
            Tu.write().setReal(field.parse_real(&ok));
            if (!ok) return 22;
            if (!FP::defined(Tu.read().toReal())) return 23;

            if (fields.empty()) return 24;
            field = fields.front().string_trimmed();
            fields.pop_front();
            T5.write().setReal(field.parse_real(&ok));
            if (!ok) return 25;
            if (!FP::defined(T5.read().toReal())) return 26;

            if (fields.empty()) return 27;
            field = fields.front().string_trimmed();
            fields.pop_front();
            T4.write().setReal(field.parse_real(&ok));
            if (!ok) return 28;
            if (!FP::defined(T4.read().toReal())) return 29;

            if (fields.empty()) return 30;
            field = fields.front().string_trimmed();
            fields.pop_front();
            {
                double v = field.parse_real(&ok);
                if (!ok) return 31;
                if (!FP::defined(v)) return 32;
                Q1.write().setDouble(v * 0.001);
            }

            if (fields.empty()) return 33;
            field = fields.front().string_trimmed();
            fields.pop_front();
            {
                double v = field.parse_real(&ok);
                if (!ok) return 34;
                if (!FP::defined(v)) return 35;
                Q2.write().setDouble(v * 0.001);
            }

            if (fields.empty()) return 36;
            field = fields.front().string_trimmed();
            fields.pop_front();
            P.write().setReal(field.parse_real(&ok));
            if (!ok) return 37;
            if (!FP::defined(P.read().toReal())) return 38;

            if (fields.empty()) return 39;
            field = fields.front().string_trimmed();
            fields.pop_front();
            A.write().setReal(field.parse_real(&ok));
            if (!ok) return 40;
            if (!FP::defined(A.read().toReal())) return 41;

            if (fields.empty()) return 42;
            field = fields.front().string_trimmed();
            fields.pop_front();
            V1.write().setReal(field.parse_real(&ok));
            if (!ok) return 43;
            if (!FP::defined(V1.read().toReal())) return 44;

            if (fields.empty()) return 45;
            field = fields.front().string_trimmed();
            fields.pop_front();
            V2.write().setReal(field.parse_real(&ok));
            if (!ok) return 46;
            if (!FP::defined(V2.read().toReal())) return 47;

            return 0;
        }
    };

    ColumnData columnA;
    {
        int rc = columnA.parse(fields);
        if (rc < 0) return rc;
        else if (rc > 0) return 1100 + rc;
    }
    remap("U", columnA.U);
    remap("ZSTABLE", columnA.tempStable);
    remap("DT", columnA.DT);
    remap("T1", columnA.T1);
    remap("T2", columnA.T2);
    remap("T3", columnA.T3);
    remap("T4", columnA.T4);
    remap("T5", columnA.T5);
    remap("T6", columnA.T6);
    remap("Tu", columnA.Tu);
    remap("Q1", columnA.Q1);
    remap("Q2", columnA.Q2);
    remap("P", columnA.P);
    remap("A", columnA.A);
    remap("V1", columnA.V1);
    remap("V2", columnA.V2);

    ColumnData columnB;
    {
        int rc = columnB.parse(fields);
        if (rc < 0) return rc;
        else if (rc > 0) return 1200 + rc;
    }
    remap("Us", columnB.U);
    remap("ZsSTABLE", columnB.tempStable);
    remap("DTs", columnB.DT);
    remap("Ts1", columnB.T1);
    remap("Ts2", columnB.T2);
    remap("Ts3", columnB.T3);
    remap("Ts4", columnB.T4);
    remap("Ts5", columnB.T5);
    remap("Ts6", columnB.T6);
    remap("Tus", columnB.Tu);
    remap("Qs1", columnB.Q1);
    remap("Qs2", columnB.Q2);
    remap("Ps", columnB.P);
    remap("As", columnB.A);
    remap("Vs1", columnB.V1);
    remap("Vs2", columnB.V2);

    Variant::Root alarmCode(0);
    if (!fields.empty()) {
        field = fields.front().string_trimmed();
        alarmCode.write().setInteger(field.parse_i64(&ok));
        if (!ok)
            alarmCode.write().setInteger(static_cast<std::int_fast64_t>(field.parse_real(&ok)));
        if (!ok) {
            alarmCode.write().setInteger(0);
        } else {
            fields.pop_front();
        }
    }
    remap("ZALARM", alarmCode);

    if (!fields.empty()) {
        if (fields.front().string_trimmed() == "C") {
            fields.pop_front();
            int code = processC(fields, frameTime);
            if (code != 0)
                return code;
        }
    }

    if (config.front().strictMode) {
        if (!fields.empty())
            return 1999;
    }

    emitMetadata(frameTime);

    if (INTEGER::defined(alarmCode.read().toInt64())) {
        auto flagsBits = alarmCode.read().toInteger();
        if (flagsBits < 0) {
            flagsBits = -flagsBits;
            instrumentAlarmFlags.insert("SafeModeActive");
        }
        for (std::size_t i = 0; i < instrumentFlagTranslation.size(); i++) {
            std::int_fast64_t bits = static_cast<std::int_fast64_t>(1) << i;
            if (!(flagsBits & bits))
                continue;
            const auto &flag = instrumentFlagTranslation[i];
            if (flag.empty()) {
                qCDebug(log) << "Unrecognized bit" << hex << ((1 << i)) << "set in alarm code";
                continue;;
            }

            instrumentAlarmFlags.insert(flag);
        }
    }

    reportedStableA = columnA.tempStable.read().toBool();
    reportedStableB = columnB.tempStable.read().toBool();
    if (!reportedStableA || !reportedStableB) {
        instrumentAlarmFlags.insert("ReportedTemperatureInstability");
    }

    lastSampleFlowA = columnA.Q1.read().toDouble();
    lastSampleFlowB = columnB.Q1.read().toDouble();

    double startTime = streamTime[static_cast<std::size_t>(LogStreamID::H)];
    double endTime = frameTime;
    if (!FP::defined(startTime) || !FP::defined(endTime)) {
        streamTime[static_cast<std::size_t>(LogStreamID::H)] = endTime;
        return 0;
    }

    if (config.front().enableModel) {
        ModelPoint add;
        add.start = startTime;
        add.end = endTime;

        add.Q1 = remapModelInput("Q1", columnA.Q1);
        add.Q2 = remapModelInput("Q2", columnA.Q2);
        add.P = remapModelInput("P", columnA.P);
        add.T1 = remapModelInput("T1", columnA.T1);
        add.T3 = remapModelInput("T3", columnA.T3);
        add.Tu = remapModelInput("Tu", columnA.Tu);
        modelDataA.emplace_back(add);

        add.Q1 = remapModelInput("Qs1", columnB.Q1);
        add.Q2 = remapModelInput("Qs2", columnB.Q2);
        add.P = remapModelInput("Ps", columnB.P);
        add.T1 = remapModelInput("Ts1", columnB.T1);
        add.T3 = remapModelInput("Ts3", columnB.T3);
        add.Tu = remapModelInput("Tus", columnB.Tu);
        modelDataB.emplace_back(add);
    }

    double calculatedDt = calculateDt(columnA.T1.read().toDouble(), columnA.T3.read().toDouble());
    {
        ProcessingPoint add;
        add.start = startTime;
        add.end = endTime;
        add.DT = calculatedDt;
        processingDataA.emplace_back(add);
    }
    dtStabilityA->add(calculatedDt, startTime, endTime);
    if (!dtStabilityA->stable()) {
        instrumentAlarmFlags.insert("CalculatedTemperatureInstability");
    }

    calculatedDt = calculateDt(columnB.T1.read().toDouble(), columnB.T3.read().toDouble());
    {
        ProcessingPoint add;
        add.start = startTime;
        add.end = endTime;
        add.DT = calculatedDt;
        processingDataB.emplace_back(add);
    }
    dtStabilityB->add(calculatedDt, startTime, endTime);
    if (!dtStabilityB->stable()) {
        instrumentAlarmFlags.insert("CalculatedTemperatureInstability");
    }

    logValue(startTime, endTime, "U", std::move(columnA.U), LogStreamID::H);
    logValue(startTime, endTime, "T1", std::move(columnA.T1), LogStreamID::H);
    logValue(startTime, endTime, "T2", std::move(columnA.T2), LogStreamID::H);
    logValue(startTime, endTime, "T3", std::move(columnA.T3), LogStreamID::H);
    logValue(startTime, endTime, "T4", std::move(columnA.T4), LogStreamID::H);
    logValue(startTime, endTime, "T5", std::move(columnA.T5), LogStreamID::H);
    logValue(startTime, endTime, "T6", std::move(columnA.T6), LogStreamID::H);
    logValue(startTime, endTime, "Tu", std::move(columnA.Tu), LogStreamID::H);
    logValue(startTime, endTime, "Q1", std::move(columnA.Q1), LogStreamID::H);
    logValue(startTime, endTime, "Q2", std::move(columnA.Q2), LogStreamID::H);
    logValue(startTime, endTime, "P", std::move(columnA.P), LogStreamID::H);
    logValue(startTime, endTime, "A", std::move(columnA.A), LogStreamID::H);
    logValue(startTime, endTime, "V1", std::move(columnA.V1), LogStreamID::H);
    logValue(startTime, endTime, "V2", std::move(columnA.V2), LogStreamID::H);
    logValue(startTime, endTime, "DT", std::move(columnA.DT), LogStreamID::H);

    logValue(startTime, endTime, "Us", std::move(columnB.U), LogStreamID::H);
    logValue(startTime, endTime, "Ts1", std::move(columnB.T1), LogStreamID::H);
    logValue(startTime, endTime, "Ts2", std::move(columnB.T2), LogStreamID::H);
    logValue(startTime, endTime, "Ts3", std::move(columnB.T3), LogStreamID::H);
    logValue(startTime, endTime, "Ts4", std::move(columnB.T4), LogStreamID::H);
    logValue(startTime, endTime, "Ts5", std::move(columnB.T5), LogStreamID::H);
    logValue(startTime, endTime, "Ts6", std::move(columnB.T6), LogStreamID::H);
    logValue(startTime, endTime, "Tus", std::move(columnB.Tu), LogStreamID::H);
    logValue(startTime, endTime, "Qs1", std::move(columnB.Q1), LogStreamID::H);
    logValue(startTime, endTime, "Qs2", std::move(columnB.Q2), LogStreamID::H);
    logValue(startTime, endTime, "Ps", std::move(columnB.P), LogStreamID::H);
    logValue(startTime, endTime, "As", std::move(columnB.A), LogStreamID::H);
    logValue(startTime, endTime, "Vs1", std::move(columnB.V1), LogStreamID::H);
    logValue(startTime, endTime, "Vs2", std::move(columnB.V2), LogStreamID::H);
    logValue(startTime, endTime, "DTs", std::move(columnB.DT), LogStreamID::H);

    advanceStream(LogStreamID::H, frameTime);
    return 0;
}

int AcquireDMTCCN200::processC(std::deque<Util::ByteView> &fields, double &frameTime)
{
    struct ColumnData {
        Variant::Root ZBin;
        Variant::Root N;
        Variant::Root counts;

        int parse(std::deque<Util::ByteView> &fields)
        {
            bool ok = false;

            if (fields.empty()) return 1;
            auto field = fields.front().string_trimmed();
            fields.pop_front();
            {
                double v = field.parse_real(&ok);
                if (!ok) return 2;
                if (!FP::defined(v)) return 3;
                ZBin.write().setInteger(static_cast<std::int_fast64_t>(v));
            }

            if (fields.empty()) return 4;
            field = fields.front().string_trimmed();
            fields.pop_front();
            N.write().setReal(field.parse_real(&ok));
            if (!ok) return 5;
            if (!FP::defined(N.read().toReal())) return 6;

            if (fields.empty()) return 7;
            {
                field = fields.front().string_trimmed();
                fields.pop_front();
                Variant::Root overflow(field.parse_real(&ok));
                if (!ok) return 8;
                if (!FP::defined(overflow.read().toReal())) return 9;
                counts.write().array(totalBins - 1).set(overflow);
            }

            for (std::size_t i = 0; i < totalBins - 1; i++) {
                if (fields.empty()) return 10 + i * 3;
                field = fields.front().string_trimmed();
                fields.pop_front();
                Variant::Root bin(field.parse_real(&ok));
                if (!ok) return 11 + i * 3;
                if (!FP::defined(bin.read().toReal())) return 12 + i * 3;
                counts.write().array(i).set(bin);
            }

            return 0;
        }
    };

    ColumnData columnA;
    {
        int rc = columnA.parse(fields);
        if (rc < 0) return rc;
        else if (rc > 0) return 2100 + rc;
    }
    remap("ZBin", columnA.ZBin);
    remap("N", columnA.N);
    for (std::size_t i = 0; i < totalBins; i++) {
        Variant::Root rm(columnA.counts.read().array(i));
        remap("ZN" + std::to_string(i + 1), rm);
        columnA.counts.write().array(i).set(rm);
    }
    remap("ZN", columnA.counts);

    ColumnData columnB;
    {
        int rc = columnB.parse(fields);
        if (rc < 0) return rc;
        else if (rc > 0) return 2300 + rc;
    }
    remap("ZsBin", columnB.ZBin);
    remap("Ns", columnB.N);
    for (std::size_t i = 0; i < totalBins; i++) {
        Variant::Root rm(columnB.counts.read().array(i));
        remap("ZN" + std::to_string(i + 1), rm);
        columnB.counts.write().array(i).set(rm);
    }
    remap("ZNs", columnB.counts);

    if (!fields.empty()) {
        if (fields.front().string_trimmed() == "H") {
            fields.pop_front();
            int code = processH(fields, frameTime);
            if (code != 0)
                return code;
        }
    }

    if (config.front().strictMode) {
        if (!fields.empty())
            return 2999;
    }

    if (!FP::defined(frameTime))
        frameTime = lastHRecordTime;
    else
        lastHRecordTime = frameTime;

    emitMetadata(frameTime);

    Variant::Root Nb;
    for (std::size_t i = 0, max = columnA.counts.read().toArray().size(); i < max; i++) {
        Variant::Root conc(calculateConcentration(columnA.counts.read().array(i).toDouble(),
                                                  lastSampleFlowA));
        remap("Nb" + std::to_string(i + 1), conc);
        Nb.write().array(i).set(conc);
    }
    remap("Nb", Nb);

    Variant::Root Nbs;
    for (std::size_t i = 0, max = columnB.counts.read().toArray().size(); i < max; i++) {
        Variant::Root conc(calculateConcentration(columnB.counts.read().array(i).toDouble(),
                                                  lastSampleFlowB));
        remap("Nbs" + std::to_string(i + 1), conc);
        Nbs.write().array(i).set(conc);
    }
    remap("Nbs", Nbs);

    lastBinNumberA = columnA.ZBin.read().toInteger();
    lastBinNumberB = columnB.ZBin.read().toInteger();

    double startTime = streamTime[static_cast<std::size_t>(LogStreamID::C)];
    double endTime = frameTime;
    if (!FP::defined(startTime) || !FP::defined(endTime)) {
        streamTime[static_cast<std::size_t>(LogStreamID::C)] = endTime;
        return 0;
    }

    logValue(startTime, endTime, "Nb", std::move(Nb), LogStreamID::C);
    realtimeValue(endTime, "ZNc", std::move(columnA.counts));
    if ((config.front().useReportedStability && reportedStableA) ||
            (config.front().useCalculatedStability && dtStabilityA->stable())) {
        logValue(startTime, endTime, "Np", Variant::Root(columnA.N), LogStreamID::C);
    }
    logValue(startTime, endTime, "N", std::move(columnA.N), LogStreamID::C);

    logValue(startTime, endTime, "Nbs", std::move(Nbs), LogStreamID::C);
    realtimeValue(endTime, "ZNcs", std::move(columnB.counts));
    if ((config.front().useReportedStability && reportedStableB) ||
            (config.front().useCalculatedStability && dtStabilityB->stable())) {
        logValue(startTime, endTime, "Nps", Variant::Root(columnB.N), LogStreamID::C);
    }
    logValue(startTime, endTime, "Ns", std::move(columnB.N), LogStreamID::C);

    advanceStream(LogStreamID::C, frameTime);
    return 0;
}

int AcquireDMTCCN200::processRecord(const Util::ByteView &line, double &frameTime)
{
    Q_ASSERT(!config.empty());

    if (line.size() < 3)
        return 1;

    auto fields = Util::as_deque(line.split(','));
    if (fields.size() < 2)
        return 2;
    auto code = fields.front().string_trimmed();
    fields.pop_front();
    if (code.size() != 1)
        return 3;

    switch (code[0]) {
    case 'C':
        return processC(fields, frameTime);
    case 'H':
        return processH(fields, frameTime);
    default:
        return 4;
    }

    return -1;
}

void AcquireDMTCCN200::configurationAdvance(double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.empty());
}

void AcquireDMTCCN200::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    configAdvance(frameTime);

    switch (responseState) {
    case ResponseState::Autoprobe_Wait:
    case ResponseState::Autoprobe_Initialize: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Autoprobe succeeded at" << Logging::time(frameTime);

            responseState = ResponseState::Run;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
            }
            autoprobeStatusUpdated();
            generalStatusUpdated();

            timeoutAt(frameTime + config.front().reportInterval * 2.0);

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("AutoprobeWait");
            event(frameTime, QObject::tr("Autoprobe succeeded."), false, info);
        } else {
            qCDebug(log) << "Autoprobe failed at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            responseState = ResponseState::Wait;
            invalidateLogValues(frameTime);
        }
        break;
    }

    case ResponseState::Wait:
    case ResponseState::Initialize: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            qCDebug(log) << "Comms established at" << Logging::time(frameTime);

            responseState = ResponseState::Run;
            generalStatusUpdated();

            timeoutAt(frameTime + config.front().reportInterval * 2.0);

            Variant::Write info = Variant::Write::empty();
            info.hash("ResponseState").setString("Wait");
            event(frameTime, QObject::tr("Communications established."), false, info);
        } else if (code > 0) {
            invalidateLogValues(frameTime);
        }
        break;
    }

    case ResponseState::Run: {
        int code = processRecord(frame, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + config.front().reportInterval * 2.0);
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            responseState = ResponseState::Wait;
            discardData(frameTime + 0.5, 1);
            generalStatusUpdated();

            Variant::Write info = Variant::Write::empty();
            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            info.hash("ResponseState").setString("Run");
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            invalidateLogValues(frameTime);
        }
        break;
    }

    default:
        break;
    }

    if (!FP::defined(frameTime))
        frameTime = lastHRecordTime;
    else
        lastHRecordTime = frameTime;
    if (model)
        processModelPending(frameTime);
}

void AcquireDMTCCN200::incomingControlFrame(const Util::ByteArray &, double)
{ }

void AcquireDMTCCN200::incomingInstrumentTimeout(double frameTime)
{
    if (!FP::defined(frameTime))
        return;

    switch (responseState) {
    case ResponseState::Run: {
        qCDebug(log) << "Timeout in unpolled mode at" << Logging::time(frameTime);

        if (controlStream)
            controlStream->resetControl();
        responseState = ResponseState::Wait;
        discardData(frameTime + 2.0, 1);
        generalStatusUpdated();

        Variant::Write info = Variant::Write::empty();
        info.hash("ResponseState").setString("Run");
        event(frameTime, QObject::tr("Timeout waiting for report.  Communications dropped."), true,
              info);

        invalidateLogValues(frameTime);
        break;
    }

    case ResponseState::Autoprobe_Wait:
        qCDebug(log) << "Timeout waiting for autoprobe records at" << Logging::time(frameTime);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();

        if (controlStream)
            controlStream->resetControl();
        discardData(frameTime + 2.0, 1);
        responseState = ResponseState::Wait;

        invalidateLogValues(frameTime);
        break;

    case ResponseState::Initialize:
        discardData(frameTime + 0.5, 1);
        timeoutAt(frameTime + config.front().reportInterval * 3.0 + 1.0);
        responseState = ResponseState::Wait;
        break;

    case ResponseState::Autoprobe_Initialize:
        discardData(frameTime + 0.5, 1);
        timeoutAt(frameTime + config.front().reportInterval * 3.0 + 1.0);
        responseState = ResponseState::Autoprobe_Wait;
        break;

    default:
        discardData(frameTime + 0.5, 1);
        invalidateLogValues(frameTime);
        break;
    }
}

void AcquireDMTCCN200::discardDataCompleted(double)
{ }

void AcquireDMTCCN200::shutdownInProgress(double frameTime)
{
    emptyModelProcessing(frameTime);

    loggingMux.finish(static_cast<std::size_t>(LogStreamID::Model), loggingEgress);
    loggingMux.finish(static_cast<std::size_t>(LogStreamID::Processing), loggingEgress);
    processingDataA.clear();
    modelDataA.clear();
    processingDataB.clear();
    modelDataB.clear();
}


Variant::Root AcquireDMTCCN200::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireDMTCCN200::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case ResponseState::Run:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

AcquisitionInterface::AutoprobeStatus AcquireDMTCCN200::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireDMTCCN200::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case ResponseState::Run:
        /* Already running, just notify that we succeeded. */
        qCDebug(log) << "Interactive autoprobe requested with communications established at"
                     << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    default:
        qCDebug(log) << "Interactive autoprobe started from state"
                     << static_cast<int>(responseState) << "at" << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        timeoutAt(time + config.front().reportInterval * 3.0 + 1.0);
        discardData(time + 0.5, 1);
        responseState = ResponseState::Autoprobe_Wait;
        generalStatusUpdated();
        break;
    }
}

void AcquireDMTCCN200::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    configAdvance(time);

    qCDebug(log) << "Reset to passive autoprobe from state" << static_cast<int>(responseState)
                 << "at" << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    timeoutAt(time + config.front().reportInterval * 3.0 + 1.0);
    discardData(time + 0.5, 1);
    responseState = ResponseState::Autoprobe_Wait;
    generalStatusUpdated();
}


void AcquireDMTCCN200::autoprobePromote(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + config.front().reportInterval * 2.0);

    switch (responseState) {
    case ResponseState::Run:
    case ResponseState::Wait:
    case ResponseState::Initialize:
        qCDebug(log) << "Promoted from state" << static_cast<int>(responseState)
                     << "to interactive acquisition at" << Logging::time(time);
        break;

    case ResponseState::Autoprobe_Wait:
    case ResponseState::Autoprobe_Initialize:
        qCDebug(log) << "Promoted from passive autoprobe to interactive acquisition at"
                     << Logging::time(time);

        responseState = ResponseState::Wait;
        break;
    }
}

void AcquireDMTCCN200::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + config.front().reportInterval * 2.0);

    switch (responseState) {
    case ResponseState::Run:
    case ResponseState::Wait:
    case ResponseState::Initialize:
        qCDebug(log) << "Promoted from state" << static_cast<int>(responseState)
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case ResponseState::Autoprobe_Wait:
    case ResponseState::Autoprobe_Initialize:
        qCDebug(log) << "Promoted from passive autoprobe to passive acquisition at"
                     << Logging::time(time);

        responseState = ResponseState::Wait;
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireDMTCCN200::getDefaults()
{
    AutomaticDefaults result;
    result.name = "N$1$2";
    result.setSerialN81(57600);
    return result;
}


ComponentOptions AcquireDMTCCN200Component::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options, true);

    options.add("basetime",
                new ComponentOptionSingleTime(tr("basetime", "name"), tr("File time origin"),
                                              tr("When dealing with files without a full timestamp on each "
                                                 "record, this time is used to reconstruct the full time from "
                                                 "the time of day on the H records."), ""));

    return options;
}

ComponentOptions AcquireDMTCCN200Component::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    ComponentOptionSingleInteger *i = new ComponentOptionSingleInteger(tr("model-backlog", "name"),
                                                                       tr("Maximum model backlog"),
                                                                       tr("This is the maximum number of model processing requests allowed "
                                                                          "before they start being dropped."),
                                                                       tr("None",
                                                                          "default backlog"));
    i->setMinimum(1);
    i->setAllowUndefined(true);
    options.add("model-backlog", i);

    return options;
}

QList<ComponentExample> AcquireDMTCCN200Component::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples());
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireDMTCCN200Component::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireDMTCCN200Component::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options, true);
}

QList<ComponentExample> AcquireDMTCCN200Component::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireDMTCCN200Component::createAcquisitionPassive(const ComponentOptions &options,
                                                                                  const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireDMTCCN200(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireDMTCCN200Component::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireDMTCCN200(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireDMTCCN200Component::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                    const std::string &loggingContext)
{
    std::unique_ptr<AcquireDMTCCN200> i(new AcquireDMTCCN200(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<
        AcquisitionInterface> AcquireDMTCCN200Component::createAcquisitionInteractive(const ValueSegment::Transfer &config,
                                                                                      const std::string &loggingContext)
{
    std::unique_ptr<AcquireDMTCCN200> i(new AcquireDMTCCN200(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
