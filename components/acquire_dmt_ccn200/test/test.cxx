/*
 * Copyright (c) 2021 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <array>
#include <cstdint>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

static constexpr bool enableModel = true;
static constexpr std::size_t binCount = 21;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    double unpolledRemaining;

    double timeOfDay;

    struct Column {
        double ssSP;
        double dtSP;
        bool tempStable;
        double T1;
        double T2;
        double T3;
        double Tsample;
        double Tinlet;
        double TOPC;
        double Tnafion;
        double Qsample;
        double Qsheath;
        double P;
        double Alaser;
        double V1stStage;
        double VpropValve;

        int minimumBin;
        std::array<double, binCount> counts;
    };

    Column a;
    Column b;

    std::uint_fast16_t alarmBits;
    bool multiLine;

    ModelInstrument() : incoming(), outgoing(), unpolledRemaining(0), timeOfDay(0.0)
    {
        a.ssSP = 1.0;
        a.dtSP = 2.0;
        a.tempStable = true;
        a.T1 = 21.0;
        a.T2 = 22.0;
        a.T3 = 23.0;
        a.Tsample = 24.0;
        a.Tinlet = 20.0;
        a.TOPC = 25.0;
        a.Tnafion = 26.0;
        a.Qsample = 0.05;
        a.Qsheath = 0.45;
        a.P = 1013.25;
        a.Alaser = 500.0;
        a.V1stStage = 3.9;
        a.VpropValve = 2.3;
        a.minimumBin = 0;
        for (std::size_t i = 0; i < a.counts.size(); i++) {
            a.counts[a.counts.size() - 1 - i] = i * 10.0;
        }

        b.ssSP = 1.5;
        b.dtSP = 2.5;
        b.tempStable = true;
        b.T1 = 21.5;
        b.T2 = 22.5;
        b.T3 = 23.5;
        b.Tsample = 24.5;
        b.Tinlet = 20.5;
        b.TOPC = 25.5;
        b.Tnafion = 26.5;
        b.Qsample = 0.05;
        b.Qsheath = 0.50;
        b.P = 1013.20;
        b.Alaser = 501.0;
        b.V1stStage = 4.9;
        b.VpropValve = 3.3;
        b.minimumBin = 1;
        for (std::size_t i = 0; i < b.counts.size(); i++) {
            b.counts[b.counts.size() - 1 - i] = i * 11.0;
        }

        alarmBits = 0;
        multiLine = true;
    }

    void advance(double seconds)
    {
        unpolledRemaining -= seconds;
        timeOfDay += seconds;

        while (unpolledRemaining <= 0.0) {
            unpolledRemaining += 1.0;

            outgoing.append("H,");
            outgoing.append(
                    QByteArray::number((int) (timeOfDay / 3600.0) % 24).rightJustified(2, '0'));
            outgoing.append(':');
            outgoing.append(
                    QByteArray::number((int) (timeOfDay / 60.0) % 60).rightJustified(2, '0'));
            outgoing.append(':');
            outgoing.append(QByteArray::number((int) (timeOfDay) % 60).rightJustified(2, '0'));
            outgoing.append(',');

            outgoing.append(QByteArray::number(a.ssSP, 'f', 2));
            outgoing.append(',');
            outgoing.append(a.tempStable ? "1.00" : "0.00");
            outgoing.append(',');
            outgoing.append(QByteArray::number(a.dtSP, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(a.T1, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(a.T2, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(a.T3, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(a.Tnafion, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(a.Tinlet, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(a.TOPC, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(a.Tsample, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(a.Qsample * 1000.0, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(a.Qsheath * 1000.0, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(a.P, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(a.Alaser, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(a.V1stStage, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(a.VpropValve, 'f', 2));
            outgoing.append(',');

            outgoing.append(QByteArray::number(b.ssSP, 'f', 2));
            outgoing.append(',');
            outgoing.append(b.tempStable ? "1.00" : "0.00");
            outgoing.append(',');
            outgoing.append(QByteArray::number(b.dtSP, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(b.T1, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(b.T2, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(b.T3, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(b.Tnafion, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(b.Tinlet, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(b.TOPC, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(b.Tsample, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(b.Qsample * 1000.0, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(b.Qsheath * 1000.0, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(b.P, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(b.Alaser, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(b.V1stStage, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(b.VpropValve, 'f', 2));
            outgoing.append(',');

            outgoing.append(QByteArray::number(static_cast<quint16>(alarmBits)));
            if (multiLine) {
                outgoing.append('\r');
            } else {
                outgoing.append(',');
            }


            outgoing.append("C,");
            outgoing.append(QByteArray::number(a.minimumBin, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(totalConcentration(true), 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(a.counts.back(), 'f', 2));
            for (std::size_t i = 0; i < a.counts.size() - 1; i++) {
                outgoing.append(',');
                outgoing.append(QByteArray::number(a.counts[i], 'f', 2));
            }
            outgoing.append(',');
            outgoing.append(QByteArray::number(b.minimumBin, 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(totalConcentration(false), 'f', 2));
            outgoing.append(',');
            outgoing.append(QByteArray::number(b.counts.back(), 'f', 2));
            for (std::size_t i = 0; i < b.counts.size() - 1; i++) {
                outgoing.append(',');
                outgoing.append(QByteArray::number(b.counts[i], 'f', 2));
            }
            outgoing.append('\r');
        }
    }

    double binConcentration(int idx, bool columnA = true) const
    {
        const Column &src = columnA ? a : b;
        return src.counts[idx] / (src.Qsample * (1000.0 / 60.0));
    }

    Variant::Root allBinsCounts(bool columnA = true) const
    {
        const Column &src = columnA ? a : b;
        Variant::Root result;
        for (std::size_t i = 0; i < src.counts.size(); i++) {
            result.write().array(i).setDouble(src.counts[i]);
        }
        return result;
    }

    Variant::Root allBinsConcentrations(bool columnA = true) const
    {
        Variant::Root result;
        for (std::size_t i = 0; i < binCount; i++) {
            result.write().array(i).setDouble(binConcentration(i, columnA));
        }
        return result;
    }

    double totalConcentration(bool columnA = true) const
    {
        const Column &src = columnA ? a : b;
        double sum = 0.0;
        for (std::size_t i = src.minimumBin; i < src.counts.size(); i++) {
            sum += binConcentration(i, columnA);
        }
        return sum;
    }
};


class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream,
                   double time = FP::undefined(),
                   const SequenceName::Component &suffix = {})
    {
        if (!stream.hasMeta("F1" + suffix, Variant::Root(), QString(), time))
            return false;

        if (!stream.hasMeta("T1" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T2" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T3" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T4" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T5" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T6" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Tu" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("DT" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("DTg" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Q1" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Q2" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("U" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("P" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("V1" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("V2" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("A" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("N" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Np" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZBin" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Nb" + suffix, Variant::Root(static_cast<int>(binCount)), "^Count",
                            time))
            return false;

        if (!stream.hasMeta("Ts1" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Ts2" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Ts3" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Ts4" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Ts5" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Ts6" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Tus" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("DTs" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("DTgs" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Qs1" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Qs2" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Us" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Ps" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Vs1" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Vs2" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("As" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Ns" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Nps" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZsBin" + suffix, Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Nb" + suffix, Variant::Root(static_cast<int>(binCount)), "^Count",
                            time))
            return false;

        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("ZNc", Variant::Root(static_cast<int>(binCount)), "^Count", time))
            return false;
        if (!stream.hasMeta("ZNcs", Variant::Root(static_cast<int>(binCount)), "^Count", time))
            return false;
        return true;
    }

    bool checkContiguous(StreamCapture &stream)
    {
        if (!stream.checkContiguous("F1"))
            return false;

        if (!stream.checkContiguous("T1"))
            return false;
        if (!stream.checkContiguous("T2"))
            return false;
        if (!stream.checkContiguous("T3"))
            return false;
        if (!stream.checkContiguous("T4"))
            return false;
        if (!stream.checkContiguous("T5"))
            return false;
        if (!stream.checkContiguous("T6"))
            return false;
        if (!stream.checkContiguous("Tu"))
            return false;
        if (!stream.checkContiguous("DT"))
            return false;
        if (!stream.checkContiguous("DTg"))
            return false;
        if (!stream.checkContiguous("Q1"))
            return false;
        if (!stream.checkContiguous("Q2"))
            return false;
        if (!stream.checkContiguous("U"))
            return false;
        if (!stream.checkContiguous("P"))
            return false;
        if (!stream.checkContiguous("V1"))
            return false;
        if (!stream.checkContiguous("V2"))
            return false;
        if (!stream.checkContiguous("A"))
            return false;
        if (!stream.checkContiguous("N"))
            return false;
        if (!stream.checkContiguous("Np"))
            return false;
        if (!stream.checkContiguous("ZBin"))
            return false;
        if (!stream.checkContiguous("Nb"))
            return false;

        if (!stream.checkContiguous("Ts1"))
            return false;
        if (!stream.checkContiguous("Ts2"))
            return false;
        if (!stream.checkContiguous("Ts3"))
            return false;
        if (!stream.checkContiguous("Ts4"))
            return false;
        if (!stream.checkContiguous("Ts5"))
            return false;
        if (!stream.checkContiguous("Ts6"))
            return false;
        if (!stream.checkContiguous("Tus"))
            return false;
        if (!stream.checkContiguous("DTs"))
            return false;
        if (!stream.checkContiguous("DTgs"))
            return false;
        if (!stream.checkContiguous("Qs1"))
            return false;
        if (!stream.checkContiguous("Qs2"))
            return false;
        if (!stream.checkContiguous("Us"))
            return false;
        if (!stream.checkContiguous("Ps"))
            return false;
        if (!stream.checkContiguous("Vs1"))
            return false;
        if (!stream.checkContiguous("Vs2"))
            return false;
        if (!stream.checkContiguous("As"))
            return false;
        if (!stream.checkContiguous("Ns"))
            return false;
        if (!stream.checkContiguous("Nps"))
            return false;
        if (!stream.checkContiguous("ZsBin"))
            return false;
        if (!stream.checkContiguous("Nbs"))
            return false;

        return true;
    }

    bool checkValues(StreamCapture &stream,
                     const ModelInstrument &model,
                     double time = FP::undefined(),
                     const SequenceName::Component &suffix = {})
    {
        if (!stream.hasAnyMatchingValue("F1" + suffix, Variant::Root(), time))
            return false;

        if (!stream.hasAnyMatchingValue("T1" + suffix, Variant::Root(model.a.T1), time))
            return false;
        if (!stream.hasAnyMatchingValue("T2" + suffix, Variant::Root(model.a.T2), time))
            return false;
        if (!stream.hasAnyMatchingValue("T3" + suffix, Variant::Root(model.a.T3), time))
            return false;
        if (!stream.hasAnyMatchingValue("T4" + suffix, Variant::Root(model.a.Tsample), time))
            return false;
        if (!stream.hasAnyMatchingValue("T5" + suffix, Variant::Root(model.a.TOPC), time))
            return false;
        if (!stream.hasAnyMatchingValue("T6" + suffix, Variant::Root(model.a.Tnafion), time))
            return false;
        if (!stream.hasAnyMatchingValue("Tu" + suffix, Variant::Root(model.a.Tinlet), time))
            return false;
        if (!stream.hasAnyMatchingValue("DT" + suffix, Variant::Root(model.a.dtSP), time))
            return false;
        if (!stream.hasAnyMatchingValue("Q1" + suffix, Variant::Root(model.a.Qsample), time))
            return false;
        if (!stream.hasAnyMatchingValue("Q2" + suffix, Variant::Root(model.a.Qsheath), time))
            return false;
        if (!stream.hasAnyMatchingValue("U" + suffix, Variant::Root(model.a.ssSP), time))
            return false;
        if (!stream.hasAnyMatchingValue("P" + suffix, Variant::Root(model.a.P), time))
            return false;
        if (!stream.hasAnyMatchingValue("V1" + suffix, Variant::Root(model.a.V1stStage), time))
            return false;
        if (!stream.hasAnyMatchingValue("V2" + suffix, Variant::Root(model.a.VpropValve), time))
            return false;
        if (!stream.hasAnyMatchingValue("A" + suffix, Variant::Root(model.a.Alaser), time))
            return false;
        if (!stream.hasAnyMatchingValue("N" + suffix, Variant::Root(model.totalConcentration(true)),
                                        time))
            return false;
        if (!stream.hasAnyMatchingValue("Nb" + suffix, model.allBinsConcentrations(true), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZBin" + suffix, Variant::Root(model.a.minimumBin), time))
            return false;
        if (model.a.tempStable) {
            if (!stream.hasAnyMatchingValue("Np" + suffix,
                                            Variant::Root(model.totalConcentration(true)), time))
                return false;
        }

        if (!stream.hasAnyMatchingValue("Ts1" + suffix, Variant::Root(model.b.T1), time))
            return false;
        if (!stream.hasAnyMatchingValue("Ts2" + suffix, Variant::Root(model.b.T2), time))
            return false;
        if (!stream.hasAnyMatchingValue("Ts3" + suffix, Variant::Root(model.b.T3), time))
            return false;
        if (!stream.hasAnyMatchingValue("Ts4" + suffix, Variant::Root(model.b.Tsample), time))
            return false;
        if (!stream.hasAnyMatchingValue("Ts5" + suffix, Variant::Root(model.b.TOPC), time))
            return false;
        if (!stream.hasAnyMatchingValue("Ts6" + suffix, Variant::Root(model.b.Tnafion), time))
            return false;
        if (!stream.hasAnyMatchingValue("Tus" + suffix, Variant::Root(model.b.Tinlet), time))
            return false;
        if (!stream.hasAnyMatchingValue("DTs" + suffix, Variant::Root(model.b.dtSP), time))
            return false;
        if (!stream.hasAnyMatchingValue("Qs1" + suffix, Variant::Root(model.b.Qsample), time))
            return false;
        if (!stream.hasAnyMatchingValue("Qs2" + suffix, Variant::Root(model.b.Qsheath), time))
            return false;
        if (!stream.hasAnyMatchingValue("Us" + suffix, Variant::Root(model.b.ssSP), time))
            return false;
        if (!stream.hasAnyMatchingValue("Ps" + suffix, Variant::Root(model.b.P), time))
            return false;
        if (!stream.hasAnyMatchingValue("Vs1" + suffix, Variant::Root(model.b.V1stStage), time))
            return false;
        if (!stream.hasAnyMatchingValue("Vs2" + suffix, Variant::Root(model.b.VpropValve), time))
            return false;
        if (!stream.hasAnyMatchingValue("As" + suffix, Variant::Root(model.b.Alaser), time))
            return false;
        if (!stream.hasAnyMatchingValue("Ns" + suffix,
                                        Variant::Root(model.totalConcentration(false)), time))
            return false;
        if (!stream.hasAnyMatchingValue("Nbs" + suffix, model.allBinsConcentrations(false), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZsBin" + suffix, Variant::Root(model.b.minimumBin), time))
            return false;
        if (model.b.tempStable) {
            if (!stream.hasAnyMatchingValue("Nps" + suffix,
                                            Variant::Root(model.totalConcentration(false)), time))
                return false;
        }

        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream,
                             const ModelInstrument &model,
                             double time = FP::undefined())
    {
        if (!stream.hasAnyMatchingValue("ZNc", model.allBinsCounts(true), time))
            return false;
        if (!stream.hasAnyMatchingValue("ZNcs", model.allBinsCounts(false), time))
            return false;
        return true;
    }

private slots:

    void initTestCase()
    {
        component =
                qobject_cast<AcquisitionComponent *>(ComponentLoader::create("acquire_dmt_ccn200"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_dmt_ccn200"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["StrictMode"].setBool(true);
        if (!enableModel)
            cv["Model/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 240; i++) {
            control.advance(0.5);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated.wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 240; i++) {
            control.advance(0.5);
            if (i % 10 == 0)
                QTest::qSleep(50);
        }
        interface->initiateShutdown();
        QVERIFY(interface->wait(30));
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        if (enableModel) {
            QVERIFY(logging.checkContiguous("Uc"));
            QVERIFY(logging.hasMeta("Uc", Variant::Root(), QString()));
            QVERIFY(logging.hasAnyMatchingValue("Uc", Variant::Root(0.17279387), FP::undefined(),
                                                QString(), 1E-5));
            QVERIFY(realtime.hasMeta("Uc", Variant::Root(), QString()));
            QVERIFY(realtime.hasAnyMatchingValue("Uc", Variant::Root(0.17279387), FP::undefined(),
                                                 QString(), 1E-5));
        }
    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["StrictMode"].setBool(true);
        if (!enableModel)
            cv["Model/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.multiLine = false;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 240; i++) {
            control.advance(0.5);
        }
        for (int i = 0; i < 240; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        QVERIFY(interface->wait(30));
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        if (enableModel) {
            QVERIFY(logging.checkContiguous("Uc"));
            QVERIFY(logging.hasMeta("Uc", Variant::Root(), QString()));
            QVERIFY(logging.hasAnyMatchingValue("Uc", Variant::Root(0.17279387), FP::undefined(),
                                                QString(), 1E-5));
            QVERIFY(realtime.hasMeta("Uc", Variant::Root(), QString()));
            QVERIFY(realtime.hasAnyMatchingValue("Uc", Variant::Root(0.17279387), FP::undefined(),
                                                 QString(), 1E-5));
        }
    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Model/Enable"].setBool(false);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.a.tempStable = false;

        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 240; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }
        double checkTime = control.time();
        for (int i = 0; i < 240; i++) {
            control.advance(0.5);
        }
        while (control.time() < 360.0) {
            if (logging.hasAnyMatchingValue("U", Variant::Root(instrument.a.ssSP), checkTime))
                break;
            control.advance(0.5);
            QTest::qSleep(50);
        }
        QVERIFY(logging.hasAnyMatchingValue("U", Variant::Root(instrument.a.ssSP), checkTime));

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        QVERIFY(interface->wait(30));
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        QVERIFY(!logging.hasAnyMatchingValue("Np", Variant::Root()));
        QVERIFY(!realtime.hasAnyMatchingValue("Np", Variant::Root()));
    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        if (enableModel) {
            cv["Model/Interval/Unit"].setString("Seconds");
            cv["Model/Interval/Count"].setInt64(30);
            cv["Model/Interval/Aligned"].setBool(false);
            cv["Model/MaximumBacklog"].setInt64(-1);
        } else {
            cv["Model/Enable"].setBool(false);
        }
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        for (int i = 0; i < 240; i++) {
            control.advance(0.5);
            QTest::qSleep(50);
        }

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        QVERIFY(interface->wait(30));
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        if (enableModel) {
            QVERIFY(logging.checkContiguous("Uc"));
            QVERIFY(logging.hasMeta("Uc", Variant::Root(), QString()));
            QVERIFY(logging.hasAnyMatchingValue("Uc", Variant::Root(0.17279387), FP::undefined(),
                                                QString(), 1E-5));
            QVERIFY(realtime.hasMeta("Uc", Variant::Root(), QString()));
            QVERIFY(realtime.hasAnyMatchingValue("Uc", Variant::Root(0.17279387), FP::undefined(),
                                                 QString(), 1E-5));
        }
    }

    void badModel()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Model/Program"].setString("zzzz_bad");
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 240; i++) {
            control.advance(0.5);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated.wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 240; i++) {
            control.advance(0.5);
            if (i % 10 == 0)
                QTest::qSleep(50);
        }
        interface->initiateShutdown();
        QVERIFY(interface->wait(30));
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));
    }

    void instrumentTimeConvert()
    {
        ComponentOptions options;
        options = ingress->getOptions();

        qobject_cast<ComponentOptionSingleTime *>(options.get("basetime"))->set(86400.0);
        std::unique_ptr<ExternalConverter> input(ingress->createDataIngress(options));
        QVERIFY(input.get());
        input->start();

        ModelInstrument instrument;
        for (int i = 0; i < 480; i++) {
            instrument.advance(0.5);
        }

        StreamCapture logging;
        input->setEgress(&logging);
        input->incomingData(instrument.outgoing);
        input->endData();
        input->wait(30);
        input.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkMeta(logging, FP::undefined(), "_N11"));
        QVERIFY(checkValues(logging, instrument, 86460.0, "_N11"));
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
