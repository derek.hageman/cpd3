/*
 * Copyright (c) 2021 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#ifndef ACQUIREDMTCCN200_HXX
#define ACQUIREDMTCCN200_HXX

#include "core/first.hxx"

#include <memory>
#include <deque>
#include <map>
#include <array>
#include <QtGlobal>
#include <QObject>

#include "core/component.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/framedinstrument.hxx"
#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "datacore/staticmultiplexer.hxx"
#include "core/number.hxx"
#include "smoothing/baseline.hxx"
#include "datacore/dynamictimeinterval.hxx"

class AcquireDMTCCN200 : public CPD3::Acquisition::FramedInstrument {
    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus autoprobeStatus;

    enum class ResponseState : int {
        /* Waiting for enough valid records to confirm autoprobe success */
        Autoprobe_Wait,

        /* Waiting for a valid record to start acquisition */
        Wait,

        /* Acquiring data in passive mode */
        Run,

        /* Same as wait, but discard the first timeout */
        Initialize,

        /* Same as initialize but autoprobing */
        Autoprobe_Initialize,
    } responseState;

    enum class LogStreamID : std::size_t {
        Metadata = 0, State, Model, Processing,

        H, C,

        TOTAL, RecordBegin = H
    };
    CPD3::Data::StaticMultiplexer loggingMux;
    std::array<std::uint_fast32_t, static_cast<std::size_t>(LogStreamID::TOTAL)> streamAge;
    std::array<double, static_cast<std::size_t>(LogStreamID::TOTAL)> streamTime;

    class Configuration {
        double start;
        double end;

    public:
        bool enableModel;
        std::string modelProgram;
        std::string modelParameters;
        std::size_t maximumModelBacklog;

        bool useReportedStability;
        bool useCalculatedStability;

        double timeOrigin;
        double reportInterval;
        bool strictMode;


        inline double getStart() const
        { return start; }

        inline double getEnd() const
        { return end; }

        inline void setStart(double v)
        { start = v; }

        inline void setEnd(double v)
        { end = v; }

        Configuration();

        Configuration(const Configuration &other, double start, double end);

        Configuration(const CPD3::Data::ValueSegment &other, double start, double end);

        Configuration(const Configuration &under,
                      const CPD3::Data::ValueSegment &over,
                      double start,
                      double end);

        void setFromSegment(const CPD3::Data::ValueSegment &config);
    };

    std::deque<Configuration> config;

    std::unique_ptr<CPD3::Data::DynamicTimeInterval> modelInterval;
    std::unique_ptr<CPD3::Data::DynamicTimeInterval> processingInterval;
    std::unique_ptr<CPD3::Smoothing::BaselineSmoother> dtStabilityA;
    std::unique_ptr<CPD3::Smoothing::BaselineSmoother> dtStabilityB;

    void setDefaultInvalid();

    CPD3::Data::Variant::Root instrumentMeta;

    bool haveEmittedLogMeta;
    bool haveEmittedRealtimeMeta;
    bool realtimeStateUpdated;

    struct ModelPoint;

    struct ModelInput {
        double start;
        double end;
        double Qtot;
        double Qshaer;
        double P;
        double Tinlet;
        double Tcold;
        double Thot;
        int columnNumber;

        bool valid() const;

        ModelInput() = default;

        explicit ModelInput(std::deque<ModelPoint> &points, double intervalEnd);
    };

    struct ModelResult {
        double ss;
        bool valid;

        ModelInput input;
    };

    struct ProcessingPoint {
        double start;
        double end;
        double DT;
    };

    struct ModelPoint {
        double start;
        double end;
        double Q1;
        double Q2;
        double P;
        double T1;
        double T3;
        double Tu;
    };

    class ModelRunner;

    std::unique_ptr<ModelRunner> model;
    std::deque<ModelPoint> modelDataA;
    std::deque<ModelPoint> modelDataB;
    std::deque<ProcessingPoint> processingDataA;
    std::deque<ProcessingPoint> processingDataB;

    double lastHRecordTime;

    CPD3::Data::Variant::Flags instrumentAlarmFlags;
    bool reportedStableA;
    bool reportedStableB;
    double lastSampleFlowA;
    double lastSampleFlowB;
    std::int_fast64_t lastBinNumberA;
    std::int_fast64_t lastBinNumberB;

    void logValue(double startTime,
                  double endTime,
                  CPD3::Data::SequenceName::Component name,
                  CPD3::Data::Variant::Root &&value,
                  LogStreamID streamID);

    void realtimeValue(double time,
                       CPD3::Data::SequenceName::Component name,
                       CPD3::Data::Variant::Root &&value);

    void configAdvance(double frameTime);

    CPD3::Data::SequenceValue::Transfer buildLogMeta(double time) const;

    CPD3::Data::SequenceValue::Transfer buildRealtimeMeta(double time) const;

    void invalidateLogValues(double frameTime);

    void configurationAdvance(double frameTime);

    void emptyModelProcessing(double frameTime = CPD3::FP::undefined());

    void configurationChanged(double frameTime = CPD3::FP::undefined());

    void processModelPending(double frameTime = CPD3::FP::undefined());

    void emitMetadata(double frameTime);

    void advanceStream(LogStreamID streamID, double frameTime);

    double remapModelInput(const CPD3::Data::SequenceName::Component &name,
                           const CPD3::Data::Variant::Read &input);

    int processH(std::deque<CPD3::Util::ByteView> &fields, double &frameTime);

    int processC(std::deque<CPD3::Util::ByteView> &fields, double &frameTime);

    int processRecord(const CPD3::Util::ByteView &line, double &frameTime);

public:
    AcquireDMTCCN200(const CPD3::Data::ValueSegment::Transfer &config,
                     const std::string &loggingContext);

    AcquireDMTCCN200(const CPD3::ComponentOptions &options, const std::string &loggingContext);

    virtual ~AcquireDMTCCN200();

    CPD3::Data::Variant::Root getSourceMetadata() override;

    CPD3::Acquisition::AcquisitionInterface::GeneralStatus getGeneralStatus() override;

    CPD3::Data::SequenceMatch::Composite getExplicitGroupedVariables() override;

    CPD3::Acquisition::AcquisitionInterface::AutoprobeStatus getAutoprobeStatus() override;

    void autoprobeTryInteractive(double time) override;

    void autoprobeResetPassive(double time) override;

    void autoprobePromote(double time) override;

    void autoprobePromotePassive(double time) override;

    AutomaticDefaults getDefaults() override;

    void setToAutoprobe();

    void setToInteractive();

protected:
    void incomingDataFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingControlFrame(const CPD3::Util::ByteArray &frame, double time) override;

    void incomingInstrumentTimeout(double time) override;

    void discardDataCompleted(double time) override;

    void shutdownInProgress(double time) override;
};

class AcquireDMTCCN200Component
        : public QObject,
          virtual public CPD3::Acquisition::AcquisitionComponent,
          virtual public CPD3::Data::ExternalConverterComponent {
Q_OBJECT
    Q_INTERFACES(CPD3::Data::ExternalConverterComponent
                         CPD3::Acquisition::AcquisitionComponent)

    Q_PLUGIN_METADATA(IID
                              "CPD3.acquire_dmt_ccn200"
                              FILE
                              "acquire_dmt_ccn200.json")

    CPD3::ComponentOptions getBaseOptions();

public:
    CPD3::ComponentOptions getOptions() override;

    QList<CPD3::ComponentExample> getExamples() override;

    bool requiresInputDevice() override;

    CPD3::Data::ExternalConverter *createDataIngress(const CPD3::ComponentOptions &options = {}) override;

    CPD3::ComponentOptions getPassiveOptions() override;

    QList<CPD3::ComponentExample> getPassiveExamples() override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::ComponentOptions &options = {},
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionPassive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                              const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionAutoprobe(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                const std::string &loggingContext = {}) override;

    std::unique_ptr<
            CPD3::Acquisition::AcquisitionInterface> createAcquisitionInteractive(const CPD3::Data::ValueSegment::Transfer &config,
                                                                                  const std::string &loggingContext = {}) override;
};


#endif
