/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <algorithm>

#include "core/component.hxx"
#include "core/environment.hxx"
#include "core/range.hxx"
#include "core/qtcompat.hxx"
#include "datacore/stream.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "acquisition/lineingress.hxx"

#include "acquire_tsi_cpc302x.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

AcquireTSICPC302X::Configuration::Configuration() : start(FP::undefined()),
                                                    end(FP::undefined()),
                                                    flow(FP::undefined()),
                                                    useMeasuredTime(false)
{ }

AcquireTSICPC302X::Configuration::Configuration(const Configuration &other, double s, double e)
        : start(s), end(e), flow(other.flow), useMeasuredTime(other.useMeasuredTime)
{ }

AcquireTSICPC302X::Configuration::Configuration(const ValueSegment &other, double s, double e)
        : start(s), end(e), flow(FP::undefined()), useMeasuredTime(false)
{
    setFromSegment(other);
}

AcquireTSICPC302X::Configuration::Configuration(const Configuration &under,
                                                const ValueSegment &over,
                                                double s,
                                                double e) : start(s),
                                                            end(e),
                                                            flow(under.flow),
                                                            useMeasuredTime(under.useMeasuredTime)
{
    setFromSegment(over);
}

void AcquireTSICPC302X::setDefaultInvalid()
{
    instrumentMeta["Manufacturer"].setString("TSI");
    instrumentMeta["Model"].setString("302x");

    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
    forceRealtimeStateEmit = true;

    memset(streamAge, 0, sizeof(streamAge));
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
    }

    countSeconds = 0;
    countPulses = 0;
    liquidFull = true;
    instrumentReady = true;
    lastReportedFlow = FP::undefined();
}

void AcquireTSICPC302X::Configuration::setFromSegment(const ValueSegment &config)
{
    if (config["Flow"].exists())
        flow = config["Flow"].toDouble();

    if (config["UseMeasuredTime"].exists())
        useMeasuredTime = config["UseMeasuredTime"].toBool();
}

AcquireTSICPC302X::AcquireTSICPC302X(const ValueSegment::Transfer &configData,
                                     const std::string &loggingContext) : FramedInstrument(
        "tsi302x", loggingContext),
                                                                          autoprobeStatus(
                                                                                  AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                          responseState(
                                                                                  RESP_PASSIVE_WAIT),
                                                                          autoprobePassiveValidRecords(
                                                                                  0),
                                                                          loggingMux(
                                                                                  LogStream_TOTAL)
{
    setDefaultInvalid();
    config.append(Configuration());

    for (const auto &add : configData) { Range::overlayFragmenting(config, add); }

    configurationChanged();
}

void AcquireTSICPC302X::setToAutoprobe()
{ responseState = RESP_AUTOPROBE_PASSIVE_INITIALIZE; }

void AcquireTSICPC302X::setToInteractive()
{ responseState = RESP_INTERACTIVE_INITIALIZE; }


ComponentOptions AcquireTSICPC302XComponent::getBaseOptions()
{
    ComponentOptions options;

    ComponentOptionSingleDouble *d =
            new ComponentOptionSingleDouble(tr("q", "name"), tr("Flow rate"),
                                            tr("This is the sample flow rate of the CPC at lpm measured at ambient "
                                               "conditions."), tr("0.03 lpm", "default flow rate"),
                                            1);
    d->setMinimum(0.0, false);
    options.add("q", d);

    options.add("measuredtime", new ComponentOptionBoolean(tr("measuredtime", "name"),
                                                           tr("Use measured time interval"),
                                                           tr("If set then the measured time interval between reports is used "
                                                              "instead of assuming one second.  This is generally not "
                                                              "recommended because it is affected by processing delays and "
                                                              "jitter in the system scheduling."),
                                                           QString()));

    return options;
}

AcquireTSICPC302X::AcquireTSICPC302X(const ComponentOptions &options,
                                     const std::string &loggingContext) : FramedInstrument(
        "tsi302x", loggingContext),
                                                                          autoprobeStatus(
                                                                                  AcquisitionInterface::AutoprobeStatus::InProgress),
                                                                          responseState(
                                                                                  RESP_PASSIVE_WAIT),
                                                                          autoprobePassiveValidRecords(
                                                                                  0),
                                                                          loggingMux(
                                                                                  LogStream_TOTAL)
{
    setDefaultInvalid();
    config.append(Configuration());

    if (options.isSet("q")) {
        double value = qobject_cast<ComponentOptionSingleDouble *>(options.get("q"))->get();
        if (FP::defined(value) && value > 0.0)
            config.last().flow = value;
    }
    if (options.isSet("measuredtime")) {
        config.last().useMeasuredTime =
                qobject_cast<ComponentOptionBoolean *>(options.get("measuredtime"))->get();
    }

    configurationChanged();
}

AcquireTSICPC302X::~AcquireTSICPC302X()
{
}

SequenceValue::Transfer AcquireTSICPC302X::buildLogMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_tsi_cpc302x");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["CalibrationLabel"].set(instrumentMeta["CalibrationLabel"]);

    result.emplace_back(SequenceName({}, "raw_meta", "N"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0");
    result.back().write().metadataReal("Units").setString("cm\xE2\x81\xBB³");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Condensation nuclei concentration");
    if (FP::defined(config.first().flow)) {
        result.back().write().metadataReal("SampleFlow").setDouble(config.first().flow);
    }
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Concentration"));

    result.emplace_back(SequenceName({}, "raw_meta", "T1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Saturator temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Saturator"));

    result.emplace_back(SequenceName({}, "raw_meta", "T2"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Condenser temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Condenser"));

    result.emplace_back(SequenceName({}, "raw_meta", "T3"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("00.0");
    result.back().write().metadataReal("Units").setString("\xC2\xB0\x43");
    result.back().write().metadataReal("Description").setString("Optics temperature");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Optics"));

    result.emplace_back(SequenceName({}, "raw_meta", "Q"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Sample flow rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back().write().metadataReal("Realtime").hash("Name").setString(QObject::tr("Sample"));

    result.emplace_back(SequenceName({}, "raw_meta", "F1"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataFlags("Format").setString("FFFFFFFF");
    result.back().write().metadataFlags("Description").setString("Instrument flags");
    result.back().write().metadataFlags("Source").set(instrumentMeta);
    result.back().write().metadataFlags("Processing").toArray().after_back().set(processing);

    result.back()
          .write()
          .metadataSingleFlag("LiquidLow")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_cpc302x");
    result.back().write().metadataSingleFlag("LiquidLow").hash("Bits").setInt64(0x20000);
    result.back()
          .write()
          .metadataSingleFlag("LiquidLow")
          .hash("Description")
          .setString("Instrument reporting low butanol level");

    result.back()
          .write()
          .metadataSingleFlag("InstrumentNotReady")
          .hash("Origin")
          .toArray()
          .after_back()
          .setString("acquire_tsi_cpc302x");
    result.back().write().metadataSingleFlag("InstrumentNotReady").hash("Bits").setInt64(0x10000);
    result.back()
          .write()
          .metadataSingleFlag("InstrumentNotReady")
          .hash("Description")
          .setString(
                  "Instrument reporting a not-ready condition (laser, temperature, flow or liquid out of expected range)");

    return result;
}

SequenceValue::Transfer AcquireTSICPC302X::buildRealtimeMeta(double time) const
{
    SequenceValue::Transfer result;

    Variant::Root processing;
    processing["By"].setString("acquire_tsi_cpc302x");
    processing["At"].setDouble(Time::time());
    processing["Environment"].setString(Environment::describe());
    processing["Revision"].setString(Environment::revision());
    processing["CalibrationLabel"].set(instrumentMeta["CalibrationLabel"]);

    result.emplace_back(SequenceName({}, "raw_meta", "C"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("000000.0");
    result.back().write().metadataReal("Units").setString("Hz");
    result.back().write().metadataReal("Description").setString("Count rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Count rate"));

    result.emplace_back(SequenceName({}, "raw_meta", "ZND"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataReal("Format").setString("00000.0");
    result.back().write().metadataReal("Units").setString("cm\xE2\x81\xBB³");
    result.back()
          .write()
          .metadataReal("Description")
          .setString("Displayed condensation nuclei concentration");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Display concentration"));

    result.emplace_back(SequenceName({}, "raw_meta", "ZQ"), Variant::Root(), time, FP::undefined());
    result.back().write().metadataReal("Format").setString("0.000");
    result.back().write().metadataReal("Units").setString("lpm");
    result.back().write().metadataReal("Description").setString("Reported flow rate");
    result.back().write().metadataReal("Source").set(instrumentMeta);
    result.back().write().metadataReal("Processing").toArray().after_back().set(processing);
    result.back()
          .write()
          .metadataReal("Realtime")
          .hash("Name")
          .setString(QObject::tr("Display flow"));

    result.emplace_back(SequenceName({}, "raw_meta", "ZSTATE"), Variant::Root(), time,
                        FP::undefined());
    result.back().write().metadataString("Description").setString("Current state enumeration type");
    result.back().write().metadataString("Source").set(instrumentMeta);
    result.back().write().metadataString("Processing").toArray().after_back().set(processing);
    result.back().write().metadataString("Realtime").hash("Persistent").setBool(true);
    result.back().write().metadataString("Realtime").hash("Hide").setBool(true);
    /*result.back().write().metadataString("Realtime").hash("Name").
        setString(QString());
    result.back().write().metadataString("Realtime").hash("Units").
        setString(QString());
    result.back().write().metadataString("Realtime").hash("FullLine").
        setBool(true);        
    result.back().write().metadataString("Realtime").hash("RowOrder").
        setInt64(1);
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("Run").setString(QObject::tr("", "run state string"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("InvalidResponse").setString(QObject::tr("NO COMMS: Invalid response"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("Timeout").setString(QObject::tr("NO COMMS: Timeout"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractive").setString(QObject::tr("STARTING COMMS"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveDisambiguateC").setString(QObject::tr("STARTING COMMS: Instrument disambiguation"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveDisambiguateRV").setString(QObject::tr("STARTING COMMS: Instrument disambiguation"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveDisambiguateRC").setString(QObject::tr("STARTING COMMS: Instrument disambiguation"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveReadCondenser").setString(QObject::tr("STARTING COMMS: Reading condenser temperature"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveReadSaturator").setString(QObject::tr("STARTING COMMS: Reading saturator temperature"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveReadOptics").setString(QObject::tr("STARTING COMMS: Reading optics temperature"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveReadI").setString(QObject::tr("STARTING COMMS: Reading calibration label"));
    result.back().write().metadataString("Realtime").hash("Translation").
        hash("StartInteractiveReadCounts").setString(QObject::tr("STARTING COMMS: Waiting for non-zero count time"));*/


    return result;
}

SequenceMatch::Composite AcquireTSICPC302X::getExplicitGroupedVariables()
{
    SequenceMatch::Composite sel;
    sel.append({}, {}, "ZSTATE");
    return sel;
}

void AcquireTSICPC302X::logValue(double startTime,
                                 double endTime,
                                 int streamID,
                                 SequenceName::Component name,
                                 Variant::Root &&value)
{
    if (!loggingEgress && !realtimeEgress)
        return;
    if (!FP::defined(startTime) || !FP::defined(endTime)) {
        if (realtimeEgress && FP::defined(endTime)) {
            realtimeEgress->emplaceData(SequenceName({}, "raw", std::move(name)), std::move(value),
                                        endTime, endTime + 1.0);
        }
        return;
    }
    SequenceValue
            dv(SequenceName({}, "raw", std::move(name)), std::move(value), startTime, endTime);
    if (!realtimeEgress) {
        loggingMux.incoming(streamID, std::move(dv), loggingEgress);
        return;
    }
    if (loggingEgress) {
        loggingMux.incoming(streamID, dv, loggingEgress);
    }
    dv.setStart(endTime);
    dv.setEnd(endTime + 1.0);
    realtimeEgress->incomingData(std::move(dv));
}

void AcquireTSICPC302X::realtimeValue(double time,
                                      SequenceName::Component name,
                                      Variant::Root &&value)
{
    if (!realtimeEgress) return;
    realtimeEgress->emplaceData(SequenceIdentity({}, "raw", std::move(name)), std::move(value),
                                time, time + 1.0);
}

void AcquireTSICPC302X::emitMetadata(double frameTime)
{
    if (loggingEgress != NULL) {
        if (!haveEmittedLogMeta) {
            haveEmittedLogMeta = true;
            loggingMux.incoming(LogStream_Metadata, buildLogMeta(frameTime), loggingEgress);
        }
        loggingMux.advance(LogStream_Metadata, frameTime, loggingEgress);
    } else {
        invalidateLogValues(frameTime);
    }

    if (!haveEmittedRealtimeMeta && realtimeEgress != NULL) {
        haveEmittedRealtimeMeta = true;

        SequenceValue::Transfer meta = buildLogMeta(frameTime);
        Util::append(buildRealtimeMeta(frameTime), meta);
        realtimeEgress->incomingData(std::move(meta));
    }
}

void AcquireTSICPC302X::streamAdvance(int streamID, double time)
{
    quint32 bits = 1 << streamID;
    if (loggingEgress != NULL) {
        for (int i = LogStream_RecordBaseStart; i < LogStream_TOTAL; i++) {
            if (i == streamID)
                continue;
            if (!(streamAge[i] & bits)) {
                streamAge[i] |= bits;
                continue;
            }
            streamAge[i] = bits;
            streamTime[i] = FP::undefined();
            loggingMux.advance(i, time, loggingEgress);
        }

        streamAge[streamID] = bits;
        streamTime[streamID] = time;
        loggingMux.advance(streamID, time, loggingEgress);
    } else {
        streamAge[streamID] = 0;
        streamTime[streamID] = FP::undefined();
    }

    if (!(streamAge[LogStream_State] & bits)) {
        streamAge[LogStream_State] |= bits;
    } else {
        double startTime = streamTime[LogStream_State];
        double endTime = time;

        if (loggingEgress != NULL) {
            streamAge[LogStream_State] = bits;
            streamTime[LogStream_State] = endTime;
        } else {
            streamAge[LogStream_State] = 0;
            streamTime[LogStream_State] = FP::undefined();
        }

        Variant::Flags flags;
        if (!liquidFull)
            flags.insert("LiquidLow");
        if (!instrumentReady)
            flags.insert("InstrumentNotReady");

        logValue(startTime, endTime, LogStream_State, "F1", Variant::Root(std::move(flags)));
        loggingMux.advance(LogStream_State, endTime, loggingEgress);
    }
}

void AcquireTSICPC302X::describeState(Variant::Write &info, const Command &command) const
{
    if (command.getType() != COMMAND_INVALID) {
        info.hash("Command").set(command.stateDescription());
    } else if (!commandQueue.isEmpty()) {
        info.hash("Command").set(commandQueue.first().stateDescription());
    }

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        info.hash("ResponseState").setString("AutoprobePassiveInitialize");
        break;
    case RESP_AUTOPROBE_PASSIVE_WAIT:
        info.hash("ResponseState").setString("AutoprobePassiveWait");
        break;
    case RESP_PASSIVE_WAIT:
        info.hash("ResponseState").setString("PassiveWait");
        break;
    case RESP_PASSIVE_RUN:
        info.hash("ResponseState").setString("PassiveRun");
        break;
    case RESP_INTERACTIVE_INITIALIZE:
        info.hash("ResponseState").setString("InteractiveInitialize");
        break;
    case RESP_INTERACTIVE_RESTART_WAIT:
        info.hash("ResponseState").setString("InteractiveRestartWait");
        break;

    case RESP_INTERACTIVE_START_READ_COUNTS:
        info.hash("ResponseState").setString("InteractiveStartReadCounts");
        break;
    case RESP_INTERACTIVE_START_READ_CONDENSERTEMP:
        info.hash("ResponseState").setString("InteractiveStartReadCondenser");
        break;
    case RESP_INTERACTIVE_START_READ_SATURATORTEMP:
        info.hash("ResponseState").setString("InteractiveStartReadSaturator");
        break;
    case RESP_INTERACTIVE_START_READ_OPTICSTEMP:
        info.hash("ResponseState").setString("InteractiveStartReadOptics");
        break;
    case RESP_INTERACTIVE_START_READ_I:
        info.hash("ResponseState").setString("InteractiveReadI");
        break;
    case RESP_INTERACTIVE_START_DISAMBIGUATE_C:
        info.hash("ResponseState").setString("InteractiveStartDisamiguateC");
        break;
    case RESP_INTERACTIVE_START_DISAMBIGUATE_RV:
        info.hash("ResponseState").setString("InteractiveStartDisamiguateRV");
        break;
    case RESP_INTERACTIVE_START_DISAMBIGUATE_RC:
        info.hash("ResponseState").setString("InteractiveStartDisamiguateRC");
        break;

    case RESP_INTERACTIVE_RUN_READ_COUNTS:
        info.hash("ResponseState").setString("InteractiveRunCounts");
        break;
    case RESP_INTERACTIVE_RUN_READ_CONDENSERTEMP:
        info.hash("ResponseState").setString("InteractiveRunCondenser");
        break;
    case RESP_INTERACTIVE_RUN_READ_SATURATORTEMP:
        info.hash("ResponseState").setString("InteractiveRunSaturator");
        break;
    case RESP_INTERACTIVE_RUN_READ_OPTICSTEMP:
        info.hash("ResponseState").setString("InteractiveRunOptics");
        break;
    case RESP_INTERACTIVE_RUN_READ_FLOW:
        info.hash("ResponseState").setString("InteractiveRunFlow");
        break;
    case RESP_INTERACTIVE_RUN_READ_DISPLAY:
        info.hash("ResponseState").setString("InteractiveRunDisplay");
        break;
    case RESP_INTERACTIVE_RUN_READ_LIQUID:
        info.hash("ResponseState").setString("InteractiveRunLiquid");
        break;
    case RESP_INTERACTIVE_RUN_READ_STATUS:
        info.hash("ResponseState").setString("InteractiveRunStatus");
        break;
    case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
        info.hash("ResponseState").setString("InteractiveRunExternal");
        break;
    }
}

AcquireTSICPC302X::Command::Command() : type(COMMAND_INVALID), counter(0)
{ }

AcquireTSICPC302X::Command::Command(CommandType t) : type(t), counter(0)
{ }

AcquireTSICPC302X::Command::Command(const Command &other) : type(other.type), counter(other.counter)
{ }

AcquireTSICPC302X::Command &AcquireTSICPC302X::Command::operator=(const Command &other)
{
    if (&other == this)
        return *this;
    type = other.type;
    counter = other.counter;
    return *this;
}

Variant::Root AcquireTSICPC302X::Command::stateDescription() const
{
    Variant::Root result;

    switch (type) {
    default:
        result["Type"].setString("Unknown");
        result["Counter"].setInt64(counter);
        break;
    case COMMAND_D:
        result["Type"].setString("D");
        result["Counter"].setInt64(counter);
        break;
    case COMMAND_DC:
        result["Type"].setString("DC");
        break;
    case COMMAND_C:
        result["Type"].setString("C");
        break;
    case COMMAND_R0:
        result["Type"].setString("R0");
        break;
    case COMMAND_R1:
        result["Type"].setString("R1");
        break;
    case COMMAND_R2:
        result["Type"].setString("R2");
        break;
    case COMMAND_R3:
        result["Type"].setString("R3");
        break;
    case COMMAND_R4:
        result["Type"].setString("R4");
        break;
    case COMMAND_R5:
        result["Type"].setString("R5");
        break;
    case COMMAND_RD:
        result["Type"].setString("RD");
        break;
    case COMMAND_OK:
        result["Type"].setString("OK");
        break;
    case COMMAND_ERROR:
        result["Type"].setString("Error");
        break;
    }
    return result;
}

void AcquireTSICPC302X::configurationChanged()
{
    haveEmittedLogMeta = false;
    haveEmittedRealtimeMeta = false;
}

void AcquireTSICPC302X::configurationAdvance(double frameTime)
{
    Q_ASSERT(!config.isEmpty());
    int oldSize = config.size();
    if (!Range::intersectShift(config, frameTime)) {
        qCWarning(log) << "Can't find active configuration (list corrupted)";
    }
    Q_ASSERT(!config.isEmpty());

    if (oldSize == config.size())
        return;
    configurationChanged();
}

static bool validDigit(char c)
{ return c >= '0' && c <= '9'; }

static bool isOnlyDigits(const Util::ByteView &frame)
{
    if (frame.empty())
        return false;
    for (auto c : frame) {
        if (!validDigit(c))
            return false;
    }
    return true;
}

static double calculateConcentration(double C, double Q)
{
    if (!FP::defined(C) || !FP::defined(Q))
        return FP::undefined();
    if (Q <= 0.0)
        return FP::undefined();
    return C / (Q * (1000.0 / 60.0));
}

void AcquireTSICPC302X::interactiveEstablished(double frameTime)
{
    Variant::Write info = Variant::Write::empty();
    describeState(info);
    event(frameTime, QObject::tr("Communications established."), false, info);

    if (controlStream != NULL) {
        controlStream->writeControl("DC\r");
    }
    responseState = RESP_INTERACTIVE_RUN_READ_COUNTS;
    commandQueue.append(Command(COMMAND_DC));
    forceRealtimeStateEmit = true;

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
    }
    autoprobeStatusUpdated();
    generalStatusUpdated();

    if (realtimeEgress != NULL) {
        realtimeEgress->incomingData(
                SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                              FP::undefined()));
    }
}

int AcquireTSICPC302X::processResponse(const Util::ByteView &frame, double frameTime)
{
    if (commandQueue.isEmpty()) {
        if (!isOnlyDigits(frame))
            return -1;
        /* If we have no command and we get a line of only digits, assume
         * it's the start of a D command so we can use the unpolled
         * mode */
        commandQueue.append(Command(COMMAND_D));
    }

    Util::ByteView field;
    bool ok = false;

    Command command(commandQueue.takeFirst());
    switch (command.getType()) {
    case COMMAND_ERROR:
        if (frame != "ERROR")
            return 1;

        switch (responseState) {
        case RESP_INTERACTIVE_START_DISAMBIGUATE_RV:
            if (controlStream != NULL) {
                controlStream->writeControl("RC\r");
            }
            responseState = RESP_INTERACTIVE_START_DISAMBIGUATE_RC;
            commandQueue.append(Command(COMMAND_RC));

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveDisambiguateRC"), frameTime, FP::undefined()));
            }
            return 0;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("DC\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_COUNTS;
            commandQueue.append(Command(COMMAND_DC));
            timeoutAt(frameTime + 2.0);
            break;

        default:
            break;
        }
        return -1;

    case COMMAND_INVALID:
        return 2;

    case COMMAND_IGNORED:
        return -1;

    case COMMAND_D:
        if (command.getCounter() == 0) {
            countSeconds = Util::ByteView(frame).string_trimmed().parse_i64(&ok, 10);
            if (!ok) return 1000;
            if (countSeconds < 0) return 1001;
            command.setCounter(1);
            commandQueue.prepend(command);
            return -2;
        } else {
            auto fields = Util::as_deque(frame.split(','));
            if (fields.size() != 2) return 1002;
            if (command.getCounter() == 1) {
                field = fields.front().string_trimmed();
                fields.pop_front();
                if (!isOnlyDigits(field)) return 1003;
                countPulses = field.parse_i64(&ok, 10);
                if (countPulses < 0) return 1004;
                if (!INTEGER::defined(countPulses)) return 1005;
            } else {
                field = fields.front().string_trimmed();
                fields.pop_front();
                if (!isOnlyDigits(field)) return 1006;
                field.parse_i64(&ok, 10);
            }
            if (!ok) return 1007;
            field = fields.front().string_trimmed();
            fields.pop_front();
            if (!isOnlyDigits(field)) return 1008;
            field.parse_i64(&ok, 10);
            if (!ok) return 1009;
            Q_ASSERT(fields.empty());

            if (command.getCounter() < 16) {
                command.setCounter(command.getCounter() + 1);
                commandQueue.prepend(command);
                return -2;
            }
        }

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            break;

        case RESP_INTERACTIVE_RUN_READ_COUNTS:
            if (countSeconds > 0) {
                if (controlStream != NULL) {
                    controlStream->writeControl("R1\r");
                }
                responseState = RESP_INTERACTIVE_RUN_READ_CONDENSERTEMP;
                commandQueue.append(Command(COMMAND_R1));
            } else {
                if (controlStream != NULL) {
                    controlStream->writeControl("DC\r");
                }
                commandQueue.append(Command(COMMAND_DC));
            }
            break;

        case RESP_INTERACTIVE_START_READ_COUNTS:
            if (countSeconds <= 0) {
                if (controlStream != NULL) {
                    controlStream->writeControl("DC\r");
                }
                commandQueue.append(Command(COMMAND_DC));
                return -1;
            }

            if (controlStream != NULL) {
                controlStream->writeControl("R1\r");
            }
            responseState = RESP_INTERACTIVE_START_READ_CONDENSERTEMP;
            commandQueue.append(Command(COMMAND_R1));

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadCondenser"), frameTime, FP::undefined()));
            }
            return 0;

        case RESP_PASSIVE_RUN:
            break;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("DC\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_COUNTS;
            commandQueue.append(Command(COMMAND_DC));
            timeoutAt(frameTime + 2.0);
            break;

        default:
            return -1;
        }

        emitMetadata(frameTime);

        if (countSeconds > 0) {
            double startTime = streamTime[LogStream_Counts];
            double endTime = frameTime;
            double rate;
            if (config.first().useMeasuredTime && FP::defined(startTime) && FP::defined(endTime)) {
                double dT = endTime - startTime;
                if (dT > 0.0)
                    rate = (double) countPulses / dT;
                else
                    rate = FP::undefined();
            } else {
                rate = (double) countPulses / (double) countSeconds;
            }

            Variant::Root C(rate);
            remap("C", C);

            double Q = lastReportedFlow;
            if (!FP::defined(Q)) {
                if (FP::defined(config.first().flow))
                    Q = config.first().flow;
                else if (instrumentMeta["Model"].toString() == "3025")
                    Q = 0.5;
                else
                    Q = 0.3;
                Variant::Root v(Q);
                remap("Q", v);
                Q = v.read().toDouble();
            }

            Variant::Root N(calculateConcentration(C.read().toDouble(), Q));
            remap("N", N);

            logValue(startTime, endTime, LogStream_Counts, "N", std::move(N));
            realtimeValue(frameTime, "C", std::move(C));

            streamAdvance(LogStream_Counts, frameTime);
            break;
        }

        /* Require non-zero seconds from the instrument before acknowledging
         * the record (i.e. resetting timeouts) */
        return -1;

    case COMMAND_DC: {
        auto fields = Util::as_deque(frame.split(','));
        if (fields.size() != 2) return 2000;

        field = fields.front().string_trimmed();
        fields.pop_front();
        if (!isOnlyDigits(field)) return 2001;
        qint64 seconds = field.parse_i64(&ok, 10);
        if (!ok) return 2002;
        if (seconds < 0) return 2003;
        if (!INTEGER::defined(seconds)) return 2004;

        field = fields.front().string_trimmed();
        fields.pop_front();
        if (!isOnlyDigits(field)) return 2005;
        qint64 pulses = field.parse_i64(&ok, 10);
        if (!ok) return 2006;
        if (pulses < 0) return 2007;
        if (!INTEGER::defined(pulses)) return 2008;

        Q_ASSERT(fields.empty());

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            break;

        case RESP_INTERACTIVE_RUN_READ_COUNTS:
            if (seconds > 0) {
                if (controlStream != NULL) {
                    controlStream->writeControl("R1\r");
                }
                responseState = RESP_INTERACTIVE_RUN_READ_CONDENSERTEMP;
                commandQueue.append(Command(COMMAND_R1));
            } else {
                if (controlStream != NULL) {
                    controlStream->writeControl("DC\r");
                }
                commandQueue.append(Command(COMMAND_DC));
            }
            break;

        case RESP_INTERACTIVE_START_READ_COUNTS:
            if (seconds <= 0) {
                if (controlStream != NULL) {
                    controlStream->writeControl("DC\r");
                }
                commandQueue.append(Command(COMMAND_DC));
                return -1;
            }

            if (controlStream != NULL) {
                controlStream->writeControl("R1\r");
            }
            responseState = RESP_INTERACTIVE_START_READ_CONDENSERTEMP;
            commandQueue.append(Command(COMMAND_R1));

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadCondenser"), frameTime, FP::undefined()));
            }
            return 0;

        case RESP_PASSIVE_RUN:
            break;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("DC\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_COUNTS;
            commandQueue.append(Command(COMMAND_DC));
            break;

        default:
            return -1;
        }

        emitMetadata(frameTime);

        if (seconds > 0) {
            double startTime = streamTime[LogStream_Counts];
            double endTime = frameTime;
            double rate;
            if (config.first().useMeasuredTime && FP::defined(startTime) && FP::defined(endTime)) {
                double dT = endTime - startTime;
                if (dT > 0.0)
                    rate = (double) pulses / dT;
                else
                    rate = FP::undefined();
            } else {
                rate = (double) pulses / (double) seconds;
            }

            Variant::Root C(rate);
            remap("C", C);

            double Q = lastReportedFlow;
            if (!FP::defined(Q)) {
                if (FP::defined(config.first().flow))
                    Q = config.first().flow;
                else if (instrumentMeta["Model"].toString() == "3025")
                    Q = 0.5;
                else
                    Q = 0.3;
                Variant::Root v(Q);
                remap("Q", v);
                Q = v.read().toDouble();
            }

            Variant::Root N(calculateConcentration(C.read().toDouble(), Q));
            remap("N", N);

            logValue(startTime, endTime, LogStream_Counts, "N", std::move(N));
            realtimeValue(frameTime, "C", std::move(C));

            streamAdvance(LogStream_Counts, frameTime);
            break;
        }

        /* Require non-zero seconds from the instrument before acknowledging
         * the record (i.e. resetting timeouts) */
        return -3;
    }

    case COMMAND_I: {
        field = Util::ByteView(frame).string_trimmed();
        if (field == "ERROR")
            return 100;
        auto label = field.toString();
        if (instrumentMeta["CalibrationLabel"].toString() != label) {
            instrumentMeta["CalibrationLabel"].setString(std::move(label));
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_START_READ_I:
            if (controlStream != NULL) {
                controlStream->writeControl("C\r");
            }
            responseState = RESP_INTERACTIVE_START_DISAMBIGUATE_C;
            commandQueue.append(Command(COMMAND_C));

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveDisambiguateC"), frameTime, FP::undefined()));
            }
            break;

        case RESP_PASSIVE_RUN:
            break;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("DC\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_COUNTS;
            commandQueue.append(Command(COMMAND_DC));
            timeoutAt(frameTime + 2.0);
            break;

        default:
            return -1;
        }
        break;
    }

    case COMMAND_C: {
        field = Util::ByteView(frame).string_trimmed();
        if (field == "ERROR")
            return 101;

        commandQueue.prepend(command);
        discardData(frameTime + 0.5);

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_START_DISAMBIGUATE_C:
        case RESP_PASSIVE_RUN:
            break;

        default:
            return -1;
        }
        return 0;
    }

    case COMMAND_RC:
        field = Util::ByteView(frame).string_trimmed();
        if (field == "ERROR") {
            if (instrumentMeta["Model"].toString() != "3025") {
                instrumentMeta["Model"].setString("3025");
                haveEmittedLogMeta = false;
                haveEmittedRealtimeMeta = false;
                sourceMetadataUpdated();
            }

            switch (responseState) {
            case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
            case RESP_AUTOPROBE_PASSIVE_WAIT:
            case RESP_PASSIVE_WAIT:
                return -3;

            case RESP_INTERACTIVE_START_DISAMBIGUATE_RC:
                interactiveEstablished(frameTime);
                break;

            default:
                return -1;
            }
            return 0;
        }
        field.parse_real(&ok);
        if (!ok)
            return 102;

        if (instrumentMeta["Model"].toString() != "3022") {
            instrumentMeta["Model"].setString("3022");
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_START_DISAMBIGUATE_RC:
            interactiveEstablished(frameTime);
            break;

        default:
            return -1;
        }
        break;

    case COMMAND_R0:
        field = Util::ByteView(frame).string_trimmed();
        if (field == "FULL") {
            if (!liquidFull)
                forceRealtimeStateEmit = true;
            liquidFull = true;
        } else if (field == "NOTFULL") {
            if (liquidFull)
                forceRealtimeStateEmit = true;
            liquidFull = false;
        } else {
            return 103;
        }

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_RUN_READ_LIQUID:
            if (controlStream != NULL) {
                controlStream->writeControl("R5\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_STATUS;
            commandQueue.append(Command(COMMAND_R5));
            break;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("DC\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_COUNTS;
            commandQueue.append(Command(COMMAND_DC));
            timeoutAt(frameTime + 2.0);
            break;

        default:
            return -1;
        }
        break;

    case COMMAND_R1: {
        field = Util::ByteView(frame).string_trimmed();
        Variant::Root T2(field.parse_real(&ok));
        if (!ok) return 3000;
        if (!FP::defined(T2.read().toReal())) return 3001;
        remap("T2", T2);

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_RUN_READ_CONDENSERTEMP:
            if (controlStream != NULL) {
                controlStream->writeControl("R2\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_SATURATORTEMP;
            commandQueue.append(Command(COMMAND_R2));
            break;

        case RESP_INTERACTIVE_START_READ_CONDENSERTEMP:
            if (controlStream != NULL) {
                controlStream->writeControl("R2\r");
            }
            responseState = RESP_INTERACTIVE_START_READ_SATURATORTEMP;
            commandQueue.append(Command(COMMAND_R2));

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadSaturator"), frameTime, FP::undefined()));
            }
            return 0;

        case RESP_PASSIVE_RUN:
            break;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("DC\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_COUNTS;
            commandQueue.append(Command(COMMAND_DC));
            timeoutAt(frameTime + 2.0);
            break;

        default:
            return -1;
        }

        emitMetadata(frameTime);
        logValue(streamTime[LogStream_CondenserTemperature], frameTime,
                 LogStream_CondenserTemperature, "T2", std::move(T2));

        streamAdvance(LogStream_CondenserTemperature, frameTime);
        return 0;
    }

    case COMMAND_R2: {
        field = Util::ByteView(frame).string_trimmed();
        Variant::Root T1(field.parse_real(&ok));
        if (!ok) return 3000;
        if (!FP::defined(T1.read().toReal())) return 3001;
        remap("T1", T1);

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_RUN_READ_SATURATORTEMP:
            if (controlStream != NULL) {
                controlStream->writeControl("R3\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_OPTICSTEMP;
            commandQueue.append(Command(COMMAND_R3));
            break;

        case RESP_INTERACTIVE_START_READ_SATURATORTEMP:
            if (controlStream != NULL) {
                controlStream->writeControl("R3\r");
            }
            responseState = RESP_INTERACTIVE_START_READ_OPTICSTEMP;
            commandQueue.append(Command(COMMAND_R3));

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                        "StartInteractiveReadOptics"), frameTime, FP::undefined()));
            }
            return 0;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("DC\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_COUNTS;
            commandQueue.append(Command(COMMAND_DC));
            timeoutAt(frameTime + 2.0);
            break;

        case RESP_PASSIVE_RUN:
            break;

        default:
            return -1;
        }

        emitMetadata(frameTime);
        logValue(streamTime[LogStream_SaturatorTemperature], frameTime,
                 LogStream_SaturatorTemperature, "T1", std::move(T1));

        streamAdvance(LogStream_SaturatorTemperature, frameTime);
        return 0;
    }

    case COMMAND_R3: {
        field = Util::ByteView(frame).string_trimmed();
        Variant::Root T3(field.parse_real(&ok));
        if (!ok) return 4000;
        if (!FP::defined(T3.read().toReal())) return 4001;
        remap("T3", T3);

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_RUN_READ_OPTICSTEMP:
            if (controlStream != NULL) {
                controlStream->writeControl("R4\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_FLOW;
            commandQueue.append(Command(COMMAND_R4));
            break;

        case RESP_INTERACTIVE_START_READ_OPTICSTEMP:
            if (controlStream != NULL) {
                controlStream->writeControl("I\r");
            }
            responseState = RESP_INTERACTIVE_START_READ_I;
            commandQueue.append(Command(COMMAND_I));

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractiveReadI"),
                                      frameTime, FP::undefined()));
            }
            return 0;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("DC\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_COUNTS;
            commandQueue.append(Command(COMMAND_DC));
            timeoutAt(frameTime + 2.0);
            break;

        case RESP_PASSIVE_RUN:
            break;

        default:
            return -1;
        }

        emitMetadata(frameTime);
        logValue(streamTime[LogStream_OpticsTemperature], frameTime, LogStream_OpticsTemperature,
                 "T3", std::move(T3));

        streamAdvance(LogStream_OpticsTemperature, frameTime);
        return 0;
    }

    case COMMAND_R4: {
        field = Util::ByteView(frame).string_trimmed();
        double value = field.parse_real(&ok);
        if (!ok) return 4000;
        if (!FP::defined(value)) return 4001;
        Variant::Root Q(value * (60.0 / 1000.0));
        Variant::Root ZQ = Q;
        remap("ZQ", ZQ);
        if (FP::defined(config.first().flow))
            Q.write().setDouble(config.first().flow);
        remap("Q", Q);
        lastReportedFlow = Q.read().toDouble();

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_RUN_READ_FLOW:
            if (controlStream != NULL) {
                controlStream->writeControl("RD\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_DISPLAY;
            commandQueue.append(Command(COMMAND_RD));
            break;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("DC\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_COUNTS;
            commandQueue.append(Command(COMMAND_DC));
            timeoutAt(frameTime + 2.0);
            break;

        case RESP_PASSIVE_RUN:
            break;

        default:
            return -1;
        }

        emitMetadata(frameTime);
        logValue(streamTime[LogStream_Flow], frameTime, LogStream_Flow, "Q", std::move(Q));
        realtimeValue(frameTime, "ZQ", std::move(ZQ));

        streamAdvance(LogStream_Flow, frameTime);
        return 0;
    }

    case COMMAND_R5:
        field = Util::ByteView(frame).string_trimmed();
        if (field == "READY") {
            if (!instrumentReady)
                forceRealtimeStateEmit = true;
            instrumentReady = true;
        } else if (field == "NOTREADY") {
            if (instrumentReady)
                forceRealtimeStateEmit = true;
            instrumentReady = false;
        } else {
            return 104;
        }

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_RUN_READ_STATUS:
            if (controlStream != NULL) {
                controlStream->writeControl("DC\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_COUNTS;
            commandQueue.append(Command(COMMAND_DC));
            break;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("DC\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_COUNTS;
            commandQueue.append(Command(COMMAND_DC));
            timeoutAt(frameTime + 2.0);
            break;

        default:
            return -1;
        }
        break;

    case COMMAND_RD: {
        field = Util::ByteView(frame).string_trimmed();
        Variant::Root ZND(field.parse_real(&ok));
        if (!ok) return 5000;
        if (!FP::defined(ZND.read().toReal())) return 5001;
        remap("ZND", ZND);

        switch (responseState) {
        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -3;

        case RESP_INTERACTIVE_RUN_READ_DISPLAY:
            if (controlStream != NULL) {
                controlStream->writeControl("R0\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_LIQUID;
            commandQueue.append(Command(COMMAND_R0));
            break;

        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            if (controlStream != NULL) {
                controlStream->writeControl("DC\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_COUNTS;
            commandQueue.append(Command(COMMAND_DC));
            timeoutAt(frameTime + 2.0);
            break;

        default:
            return -1;
        }

        realtimeValue(frameTime, "ZND", std::move(ZND));
        break;
    }

    case COMMAND_OK:
        if (frame != "OK")
            return 3;

        switch (responseState) {
        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            if (!externalCommandQueue.empty())
                break;
            /* Fall through */
        case RESP_INTERACTIVE_RUN_READ_COUNTS:
        case RESP_INTERACTIVE_RUN_READ_CONDENSERTEMP:
        case RESP_INTERACTIVE_RUN_READ_SATURATORTEMP:
        case RESP_INTERACTIVE_RUN_READ_OPTICSTEMP:
        case RESP_INTERACTIVE_RUN_READ_FLOW:
        case RESP_INTERACTIVE_RUN_READ_DISPLAY:
        case RESP_INTERACTIVE_RUN_READ_LIQUID:
        case RESP_INTERACTIVE_RUN_READ_STATUS:
            /* In all interactive states, go back to reading counts */
            if (controlStream != NULL) {
                controlStream->writeControl("DC\r");
            }
            responseState = RESP_INTERACTIVE_RUN_READ_COUNTS;
            commandQueue.append(Command(COMMAND_DC));
            timeoutAt(frameTime + 2.0);
            break;

        case RESP_INTERACTIVE_START_READ_COUNTS:
        case RESP_INTERACTIVE_START_READ_CONDENSERTEMP:
        case RESP_INTERACTIVE_START_READ_SATURATORTEMP:
        case RESP_INTERACTIVE_START_READ_OPTICSTEMP:
        case RESP_INTERACTIVE_START_READ_I:
        case RESP_INTERACTIVE_START_DISAMBIGUATE_C:
        case RESP_INTERACTIVE_START_DISAMBIGUATE_RV:
        case RESP_INTERACTIVE_START_DISAMBIGUATE_RC:
            return 4;

        case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        case RESP_AUTOPROBE_PASSIVE_WAIT:
        case RESP_PASSIVE_WAIT:
            return -1;

        default:
            break;
        }

        break;
    }

    if (!externalCommandQueue.empty()) {
        switch (responseState) {
        case RESP_INTERACTIVE_RUN_READ_COUNTS:
        case RESP_INTERACTIVE_RUN_READ_CONDENSERTEMP:
        case RESP_INTERACTIVE_RUN_READ_SATURATORTEMP:
        case RESP_INTERACTIVE_RUN_READ_OPTICSTEMP:
        case RESP_INTERACTIVE_RUN_READ_FLOW:
        case RESP_INTERACTIVE_RUN_READ_DISPLAY:
        case RESP_INTERACTIVE_RUN_READ_LIQUID:
        case RESP_INTERACTIVE_RUN_READ_STATUS:
        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND: {
            auto issue = std::move(externalCommandQueue.front());
            externalCommandQueue.pop_front();

            responseState = RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND;
            incomingControlFrame(issue, frameTime);

            if (controlStream) {
                issue += "\r";
                controlStream->writeControl(std::move(issue));
            }

            timeoutAt(frameTime + 2.0);
            break;
        }

        default:
            break;
        }
    }

    if (forceRealtimeStateEmit && realtimeEgress != NULL) {
        forceRealtimeStateEmit = false;
        if (liquidFull && instrumentReady) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                  FP::undefined()));
        } else {
            QStringList conditions;
            if (!liquidFull)
                conditions.append(QObject::tr("Low butanol"));
            if (!instrumentReady)
                conditions.append(QObject::tr("Instrument fault"));
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    QObject::tr("Abnormal status: %1").arg(conditions.join(", "))), frameTime,
                                                       FP::undefined()));
        }
    }

    return 0;
}

void AcquireTSICPC302X::invalidateLogValues(double frameTime)
{
    for (int i = 0; i < LogStream_TOTAL; i++) {
        streamTime[i] = FP::undefined();
        streamAge[i] = 0;

        if (loggingEgress != NULL)
            loggingMux.advance(i, frameTime, loggingEgress);
    }
    loggingMux.clear();
    loggingLost(frameTime);
}

void AcquireTSICPC302X::incomingDataFrame(const Util::ByteArray &frame, double frameTime)
{
    if (!FP::defined(frameTime))
        return;
    configurationAdvance(frameTime);

    Command command;
    if (!commandQueue.isEmpty())
        command = commandQueue.first();

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        int code = processResponse(frame, frameTime);
        if (code == 0) {
            if (++autoprobePassiveValidRecords > 10) {
                qCDebug(log) << "Passive autoprobe succeeded at" << Logging::time(frameTime);

                responseState = RESP_PASSIVE_RUN;

                {
                    std::lock_guard<std::mutex> lock(mutex);
                    autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
                }
                autoprobeStatusUpdated();
                generalStatusUpdated();

                timeoutAt(frameTime + 2.0);

                forceRealtimeStateEmit = true;

                Variant::Write info = Variant::Write::empty();
                describeState(info, command);
                info.hash("ResponseState").setString("PassiveAutoprobeWait");
                event(frameTime, QObject::tr("Passive autoprobe succeeded."), false, info);

                if (realtimeEgress != NULL) {
                    realtimeEgress->incomingData(
                            SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime, FP::undefined()));
                }
            }
        } else if (code > 0) {
            qCDebug(log) << "Passive autoprobe failed at" << Logging::time(frameTime) << ":"
                        << frame << "rejected with code" << code;

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();

            responseState = RESP_PASSIVE_WAIT;
            autoprobePassiveValidRecords = 0;

            invalidateLogValues(frameTime);
        } else if (code == -1) {
            autoprobePassiveValidRecords = 0;
        }
        break;
    }

    case RESP_PASSIVE_WAIT:
        if (processResponse(frame, frameTime) == 0) {
            qCDebug(log) << "Passive comms established at" << Logging::time(frameTime);
            responseState = RESP_PASSIVE_RUN;
            generalStatusUpdated();

            timeoutAt(frameTime + 2.0);

            Variant::Write info = Variant::Write::empty();
            describeState(info, command);
            info.hash("ResponseState").setString("PassiveWait");
            event(frameTime, QObject::tr("Passive communications established."), false, info);

            forceRealtimeStateEmit = true;

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Run"), frameTime,
                                      FP::undefined()));
            }
        } else {
            invalidateLogValues(frameTime);
        }
        break;

    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RUN_READ_COUNTS:
    case RESP_INTERACTIVE_RUN_READ_CONDENSERTEMP:
    case RESP_INTERACTIVE_RUN_READ_SATURATORTEMP:
    case RESP_INTERACTIVE_RUN_READ_OPTICSTEMP:
    case RESP_INTERACTIVE_RUN_READ_FLOW:
    case RESP_INTERACTIVE_RUN_READ_DISPLAY:
    case RESP_INTERACTIVE_RUN_READ_LIQUID:
    case RESP_INTERACTIVE_RUN_READ_STATUS:
    case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND: {
        int code = processResponse(frame, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + 2.0);
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected with code" << code;

            Variant::Write info = Variant::Write::empty();
            describeState(info, command);
            if (responseState == RESP_PASSIVE_RUN) {
                timeoutAt(FP::undefined());
                responseState = RESP_PASSIVE_WAIT;
            } else {
                if (controlStream != NULL) {
                    controlStream->writeControl("DC\r");
                }
                responseState = RESP_INTERACTIVE_START_READ_COUNTS;
            }
            generalStatusUpdated();

            info.hash("Code").setInt64(code);
            info.hash("Line").setString(frame.toString());
            event(frameTime,
                  QObject::tr("Invalid line received (code %1).  Communications dropped.").arg(
                          code), true, info);

            invalidateLogValues(frameTime);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }
        } /* Ignored line */
        break;
    }

    case RESP_INTERACTIVE_START_READ_COUNTS:
    case RESP_INTERACTIVE_START_READ_CONDENSERTEMP:
    case RESP_INTERACTIVE_START_READ_SATURATORTEMP:
    case RESP_INTERACTIVE_START_READ_OPTICSTEMP:
    case RESP_INTERACTIVE_START_READ_I:
    case RESP_INTERACTIVE_START_DISAMBIGUATE_C:
    case RESP_INTERACTIVE_START_DISAMBIGUATE_RC:
    case RESP_INTERACTIVE_START_DISAMBIGUATE_RV: {
        int code = processResponse(frame, frameTime);
        if (code == 0) {
            timeoutAt(frameTime + 2.0);
        } else if (code > 0) {
            qCDebug(log) << "Line at" << Logging::time(frameTime) << ":" << frame
                         << "rejected during start communications with code" << code;

            responseState = RESP_INTERACTIVE_RESTART_WAIT;
            if (controlStream != NULL)
                controlStream->resetControl();
            discardData(frameTime + 10.0);
            timeoutAt(frameTime + 30.0);
            generalStatusUpdated();

            invalidateLogValues(frameTime);

            if (realtimeEgress != NULL) {
                realtimeEgress->incomingData(
                        SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("InvalidResponse"),
                                      frameTime, FP::undefined()));
            }

            {
                std::lock_guard<std::mutex> lock(mutex);
                autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
            }
            autoprobeStatusUpdated();
        } /* Ignored line */
        break;
    }

    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        break;
    }
}

void AcquireTSICPC302X::incomingInstrumentTimeout(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);

    switch (responseState) {
    case RESP_AUTOPROBE_PASSIVE_WAIT: {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
    }
        autoprobeStatusUpdated();
        /* Fall through */
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
        responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
        /* Fall through */
    case RESP_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
        commandQueue.clear();
        invalidateLogValues(frameTime);
        break;

    case RESP_PASSIVE_RUN:
        qCDebug(log) << "Timeout in passive mode at" << Logging::time(frameTime);
        timeoutAt(FP::undefined());

        {
            Variant::Write info = Variant::Write::empty();
            describeState(info);
            event(frameTime, QObject::tr("Timeout waiting for response.  Communications Dropped."),
                  true, info);
        }

        commandQueue.clear();
        responseState = RESP_PASSIVE_WAIT;
        generalStatusUpdated();
        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        invalidateLogValues(frameTime);
        break;

    case RESP_INTERACTIVE_RUN_READ_COUNTS:
    case RESP_INTERACTIVE_RUN_READ_CONDENSERTEMP:
    case RESP_INTERACTIVE_RUN_READ_SATURATORTEMP:
    case RESP_INTERACTIVE_RUN_READ_OPTICSTEMP:
    case RESP_INTERACTIVE_RUN_READ_FLOW:
    case RESP_INTERACTIVE_RUN_READ_DISPLAY:
    case RESP_INTERACTIVE_RUN_READ_LIQUID:
    case RESP_INTERACTIVE_RUN_READ_STATUS:
    case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
        qCDebug(log) << "Timeout in interactive state" << responseState << "at"
                     << Logging::time(frameTime);

        {
            Variant::Write info = Variant::Write::empty();
            describeState(info);
            event(frameTime, QObject::tr("Timeout waiting for response.  Communications Dropped."),
                  true, info);
        }

        commandQueue.clear();
        if (controlStream != NULL) {
            controlStream->writeControl("DC\r");
        }
        responseState = RESP_INTERACTIVE_START_READ_COUNTS;
        commandQueue.append(Command(COMMAND_DC));

        timeoutAt(frameTime + 2.0);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }
        invalidateLogValues(frameTime);
        break;


    case RESP_INTERACTIVE_START_READ_COUNTS:
    case RESP_INTERACTIVE_START_READ_CONDENSERTEMP:
    case RESP_INTERACTIVE_START_READ_SATURATORTEMP:
    case RESP_INTERACTIVE_START_READ_OPTICSTEMP:
    case RESP_INTERACTIVE_START_READ_I:
    case RESP_INTERACTIVE_START_DISAMBIGUATE_C:
        qCDebug(log) << "Timeout in interactive start state" << responseState << "at"
                     << Logging::time(frameTime);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("Timeout"), frameTime,
                                  FP::undefined()));
        }

        commandQueue.clear();
        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        if (controlStream != NULL)
            controlStream->resetControl();
        timeoutAt(FP::undefined());
        discardData(frameTime + 10.0);
        invalidateLogValues(frameTime);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Failure;
        }
        autoprobeStatusUpdated();
        break;

        /* Timeouts in these are acceptable (though they should return "ERROR") */
    case RESP_INTERACTIVE_START_DISAMBIGUATE_RV:
        commandQueue.clear();

        if (controlStream != NULL) {
            controlStream->writeControl("RC\r");
        }
        responseState = RESP_INTERACTIVE_START_DISAMBIGUATE_RC;
        commandQueue.append(Command(COMMAND_RC));
        timeoutAt(frameTime + 2.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveDisambiguateRC"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_DISAMBIGUATE_RC:
        commandQueue.clear();

        if (instrumentMeta["Model"].toString() != "3025") {
            instrumentMeta["Model"].setString("3025");
            haveEmittedLogMeta = false;
            haveEmittedRealtimeMeta = false;
            sourceMetadataUpdated();
        }

        interactiveEstablished(frameTime);
        break;

    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive start up beginning at" << Logging::time(frameTime);

        commandQueue.clear();
        timeoutAt(frameTime + 30.0);
        discardData(frameTime + 0.5);
        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        invalidateLogValues(frameTime);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"),
                                  frameTime, FP::undefined()));
        }
        break;
    }
}

void AcquireTSICPC302X::discardDataCompleted(double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);

    switch (responseState) {
    case RESP_INTERACTIVE_RESTART_WAIT:
        commandQueue.clear();
        if (controlStream != NULL) {
            controlStream->writeControl("DC\r");
        }
        responseState = RESP_INTERACTIVE_START_READ_COUNTS;
        commandQueue.append(Command(COMMAND_DC));
        invalidateLogValues(frameTime);

        timeoutAt(frameTime + 2.0);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"},
                                                       Variant::Root("StartInteractiveReadCounts"),
                                                       frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_START_DISAMBIGUATE_C:
        commandQueue.clear();

        if (controlStream != NULL) {
            controlStream->writeControl("RV\r");
        }
        responseState = RESP_INTERACTIVE_START_DISAMBIGUATE_RV;
        commandQueue.append(Command(COMMAND_ERROR));
        timeoutAt(frameTime + 2.0);

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root(
                    "StartInteractiveDisambiguateRV"), frameTime, FP::undefined()));
        }
        break;

    case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
    case RESP_INTERACTIVE_RUN_READ_COUNTS:
    case RESP_INTERACTIVE_RUN_READ_CONDENSERTEMP:
    case RESP_INTERACTIVE_RUN_READ_SATURATORTEMP:
    case RESP_INTERACTIVE_RUN_READ_OPTICSTEMP:
    case RESP_INTERACTIVE_RUN_READ_FLOW:
    case RESP_INTERACTIVE_RUN_READ_DISPLAY:
    case RESP_INTERACTIVE_RUN_READ_LIQUID:
    case RESP_INTERACTIVE_RUN_READ_STATUS:
        if (commandQueue.isEmpty())
            break;
        if (commandQueue.first().getType() != COMMAND_C)
            break;
        commandQueue.removeFirst();

        if (controlStream != NULL) {
            controlStream->writeControl("DC\r");
        }
        responseState = RESP_INTERACTIVE_RUN_READ_COUNTS;
        commandQueue.append(Command(COMMAND_DC));
        timeoutAt(frameTime + 2.0);
        break;

    default:
        if (commandQueue.isEmpty())
            break;
        if (commandQueue.first().getType() != COMMAND_C)
            break;
        commandQueue.removeFirst();
        break;
    }
}


void AcquireTSICPC302X::incomingControlFrame(const Util::ByteArray &frame, double frameTime)
{
    Q_ASSERT(FP::defined(frameTime));
    configurationAdvance(frameTime);

    if (frame == "D") {
        commandQueue.append(Command(COMMAND_D));
    } else if (frame == "DC") {
        commandQueue.append(Command(COMMAND_DC));
    } else if (frame == "R0") {
        commandQueue.append(Command(COMMAND_R0));
    } else if (frame == "R1") {
        commandQueue.append(Command(COMMAND_R1));
    } else if (frame == "R2") {
        commandQueue.append(Command(COMMAND_R2));
    } else if (frame == "R3") {
        commandQueue.append(Command(COMMAND_R3));
    } else if (frame == "R4") {
        commandQueue.append(Command(COMMAND_R4));
    } else if (frame == "R5") {
        commandQueue.append(Command(COMMAND_R5));
    } else if (frame == "RA") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame == "RB") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame == "RC") {
        commandQueue.append(Command(COMMAND_IGNORED));
    } else if (frame == "RD") {
        commandQueue.append(Command(COMMAND_RD));
    } else if (frame == "RE") {
        commandQueue.append(Command(COMMAND_RD));
    } else if (frame.string_start("V")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame.string_start("W")) {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame.string_start("C")) {
        commandQueue.append(Command(COMMAND_C));
    } else if (frame == "X0") {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "X1") {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "X2") {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "X3") {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "X4") {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "X5") {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "X6") {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "X7") {
        commandQueue.append(Command(COMMAND_OK));
    } else if (frame == "X8") {
        commandQueue.append(Command(COMMAND_OK));
    }
}

Variant::Root AcquireTSICPC302X::getSourceMetadata()
{
    PauseLock paused(*this);
    return instrumentMeta;
}

AcquisitionInterface::GeneralStatus AcquireTSICPC302X::getGeneralStatus()
{
    PauseLock paused(*this);
    switch (responseState) {
    case RESP_PASSIVE_RUN:
    case RESP_INTERACTIVE_RUN_READ_COUNTS:
    case RESP_INTERACTIVE_RUN_READ_CONDENSERTEMP:
    case RESP_INTERACTIVE_RUN_READ_SATURATORTEMP:
    case RESP_INTERACTIVE_RUN_READ_OPTICSTEMP:
    case RESP_INTERACTIVE_RUN_READ_FLOW:
    case RESP_INTERACTIVE_RUN_READ_DISPLAY:
    case RESP_INTERACTIVE_RUN_READ_LIQUID:
    case RESP_INTERACTIVE_RUN_READ_STATUS:
    case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
        return AcquisitionInterface::GeneralStatus::Normal;
    default:
        return AcquisitionInterface::GeneralStatus::NoCommunications;
    }
}

void AcquireTSICPC302X::command(const Variant::Read &command)
{
    if (command.hash("Fill").exists()) {
        switch (responseState) {
        case RESP_INTERACTIVE_RUN_READ_COUNTS:
        case RESP_INTERACTIVE_RUN_READ_CONDENSERTEMP:
        case RESP_INTERACTIVE_RUN_READ_SATURATORTEMP:
        case RESP_INTERACTIVE_RUN_READ_OPTICSTEMP:
        case RESP_INTERACTIVE_RUN_READ_FLOW:
        case RESP_INTERACTIVE_RUN_READ_DISPLAY:
        case RESP_INTERACTIVE_RUN_READ_LIQUID:
        case RESP_INTERACTIVE_RUN_READ_STATUS:
        case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
            qCDebug(log) << "Queuing fill command in interactive state" << responseState;
            externalCommandQueue.emplace_back("X5");
            break;
        default:
            qCDebug(log) << "Ignored fill command in state" << responseState;
            break;
        }
    }
}

Variant::Root AcquireTSICPC302X::getCommands()
{
    Variant::Root result;

    result["Fill"].hash("DisplayName").setString("Execute &Fill");
    result["Fill"].hash("ToolTip")
                  .setString(
                          "Execute a fill command as if the fill button was pressed on the instrument.");
    result["Fill"].hash("Confirm").setString("Execute fill command on CPC?");

    return result;
}

AcquisitionInterface::AutoprobeStatus AcquireTSICPC302X::getAutoprobeStatus()
{
    std::lock_guard<std::mutex> lock(mutex);
    return autoprobeStatus;
}

void AcquireTSICPC302X::autoprobeTryInteractive(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_READ_COUNTS:
    case RESP_INTERACTIVE_RUN_READ_CONDENSERTEMP:
    case RESP_INTERACTIVE_RUN_READ_SATURATORTEMP:
    case RESP_INTERACTIVE_RUN_READ_OPTICSTEMP:
    case RESP_INTERACTIVE_RUN_READ_FLOW:
    case RESP_INTERACTIVE_RUN_READ_DISPLAY:
    case RESP_INTERACTIVE_RUN_READ_LIQUID:
    case RESP_INTERACTIVE_RUN_READ_STATUS:
    case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
        /* Already running/started, do nothing */
        qCDebug(log) << "Interactive autoprobe requested while already in interactive state"
                     << responseState << "at" << Logging::time(time);

        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::Success;
        }
        autoprobeStatusUpdated();
        break;

    case RESP_INTERACTIVE_START_READ_COUNTS:
    case RESP_INTERACTIVE_START_READ_CONDENSERTEMP:
    case RESP_INTERACTIVE_START_READ_SATURATORTEMP:
    case RESP_INTERACTIVE_START_READ_OPTICSTEMP:
    case RESP_INTERACTIVE_START_READ_I:
    case RESP_INTERACTIVE_START_DISAMBIGUATE_C:
    case RESP_INTERACTIVE_START_DISAMBIGUATE_RC:
    case RESP_INTERACTIVE_START_DISAMBIGUATE_RV:
    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Interactive autoprobe started from state" << responseState << "at"
                     << Logging::time(time);

        /* Reset this if needed, so we don't report success until we can
         * finish interrogating the instrument. */
        {
            std::lock_guard<std::mutex> lock(mutex);
            autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
        }
        autoprobeStatusUpdated();

        commandQueue.clear();
        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        discardData(time + 0.5);
        timeoutAt(time + 30.0);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireTSICPC302X::autoprobeResetPassive(double time)
{
    PauseLock paused(*this);

    qCDebug(log) << "Reset to passive autoprobe from state" << responseState << "at"
                 << Logging::time(time);

    {
        std::lock_guard<std::mutex> lock(mutex);
        autoprobeStatus = AcquisitionInterface::AutoprobeStatus::InProgress;
    }
    autoprobeStatusUpdated();

    responseState = RESP_AUTOPROBE_PASSIVE_WAIT;
    autoprobePassiveValidRecords = 0;
    commandQueue.clear();
    discardData(time + 0.5, 1);
    timeoutAt(time + 15.0);
    generalStatusUpdated();
}

void AcquireTSICPC302X::autoprobePromote(double time)
{
    PauseLock paused(*this);

    switch (responseState) {
    case RESP_INTERACTIVE_RUN_READ_COUNTS:
    case RESP_INTERACTIVE_RUN_READ_CONDENSERTEMP:
    case RESP_INTERACTIVE_RUN_READ_SATURATORTEMP:
    case RESP_INTERACTIVE_RUN_READ_OPTICSTEMP:
    case RESP_INTERACTIVE_RUN_READ_FLOW:
    case RESP_INTERACTIVE_RUN_READ_DISPLAY:
    case RESP_INTERACTIVE_RUN_READ_LIQUID:
    case RESP_INTERACTIVE_RUN_READ_STATUS:
    case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
    case RESP_INTERACTIVE_START_READ_COUNTS:
    case RESP_INTERACTIVE_START_READ_CONDENSERTEMP:
    case RESP_INTERACTIVE_START_READ_SATURATORTEMP:
    case RESP_INTERACTIVE_START_READ_OPTICSTEMP:
    case RESP_INTERACTIVE_START_READ_I:
    case RESP_INTERACTIVE_START_DISAMBIGUATE_C:
    case RESP_INTERACTIVE_START_DISAMBIGUATE_RV:
    case RESP_INTERACTIVE_START_DISAMBIGUATE_RC:
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        timeoutAt(time + 2.0);
        break;

    case RESP_PASSIVE_RUN:
    case RESP_PASSIVE_WAIT:
    case RESP_AUTOPROBE_PASSIVE_INITIALIZE:
    case RESP_AUTOPROBE_PASSIVE_WAIT:
    case RESP_INTERACTIVE_RESTART_WAIT:
    case RESP_INTERACTIVE_INITIALIZE:
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to interactive acquisition at" << Logging::time(time);

        commandQueue.clear();
        responseState = RESP_INTERACTIVE_RESTART_WAIT;
        discardData(time + 0.5);
        timeoutAt(time + 30.0);
        generalStatusUpdated();

        if (realtimeEgress != NULL) {
            realtimeEgress->incomingData(
                    SequenceValue({{}, "raw", "ZSTATE"}, Variant::Root("StartInteractive"), time,
                                  FP::undefined()));
        }
        break;
    }
}

void AcquireTSICPC302X::autoprobePromotePassive(double time)
{
    PauseLock paused(*this);

    timeoutAt(time + 2.0);

    switch (responseState) {
    case RESP_PASSIVE_WAIT:
    case RESP_PASSIVE_RUN:
        /* Already in passive, so do nothing */
        qCDebug(log) << "Promoted from passive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);
        break;

    case RESP_INTERACTIVE_RUN_READ_COUNTS:
    case RESP_INTERACTIVE_RUN_READ_CONDENSERTEMP:
    case RESP_INTERACTIVE_RUN_READ_SATURATORTEMP:
    case RESP_INTERACTIVE_RUN_READ_OPTICSTEMP:
    case RESP_INTERACTIVE_RUN_READ_DISPLAY:
    case RESP_INTERACTIVE_RUN_READ_LIQUID:
    case RESP_INTERACTIVE_RUN_READ_STATUS:
    case RESP_INTERACTIVE_RUN_EXTERNAL_COMMAND:
        qCDebug(log) << "Promoted from interactive state" << responseState
                     << "to passive acquisition at" << Logging::time(time);

        responseState = RESP_PASSIVE_RUN;
        break;

    default:
        qCDebug(log) << "Promoted from state" << responseState << "to passive acquisition at"
                     << Logging::time(time);

        responseState = RESP_PASSIVE_WAIT;
        break;
    }
}

AcquisitionInterface::AutomaticDefaults AcquireTSICPC302X::getDefaults()
{
    AutomaticDefaults result;
    result.name = "N$1$2";
    result.interface["Baud"].setInt64(9600);
    result.interface["Parity"].setString("Even");
    result.interface["DataBits"].setInt64(7);
    result.interface["StopBits"].setInt64(1);
    return result;
}


ComponentOptions AcquireTSICPC302XComponent::getOptions()
{
    ComponentOptions options(getBaseOptions());
    LineIngressWrapper::addOptions(options);
    return options;
}

ComponentOptions AcquireTSICPC302XComponent::getPassiveOptions()
{
    ComponentOptions options(getBaseOptions());

    return options;
}

QList<ComponentExample> AcquireTSICPC302XComponent::getExamples()
{
    QList<ComponentExample> examples(LineIngressWrapper::standardExamples());
    examples.append(getPassiveExamples());
    return examples;
}

bool AcquireTSICPC302XComponent::requiresInputDevice()
{ return true; }

ExternalConverter *AcquireTSICPC302XComponent::createDataIngress(const ComponentOptions &options)
{
    return LineIngressWrapper::create(createAcquisitionPassive(options), options);
}

QList<ComponentExample> AcquireTSICPC302XComponent::getPassiveExamples()
{
    QList<ComponentExample> examples;

    ComponentOptions options = getPassiveOptions();
    examples.append(ComponentExample(options, tr("Convert data with the default flow rate.")));

    options = getBaseOptions();
    (qobject_cast<ComponentOptionSingleDouble *>(options.get("q")))->set(0.032);
    examples.append(ComponentExample(options, tr("Explicitly set flow rate.")));

    return examples;
}

std::unique_ptr<
        AcquisitionInterface> AcquireTSICPC302XComponent::createAcquisitionPassive(const ComponentOptions &options,
                                                                                   const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireTSICPC302X(options, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireTSICPC302XComponent::createAcquisitionPassive(const ValueSegment::Transfer &config,
                                                                                   const std::string &loggingContext)
{ return std::unique_ptr<AcquisitionInterface>(new AcquireTSICPC302X(config, loggingContext)); }

std::unique_ptr<
        AcquisitionInterface> AcquireTSICPC302XComponent::createAcquisitionAutoprobe(const ValueSegment::Transfer &config,
                                                                                     const std::string &loggingContext)
{
    std::unique_ptr<AcquireTSICPC302X> i(new AcquireTSICPC302X(config, loggingContext));
    i->setToAutoprobe();
    return std::move(i);
}

std::unique_ptr<
        AcquisitionInterface> AcquireTSICPC302XComponent::createAcquisitionInteractive(const ValueSegment::Transfer &config,
                                                                                       const std::string &loggingContext)
{
    std::unique_ptr<AcquireTSICPC302X> i(new AcquireTSICPC302X(config, loggingContext));
    i->setToInteractive();
    return std::move(i);
}
