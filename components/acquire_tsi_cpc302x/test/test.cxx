/*
 * Copyright (c) 2019 University of Colorado
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 *
 * Author: Derek Hageman <Derek.Hageman@noaa.gov>
 */

#include "core/first.hxx"

#include <mutex>
#include <QTest>
#include <QtGlobal>
#include <QObject>
#include <QSignalSpy>

#include "datacore/stream.hxx"
#include "datacore/externalsource.hxx"
#include "acquisition/acquisitioncomponent.hxx"
#include "core/component.hxx"
#include "core/range.hxx"
#include "core/waitutils.hxx"
#include "core/qtcompat.hxx"

#include "acquire_test_control.hxx"
#include "acquire_test_capture.hxx"

using namespace CPD3;
using namespace CPD3::Data;
using namespace CPD3::Acquisition;

class ModelInstrument {
public:
    QByteArray incoming;
    QByteArray outgoing;
    double accumulatorRemaining;
    int countSum;
    int countSeconds;

    int countRate;
    bool liquidOk;
    bool laserOk;
    double Tcondenser;
    double Tsaturator;
    double Toptics;
    double flow;

    bool enableUnpolled;
    enum {
        Mode_3022, Mode_3025, Mode_3010,
    } mode;

    ModelInstrument()
            : incoming(), outgoing(), accumulatorRemaining(0.0), countSum(0), countSeconds(0)
    {
        countRate = 6000;
        liquidOk = true;
        laserOk = true;
        Tcondenser = 25.0;
        Tsaturator = 35.0;
        Toptics = 27.1;
        flow = 0.3;

        enableUnpolled = false;
        mode = Mode_3022;
    }

    void writeD()
    {
        outgoing.append(QByteArray::number(countSeconds));
        outgoing.append('\r');
        outgoing.append(QByteArray::number(countSum));
        outgoing.append(",0\r");
        for (int i = 0; i < 15; i++) {
            outgoing.append("0,0\r");
        }

        countSum = 0;
        countSeconds = 0;
    }

    double concentration() const
    {
        return (double) countRate / (flow * (1000.0 / 60.0));
    }

    void advance(double seconds)
    {
        int idxCR = -1;
        while ((idxCR = incoming.indexOf('\r')) >= 0) {
            QByteArray line(incoming.mid(0, idxCR));

            if (line == "D") {
                writeD();
            } else if (line == "DC") {
                outgoing.append(QByteArray::number(countSeconds));
                outgoing.append(',');
                outgoing.append(QByteArray::number(countSum));
                outgoing.append('\r');
                countSum = 0;
                countSeconds = 0;
            } else if (line == "R0") {
                if (liquidOk)
                    outgoing.append("FULL\r");
                else
                    outgoing.append("NOTFULL\r");
            } else if (line == "R1") {
                outgoing.append(QByteArray::number(Tcondenser, 'f', 1));
                outgoing.append('\r');
            } else if (line == "R2") {
                outgoing.append(QByteArray::number(Tsaturator, 'f', 1));
                outgoing.append('\r');
            } else if (line == "R3") {
                outgoing.append(QByteArray::number(Toptics, 'f', 1));
                outgoing.append('\r');
            } else if (line == "R4") {
                outgoing.append(QByteArray::number(flow * (1000.0 / 60), 'f', 1));
                outgoing.append('\r');
            } else if (line == "R5") {
                if (liquidOk && laserOk)
                    outgoing.append("READY\r");
                else
                    outgoing.append("NOTREADY\r");
            } else if (line == "R6") {
                outgoing.append(QByteArray::number(2.0, 'f', 3));
                outgoing.append('\r');
            } else if (line == "R7") {
                outgoing.append(QByteArray::number(2.345, 'f', 3));
                outgoing.append('\r');
            } else if (line == "R8") {
                outgoing.append(QByteArray::number(countRate * 200));
                outgoing.append('\r');
            } else if (line == "R9") {
                outgoing.append(QByteArray::number(countRate * 20));
                outgoing.append('\r');
            } else if (line == "RA") {
                outgoing.append(QByteArray::number(countRate * 2));
                outgoing.append('\r');
            } else if (line == "RB") {
                outgoing.append(QByteArray::number(countRate));
                outgoing.append('\r');
            } else if (line == "RC" && mode == Mode_3022) {
                outgoing.append(QByteArray::number(concentration(), 'f', 0));
                outgoing.append('\r');
            } else if (line == "RD") {
                outgoing.append(QByteArray::number(concentration(), 'f', 1));
                outgoing.append('\r');
            } else if (line == "RE") {
                outgoing.append(QByteArray::number(128, 'f', 0));
                outgoing.append('\r');
            } else if (line == "RV" && mode == Mode_3010) {
                outgoing.append("VAC\r");
            } else if (line.startsWith('V') || line.startsWith('L') || line.startsWith('W')) {
                outgoing.append("OK\r");
            } else if (line == "X0" ||
                    line == "X1" ||
                    line == "X2" ||
                    line == "X3" ||
                    line == "X4" ||
                    line == "X5" ||
                    line == "X6" ||
                    line == "X7" ||
                    line == "X8") {
                outgoing.append("OK\r");
            } else if (line == "C") {
                for (int i = 0; i < 121; i++) {
                    outgoing.append("0,0\r");
                }
            } else if (line == "S") {
                for (int i = 0; i < 16; i++) {
                    outgoing.append("0\r");
                }
            } else if (line == "I") {
                outgoing.append("Calibration label\r");
            } else {
                outgoing.append("ERROR\r");
            }

            if (idxCR >= incoming.size() - 1) {
                incoming.clear();
                break;
            }
            incoming = incoming.mid(idxCR + 1);
        }

        accumulatorRemaining -= seconds;
        while (accumulatorRemaining <= 0.0) {
            accumulatorRemaining += 1.0;
            countSum += countRate;
            countSeconds++;

            if (enableUnpolled)
                writeD();
        }
    }
};

class TestComponent : public QObject {
Q_OBJECT

    AcquisitionComponent *component;
    ExternalConverterComponent *ingress;

    bool checkMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("F1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("N", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T1", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T2", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("T3", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("Q", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkRealtimeMeta(StreamCapture &stream, double time = FP::undefined())
    {
        if (!stream.hasMeta("C", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZND", Variant::Root(), QString(), time))
            return false;
        if (!stream.hasMeta("ZQ", Variant::Root(), QString(), time))
            return false;
        return true;
    }

    bool checkContiguous(StreamCapture &stream)
    {
        if (!stream.checkContiguous("F1"))
            return false;
        if (!stream.checkContiguous("N"))
            return false;
        if (!stream.checkContiguous("T1"))
            return false;
        if (!stream.checkContiguous("T2"))
            return false;
        if (!stream.checkContiguous("T3"))
            return false;
        if (!stream.checkContiguous("Q"))
            return false;
        return true;
    }

    bool checkValues(StreamCapture &stream, const ModelInstrument &model,
                     double time = FP::undefined(),
                     bool includeHousekeeping = true)
    {
        if (!stream.hasAnyMatchingValue("N", Variant::Root(model.concentration()), time))
            return false;
        if (includeHousekeeping) {
            if (!stream.hasAnyMatchingValue("T1", Variant::Root(model.Tsaturator), time))
                return false;
            if (!stream.hasAnyMatchingValue("T2", Variant::Root(model.Tcondenser), time))
                return false;
            if (!stream.hasAnyMatchingValue("T3", Variant::Root(model.Toptics), time))
                return false;
            if (!stream.hasAnyMatchingValue("Q", Variant::Root(model.flow), time))
                return false;
        }
        Variant::Flags flags;
        if (!model.liquidOk || !model.laserOk)
            flags.insert("InstrumentNotReady");
        if (!model.liquidOk)
            flags.insert("LiquidLow");
        if (!stream.hasAnyMatchingValue("F1", Variant::Root(std::move(flags)), time))
            return false;
        return true;
    }

    bool checkRealtimeValues(StreamCapture &stream, const ModelInstrument &model,
                             double time = FP::undefined(),
                             bool includeHousekeeping = true)
    {
        if (!stream.hasAnyMatchingValue("C", Variant::Root((double) model.countRate), time))
            return false;
        if (includeHousekeeping) {
            if (!stream.hasAnyMatchingValue("ZQ", Variant::Root(model.flow), time))
                return false;
        }
        return true;
    }

private slots:

    void initTestCase()
    {
        component = qobject_cast<AcquisitionComponent *>(
                ComponentLoader::create("acquire_tsi_cpc302x"));
        QVERIFY(component);
        ingress = qobject_cast<ExternalConverterComponent *>(
                ComponentLoader::create("acquire_tsi_cpc302x"));
        QVERIFY(ingress);

        Logging::suppressForTesting();
    }

    void options()
    {
        ComponentOptions options;
        options = component->getPassiveOptions();
    }

    void passiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.enableUnpolled = true;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        for (int i = 0; i < 100; i++) {
            control.advance(0.2);
            if (interface->getAutoprobeStatus() == AcquisitionInterface::AutoprobeStatus::Success)
                break;
            QTest::qSleep(50);
        }

        interface->autoprobeStatusUpdated
                 .wait([&] {
                     return interface->getAutoprobeStatus() ==
                             AcquisitionInterface::AutoprobeStatus::Success;
                 }, 30.0);
        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(0.2);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, FP::undefined(), false));
        QVERIFY(checkValues(realtime, instrument, FP::undefined(), false));
        QVERIFY(checkRealtimeValues(realtime, instrument, FP::undefined(), false));

    }

    void interactiveAutoprobe()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.mode = ModelInstrument::Mode_3022;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromote(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 100; i++) {
            control.advance(0.05);
            QTest::qSleep(50);
        }
        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        Variant::Write source = Variant::Write::empty();
        source.hash("CalibrationLabel").setString("Calibration label");
        source.hash("Model").setString("3022");
        source.hash("Manufacturer").setString("TSI");
        QVERIFY(logging.hasAnyMatchingMetaValue("F1", source, FP::undefined(), "^Source"));

    }

    void passiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        cv["Flow"].setDouble(0.31);
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.enableUnpolled = true;
        instrument.flow = 0.31;

        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionPassive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.5);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);
        control.advance(0.1);

        for (int i = 0; i < 100; i++) {
            control.advance(0.2);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        SequenceValue::Transfer persistentValues(interface->getPersistentValues());
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument, FP::undefined(), false));
        QVERIFY(checkValues(realtime, instrument, FP::undefined(), false));
        QVERIFY(checkRealtimeValues(realtime, instrument, FP::undefined(), false));

    }

    void interactiveAcquisition()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.laserOk = false;
        instrument.mode = ModelInstrument::Mode_3025;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionInteractive(config));
        QVERIFY(interface.get());
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->setState(&control);
        interface->setControlStream(&control);
        control.requestTimeout(0.0);
        interface->start();

        StreamCapture logging;
        StreamCapture realtime;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setRealtimeEgress(&realtime);
        interface->setPersistentEgress(&persistent);

        while (control.time() < 60.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("ZSTATE", Variant::Root("Run")))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 60.0);

        double checkTime = control.time();
        while (control.time() < 120.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        control.advance(0.125);
        checkTime = control.time();
        while (control.time() < 120.0) {
            control.advance(0.125);
            if (realtime.hasAnyMatchingValue("F1", Variant::Root(), checkTime))
                break;
            QTest::qSleep(50);
        }
        QVERIFY(control.time() < 120.0);

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

        Variant::Write source = Variant::Write::empty();
        source.hash("CalibrationLabel").setString("Calibration label");
        source.hash("Model").setString("3025");
        source.hash("Manufacturer").setString("TSI");
        QVERIFY(logging.hasAnyMatchingMetaValue("F1", source, FP::undefined(), "^Source"));

    }

    void passiveAutoprobeExternal()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        StreamCapture realtime;
        interface->setRealtimeEgress(&realtime);

        while (control.time() < 60.0) {
            control.externalControl("DC\r");
            control.advance(0.1);
            QTest::qSleep(50);
            control.externalControl("R1\r");
            control.advance(0.1);
            QTest::qSleep(50);
            control.externalControl("R2\r");
            control.advance(0.1);
            QTest::qSleep(50);
            control.externalControl("R3\r");
            control.advance(0.1);
            QTest::qSleep(50);
            control.externalControl("R4\r");
            control.advance(0.3);
            QTest::qSleep(50);
            control.advance(0.3);
            QTest::qSleep(50);
        }

        while (control.time() < 120.0) {
            control.externalControl("DC\r");
            control.advance(5.0);
            if (realtime.hasAnyMatchingMetaValue("N"))
                break;
            control.externalControl("DC\r");
            control.advance(0.1);
            QTest::qSleep(50);
            control.externalControl("R1\r");
            control.advance(0.1);
            QTest::qSleep(50);
            control.externalControl("R2\r");
            control.advance(0.1);
            QTest::qSleep(50);
            control.externalControl("R3\r");
            control.advance(0.1);
            QTest::qSleep(50);
            control.externalControl("R4\r");
            control.advance(0.3);
            QTest::qSleep(50);
            control.advance(0.3);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Success);

        StreamCapture logging;
        StreamCapture persistent;

        interface->setLoggingEgress(&logging);
        interface->setPersistentEgress(&persistent);
        interface->autoprobePromotePassive(control.time());

        SequenceValue::Transfer persistentValues(interface->getPersistentValues());

        for (int i = 0; i < 10; i++) {
            control.externalControl("DC\r");
            control.advance(0.1);
            QTest::qSleep(50);
            control.externalControl("R1\r");
            control.advance(0.1);
            QTest::qSleep(50);
            control.externalControl("R2\r");
            control.advance(0.1);
            QTest::qSleep(50);
            control.externalControl("R3\r");
            control.advance(0.1);
            QTest::qSleep(50);
            control.externalControl("R4\r");
            control.advance(0.3);
            QTest::qSleep(50);
            control.advance(0.3);
            QTest::qSleep(50);
        }

        interface->initiateShutdown();
        interface->wait();
        interface.reset();

        QVERIFY(logging.checkAscending());
        QVERIFY(checkContiguous(logging));

        QVERIFY(checkMeta(logging));
        QVERIFY(checkMeta(realtime));
        QVERIFY(checkRealtimeMeta(realtime));

        QVERIFY(checkValues(logging, instrument));
        QVERIFY(checkValues(realtime, instrument));
        QVERIFY(checkRealtimeValues(realtime, instrument));

    }

    void interactiveAutoprobeDisambiguate()
    {
        ValueSegment::Transfer config;
        Variant::Root cv;
        config.emplace_back(FP::undefined(), FP::undefined(), cv);

        ModelInstrument instrument;
        instrument.mode = ModelInstrument::Mode_3010;
        std::unique_ptr<AcquisitionInterface>
                interface(component->createAcquisitionAutoprobe(config));
        QVERIFY(interface.get());
        int autoprobeUpdated = 0;
        interface->autoprobeStatusUpdated.connect([&]() { ++autoprobeUpdated; });
        ModelInstrumentControl<ModelInstrument> control(instrument, interface.get());
        interface->start();

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        interface->setState(&control);
        interface->setControlStream(&control);
        interface->autoprobeTryInteractive(0.0);

        QCOMPARE(interface->getAutoprobeStatus(),
                 AcquisitionInterface::AutoprobeStatus::InProgress);

        ElapsedTimer realTime;
        realTime.start();
        for (; realTime.elapsed() < 30000 &&
                interface->getAutoprobeStatus() ==
                        AcquisitionInterface::AutoprobeStatus::InProgress;) {
            control.advance(0.1);
            QTest::qSleep(50);
        }

        QVERIFY(autoprobeUpdated > 0);
        QCOMPARE(interface->getAutoprobeStatus(), AcquisitionInterface::AutoprobeStatus::Failure);

        interface->initiateShutdown();
        interface->wait();
        interface.reset();
    }
};

QTEST_MAIN(TestComponent)

#include "test.moc"
